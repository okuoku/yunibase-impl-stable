/*===========================================================================*/
/*   (Heap/restore.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Heap/restore.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_HEAP_RESTORE_TYPE_DEFINITIONS
#define BGL_HEAP_RESTORE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_HEAP_RESTORE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	extern obj_t BGl_setzd2genvz12zc0zzast_envz00(obj_t);
	static obj_t BGl_z62restorezd2heapzb0zzheap_restorez00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzheap_restorez00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31202ze3ze5zzheap_restorez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_restorezd2additionalzd2heapsz00zzheap_restorez00(void);
	extern obj_t BGl_za2cczd2optionsza2zd2zzengine_paramz00;
	BGL_IMPORT bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	static obj_t BGl_z62heapzd2modulezd2listz62zzheap_restorez00(obj_t, obj_t);
	extern obj_t BGl_za2passza2z00zzengine_paramz00;
	static obj_t BGl_toplevelzd2initzd2zzheap_restorez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_restorezd2additionalzd2heapz00zzheap_restorez00(obj_t);
	extern obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t input_obj(obj_t);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzheap_restorez00(void);
	static obj_t BGl_objectzd2initzd2zzheap_restorez00(void);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_addzd2genvz12zc0zzast_envz00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2binaryzd2filezd2zz__binaryz00(obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_unbindzd2callzf2ccz12z32zzheap_restorez00(void);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzheap_restorez00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzheap_restorez00(void);
	static obj_t BGl_z62zc3z04anonymousza31316ze3ze5zzheap_restorez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31308ze3ze5zzheap_restorez00(obj_t,
		obj_t);
	extern obj_t BGl_za2additionalzd2includezd2foreignza2z00zzengine_paramz00;
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	BGL_IMPORT obj_t close_binary_port(obj_t);
	static obj_t BGl_za2heapzd2modulezd2listza2z00zzheap_restorez00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31328ze3ze5zzheap_restorez00(obj_t);
	extern obj_t BGl_unbindzd2globalz12zc0zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_za2bigloozd2versionza2zd2zzengine_paramz00;
	static obj_t BGl_z62zc3z04anonymousza31184ze3ze5zzheap_restorez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_restorezd2heapzd2zzheap_restorez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzheap_restorez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_jvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinit_mainz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_enginez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__binaryz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62heapzd2filezd2namez62zzheap_restorez00(obj_t, obj_t);
	extern obj_t BGl_za2heapzd2nameza2zd2zzengine_paramz00;
	extern obj_t BGl_za2additionalzd2heapzd2namesza2z00zzengine_paramz00;
	static obj_t BGl_z62zc3z04anonymousza31195ze3ze5zzheap_restorez00(obj_t,
		obj_t);
	extern obj_t BGl_setzd2tenvz12zc0zztype_envz00(obj_t);
	extern obj_t BGl_za2libzd2dirza2zd2zzengine_paramz00;
	static obj_t BGl_z62restorezd2additionalzd2heapz62zzheap_restorez00(obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzheap_restorez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzheap_restorez00(void);
	extern obj_t BGl_za2unsafezd2heapza2zd2zzengine_paramz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzheap_restorez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzheap_restorez00(void);
	BGL_IMPORT obj_t BGl_hashtablezd2forzd2eachz00zz__hashz00(obj_t, obj_t);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	static obj_t BGl_za2restoredzd2heapzd2namesza2z00zzheap_restorez00 = BUNSPEC;
	static obj_t BGl_z62restorezd2additionalzd2heapsz62zzheap_restorez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_heapzd2filezd2namez00zzheap_restorez00(obj_t);
	BGL_IMPORT obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_za2callzf2cczf3za2z01zzengine_paramz00;
	BGL_IMPORT obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
	extern obj_t BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_addzd2tenvz12zc0zztype_envz00(obj_t);
	extern obj_t BGl_za2bigloozd2specificzd2versionza2z00zzengine_paramz00;
	BGL_IMPORT obj_t
		BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_heapzd2modulezd2listz00zzheap_restorez00(obj_t);
	extern obj_t BGl_compilerzd2exitzd2zzinit_mainz00(obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static bool_t BGl_backendzd2heapzd2compatiblezf3zf3zzheap_restorez00(obj_t);
	static obj_t BGl_za2heapzd2markza2zd2zzheap_restorez00 = BUNSPEC;
	static obj_t __cnst[6];


	   
		 
		DEFINE_STRING(BGl_string1402z00zzheap_restorez00,
		BgL_bgl_string1402za700za7za7h1409za7, "-", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_restorezd2additionalzd2heapszd2envzd2zzheap_restorez00,
		BgL_bgl_za762restoreza7d2add1410z00,
		BGl_z62restorezd2additionalzd2heapsz62zzheap_restorez00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1403z00zzheap_restorez00,
		BgL_bgl_string1403za700za7za7h1411za7, "heap_restore", 12);
	      DEFINE_STRING(BGl_string1404z00zzheap_restorez00,
		BgL_bgl_string1404za700za7za7h1412za7,
		"make-heap call-with-current-continuation __r4_control_features_6_9 call/cc pass-started heap-mark ",
		98);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1400z00zzheap_restorez00,
		BgL_bgl_za762za7c3za704anonymo1413za7,
		BGl_z62zc3z04anonymousza31308ze3ze5zzheap_restorez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1401z00zzheap_restorez00,
		BgL_bgl_za762za7c3za704anonymo1414za7,
		BGl_z62zc3z04anonymousza31316ze3ze5zzheap_restorez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_heapzd2modulezd2listzd2envzd2zzheap_restorez00,
		BgL_bgl_za762heapza7d2module1415z00, va_generic_entry,
		BGl_z62heapzd2modulezd2listz62zzheap_restorez00, BUNSPEC, -1);
	      DEFINE_STRING(BGl_string1383z00zzheap_restorez00,
		BgL_bgl_string1383za700za7za7h1416za7, "Heap", 4);
	      DEFINE_STRING(BGl_string1384z00zzheap_restorez00,
		BgL_bgl_string1384za700za7za7h1417za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1385z00zzheap_restorez00,
		BgL_bgl_string1385za700za7za7h1418za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1386z00zzheap_restorez00,
		BgL_bgl_string1386za700za7za7h1419za7, "]", 1);
	      DEFINE_STRING(BGl_string1387z00zzheap_restorez00,
		BgL_bgl_string1387za700za7za7h1420za7, "      [reading ", 15);
	      DEFINE_STRING(BGl_string1388z00zzheap_restorez00,
		BgL_bgl_string1388za700za7za7h1421za7, "Corrupted heap", 14);
	      DEFINE_STRING(BGl_string1389z00zzheap_restorez00,
		BgL_bgl_string1389za700za7za7h1422za7, "~a vs. ~a", 9);
	      DEFINE_STRING(BGl_string1390z00zzheap_restorez00,
		BgL_bgl_string1390za700za7za7h1423za7, "Target language mismatch", 24);
	      DEFINE_STRING(BGl_string1391z00zzheap_restorez00,
		BgL_bgl_string1391za700za7za7h1424za7, "Heap is `~a', Bigloo is `~a'", 28);
	      DEFINE_STRING(BGl_string1392z00zzheap_restorez00,
		BgL_bgl_string1392za700za7za7h1425za7, "Release mismatch", 16);
	      DEFINE_STRING(BGl_string1393z00zzheap_restorez00,
		BgL_bgl_string1393za700za7za7h1426za7, "Specific version mismatch", 25);
	      DEFINE_STRING(BGl_string1396z00zzheap_restorez00,
		BgL_bgl_string1396za700za7za7h1427za7, "Cannot open heap file ~s", 24);
	      DEFINE_STRING(BGl_string1397z00zzheap_restorez00,
		BgL_bgl_string1397za700za7za7h1428za7, "restore-heap", 12);
	      DEFINE_STRING(BGl_string1398z00zzheap_restorez00,
		BgL_bgl_string1398za700za7za7h1429za7, ".", 1);
	      DEFINE_STRING(BGl_string1399z00zzheap_restorez00,
		BgL_bgl_string1399za700za7za7h1430za7, "Library", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1394z00zzheap_restorez00,
		BgL_bgl_za762za7c3za704anonymo1431za7,
		BGl_z62zc3z04anonymousza31184ze3ze5zzheap_restorez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1395z00zzheap_restorez00,
		BgL_bgl_za762za7c3za704anonymo1432za7,
		BGl_z62zc3z04anonymousza31195ze3ze5zzheap_restorez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_restorezd2heapzd2envz00zzheap_restorez00,
		BgL_bgl_za762restoreza7d2hea1433z00,
		BGl_z62restorezd2heapzb0zzheap_restorez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_restorezd2additionalzd2heapzd2envzd2zzheap_restorez00,
		BgL_bgl_za762restoreza7d2add1434z00,
		BGl_z62restorezd2additionalzd2heapz62zzheap_restorez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_heapzd2filezd2namezd2envzd2zzheap_restorez00,
		BgL_bgl_za762heapza7d2fileza7d1435za7,
		BGl_z62heapzd2filezd2namez62zzheap_restorez00, 0L, BUNSPEC, 1);
	BGL_IMPORT obj_t BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzheap_restorez00));
		   
			 ADD_ROOT((void *) (&BGl_za2heapzd2modulezd2listza2z00zzheap_restorez00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2restoredzd2heapzd2namesza2z00zzheap_restorez00));
		     ADD_ROOT((void *) (&BGl_za2heapzd2markza2zd2zzheap_restorez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzheap_restorez00(long
		BgL_checksumz00_925, char *BgL_fromz00_926)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzheap_restorez00))
				{
					BGl_requirezd2initializa7ationz75zzheap_restorez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzheap_restorez00();
					BGl_libraryzd2moduleszd2initz00zzheap_restorez00();
					BGl_cnstzd2initzd2zzheap_restorez00();
					BGl_importedzd2moduleszd2initz00zzheap_restorez00();
					return BGl_toplevelzd2initzd2zzheap_restorez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzheap_restorez00(void)
	{
		{	/* Heap/restore.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "heap_restore");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"heap_restore");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"heap_restore");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"heap_restore");
			BGl_modulezd2initializa7ationz75zz__binaryz00(0L, "heap_restore");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "heap_restore");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"heap_restore");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "heap_restore");
			BGl_modulezd2initializa7ationz75zz__hashz00(0L, "heap_restore");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "heap_restore");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"heap_restore");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "heap_restore");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "heap_restore");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "heap_restore");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzheap_restorez00(void)
	{
		{	/* Heap/restore.scm 15 */
			{	/* Heap/restore.scm 15 */
				obj_t BgL_cportz00_898;

				{	/* Heap/restore.scm 15 */
					obj_t BgL_stringz00_905;

					BgL_stringz00_905 = BGl_string1404z00zzheap_restorez00;
					{	/* Heap/restore.scm 15 */
						obj_t BgL_startz00_906;

						BgL_startz00_906 = BINT(0L);
						{	/* Heap/restore.scm 15 */
							obj_t BgL_endz00_907;

							BgL_endz00_907 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_905)));
							{	/* Heap/restore.scm 15 */

								BgL_cportz00_898 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_905, BgL_startz00_906, BgL_endz00_907);
				}}}}
				{
					long BgL_iz00_899;

					BgL_iz00_899 = 5L;
				BgL_loopz00_900:
					if ((BgL_iz00_899 == -1L))
						{	/* Heap/restore.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Heap/restore.scm 15 */
							{	/* Heap/restore.scm 15 */
								obj_t BgL_arg1408z00_901;

								{	/* Heap/restore.scm 15 */

									{	/* Heap/restore.scm 15 */
										obj_t BgL_locationz00_903;

										BgL_locationz00_903 = BBOOL(((bool_t) 0));
										{	/* Heap/restore.scm 15 */

											BgL_arg1408z00_901 =
												BGl_readz00zz__readerz00(BgL_cportz00_898,
												BgL_locationz00_903);
										}
									}
								}
								{	/* Heap/restore.scm 15 */
									int BgL_tmpz00_958;

									BgL_tmpz00_958 = (int) (BgL_iz00_899);
									CNST_TABLE_SET(BgL_tmpz00_958, BgL_arg1408z00_901);
							}}
							{	/* Heap/restore.scm 15 */
								int BgL_auxz00_904;

								BgL_auxz00_904 = (int) ((BgL_iz00_899 - 1L));
								{
									long BgL_iz00_963;

									BgL_iz00_963 = (long) (BgL_auxz00_904);
									BgL_iz00_899 = BgL_iz00_963;
									goto BgL_loopz00_900;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzheap_restorez00(void)
	{
		{	/* Heap/restore.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzheap_restorez00(void)
	{
		{	/* Heap/restore.scm 15 */
			BGl_za2restoredzd2heapzd2namesza2z00zzheap_restorez00 = BNIL;
			BGl_za2heapzd2modulezd2listza2z00zzheap_restorez00 = BNIL;
			return (BGl_za2heapzd2markza2zd2zzheap_restorez00 =
				CNST_TABLE_REF(0), BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzheap_restorez00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_535;

				BgL_headz00_535 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_536;
					obj_t BgL_tailz00_537;

					BgL_prevz00_536 = BgL_headz00_535;
					BgL_tailz00_537 = BgL_l1z00_1;
				BgL_loopz00_538:
					if (PAIRP(BgL_tailz00_537))
						{
							obj_t BgL_newzd2prevzd2_540;

							BgL_newzd2prevzd2_540 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_537), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_536, BgL_newzd2prevzd2_540);
							{
								obj_t BgL_tailz00_974;
								obj_t BgL_prevz00_973;

								BgL_prevz00_973 = BgL_newzd2prevzd2_540;
								BgL_tailz00_974 = CDR(BgL_tailz00_537);
								BgL_tailz00_537 = BgL_tailz00_974;
								BgL_prevz00_536 = BgL_prevz00_973;
								goto BgL_loopz00_538;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_535);
				}
			}
		}

	}



/* backend-heap-compatible? */
	bool_t BGl_backendzd2heapzd2compatiblezf3zf3zzheap_restorez00(obj_t
		BgL_targetz00_3)
	{
		{	/* Heap/restore.scm 37 */
			{	/* Heap/restore.scm 38 */
				BgL_backendz00_bglt BgL_i1065z00_543;

				BgL_i1065z00_543 =
					((BgL_backendz00_bglt) BGl_thezd2backendzd2zzbackend_backendz00());
				{	/* Heap/restore.scm 39 */
					bool_t BgL__ortest_1066z00_544;

					BgL__ortest_1066z00_544 =
						(BgL_targetz00_3 ==
						(((BgL_backendz00_bglt) COBJECT(BgL_i1065z00_543))->
							BgL_languagez00));
					if (BgL__ortest_1066z00_544)
						{	/* Heap/restore.scm 39 */
							return BgL__ortest_1066z00_544;
						}
					else
						{	/* Heap/restore.scm 39 */
							return
								(BgL_targetz00_3 ==
								(((BgL_backendz00_bglt) COBJECT(BgL_i1065z00_543))->
									BgL_heapzd2compatiblezd2));
						}
				}
			}
		}

	}



/* restore-heap */
	BGL_EXPORTED_DEF obj_t BGl_restorezd2heapzd2zzheap_restorez00(void)
	{
		{	/* Heap/restore.scm 44 */
			if (STRINGP(BGl_za2heapzd2nameza2zd2zzengine_paramz00))
				{	/* Heap/restore.scm 45 */
					{	/* Heap/restore.scm 46 */
						obj_t BgL_list1125z00_548;

						{	/* Heap/restore.scm 46 */
							obj_t BgL_arg1126z00_549;

							{	/* Heap/restore.scm 46 */
								obj_t BgL_arg1127z00_550;

								BgL_arg1127z00_550 =
									MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
								BgL_arg1126z00_549 =
									MAKE_YOUNG_PAIR(BGl_string1383z00zzheap_restorez00,
									BgL_arg1127z00_550);
							}
							BgL_list1125z00_548 =
								MAKE_YOUNG_PAIR(BGl_string1384z00zzheap_restorez00,
								BgL_arg1126z00_549);
						}
						BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1125z00_548);
					}
					BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
					BGl_za2currentzd2passza2zd2zzengine_passz00 =
						BGl_string1383z00zzheap_restorez00;
					{	/* Heap/restore.scm 46 */
						obj_t BgL_g1067z00_551;

						BgL_g1067z00_551 = BNIL;
						{
							obj_t BgL_hooksz00_554;
							obj_t BgL_hnamesz00_555;

							BgL_hooksz00_554 = BgL_g1067z00_551;
							BgL_hnamesz00_555 = BNIL;
						BgL_zc3z04anonymousza31128ze3z87_556:
							if (NULLP(BgL_hooksz00_554))
								{	/* Heap/restore.scm 46 */
									CNST_TABLE_REF(1);
								}
							else
								{	/* Heap/restore.scm 46 */
									bool_t BgL_test1442z00_996;

									{	/* Heap/restore.scm 46 */
										obj_t BgL_fun1139z00_563;

										BgL_fun1139z00_563 = CAR(((obj_t) BgL_hooksz00_554));
										BgL_test1442z00_996 =
											CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1139z00_563));
									}
									if (BgL_test1442z00_996)
										{	/* Heap/restore.scm 46 */
											obj_t BgL_arg1132z00_560;
											obj_t BgL_arg1137z00_561;

											BgL_arg1132z00_560 = CDR(((obj_t) BgL_hooksz00_554));
											BgL_arg1137z00_561 = CDR(((obj_t) BgL_hnamesz00_555));
											{
												obj_t BgL_hnamesz00_1008;
												obj_t BgL_hooksz00_1007;

												BgL_hooksz00_1007 = BgL_arg1132z00_560;
												BgL_hnamesz00_1008 = BgL_arg1137z00_561;
												BgL_hnamesz00_555 = BgL_hnamesz00_1008;
												BgL_hooksz00_554 = BgL_hooksz00_1007;
												goto BgL_zc3z04anonymousza31128ze3z87_556;
											}
										}
									else
										{	/* Heap/restore.scm 46 */
											obj_t BgL_arg1138z00_562;

											BgL_arg1138z00_562 = CAR(((obj_t) BgL_hnamesz00_555));
											BGl_internalzd2errorzd2zztools_errorz00
												(BGl_string1383z00zzheap_restorez00,
												BGl_string1385z00zzheap_restorez00, BgL_arg1138z00_562);
										}
								}
						}
					}
					{	/* Heap/restore.scm 47 */
						obj_t BgL_fnamez00_566;

						BgL_fnamez00_566 =
							BGl_findzd2filezf2pathz20zz__osz00
							(BGl_za2heapzd2nameza2zd2zzengine_paramz00,
							BGl_za2libzd2dirza2zd2zzengine_paramz00);
						if (STRINGP(BgL_fnamez00_566))
							{	/* Heap/restore.scm 49 */
								obj_t BgL_portz00_568;

								BgL_portz00_568 =
									BGl_openzd2inputzd2binaryzd2filezd2zz__binaryz00
									(BgL_fnamez00_566);
								if (BINARY_PORTP(BgL_portz00_568))
									{	/* Heap/restore.scm 50 */
										{	/* Heap/restore.scm 55 */
											obj_t BgL_list1143z00_570;

											{	/* Heap/restore.scm 55 */
												obj_t BgL_arg1145z00_571;

												{	/* Heap/restore.scm 55 */
													obj_t BgL_arg1148z00_572;

													{	/* Heap/restore.scm 55 */
														obj_t BgL_arg1149z00_573;

														BgL_arg1149z00_573 =
															MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)),
															BNIL);
														BgL_arg1148z00_572 =
															MAKE_YOUNG_PAIR
															(BGl_string1386z00zzheap_restorez00,
															BgL_arg1149z00_573);
													}
													BgL_arg1145z00_571 =
														MAKE_YOUNG_PAIR(BgL_fnamez00_566,
														BgL_arg1148z00_572);
												}
												BgL_list1143z00_570 =
													MAKE_YOUNG_PAIR(BGl_string1387z00zzheap_restorez00,
													BgL_arg1145z00_571);
											}
											BGl_verbosez00zztools_speekz00(BINT(2L),
												BgL_list1143z00_570);
										}
										{	/* Heap/restore.scm 56 */
											obj_t BgL_exitd1069z00_574;

											BgL_exitd1069z00_574 = BGL_EXITD_TOP_AS_OBJ();
											{	/* Heap/restore.scm 110 */
												obj_t BgL_zc3z04anonymousza31202ze3z87_870;

												BgL_zc3z04anonymousza31202ze3z87_870 =
													MAKE_FX_PROCEDURE
													(BGl_z62zc3z04anonymousza31202ze3ze5zzheap_restorez00,
													(int) (0L), (int) (1L));
												PROCEDURE_SET(BgL_zc3z04anonymousza31202ze3z87_870,
													(int) (0L), BgL_portz00_568);
												{	/* Heap/restore.scm 56 */
													obj_t BgL_arg1828z00_796;

													{	/* Heap/restore.scm 56 */
														obj_t BgL_arg1829z00_797;

														BgL_arg1829z00_797 =
															BGL_EXITD_PROTECT(BgL_exitd1069z00_574);
														BgL_arg1828z00_796 =
															MAKE_YOUNG_PAIR
															(BgL_zc3z04anonymousza31202ze3z87_870,
															BgL_arg1829z00_797);
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1069z00_574,
														BgL_arg1828z00_796);
													BUNSPEC;
												}
												{	/* Heap/restore.scm 57 */
													bool_t BgL_tmp1071z00_576;

													{	/* Heap/restore.scm 57 */
														obj_t BgL_envsz00_577;

														BgL_envsz00_577 = input_obj(BgL_portz00_568);
														{	/* Heap/restore.scm 57 */
															obj_t BgL__z00_578;

															{	/* Heap/restore.scm 58 */
																bool_t BgL_test1445z00_1035;

																if (VECTORP(BgL_envsz00_577))
																	{	/* Heap/restore.scm 58 */
																		BgL_test1445z00_1035 =
																			(VECTOR_LENGTH(BgL_envsz00_577) == 5L);
																	}
																else
																	{	/* Heap/restore.scm 58 */
																		BgL_test1445z00_1035 = ((bool_t) 0);
																	}
																if (BgL_test1445z00_1035)
																	{	/* Heap/restore.scm 58 */
																		BgL__z00_578 = BFALSE;
																	}
																else
																	{	/* Heap/restore.scm 58 */
																		BgL__z00_578 =
																			BGl_errorz00zz__errorz00
																			(BGl_za2heapzd2nameza2zd2zzengine_paramz00,
																			BGl_string1388z00zzheap_restorez00,
																			BgL_envsz00_577);
																	}
															}
															{	/* Heap/restore.scm 58 */
																obj_t BgL_targetz00_579;

																BgL_targetz00_579 =
																	VECTOR_REF(((obj_t) BgL_envsz00_577), 0L);
																{	/* Heap/restore.scm 63 */
																	obj_t BgL_versionz00_580;

																	BgL_versionz00_580 =
																		VECTOR_REF(((obj_t) BgL_envsz00_577), 1L);
																	{	/* Heap/restore.scm 64 */
																		obj_t BgL_specificz00_581;

																		BgL_specificz00_581 =
																			VECTOR_REF(((obj_t) BgL_envsz00_577), 2L);
																		{	/* Heap/restore.scm 65 */
																			obj_t BgL_genvz00_582;

																			BgL_genvz00_582 =
																				VECTOR_REF(
																				((obj_t) BgL_envsz00_577), 3L);
																			{	/* Heap/restore.scm 66 */
																				obj_t BgL_tenvz00_583;

																				BgL_tenvz00_583 =
																					VECTOR_REF(
																					((obj_t) BgL_envsz00_577), 4L);
																				{	/* Heap/restore.scm 67 */

																					if (BGl_backendzd2heapzd2compatiblezf3zf3zzheap_restorez00(BgL_targetz00_579))
																						{	/* Heap/restore.scm 69 */
																							BFALSE;
																						}
																					else
																						{	/* Heap/restore.scm 74 */
																							obj_t BgL_arg1152z00_585;

																							{	/* Heap/restore.scm 74 */
																								obj_t BgL_arg1153z00_586;

																								{	/* Heap/restore.scm 74 */
																									obj_t BgL_arg1158z00_589;

																									BgL_arg1158z00_589 =
																										BGl_thezd2backendzd2zzbackend_backendz00
																										();
																									BgL_arg1153z00_586 =
																										(((BgL_backendz00_bglt)
																											COBJECT((
																													(BgL_backendz00_bglt)
																													BgL_arg1158z00_589)))->
																										BgL_languagez00);
																								}
																								{	/* Heap/restore.scm 72 */
																									obj_t BgL_list1154z00_587;

																									{	/* Heap/restore.scm 72 */
																										obj_t BgL_arg1157z00_588;

																										BgL_arg1157z00_588 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1153z00_586,
																											BNIL);
																										BgL_list1154z00_587 =
																											MAKE_YOUNG_PAIR
																											(BgL_targetz00_579,
																											BgL_arg1157z00_588);
																									}
																									BgL_arg1152z00_585 =
																										BGl_formatz00zz__r4_output_6_10_3z00
																										(BGl_string1389z00zzheap_restorez00,
																										BgL_list1154z00_587);
																								}
																							}
																							BGl_errorz00zz__errorz00
																								(BGl_za2heapzd2nameza2zd2zzengine_paramz00,
																								BGl_string1390z00zzheap_restorez00,
																								BgL_arg1152z00_585);
																						}
																					{	/* Heap/restore.scm 75 */
																						bool_t BgL_test1448z00_1060;

																						if (CBOOL
																							(BGl_za2unsafezd2heapza2zd2zzengine_paramz00))
																							{	/* Heap/restore.scm 75 */
																								BgL_test1448z00_1060 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Heap/restore.scm 75 */
																								BgL_test1448z00_1060 =
																									BGl_equalzf3zf3zz__r4_equivalence_6_2z00
																									(BgL_versionz00_580,
																									BGl_za2bigloozd2versionza2zd2zzengine_paramz00);
																							}
																						if (BgL_test1448z00_1060)
																							{	/* Heap/restore.scm 75 */
																								BFALSE;
																							}
																						else
																							{	/* Heap/restore.scm 79 */
																								obj_t BgL_arg1162z00_591;

																								{	/* Heap/restore.scm 79 */
																									obj_t BgL_list1163z00_592;

																									{	/* Heap/restore.scm 79 */
																										obj_t BgL_arg1164z00_593;

																										BgL_arg1164z00_593 =
																											MAKE_YOUNG_PAIR
																											(BGl_za2bigloozd2versionza2zd2zzengine_paramz00,
																											BNIL);
																										BgL_list1163z00_592 =
																											MAKE_YOUNG_PAIR
																											(BgL_versionz00_580,
																											BgL_arg1164z00_593);
																									}
																									BgL_arg1162z00_591 =
																										BGl_formatz00zz__r4_output_6_10_3z00
																										(BGl_string1391z00zzheap_restorez00,
																										BgL_list1163z00_592);
																								}
																								BGl_errorz00zz__errorz00
																									(BGl_za2heapzd2nameza2zd2zzengine_paramz00,
																									BGl_string1392z00zzheap_restorez00,
																									BgL_arg1162z00_591);
																							}
																					}
																					{	/* Heap/restore.scm 82 */
																						bool_t BgL_test1450z00_1068;

																						if (CBOOL
																							(BGl_za2unsafezd2heapza2zd2zzengine_paramz00))
																							{	/* Heap/restore.scm 82 */
																								BgL_test1450z00_1068 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Heap/restore.scm 82 */
																								BgL_test1450z00_1068 =
																									BGl_equalzf3zf3zz__r4_equivalence_6_2z00
																									(BgL_specificz00_581,
																									BGl_za2bigloozd2specificzd2versionza2z00zzengine_paramz00);
																							}
																						if (BgL_test1450z00_1068)
																							{	/* Heap/restore.scm 82 */
																								BFALSE;
																							}
																						else
																							{	/* Heap/restore.scm 86 */
																								obj_t BgL_arg1166z00_595;

																								{	/* Heap/restore.scm 86 */
																									obj_t BgL_list1167z00_596;

																									{	/* Heap/restore.scm 86 */
																										obj_t BgL_arg1171z00_597;

																										BgL_arg1171z00_597 =
																											MAKE_YOUNG_PAIR
																											(BGl_za2bigloozd2specificzd2versionza2z00zzengine_paramz00,
																											BNIL);
																										BgL_list1167z00_596 =
																											MAKE_YOUNG_PAIR
																											(BgL_specificz00_581,
																											BgL_arg1171z00_597);
																									}
																									BgL_arg1166z00_595 =
																										BGl_formatz00zz__r4_output_6_10_3z00
																										(BGl_string1391z00zzheap_restorez00,
																										BgL_list1167z00_596);
																								}
																								BGl_errorz00zz__errorz00
																									(BGl_za2heapzd2nameza2zd2zzengine_paramz00,
																									BGl_string1393z00zzheap_restorez00,
																									BgL_arg1166z00_595);
																							}
																					}
																					BGl_setzd2genvz12zc0zzast_envz00
																						(BgL_genvz00_582);
																					BGl_setzd2tenvz12zc0zztype_envz00
																						(BgL_tenvz00_583);
																					if (CBOOL
																						(BGl_za2callzf2cczf3za2z01zzengine_paramz00))
																						{	/* Heap/restore.scm 94 */
																							BFALSE;
																						}
																					else
																						{	/* Heap/restore.scm 94 */
																							BGl_unbindzd2callzf2ccz12z32zzheap_restorez00
																								();
																						}
																					{	/* Heap/restore.scm 97 */
																						bool_t BgL_test1455z00_1081;

																						{	/* Heap/restore.scm 97 */
																							obj_t BgL_arg1191z00_609;

																							BgL_arg1191z00_609 =
																								BGl_thezd2backendzd2zzbackend_backendz00
																								();
																							BgL_test1455z00_1081 =
																								(((BgL_backendz00_bglt)
																									COBJECT(((BgL_backendz00_bglt)
																											BgL_arg1191z00_609)))->
																								BgL_qualifiedzd2typeszd2);
																						}
																						if (BgL_test1455z00_1081)
																							{	/* Heap/restore.scm 97 */
																								BGl_forzd2eachzd2globalz12z12zzast_envz00
																									(BGl_proc1394z00zzheap_restorez00,
																									BNIL);
																								BgL_genvz00_582;
																							}
																						else
																							{	/* Heap/restore.scm 97 */
																								BFALSE;
																							}
																					}
																					BGl_forzd2eachzd2globalz12z12zzast_envz00
																						(BGl_proc1395z00zzheap_restorez00,
																						BNIL);
																					BgL_tmp1071z00_576 = ((bool_t) 1);
																				}
																			}
																		}
																	}
																}
															}
														}
													}
													{	/* Heap/restore.scm 56 */
														bool_t BgL_test1456z00_1087;

														{	/* Heap/restore.scm 56 */
															obj_t BgL_arg1827z00_814;

															BgL_arg1827z00_814 =
																BGL_EXITD_PROTECT(BgL_exitd1069z00_574);
															BgL_test1456z00_1087 = PAIRP(BgL_arg1827z00_814);
														}
														if (BgL_test1456z00_1087)
															{	/* Heap/restore.scm 56 */
																obj_t BgL_arg1825z00_815;

																{	/* Heap/restore.scm 56 */
																	obj_t BgL_arg1826z00_816;

																	BgL_arg1826z00_816 =
																		BGL_EXITD_PROTECT(BgL_exitd1069z00_574);
																	BgL_arg1825z00_815 =
																		CDR(((obj_t) BgL_arg1826z00_816));
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1069z00_574,
																	BgL_arg1825z00_815);
																BUNSPEC;
															}
														else
															{	/* Heap/restore.scm 56 */
																BFALSE;
															}
													}
													close_binary_port(BgL_portz00_568);
													return BBOOL(BgL_tmp1071z00_576);
												}
											}
										}
									}
								else
									{	/* Heap/restore.scm 51 */
										obj_t BgL_mz00_624;

										{	/* Heap/restore.scm 51 */
											obj_t BgL_list1203z00_625;

											BgL_list1203z00_625 =
												MAKE_YOUNG_PAIR(BgL_fnamez00_566, BNIL);
											BgL_mz00_624 =
												BGl_formatz00zz__r4_output_6_10_3z00
												(BGl_string1396z00zzheap_restorez00,
												BgL_list1203z00_625);
										}
										BGl_errorz00zz__errorz00(BGl_string1397z00zzheap_restorez00,
											BgL_mz00_624, BGl_za2libzd2dirza2zd2zzengine_paramz00);
										return BGl_compilerzd2exitzd2zzinit_mainz00(BINT(5L));
									}
							}
						else
							{	/* Heap/restore.scm 111 */
								obj_t BgL_mz00_626;

								{	/* Heap/restore.scm 111 */
									obj_t BgL_list1204z00_627;

									BgL_list1204z00_627 =
										MAKE_YOUNG_PAIR(BGl_za2heapzd2nameza2zd2zzengine_paramz00,
										BNIL);
									BgL_mz00_626 =
										BGl_formatz00zz__r4_output_6_10_3z00
										(BGl_string1396z00zzheap_restorez00, BgL_list1204z00_627);
								}
								BGl_errorz00zz__errorz00(BGl_string1397z00zzheap_restorez00,
									BgL_mz00_626, BGl_za2libzd2dirza2zd2zzengine_paramz00);
								return BGl_compilerzd2exitzd2zzinit_mainz00(BINT(5L));
							}
					}
				}
			else
				{	/* Heap/restore.scm 45 */
					return BFALSE;
				}
		}

	}



/* &restore-heap */
	obj_t BGl_z62restorezd2heapzb0zzheap_restorez00(obj_t BgL_envz00_871)
	{
		{	/* Heap/restore.scm 44 */
			return BGl_restorezd2heapzd2zzheap_restorez00();
		}

	}



/* &<@anonymous:1195> */
	obj_t BGl_z62zc3z04anonymousza31195ze3ze5zzheap_restorez00(obj_t
		BgL_envz00_872, obj_t BgL_newz00_873)
	{
		{	/* Heap/restore.scm 107 */
			{	/* Heap/restore.scm 108 */
				obj_t BgL_arg1196z00_909;

				BgL_arg1196z00_909 =
					(((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_newz00_873)))->BgL_modulez00);
				{	/* Heap/restore.scm 108 */
					obj_t BgL_list1197z00_910;

					BgL_list1197z00_910 = MAKE_YOUNG_PAIR(BgL_arg1196z00_909, BNIL);
					return
						BGl_heapzd2modulezd2listz00zzheap_restorez00(BgL_list1197z00_910);
				}
			}
		}

	}



/* &<@anonymous:1184> */
	obj_t BGl_z62zc3z04anonymousza31184ze3ze5zzheap_restorez00(obj_t
		BgL_envz00_874, obj_t BgL_newz00_875)
	{
		{	/* Heap/restore.scm 99 */
			{	/* Heap/restore.scm 101 */
				obj_t BgL_arg1187z00_911;
				obj_t BgL_arg1188z00_912;
				obj_t BgL_arg1189z00_913;

				BgL_arg1187z00_911 =
					(((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_newz00_875)))->BgL_modulez00);
				BgL_arg1188z00_912 =
					(((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_newz00_875)))->
					BgL_jvmzd2typezd2namez00);
				BgL_arg1189z00_913 = BGl_shapez00zztools_shapez00(BgL_newz00_875);
				{	/* Heap/restore.scm 100 */
					obj_t BgL_list1190z00_914;

					BgL_list1190z00_914 = MAKE_YOUNG_PAIR(BgL_arg1189z00_913, BNIL);
					return
						BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00(BgL_arg1187z00_911,
						BgL_arg1188z00_912, BgL_list1190z00_914);
				}
			}
		}

	}



/* &<@anonymous:1202> */
	obj_t BGl_z62zc3z04anonymousza31202ze3ze5zzheap_restorez00(obj_t
		BgL_envz00_876)
	{
		{	/* Heap/restore.scm 56 */
			{	/* Heap/restore.scm 110 */
				obj_t BgL_portz00_877;

				BgL_portz00_877 = PROCEDURE_REF(BgL_envz00_876, (int) (0L));
				return close_binary_port(((obj_t) BgL_portz00_877));
			}
		}

	}



/* unbind-call/cc! */
	obj_t BGl_unbindzd2callzf2ccz12z32zzheap_restorez00(void)
	{
		{	/* Heap/restore.scm 118 */
			if (CBOOL(BGl_findzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(2),
						CNST_TABLE_REF(3))))
				{	/* Heap/restore.scm 119 */
					BGl_unbindzd2globalz12zc0zzast_envz00(CNST_TABLE_REF(2),
						CNST_TABLE_REF(3));
				}
			else
				{	/* Heap/restore.scm 119 */
					BFALSE;
				}
			if (CBOOL(BGl_findzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(4),
						CNST_TABLE_REF(3))))
				{	/* Heap/restore.scm 121 */
					return
						BGl_unbindzd2globalz12zc0zzast_envz00(CNST_TABLE_REF(4),
						CNST_TABLE_REF(3));
				}
			else
				{	/* Heap/restore.scm 121 */
					return BFALSE;
				}
		}

	}



/* heap-file-name */
	BGL_EXPORTED_DEF obj_t BGl_heapzd2filezd2namez00zzheap_restorez00(obj_t
		BgL_heapz00_4)
	{
		{	/* Heap/restore.scm 129 */
			{	/* Heap/restore.scm 130 */
				obj_t BgL_arg1208z00_819;

				{	/* Heap/restore.scm 130 */
					obj_t BgL_arg1209z00_820;

					BgL_arg1209z00_820 = BGl_thezd2backendzd2zzbackend_backendz00();
					BgL_arg1208z00_819 =
						(((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_arg1209z00_820)))->
						BgL_heapzd2suffixzd2);
				}
				return
					string_append_3(BgL_heapz00_4, BGl_string1398z00zzheap_restorez00,
					BgL_arg1208z00_819);
			}
		}

	}



/* &heap-file-name */
	obj_t BGl_z62heapzd2filezd2namez62zzheap_restorez00(obj_t BgL_envz00_878,
		obj_t BgL_heapz00_879)
	{
		{	/* Heap/restore.scm 129 */
			return BGl_heapzd2filezd2namez00zzheap_restorez00(BgL_heapz00_879);
		}

	}



/* restore-additional-heaps */
	BGL_EXPORTED_DEF obj_t
		BGl_restorezd2additionalzd2heapsz00zzheap_restorez00(void)
	{
		{	/* Heap/restore.scm 135 */
			if (PAIRP(BGl_za2additionalzd2heapzd2namesza2z00zzengine_paramz00))
				{	/* Heap/restore.scm 136 */
					{	/* Heap/restore.scm 137 */
						obj_t BgL_list1211z00_633;

						{	/* Heap/restore.scm 137 */
							obj_t BgL_arg1212z00_634;

							{	/* Heap/restore.scm 137 */
								obj_t BgL_arg1215z00_635;

								BgL_arg1215z00_635 =
									MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
								BgL_arg1212z00_634 =
									MAKE_YOUNG_PAIR(BGl_string1399z00zzheap_restorez00,
									BgL_arg1215z00_635);
							}
							BgL_list1211z00_633 =
								MAKE_YOUNG_PAIR(BGl_string1384z00zzheap_restorez00,
								BgL_arg1212z00_634);
						}
						BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1211z00_633);
					}
					BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
					BGl_za2currentzd2passza2zd2zzengine_passz00 =
						BGl_string1399z00zzheap_restorez00;
					{	/* Heap/restore.scm 137 */
						obj_t BgL_g1073z00_636;

						BgL_g1073z00_636 = BNIL;
						{
							obj_t BgL_hooksz00_639;
							obj_t BgL_hnamesz00_640;

							BgL_hooksz00_639 = BgL_g1073z00_636;
							BgL_hnamesz00_640 = BNIL;
						BgL_zc3z04anonymousza31216ze3z87_641:
							if (NULLP(BgL_hooksz00_639))
								{	/* Heap/restore.scm 137 */
									CNST_TABLE_REF(1);
								}
							else
								{	/* Heap/restore.scm 137 */
									bool_t BgL_test1461z00_1155;

									{	/* Heap/restore.scm 137 */
										obj_t BgL_fun1224z00_648;

										BgL_fun1224z00_648 = CAR(((obj_t) BgL_hooksz00_639));
										BgL_test1461z00_1155 =
											CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1224z00_648));
									}
									if (BgL_test1461z00_1155)
										{	/* Heap/restore.scm 137 */
											obj_t BgL_arg1220z00_645;
											obj_t BgL_arg1221z00_646;

											BgL_arg1220z00_645 = CDR(((obj_t) BgL_hooksz00_639));
											BgL_arg1221z00_646 = CDR(((obj_t) BgL_hnamesz00_640));
											{
												obj_t BgL_hnamesz00_1167;
												obj_t BgL_hooksz00_1166;

												BgL_hooksz00_1166 = BgL_arg1220z00_645;
												BgL_hnamesz00_1167 = BgL_arg1221z00_646;
												BgL_hnamesz00_640 = BgL_hnamesz00_1167;
												BgL_hooksz00_639 = BgL_hooksz00_1166;
												goto BgL_zc3z04anonymousza31216ze3z87_641;
											}
										}
									else
										{	/* Heap/restore.scm 137 */
											obj_t BgL_arg1223z00_647;

											BgL_arg1223z00_647 = CAR(((obj_t) BgL_hnamesz00_640));
											BGl_internalzd2errorzd2zztools_errorz00
												(BGl_string1399z00zzheap_restorez00,
												BGl_string1385z00zzheap_restorez00, BgL_arg1223z00_647);
										}
								}
						}
					}
					{	/* Heap/restore.scm 138 */
						obj_t BgL_g1108z00_651;

						BgL_g1108z00_651 =
							bgl_reverse
							(BGl_za2additionalzd2heapzd2namesza2z00zzengine_paramz00);
						{
							obj_t BgL_l1106z00_653;

							{	/* Heap/restore.scm 143 */
								bool_t BgL_tmpz00_1172;

								BgL_l1106z00_653 = BgL_g1108z00_651;
							BgL_zc3z04anonymousza31226ze3z87_654:
								if (PAIRP(BgL_l1106z00_653))
									{	/* Heap/restore.scm 143 */
										{	/* Heap/restore.scm 139 */
											obj_t BgL_hz00_656;

											BgL_hz00_656 = CAR(BgL_l1106z00_653);
											if (CBOOL(BGl_memberz00zz__r4_pairs_and_lists_6_3z00
													(BgL_hz00_656,
														BGl_za2restoredzd2heapzd2namesza2z00zzheap_restorez00)))
												{	/* Heap/restore.scm 139 */
													BFALSE;
												}
											else
												{	/* Heap/restore.scm 139 */
													BGl_za2restoredzd2heapzd2namesza2z00zzheap_restorez00
														=
														MAKE_YOUNG_PAIR(BgL_hz00_656,
														BGl_za2restoredzd2heapzd2namesza2z00zzheap_restorez00);
													{	/* Heap/restore.scm 142 */
														obj_t BgL_arg1229z00_658;

														{	/* Heap/restore.scm 130 */
															obj_t BgL_arg1208z00_830;

															{	/* Heap/restore.scm 130 */
																obj_t BgL_arg1209z00_831;

																BgL_arg1209z00_831 =
																	BGl_thezd2backendzd2zzbackend_backendz00();
																BgL_arg1208z00_830 =
																	(((BgL_backendz00_bglt) COBJECT(
																			((BgL_backendz00_bglt)
																				BgL_arg1209z00_831)))->
																	BgL_heapzd2suffixzd2);
															}
															BgL_arg1229z00_658 =
																string_append_3(
																((obj_t) BgL_hz00_656),
																BGl_string1398z00zzheap_restorez00,
																BgL_arg1208z00_830);
														}
														BGl_restorezd2additionalzd2heapz00zzheap_restorez00
															(BgL_arg1229z00_658);
													}
												}
										}
										{
											obj_t BgL_l1106z00_1186;

											BgL_l1106z00_1186 = CDR(BgL_l1106z00_653);
											BgL_l1106z00_653 = BgL_l1106z00_1186;
											goto BgL_zc3z04anonymousza31226ze3z87_654;
										}
									}
								else
									{	/* Heap/restore.scm 143 */
										BgL_tmpz00_1172 = ((bool_t) 1);
									}
								return BBOOL(BgL_tmpz00_1172);
							}
						}
					}
				}
			else
				{	/* Heap/restore.scm 136 */
					return BFALSE;
				}
		}

	}



/* &restore-additional-heaps */
	obj_t BGl_z62restorezd2additionalzd2heapsz62zzheap_restorez00(obj_t
		BgL_envz00_880)
	{
		{	/* Heap/restore.scm 135 */
			return BGl_restorezd2additionalzd2heapsz00zzheap_restorez00();
		}

	}



/* restore-additional-heap */
	BGL_EXPORTED_DEF obj_t
		BGl_restorezd2additionalzd2heapz00zzheap_restorez00(obj_t BgL_heapz00_5)
	{
		{	/* Heap/restore.scm 153 */
			{	/* Heap/restore.scm 154 */
				obj_t BgL_fnamez00_661;

				BgL_fnamez00_661 =
					BGl_findzd2filezf2pathz20zz__osz00(BgL_heapz00_5,
					BGl_za2libzd2dirza2zd2zzengine_paramz00);
				if (STRINGP(BgL_fnamez00_661))
					{	/* Heap/restore.scm 156 */
						obj_t BgL_portz00_663;

						BgL_portz00_663 =
							BGl_openzd2inputzd2binaryzd2filezd2zz__binaryz00
							(BgL_fnamez00_661);
						if (BINARY_PORTP(BgL_portz00_663))
							{	/* Heap/restore.scm 157 */
								{	/* Heap/restore.scm 165 */
									obj_t BgL_list1233z00_665;

									{	/* Heap/restore.scm 165 */
										obj_t BgL_arg1234z00_666;

										{	/* Heap/restore.scm 165 */
											obj_t BgL_arg1236z00_667;

											{	/* Heap/restore.scm 165 */
												obj_t BgL_arg1238z00_668;

												BgL_arg1238z00_668 =
													MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
												BgL_arg1236z00_667 =
													MAKE_YOUNG_PAIR(BGl_string1386z00zzheap_restorez00,
													BgL_arg1238z00_668);
											}
											BgL_arg1234z00_666 =
												MAKE_YOUNG_PAIR(BgL_fnamez00_661, BgL_arg1236z00_667);
										}
										BgL_list1233z00_665 =
											MAKE_YOUNG_PAIR(BGl_string1387z00zzheap_restorez00,
											BgL_arg1234z00_666);
									}
									BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1233z00_665);
								}
								{	/* Heap/restore.scm 166 */
									obj_t BgL_exitd1075z00_669;

									BgL_exitd1075z00_669 = BGL_EXITD_TOP_AS_OBJ();
									{	/* Heap/restore.scm 234 */
										obj_t BgL_zc3z04anonymousza31328ze3z87_883;

										BgL_zc3z04anonymousza31328ze3z87_883 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31328ze3ze5zzheap_restorez00,
											(int) (0L), (int) (1L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31328ze3z87_883,
											(int) (0L), BgL_portz00_663);
										{	/* Heap/restore.scm 166 */
											obj_t BgL_arg1828z00_835;

											{	/* Heap/restore.scm 166 */
												obj_t BgL_arg1829z00_836;

												BgL_arg1829z00_836 =
													BGL_EXITD_PROTECT(BgL_exitd1075z00_669);
												BgL_arg1828z00_835 =
													MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31328ze3z87_883,
													BgL_arg1829z00_836);
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1075z00_669,
												BgL_arg1828z00_835);
											BUNSPEC;
										}
										{	/* Heap/restore.scm 167 */
											bool_t BgL_tmp1077z00_671;

											{	/* Heap/restore.scm 167 */
												obj_t BgL_envsz00_672;

												BgL_envsz00_672 = input_obj(BgL_portz00_663);
												{	/* Heap/restore.scm 167 */
													obj_t BgL__z00_673;

													{	/* Heap/restore.scm 168 */
														bool_t BgL_test1466z00_1213;

														if (VECTORP(BgL_envsz00_672))
															{	/* Heap/restore.scm 168 */
																BgL_test1466z00_1213 =
																	(VECTOR_LENGTH(BgL_envsz00_672) == 7L);
															}
														else
															{	/* Heap/restore.scm 168 */
																BgL_test1466z00_1213 = ((bool_t) 0);
															}
														if (BgL_test1466z00_1213)
															{	/* Heap/restore.scm 168 */
																BgL__z00_673 = BFALSE;
															}
														else
															{	/* Heap/restore.scm 168 */
																BgL__z00_673 =
																	BGl_errorz00zz__errorz00(BgL_heapz00_5,
																	BGl_string1388z00zzheap_restorez00,
																	BgL_envsz00_672);
															}
													}
													{	/* Heap/restore.scm 168 */
														obj_t BgL_targetz00_674;

														BgL_targetz00_674 =
															VECTOR_REF(((obj_t) BgL_envsz00_672), 0L);
														{	/* Heap/restore.scm 171 */
															obj_t BgL_versionz00_675;

															BgL_versionz00_675 =
																VECTOR_REF(((obj_t) BgL_envsz00_672), 1L);
															{	/* Heap/restore.scm 172 */
																obj_t BgL_specificz00_676;

																BgL_specificz00_676 =
																	VECTOR_REF(((obj_t) BgL_envsz00_672), 2L);
																{	/* Heap/restore.scm 173 */
																	obj_t BgL_genvz00_677;

																	BgL_genvz00_677 =
																		VECTOR_REF(((obj_t) BgL_envsz00_672), 3L);
																	{	/* Heap/restore.scm 174 */
																		obj_t BgL_tenvz00_678;

																		BgL_tenvz00_678 =
																			VECTOR_REF(((obj_t) BgL_envsz00_672), 4L);
																		{	/* Heap/restore.scm 175 */
																			obj_t BgL_includesz00_679;

																			BgL_includesz00_679 =
																				VECTOR_REF(
																				((obj_t) BgL_envsz00_672), 5L);
																			{	/* Heap/restore.scm 176 */
																				obj_t BgL_ccoptsz00_680;

																				BgL_ccoptsz00_680 =
																					VECTOR_REF(
																					((obj_t) BgL_envsz00_672), 6L);
																				{	/* Heap/restore.scm 177 */

																					if (BGl_backendzd2heapzd2compatiblezf3zf3zzheap_restorez00(BgL_targetz00_674))
																						{	/* Heap/restore.scm 179 */
																							BFALSE;
																						}
																					else
																						{	/* Heap/restore.scm 184 */
																							obj_t BgL_arg1242z00_682;

																							{	/* Heap/restore.scm 184 */
																								obj_t BgL_arg1244z00_683;

																								{	/* Heap/restore.scm 184 */
																									obj_t BgL_arg1249z00_686;

																									BgL_arg1249z00_686 =
																										BGl_thezd2backendzd2zzbackend_backendz00
																										();
																									BgL_arg1244z00_683 =
																										(((BgL_backendz00_bglt)
																											COBJECT((
																													(BgL_backendz00_bglt)
																													BgL_arg1249z00_686)))->
																										BgL_languagez00);
																								}
																								{	/* Heap/restore.scm 182 */
																									obj_t BgL_list1245z00_684;

																									{	/* Heap/restore.scm 182 */
																										obj_t BgL_arg1248z00_685;

																										BgL_arg1248z00_685 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1244z00_683,
																											BNIL);
																										BgL_list1245z00_684 =
																											MAKE_YOUNG_PAIR
																											(BgL_targetz00_674,
																											BgL_arg1248z00_685);
																									}
																									BgL_arg1242z00_682 =
																										BGl_formatz00zz__r4_output_6_10_3z00
																										(BGl_string1389z00zzheap_restorez00,
																										BgL_list1245z00_684);
																								}
																							}
																							BGl_errorz00zz__errorz00
																								(BgL_heapz00_5,
																								BGl_string1390z00zzheap_restorez00,
																								BgL_arg1242z00_682);
																						}
																					if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_versionz00_675, BGl_za2bigloozd2versionza2zd2zzengine_paramz00))
																						{	/* Heap/restore.scm 185 */
																							BFALSE;
																						}
																					else
																						{	/* Heap/restore.scm 188 */
																							obj_t BgL_arg1252z00_688;

																							{	/* Heap/restore.scm 188 */
																								obj_t BgL_list1253z00_689;

																								{	/* Heap/restore.scm 188 */
																									obj_t BgL_arg1268z00_690;

																									BgL_arg1268z00_690 =
																										MAKE_YOUNG_PAIR
																										(BGl_za2bigloozd2versionza2zd2zzengine_paramz00,
																										BNIL);
																									BgL_list1253z00_689 =
																										MAKE_YOUNG_PAIR
																										(BgL_versionz00_675,
																										BgL_arg1268z00_690);
																								}
																								BgL_arg1252z00_688 =
																									BGl_formatz00zz__r4_output_6_10_3z00
																									(BGl_string1391z00zzheap_restorez00,
																									BgL_list1253z00_689);
																							}
																							BGl_errorz00zz__errorz00
																								(BGl_za2heapzd2nameza2zd2zzengine_paramz00,
																								BGl_string1392z00zzheap_restorez00,
																								BgL_arg1252z00_688);
																						}
																					if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_specificz00_676, BGl_za2bigloozd2specificzd2versionza2z00zzengine_paramz00))
																						{	/* Heap/restore.scm 191 */
																							BFALSE;
																						}
																					else
																						{	/* Heap/restore.scm 194 */
																							obj_t BgL_arg1272z00_692;

																							{	/* Heap/restore.scm 194 */
																								obj_t BgL_list1273z00_693;

																								{	/* Heap/restore.scm 194 */
																									obj_t BgL_arg1284z00_694;

																									BgL_arg1284z00_694 =
																										MAKE_YOUNG_PAIR
																										(BGl_za2bigloozd2specificzd2versionza2z00zzengine_paramz00,
																										BNIL);
																									BgL_list1273z00_693 =
																										MAKE_YOUNG_PAIR
																										(BgL_specificz00_676,
																										BgL_arg1284z00_694);
																								}
																								BgL_arg1272z00_692 =
																									BGl_formatz00zz__r4_output_6_10_3z00
																									(BGl_string1391z00zzheap_restorez00,
																									BgL_list1273z00_693);
																							}
																							BGl_errorz00zz__errorz00
																								(BGl_za2heapzd2nameza2zd2zzengine_paramz00,
																								BGl_string1393z00zzheap_restorez00,
																								BgL_arg1272z00_692);
																						}
																					BGl_addzd2tenvz12zc0zztype_envz00
																						(BgL_tenvz00_678);
																					BGl_addzd2genvz12zc0zzast_envz00
																						(BgL_genvz00_677);
																					{	/* Heap/restore.scm 210 */
																						bool_t BgL_test1471z00_1256;

																						{	/* Heap/restore.scm 210 */
																							obj_t BgL_arg1314z00_706;

																							BgL_arg1314z00_706 =
																								BGl_thezd2backendzd2zzbackend_backendz00
																								();
																							BgL_test1471z00_1256 =
																								(((BgL_backendz00_bglt)
																									COBJECT(((BgL_backendz00_bglt)
																											BgL_arg1314z00_706)))->
																								BgL_qualifiedzd2typeszd2);
																						}
																						if (BgL_test1471z00_1256)
																							{	/* Heap/restore.scm 210 */
																								BGl_forzd2eachzd2globalz12z12zzast_envz00
																									(BGl_proc1400z00zzheap_restorez00,
																									BNIL);
																							}
																						else
																							{	/* Heap/restore.scm 210 */
																								BFALSE;
																							}
																					}
																					BGl_hashtablezd2forzd2eachz00zz__hashz00
																						(BgL_genvz00_677,
																						BGl_proc1401z00zzheap_restorez00);
																					if (
																						(BGl_za2passza2z00zzengine_paramz00
																							== CNST_TABLE_REF(5)))
																						{	/* Heap/restore.scm 226 */
																							BFALSE;
																						}
																					else
																						{	/* Heap/restore.scm 226 */
																							BGl_za2additionalzd2includezd2foreignza2z00zzengine_paramz00
																								=
																								BGl_appendzd221011zd2zzheap_restorez00
																								(BGl_za2additionalzd2includezd2foreignza2z00zzengine_paramz00,
																								BgL_includesz00_679);
																							{	/* Heap/restore.scm 232 */
																								obj_t BgL_arg1322z00_722;

																								BgL_arg1322z00_722 =
																									BGl_appendzd221011zd2zzheap_restorez00
																									(BGl_za2cczd2optionsza2zd2zzengine_paramz00,
																									BgL_ccoptsz00_680);
																								{	/* Heap/restore.scm 231 */

																									BGl_za2cczd2optionsza2zd2zzengine_paramz00
																										=
																										BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00
																										(BgL_arg1322z00_722,
																										BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00);
																								}
																							}
																						}
																					BgL_tmp1077z00_671 = ((bool_t) 1);
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
											{	/* Heap/restore.scm 166 */
												bool_t BgL_test1473z00_1268;

												{	/* Heap/restore.scm 166 */
													obj_t BgL_arg1827z00_856;

													BgL_arg1827z00_856 =
														BGL_EXITD_PROTECT(BgL_exitd1075z00_669);
													BgL_test1473z00_1268 = PAIRP(BgL_arg1827z00_856);
												}
												if (BgL_test1473z00_1268)
													{	/* Heap/restore.scm 166 */
														obj_t BgL_arg1825z00_857;

														{	/* Heap/restore.scm 166 */
															obj_t BgL_arg1826z00_858;

															BgL_arg1826z00_858 =
																BGL_EXITD_PROTECT(BgL_exitd1075z00_669);
															BgL_arg1825z00_857 =
																CDR(((obj_t) BgL_arg1826z00_858));
														}
														BGL_EXITD_PROTECT_SET(BgL_exitd1075z00_669,
															BgL_arg1825z00_857);
														BUNSPEC;
													}
												else
													{	/* Heap/restore.scm 166 */
														BFALSE;
													}
											}
											close_binary_port(BgL_portz00_663);
											return BBOOL(BgL_tmp1077z00_671);
										}
									}
								}
							}
						else
							{	/* Heap/restore.scm 158 */
								obj_t BgL_mz00_732;

								{	/* Heap/restore.scm 158 */
									obj_t BgL_list1331z00_735;

									BgL_list1331z00_735 = MAKE_YOUNG_PAIR(BgL_fnamez00_661, BNIL);
									BgL_mz00_732 =
										BGl_formatz00zz__r4_output_6_10_3z00
										(BGl_string1396z00zzheap_restorez00, BgL_list1331z00_735);
								}
								{	/* Heap/restore.scm 159 */
									obj_t BgL_arg1329z00_733;

									if (PAIRP(BGl_za2srczd2filesza2zd2zzengine_paramz00))
										{	/* Heap/restore.scm 159 */
											BgL_arg1329z00_733 =
												CAR(BGl_za2srczd2filesza2zd2zzengine_paramz00);
										}
									else
										{	/* Heap/restore.scm 159 */
											BgL_arg1329z00_733 = BGl_string1402z00zzheap_restorez00;
										}
									BGl_errorz00zz__errorz00(BgL_arg1329z00_733, BgL_mz00_732,
										BGl_za2libzd2dirza2zd2zzengine_paramz00);
								}
								return BGl_compilerzd2exitzd2zzinit_mainz00(BINT(6L));
							}
					}
				else
					{	/* Heap/restore.scm 235 */
						obj_t BgL_mz00_736;

						{	/* Heap/restore.scm 235 */
							obj_t BgL_list1335z00_739;

							BgL_list1335z00_739 = MAKE_YOUNG_PAIR(BgL_heapz00_5, BNIL);
							BgL_mz00_736 =
								BGl_formatz00zz__r4_output_6_10_3z00
								(BGl_string1396z00zzheap_restorez00, BgL_list1335z00_739);
						}
						{	/* Heap/restore.scm 236 */
							obj_t BgL_arg1332z00_737;

							if (PAIRP(BGl_za2srczd2filesza2zd2zzengine_paramz00))
								{	/* Heap/restore.scm 236 */
									BgL_arg1332z00_737 =
										CAR(BGl_za2srczd2filesza2zd2zzengine_paramz00);
								}
							else
								{	/* Heap/restore.scm 236 */
									BgL_arg1332z00_737 = BGl_string1402z00zzheap_restorez00;
								}
							BGl_errorz00zz__errorz00(BgL_arg1332z00_737, BgL_mz00_736,
								BGl_za2libzd2dirza2zd2zzengine_paramz00);
						}
						return BGl_compilerzd2exitzd2zzinit_mainz00(BINT(6L));
					}
			}
		}

	}



/* &restore-additional-heap */
	obj_t BGl_z62restorezd2additionalzd2heapz62zzheap_restorez00(obj_t
		BgL_envz00_884, obj_t BgL_heapz00_885)
	{
		{	/* Heap/restore.scm 153 */
			return
				BGl_restorezd2additionalzd2heapz00zzheap_restorez00(BgL_heapz00_885);
		}

	}



/* &<@anonymous:1316> */
	obj_t BGl_z62zc3z04anonymousza31316ze3ze5zzheap_restorez00(obj_t
		BgL_envz00_886, obj_t BgL_kz00_887, obj_t BgL_bucketz00_888)
	{
		{	/* Heap/restore.scm 220 */
			{	/* Heap/restore.scm 221 */
				bool_t BgL_tmpz00_1294;

				{	/* Heap/restore.scm 221 */
					obj_t BgL_g1111z00_915;

					BgL_g1111z00_915 = CDR(((obj_t) BgL_bucketz00_888));
					{
						obj_t BgL_l1109z00_917;

						BgL_l1109z00_917 = BgL_g1111z00_915;
					BgL_zc3z04anonymousza31317ze3z87_916:
						if (PAIRP(BgL_l1109z00_917))
							{	/* Heap/restore.scm 224 */
								{	/* Heap/restore.scm 222 */
									obj_t BgL_newz00_918;

									BgL_newz00_918 = CAR(BgL_l1109z00_917);
									{	/* Heap/restore.scm 223 */
										obj_t BgL_arg1319z00_919;

										BgL_arg1319z00_919 =
											(((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt) BgL_newz00_918)))->
											BgL_modulez00);
										{	/* Heap/restore.scm 222 */
											obj_t BgL_list1320z00_920;

											BgL_list1320z00_920 =
												MAKE_YOUNG_PAIR(BgL_arg1319z00_919, BNIL);
											BGl_heapzd2modulezd2listz00zzheap_restorez00
												(BgL_list1320z00_920);
										}
									}
								}
								{
									obj_t BgL_l1109z00_1304;

									BgL_l1109z00_1304 = CDR(BgL_l1109z00_917);
									BgL_l1109z00_917 = BgL_l1109z00_1304;
									goto BgL_zc3z04anonymousza31317ze3z87_916;
								}
							}
						else
							{	/* Heap/restore.scm 224 */
								BgL_tmpz00_1294 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_1294);
			}
		}

	}



/* &<@anonymous:1308> */
	obj_t BGl_z62zc3z04anonymousza31308ze3ze5zzheap_restorez00(obj_t
		BgL_envz00_889, obj_t BgL_newz00_890)
	{
		{	/* Heap/restore.scm 212 */
			{	/* Heap/restore.scm 214 */
				obj_t BgL_arg1310z00_921;
				obj_t BgL_arg1311z00_922;
				obj_t BgL_arg1312z00_923;

				BgL_arg1310z00_921 =
					(((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_newz00_890)))->BgL_modulez00);
				BgL_arg1311z00_922 =
					(((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_newz00_890)))->
					BgL_jvmzd2typezd2namez00);
				BgL_arg1312z00_923 = BGl_shapez00zztools_shapez00(BgL_newz00_890);
				{	/* Heap/restore.scm 213 */
					obj_t BgL_list1313z00_924;

					BgL_list1313z00_924 = MAKE_YOUNG_PAIR(BgL_arg1312z00_923, BNIL);
					return
						BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00(BgL_arg1310z00_921,
						BgL_arg1311z00_922, BgL_list1313z00_924);
				}
			}
		}

	}



/* &<@anonymous:1328> */
	obj_t BGl_z62zc3z04anonymousza31328ze3ze5zzheap_restorez00(obj_t
		BgL_envz00_891)
	{
		{	/* Heap/restore.scm 166 */
			{	/* Heap/restore.scm 234 */
				obj_t BgL_portz00_892;

				BgL_portz00_892 = PROCEDURE_REF(BgL_envz00_891, (int) (0L));
				return close_binary_port(((obj_t) BgL_portz00_892));
			}
		}

	}



/* heap-module-list */
	BGL_EXPORTED_DEF obj_t BGl_heapzd2modulezd2listz00zzheap_restorez00(obj_t
		BgL_argsz00_6)
	{
		{	/* Heap/restore.scm 255 */
			if (NULLP(BgL_argsz00_6))
				{	/* Heap/restore.scm 257 */
					return BGl_za2heapzd2modulezd2listza2z00zzheap_restorez00;
				}
			else
				{	/* Heap/restore.scm 257 */
					if (CBOOL(BGl_getpropz00zz__r4_symbols_6_4z00(CAR(
									((obj_t) BgL_argsz00_6)), CNST_TABLE_REF(0))))
						{	/* Heap/restore.scm 259 */
							return BFALSE;
						}
					else
						{	/* Heap/restore.scm 259 */
							{	/* Heap/restore.scm 260 */
								obj_t BgL_arg1342z00_743;

								BgL_arg1342z00_743 = CAR(((obj_t) BgL_argsz00_6));
								BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_arg1342z00_743,
									CNST_TABLE_REF(0), BTRUE);
							}
							{	/* Heap/restore.scm 261 */
								obj_t BgL_arg1343z00_744;

								BgL_arg1343z00_744 = CAR(((obj_t) BgL_argsz00_6));
								return (BGl_za2heapzd2modulezd2listza2z00zzheap_restorez00 =
									MAKE_YOUNG_PAIR(BgL_arg1343z00_744,
										BGl_za2heapzd2modulezd2listza2z00zzheap_restorez00),
									BUNSPEC);
							}
						}
				}
		}

	}



/* &heap-module-list */
	obj_t BGl_z62heapzd2modulezd2listz62zzheap_restorez00(obj_t BgL_envz00_896,
		obj_t BgL_argsz00_897)
	{
		{	/* Heap/restore.scm 255 */
			return BGl_heapzd2modulezd2listz00zzheap_restorez00(BgL_argsz00_897);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzheap_restorez00(void)
	{
		{	/* Heap/restore.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzheap_restorez00(void)
	{
		{	/* Heap/restore.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzheap_restorez00(void)
	{
		{	/* Heap/restore.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzheap_restorez00(void)
	{
		{	/* Heap/restore.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1403z00zzheap_restorez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1403z00zzheap_restorez00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1403z00zzheap_restorez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1403z00zzheap_restorez00));
			BGl_modulezd2initializa7ationz75zzengine_enginez00(373986149L,
				BSTRING_TO_STRING(BGl_string1403z00zzheap_restorez00));
			BGl_modulezd2initializa7ationz75zzinit_mainz00(288050968L,
				BSTRING_TO_STRING(BGl_string1403z00zzheap_restorez00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1403z00zzheap_restorez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1403z00zzheap_restorez00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1403z00zzheap_restorez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1403z00zzheap_restorez00));
			BGl_modulezd2initializa7ationz75zzread_jvmz00(261574382L,
				BSTRING_TO_STRING(BGl_string1403z00zzheap_restorez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1403z00zzheap_restorez00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1403z00zzheap_restorez00));
		}

	}

#ifdef __cplusplus
}
#endif
