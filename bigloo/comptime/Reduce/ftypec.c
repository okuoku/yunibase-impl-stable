/*===========================================================================*/
/*   (Reduce/ftypec.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Reduce/ftypec.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_REDUCE_FLOWzd2TYPECzd2_TYPE_DEFINITIONS
#define BGL_BgL_REDUCE_FLOWzd2TYPECzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_BgL_REDUCE_FLOWzd2TYPECzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2setzd2exzd2i1306z70zzreduce_flowzd2typeczd2(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2boxzd2setz121312zb0zzreduce_flowzd2typeczd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2makezd2box1310za2zzreduce_flowzd2typeczd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzreduce_flowzd2typeczd2 =
		BUNSPEC;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2jumpzd2exzd21308z70zzreduce_flowzd2typeczd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzreduce_flowzd2typeczd2(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzreduce_flowzd2typeczd2(void);
	static obj_t BGl_objectzd2initzd2zzreduce_flowzd2typeczd2(void);
	extern obj_t BGl_castz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2letzd2var1304za2zzreduce_flowzd2typeczd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	extern obj_t BGl_typez00zztype_typez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2conditio1296z70zzreduce_flowzd2typeczd2(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_nodez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2boxzd2ref1314za2zzreduce_flowzd2typeczd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzreduce_flowzd2typeczd2(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2cast1292z70zzreduce_flowzd2typeczd2(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2switch1300z70zzreduce_flowzd2typeczd2(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2setq1294z70zzreduce_flowzd2typeczd2(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2funcall1288z70zzreduce_flowzd2typeczd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2sync1284z70zzreduce_flowzd2typeczd2(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2fail1298z70zzreduce_flowzd2typeczd2(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2appzd2ly1286za2zzreduce_flowzd2typeczd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62reducezd2flowzd2typezd2checkz12za2zzreduce_flowzd2typeczd2(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2sequence1282z70zzreduce_flowzd2typeczd2(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12za2zzreduce_flowzd2typeczd2(obj_t, obj_t, obj_t);
	static obj_t BGl_nodezd2typecza2z12z62zzreduce_flowzd2typeczd2(obj_t, obj_t);
	static long BGl_za2typezd2checkszd2removedza2z00zzreduce_flowzd2typeczd2 = 0L;
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzreduce_flowzd2typeczd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2extern1290z70zzreduce_flowzd2typeczd2(obj_t, obj_t,
		obj_t);
	static long BGl_za2typezd2checkszd2remainingza2z00zzreduce_flowzd2typeczd2 =
		0L;
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62nodezd2typecz121279za2zzreduce_flowzd2typeczd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzreduce_flowzd2typeczd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzreduce_flowzd2typeczd2(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzreduce_flowzd2typeczd2(void);
	static obj_t BGl_gczd2rootszd2initz00zzreduce_flowzd2typeczd2(void);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2letzd2fun1302za2zzreduce_flowzd2typeczd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_isazd2ofzd2zztype_miscz00(BgL_nodez00_bglt);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2app1316z70zzreduce_flowzd2typeczd2(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reducezd2flowzd2typezd2checkz12zc0zzreduce_flowzd2typeczd2(obj_t);
	static obj_t __cnst[2];


	   
		 
		DEFINE_STATIC_BGL_GENERIC
		(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71884za7,
		BGl_z62nodezd2typecz12za2zzreduce_flowzd2typeczd2, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1856z00zzreduce_flowzd2typeczd2,
		BgL_bgl_string1856za700za7za7r1885za7, "      flow type check        ", 29);
	      DEFINE_STRING(BGl_string1857z00zzreduce_flowzd2typeczd2,
		BgL_bgl_string1857za700za7za7r1886za7, ")\n", 2);
	      DEFINE_STRING(BGl_string1858z00zzreduce_flowzd2typeczd2,
		BgL_bgl_string1858za700za7za7r1887za7, ", remaining: ", 13);
	      DEFINE_STRING(BGl_string1859z00zzreduce_flowzd2typeczd2,
		BgL_bgl_string1859za700za7za7r1888za7, "(removed: ", 10);
	      DEFINE_STRING(BGl_string1861z00zzreduce_flowzd2typeczd2,
		BgL_bgl_string1861za700za7za7r1889za7, "node-typec!1279", 15);
	      DEFINE_STRING(BGl_string1863z00zzreduce_flowzd2typeczd2,
		BgL_bgl_string1863za700za7za7r1890za7, "node-typec!", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1860z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71891za7,
		BGl_z62nodezd2typecz121279za2zzreduce_flowzd2typeczd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1862z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71892za7,
		BGl_z62nodezd2typecz12zd2sequence1282z70zzreduce_flowzd2typeczd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1864z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71893za7,
		BGl_z62nodezd2typecz12zd2sync1284z70zzreduce_flowzd2typeczd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1865z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71894za7,
		BGl_z62nodezd2typecz12zd2appzd2ly1286za2zzreduce_flowzd2typeczd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1866z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71895za7,
		BGl_z62nodezd2typecz12zd2funcall1288z70zzreduce_flowzd2typeczd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1867z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71896za7,
		BGl_z62nodezd2typecz12zd2extern1290z70zzreduce_flowzd2typeczd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1868z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71897za7,
		BGl_z62nodezd2typecz12zd2cast1292z70zzreduce_flowzd2typeczd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1869z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71898za7,
		BGl_z62nodezd2typecz12zd2setq1294z70zzreduce_flowzd2typeczd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1870z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71899za7,
		BGl_z62nodezd2typecz12zd2conditio1296z70zzreduce_flowzd2typeczd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1871z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71900za7,
		BGl_z62nodezd2typecz12zd2fail1298z70zzreduce_flowzd2typeczd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1872z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71901za7,
		BGl_z62nodezd2typecz12zd2switch1300z70zzreduce_flowzd2typeczd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1873z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71902za7,
		BGl_z62nodezd2typecz12zd2letzd2fun1302za2zzreduce_flowzd2typeczd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1874z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71903za7,
		BGl_z62nodezd2typecz12zd2letzd2var1304za2zzreduce_flowzd2typeczd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1881z00zzreduce_flowzd2typeczd2,
		BgL_bgl_string1881za700za7za7r1904za7, "reduce_flow-typec", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1875z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71905za7,
		BGl_z62nodezd2typecz12zd2setzd2exzd2i1306z70zzreduce_flowzd2typeczd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1882z00zzreduce_flowzd2typeczd2,
		BgL_bgl_string1882za700za7za7r1906za7, "read done ", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1876z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71907za7,
		BGl_z62nodezd2typecz12zd2jumpzd2exzd21308z70zzreduce_flowzd2typeczd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1877z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71908za7,
		BGl_z62nodezd2typecz12zd2makezd2box1310za2zzreduce_flowzd2typeczd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1878z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71909za7,
		BGl_z62nodezd2typecz12zd2boxzd2setz121312zb0zzreduce_flowzd2typeczd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1879z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71910za7,
		BGl_z62nodezd2typecz12zd2boxzd2ref1314za2zzreduce_flowzd2typeczd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1880z00zzreduce_flowzd2typeczd2,
		BgL_bgl_za762nodeza7d2typecza71911za7,
		BGl_z62nodezd2typecz12zd2app1316z70zzreduce_flowzd2typeczd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reducezd2flowzd2typezd2checkz12zd2envz12zzreduce_flowzd2typeczd2,
		BgL_bgl_za762reduceza7d2flow1912z00,
		BGl_z62reducezd2flowzd2typezd2checkz12za2zzreduce_flowzd2typeczd2, 0L,
		BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzreduce_flowzd2typeczd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzreduce_flowzd2typeczd2(long
		BgL_checksumz00_2643, char *BgL_fromz00_2644)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzreduce_flowzd2typeczd2))
				{
					BGl_requirezd2initializa7ationz75zzreduce_flowzd2typeczd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzreduce_flowzd2typeczd2();
					BGl_libraryzd2moduleszd2initz00zzreduce_flowzd2typeczd2();
					BGl_cnstzd2initzd2zzreduce_flowzd2typeczd2();
					BGl_importedzd2moduleszd2initz00zzreduce_flowzd2typeczd2();
					BGl_genericzd2initzd2zzreduce_flowzd2typeczd2();
					BGl_methodzd2initzd2zzreduce_flowzd2typeczd2();
					return BGl_toplevelzd2initzd2zzreduce_flowzd2typeczd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzreduce_flowzd2typeczd2(void)
	{
		{	/* Reduce/ftypec.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "reduce_flow-typec");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"reduce_flow-typec");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "reduce_flow-typec");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"reduce_flow-typec");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"reduce_flow-typec");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"reduce_flow-typec");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"reduce_flow-typec");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "reduce_flow-typec");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"reduce_flow-typec");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzreduce_flowzd2typeczd2(void)
	{
		{	/* Reduce/ftypec.scm 15 */
			{	/* Reduce/ftypec.scm 15 */
				obj_t BgL_cportz00_2485;

				{	/* Reduce/ftypec.scm 15 */
					obj_t BgL_stringz00_2492;

					BgL_stringz00_2492 = BGl_string1882z00zzreduce_flowzd2typeczd2;
					{	/* Reduce/ftypec.scm 15 */
						obj_t BgL_startz00_2493;

						BgL_startz00_2493 = BINT(0L);
						{	/* Reduce/ftypec.scm 15 */
							obj_t BgL_endz00_2494;

							BgL_endz00_2494 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2492)));
							{	/* Reduce/ftypec.scm 15 */

								BgL_cportz00_2485 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2492, BgL_startz00_2493, BgL_endz00_2494);
				}}}}
				{
					long BgL_iz00_2486;

					BgL_iz00_2486 = 1L;
				BgL_loopz00_2487:
					if ((BgL_iz00_2486 == -1L))
						{	/* Reduce/ftypec.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Reduce/ftypec.scm 15 */
							{	/* Reduce/ftypec.scm 15 */
								obj_t BgL_arg1883z00_2488;

								{	/* Reduce/ftypec.scm 15 */

									{	/* Reduce/ftypec.scm 15 */
										obj_t BgL_locationz00_2490;

										BgL_locationz00_2490 = BBOOL(((bool_t) 0));
										{	/* Reduce/ftypec.scm 15 */

											BgL_arg1883z00_2488 =
												BGl_readz00zz__readerz00(BgL_cportz00_2485,
												BgL_locationz00_2490);
										}
									}
								}
								{	/* Reduce/ftypec.scm 15 */
									int BgL_tmpz00_2673;

									BgL_tmpz00_2673 = (int) (BgL_iz00_2486);
									CNST_TABLE_SET(BgL_tmpz00_2673, BgL_arg1883z00_2488);
							}}
							{	/* Reduce/ftypec.scm 15 */
								int BgL_auxz00_2491;

								BgL_auxz00_2491 = (int) ((BgL_iz00_2486 - 1L));
								{
									long BgL_iz00_2678;

									BgL_iz00_2678 = (long) (BgL_auxz00_2491);
									BgL_iz00_2486 = BgL_iz00_2678;
									goto BgL_loopz00_2487;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzreduce_flowzd2typeczd2(void)
	{
		{	/* Reduce/ftypec.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzreduce_flowzd2typeczd2(void)
	{
		{	/* Reduce/ftypec.scm 15 */
			BGl_za2typezd2checkszd2remainingza2z00zzreduce_flowzd2typeczd2 = 0L;
			BGl_za2typezd2checkszd2removedza2z00zzreduce_flowzd2typeczd2 = 0L;
			return BUNSPEC;
		}

	}



/* reduce-flow-type-check! */
	BGL_EXPORTED_DEF obj_t
		BGl_reducezd2flowzd2typezd2checkz12zc0zzreduce_flowzd2typeczd2(obj_t
		BgL_globalsz00_3)
	{
		{	/* Reduce/ftypec.scm 54 */
			{	/* Reduce/ftypec.scm 55 */
				obj_t BgL_list1320z00_1618;

				BgL_list1320z00_1618 =
					MAKE_YOUNG_PAIR(BGl_string1856z00zzreduce_flowzd2typeczd2, BNIL);
				BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1320z00_1618);
			}
			{
				obj_t BgL_l1268z00_1620;

				BgL_l1268z00_1620 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31321ze3z87_1621:
				if (PAIRP(BgL_l1268z00_1620))
					{	/* Reduce/ftypec.scm 56 */
						{	/* Reduce/ftypec.scm 57 */
							obj_t BgL_globalz00_1623;

							BgL_globalz00_1623 = CAR(BgL_l1268z00_1620);
							{	/* Reduce/ftypec.scm 57 */
								BgL_valuez00_bglt BgL_funz00_1624;

								BgL_funz00_1624 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_1623))))->
									BgL_valuez00);
								{	/* Reduce/ftypec.scm 57 */
									obj_t BgL_nodez00_1625;

									BgL_nodez00_1625 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_funz00_1624)))->BgL_bodyz00);
									{	/* Reduce/ftypec.scm 58 */

										{	/* Reduce/ftypec.scm 59 */
											BgL_nodez00_bglt BgL_arg1323z00_1626;

											BgL_arg1323z00_1626 =
												BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(
												((BgL_nodez00_bglt) BgL_nodez00_1625), BNIL);
											((((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_funz00_1624)))->
													BgL_bodyz00) =
												((obj_t) ((obj_t) BgL_arg1323z00_1626)), BUNSPEC);
										}
										BUNSPEC;
									}
								}
							}
						}
						{
							obj_t BgL_l1268z00_2697;

							BgL_l1268z00_2697 = CDR(BgL_l1268z00_1620);
							BgL_l1268z00_1620 = BgL_l1268z00_2697;
							goto BgL_zc3z04anonymousza31321ze3z87_1621;
						}
					}
				else
					{	/* Reduce/ftypec.scm 56 */
						((bool_t) 1);
					}
			}
			{	/* Reduce/ftypec.scm 62 */
				obj_t BgL_list1326z00_1629;

				{	/* Reduce/ftypec.scm 62 */
					obj_t BgL_arg1327z00_1630;

					{	/* Reduce/ftypec.scm 62 */
						obj_t BgL_arg1328z00_1631;

						{	/* Reduce/ftypec.scm 62 */
							obj_t BgL_arg1329z00_1632;

							{	/* Reduce/ftypec.scm 62 */
								obj_t BgL_arg1331z00_1633;

								BgL_arg1331z00_1633 =
									MAKE_YOUNG_PAIR(BGl_string1857z00zzreduce_flowzd2typeczd2,
									BNIL);
								BgL_arg1329z00_1632 =
									MAKE_YOUNG_PAIR(BINT
									(BGl_za2typezd2checkszd2remainingza2z00zzreduce_flowzd2typeczd2),
									BgL_arg1331z00_1633);
							}
							BgL_arg1328z00_1631 =
								MAKE_YOUNG_PAIR(BGl_string1858z00zzreduce_flowzd2typeczd2,
								BgL_arg1329z00_1632);
						}
						BgL_arg1327z00_1630 =
							MAKE_YOUNG_PAIR(BINT
							(BGl_za2typezd2checkszd2removedza2z00zzreduce_flowzd2typeczd2),
							BgL_arg1328z00_1631);
					}
					BgL_list1326z00_1629 =
						MAKE_YOUNG_PAIR(BGl_string1859z00zzreduce_flowzd2typeczd2,
						BgL_arg1327z00_1630);
				}
				BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1326z00_1629);
			}
			return BgL_globalsz00_3;
		}

	}



/* &reduce-flow-type-check! */
	obj_t BGl_z62reducezd2flowzd2typezd2checkz12za2zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2404, obj_t BgL_globalsz00_2405)
	{
		{	/* Reduce/ftypec.scm 54 */
			return
				BGl_reducezd2flowzd2typezd2checkz12zc0zzreduce_flowzd2typeczd2
				(BgL_globalsz00_2405);
		}

	}



/* node-typec*! */
	obj_t BGl_nodezd2typecza2z12z62zzreduce_flowzd2typeczd2(obj_t
		BgL_nodeza2za2_40, obj_t BgL_stackz00_41)
	{
		{	/* Reduce/ftypec.scm 304 */
			{
				obj_t BgL_nodeza2za2_1635;

				BgL_nodeza2za2_1635 = BgL_nodeza2za2_40;
			BgL_zc3z04anonymousza31332ze3z87_1636:
				if (NULLP(BgL_nodeza2za2_1635))
					{	/* Reduce/ftypec.scm 306 */
						return CNST_TABLE_REF(0);
					}
				else
					{	/* Reduce/ftypec.scm 306 */
						{	/* Reduce/ftypec.scm 309 */
							BgL_nodez00_bglt BgL_arg1335z00_1638;

							{	/* Reduce/ftypec.scm 309 */
								obj_t BgL_arg1339z00_1639;

								BgL_arg1339z00_1639 = CAR(((obj_t) BgL_nodeza2za2_1635));
								BgL_arg1335z00_1638 =
									BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(
									((BgL_nodez00_bglt) BgL_arg1339z00_1639), BgL_stackz00_41);
							}
							{	/* Reduce/ftypec.scm 309 */
								obj_t BgL_auxz00_2718;
								obj_t BgL_tmpz00_2716;

								BgL_auxz00_2718 = ((obj_t) BgL_arg1335z00_1638);
								BgL_tmpz00_2716 = ((obj_t) BgL_nodeza2za2_1635);
								SET_CAR(BgL_tmpz00_2716, BgL_auxz00_2718);
							}
						}
						{	/* Reduce/ftypec.scm 310 */
							obj_t BgL_arg1340z00_1640;

							BgL_arg1340z00_1640 = CDR(((obj_t) BgL_nodeza2za2_1635));
							{
								obj_t BgL_nodeza2za2_2723;

								BgL_nodeza2za2_2723 = BgL_arg1340z00_1640;
								BgL_nodeza2za2_1635 = BgL_nodeza2za2_2723;
								goto BgL_zc3z04anonymousza31332ze3z87_1636;
							}
						}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzreduce_flowzd2typeczd2(void)
	{
		{	/* Reduce/ftypec.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzreduce_flowzd2typeczd2(void)
	{
		{	/* Reduce/ftypec.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_proc1860z00zzreduce_flowzd2typeczd2, BGl_nodez00zzast_nodez00,
				BGl_string1861z00zzreduce_flowzd2typeczd2);
		}

	}



/* &node-typec!1279 */
	obj_t BGl_z62nodezd2typecz121279za2zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2407, obj_t BgL_nodez00_2408, obj_t BgL_stackz00_2409)
	{
		{	/* Reduce/ftypec.scm 76 */
			return ((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2408));
		}

	}



/* node-typec! */
	BgL_nodez00_bglt
		BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_nodez00_bglt
		BgL_nodez00_4, obj_t BgL_stackz00_5)
	{
		{	/* Reduce/ftypec.scm 76 */
			{	/* Reduce/ftypec.scm 76 */
				obj_t BgL_method1280z00_1647;

				{	/* Reduce/ftypec.scm 76 */
					obj_t BgL_res1848z00_2117;

					{	/* Reduce/ftypec.scm 76 */
						long BgL_objzd2classzd2numz00_2088;

						BgL_objzd2classzd2numz00_2088 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_4));
						{	/* Reduce/ftypec.scm 76 */
							obj_t BgL_arg1811z00_2089;

							BgL_arg1811z00_2089 =
								PROCEDURE_REF
								(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
								(int) (1L));
							{	/* Reduce/ftypec.scm 76 */
								int BgL_offsetz00_2092;

								BgL_offsetz00_2092 = (int) (BgL_objzd2classzd2numz00_2088);
								{	/* Reduce/ftypec.scm 76 */
									long BgL_offsetz00_2093;

									BgL_offsetz00_2093 =
										((long) (BgL_offsetz00_2092) - OBJECT_TYPE);
									{	/* Reduce/ftypec.scm 76 */
										long BgL_modz00_2094;

										BgL_modz00_2094 =
											(BgL_offsetz00_2093 >> (int) ((long) ((int) (4L))));
										{	/* Reduce/ftypec.scm 76 */
											long BgL_restz00_2096;

											BgL_restz00_2096 =
												(BgL_offsetz00_2093 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Reduce/ftypec.scm 76 */

												{	/* Reduce/ftypec.scm 76 */
													obj_t BgL_bucketz00_2098;

													BgL_bucketz00_2098 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2089), BgL_modz00_2094);
													BgL_res1848z00_2117 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2098), BgL_restz00_2096);
					}}}}}}}}
					BgL_method1280z00_1647 = BgL_res1848z00_2117;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL2(BgL_method1280z00_1647,
						((obj_t) BgL_nodez00_4), BgL_stackz00_5));
			}
		}

	}



/* &node-typec! */
	BgL_nodez00_bglt BGl_z62nodezd2typecz12za2zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2410, obj_t BgL_nodez00_2411, obj_t BgL_stackz00_2412)
	{
		{	/* Reduce/ftypec.scm 76 */
			return
				BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(
				((BgL_nodez00_bglt) BgL_nodez00_2411), BgL_stackz00_2412);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzreduce_flowzd2typeczd2(void)
	{
		{	/* Reduce/ftypec.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_sequencez00zzast_nodez00, BGl_proc1862z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_syncz00zzast_nodez00, BGl_proc1864z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1865z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_funcallz00zzast_nodez00, BGl_proc1866z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_externz00zzast_nodez00, BGl_proc1867z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_castz00zzast_nodez00, BGl_proc1868z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_setqz00zzast_nodez00, BGl_proc1869z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_conditionalz00zzast_nodez00,
				BGl_proc1870z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_failz00zzast_nodez00, BGl_proc1871z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_switchz00zzast_nodez00, BGl_proc1872z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1873z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1874z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc1875z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc1876z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1877z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc1878z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1879z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_flowzd2typeczd2,
				BGl_appz00zzast_nodez00, BGl_proc1880z00zzreduce_flowzd2typeczd2,
				BGl_string1863z00zzreduce_flowzd2typeczd2);
		}

	}



/* &node-typec!-app1316 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2app1316z70zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2431, obj_t BgL_nodez00_2432, obj_t BgL_stackz00_2433)
	{
		{	/* Reduce/ftypec.scm 320 */
			BGl_nodezd2typecza2z12z62zzreduce_flowzd2typeczd2(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2432)))->BgL_argsz00),
				BgL_stackz00_2433);
			return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2432));
		}

	}



/* &node-typec!-box-ref1314 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2boxzd2ref1314za2zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2434, obj_t BgL_nodez00_2435, obj_t BgL_stackz00_2436)
	{
		{	/* Reduce/ftypec.scm 296 */
			{
				BgL_varz00_bglt BgL_auxz00_2784;

				{	/* Reduce/ftypec.scm 298 */
					BgL_varz00_bglt BgL_arg1701z00_2499;

					BgL_arg1701z00_2499 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2435)))->BgL_varz00);
					BgL_auxz00_2784 =
						((BgL_varz00_bglt)
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(
							((BgL_nodez00_bglt) BgL_arg1701z00_2499), BgL_stackz00_2436));
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2435)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_2784), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2435));
		}

	}



/* &node-typec!-box-set!1312 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2boxzd2setz121312zb0zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2437, obj_t BgL_nodez00_2438, obj_t BgL_stackz00_2439)
	{
		{	/* Reduce/ftypec.scm 287 */
			{
				BgL_varz00_bglt BgL_auxz00_2794;

				{	/* Reduce/ftypec.scm 289 */
					BgL_varz00_bglt BgL_arg1699z00_2501;

					BgL_arg1699z00_2501 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2438)))->BgL_varz00);
					BgL_auxz00_2794 =
						((BgL_varz00_bglt)
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(
							((BgL_nodez00_bglt) BgL_arg1699z00_2501), BgL_stackz00_2439));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2438)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_2794), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2802;

				{	/* Reduce/ftypec.scm 290 */
					BgL_nodez00_bglt BgL_arg1700z00_2502;

					BgL_arg1700z00_2502 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2438)))->BgL_valuez00);
					BgL_auxz00_2802 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1700z00_2502,
						BgL_stackz00_2439);
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2438)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_2802), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2438));
		}

	}



/* &node-typec!-make-box1310 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2makezd2box1310za2zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2440, obj_t BgL_nodez00_2441, obj_t BgL_stackz00_2442)
	{
		{	/* Reduce/ftypec.scm 279 */
			{
				BgL_nodez00_bglt BgL_auxz00_2810;

				{	/* Reduce/ftypec.scm 281 */
					BgL_nodez00_bglt BgL_arg1692z00_2504;

					BgL_arg1692z00_2504 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2441)))->BgL_valuez00);
					BgL_auxz00_2810 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1692z00_2504,
						BgL_stackz00_2442);
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2441)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_2810), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2441));
		}

	}



/* &node-typec!-jump-ex-1308 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2jumpzd2exzd21308z70zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2443, obj_t BgL_nodez00_2444, obj_t BgL_stackz00_2445)
	{
		{	/* Reduce/ftypec.scm 270 */
			{
				BgL_nodez00_bglt BgL_auxz00_2818;

				{	/* Reduce/ftypec.scm 272 */
					BgL_nodez00_bglt BgL_arg1689z00_2506;

					BgL_arg1689z00_2506 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2444)))->BgL_exitz00);
					BgL_auxz00_2818 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1689z00_2506,
						BgL_stackz00_2445);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2444)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_2818), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2824;

				{	/* Reduce/ftypec.scm 273 */
					BgL_nodez00_bglt BgL_arg1691z00_2507;

					BgL_arg1691z00_2507 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2444)))->
						BgL_valuez00);
					BgL_auxz00_2824 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1691z00_2507,
						BgL_stackz00_2445);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2444)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_2824), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2444));
		}

	}



/* &node-typec!-set-ex-i1306 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2setzd2exzd2i1306z70zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2446, obj_t BgL_nodez00_2447, obj_t BgL_stackz00_2448)
	{
		{	/* Reduce/ftypec.scm 260 */
			{
				BgL_nodez00_bglt BgL_auxz00_2832;

				{	/* Reduce/ftypec.scm 262 */
					BgL_nodez00_bglt BgL_arg1678z00_2509;

					BgL_arg1678z00_2509 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2447)))->BgL_bodyz00);
					BgL_auxz00_2832 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1678z00_2509,
						BgL_stackz00_2448);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2447)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2832), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2838;

				{	/* Reduce/ftypec.scm 263 */
					BgL_nodez00_bglt BgL_arg1681z00_2510;

					BgL_arg1681z00_2510 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2447)))->
						BgL_onexitz00);
					BgL_auxz00_2838 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1681z00_2510,
						BgL_stackz00_2448);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2447)))->
						BgL_onexitz00) = ((BgL_nodez00_bglt) BgL_auxz00_2838), BUNSPEC);
			}
			{
				BgL_varz00_bglt BgL_auxz00_2844;

				{	/* Reduce/ftypec.scm 264 */
					BgL_varz00_bglt BgL_arg1688z00_2511;

					BgL_arg1688z00_2511 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2447)))->BgL_varz00);
					BgL_auxz00_2844 =
						((BgL_varz00_bglt)
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(
							((BgL_nodez00_bglt) BgL_arg1688z00_2511), BgL_stackz00_2448));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2447)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_2844), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2447));
		}

	}



/* &node-typec!-let-var1304 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2letzd2var1304za2zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2449, obj_t BgL_nodez00_2450, obj_t BgL_stackz00_2451)
	{
		{	/* Reduce/ftypec.scm 249 */
			{	/* Reduce/ftypec.scm 251 */
				obj_t BgL_g1278z00_2513;

				BgL_g1278z00_2513 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2450)))->BgL_bindingsz00);
				{
					obj_t BgL_l1276z00_2515;

					BgL_l1276z00_2515 = BgL_g1278z00_2513;
				BgL_zc3z04anonymousza31651ze3z87_2514:
					if (PAIRP(BgL_l1276z00_2515))
						{	/* Reduce/ftypec.scm 251 */
							{	/* Reduce/ftypec.scm 252 */
								obj_t BgL_bindingz00_2516;

								BgL_bindingz00_2516 = CAR(BgL_l1276z00_2515);
								{	/* Reduce/ftypec.scm 252 */
									BgL_nodez00_bglt BgL_arg1654z00_2517;

									{	/* Reduce/ftypec.scm 252 */
										obj_t BgL_arg1661z00_2518;

										BgL_arg1661z00_2518 = CDR(((obj_t) BgL_bindingz00_2516));
										BgL_arg1654z00_2517 =
											BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(
											((BgL_nodez00_bglt) BgL_arg1661z00_2518),
											BgL_stackz00_2451);
									}
									{	/* Reduce/ftypec.scm 252 */
										obj_t BgL_auxz00_2865;
										obj_t BgL_tmpz00_2863;

										BgL_auxz00_2865 = ((obj_t) BgL_arg1654z00_2517);
										BgL_tmpz00_2863 = ((obj_t) BgL_bindingz00_2516);
										SET_CDR(BgL_tmpz00_2863, BgL_auxz00_2865);
									}
								}
							}
							{
								obj_t BgL_l1276z00_2868;

								BgL_l1276z00_2868 = CDR(BgL_l1276z00_2515);
								BgL_l1276z00_2515 = BgL_l1276z00_2868;
								goto BgL_zc3z04anonymousza31651ze3z87_2514;
							}
						}
					else
						{	/* Reduce/ftypec.scm 251 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2870;

				{	/* Reduce/ftypec.scm 254 */
					BgL_nodez00_bglt BgL_arg1675z00_2519;

					BgL_arg1675z00_2519 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2450)))->BgL_bodyz00);
					BgL_auxz00_2870 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1675z00_2519,
						BgL_stackz00_2451);
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2450)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2870), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_2450));
		}

	}



/* &node-typec!-let-fun1302 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2letzd2fun1302za2zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2452, obj_t BgL_nodez00_2453, obj_t BgL_stackz00_2454)
	{
		{	/* Reduce/ftypec.scm 237 */
			{	/* Reduce/ftypec.scm 239 */
				obj_t BgL_g1275z00_2521;

				BgL_g1275z00_2521 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2453)))->BgL_localsz00);
				{
					obj_t BgL_l1273z00_2523;

					BgL_l1273z00_2523 = BgL_g1275z00_2521;
				BgL_zc3z04anonymousza31628ze3z87_2522:
					if (PAIRP(BgL_l1273z00_2523))
						{	/* Reduce/ftypec.scm 239 */
							{	/* Reduce/ftypec.scm 240 */
								obj_t BgL_localz00_2524;

								BgL_localz00_2524 = CAR(BgL_l1273z00_2523);
								{	/* Reduce/ftypec.scm 240 */
									BgL_valuez00_bglt BgL_funz00_2525;

									BgL_funz00_2525 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2524))))->
										BgL_valuez00);
									{	/* Reduce/ftypec.scm 241 */
										BgL_nodez00_bglt BgL_arg1630z00_2526;

										{	/* Reduce/ftypec.scm 241 */
											obj_t BgL_arg1642z00_2527;

											BgL_arg1642z00_2527 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2525)))->
												BgL_bodyz00);
											BgL_arg1630z00_2526 =
												BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2((
													(BgL_nodez00_bglt) BgL_arg1642z00_2527),
												BgL_stackz00_2454);
										}
										((((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2525)))->
												BgL_bodyz00) =
											((obj_t) ((obj_t) BgL_arg1630z00_2526)), BUNSPEC);
									}
								}
							}
							{
								obj_t BgL_l1273z00_2893;

								BgL_l1273z00_2893 = CDR(BgL_l1273z00_2523);
								BgL_l1273z00_2523 = BgL_l1273z00_2893;
								goto BgL_zc3z04anonymousza31628ze3z87_2522;
							}
						}
					else
						{	/* Reduce/ftypec.scm 239 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2895;

				{	/* Reduce/ftypec.scm 243 */
					BgL_nodez00_bglt BgL_arg1650z00_2528;

					BgL_arg1650z00_2528 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2453)))->BgL_bodyz00);
					BgL_auxz00_2895 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1650z00_2528,
						BgL_stackz00_2454);
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2453)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2895), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2453));
		}

	}



/* &node-typec!-switch1300 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2switch1300z70zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2455, obj_t BgL_nodez00_2456, obj_t BgL_stackz00_2457)
	{
		{	/* Reduce/ftypec.scm 226 */
			{
				BgL_nodez00_bglt BgL_auxz00_2903;

				{	/* Reduce/ftypec.scm 228 */
					BgL_nodez00_bglt BgL_arg1615z00_2530;

					BgL_arg1615z00_2530 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2456)))->BgL_testz00);
					BgL_auxz00_2903 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1615z00_2530,
						BgL_stackz00_2457);
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2456)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2903), BUNSPEC);
			}
			{	/* Reduce/ftypec.scm 229 */
				obj_t BgL_g1272z00_2531;

				BgL_g1272z00_2531 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2456)))->BgL_clausesz00);
				{
					obj_t BgL_l1270z00_2533;

					BgL_l1270z00_2533 = BgL_g1272z00_2531;
				BgL_zc3z04anonymousza31616ze3z87_2532:
					if (PAIRP(BgL_l1270z00_2533))
						{	/* Reduce/ftypec.scm 229 */
							{	/* Reduce/ftypec.scm 230 */
								obj_t BgL_clausez00_2534;

								BgL_clausez00_2534 = CAR(BgL_l1270z00_2533);
								{	/* Reduce/ftypec.scm 230 */
									BgL_nodez00_bglt BgL_arg1625z00_2535;

									{	/* Reduce/ftypec.scm 230 */
										obj_t BgL_arg1626z00_2536;

										BgL_arg1626z00_2536 = CDR(((obj_t) BgL_clausez00_2534));
										BgL_arg1625z00_2535 =
											BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(
											((BgL_nodez00_bglt) BgL_arg1626z00_2536),
											BgL_stackz00_2457);
									}
									{	/* Reduce/ftypec.scm 230 */
										obj_t BgL_auxz00_2920;
										obj_t BgL_tmpz00_2918;

										BgL_auxz00_2920 = ((obj_t) BgL_arg1625z00_2535);
										BgL_tmpz00_2918 = ((obj_t) BgL_clausez00_2534);
										SET_CDR(BgL_tmpz00_2918, BgL_auxz00_2920);
									}
								}
							}
							{
								obj_t BgL_l1270z00_2923;

								BgL_l1270z00_2923 = CDR(BgL_l1270z00_2533);
								BgL_l1270z00_2533 = BgL_l1270z00_2923;
								goto BgL_zc3z04anonymousza31616ze3z87_2532;
							}
						}
					else
						{	/* Reduce/ftypec.scm 229 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_2456));
		}

	}



/* &node-typec!-fail1298 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2fail1298z70zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2458, obj_t BgL_nodez00_2459, obj_t BgL_stackz00_2460)
	{
		{	/* Reduce/ftypec.scm 216 */
			{
				BgL_nodez00_bglt BgL_auxz00_2927;

				{	/* Reduce/ftypec.scm 218 */
					BgL_nodez00_bglt BgL_arg1609z00_2538;

					BgL_arg1609z00_2538 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2459)))->BgL_procz00);
					BgL_auxz00_2927 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1609z00_2538,
						BgL_stackz00_2460);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2459)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2927), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2933;

				{	/* Reduce/ftypec.scm 219 */
					BgL_nodez00_bglt BgL_arg1611z00_2539;

					BgL_arg1611z00_2539 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2459)))->BgL_msgz00);
					BgL_auxz00_2933 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1611z00_2539,
						BgL_stackz00_2460);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2459)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2933), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2939;

				{	/* Reduce/ftypec.scm 220 */
					BgL_nodez00_bglt BgL_arg1613z00_2540;

					BgL_arg1613z00_2540 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2459)))->BgL_objz00);
					BgL_auxz00_2939 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1613z00_2540,
						BgL_stackz00_2460);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2459)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2939), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_2459));
		}

	}



/* &node-typec!-conditio1296 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2conditio1296z70zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2461, obj_t BgL_nodez00_2462, obj_t BgL_stackz00_2463)
	{
		{	/* Reduce/ftypec.scm 143 */
			{
				BgL_nodez00_bglt BgL_nodez00_2543;

				{
					BgL_nodez00_bglt BgL_auxz00_2947;

					{	/* Reduce/ftypec.scm 187 */
						BgL_nodez00_bglt BgL_arg1376z00_2596;

						BgL_arg1376z00_2596 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_2462)))->BgL_testz00);
						BgL_auxz00_2947 =
							BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2
							(BgL_arg1376z00_2596, BgL_stackz00_2463);
					}
					((((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_2462)))->BgL_testz00) =
						((BgL_nodez00_bglt) BgL_auxz00_2947), BUNSPEC);
				}
				{	/* Reduce/ftypec.scm 188 */
					obj_t BgL_typecz00_2597;

					BgL_nodez00_2543 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2462)))->BgL_testz00);
					{	/* Reduce/ftypec.scm 145 */
						bool_t BgL_test1920z00_2953;

						{	/* Reduce/ftypec.scm 145 */
							obj_t BgL_classz00_2544;

							BgL_classz00_2544 = BGl_appz00zzast_nodez00;
							{	/* Reduce/ftypec.scm 145 */
								BgL_objectz00_bglt BgL_arg1807z00_2545;

								{	/* Reduce/ftypec.scm 145 */
									obj_t BgL_tmpz00_2954;

									BgL_tmpz00_2954 =
										((obj_t) ((BgL_objectz00_bglt) BgL_nodez00_2543));
									BgL_arg1807z00_2545 = (BgL_objectz00_bglt) (BgL_tmpz00_2954);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Reduce/ftypec.scm 145 */
										long BgL_idxz00_2546;

										BgL_idxz00_2546 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2545);
										BgL_test1920z00_2953 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2546 + 3L)) == BgL_classz00_2544);
									}
								else
									{	/* Reduce/ftypec.scm 145 */
										bool_t BgL_res1849z00_2549;

										{	/* Reduce/ftypec.scm 145 */
											obj_t BgL_oclassz00_2550;

											{	/* Reduce/ftypec.scm 145 */
												obj_t BgL_arg1815z00_2551;
												long BgL_arg1816z00_2552;

												BgL_arg1815z00_2551 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Reduce/ftypec.scm 145 */
													long BgL_arg1817z00_2553;

													BgL_arg1817z00_2553 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2545);
													BgL_arg1816z00_2552 =
														(BgL_arg1817z00_2553 - OBJECT_TYPE);
												}
												BgL_oclassz00_2550 =
													VECTOR_REF(BgL_arg1815z00_2551, BgL_arg1816z00_2552);
											}
											{	/* Reduce/ftypec.scm 145 */
												bool_t BgL__ortest_1115z00_2554;

												BgL__ortest_1115z00_2554 =
													(BgL_classz00_2544 == BgL_oclassz00_2550);
												if (BgL__ortest_1115z00_2554)
													{	/* Reduce/ftypec.scm 145 */
														BgL_res1849z00_2549 = BgL__ortest_1115z00_2554;
													}
												else
													{	/* Reduce/ftypec.scm 145 */
														long BgL_odepthz00_2555;

														{	/* Reduce/ftypec.scm 145 */
															obj_t BgL_arg1804z00_2556;

															BgL_arg1804z00_2556 = (BgL_oclassz00_2550);
															BgL_odepthz00_2555 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2556);
														}
														if ((3L < BgL_odepthz00_2555))
															{	/* Reduce/ftypec.scm 145 */
																obj_t BgL_arg1802z00_2557;

																{	/* Reduce/ftypec.scm 145 */
																	obj_t BgL_arg1803z00_2558;

																	BgL_arg1803z00_2558 = (BgL_oclassz00_2550);
																	BgL_arg1802z00_2557 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2558,
																		3L);
																}
																BgL_res1849z00_2549 =
																	(BgL_arg1802z00_2557 == BgL_classz00_2544);
															}
														else
															{	/* Reduce/ftypec.scm 145 */
																BgL_res1849z00_2549 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1920z00_2953 = BgL_res1849z00_2549;
									}
							}
						}
						if (BgL_test1920z00_2953)
							{	/* Reduce/ftypec.scm 147 */
								obj_t BgL_g1116z00_2559;

								BgL_g1116z00_2559 =
									BGl_isazd2ofzd2zztype_miscz00(BgL_nodez00_2543);
								if (CBOOL(BgL_g1116z00_2559))
									{	/* Reduce/ftypec.scm 151 */
										bool_t BgL_test1925z00_2980;

										{	/* Reduce/ftypec.scm 151 */
											obj_t BgL_arg1544z00_2560;

											{	/* Reduce/ftypec.scm 151 */
												obj_t BgL_pairz00_2561;

												BgL_pairz00_2561 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_2543)))->
													BgL_argsz00);
												BgL_arg1544z00_2560 = CAR(BgL_pairz00_2561);
											}
											{	/* Reduce/ftypec.scm 151 */
												obj_t BgL_classz00_2562;

												BgL_classz00_2562 = BGl_varz00zzast_nodez00;
												if (BGL_OBJECTP(BgL_arg1544z00_2560))
													{	/* Reduce/ftypec.scm 151 */
														BgL_objectz00_bglt BgL_arg1807z00_2563;

														BgL_arg1807z00_2563 =
															(BgL_objectz00_bglt) (BgL_arg1544z00_2560);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Reduce/ftypec.scm 151 */
																long BgL_idxz00_2564;

																BgL_idxz00_2564 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_2563);
																BgL_test1925z00_2980 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_2564 + 2L)) ==
																	BgL_classz00_2562);
															}
														else
															{	/* Reduce/ftypec.scm 151 */
																bool_t BgL_res1850z00_2567;

																{	/* Reduce/ftypec.scm 151 */
																	obj_t BgL_oclassz00_2568;

																	{	/* Reduce/ftypec.scm 151 */
																		obj_t BgL_arg1815z00_2569;
																		long BgL_arg1816z00_2570;

																		BgL_arg1815z00_2569 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Reduce/ftypec.scm 151 */
																			long BgL_arg1817z00_2571;

																			BgL_arg1817z00_2571 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_2563);
																			BgL_arg1816z00_2570 =
																				(BgL_arg1817z00_2571 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_2568 =
																			VECTOR_REF(BgL_arg1815z00_2569,
																			BgL_arg1816z00_2570);
																	}
																	{	/* Reduce/ftypec.scm 151 */
																		bool_t BgL__ortest_1115z00_2572;

																		BgL__ortest_1115z00_2572 =
																			(BgL_classz00_2562 == BgL_oclassz00_2568);
																		if (BgL__ortest_1115z00_2572)
																			{	/* Reduce/ftypec.scm 151 */
																				BgL_res1850z00_2567 =
																					BgL__ortest_1115z00_2572;
																			}
																		else
																			{	/* Reduce/ftypec.scm 151 */
																				long BgL_odepthz00_2573;

																				{	/* Reduce/ftypec.scm 151 */
																					obj_t BgL_arg1804z00_2574;

																					BgL_arg1804z00_2574 =
																						(BgL_oclassz00_2568);
																					BgL_odepthz00_2573 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_2574);
																				}
																				if ((2L < BgL_odepthz00_2573))
																					{	/* Reduce/ftypec.scm 151 */
																						obj_t BgL_arg1802z00_2575;

																						{	/* Reduce/ftypec.scm 151 */
																							obj_t BgL_arg1803z00_2576;

																							BgL_arg1803z00_2576 =
																								(BgL_oclassz00_2568);
																							BgL_arg1802z00_2575 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_2576, 2L);
																						}
																						BgL_res1850z00_2567 =
																							(BgL_arg1802z00_2575 ==
																							BgL_classz00_2562);
																					}
																				else
																					{	/* Reduce/ftypec.scm 151 */
																						BgL_res1850z00_2567 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test1925z00_2980 = BgL_res1850z00_2567;
															}
													}
												else
													{	/* Reduce/ftypec.scm 151 */
														BgL_test1925z00_2980 = ((bool_t) 0);
													}
											}
										}
										if (BgL_test1925z00_2980)
											{	/* Reduce/ftypec.scm 151 */
												BgL_typecz00_2597 = BgL_g1116z00_2559;
											}
										else
											{	/* Reduce/ftypec.scm 151 */
												BgL_typecz00_2597 = BFALSE;
											}
									}
								else
									{	/* Reduce/ftypec.scm 155 */
										bool_t BgL_test1930z00_3006;

										{	/* Reduce/ftypec.scm 155 */
											obj_t BgL_tmpz00_3007;

											BgL_tmpz00_3007 =
												(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_nodez00_2543)))->
												BgL_argsz00);
											BgL_test1930z00_3006 = PAIRP(BgL_tmpz00_3007);
										}
										if (BgL_test1930z00_3006)
											{	/* Reduce/ftypec.scm 156 */
												bool_t BgL_test1931z00_3011;

												{	/* Reduce/ftypec.scm 156 */
													obj_t BgL_tmpz00_3012;

													{	/* Reduce/ftypec.scm 156 */
														obj_t BgL_pairz00_2577;

														BgL_pairz00_2577 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_2543)))->
															BgL_argsz00);
														BgL_tmpz00_3012 = CDR(BgL_pairz00_2577);
													}
													BgL_test1931z00_3011 = NULLP(BgL_tmpz00_3012);
												}
												if (BgL_test1931z00_3011)
													{	/* Reduce/ftypec.scm 157 */
														bool_t BgL_test1932z00_3017;

														{	/* Reduce/ftypec.scm 157 */
															obj_t BgL_arg1565z00_2578;

															{	/* Reduce/ftypec.scm 157 */
																obj_t BgL_pairz00_2579;

																BgL_pairz00_2579 =
																	(((BgL_appz00_bglt) COBJECT(
																			((BgL_appz00_bglt) BgL_nodez00_2543)))->
																	BgL_argsz00);
																BgL_arg1565z00_2578 = CAR(BgL_pairz00_2579);
															}
															{	/* Reduce/ftypec.scm 157 */
																obj_t BgL_classz00_2580;

																BgL_classz00_2580 = BGl_varz00zzast_nodez00;
																if (BGL_OBJECTP(BgL_arg1565z00_2578))
																	{	/* Reduce/ftypec.scm 157 */
																		BgL_objectz00_bglt BgL_arg1807z00_2581;

																		BgL_arg1807z00_2581 =
																			(BgL_objectz00_bglt)
																			(BgL_arg1565z00_2578);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Reduce/ftypec.scm 157 */
																				long BgL_idxz00_2582;

																				BgL_idxz00_2582 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2581);
																				BgL_test1932z00_3017 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2582 + 2L)) ==
																					BgL_classz00_2580);
																			}
																		else
																			{	/* Reduce/ftypec.scm 157 */
																				bool_t BgL_res1851z00_2585;

																				{	/* Reduce/ftypec.scm 157 */
																					obj_t BgL_oclassz00_2586;

																					{	/* Reduce/ftypec.scm 157 */
																						obj_t BgL_arg1815z00_2587;
																						long BgL_arg1816z00_2588;

																						BgL_arg1815z00_2587 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Reduce/ftypec.scm 157 */
																							long BgL_arg1817z00_2589;

																							BgL_arg1817z00_2589 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2581);
																							BgL_arg1816z00_2588 =
																								(BgL_arg1817z00_2589 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2586 =
																							VECTOR_REF(BgL_arg1815z00_2587,
																							BgL_arg1816z00_2588);
																					}
																					{	/* Reduce/ftypec.scm 157 */
																						bool_t BgL__ortest_1115z00_2590;

																						BgL__ortest_1115z00_2590 =
																							(BgL_classz00_2580 ==
																							BgL_oclassz00_2586);
																						if (BgL__ortest_1115z00_2590)
																							{	/* Reduce/ftypec.scm 157 */
																								BgL_res1851z00_2585 =
																									BgL__ortest_1115z00_2590;
																							}
																						else
																							{	/* Reduce/ftypec.scm 157 */
																								long BgL_odepthz00_2591;

																								{	/* Reduce/ftypec.scm 157 */
																									obj_t BgL_arg1804z00_2592;

																									BgL_arg1804z00_2592 =
																										(BgL_oclassz00_2586);
																									BgL_odepthz00_2591 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2592);
																								}
																								if ((2L < BgL_odepthz00_2591))
																									{	/* Reduce/ftypec.scm 157 */
																										obj_t BgL_arg1802z00_2593;

																										{	/* Reduce/ftypec.scm 157 */
																											obj_t BgL_arg1803z00_2594;

																											BgL_arg1803z00_2594 =
																												(BgL_oclassz00_2586);
																											BgL_arg1802z00_2593 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2594,
																												2L);
																										}
																										BgL_res1851z00_2585 =
																											(BgL_arg1802z00_2593 ==
																											BgL_classz00_2580);
																									}
																								else
																									{	/* Reduce/ftypec.scm 157 */
																										BgL_res1851z00_2585 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test1932z00_3017 =
																					BgL_res1851z00_2585;
																			}
																	}
																else
																	{	/* Reduce/ftypec.scm 157 */
																		BgL_test1932z00_3017 = ((bool_t) 0);
																	}
															}
														}
														if (BgL_test1932z00_3017)
															{	/* Reduce/ftypec.scm 158 */
																bool_t BgL_test1937z00_3043;

																{	/* Reduce/ftypec.scm 158 */
																	obj_t BgL_tmpz00_3044;

																	{
																		BgL_variablez00_bglt BgL_auxz00_3045;

																		{
																			BgL_varz00_bglt BgL_auxz00_3046;

																			{	/* Reduce/ftypec.scm 158 */
																				obj_t BgL_pairz00_2595;

																				BgL_pairz00_2595 =
																					(((BgL_appz00_bglt) COBJECT(
																							((BgL_appz00_bglt)
																								BgL_nodez00_2543)))->
																					BgL_argsz00);
																				BgL_auxz00_3046 =
																					((BgL_varz00_bglt)
																					CAR(BgL_pairz00_2595));
																			}
																			BgL_auxz00_3045 =
																				(((BgL_varz00_bglt)
																					COBJECT(BgL_auxz00_3046))->
																				BgL_variablez00);
																		}
																		BgL_tmpz00_3044 =
																			(((BgL_variablez00_bglt)
																				COBJECT(BgL_auxz00_3045))->
																			BgL_accessz00);
																	}
																	BgL_test1937z00_3043 =
																		(BgL_tmpz00_3044 == CNST_TABLE_REF(1));
																}
																if (BgL_test1937z00_3043)
																	{	/* Reduce/ftypec.scm 161 */

																		BgL_typecz00_2597 =
																			(((BgL_funz00_bglt) COBJECT(
																					((BgL_funz00_bglt)
																						(((BgL_variablez00_bglt) COBJECT(
																									(((BgL_varz00_bglt) COBJECT(
																												(((BgL_appz00_bglt)
																														COBJECT((
																																(BgL_appz00_bglt)
																																BgL_nodez00_2543)))->
																													BgL_funz00)))->
																										BgL_variablez00)))->
																							BgL_valuez00))))->
																			BgL_predicatezd2ofzd2);
																	}
																else
																	{	/* Reduce/ftypec.scm 158 */
																		BgL_typecz00_2597 = BFALSE;
																	}
															}
														else
															{	/* Reduce/ftypec.scm 157 */
																BgL_typecz00_2597 = BFALSE;
															}
													}
												else
													{	/* Reduce/ftypec.scm 156 */
														BgL_typecz00_2597 = BFALSE;
													}
											}
										else
											{	/* Reduce/ftypec.scm 155 */
												BgL_typecz00_2597 = BFALSE;
											}
									}
							}
						else
							{	/* Reduce/ftypec.scm 145 */
								BgL_typecz00_2597 = BFALSE;
							}
					}
					{	/* Reduce/ftypec.scm 189 */
						bool_t BgL_test1938z00_3063;

						{	/* Reduce/ftypec.scm 189 */
							obj_t BgL_classz00_2598;

							BgL_classz00_2598 = BGl_typez00zztype_typez00;
							if (BGL_OBJECTP(BgL_typecz00_2597))
								{	/* Reduce/ftypec.scm 189 */
									BgL_objectz00_bglt BgL_arg1807z00_2599;

									BgL_arg1807z00_2599 =
										(BgL_objectz00_bglt) (BgL_typecz00_2597);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Reduce/ftypec.scm 189 */
											long BgL_idxz00_2600;

											BgL_idxz00_2600 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2599);
											BgL_test1938z00_3063 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2600 + 1L)) == BgL_classz00_2598);
										}
									else
										{	/* Reduce/ftypec.scm 189 */
											bool_t BgL_res1852z00_2603;

											{	/* Reduce/ftypec.scm 189 */
												obj_t BgL_oclassz00_2604;

												{	/* Reduce/ftypec.scm 189 */
													obj_t BgL_arg1815z00_2605;
													long BgL_arg1816z00_2606;

													BgL_arg1815z00_2605 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Reduce/ftypec.scm 189 */
														long BgL_arg1817z00_2607;

														BgL_arg1817z00_2607 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2599);
														BgL_arg1816z00_2606 =
															(BgL_arg1817z00_2607 - OBJECT_TYPE);
													}
													BgL_oclassz00_2604 =
														VECTOR_REF(BgL_arg1815z00_2605,
														BgL_arg1816z00_2606);
												}
												{	/* Reduce/ftypec.scm 189 */
													bool_t BgL__ortest_1115z00_2608;

													BgL__ortest_1115z00_2608 =
														(BgL_classz00_2598 == BgL_oclassz00_2604);
													if (BgL__ortest_1115z00_2608)
														{	/* Reduce/ftypec.scm 189 */
															BgL_res1852z00_2603 = BgL__ortest_1115z00_2608;
														}
													else
														{	/* Reduce/ftypec.scm 189 */
															long BgL_odepthz00_2609;

															{	/* Reduce/ftypec.scm 189 */
																obj_t BgL_arg1804z00_2610;

																BgL_arg1804z00_2610 = (BgL_oclassz00_2604);
																BgL_odepthz00_2609 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2610);
															}
															if ((1L < BgL_odepthz00_2609))
																{	/* Reduce/ftypec.scm 189 */
																	obj_t BgL_arg1802z00_2611;

																	{	/* Reduce/ftypec.scm 189 */
																		obj_t BgL_arg1803z00_2612;

																		BgL_arg1803z00_2612 = (BgL_oclassz00_2604);
																		BgL_arg1802z00_2611 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2612, 1L);
																	}
																	BgL_res1852z00_2603 =
																		(BgL_arg1802z00_2611 == BgL_classz00_2598);
																}
															else
																{	/* Reduce/ftypec.scm 189 */
																	BgL_res1852z00_2603 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1938z00_3063 = BgL_res1852z00_2603;
										}
								}
							else
								{	/* Reduce/ftypec.scm 189 */
									BgL_test1938z00_3063 = ((bool_t) 0);
								}
						}
						if (BgL_test1938z00_3063)
							{	/* Reduce/ftypec.scm 194 */
								obj_t BgL_argsz00_2613;

								BgL_argsz00_2613 =
									(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt)
												(((BgL_conditionalz00_bglt) COBJECT(
															((BgL_conditionalz00_bglt) BgL_nodez00_2462)))->
													BgL_testz00))))->BgL_argsz00);
								{	/* Reduce/ftypec.scm 194 */
									BgL_variablez00_bglt BgL_varz00_2614;

									BgL_varz00_2614 =
										(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt)
													CAR(((obj_t) BgL_argsz00_2613)))))->BgL_variablez00);
									{	/* Reduce/ftypec.scm 195 */
										obj_t BgL_typesz00_2615;

										BgL_typesz00_2615 =
											BGl_assqz00zz__r4_pairs_and_lists_6_3z00(
											((obj_t) BgL_varz00_2614), BgL_stackz00_2463);
										{	/* Reduce/ftypec.scm 196 */
											obj_t BgL_tzd2stackzd2_2616;

											{	/* Reduce/ftypec.scm 197 */
												obj_t BgL_arg1485z00_2617;

												{	/* Reduce/ftypec.scm 197 */
													obj_t BgL_arg1489z00_2618;

													BgL_arg1489z00_2618 =
														MAKE_YOUNG_PAIR(BgL_typecz00_2597, BTRUE);
													BgL_arg1485z00_2617 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_varz00_2614), BgL_arg1489z00_2618);
												}
												BgL_tzd2stackzd2_2616 =
													MAKE_YOUNG_PAIR(BgL_arg1485z00_2617,
													BgL_stackz00_2463);
											}
											{	/* Reduce/ftypec.scm 197 */
												obj_t BgL_fzd2stackzd2_2619;

												{	/* Reduce/ftypec.scm 198 */
													obj_t BgL_arg1472z00_2620;

													{	/* Reduce/ftypec.scm 198 */
														obj_t BgL_arg1473z00_2621;

														BgL_arg1473z00_2621 =
															MAKE_YOUNG_PAIR(BgL_typecz00_2597, BFALSE);
														BgL_arg1472z00_2620 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_varz00_2614), BgL_arg1473z00_2621);
													}
													BgL_fzd2stackzd2_2619 =
														MAKE_YOUNG_PAIR(BgL_arg1472z00_2620,
														BgL_stackz00_2463);
												}
												{	/* Reduce/ftypec.scm 198 */

													{	/* Reduce/ftypec.scm 199 */
														bool_t BgL_test1943z00_3104;

														if (CBOOL(BgL_typesz00_2615))
															{	/* Reduce/ftypec.scm 199 */
																bool_t BgL_test1945z00_3107;

																{	/* Reduce/ftypec.scm 199 */
																	obj_t BgL_tmpz00_3108;

																	{	/* Reduce/ftypec.scm 199 */
																		obj_t BgL_pairz00_2622;

																		BgL_pairz00_2622 =
																			CDR(((obj_t) BgL_typesz00_2615));
																		BgL_tmpz00_3108 = CAR(BgL_pairz00_2622);
																	}
																	BgL_test1945z00_3107 =
																		(BgL_tmpz00_3108 == BgL_typecz00_2597);
																}
																if (BgL_test1945z00_3107)
																	{	/* Reduce/ftypec.scm 199 */
																		BgL_test1943z00_3104 = ((bool_t) 0);
																	}
																else
																	{	/* Reduce/ftypec.scm 199 */
																		BgL_test1943z00_3104 = ((bool_t) 1);
																	}
															}
														else
															{	/* Reduce/ftypec.scm 199 */
																BgL_test1943z00_3104 = ((bool_t) 1);
															}
														if (BgL_test1943z00_3104)
															{	/* Reduce/ftypec.scm 199 */
																BGl_za2typezd2checkszd2remainingza2z00zzreduce_flowzd2typeczd2
																	=
																	(1L +
																	BGl_za2typezd2checkszd2remainingza2z00zzreduce_flowzd2typeczd2);
																{
																	BgL_nodez00_bglt BgL_auxz00_3114;

																	{	/* Reduce/ftypec.scm 203 */
																		BgL_nodez00_bglt BgL_arg1434z00_2623;

																		BgL_arg1434z00_2623 =
																			(((BgL_conditionalz00_bglt) COBJECT(
																					((BgL_conditionalz00_bglt)
																						BgL_nodez00_2462)))->BgL_truez00);
																		BgL_auxz00_3114 =
																			BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2
																			(BgL_arg1434z00_2623,
																			BgL_tzd2stackzd2_2616);
																	}
																	((((BgL_conditionalz00_bglt) COBJECT(
																					((BgL_conditionalz00_bglt)
																						BgL_nodez00_2462)))->BgL_truez00) =
																		((BgL_nodez00_bglt) BgL_auxz00_3114),
																		BUNSPEC);
																}
																{
																	BgL_nodez00_bglt BgL_auxz00_3120;

																	{	/* Reduce/ftypec.scm 204 */
																		BgL_nodez00_bglt BgL_arg1437z00_2624;

																		BgL_arg1437z00_2624 =
																			(((BgL_conditionalz00_bglt) COBJECT(
																					((BgL_conditionalz00_bglt)
																						BgL_nodez00_2462)))->BgL_falsez00);
																		BgL_auxz00_3120 =
																			BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2
																			(BgL_arg1437z00_2624,
																			BgL_fzd2stackzd2_2619);
																	}
																	((((BgL_conditionalz00_bglt) COBJECT(
																					((BgL_conditionalz00_bglt)
																						BgL_nodez00_2462)))->BgL_falsez00) =
																		((BgL_nodez00_bglt) BgL_auxz00_3120),
																		BUNSPEC);
																}
																return
																	((BgL_nodez00_bglt)
																	((BgL_conditionalz00_bglt) BgL_nodez00_2462));
															}
														else
															{	/* Reduce/ftypec.scm 199 */
																BGl_za2typezd2checkszd2removedza2z00zzreduce_flowzd2typeczd2
																	=
																	(1L +
																	BGl_za2typezd2checkszd2removedza2z00zzreduce_flowzd2typeczd2);
																{	/* Reduce/ftypec.scm 209 */
																	bool_t BgL_test1946z00_3129;

																	{	/* Reduce/ftypec.scm 209 */
																		obj_t BgL_pairz00_2625;

																		BgL_pairz00_2625 =
																			CDR(((obj_t) BgL_typesz00_2615));
																		BgL_test1946z00_3129 =
																			CBOOL(CDR(BgL_pairz00_2625));
																	}
																	if (BgL_test1946z00_3129)
																		{	/* Reduce/ftypec.scm 209 */
																			return
																				BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2
																				((((BgL_conditionalz00_bglt)
																						COBJECT(((BgL_conditionalz00_bglt)
																								BgL_nodez00_2462)))->
																					BgL_truez00), BgL_tzd2stackzd2_2616);
																		}
																	else
																		{	/* Reduce/ftypec.scm 209 */
																			return
																				BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2
																				((((BgL_conditionalz00_bglt)
																						COBJECT(((BgL_conditionalz00_bglt)
																								BgL_nodez00_2462)))->
																					BgL_falsez00), BgL_fzd2stackzd2_2619);
																		}
																}
															}
													}
												}
											}
										}
									}
								}
							}
						else
							{	/* Reduce/ftypec.scm 189 */
								{
									BgL_nodez00_bglt BgL_auxz00_3140;

									{	/* Reduce/ftypec.scm 191 */
										BgL_nodez00_bglt BgL_arg1513z00_2626;

										BgL_arg1513z00_2626 =
											(((BgL_conditionalz00_bglt) COBJECT(
													((BgL_conditionalz00_bglt) BgL_nodez00_2462)))->
											BgL_truez00);
										BgL_auxz00_3140 =
											BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2
											(BgL_arg1513z00_2626, BgL_stackz00_2463);
									}
									((((BgL_conditionalz00_bglt) COBJECT(
													((BgL_conditionalz00_bglt) BgL_nodez00_2462)))->
											BgL_truez00) =
										((BgL_nodez00_bglt) BgL_auxz00_3140), BUNSPEC);
								}
								{
									BgL_nodez00_bglt BgL_auxz00_3146;

									{	/* Reduce/ftypec.scm 192 */
										BgL_nodez00_bglt BgL_arg1514z00_2627;

										BgL_arg1514z00_2627 =
											(((BgL_conditionalz00_bglt) COBJECT(
													((BgL_conditionalz00_bglt) BgL_nodez00_2462)))->
											BgL_falsez00);
										BgL_auxz00_3146 =
											BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2
											(BgL_arg1514z00_2627, BgL_stackz00_2463);
									}
									((((BgL_conditionalz00_bglt) COBJECT(
													((BgL_conditionalz00_bglt) BgL_nodez00_2462)))->
											BgL_falsez00) =
										((BgL_nodez00_bglt) BgL_auxz00_3146), BUNSPEC);
								}
								return
									((BgL_nodez00_bglt)
									((BgL_conditionalz00_bglt) BgL_nodez00_2462));
							}
					}
				}
			}
		}

	}



/* &node-typec!-setq1294 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2setq1294z70zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2464, obj_t BgL_nodez00_2465, obj_t BgL_stackz00_2466)
	{
		{	/* Reduce/ftypec.scm 134 */
			{
				BgL_nodez00_bglt BgL_auxz00_3154;

				{	/* Reduce/ftypec.scm 136 */
					BgL_nodez00_bglt BgL_arg1371z00_2629;

					BgL_arg1371z00_2629 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2465)))->BgL_valuez00);
					BgL_auxz00_3154 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1371z00_2629,
						BgL_stackz00_2466);
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2465)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3154), BUNSPEC);
			}
			{
				BgL_varz00_bglt BgL_auxz00_3160;

				{	/* Reduce/ftypec.scm 137 */
					BgL_varz00_bglt BgL_arg1375z00_2630;

					BgL_arg1375z00_2630 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2465)))->BgL_varz00);
					BgL_auxz00_3160 =
						((BgL_varz00_bglt)
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(
							((BgL_nodez00_bglt) BgL_arg1375z00_2630), BgL_stackz00_2466));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2465)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3160), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_2465));
		}

	}



/* &node-typec!-cast1292 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2cast1292z70zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2467, obj_t BgL_nodez00_2468, obj_t BgL_stackz00_2469)
	{
		{	/* Reduce/ftypec.scm 126 */
			BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2468)))->BgL_argz00),
				BgL_stackz00_2469);
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_2468));
		}

	}



/* &node-typec!-extern1290 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2extern1290z70zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2470, obj_t BgL_nodez00_2471, obj_t BgL_stackz00_2472)
	{
		{	/* Reduce/ftypec.scm 118 */
			BGl_nodezd2typecza2z12z62zzreduce_flowzd2typeczd2(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2471)))->BgL_exprza2za2),
				BgL_stackz00_2472);
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2471));
		}

	}



/* &node-typec!-funcall1288 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2funcall1288z70zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2473, obj_t BgL_nodez00_2474, obj_t BgL_stackz00_2475)
	{
		{	/* Reduce/ftypec.scm 109 */
			{
				BgL_nodez00_bglt BgL_auxz00_3180;

				{	/* Reduce/ftypec.scm 111 */
					BgL_nodez00_bglt BgL_arg1361z00_2634;

					BgL_arg1361z00_2634 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2474)))->BgL_funz00);
					BgL_auxz00_3180 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1361z00_2634,
						BgL_stackz00_2475);
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2474)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3180), BUNSPEC);
			}
			BGl_nodezd2typecza2z12z62zzreduce_flowzd2typeczd2(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2474)))->BgL_argsz00),
				BgL_stackz00_2475);
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_2474));
		}

	}



/* &node-typec!-app-ly1286 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2appzd2ly1286za2zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2476, obj_t BgL_nodez00_2477, obj_t BgL_stackz00_2478)
	{
		{	/* Reduce/ftypec.scm 100 */
			{
				BgL_nodez00_bglt BgL_auxz00_3191;

				{	/* Reduce/ftypec.scm 102 */
					BgL_nodez00_bglt BgL_arg1351z00_2636;

					BgL_arg1351z00_2636 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2477)))->BgL_funz00);
					BgL_auxz00_3191 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1351z00_2636,
						BgL_stackz00_2478);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2477)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3191), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3197;

				{	/* Reduce/ftypec.scm 103 */
					BgL_nodez00_bglt BgL_arg1352z00_2637;

					BgL_arg1352z00_2637 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2477)))->BgL_argz00);
					BgL_auxz00_3197 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1352z00_2637,
						BgL_stackz00_2478);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2477)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3197), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2477));
		}

	}



/* &node-typec!-sync1284 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2sync1284z70zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2479, obj_t BgL_nodez00_2480, obj_t BgL_stackz00_2481)
	{
		{	/* Reduce/ftypec.scm 90 */
			{
				BgL_nodez00_bglt BgL_auxz00_3205;

				{	/* Reduce/ftypec.scm 92 */
					BgL_nodez00_bglt BgL_arg1346z00_2639;

					BgL_arg1346z00_2639 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2480)))->BgL_mutexz00);
					BgL_auxz00_3205 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1346z00_2639,
						BgL_stackz00_2481);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2480)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3205), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3211;

				{	/* Reduce/ftypec.scm 93 */
					BgL_nodez00_bglt BgL_arg1348z00_2640;

					BgL_arg1348z00_2640 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2480)))->BgL_prelockz00);
					BgL_auxz00_3211 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1348z00_2640,
						BgL_stackz00_2481);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2480)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3211), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3217;

				{	/* Reduce/ftypec.scm 94 */
					BgL_nodez00_bglt BgL_arg1349z00_2641;

					BgL_arg1349z00_2641 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2480)))->BgL_bodyz00);
					BgL_auxz00_3217 =
						BGl_nodezd2typecz12zc0zzreduce_flowzd2typeczd2(BgL_arg1349z00_2641,
						BgL_stackz00_2481);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2480)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3217), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2480));
		}

	}



/* &node-typec!-sequence1282 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2sequence1282z70zzreduce_flowzd2typeczd2(obj_t
		BgL_envz00_2482, obj_t BgL_nodez00_2483, obj_t BgL_stackz00_2484)
	{
		{	/* Reduce/ftypec.scm 82 */
			BGl_nodezd2typecza2z12z62zzreduce_flowzd2typeczd2(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2483)))->BgL_nodesz00),
				BgL_stackz00_2484);
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_2483));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzreduce_flowzd2typeczd2(void)
	{
		{	/* Reduce/ftypec.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1881z00zzreduce_flowzd2typeczd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1881z00zzreduce_flowzd2typeczd2));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1881z00zzreduce_flowzd2typeczd2));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1881z00zzreduce_flowzd2typeczd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1881z00zzreduce_flowzd2typeczd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1881z00zzreduce_flowzd2typeczd2));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1881z00zzreduce_flowzd2typeczd2));
			BGl_modulezd2initializa7ationz75zztype_miscz00(49975086L,
				BSTRING_TO_STRING(BGl_string1881z00zzreduce_flowzd2typeczd2));
			BGl_modulezd2initializa7ationz75zzcoerce_coercez00(361167184L,
				BSTRING_TO_STRING(BGl_string1881z00zzreduce_flowzd2typeczd2));
			BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string1881z00zzreduce_flowzd2typeczd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1881z00zzreduce_flowzd2typeczd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1881z00zzreduce_flowzd2typeczd2));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string1881z00zzreduce_flowzd2typeczd2));
			return
				BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1881z00zzreduce_flowzd2typeczd2));
		}

	}

#ifdef __cplusplus
}
#endif
