/*===========================================================================*/
/*   (Reduce/typec.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Reduce/typec.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_REDUCE_TYPEC_TYPE_DEFINITIONS
#define BGL_REDUCE_TYPEC_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_REDUCE_TYPEC_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_returnz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2setzd2exzd2i1317z70zzreduce_typecz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2kwote1287z70zzreduce_typecz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2conditio1307z70zzreduce_typecz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzreduce_typecz00 = BUNSPEC;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2boxzd2setz121327zb0zzreduce_typecz00(obj_t, obj_t);
	extern obj_t BGl_closurez00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2makezd2box1325za2zzreduce_typecz00(obj_t, obj_t);
	extern obj_t BGl_za2pairzd2nilza2zd2zztype_cachez00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzreduce_typecz00(void);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2jumpzd2exzd21319z70zzreduce_typecz00(obj_t, obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2cast1303z70zzreduce_typecz00(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzreduce_typecz00(void);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2setq1305z70zzreduce_typecz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzreduce_typecz00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2fail1309z70zzreduce_typecz00(obj_t, obj_t);
	extern obj_t BGl_za2foreignza2z00zztype_cachez00;
	extern obj_t BGl_typez00zztype_typez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2letzd2var1315za2zzreduce_typecz00(obj_t, obj_t);
	static obj_t BGl_z62reducezd2typezd2checkz12z70zzreduce_typecz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static bool_t
		BGl_pairzd2ofzd2pairzd2nilzf3z21zzreduce_typecz00(BgL_nodez00_bglt);
	static obj_t BGl_za2pairzf3za2zf3zzreduce_typecz00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zzreduce_typecz00(void);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2boxzd2ref1329za2zzreduce_typecz00(obj_t, obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2return1323z70zzreduce_typecz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2extern1301z70zzreduce_typecz00(obj_t, obj_t);
	extern bool_t BGl_foreignzd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2switch1311z70zzreduce_typecz00(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t BGl_za2nullzf3za2zf3zzreduce_typecz00 = BUNSPEC;
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2funcall1299z70zzreduce_typecz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern bool_t BGl_typezd2lesszd2specificzf3zf3zztype_miscz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2sync1295z70zzreduce_typecz00(obj_t, obj_t);
	extern obj_t BGl_findzd2coercerzd2zztype_coercionz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2appzd2ly1297za2zzreduce_typecz00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62nodezd2typecz12za2zzreduce_typecz00(obj_t,
		obj_t);
	extern obj_t BGl_jclassz00zzobject_classz00;
	static obj_t BGl_nodezd2typecza2z12z62zzreduce_typecz00(obj_t);
	extern BgL_nodez00_bglt BGl_coercez12z12zzcoerce_coercez00(BgL_nodez00_bglt,
		obj_t, BgL_typez00_bglt, bool_t);
	static long BGl_za2typezd2checkszd2removedza2z00zzreduce_typecz00 = 0L;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2sequence1293z70zzreduce_typecz00(obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2closure1291z70zzreduce_typecz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzreduce_typecz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_coercionz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2retblock1321z70zzreduce_typecz00(obj_t, obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static long BGl_za2typezd2checkszd2remainingza2z00zzreduce_typecz00 = 0L;
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzreduce_typecz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzreduce_typecz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzreduce_typecz00(void);
	static obj_t BGl_z62nodezd2typecz121282za2zzreduce_typecz00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzreduce_typecz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_reducezd2typezd2checkz12z12zzreduce_typecz00(obj_t);
	extern bool_t BGl_sidezd2effectzf3z21zzeffect_effectz00(BgL_nodez00_bglt);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_retblockz00zzast_nodez00;
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_literalz00zzast_nodez00;
	extern obj_t BGl_switchz00zzast_nodez00;
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2letzd2fun1313za2zzreduce_typecz00(obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_isazd2ofzd2zztype_miscz00(BgL_nodez00_bglt);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2var1289z70zzreduce_typecz00(obj_t, obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2atom1285z70zzreduce_typecz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2app1331z70zzreduce_typecz00(obj_t, obj_t);
	static obj_t __cnst[5];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reducezd2typezd2checkz12zd2envzc0zzreduce_typecz00,
		BgL_bgl_za762reduceza7d2type1912z00,
		BGl_z62reducezd2typezd2checkz12z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1909z00zzreduce_typecz00,
		BgL_bgl_string1909za700za7za7r1913za7, "reduce_typec", 12);
	      DEFINE_STATIC_BGL_GENERIC(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71914za7,
		BGl_z62nodezd2typecz12za2zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1900z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71915za7,
		BGl_z62nodezd2typecz12zd2letzd2var1315za2zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1901z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71916za7,
		BGl_z62nodezd2typecz12zd2setzd2exzd2i1317z70zzreduce_typecz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1902z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71917za7,
		BGl_z62nodezd2typecz12zd2jumpzd2exzd21319z70zzreduce_typecz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1903z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71918za7,
		BGl_z62nodezd2typecz12zd2retblock1321z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1910z00zzreduce_typecz00,
		BgL_bgl_string1910za700za7za7r1919za7,
		"node-typec!1282 done $null? foreign $pair? ", 43);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1904z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71920za7,
		BGl_z62nodezd2typecz12zd2return1323z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1905z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71921za7,
		BGl_z62nodezd2typecz12zd2makezd2box1325za2zzreduce_typecz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1906z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71922za7,
		BGl_z62nodezd2typecz12zd2boxzd2setz121327zb0zzreduce_typecz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1907z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71923za7,
		BGl_z62nodezd2typecz12zd2boxzd2ref1329za2zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1908z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71924za7,
		BGl_z62nodezd2typecz12zd2app1331z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1877z00zzreduce_typecz00,
		BgL_bgl_string1877za700za7za7r1925za7, "      type check             ", 29);
	      DEFINE_STRING(BGl_string1878z00zzreduce_typecz00,
		BgL_bgl_string1878za700za7za7r1926za7, ")\n", 2);
	      DEFINE_STRING(BGl_string1879z00zzreduce_typecz00,
		BgL_bgl_string1879za700za7za7r1927za7, ", remaining: ", 13);
	      DEFINE_STRING(BGl_string1880z00zzreduce_typecz00,
		BgL_bgl_string1880za700za7za7r1928za7, "(removed: ", 10);
	      DEFINE_STRING(BGl_string1882z00zzreduce_typecz00,
		BgL_bgl_string1882za700za7za7r1929za7, "node-typec!1282", 15);
	      DEFINE_STRING(BGl_string1883z00zzreduce_typecz00,
		BgL_bgl_string1883za700za7za7r1930za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1885z00zzreduce_typecz00,
		BgL_bgl_string1885za700za7za7r1931za7, "node-typec!", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1881z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71932za7,
		BGl_z62nodezd2typecz121282za2zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1884z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71933za7,
		BGl_z62nodezd2typecz12zd2atom1285z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1886z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71934za7,
		BGl_z62nodezd2typecz12zd2kwote1287z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1887z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71935za7,
		BGl_z62nodezd2typecz12zd2var1289z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1888z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71936za7,
		BGl_z62nodezd2typecz12zd2closure1291z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1889z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71937za7,
		BGl_z62nodezd2typecz12zd2sequence1293z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1890z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71938za7,
		BGl_z62nodezd2typecz12zd2sync1295z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1891z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71939za7,
		BGl_z62nodezd2typecz12zd2appzd2ly1297za2zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1892z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71940za7,
		BGl_z62nodezd2typecz12zd2funcall1299z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1893z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71941za7,
		BGl_z62nodezd2typecz12zd2extern1301z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1894z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71942za7,
		BGl_z62nodezd2typecz12zd2cast1303z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1895z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71943za7,
		BGl_z62nodezd2typecz12zd2setq1305z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1896z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71944za7,
		BGl_z62nodezd2typecz12zd2conditio1307z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1897z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71945za7,
		BGl_z62nodezd2typecz12zd2fail1309z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1898z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71946za7,
		BGl_z62nodezd2typecz12zd2switch1311z70zzreduce_typecz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1899z00zzreduce_typecz00,
		BgL_bgl_za762nodeza7d2typecza71947za7,
		BGl_z62nodezd2typecz12zd2letzd2fun1313za2zzreduce_typecz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzreduce_typecz00));
		     ADD_ROOT((void *) (&BGl_za2pairzf3za2zf3zzreduce_typecz00));
		     ADD_ROOT((void *) (&BGl_za2nullzf3za2zf3zzreduce_typecz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzreduce_typecz00(long
		BgL_checksumz00_2694, char *BgL_fromz00_2695)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzreduce_typecz00))
				{
					BGl_requirezd2initializa7ationz75zzreduce_typecz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzreduce_typecz00();
					BGl_libraryzd2moduleszd2initz00zzreduce_typecz00();
					BGl_cnstzd2initzd2zzreduce_typecz00();
					BGl_importedzd2moduleszd2initz00zzreduce_typecz00();
					BGl_genericzd2initzd2zzreduce_typecz00();
					BGl_methodzd2initzd2zzreduce_typecz00();
					return BGl_toplevelzd2initzd2zzreduce_typecz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzreduce_typecz00(void)
	{
		{	/* Reduce/typec.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "reduce_typec");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "reduce_typec");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"reduce_typec");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "reduce_typec");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "reduce_typec");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "reduce_typec");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"reduce_typec");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "reduce_typec");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"reduce_typec");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"reduce_typec");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzreduce_typecz00(void)
	{
		{	/* Reduce/typec.scm 15 */
			{	/* Reduce/typec.scm 15 */
				obj_t BgL_cportz00_2534;

				{	/* Reduce/typec.scm 15 */
					obj_t BgL_stringz00_2541;

					BgL_stringz00_2541 = BGl_string1910z00zzreduce_typecz00;
					{	/* Reduce/typec.scm 15 */
						obj_t BgL_startz00_2542;

						BgL_startz00_2542 = BINT(0L);
						{	/* Reduce/typec.scm 15 */
							obj_t BgL_endz00_2543;

							BgL_endz00_2543 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2541)));
							{	/* Reduce/typec.scm 15 */

								BgL_cportz00_2534 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2541, BgL_startz00_2542, BgL_endz00_2543);
				}}}}
				{
					long BgL_iz00_2535;

					BgL_iz00_2535 = 4L;
				BgL_loopz00_2536:
					if ((BgL_iz00_2535 == -1L))
						{	/* Reduce/typec.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Reduce/typec.scm 15 */
							{	/* Reduce/typec.scm 15 */
								obj_t BgL_arg1911z00_2537;

								{	/* Reduce/typec.scm 15 */

									{	/* Reduce/typec.scm 15 */
										obj_t BgL_locationz00_2539;

										BgL_locationz00_2539 = BBOOL(((bool_t) 0));
										{	/* Reduce/typec.scm 15 */

											BgL_arg1911z00_2537 =
												BGl_readz00zz__readerz00(BgL_cportz00_2534,
												BgL_locationz00_2539);
										}
									}
								}
								{	/* Reduce/typec.scm 15 */
									int BgL_tmpz00_2725;

									BgL_tmpz00_2725 = (int) (BgL_iz00_2535);
									CNST_TABLE_SET(BgL_tmpz00_2725, BgL_arg1911z00_2537);
							}}
							{	/* Reduce/typec.scm 15 */
								int BgL_auxz00_2540;

								BgL_auxz00_2540 = (int) ((BgL_iz00_2535 - 1L));
								{
									long BgL_iz00_2730;

									BgL_iz00_2730 = (long) (BgL_auxz00_2540);
									BgL_iz00_2535 = BgL_iz00_2730;
									goto BgL_loopz00_2536;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzreduce_typecz00(void)
	{
		{	/* Reduce/typec.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzreduce_typecz00(void)
	{
		{	/* Reduce/typec.scm 15 */
			BGl_za2pairzf3za2zf3zzreduce_typecz00 = BFALSE;
			BGl_za2nullzf3za2zf3zzreduce_typecz00 = BFALSE;
			BGl_za2typezd2checkszd2remainingza2z00zzreduce_typecz00 = 0L;
			BGl_za2typezd2checkszd2removedza2z00zzreduce_typecz00 = 0L;
			return BUNSPEC;
		}

	}



/* reduce-type-check! */
	BGL_EXPORTED_DEF obj_t BGl_reducezd2typezd2checkz12z12zzreduce_typecz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Reduce/typec.scm 38 */
			{	/* Reduce/typec.scm 39 */
				obj_t BgL_list1336z00_1606;

				BgL_list1336z00_1606 =
					MAKE_YOUNG_PAIR(BGl_string1877z00zzreduce_typecz00, BNIL);
				BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1336z00_1606);
			}
			BGl_za2pairzf3za2zf3zzreduce_typecz00 =
				BGl_findzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(0),
				CNST_TABLE_REF(1));
			BGl_za2nullzf3za2zf3zzreduce_typecz00 =
				BGl_findzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(2),
				CNST_TABLE_REF(1));
			{
				obj_t BgL_l1271z00_1608;

				BgL_l1271z00_1608 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31337ze3z87_1609:
				if (PAIRP(BgL_l1271z00_1608))
					{	/* Reduce/typec.scm 42 */
						{	/* Reduce/typec.scm 43 */
							obj_t BgL_globalz00_1611;

							BgL_globalz00_1611 = CAR(BgL_l1271z00_1608);
							{	/* Reduce/typec.scm 43 */
								BgL_valuez00_bglt BgL_funz00_1612;

								BgL_funz00_1612 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_1611))))->
									BgL_valuez00);
								{	/* Reduce/typec.scm 43 */
									obj_t BgL_nodez00_1613;

									BgL_nodez00_1613 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_funz00_1612)))->BgL_bodyz00);
									{	/* Reduce/typec.scm 44 */

										{	/* Reduce/typec.scm 45 */
											BgL_nodez00_bglt BgL_arg1339z00_1614;

											BgL_arg1339z00_1614 =
												BGl_nodezd2typecz12zc0zzreduce_typecz00(
												((BgL_nodez00_bglt) BgL_nodez00_1613));
											((((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_funz00_1612)))->
													BgL_bodyz00) =
												((obj_t) ((obj_t) BgL_arg1339z00_1614)), BUNSPEC);
										}
										BUNSPEC;
									}
								}
							}
						}
						{
							obj_t BgL_l1271z00_2755;

							BgL_l1271z00_2755 = CDR(BgL_l1271z00_1608);
							BgL_l1271z00_1608 = BgL_l1271z00_2755;
							goto BgL_zc3z04anonymousza31337ze3z87_1609;
						}
					}
				else
					{	/* Reduce/typec.scm 42 */
						((bool_t) 1);
					}
			}
			{	/* Reduce/typec.scm 48 */
				obj_t BgL_list1341z00_1617;

				{	/* Reduce/typec.scm 48 */
					obj_t BgL_arg1342z00_1618;

					{	/* Reduce/typec.scm 48 */
						obj_t BgL_arg1343z00_1619;

						{	/* Reduce/typec.scm 48 */
							obj_t BgL_arg1346z00_1620;

							{	/* Reduce/typec.scm 48 */
								obj_t BgL_arg1348z00_1621;

								BgL_arg1348z00_1621 =
									MAKE_YOUNG_PAIR(BGl_string1878z00zzreduce_typecz00, BNIL);
								BgL_arg1346z00_1620 =
									MAKE_YOUNG_PAIR(BINT
									(BGl_za2typezd2checkszd2remainingza2z00zzreduce_typecz00),
									BgL_arg1348z00_1621);
							}
							BgL_arg1343z00_1619 =
								MAKE_YOUNG_PAIR(BGl_string1879z00zzreduce_typecz00,
								BgL_arg1346z00_1620);
						}
						BgL_arg1342z00_1618 =
							MAKE_YOUNG_PAIR(BINT
							(BGl_za2typezd2checkszd2removedza2z00zzreduce_typecz00),
							BgL_arg1343z00_1619);
					}
					BgL_list1341z00_1617 =
						MAKE_YOUNG_PAIR(BGl_string1880z00zzreduce_typecz00,
						BgL_arg1342z00_1618);
				}
				BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1341z00_1617);
			}
			BGl_za2pairzf3za2zf3zzreduce_typecz00 = BFALSE;
			BGl_za2nullzf3za2zf3zzreduce_typecz00 = BFALSE;
			return BgL_globalsz00_3;
		}

	}



/* &reduce-type-check! */
	obj_t BGl_z62reducezd2typezd2checkz12z70zzreduce_typecz00(obj_t
		BgL_envz00_2455, obj_t BgL_globalsz00_2456)
	{
		{	/* Reduce/typec.scm 38 */
			return
				BGl_reducezd2typezd2checkz12z12zzreduce_typecz00(BgL_globalsz00_2456);
		}

	}



/* node-typec*! */
	obj_t BGl_nodezd2typecza2z12z62zzreduce_typecz00(obj_t BgL_nodeza2za2_28)
	{
		{	/* Reduce/typec.scm 287 */
			{
				obj_t BgL_nodeza2za2_1623;

				BgL_nodeza2za2_1623 = BgL_nodeza2za2_28;
			BgL_zc3z04anonymousza31349ze3z87_1624:
				if (NULLP(BgL_nodeza2za2_1623))
					{	/* Reduce/typec.scm 289 */
						return CNST_TABLE_REF(3);
					}
				else
					{	/* Reduce/typec.scm 289 */
						{	/* Reduce/typec.scm 292 */
							BgL_nodez00_bglt BgL_arg1351z00_1626;

							{	/* Reduce/typec.scm 292 */
								obj_t BgL_arg1352z00_1627;

								BgL_arg1352z00_1627 = CAR(((obj_t) BgL_nodeza2za2_1623));
								BgL_arg1351z00_1626 =
									BGl_nodezd2typecz12zc0zzreduce_typecz00(
									((BgL_nodez00_bglt) BgL_arg1352z00_1627));
							}
							{	/* Reduce/typec.scm 292 */
								obj_t BgL_auxz00_2776;
								obj_t BgL_tmpz00_2774;

								BgL_auxz00_2776 = ((obj_t) BgL_arg1351z00_1626);
								BgL_tmpz00_2774 = ((obj_t) BgL_nodeza2za2_1623);
								SET_CAR(BgL_tmpz00_2774, BgL_auxz00_2776);
							}
						}
						{	/* Reduce/typec.scm 293 */
							obj_t BgL_arg1361z00_1628;

							BgL_arg1361z00_1628 = CDR(((obj_t) BgL_nodeza2za2_1623));
							{
								obj_t BgL_nodeza2za2_2781;

								BgL_nodeza2za2_2781 = BgL_arg1361z00_1628;
								BgL_nodeza2za2_1623 = BgL_nodeza2za2_2781;
								goto BgL_zc3z04anonymousza31349ze3z87_1624;
							}
						}
					}
			}
		}

	}



/* pair-of-pair-nil? */
	bool_t BGl_pairzd2ofzd2pairzd2nilzf3z21zzreduce_typecz00(BgL_nodez00_bglt
		BgL_nodez00_30)
	{
		{	/* Reduce/typec.scm 380 */
			{	/* Reduce/typec.scm 381 */
				bool_t BgL_test1952z00_2782;

				{	/* Reduce/typec.scm 381 */
					obj_t BgL_classz00_2106;

					BgL_classz00_2106 = BGl_appz00zzast_nodez00;
					{	/* Reduce/typec.scm 381 */
						BgL_objectz00_bglt BgL_arg1807z00_2108;

						{	/* Reduce/typec.scm 381 */
							obj_t BgL_tmpz00_2783;

							BgL_tmpz00_2783 = ((obj_t) ((BgL_objectz00_bglt) BgL_nodez00_30));
							BgL_arg1807z00_2108 = (BgL_objectz00_bglt) (BgL_tmpz00_2783);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Reduce/typec.scm 381 */
								long BgL_idxz00_2114;

								BgL_idxz00_2114 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2108);
								BgL_test1952z00_2782 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2114 + 3L)) == BgL_classz00_2106);
							}
						else
							{	/* Reduce/typec.scm 381 */
								bool_t BgL_res1864z00_2139;

								{	/* Reduce/typec.scm 381 */
									obj_t BgL_oclassz00_2122;

									{	/* Reduce/typec.scm 381 */
										obj_t BgL_arg1815z00_2130;
										long BgL_arg1816z00_2131;

										BgL_arg1815z00_2130 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Reduce/typec.scm 381 */
											long BgL_arg1817z00_2132;

											BgL_arg1817z00_2132 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2108);
											BgL_arg1816z00_2131 = (BgL_arg1817z00_2132 - OBJECT_TYPE);
										}
										BgL_oclassz00_2122 =
											VECTOR_REF(BgL_arg1815z00_2130, BgL_arg1816z00_2131);
									}
									{	/* Reduce/typec.scm 381 */
										bool_t BgL__ortest_1115z00_2123;

										BgL__ortest_1115z00_2123 =
											(BgL_classz00_2106 == BgL_oclassz00_2122);
										if (BgL__ortest_1115z00_2123)
											{	/* Reduce/typec.scm 381 */
												BgL_res1864z00_2139 = BgL__ortest_1115z00_2123;
											}
										else
											{	/* Reduce/typec.scm 381 */
												long BgL_odepthz00_2124;

												{	/* Reduce/typec.scm 381 */
													obj_t BgL_arg1804z00_2125;

													BgL_arg1804z00_2125 = (BgL_oclassz00_2122);
													BgL_odepthz00_2124 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2125);
												}
												if ((3L < BgL_odepthz00_2124))
													{	/* Reduce/typec.scm 381 */
														obj_t BgL_arg1802z00_2127;

														{	/* Reduce/typec.scm 381 */
															obj_t BgL_arg1803z00_2128;

															BgL_arg1803z00_2128 = (BgL_oclassz00_2122);
															BgL_arg1802z00_2127 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2128,
																3L);
														}
														BgL_res1864z00_2139 =
															(BgL_arg1802z00_2127 == BgL_classz00_2106);
													}
												else
													{	/* Reduce/typec.scm 381 */
														BgL_res1864z00_2139 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test1952z00_2782 = BgL_res1864z00_2139;
							}
					}
				}
				if (BgL_test1952z00_2782)
					{	/* Reduce/typec.scm 383 */
						bool_t BgL_test1956z00_2806;

						{	/* Reduce/typec.scm 383 */
							BgL_variablez00_bglt BgL_arg1377z00_1641;

							BgL_arg1377z00_1641 =
								(((BgL_varz00_bglt) COBJECT(
										(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt) BgL_nodez00_30)))->BgL_funz00)))->
								BgL_variablez00);
							BgL_test1956z00_2806 =
								(((obj_t) BgL_arg1377z00_1641) ==
								BGl_za2pairzf3za2zf3zzreduce_typecz00);
						}
						if (BgL_test1956z00_2806)
							{	/* Reduce/typec.scm 384 */
								bool_t BgL_test1957z00_2812;

								{	/* Reduce/typec.scm 384 */
									obj_t BgL_tmpz00_2813;

									BgL_tmpz00_2813 =
										(((BgL_appz00_bglt) COBJECT(
												((BgL_appz00_bglt) BgL_nodez00_30)))->BgL_argsz00);
									BgL_test1957z00_2812 = PAIRP(BgL_tmpz00_2813);
								}
								if (BgL_test1957z00_2812)
									{	/* Reduce/typec.scm 385 */
										bool_t BgL_test1958z00_2817;

										{	/* Reduce/typec.scm 385 */
											obj_t BgL_tmpz00_2818;

											{	/* Reduce/typec.scm 385 */
												obj_t BgL_pairz00_2141;

												BgL_pairz00_2141 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_30)))->
													BgL_argsz00);
												BgL_tmpz00_2818 = CDR(BgL_pairz00_2141);
											}
											BgL_test1958z00_2817 = NULLP(BgL_tmpz00_2818);
										}
										if (BgL_test1958z00_2817)
											{	/* Reduce/typec.scm 386 */
												BgL_typez00_bglt BgL_arg1364z00_1635;

												{	/* Reduce/typec.scm 386 */
													obj_t BgL_arg1367z00_1636;

													{	/* Reduce/typec.scm 386 */
														obj_t BgL_pairz00_2142;

														BgL_pairz00_2142 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_30)))->
															BgL_argsz00);
														BgL_arg1367z00_1636 = CAR(BgL_pairz00_2142);
													}
													BgL_arg1364z00_1635 =
														BGl_getzd2typezd2zztype_typeofz00(
														((BgL_nodez00_bglt) BgL_arg1367z00_1636),
														((bool_t) 0));
												}
												return
													(
													((obj_t) BgL_arg1364z00_1635) ==
													BGl_za2pairzd2nilza2zd2zztype_cachez00);
											}
										else
											{	/* Reduce/typec.scm 385 */
												return ((bool_t) 0);
											}
									}
								else
									{	/* Reduce/typec.scm 384 */
										return ((bool_t) 0);
									}
							}
						else
							{	/* Reduce/typec.scm 383 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* Reduce/typec.scm 381 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzreduce_typecz00(void)
	{
		{	/* Reduce/typec.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzreduce_typecz00(void)
	{
		{	/* Reduce/typec.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_proc1881z00zzreduce_typecz00, BGl_nodez00zzast_nodez00,
				BGl_string1882z00zzreduce_typecz00);
		}

	}



/* &node-typec!1282 */
	obj_t BGl_z62nodezd2typecz121282za2zzreduce_typecz00(obj_t BgL_envz00_2458,
		obj_t BgL_nodez00_2459)
	{
		{	/* Reduce/typec.scm 69 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(4),
				BGl_string1883z00zzreduce_typecz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2459)));
		}

	}



/* node-typec! */
	BgL_nodez00_bglt BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_nodez00_bglt
		BgL_nodez00_4)
	{
		{	/* Reduce/typec.scm 69 */
			{	/* Reduce/typec.scm 69 */
				obj_t BgL_method1283z00_1649;

				{	/* Reduce/typec.scm 69 */
					obj_t BgL_res1869z00_2175;

					{	/* Reduce/typec.scm 69 */
						long BgL_objzd2classzd2numz00_2146;

						BgL_objzd2classzd2numz00_2146 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_4));
						{	/* Reduce/typec.scm 69 */
							obj_t BgL_arg1811z00_2147;

							BgL_arg1811z00_2147 =
								PROCEDURE_REF(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
								(int) (1L));
							{	/* Reduce/typec.scm 69 */
								int BgL_offsetz00_2150;

								BgL_offsetz00_2150 = (int) (BgL_objzd2classzd2numz00_2146);
								{	/* Reduce/typec.scm 69 */
									long BgL_offsetz00_2151;

									BgL_offsetz00_2151 =
										((long) (BgL_offsetz00_2150) - OBJECT_TYPE);
									{	/* Reduce/typec.scm 69 */
										long BgL_modz00_2152;

										BgL_modz00_2152 =
											(BgL_offsetz00_2151 >> (int) ((long) ((int) (4L))));
										{	/* Reduce/typec.scm 69 */
											long BgL_restz00_2154;

											BgL_restz00_2154 =
												(BgL_offsetz00_2151 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Reduce/typec.scm 69 */

												{	/* Reduce/typec.scm 69 */
													obj_t BgL_bucketz00_2156;

													BgL_bucketz00_2156 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2147), BgL_modz00_2152);
													BgL_res1869z00_2175 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2156), BgL_restz00_2154);
					}}}}}}}}
					BgL_method1283z00_1649 = BgL_res1869z00_2175;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1283z00_1649, ((obj_t) BgL_nodez00_4)));
			}
		}

	}



/* &node-typec! */
	BgL_nodez00_bglt BGl_z62nodezd2typecz12za2zzreduce_typecz00(obj_t
		BgL_envz00_2460, obj_t BgL_nodez00_2461)
	{
		{	/* Reduce/typec.scm 69 */
			return
				BGl_nodezd2typecz12zc0zzreduce_typecz00(
				((BgL_nodez00_bglt) BgL_nodez00_2461));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzreduce_typecz00(void)
	{
		{	/* Reduce/typec.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_atomz00zzast_nodez00, BGl_proc1884z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_kwotez00zzast_nodez00, BGl_proc1886z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00, BGl_varz00zzast_nodez00,
				BGl_proc1887z00zzreduce_typecz00, BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_closurez00zzast_nodez00, BGl_proc1888z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1889z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_syncz00zzast_nodez00, BGl_proc1890z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1891z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_funcallz00zzast_nodez00, BGl_proc1892z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_externz00zzast_nodez00, BGl_proc1893z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_castz00zzast_nodez00, BGl_proc1894z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_setqz00zzast_nodez00, BGl_proc1895z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1896z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_failz00zzast_nodez00, BGl_proc1897z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_switchz00zzast_nodez00, BGl_proc1898z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1899z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1900z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1901z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1902z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_retblockz00zzast_nodez00, BGl_proc1903z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_returnz00zzast_nodez00, BGl_proc1904z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1905z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1906z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1907z00zzreduce_typecz00,
				BGl_string1885z00zzreduce_typecz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2typecz12zd2envz12zzreduce_typecz00, BGl_appz00zzast_nodez00,
				BGl_proc1908z00zzreduce_typecz00, BGl_string1885z00zzreduce_typecz00);
		}

	}



/* &node-typec!-app1331 */
	BgL_nodez00_bglt BGl_z62nodezd2typecz12zd2app1331z70zzreduce_typecz00(obj_t
		BgL_envz00_2486, obj_t BgL_nodez00_2487)
	{
		{	/* Reduce/typec.scm 303 */
			{
				obj_t BgL_typecz00_2563;
				BgL_typez00_bglt BgL_typeaz00_2564;
				BgL_appz00_bglt BgL_nodez00_2549;
				obj_t BgL_typecz00_2550;
				BgL_typez00_bglt BgL_typeaz00_2551;

				BGl_nodezd2typecza2z12z62zzreduce_typecz00(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2487)))->BgL_argsz00));
				{	/* Reduce/typec.scm 362 */
					BgL_variablez00_bglt BgL_varz00_2581;

					BgL_varz00_2581 =
						(((BgL_varz00_bglt) COBJECT(
								(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_2487)))->BgL_funz00)))->
						BgL_variablez00);
					{	/* Reduce/typec.scm 362 */
						obj_t BgL_typecz00_2582;

						BgL_typecz00_2582 =
							(((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt)
										(((BgL_variablez00_bglt) COBJECT(BgL_varz00_2581))->
											BgL_valuez00))))->BgL_predicatezd2ofzd2);
						{	/* Reduce/typec.scm 363 */

							{	/* Reduce/typec.scm 365 */
								bool_t BgL_test1959z00_2901;

								{	/* Reduce/typec.scm 365 */
									bool_t BgL_test1960z00_2902;

									{	/* Reduce/typec.scm 365 */
										obj_t BgL_tmpz00_2903;

										BgL_tmpz00_2903 =
											(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt) BgL_nodez00_2487)))->BgL_argsz00);
										BgL_test1960z00_2902 = PAIRP(BgL_tmpz00_2903);
									}
									if (BgL_test1960z00_2902)
										{	/* Reduce/typec.scm 365 */
											if (NULLP(CDR(
														(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_2487)))->
															BgL_argsz00))))
												{	/* Reduce/typec.scm 367 */
													bool_t BgL_test1962z00_2912;

													{	/* Reduce/typec.scm 367 */
														obj_t BgL_classz00_2583;

														BgL_classz00_2583 = BGl_typez00zztype_typez00;
														if (BGL_OBJECTP(BgL_typecz00_2582))
															{	/* Reduce/typec.scm 367 */
																BgL_objectz00_bglt BgL_arg1807z00_2584;

																BgL_arg1807z00_2584 =
																	(BgL_objectz00_bglt) (BgL_typecz00_2582);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Reduce/typec.scm 367 */
																		long BgL_idxz00_2585;

																		BgL_idxz00_2585 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2584);
																		BgL_test1962z00_2912 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2585 + 1L)) ==
																			BgL_classz00_2583);
																	}
																else
																	{	/* Reduce/typec.scm 367 */
																		bool_t BgL_res1871z00_2588;

																		{	/* Reduce/typec.scm 367 */
																			obj_t BgL_oclassz00_2589;

																			{	/* Reduce/typec.scm 367 */
																				obj_t BgL_arg1815z00_2590;
																				long BgL_arg1816z00_2591;

																				BgL_arg1815z00_2590 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Reduce/typec.scm 367 */
																					long BgL_arg1817z00_2592;

																					BgL_arg1817z00_2592 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2584);
																					BgL_arg1816z00_2591 =
																						(BgL_arg1817z00_2592 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2589 =
																					VECTOR_REF(BgL_arg1815z00_2590,
																					BgL_arg1816z00_2591);
																			}
																			{	/* Reduce/typec.scm 367 */
																				bool_t BgL__ortest_1115z00_2593;

																				BgL__ortest_1115z00_2593 =
																					(BgL_classz00_2583 ==
																					BgL_oclassz00_2589);
																				if (BgL__ortest_1115z00_2593)
																					{	/* Reduce/typec.scm 367 */
																						BgL_res1871z00_2588 =
																							BgL__ortest_1115z00_2593;
																					}
																				else
																					{	/* Reduce/typec.scm 367 */
																						long BgL_odepthz00_2594;

																						{	/* Reduce/typec.scm 367 */
																							obj_t BgL_arg1804z00_2595;

																							BgL_arg1804z00_2595 =
																								(BgL_oclassz00_2589);
																							BgL_odepthz00_2594 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2595);
																						}
																						if ((1L < BgL_odepthz00_2594))
																							{	/* Reduce/typec.scm 367 */
																								obj_t BgL_arg1802z00_2596;

																								{	/* Reduce/typec.scm 367 */
																									obj_t BgL_arg1803z00_2597;

																									BgL_arg1803z00_2597 =
																										(BgL_oclassz00_2589);
																									BgL_arg1802z00_2596 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2597, 1L);
																								}
																								BgL_res1871z00_2588 =
																									(BgL_arg1802z00_2596 ==
																									BgL_classz00_2583);
																							}
																						else
																							{	/* Reduce/typec.scm 367 */
																								BgL_res1871z00_2588 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test1962z00_2912 = BgL_res1871z00_2588;
																	}
															}
														else
															{	/* Reduce/typec.scm 367 */
																BgL_test1962z00_2912 = ((bool_t) 0);
															}
													}
													if (BgL_test1962z00_2912)
														{	/* Reduce/typec.scm 368 */
															bool_t BgL__ortest_1136z00_2598;

															{	/* Reduce/typec.scm 368 */
																bool_t BgL_test1967z00_2935;

																{	/* Reduce/typec.scm 368 */
																	obj_t BgL_arg1702z00_2599;

																	BgL_arg1702z00_2599 =
																		CAR(
																		(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						BgL_nodez00_2487)))->BgL_argsz00));
																	BgL_test1967z00_2935 =
																		BGl_sidezd2effectzf3z21zzeffect_effectz00((
																			(BgL_nodez00_bglt) BgL_arg1702z00_2599));
																}
																if (BgL_test1967z00_2935)
																	{	/* Reduce/typec.scm 368 */
																		BgL__ortest_1136z00_2598 = ((bool_t) 0);
																	}
																else
																	{	/* Reduce/typec.scm 368 */
																		BgL__ortest_1136z00_2598 = ((bool_t) 1);
																	}
															}
															if (BgL__ortest_1136z00_2598)
																{	/* Reduce/typec.scm 368 */
																	BgL_test1959z00_2901 =
																		BgL__ortest_1136z00_2598;
																}
															else
																{	/* Reduce/typec.scm 368 */
																	obj_t BgL_arg1700z00_2600;

																	BgL_arg1700z00_2600 =
																		CAR(
																		(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						BgL_nodez00_2487)))->BgL_argsz00));
																	{	/* Reduce/typec.scm 368 */
																		obj_t BgL_classz00_2601;

																		BgL_classz00_2601 = BGl_varz00zzast_nodez00;
																		if (BGL_OBJECTP(BgL_arg1700z00_2600))
																			{	/* Reduce/typec.scm 368 */
																				BgL_objectz00_bglt BgL_arg1807z00_2602;

																				BgL_arg1807z00_2602 =
																					(BgL_objectz00_bglt)
																					(BgL_arg1700z00_2600);
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Reduce/typec.scm 368 */
																						long BgL_idxz00_2603;

																						BgL_idxz00_2603 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_2602);
																						BgL_test1959z00_2901 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_2603 + 2L)) ==
																							BgL_classz00_2601);
																					}
																				else
																					{	/* Reduce/typec.scm 368 */
																						bool_t BgL_res1872z00_2606;

																						{	/* Reduce/typec.scm 368 */
																							obj_t BgL_oclassz00_2607;

																							{	/* Reduce/typec.scm 368 */
																								obj_t BgL_arg1815z00_2608;
																								long BgL_arg1816z00_2609;

																								BgL_arg1815z00_2608 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Reduce/typec.scm 368 */
																									long BgL_arg1817z00_2610;

																									BgL_arg1817z00_2610 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_2602);
																									BgL_arg1816z00_2609 =
																										(BgL_arg1817z00_2610 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_2607 =
																									VECTOR_REF
																									(BgL_arg1815z00_2608,
																									BgL_arg1816z00_2609);
																							}
																							{	/* Reduce/typec.scm 368 */
																								bool_t BgL__ortest_1115z00_2611;

																								BgL__ortest_1115z00_2611 =
																									(BgL_classz00_2601 ==
																									BgL_oclassz00_2607);
																								if (BgL__ortest_1115z00_2611)
																									{	/* Reduce/typec.scm 368 */
																										BgL_res1872z00_2606 =
																											BgL__ortest_1115z00_2611;
																									}
																								else
																									{	/* Reduce/typec.scm 368 */
																										long BgL_odepthz00_2612;

																										{	/* Reduce/typec.scm 368 */
																											obj_t BgL_arg1804z00_2613;

																											BgL_arg1804z00_2613 =
																												(BgL_oclassz00_2607);
																											BgL_odepthz00_2612 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_2613);
																										}
																										if (
																											(2L < BgL_odepthz00_2612))
																											{	/* Reduce/typec.scm 368 */
																												obj_t
																													BgL_arg1802z00_2614;
																												{	/* Reduce/typec.scm 368 */
																													obj_t
																														BgL_arg1803z00_2615;
																													BgL_arg1803z00_2615 =
																														(BgL_oclassz00_2607);
																													BgL_arg1802z00_2614 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_2615,
																														2L);
																												}
																												BgL_res1872z00_2606 =
																													(BgL_arg1802z00_2614
																													== BgL_classz00_2601);
																											}
																										else
																											{	/* Reduce/typec.scm 368 */
																												BgL_res1872z00_2606 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test1959z00_2901 =
																							BgL_res1872z00_2606;
																					}
																			}
																		else
																			{	/* Reduce/typec.scm 368 */
																				BgL_test1959z00_2901 = ((bool_t) 0);
																			}
																	}
																}
														}
													else
														{	/* Reduce/typec.scm 367 */
															BgL_test1959z00_2901 = ((bool_t) 0);
														}
												}
											else
												{	/* Reduce/typec.scm 366 */
													BgL_test1959z00_2901 = ((bool_t) 0);
												}
										}
									else
										{	/* Reduce/typec.scm 365 */
											BgL_test1959z00_2901 = ((bool_t) 0);
										}
								}
								if (BgL_test1959z00_2901)
									{	/* Reduce/typec.scm 369 */
										BgL_typez00_bglt BgL_arg1681z00_2616;

										{	/* Reduce/typec.scm 369 */
											obj_t BgL_arg1688z00_2617;

											BgL_arg1688z00_2617 =
												CAR(
												(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_2487)))->
													BgL_argsz00));
											BgL_arg1681z00_2616 =
												BGl_getzd2typezd2zztype_typeofz00(((BgL_nodez00_bglt)
													BgL_arg1688z00_2617), ((bool_t) 0));
										}
										BgL_nodez00_2549 = ((BgL_appz00_bglt) BgL_nodez00_2487);
										BgL_typecz00_2550 = BgL_typecz00_2582;
										BgL_typeaz00_2551 = BgL_arg1681z00_2616;
									BgL_checkzd2typezd2_2547:
										{	/* Reduce/typec.scm 327 */
											BgL_typez00_bglt BgL_typez00_2552;

											BgL_typez00_2552 =
												BGl_getzd2typezd2zztype_typeofz00(
												((BgL_nodez00_bglt) BgL_nodez00_2549), ((bool_t) 0));
											if (BGl_typezd2lesszd2specificzf3zf3zztype_miscz00(
													((BgL_typez00_bglt) BgL_typecz00_2550),
													BgL_typeaz00_2551))
												{	/* Reduce/typec.scm 329 */
													BGl_za2typezd2checkszd2removedza2z00zzreduce_typecz00
														=
														(1L +
														BGl_za2typezd2checkszd2removedza2z00zzreduce_typecz00);
													{	/* Reduce/typec.scm 333 */
														BgL_nodez00_bglt BgL_nodez00_2553;

														{	/* Reduce/typec.scm 333 */
															BgL_literalz00_bglt BgL_arg1733z00_2554;

															{	/* Reduce/typec.scm 333 */
																BgL_literalz00_bglt BgL_new1131z00_2555;

																{	/* Reduce/typec.scm 334 */
																	BgL_literalz00_bglt BgL_new1130z00_2556;

																	BgL_new1130z00_2556 =
																		((BgL_literalz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_literalz00_bgl))));
																	{	/* Reduce/typec.scm 334 */
																		long BgL_arg1734z00_2557;

																		BgL_arg1734z00_2557 =
																			BGL_CLASS_NUM
																			(BGl_literalz00zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt)
																				BgL_new1130z00_2556),
																			BgL_arg1734z00_2557);
																	}
																	{	/* Reduce/typec.scm 334 */
																		BgL_objectz00_bglt BgL_tmpz00_2982;

																		BgL_tmpz00_2982 =
																			((BgL_objectz00_bglt)
																			BgL_new1130z00_2556);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2982,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1130z00_2556);
																	BgL_new1131z00_2555 = BgL_new1130z00_2556;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1131z00_2555)))->BgL_locz00) =
																	((obj_t) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt)
																						BgL_nodez00_2549)))->BgL_locz00)),
																	BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1131z00_2555)))->BgL_typez00) =
																	((BgL_typez00_bglt) BgL_typez00_2552),
																	BUNSPEC);
																((((BgL_atomz00_bglt)
																			COBJECT(((BgL_atomz00_bglt)
																					BgL_new1131z00_2555)))->
																		BgL_valuez00) = ((obj_t) BTRUE), BUNSPEC);
																BgL_arg1733z00_2554 = BgL_new1131z00_2555;
															}
															BgL_nodez00_2553 =
																BGl_coercez12z12zzcoerce_coercez00(
																((BgL_nodez00_bglt) BgL_arg1733z00_2554),
																BUNSPEC, BgL_typez00_2552, ((bool_t) 0));
														}
														return BgL_nodez00_2553;
													}
												}
											else
												{	/* Reduce/typec.scm 341 */
													bool_t BgL_test1974z00_2996;

													BgL_typecz00_2563 = BgL_typecz00_2550;
													BgL_typeaz00_2564 = BgL_typeaz00_2551;
													if (
														(BgL_typecz00_2563 == ((obj_t) BgL_typeaz00_2564)))
														{	/* Reduce/typec.scm 307 */
															BgL_test1974z00_2996 = ((bool_t) 0);
														}
													else
														{	/* Reduce/typec.scm 307 */
															if (
																(((obj_t) BgL_typeaz00_2564) ==
																	BGl_za2_za2z00zztype_cachez00))
																{	/* Reduce/typec.scm 309 */
																	BgL_test1974z00_2996 = ((bool_t) 0);
																}
															else
																{	/* Reduce/typec.scm 311 */
																	bool_t BgL_test1977z00_3003;

																	if (BGl_bigloozd2typezf3z21zztype_typez00(
																			((BgL_typez00_bglt) BgL_typecz00_2563)))
																		{	/* Reduce/typec.scm 311 */
																			if (BGl_bigloozd2typezf3z21zztype_typez00
																				(BgL_typeaz00_2564))
																				{	/* Reduce/typec.scm 311 */
																					BgL_test1977z00_3003 = ((bool_t) 0);
																				}
																			else
																				{	/* Reduce/typec.scm 311 */
																					BgL_test1977z00_3003 = ((bool_t) 1);
																				}
																		}
																	else
																		{	/* Reduce/typec.scm 311 */
																			BgL_test1977z00_3003 = ((bool_t) 0);
																		}
																	if (BgL_test1977z00_3003)
																		{	/* Reduce/typec.scm 311 */
																			if (CBOOL
																				(BGl_findzd2coercerzd2zztype_coercionz00
																					(BgL_typeaz00_2564,
																						((BgL_typez00_bglt)
																							BgL_typecz00_2563))))
																				{	/* Reduce/typec.scm 312 */
																					BgL_test1974z00_2996 = ((bool_t) 0);
																				}
																			else
																				{	/* Reduce/typec.scm 312 */
																					BgL_test1974z00_2996 = ((bool_t) 1);
																				}
																		}
																	else
																		{	/* Reduce/typec.scm 313 */
																			bool_t BgL_test1981z00_3013;

																			if (BGl_typezd2lesszd2specificzf3zf3zztype_miscz00(((BgL_typez00_bglt) BgL_typecz00_2563), BgL_typeaz00_2564))
																				{	/* Reduce/typec.scm 313 */
																					BgL_test1981z00_3013 = ((bool_t) 1);
																				}
																			else
																				{	/* Reduce/typec.scm 313 */
																					BgL_test1981z00_3013 =
																						BGl_typezd2lesszd2specificzf3zf3zztype_miscz00
																						(BgL_typeaz00_2564,
																						((BgL_typez00_bglt)
																							BgL_typecz00_2563));
																				}
																			if (BgL_test1981z00_3013)
																				{	/* Reduce/typec.scm 313 */
																					BgL_test1974z00_2996 = ((bool_t) 0);
																				}
																			else
																				{	/* Reduce/typec.scm 316 */
																					bool_t BgL_test1983z00_3019;

																					if (CBOOL
																						(BGl_findzd2coercerzd2zztype_coercionz00
																							(((BgL_typez00_bglt)
																									BgL_typecz00_2563),
																								BgL_typeaz00_2564)))
																						{	/* Reduce/typec.scm 316 */
																							BgL_test1983z00_3019 =
																								((bool_t) 1);
																						}
																					else
																						{	/* Reduce/typec.scm 316 */
																							BgL_test1983z00_3019 =
																								CBOOL
																								(BGl_findzd2coercerzd2zztype_coercionz00
																								(BgL_typeaz00_2564,
																									((BgL_typez00_bglt)
																										BgL_typecz00_2563)));
																						}
																					if (BgL_test1983z00_3019)
																						{	/* Reduce/typec.scm 316 */
																							BgL_test1974z00_2996 =
																								((bool_t) 0);
																						}
																					else
																						{	/* Reduce/typec.scm 318 */
																							bool_t BgL_test1985z00_3027;

																							if (BGl_foreignzd2typezf3z21zztype_typez00(((BgL_typez00_bglt) BgL_typecz00_2563)))
																								{	/* Reduce/typec.scm 318 */
																									BgL_test1985z00_3027 =
																										BGl_foreignzd2typezf3z21zztype_typez00
																										(BgL_typeaz00_2564);
																								}
																							else
																								{	/* Reduce/typec.scm 318 */
																									BgL_test1985z00_3027 =
																										((bool_t) 0);
																								}
																							if (BgL_test1985z00_3027)
																								{	/* Reduce/typec.scm 318 */
																									BgL_test1974z00_2996 =
																										((bool_t) 0);
																								}
																							else
																								{	/* Reduce/typec.scm 320 */
																									bool_t BgL_test1987z00_3032;

																									if (
																										(BgL_typecz00_2563 ==
																											BGl_za2foreignza2z00zztype_cachez00))
																										{	/* Reduce/typec.scm 320 */
																											bool_t
																												BgL__ortest_1128z00_2565;
																											{	/* Reduce/typec.scm 320 */
																												obj_t BgL_classz00_2566;

																												BgL_classz00_2566 =
																													BGl_jclassz00zzobject_classz00;
																												{	/* Reduce/typec.scm 320 */
																													BgL_objectz00_bglt
																														BgL_arg1807z00_2567;
																													{	/* Reduce/typec.scm 320 */
																														obj_t
																															BgL_tmpz00_3035;
																														BgL_tmpz00_3035 =
																															((obj_t) (
																																(BgL_objectz00_bglt)
																																BgL_typeaz00_2564));
																														BgL_arg1807z00_2567
																															=
																															(BgL_objectz00_bglt)
																															(BgL_tmpz00_3035);
																													}
																													if (BGL_CONDEXPAND_ISA_ARCH64())
																														{	/* Reduce/typec.scm 320 */
																															long
																																BgL_idxz00_2568;
																															BgL_idxz00_2568 =
																																BGL_OBJECT_INHERITANCE_NUM
																																(BgL_arg1807z00_2567);
																															BgL__ortest_1128z00_2565
																																=
																																(VECTOR_REF
																																(BGl_za2inheritancesza2z00zz__objectz00,
																																	(BgL_idxz00_2568
																																		+ 2L)) ==
																																BgL_classz00_2566);
																														}
																													else
																														{	/* Reduce/typec.scm 320 */
																															bool_t
																																BgL_res1870z00_2571;
																															{	/* Reduce/typec.scm 320 */
																																obj_t
																																	BgL_oclassz00_2572;
																																{	/* Reduce/typec.scm 320 */
																																	obj_t
																																		BgL_arg1815z00_2573;
																																	long
																																		BgL_arg1816z00_2574;
																																	BgL_arg1815z00_2573
																																		=
																																		(BGl_za2classesza2z00zz__objectz00);
																																	{	/* Reduce/typec.scm 320 */
																																		long
																																			BgL_arg1817z00_2575;
																																		BgL_arg1817z00_2575
																																			=
																																			BGL_OBJECT_CLASS_NUM
																																			(BgL_arg1807z00_2567);
																																		BgL_arg1816z00_2574
																																			=
																																			(BgL_arg1817z00_2575
																																			-
																																			OBJECT_TYPE);
																																	}
																																	BgL_oclassz00_2572
																																		=
																																		VECTOR_REF
																																		(BgL_arg1815z00_2573,
																																		BgL_arg1816z00_2574);
																																}
																																{	/* Reduce/typec.scm 320 */
																																	bool_t
																																		BgL__ortest_1115z00_2576;
																																	BgL__ortest_1115z00_2576
																																		=
																																		(BgL_classz00_2566
																																		==
																																		BgL_oclassz00_2572);
																																	if (BgL__ortest_1115z00_2576)
																																		{	/* Reduce/typec.scm 320 */
																																			BgL_res1870z00_2571
																																				=
																																				BgL__ortest_1115z00_2576;
																																		}
																																	else
																																		{	/* Reduce/typec.scm 320 */
																																			long
																																				BgL_odepthz00_2577;
																																			{	/* Reduce/typec.scm 320 */
																																				obj_t
																																					BgL_arg1804z00_2578;
																																				BgL_arg1804z00_2578
																																					=
																																					(BgL_oclassz00_2572);
																																				BgL_odepthz00_2577
																																					=
																																					BGL_CLASS_DEPTH
																																					(BgL_arg1804z00_2578);
																																			}
																																			if (
																																				(2L <
																																					BgL_odepthz00_2577))
																																				{	/* Reduce/typec.scm 320 */
																																					obj_t
																																						BgL_arg1802z00_2579;
																																					{	/* Reduce/typec.scm 320 */
																																						obj_t
																																							BgL_arg1803z00_2580;
																																						BgL_arg1803z00_2580
																																							=
																																							(BgL_oclassz00_2572);
																																						BgL_arg1802z00_2579
																																							=
																																							BGL_CLASS_ANCESTORS_REF
																																							(BgL_arg1803z00_2580,
																																							2L);
																																					}
																																					BgL_res1870z00_2571
																																						=
																																						(BgL_arg1802z00_2579
																																						==
																																						BgL_classz00_2566);
																																				}
																																			else
																																				{	/* Reduce/typec.scm 320 */
																																					BgL_res1870z00_2571
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																															}
																															BgL__ortest_1128z00_2565
																																=
																																BgL_res1870z00_2571;
																														}
																												}
																											}
																											if (BgL__ortest_1128z00_2565)
																												{	/* Reduce/typec.scm 320 */
																													BgL_test1987z00_3032 =
																														BgL__ortest_1128z00_2565;
																												}
																											else
																												{	/* Reduce/typec.scm 320 */
																													BgL_test1987z00_3032 =
																														BGl_foreignzd2typezf3z21zztype_typez00
																														(BgL_typeaz00_2564);
																												}
																										}
																									else
																										{	/* Reduce/typec.scm 320 */
																											BgL_test1987z00_3032 =
																												((bool_t) 0);
																										}
																									if (BgL_test1987z00_3032)
																										{	/* Reduce/typec.scm 320 */
																											BgL_test1974z00_2996 =
																												((bool_t) 0);
																										}
																									else
																										{	/* Reduce/typec.scm 320 */
																											BgL_test1974z00_2996 =
																												((bool_t) 1);
																										}
																								}
																						}
																				}
																		}
																}
														}
													if (BgL_test1974z00_2996)
														{	/* Reduce/typec.scm 341 */
															BGl_za2typezd2checkszd2removedza2z00zzreduce_typecz00
																=
																(1L +
																BGl_za2typezd2checkszd2removedza2z00zzreduce_typecz00);
															{	/* Reduce/typec.scm 348 */
																BgL_nodez00_bglt BgL_nodez00_2558;

																{	/* Reduce/typec.scm 348 */
																	BgL_literalz00_bglt BgL_arg1737z00_2559;

																	{	/* Reduce/typec.scm 348 */
																		BgL_literalz00_bglt BgL_new1134z00_2560;

																		{	/* Reduce/typec.scm 349 */
																			BgL_literalz00_bglt BgL_new1133z00_2561;

																			BgL_new1133z00_2561 =
																				((BgL_literalz00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_literalz00_bgl))));
																			{	/* Reduce/typec.scm 349 */
																				long BgL_arg1738z00_2562;

																				BgL_arg1738z00_2562 =
																					BGL_CLASS_NUM
																					(BGl_literalz00zzast_nodez00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1133z00_2561),
																					BgL_arg1738z00_2562);
																			}
																			{	/* Reduce/typec.scm 349 */
																				BgL_objectz00_bglt BgL_tmpz00_3065;

																				BgL_tmpz00_3065 =
																					((BgL_objectz00_bglt)
																					BgL_new1133z00_2561);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3065,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1133z00_2561);
																			BgL_new1134z00_2560 = BgL_new1133z00_2561;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1134z00_2560)))->
																				BgL_locz00) =
																			((obj_t) (((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_nodez00_2549)))->
																					BgL_locz00)), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1134z00_2560)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) BgL_typez00_2552),
																			BUNSPEC);
																		((((BgL_atomz00_bglt)
																					COBJECT(((BgL_atomz00_bglt)
																							BgL_new1134z00_2560)))->
																				BgL_valuez00) =
																			((obj_t) BFALSE), BUNSPEC);
																		BgL_arg1737z00_2559 = BgL_new1134z00_2560;
																	}
																	BgL_nodez00_2558 =
																		BGl_coercez12z12zzcoerce_coercez00(
																		((BgL_nodez00_bglt) BgL_arg1737z00_2559),
																		BUNSPEC, BgL_typez00_2552, ((bool_t) 0));
																}
																return BgL_nodez00_2558;
															}
														}
													else
														{	/* Reduce/typec.scm 341 */
															BGl_za2typezd2checkszd2remainingza2z00zzreduce_typecz00
																=
																(1L +
																BGl_za2typezd2checkszd2remainingza2z00zzreduce_typecz00);
															return ((BgL_nodez00_bglt) BgL_nodez00_2549);
														}
												}
										}
									}
								else
									{	/* Reduce/typec.scm 370 */
										obj_t BgL_g1137z00_2618;

										BgL_g1137z00_2618 =
											BGl_isazd2ofzd2zztype_miscz00(
											((BgL_nodez00_bglt)
												((BgL_appz00_bglt) BgL_nodez00_2487)));
										if (CBOOL(BgL_g1137z00_2618))
											{	/* Reduce/typec.scm 373 */
												BgL_typez00_bglt BgL_arg1691z00_2619;

												{	/* Reduce/typec.scm 373 */
													obj_t BgL_arg1692z00_2620;

													BgL_arg1692z00_2620 =
														CAR(
														(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_2487)))->
															BgL_argsz00));
													BgL_arg1691z00_2619 =
														BGl_getzd2typezd2zztype_typeofz00((
															(BgL_nodez00_bglt) BgL_arg1692z00_2620),
														((bool_t) 0));
												}
												{
													BgL_typez00_bglt BgL_typeaz00_3095;
													obj_t BgL_typecz00_3094;
													BgL_appz00_bglt BgL_nodez00_3092;

													BgL_nodez00_3092 =
														((BgL_appz00_bglt) BgL_nodez00_2487);
													BgL_typecz00_3094 = BgL_g1137z00_2618;
													BgL_typeaz00_3095 = BgL_arg1691z00_2619;
													BgL_typeaz00_2551 = BgL_typeaz00_3095;
													BgL_typecz00_2550 = BgL_typecz00_3094;
													BgL_nodez00_2549 = BgL_nodez00_3092;
													goto BgL_checkzd2typezd2_2547;
												}
											}
										else
											{	/* Reduce/typec.scm 370 */
												return
													((BgL_nodez00_bglt)
													((BgL_appz00_bglt) BgL_nodez00_2487));
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* &node-typec!-box-ref1329 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2boxzd2ref1329za2zzreduce_typecz00(obj_t
		BgL_envz00_2488, obj_t BgL_nodez00_2489)
	{
		{	/* Reduce/typec.scm 279 */
			{
				BgL_varz00_bglt BgL_auxz00_3098;

				{	/* Reduce/typec.scm 281 */
					BgL_varz00_bglt BgL_arg1615z00_2622;

					BgL_arg1615z00_2622 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2489)))->BgL_varz00);
					BgL_auxz00_3098 =
						((BgL_varz00_bglt)
						BGl_nodezd2typecz12zc0zzreduce_typecz00(
							((BgL_nodez00_bglt) BgL_arg1615z00_2622)));
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2489)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3098), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2489));
		}

	}



/* &node-typec!-box-set!1327 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2boxzd2setz121327zb0zzreduce_typecz00(obj_t
		BgL_envz00_2490, obj_t BgL_nodez00_2491)
	{
		{	/* Reduce/typec.scm 270 */
			{
				BgL_varz00_bglt BgL_auxz00_3108;

				{	/* Reduce/typec.scm 272 */
					BgL_varz00_bglt BgL_arg1611z00_2624;

					BgL_arg1611z00_2624 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2491)))->BgL_varz00);
					BgL_auxz00_3108 =
						((BgL_varz00_bglt)
						BGl_nodezd2typecz12zc0zzreduce_typecz00(
							((BgL_nodez00_bglt) BgL_arg1611z00_2624)));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2491)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3108), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3116;

				{	/* Reduce/typec.scm 273 */
					BgL_nodez00_bglt BgL_arg1613z00_2625;

					BgL_arg1613z00_2625 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2491)))->BgL_valuez00);
					BgL_auxz00_3116 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1613z00_2625);
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2491)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3116), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2491));
		}

	}



/* &node-typec!-make-box1325 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2makezd2box1325za2zzreduce_typecz00(obj_t
		BgL_envz00_2492, obj_t BgL_nodez00_2493)
	{
		{	/* Reduce/typec.scm 262 */
			{
				BgL_nodez00_bglt BgL_auxz00_3124;

				{	/* Reduce/typec.scm 264 */
					BgL_nodez00_bglt BgL_arg1609z00_2627;

					BgL_arg1609z00_2627 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2493)))->BgL_valuez00);
					BgL_auxz00_3124 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1609z00_2627);
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2493)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3124), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2493));
		}

	}



/* &node-typec!-return1323 */
	BgL_nodez00_bglt BGl_z62nodezd2typecz12zd2return1323z70zzreduce_typecz00(obj_t
		BgL_envz00_2494, obj_t BgL_nodez00_2495)
	{
		{	/* Reduce/typec.scm 254 */
			{
				BgL_nodez00_bglt BgL_auxz00_3132;

				{	/* Reduce/typec.scm 256 */
					BgL_nodez00_bglt BgL_arg1606z00_2629;

					BgL_arg1606z00_2629 =
						(((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nodez00_2495)))->BgL_valuez00);
					BgL_auxz00_3132 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1606z00_2629);
				}
				((((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nodez00_2495)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3132), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_returnz00_bglt) BgL_nodez00_2495));
		}

	}



/* &node-typec!-retblock1321 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2retblock1321z70zzreduce_typecz00(obj_t
		BgL_envz00_2496, obj_t BgL_nodez00_2497)
	{
		{	/* Reduce/typec.scm 246 */
			{
				BgL_nodez00_bglt BgL_auxz00_3140;

				{	/* Reduce/typec.scm 248 */
					BgL_nodez00_bglt BgL_arg1605z00_2631;

					BgL_arg1605z00_2631 =
						(((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nodez00_2497)))->BgL_bodyz00);
					BgL_auxz00_3140 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1605z00_2631);
				}
				((((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nodez00_2497)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3140), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_retblockz00_bglt) BgL_nodez00_2497));
		}

	}



/* &node-typec!-jump-ex-1319 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2jumpzd2exzd21319z70zzreduce_typecz00(obj_t
		BgL_envz00_2498, obj_t BgL_nodez00_2499)
	{
		{	/* Reduce/typec.scm 237 */
			{
				BgL_nodez00_bglt BgL_auxz00_3148;

				{	/* Reduce/typec.scm 239 */
					BgL_nodez00_bglt BgL_arg1595z00_2633;

					BgL_arg1595z00_2633 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2499)))->BgL_exitz00);
					BgL_auxz00_3148 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1595z00_2633);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2499)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_3148), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3154;

				{	/* Reduce/typec.scm 240 */
					BgL_nodez00_bglt BgL_arg1602z00_2634;

					BgL_arg1602z00_2634 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2499)))->
						BgL_valuez00);
					BgL_auxz00_3154 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1602z00_2634);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2499)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_3154), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2499));
		}

	}



/* &node-typec!-set-ex-i1317 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2setzd2exzd2i1317z70zzreduce_typecz00(obj_t
		BgL_envz00_2500, obj_t BgL_nodez00_2501)
	{
		{	/* Reduce/typec.scm 227 */
			{
				BgL_nodez00_bglt BgL_auxz00_3162;

				{	/* Reduce/typec.scm 229 */
					BgL_nodez00_bglt BgL_arg1591z00_2636;

					BgL_arg1591z00_2636 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2501)))->BgL_bodyz00);
					BgL_auxz00_3162 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1591z00_2636);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2501)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3162), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3168;

				{	/* Reduce/typec.scm 230 */
					BgL_nodez00_bglt BgL_arg1593z00_2637;

					BgL_arg1593z00_2637 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2501)))->
						BgL_onexitz00);
					BgL_auxz00_3168 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1593z00_2637);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2501)))->
						BgL_onexitz00) = ((BgL_nodez00_bglt) BgL_auxz00_3168), BUNSPEC);
			}
			{
				BgL_varz00_bglt BgL_auxz00_3174;

				{	/* Reduce/typec.scm 231 */
					BgL_varz00_bglt BgL_arg1594z00_2638;

					BgL_arg1594z00_2638 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2501)))->BgL_varz00);
					BgL_auxz00_3174 =
						((BgL_varz00_bglt)
						BGl_nodezd2typecz12zc0zzreduce_typecz00(
							((BgL_nodez00_bglt) BgL_arg1594z00_2638)));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2501)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3174), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2501));
		}

	}



/* &node-typec!-let-var1315 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2letzd2var1315za2zzreduce_typecz00(obj_t
		BgL_envz00_2502, obj_t BgL_nodez00_2503)
	{
		{	/* Reduce/typec.scm 216 */
			{	/* Reduce/typec.scm 218 */
				obj_t BgL_g1281z00_2640;

				BgL_g1281z00_2640 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2503)))->BgL_bindingsz00);
				{
					obj_t BgL_l1279z00_2642;

					BgL_l1279z00_2642 = BgL_g1281z00_2640;
				BgL_zc3z04anonymousza31574ze3z87_2641:
					if (PAIRP(BgL_l1279z00_2642))
						{	/* Reduce/typec.scm 218 */
							{	/* Reduce/typec.scm 219 */
								obj_t BgL_bindingz00_2643;

								BgL_bindingz00_2643 = CAR(BgL_l1279z00_2642);
								{	/* Reduce/typec.scm 219 */
									BgL_nodez00_bglt BgL_arg1576z00_2644;

									{	/* Reduce/typec.scm 219 */
										obj_t BgL_arg1584z00_2645;

										BgL_arg1584z00_2645 = CDR(((obj_t) BgL_bindingz00_2643));
										BgL_arg1576z00_2644 =
											BGl_nodezd2typecz12zc0zzreduce_typecz00(
											((BgL_nodez00_bglt) BgL_arg1584z00_2645));
									}
									{	/* Reduce/typec.scm 219 */
										obj_t BgL_auxz00_3195;
										obj_t BgL_tmpz00_3193;

										BgL_auxz00_3195 = ((obj_t) BgL_arg1576z00_2644);
										BgL_tmpz00_3193 = ((obj_t) BgL_bindingz00_2643);
										SET_CDR(BgL_tmpz00_3193, BgL_auxz00_3195);
									}
								}
							}
							{
								obj_t BgL_l1279z00_3198;

								BgL_l1279z00_3198 = CDR(BgL_l1279z00_2642);
								BgL_l1279z00_2642 = BgL_l1279z00_3198;
								goto BgL_zc3z04anonymousza31574ze3z87_2641;
							}
						}
					else
						{	/* Reduce/typec.scm 218 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3200;

				{	/* Reduce/typec.scm 221 */
					BgL_nodez00_bglt BgL_arg1589z00_2646;

					BgL_arg1589z00_2646 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2503)))->BgL_bodyz00);
					BgL_auxz00_3200 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1589z00_2646);
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2503)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3200), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_2503));
		}

	}



/* &node-typec!-let-fun1313 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2letzd2fun1313za2zzreduce_typecz00(obj_t
		BgL_envz00_2504, obj_t BgL_nodez00_2505)
	{
		{	/* Reduce/typec.scm 204 */
			{	/* Reduce/typec.scm 206 */
				obj_t BgL_g1278z00_2648;

				BgL_g1278z00_2648 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2505)))->BgL_localsz00);
				{
					obj_t BgL_l1276z00_2650;

					BgL_l1276z00_2650 = BgL_g1278z00_2648;
				BgL_zc3z04anonymousza31560ze3z87_2649:
					if (PAIRP(BgL_l1276z00_2650))
						{	/* Reduce/typec.scm 206 */
							{	/* Reduce/typec.scm 207 */
								obj_t BgL_localz00_2651;

								BgL_localz00_2651 = CAR(BgL_l1276z00_2650);
								{	/* Reduce/typec.scm 207 */
									BgL_valuez00_bglt BgL_funz00_2652;

									BgL_funz00_2652 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2651))))->
										BgL_valuez00);
									{	/* Reduce/typec.scm 208 */
										BgL_nodez00_bglt BgL_arg1564z00_2653;

										{	/* Reduce/typec.scm 208 */
											obj_t BgL_arg1565z00_2654;

											BgL_arg1565z00_2654 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2652)))->
												BgL_bodyz00);
											BgL_arg1564z00_2653 =
												BGl_nodezd2typecz12zc0zzreduce_typecz00((
													(BgL_nodez00_bglt) BgL_arg1565z00_2654));
										}
										((((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2652)))->
												BgL_bodyz00) =
											((obj_t) ((obj_t) BgL_arg1564z00_2653)), BUNSPEC);
									}
								}
							}
							{
								obj_t BgL_l1276z00_3223;

								BgL_l1276z00_3223 = CDR(BgL_l1276z00_2650);
								BgL_l1276z00_2650 = BgL_l1276z00_3223;
								goto BgL_zc3z04anonymousza31560ze3z87_2649;
							}
						}
					else
						{	/* Reduce/typec.scm 206 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3225;

				{	/* Reduce/typec.scm 210 */
					BgL_nodez00_bglt BgL_arg1573z00_2655;

					BgL_arg1573z00_2655 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2505)))->BgL_bodyz00);
					BgL_auxz00_3225 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1573z00_2655);
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2505)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3225), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2505));
		}

	}



/* &node-typec!-switch1311 */
	BgL_nodez00_bglt BGl_z62nodezd2typecz12zd2switch1311z70zzreduce_typecz00(obj_t
		BgL_envz00_2506, obj_t BgL_nodez00_2507)
	{
		{	/* Reduce/typec.scm 193 */
			{
				BgL_nodez00_bglt BgL_auxz00_3233;

				{	/* Reduce/typec.scm 195 */
					BgL_nodez00_bglt BgL_arg1544z00_2657;

					BgL_arg1544z00_2657 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2507)))->BgL_testz00);
					BgL_auxz00_3233 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1544z00_2657);
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2507)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3233), BUNSPEC);
			}
			{	/* Reduce/typec.scm 196 */
				obj_t BgL_g1275z00_2658;

				BgL_g1275z00_2658 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2507)))->BgL_clausesz00);
				{
					obj_t BgL_l1273z00_2660;

					BgL_l1273z00_2660 = BgL_g1275z00_2658;
				BgL_zc3z04anonymousza31545ze3z87_2659:
					if (PAIRP(BgL_l1273z00_2660))
						{	/* Reduce/typec.scm 196 */
							{	/* Reduce/typec.scm 197 */
								obj_t BgL_clausez00_2661;

								BgL_clausez00_2661 = CAR(BgL_l1273z00_2660);
								{	/* Reduce/typec.scm 197 */
									BgL_nodez00_bglt BgL_arg1552z00_2662;

									{	/* Reduce/typec.scm 197 */
										obj_t BgL_arg1553z00_2663;

										BgL_arg1553z00_2663 = CDR(((obj_t) BgL_clausez00_2661));
										BgL_arg1552z00_2662 =
											BGl_nodezd2typecz12zc0zzreduce_typecz00(
											((BgL_nodez00_bglt) BgL_arg1553z00_2663));
									}
									{	/* Reduce/typec.scm 197 */
										obj_t BgL_auxz00_3250;
										obj_t BgL_tmpz00_3248;

										BgL_auxz00_3250 = ((obj_t) BgL_arg1552z00_2662);
										BgL_tmpz00_3248 = ((obj_t) BgL_clausez00_2661);
										SET_CDR(BgL_tmpz00_3248, BgL_auxz00_3250);
									}
								}
							}
							{
								obj_t BgL_l1273z00_3253;

								BgL_l1273z00_3253 = CDR(BgL_l1273z00_2660);
								BgL_l1273z00_2660 = BgL_l1273z00_3253;
								goto BgL_zc3z04anonymousza31545ze3z87_2659;
							}
						}
					else
						{	/* Reduce/typec.scm 196 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_2507));
		}

	}



/* &node-typec!-fail1309 */
	BgL_nodez00_bglt BGl_z62nodezd2typecz12zd2fail1309z70zzreduce_typecz00(obj_t
		BgL_envz00_2508, obj_t BgL_nodez00_2509)
	{
		{	/* Reduce/typec.scm 183 */
			{
				BgL_nodez00_bglt BgL_auxz00_3257;

				{	/* Reduce/typec.scm 185 */
					BgL_nodez00_bglt BgL_arg1516z00_2665;

					BgL_arg1516z00_2665 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2509)))->BgL_procz00);
					BgL_auxz00_3257 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1516z00_2665);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2509)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3257), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3263;

				{	/* Reduce/typec.scm 186 */
					BgL_nodez00_bglt BgL_arg1535z00_2666;

					BgL_arg1535z00_2666 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2509)))->BgL_msgz00);
					BgL_auxz00_3263 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1535z00_2666);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2509)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3263), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3269;

				{	/* Reduce/typec.scm 187 */
					BgL_nodez00_bglt BgL_arg1540z00_2667;

					BgL_arg1540z00_2667 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2509)))->BgL_objz00);
					BgL_auxz00_3269 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1540z00_2667);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2509)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3269), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_2509));
		}

	}



/* &node-typec!-conditio1307 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2conditio1307z70zzreduce_typecz00(obj_t
		BgL_envz00_2510, obj_t BgL_nodez00_2511)
	{
		{	/* Reduce/typec.scm 159 */
			{
				BgL_nodez00_bglt BgL_auxz00_3277;

				{	/* Reduce/typec.scm 161 */
					BgL_nodez00_bglt BgL_arg1485z00_2669;

					BgL_arg1485z00_2669 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2511)))->BgL_testz00);
					BgL_auxz00_3277 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1485z00_2669);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2511)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3277), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3283;

				{	/* Reduce/typec.scm 162 */
					BgL_nodez00_bglt BgL_arg1489z00_2670;

					BgL_arg1489z00_2670 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2511)))->BgL_truez00);
					BgL_auxz00_3283 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1489z00_2670);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2511)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3283), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3289;

				{	/* Reduce/typec.scm 163 */
					BgL_nodez00_bglt BgL_arg1502z00_2671;

					BgL_arg1502z00_2671 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2511)))->BgL_falsez00);
					BgL_auxz00_3289 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1502z00_2671);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2511)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3289), BUNSPEC);
			}
			if (BGl_pairzd2ofzd2pairzd2nilzf3z21zzreduce_typecz00(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2511)))->BgL_testz00)))
				{	/* Reduce/typec.scm 164 */
					{	/* Reduce/typec.scm 171 */
						BgL_nodez00_bglt BgL_arg1513z00_2672;

						BgL_arg1513z00_2672 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_2511)))->BgL_testz00);
						{	/* Reduce/typec.scm 393 */
							BgL_varz00_bglt BgL_i1144z00_2673;

							BgL_i1144z00_2673 =
								(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_arg1513z00_2672)))->BgL_funz00);
							((((BgL_varz00_bglt) COBJECT(BgL_i1144z00_2673))->
									BgL_variablez00) =
								((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
										BGl_za2nullzf3za2zf3zzreduce_typecz00)), BUNSPEC);
						}
					}
					{	/* Reduce/typec.scm 172 */
						BgL_nodez00_bglt BgL_otruez00_2674;

						BgL_otruez00_2674 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_2511)))->BgL_truez00);
						((((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_nodez00_2511)))->
								BgL_truez00) =
							((BgL_nodez00_bglt) (((BgL_conditionalz00_bglt)
										COBJECT(((BgL_conditionalz00_bglt) BgL_nodez00_2511)))->
									BgL_falsez00)), BUNSPEC);
						((((BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt)
											BgL_nodez00_2511)))->BgL_falsez00) =
							((BgL_nodez00_bglt) BgL_otruez00_2674), BUNSPEC);
						BNIL;
					}
				}
			else
				{	/* Reduce/typec.scm 164 */
					BFALSE;
				}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_2511));
		}

	}



/* &node-typec!-setq1305 */
	BgL_nodez00_bglt BGl_z62nodezd2typecz12zd2setq1305z70zzreduce_typecz00(obj_t
		BgL_envz00_2512, obj_t BgL_nodez00_2513)
	{
		{	/* Reduce/typec.scm 150 */
			{
				BgL_nodez00_bglt BgL_auxz00_3315;

				{	/* Reduce/typec.scm 152 */
					BgL_nodez00_bglt BgL_arg1472z00_2676;

					BgL_arg1472z00_2676 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2513)))->BgL_valuez00);
					BgL_auxz00_3315 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1472z00_2676);
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2513)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3315), BUNSPEC);
			}
			{
				BgL_varz00_bglt BgL_auxz00_3321;

				{	/* Reduce/typec.scm 153 */
					BgL_varz00_bglt BgL_arg1473z00_2677;

					BgL_arg1473z00_2677 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2513)))->BgL_varz00);
					BgL_auxz00_3321 =
						((BgL_varz00_bglt)
						BGl_nodezd2typecz12zc0zzreduce_typecz00(
							((BgL_nodez00_bglt) BgL_arg1473z00_2677)));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2513)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3321), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_2513));
		}

	}



/* &node-typec!-cast1303 */
	BgL_nodez00_bglt BGl_z62nodezd2typecz12zd2cast1303z70zzreduce_typecz00(obj_t
		BgL_envz00_2514, obj_t BgL_nodez00_2515)
	{
		{	/* Reduce/typec.scm 142 */
			BGl_nodezd2typecz12zc0zzreduce_typecz00(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2515)))->BgL_argz00));
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_2515));
		}

	}



/* &node-typec!-extern1301 */
	BgL_nodez00_bglt BGl_z62nodezd2typecz12zd2extern1301z70zzreduce_typecz00(obj_t
		BgL_envz00_2516, obj_t BgL_nodez00_2517)
	{
		{	/* Reduce/typec.scm 134 */
			BGl_nodezd2typecza2z12z62zzreduce_typecz00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2517)))->BgL_exprza2za2));
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2517));
		}

	}



/* &node-typec!-funcall1299 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2funcall1299z70zzreduce_typecz00(obj_t
		BgL_envz00_2518, obj_t BgL_nodez00_2519)
	{
		{	/* Reduce/typec.scm 125 */
			{
				BgL_nodez00_bglt BgL_auxz00_3341;

				{	/* Reduce/typec.scm 127 */
					BgL_nodez00_bglt BgL_arg1437z00_2681;

					BgL_arg1437z00_2681 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2519)))->BgL_funz00);
					BgL_auxz00_3341 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1437z00_2681);
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2519)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3341), BUNSPEC);
			}
			BGl_nodezd2typecza2z12z62zzreduce_typecz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2519)))->BgL_argsz00));
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_2519));
		}

	}



/* &node-typec!-app-ly1297 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2appzd2ly1297za2zzreduce_typecz00(obj_t
		BgL_envz00_2520, obj_t BgL_nodez00_2521)
	{
		{	/* Reduce/typec.scm 116 */
			{
				BgL_nodez00_bglt BgL_auxz00_3352;

				{	/* Reduce/typec.scm 118 */
					BgL_nodez00_bglt BgL_arg1422z00_2683;

					BgL_arg1422z00_2683 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2521)))->BgL_funz00);
					BgL_auxz00_3352 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1422z00_2683);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2521)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3352), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3358;

				{	/* Reduce/typec.scm 119 */
					BgL_nodez00_bglt BgL_arg1434z00_2684;

					BgL_arg1434z00_2684 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2521)))->BgL_argz00);
					BgL_auxz00_3358 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1434z00_2684);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2521)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3358), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2521));
		}

	}



/* &node-typec!-sync1295 */
	BgL_nodez00_bglt BGl_z62nodezd2typecz12zd2sync1295z70zzreduce_typecz00(obj_t
		BgL_envz00_2522, obj_t BgL_nodez00_2523)
	{
		{	/* Reduce/typec.scm 106 */
			{
				BgL_nodez00_bglt BgL_auxz00_3366;

				{	/* Reduce/typec.scm 108 */
					BgL_nodez00_bglt BgL_arg1408z00_2686;

					BgL_arg1408z00_2686 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2523)))->BgL_mutexz00);
					BgL_auxz00_3366 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1408z00_2686);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2523)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3366), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3372;

				{	/* Reduce/typec.scm 109 */
					BgL_nodez00_bglt BgL_arg1410z00_2687;

					BgL_arg1410z00_2687 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2523)))->BgL_prelockz00);
					BgL_auxz00_3372 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1410z00_2687);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2523)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3372), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3378;

				{	/* Reduce/typec.scm 110 */
					BgL_nodez00_bglt BgL_arg1421z00_2688;

					BgL_arg1421z00_2688 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2523)))->BgL_bodyz00);
					BgL_auxz00_3378 =
						BGl_nodezd2typecz12zc0zzreduce_typecz00(BgL_arg1421z00_2688);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2523)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3378), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2523));
		}

	}



/* &node-typec!-sequence1293 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2sequence1293z70zzreduce_typecz00(obj_t
		BgL_envz00_2524, obj_t BgL_nodez00_2525)
	{
		{	/* Reduce/typec.scm 98 */
			BGl_nodezd2typecza2z12z62zzreduce_typecz00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2525)))->BgL_nodesz00));
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_2525));
		}

	}



/* &node-typec!-closure1291 */
	BgL_nodez00_bglt
		BGl_z62nodezd2typecz12zd2closure1291z70zzreduce_typecz00(obj_t
		BgL_envz00_2526, obj_t BgL_nodez00_2527)
	{
		{	/* Reduce/typec.scm 92 */
			return ((BgL_nodez00_bglt) ((BgL_closurez00_bglt) BgL_nodez00_2527));
		}

	}



/* &node-typec!-var1289 */
	BgL_nodez00_bglt BGl_z62nodezd2typecz12zd2var1289z70zzreduce_typecz00(obj_t
		BgL_envz00_2528, obj_t BgL_nodez00_2529)
	{
		{	/* Reduce/typec.scm 86 */
			return ((BgL_nodez00_bglt) ((BgL_varz00_bglt) BgL_nodez00_2529));
		}

	}



/* &node-typec!-kwote1287 */
	BgL_nodez00_bglt BGl_z62nodezd2typecz12zd2kwote1287z70zzreduce_typecz00(obj_t
		BgL_envz00_2530, obj_t BgL_nodez00_2531)
	{
		{	/* Reduce/typec.scm 80 */
			return ((BgL_nodez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_2531));
		}

	}



/* &node-typec!-atom1285 */
	BgL_nodez00_bglt BGl_z62nodezd2typecz12zd2atom1285z70zzreduce_typecz00(obj_t
		BgL_envz00_2532, obj_t BgL_nodez00_2533)
	{
		{	/* Reduce/typec.scm 74 */
			return ((BgL_nodez00_bglt) ((BgL_atomz00_bglt) BgL_nodez00_2533));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzreduce_typecz00(void)
	{
		{	/* Reduce/typec.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
			BGl_modulezd2initializa7ationz75zztype_miscz00(49975086L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
			BGl_modulezd2initializa7ationz75zztype_coercionz00(116865673L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
			BGl_modulezd2initializa7ationz75zzcoerce_coercez00(361167184L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
			BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
			return
				BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1909z00zzreduce_typecz00));
		}

	}

#ifdef __cplusplus
}
#endif
