/*===========================================================================*/
/*   (Reduce/walk.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Reduce/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_REDUCE_WALK_TYPE_DEFINITIONS
#define BGL_REDUCE_WALK_TYPE_DEFINITIONS
#endif													// BGL_REDUCE_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	static obj_t BGl_requirezd2initializa7ationz75zzreduce_walkz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_reducezd2walkz12zc0zzreduce_walkz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzreduce_walkz00(void);
	extern obj_t BGl_reducezd2csez12zc0zzreduce_csez00(obj_t);
	static obj_t BGl_objectzd2initzd2zzreduce_walkz00(void);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_reducezd2conditionalz12zc0zzreduce_condz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzreduce_walkz00(void);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	extern obj_t BGl_removezd2varzd2zzast_removez00(obj_t, obj_t);
	static obj_t BGl_z62reducezd2walkz12za2zzreduce_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	extern obj_t BGl_reducezd2copyz12zc0zzreduce_copyz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzreduce_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzreduce_betaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzreduce_1occz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzreduce_flowzd2typeczd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzreduce_typecz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzreduce_condz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzreduce_csez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzreduce_copyz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzreduce_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzreduce_walkz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzreduce_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzreduce_walkz00(void);
	extern obj_t BGl_reducezd2typezd2checkz12z12zzreduce_typecz00(obj_t);
	extern obj_t BGl_za2optimzd2dataflowzf3za2z21zzengine_paramz00;
	extern obj_t BGl_reducezd21occz12zc0zzreduce_1occz00(obj_t);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	extern obj_t BGl_reducezd2betaz12zc0zzreduce_betaz00(obj_t);
	extern obj_t BGl_za2optimzd2reducezd2betazf3za2zf3zzengine_paramz00;
	extern obj_t
		BGl_reducezd2flowzd2typezd2checkz12zc0zzreduce_flowzd2typeczd2(obj_t);
	static obj_t __cnst[2];


	   
		 
		DEFINE_STRING(BGl_string1556z00zzreduce_walkz00,
		BgL_bgl_string1556za700za7za7r1566za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1557z00zzreduce_walkz00,
		BgL_bgl_string1557za700za7za7r1567za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1558z00zzreduce_walkz00,
		BgL_bgl_string1558za700za7za7r1568za7, " error", 6);
	      DEFINE_STRING(BGl_string1559z00zzreduce_walkz00,
		BgL_bgl_string1559za700za7za7r1569za7, "s", 1);
	      DEFINE_STRING(BGl_string1560z00zzreduce_walkz00,
		BgL_bgl_string1560za700za7za7r1570za7, "", 0);
	      DEFINE_STRING(BGl_string1561z00zzreduce_walkz00,
		BgL_bgl_string1561za700za7za7r1571za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1562z00zzreduce_walkz00,
		BgL_bgl_string1562za700za7za7r1572za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1563z00zzreduce_walkz00,
		BgL_bgl_string1563za700za7za7r1573za7, "reduce_walk", 11);
	      DEFINE_STRING(BGl_string1564z00zzreduce_walkz00,
		BgL_bgl_string1564za700za7za7r1574za7, "reduce pass-started ", 20);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_reducezd2walkz12zd2envz12zzreduce_walkz00,
		BgL_bgl_za762reduceza7d2walk1575z00, va_generic_entry,
		BGl_z62reducezd2walkz12za2zzreduce_walkz00, BUNSPEC, -3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzreduce_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzreduce_walkz00(long
		BgL_checksumz00_1630, char *BgL_fromz00_1631)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzreduce_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzreduce_walkz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzreduce_walkz00();
					BGl_libraryzd2moduleszd2initz00zzreduce_walkz00();
					BGl_cnstzd2initzd2zzreduce_walkz00();
					BGl_importedzd2moduleszd2initz00zzreduce_walkz00();
					return BGl_methodzd2initzd2zzreduce_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzreduce_walkz00(void)
	{
		{	/* Reduce/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"reduce_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"reduce_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "reduce_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "reduce_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "reduce_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "reduce_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "reduce_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"reduce_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzreduce_walkz00(void)
	{
		{	/* Reduce/walk.scm 15 */
			{	/* Reduce/walk.scm 15 */
				obj_t BgL_cportz00_1619;

				{	/* Reduce/walk.scm 15 */
					obj_t BgL_stringz00_1626;

					BgL_stringz00_1626 = BGl_string1564z00zzreduce_walkz00;
					{	/* Reduce/walk.scm 15 */
						obj_t BgL_startz00_1627;

						BgL_startz00_1627 = BINT(0L);
						{	/* Reduce/walk.scm 15 */
							obj_t BgL_endz00_1628;

							BgL_endz00_1628 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1626)));
							{	/* Reduce/walk.scm 15 */

								BgL_cportz00_1619 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1626, BgL_startz00_1627, BgL_endz00_1628);
				}}}}
				{
					long BgL_iz00_1620;

					BgL_iz00_1620 = 1L;
				BgL_loopz00_1621:
					if ((BgL_iz00_1620 == -1L))
						{	/* Reduce/walk.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Reduce/walk.scm 15 */
							{	/* Reduce/walk.scm 15 */
								obj_t BgL_arg1565z00_1622;

								{	/* Reduce/walk.scm 15 */

									{	/* Reduce/walk.scm 15 */
										obj_t BgL_locationz00_1624;

										BgL_locationz00_1624 = BBOOL(((bool_t) 0));
										{	/* Reduce/walk.scm 15 */

											BgL_arg1565z00_1622 =
												BGl_readz00zz__readerz00(BgL_cportz00_1619,
												BgL_locationz00_1624);
										}
									}
								}
								{	/* Reduce/walk.scm 15 */
									int BgL_tmpz00_1657;

									BgL_tmpz00_1657 = (int) (BgL_iz00_1620);
									CNST_TABLE_SET(BgL_tmpz00_1657, BgL_arg1565z00_1622);
							}}
							{	/* Reduce/walk.scm 15 */
								int BgL_auxz00_1625;

								BgL_auxz00_1625 = (int) ((BgL_iz00_1620 - 1L));
								{
									long BgL_iz00_1662;

									BgL_iz00_1662 = (long) (BgL_auxz00_1625);
									BgL_iz00_1620 = BgL_iz00_1662;
									goto BgL_loopz00_1621;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzreduce_walkz00(void)
	{
		{	/* Reduce/walk.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* reduce-walk! */
	BGL_EXPORTED_DEF obj_t BGl_reducezd2walkz12zc0zzreduce_walkz00(obj_t
		BgL_globalsz00_3, obj_t BgL_msgz00_4, obj_t BgL_typezd2unsafezd2_5)
	{
		{	/* Reduce/walk.scm 36 */
			{	/* Reduce/walk.scm 37 */
				obj_t BgL_list1235z00_1355;

				{	/* Reduce/walk.scm 37 */
					obj_t BgL_arg1236z00_1356;

					{	/* Reduce/walk.scm 37 */
						obj_t BgL_arg1238z00_1357;

						BgL_arg1238z00_1357 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1236z00_1356 =
							MAKE_YOUNG_PAIR(BgL_msgz00_4, BgL_arg1238z00_1357);
					}
					BgL_list1235z00_1355 =
						MAKE_YOUNG_PAIR(BGl_string1556z00zzreduce_walkz00,
						BgL_arg1236z00_1356);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1235z00_1355);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 = BgL_msgz00_4;
			{	/* Reduce/walk.scm 37 */
				obj_t BgL_g1106z00_1358;

				BgL_g1106z00_1358 = BNIL;
				{
					obj_t BgL_hooksz00_1361;
					obj_t BgL_hnamesz00_1362;

					BgL_hooksz00_1361 = BgL_g1106z00_1358;
					BgL_hnamesz00_1362 = BNIL;
				BgL_zc3z04anonymousza31239ze3z87_1363:
					if (NULLP(BgL_hooksz00_1361))
						{	/* Reduce/walk.scm 37 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Reduce/walk.scm 37 */
							bool_t BgL_test1579z00_1675;

							{	/* Reduce/walk.scm 37 */
								obj_t BgL_fun1251z00_1370;

								BgL_fun1251z00_1370 = CAR(((obj_t) BgL_hooksz00_1361));
								BgL_test1579z00_1675 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1251z00_1370));
							}
							if (BgL_test1579z00_1675)
								{	/* Reduce/walk.scm 37 */
									obj_t BgL_arg1244z00_1367;
									obj_t BgL_arg1248z00_1368;

									BgL_arg1244z00_1367 = CDR(((obj_t) BgL_hooksz00_1361));
									BgL_arg1248z00_1368 = CDR(((obj_t) BgL_hnamesz00_1362));
									{
										obj_t BgL_hnamesz00_1687;
										obj_t BgL_hooksz00_1686;

										BgL_hooksz00_1686 = BgL_arg1244z00_1367;
										BgL_hnamesz00_1687 = BgL_arg1248z00_1368;
										BgL_hnamesz00_1362 = BgL_hnamesz00_1687;
										BgL_hooksz00_1361 = BgL_hooksz00_1686;
										goto BgL_zc3z04anonymousza31239ze3z87_1363;
									}
								}
							else
								{	/* Reduce/walk.scm 37 */
									obj_t BgL_arg1249z00_1369;

									BgL_arg1249z00_1369 = CAR(((obj_t) BgL_hnamesz00_1362));
									BGl_internalzd2errorzd2zztools_errorz00(BgL_msgz00_4,
										BGl_string1557z00zzreduce_walkz00, BgL_arg1249z00_1369);
								}
						}
				}
			}
			{	/* Reduce/walk.scm 39 */
				bool_t BgL_test1580z00_1691;

				if (PAIRP(BgL_typezd2unsafezd2_5))
					{	/* Reduce/walk.scm 39 */
						BgL_test1580z00_1691 = CBOOL(CAR(BgL_typezd2unsafezd2_5));
					}
				else
					{	/* Reduce/walk.scm 39 */
						BgL_test1580z00_1691 = ((bool_t) 0);
					}
				if (BgL_test1580z00_1691)
					{	/* Reduce/walk.scm 39 */
						BGl_reducezd21occz12zc0zzreduce_1occz00(BgL_globalsz00_3);
						BGl_reducezd2typezd2checkz12z12zzreduce_typecz00(BgL_globalsz00_3);
						BGl_reducezd2conditionalz12zc0zzreduce_condz00(BgL_globalsz00_3);
						{	/* Reduce/walk.scm 43 */
							obj_t BgL_valuez00_1375;

							BgL_valuez00_1375 =
								BGl_removezd2varzd2zzast_removez00(CNST_TABLE_REF(1),
								BgL_globalsz00_3);
							if (((long)
									CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) >
									0L))
								{	/* Reduce/walk.scm 43 */
									{	/* Reduce/walk.scm 43 */
										obj_t BgL_port1230z00_1377;

										{	/* Reduce/walk.scm 43 */
											obj_t BgL_tmpz00_1704;

											BgL_tmpz00_1704 = BGL_CURRENT_DYNAMIC_ENV();
											BgL_port1230z00_1377 =
												BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_1704);
										}
										bgl_display_obj
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
											BgL_port1230z00_1377);
										bgl_display_string(BGl_string1558z00zzreduce_walkz00,
											BgL_port1230z00_1377);
										{	/* Reduce/walk.scm 43 */
											obj_t BgL_arg1268z00_1378;

											{	/* Reduce/walk.scm 43 */
												bool_t BgL_test1584z00_1709;

												if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
													{	/* Reduce/walk.scm 43 */
														if (INTEGERP
															(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
															{	/* Reduce/walk.scm 43 */
																BgL_test1584z00_1709 =
																	(
																	(long)
																	CINT
																	(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
																	> 1L);
															}
														else
															{	/* Reduce/walk.scm 43 */
																BgL_test1584z00_1709 =
																	BGl_2ze3ze3zz__r4_numbers_6_5z00
																	(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
																	BINT(1L));
															}
													}
												else
													{	/* Reduce/walk.scm 43 */
														BgL_test1584z00_1709 = ((bool_t) 0);
													}
												if (BgL_test1584z00_1709)
													{	/* Reduce/walk.scm 43 */
														BgL_arg1268z00_1378 =
															BGl_string1559z00zzreduce_walkz00;
													}
												else
													{	/* Reduce/walk.scm 43 */
														BgL_arg1268z00_1378 =
															BGl_string1560z00zzreduce_walkz00;
													}
											}
											bgl_display_obj(BgL_arg1268z00_1378,
												BgL_port1230z00_1377);
										}
										bgl_display_string(BGl_string1561z00zzreduce_walkz00,
											BgL_port1230z00_1377);
										bgl_display_char(((unsigned char) 10),
											BgL_port1230z00_1377);
									}
									{	/* Reduce/walk.scm 43 */
										obj_t BgL_list1271z00_1382;

										BgL_list1271z00_1382 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
										return BGl_exitz00zz__errorz00(BgL_list1271z00_1382);
									}
								}
							else
								{	/* Reduce/walk.scm 43 */
									obj_t BgL_g1108z00_1383;

									BgL_g1108z00_1383 = BNIL;
									{
										obj_t BgL_hooksz00_1386;
										obj_t BgL_hnamesz00_1387;

										BgL_hooksz00_1386 = BgL_g1108z00_1383;
										BgL_hnamesz00_1387 = BNIL;
									BgL_zc3z04anonymousza31272ze3z87_1388:
										if (NULLP(BgL_hooksz00_1386))
											{	/* Reduce/walk.scm 43 */
												return BgL_valuez00_1375;
											}
										else
											{	/* Reduce/walk.scm 43 */
												bool_t BgL_test1588z00_1726;

												{	/* Reduce/walk.scm 43 */
													obj_t BgL_fun1306z00_1395;

													BgL_fun1306z00_1395 =
														CAR(((obj_t) BgL_hooksz00_1386));
													BgL_test1588z00_1726 =
														CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1306z00_1395));
												}
												if (BgL_test1588z00_1726)
													{	/* Reduce/walk.scm 43 */
														obj_t BgL_arg1284z00_1392;
														obj_t BgL_arg1304z00_1393;

														BgL_arg1284z00_1392 =
															CDR(((obj_t) BgL_hooksz00_1386));
														BgL_arg1304z00_1393 =
															CDR(((obj_t) BgL_hnamesz00_1387));
														{
															obj_t BgL_hnamesz00_1738;
															obj_t BgL_hooksz00_1737;

															BgL_hooksz00_1737 = BgL_arg1284z00_1392;
															BgL_hnamesz00_1738 = BgL_arg1304z00_1393;
															BgL_hnamesz00_1387 = BgL_hnamesz00_1738;
															BgL_hooksz00_1386 = BgL_hooksz00_1737;
															goto BgL_zc3z04anonymousza31272ze3z87_1388;
														}
													}
												else
													{	/* Reduce/walk.scm 43 */
														obj_t BgL_arg1305z00_1394;

														BgL_arg1305z00_1394 =
															CAR(((obj_t) BgL_hnamesz00_1387));
														return
															BGl_internalzd2errorzd2zztools_errorz00
															(BGl_za2currentzd2passza2zd2zzengine_passz00,
															BGl_string1562z00zzreduce_walkz00,
															BgL_arg1305z00_1394);
													}
											}
									}
								}
						}
					}
				else
					{	/* Reduce/walk.scm 39 */
						if (CBOOL(BGl_za2optimzd2dataflowzf3za2z21zzengine_paramz00))
							{	/* Reduce/walk.scm 44 */
								BGl_reducezd2copyz12zc0zzreduce_copyz00(BgL_globalsz00_3);
								if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 2L))
									{	/* Reduce/walk.scm 46 */
										BGl_reducezd2csez12zc0zzreduce_csez00(BgL_globalsz00_3);
									}
								else
									{	/* Reduce/walk.scm 46 */
										BFALSE;
									}
								BGl_reducezd2typezd2checkz12z12zzreduce_typecz00
									(BgL_globalsz00_3);
								BGl_reducezd2copyz12zc0zzreduce_copyz00(BgL_globalsz00_3);
								BGl_reducezd2conditionalz12zc0zzreduce_condz00
									(BgL_globalsz00_3);
								BGl_reducezd21occz12zc0zzreduce_1occz00(BgL_globalsz00_3);
								if (CBOOL
									(BGl_za2optimzd2reducezd2betazf3za2zf3zzengine_paramz00))
									{	/* Reduce/walk.scm 51 */
										BGl_reducezd2betaz12zc0zzreduce_betaz00(BgL_globalsz00_3);
									}
								else
									{	/* Reduce/walk.scm 51 */
										BFALSE;
									}
								BGl_reducezd2flowzd2typezd2checkz12zc0zzreduce_flowzd2typeczd2
									(BgL_globalsz00_3);
								{	/* Reduce/walk.scm 53 */
									obj_t BgL_valuez00_1399;

									BgL_valuez00_1399 =
										BGl_removezd2varzd2zzast_removez00(CNST_TABLE_REF(1),
										BgL_globalsz00_3);
									if (((long)
											CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
											> 0L))
										{	/* Reduce/walk.scm 53 */
											{	/* Reduce/walk.scm 53 */
												obj_t BgL_port1231z00_1401;

												{	/* Reduce/walk.scm 53 */
													obj_t BgL_tmpz00_1762;

													BgL_tmpz00_1762 = BGL_CURRENT_DYNAMIC_ENV();
													BgL_port1231z00_1401 =
														BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_1762);
												}
												bgl_display_obj
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BgL_port1231z00_1401);
												bgl_display_string(BGl_string1558z00zzreduce_walkz00,
													BgL_port1231z00_1401);
												{	/* Reduce/walk.scm 53 */
													obj_t BgL_arg1310z00_1402;

													{	/* Reduce/walk.scm 53 */
														bool_t BgL_test1596z00_1767;

														if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
															(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
															{	/* Reduce/walk.scm 53 */
																if (INTEGERP
																	(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
																	{	/* Reduce/walk.scm 53 */
																		BgL_test1596z00_1767 =
																			(
																			(long)
																			CINT
																			(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
																			> 1L);
																	}
																else
																	{	/* Reduce/walk.scm 53 */
																		BgL_test1596z00_1767 =
																			BGl_2ze3ze3zz__r4_numbers_6_5z00
																			(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
																			BINT(1L));
																	}
															}
														else
															{	/* Reduce/walk.scm 53 */
																BgL_test1596z00_1767 = ((bool_t) 0);
															}
														if (BgL_test1596z00_1767)
															{	/* Reduce/walk.scm 53 */
																BgL_arg1310z00_1402 =
																	BGl_string1559z00zzreduce_walkz00;
															}
														else
															{	/* Reduce/walk.scm 53 */
																BgL_arg1310z00_1402 =
																	BGl_string1560z00zzreduce_walkz00;
															}
													}
													bgl_display_obj(BgL_arg1310z00_1402,
														BgL_port1231z00_1401);
												}
												bgl_display_string(BGl_string1561z00zzreduce_walkz00,
													BgL_port1231z00_1401);
												bgl_display_char(((unsigned char) 10),
													BgL_port1231z00_1401);
											}
											{	/* Reduce/walk.scm 53 */
												obj_t BgL_list1313z00_1406;

												BgL_list1313z00_1406 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
												return BGl_exitz00zz__errorz00(BgL_list1313z00_1406);
											}
										}
									else
										{	/* Reduce/walk.scm 53 */
											obj_t BgL_g1110z00_1407;

											BgL_g1110z00_1407 = BNIL;
											{
												obj_t BgL_hooksz00_1410;
												obj_t BgL_hnamesz00_1411;

												BgL_hooksz00_1410 = BgL_g1110z00_1407;
												BgL_hnamesz00_1411 = BNIL;
											BgL_zc3z04anonymousza31314ze3z87_1412:
												if (NULLP(BgL_hooksz00_1410))
													{	/* Reduce/walk.scm 53 */
														return BgL_valuez00_1399;
													}
												else
													{	/* Reduce/walk.scm 53 */
														bool_t BgL_test1601z00_1784;

														{	/* Reduce/walk.scm 53 */
															obj_t BgL_fun1321z00_1419;

															BgL_fun1321z00_1419 =
																CAR(((obj_t) BgL_hooksz00_1410));
															BgL_test1601z00_1784 =
																CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1321z00_1419));
														}
														if (BgL_test1601z00_1784)
															{	/* Reduce/walk.scm 53 */
																obj_t BgL_arg1318z00_1416;
																obj_t BgL_arg1319z00_1417;

																BgL_arg1318z00_1416 =
																	CDR(((obj_t) BgL_hooksz00_1410));
																BgL_arg1319z00_1417 =
																	CDR(((obj_t) BgL_hnamesz00_1411));
																{
																	obj_t BgL_hnamesz00_1796;
																	obj_t BgL_hooksz00_1795;

																	BgL_hooksz00_1795 = BgL_arg1318z00_1416;
																	BgL_hnamesz00_1796 = BgL_arg1319z00_1417;
																	BgL_hnamesz00_1411 = BgL_hnamesz00_1796;
																	BgL_hooksz00_1410 = BgL_hooksz00_1795;
																	goto BgL_zc3z04anonymousza31314ze3z87_1412;
																}
															}
														else
															{	/* Reduce/walk.scm 53 */
																obj_t BgL_arg1320z00_1418;

																BgL_arg1320z00_1418 =
																	CAR(((obj_t) BgL_hnamesz00_1411));
																return
																	BGl_internalzd2errorzd2zztools_errorz00
																	(BGl_za2currentzd2passza2zd2zzengine_passz00,
																	BGl_string1562z00zzreduce_walkz00,
																	BgL_arg1320z00_1418);
															}
													}
											}
										}
								}
							}
						else
							{	/* Reduce/walk.scm 44 */
								return
									BGl_removezd2varzd2zzast_removez00(CNST_TABLE_REF(1),
									BgL_globalsz00_3);
							}
					}
			}
		}

	}



/* &reduce-walk! */
	obj_t BGl_z62reducezd2walkz12za2zzreduce_walkz00(obj_t BgL_envz00_1615,
		obj_t BgL_globalsz00_1616, obj_t BgL_msgz00_1617,
		obj_t BgL_typezd2unsafezd2_1618)
	{
		{	/* Reduce/walk.scm 36 */
			return
				BGl_reducezd2walkz12zc0zzreduce_walkz00(BgL_globalsz00_1616,
				BgL_msgz00_1617, BgL_typezd2unsafezd2_1618);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzreduce_walkz00(void)
	{
		{	/* Reduce/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzreduce_walkz00(void)
	{
		{	/* Reduce/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzreduce_walkz00(void)
	{
		{	/* Reduce/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzreduce_walkz00(void)
	{
		{	/* Reduce/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1563z00zzreduce_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1563z00zzreduce_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1563z00zzreduce_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1563z00zzreduce_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1563z00zzreduce_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1563z00zzreduce_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1563z00zzreduce_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1563z00zzreduce_walkz00));
			BGl_modulezd2initializa7ationz75zzreduce_copyz00(215645584L,
				BSTRING_TO_STRING(BGl_string1563z00zzreduce_walkz00));
			BGl_modulezd2initializa7ationz75zzreduce_csez00(347839157L,
				BSTRING_TO_STRING(BGl_string1563z00zzreduce_walkz00));
			BGl_modulezd2initializa7ationz75zzreduce_condz00(39795278L,
				BSTRING_TO_STRING(BGl_string1563z00zzreduce_walkz00));
			BGl_modulezd2initializa7ationz75zzreduce_typecz00(433031772L,
				BSTRING_TO_STRING(BGl_string1563z00zzreduce_walkz00));
			BGl_modulezd2initializa7ationz75zzreduce_flowzd2typeczd2(67029510L,
				BSTRING_TO_STRING(BGl_string1563z00zzreduce_walkz00));
			BGl_modulezd2initializa7ationz75zzreduce_1occz00(256644222L,
				BSTRING_TO_STRING(BGl_string1563z00zzreduce_walkz00));
			BGl_modulezd2initializa7ationz75zzreduce_betaz00(214257086L,
				BSTRING_TO_STRING(BGl_string1563z00zzreduce_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string1563z00zzreduce_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
