/*===========================================================================*/
/*   (Reduce/sbeta.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Reduce/sbeta.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_REDUCE_BETA_TYPE_DEFINITIONS
#define BGL_REDUCE_BETA_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_getfieldz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_REDUCE_BETA_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static bool_t BGl_simplezf3ze70z14zzreduce_betaz00(obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_za2intza2z00zztype_cachez00;
	static obj_t BGl_requirezd2initializa7ationz75zzreduce_betaz00 = BUNSPEC;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzreduce_betaz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzreduce_betaz00(void);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2appzd2ly1371za2zzreduce_betaz00(obj_t, obj_t);
	extern obj_t BGl_getfieldz00zzast_nodez00;
	static obj_t BGl_objectzd2initzd2zzreduce_betaz00(void);
	static obj_t BGl_nodezd2betaza2z12z62zzreduce_betaz00(obj_t);
	static obj_t BGl_za2czd2stringzd2lengthza2z00zzreduce_betaz00 = BUNSPEC;
	extern obj_t BGl_castz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	static long BGl_za2removedza2z00zzreduce_betaz00 = 0L;
	extern obj_t BGl_za2brealza2z00zztype_cachez00;
	static BgL_nodez00_bglt BGl_z62nodezd2betaz12za2zzreduce_betaz00(obj_t,
		obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2condition1382z70zzreduce_betaz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2betaz121360za2zzreduce_betaz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzreduce_betaz00(void);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2switch1384z70zzreduce_betaz00(obj_t, obj_t);
	static bool_t BGl_iszd2argumentzf3z21zzreduce_betaz00(BgL_variablez00_bglt,
		obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2boxzd2setz121394zb0zzreduce_betaz00(obj_t, obj_t);
	extern obj_t BGl_za2realza2z00zztype_cachez00;
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2makezd2box1392za2zzreduce_betaz00(obj_t, obj_t);
	static obj_t BGl_z62reducezd2betaz12za2zzreduce_betaz00(obj_t, obj_t);
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	static BgL_nodez00_bglt
		BGl_nodezd2betazd2earlyzd2appz12zc0zzreduce_betaz00(BgL_appz00_bglt);
	extern obj_t BGl_varz00zzast_nodez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2letzd2fun1386za2zzreduce_betaz00(obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2jumpzd2exzd2i1390z70zzreduce_betaz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzreduce_betaz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_occurz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_cfunz00zzast_varz00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2setzd2exzd2it1388z70zzreduce_betaz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2cast1377z70zzreduce_betaz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2sequence1366z70zzreduce_betaz00(obj_t, obj_t);
	static obj_t
		BGl_nodezd2betazd2predicatez12z12zzreduce_betaz00(BgL_appz00_bglt);
	extern obj_t BGl_vrefz00zzast_nodez00;
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	extern obj_t BGl_occurzd2varzd2zzast_occurz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzreduce_betaz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzreduce_betaz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzreduce_betaz00(void);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2setq1380z70zzreduce_betaz00(obj_t, obj_t);
	static obj_t BGl_za2z42fixnumzf3za2zb1zzreduce_betaz00 = BUNSPEC;
	static obj_t BGl_gczd2rootszd2initz00zzreduce_betaz00(void);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2sync1368z70zzreduce_betaz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2app1396z70zzreduce_betaz00(obj_t, obj_t);
	extern bool_t BGl_sidezd2effectzf3z21zzeffect_effectz00(BgL_nodez00_bglt);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_makezd2argszd2listz00zzreduce_betaz00(obj_t, obj_t);
	static bool_t BGl_sidezd2effectzd2safezf3zf3zzreduce_betaz00(obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2letzd2var1364za2zzreduce_betaz00(obj_t, obj_t);
	extern obj_t BGl_za2bintza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_za2z42flonumzf3za2zb1zzreduce_betaz00 = BUNSPEC;
	extern obj_t BGl_literalz00zzast_nodez00;
	extern obj_t BGl_switchz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2funcall1374z70zzreduce_betaz00(obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_findzd2actualzd2expressionz00zzreduce_betaz00(BgL_nodez00_bglt);
	static bool_t BGl_dangerouszf3zf3zzreduce_betaz00(obj_t);
	static obj_t BGl_predicatezf3zf3zzreduce_betaz00(BgL_appz00_bglt);
	static obj_t BGl_za2predicatesza2z00zzreduce_betaz00 = BUNSPEC;
	static bool_t BGl_argumentzf3zf3zzreduce_betaz00(BgL_variablez00_bglt, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_reducezd2betaz12zc0zzreduce_betaz00(obj_t);
	static obj_t __cnst[6];


	   
		 
		DEFINE_STRING(BGl_string2200z00zzreduce_betaz00,
		BgL_bgl_string2200za700za7za7r2202za7,
		"bdb nesting $string-length $flonum? $fixnum? foreign ", 53);
	      DEFINE_STRING(BGl_string2177z00zzreduce_betaz00,
		BgL_bgl_string2177za700za7za7r2203za7, "      simple beta reduction  ", 29);
	      DEFINE_STRING(BGl_string2178z00zzreduce_betaz00,
		BgL_bgl_string2178za700za7za7r2204za7, "(removed: ", 10);
	      DEFINE_STRING(BGl_string2179z00zzreduce_betaz00,
		BgL_bgl_string2179za700za7za7r2205za7, "node-beta-predicate!", 20);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_reducezd2betaz12zd2envz12zzreduce_betaz00,
		BgL_bgl_za762reduceza7d2beta2206z00,
		BGl_z62reducezd2betaz12za2zzreduce_betaz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2180z00zzreduce_betaz00,
		BgL_bgl_string2180za700za7za7r2207za7, "Illegal predicate", 17);
	      DEFINE_STRING(BGl_string2182z00zzreduce_betaz00,
		BgL_bgl_string2182za700za7za7r2208za7, "node-beta!1360", 14);
	      DEFINE_STRING(BGl_string2184z00zzreduce_betaz00,
		BgL_bgl_string2184za700za7za7r2209za7, "node-beta!", 10);
	      DEFINE_STATIC_BGL_GENERIC(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712210za7,
		BGl_z62nodezd2betaz12za2zzreduce_betaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2181z00zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712211za7,
		BGl_z62nodezd2betaz121360za2zzreduce_betaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2183z00zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712212za7,
		BGl_z62nodezd2betaz12zd2letzd2var1364za2zzreduce_betaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2185z00zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712213za7,
		BGl_z62nodezd2betaz12zd2sequence1366z70zzreduce_betaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2186z00zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712214za7,
		BGl_z62nodezd2betaz12zd2sync1368z70zzreduce_betaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2187z00zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712215za7,
		BGl_z62nodezd2betaz12zd2appzd2ly1371za2zzreduce_betaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2188z00zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712216za7,
		BGl_z62nodezd2betaz12zd2funcall1374z70zzreduce_betaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2189z00zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712217za7,
		BGl_z62nodezd2betaz12zd2cast1377z70zzreduce_betaz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2199z00zzreduce_betaz00,
		BgL_bgl_string2199za700za7za7r2218za7, "reduce_beta", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2190z00zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712219za7,
		BGl_z62nodezd2betaz12zd2setq1380z70zzreduce_betaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2191z00zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712220za7,
		BGl_z62nodezd2betaz12zd2condition1382z70zzreduce_betaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2192z00zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712221za7,
		BGl_z62nodezd2betaz12zd2switch1384z70zzreduce_betaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2193z00zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712222za7,
		BGl_z62nodezd2betaz12zd2letzd2fun1386za2zzreduce_betaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2194z00zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712223za7,
		BGl_z62nodezd2betaz12zd2setzd2exzd2it1388z70zzreduce_betaz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2195z00zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712224za7,
		BGl_z62nodezd2betaz12zd2jumpzd2exzd2i1390z70zzreduce_betaz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2196z00zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712225za7,
		BGl_z62nodezd2betaz12zd2makezd2box1392za2zzreduce_betaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2197z00zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712226za7,
		BGl_z62nodezd2betaz12zd2boxzd2setz121394zb0zzreduce_betaz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2198z00zzreduce_betaz00,
		BgL_bgl_za762nodeza7d2betaza712227za7,
		BGl_z62nodezd2betaz12zd2app1396z70zzreduce_betaz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzreduce_betaz00));
		     ADD_ROOT((void *) (&BGl_za2czd2stringzd2lengthza2z00zzreduce_betaz00));
		     ADD_ROOT((void *) (&BGl_za2z42fixnumzf3za2zb1zzreduce_betaz00));
		     ADD_ROOT((void *) (&BGl_za2z42flonumzf3za2zb1zzreduce_betaz00));
		     ADD_ROOT((void *) (&BGl_za2predicatesza2z00zzreduce_betaz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzreduce_betaz00(long
		BgL_checksumz00_4426, char *BgL_fromz00_4427)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzreduce_betaz00))
				{
					BGl_requirezd2initializa7ationz75zzreduce_betaz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzreduce_betaz00();
					BGl_libraryzd2moduleszd2initz00zzreduce_betaz00();
					BGl_cnstzd2initzd2zzreduce_betaz00();
					BGl_importedzd2moduleszd2initz00zzreduce_betaz00();
					BGl_genericzd2initzd2zzreduce_betaz00();
					BGl_methodzd2initzd2zzreduce_betaz00();
					return BGl_toplevelzd2initzd2zzreduce_betaz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzreduce_betaz00(void)
	{
		{	/* Reduce/sbeta.scm 25 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "reduce_beta");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"reduce_beta");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "reduce_beta");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "reduce_beta");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "reduce_beta");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "reduce_beta");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"reduce_beta");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "reduce_beta");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"reduce_beta");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzreduce_betaz00(void)
	{
		{	/* Reduce/sbeta.scm 25 */
			{	/* Reduce/sbeta.scm 25 */
				obj_t BgL_cportz00_4178;

				{	/* Reduce/sbeta.scm 25 */
					obj_t BgL_stringz00_4185;

					BgL_stringz00_4185 = BGl_string2200z00zzreduce_betaz00;
					{	/* Reduce/sbeta.scm 25 */
						obj_t BgL_startz00_4186;

						BgL_startz00_4186 = BINT(0L);
						{	/* Reduce/sbeta.scm 25 */
							obj_t BgL_endz00_4187;

							BgL_endz00_4187 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4185)));
							{	/* Reduce/sbeta.scm 25 */

								BgL_cportz00_4178 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4185, BgL_startz00_4186, BgL_endz00_4187);
				}}}}
				{
					long BgL_iz00_4179;

					BgL_iz00_4179 = 5L;
				BgL_loopz00_4180:
					if ((BgL_iz00_4179 == -1L))
						{	/* Reduce/sbeta.scm 25 */
							return BUNSPEC;
						}
					else
						{	/* Reduce/sbeta.scm 25 */
							{	/* Reduce/sbeta.scm 25 */
								obj_t BgL_arg2201z00_4181;

								{	/* Reduce/sbeta.scm 25 */

									{	/* Reduce/sbeta.scm 25 */
										obj_t BgL_locationz00_4183;

										BgL_locationz00_4183 = BBOOL(((bool_t) 0));
										{	/* Reduce/sbeta.scm 25 */

											BgL_arg2201z00_4181 =
												BGl_readz00zz__readerz00(BgL_cportz00_4178,
												BgL_locationz00_4183);
										}
									}
								}
								{	/* Reduce/sbeta.scm 25 */
									int BgL_tmpz00_4456;

									BgL_tmpz00_4456 = (int) (BgL_iz00_4179);
									CNST_TABLE_SET(BgL_tmpz00_4456, BgL_arg2201z00_4181);
							}}
							{	/* Reduce/sbeta.scm 25 */
								int BgL_auxz00_4184;

								BgL_auxz00_4184 = (int) ((BgL_iz00_4179 - 1L));
								{
									long BgL_iz00_4461;

									BgL_iz00_4461 = (long) (BgL_auxz00_4184);
									BgL_iz00_4179 = BgL_iz00_4461;
									goto BgL_loopz00_4180;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzreduce_betaz00(void)
	{
		{	/* Reduce/sbeta.scm 25 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzreduce_betaz00(void)
	{
		{	/* Reduce/sbeta.scm 25 */
			BGl_za2removedza2z00zzreduce_betaz00 = 0L;
			BGl_za2z42fixnumzf3za2zb1zzreduce_betaz00 = BUNSPEC;
			BGl_za2z42flonumzf3za2zb1zzreduce_betaz00 = BUNSPEC;
			BGl_za2czd2stringzd2lengthza2z00zzreduce_betaz00 = BUNSPEC;
			BGl_za2predicatesza2z00zzreduce_betaz00 = BNIL;
			return BUNSPEC;
		}

	}



/* reduce-beta! */
	BGL_EXPORTED_DEF obj_t BGl_reducezd2betaz12zc0zzreduce_betaz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Reduce/sbeta.scm 47 */
			{	/* Reduce/sbeta.scm 48 */
				obj_t BgL_list1411z00_1482;

				BgL_list1411z00_1482 =
					MAKE_YOUNG_PAIR(BGl_string2177z00zzreduce_betaz00, BNIL);
				BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1411z00_1482);
			}
			BGl_occurzd2varzd2zzast_occurz00(BgL_globalsz00_3);
			BGl_za2removedza2z00zzreduce_betaz00 = 0L;
			{	/* Reduce/sbeta.scm 55 */
				obj_t BgL_list1412z00_1483;

				BgL_list1412z00_1483 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
				BGl_za2z42fixnumzf3za2zb1zzreduce_betaz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(1),
					BgL_list1412z00_1483);
			}
			{	/* Reduce/sbeta.scm 56 */
				obj_t BgL_list1413z00_1484;

				BgL_list1413z00_1484 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
				BGl_za2z42flonumzf3za2zb1zzreduce_betaz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(2),
					BgL_list1413z00_1484);
			}
			{	/* Reduce/sbeta.scm 57 */
				obj_t BgL_list1414z00_1485;

				BgL_list1414z00_1485 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
				BGl_za2czd2stringzd2lengthza2z00zzreduce_betaz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(3),
					BgL_list1414z00_1485);
			}
			{	/* Reduce/sbeta.scm 58 */
				obj_t BgL_list1415z00_1486;

				{	/* Reduce/sbeta.scm 58 */
					obj_t BgL_arg1421z00_1487;

					BgL_arg1421z00_1487 =
						MAKE_YOUNG_PAIR(BGl_za2z42flonumzf3za2zb1zzreduce_betaz00, BNIL);
					BgL_list1415z00_1486 =
						MAKE_YOUNG_PAIR(BGl_za2z42fixnumzf3za2zb1zzreduce_betaz00,
						BgL_arg1421z00_1487);
				}
				BGl_za2predicatesza2z00zzreduce_betaz00 = BgL_list1415z00_1486;
			}
			{
				obj_t BgL_l1276z00_1489;

				BgL_l1276z00_1489 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31422ze3z87_1490:
				if (PAIRP(BgL_l1276z00_1489))
					{	/* Reduce/sbeta.scm 60 */
						{	/* Reduce/sbeta.scm 61 */
							obj_t BgL_globalz00_1492;

							BgL_globalz00_1492 = CAR(BgL_l1276z00_1489);
							{	/* Reduce/sbeta.scm 61 */
								BgL_valuez00_bglt BgL_funz00_1493;

								BgL_funz00_1493 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_1492))))->
									BgL_valuez00);
								{	/* Reduce/sbeta.scm 61 */
									obj_t BgL_nodez00_1494;

									BgL_nodez00_1494 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_funz00_1493)))->BgL_bodyz00);
									{	/* Reduce/sbeta.scm 62 */

										{	/* Reduce/sbeta.scm 63 */
											BgL_nodez00_bglt BgL_arg1434z00_1495;

											BgL_arg1434z00_1495 =
												BGl_nodezd2betaz12zc0zzreduce_betaz00(
												((BgL_nodez00_bglt) BgL_nodez00_1494));
											((((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_funz00_1493)))->
													BgL_bodyz00) =
												((obj_t) ((obj_t) BgL_arg1434z00_1495)), BUNSPEC);
										}
										BUNSPEC;
									}
								}
							}
						}
						{
							obj_t BgL_l1276z00_4495;

							BgL_l1276z00_4495 = CDR(BgL_l1276z00_1489);
							BgL_l1276z00_1489 = BgL_l1276z00_4495;
							goto BgL_zc3z04anonymousza31422ze3z87_1490;
						}
					}
				else
					{	/* Reduce/sbeta.scm 60 */
						((bool_t) 1);
					}
			}
			BGl_za2z42fixnumzf3za2zb1zzreduce_betaz00 = BUNSPEC;
			BGl_za2z42flonumzf3za2zb1zzreduce_betaz00 = BUNSPEC;
			BGl_za2czd2stringzd2lengthza2z00zzreduce_betaz00 = BUNSPEC;
			BGl_za2predicatesza2z00zzreduce_betaz00 = BNIL;
			{	/* Reduce/sbeta.scm 72 */
				obj_t BgL_list1438z00_1498;

				{	/* Reduce/sbeta.scm 72 */
					obj_t BgL_arg1448z00_1499;

					{	/* Reduce/sbeta.scm 72 */
						obj_t BgL_arg1453z00_1500;

						{	/* Reduce/sbeta.scm 72 */
							obj_t BgL_arg1454z00_1501;

							BgL_arg1454z00_1501 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
							BgL_arg1453z00_1500 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ')')),
								BgL_arg1454z00_1501);
						}
						BgL_arg1448z00_1499 =
							MAKE_YOUNG_PAIR(BINT(BGl_za2removedza2z00zzreduce_betaz00),
							BgL_arg1453z00_1500);
					}
					BgL_list1438z00_1498 =
						MAKE_YOUNG_PAIR(BGl_string2178z00zzreduce_betaz00,
						BgL_arg1448z00_1499);
				}
				BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1438z00_1498);
			}
			return BgL_globalsz00_3;
		}

	}



/* &reduce-beta! */
	obj_t BGl_z62reducezd2betaz12za2zzreduce_betaz00(obj_t BgL_envz00_4126,
		obj_t BgL_globalsz00_4127)
	{
		{	/* Reduce/sbeta.scm 47 */
			return BGl_reducezd2betaz12zc0zzreduce_betaz00(BgL_globalsz00_4127);
		}

	}



/* find-actual-expression */
	BgL_nodez00_bglt
		BGl_findzd2actualzd2expressionz00zzreduce_betaz00(BgL_nodez00_bglt
		BgL_bodyz00_5)
	{
		{	/* Reduce/sbeta.scm 97 */
		BGl_findzd2actualzd2expressionz00zzreduce_betaz00:
			{	/* Reduce/sbeta.scm 98 */
				bool_t BgL_test2231z00_4507;

				{	/* Reduce/sbeta.scm 98 */
					obj_t BgL_classz00_2407;

					BgL_classz00_2407 = BGl_sequencez00zzast_nodez00;
					{	/* Reduce/sbeta.scm 98 */
						BgL_objectz00_bglt BgL_arg1807z00_2409;

						{	/* Reduce/sbeta.scm 98 */
							obj_t BgL_tmpz00_4508;

							BgL_tmpz00_4508 = ((obj_t) BgL_bodyz00_5);
							BgL_arg1807z00_2409 = (BgL_objectz00_bglt) (BgL_tmpz00_4508);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Reduce/sbeta.scm 98 */
								long BgL_idxz00_2415;

								BgL_idxz00_2415 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2409);
								BgL_test2231z00_4507 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2415 + 3L)) == BgL_classz00_2407);
							}
						else
							{	/* Reduce/sbeta.scm 98 */
								bool_t BgL_res2128z00_2440;

								{	/* Reduce/sbeta.scm 98 */
									obj_t BgL_oclassz00_2423;

									{	/* Reduce/sbeta.scm 98 */
										obj_t BgL_arg1815z00_2431;
										long BgL_arg1816z00_2432;

										BgL_arg1815z00_2431 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Reduce/sbeta.scm 98 */
											long BgL_arg1817z00_2433;

											BgL_arg1817z00_2433 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2409);
											BgL_arg1816z00_2432 = (BgL_arg1817z00_2433 - OBJECT_TYPE);
										}
										BgL_oclassz00_2423 =
											VECTOR_REF(BgL_arg1815z00_2431, BgL_arg1816z00_2432);
									}
									{	/* Reduce/sbeta.scm 98 */
										bool_t BgL__ortest_1115z00_2424;

										BgL__ortest_1115z00_2424 =
											(BgL_classz00_2407 == BgL_oclassz00_2423);
										if (BgL__ortest_1115z00_2424)
											{	/* Reduce/sbeta.scm 98 */
												BgL_res2128z00_2440 = BgL__ortest_1115z00_2424;
											}
										else
											{	/* Reduce/sbeta.scm 98 */
												long BgL_odepthz00_2425;

												{	/* Reduce/sbeta.scm 98 */
													obj_t BgL_arg1804z00_2426;

													BgL_arg1804z00_2426 = (BgL_oclassz00_2423);
													BgL_odepthz00_2425 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2426);
												}
												if ((3L < BgL_odepthz00_2425))
													{	/* Reduce/sbeta.scm 98 */
														obj_t BgL_arg1802z00_2428;

														{	/* Reduce/sbeta.scm 98 */
															obj_t BgL_arg1803z00_2429;

															BgL_arg1803z00_2429 = (BgL_oclassz00_2423);
															BgL_arg1802z00_2428 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2429,
																3L);
														}
														BgL_res2128z00_2440 =
															(BgL_arg1802z00_2428 == BgL_classz00_2407);
													}
												else
													{	/* Reduce/sbeta.scm 98 */
														BgL_res2128z00_2440 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test2231z00_4507 = BgL_res2128z00_2440;
							}
					}
				}
				if (BgL_test2231z00_4507)
					{	/* Reduce/sbeta.scm 100 */
						bool_t BgL_test2235z00_4530;

						if (NULLP(
								(((BgL_sequencez00_bglt) COBJECT(
											((BgL_sequencez00_bglt) BgL_bodyz00_5)))->BgL_nodesz00)))
							{	/* Reduce/sbeta.scm 100 */
								BgL_test2235z00_4530 = ((bool_t) 0);
							}
						else
							{	/* Reduce/sbeta.scm 100 */
								bool_t BgL_test2237z00_4535;

								{	/* Reduce/sbeta.scm 100 */
									obj_t BgL_tmpz00_4536;

									{	/* Reduce/sbeta.scm 100 */
										obj_t BgL_pairz00_2441;

										BgL_pairz00_2441 =
											(((BgL_sequencez00_bglt) COBJECT(
													((BgL_sequencez00_bglt) BgL_bodyz00_5)))->
											BgL_nodesz00);
										BgL_tmpz00_4536 = CDR(BgL_pairz00_2441);
									}
									BgL_test2237z00_4535 = NULLP(BgL_tmpz00_4536);
								}
								if (BgL_test2237z00_4535)
									{	/* Reduce/sbeta.scm 100 */
										BgL_test2235z00_4530 =
											NULLP(
											(((BgL_sequencez00_bglt) COBJECT(
														((BgL_sequencez00_bglt) BgL_bodyz00_5)))->
												BgL_metaz00));
									}
								else
									{	/* Reduce/sbeta.scm 100 */
										BgL_test2235z00_4530 = ((bool_t) 0);
									}
							}
						if (BgL_test2235z00_4530)
							{	/* Reduce/sbeta.scm 101 */
								obj_t BgL_arg1516z00_1514;

								{	/* Reduce/sbeta.scm 101 */
									obj_t BgL_pairz00_2442;

									BgL_pairz00_2442 =
										(((BgL_sequencez00_bglt) COBJECT(
												((BgL_sequencez00_bglt) BgL_bodyz00_5)))->BgL_nodesz00);
									BgL_arg1516z00_1514 = CAR(BgL_pairz00_2442);
								}
								{
									BgL_nodez00_bglt BgL_bodyz00_4547;

									BgL_bodyz00_4547 = ((BgL_nodez00_bglt) BgL_arg1516z00_1514);
									BgL_bodyz00_5 = BgL_bodyz00_4547;
									goto BGl_findzd2actualzd2expressionz00zzreduce_betaz00;
								}
							}
						else
							{	/* Reduce/sbeta.scm 100 */
								return BgL_bodyz00_5;
							}
					}
				else
					{	/* Reduce/sbeta.scm 98 */
						return BgL_bodyz00_5;
					}
			}
		}

	}



/* is-argument? */
	bool_t BGl_iszd2argumentzf3z21zzreduce_betaz00(BgL_variablez00_bglt
		BgL_varz00_6, obj_t BgL_bodyz00_7)
	{
		{	/* Reduce/sbeta.scm 108 */
			{	/* Reduce/sbeta.scm 110 */
				bool_t BgL_test2238z00_4549;

				{	/* Reduce/sbeta.scm 207 */
					bool_t BgL_test2239z00_4550;

					{	/* Reduce/sbeta.scm 207 */
						obj_t BgL_classz00_2447;

						BgL_classz00_2447 = BGl_varz00zzast_nodez00;
						{	/* Reduce/sbeta.scm 207 */
							BgL_objectz00_bglt BgL_arg1807z00_2449;

							{	/* Reduce/sbeta.scm 207 */
								obj_t BgL_tmpz00_4551;

								BgL_tmpz00_4551 = ((obj_t) ((BgL_nodez00_bglt) BgL_bodyz00_7));
								BgL_arg1807z00_2449 = (BgL_objectz00_bglt) (BgL_tmpz00_4551);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Reduce/sbeta.scm 207 */
									long BgL_idxz00_2455;

									BgL_idxz00_2455 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2449);
									BgL_test2239z00_4550 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2455 + 2L)) == BgL_classz00_2447);
								}
							else
								{	/* Reduce/sbeta.scm 207 */
									bool_t BgL_res2129z00_2480;

									{	/* Reduce/sbeta.scm 207 */
										obj_t BgL_oclassz00_2463;

										{	/* Reduce/sbeta.scm 207 */
											obj_t BgL_arg1815z00_2471;
											long BgL_arg1816z00_2472;

											BgL_arg1815z00_2471 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Reduce/sbeta.scm 207 */
												long BgL_arg1817z00_2473;

												BgL_arg1817z00_2473 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2449);
												BgL_arg1816z00_2472 =
													(BgL_arg1817z00_2473 - OBJECT_TYPE);
											}
											BgL_oclassz00_2463 =
												VECTOR_REF(BgL_arg1815z00_2471, BgL_arg1816z00_2472);
										}
										{	/* Reduce/sbeta.scm 207 */
											bool_t BgL__ortest_1115z00_2464;

											BgL__ortest_1115z00_2464 =
												(BgL_classz00_2447 == BgL_oclassz00_2463);
											if (BgL__ortest_1115z00_2464)
												{	/* Reduce/sbeta.scm 207 */
													BgL_res2129z00_2480 = BgL__ortest_1115z00_2464;
												}
											else
												{	/* Reduce/sbeta.scm 207 */
													long BgL_odepthz00_2465;

													{	/* Reduce/sbeta.scm 207 */
														obj_t BgL_arg1804z00_2466;

														BgL_arg1804z00_2466 = (BgL_oclassz00_2463);
														BgL_odepthz00_2465 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2466);
													}
													if ((2L < BgL_odepthz00_2465))
														{	/* Reduce/sbeta.scm 207 */
															obj_t BgL_arg1802z00_2468;

															{	/* Reduce/sbeta.scm 207 */
																obj_t BgL_arg1803z00_2469;

																BgL_arg1803z00_2469 = (BgL_oclassz00_2463);
																BgL_arg1802z00_2468 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2469,
																	2L);
															}
															BgL_res2129z00_2480 =
																(BgL_arg1802z00_2468 == BgL_classz00_2447);
														}
													else
														{	/* Reduce/sbeta.scm 207 */
															BgL_res2129z00_2480 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2239z00_4550 = BgL_res2129z00_2480;
								}
						}
					}
					if (BgL_test2239z00_4550)
						{	/* Reduce/sbeta.scm 207 */
							BgL_test2238z00_4549 =
								(
								((obj_t)
									(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt)
													((BgL_nodez00_bglt) BgL_bodyz00_7))))->
										BgL_variablez00)) == ((obj_t) BgL_varz00_6));
						}
					else
						{	/* Reduce/sbeta.scm 207 */
							BgL_test2238z00_4549 = ((bool_t) 0);
						}
				}
				if (BgL_test2238z00_4549)
					{	/* Reduce/sbeta.scm 110 */
						return ((bool_t) 1);
					}
				else
					{	/* Reduce/sbeta.scm 112 */
						bool_t BgL_test2243z00_4580;

						{	/* Reduce/sbeta.scm 112 */
							obj_t BgL_classz00_2482;

							BgL_classz00_2482 = BGl_castz00zzast_nodez00;
							if (BGL_OBJECTP(BgL_bodyz00_7))
								{	/* Reduce/sbeta.scm 112 */
									BgL_objectz00_bglt BgL_arg1807z00_2484;

									BgL_arg1807z00_2484 = (BgL_objectz00_bglt) (BgL_bodyz00_7);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Reduce/sbeta.scm 112 */
											long BgL_idxz00_2490;

											BgL_idxz00_2490 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2484);
											BgL_test2243z00_4580 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2490 + 2L)) == BgL_classz00_2482);
										}
									else
										{	/* Reduce/sbeta.scm 112 */
											bool_t BgL_res2130z00_2515;

											{	/* Reduce/sbeta.scm 112 */
												obj_t BgL_oclassz00_2498;

												{	/* Reduce/sbeta.scm 112 */
													obj_t BgL_arg1815z00_2506;
													long BgL_arg1816z00_2507;

													BgL_arg1815z00_2506 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Reduce/sbeta.scm 112 */
														long BgL_arg1817z00_2508;

														BgL_arg1817z00_2508 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2484);
														BgL_arg1816z00_2507 =
															(BgL_arg1817z00_2508 - OBJECT_TYPE);
													}
													BgL_oclassz00_2498 =
														VECTOR_REF(BgL_arg1815z00_2506,
														BgL_arg1816z00_2507);
												}
												{	/* Reduce/sbeta.scm 112 */
													bool_t BgL__ortest_1115z00_2499;

													BgL__ortest_1115z00_2499 =
														(BgL_classz00_2482 == BgL_oclassz00_2498);
													if (BgL__ortest_1115z00_2499)
														{	/* Reduce/sbeta.scm 112 */
															BgL_res2130z00_2515 = BgL__ortest_1115z00_2499;
														}
													else
														{	/* Reduce/sbeta.scm 112 */
															long BgL_odepthz00_2500;

															{	/* Reduce/sbeta.scm 112 */
																obj_t BgL_arg1804z00_2501;

																BgL_arg1804z00_2501 = (BgL_oclassz00_2498);
																BgL_odepthz00_2500 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2501);
															}
															if ((2L < BgL_odepthz00_2500))
																{	/* Reduce/sbeta.scm 112 */
																	obj_t BgL_arg1802z00_2503;

																	{	/* Reduce/sbeta.scm 112 */
																		obj_t BgL_arg1803z00_2504;

																		BgL_arg1803z00_2504 = (BgL_oclassz00_2498);
																		BgL_arg1802z00_2503 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2504, 2L);
																	}
																	BgL_res2130z00_2515 =
																		(BgL_arg1802z00_2503 == BgL_classz00_2482);
																}
															else
																{	/* Reduce/sbeta.scm 112 */
																	BgL_res2130z00_2515 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2243z00_4580 = BgL_res2130z00_2515;
										}
								}
							else
								{	/* Reduce/sbeta.scm 112 */
									BgL_test2243z00_4580 = ((bool_t) 0);
								}
						}
						if (BgL_test2243z00_4580)
							{	/* Reduce/sbeta.scm 114 */
								BgL_nodez00_bglt BgL_nodez00_2517;

								BgL_nodez00_2517 =
									(((BgL_castz00_bglt) COBJECT(
											((BgL_castz00_bglt) BgL_bodyz00_7)))->BgL_argz00);
								{	/* Reduce/sbeta.scm 207 */
									bool_t BgL_test2248z00_4605;

									{	/* Reduce/sbeta.scm 207 */
										obj_t BgL_classz00_2520;

										BgL_classz00_2520 = BGl_varz00zzast_nodez00;
										{	/* Reduce/sbeta.scm 207 */
											BgL_objectz00_bglt BgL_arg1807z00_2522;

											{	/* Reduce/sbeta.scm 207 */
												obj_t BgL_tmpz00_4606;

												BgL_tmpz00_4606 = ((obj_t) BgL_nodez00_2517);
												BgL_arg1807z00_2522 =
													(BgL_objectz00_bglt) (BgL_tmpz00_4606);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Reduce/sbeta.scm 207 */
													long BgL_idxz00_2528;

													BgL_idxz00_2528 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2522);
													BgL_test2248z00_4605 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2528 + 2L)) == BgL_classz00_2520);
												}
											else
												{	/* Reduce/sbeta.scm 207 */
													bool_t BgL_res2131z00_2553;

													{	/* Reduce/sbeta.scm 207 */
														obj_t BgL_oclassz00_2536;

														{	/* Reduce/sbeta.scm 207 */
															obj_t BgL_arg1815z00_2544;
															long BgL_arg1816z00_2545;

															BgL_arg1815z00_2544 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Reduce/sbeta.scm 207 */
																long BgL_arg1817z00_2546;

																BgL_arg1817z00_2546 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2522);
																BgL_arg1816z00_2545 =
																	(BgL_arg1817z00_2546 - OBJECT_TYPE);
															}
															BgL_oclassz00_2536 =
																VECTOR_REF(BgL_arg1815z00_2544,
																BgL_arg1816z00_2545);
														}
														{	/* Reduce/sbeta.scm 207 */
															bool_t BgL__ortest_1115z00_2537;

															BgL__ortest_1115z00_2537 =
																(BgL_classz00_2520 == BgL_oclassz00_2536);
															if (BgL__ortest_1115z00_2537)
																{	/* Reduce/sbeta.scm 207 */
																	BgL_res2131z00_2553 =
																		BgL__ortest_1115z00_2537;
																}
															else
																{	/* Reduce/sbeta.scm 207 */
																	long BgL_odepthz00_2538;

																	{	/* Reduce/sbeta.scm 207 */
																		obj_t BgL_arg1804z00_2539;

																		BgL_arg1804z00_2539 = (BgL_oclassz00_2536);
																		BgL_odepthz00_2538 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2539);
																	}
																	if ((2L < BgL_odepthz00_2538))
																		{	/* Reduce/sbeta.scm 207 */
																			obj_t BgL_arg1802z00_2541;

																			{	/* Reduce/sbeta.scm 207 */
																				obj_t BgL_arg1803z00_2542;

																				BgL_arg1803z00_2542 =
																					(BgL_oclassz00_2536);
																				BgL_arg1802z00_2541 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2542, 2L);
																			}
																			BgL_res2131z00_2553 =
																				(BgL_arg1802z00_2541 ==
																				BgL_classz00_2520);
																		}
																	else
																		{	/* Reduce/sbeta.scm 207 */
																			BgL_res2131z00_2553 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2248z00_4605 = BgL_res2131z00_2553;
												}
										}
									}
									if (BgL_test2248z00_4605)
										{	/* Reduce/sbeta.scm 207 */
											return
												(
												((obj_t)
													(((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_nodez00_2517)))->
														BgL_variablez00)) == ((obj_t) BgL_varz00_6));
										}
									else
										{	/* Reduce/sbeta.scm 207 */
											return ((bool_t) 0);
										}
								}
							}
						else
							{	/* Reduce/sbeta.scm 112 */
								return ((bool_t) 0);
							}
					}
			}
		}

	}



/* argument? */
	bool_t BGl_argumentzf3zf3zzreduce_betaz00(BgL_variablez00_bglt BgL_varz00_8,
		obj_t BgL_argsz00_9)
	{
		{	/* Reduce/sbeta.scm 121 */
			{
				obj_t BgL_argsz00_1527;

				BgL_argsz00_1527 = BgL_argsz00_9;
			BgL_zc3z04anonymousza31560ze3z87_1528:
				if (NULLP(BgL_argsz00_1527))
					{	/* Reduce/sbeta.scm 124 */
						return ((bool_t) 0);
					}
				else
					{	/* Reduce/sbeta.scm 126 */
						bool_t BgL_test2253z00_4635;

						{	/* Reduce/sbeta.scm 126 */
							obj_t BgL_arg1573z00_1533;

							BgL_arg1573z00_1533 = CAR(((obj_t) BgL_argsz00_1527));
							BgL_test2253z00_4635 =
								BGl_iszd2argumentzf3z21zzreduce_betaz00(BgL_varz00_8,
								BgL_arg1573z00_1533);
						}
						if (BgL_test2253z00_4635)
							{	/* Reduce/sbeta.scm 126 */
								return ((bool_t) 1);
							}
						else
							{	/* Reduce/sbeta.scm 129 */
								obj_t BgL_arg1571z00_1532;

								BgL_arg1571z00_1532 = CDR(((obj_t) BgL_argsz00_1527));
								{
									obj_t BgL_argsz00_4641;

									BgL_argsz00_4641 = BgL_arg1571z00_1532;
									BgL_argsz00_1527 = BgL_argsz00_4641;
									goto BgL_zc3z04anonymousza31560ze3z87_1528;
								}
							}
					}
			}
		}

	}



/* make-args-list */
	obj_t BGl_makezd2argszd2listz00zzreduce_betaz00(obj_t BgL_bindingsz00_10,
		obj_t BgL_argsz00_11)
	{
		{	/* Reduce/sbeta.scm 134 */
			if (NULLP(BgL_argsz00_11))
				{	/* Reduce/sbeta.scm 135 */
					return BNIL;
				}
			else
				{	/* Reduce/sbeta.scm 135 */
					obj_t BgL_head1280z00_1537;

					BgL_head1280z00_1537 = MAKE_YOUNG_PAIR(BNIL, BNIL);
					{
						obj_t BgL_l1278z00_1539;
						obj_t BgL_tail1281z00_1540;

						BgL_l1278z00_1539 = BgL_argsz00_11;
						BgL_tail1281z00_1540 = BgL_head1280z00_1537;
					BgL_zc3z04anonymousza31575ze3z87_1541:
						if (NULLP(BgL_l1278z00_1539))
							{	/* Reduce/sbeta.scm 135 */
								return CDR(BgL_head1280z00_1537);
							}
						else
							{	/* Reduce/sbeta.scm 135 */
								obj_t BgL_newtail1282z00_1543;

								{	/* Reduce/sbeta.scm 135 */
									obj_t BgL_arg1585z00_1545;

									{	/* Reduce/sbeta.scm 135 */
										obj_t BgL_az00_1546;

										BgL_az00_1546 = CAR(((obj_t) BgL_l1278z00_1539));
										{
											obj_t BgL_bindingsz00_1548;

											BgL_bindingsz00_1548 = BgL_bindingsz00_10;
										BgL_zc3z04anonymousza31586ze3z87_1549:
											if (NULLP(BgL_bindingsz00_1548))
												{	/* Reduce/sbeta.scm 138 */
													BgL_arg1585z00_1545 = BgL_az00_1546;
												}
											else
												{	/* Reduce/sbeta.scm 140 */
													bool_t BgL_test2257z00_4652;

													{	/* Reduce/sbeta.scm 140 */
														obj_t BgL_arg1594z00_1554;

														{	/* Reduce/sbeta.scm 140 */
															obj_t BgL_pairz00_2562;

															BgL_pairz00_2562 =
																CAR(((obj_t) BgL_bindingsz00_1548));
															BgL_arg1594z00_1554 = CAR(BgL_pairz00_2562);
														}
														BgL_test2257z00_4652 =
															BGl_iszd2argumentzf3z21zzreduce_betaz00(
															((BgL_variablez00_bglt) BgL_arg1594z00_1554),
															BgL_az00_1546);
													}
													if (BgL_test2257z00_4652)
														{	/* Reduce/sbeta.scm 141 */
															obj_t BgL_pairz00_2566;

															BgL_pairz00_2566 =
																CAR(((obj_t) BgL_bindingsz00_1548));
															BgL_arg1585z00_1545 = CDR(BgL_pairz00_2566);
														}
													else
														{	/* Reduce/sbeta.scm 143 */
															obj_t BgL_arg1593z00_1553;

															BgL_arg1593z00_1553 =
																CDR(((obj_t) BgL_bindingsz00_1548));
															{
																obj_t BgL_bindingsz00_4663;

																BgL_bindingsz00_4663 = BgL_arg1593z00_1553;
																BgL_bindingsz00_1548 = BgL_bindingsz00_4663;
																goto BgL_zc3z04anonymousza31586ze3z87_1549;
															}
														}
												}
										}
									}
									BgL_newtail1282z00_1543 =
										MAKE_YOUNG_PAIR(BgL_arg1585z00_1545, BNIL);
								}
								SET_CDR(BgL_tail1281z00_1540, BgL_newtail1282z00_1543);
								{	/* Reduce/sbeta.scm 135 */
									obj_t BgL_arg1584z00_1544;

									BgL_arg1584z00_1544 = CDR(((obj_t) BgL_l1278z00_1539));
									{
										obj_t BgL_tail1281z00_4669;
										obj_t BgL_l1278z00_4668;

										BgL_l1278z00_4668 = BgL_arg1584z00_1544;
										BgL_tail1281z00_4669 = BgL_newtail1282z00_1543;
										BgL_tail1281z00_1540 = BgL_tail1281z00_4669;
										BgL_l1278z00_1539 = BgL_l1278z00_4668;
										goto BgL_zc3z04anonymousza31575ze3z87_1541;
									}
								}
							}
					}
				}
		}

	}



/* dangerous? */
	bool_t BGl_dangerouszf3zf3zzreduce_betaz00(obj_t BgL_exprz00_12)
	{
		{	/* Reduce/sbeta.scm 149 */
			{	/* Reduce/sbeta.scm 150 */
				BgL_nodez00_bglt BgL_exprz00_1557;

				BgL_exprz00_1557 =
					BGl_findzd2actualzd2expressionz00zzreduce_betaz00(
					((BgL_nodez00_bglt) BgL_exprz00_12));
				{	/* Reduce/sbeta.scm 152 */
					bool_t BgL_test2258z00_4672;

					{	/* Reduce/sbeta.scm 152 */
						bool_t BgL_test2259z00_4673;

						{	/* Reduce/sbeta.scm 152 */
							obj_t BgL_classz00_2570;

							BgL_classz00_2570 = BGl_varz00zzast_nodez00;
							{	/* Reduce/sbeta.scm 152 */
								BgL_objectz00_bglt BgL_arg1807z00_2572;

								{	/* Reduce/sbeta.scm 152 */
									obj_t BgL_tmpz00_4674;

									BgL_tmpz00_4674 =
										((obj_t) ((BgL_objectz00_bglt) BgL_exprz00_1557));
									BgL_arg1807z00_2572 = (BgL_objectz00_bglt) (BgL_tmpz00_4674);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Reduce/sbeta.scm 152 */
										long BgL_idxz00_2578;

										BgL_idxz00_2578 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2572);
										BgL_test2259z00_4673 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2578 + 2L)) == BgL_classz00_2570);
									}
								else
									{	/* Reduce/sbeta.scm 152 */
										bool_t BgL_res2132z00_2603;

										{	/* Reduce/sbeta.scm 152 */
											obj_t BgL_oclassz00_2586;

											{	/* Reduce/sbeta.scm 152 */
												obj_t BgL_arg1815z00_2594;
												long BgL_arg1816z00_2595;

												BgL_arg1815z00_2594 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Reduce/sbeta.scm 152 */
													long BgL_arg1817z00_2596;

													BgL_arg1817z00_2596 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2572);
													BgL_arg1816z00_2595 =
														(BgL_arg1817z00_2596 - OBJECT_TYPE);
												}
												BgL_oclassz00_2586 =
													VECTOR_REF(BgL_arg1815z00_2594, BgL_arg1816z00_2595);
											}
											{	/* Reduce/sbeta.scm 152 */
												bool_t BgL__ortest_1115z00_2587;

												BgL__ortest_1115z00_2587 =
													(BgL_classz00_2570 == BgL_oclassz00_2586);
												if (BgL__ortest_1115z00_2587)
													{	/* Reduce/sbeta.scm 152 */
														BgL_res2132z00_2603 = BgL__ortest_1115z00_2587;
													}
												else
													{	/* Reduce/sbeta.scm 152 */
														long BgL_odepthz00_2588;

														{	/* Reduce/sbeta.scm 152 */
															obj_t BgL_arg1804z00_2589;

															BgL_arg1804z00_2589 = (BgL_oclassz00_2586);
															BgL_odepthz00_2588 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2589);
														}
														if ((2L < BgL_odepthz00_2588))
															{	/* Reduce/sbeta.scm 152 */
																obj_t BgL_arg1802z00_2591;

																{	/* Reduce/sbeta.scm 152 */
																	obj_t BgL_arg1803z00_2592;

																	BgL_arg1803z00_2592 = (BgL_oclassz00_2586);
																	BgL_arg1802z00_2591 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2592,
																		2L);
																}
																BgL_res2132z00_2603 =
																	(BgL_arg1802z00_2591 == BgL_classz00_2570);
															}
														else
															{	/* Reduce/sbeta.scm 152 */
																BgL_res2132z00_2603 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2259z00_4673 = BgL_res2132z00_2603;
									}
							}
						}
						if (BgL_test2259z00_4673)
							{	/* Reduce/sbeta.scm 152 */
								BgL_test2258z00_4672 = ((bool_t) 1);
							}
						else
							{	/* Reduce/sbeta.scm 152 */
								bool_t BgL_test2263z00_4697;

								{	/* Reduce/sbeta.scm 152 */
									obj_t BgL_classz00_2604;

									BgL_classz00_2604 = BGl_atomz00zzast_nodez00;
									{	/* Reduce/sbeta.scm 152 */
										BgL_objectz00_bglt BgL_arg1807z00_2606;

										{	/* Reduce/sbeta.scm 152 */
											obj_t BgL_tmpz00_4698;

											BgL_tmpz00_4698 =
												((obj_t) ((BgL_objectz00_bglt) BgL_exprz00_1557));
											BgL_arg1807z00_2606 =
												(BgL_objectz00_bglt) (BgL_tmpz00_4698);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Reduce/sbeta.scm 152 */
												long BgL_idxz00_2612;

												BgL_idxz00_2612 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2606);
												BgL_test2263z00_4697 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2612 + 2L)) == BgL_classz00_2604);
											}
										else
											{	/* Reduce/sbeta.scm 152 */
												bool_t BgL_res2133z00_2637;

												{	/* Reduce/sbeta.scm 152 */
													obj_t BgL_oclassz00_2620;

													{	/* Reduce/sbeta.scm 152 */
														obj_t BgL_arg1815z00_2628;
														long BgL_arg1816z00_2629;

														BgL_arg1815z00_2628 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Reduce/sbeta.scm 152 */
															long BgL_arg1817z00_2630;

															BgL_arg1817z00_2630 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2606);
															BgL_arg1816z00_2629 =
																(BgL_arg1817z00_2630 - OBJECT_TYPE);
														}
														BgL_oclassz00_2620 =
															VECTOR_REF(BgL_arg1815z00_2628,
															BgL_arg1816z00_2629);
													}
													{	/* Reduce/sbeta.scm 152 */
														bool_t BgL__ortest_1115z00_2621;

														BgL__ortest_1115z00_2621 =
															(BgL_classz00_2604 == BgL_oclassz00_2620);
														if (BgL__ortest_1115z00_2621)
															{	/* Reduce/sbeta.scm 152 */
																BgL_res2133z00_2637 = BgL__ortest_1115z00_2621;
															}
														else
															{	/* Reduce/sbeta.scm 152 */
																long BgL_odepthz00_2622;

																{	/* Reduce/sbeta.scm 152 */
																	obj_t BgL_arg1804z00_2623;

																	BgL_arg1804z00_2623 = (BgL_oclassz00_2620);
																	BgL_odepthz00_2622 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2623);
																}
																if ((2L < BgL_odepthz00_2622))
																	{	/* Reduce/sbeta.scm 152 */
																		obj_t BgL_arg1802z00_2625;

																		{	/* Reduce/sbeta.scm 152 */
																			obj_t BgL_arg1803z00_2626;

																			BgL_arg1803z00_2626 =
																				(BgL_oclassz00_2620);
																			BgL_arg1802z00_2625 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2626, 2L);
																		}
																		BgL_res2133z00_2637 =
																			(BgL_arg1802z00_2625 ==
																			BgL_classz00_2604);
																	}
																else
																	{	/* Reduce/sbeta.scm 152 */
																		BgL_res2133z00_2637 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2263z00_4697 = BgL_res2133z00_2637;
											}
									}
								}
								if (BgL_test2263z00_4697)
									{	/* Reduce/sbeta.scm 152 */
										BgL_test2258z00_4672 = ((bool_t) 1);
									}
								else
									{	/* Reduce/sbeta.scm 152 */
										obj_t BgL_classz00_2638;

										BgL_classz00_2638 = BGl_kwotez00zzast_nodez00;
										{	/* Reduce/sbeta.scm 152 */
											BgL_objectz00_bglt BgL_arg1807z00_2640;

											{	/* Reduce/sbeta.scm 152 */
												obj_t BgL_tmpz00_4721;

												BgL_tmpz00_4721 =
													((obj_t) ((BgL_objectz00_bglt) BgL_exprz00_1557));
												BgL_arg1807z00_2640 =
													(BgL_objectz00_bglt) (BgL_tmpz00_4721);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Reduce/sbeta.scm 152 */
													long BgL_idxz00_2646;

													BgL_idxz00_2646 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2640);
													BgL_test2258z00_4672 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2646 + 2L)) == BgL_classz00_2638);
												}
											else
												{	/* Reduce/sbeta.scm 152 */
													bool_t BgL_res2134z00_2671;

													{	/* Reduce/sbeta.scm 152 */
														obj_t BgL_oclassz00_2654;

														{	/* Reduce/sbeta.scm 152 */
															obj_t BgL_arg1815z00_2662;
															long BgL_arg1816z00_2663;

															BgL_arg1815z00_2662 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Reduce/sbeta.scm 152 */
																long BgL_arg1817z00_2664;

																BgL_arg1817z00_2664 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2640);
																BgL_arg1816z00_2663 =
																	(BgL_arg1817z00_2664 - OBJECT_TYPE);
															}
															BgL_oclassz00_2654 =
																VECTOR_REF(BgL_arg1815z00_2662,
																BgL_arg1816z00_2663);
														}
														{	/* Reduce/sbeta.scm 152 */
															bool_t BgL__ortest_1115z00_2655;

															BgL__ortest_1115z00_2655 =
																(BgL_classz00_2638 == BgL_oclassz00_2654);
															if (BgL__ortest_1115z00_2655)
																{	/* Reduce/sbeta.scm 152 */
																	BgL_res2134z00_2671 =
																		BgL__ortest_1115z00_2655;
																}
															else
																{	/* Reduce/sbeta.scm 152 */
																	long BgL_odepthz00_2656;

																	{	/* Reduce/sbeta.scm 152 */
																		obj_t BgL_arg1804z00_2657;

																		BgL_arg1804z00_2657 = (BgL_oclassz00_2654);
																		BgL_odepthz00_2656 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2657);
																	}
																	if ((2L < BgL_odepthz00_2656))
																		{	/* Reduce/sbeta.scm 152 */
																			obj_t BgL_arg1802z00_2659;

																			{	/* Reduce/sbeta.scm 152 */
																				obj_t BgL_arg1803z00_2660;

																				BgL_arg1803z00_2660 =
																					(BgL_oclassz00_2654);
																				BgL_arg1802z00_2659 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2660, 2L);
																			}
																			BgL_res2134z00_2671 =
																				(BgL_arg1802z00_2659 ==
																				BgL_classz00_2638);
																		}
																	else
																		{	/* Reduce/sbeta.scm 152 */
																			BgL_res2134z00_2671 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2258z00_4672 = BgL_res2134z00_2671;
												}
										}
									}
							}
					}
					if (BgL_test2258z00_4672)
						{	/* Reduce/sbeta.scm 152 */
							return ((bool_t) 0);
						}
					else
						{	/* Reduce/sbeta.scm 154 */
							bool_t BgL_test2270z00_4744;

							{	/* Reduce/sbeta.scm 154 */
								obj_t BgL_classz00_2672;

								BgL_classz00_2672 = BGl_vrefz00zzast_nodez00;
								{	/* Reduce/sbeta.scm 154 */
									BgL_objectz00_bglt BgL_arg1807z00_2674;

									{	/* Reduce/sbeta.scm 154 */
										obj_t BgL_tmpz00_4745;

										BgL_tmpz00_4745 =
											((obj_t) ((BgL_objectz00_bglt) BgL_exprz00_1557));
										BgL_arg1807z00_2674 =
											(BgL_objectz00_bglt) (BgL_tmpz00_4745);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Reduce/sbeta.scm 154 */
											long BgL_idxz00_2680;

											BgL_idxz00_2680 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2674);
											BgL_test2270z00_4744 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2680 + 5L)) == BgL_classz00_2672);
										}
									else
										{	/* Reduce/sbeta.scm 154 */
											bool_t BgL_res2135z00_2705;

											{	/* Reduce/sbeta.scm 154 */
												obj_t BgL_oclassz00_2688;

												{	/* Reduce/sbeta.scm 154 */
													obj_t BgL_arg1815z00_2696;
													long BgL_arg1816z00_2697;

													BgL_arg1815z00_2696 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Reduce/sbeta.scm 154 */
														long BgL_arg1817z00_2698;

														BgL_arg1817z00_2698 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2674);
														BgL_arg1816z00_2697 =
															(BgL_arg1817z00_2698 - OBJECT_TYPE);
													}
													BgL_oclassz00_2688 =
														VECTOR_REF(BgL_arg1815z00_2696,
														BgL_arg1816z00_2697);
												}
												{	/* Reduce/sbeta.scm 154 */
													bool_t BgL__ortest_1115z00_2689;

													BgL__ortest_1115z00_2689 =
														(BgL_classz00_2672 == BgL_oclassz00_2688);
													if (BgL__ortest_1115z00_2689)
														{	/* Reduce/sbeta.scm 154 */
															BgL_res2135z00_2705 = BgL__ortest_1115z00_2689;
														}
													else
														{	/* Reduce/sbeta.scm 154 */
															long BgL_odepthz00_2690;

															{	/* Reduce/sbeta.scm 154 */
																obj_t BgL_arg1804z00_2691;

																BgL_arg1804z00_2691 = (BgL_oclassz00_2688);
																BgL_odepthz00_2690 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2691);
															}
															if ((5L < BgL_odepthz00_2690))
																{	/* Reduce/sbeta.scm 154 */
																	obj_t BgL_arg1802z00_2693;

																	{	/* Reduce/sbeta.scm 154 */
																		obj_t BgL_arg1803z00_2694;

																		BgL_arg1803z00_2694 = (BgL_oclassz00_2688);
																		BgL_arg1802z00_2693 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2694, 5L);
																	}
																	BgL_res2135z00_2705 =
																		(BgL_arg1802z00_2693 == BgL_classz00_2672);
																}
															else
																{	/* Reduce/sbeta.scm 154 */
																	BgL_res2135z00_2705 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2270z00_4744 = BgL_res2135z00_2705;
										}
								}
							}
							if (BgL_test2270z00_4744)
								{	/* Reduce/sbeta.scm 155 */
									obj_t BgL_g1285z00_1562;

									BgL_g1285z00_1562 =
										(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt)
													((BgL_vrefz00_bglt) BgL_exprz00_1557))))->
										BgL_exprza2za2);
									{
										obj_t BgL_l1283z00_1564;

										BgL_l1283z00_1564 = BgL_g1285z00_1562;
									BgL_zc3z04anonymousza31600ze3z87_1565:
										if (NULLP(BgL_l1283z00_1564))
											{	/* Reduce/sbeta.scm 155 */
												return ((bool_t) 0);
											}
										else
											{	/* Reduce/sbeta.scm 155 */
												bool_t BgL__ortest_1286z00_1567;

												{	/* Reduce/sbeta.scm 155 */
													obj_t BgL_arg1605z00_1569;

													BgL_arg1605z00_1569 =
														CAR(((obj_t) BgL_l1283z00_1564));
													BgL__ortest_1286z00_1567 =
														BGl_dangerouszf3zf3zzreduce_betaz00
														(BgL_arg1605z00_1569);
												}
												if (BgL__ortest_1286z00_1567)
													{	/* Reduce/sbeta.scm 155 */
														return BgL__ortest_1286z00_1567;
													}
												else
													{	/* Reduce/sbeta.scm 155 */
														obj_t BgL_arg1602z00_1568;

														BgL_arg1602z00_1568 =
															CDR(((obj_t) BgL_l1283z00_1564));
														{
															obj_t BgL_l1283z00_4779;

															BgL_l1283z00_4779 = BgL_arg1602z00_1568;
															BgL_l1283z00_1564 = BgL_l1283z00_4779;
															goto BgL_zc3z04anonymousza31600ze3z87_1565;
														}
													}
											}
									}
								}
							else
								{	/* Reduce/sbeta.scm 156 */
									bool_t BgL_test2276z00_4780;

									{	/* Reduce/sbeta.scm 156 */
										obj_t BgL_classz00_2709;

										BgL_classz00_2709 = BGl_getfieldz00zzast_nodez00;
										{	/* Reduce/sbeta.scm 156 */
											BgL_objectz00_bglt BgL_arg1807z00_2711;

											{	/* Reduce/sbeta.scm 156 */
												obj_t BgL_tmpz00_4781;

												BgL_tmpz00_4781 =
													((obj_t) ((BgL_objectz00_bglt) BgL_exprz00_1557));
												BgL_arg1807z00_2711 =
													(BgL_objectz00_bglt) (BgL_tmpz00_4781);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Reduce/sbeta.scm 156 */
													long BgL_idxz00_2717;

													BgL_idxz00_2717 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2711);
													BgL_test2276z00_4780 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2717 + 5L)) == BgL_classz00_2709);
												}
											else
												{	/* Reduce/sbeta.scm 156 */
													bool_t BgL_res2136z00_2742;

													{	/* Reduce/sbeta.scm 156 */
														obj_t BgL_oclassz00_2725;

														{	/* Reduce/sbeta.scm 156 */
															obj_t BgL_arg1815z00_2733;
															long BgL_arg1816z00_2734;

															BgL_arg1815z00_2733 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Reduce/sbeta.scm 156 */
																long BgL_arg1817z00_2735;

																BgL_arg1817z00_2735 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2711);
																BgL_arg1816z00_2734 =
																	(BgL_arg1817z00_2735 - OBJECT_TYPE);
															}
															BgL_oclassz00_2725 =
																VECTOR_REF(BgL_arg1815z00_2733,
																BgL_arg1816z00_2734);
														}
														{	/* Reduce/sbeta.scm 156 */
															bool_t BgL__ortest_1115z00_2726;

															BgL__ortest_1115z00_2726 =
																(BgL_classz00_2709 == BgL_oclassz00_2725);
															if (BgL__ortest_1115z00_2726)
																{	/* Reduce/sbeta.scm 156 */
																	BgL_res2136z00_2742 =
																		BgL__ortest_1115z00_2726;
																}
															else
																{	/* Reduce/sbeta.scm 156 */
																	long BgL_odepthz00_2727;

																	{	/* Reduce/sbeta.scm 156 */
																		obj_t BgL_arg1804z00_2728;

																		BgL_arg1804z00_2728 = (BgL_oclassz00_2725);
																		BgL_odepthz00_2727 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2728);
																	}
																	if ((5L < BgL_odepthz00_2727))
																		{	/* Reduce/sbeta.scm 156 */
																			obj_t BgL_arg1802z00_2730;

																			{	/* Reduce/sbeta.scm 156 */
																				obj_t BgL_arg1803z00_2731;

																				BgL_arg1803z00_2731 =
																					(BgL_oclassz00_2725);
																				BgL_arg1802z00_2730 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2731, 5L);
																			}
																			BgL_res2136z00_2742 =
																				(BgL_arg1802z00_2730 ==
																				BgL_classz00_2709);
																		}
																	else
																		{	/* Reduce/sbeta.scm 156 */
																			BgL_res2136z00_2742 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2276z00_4780 = BgL_res2136z00_2742;
												}
										}
									}
									if (BgL_test2276z00_4780)
										{	/* Reduce/sbeta.scm 157 */
											obj_t BgL_g1289z00_1572;

											BgL_g1289z00_1572 =
												(((BgL_externz00_bglt) COBJECT(
														((BgL_externz00_bglt)
															((BgL_getfieldz00_bglt) BgL_exprz00_1557))))->
												BgL_exprza2za2);
											{
												obj_t BgL_l1287z00_1574;

												BgL_l1287z00_1574 = BgL_g1289z00_1572;
											BgL_zc3z04anonymousza31607ze3z87_1575:
												if (NULLP(BgL_l1287z00_1574))
													{	/* Reduce/sbeta.scm 157 */
														return ((bool_t) 0);
													}
												else
													{	/* Reduce/sbeta.scm 157 */
														bool_t BgL__ortest_1290z00_1577;

														{	/* Reduce/sbeta.scm 157 */
															obj_t BgL_arg1611z00_1579;

															BgL_arg1611z00_1579 =
																CAR(((obj_t) BgL_l1287z00_1574));
															BgL__ortest_1290z00_1577 =
																BGl_dangerouszf3zf3zzreduce_betaz00
																(BgL_arg1611z00_1579);
														}
														if (BgL__ortest_1290z00_1577)
															{	/* Reduce/sbeta.scm 157 */
																return BgL__ortest_1290z00_1577;
															}
														else
															{	/* Reduce/sbeta.scm 157 */
																obj_t BgL_arg1609z00_1578;

																BgL_arg1609z00_1578 =
																	CDR(((obj_t) BgL_l1287z00_1574));
																{
																	obj_t BgL_l1287z00_4815;

																	BgL_l1287z00_4815 = BgL_arg1609z00_1578;
																	BgL_l1287z00_1574 = BgL_l1287z00_4815;
																	goto BgL_zc3z04anonymousza31607ze3z87_1575;
																}
															}
													}
											}
										}
									else
										{	/* Reduce/sbeta.scm 158 */
											bool_t BgL_test2282z00_4816;

											{	/* Reduce/sbeta.scm 158 */
												obj_t BgL_classz00_2746;

												BgL_classz00_2746 = BGl_appz00zzast_nodez00;
												{	/* Reduce/sbeta.scm 158 */
													BgL_objectz00_bglt BgL_arg1807z00_2748;

													{	/* Reduce/sbeta.scm 158 */
														obj_t BgL_tmpz00_4817;

														BgL_tmpz00_4817 =
															((obj_t) ((BgL_objectz00_bglt) BgL_exprz00_1557));
														BgL_arg1807z00_2748 =
															(BgL_objectz00_bglt) (BgL_tmpz00_4817);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Reduce/sbeta.scm 158 */
															long BgL_idxz00_2754;

															BgL_idxz00_2754 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2748);
															BgL_test2282z00_4816 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2754 + 3L)) == BgL_classz00_2746);
														}
													else
														{	/* Reduce/sbeta.scm 158 */
															bool_t BgL_res2137z00_2779;

															{	/* Reduce/sbeta.scm 158 */
																obj_t BgL_oclassz00_2762;

																{	/* Reduce/sbeta.scm 158 */
																	obj_t BgL_arg1815z00_2770;
																	long BgL_arg1816z00_2771;

																	BgL_arg1815z00_2770 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Reduce/sbeta.scm 158 */
																		long BgL_arg1817z00_2772;

																		BgL_arg1817z00_2772 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2748);
																		BgL_arg1816z00_2771 =
																			(BgL_arg1817z00_2772 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2762 =
																		VECTOR_REF(BgL_arg1815z00_2770,
																		BgL_arg1816z00_2771);
																}
																{	/* Reduce/sbeta.scm 158 */
																	bool_t BgL__ortest_1115z00_2763;

																	BgL__ortest_1115z00_2763 =
																		(BgL_classz00_2746 == BgL_oclassz00_2762);
																	if (BgL__ortest_1115z00_2763)
																		{	/* Reduce/sbeta.scm 158 */
																			BgL_res2137z00_2779 =
																				BgL__ortest_1115z00_2763;
																		}
																	else
																		{	/* Reduce/sbeta.scm 158 */
																			long BgL_odepthz00_2764;

																			{	/* Reduce/sbeta.scm 158 */
																				obj_t BgL_arg1804z00_2765;

																				BgL_arg1804z00_2765 =
																					(BgL_oclassz00_2762);
																				BgL_odepthz00_2764 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2765);
																			}
																			if ((3L < BgL_odepthz00_2764))
																				{	/* Reduce/sbeta.scm 158 */
																					obj_t BgL_arg1802z00_2767;

																					{	/* Reduce/sbeta.scm 158 */
																						obj_t BgL_arg1803z00_2768;

																						BgL_arg1803z00_2768 =
																							(BgL_oclassz00_2762);
																						BgL_arg1802z00_2767 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2768, 3L);
																					}
																					BgL_res2137z00_2779 =
																						(BgL_arg1802z00_2767 ==
																						BgL_classz00_2746);
																				}
																			else
																				{	/* Reduce/sbeta.scm 158 */
																					BgL_res2137z00_2779 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2282z00_4816 = BgL_res2137z00_2779;
														}
												}
											}
											if (BgL_test2282z00_4816)
												{	/* Reduce/sbeta.scm 162 */
													BgL_variablez00_bglt BgL_varz00_1583;

													BgL_varz00_1583 =
														(((BgL_varz00_bglt) COBJECT(
																(((BgL_appz00_bglt) COBJECT(
																			((BgL_appz00_bglt) BgL_exprz00_1557)))->
																	BgL_funz00)))->BgL_variablez00);
													{	/* Reduce/sbeta.scm 162 */
														BgL_valuez00_bglt BgL_valz00_1584;

														BgL_valz00_1584 =
															(((BgL_variablez00_bglt)
																COBJECT(BgL_varz00_1583))->BgL_valuez00);
														{	/* Reduce/sbeta.scm 163 */

															{	/* Reduce/sbeta.scm 164 */
																bool_t BgL_test2286z00_4844;

																{	/* Reduce/sbeta.scm 164 */
																	bool_t BgL_test2287z00_4845;

																	{	/* Reduce/sbeta.scm 164 */
																		obj_t BgL_classz00_2782;

																		BgL_classz00_2782 =
																			BGl_globalz00zzast_varz00;
																		{	/* Reduce/sbeta.scm 164 */
																			BgL_objectz00_bglt BgL_arg1807z00_2784;

																			{	/* Reduce/sbeta.scm 164 */
																				obj_t BgL_tmpz00_4846;

																				BgL_tmpz00_4846 =
																					((obj_t)
																					((BgL_objectz00_bglt)
																						BgL_varz00_1583));
																				BgL_arg1807z00_2784 =
																					(BgL_objectz00_bglt)
																					(BgL_tmpz00_4846);
																			}
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Reduce/sbeta.scm 164 */
																					long BgL_idxz00_2790;

																					BgL_idxz00_2790 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2784);
																					BgL_test2287z00_4845 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2790 + 2L)) ==
																						BgL_classz00_2782);
																				}
																			else
																				{	/* Reduce/sbeta.scm 164 */
																					bool_t BgL_res2138z00_2815;

																					{	/* Reduce/sbeta.scm 164 */
																						obj_t BgL_oclassz00_2798;

																						{	/* Reduce/sbeta.scm 164 */
																							obj_t BgL_arg1815z00_2806;
																							long BgL_arg1816z00_2807;

																							BgL_arg1815z00_2806 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Reduce/sbeta.scm 164 */
																								long BgL_arg1817z00_2808;

																								BgL_arg1817z00_2808 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2784);
																								BgL_arg1816z00_2807 =
																									(BgL_arg1817z00_2808 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2798 =
																								VECTOR_REF(BgL_arg1815z00_2806,
																								BgL_arg1816z00_2807);
																						}
																						{	/* Reduce/sbeta.scm 164 */
																							bool_t BgL__ortest_1115z00_2799;

																							BgL__ortest_1115z00_2799 =
																								(BgL_classz00_2782 ==
																								BgL_oclassz00_2798);
																							if (BgL__ortest_1115z00_2799)
																								{	/* Reduce/sbeta.scm 164 */
																									BgL_res2138z00_2815 =
																										BgL__ortest_1115z00_2799;
																								}
																							else
																								{	/* Reduce/sbeta.scm 164 */
																									long BgL_odepthz00_2800;

																									{	/* Reduce/sbeta.scm 164 */
																										obj_t BgL_arg1804z00_2801;

																										BgL_arg1804z00_2801 =
																											(BgL_oclassz00_2798);
																										BgL_odepthz00_2800 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2801);
																									}
																									if ((2L < BgL_odepthz00_2800))
																										{	/* Reduce/sbeta.scm 164 */
																											obj_t BgL_arg1802z00_2803;

																											{	/* Reduce/sbeta.scm 164 */
																												obj_t
																													BgL_arg1803z00_2804;
																												BgL_arg1803z00_2804 =
																													(BgL_oclassz00_2798);
																												BgL_arg1802z00_2803 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2804,
																													2L);
																											}
																											BgL_res2138z00_2815 =
																												(BgL_arg1802z00_2803 ==
																												BgL_classz00_2782);
																										}
																									else
																										{	/* Reduce/sbeta.scm 164 */
																											BgL_res2138z00_2815 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2287z00_4845 =
																						BgL_res2138z00_2815;
																				}
																		}
																	}
																	if (BgL_test2287z00_4845)
																		{	/* Reduce/sbeta.scm 165 */
																			bool_t BgL_test2291z00_4869;

																			{	/* Reduce/sbeta.scm 165 */
																				obj_t BgL_classz00_2816;

																				BgL_classz00_2816 =
																					BGl_cfunz00zzast_varz00;
																				{	/* Reduce/sbeta.scm 165 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_2818;
																					{	/* Reduce/sbeta.scm 165 */
																						obj_t BgL_tmpz00_4870;

																						BgL_tmpz00_4870 =
																							((obj_t)
																							((BgL_objectz00_bglt)
																								BgL_valz00_1584));
																						BgL_arg1807z00_2818 =
																							(BgL_objectz00_bglt)
																							(BgL_tmpz00_4870);
																					}
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Reduce/sbeta.scm 165 */
																							long BgL_idxz00_2824;

																							BgL_idxz00_2824 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_2818);
																							BgL_test2291z00_4869 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_2824 + 3L)) ==
																								BgL_classz00_2816);
																						}
																					else
																						{	/* Reduce/sbeta.scm 165 */
																							bool_t BgL_res2139z00_2849;

																							{	/* Reduce/sbeta.scm 165 */
																								obj_t BgL_oclassz00_2832;

																								{	/* Reduce/sbeta.scm 165 */
																									obj_t BgL_arg1815z00_2840;
																									long BgL_arg1816z00_2841;

																									BgL_arg1815z00_2840 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Reduce/sbeta.scm 165 */
																										long BgL_arg1817z00_2842;

																										BgL_arg1817z00_2842 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_2818);
																										BgL_arg1816z00_2841 =
																											(BgL_arg1817z00_2842 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_2832 =
																										VECTOR_REF
																										(BgL_arg1815z00_2840,
																										BgL_arg1816z00_2841);
																								}
																								{	/* Reduce/sbeta.scm 165 */
																									bool_t
																										BgL__ortest_1115z00_2833;
																									BgL__ortest_1115z00_2833 =
																										(BgL_classz00_2816 ==
																										BgL_oclassz00_2832);
																									if (BgL__ortest_1115z00_2833)
																										{	/* Reduce/sbeta.scm 165 */
																											BgL_res2139z00_2849 =
																												BgL__ortest_1115z00_2833;
																										}
																									else
																										{	/* Reduce/sbeta.scm 165 */
																											long BgL_odepthz00_2834;

																											{	/* Reduce/sbeta.scm 165 */
																												obj_t
																													BgL_arg1804z00_2835;
																												BgL_arg1804z00_2835 =
																													(BgL_oclassz00_2832);
																												BgL_odepthz00_2834 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_2835);
																											}
																											if (
																												(3L <
																													BgL_odepthz00_2834))
																												{	/* Reduce/sbeta.scm 165 */
																													obj_t
																														BgL_arg1802z00_2837;
																													{	/* Reduce/sbeta.scm 165 */
																														obj_t
																															BgL_arg1803z00_2838;
																														BgL_arg1803z00_2838
																															=
																															(BgL_oclassz00_2832);
																														BgL_arg1802z00_2837
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_2838,
																															3L);
																													}
																													BgL_res2139z00_2849 =
																														(BgL_arg1802z00_2837
																														==
																														BgL_classz00_2816);
																												}
																											else
																												{	/* Reduce/sbeta.scm 165 */
																													BgL_res2139z00_2849 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test2291z00_4869 =
																								BgL_res2139z00_2849;
																						}
																				}
																			}
																			if (BgL_test2291z00_4869)
																				{	/* Reduce/sbeta.scm 165 */
																					if (
																						(((BgL_cfunz00_bglt) COBJECT(
																									((BgL_cfunz00_bglt)
																										BgL_valz00_1584)))->
																							BgL_macrozf3zf3))
																						{	/* Reduce/sbeta.scm 166 */
																							if (CBOOL
																								(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																									(CNST_TABLE_REF(4),
																										(((BgL_globalz00_bglt)
																												COBJECT((
																														(BgL_globalz00_bglt)
																														BgL_varz00_1583)))->
																											BgL_pragmaz00))))
																								{	/* Reduce/sbeta.scm 167 */
																									BgL_test2286z00_4844 =
																										((bool_t) 0);
																								}
																							else
																								{	/* Reduce/sbeta.scm 167 */
																									BgL_test2286z00_4844 =
																										((bool_t) 1);
																								}
																						}
																					else
																						{	/* Reduce/sbeta.scm 166 */
																							BgL_test2286z00_4844 =
																								((bool_t) 0);
																						}
																				}
																			else
																				{	/* Reduce/sbeta.scm 165 */
																					BgL_test2286z00_4844 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Reduce/sbeta.scm 164 */
																			BgL_test2286z00_4844 = ((bool_t) 0);
																		}
																}
																if (BgL_test2286z00_4844)
																	{	/* Reduce/sbeta.scm 164 */
																		return ((bool_t) 1);
																	}
																else
																	{	/* Reduce/sbeta.scm 169 */
																		obj_t BgL_g1293z00_1592;

																		BgL_g1293z00_1592 =
																			(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						BgL_exprz00_1557)))->BgL_argsz00);
																		{
																			obj_t BgL_l1291z00_1594;

																			BgL_l1291z00_1594 = BgL_g1293z00_1592;
																		BgL_zc3z04anonymousza31629ze3z87_1595:
																			if (NULLP(BgL_l1291z00_1594))
																				{	/* Reduce/sbeta.scm 169 */
																					return ((bool_t) 0);
																				}
																			else
																				{	/* Reduce/sbeta.scm 169 */
																					bool_t BgL__ortest_1294z00_1597;

																					{	/* Reduce/sbeta.scm 169 */
																						obj_t BgL_arg1646z00_1599;

																						BgL_arg1646z00_1599 =
																							CAR(((obj_t) BgL_l1291z00_1594));
																						BgL__ortest_1294z00_1597 =
																							BGl_dangerouszf3zf3zzreduce_betaz00
																							(BgL_arg1646z00_1599);
																					}
																					if (BgL__ortest_1294z00_1597)
																						{	/* Reduce/sbeta.scm 169 */
																							return BgL__ortest_1294z00_1597;
																						}
																					else
																						{	/* Reduce/sbeta.scm 169 */
																							obj_t BgL_arg1642z00_1598;

																							BgL_arg1642z00_1598 =
																								CDR(
																								((obj_t) BgL_l1291z00_1594));
																							{
																								obj_t BgL_l1291z00_4912;

																								BgL_l1291z00_4912 =
																									BgL_arg1642z00_1598;
																								BgL_l1291z00_1594 =
																									BgL_l1291z00_4912;
																								goto
																									BgL_zc3z04anonymousza31629ze3z87_1595;
																							}
																						}
																				}
																		}
																	}
															}
														}
													}
												}
											else
												{	/* Reduce/sbeta.scm 158 */
													return ((bool_t) 1);
												}
										}
								}
						}
				}
			}
		}

	}



/* side-effect-safe? */
	bool_t BGl_sidezd2effectzd2safezf3zf3zzreduce_betaz00(obj_t BgL_exprz00_13)
	{
		{	/* Reduce/sbeta.scm 180 */
			{	/* Reduce/sbeta.scm 181 */
				BgL_nodez00_bglt BgL_exprz00_1609;

				BgL_exprz00_1609 =
					BGl_findzd2actualzd2expressionz00zzreduce_betaz00(
					((BgL_nodez00_bglt) BgL_exprz00_13));
				{	/* Reduce/sbeta.scm 183 */
					bool_t BgL_test2299z00_4915;

					{	/* Reduce/sbeta.scm 183 */
						bool_t BgL_test2300z00_4916;

						{	/* Reduce/sbeta.scm 183 */
							obj_t BgL_classz00_2854;

							BgL_classz00_2854 = BGl_varz00zzast_nodez00;
							{	/* Reduce/sbeta.scm 183 */
								BgL_objectz00_bglt BgL_arg1807z00_2856;

								{	/* Reduce/sbeta.scm 183 */
									obj_t BgL_tmpz00_4917;

									BgL_tmpz00_4917 =
										((obj_t) ((BgL_objectz00_bglt) BgL_exprz00_1609));
									BgL_arg1807z00_2856 = (BgL_objectz00_bglt) (BgL_tmpz00_4917);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Reduce/sbeta.scm 183 */
										long BgL_idxz00_2862;

										BgL_idxz00_2862 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2856);
										BgL_test2300z00_4916 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2862 + 2L)) == BgL_classz00_2854);
									}
								else
									{	/* Reduce/sbeta.scm 183 */
										bool_t BgL_res2140z00_2887;

										{	/* Reduce/sbeta.scm 183 */
											obj_t BgL_oclassz00_2870;

											{	/* Reduce/sbeta.scm 183 */
												obj_t BgL_arg1815z00_2878;
												long BgL_arg1816z00_2879;

												BgL_arg1815z00_2878 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Reduce/sbeta.scm 183 */
													long BgL_arg1817z00_2880;

													BgL_arg1817z00_2880 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2856);
													BgL_arg1816z00_2879 =
														(BgL_arg1817z00_2880 - OBJECT_TYPE);
												}
												BgL_oclassz00_2870 =
													VECTOR_REF(BgL_arg1815z00_2878, BgL_arg1816z00_2879);
											}
											{	/* Reduce/sbeta.scm 183 */
												bool_t BgL__ortest_1115z00_2871;

												BgL__ortest_1115z00_2871 =
													(BgL_classz00_2854 == BgL_oclassz00_2870);
												if (BgL__ortest_1115z00_2871)
													{	/* Reduce/sbeta.scm 183 */
														BgL_res2140z00_2887 = BgL__ortest_1115z00_2871;
													}
												else
													{	/* Reduce/sbeta.scm 183 */
														long BgL_odepthz00_2872;

														{	/* Reduce/sbeta.scm 183 */
															obj_t BgL_arg1804z00_2873;

															BgL_arg1804z00_2873 = (BgL_oclassz00_2870);
															BgL_odepthz00_2872 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2873);
														}
														if ((2L < BgL_odepthz00_2872))
															{	/* Reduce/sbeta.scm 183 */
																obj_t BgL_arg1802z00_2875;

																{	/* Reduce/sbeta.scm 183 */
																	obj_t BgL_arg1803z00_2876;

																	BgL_arg1803z00_2876 = (BgL_oclassz00_2870);
																	BgL_arg1802z00_2875 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2876,
																		2L);
																}
																BgL_res2140z00_2887 =
																	(BgL_arg1802z00_2875 == BgL_classz00_2854);
															}
														else
															{	/* Reduce/sbeta.scm 183 */
																BgL_res2140z00_2887 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2300z00_4916 = BgL_res2140z00_2887;
									}
							}
						}
						if (BgL_test2300z00_4916)
							{	/* Reduce/sbeta.scm 183 */
								BgL_test2299z00_4915 = ((bool_t) 1);
							}
						else
							{	/* Reduce/sbeta.scm 183 */
								bool_t BgL_test2304z00_4940;

								{	/* Reduce/sbeta.scm 183 */
									obj_t BgL_classz00_2888;

									BgL_classz00_2888 = BGl_atomz00zzast_nodez00;
									{	/* Reduce/sbeta.scm 183 */
										BgL_objectz00_bglt BgL_arg1807z00_2890;

										{	/* Reduce/sbeta.scm 183 */
											obj_t BgL_tmpz00_4941;

											BgL_tmpz00_4941 =
												((obj_t) ((BgL_objectz00_bglt) BgL_exprz00_1609));
											BgL_arg1807z00_2890 =
												(BgL_objectz00_bglt) (BgL_tmpz00_4941);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Reduce/sbeta.scm 183 */
												long BgL_idxz00_2896;

												BgL_idxz00_2896 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2890);
												BgL_test2304z00_4940 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2896 + 2L)) == BgL_classz00_2888);
											}
										else
											{	/* Reduce/sbeta.scm 183 */
												bool_t BgL_res2141z00_2921;

												{	/* Reduce/sbeta.scm 183 */
													obj_t BgL_oclassz00_2904;

													{	/* Reduce/sbeta.scm 183 */
														obj_t BgL_arg1815z00_2912;
														long BgL_arg1816z00_2913;

														BgL_arg1815z00_2912 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Reduce/sbeta.scm 183 */
															long BgL_arg1817z00_2914;

															BgL_arg1817z00_2914 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2890);
															BgL_arg1816z00_2913 =
																(BgL_arg1817z00_2914 - OBJECT_TYPE);
														}
														BgL_oclassz00_2904 =
															VECTOR_REF(BgL_arg1815z00_2912,
															BgL_arg1816z00_2913);
													}
													{	/* Reduce/sbeta.scm 183 */
														bool_t BgL__ortest_1115z00_2905;

														BgL__ortest_1115z00_2905 =
															(BgL_classz00_2888 == BgL_oclassz00_2904);
														if (BgL__ortest_1115z00_2905)
															{	/* Reduce/sbeta.scm 183 */
																BgL_res2141z00_2921 = BgL__ortest_1115z00_2905;
															}
														else
															{	/* Reduce/sbeta.scm 183 */
																long BgL_odepthz00_2906;

																{	/* Reduce/sbeta.scm 183 */
																	obj_t BgL_arg1804z00_2907;

																	BgL_arg1804z00_2907 = (BgL_oclassz00_2904);
																	BgL_odepthz00_2906 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2907);
																}
																if ((2L < BgL_odepthz00_2906))
																	{	/* Reduce/sbeta.scm 183 */
																		obj_t BgL_arg1802z00_2909;

																		{	/* Reduce/sbeta.scm 183 */
																			obj_t BgL_arg1803z00_2910;

																			BgL_arg1803z00_2910 =
																				(BgL_oclassz00_2904);
																			BgL_arg1802z00_2909 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2910, 2L);
																		}
																		BgL_res2141z00_2921 =
																			(BgL_arg1802z00_2909 ==
																			BgL_classz00_2888);
																	}
																else
																	{	/* Reduce/sbeta.scm 183 */
																		BgL_res2141z00_2921 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2304z00_4940 = BgL_res2141z00_2921;
											}
									}
								}
								if (BgL_test2304z00_4940)
									{	/* Reduce/sbeta.scm 183 */
										BgL_test2299z00_4915 = ((bool_t) 1);
									}
								else
									{	/* Reduce/sbeta.scm 183 */
										obj_t BgL_classz00_2922;

										BgL_classz00_2922 = BGl_kwotez00zzast_nodez00;
										{	/* Reduce/sbeta.scm 183 */
											BgL_objectz00_bglt BgL_arg1807z00_2924;

											{	/* Reduce/sbeta.scm 183 */
												obj_t BgL_tmpz00_4964;

												BgL_tmpz00_4964 =
													((obj_t) ((BgL_objectz00_bglt) BgL_exprz00_1609));
												BgL_arg1807z00_2924 =
													(BgL_objectz00_bglt) (BgL_tmpz00_4964);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Reduce/sbeta.scm 183 */
													long BgL_idxz00_2930;

													BgL_idxz00_2930 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2924);
													BgL_test2299z00_4915 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2930 + 2L)) == BgL_classz00_2922);
												}
											else
												{	/* Reduce/sbeta.scm 183 */
													bool_t BgL_res2142z00_2955;

													{	/* Reduce/sbeta.scm 183 */
														obj_t BgL_oclassz00_2938;

														{	/* Reduce/sbeta.scm 183 */
															obj_t BgL_arg1815z00_2946;
															long BgL_arg1816z00_2947;

															BgL_arg1815z00_2946 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Reduce/sbeta.scm 183 */
																long BgL_arg1817z00_2948;

																BgL_arg1817z00_2948 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2924);
																BgL_arg1816z00_2947 =
																	(BgL_arg1817z00_2948 - OBJECT_TYPE);
															}
															BgL_oclassz00_2938 =
																VECTOR_REF(BgL_arg1815z00_2946,
																BgL_arg1816z00_2947);
														}
														{	/* Reduce/sbeta.scm 183 */
															bool_t BgL__ortest_1115z00_2939;

															BgL__ortest_1115z00_2939 =
																(BgL_classz00_2922 == BgL_oclassz00_2938);
															if (BgL__ortest_1115z00_2939)
																{	/* Reduce/sbeta.scm 183 */
																	BgL_res2142z00_2955 =
																		BgL__ortest_1115z00_2939;
																}
															else
																{	/* Reduce/sbeta.scm 183 */
																	long BgL_odepthz00_2940;

																	{	/* Reduce/sbeta.scm 183 */
																		obj_t BgL_arg1804z00_2941;

																		BgL_arg1804z00_2941 = (BgL_oclassz00_2938);
																		BgL_odepthz00_2940 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2941);
																	}
																	if ((2L < BgL_odepthz00_2940))
																		{	/* Reduce/sbeta.scm 183 */
																			obj_t BgL_arg1802z00_2943;

																			{	/* Reduce/sbeta.scm 183 */
																				obj_t BgL_arg1803z00_2944;

																				BgL_arg1803z00_2944 =
																					(BgL_oclassz00_2938);
																				BgL_arg1802z00_2943 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2944, 2L);
																			}
																			BgL_res2142z00_2955 =
																				(BgL_arg1802z00_2943 ==
																				BgL_classz00_2922);
																		}
																	else
																		{	/* Reduce/sbeta.scm 183 */
																			BgL_res2142z00_2955 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2299z00_4915 = BgL_res2142z00_2955;
												}
										}
									}
							}
					}
					if (BgL_test2299z00_4915)
						{	/* Reduce/sbeta.scm 183 */
							return ((bool_t) 1);
						}
					else
						{	/* Reduce/sbeta.scm 185 */
							bool_t BgL_test2311z00_4987;

							{	/* Reduce/sbeta.scm 185 */
								obj_t BgL_classz00_2956;

								BgL_classz00_2956 = BGl_vrefz00zzast_nodez00;
								{	/* Reduce/sbeta.scm 185 */
									BgL_objectz00_bglt BgL_arg1807z00_2958;

									{	/* Reduce/sbeta.scm 185 */
										obj_t BgL_tmpz00_4988;

										BgL_tmpz00_4988 =
											((obj_t) ((BgL_objectz00_bglt) BgL_exprz00_1609));
										BgL_arg1807z00_2958 =
											(BgL_objectz00_bglt) (BgL_tmpz00_4988);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Reduce/sbeta.scm 185 */
											long BgL_idxz00_2964;

											BgL_idxz00_2964 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2958);
											BgL_test2311z00_4987 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2964 + 5L)) == BgL_classz00_2956);
										}
									else
										{	/* Reduce/sbeta.scm 185 */
											bool_t BgL_res2143z00_2989;

											{	/* Reduce/sbeta.scm 185 */
												obj_t BgL_oclassz00_2972;

												{	/* Reduce/sbeta.scm 185 */
													obj_t BgL_arg1815z00_2980;
													long BgL_arg1816z00_2981;

													BgL_arg1815z00_2980 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Reduce/sbeta.scm 185 */
														long BgL_arg1817z00_2982;

														BgL_arg1817z00_2982 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2958);
														BgL_arg1816z00_2981 =
															(BgL_arg1817z00_2982 - OBJECT_TYPE);
													}
													BgL_oclassz00_2972 =
														VECTOR_REF(BgL_arg1815z00_2980,
														BgL_arg1816z00_2981);
												}
												{	/* Reduce/sbeta.scm 185 */
													bool_t BgL__ortest_1115z00_2973;

													BgL__ortest_1115z00_2973 =
														(BgL_classz00_2956 == BgL_oclassz00_2972);
													if (BgL__ortest_1115z00_2973)
														{	/* Reduce/sbeta.scm 185 */
															BgL_res2143z00_2989 = BgL__ortest_1115z00_2973;
														}
													else
														{	/* Reduce/sbeta.scm 185 */
															long BgL_odepthz00_2974;

															{	/* Reduce/sbeta.scm 185 */
																obj_t BgL_arg1804z00_2975;

																BgL_arg1804z00_2975 = (BgL_oclassz00_2972);
																BgL_odepthz00_2974 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2975);
															}
															if ((5L < BgL_odepthz00_2974))
																{	/* Reduce/sbeta.scm 185 */
																	obj_t BgL_arg1802z00_2977;

																	{	/* Reduce/sbeta.scm 185 */
																		obj_t BgL_arg1803z00_2978;

																		BgL_arg1803z00_2978 = (BgL_oclassz00_2972);
																		BgL_arg1802z00_2977 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2978, 5L);
																	}
																	BgL_res2143z00_2989 =
																		(BgL_arg1802z00_2977 == BgL_classz00_2956);
																}
															else
																{	/* Reduce/sbeta.scm 185 */
																	BgL_res2143z00_2989 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2311z00_4987 = BgL_res2143z00_2989;
										}
								}
							}
							if (BgL_test2311z00_4987)
								{	/* Reduce/sbeta.scm 186 */
									obj_t BgL_g1297z00_1614;

									BgL_g1297z00_1614 =
										(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt)
													((BgL_vrefz00_bglt) BgL_exprz00_1609))))->
										BgL_exprza2za2);
									{
										obj_t BgL_l1295z00_1616;

										BgL_l1295z00_1616 = BgL_g1297z00_1614;
									BgL_zc3z04anonymousza31656ze3z87_1617:
										if (NULLP(BgL_l1295z00_1616))
											{	/* Reduce/sbeta.scm 186 */
												return ((bool_t) 1);
											}
										else
											{	/* Reduce/sbeta.scm 186 */
												bool_t BgL_test2316z00_5016;

												{	/* Reduce/sbeta.scm 186 */
													obj_t BgL_arg1663z00_1622;

													BgL_arg1663z00_1622 =
														CAR(((obj_t) BgL_l1295z00_1616));
													BgL_test2316z00_5016 =
														BGl_sidezd2effectzd2safezf3zf3zzreduce_betaz00
														(BgL_arg1663z00_1622);
												}
												if (BgL_test2316z00_5016)
													{	/* Reduce/sbeta.scm 186 */
														obj_t BgL_arg1661z00_1621;

														BgL_arg1661z00_1621 =
															CDR(((obj_t) BgL_l1295z00_1616));
														{
															obj_t BgL_l1295z00_5022;

															BgL_l1295z00_5022 = BgL_arg1661z00_1621;
															BgL_l1295z00_1616 = BgL_l1295z00_5022;
															goto BgL_zc3z04anonymousza31656ze3z87_1617;
														}
													}
												else
													{	/* Reduce/sbeta.scm 186 */
														return ((bool_t) 0);
													}
											}
									}
								}
							else
								{	/* Reduce/sbeta.scm 187 */
									bool_t BgL_test2317z00_5023;

									{	/* Reduce/sbeta.scm 187 */
										obj_t BgL_classz00_2993;

										BgL_classz00_2993 = BGl_getfieldz00zzast_nodez00;
										{	/* Reduce/sbeta.scm 187 */
											BgL_objectz00_bglt BgL_arg1807z00_2995;

											{	/* Reduce/sbeta.scm 187 */
												obj_t BgL_tmpz00_5024;

												BgL_tmpz00_5024 =
													((obj_t) ((BgL_objectz00_bglt) BgL_exprz00_1609));
												BgL_arg1807z00_2995 =
													(BgL_objectz00_bglt) (BgL_tmpz00_5024);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Reduce/sbeta.scm 187 */
													long BgL_idxz00_3001;

													BgL_idxz00_3001 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2995);
													BgL_test2317z00_5023 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_3001 + 5L)) == BgL_classz00_2993);
												}
											else
												{	/* Reduce/sbeta.scm 187 */
													bool_t BgL_res2144z00_3026;

													{	/* Reduce/sbeta.scm 187 */
														obj_t BgL_oclassz00_3009;

														{	/* Reduce/sbeta.scm 187 */
															obj_t BgL_arg1815z00_3017;
															long BgL_arg1816z00_3018;

															BgL_arg1815z00_3017 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Reduce/sbeta.scm 187 */
																long BgL_arg1817z00_3019;

																BgL_arg1817z00_3019 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2995);
																BgL_arg1816z00_3018 =
																	(BgL_arg1817z00_3019 - OBJECT_TYPE);
															}
															BgL_oclassz00_3009 =
																VECTOR_REF(BgL_arg1815z00_3017,
																BgL_arg1816z00_3018);
														}
														{	/* Reduce/sbeta.scm 187 */
															bool_t BgL__ortest_1115z00_3010;

															BgL__ortest_1115z00_3010 =
																(BgL_classz00_2993 == BgL_oclassz00_3009);
															if (BgL__ortest_1115z00_3010)
																{	/* Reduce/sbeta.scm 187 */
																	BgL_res2144z00_3026 =
																		BgL__ortest_1115z00_3010;
																}
															else
																{	/* Reduce/sbeta.scm 187 */
																	long BgL_odepthz00_3011;

																	{	/* Reduce/sbeta.scm 187 */
																		obj_t BgL_arg1804z00_3012;

																		BgL_arg1804z00_3012 = (BgL_oclassz00_3009);
																		BgL_odepthz00_3011 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_3012);
																	}
																	if ((5L < BgL_odepthz00_3011))
																		{	/* Reduce/sbeta.scm 187 */
																			obj_t BgL_arg1802z00_3014;

																			{	/* Reduce/sbeta.scm 187 */
																				obj_t BgL_arg1803z00_3015;

																				BgL_arg1803z00_3015 =
																					(BgL_oclassz00_3009);
																				BgL_arg1802z00_3014 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_3015, 5L);
																			}
																			BgL_res2144z00_3026 =
																				(BgL_arg1802z00_3014 ==
																				BgL_classz00_2993);
																		}
																	else
																		{	/* Reduce/sbeta.scm 187 */
																			BgL_res2144z00_3026 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2317z00_5023 = BgL_res2144z00_3026;
												}
										}
									}
									if (BgL_test2317z00_5023)
										{	/* Reduce/sbeta.scm 188 */
											obj_t BgL_g1301z00_1625;

											BgL_g1301z00_1625 =
												(((BgL_externz00_bglt) COBJECT(
														((BgL_externz00_bglt)
															((BgL_getfieldz00_bglt) BgL_exprz00_1609))))->
												BgL_exprza2za2);
											{
												obj_t BgL_l1299z00_1627;

												BgL_l1299z00_1627 = BgL_g1301z00_1625;
											BgL_zc3z04anonymousza31665ze3z87_1628:
												if (NULLP(BgL_l1299z00_1627))
													{	/* Reduce/sbeta.scm 188 */
														return ((bool_t) 1);
													}
												else
													{	/* Reduce/sbeta.scm 188 */
														bool_t BgL_test2322z00_5052;

														{	/* Reduce/sbeta.scm 188 */
															obj_t BgL_arg1678z00_1633;

															BgL_arg1678z00_1633 =
																CAR(((obj_t) BgL_l1299z00_1627));
															BgL_test2322z00_5052 =
																BGl_sidezd2effectzd2safezf3zf3zzreduce_betaz00
																(BgL_arg1678z00_1633);
														}
														if (BgL_test2322z00_5052)
															{	/* Reduce/sbeta.scm 188 */
																obj_t BgL_arg1675z00_1632;

																BgL_arg1675z00_1632 =
																	CDR(((obj_t) BgL_l1299z00_1627));
																{
																	obj_t BgL_l1299z00_5058;

																	BgL_l1299z00_5058 = BgL_arg1675z00_1632;
																	BgL_l1299z00_1627 = BgL_l1299z00_5058;
																	goto BgL_zc3z04anonymousza31665ze3z87_1628;
																}
															}
														else
															{	/* Reduce/sbeta.scm 188 */
																return ((bool_t) 0);
															}
													}
											}
										}
									else
										{	/* Reduce/sbeta.scm 189 */
											bool_t BgL_test2323z00_5059;

											{	/* Reduce/sbeta.scm 189 */
												obj_t BgL_classz00_3030;

												BgL_classz00_3030 = BGl_appz00zzast_nodez00;
												{	/* Reduce/sbeta.scm 189 */
													BgL_objectz00_bglt BgL_arg1807z00_3032;

													{	/* Reduce/sbeta.scm 189 */
														obj_t BgL_tmpz00_5060;

														BgL_tmpz00_5060 =
															((obj_t) ((BgL_objectz00_bglt) BgL_exprz00_1609));
														BgL_arg1807z00_3032 =
															(BgL_objectz00_bglt) (BgL_tmpz00_5060);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Reduce/sbeta.scm 189 */
															long BgL_idxz00_3038;

															BgL_idxz00_3038 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3032);
															BgL_test2323z00_5059 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_3038 + 3L)) == BgL_classz00_3030);
														}
													else
														{	/* Reduce/sbeta.scm 189 */
															bool_t BgL_res2145z00_3063;

															{	/* Reduce/sbeta.scm 189 */
																obj_t BgL_oclassz00_3046;

																{	/* Reduce/sbeta.scm 189 */
																	obj_t BgL_arg1815z00_3054;
																	long BgL_arg1816z00_3055;

																	BgL_arg1815z00_3054 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Reduce/sbeta.scm 189 */
																		long BgL_arg1817z00_3056;

																		BgL_arg1817z00_3056 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3032);
																		BgL_arg1816z00_3055 =
																			(BgL_arg1817z00_3056 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_3046 =
																		VECTOR_REF(BgL_arg1815z00_3054,
																		BgL_arg1816z00_3055);
																}
																{	/* Reduce/sbeta.scm 189 */
																	bool_t BgL__ortest_1115z00_3047;

																	BgL__ortest_1115z00_3047 =
																		(BgL_classz00_3030 == BgL_oclassz00_3046);
																	if (BgL__ortest_1115z00_3047)
																		{	/* Reduce/sbeta.scm 189 */
																			BgL_res2145z00_3063 =
																				BgL__ortest_1115z00_3047;
																		}
																	else
																		{	/* Reduce/sbeta.scm 189 */
																			long BgL_odepthz00_3048;

																			{	/* Reduce/sbeta.scm 189 */
																				obj_t BgL_arg1804z00_3049;

																				BgL_arg1804z00_3049 =
																					(BgL_oclassz00_3046);
																				BgL_odepthz00_3048 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_3049);
																			}
																			if ((3L < BgL_odepthz00_3048))
																				{	/* Reduce/sbeta.scm 189 */
																					obj_t BgL_arg1802z00_3051;

																					{	/* Reduce/sbeta.scm 189 */
																						obj_t BgL_arg1803z00_3052;

																						BgL_arg1803z00_3052 =
																							(BgL_oclassz00_3046);
																						BgL_arg1802z00_3051 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_3052, 3L);
																					}
																					BgL_res2145z00_3063 =
																						(BgL_arg1802z00_3051 ==
																						BgL_classz00_3030);
																				}
																			else
																				{	/* Reduce/sbeta.scm 189 */
																					BgL_res2145z00_3063 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2323z00_5059 = BgL_res2145z00_3063;
														}
												}
											}
											if (BgL_test2323z00_5059)
												{	/* Reduce/sbeta.scm 189 */
													if (CBOOL(
															(((BgL_funz00_bglt) COBJECT(
																		((BgL_funz00_bglt)
																			(((BgL_variablez00_bglt) COBJECT(
																						(((BgL_varz00_bglt) COBJECT(
																									(((BgL_appz00_bglt) COBJECT(
																												((BgL_appz00_bglt)
																													BgL_exprz00_1609)))->
																										BgL_funz00)))->
																							BgL_variablez00)))->
																				BgL_valuez00))))->
																BgL_sidezd2effectzd2)))
														{	/* Reduce/sbeta.scm 193 */
															return ((bool_t) 0);
														}
													else
														{	/* Reduce/sbeta.scm 194 */
															obj_t BgL_g1305z00_1641;

															BgL_g1305z00_1641 =
																(((BgL_appz00_bglt) COBJECT(
																		((BgL_appz00_bglt) BgL_exprz00_1609)))->
																BgL_argsz00);
															{
																obj_t BgL_l1303z00_1643;

																BgL_l1303z00_1643 = BgL_g1305z00_1641;
															BgL_zc3z04anonymousza31691ze3z87_1644:
																if (NULLP(BgL_l1303z00_1643))
																	{	/* Reduce/sbeta.scm 194 */
																		return ((bool_t) 1);
																	}
																else
																	{	/* Reduce/sbeta.scm 194 */
																		bool_t BgL_test2329z00_5095;

																		{	/* Reduce/sbeta.scm 194 */
																			obj_t BgL_arg1700z00_1649;

																			BgL_arg1700z00_1649 =
																				CAR(((obj_t) BgL_l1303z00_1643));
																			BgL_test2329z00_5095 =
																				BGl_sidezd2effectzd2safezf3zf3zzreduce_betaz00
																				(BgL_arg1700z00_1649);
																		}
																		if (BgL_test2329z00_5095)
																			{	/* Reduce/sbeta.scm 194 */
																				obj_t BgL_arg1699z00_1648;

																				BgL_arg1699z00_1648 =
																					CDR(((obj_t) BgL_l1303z00_1643));
																				{
																					obj_t BgL_l1303z00_5101;

																					BgL_l1303z00_5101 =
																						BgL_arg1699z00_1648;
																					BgL_l1303z00_1643 = BgL_l1303z00_5101;
																					goto
																						BgL_zc3z04anonymousza31691ze3z87_1644;
																				}
																			}
																		else
																			{	/* Reduce/sbeta.scm 194 */
																				return ((bool_t) 0);
																			}
																	}
															}
														}
												}
											else
												{	/* Reduce/sbeta.scm 189 */
													return ((bool_t) 0);
												}
										}
								}
						}
				}
			}
		}

	}



/* predicate? */
	obj_t BGl_predicatezf3zf3zzreduce_betaz00(BgL_appz00_bglt BgL_nodez00_32)
	{
		{	/* Reduce/sbeta.scm 449 */
			{	/* Reduce/sbeta.scm 451 */
				bool_t BgL_test2330z00_5102;

				{	/* Reduce/sbeta.scm 451 */
					obj_t BgL_tmpz00_5103;

					BgL_tmpz00_5103 =
						(((BgL_appz00_bglt) COBJECT(BgL_nodez00_32))->BgL_argsz00);
					BgL_test2330z00_5102 = PAIRP(BgL_tmpz00_5103);
				}
				if (BgL_test2330z00_5102)
					{	/* Reduce/sbeta.scm 451 */
						if (NULLP(CDR(
									(((BgL_appz00_bglt) COBJECT(BgL_nodez00_32))->BgL_argsz00))))
							{	/* Reduce/sbeta.scm 453 */
								bool_t BgL_test2332z00_5110;

								{	/* Reduce/sbeta.scm 453 */
									bool_t BgL__ortest_1150z00_1665;

									{	/* Reduce/sbeta.scm 453 */
										bool_t BgL_test2333z00_5111;

										{	/* Reduce/sbeta.scm 453 */
											obj_t BgL_arg1720z00_1671;

											BgL_arg1720z00_1671 =
												CAR(
												(((BgL_appz00_bglt) COBJECT(BgL_nodez00_32))->
													BgL_argsz00));
											BgL_test2333z00_5111 =
												BGl_sidezd2effectzf3z21zzeffect_effectz00((
													(BgL_nodez00_bglt) BgL_arg1720z00_1671));
										}
										if (BgL_test2333z00_5111)
											{	/* Reduce/sbeta.scm 453 */
												BgL__ortest_1150z00_1665 = ((bool_t) 0);
											}
										else
											{	/* Reduce/sbeta.scm 453 */
												BgL__ortest_1150z00_1665 = ((bool_t) 1);
											}
									}
									if (BgL__ortest_1150z00_1665)
										{	/* Reduce/sbeta.scm 453 */
											BgL_test2332z00_5110 = BgL__ortest_1150z00_1665;
										}
									else
										{	/* Reduce/sbeta.scm 453 */
											BgL_test2332z00_5110 =
												BGl_sidezd2effectzd2safezf3zf3zzreduce_betaz00(CAR(
													(((BgL_appz00_bglt) COBJECT(BgL_nodez00_32))->
														BgL_argsz00)));
										}
								}
								if (BgL_test2332z00_5110)
									{	/* Reduce/sbeta.scm 455 */
										BgL_variablez00_bglt BgL_arg1709z00_1663;

										BgL_arg1709z00_1663 =
											(((BgL_varz00_bglt) COBJECT(
													(((BgL_appz00_bglt) COBJECT(BgL_nodez00_32))->
														BgL_funz00)))->BgL_variablez00);
										return BGl_memqz00zz__r4_pairs_and_lists_6_3z00(((obj_t)
												BgL_arg1709z00_1663),
											BGl_za2predicatesza2z00zzreduce_betaz00);
									}
								else
									{	/* Reduce/sbeta.scm 453 */
										return BFALSE;
									}
							}
						else
							{	/* Reduce/sbeta.scm 452 */
								return BFALSE;
							}
					}
				else
					{	/* Reduce/sbeta.scm 451 */
						return BFALSE;
					}
			}
		}

	}



/* node-beta-predicate! */
	obj_t BGl_nodezd2betazd2predicatez12z12zzreduce_betaz00(BgL_appz00_bglt
		BgL_nodez00_33)
	{
		{	/* Reduce/sbeta.scm 460 */
			{	/* Reduce/sbeta.scm 462 */
				BgL_variablez00_bglt BgL_vfunz00_1677;
				BgL_typez00_bglt BgL_atypez00_1678;

				BgL_vfunz00_1677 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(BgL_nodez00_33))->BgL_funz00)))->
					BgL_variablez00);
				{	/* Reduce/sbeta.scm 463 */
					obj_t BgL_arg1739z00_1690;

					BgL_arg1739z00_1690 =
						CAR((((BgL_appz00_bglt) COBJECT(BgL_nodez00_33))->BgL_argsz00));
					BgL_atypez00_1678 =
						BGl_getzd2typezd2zztype_typeofz00(
						((BgL_nodez00_bglt) BgL_arg1739z00_1690), ((bool_t) 0));
				}
				if ((((obj_t) BgL_atypez00_1678) == BGl_za2objza2z00zztype_cachez00))
					{	/* Reduce/sbeta.scm 465 */
						return ((obj_t) BgL_nodez00_33);
					}
				else
					{	/* Reduce/sbeta.scm 465 */
						if (
							(((obj_t) BgL_vfunz00_1677) ==
								BGl_za2z42fixnumzf3za2zb1zzreduce_betaz00))
							{	/* Reduce/sbeta.scm 467 */
								BGl_za2removedza2z00zzreduce_betaz00 =
									(BGl_za2removedza2z00zzreduce_betaz00 + 1L);
								{	/* Reduce/sbeta.scm 469 */
									BgL_literalz00_bglt BgL_new1153z00_1679;

									{	/* Reduce/sbeta.scm 471 */
										BgL_literalz00_bglt BgL_new1152z00_1682;

										BgL_new1152z00_1682 =
											((BgL_literalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_literalz00_bgl))));
										{	/* Reduce/sbeta.scm 471 */
											long BgL_arg1735z00_1683;

											BgL_arg1735z00_1683 =
												BGL_CLASS_NUM(BGl_literalz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1152z00_1682),
												BgL_arg1735z00_1683);
										}
										{	/* Reduce/sbeta.scm 471 */
											BgL_objectz00_bglt BgL_tmpz00_5142;

											BgL_tmpz00_5142 =
												((BgL_objectz00_bglt) BgL_new1152z00_1682);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5142, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1152z00_1682);
										BgL_new1153z00_1679 = BgL_new1152z00_1682;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1153z00_1679)))->
											BgL_locz00) =
										((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
															BgL_nodez00_33)))->BgL_locz00)), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1153z00_1679)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_nodez00_bglt)
													COBJECT(((BgL_nodez00_bglt) BgL_nodez00_33)))->
												BgL_typez00)), BUNSPEC);
									{
										obj_t BgL_auxz00_5154;

										{	/* Reduce/sbeta.scm 472 */
											bool_t BgL__ortest_1154z00_1680;

											BgL__ortest_1154z00_1680 =
												(
												((obj_t) BgL_atypez00_1678) ==
												BGl_za2bintza2z00zztype_cachez00);
											if (BgL__ortest_1154z00_1680)
												{	/* Reduce/sbeta.scm 472 */
													BgL_auxz00_5154 = BBOOL(BgL__ortest_1154z00_1680);
												}
											else
												{	/* Reduce/sbeta.scm 473 */
													bool_t BgL__ortest_1155z00_1681;

													BgL__ortest_1155z00_1681 =
														(
														((obj_t) BgL_atypez00_1678) ==
														BGl_za2intza2z00zztype_cachez00);
													if (BgL__ortest_1155z00_1681)
														{	/* Reduce/sbeta.scm 473 */
															BgL_auxz00_5154 = BBOOL(BgL__ortest_1155z00_1681);
														}
													else
														{	/* Reduce/sbeta.scm 473 */
															BgL_auxz00_5154 =
																BBOOL(
																(((obj_t) BgL_atypez00_1678) ==
																	BGl_za2longza2z00zztype_cachez00));
														}
												}
										}
										((((BgL_atomz00_bglt) COBJECT(
														((BgL_atomz00_bglt) BgL_new1153z00_1679)))->
												BgL_valuez00) = ((obj_t) BgL_auxz00_5154), BUNSPEC);
									}
									return ((obj_t) BgL_new1153z00_1679);
								}
							}
						else
							{	/* Reduce/sbeta.scm 467 */
								if (
									(((obj_t) BgL_vfunz00_1677) ==
										BGl_za2z42flonumzf3za2zb1zzreduce_betaz00))
									{	/* Reduce/sbeta.scm 475 */
										BGl_za2removedza2z00zzreduce_betaz00 =
											(BGl_za2removedza2z00zzreduce_betaz00 + 1L);
										{	/* Reduce/sbeta.scm 477 */
											BgL_literalz00_bglt BgL_new1157z00_1684;

											{	/* Reduce/sbeta.scm 479 */
												BgL_literalz00_bglt BgL_new1156z00_1686;

												BgL_new1156z00_1686 =
													((BgL_literalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_literalz00_bgl))));
												{	/* Reduce/sbeta.scm 479 */
													long BgL_arg1736z00_1687;

													BgL_arg1736z00_1687 =
														BGL_CLASS_NUM(BGl_literalz00zzast_nodez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1156z00_1686),
														BgL_arg1736z00_1687);
												}
												{	/* Reduce/sbeta.scm 479 */
													BgL_objectz00_bglt BgL_tmpz00_5177;

													BgL_tmpz00_5177 =
														((BgL_objectz00_bglt) BgL_new1156z00_1686);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5177, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1156z00_1686);
												BgL_new1157z00_1684 = BgL_new1156z00_1686;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1157z00_1684)))->
													BgL_locz00) =
												((obj_t) (((BgL_nodez00_bglt)
															COBJECT(((BgL_nodez00_bglt) BgL_nodez00_33)))->
														BgL_locz00)), BUNSPEC);
											((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_new1157z00_1684)))->BgL_typez00) =
												((BgL_typez00_bglt) (((BgL_nodez00_bglt)
															COBJECT(((BgL_nodez00_bglt) BgL_nodez00_33)))->
														BgL_typez00)), BUNSPEC);
											{
												obj_t BgL_auxz00_5189;

												{	/* Reduce/sbeta.scm 480 */
													bool_t BgL__ortest_1158z00_1685;

													BgL__ortest_1158z00_1685 =
														(
														((obj_t) BgL_atypez00_1678) ==
														BGl_za2realza2z00zztype_cachez00);
													if (BgL__ortest_1158z00_1685)
														{	/* Reduce/sbeta.scm 480 */
															BgL_auxz00_5189 = BBOOL(BgL__ortest_1158z00_1685);
														}
													else
														{	/* Reduce/sbeta.scm 480 */
															BgL_auxz00_5189 =
																BBOOL(
																(((obj_t) BgL_atypez00_1678) ==
																	BGl_za2brealza2z00zztype_cachez00));
														}
												}
												((((BgL_atomz00_bglt) COBJECT(
																((BgL_atomz00_bglt) BgL_new1157z00_1684)))->
														BgL_valuez00) = ((obj_t) BgL_auxz00_5189), BUNSPEC);
											}
											return ((obj_t) BgL_new1157z00_1684);
										}
									}
								else
									{	/* Reduce/sbeta.scm 485 */
										obj_t BgL_arg1737z00_1688;

										BgL_arg1737z00_1688 =
											BGl_shapez00zztools_shapez00(((obj_t) BgL_nodez00_33));
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_string2179z00zzreduce_betaz00,
											BGl_string2180z00zzreduce_betaz00, BgL_arg1737z00_1688);
									}
							}
					}
			}
		}

	}



/* node-beta*! */
	obj_t BGl_nodezd2betaza2z12z62zzreduce_betaz00(obj_t BgL_nodesz00_34)
	{
		{	/* Reduce/sbeta.scm 490 */
			{
				obj_t BgL_nodesz00_1693;

				BgL_nodesz00_1693 = BgL_nodesz00_34;
			BgL_zc3z04anonymousza31741ze3z87_1694:
				if (NULLP(BgL_nodesz00_1693))
					{	/* Reduce/sbeta.scm 492 */
						return BUNSPEC;
					}
				else
					{	/* Reduce/sbeta.scm 492 */
						{	/* Reduce/sbeta.scm 495 */
							BgL_nodez00_bglt BgL_arg1746z00_1696;

							{	/* Reduce/sbeta.scm 495 */
								obj_t BgL_arg1747z00_1697;

								BgL_arg1747z00_1697 = CAR(((obj_t) BgL_nodesz00_1693));
								BgL_arg1746z00_1696 =
									BGl_nodezd2betaz12zc0zzreduce_betaz00(
									((BgL_nodez00_bglt) BgL_arg1747z00_1697));
							}
							{	/* Reduce/sbeta.scm 495 */
								obj_t BgL_auxz00_5211;
								obj_t BgL_tmpz00_5209;

								BgL_auxz00_5211 = ((obj_t) BgL_arg1746z00_1696);
								BgL_tmpz00_5209 = ((obj_t) BgL_nodesz00_1693);
								SET_CAR(BgL_tmpz00_5209, BgL_auxz00_5211);
							}
						}
						{	/* Reduce/sbeta.scm 496 */
							obj_t BgL_arg1748z00_1698;

							BgL_arg1748z00_1698 = CDR(((obj_t) BgL_nodesz00_1693));
							{
								obj_t BgL_nodesz00_5216;

								BgL_nodesz00_5216 = BgL_arg1748z00_1698;
								BgL_nodesz00_1693 = BgL_nodesz00_5216;
								goto BgL_zc3z04anonymousza31741ze3z87_1694;
							}
						}
					}
			}
		}

	}



/* node-beta-early-app! */
	BgL_nodez00_bglt
		BGl_nodezd2betazd2earlyzd2appz12zc0zzreduce_betaz00(BgL_appz00_bglt
		BgL_nodez00_35)
	{
		{	/* Reduce/sbeta.scm 504 */
			{	/* Reduce/sbeta.scm 506 */
				bool_t BgL_test2342z00_5217;

				{	/* Reduce/sbeta.scm 506 */
					obj_t BgL_g1358z00_1738;

					BgL_g1358z00_1738 =
						(((BgL_appz00_bglt) COBJECT(BgL_nodez00_35))->BgL_argsz00);
					{
						obj_t BgL_l1356z00_1740;

						BgL_l1356z00_1740 = BgL_g1358z00_1738;
					BgL_zc3z04anonymousza31833ze3z87_1741:
						if (NULLP(BgL_l1356z00_1740))
							{	/* Reduce/sbeta.scm 506 */
								BgL_test2342z00_5217 = ((bool_t) 1);
							}
						else
							{	/* Reduce/sbeta.scm 506 */
								bool_t BgL_test2344z00_5221;

								{	/* Reduce/sbeta.scm 506 */
									obj_t BgL_arg1835z00_1746;

									BgL_arg1835z00_1746 = CAR(((obj_t) BgL_l1356z00_1740));
									{	/* Reduce/sbeta.scm 506 */
										obj_t BgL_classz00_3128;

										BgL_classz00_3128 = BGl_atomz00zzast_nodez00;
										if (BGL_OBJECTP(BgL_arg1835z00_1746))
											{	/* Reduce/sbeta.scm 506 */
												BgL_objectz00_bglt BgL_arg1807z00_3130;

												BgL_arg1807z00_3130 =
													(BgL_objectz00_bglt) (BgL_arg1835z00_1746);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Reduce/sbeta.scm 506 */
														long BgL_idxz00_3136;

														BgL_idxz00_3136 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3130);
														BgL_test2344z00_5221 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3136 + 2L)) == BgL_classz00_3128);
													}
												else
													{	/* Reduce/sbeta.scm 506 */
														bool_t BgL_res2147z00_3161;

														{	/* Reduce/sbeta.scm 506 */
															obj_t BgL_oclassz00_3144;

															{	/* Reduce/sbeta.scm 506 */
																obj_t BgL_arg1815z00_3152;
																long BgL_arg1816z00_3153;

																BgL_arg1815z00_3152 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Reduce/sbeta.scm 506 */
																	long BgL_arg1817z00_3154;

																	BgL_arg1817z00_3154 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3130);
																	BgL_arg1816z00_3153 =
																		(BgL_arg1817z00_3154 - OBJECT_TYPE);
																}
																BgL_oclassz00_3144 =
																	VECTOR_REF(BgL_arg1815z00_3152,
																	BgL_arg1816z00_3153);
															}
															{	/* Reduce/sbeta.scm 506 */
																bool_t BgL__ortest_1115z00_3145;

																BgL__ortest_1115z00_3145 =
																	(BgL_classz00_3128 == BgL_oclassz00_3144);
																if (BgL__ortest_1115z00_3145)
																	{	/* Reduce/sbeta.scm 506 */
																		BgL_res2147z00_3161 =
																			BgL__ortest_1115z00_3145;
																	}
																else
																	{	/* Reduce/sbeta.scm 506 */
																		long BgL_odepthz00_3146;

																		{	/* Reduce/sbeta.scm 506 */
																			obj_t BgL_arg1804z00_3147;

																			BgL_arg1804z00_3147 =
																				(BgL_oclassz00_3144);
																			BgL_odepthz00_3146 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3147);
																		}
																		if ((2L < BgL_odepthz00_3146))
																			{	/* Reduce/sbeta.scm 506 */
																				obj_t BgL_arg1802z00_3149;

																				{	/* Reduce/sbeta.scm 506 */
																					obj_t BgL_arg1803z00_3150;

																					BgL_arg1803z00_3150 =
																						(BgL_oclassz00_3144);
																					BgL_arg1802z00_3149 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3150, 2L);
																				}
																				BgL_res2147z00_3161 =
																					(BgL_arg1802z00_3149 ==
																					BgL_classz00_3128);
																			}
																		else
																			{	/* Reduce/sbeta.scm 506 */
																				BgL_res2147z00_3161 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2344z00_5221 = BgL_res2147z00_3161;
													}
											}
										else
											{	/* Reduce/sbeta.scm 506 */
												BgL_test2344z00_5221 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test2344z00_5221)
									{	/* Reduce/sbeta.scm 506 */
										obj_t BgL_arg1834z00_1745;

										BgL_arg1834z00_1745 = CDR(((obj_t) BgL_l1356z00_1740));
										{
											obj_t BgL_l1356z00_5248;

											BgL_l1356z00_5248 = BgL_arg1834z00_1745;
											BgL_l1356z00_1740 = BgL_l1356z00_5248;
											goto BgL_zc3z04anonymousza31833ze3z87_1741;
										}
									}
								else
									{	/* Reduce/sbeta.scm 506 */
										BgL_test2342z00_5217 = ((bool_t) 0);
									}
							}
					}
				}
				if (BgL_test2342z00_5217)
					{	/* Reduce/sbeta.scm 507 */
						bool_t BgL_test2349z00_5249;

						if (
							(bgl_list_length(
									(((BgL_appz00_bglt) COBJECT(BgL_nodez00_35))->BgL_argsz00)) ==
								1L))
							{	/* Reduce/sbeta.scm 507 */
								obj_t BgL_tmpz00_5254;

								{
									BgL_atomz00_bglt BgL_auxz00_5255;

									{	/* Reduce/sbeta.scm 507 */
										obj_t BgL_pairz00_3164;

										BgL_pairz00_3164 =
											(((BgL_appz00_bglt) COBJECT(BgL_nodez00_35))->
											BgL_argsz00);
										BgL_auxz00_5255 =
											((BgL_atomz00_bglt) CAR(BgL_pairz00_3164));
									}
									BgL_tmpz00_5254 =
										(((BgL_atomz00_bglt) COBJECT(BgL_auxz00_5255))->
										BgL_valuez00);
								}
								BgL_test2349z00_5249 = STRINGP(BgL_tmpz00_5254);
							}
						else
							{	/* Reduce/sbeta.scm 507 */
								BgL_test2349z00_5249 = ((bool_t) 0);
							}
						if (BgL_test2349z00_5249)
							{	/* Reduce/sbeta.scm 508 */
								bool_t BgL_test2351z00_5261;

								{	/* Reduce/sbeta.scm 508 */
									BgL_variablez00_bglt BgL_arg1808z00_1730;

									BgL_arg1808z00_1730 =
										(((BgL_varz00_bglt) COBJECT(
												(((BgL_appz00_bglt) COBJECT(BgL_nodez00_35))->
													BgL_funz00)))->BgL_variablez00);
									BgL_test2351z00_5261 =
										(((obj_t) BgL_arg1808z00_1730) ==
										BGl_za2czd2stringzd2lengthza2z00zzreduce_betaz00);
								}
								if (BgL_test2351z00_5261)
									{	/* Reduce/sbeta.scm 509 */
										BgL_literalz00_bglt BgL_new1161z00_1724;

										{	/* Reduce/sbeta.scm 510 */
											BgL_literalz00_bglt BgL_new1160z00_1728;

											BgL_new1160z00_1728 =
												((BgL_literalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_literalz00_bgl))));
											{	/* Reduce/sbeta.scm 510 */
												long BgL_arg1806z00_1729;

												BgL_arg1806z00_1729 =
													BGL_CLASS_NUM(BGl_literalz00zzast_nodez00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1160z00_1728),
													BgL_arg1806z00_1729);
											}
											{	/* Reduce/sbeta.scm 510 */
												BgL_objectz00_bglt BgL_tmpz00_5270;

												BgL_tmpz00_5270 =
													((BgL_objectz00_bglt) BgL_new1160z00_1728);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5270, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1160z00_1728);
											BgL_new1161z00_1724 = BgL_new1160z00_1728;
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1161z00_1724)))->
												BgL_locz00) =
											((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_nodez00_35)))->BgL_locz00)), BUNSPEC);
										((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
															BgL_new1161z00_1724)))->BgL_typez00) =
											((BgL_typez00_bglt) (((BgL_nodez00_bglt)
														COBJECT(((BgL_nodez00_bglt) BgL_nodez00_35)))->
													BgL_typez00)), BUNSPEC);
										{
											obj_t BgL_auxz00_5282;

											{	/* Reduce/sbeta.scm 512 */
												obj_t BgL_stringz00_3175;

												{
													BgL_atomz00_bglt BgL_auxz00_5284;

													{	/* Reduce/sbeta.scm 512 */
														obj_t BgL_pairz00_3173;

														BgL_pairz00_3173 =
															(((BgL_appz00_bglt) COBJECT(BgL_nodez00_35))->
															BgL_argsz00);
														BgL_auxz00_5284 =
															((BgL_atomz00_bglt) CAR(BgL_pairz00_3173));
													}
													BgL_stringz00_3175 =
														(((BgL_atomz00_bglt) COBJECT(BgL_auxz00_5284))->
														BgL_valuez00);
												}
												BgL_auxz00_5282 =
													BINT(STRING_LENGTH(BgL_stringz00_3175));
											}
											((((BgL_atomz00_bglt) COBJECT(
															((BgL_atomz00_bglt) BgL_new1161z00_1724)))->
													BgL_valuez00) = ((obj_t) BgL_auxz00_5282), BUNSPEC);
										}
										return ((BgL_nodez00_bglt) BgL_new1161z00_1724);
									}
								else
									{	/* Reduce/sbeta.scm 508 */
										return ((BgL_nodez00_bglt) BgL_nodez00_35);
									}
							}
						else
							{	/* Reduce/sbeta.scm 507 */
								return ((BgL_nodez00_bglt) BgL_nodez00_35);
							}
					}
				else
					{	/* Reduce/sbeta.scm 506 */
						return ((BgL_nodez00_bglt) BgL_nodez00_35);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzreduce_betaz00(void)
	{
		{	/* Reduce/sbeta.scm 25 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzreduce_betaz00(void)
	{
		{	/* Reduce/sbeta.scm 25 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00,
				BGl_proc2181z00zzreduce_betaz00, BGl_nodez00zzast_nodez00,
				BGl_string2182z00zzreduce_betaz00);
		}

	}



/* &node-beta!1360 */
	obj_t BGl_z62nodezd2betaz121360za2zzreduce_betaz00(obj_t BgL_envz00_4129,
		obj_t BgL_nodez00_4130)
	{
		{	/* Reduce/sbeta.scm 91 */
			return ((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_4130));
		}

	}



/* node-beta! */
	BgL_nodez00_bglt BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_nodez00_bglt
		BgL_nodez00_4)
	{
		{	/* Reduce/sbeta.scm 91 */
			{	/* Reduce/sbeta.scm 91 */
				obj_t BgL_method1362z00_1752;

				{	/* Reduce/sbeta.scm 91 */
					obj_t BgL_res2152z00_3206;

					{	/* Reduce/sbeta.scm 91 */
						long BgL_objzd2classzd2numz00_3177;

						BgL_objzd2classzd2numz00_3177 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_4));
						{	/* Reduce/sbeta.scm 91 */
							obj_t BgL_arg1811z00_3178;

							BgL_arg1811z00_3178 =
								PROCEDURE_REF(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00,
								(int) (1L));
							{	/* Reduce/sbeta.scm 91 */
								int BgL_offsetz00_3181;

								BgL_offsetz00_3181 = (int) (BgL_objzd2classzd2numz00_3177);
								{	/* Reduce/sbeta.scm 91 */
									long BgL_offsetz00_3182;

									BgL_offsetz00_3182 =
										((long) (BgL_offsetz00_3181) - OBJECT_TYPE);
									{	/* Reduce/sbeta.scm 91 */
										long BgL_modz00_3183;

										BgL_modz00_3183 =
											(BgL_offsetz00_3182 >> (int) ((long) ((int) (4L))));
										{	/* Reduce/sbeta.scm 91 */
											long BgL_restz00_3185;

											BgL_restz00_3185 =
												(BgL_offsetz00_3182 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Reduce/sbeta.scm 91 */

												{	/* Reduce/sbeta.scm 91 */
													obj_t BgL_bucketz00_3187;

													BgL_bucketz00_3187 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3178), BgL_modz00_3183);
													BgL_res2152z00_3206 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3187), BgL_restz00_3185);
					}}}}}}}}
					BgL_method1362z00_1752 = BgL_res2152z00_3206;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1362z00_1752, ((obj_t) BgL_nodez00_4)));
			}
		}

	}



/* &node-beta! */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12za2zzreduce_betaz00(obj_t
		BgL_envz00_4131, obj_t BgL_nodez00_4132)
	{
		{	/* Reduce/sbeta.scm 91 */
			return
				BGl_nodezd2betaz12zc0zzreduce_betaz00(
				((BgL_nodez00_bglt) BgL_nodez00_4132));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzreduce_betaz00(void)
	{
		{	/* Reduce/sbeta.scm 25 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2183z00zzreduce_betaz00,
				BGl_string2184z00zzreduce_betaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00,
				BGl_sequencez00zzast_nodez00, BGl_proc2185z00zzreduce_betaz00,
				BGl_string2184z00zzreduce_betaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00, BGl_syncz00zzast_nodez00,
				BGl_proc2186z00zzreduce_betaz00, BGl_string2184z00zzreduce_betaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc2187z00zzreduce_betaz00,
				BGl_string2184z00zzreduce_betaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00,
				BGl_funcallz00zzast_nodez00, BGl_proc2188z00zzreduce_betaz00,
				BGl_string2184z00zzreduce_betaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00, BGl_castz00zzast_nodez00,
				BGl_proc2189z00zzreduce_betaz00, BGl_string2184z00zzreduce_betaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00, BGl_setqz00zzast_nodez00,
				BGl_proc2190z00zzreduce_betaz00, BGl_string2184z00zzreduce_betaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2191z00zzreduce_betaz00,
				BGl_string2184z00zzreduce_betaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00,
				BGl_switchz00zzast_nodez00, BGl_proc2192z00zzreduce_betaz00,
				BGl_string2184z00zzreduce_betaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2193z00zzreduce_betaz00,
				BGl_string2184z00zzreduce_betaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2194z00zzreduce_betaz00,
				BGl_string2184z00zzreduce_betaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2195z00zzreduce_betaz00,
				BGl_string2184z00zzreduce_betaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2196z00zzreduce_betaz00,
				BGl_string2184z00zzreduce_betaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2197z00zzreduce_betaz00,
				BGl_string2184z00zzreduce_betaz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzreduce_betaz00, BGl_appz00zzast_nodez00,
				BGl_proc2198z00zzreduce_betaz00, BGl_string2184z00zzreduce_betaz00);
		}

	}



/* &node-beta!-app1396 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2app1396z70zzreduce_betaz00(obj_t
		BgL_envz00_4148, obj_t BgL_nodez00_4149)
	{
		{	/* Reduce/sbeta.scm 439 */
			BGl_nodezd2betaza2z12z62zzreduce_betaz00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_4149)))->BgL_argsz00));
			if (CBOOL(BGl_predicatezf3zf3zzreduce_betaz00(
						((BgL_appz00_bglt) BgL_nodez00_4149))))
				{	/* Reduce/sbeta.scm 442 */
					return
						((BgL_nodez00_bglt)
						BGl_nodezd2betazd2predicatez12z12zzreduce_betaz00(
							((BgL_appz00_bglt) BgL_nodez00_4149)));
				}
			else
				{	/* Reduce/sbeta.scm 442 */
					return
						BGl_nodezd2betazd2earlyzd2appz12zc0zzreduce_betaz00(
						((BgL_appz00_bglt) BgL_nodez00_4149));
				}
		}

	}



/* &node-beta!-box-set!1394 */
	BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2boxzd2setz121394zb0zzreduce_betaz00(obj_t
		BgL_envz00_4150, obj_t BgL_nodez00_4151)
	{
		{	/* Reduce/sbeta.scm 431 */
			{
				BgL_nodez00_bglt BgL_auxz00_5359;

				{	/* Reduce/sbeta.scm 433 */
					BgL_nodez00_bglt BgL_arg2067z00_4192;

					BgL_arg2067z00_4192 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_4151)))->BgL_valuez00);
					BgL_auxz00_5359 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2067z00_4192);
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_4151)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_5359), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_4151));
		}

	}



/* &node-beta!-make-box1392 */
	BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2makezd2box1392za2zzreduce_betaz00(obj_t
		BgL_envz00_4152, obj_t BgL_nodez00_4153)
	{
		{	/* Reduce/sbeta.scm 423 */
			{
				BgL_nodez00_bglt BgL_auxz00_5367;

				{	/* Reduce/sbeta.scm 425 */
					BgL_nodez00_bglt BgL_arg2065z00_4194;

					BgL_arg2065z00_4194 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_4153)))->BgL_valuez00);
					BgL_auxz00_5367 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2065z00_4194);
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_4153)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_5367), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_4153));
		}

	}



/* &node-beta!-jump-ex-i1390 */
	BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2jumpzd2exzd2i1390z70zzreduce_betaz00(obj_t
		BgL_envz00_4154, obj_t BgL_nodez00_4155)
	{
		{	/* Reduce/sbeta.scm 414 */
			{
				BgL_nodez00_bglt BgL_auxz00_5375;

				{	/* Reduce/sbeta.scm 416 */
					BgL_nodez00_bglt BgL_arg2063z00_4196;

					BgL_arg2063z00_4196 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_4155)))->BgL_exitz00);
					BgL_auxz00_5375 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2063z00_4196);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_4155)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_5375), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_5381;

				{	/* Reduce/sbeta.scm 417 */
					BgL_nodez00_bglt BgL_arg2064z00_4197;

					BgL_arg2064z00_4197 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_4155)))->
						BgL_valuez00);
					BgL_auxz00_5381 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2064z00_4197);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_4155)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_5381), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_4155));
		}

	}



/* &node-beta!-set-ex-it1388 */
	BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2setzd2exzd2it1388z70zzreduce_betaz00(obj_t
		BgL_envz00_4156, obj_t BgL_nodez00_4157)
	{
		{	/* Reduce/sbeta.scm 405 */
			{
				BgL_nodez00_bglt BgL_auxz00_5389;

				{	/* Reduce/sbeta.scm 407 */
					BgL_nodez00_bglt BgL_arg2061z00_4199;

					BgL_arg2061z00_4199 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4157)))->BgL_bodyz00);
					BgL_auxz00_5389 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2061z00_4199);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4157)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5389), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_5395;

				{	/* Reduce/sbeta.scm 408 */
					BgL_nodez00_bglt BgL_arg2062z00_4200;

					BgL_arg2062z00_4200 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4157)))->
						BgL_onexitz00);
					BgL_auxz00_5395 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2062z00_4200);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4157)))->
						BgL_onexitz00) = ((BgL_nodez00_bglt) BgL_auxz00_5395), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4157));
		}

	}



/* &node-beta!-let-fun1386 */
	BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2letzd2fun1386za2zzreduce_betaz00(obj_t
		BgL_envz00_4158, obj_t BgL_nodez00_4159)
	{
		{	/* Reduce/sbeta.scm 391 */
			{
				BgL_nodez00_bglt BgL_auxz00_5403;

				{	/* Reduce/sbeta.scm 393 */
					BgL_nodez00_bglt BgL_arg2052z00_4202;

					BgL_arg2052z00_4202 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_4159)))->BgL_bodyz00);
					BgL_auxz00_5403 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2052z00_4202);
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_4159)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5403), BUNSPEC);
			}
			{
				obj_t BgL_localsz00_4204;

				{	/* Reduce/sbeta.scm 394 */
					obj_t BgL_arg2055z00_4210;

					BgL_arg2055z00_4210 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_4159)))->BgL_localsz00);
					{
						BgL_letzd2funzd2_bglt BgL_auxz00_5411;

						BgL_localsz00_4204 = BgL_arg2055z00_4210;
					BgL_loopz00_4203:
						if (NULLP(BgL_localsz00_4204))
							{	/* Reduce/sbeta.scm 395 */
								BgL_auxz00_5411 = ((BgL_letzd2funzd2_bglt) BgL_nodez00_4159);
							}
						else
							{	/* Reduce/sbeta.scm 397 */
								obj_t BgL_localz00_4205;

								BgL_localz00_4205 = CAR(((obj_t) BgL_localsz00_4204));
								{	/* Reduce/sbeta.scm 397 */
									BgL_valuez00_bglt BgL_sfunz00_4206;

									BgL_sfunz00_4206 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_4205))))->
										BgL_valuez00);
									{	/* Reduce/sbeta.scm 398 */

										{	/* Reduce/sbeta.scm 399 */
											BgL_nodez00_bglt BgL_arg2058z00_4207;

											{	/* Reduce/sbeta.scm 399 */
												obj_t BgL_arg2059z00_4208;

												BgL_arg2059z00_4208 =
													(((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_sfunz00_4206)))->
													BgL_bodyz00);
												BgL_arg2058z00_4207 =
													BGl_nodezd2betaz12zc0zzreduce_betaz00((
														(BgL_nodez00_bglt) BgL_arg2059z00_4208));
											}
											((((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_sfunz00_4206)))->
													BgL_bodyz00) =
												((obj_t) ((obj_t) BgL_arg2058z00_4207)), BUNSPEC);
										}
										{	/* Reduce/sbeta.scm 400 */
											obj_t BgL_arg2060z00_4209;

											BgL_arg2060z00_4209 = CDR(((obj_t) BgL_localsz00_4204));
											{
												obj_t BgL_localsz00_5429;

												BgL_localsz00_5429 = BgL_arg2060z00_4209;
												BgL_localsz00_4204 = BgL_localsz00_5429;
												goto BgL_loopz00_4203;
											}
										}
									}
								}
							}
						return ((BgL_nodez00_bglt) BgL_auxz00_5411);
					}
				}
			}
		}

	}



/* &node-beta!-switch1384 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2switch1384z70zzreduce_betaz00(obj_t
		BgL_envz00_4160, obj_t BgL_nodez00_4161)
	{
		{	/* Reduce/sbeta.scm 378 */
			{
				BgL_nodez00_bglt BgL_auxz00_5431;

				{	/* Reduce/sbeta.scm 380 */
					BgL_nodez00_bglt BgL_arg2045z00_4212;

					BgL_arg2045z00_4212 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_4161)))->BgL_testz00);
					BgL_auxz00_5431 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2045z00_4212);
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_4161)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5431), BUNSPEC);
			}
			{
				obj_t BgL_clausesz00_4214;

				{	/* Reduce/sbeta.scm 381 */
					obj_t BgL_arg2046z00_4219;

					BgL_arg2046z00_4219 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_4161)))->BgL_clausesz00);
					{
						BgL_switchz00_bglt BgL_auxz00_5439;

						BgL_clausesz00_4214 = BgL_arg2046z00_4219;
					BgL_loopz00_4213:
						if (NULLP(BgL_clausesz00_4214))
							{	/* Reduce/sbeta.scm 382 */
								BgL_auxz00_5439 = ((BgL_switchz00_bglt) BgL_nodez00_4161);
							}
						else
							{	/* Reduce/sbeta.scm 384 */
								obj_t BgL_clausez00_4215;

								BgL_clausez00_4215 = CAR(((obj_t) BgL_clausesz00_4214));
								{	/* Reduce/sbeta.scm 385 */
									BgL_nodez00_bglt BgL_arg2049z00_4216;

									{	/* Reduce/sbeta.scm 385 */
										obj_t BgL_arg2050z00_4217;

										BgL_arg2050z00_4217 = CDR(((obj_t) BgL_clausez00_4215));
										BgL_arg2049z00_4216 =
											BGl_nodezd2betaz12zc0zzreduce_betaz00(
											((BgL_nodez00_bglt) BgL_arg2050z00_4217));
									}
									{	/* Reduce/sbeta.scm 385 */
										obj_t BgL_auxz00_5451;
										obj_t BgL_tmpz00_5449;

										BgL_auxz00_5451 = ((obj_t) BgL_arg2049z00_4216);
										BgL_tmpz00_5449 = ((obj_t) BgL_clausez00_4215);
										SET_CDR(BgL_tmpz00_5449, BgL_auxz00_5451);
									}
								}
								{	/* Reduce/sbeta.scm 386 */
									obj_t BgL_arg2051z00_4218;

									BgL_arg2051z00_4218 = CDR(((obj_t) BgL_clausesz00_4214));
									{
										obj_t BgL_clausesz00_5456;

										BgL_clausesz00_5456 = BgL_arg2051z00_4218;
										BgL_clausesz00_4214 = BgL_clausesz00_5456;
										goto BgL_loopz00_4213;
									}
								}
							}
						return ((BgL_nodez00_bglt) BgL_auxz00_5439);
					}
				}
			}
		}

	}



/* &node-beta!-condition1382 */
	BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2condition1382z70zzreduce_betaz00(obj_t
		BgL_envz00_4162, obj_t BgL_nodez00_4163)
	{
		{	/* Reduce/sbeta.scm 368 */
			{
				BgL_nodez00_bglt BgL_auxz00_5458;

				{	/* Reduce/sbeta.scm 370 */
					BgL_nodez00_bglt BgL_arg2041z00_4221;

					BgL_arg2041z00_4221 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_4163)))->BgL_testz00);
					BgL_auxz00_5458 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2041z00_4221);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_4163)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5458), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_5464;

				{	/* Reduce/sbeta.scm 371 */
					BgL_nodez00_bglt BgL_arg2042z00_4222;

					BgL_arg2042z00_4222 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_4163)))->BgL_truez00);
					BgL_auxz00_5464 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2042z00_4222);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_4163)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_5464), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_5470;

				{	/* Reduce/sbeta.scm 372 */
					BgL_nodez00_bglt BgL_arg2044z00_4223;

					BgL_arg2044z00_4223 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_4163)))->BgL_falsez00);
					BgL_auxz00_5470 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2044z00_4223);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_4163)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_5470), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_4163));
		}

	}



/* &node-beta!-setq1380 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2setq1380z70zzreduce_betaz00(obj_t
		BgL_envz00_4164, obj_t BgL_nodez00_4165)
	{
		{	/* Reduce/sbeta.scm 360 */
			{
				BgL_nodez00_bglt BgL_auxz00_5478;

				{	/* Reduce/sbeta.scm 362 */
					BgL_nodez00_bglt BgL_arg2040z00_4225;

					BgL_arg2040z00_4225 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_4165)))->BgL_valuez00);
					BgL_auxz00_5478 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2040z00_4225);
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_4165)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_5478), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_4165));
		}

	}



/* &node-beta!-cast1377 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2cast1377z70zzreduce_betaz00(obj_t
		BgL_envz00_4166, obj_t BgL_nodez00_4167)
	{
		{	/* Reduce/sbeta.scm 352 */
			{
				BgL_nodez00_bglt BgL_auxz00_5486;

				{	/* Reduce/sbeta.scm 354 */
					BgL_nodez00_bglt BgL_arg2039z00_4227;

					BgL_arg2039z00_4227 =
						(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_4167)))->BgL_argz00);
					BgL_auxz00_5486 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2039z00_4227);
				}
				((((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_4167)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5486), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_4167));
		}

	}



/* &node-beta!-funcall1374 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2funcall1374z70zzreduce_betaz00(obj_t
		BgL_envz00_4168, obj_t BgL_nodez00_4169)
	{
		{	/* Reduce/sbeta.scm 344 */
			BGl_nodezd2betaza2z12z62zzreduce_betaz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_4169)))->BgL_argsz00));
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_4169));
		}

	}



/* &node-beta!-app-ly1371 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2appzd2ly1371za2zzreduce_betaz00(obj_t
		BgL_envz00_4170, obj_t BgL_nodez00_4171)
	{
		{	/* Reduce/sbeta.scm 335 */
			{
				BgL_nodez00_bglt BgL_auxz00_5499;

				{	/* Reduce/sbeta.scm 337 */
					BgL_nodez00_bglt BgL_arg2036z00_4230;

					BgL_arg2036z00_4230 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_4171)))->BgL_funz00);
					BgL_auxz00_5499 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2036z00_4230);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_4171)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5499), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_5505;

				{	/* Reduce/sbeta.scm 338 */
					BgL_nodez00_bglt BgL_arg2037z00_4231;

					BgL_arg2037z00_4231 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_4171)))->BgL_argz00);
					BgL_auxz00_5505 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2037z00_4231);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_4171)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5505), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_4171));
		}

	}



/* &node-beta!-sync1368 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2sync1368z70zzreduce_betaz00(obj_t
		BgL_envz00_4172, obj_t BgL_nodez00_4173)
	{
		{	/* Reduce/sbeta.scm 325 */
			{
				BgL_nodez00_bglt BgL_auxz00_5513;

				{	/* Reduce/sbeta.scm 327 */
					BgL_nodez00_bglt BgL_arg2031z00_4233;

					BgL_arg2031z00_4233 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_4173)))->BgL_mutexz00);
					BgL_auxz00_5513 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2031z00_4233);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_4173)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5513), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_5519;

				{	/* Reduce/sbeta.scm 328 */
					BgL_nodez00_bglt BgL_arg2033z00_4234;

					BgL_arg2033z00_4234 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_4173)))->BgL_prelockz00);
					BgL_auxz00_5519 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2033z00_4234);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_4173)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5519), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_5525;

				{	/* Reduce/sbeta.scm 329 */
					BgL_nodez00_bglt BgL_arg2034z00_4235;

					BgL_arg2034z00_4235 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_4173)))->BgL_bodyz00);
					BgL_auxz00_5525 =
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_arg2034z00_4235);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_4173)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5525), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_4173));
		}

	}



/* &node-beta!-sequence1366 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2sequence1366z70zzreduce_betaz00(obj_t
		BgL_envz00_4174, obj_t BgL_nodez00_4175)
	{
		{	/* Reduce/sbeta.scm 317 */
			BGl_nodezd2betaza2z12z62zzreduce_betaz00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_4175)))->BgL_nodesz00));
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_4175));
		}

	}



/* &node-beta!-let-var1364 */
	BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2letzd2var1364za2zzreduce_betaz00(obj_t
		BgL_envz00_4176, obj_t BgL_nodez00_4177)
	{
		{	/* Reduce/sbeta.scm 224 */
			{	/* Reduce/sbeta.scm 232 */
				BgL_nodez00_bglt BgL_abodyz00_4238;

				BgL_abodyz00_4238 =
					BGl_findzd2actualzd2expressionz00zzreduce_betaz00(
					(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_4177)))->BgL_bodyz00));
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_4177)))->BgL_bodyz00) =
					((BgL_nodez00_bglt)
						BGl_nodezd2betaz12zc0zzreduce_betaz00(BgL_abodyz00_4238)), BUNSPEC);
				{	/* Reduce/sbeta.scm 236 */
					obj_t BgL_g1321z00_4239;

					BgL_g1321z00_4239 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_4177)))->BgL_bindingsz00);
					{
						obj_t BgL_l1319z00_4241;

						BgL_l1319z00_4241 = BgL_g1321z00_4239;
					BgL_zc3z04anonymousza31837ze3z87_4240:
						if (PAIRP(BgL_l1319z00_4241))
							{	/* Reduce/sbeta.scm 236 */
								{	/* Reduce/sbeta.scm 237 */
									obj_t BgL_bindingz00_4242;

									BgL_bindingz00_4242 = CAR(BgL_l1319z00_4241);
									{	/* Reduce/sbeta.scm 237 */
										BgL_nodez00_bglt BgL_arg1839z00_4243;

										{	/* Reduce/sbeta.scm 237 */
											obj_t BgL_arg1840z00_4244;

											BgL_arg1840z00_4244 = CDR(((obj_t) BgL_bindingz00_4242));
											BgL_arg1839z00_4243 =
												BGl_nodezd2betaz12zc0zzreduce_betaz00(
												((BgL_nodez00_bglt) BgL_arg1840z00_4244));
										}
										{	/* Reduce/sbeta.scm 237 */
											obj_t BgL_auxz00_5555;
											obj_t BgL_tmpz00_5553;

											BgL_auxz00_5555 = ((obj_t) BgL_arg1839z00_4243);
											BgL_tmpz00_5553 = ((obj_t) BgL_bindingz00_4242);
											SET_CDR(BgL_tmpz00_5553, BgL_auxz00_5555);
										}
									}
								}
								{
									obj_t BgL_l1319z00_5558;

									BgL_l1319z00_5558 = CDR(BgL_l1319z00_4241);
									BgL_l1319z00_4241 = BgL_l1319z00_5558;
									goto BgL_zc3z04anonymousza31837ze3z87_4240;
								}
							}
						else
							{	/* Reduce/sbeta.scm 236 */
								((bool_t) 1);
							}
					}
				}
				if (
					(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_4177)))->
						BgL_removablezf3zf3))
					{	/* Reduce/sbeta.scm 241 */
						if (NULLP(
								(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_nodez00_4177)))->
									BgL_bindingsz00)))
							{	/* Reduce/sbeta.scm 243 */
								return BgL_abodyz00_4238;
							}
						else
							{	/* Reduce/sbeta.scm 245 */
								bool_t BgL_test2358z00_5567;

								{	/* Reduce/sbeta.scm 245 */
									bool_t BgL_test2359z00_5568;

									{	/* Reduce/sbeta.scm 245 */
										obj_t BgL_g1324z00_4245;

										BgL_g1324z00_4245 =
											(((BgL_letzd2varzd2_bglt) COBJECT(
													((BgL_letzd2varzd2_bglt) BgL_nodez00_4177)))->
											BgL_bindingsz00);
										{
											obj_t BgL_l1322z00_4247;

											BgL_l1322z00_4247 = BgL_g1324z00_4245;
										BgL_zc3z04anonymousza32004ze3z87_4246:
											if (NULLP(BgL_l1322z00_4247))
												{	/* Reduce/sbeta.scm 245 */
													BgL_test2359z00_5568 = ((bool_t) 0);
												}
											else
												{	/* Reduce/sbeta.scm 249 */
													bool_t BgL__ortest_1325z00_4248;

													{	/* Reduce/sbeta.scm 249 */
														obj_t BgL_bz00_4249;

														BgL_bz00_4249 = CAR(((obj_t) BgL_l1322z00_4247));
														{	/* Reduce/sbeta.scm 249 */
															bool_t BgL__ortest_1125z00_4250;

															BgL__ortest_1125z00_4250 =
																(
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					CAR(
																						((obj_t) BgL_bz00_4249))))))->
																	BgL_occurrencez00) > 1L);
															if (BgL__ortest_1125z00_4250)
																{	/* Reduce/sbeta.scm 249 */
																	BgL__ortest_1325z00_4248 =
																		BgL__ortest_1125z00_4250;
																}
															else
																{	/* Reduce/sbeta.scm 249 */
																	if (
																		(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_localz00_bglt)
																							CAR(
																								((obj_t) BgL_bz00_4249))))))->
																			BgL_userzf3zf3))
																		{	/* Reduce/sbeta.scm 251 */
																			obj_t BgL__andtest_1127z00_4251;

																			{	/* Reduce/sbeta.scm 252 */
																				obj_t BgL_arg2007z00_4252;

																				{	/* Reduce/sbeta.scm 252 */
																					obj_t BgL_arg2008z00_4253;

																					BgL_arg2008z00_4253 =
																						BGl_thezd2backendzd2zzbackend_backendz00
																						();
																					BgL_arg2007z00_4252 =
																						(((BgL_backendz00_bglt)
																							COBJECT(((BgL_backendz00_bglt)
																									BgL_arg2008z00_4253)))->
																						BgL_debugzd2supportzd2);
																				}
																				BgL__andtest_1127z00_4251 =
																					BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																					(CNST_TABLE_REF(5),
																					BgL_arg2007z00_4252);
																			}
																			if (CBOOL(BgL__andtest_1127z00_4251))
																				{	/* Reduce/sbeta.scm 251 */
																					BgL__ortest_1325z00_4248 =
																						(
																						(long)
																						CINT
																						(BGl_za2bdbzd2debugza2zd2zzengine_paramz00)
																						> 0L);
																				}
																			else
																				{	/* Reduce/sbeta.scm 251 */
																					BgL__ortest_1325z00_4248 =
																						((bool_t) 0);
																				}
																		}
																	else
																		{	/* Reduce/sbeta.scm 250 */
																			BgL__ortest_1325z00_4248 = ((bool_t) 0);
																		}
																}
														}
													}
													if (BgL__ortest_1325z00_4248)
														{	/* Reduce/sbeta.scm 245 */
															BgL_test2359z00_5568 = BgL__ortest_1325z00_4248;
														}
													else
														{	/* Reduce/sbeta.scm 245 */
															obj_t BgL_arg2006z00_4254;

															BgL_arg2006z00_4254 =
																CDR(((obj_t) BgL_l1322z00_4247));
															{
																obj_t BgL_l1322z00_5600;

																BgL_l1322z00_5600 = BgL_arg2006z00_4254;
																BgL_l1322z00_4247 = BgL_l1322z00_5600;
																goto BgL_zc3z04anonymousza32004ze3z87_4246;
															}
														}
												}
										}
									}
									if (BgL_test2359z00_5568)
										{	/* Reduce/sbeta.scm 245 */
											BgL_test2358z00_5567 = ((bool_t) 1);
										}
									else
										{	/* Reduce/sbeta.scm 255 */
											obj_t BgL_g1328z00_4255;

											BgL_g1328z00_4255 =
												(((BgL_letzd2varzd2_bglt) COBJECT(
														((BgL_letzd2varzd2_bglt) BgL_nodez00_4177)))->
												BgL_bindingsz00);
											{
												obj_t BgL_l1326z00_4257;

												BgL_l1326z00_4257 = BgL_g1328z00_4255;
											BgL_zc3z04anonymousza32001ze3z87_4256:
												if (NULLP(BgL_l1326z00_4257))
													{	/* Reduce/sbeta.scm 255 */
														BgL_test2358z00_5567 = ((bool_t) 0);
													}
												else
													{	/* Reduce/sbeta.scm 255 */
														bool_t BgL__ortest_1329z00_4258;

														{	/* Reduce/sbeta.scm 255 */
															obj_t BgL_arg2003z00_4259;

															BgL_arg2003z00_4259 =
																CAR(((obj_t) BgL_l1326z00_4257));
															{	/* Reduce/sbeta.scm 201 */
																obj_t BgL_arg1705z00_4260;

																BgL_arg1705z00_4260 =
																	CDR(((obj_t) BgL_arg2003z00_4259));
																BgL__ortest_1329z00_4258 =
																	BGl_dangerouszf3zf3zzreduce_betaz00
																	(BgL_arg1705z00_4260);
															}
														}
														if (BgL__ortest_1329z00_4258)
															{	/* Reduce/sbeta.scm 255 */
																BgL_test2358z00_5567 = BgL__ortest_1329z00_4258;
															}
														else
															{	/* Reduce/sbeta.scm 255 */
																obj_t BgL_arg2002z00_4261;

																BgL_arg2002z00_4261 =
																	CDR(((obj_t) BgL_l1326z00_4257));
																{
																	obj_t BgL_l1326z00_5613;

																	BgL_l1326z00_5613 = BgL_arg2002z00_4261;
																	BgL_l1326z00_4257 = BgL_l1326z00_5613;
																	goto BgL_zc3z04anonymousza32001ze3z87_4256;
																}
															}
													}
											}
										}
								}
								if (BgL_test2358z00_5567)
									{	/* Reduce/sbeta.scm 245 */
										return
											((BgL_nodez00_bglt)
											((BgL_letzd2varzd2_bglt) BgL_nodez00_4177));
									}
								else
									{	/* Reduce/sbeta.scm 257 */
										bool_t BgL_test2367z00_5616;

										{	/* Reduce/sbeta.scm 257 */
											bool_t BgL_test2368z00_5617;

											{	/* Reduce/sbeta.scm 257 */
												obj_t BgL_tmpz00_5618;

												BgL_tmpz00_5618 =
													(((BgL_letzd2varzd2_bglt) COBJECT(
															((BgL_letzd2varzd2_bglt) BgL_nodez00_4177)))->
													BgL_bindingsz00);
												BgL_test2368z00_5617 = PAIRP(BgL_tmpz00_5618);
											}
											if (BgL_test2368z00_5617)
												{	/* Reduce/sbeta.scm 257 */
													if (NULLP(CDR(
																(((BgL_letzd2varzd2_bglt) COBJECT(
																			((BgL_letzd2varzd2_bglt)
																				BgL_nodez00_4177)))->BgL_bindingsz00))))
														{	/* Reduce/sbeta.scm 259 */
															BgL_variablez00_bglt BgL_varz00_4262;

															{	/* Reduce/sbeta.scm 259 */
																obj_t BgL_pairz00_4263;

																BgL_pairz00_4263 =
																	CAR(
																	(((BgL_letzd2varzd2_bglt) COBJECT(
																				((BgL_letzd2varzd2_bglt)
																					BgL_nodez00_4177)))->
																		BgL_bindingsz00));
																BgL_varz00_4262 =
																	((BgL_variablez00_bglt)
																	CAR(BgL_pairz00_4263));
															}
															{	/* Reduce/sbeta.scm 207 */
																bool_t BgL_test2370z00_5632;

																{	/* Reduce/sbeta.scm 207 */
																	obj_t BgL_classz00_4264;

																	BgL_classz00_4264 = BGl_varz00zzast_nodez00;
																	{	/* Reduce/sbeta.scm 207 */
																		BgL_objectz00_bglt BgL_arg1807z00_4265;

																		{	/* Reduce/sbeta.scm 207 */
																			obj_t BgL_tmpz00_5633;

																			BgL_tmpz00_5633 =
																				((obj_t) BgL_abodyz00_4238);
																			BgL_arg1807z00_4265 =
																				(BgL_objectz00_bglt) (BgL_tmpz00_5633);
																		}
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Reduce/sbeta.scm 207 */
																				long BgL_idxz00_4266;

																				BgL_idxz00_4266 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_4265);
																				BgL_test2370z00_5632 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_4266 + 2L)) ==
																					BgL_classz00_4264);
																			}
																		else
																			{	/* Reduce/sbeta.scm 207 */
																				bool_t BgL_res2158z00_4269;

																				{	/* Reduce/sbeta.scm 207 */
																					obj_t BgL_oclassz00_4270;

																					{	/* Reduce/sbeta.scm 207 */
																						obj_t BgL_arg1815z00_4271;
																						long BgL_arg1816z00_4272;

																						BgL_arg1815z00_4271 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Reduce/sbeta.scm 207 */
																							long BgL_arg1817z00_4273;

																							BgL_arg1817z00_4273 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_4265);
																							BgL_arg1816z00_4272 =
																								(BgL_arg1817z00_4273 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_4270 =
																							VECTOR_REF(BgL_arg1815z00_4271,
																							BgL_arg1816z00_4272);
																					}
																					{	/* Reduce/sbeta.scm 207 */
																						bool_t BgL__ortest_1115z00_4274;

																						BgL__ortest_1115z00_4274 =
																							(BgL_classz00_4264 ==
																							BgL_oclassz00_4270);
																						if (BgL__ortest_1115z00_4274)
																							{	/* Reduce/sbeta.scm 207 */
																								BgL_res2158z00_4269 =
																									BgL__ortest_1115z00_4274;
																							}
																						else
																							{	/* Reduce/sbeta.scm 207 */
																								long BgL_odepthz00_4275;

																								{	/* Reduce/sbeta.scm 207 */
																									obj_t BgL_arg1804z00_4276;

																									BgL_arg1804z00_4276 =
																										(BgL_oclassz00_4270);
																									BgL_odepthz00_4275 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_4276);
																								}
																								if ((2L < BgL_odepthz00_4275))
																									{	/* Reduce/sbeta.scm 207 */
																										obj_t BgL_arg1802z00_4277;

																										{	/* Reduce/sbeta.scm 207 */
																											obj_t BgL_arg1803z00_4278;

																											BgL_arg1803z00_4278 =
																												(BgL_oclassz00_4270);
																											BgL_arg1802z00_4277 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_4278,
																												2L);
																										}
																										BgL_res2158z00_4269 =
																											(BgL_arg1802z00_4277 ==
																											BgL_classz00_4264);
																									}
																								else
																									{	/* Reduce/sbeta.scm 207 */
																										BgL_res2158z00_4269 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2370z00_5632 =
																					BgL_res2158z00_4269;
																			}
																	}
																}
																if (BgL_test2370z00_5632)
																	{	/* Reduce/sbeta.scm 207 */
																		BgL_test2367z00_5616 =
																			(
																			((obj_t)
																				(((BgL_varz00_bglt) COBJECT(
																							((BgL_varz00_bglt)
																								BgL_abodyz00_4238)))->
																					BgL_variablez00)) ==
																			((obj_t) BgL_varz00_4262));
																	}
																else
																	{	/* Reduce/sbeta.scm 207 */
																		BgL_test2367z00_5616 = ((bool_t) 0);
																	}
															}
														}
													else
														{	/* Reduce/sbeta.scm 258 */
															BgL_test2367z00_5616 = ((bool_t) 0);
														}
												}
											else
												{	/* Reduce/sbeta.scm 257 */
													BgL_test2367z00_5616 = ((bool_t) 0);
												}
										}
										if (BgL_test2367z00_5616)
											{	/* Reduce/sbeta.scm 257 */
												BGl_za2removedza2z00zzreduce_betaz00 =
													(BGl_za2removedza2z00zzreduce_betaz00 + 1L);
												{	/* Reduce/sbeta.scm 262 */
													obj_t BgL_arg1882z00_4279;

													{	/* Reduce/sbeta.scm 262 */
														obj_t BgL_pairz00_4280;

														BgL_pairz00_4280 =
															(((BgL_letzd2varzd2_bglt) COBJECT(
																	((BgL_letzd2varzd2_bglt) BgL_nodez00_4177)))->
															BgL_bindingsz00);
														BgL_arg1882z00_4279 = CDR(CAR(BgL_pairz00_4280));
													}
													return
														BGl_nodezd2betaz12zc0zzreduce_betaz00(
														((BgL_nodez00_bglt) BgL_arg1882z00_4279));
												}
											}
										else
											{	/* Reduce/sbeta.scm 263 */
												bool_t BgL_test2374z00_5667;

												{	/* Reduce/sbeta.scm 263 */
													bool_t BgL_test2375z00_5668;

													{	/* Reduce/sbeta.scm 263 */
														obj_t BgL_classz00_4281;

														BgL_classz00_4281 = BGl_conditionalz00zzast_nodez00;
														{	/* Reduce/sbeta.scm 263 */
															BgL_objectz00_bglt BgL_arg1807z00_4282;

															{	/* Reduce/sbeta.scm 263 */
																obj_t BgL_tmpz00_5669;

																BgL_tmpz00_5669 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_abodyz00_4238));
																BgL_arg1807z00_4282 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_5669);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Reduce/sbeta.scm 263 */
																	long BgL_idxz00_4283;

																	BgL_idxz00_4283 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_4282);
																	BgL_test2375z00_5668 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_4283 + 3L)) ==
																		BgL_classz00_4281);
																}
															else
																{	/* Reduce/sbeta.scm 263 */
																	bool_t BgL_res2159z00_4286;

																	{	/* Reduce/sbeta.scm 263 */
																		obj_t BgL_oclassz00_4287;

																		{	/* Reduce/sbeta.scm 263 */
																			obj_t BgL_arg1815z00_4288;
																			long BgL_arg1816z00_4289;

																			BgL_arg1815z00_4288 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Reduce/sbeta.scm 263 */
																				long BgL_arg1817z00_4290;

																				BgL_arg1817z00_4290 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_4282);
																				BgL_arg1816z00_4289 =
																					(BgL_arg1817z00_4290 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_4287 =
																				VECTOR_REF(BgL_arg1815z00_4288,
																				BgL_arg1816z00_4289);
																		}
																		{	/* Reduce/sbeta.scm 263 */
																			bool_t BgL__ortest_1115z00_4291;

																			BgL__ortest_1115z00_4291 =
																				(BgL_classz00_4281 ==
																				BgL_oclassz00_4287);
																			if (BgL__ortest_1115z00_4291)
																				{	/* Reduce/sbeta.scm 263 */
																					BgL_res2159z00_4286 =
																						BgL__ortest_1115z00_4291;
																				}
																			else
																				{	/* Reduce/sbeta.scm 263 */
																					long BgL_odepthz00_4292;

																					{	/* Reduce/sbeta.scm 263 */
																						obj_t BgL_arg1804z00_4293;

																						BgL_arg1804z00_4293 =
																							(BgL_oclassz00_4287);
																						BgL_odepthz00_4292 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_4293);
																					}
																					if ((3L < BgL_odepthz00_4292))
																						{	/* Reduce/sbeta.scm 263 */
																							obj_t BgL_arg1802z00_4294;

																							{	/* Reduce/sbeta.scm 263 */
																								obj_t BgL_arg1803z00_4295;

																								BgL_arg1803z00_4295 =
																									(BgL_oclassz00_4287);
																								BgL_arg1802z00_4294 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_4295, 3L);
																							}
																							BgL_res2159z00_4286 =
																								(BgL_arg1802z00_4294 ==
																								BgL_classz00_4281);
																						}
																					else
																						{	/* Reduce/sbeta.scm 263 */
																							BgL_res2159z00_4286 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2375z00_5668 = BgL_res2159z00_4286;
																}
														}
													}
													if (BgL_test2375z00_5668)
														{	/* Reduce/sbeta.scm 263 */
															if (NULLP(CDR(
																		(((BgL_letzd2varzd2_bglt) COBJECT(
																					((BgL_letzd2varzd2_bglt)
																						BgL_nodez00_4177)))->
																			BgL_bindingsz00))))
																{	/* Reduce/sbeta.scm 265 */
																	BgL_variablez00_bglt BgL_varz00_4296;
																	BgL_nodez00_bglt BgL_nodez00_4297;

																	{	/* Reduce/sbeta.scm 265 */
																		obj_t BgL_pairz00_4298;

																		BgL_pairz00_4298 =
																			CAR(
																			(((BgL_letzd2varzd2_bglt) COBJECT(
																						((BgL_letzd2varzd2_bglt)
																							BgL_nodez00_4177)))->
																				BgL_bindingsz00));
																		BgL_varz00_4296 =
																			((BgL_variablez00_bglt)
																			CAR(BgL_pairz00_4298));
																	}
																	BgL_nodez00_4297 =
																		(((BgL_conditionalz00_bglt) COBJECT(
																				((BgL_conditionalz00_bglt)
																					BgL_abodyz00_4238)))->BgL_testz00);
																	{	/* Reduce/sbeta.scm 207 */
																		bool_t BgL_test2380z00_5704;

																		{	/* Reduce/sbeta.scm 207 */
																			obj_t BgL_classz00_4299;

																			BgL_classz00_4299 =
																				BGl_varz00zzast_nodez00;
																			{	/* Reduce/sbeta.scm 207 */
																				BgL_objectz00_bglt BgL_arg1807z00_4300;

																				{	/* Reduce/sbeta.scm 207 */
																					obj_t BgL_tmpz00_5705;

																					BgL_tmpz00_5705 =
																						((obj_t) BgL_nodez00_4297);
																					BgL_arg1807z00_4300 =
																						(BgL_objectz00_bglt)
																						(BgL_tmpz00_5705);
																				}
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Reduce/sbeta.scm 207 */
																						long BgL_idxz00_4301;

																						BgL_idxz00_4301 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_4300);
																						BgL_test2380z00_5704 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_4301 + 2L)) ==
																							BgL_classz00_4299);
																					}
																				else
																					{	/* Reduce/sbeta.scm 207 */
																						bool_t BgL_res2160z00_4304;

																						{	/* Reduce/sbeta.scm 207 */
																							obj_t BgL_oclassz00_4305;

																							{	/* Reduce/sbeta.scm 207 */
																								obj_t BgL_arg1815z00_4306;
																								long BgL_arg1816z00_4307;

																								BgL_arg1815z00_4306 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Reduce/sbeta.scm 207 */
																									long BgL_arg1817z00_4308;

																									BgL_arg1817z00_4308 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_4300);
																									BgL_arg1816z00_4307 =
																										(BgL_arg1817z00_4308 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_4305 =
																									VECTOR_REF
																									(BgL_arg1815z00_4306,
																									BgL_arg1816z00_4307);
																							}
																							{	/* Reduce/sbeta.scm 207 */
																								bool_t BgL__ortest_1115z00_4309;

																								BgL__ortest_1115z00_4309 =
																									(BgL_classz00_4299 ==
																									BgL_oclassz00_4305);
																								if (BgL__ortest_1115z00_4309)
																									{	/* Reduce/sbeta.scm 207 */
																										BgL_res2160z00_4304 =
																											BgL__ortest_1115z00_4309;
																									}
																								else
																									{	/* Reduce/sbeta.scm 207 */
																										long BgL_odepthz00_4310;

																										{	/* Reduce/sbeta.scm 207 */
																											obj_t BgL_arg1804z00_4311;

																											BgL_arg1804z00_4311 =
																												(BgL_oclassz00_4305);
																											BgL_odepthz00_4310 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_4311);
																										}
																										if (
																											(2L < BgL_odepthz00_4310))
																											{	/* Reduce/sbeta.scm 207 */
																												obj_t
																													BgL_arg1802z00_4312;
																												{	/* Reduce/sbeta.scm 207 */
																													obj_t
																														BgL_arg1803z00_4313;
																													BgL_arg1803z00_4313 =
																														(BgL_oclassz00_4305);
																													BgL_arg1802z00_4312 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_4313,
																														2L);
																												}
																												BgL_res2160z00_4304 =
																													(BgL_arg1802z00_4312
																													== BgL_classz00_4299);
																											}
																										else
																											{	/* Reduce/sbeta.scm 207 */
																												BgL_res2160z00_4304 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test2380z00_5704 =
																							BgL_res2160z00_4304;
																					}
																			}
																		}
																		if (BgL_test2380z00_5704)
																			{	/* Reduce/sbeta.scm 207 */
																				BgL_test2374z00_5667 =
																					(
																					((obj_t)
																						(((BgL_varz00_bglt) COBJECT(
																									((BgL_varz00_bglt)
																										BgL_nodez00_4297)))->
																							BgL_variablez00)) ==
																					((obj_t) BgL_varz00_4296));
																			}
																		else
																			{	/* Reduce/sbeta.scm 207 */
																				BgL_test2374z00_5667 = ((bool_t) 0);
																			}
																	}
																}
															else
																{	/* Reduce/sbeta.scm 264 */
																	BgL_test2374z00_5667 = ((bool_t) 0);
																}
														}
													else
														{	/* Reduce/sbeta.scm 263 */
															BgL_test2374z00_5667 = ((bool_t) 0);
														}
												}
												if (BgL_test2374z00_5667)
													{	/* Reduce/sbeta.scm 263 */
														BGl_za2removedza2z00zzreduce_betaz00 =
															(BGl_za2removedza2z00zzreduce_betaz00 + 1L);
														{	/* Reduce/sbeta.scm 269 */
															obj_t BgL_valz00_4314;

															BgL_valz00_4314 =
																CDR(CAR(
																	(((BgL_letzd2varzd2_bglt) COBJECT(
																				((BgL_letzd2varzd2_bglt)
																					BgL_nodez00_4177)))->
																		BgL_bindingsz00)));
															((((BgL_conditionalz00_bglt)
																		COBJECT(((BgL_conditionalz00_bglt)
																				BgL_abodyz00_4238)))->BgL_testz00) =
																((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
																		BgL_valz00_4314)), BUNSPEC);
															if (CBOOL((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt) (
																						(BgL_conditionalz00_bglt)
																						BgL_abodyz00_4238))))->
																		BgL_sidezd2effectzd2)))
																{	/* Reduce/sbeta.scm 271 */
																	BFALSE;
																}
															else
																{	/* Reduce/sbeta.scm 272 */
																	bool_t BgL_arg1897z00_4315;

																	BgL_arg1897z00_4315 =
																		BGl_sidezd2effectzf3z21zzeffect_effectz00(
																		((BgL_nodez00_bglt) BgL_valz00_4314));
																	((((BgL_nodezf2effectzf2_bglt) COBJECT(
																					((BgL_nodezf2effectzf2_bglt)
																						((BgL_conditionalz00_bglt)
																							BgL_abodyz00_4238))))->
																			BgL_sidezd2effectzd2) =
																		((obj_t) BBOOL(BgL_arg1897z00_4315)),
																		BUNSPEC);
																}
														}
														return BgL_abodyz00_4238;
													}
												else
													{	/* Reduce/sbeta.scm 274 */
														bool_t BgL_test2385z00_5751;

														{	/* Reduce/sbeta.scm 274 */
															bool_t BgL_test2386z00_5752;

															{	/* Reduce/sbeta.scm 274 */
																obj_t BgL_classz00_4316;

																BgL_classz00_4316 = BGl_appz00zzast_nodez00;
																{	/* Reduce/sbeta.scm 274 */
																	BgL_objectz00_bglt BgL_arg1807z00_4317;

																	{	/* Reduce/sbeta.scm 274 */
																		obj_t BgL_tmpz00_5753;

																		BgL_tmpz00_5753 =
																			((obj_t)
																			((BgL_objectz00_bglt) BgL_abodyz00_4238));
																		BgL_arg1807z00_4317 =
																			(BgL_objectz00_bglt) (BgL_tmpz00_5753);
																	}
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Reduce/sbeta.scm 274 */
																			long BgL_idxz00_4318;

																			BgL_idxz00_4318 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_4317);
																			BgL_test2386z00_5752 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_4318 + 3L)) ==
																				BgL_classz00_4316);
																		}
																	else
																		{	/* Reduce/sbeta.scm 274 */
																			bool_t BgL_res2161z00_4321;

																			{	/* Reduce/sbeta.scm 274 */
																				obj_t BgL_oclassz00_4322;

																				{	/* Reduce/sbeta.scm 274 */
																					obj_t BgL_arg1815z00_4323;
																					long BgL_arg1816z00_4324;

																					BgL_arg1815z00_4323 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Reduce/sbeta.scm 274 */
																						long BgL_arg1817z00_4325;

																						BgL_arg1817z00_4325 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_4317);
																						BgL_arg1816z00_4324 =
																							(BgL_arg1817z00_4325 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_4322 =
																						VECTOR_REF(BgL_arg1815z00_4323,
																						BgL_arg1816z00_4324);
																				}
																				{	/* Reduce/sbeta.scm 274 */
																					bool_t BgL__ortest_1115z00_4326;

																					BgL__ortest_1115z00_4326 =
																						(BgL_classz00_4316 ==
																						BgL_oclassz00_4322);
																					if (BgL__ortest_1115z00_4326)
																						{	/* Reduce/sbeta.scm 274 */
																							BgL_res2161z00_4321 =
																								BgL__ortest_1115z00_4326;
																						}
																					else
																						{	/* Reduce/sbeta.scm 274 */
																							long BgL_odepthz00_4327;

																							{	/* Reduce/sbeta.scm 274 */
																								obj_t BgL_arg1804z00_4328;

																								BgL_arg1804z00_4328 =
																									(BgL_oclassz00_4322);
																								BgL_odepthz00_4327 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_4328);
																							}
																							if ((3L < BgL_odepthz00_4327))
																								{	/* Reduce/sbeta.scm 274 */
																									obj_t BgL_arg1802z00_4329;

																									{	/* Reduce/sbeta.scm 274 */
																										obj_t BgL_arg1803z00_4330;

																										BgL_arg1803z00_4330 =
																											(BgL_oclassz00_4322);
																										BgL_arg1802z00_4329 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_4330, 3L);
																									}
																									BgL_res2161z00_4321 =
																										(BgL_arg1802z00_4329 ==
																										BgL_classz00_4316);
																								}
																							else
																								{	/* Reduce/sbeta.scm 274 */
																									BgL_res2161z00_4321 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2386z00_5752 =
																				BgL_res2161z00_4321;
																		}
																}
															}
															if (BgL_test2386z00_5752)
																{	/* Reduce/sbeta.scm 275 */
																	bool_t BgL_test2390z00_5776;

																	{	/* Reduce/sbeta.scm 275 */
																		obj_t BgL_g1332z00_4331;

																		BgL_g1332z00_4331 =
																			(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						BgL_abodyz00_4238)))->BgL_argsz00);
																		{
																			obj_t BgL_l1330z00_4333;

																			BgL_l1330z00_4333 = BgL_g1332z00_4331;
																		BgL_zc3z04anonymousza31986ze3z87_4332:
																			if (NULLP(BgL_l1330z00_4333))
																				{	/* Reduce/sbeta.scm 275 */
																					BgL_test2390z00_5776 = ((bool_t) 1);
																				}
																			else
																				{	/* Reduce/sbeta.scm 275 */
																					bool_t BgL_test2392z00_5781;

																					{	/* Reduce/sbeta.scm 275 */
																						obj_t BgL_arg1988z00_4334;

																						BgL_arg1988z00_4334 =
																							CAR(((obj_t) BgL_l1330z00_4333));
																						BgL_test2392z00_5781 =
																							BGl_simplezf3ze70z14zzreduce_betaz00
																							(BgL_arg1988z00_4334);
																					}
																					if (BgL_test2392z00_5781)
																						{	/* Reduce/sbeta.scm 275 */
																							obj_t BgL_arg1987z00_4335;

																							BgL_arg1987z00_4335 =
																								CDR(
																								((obj_t) BgL_l1330z00_4333));
																							{
																								obj_t BgL_l1330z00_5787;

																								BgL_l1330z00_5787 =
																									BgL_arg1987z00_4335;
																								BgL_l1330z00_4333 =
																									BgL_l1330z00_5787;
																								goto
																									BgL_zc3z04anonymousza31986ze3z87_4332;
																							}
																						}
																					else
																						{	/* Reduce/sbeta.scm 275 */
																							BgL_test2390z00_5776 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	if (BgL_test2390z00_5776)
																		{	/* Reduce/sbeta.scm 276 */
																			bool_t BgL_test2393z00_5788;

																			{	/* Reduce/sbeta.scm 276 */
																				obj_t BgL_g1336z00_4336;

																				BgL_g1336z00_4336 =
																					(((BgL_appz00_bglt) COBJECT(
																							((BgL_appz00_bglt)
																								BgL_abodyz00_4238)))->
																					BgL_argsz00);
																				{
																					obj_t BgL_l1334z00_4338;

																					BgL_l1334z00_4338 = BgL_g1336z00_4336;
																				BgL_zc3z04anonymousza31983ze3z87_4337:
																					if (NULLP(BgL_l1334z00_4338))
																						{	/* Reduce/sbeta.scm 276 */
																							BgL_test2393z00_5788 =
																								((bool_t) 0);
																						}
																					else
																						{	/* Reduce/sbeta.scm 276 */
																							bool_t BgL__ortest_1337z00_4339;

																							{	/* Reduce/sbeta.scm 276 */
																								obj_t BgL_arg1985z00_4340;

																								BgL_arg1985z00_4340 =
																									CAR(
																									((obj_t) BgL_l1334z00_4338));
																								BgL__ortest_1337z00_4339 =
																									BGl_sidezd2effectzf3z21zzeffect_effectz00
																									(((BgL_nodez00_bglt)
																										BgL_arg1985z00_4340));
																							}
																							if (BgL__ortest_1337z00_4339)
																								{	/* Reduce/sbeta.scm 276 */
																									BgL_test2393z00_5788 =
																										BgL__ortest_1337z00_4339;
																								}
																							else
																								{	/* Reduce/sbeta.scm 276 */
																									obj_t BgL_arg1984z00_4341;

																									BgL_arg1984z00_4341 =
																										CDR(
																										((obj_t)
																											BgL_l1334z00_4338));
																									{
																										obj_t BgL_l1334z00_5800;

																										BgL_l1334z00_5800 =
																											BgL_arg1984z00_4341;
																										BgL_l1334z00_4338 =
																											BgL_l1334z00_5800;
																										goto
																											BgL_zc3z04anonymousza31983ze3z87_4337;
																									}
																								}
																						}
																				}
																			}
																			if (BgL_test2393z00_5788)
																				{	/* Reduce/sbeta.scm 276 */
																					BgL_test2385z00_5751 = ((bool_t) 0);
																				}
																			else
																				{	/* Reduce/sbeta.scm 277 */
																					obj_t BgL_g1340z00_4342;

																					BgL_g1340z00_4342 =
																						(((BgL_letzd2varzd2_bglt) COBJECT(
																								((BgL_letzd2varzd2_bglt)
																									BgL_nodez00_4177)))->
																						BgL_bindingsz00);
																					{
																						obj_t BgL_l1338z00_4344;

																						BgL_l1338z00_4344 =
																							BgL_g1340z00_4342;
																					BgL_zc3z04anonymousza31978ze3z87_4343:
																						if (NULLP
																							(BgL_l1338z00_4344))
																							{	/* Reduce/sbeta.scm 277 */
																								BgL_test2385z00_5751 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Reduce/sbeta.scm 277 */
																								bool_t BgL_test2397z00_5805;

																								{	/* Reduce/sbeta.scm 278 */
																									obj_t BgL_bz00_4345;

																									BgL_bz00_4345 =
																										CAR(
																										((obj_t)
																											BgL_l1338z00_4344));
																									{	/* Reduce/sbeta.scm 278 */
																										bool_t BgL_test2398z00_5808;

																										{	/* Reduce/sbeta.scm 278 */
																											obj_t BgL_arg1981z00_4346;
																											obj_t BgL_arg1982z00_4347;

																											BgL_arg1981z00_4346 =
																												CAR(
																												((obj_t)
																													BgL_bz00_4345));
																											BgL_arg1982z00_4347 =
																												(((BgL_appz00_bglt)
																													COBJECT((
																															(BgL_appz00_bglt)
																															BgL_abodyz00_4238)))->
																												BgL_argsz00);
																											BgL_test2398z00_5808 =
																												BGl_argumentzf3zf3zzreduce_betaz00
																												(((BgL_variablez00_bglt)
																													BgL_arg1981z00_4346),
																												BgL_arg1982z00_4347);
																										}
																										if (BgL_test2398z00_5808)
																											{	/* Reduce/sbeta.scm 282 */
																												bool_t
																													BgL_test2399z00_5815;
																												{	/* Reduce/sbeta.scm 282 */
																													obj_t
																														BgL_arg1980z00_4348;
																													BgL_arg1980z00_4348 =
																														CDR(((obj_t)
																															BgL_bz00_4345));
																													{	/* Reduce/sbeta.scm 282 */
																														obj_t
																															BgL_classz00_4349;
																														BgL_classz00_4349 =
																															BGl_kwotez00zzast_nodez00;
																														if (BGL_OBJECTP
																															(BgL_arg1980z00_4348))
																															{	/* Reduce/sbeta.scm 282 */
																																BgL_objectz00_bglt
																																	BgL_arg1807z00_4350;
																																BgL_arg1807z00_4350
																																	=
																																	(BgL_objectz00_bglt)
																																	(BgL_arg1980z00_4348);
																																if (BGL_CONDEXPAND_ISA_ARCH64())
																																	{	/* Reduce/sbeta.scm 282 */
																																		long
																																			BgL_idxz00_4351;
																																		BgL_idxz00_4351
																																			=
																																			BGL_OBJECT_INHERITANCE_NUM
																																			(BgL_arg1807z00_4350);
																																		BgL_test2399z00_5815
																																			=
																																			(VECTOR_REF
																																			(BGl_za2inheritancesza2z00zz__objectz00,
																																				(BgL_idxz00_4351
																																					+
																																					2L))
																																			==
																																			BgL_classz00_4349);
																																	}
																																else
																																	{	/* Reduce/sbeta.scm 282 */
																																		bool_t
																																			BgL_res2162z00_4354;
																																		{	/* Reduce/sbeta.scm 282 */
																																			obj_t
																																				BgL_oclassz00_4355;
																																			{	/* Reduce/sbeta.scm 282 */
																																				obj_t
																																					BgL_arg1815z00_4356;
																																				long
																																					BgL_arg1816z00_4357;
																																				BgL_arg1815z00_4356
																																					=
																																					(BGl_za2classesza2z00zz__objectz00);
																																				{	/* Reduce/sbeta.scm 282 */
																																					long
																																						BgL_arg1817z00_4358;
																																					BgL_arg1817z00_4358
																																						=
																																						BGL_OBJECT_CLASS_NUM
																																						(BgL_arg1807z00_4350);
																																					BgL_arg1816z00_4357
																																						=
																																						(BgL_arg1817z00_4358
																																						-
																																						OBJECT_TYPE);
																																				}
																																				BgL_oclassz00_4355
																																					=
																																					VECTOR_REF
																																					(BgL_arg1815z00_4356,
																																					BgL_arg1816z00_4357);
																																			}
																																			{	/* Reduce/sbeta.scm 282 */
																																				bool_t
																																					BgL__ortest_1115z00_4359;
																																				BgL__ortest_1115z00_4359
																																					=
																																					(BgL_classz00_4349
																																					==
																																					BgL_oclassz00_4355);
																																				if (BgL__ortest_1115z00_4359)
																																					{	/* Reduce/sbeta.scm 282 */
																																						BgL_res2162z00_4354
																																							=
																																							BgL__ortest_1115z00_4359;
																																					}
																																				else
																																					{	/* Reduce/sbeta.scm 282 */
																																						long
																																							BgL_odepthz00_4360;
																																						{	/* Reduce/sbeta.scm 282 */
																																							obj_t
																																								BgL_arg1804z00_4361;
																																							BgL_arg1804z00_4361
																																								=
																																								(BgL_oclassz00_4355);
																																							BgL_odepthz00_4360
																																								=
																																								BGL_CLASS_DEPTH
																																								(BgL_arg1804z00_4361);
																																						}
																																						if (
																																							(2L
																																								<
																																								BgL_odepthz00_4360))
																																							{	/* Reduce/sbeta.scm 282 */
																																								obj_t
																																									BgL_arg1802z00_4362;
																																								{	/* Reduce/sbeta.scm 282 */
																																									obj_t
																																										BgL_arg1803z00_4363;
																																									BgL_arg1803z00_4363
																																										=
																																										(BgL_oclassz00_4355);
																																									BgL_arg1802z00_4362
																																										=
																																										BGL_CLASS_ANCESTORS_REF
																																										(BgL_arg1803z00_4363,
																																										2L);
																																								}
																																								BgL_res2162z00_4354
																																									=
																																									(BgL_arg1802z00_4362
																																									==
																																									BgL_classz00_4349);
																																							}
																																						else
																																							{	/* Reduce/sbeta.scm 282 */
																																								BgL_res2162z00_4354
																																									=
																																									(
																																									(bool_t)
																																									0);
																																							}
																																					}
																																			}
																																		}
																																		BgL_test2399z00_5815
																																			=
																																			BgL_res2162z00_4354;
																																	}
																															}
																														else
																															{	/* Reduce/sbeta.scm 282 */
																																BgL_test2399z00_5815
																																	=
																																	((bool_t) 0);
																															}
																													}
																												}
																												if (BgL_test2399z00_5815)
																													{	/* Reduce/sbeta.scm 282 */
																														BgL_test2397z00_5805
																															= ((bool_t) 0);
																													}
																												else
																													{	/* Reduce/sbeta.scm 282 */
																														BgL_test2397z00_5805
																															= ((bool_t) 1);
																													}
																											}
																										else
																											{	/* Reduce/sbeta.scm 278 */
																												BgL_test2397z00_5805 =
																													((bool_t) 0);
																											}
																									}
																								}
																								if (BgL_test2397z00_5805)
																									{	/* Reduce/sbeta.scm 277 */
																										obj_t BgL_arg1979z00_4364;

																										BgL_arg1979z00_4364 =
																											CDR(
																											((obj_t)
																												BgL_l1338z00_4344));
																										{
																											obj_t BgL_l1338z00_5842;

																											BgL_l1338z00_5842 =
																												BgL_arg1979z00_4364;
																											BgL_l1338z00_4344 =
																												BgL_l1338z00_5842;
																											goto
																												BgL_zc3z04anonymousza31978ze3z87_4343;
																										}
																									}
																								else
																									{	/* Reduce/sbeta.scm 277 */
																										BgL_test2385z00_5751 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																		}
																	else
																		{	/* Reduce/sbeta.scm 275 */
																			BgL_test2385z00_5751 = ((bool_t) 0);
																		}
																}
															else
																{	/* Reduce/sbeta.scm 274 */
																	BgL_test2385z00_5751 = ((bool_t) 0);
																}
														}
														if (BgL_test2385z00_5751)
															{	/* Reduce/sbeta.scm 274 */
																BGl_za2removedza2z00zzreduce_betaz00 =
																	(BGl_za2removedza2z00zzreduce_betaz00 +
																	bgl_list_length(
																		(((BgL_letzd2varzd2_bglt) COBJECT(
																					((BgL_letzd2varzd2_bglt)
																						BgL_nodez00_4177)))->
																			BgL_bindingsz00)));
																{	/* Reduce/sbeta.scm 288 */
																	obj_t BgL_nargsz00_4365;

																	{	/* Reduce/sbeta.scm 288 */
																		obj_t BgL_arg1939z00_4366;
																		obj_t BgL_arg1940z00_4367;

																		BgL_arg1939z00_4366 =
																			(((BgL_letzd2varzd2_bglt) COBJECT(
																					((BgL_letzd2varzd2_bglt)
																						BgL_nodez00_4177)))->
																			BgL_bindingsz00);
																		BgL_arg1940z00_4367 =
																			(((BgL_appz00_bglt)
																				COBJECT(((BgL_appz00_bglt)
																						BgL_abodyz00_4238)))->BgL_argsz00);
																		BgL_nargsz00_4365 =
																			BGl_makezd2argszd2listz00zzreduce_betaz00
																			(BgL_arg1939z00_4366,
																			BgL_arg1940z00_4367);
																	}
																	((((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						BgL_abodyz00_4238)))->BgL_argsz00) =
																		((obj_t) BgL_nargsz00_4365), BUNSPEC);
																	if (CBOOL((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt) (
																								(BgL_appz00_bglt)
																								BgL_abodyz00_4238))))->
																				BgL_sidezd2effectzd2)))
																		{	/* Reduce/sbeta.scm 290 */
																			BFALSE;
																		}
																	else
																		{	/* Reduce/sbeta.scm 291 */
																			bool_t BgL_arg1934z00_4368;

																			{
																				obj_t BgL_l1342z00_4370;

																				BgL_l1342z00_4370 = BgL_nargsz00_4365;
																			BgL_zc3z04anonymousza31935ze3z87_4369:
																				if (NULLP(BgL_l1342z00_4370))
																					{	/* Reduce/sbeta.scm 291 */
																						BgL_arg1934z00_4368 = ((bool_t) 0);
																					}
																				else
																					{	/* Reduce/sbeta.scm 291 */
																						bool_t BgL__ortest_1344z00_4371;

																						{	/* Reduce/sbeta.scm 291 */
																							obj_t BgL_arg1938z00_4372;

																							BgL_arg1938z00_4372 =
																								CAR(
																								((obj_t) BgL_l1342z00_4370));
																							BgL__ortest_1344z00_4371 =
																								BGl_sidezd2effectzf3z21zzeffect_effectz00
																								(((BgL_nodez00_bglt)
																									BgL_arg1938z00_4372));
																						}
																						if (BgL__ortest_1344z00_4371)
																							{	/* Reduce/sbeta.scm 291 */
																								BgL_arg1934z00_4368 =
																									BgL__ortest_1344z00_4371;
																							}
																						else
																							{	/* Reduce/sbeta.scm 291 */
																								obj_t BgL_arg1937z00_4373;

																								BgL_arg1937z00_4373 =
																									CDR(
																									((obj_t) BgL_l1342z00_4370));
																								{
																									obj_t BgL_l1342z00_5868;

																									BgL_l1342z00_5868 =
																										BgL_arg1937z00_4373;
																									BgL_l1342z00_4370 =
																										BgL_l1342z00_5868;
																									goto
																										BgL_zc3z04anonymousza31935ze3z87_4369;
																								}
																							}
																					}
																			}
																			((((BgL_nodezf2effectzf2_bglt) COBJECT(
																							((BgL_nodezf2effectzf2_bglt)
																								((BgL_appz00_bglt)
																									BgL_abodyz00_4238))))->
																					BgL_sidezd2effectzd2) =
																				((obj_t) BBOOL(BgL_arg1934z00_4368)),
																				BUNSPEC);
																		}
																}
																return BgL_abodyz00_4238;
															}
														else
															{	/* Reduce/sbeta.scm 293 */
																bool_t BgL_test2407z00_5873;

																{	/* Reduce/sbeta.scm 293 */
																	bool_t BgL_test2408z00_5874;

																	{	/* Reduce/sbeta.scm 293 */
																		obj_t BgL_classz00_4374;

																		BgL_classz00_4374 =
																			BGl_externz00zzast_nodez00;
																		{	/* Reduce/sbeta.scm 293 */
																			BgL_objectz00_bglt BgL_arg1807z00_4375;

																			{	/* Reduce/sbeta.scm 293 */
																				obj_t BgL_tmpz00_5875;

																				BgL_tmpz00_5875 =
																					((obj_t)
																					((BgL_objectz00_bglt)
																						BgL_abodyz00_4238));
																				BgL_arg1807z00_4375 =
																					(BgL_objectz00_bglt)
																					(BgL_tmpz00_5875);
																			}
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Reduce/sbeta.scm 293 */
																					long BgL_idxz00_4376;

																					BgL_idxz00_4376 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_4375);
																					BgL_test2408z00_5874 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_4376 + 3L)) ==
																						BgL_classz00_4374);
																				}
																			else
																				{	/* Reduce/sbeta.scm 293 */
																					bool_t BgL_res2163z00_4379;

																					{	/* Reduce/sbeta.scm 293 */
																						obj_t BgL_oclassz00_4380;

																						{	/* Reduce/sbeta.scm 293 */
																							obj_t BgL_arg1815z00_4381;
																							long BgL_arg1816z00_4382;

																							BgL_arg1815z00_4381 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Reduce/sbeta.scm 293 */
																								long BgL_arg1817z00_4383;

																								BgL_arg1817z00_4383 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_4375);
																								BgL_arg1816z00_4382 =
																									(BgL_arg1817z00_4383 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_4380 =
																								VECTOR_REF(BgL_arg1815z00_4381,
																								BgL_arg1816z00_4382);
																						}
																						{	/* Reduce/sbeta.scm 293 */
																							bool_t BgL__ortest_1115z00_4384;

																							BgL__ortest_1115z00_4384 =
																								(BgL_classz00_4374 ==
																								BgL_oclassz00_4380);
																							if (BgL__ortest_1115z00_4384)
																								{	/* Reduce/sbeta.scm 293 */
																									BgL_res2163z00_4379 =
																										BgL__ortest_1115z00_4384;
																								}
																							else
																								{	/* Reduce/sbeta.scm 293 */
																									long BgL_odepthz00_4385;

																									{	/* Reduce/sbeta.scm 293 */
																										obj_t BgL_arg1804z00_4386;

																										BgL_arg1804z00_4386 =
																											(BgL_oclassz00_4380);
																										BgL_odepthz00_4385 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_4386);
																									}
																									if ((3L < BgL_odepthz00_4385))
																										{	/* Reduce/sbeta.scm 293 */
																											obj_t BgL_arg1802z00_4387;

																											{	/* Reduce/sbeta.scm 293 */
																												obj_t
																													BgL_arg1803z00_4388;
																												BgL_arg1803z00_4388 =
																													(BgL_oclassz00_4380);
																												BgL_arg1802z00_4387 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_4388,
																													3L);
																											}
																											BgL_res2163z00_4379 =
																												(BgL_arg1802z00_4387 ==
																												BgL_classz00_4374);
																										}
																									else
																										{	/* Reduce/sbeta.scm 293 */
																											BgL_res2163z00_4379 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2408z00_5874 =
																						BgL_res2163z00_4379;
																				}
																		}
																	}
																	if (BgL_test2408z00_5874)
																		{	/* Reduce/sbeta.scm 294 */
																			bool_t BgL_test2412z00_5898;

																			{	/* Reduce/sbeta.scm 294 */
																				obj_t BgL_g1347z00_4389;

																				BgL_g1347z00_4389 =
																					(((BgL_externz00_bglt) COBJECT(
																							((BgL_externz00_bglt)
																								BgL_abodyz00_4238)))->
																					BgL_exprza2za2);
																				{
																					obj_t BgL_l1345z00_4391;

																					BgL_l1345z00_4391 = BgL_g1347z00_4389;
																				BgL_zc3z04anonymousza31975ze3z87_4390:
																					if (NULLP(BgL_l1345z00_4391))
																						{	/* Reduce/sbeta.scm 294 */
																							BgL_test2412z00_5898 =
																								((bool_t) 1);
																						}
																					else
																						{	/* Reduce/sbeta.scm 294 */
																							bool_t BgL_test2414z00_5903;

																							{	/* Reduce/sbeta.scm 294 */
																								obj_t BgL_arg1977z00_4392;

																								BgL_arg1977z00_4392 =
																									CAR(
																									((obj_t) BgL_l1345z00_4391));
																								BgL_test2414z00_5903 =
																									BGl_simplezf3ze70z14zzreduce_betaz00
																									(BgL_arg1977z00_4392);
																							}
																							if (BgL_test2414z00_5903)
																								{	/* Reduce/sbeta.scm 294 */
																									obj_t BgL_arg1976z00_4393;

																									BgL_arg1976z00_4393 =
																										CDR(
																										((obj_t)
																											BgL_l1345z00_4391));
																									{
																										obj_t BgL_l1345z00_5909;

																										BgL_l1345z00_5909 =
																											BgL_arg1976z00_4393;
																										BgL_l1345z00_4391 =
																											BgL_l1345z00_5909;
																										goto
																											BgL_zc3z04anonymousza31975ze3z87_4390;
																									}
																								}
																							else
																								{	/* Reduce/sbeta.scm 294 */
																									BgL_test2412z00_5898 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			if (BgL_test2412z00_5898)
																				{	/* Reduce/sbeta.scm 295 */
																					obj_t BgL_g1351z00_4394;

																					BgL_g1351z00_4394 =
																						(((BgL_letzd2varzd2_bglt) COBJECT(
																								((BgL_letzd2varzd2_bglt)
																									BgL_nodez00_4177)))->
																						BgL_bindingsz00);
																					{
																						obj_t BgL_l1349z00_4396;

																						BgL_l1349z00_4396 =
																							BgL_g1351z00_4394;
																					BgL_zc3z04anonymousza31970ze3z87_4395:
																						if (NULLP
																							(BgL_l1349z00_4396))
																							{	/* Reduce/sbeta.scm 295 */
																								BgL_test2407z00_5873 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Reduce/sbeta.scm 295 */
																								bool_t BgL_test2416z00_5914;

																								{	/* Reduce/sbeta.scm 296 */
																									obj_t BgL_bz00_4397;

																									BgL_bz00_4397 =
																										CAR(
																										((obj_t)
																											BgL_l1349z00_4396));
																									{	/* Reduce/sbeta.scm 296 */
																										bool_t BgL_test2417z00_5917;

																										{	/* Reduce/sbeta.scm 296 */
																											obj_t BgL_arg1973z00_4398;
																											obj_t BgL_arg1974z00_4399;

																											BgL_arg1973z00_4398 =
																												CAR(
																												((obj_t)
																													BgL_bz00_4397));
																											BgL_arg1974z00_4399 =
																												(((BgL_externz00_bglt)
																													COBJECT((
																															(BgL_externz00_bglt)
																															BgL_abodyz00_4238)))->
																												BgL_exprza2za2);
																											BgL_test2417z00_5917 =
																												BGl_argumentzf3zf3zzreduce_betaz00
																												(((BgL_variablez00_bglt)
																													BgL_arg1973z00_4398),
																												BgL_arg1974z00_4399);
																										}
																										if (BgL_test2417z00_5917)
																											{	/* Reduce/sbeta.scm 300 */
																												bool_t
																													BgL_test2418z00_5924;
																												{	/* Reduce/sbeta.scm 300 */
																													obj_t
																														BgL_arg1972z00_4400;
																													BgL_arg1972z00_4400 =
																														CDR(((obj_t)
																															BgL_bz00_4397));
																													{	/* Reduce/sbeta.scm 300 */
																														obj_t
																															BgL_classz00_4401;
																														BgL_classz00_4401 =
																															BGl_kwotez00zzast_nodez00;
																														if (BGL_OBJECTP
																															(BgL_arg1972z00_4400))
																															{	/* Reduce/sbeta.scm 300 */
																																BgL_objectz00_bglt
																																	BgL_arg1807z00_4402;
																																BgL_arg1807z00_4402
																																	=
																																	(BgL_objectz00_bglt)
																																	(BgL_arg1972z00_4400);
																																if (BGL_CONDEXPAND_ISA_ARCH64())
																																	{	/* Reduce/sbeta.scm 300 */
																																		long
																																			BgL_idxz00_4403;
																																		BgL_idxz00_4403
																																			=
																																			BGL_OBJECT_INHERITANCE_NUM
																																			(BgL_arg1807z00_4402);
																																		BgL_test2418z00_5924
																																			=
																																			(VECTOR_REF
																																			(BGl_za2inheritancesza2z00zz__objectz00,
																																				(BgL_idxz00_4403
																																					+
																																					2L))
																																			==
																																			BgL_classz00_4401);
																																	}
																																else
																																	{	/* Reduce/sbeta.scm 300 */
																																		bool_t
																																			BgL_res2164z00_4406;
																																		{	/* Reduce/sbeta.scm 300 */
																																			obj_t
																																				BgL_oclassz00_4407;
																																			{	/* Reduce/sbeta.scm 300 */
																																				obj_t
																																					BgL_arg1815z00_4408;
																																				long
																																					BgL_arg1816z00_4409;
																																				BgL_arg1815z00_4408
																																					=
																																					(BGl_za2classesza2z00zz__objectz00);
																																				{	/* Reduce/sbeta.scm 300 */
																																					long
																																						BgL_arg1817z00_4410;
																																					BgL_arg1817z00_4410
																																						=
																																						BGL_OBJECT_CLASS_NUM
																																						(BgL_arg1807z00_4402);
																																					BgL_arg1816z00_4409
																																						=
																																						(BgL_arg1817z00_4410
																																						-
																																						OBJECT_TYPE);
																																				}
																																				BgL_oclassz00_4407
																																					=
																																					VECTOR_REF
																																					(BgL_arg1815z00_4408,
																																					BgL_arg1816z00_4409);
																																			}
																																			{	/* Reduce/sbeta.scm 300 */
																																				bool_t
																																					BgL__ortest_1115z00_4411;
																																				BgL__ortest_1115z00_4411
																																					=
																																					(BgL_classz00_4401
																																					==
																																					BgL_oclassz00_4407);
																																				if (BgL__ortest_1115z00_4411)
																																					{	/* Reduce/sbeta.scm 300 */
																																						BgL_res2164z00_4406
																																							=
																																							BgL__ortest_1115z00_4411;
																																					}
																																				else
																																					{	/* Reduce/sbeta.scm 300 */
																																						long
																																							BgL_odepthz00_4412;
																																						{	/* Reduce/sbeta.scm 300 */
																																							obj_t
																																								BgL_arg1804z00_4413;
																																							BgL_arg1804z00_4413
																																								=
																																								(BgL_oclassz00_4407);
																																							BgL_odepthz00_4412
																																								=
																																								BGL_CLASS_DEPTH
																																								(BgL_arg1804z00_4413);
																																						}
																																						if (
																																							(2L
																																								<
																																								BgL_odepthz00_4412))
																																							{	/* Reduce/sbeta.scm 300 */
																																								obj_t
																																									BgL_arg1802z00_4414;
																																								{	/* Reduce/sbeta.scm 300 */
																																									obj_t
																																										BgL_arg1803z00_4415;
																																									BgL_arg1803z00_4415
																																										=
																																										(BgL_oclassz00_4407);
																																									BgL_arg1802z00_4414
																																										=
																																										BGL_CLASS_ANCESTORS_REF
																																										(BgL_arg1803z00_4415,
																																										2L);
																																								}
																																								BgL_res2164z00_4406
																																									=
																																									(BgL_arg1802z00_4414
																																									==
																																									BgL_classz00_4401);
																																							}
																																						else
																																							{	/* Reduce/sbeta.scm 300 */
																																								BgL_res2164z00_4406
																																									=
																																									(
																																									(bool_t)
																																									0);
																																							}
																																					}
																																			}
																																		}
																																		BgL_test2418z00_5924
																																			=
																																			BgL_res2164z00_4406;
																																	}
																															}
																														else
																															{	/* Reduce/sbeta.scm 300 */
																																BgL_test2418z00_5924
																																	=
																																	((bool_t) 0);
																															}
																													}
																												}
																												if (BgL_test2418z00_5924)
																													{	/* Reduce/sbeta.scm 300 */
																														BgL_test2416z00_5914
																															= ((bool_t) 0);
																													}
																												else
																													{	/* Reduce/sbeta.scm 300 */
																														BgL_test2416z00_5914
																															= ((bool_t) 1);
																													}
																											}
																										else
																											{	/* Reduce/sbeta.scm 296 */
																												BgL_test2416z00_5914 =
																													((bool_t) 0);
																											}
																									}
																								}
																								if (BgL_test2416z00_5914)
																									{	/* Reduce/sbeta.scm 295 */
																										obj_t BgL_arg1971z00_4416;

																										BgL_arg1971z00_4416 =
																											CDR(
																											((obj_t)
																												BgL_l1349z00_4396));
																										{
																											obj_t BgL_l1349z00_5951;

																											BgL_l1349z00_5951 =
																												BgL_arg1971z00_4416;
																											BgL_l1349z00_4396 =
																												BgL_l1349z00_5951;
																											goto
																												BgL_zc3z04anonymousza31970ze3z87_4395;
																										}
																									}
																								else
																									{	/* Reduce/sbeta.scm 295 */
																										BgL_test2407z00_5873 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																			else
																				{	/* Reduce/sbeta.scm 294 */
																					BgL_test2407z00_5873 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Reduce/sbeta.scm 293 */
																			BgL_test2407z00_5873 = ((bool_t) 0);
																		}
																}
																if (BgL_test2407z00_5873)
																	{	/* Reduce/sbeta.scm 293 */
																		BGl_za2removedza2z00zzreduce_betaz00 =
																			(BGl_za2removedza2z00zzreduce_betaz00 +
																			bgl_list_length(
																				(((BgL_letzd2varzd2_bglt) COBJECT(
																							((BgL_letzd2varzd2_bglt)
																								BgL_nodez00_4177)))->
																					BgL_bindingsz00)));
																		{	/* Reduce/sbeta.scm 306 */
																			obj_t BgL_nexprza2za2_4417;

																			{	/* Reduce/sbeta.scm 306 */
																				obj_t BgL_arg1967z00_4418;
																				obj_t BgL_arg1968z00_4419;

																				BgL_arg1967z00_4418 =
																					(((BgL_letzd2varzd2_bglt) COBJECT(
																							((BgL_letzd2varzd2_bglt)
																								BgL_nodez00_4177)))->
																					BgL_bindingsz00);
																				BgL_arg1968z00_4419 =
																					(((BgL_externz00_bglt)
																						COBJECT(((BgL_externz00_bglt) ((
																										(BgL_letzd2varzd2_bglt)
																										COBJECT((
																												(BgL_letzd2varzd2_bglt)
																												BgL_nodez00_4177)))->
																									BgL_bodyz00))))->
																					BgL_exprza2za2);
																				BgL_nexprza2za2_4417 =
																					BGl_makezd2argszd2listz00zzreduce_betaz00
																					(BgL_arg1967z00_4418,
																					BgL_arg1968z00_4419);
																			}
																			((((BgL_externz00_bglt) COBJECT(
																							((BgL_externz00_bglt)
																								BgL_abodyz00_4238)))->
																					BgL_exprza2za2) =
																				((obj_t) ((obj_t)
																						BgL_nexprza2za2_4417)), BUNSPEC);
																			if (CBOOL((((BgL_nodezf2effectzf2_bglt)
																							COBJECT((
																									(BgL_nodezf2effectzf2_bglt) (
																										(BgL_externz00_bglt)
																										BgL_abodyz00_4238))))->
																						BgL_sidezd2effectzd2)))
																				{	/* Reduce/sbeta.scm 308 */
																					BFALSE;
																				}
																			else
																				{	/* Reduce/sbeta.scm 309 */
																					bool_t BgL_arg1962z00_4420;

																					{
																						obj_t BgL_l1353z00_4422;

																						BgL_l1353z00_4422 =
																							BgL_nexprza2za2_4417;
																					BgL_zc3z04anonymousza31963ze3z87_4421:
																						if (NULLP
																							(BgL_l1353z00_4422))
																							{	/* Reduce/sbeta.scm 309 */
																								BgL_arg1962z00_4420 =
																									((bool_t) 0);
																							}
																						else
																							{	/* Reduce/sbeta.scm 309 */
																								bool_t BgL__ortest_1355z00_4423;

																								{	/* Reduce/sbeta.scm 309 */
																									obj_t BgL_arg1966z00_4424;

																									BgL_arg1966z00_4424 =
																										CAR(
																										((obj_t)
																											BgL_l1353z00_4422));
																									BgL__ortest_1355z00_4423 =
																										BGl_sidezd2effectzf3z21zzeffect_effectz00
																										(((BgL_nodez00_bglt)
																											BgL_arg1966z00_4424));
																								}
																								if (BgL__ortest_1355z00_4423)
																									{	/* Reduce/sbeta.scm 309 */
																										BgL_arg1962z00_4420 =
																											BgL__ortest_1355z00_4423;
																									}
																								else
																									{	/* Reduce/sbeta.scm 309 */
																										obj_t BgL_arg1965z00_4425;

																										BgL_arg1965z00_4425 =
																											CDR(
																											((obj_t)
																												BgL_l1353z00_4422));
																										{
																											obj_t BgL_l1353z00_5980;

																											BgL_l1353z00_5980 =
																												BgL_arg1965z00_4425;
																											BgL_l1353z00_4422 =
																												BgL_l1353z00_5980;
																											goto
																												BgL_zc3z04anonymousza31963ze3z87_4421;
																										}
																									}
																							}
																					}
																					((((BgL_nodezf2effectzf2_bglt)
																								COBJECT((
																										(BgL_nodezf2effectzf2_bglt)
																										((BgL_externz00_bglt)
																											BgL_abodyz00_4238))))->
																							BgL_sidezd2effectzd2) =
																						((obj_t)
																							BBOOL(BgL_arg1962z00_4420)),
																						BUNSPEC);
																				}
																		}
																		return BgL_abodyz00_4238;
																	}
																else
																	{	/* Reduce/sbeta.scm 293 */
																		return
																			((BgL_nodez00_bglt)
																			((BgL_letzd2varzd2_bglt)
																				BgL_nodez00_4177));
																	}
															}
													}
											}
									}
							}
					}
				else
					{	/* Reduce/sbeta.scm 241 */
						return
							((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_4177));
					}
			}
		}

	}



/* simple?~0 */
	bool_t BGl_simplezf3ze70z14zzreduce_betaz00(obj_t BgL_ez00_2086)
	{
		{	/* Reduce/sbeta.scm 230 */
			{	/* Reduce/sbeta.scm 226 */
				bool_t BgL__ortest_1114z00_2088;

				{	/* Reduce/sbeta.scm 226 */
					obj_t BgL_classz00_3207;

					BgL_classz00_3207 = BGl_atomz00zzast_nodez00;
					if (BGL_OBJECTP(BgL_ez00_2086))
						{	/* Reduce/sbeta.scm 226 */
							BgL_objectz00_bglt BgL_arg1807z00_3209;

							BgL_arg1807z00_3209 = (BgL_objectz00_bglt) (BgL_ez00_2086);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Reduce/sbeta.scm 226 */
									long BgL_idxz00_3215;

									BgL_idxz00_3215 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3209);
									BgL__ortest_1114z00_2088 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3215 + 2L)) == BgL_classz00_3207);
								}
							else
								{	/* Reduce/sbeta.scm 226 */
									bool_t BgL_res2153z00_3240;

									{	/* Reduce/sbeta.scm 226 */
										obj_t BgL_oclassz00_3223;

										{	/* Reduce/sbeta.scm 226 */
											obj_t BgL_arg1815z00_3231;
											long BgL_arg1816z00_3232;

											BgL_arg1815z00_3231 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Reduce/sbeta.scm 226 */
												long BgL_arg1817z00_3233;

												BgL_arg1817z00_3233 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3209);
												BgL_arg1816z00_3232 =
													(BgL_arg1817z00_3233 - OBJECT_TYPE);
											}
											BgL_oclassz00_3223 =
												VECTOR_REF(BgL_arg1815z00_3231, BgL_arg1816z00_3232);
										}
										{	/* Reduce/sbeta.scm 226 */
											bool_t BgL__ortest_1115z00_3224;

											BgL__ortest_1115z00_3224 =
												(BgL_classz00_3207 == BgL_oclassz00_3223);
											if (BgL__ortest_1115z00_3224)
												{	/* Reduce/sbeta.scm 226 */
													BgL_res2153z00_3240 = BgL__ortest_1115z00_3224;
												}
											else
												{	/* Reduce/sbeta.scm 226 */
													long BgL_odepthz00_3225;

													{	/* Reduce/sbeta.scm 226 */
														obj_t BgL_arg1804z00_3226;

														BgL_arg1804z00_3226 = (BgL_oclassz00_3223);
														BgL_odepthz00_3225 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3226);
													}
													if ((2L < BgL_odepthz00_3225))
														{	/* Reduce/sbeta.scm 226 */
															obj_t BgL_arg1802z00_3228;

															{	/* Reduce/sbeta.scm 226 */
																obj_t BgL_arg1803z00_3229;

																BgL_arg1803z00_3229 = (BgL_oclassz00_3223);
																BgL_arg1802z00_3228 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3229,
																	2L);
															}
															BgL_res2153z00_3240 =
																(BgL_arg1802z00_3228 == BgL_classz00_3207);
														}
													else
														{	/* Reduce/sbeta.scm 226 */
															BgL_res2153z00_3240 = ((bool_t) 0);
														}
												}
										}
									}
									BgL__ortest_1114z00_2088 = BgL_res2153z00_3240;
								}
						}
					else
						{	/* Reduce/sbeta.scm 226 */
							BgL__ortest_1114z00_2088 = ((bool_t) 0);
						}
				}
				if (BgL__ortest_1114z00_2088)
					{	/* Reduce/sbeta.scm 226 */
						return BgL__ortest_1114z00_2088;
					}
				else
					{	/* Reduce/sbeta.scm 227 */
						bool_t BgL__ortest_1116z00_2089;

						{	/* Reduce/sbeta.scm 227 */
							obj_t BgL_classz00_3241;

							BgL_classz00_3241 = BGl_varz00zzast_nodez00;
							if (BGL_OBJECTP(BgL_ez00_2086))
								{	/* Reduce/sbeta.scm 227 */
									BgL_objectz00_bglt BgL_arg1807z00_3243;

									BgL_arg1807z00_3243 = (BgL_objectz00_bglt) (BgL_ez00_2086);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Reduce/sbeta.scm 227 */
											long BgL_idxz00_3249;

											BgL_idxz00_3249 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3243);
											BgL__ortest_1116z00_2089 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_3249 + 2L)) == BgL_classz00_3241);
										}
									else
										{	/* Reduce/sbeta.scm 227 */
											bool_t BgL_res2154z00_3274;

											{	/* Reduce/sbeta.scm 227 */
												obj_t BgL_oclassz00_3257;

												{	/* Reduce/sbeta.scm 227 */
													obj_t BgL_arg1815z00_3265;
													long BgL_arg1816z00_3266;

													BgL_arg1815z00_3265 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Reduce/sbeta.scm 227 */
														long BgL_arg1817z00_3267;

														BgL_arg1817z00_3267 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3243);
														BgL_arg1816z00_3266 =
															(BgL_arg1817z00_3267 - OBJECT_TYPE);
													}
													BgL_oclassz00_3257 =
														VECTOR_REF(BgL_arg1815z00_3265,
														BgL_arg1816z00_3266);
												}
												{	/* Reduce/sbeta.scm 227 */
													bool_t BgL__ortest_1115z00_3258;

													BgL__ortest_1115z00_3258 =
														(BgL_classz00_3241 == BgL_oclassz00_3257);
													if (BgL__ortest_1115z00_3258)
														{	/* Reduce/sbeta.scm 227 */
															BgL_res2154z00_3274 = BgL__ortest_1115z00_3258;
														}
													else
														{	/* Reduce/sbeta.scm 227 */
															long BgL_odepthz00_3259;

															{	/* Reduce/sbeta.scm 227 */
																obj_t BgL_arg1804z00_3260;

																BgL_arg1804z00_3260 = (BgL_oclassz00_3257);
																BgL_odepthz00_3259 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_3260);
															}
															if ((2L < BgL_odepthz00_3259))
																{	/* Reduce/sbeta.scm 227 */
																	obj_t BgL_arg1802z00_3262;

																	{	/* Reduce/sbeta.scm 227 */
																		obj_t BgL_arg1803z00_3263;

																		BgL_arg1803z00_3263 = (BgL_oclassz00_3257);
																		BgL_arg1802z00_3262 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_3263, 2L);
																	}
																	BgL_res2154z00_3274 =
																		(BgL_arg1802z00_3262 == BgL_classz00_3241);
																}
															else
																{	/* Reduce/sbeta.scm 227 */
																	BgL_res2154z00_3274 = ((bool_t) 0);
																}
														}
												}
											}
											BgL__ortest_1116z00_2089 = BgL_res2154z00_3274;
										}
								}
							else
								{	/* Reduce/sbeta.scm 227 */
									BgL__ortest_1116z00_2089 = ((bool_t) 0);
								}
						}
						if (BgL__ortest_1116z00_2089)
							{	/* Reduce/sbeta.scm 227 */
								return BgL__ortest_1116z00_2089;
							}
						else
							{	/* Reduce/sbeta.scm 228 */
								bool_t BgL__ortest_1118z00_2090;

								{	/* Reduce/sbeta.scm 228 */
									bool_t BgL_test2436z00_6035;

									{	/* Reduce/sbeta.scm 228 */
										obj_t BgL_classz00_3275;

										BgL_classz00_3275 = BGl_vrefz00zzast_nodez00;
										if (BGL_OBJECTP(BgL_ez00_2086))
											{	/* Reduce/sbeta.scm 228 */
												BgL_objectz00_bglt BgL_arg1807z00_3277;

												BgL_arg1807z00_3277 =
													(BgL_objectz00_bglt) (BgL_ez00_2086);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Reduce/sbeta.scm 228 */
														long BgL_idxz00_3283;

														BgL_idxz00_3283 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3277);
														BgL_test2436z00_6035 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3283 + 5L)) == BgL_classz00_3275);
													}
												else
													{	/* Reduce/sbeta.scm 228 */
														bool_t BgL_res2155z00_3308;

														{	/* Reduce/sbeta.scm 228 */
															obj_t BgL_oclassz00_3291;

															{	/* Reduce/sbeta.scm 228 */
																obj_t BgL_arg1815z00_3299;
																long BgL_arg1816z00_3300;

																BgL_arg1815z00_3299 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Reduce/sbeta.scm 228 */
																	long BgL_arg1817z00_3301;

																	BgL_arg1817z00_3301 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3277);
																	BgL_arg1816z00_3300 =
																		(BgL_arg1817z00_3301 - OBJECT_TYPE);
																}
																BgL_oclassz00_3291 =
																	VECTOR_REF(BgL_arg1815z00_3299,
																	BgL_arg1816z00_3300);
															}
															{	/* Reduce/sbeta.scm 228 */
																bool_t BgL__ortest_1115z00_3292;

																BgL__ortest_1115z00_3292 =
																	(BgL_classz00_3275 == BgL_oclassz00_3291);
																if (BgL__ortest_1115z00_3292)
																	{	/* Reduce/sbeta.scm 228 */
																		BgL_res2155z00_3308 =
																			BgL__ortest_1115z00_3292;
																	}
																else
																	{	/* Reduce/sbeta.scm 228 */
																		long BgL_odepthz00_3293;

																		{	/* Reduce/sbeta.scm 228 */
																			obj_t BgL_arg1804z00_3294;

																			BgL_arg1804z00_3294 =
																				(BgL_oclassz00_3291);
																			BgL_odepthz00_3293 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3294);
																		}
																		if ((5L < BgL_odepthz00_3293))
																			{	/* Reduce/sbeta.scm 228 */
																				obj_t BgL_arg1802z00_3296;

																				{	/* Reduce/sbeta.scm 228 */
																					obj_t BgL_arg1803z00_3297;

																					BgL_arg1803z00_3297 =
																						(BgL_oclassz00_3291);
																					BgL_arg1802z00_3296 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3297, 5L);
																				}
																				BgL_res2155z00_3308 =
																					(BgL_arg1802z00_3296 ==
																					BgL_classz00_3275);
																			}
																		else
																			{	/* Reduce/sbeta.scm 228 */
																				BgL_res2155z00_3308 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2436z00_6035 = BgL_res2155z00_3308;
													}
											}
										else
											{	/* Reduce/sbeta.scm 228 */
												BgL_test2436z00_6035 = ((bool_t) 0);
											}
									}
									if (BgL_test2436z00_6035)
										{	/* Reduce/sbeta.scm 228 */
											obj_t BgL_g1309z00_2117;

											BgL_g1309z00_2117 =
												(((BgL_externz00_bglt) COBJECT(
														((BgL_externz00_bglt)
															((BgL_vrefz00_bglt) BgL_ez00_2086))))->
												BgL_exprza2za2);
											{
												obj_t BgL_l1307z00_2119;

												BgL_l1307z00_2119 = BgL_g1309z00_2117;
											BgL_zc3z04anonymousza32025ze3z87_2120:
												if (NULLP(BgL_l1307z00_2119))
													{	/* Reduce/sbeta.scm 228 */
														BgL__ortest_1118z00_2090 = ((bool_t) 1);
													}
												else
													{	/* Reduce/sbeta.scm 228 */
														bool_t BgL_test2442z00_6063;

														{	/* Reduce/sbeta.scm 228 */
															obj_t BgL_arg2029z00_2125;

															BgL_arg2029z00_2125 =
																CAR(((obj_t) BgL_l1307z00_2119));
															BgL_test2442z00_6063 =
																BGl_simplezf3ze70z14zzreduce_betaz00
																(BgL_arg2029z00_2125);
														}
														if (BgL_test2442z00_6063)
															{	/* Reduce/sbeta.scm 228 */
																obj_t BgL_arg2027z00_2124;

																BgL_arg2027z00_2124 =
																	CDR(((obj_t) BgL_l1307z00_2119));
																{
																	obj_t BgL_l1307z00_6069;

																	BgL_l1307z00_6069 = BgL_arg2027z00_2124;
																	BgL_l1307z00_2119 = BgL_l1307z00_6069;
																	goto BgL_zc3z04anonymousza32025ze3z87_2120;
																}
															}
														else
															{	/* Reduce/sbeta.scm 228 */
																BgL__ortest_1118z00_2090 = ((bool_t) 0);
															}
													}
											}
										}
									else
										{	/* Reduce/sbeta.scm 228 */
											BgL__ortest_1118z00_2090 = ((bool_t) 0);
										}
								}
								if (BgL__ortest_1118z00_2090)
									{	/* Reduce/sbeta.scm 228 */
										return BgL__ortest_1118z00_2090;
									}
								else
									{	/* Reduce/sbeta.scm 229 */
										bool_t BgL__ortest_1119z00_2091;

										{	/* Reduce/sbeta.scm 229 */
											bool_t BgL_test2444z00_6071;

											{	/* Reduce/sbeta.scm 229 */
												obj_t BgL_classz00_3312;

												BgL_classz00_3312 = BGl_appz00zzast_nodez00;
												if (BGL_OBJECTP(BgL_ez00_2086))
													{	/* Reduce/sbeta.scm 229 */
														BgL_objectz00_bglt BgL_arg1807z00_3314;

														BgL_arg1807z00_3314 =
															(BgL_objectz00_bglt) (BgL_ez00_2086);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Reduce/sbeta.scm 229 */
																long BgL_idxz00_3320;

																BgL_idxz00_3320 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3314);
																BgL_test2444z00_6071 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3320 + 3L)) ==
																	BgL_classz00_3312);
															}
														else
															{	/* Reduce/sbeta.scm 229 */
																bool_t BgL_res2156z00_3345;

																{	/* Reduce/sbeta.scm 229 */
																	obj_t BgL_oclassz00_3328;

																	{	/* Reduce/sbeta.scm 229 */
																		obj_t BgL_arg1815z00_3336;
																		long BgL_arg1816z00_3337;

																		BgL_arg1815z00_3336 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Reduce/sbeta.scm 229 */
																			long BgL_arg1817z00_3338;

																			BgL_arg1817z00_3338 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3314);
																			BgL_arg1816z00_3337 =
																				(BgL_arg1817z00_3338 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3328 =
																			VECTOR_REF(BgL_arg1815z00_3336,
																			BgL_arg1816z00_3337);
																	}
																	{	/* Reduce/sbeta.scm 229 */
																		bool_t BgL__ortest_1115z00_3329;

																		BgL__ortest_1115z00_3329 =
																			(BgL_classz00_3312 == BgL_oclassz00_3328);
																		if (BgL__ortest_1115z00_3329)
																			{	/* Reduce/sbeta.scm 229 */
																				BgL_res2156z00_3345 =
																					BgL__ortest_1115z00_3329;
																			}
																		else
																			{	/* Reduce/sbeta.scm 229 */
																				long BgL_odepthz00_3330;

																				{	/* Reduce/sbeta.scm 229 */
																					obj_t BgL_arg1804z00_3331;

																					BgL_arg1804z00_3331 =
																						(BgL_oclassz00_3328);
																					BgL_odepthz00_3330 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3331);
																				}
																				if ((3L < BgL_odepthz00_3330))
																					{	/* Reduce/sbeta.scm 229 */
																						obj_t BgL_arg1802z00_3333;

																						{	/* Reduce/sbeta.scm 229 */
																							obj_t BgL_arg1803z00_3334;

																							BgL_arg1803z00_3334 =
																								(BgL_oclassz00_3328);
																							BgL_arg1802z00_3333 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3334, 3L);
																						}
																						BgL_res2156z00_3345 =
																							(BgL_arg1802z00_3333 ==
																							BgL_classz00_3312);
																					}
																				else
																					{	/* Reduce/sbeta.scm 229 */
																						BgL_res2156z00_3345 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2444z00_6071 = BgL_res2156z00_3345;
															}
													}
												else
													{	/* Reduce/sbeta.scm 229 */
														BgL_test2444z00_6071 = ((bool_t) 0);
													}
											}
											if (BgL_test2444z00_6071)
												{	/* Reduce/sbeta.scm 229 */
													bool_t BgL_test2449z00_6094;

													{	/* Reduce/sbeta.scm 229 */
														obj_t BgL_g1313z00_2106;

														BgL_g1313z00_2106 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_ez00_2086)))->
															BgL_argsz00);
														{
															obj_t BgL_l1311z00_2108;

															BgL_l1311z00_2108 = BgL_g1313z00_2106;
														BgL_zc3z04anonymousza32020ze3z87_2109:
															if (NULLP(BgL_l1311z00_2108))
																{	/* Reduce/sbeta.scm 229 */
																	BgL_test2449z00_6094 = ((bool_t) 1);
																}
															else
																{	/* Reduce/sbeta.scm 229 */
																	bool_t BgL_test2451z00_6099;

																	{	/* Reduce/sbeta.scm 229 */
																		obj_t BgL_arg2024z00_2114;

																		BgL_arg2024z00_2114 =
																			CAR(((obj_t) BgL_l1311z00_2108));
																		BgL_test2451z00_6099 =
																			BGl_simplezf3ze70z14zzreduce_betaz00
																			(BgL_arg2024z00_2114);
																	}
																	if (BgL_test2451z00_6099)
																		{	/* Reduce/sbeta.scm 229 */
																			obj_t BgL_arg2022z00_2113;

																			BgL_arg2022z00_2113 =
																				CDR(((obj_t) BgL_l1311z00_2108));
																			{
																				obj_t BgL_l1311z00_6105;

																				BgL_l1311z00_6105 = BgL_arg2022z00_2113;
																				BgL_l1311z00_2108 = BgL_l1311z00_6105;
																				goto
																					BgL_zc3z04anonymousza32020ze3z87_2109;
																			}
																		}
																	else
																		{	/* Reduce/sbeta.scm 229 */
																			BgL_test2449z00_6094 = ((bool_t) 0);
																		}
																}
														}
													}
													if (BgL_test2449z00_6094)
														{	/* Reduce/sbeta.scm 229 */
															if (BGl_dangerouszf3zf3zzreduce_betaz00
																(BgL_ez00_2086))
																{	/* Reduce/sbeta.scm 229 */
																	BgL__ortest_1119z00_2091 = ((bool_t) 0);
																}
															else
																{	/* Reduce/sbeta.scm 229 */
																	BgL__ortest_1119z00_2091 = ((bool_t) 1);
																}
														}
													else
														{	/* Reduce/sbeta.scm 229 */
															BgL__ortest_1119z00_2091 = ((bool_t) 0);
														}
												}
											else
												{	/* Reduce/sbeta.scm 229 */
													BgL__ortest_1119z00_2091 = ((bool_t) 0);
												}
										}
										if (BgL__ortest_1119z00_2091)
											{	/* Reduce/sbeta.scm 229 */
												return BgL__ortest_1119z00_2091;
											}
										else
											{	/* Reduce/sbeta.scm 230 */
												bool_t BgL_test2454z00_6109;

												{	/* Reduce/sbeta.scm 230 */
													obj_t BgL_classz00_3349;

													BgL_classz00_3349 = BGl_getfieldz00zzast_nodez00;
													if (BGL_OBJECTP(BgL_ez00_2086))
														{	/* Reduce/sbeta.scm 230 */
															BgL_objectz00_bglt BgL_arg1807z00_3351;

															BgL_arg1807z00_3351 =
																(BgL_objectz00_bglt) (BgL_ez00_2086);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Reduce/sbeta.scm 230 */
																	long BgL_idxz00_3357;

																	BgL_idxz00_3357 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3351);
																	BgL_test2454z00_6109 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3357 + 5L)) ==
																		BgL_classz00_3349);
																}
															else
																{	/* Reduce/sbeta.scm 230 */
																	bool_t BgL_res2157z00_3382;

																	{	/* Reduce/sbeta.scm 230 */
																		obj_t BgL_oclassz00_3365;

																		{	/* Reduce/sbeta.scm 230 */
																			obj_t BgL_arg1815z00_3373;
																			long BgL_arg1816z00_3374;

																			BgL_arg1815z00_3373 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Reduce/sbeta.scm 230 */
																				long BgL_arg1817z00_3375;

																				BgL_arg1817z00_3375 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3351);
																				BgL_arg1816z00_3374 =
																					(BgL_arg1817z00_3375 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3365 =
																				VECTOR_REF(BgL_arg1815z00_3373,
																				BgL_arg1816z00_3374);
																		}
																		{	/* Reduce/sbeta.scm 230 */
																			bool_t BgL__ortest_1115z00_3366;

																			BgL__ortest_1115z00_3366 =
																				(BgL_classz00_3349 ==
																				BgL_oclassz00_3365);
																			if (BgL__ortest_1115z00_3366)
																				{	/* Reduce/sbeta.scm 230 */
																					BgL_res2157z00_3382 =
																						BgL__ortest_1115z00_3366;
																				}
																			else
																				{	/* Reduce/sbeta.scm 230 */
																					long BgL_odepthz00_3367;

																					{	/* Reduce/sbeta.scm 230 */
																						obj_t BgL_arg1804z00_3368;

																						BgL_arg1804z00_3368 =
																							(BgL_oclassz00_3365);
																						BgL_odepthz00_3367 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3368);
																					}
																					if ((5L < BgL_odepthz00_3367))
																						{	/* Reduce/sbeta.scm 230 */
																							obj_t BgL_arg1802z00_3370;

																							{	/* Reduce/sbeta.scm 230 */
																								obj_t BgL_arg1803z00_3371;

																								BgL_arg1803z00_3371 =
																									(BgL_oclassz00_3365);
																								BgL_arg1802z00_3370 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3371, 5L);
																							}
																							BgL_res2157z00_3382 =
																								(BgL_arg1802z00_3370 ==
																								BgL_classz00_3349);
																						}
																					else
																						{	/* Reduce/sbeta.scm 230 */
																							BgL_res2157z00_3382 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2454z00_6109 = BgL_res2157z00_3382;
																}
														}
													else
														{	/* Reduce/sbeta.scm 230 */
															BgL_test2454z00_6109 = ((bool_t) 0);
														}
												}
												if (BgL_test2454z00_6109)
													{	/* Reduce/sbeta.scm 230 */
														obj_t BgL_g1317z00_2093;

														BgL_g1317z00_2093 =
															(((BgL_externz00_bglt) COBJECT(
																	((BgL_externz00_bglt)
																		((BgL_getfieldz00_bglt) BgL_ez00_2086))))->
															BgL_exprza2za2);
														{
															obj_t BgL_l1315z00_2095;

															BgL_l1315z00_2095 = BgL_g1317z00_2093;
														BgL_zc3z04anonymousza32015ze3z87_2096:
															if (NULLP(BgL_l1315z00_2095))
																{	/* Reduce/sbeta.scm 230 */
																	return ((bool_t) 1);
																}
															else
																{	/* Reduce/sbeta.scm 230 */
																	bool_t BgL_test2460z00_6137;

																	{	/* Reduce/sbeta.scm 230 */
																		obj_t BgL_arg2018z00_2101;

																		BgL_arg2018z00_2101 =
																			CAR(((obj_t) BgL_l1315z00_2095));
																		BgL_test2460z00_6137 =
																			BGl_simplezf3ze70z14zzreduce_betaz00
																			(BgL_arg2018z00_2101);
																	}
																	if (BgL_test2460z00_6137)
																		{	/* Reduce/sbeta.scm 230 */
																			obj_t BgL_arg2017z00_2100;

																			BgL_arg2017z00_2100 =
																				CDR(((obj_t) BgL_l1315z00_2095));
																			{
																				obj_t BgL_l1315z00_6143;

																				BgL_l1315z00_6143 = BgL_arg2017z00_2100;
																				BgL_l1315z00_2095 = BgL_l1315z00_6143;
																				goto
																					BgL_zc3z04anonymousza32015ze3z87_2096;
																			}
																		}
																	else
																		{	/* Reduce/sbeta.scm 230 */
																			return ((bool_t) 0);
																		}
																}
														}
													}
												else
													{	/* Reduce/sbeta.scm 230 */
														return ((bool_t) 0);
													}
											}
									}
							}
					}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzreduce_betaz00(void)
	{
		{	/* Reduce/sbeta.scm 25 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2199z00zzreduce_betaz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2199z00zzreduce_betaz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2199z00zzreduce_betaz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2199z00zzreduce_betaz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2199z00zzreduce_betaz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2199z00zzreduce_betaz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2199z00zzreduce_betaz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2199z00zzreduce_betaz00));
			BGl_modulezd2initializa7ationz75zzcoerce_coercez00(361167184L,
				BSTRING_TO_STRING(BGl_string2199z00zzreduce_betaz00));
			BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string2199z00zzreduce_betaz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2199z00zzreduce_betaz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2199z00zzreduce_betaz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2199z00zzreduce_betaz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2199z00zzreduce_betaz00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string2199z00zzreduce_betaz00));
			return
				BGl_modulezd2initializa7ationz75zzast_occurz00(282085879L,
				BSTRING_TO_STRING(BGl_string2199z00zzreduce_betaz00));
		}

	}

#ifdef __cplusplus
}
#endif
