/*===========================================================================*/
/*   (Reduce/cond.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Reduce/cond.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_REDUCE_COND_TYPE_DEFINITIONS
#define BGL_REDUCE_COND_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_REDUCE_COND_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62staticzd2valuezd2atom1327z62zzreduce_condz00(obj_t,
		obj_t);
	static obj_t BGl_z62staticzd2valuezd2sequenc1329z62zzreduce_condz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_nodezd2condz12zc0zzreduce_condz00(BgL_nodez00_bglt);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_returnz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2letzd2fun1305za2zzreduce_condz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzreduce_condz00 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	static obj_t BGl_z62reducezd2conditionalz12za2zzreduce_condz00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62nodezd2condz12za2zzreduce_condz00(obj_t,
		obj_t);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62nodezd2condz121271za2zzreduce_condz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzreduce_condz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2boxzd2setz121319zb0zzreduce_condz00(obj_t, obj_t);
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzreduce_condz00(void);
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2makezd2box1317za2zzreduce_condz00(obj_t, obj_t);
	static obj_t BGl_staticzd2valuezd2zzreduce_condz00(BgL_nodez00_bglt);
	static obj_t BGl_objectzd2initzd2zzreduce_condz00(void);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2var1279z70zzreduce_condz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2atom1274z70zzreduce_condz00(obj_t, obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_nodezd2condza2z12z62zzreduce_condz00(obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2kwote1276z70zzreduce_condz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_reducezd2conditionalz12zc0zzreduce_condz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzreduce_condz00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2app1287z70zzreduce_condz00(obj_t, obj_t);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2letzd2var1307za2zzreduce_condz00(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_za2z42zd3fxza2z91zzreduce_condz00 = BUNSPEC;
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2fail1301z70zzreduce_condz00(obj_t, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2boxzd2ref1321za2zzreduce_condz00(obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2condition1299z70zzreduce_condz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2return1315z70zzreduce_condz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzreduce_condz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2jumpzd2exzd2i1311z70zzreduce_condz00(obj_t, obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_resetzd2condzd2cachez12z12zzreduce_condz00(void);
	static long BGl_za2condzd2reducedza2zd2zzreduce_condz00 = 0L;
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2switch1303z70zzreduce_condz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2setzd2exzd2it1309z70zzreduce_condz00(obj_t, obj_t);
	static obj_t BGl_z62staticzd2valuezb0zzreduce_condz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2funcall1291z70zzreduce_condz00(obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2cast1295z70zzreduce_condz00(obj_t, obj_t);
	static obj_t BGl_initzd2condzd2cachez12z12zzreduce_condz00(void);
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2sequence1283z70zzreduce_condz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzreduce_condz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzreduce_condz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzreduce_condz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzreduce_condz00(void);
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2appzd2ly1289za2zzreduce_condz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2setq1297z70zzreduce_condz00(obj_t, obj_t);
	extern bool_t BGl_sidezd2effectzf3z21zzeffect_effectz00(BgL_nodez00_bglt);
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2retblock1313z70zzreduce_condz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2closure1281z70zzreduce_condz00(obj_t, obj_t);
	extern obj_t BGl_retblockz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2sync1285z70zzreduce_condz00(obj_t, obj_t);
	static obj_t BGl_z62staticzd2valuezd2letzd2var1325zb0zzreduce_condz00(obj_t,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_z62staticzd2value1322zb0zzreduce_condz00(obj_t, obj_t);
	static obj_t BGl_za2z42eqza2z42zzreduce_condz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_literalz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2extern1293z70zzreduce_condz00(obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[7];


	   
		 
		DEFINE_STATIC_BGL_GENERIC(BGl_staticzd2valuezd2envz00zzreduce_condz00,
		BgL_bgl_za762staticza7d2valu1999z00,
		BGl_z62staticzd2valuezb0zzreduce_condz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reducezd2conditionalz12zd2envz12zzreduce_condz00,
		BgL_bgl_za762reduceza7d2cond2000z00,
		BGl_z62reducezd2conditionalz12za2zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1960z00zzreduce_condz00,
		BgL_bgl_string1960za700za7za7r2001za7, "      conditional expression ", 29);
	      DEFINE_STRING(BGl_string1961z00zzreduce_condz00,
		BgL_bgl_string1961za700za7za7r2002za7, "(reduced: ", 10);
	      DEFINE_STRING(BGl_string1963z00zzreduce_condz00,
		BgL_bgl_string1963za700za7za7r2003za7, "node-cond!1271", 14);
	      DEFINE_STRING(BGl_string1965z00zzreduce_condz00,
		BgL_bgl_string1965za700za7za7r2004za7, "static-value1322", 16);
	      DEFINE_STRING(BGl_string1966z00zzreduce_condz00,
		BgL_bgl_string1966za700za7za7r2005za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1968z00zzreduce_condz00,
		BgL_bgl_string1968za700za7za7r2006za7, "node-cond!", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1962z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712007za7,
		BGl_z62nodezd2condz121271za2zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1964z00zzreduce_condz00,
		BgL_bgl_za762staticza7d2valu2008z00,
		BGl_z62staticzd2value1322zb0zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1967z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712009za7,
		BGl_z62nodezd2condz12zd2atom1274z70zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1969z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712010za7,
		BGl_z62nodezd2condz12zd2kwote1276z70zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1970z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712011za7,
		BGl_z62nodezd2condz12zd2var1279z70zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1971z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712012za7,
		BGl_z62nodezd2condz12zd2closure1281z70zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1972z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712013za7,
		BGl_z62nodezd2condz12zd2sequence1283z70zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1973z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712014za7,
		BGl_z62nodezd2condz12zd2sync1285z70zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1974z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712015za7,
		BGl_z62nodezd2condz12zd2app1287z70zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1975z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712016za7,
		BGl_z62nodezd2condz12zd2appzd2ly1289za2zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1976z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712017za7,
		BGl_z62nodezd2condz12zd2funcall1291z70zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1977z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712018za7,
		BGl_z62nodezd2condz12zd2extern1293z70zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1978z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712019za7,
		BGl_z62nodezd2condz12zd2cast1295z70zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1979z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712020za7,
		BGl_z62nodezd2condz12zd2setq1297z70zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1980z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712021za7,
		BGl_z62nodezd2condz12zd2condition1299z70zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1981z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712022za7,
		BGl_z62nodezd2condz12zd2fail1301z70zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1982z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712023za7,
		BGl_z62nodezd2condz12zd2switch1303z70zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1983z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712024za7,
		BGl_z62nodezd2condz12zd2letzd2fun1305za2zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1984z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712025za7,
		BGl_z62nodezd2condz12zd2letzd2var1307za2zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1985z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712026za7,
		BGl_z62nodezd2condz12zd2setzd2exzd2it1309z70zzreduce_condz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1986z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712027za7,
		BGl_z62nodezd2condz12zd2jumpzd2exzd2i1311z70zzreduce_condz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1993z00zzreduce_condz00,
		BgL_bgl_string1993za700za7za7r2028za7, "static-value", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1987z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712029za7,
		BGl_z62nodezd2condz12zd2retblock1313z70zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1988z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712030za7,
		BGl_z62nodezd2condz12zd2return1315z70zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1989z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712031za7,
		BGl_z62nodezd2condz12zd2makezd2box1317za2zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1996z00zzreduce_condz00,
		BgL_bgl_string1996za700za7za7r2032za7, "reduce_cond", 11);
	      DEFINE_STRING(BGl_string1997z00zzreduce_condz00,
		BgL_bgl_string1997za700za7za7r2033za7,
		"true false node-cond!1271 done $=fx c-eq? foreign ", 50);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1990z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712034za7,
		BGl_z62nodezd2condz12zd2boxzd2setz121319zb0zzreduce_condz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1991z00zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712035za7,
		BGl_z62nodezd2condz12zd2boxzd2ref1321za2zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1992z00zzreduce_condz00,
		BgL_bgl_za762staticza7d2valu2036z00,
		BGl_z62staticzd2valuezd2letzd2var1325zb0zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1994z00zzreduce_condz00,
		BgL_bgl_za762staticza7d2valu2037z00,
		BGl_z62staticzd2valuezd2atom1327z62zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1995z00zzreduce_condz00,
		BgL_bgl_za762staticza7d2valu2038z00,
		BGl_z62staticzd2valuezd2sequenc1329z62zzreduce_condz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
		BgL_bgl_za762nodeza7d2condza712039za7,
		BGl_z62nodezd2condz12za2zzreduce_condz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzreduce_condz00));
		     ADD_ROOT((void *) (&BGl_za2z42zd3fxza2z91zzreduce_condz00));
		     ADD_ROOT((void *) (&BGl_za2z42eqza2z42zzreduce_condz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzreduce_condz00(long
		BgL_checksumz00_2902, char *BgL_fromz00_2903)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzreduce_condz00))
				{
					BGl_requirezd2initializa7ationz75zzreduce_condz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzreduce_condz00();
					BGl_libraryzd2moduleszd2initz00zzreduce_condz00();
					BGl_cnstzd2initzd2zzreduce_condz00();
					BGl_importedzd2moduleszd2initz00zzreduce_condz00();
					BGl_genericzd2initzd2zzreduce_condz00();
					BGl_methodzd2initzd2zzreduce_condz00();
					return BGl_toplevelzd2initzd2zzreduce_condz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzreduce_condz00(void)
	{
		{	/* Reduce/cond.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"reduce_cond");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "reduce_cond");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "reduce_cond");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "reduce_cond");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "reduce_cond");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"reduce_cond");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "reduce_cond");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"reduce_cond");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "reduce_cond");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "reduce_cond");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"reduce_cond");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzreduce_condz00(void)
	{
		{	/* Reduce/cond.scm 15 */
			{	/* Reduce/cond.scm 15 */
				obj_t BgL_cportz00_2610;

				{	/* Reduce/cond.scm 15 */
					obj_t BgL_stringz00_2617;

					BgL_stringz00_2617 = BGl_string1997z00zzreduce_condz00;
					{	/* Reduce/cond.scm 15 */
						obj_t BgL_startz00_2618;

						BgL_startz00_2618 = BINT(0L);
						{	/* Reduce/cond.scm 15 */
							obj_t BgL_endz00_2619;

							BgL_endz00_2619 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2617)));
							{	/* Reduce/cond.scm 15 */

								BgL_cportz00_2610 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2617, BgL_startz00_2618, BgL_endz00_2619);
				}}}}
				{
					long BgL_iz00_2611;

					BgL_iz00_2611 = 6L;
				BgL_loopz00_2612:
					if ((BgL_iz00_2611 == -1L))
						{	/* Reduce/cond.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Reduce/cond.scm 15 */
							{	/* Reduce/cond.scm 15 */
								obj_t BgL_arg1998z00_2613;

								{	/* Reduce/cond.scm 15 */

									{	/* Reduce/cond.scm 15 */
										obj_t BgL_locationz00_2615;

										BgL_locationz00_2615 = BBOOL(((bool_t) 0));
										{	/* Reduce/cond.scm 15 */

											BgL_arg1998z00_2613 =
												BGl_readz00zz__readerz00(BgL_cportz00_2610,
												BgL_locationz00_2615);
										}
									}
								}
								{	/* Reduce/cond.scm 15 */
									int BgL_tmpz00_2934;

									BgL_tmpz00_2934 = (int) (BgL_iz00_2611);
									CNST_TABLE_SET(BgL_tmpz00_2934, BgL_arg1998z00_2613);
							}}
							{	/* Reduce/cond.scm 15 */
								int BgL_auxz00_2616;

								BgL_auxz00_2616 = (int) ((BgL_iz00_2611 - 1L));
								{
									long BgL_iz00_2939;

									BgL_iz00_2939 = (long) (BgL_auxz00_2616);
									BgL_iz00_2611 = BgL_iz00_2939;
									goto BgL_loopz00_2612;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzreduce_condz00(void)
	{
		{	/* Reduce/cond.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzreduce_condz00(void)
	{
		{	/* Reduce/cond.scm 15 */
			BGl_za2z42eqza2z42zzreduce_condz00 = BFALSE;
			BGl_za2z42zd3fxza2z91zzreduce_condz00 = BFALSE;
			BGl_za2condzd2reducedza2zd2zzreduce_condz00 = 0L;
			return BUNSPEC;
		}

	}



/* reduce-conditional! */
	BGL_EXPORTED_DEF obj_t BGl_reducezd2conditionalz12zc0zzreduce_condz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Reduce/cond.scm 33 */
			{	/* Reduce/cond.scm 34 */
				obj_t BgL_list1333z00_1383;

				BgL_list1333z00_1383 =
					MAKE_YOUNG_PAIR(BGl_string1960z00zzreduce_condz00, BNIL);
				BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1333z00_1383);
			}
			BGl_initzd2condzd2cachez12z12zzreduce_condz00();
			{
				obj_t BgL_l1256z00_1385;

				BgL_l1256z00_1385 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31334ze3z87_1386:
				if (PAIRP(BgL_l1256z00_1385))
					{	/* Reduce/cond.scm 36 */
						{	/* Reduce/cond.scm 37 */
							obj_t BgL_globalz00_1388;

							BgL_globalz00_1388 = CAR(BgL_l1256z00_1385);
							{	/* Reduce/cond.scm 37 */
								BgL_valuez00_bglt BgL_funz00_1389;

								BgL_funz00_1389 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_1388))))->
									BgL_valuez00);
								{	/* Reduce/cond.scm 37 */
									obj_t BgL_nodez00_1390;

									BgL_nodez00_1390 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_funz00_1389)))->BgL_bodyz00);
									{	/* Reduce/cond.scm 38 */

										{	/* Reduce/cond.scm 39 */
											BgL_nodez00_bglt BgL_arg1339z00_1391;

											BgL_arg1339z00_1391 =
												BGl_nodezd2condz12zc0zzreduce_condz00(
												((BgL_nodez00_bglt) BgL_nodez00_1390));
											((((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_funz00_1389)))->
													BgL_bodyz00) =
												((obj_t) ((obj_t) BgL_arg1339z00_1391)), BUNSPEC);
										}
										BUNSPEC;
									}
								}
							}
						}
						{
							obj_t BgL_l1256z00_2959;

							BgL_l1256z00_2959 = CDR(BgL_l1256z00_1385);
							BgL_l1256z00_1385 = BgL_l1256z00_2959;
							goto BgL_zc3z04anonymousza31334ze3z87_1386;
						}
					}
				else
					{	/* Reduce/cond.scm 36 */
						((bool_t) 1);
					}
			}
			{	/* Reduce/cond.scm 42 */
				obj_t BgL_list1341z00_1394;

				{	/* Reduce/cond.scm 42 */
					obj_t BgL_arg1342z00_1395;

					{	/* Reduce/cond.scm 42 */
						obj_t BgL_arg1343z00_1396;

						{	/* Reduce/cond.scm 42 */
							obj_t BgL_arg1346z00_1397;

							BgL_arg1346z00_1397 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
							BgL_arg1343z00_1396 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ')')),
								BgL_arg1346z00_1397);
						}
						BgL_arg1342z00_1395 =
							MAKE_YOUNG_PAIR(BINT(BGl_za2condzd2reducedza2zd2zzreduce_condz00),
							BgL_arg1343z00_1396);
					}
					BgL_list1341z00_1394 =
						MAKE_YOUNG_PAIR(BGl_string1961z00zzreduce_condz00,
						BgL_arg1342z00_1395);
				}
				BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1341z00_1394);
			}
			BGl_resetzd2condzd2cachez12z12zzreduce_condz00();
			return BgL_globalsz00_3;
		}

	}



/* &reduce-conditional! */
	obj_t BGl_z62reducezd2conditionalz12za2zzreduce_condz00(obj_t BgL_envz00_2517,
		obj_t BgL_globalsz00_2518)
	{
		{	/* Reduce/cond.scm 33 */
			return
				BGl_reducezd2conditionalz12zc0zzreduce_condz00(BgL_globalsz00_2518);
		}

	}



/* init-cond-cache! */
	obj_t BGl_initzd2condzd2cachez12z12zzreduce_condz00(void)
	{
		{	/* Reduce/cond.scm 56 */
			{	/* Reduce/cond.scm 57 */
				obj_t BgL_list1347z00_1398;

				BgL_list1347z00_1398 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
				BGl_za2z42eqza2z42zzreduce_condz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(1),
					BgL_list1347z00_1398);
			}
			{	/* Reduce/cond.scm 58 */
				obj_t BgL_list1348z00_1399;

				BgL_list1348z00_1399 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
				return (BGl_za2z42zd3fxza2z91zzreduce_condz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(2),
						BgL_list1348z00_1399), BUNSPEC);
			}
		}

	}



/* reset-cond-cache! */
	obj_t BGl_resetzd2condzd2cachez12z12zzreduce_condz00(void)
	{
		{	/* Reduce/cond.scm 63 */
			BGl_za2z42eqza2z42zzreduce_condz00 = BFALSE;
			return (BGl_za2z42zd3fxza2z91zzreduce_condz00 = BFALSE, BUNSPEC);
		}

	}



/* node-cond*! */
	obj_t BGl_nodezd2condza2z12z62zzreduce_condz00(obj_t BgL_nodeza2za2_29)
	{
		{	/* Reduce/cond.scm 343 */
			{
				obj_t BgL_nodeza2za2_1401;

				BgL_nodeza2za2_1401 = BgL_nodeza2za2_29;
			BgL_zc3z04anonymousza31349ze3z87_1402:
				if (NULLP(BgL_nodeza2za2_1401))
					{	/* Reduce/cond.scm 345 */
						return CNST_TABLE_REF(3);
					}
				else
					{	/* Reduce/cond.scm 345 */
						{	/* Reduce/cond.scm 348 */
							BgL_nodez00_bglt BgL_arg1351z00_1404;

							{	/* Reduce/cond.scm 348 */
								obj_t BgL_arg1352z00_1405;

								BgL_arg1352z00_1405 = CAR(((obj_t) BgL_nodeza2za2_1401));
								BgL_arg1351z00_1404 =
									BGl_nodezd2condz12zc0zzreduce_condz00(
									((BgL_nodez00_bglt) BgL_arg1352z00_1405));
							}
							{	/* Reduce/cond.scm 348 */
								obj_t BgL_auxz00_2989;
								obj_t BgL_tmpz00_2987;

								BgL_auxz00_2989 = ((obj_t) BgL_arg1351z00_1404);
								BgL_tmpz00_2987 = ((obj_t) BgL_nodeza2za2_1401);
								SET_CAR(BgL_tmpz00_2987, BgL_auxz00_2989);
							}
						}
						{	/* Reduce/cond.scm 349 */
							obj_t BgL_arg1361z00_1406;

							BgL_arg1361z00_1406 = CDR(((obj_t) BgL_nodeza2za2_1401));
							{
								obj_t BgL_nodeza2za2_2994;

								BgL_nodeza2za2_2994 = BgL_arg1361z00_1406;
								BgL_nodeza2za2_1401 = BgL_nodeza2za2_2994;
								goto BgL_zc3z04anonymousza31349ze3z87_1402;
							}
						}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzreduce_condz00(void)
	{
		{	/* Reduce/cond.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzreduce_condz00(void)
	{
		{	/* Reduce/cond.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_proc1962z00zzreduce_condz00, BGl_nodez00zzast_nodez00,
				BGl_string1963z00zzreduce_condz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_staticzd2valuezd2envz00zzreduce_condz00,
				BGl_proc1964z00zzreduce_condz00, BGl_nodez00zzast_nodez00,
				BGl_string1965z00zzreduce_condz00);
		}

	}



/* &static-value1322 */
	obj_t BGl_z62staticzd2value1322zb0zzreduce_condz00(obj_t BgL_envz00_2521,
		obj_t BgL_nodez00_2522)
	{
		{	/* Reduce/cond.scm 354 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &node-cond!1271 */
	obj_t BGl_z62nodezd2condz121271za2zzreduce_condz00(obj_t BgL_envz00_2523,
		obj_t BgL_nodez00_2524)
	{
		{	/* Reduce/cond.scm 75 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(4),
				BGl_string1966z00zzreduce_condz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2524)));
		}

	}



/* node-cond! */
	BgL_nodez00_bglt BGl_nodezd2condz12zc0zzreduce_condz00(BgL_nodez00_bglt
		BgL_nodez00_4)
	{
		{	/* Reduce/cond.scm 75 */
			{	/* Reduce/cond.scm 75 */
				obj_t BgL_method1272z00_1416;

				{	/* Reduce/cond.scm 75 */
					obj_t BgL_res1940z00_1989;

					{	/* Reduce/cond.scm 75 */
						long BgL_objzd2classzd2numz00_1960;

						BgL_objzd2classzd2numz00_1960 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_4));
						{	/* Reduce/cond.scm 75 */
							obj_t BgL_arg1811z00_1961;

							BgL_arg1811z00_1961 =
								PROCEDURE_REF(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
								(int) (1L));
							{	/* Reduce/cond.scm 75 */
								int BgL_offsetz00_1964;

								BgL_offsetz00_1964 = (int) (BgL_objzd2classzd2numz00_1960);
								{	/* Reduce/cond.scm 75 */
									long BgL_offsetz00_1965;

									BgL_offsetz00_1965 =
										((long) (BgL_offsetz00_1964) - OBJECT_TYPE);
									{	/* Reduce/cond.scm 75 */
										long BgL_modz00_1966;

										BgL_modz00_1966 =
											(BgL_offsetz00_1965 >> (int) ((long) ((int) (4L))));
										{	/* Reduce/cond.scm 75 */
											long BgL_restz00_1968;

											BgL_restz00_1968 =
												(BgL_offsetz00_1965 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Reduce/cond.scm 75 */

												{	/* Reduce/cond.scm 75 */
													obj_t BgL_bucketz00_1970;

													BgL_bucketz00_1970 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1961), BgL_modz00_1966);
													BgL_res1940z00_1989 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1970), BgL_restz00_1968);
					}}}}}}}}
					BgL_method1272z00_1416 = BgL_res1940z00_1989;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1272z00_1416, ((obj_t) BgL_nodez00_4)));
			}
		}

	}



/* &node-cond! */
	BgL_nodez00_bglt BGl_z62nodezd2condz12za2zzreduce_condz00(obj_t
		BgL_envz00_2525, obj_t BgL_nodez00_2526)
	{
		{	/* Reduce/cond.scm 75 */
			return
				BGl_nodezd2condz12zc0zzreduce_condz00(
				((BgL_nodez00_bglt) BgL_nodez00_2526));
		}

	}



/* static-value */
	obj_t BGl_staticzd2valuezd2zzreduce_condz00(BgL_nodez00_bglt BgL_nodez00_30)
	{
		{	/* Reduce/cond.scm 354 */
			{	/* Reduce/cond.scm 354 */
				obj_t BgL_method1323z00_1417;

				{	/* Reduce/cond.scm 354 */
					obj_t BgL_res1945z00_2020;

					{	/* Reduce/cond.scm 354 */
						long BgL_objzd2classzd2numz00_1991;

						BgL_objzd2classzd2numz00_1991 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_30));
						{	/* Reduce/cond.scm 354 */
							obj_t BgL_arg1811z00_1992;

							BgL_arg1811z00_1992 =
								PROCEDURE_REF(BGl_staticzd2valuezd2envz00zzreduce_condz00,
								(int) (1L));
							{	/* Reduce/cond.scm 354 */
								int BgL_offsetz00_1995;

								BgL_offsetz00_1995 = (int) (BgL_objzd2classzd2numz00_1991);
								{	/* Reduce/cond.scm 354 */
									long BgL_offsetz00_1996;

									BgL_offsetz00_1996 =
										((long) (BgL_offsetz00_1995) - OBJECT_TYPE);
									{	/* Reduce/cond.scm 354 */
										long BgL_modz00_1997;

										BgL_modz00_1997 =
											(BgL_offsetz00_1996 >> (int) ((long) ((int) (4L))));
										{	/* Reduce/cond.scm 354 */
											long BgL_restz00_1999;

											BgL_restz00_1999 =
												(BgL_offsetz00_1996 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Reduce/cond.scm 354 */

												{	/* Reduce/cond.scm 354 */
													obj_t BgL_bucketz00_2001;

													BgL_bucketz00_2001 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1992), BgL_modz00_1997);
													BgL_res1945z00_2020 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2001), BgL_restz00_1999);
					}}}}}}}}
					BgL_method1323z00_1417 = BgL_res1945z00_2020;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1323z00_1417, ((obj_t) BgL_nodez00_30));
			}
		}

	}



/* &static-value */
	obj_t BGl_z62staticzd2valuezb0zzreduce_condz00(obj_t BgL_envz00_2527,
		obj_t BgL_nodez00_2528)
	{
		{	/* Reduce/cond.scm 354 */
			return
				BGl_staticzd2valuezd2zzreduce_condz00(
				((BgL_nodez00_bglt) BgL_nodez00_2528));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzreduce_condz00(void)
	{
		{	/* Reduce/cond.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00, BGl_atomz00zzast_nodez00,
				BGl_proc1967z00zzreduce_condz00, BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00, BGl_kwotez00zzast_nodez00,
				BGl_proc1969z00zzreduce_condz00, BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00, BGl_varz00zzast_nodez00,
				BGl_proc1970z00zzreduce_condz00, BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_closurez00zzast_nodez00, BGl_proc1971z00zzreduce_condz00,
				BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1972z00zzreduce_condz00,
				BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00, BGl_syncz00zzast_nodez00,
				BGl_proc1973z00zzreduce_condz00, BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00, BGl_appz00zzast_nodez00,
				BGl_proc1974z00zzreduce_condz00, BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1975z00zzreduce_condz00,
				BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_funcallz00zzast_nodez00, BGl_proc1976z00zzreduce_condz00,
				BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_externz00zzast_nodez00, BGl_proc1977z00zzreduce_condz00,
				BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00, BGl_castz00zzast_nodez00,
				BGl_proc1978z00zzreduce_condz00, BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00, BGl_setqz00zzast_nodez00,
				BGl_proc1979z00zzreduce_condz00, BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1980z00zzreduce_condz00,
				BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00, BGl_failz00zzast_nodez00,
				BGl_proc1981z00zzreduce_condz00, BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_switchz00zzast_nodez00, BGl_proc1982z00zzreduce_condz00,
				BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1983z00zzreduce_condz00,
				BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1984z00zzreduce_condz00,
				BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1985z00zzreduce_condz00,
				BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1986z00zzreduce_condz00,
				BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_retblockz00zzast_nodez00, BGl_proc1987z00zzreduce_condz00,
				BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_returnz00zzast_nodez00, BGl_proc1988z00zzreduce_condz00,
				BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1989z00zzreduce_condz00,
				BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1990z00zzreduce_condz00,
				BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2condz12zd2envz12zzreduce_condz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1991z00zzreduce_condz00,
				BGl_string1968z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_staticzd2valuezd2envz00zzreduce_condz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1992z00zzreduce_condz00,
				BGl_string1993z00zzreduce_condz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_staticzd2valuezd2envz00zzreduce_condz00, BGl_atomz00zzast_nodez00,
				BGl_proc1994z00zzreduce_condz00, BGl_string1993z00zzreduce_condz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_staticzd2valuezd2envz00zzreduce_condz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1995z00zzreduce_condz00,
				BGl_string1993z00zzreduce_condz00);
		}

	}



/* &static-value-sequenc1329 */
	obj_t BGl_z62staticzd2valuezd2sequenc1329z62zzreduce_condz00(obj_t
		BgL_envz00_2556, obj_t BgL_nodez00_2557)
	{
		{	/* Reduce/cond.scm 374 */
			if (NULLP(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_2557)))->BgL_nodesz00)))
				{	/* Reduce/cond.scm 376 */
					return BFALSE;
				}
			else
				{	/* Reduce/cond.scm 377 */
					obj_t BgL_arg1866z00_2624;

					BgL_arg1866z00_2624 =
						CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
							(((BgL_sequencez00_bglt) COBJECT(
										((BgL_sequencez00_bglt) BgL_nodez00_2557)))->
								BgL_nodesz00)));
					return BGl_staticzd2valuezd2zzreduce_condz00(((BgL_nodez00_bglt)
							BgL_arg1866z00_2624));
				}
		}

	}



/* &static-value-atom1327 */
	obj_t BGl_z62staticzd2valuezd2atom1327z62zzreduce_condz00(obj_t
		BgL_envz00_2558, obj_t BgL_nodez00_2559)
	{
		{	/* Reduce/cond.scm 367 */
			if (
				((((BgL_atomz00_bglt) COBJECT(
								((BgL_atomz00_bglt) BgL_nodez00_2559)))->BgL_valuez00) ==
					BFALSE))
				{	/* Reduce/cond.scm 369 */
					return CNST_TABLE_REF(5);
				}
			else
				{	/* Reduce/cond.scm 369 */
					return CNST_TABLE_REF(6);
				}
		}

	}



/* &static-value-let-var1325 */
	obj_t BGl_z62staticzd2valuezd2letzd2var1325zb0zzreduce_condz00(obj_t
		BgL_envz00_2560, obj_t BgL_nodez00_2561)
	{
		{	/* Reduce/cond.scm 360 */
			return
				BGl_staticzd2valuezd2zzreduce_condz00(
				(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2561)))->BgL_bodyz00));
		}

	}



/* &node-cond!-box-ref1321 */
	BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2boxzd2ref1321za2zzreduce_condz00(obj_t
		BgL_envz00_2562, obj_t BgL_nodez00_2563)
	{
		{	/* Reduce/cond.scm 335 */
			{
				BgL_varz00_bglt BgL_auxz00_3113;

				{	/* Reduce/cond.scm 337 */
					BgL_varz00_bglt BgL_arg1857z00_2628;

					BgL_arg1857z00_2628 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2563)))->BgL_varz00);
					BgL_auxz00_3113 =
						((BgL_varz00_bglt)
						BGl_nodezd2condz12zc0zzreduce_condz00(
							((BgL_nodez00_bglt) BgL_arg1857z00_2628)));
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2563)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3113), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2563));
		}

	}



/* &node-cond!-box-set!1319 */
	BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2boxzd2setz121319zb0zzreduce_condz00(obj_t
		BgL_envz00_2564, obj_t BgL_nodez00_2565)
	{
		{	/* Reduce/cond.scm 326 */
			{
				BgL_varz00_bglt BgL_auxz00_3123;

				{	/* Reduce/cond.scm 328 */
					BgL_varz00_bglt BgL_arg1854z00_2630;

					BgL_arg1854z00_2630 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2565)))->BgL_varz00);
					BgL_auxz00_3123 =
						((BgL_varz00_bglt)
						BGl_nodezd2condz12zc0zzreduce_condz00(
							((BgL_nodez00_bglt) BgL_arg1854z00_2630)));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2565)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3123), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3131;

				{	/* Reduce/cond.scm 329 */
					BgL_nodez00_bglt BgL_arg1856z00_2631;

					BgL_arg1856z00_2631 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2565)))->BgL_valuez00);
					BgL_auxz00_3131 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1856z00_2631);
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2565)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3131), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2565));
		}

	}



/* &node-cond!-make-box1317 */
	BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2makezd2box1317za2zzreduce_condz00(obj_t
		BgL_envz00_2566, obj_t BgL_nodez00_2567)
	{
		{	/* Reduce/cond.scm 318 */
			{
				BgL_nodez00_bglt BgL_auxz00_3139;

				{	/* Reduce/cond.scm 320 */
					BgL_nodez00_bglt BgL_arg1853z00_2633;

					BgL_arg1853z00_2633 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2567)))->BgL_valuez00);
					BgL_auxz00_3139 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1853z00_2633);
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2567)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3139), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2567));
		}

	}



/* &node-cond!-return1315 */
	BgL_nodez00_bglt BGl_z62nodezd2condz12zd2return1315z70zzreduce_condz00(obj_t
		BgL_envz00_2568, obj_t BgL_nodez00_2569)
	{
		{	/* Reduce/cond.scm 310 */
			{
				BgL_nodez00_bglt BgL_auxz00_3147;

				{	/* Reduce/cond.scm 312 */
					BgL_nodez00_bglt BgL_arg1852z00_2635;

					BgL_arg1852z00_2635 =
						(((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nodez00_2569)))->BgL_valuez00);
					BgL_auxz00_3147 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1852z00_2635);
				}
				((((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nodez00_2569)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3147), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_returnz00_bglt) BgL_nodez00_2569));
		}

	}



/* &node-cond!-retblock1313 */
	BgL_nodez00_bglt BGl_z62nodezd2condz12zd2retblock1313z70zzreduce_condz00(obj_t
		BgL_envz00_2570, obj_t BgL_nodez00_2571)
	{
		{	/* Reduce/cond.scm 302 */
			{
				BgL_nodez00_bglt BgL_auxz00_3155;

				{	/* Reduce/cond.scm 304 */
					BgL_nodez00_bglt BgL_arg1851z00_2637;

					BgL_arg1851z00_2637 =
						(((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nodez00_2571)))->BgL_bodyz00);
					BgL_auxz00_3155 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1851z00_2637);
				}
				((((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nodez00_2571)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3155), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_retblockz00_bglt) BgL_nodez00_2571));
		}

	}



/* &node-cond!-jump-ex-i1311 */
	BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2jumpzd2exzd2i1311z70zzreduce_condz00(obj_t
		BgL_envz00_2572, obj_t BgL_nodez00_2573)
	{
		{	/* Reduce/cond.scm 293 */
			{
				BgL_nodez00_bglt BgL_auxz00_3163;

				{	/* Reduce/cond.scm 295 */
					BgL_nodez00_bglt BgL_arg1849z00_2639;

					BgL_arg1849z00_2639 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2573)))->BgL_exitz00);
					BgL_auxz00_3163 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1849z00_2639);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2573)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_3163), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3169;

				{	/* Reduce/cond.scm 296 */
					BgL_nodez00_bglt BgL_arg1850z00_2640;

					BgL_arg1850z00_2640 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2573)))->
						BgL_valuez00);
					BgL_auxz00_3169 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1850z00_2640);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2573)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_3169), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2573));
		}

	}



/* &node-cond!-set-ex-it1309 */
	BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2setzd2exzd2it1309z70zzreduce_condz00(obj_t
		BgL_envz00_2574, obj_t BgL_nodez00_2575)
	{
		{	/* Reduce/cond.scm 283 */
			{
				BgL_nodez00_bglt BgL_auxz00_3177;

				{	/* Reduce/cond.scm 285 */
					BgL_nodez00_bglt BgL_arg1846z00_2642;

					BgL_arg1846z00_2642 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2575)))->BgL_bodyz00);
					BgL_auxz00_3177 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1846z00_2642);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2575)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3177), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3183;

				{	/* Reduce/cond.scm 286 */
					BgL_nodez00_bglt BgL_arg1847z00_2643;

					BgL_arg1847z00_2643 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2575)))->
						BgL_onexitz00);
					BgL_auxz00_3183 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1847z00_2643);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2575)))->
						BgL_onexitz00) = ((BgL_nodez00_bglt) BgL_auxz00_3183), BUNSPEC);
			}
			{
				BgL_varz00_bglt BgL_auxz00_3189;

				{	/* Reduce/cond.scm 287 */
					BgL_varz00_bglt BgL_arg1848z00_2644;

					BgL_arg1848z00_2644 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2575)))->BgL_varz00);
					BgL_auxz00_3189 =
						((BgL_varz00_bglt)
						BGl_nodezd2condz12zc0zzreduce_condz00(
							((BgL_nodez00_bglt) BgL_arg1848z00_2644)));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2575)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3189), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2575));
		}

	}



/* &node-cond!-let-var1307 */
	BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2letzd2var1307za2zzreduce_condz00(obj_t
		BgL_envz00_2576, obj_t BgL_nodez00_2577)
	{
		{	/* Reduce/cond.scm 272 */
			{	/* Reduce/cond.scm 274 */
				obj_t BgL_g1270z00_2646;

				BgL_g1270z00_2646 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2577)))->BgL_bindingsz00);
				{
					obj_t BgL_l1268z00_2648;

					BgL_l1268z00_2648 = BgL_g1270z00_2646;
				BgL_zc3z04anonymousza31840ze3z87_2647:
					if (PAIRP(BgL_l1268z00_2648))
						{	/* Reduce/cond.scm 274 */
							{	/* Reduce/cond.scm 275 */
								obj_t BgL_bindingz00_2649;

								BgL_bindingz00_2649 = CAR(BgL_l1268z00_2648);
								{	/* Reduce/cond.scm 275 */
									BgL_nodez00_bglt BgL_arg1842z00_2650;

									{	/* Reduce/cond.scm 275 */
										obj_t BgL_arg1843z00_2651;

										BgL_arg1843z00_2651 = CDR(((obj_t) BgL_bindingz00_2649));
										BgL_arg1842z00_2650 =
											BGl_nodezd2condz12zc0zzreduce_condz00(
											((BgL_nodez00_bglt) BgL_arg1843z00_2651));
									}
									{	/* Reduce/cond.scm 275 */
										obj_t BgL_auxz00_3210;
										obj_t BgL_tmpz00_3208;

										BgL_auxz00_3210 = ((obj_t) BgL_arg1842z00_2650);
										BgL_tmpz00_3208 = ((obj_t) BgL_bindingz00_2649);
										SET_CDR(BgL_tmpz00_3208, BgL_auxz00_3210);
									}
								}
							}
							{
								obj_t BgL_l1268z00_3213;

								BgL_l1268z00_3213 = CDR(BgL_l1268z00_2648);
								BgL_l1268z00_2648 = BgL_l1268z00_3213;
								goto BgL_zc3z04anonymousza31840ze3z87_2647;
							}
						}
					else
						{	/* Reduce/cond.scm 274 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3215;

				{	/* Reduce/cond.scm 277 */
					BgL_nodez00_bglt BgL_arg1845z00_2652;

					BgL_arg1845z00_2652 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2577)))->BgL_bodyz00);
					BgL_auxz00_3215 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1845z00_2652);
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2577)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3215), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_2577));
		}

	}



/* &node-cond!-let-fun1305 */
	BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2letzd2fun1305za2zzreduce_condz00(obj_t
		BgL_envz00_2578, obj_t BgL_nodez00_2579)
	{
		{	/* Reduce/cond.scm 260 */
			{	/* Reduce/cond.scm 262 */
				obj_t BgL_g1267z00_2654;

				BgL_g1267z00_2654 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2579)))->BgL_localsz00);
				{
					obj_t BgL_l1265z00_2656;

					BgL_l1265z00_2656 = BgL_g1267z00_2654;
				BgL_zc3z04anonymousza31834ze3z87_2655:
					if (PAIRP(BgL_l1265z00_2656))
						{	/* Reduce/cond.scm 262 */
							{	/* Reduce/cond.scm 263 */
								obj_t BgL_localz00_2657;

								BgL_localz00_2657 = CAR(BgL_l1265z00_2656);
								{	/* Reduce/cond.scm 263 */
									BgL_valuez00_bglt BgL_funz00_2658;

									BgL_funz00_2658 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2657))))->
										BgL_valuez00);
									{	/* Reduce/cond.scm 264 */
										BgL_nodez00_bglt BgL_arg1836z00_2659;

										{	/* Reduce/cond.scm 264 */
											obj_t BgL_arg1837z00_2660;

											BgL_arg1837z00_2660 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2658)))->
												BgL_bodyz00);
											BgL_arg1836z00_2659 =
												BGl_nodezd2condz12zc0zzreduce_condz00((
													(BgL_nodez00_bglt) BgL_arg1837z00_2660));
										}
										((((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2658)))->
												BgL_bodyz00) =
											((obj_t) ((obj_t) BgL_arg1836z00_2659)), BUNSPEC);
									}
								}
							}
							{
								obj_t BgL_l1265z00_3238;

								BgL_l1265z00_3238 = CDR(BgL_l1265z00_2656);
								BgL_l1265z00_2656 = BgL_l1265z00_3238;
								goto BgL_zc3z04anonymousza31834ze3z87_2655;
							}
						}
					else
						{	/* Reduce/cond.scm 262 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3240;

				{	/* Reduce/cond.scm 266 */
					BgL_nodez00_bglt BgL_arg1839z00_2661;

					BgL_arg1839z00_2661 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2579)))->BgL_bodyz00);
					BgL_auxz00_3240 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1839z00_2661);
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2579)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3240), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2579));
		}

	}



/* &node-cond!-switch1303 */
	BgL_nodez00_bglt BGl_z62nodezd2condz12zd2switch1303z70zzreduce_condz00(obj_t
		BgL_envz00_2580, obj_t BgL_nodez00_2581)
	{
		{	/* Reduce/cond.scm 249 */
			{
				BgL_nodez00_bglt BgL_auxz00_3248;

				{	/* Reduce/cond.scm 251 */
					BgL_nodez00_bglt BgL_arg1822z00_2663;

					BgL_arg1822z00_2663 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2581)))->BgL_testz00);
					BgL_auxz00_3248 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1822z00_2663);
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2581)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3248), BUNSPEC);
			}
			{	/* Reduce/cond.scm 252 */
				obj_t BgL_g1264z00_2664;

				BgL_g1264z00_2664 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2581)))->BgL_clausesz00);
				{
					obj_t BgL_l1262z00_2666;

					BgL_l1262z00_2666 = BgL_g1264z00_2664;
				BgL_zc3z04anonymousza31823ze3z87_2665:
					if (PAIRP(BgL_l1262z00_2666))
						{	/* Reduce/cond.scm 252 */
							{	/* Reduce/cond.scm 253 */
								obj_t BgL_clausez00_2667;

								BgL_clausez00_2667 = CAR(BgL_l1262z00_2666);
								{	/* Reduce/cond.scm 253 */
									BgL_nodez00_bglt BgL_arg1831z00_2668;

									{	/* Reduce/cond.scm 253 */
										obj_t BgL_arg1832z00_2669;

										BgL_arg1832z00_2669 = CDR(((obj_t) BgL_clausez00_2667));
										BgL_arg1831z00_2668 =
											BGl_nodezd2condz12zc0zzreduce_condz00(
											((BgL_nodez00_bglt) BgL_arg1832z00_2669));
									}
									{	/* Reduce/cond.scm 253 */
										obj_t BgL_auxz00_3265;
										obj_t BgL_tmpz00_3263;

										BgL_auxz00_3265 = ((obj_t) BgL_arg1831z00_2668);
										BgL_tmpz00_3263 = ((obj_t) BgL_clausez00_2667);
										SET_CDR(BgL_tmpz00_3263, BgL_auxz00_3265);
									}
								}
							}
							{
								obj_t BgL_l1262z00_3268;

								BgL_l1262z00_3268 = CDR(BgL_l1262z00_2666);
								BgL_l1262z00_2666 = BgL_l1262z00_3268;
								goto BgL_zc3z04anonymousza31823ze3z87_2665;
							}
						}
					else
						{	/* Reduce/cond.scm 252 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_2581));
		}

	}



/* &node-cond!-fail1301 */
	BgL_nodez00_bglt BGl_z62nodezd2condz12zd2fail1301z70zzreduce_condz00(obj_t
		BgL_envz00_2582, obj_t BgL_nodez00_2583)
	{
		{	/* Reduce/cond.scm 239 */
			{
				BgL_nodez00_bglt BgL_auxz00_3272;

				{	/* Reduce/cond.scm 241 */
					BgL_nodez00_bglt BgL_arg1808z00_2671;

					BgL_arg1808z00_2671 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2583)))->BgL_procz00);
					BgL_auxz00_3272 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1808z00_2671);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2583)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3272), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3278;

				{	/* Reduce/cond.scm 242 */
					BgL_nodez00_bglt BgL_arg1812z00_2672;

					BgL_arg1812z00_2672 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2583)))->BgL_msgz00);
					BgL_auxz00_3278 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1812z00_2672);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2583)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3278), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3284;

				{	/* Reduce/cond.scm 243 */
					BgL_nodez00_bglt BgL_arg1820z00_2673;

					BgL_arg1820z00_2673 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2583)))->BgL_objz00);
					BgL_auxz00_3284 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1820z00_2673);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2583)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3284), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_2583));
		}

	}



/* &node-cond!-condition1299 */
	BgL_nodez00_bglt
		BGl_z62nodezd2condz12zd2condition1299z70zzreduce_condz00(obj_t
		BgL_envz00_2584, obj_t BgL_nodez00_2585)
	{
		{	/* Reduce/cond.scm 200 */
			{
				BgL_nodez00_bglt BgL_auxz00_3292;

				{	/* Reduce/cond.scm 202 */
					BgL_nodez00_bglt BgL_arg1615z00_2675;

					BgL_arg1615z00_2675 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->BgL_testz00);
					BgL_auxz00_3292 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1615z00_2675);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3292), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3298;

				{	/* Reduce/cond.scm 203 */
					BgL_nodez00_bglt BgL_arg1616z00_2676;

					BgL_arg1616z00_2676 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->BgL_truez00);
					BgL_auxz00_3298 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1616z00_2676);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3298), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3304;

				{	/* Reduce/cond.scm 204 */
					BgL_nodez00_bglt BgL_arg1625z00_2677;

					BgL_arg1625z00_2677 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->BgL_falsez00);
					BgL_auxz00_3304 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1625z00_2677);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3304), BUNSPEC);
			}
			{	/* Reduce/cond.scm 206 */
				bool_t BgL_test2049z00_3310;

				if (BGl_sidezd2effectzf3z21zzeffect_effectz00(
						(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->BgL_testz00)))
					{	/* Reduce/cond.scm 206 */
						BgL_test2049z00_3310 = ((bool_t) 0);
					}
				else
					{	/* Reduce/cond.scm 207 */
						bool_t BgL_test2051z00_3315;

						{	/* Reduce/cond.scm 207 */
							BgL_nodez00_bglt BgL_arg1805z00_2678;

							BgL_arg1805z00_2678 =
								(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->
								BgL_truez00);
							{	/* Reduce/cond.scm 207 */
								obj_t BgL_classz00_2679;

								BgL_classz00_2679 = BGl_atomz00zzast_nodez00;
								{	/* Reduce/cond.scm 207 */
									BgL_objectz00_bglt BgL_arg1807z00_2680;

									{	/* Reduce/cond.scm 207 */
										obj_t BgL_tmpz00_3318;

										BgL_tmpz00_3318 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1805z00_2678));
										BgL_arg1807z00_2680 =
											(BgL_objectz00_bglt) (BgL_tmpz00_3318);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Reduce/cond.scm 207 */
											long BgL_idxz00_2681;

											BgL_idxz00_2681 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2680);
											BgL_test2051z00_3315 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2681 + 2L)) == BgL_classz00_2679);
										}
									else
										{	/* Reduce/cond.scm 207 */
											bool_t BgL_res1952z00_2684;

											{	/* Reduce/cond.scm 207 */
												obj_t BgL_oclassz00_2685;

												{	/* Reduce/cond.scm 207 */
													obj_t BgL_arg1815z00_2686;
													long BgL_arg1816z00_2687;

													BgL_arg1815z00_2686 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Reduce/cond.scm 207 */
														long BgL_arg1817z00_2688;

														BgL_arg1817z00_2688 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2680);
														BgL_arg1816z00_2687 =
															(BgL_arg1817z00_2688 - OBJECT_TYPE);
													}
													BgL_oclassz00_2685 =
														VECTOR_REF(BgL_arg1815z00_2686,
														BgL_arg1816z00_2687);
												}
												{	/* Reduce/cond.scm 207 */
													bool_t BgL__ortest_1115z00_2689;

													BgL__ortest_1115z00_2689 =
														(BgL_classz00_2679 == BgL_oclassz00_2685);
													if (BgL__ortest_1115z00_2689)
														{	/* Reduce/cond.scm 207 */
															BgL_res1952z00_2684 = BgL__ortest_1115z00_2689;
														}
													else
														{	/* Reduce/cond.scm 207 */
															long BgL_odepthz00_2690;

															{	/* Reduce/cond.scm 207 */
																obj_t BgL_arg1804z00_2691;

																BgL_arg1804z00_2691 = (BgL_oclassz00_2685);
																BgL_odepthz00_2690 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2691);
															}
															if ((2L < BgL_odepthz00_2690))
																{	/* Reduce/cond.scm 207 */
																	obj_t BgL_arg1802z00_2692;

																	{	/* Reduce/cond.scm 207 */
																		obj_t BgL_arg1803z00_2693;

																		BgL_arg1803z00_2693 = (BgL_oclassz00_2685);
																		BgL_arg1802z00_2692 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2693, 2L);
																	}
																	BgL_res1952z00_2684 =
																		(BgL_arg1802z00_2692 == BgL_classz00_2679);
																}
															else
																{	/* Reduce/cond.scm 207 */
																	BgL_res1952z00_2684 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2051z00_3315 = BgL_res1952z00_2684;
										}
								}
							}
						}
						if (BgL_test2051z00_3315)
							{	/* Reduce/cond.scm 207 */
								bool_t BgL_test2055z00_3341;

								{	/* Reduce/cond.scm 207 */
									BgL_nodez00_bglt BgL_arg1799z00_2694;

									BgL_arg1799z00_2694 =
										(((BgL_conditionalz00_bglt) COBJECT(
												((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->
										BgL_falsez00);
									{	/* Reduce/cond.scm 207 */
										obj_t BgL_classz00_2695;

										BgL_classz00_2695 = BGl_atomz00zzast_nodez00;
										{	/* Reduce/cond.scm 207 */
											BgL_objectz00_bglt BgL_arg1807z00_2696;

											{	/* Reduce/cond.scm 207 */
												obj_t BgL_tmpz00_3344;

												BgL_tmpz00_3344 =
													((obj_t) ((BgL_objectz00_bglt) BgL_arg1799z00_2694));
												BgL_arg1807z00_2696 =
													(BgL_objectz00_bglt) (BgL_tmpz00_3344);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Reduce/cond.scm 207 */
													long BgL_idxz00_2697;

													BgL_idxz00_2697 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2696);
													BgL_test2055z00_3341 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2697 + 2L)) == BgL_classz00_2695);
												}
											else
												{	/* Reduce/cond.scm 207 */
													bool_t BgL_res1953z00_2700;

													{	/* Reduce/cond.scm 207 */
														obj_t BgL_oclassz00_2701;

														{	/* Reduce/cond.scm 207 */
															obj_t BgL_arg1815z00_2702;
															long BgL_arg1816z00_2703;

															BgL_arg1815z00_2702 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Reduce/cond.scm 207 */
																long BgL_arg1817z00_2704;

																BgL_arg1817z00_2704 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2696);
																BgL_arg1816z00_2703 =
																	(BgL_arg1817z00_2704 - OBJECT_TYPE);
															}
															BgL_oclassz00_2701 =
																VECTOR_REF(BgL_arg1815z00_2702,
																BgL_arg1816z00_2703);
														}
														{	/* Reduce/cond.scm 207 */
															bool_t BgL__ortest_1115z00_2705;

															BgL__ortest_1115z00_2705 =
																(BgL_classz00_2695 == BgL_oclassz00_2701);
															if (BgL__ortest_1115z00_2705)
																{	/* Reduce/cond.scm 207 */
																	BgL_res1953z00_2700 =
																		BgL__ortest_1115z00_2705;
																}
															else
																{	/* Reduce/cond.scm 207 */
																	long BgL_odepthz00_2706;

																	{	/* Reduce/cond.scm 207 */
																		obj_t BgL_arg1804z00_2707;

																		BgL_arg1804z00_2707 = (BgL_oclassz00_2701);
																		BgL_odepthz00_2706 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2707);
																	}
																	if ((2L < BgL_odepthz00_2706))
																		{	/* Reduce/cond.scm 207 */
																			obj_t BgL_arg1802z00_2708;

																			{	/* Reduce/cond.scm 207 */
																				obj_t BgL_arg1803z00_2709;

																				BgL_arg1803z00_2709 =
																					(BgL_oclassz00_2701);
																				BgL_arg1802z00_2708 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2709, 2L);
																			}
																			BgL_res1953z00_2700 =
																				(BgL_arg1802z00_2708 ==
																				BgL_classz00_2695);
																		}
																	else
																		{	/* Reduce/cond.scm 207 */
																			BgL_res1953z00_2700 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2055z00_3341 = BgL_res1953z00_2700;
												}
										}
									}
								}
								if (BgL_test2055z00_3341)
									{	/* Reduce/cond.scm 207 */
										if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(
												(((BgL_atomz00_bglt) COBJECT(
															((BgL_atomz00_bglt)
																(((BgL_conditionalz00_bglt) COBJECT(
																			((BgL_conditionalz00_bglt)
																				BgL_nodez00_2585)))->BgL_truez00))))->
													BgL_valuez00),
												(((BgL_atomz00_bglt)
														COBJECT(((BgL_atomz00_bglt) ((
																		(BgL_conditionalz00_bglt)
																		COBJECT(((BgL_conditionalz00_bglt)
																				BgL_nodez00_2585)))->BgL_falsez00))))->
													BgL_valuez00)))
											{	/* Reduce/cond.scm 211 */
												bool_t BgL_test2060z00_3377;

												{	/* Reduce/cond.scm 211 */
													obj_t BgL_tmpz00_3378;

													BgL_tmpz00_3378 =
														(((BgL_atomz00_bglt) COBJECT(
																((BgL_atomz00_bglt)
																	(((BgL_conditionalz00_bglt) COBJECT(
																				((BgL_conditionalz00_bglt)
																					BgL_nodez00_2585)))->BgL_truez00))))->
														BgL_valuez00);
													BgL_test2060z00_3377 = REALP(BgL_tmpz00_3378);
												}
												if (BgL_test2060z00_3377)
													{	/* Reduce/cond.scm 211 */
														BgL_test2049z00_3310 = ((bool_t) 0);
													}
												else
													{	/* Reduce/cond.scm 211 */
														BgL_test2049z00_3310 = ((bool_t) 1);
													}
											}
										else
											{	/* Reduce/cond.scm 208 */
												BgL_test2049z00_3310 = ((bool_t) 0);
											}
									}
								else
									{	/* Reduce/cond.scm 207 */
										BgL_test2049z00_3310 = ((bool_t) 0);
									}
							}
						else
							{	/* Reduce/cond.scm 207 */
								BgL_test2049z00_3310 = ((bool_t) 0);
							}
					}
				if (BgL_test2049z00_3310)
					{	/* Reduce/cond.scm 206 */
						return
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->BgL_truez00);
					}
				else
					{	/* Reduce/cond.scm 214 */
						bool_t BgL_test2061z00_3386;

						{	/* Reduce/cond.scm 214 */
							bool_t BgL_test2062z00_3387;

							{	/* Reduce/cond.scm 214 */
								BgL_nodez00_bglt BgL_arg1771z00_2710;

								BgL_arg1771z00_2710 =
									(((BgL_conditionalz00_bglt) COBJECT(
											((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->
									BgL_truez00);
								{	/* Reduce/cond.scm 214 */
									obj_t BgL_classz00_2711;

									BgL_classz00_2711 = BGl_atomz00zzast_nodez00;
									{	/* Reduce/cond.scm 214 */
										BgL_objectz00_bglt BgL_arg1807z00_2712;

										{	/* Reduce/cond.scm 214 */
											obj_t BgL_tmpz00_3390;

											BgL_tmpz00_3390 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg1771z00_2710));
											BgL_arg1807z00_2712 =
												(BgL_objectz00_bglt) (BgL_tmpz00_3390);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Reduce/cond.scm 214 */
												long BgL_idxz00_2713;

												BgL_idxz00_2713 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2712);
												BgL_test2062z00_3387 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2713 + 2L)) == BgL_classz00_2711);
											}
										else
											{	/* Reduce/cond.scm 214 */
												bool_t BgL_res1954z00_2716;

												{	/* Reduce/cond.scm 214 */
													obj_t BgL_oclassz00_2717;

													{	/* Reduce/cond.scm 214 */
														obj_t BgL_arg1815z00_2718;
														long BgL_arg1816z00_2719;

														BgL_arg1815z00_2718 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Reduce/cond.scm 214 */
															long BgL_arg1817z00_2720;

															BgL_arg1817z00_2720 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2712);
															BgL_arg1816z00_2719 =
																(BgL_arg1817z00_2720 - OBJECT_TYPE);
														}
														BgL_oclassz00_2717 =
															VECTOR_REF(BgL_arg1815z00_2718,
															BgL_arg1816z00_2719);
													}
													{	/* Reduce/cond.scm 214 */
														bool_t BgL__ortest_1115z00_2721;

														BgL__ortest_1115z00_2721 =
															(BgL_classz00_2711 == BgL_oclassz00_2717);
														if (BgL__ortest_1115z00_2721)
															{	/* Reduce/cond.scm 214 */
																BgL_res1954z00_2716 = BgL__ortest_1115z00_2721;
															}
														else
															{	/* Reduce/cond.scm 214 */
																long BgL_odepthz00_2722;

																{	/* Reduce/cond.scm 214 */
																	obj_t BgL_arg1804z00_2723;

																	BgL_arg1804z00_2723 = (BgL_oclassz00_2717);
																	BgL_odepthz00_2722 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2723);
																}
																if ((2L < BgL_odepthz00_2722))
																	{	/* Reduce/cond.scm 214 */
																		obj_t BgL_arg1802z00_2724;

																		{	/* Reduce/cond.scm 214 */
																			obj_t BgL_arg1803z00_2725;

																			BgL_arg1803z00_2725 =
																				(BgL_oclassz00_2717);
																			BgL_arg1802z00_2724 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2725, 2L);
																		}
																		BgL_res1954z00_2716 =
																			(BgL_arg1802z00_2724 ==
																			BgL_classz00_2711);
																	}
																else
																	{	/* Reduce/cond.scm 214 */
																		BgL_res1954z00_2716 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2062z00_3387 = BgL_res1954z00_2716;
											}
									}
								}
							}
							if (BgL_test2062z00_3387)
								{	/* Reduce/cond.scm 214 */
									bool_t BgL_test2066z00_3413;

									{	/* Reduce/cond.scm 214 */
										BgL_nodez00_bglt BgL_arg1770z00_2726;

										BgL_arg1770z00_2726 =
											(((BgL_conditionalz00_bglt) COBJECT(
													((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->
											BgL_falsez00);
										{	/* Reduce/cond.scm 214 */
											obj_t BgL_classz00_2727;

											BgL_classz00_2727 = BGl_atomz00zzast_nodez00;
											{	/* Reduce/cond.scm 214 */
												BgL_objectz00_bglt BgL_arg1807z00_2728;

												{	/* Reduce/cond.scm 214 */
													obj_t BgL_tmpz00_3416;

													BgL_tmpz00_3416 =
														((obj_t)
														((BgL_objectz00_bglt) BgL_arg1770z00_2726));
													BgL_arg1807z00_2728 =
														(BgL_objectz00_bglt) (BgL_tmpz00_3416);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Reduce/cond.scm 214 */
														long BgL_idxz00_2729;

														BgL_idxz00_2729 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2728);
														BgL_test2066z00_3413 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2729 + 2L)) == BgL_classz00_2727);
													}
												else
													{	/* Reduce/cond.scm 214 */
														bool_t BgL_res1955z00_2732;

														{	/* Reduce/cond.scm 214 */
															obj_t BgL_oclassz00_2733;

															{	/* Reduce/cond.scm 214 */
																obj_t BgL_arg1815z00_2734;
																long BgL_arg1816z00_2735;

																BgL_arg1815z00_2734 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Reduce/cond.scm 214 */
																	long BgL_arg1817z00_2736;

																	BgL_arg1817z00_2736 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2728);
																	BgL_arg1816z00_2735 =
																		(BgL_arg1817z00_2736 - OBJECT_TYPE);
																}
																BgL_oclassz00_2733 =
																	VECTOR_REF(BgL_arg1815z00_2734,
																	BgL_arg1816z00_2735);
															}
															{	/* Reduce/cond.scm 214 */
																bool_t BgL__ortest_1115z00_2737;

																BgL__ortest_1115z00_2737 =
																	(BgL_classz00_2727 == BgL_oclassz00_2733);
																if (BgL__ortest_1115z00_2737)
																	{	/* Reduce/cond.scm 214 */
																		BgL_res1955z00_2732 =
																			BgL__ortest_1115z00_2737;
																	}
																else
																	{	/* Reduce/cond.scm 214 */
																		long BgL_odepthz00_2738;

																		{	/* Reduce/cond.scm 214 */
																			obj_t BgL_arg1804z00_2739;

																			BgL_arg1804z00_2739 =
																				(BgL_oclassz00_2733);
																			BgL_odepthz00_2738 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2739);
																		}
																		if ((2L < BgL_odepthz00_2738))
																			{	/* Reduce/cond.scm 214 */
																				obj_t BgL_arg1802z00_2740;

																				{	/* Reduce/cond.scm 214 */
																					obj_t BgL_arg1803z00_2741;

																					BgL_arg1803z00_2741 =
																						(BgL_oclassz00_2733);
																					BgL_arg1802z00_2740 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2741, 2L);
																				}
																				BgL_res1955z00_2732 =
																					(BgL_arg1802z00_2740 ==
																					BgL_classz00_2727);
																			}
																		else
																			{	/* Reduce/cond.scm 214 */
																				BgL_res1955z00_2732 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2066z00_3413 = BgL_res1955z00_2732;
													}
											}
										}
									}
									if (BgL_test2066z00_3413)
										{	/* Reduce/cond.scm 214 */
											if (
												((((BgL_atomz00_bglt) COBJECT(
																((BgL_atomz00_bglt)
																	(((BgL_conditionalz00_bglt) COBJECT(
																				((BgL_conditionalz00_bglt)
																					BgL_nodez00_2585)))->BgL_truez00))))->
														BgL_valuez00) == BTRUE))
												{	/* Reduce/cond.scm 215 */
													if (
														((((BgL_atomz00_bglt) COBJECT(
																		((BgL_atomz00_bglt)
																			(((BgL_conditionalz00_bglt) COBJECT(
																						((BgL_conditionalz00_bglt)
																							BgL_nodez00_2585)))->
																				BgL_falsez00))))->BgL_valuez00) ==
															BFALSE))
														{	/* Reduce/cond.scm 217 */
															BgL_typez00_bglt BgL_arg1754z00_2742;

															BgL_arg1754z00_2742 =
																BGl_getzd2typezd2zztype_typeofz00(
																(((BgL_conditionalz00_bglt) COBJECT(
																			((BgL_conditionalz00_bglt)
																				BgL_nodez00_2585)))->BgL_testz00),
																((bool_t) 1));
															BgL_test2061z00_3386 =
																(((obj_t) BgL_arg1754z00_2742) ==
																BGl_za2boolza2z00zztype_cachez00);
														}
													else
														{	/* Reduce/cond.scm 216 */
															BgL_test2061z00_3386 = ((bool_t) 0);
														}
												}
											else
												{	/* Reduce/cond.scm 215 */
													BgL_test2061z00_3386 = ((bool_t) 0);
												}
										}
									else
										{	/* Reduce/cond.scm 214 */
											BgL_test2061z00_3386 = ((bool_t) 0);
										}
								}
							else
								{	/* Reduce/cond.scm 214 */
									BgL_test2061z00_3386 = ((bool_t) 0);
								}
						}
						if (BgL_test2061z00_3386)
							{	/* Reduce/cond.scm 214 */
								return
									(((BgL_conditionalz00_bglt) COBJECT(
											((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->
									BgL_testz00);
							}
						else
							{	/* Reduce/cond.scm 219 */
								bool_t BgL_test2072z00_3458;

								{	/* Reduce/cond.scm 219 */
									BgL_nodez00_bglt BgL_arg1753z00_2743;

									BgL_arg1753z00_2743 =
										(((BgL_conditionalz00_bglt) COBJECT(
												((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->
										BgL_testz00);
									{	/* Reduce/cond.scm 219 */
										obj_t BgL_classz00_2744;

										BgL_classz00_2744 = BGl_atomz00zzast_nodez00;
										{	/* Reduce/cond.scm 219 */
											BgL_objectz00_bglt BgL_arg1807z00_2745;

											{	/* Reduce/cond.scm 219 */
												obj_t BgL_tmpz00_3461;

												BgL_tmpz00_3461 =
													((obj_t) ((BgL_objectz00_bglt) BgL_arg1753z00_2743));
												BgL_arg1807z00_2745 =
													(BgL_objectz00_bglt) (BgL_tmpz00_3461);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Reduce/cond.scm 219 */
													long BgL_idxz00_2746;

													BgL_idxz00_2746 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2745);
													BgL_test2072z00_3458 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2746 + 2L)) == BgL_classz00_2744);
												}
											else
												{	/* Reduce/cond.scm 219 */
													bool_t BgL_res1956z00_2749;

													{	/* Reduce/cond.scm 219 */
														obj_t BgL_oclassz00_2750;

														{	/* Reduce/cond.scm 219 */
															obj_t BgL_arg1815z00_2751;
															long BgL_arg1816z00_2752;

															BgL_arg1815z00_2751 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Reduce/cond.scm 219 */
																long BgL_arg1817z00_2753;

																BgL_arg1817z00_2753 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2745);
																BgL_arg1816z00_2752 =
																	(BgL_arg1817z00_2753 - OBJECT_TYPE);
															}
															BgL_oclassz00_2750 =
																VECTOR_REF(BgL_arg1815z00_2751,
																BgL_arg1816z00_2752);
														}
														{	/* Reduce/cond.scm 219 */
															bool_t BgL__ortest_1115z00_2754;

															BgL__ortest_1115z00_2754 =
																(BgL_classz00_2744 == BgL_oclassz00_2750);
															if (BgL__ortest_1115z00_2754)
																{	/* Reduce/cond.scm 219 */
																	BgL_res1956z00_2749 =
																		BgL__ortest_1115z00_2754;
																}
															else
																{	/* Reduce/cond.scm 219 */
																	long BgL_odepthz00_2755;

																	{	/* Reduce/cond.scm 219 */
																		obj_t BgL_arg1804z00_2756;

																		BgL_arg1804z00_2756 = (BgL_oclassz00_2750);
																		BgL_odepthz00_2755 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2756);
																	}
																	if ((2L < BgL_odepthz00_2755))
																		{	/* Reduce/cond.scm 219 */
																			obj_t BgL_arg1802z00_2757;

																			{	/* Reduce/cond.scm 219 */
																				obj_t BgL_arg1803z00_2758;

																				BgL_arg1803z00_2758 =
																					(BgL_oclassz00_2750);
																				BgL_arg1802z00_2757 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2758, 2L);
																			}
																			BgL_res1956z00_2749 =
																				(BgL_arg1802z00_2757 ==
																				BgL_classz00_2744);
																		}
																	else
																		{	/* Reduce/cond.scm 219 */
																			BgL_res1956z00_2749 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2072z00_3458 = BgL_res1956z00_2749;
												}
										}
									}
								}
								if (BgL_test2072z00_3458)
									{	/* Reduce/cond.scm 219 */
										BGl_za2condzd2reducedza2zd2zzreduce_condz00 =
											(1L + BGl_za2condzd2reducedza2zd2zzreduce_condz00);
										if (CBOOL(
												(((BgL_atomz00_bglt) COBJECT(
															((BgL_atomz00_bglt)
																(((BgL_conditionalz00_bglt) COBJECT(
																			((BgL_conditionalz00_bglt)
																				BgL_nodez00_2585)))->BgL_testz00))))->
													BgL_valuez00)))
											{	/* Reduce/cond.scm 222 */
												return
													(((BgL_conditionalz00_bglt) COBJECT(
															((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->
													BgL_truez00);
											}
										else
											{	/* Reduce/cond.scm 222 */
												return
													(((BgL_conditionalz00_bglt) COBJECT(
															((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->
													BgL_falsez00);
											}
									}
								else
									{	/* Reduce/cond.scm 225 */
										obj_t BgL_g1123z00_2759;

										BgL_g1123z00_2759 =
											BGl_staticzd2valuezd2zzreduce_condz00(
											(((BgL_conditionalz00_bglt) COBJECT(
														((BgL_conditionalz00_bglt) BgL_nodez00_2585)))->
												BgL_testz00));
										if (CBOOL(BgL_g1123z00_2759))
											{	/* Reduce/cond.scm 225 */
												BGl_za2condzd2reducedza2zd2zzreduce_condz00 =
													(1L + BGl_za2condzd2reducedza2zd2zzreduce_condz00);
												{	/* Reduce/cond.scm 230 */
													BgL_sequencez00_bglt BgL_new1127z00_2760;

													{	/* Reduce/cond.scm 230 */
														BgL_sequencez00_bglt BgL_new1125z00_2761;

														BgL_new1125z00_2761 =
															((BgL_sequencez00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_sequencez00_bgl))));
														{	/* Reduce/cond.scm 230 */
															long BgL_arg1751z00_2762;

															BgL_arg1751z00_2762 =
																BGL_CLASS_NUM(BGl_sequencez00zzast_nodez00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1125z00_2761),
																BgL_arg1751z00_2762);
														}
														{	/* Reduce/cond.scm 230 */
															BgL_objectz00_bglt BgL_tmpz00_3505;

															BgL_tmpz00_3505 =
																((BgL_objectz00_bglt) BgL_new1125z00_2761);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3505, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1125z00_2761);
														BgL_new1127z00_2760 = BgL_new1125z00_2761;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1127z00_2760)))->
															BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_new1127z00_2760)))->BgL_typez00) =
														((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																	COBJECT(((BgL_nodez00_bglt) (
																				(BgL_conditionalz00_bglt)
																				BgL_nodez00_2585))))->BgL_typez00)),
														BUNSPEC);
													((((BgL_nodezf2effectzf2_bglt)
																COBJECT(((BgL_nodezf2effectzf2_bglt)
																		BgL_new1127z00_2760)))->
															BgL_sidezd2effectzd2) =
														((obj_t) BUNSPEC), BUNSPEC);
													((((BgL_nodezf2effectzf2_bglt)
																COBJECT(((BgL_nodezf2effectzf2_bglt)
																		BgL_new1127z00_2760)))->BgL_keyz00) =
														((obj_t) BINT(-1L)), BUNSPEC);
													{
														obj_t BgL_auxz00_3521;

														{	/* Reduce/cond.scm 232 */
															BgL_nodez00_bglt BgL_arg1747z00_2763;
															BgL_nodez00_bglt BgL_arg1748z00_2764;

															BgL_arg1747z00_2763 =
																(((BgL_conditionalz00_bglt) COBJECT(
																		((BgL_conditionalz00_bglt)
																			BgL_nodez00_2585)))->BgL_testz00);
															if ((BgL_g1123z00_2759 == CNST_TABLE_REF(6)))
																{	/* Reduce/cond.scm 232 */
																	BgL_arg1748z00_2764 =
																		(((BgL_conditionalz00_bglt) COBJECT(
																				((BgL_conditionalz00_bglt)
																					BgL_nodez00_2585)))->BgL_truez00);
																}
															else
																{	/* Reduce/cond.scm 232 */
																	BgL_arg1748z00_2764 =
																		(((BgL_conditionalz00_bglt) COBJECT(
																				((BgL_conditionalz00_bglt)
																					BgL_nodez00_2585)))->BgL_falsez00);
																}
															{	/* Reduce/cond.scm 232 */
																obj_t BgL_list1749z00_2765;

																{	/* Reduce/cond.scm 232 */
																	obj_t BgL_arg1750z00_2766;

																	BgL_arg1750z00_2766 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_arg1748z00_2764), BNIL);
																	BgL_list1749z00_2765 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_arg1747z00_2763),
																		BgL_arg1750z00_2766);
																}
																BgL_auxz00_3521 = BgL_list1749z00_2765;
															}
														}
														((((BgL_sequencez00_bglt)
																	COBJECT(BgL_new1127z00_2760))->BgL_nodesz00) =
															((obj_t) BgL_auxz00_3521), BUNSPEC);
													}
													((((BgL_sequencez00_bglt)
																COBJECT(BgL_new1127z00_2760))->BgL_unsafez00) =
														((bool_t) ((bool_t) 0)), BUNSPEC);
													((((BgL_sequencez00_bglt)
																COBJECT(BgL_new1127z00_2760))->BgL_metaz00) =
														((obj_t) BNIL), BUNSPEC);
													return ((BgL_nodez00_bglt) BgL_new1127z00_2760);
												}
											}
										else
											{	/* Reduce/cond.scm 225 */
												return
													((BgL_nodez00_bglt)
													((BgL_conditionalz00_bglt) BgL_nodez00_2585));
											}
									}
							}
					}
			}
		}

	}



/* &node-cond!-setq1297 */
	BgL_nodez00_bglt BGl_z62nodezd2condz12zd2setq1297z70zzreduce_condz00(obj_t
		BgL_envz00_2586, obj_t BgL_nodez00_2587)
	{
		{	/* Reduce/cond.scm 191 */
			{
				BgL_nodez00_bglt BgL_auxz00_3541;

				{	/* Reduce/cond.scm 193 */
					BgL_nodez00_bglt BgL_arg1611z00_2768;

					BgL_arg1611z00_2768 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2587)))->BgL_valuez00);
					BgL_auxz00_3541 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1611z00_2768);
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2587)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3541), BUNSPEC);
			}
			{
				BgL_varz00_bglt BgL_auxz00_3547;

				{	/* Reduce/cond.scm 194 */
					BgL_varz00_bglt BgL_arg1613z00_2769;

					BgL_arg1613z00_2769 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2587)))->BgL_varz00);
					BgL_auxz00_3547 =
						((BgL_varz00_bglt)
						BGl_nodezd2condz12zc0zzreduce_condz00(
							((BgL_nodez00_bglt) BgL_arg1613z00_2769)));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2587)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3547), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_2587));
		}

	}



/* &node-cond!-cast1295 */
	BgL_nodez00_bglt BGl_z62nodezd2condz12zd2cast1295z70zzreduce_condz00(obj_t
		BgL_envz00_2588, obj_t BgL_nodez00_2589)
	{
		{	/* Reduce/cond.scm 183 */
			BGl_nodezd2condz12zc0zzreduce_condz00(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2589)))->BgL_argz00));
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_2589));
		}

	}



/* &node-cond!-extern1293 */
	BgL_nodez00_bglt BGl_z62nodezd2condz12zd2extern1293z70zzreduce_condz00(obj_t
		BgL_envz00_2590, obj_t BgL_nodez00_2591)
	{
		{	/* Reduce/cond.scm 175 */
			BGl_nodezd2condza2z12z62zzreduce_condz00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2591)))->BgL_exprza2za2));
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2591));
		}

	}



/* &node-cond!-funcall1291 */
	BgL_nodez00_bglt BGl_z62nodezd2condz12zd2funcall1291z70zzreduce_condz00(obj_t
		BgL_envz00_2592, obj_t BgL_nodez00_2593)
	{
		{	/* Reduce/cond.scm 166 */
			{
				BgL_nodez00_bglt BgL_auxz00_3567;

				{	/* Reduce/cond.scm 168 */
					BgL_nodez00_bglt BgL_arg1602z00_2773;

					BgL_arg1602z00_2773 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2593)))->BgL_funz00);
					BgL_auxz00_3567 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1602z00_2773);
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2593)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3567), BUNSPEC);
			}
			BGl_nodezd2condza2z12z62zzreduce_condz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2593)))->BgL_argsz00));
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_2593));
		}

	}



/* &node-cond!-app-ly1289 */
	BgL_nodez00_bglt BGl_z62nodezd2condz12zd2appzd2ly1289za2zzreduce_condz00(obj_t
		BgL_envz00_2594, obj_t BgL_nodez00_2595)
	{
		{	/* Reduce/cond.scm 157 */
			{
				BgL_nodez00_bglt BgL_auxz00_3578;

				{	/* Reduce/cond.scm 159 */
					BgL_nodez00_bglt BgL_arg1594z00_2775;

					BgL_arg1594z00_2775 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2595)))->BgL_funz00);
					BgL_auxz00_3578 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1594z00_2775);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2595)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3578), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3584;

				{	/* Reduce/cond.scm 160 */
					BgL_nodez00_bglt BgL_arg1595z00_2776;

					BgL_arg1595z00_2776 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2595)))->BgL_argz00);
					BgL_auxz00_3584 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1595z00_2776);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2595)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3584), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2595));
		}

	}



/* &node-cond!-app1287 */
	BgL_nodez00_bglt BGl_z62nodezd2condz12zd2app1287z70zzreduce_condz00(obj_t
		BgL_envz00_2596, obj_t BgL_nodez00_2597)
	{
		{	/* Reduce/cond.scm 122 */
			{
				obj_t BgL_n1z00_2827;
				obj_t BgL_n2z00_2828;
				BgL_appz00_bglt BgL_nodez00_2780;

				BGl_nodezd2condza2z12z62zzreduce_condz00(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2597)))->BgL_argsz00));
				{	/* Reduce/cond.scm 147 */
					obj_t BgL_trivz00_2889;

					BgL_nodez00_2780 = ((BgL_appz00_bglt) BgL_nodez00_2597);
					{	/* Reduce/cond.scm 135 */
						bool_t BgL_test2079z00_3595;

						{	/* Reduce/cond.scm 135 */
							bool_t BgL_test2080z00_3596;

							{	/* Reduce/cond.scm 135 */
								bool_t BgL_test2081z00_3597;

								{	/* Reduce/cond.scm 135 */
									BgL_typez00_bglt BgL_arg1593z00_2781;

									BgL_arg1593z00_2781 =
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_nodez00_2780)))->BgL_typez00);
									BgL_test2081z00_3597 =
										(
										((obj_t) BgL_arg1593z00_2781) ==
										BGl_za2boolza2z00zztype_cachez00);
								}
								if (BgL_test2081z00_3597)
									{	/* Reduce/cond.scm 135 */
										BgL_test2080z00_3596 = ((bool_t) 1);
									}
								else
									{	/* Reduce/cond.scm 135 */
										BgL_typez00_bglt BgL_arg1591z00_2782;

										BgL_arg1591z00_2782 =
											(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_nodez00_2780)))->BgL_typez00);
										BgL_test2080z00_3596 =
											(
											((obj_t) BgL_arg1591z00_2782) ==
											BGl_za2objza2z00zztype_cachez00);
									}
							}
							if (BgL_test2080z00_3596)
								{	/* Reduce/cond.scm 136 */
									obj_t BgL_g1260z00_2783;

									BgL_g1260z00_2783 =
										(((BgL_appz00_bglt) COBJECT(BgL_nodez00_2780))->
										BgL_argsz00);
									{
										obj_t BgL_l1258z00_2785;

										BgL_l1258z00_2785 = BgL_g1260z00_2783;
									BgL_zc3z04anonymousza31586ze3z87_2784:
										if (NULLP(BgL_l1258z00_2785))
											{	/* Reduce/cond.scm 136 */
												BgL_test2079z00_3595 = ((bool_t) 1);
											}
										else
											{	/* Reduce/cond.scm 136 */
												bool_t BgL_test2083z00_3609;

												{	/* Reduce/cond.scm 136 */
													obj_t BgL_nz00_2786;

													BgL_nz00_2786 = CAR(((obj_t) BgL_l1258z00_2785));
													{	/* Reduce/cond.scm 136 */
														bool_t BgL__ortest_1109z00_2787;

														{	/* Reduce/cond.scm 136 */
															obj_t BgL_classz00_2788;

															BgL_classz00_2788 = BGl_atomz00zzast_nodez00;
															if (BGL_OBJECTP(BgL_nz00_2786))
																{	/* Reduce/cond.scm 136 */
																	BgL_objectz00_bglt BgL_arg1807z00_2789;

																	BgL_arg1807z00_2789 =
																		(BgL_objectz00_bglt) (BgL_nz00_2786);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Reduce/cond.scm 136 */
																			long BgL_idxz00_2790;

																			BgL_idxz00_2790 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2789);
																			BgL__ortest_1109z00_2787 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2790 + 2L)) ==
																				BgL_classz00_2788);
																		}
																	else
																		{	/* Reduce/cond.scm 136 */
																			bool_t BgL_res1950z00_2793;

																			{	/* Reduce/cond.scm 136 */
																				obj_t BgL_oclassz00_2794;

																				{	/* Reduce/cond.scm 136 */
																					obj_t BgL_arg1815z00_2795;
																					long BgL_arg1816z00_2796;

																					BgL_arg1815z00_2795 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Reduce/cond.scm 136 */
																						long BgL_arg1817z00_2797;

																						BgL_arg1817z00_2797 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2789);
																						BgL_arg1816z00_2796 =
																							(BgL_arg1817z00_2797 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2794 =
																						VECTOR_REF(BgL_arg1815z00_2795,
																						BgL_arg1816z00_2796);
																				}
																				{	/* Reduce/cond.scm 136 */
																					bool_t BgL__ortest_1115z00_2798;

																					BgL__ortest_1115z00_2798 =
																						(BgL_classz00_2788 ==
																						BgL_oclassz00_2794);
																					if (BgL__ortest_1115z00_2798)
																						{	/* Reduce/cond.scm 136 */
																							BgL_res1950z00_2793 =
																								BgL__ortest_1115z00_2798;
																						}
																					else
																						{	/* Reduce/cond.scm 136 */
																							long BgL_odepthz00_2799;

																							{	/* Reduce/cond.scm 136 */
																								obj_t BgL_arg1804z00_2800;

																								BgL_arg1804z00_2800 =
																									(BgL_oclassz00_2794);
																								BgL_odepthz00_2799 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2800);
																							}
																							if ((2L < BgL_odepthz00_2799))
																								{	/* Reduce/cond.scm 136 */
																									obj_t BgL_arg1802z00_2801;

																									{	/* Reduce/cond.scm 136 */
																										obj_t BgL_arg1803z00_2802;

																										BgL_arg1803z00_2802 =
																											(BgL_oclassz00_2794);
																										BgL_arg1802z00_2801 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2802, 2L);
																									}
																									BgL_res1950z00_2793 =
																										(BgL_arg1802z00_2801 ==
																										BgL_classz00_2788);
																								}
																							else
																								{	/* Reduce/cond.scm 136 */
																									BgL_res1950z00_2793 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL__ortest_1109z00_2787 =
																				BgL_res1950z00_2793;
																		}
																}
															else
																{	/* Reduce/cond.scm 136 */
																	BgL__ortest_1109z00_2787 = ((bool_t) 0);
																}
														}
														if (BgL__ortest_1109z00_2787)
															{	/* Reduce/cond.scm 136 */
																BgL_test2083z00_3609 = BgL__ortest_1109z00_2787;
															}
														else
															{	/* Reduce/cond.scm 136 */
																obj_t BgL_classz00_2803;

																BgL_classz00_2803 = BGl_varz00zzast_nodez00;
																if (BGL_OBJECTP(BgL_nz00_2786))
																	{	/* Reduce/cond.scm 136 */
																		BgL_objectz00_bglt BgL_arg1807z00_2804;

																		BgL_arg1807z00_2804 =
																			(BgL_objectz00_bglt) (BgL_nz00_2786);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Reduce/cond.scm 136 */
																				long BgL_idxz00_2805;

																				BgL_idxz00_2805 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2804);
																				BgL_test2083z00_3609 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2805 + 2L)) ==
																					BgL_classz00_2803);
																			}
																		else
																			{	/* Reduce/cond.scm 136 */
																				bool_t BgL_res1951z00_2808;

																				{	/* Reduce/cond.scm 136 */
																					obj_t BgL_oclassz00_2809;

																					{	/* Reduce/cond.scm 136 */
																						obj_t BgL_arg1815z00_2810;
																						long BgL_arg1816z00_2811;

																						BgL_arg1815z00_2810 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Reduce/cond.scm 136 */
																							long BgL_arg1817z00_2812;

																							BgL_arg1817z00_2812 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2804);
																							BgL_arg1816z00_2811 =
																								(BgL_arg1817z00_2812 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2809 =
																							VECTOR_REF(BgL_arg1815z00_2810,
																							BgL_arg1816z00_2811);
																					}
																					{	/* Reduce/cond.scm 136 */
																						bool_t BgL__ortest_1115z00_2813;

																						BgL__ortest_1115z00_2813 =
																							(BgL_classz00_2803 ==
																							BgL_oclassz00_2809);
																						if (BgL__ortest_1115z00_2813)
																							{	/* Reduce/cond.scm 136 */
																								BgL_res1951z00_2808 =
																									BgL__ortest_1115z00_2813;
																							}
																						else
																							{	/* Reduce/cond.scm 136 */
																								long BgL_odepthz00_2814;

																								{	/* Reduce/cond.scm 136 */
																									obj_t BgL_arg1804z00_2815;

																									BgL_arg1804z00_2815 =
																										(BgL_oclassz00_2809);
																									BgL_odepthz00_2814 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2815);
																								}
																								if ((2L < BgL_odepthz00_2814))
																									{	/* Reduce/cond.scm 136 */
																										obj_t BgL_arg1802z00_2816;

																										{	/* Reduce/cond.scm 136 */
																											obj_t BgL_arg1803z00_2817;

																											BgL_arg1803z00_2817 =
																												(BgL_oclassz00_2809);
																											BgL_arg1802z00_2816 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2817,
																												2L);
																										}
																										BgL_res1951z00_2808 =
																											(BgL_arg1802z00_2816 ==
																											BgL_classz00_2803);
																									}
																								else
																									{	/* Reduce/cond.scm 136 */
																										BgL_res1951z00_2808 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2083z00_3609 =
																					BgL_res1951z00_2808;
																			}
																	}
																else
																	{	/* Reduce/cond.scm 136 */
																		BgL_test2083z00_3609 = ((bool_t) 0);
																	}
															}
													}
												}
												if (BgL_test2083z00_3609)
													{	/* Reduce/cond.scm 136 */
														obj_t BgL_arg1589z00_2818;

														BgL_arg1589z00_2818 =
															CDR(((obj_t) BgL_l1258z00_2785));
														{
															obj_t BgL_l1258z00_3659;

															BgL_l1258z00_3659 = BgL_arg1589z00_2818;
															BgL_l1258z00_2785 = BgL_l1258z00_3659;
															goto BgL_zc3z04anonymousza31586ze3z87_2784;
														}
													}
												else
													{	/* Reduce/cond.scm 136 */
														BgL_test2079z00_3595 = ((bool_t) 0);
													}
											}
									}
								}
							else
								{	/* Reduce/cond.scm 135 */
									BgL_test2079z00_3595 = ((bool_t) 0);
								}
						}
						if (BgL_test2079z00_3595)
							{	/* Reduce/cond.scm 138 */
								bool_t BgL_test2093z00_3660;

								{	/* Reduce/cond.scm 138 */
									BgL_variablez00_bglt BgL_arg1584z00_2819;

									BgL_arg1584z00_2819 =
										(((BgL_varz00_bglt) COBJECT(
												(((BgL_appz00_bglt) COBJECT(BgL_nodez00_2780))->
													BgL_funz00)))->BgL_variablez00);
									BgL_test2093z00_3660 =
										(((obj_t) BgL_arg1584z00_2819) ==
										BGl_za2z42eqza2z42zzreduce_condz00);
								}
								if (BgL_test2093z00_3660)
									{	/* Reduce/cond.scm 139 */
										obj_t BgL_arg1540z00_2820;
										obj_t BgL_arg1544z00_2821;

										BgL_arg1540z00_2820 =
											CAR(
											(((BgL_appz00_bglt) COBJECT(BgL_nodez00_2780))->
												BgL_argsz00));
										{	/* Reduce/cond.scm 139 */
											obj_t BgL_pairz00_2822;

											BgL_pairz00_2822 =
												(((BgL_appz00_bglt) COBJECT(BgL_nodez00_2780))->
												BgL_argsz00);
											BgL_arg1544z00_2821 = CAR(CDR(BgL_pairz00_2822));
										}
										BgL_n1z00_2827 = BgL_arg1540z00_2820;
										BgL_n2z00_2828 = BgL_arg1544z00_2821;
									BgL_eqzd2atomzf3z21_2779:
										{	/* Reduce/cond.scm 126 */
											bool_t BgL_test2094z00_3670;

											{	/* Reduce/cond.scm 126 */
												bool_t BgL_test2095z00_3671;

												{	/* Reduce/cond.scm 126 */
													obj_t BgL_classz00_2829;

													BgL_classz00_2829 = BGl_atomz00zzast_nodez00;
													if (BGL_OBJECTP(BgL_n1z00_2827))
														{	/* Reduce/cond.scm 126 */
															BgL_objectz00_bglt BgL_arg1807z00_2830;

															BgL_arg1807z00_2830 =
																(BgL_objectz00_bglt) (BgL_n1z00_2827);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Reduce/cond.scm 126 */
																	long BgL_idxz00_2831;

																	BgL_idxz00_2831 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2830);
																	BgL_test2095z00_3671 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2831 + 2L)) ==
																		BgL_classz00_2829);
																}
															else
																{	/* Reduce/cond.scm 126 */
																	bool_t BgL_res1946z00_2834;

																	{	/* Reduce/cond.scm 126 */
																		obj_t BgL_oclassz00_2835;

																		{	/* Reduce/cond.scm 126 */
																			obj_t BgL_arg1815z00_2836;
																			long BgL_arg1816z00_2837;

																			BgL_arg1815z00_2836 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Reduce/cond.scm 126 */
																				long BgL_arg1817z00_2838;

																				BgL_arg1817z00_2838 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2830);
																				BgL_arg1816z00_2837 =
																					(BgL_arg1817z00_2838 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2835 =
																				VECTOR_REF(BgL_arg1815z00_2836,
																				BgL_arg1816z00_2837);
																		}
																		{	/* Reduce/cond.scm 126 */
																			bool_t BgL__ortest_1115z00_2839;

																			BgL__ortest_1115z00_2839 =
																				(BgL_classz00_2829 ==
																				BgL_oclassz00_2835);
																			if (BgL__ortest_1115z00_2839)
																				{	/* Reduce/cond.scm 126 */
																					BgL_res1946z00_2834 =
																						BgL__ortest_1115z00_2839;
																				}
																			else
																				{	/* Reduce/cond.scm 126 */
																					long BgL_odepthz00_2840;

																					{	/* Reduce/cond.scm 126 */
																						obj_t BgL_arg1804z00_2841;

																						BgL_arg1804z00_2841 =
																							(BgL_oclassz00_2835);
																						BgL_odepthz00_2840 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2841);
																					}
																					if ((2L < BgL_odepthz00_2840))
																						{	/* Reduce/cond.scm 126 */
																							obj_t BgL_arg1802z00_2842;

																							{	/* Reduce/cond.scm 126 */
																								obj_t BgL_arg1803z00_2843;

																								BgL_arg1803z00_2843 =
																									(BgL_oclassz00_2835);
																								BgL_arg1802z00_2842 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2843, 2L);
																							}
																							BgL_res1946z00_2834 =
																								(BgL_arg1802z00_2842 ==
																								BgL_classz00_2829);
																						}
																					else
																						{	/* Reduce/cond.scm 126 */
																							BgL_res1946z00_2834 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2095z00_3671 = BgL_res1946z00_2834;
																}
														}
													else
														{	/* Reduce/cond.scm 126 */
															BgL_test2095z00_3671 = ((bool_t) 0);
														}
												}
												if (BgL_test2095z00_3671)
													{	/* Reduce/cond.scm 126 */
														obj_t BgL_classz00_2844;

														BgL_classz00_2844 = BGl_atomz00zzast_nodez00;
														if (BGL_OBJECTP(BgL_n2z00_2828))
															{	/* Reduce/cond.scm 126 */
																BgL_objectz00_bglt BgL_arg1807z00_2845;

																BgL_arg1807z00_2845 =
																	(BgL_objectz00_bglt) (BgL_n2z00_2828);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Reduce/cond.scm 126 */
																		long BgL_idxz00_2846;

																		BgL_idxz00_2846 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2845);
																		BgL_test2094z00_3670 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2846 + 2L)) ==
																			BgL_classz00_2844);
																	}
																else
																	{	/* Reduce/cond.scm 126 */
																		bool_t BgL_res1947z00_2849;

																		{	/* Reduce/cond.scm 126 */
																			obj_t BgL_oclassz00_2850;

																			{	/* Reduce/cond.scm 126 */
																				obj_t BgL_arg1815z00_2851;
																				long BgL_arg1816z00_2852;

																				BgL_arg1815z00_2851 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Reduce/cond.scm 126 */
																					long BgL_arg1817z00_2853;

																					BgL_arg1817z00_2853 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2845);
																					BgL_arg1816z00_2852 =
																						(BgL_arg1817z00_2853 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2850 =
																					VECTOR_REF(BgL_arg1815z00_2851,
																					BgL_arg1816z00_2852);
																			}
																			{	/* Reduce/cond.scm 126 */
																				bool_t BgL__ortest_1115z00_2854;

																				BgL__ortest_1115z00_2854 =
																					(BgL_classz00_2844 ==
																					BgL_oclassz00_2850);
																				if (BgL__ortest_1115z00_2854)
																					{	/* Reduce/cond.scm 126 */
																						BgL_res1947z00_2849 =
																							BgL__ortest_1115z00_2854;
																					}
																				else
																					{	/* Reduce/cond.scm 126 */
																						long BgL_odepthz00_2855;

																						{	/* Reduce/cond.scm 126 */
																							obj_t BgL_arg1804z00_2856;

																							BgL_arg1804z00_2856 =
																								(BgL_oclassz00_2850);
																							BgL_odepthz00_2855 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2856);
																						}
																						if ((2L < BgL_odepthz00_2855))
																							{	/* Reduce/cond.scm 126 */
																								obj_t BgL_arg1802z00_2857;

																								{	/* Reduce/cond.scm 126 */
																									obj_t BgL_arg1803z00_2858;

																									BgL_arg1803z00_2858 =
																										(BgL_oclassz00_2850);
																									BgL_arg1802z00_2857 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2858, 2L);
																								}
																								BgL_res1947z00_2849 =
																									(BgL_arg1802z00_2857 ==
																									BgL_classz00_2844);
																							}
																						else
																							{	/* Reduce/cond.scm 126 */
																								BgL_res1947z00_2849 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2094z00_3670 = BgL_res1947z00_2849;
																	}
															}
														else
															{	/* Reduce/cond.scm 126 */
																BgL_test2094z00_3670 = ((bool_t) 0);
															}
													}
												else
													{	/* Reduce/cond.scm 126 */
														BgL_test2094z00_3670 = ((bool_t) 0);
													}
											}
											if (BgL_test2094z00_3670)
												{	/* Reduce/cond.scm 126 */
													if (
														((((BgL_atomz00_bglt) COBJECT(
																		((BgL_atomz00_bglt) BgL_n1z00_2827)))->
																BgL_valuez00) ==
															(((BgL_atomz00_bglt) COBJECT(((BgL_atomz00_bglt)
																			BgL_n2z00_2828)))->BgL_valuez00)))
														{	/* Reduce/cond.scm 127 */
															BgL_trivz00_2889 = CNST_TABLE_REF(6);
														}
													else
														{	/* Reduce/cond.scm 127 */
															BgL_trivz00_2889 = CNST_TABLE_REF(5);
														}
												}
											else
												{	/* Reduce/cond.scm 128 */
													bool_t BgL_test2105z00_3724;

													{	/* Reduce/cond.scm 128 */
														bool_t BgL_test2106z00_3725;

														{	/* Reduce/cond.scm 128 */
															obj_t BgL_classz00_2859;

															BgL_classz00_2859 = BGl_varz00zzast_nodez00;
															if (BGL_OBJECTP(BgL_n1z00_2827))
																{	/* Reduce/cond.scm 128 */
																	BgL_objectz00_bglt BgL_arg1807z00_2860;

																	BgL_arg1807z00_2860 =
																		(BgL_objectz00_bglt) (BgL_n1z00_2827);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Reduce/cond.scm 128 */
																			long BgL_idxz00_2861;

																			BgL_idxz00_2861 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2860);
																			BgL_test2106z00_3725 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2861 + 2L)) ==
																				BgL_classz00_2859);
																		}
																	else
																		{	/* Reduce/cond.scm 128 */
																			bool_t BgL_res1948z00_2864;

																			{	/* Reduce/cond.scm 128 */
																				obj_t BgL_oclassz00_2865;

																				{	/* Reduce/cond.scm 128 */
																					obj_t BgL_arg1815z00_2866;
																					long BgL_arg1816z00_2867;

																					BgL_arg1815z00_2866 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Reduce/cond.scm 128 */
																						long BgL_arg1817z00_2868;

																						BgL_arg1817z00_2868 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2860);
																						BgL_arg1816z00_2867 =
																							(BgL_arg1817z00_2868 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2865 =
																						VECTOR_REF(BgL_arg1815z00_2866,
																						BgL_arg1816z00_2867);
																				}
																				{	/* Reduce/cond.scm 128 */
																					bool_t BgL__ortest_1115z00_2869;

																					BgL__ortest_1115z00_2869 =
																						(BgL_classz00_2859 ==
																						BgL_oclassz00_2865);
																					if (BgL__ortest_1115z00_2869)
																						{	/* Reduce/cond.scm 128 */
																							BgL_res1948z00_2864 =
																								BgL__ortest_1115z00_2869;
																						}
																					else
																						{	/* Reduce/cond.scm 128 */
																							long BgL_odepthz00_2870;

																							{	/* Reduce/cond.scm 128 */
																								obj_t BgL_arg1804z00_2871;

																								BgL_arg1804z00_2871 =
																									(BgL_oclassz00_2865);
																								BgL_odepthz00_2870 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2871);
																							}
																							if ((2L < BgL_odepthz00_2870))
																								{	/* Reduce/cond.scm 128 */
																									obj_t BgL_arg1802z00_2872;

																									{	/* Reduce/cond.scm 128 */
																										obj_t BgL_arg1803z00_2873;

																										BgL_arg1803z00_2873 =
																											(BgL_oclassz00_2865);
																										BgL_arg1802z00_2872 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2873, 2L);
																									}
																									BgL_res1948z00_2864 =
																										(BgL_arg1802z00_2872 ==
																										BgL_classz00_2859);
																								}
																							else
																								{	/* Reduce/cond.scm 128 */
																									BgL_res1948z00_2864 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2106z00_3725 =
																				BgL_res1948z00_2864;
																		}
																}
															else
																{	/* Reduce/cond.scm 128 */
																	BgL_test2106z00_3725 = ((bool_t) 0);
																}
														}
														if (BgL_test2106z00_3725)
															{	/* Reduce/cond.scm 128 */
																obj_t BgL_classz00_2874;

																BgL_classz00_2874 = BGl_varz00zzast_nodez00;
																if (BGL_OBJECTP(BgL_n2z00_2828))
																	{	/* Reduce/cond.scm 128 */
																		BgL_objectz00_bglt BgL_arg1807z00_2875;

																		BgL_arg1807z00_2875 =
																			(BgL_objectz00_bglt) (BgL_n2z00_2828);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Reduce/cond.scm 128 */
																				long BgL_idxz00_2876;

																				BgL_idxz00_2876 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2875);
																				BgL_test2105z00_3724 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2876 + 2L)) ==
																					BgL_classz00_2874);
																			}
																		else
																			{	/* Reduce/cond.scm 128 */
																				bool_t BgL_res1949z00_2879;

																				{	/* Reduce/cond.scm 128 */
																					obj_t BgL_oclassz00_2880;

																					{	/* Reduce/cond.scm 128 */
																						obj_t BgL_arg1815z00_2881;
																						long BgL_arg1816z00_2882;

																						BgL_arg1815z00_2881 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Reduce/cond.scm 128 */
																							long BgL_arg1817z00_2883;

																							BgL_arg1817z00_2883 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2875);
																							BgL_arg1816z00_2882 =
																								(BgL_arg1817z00_2883 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2880 =
																							VECTOR_REF(BgL_arg1815z00_2881,
																							BgL_arg1816z00_2882);
																					}
																					{	/* Reduce/cond.scm 128 */
																						bool_t BgL__ortest_1115z00_2884;

																						BgL__ortest_1115z00_2884 =
																							(BgL_classz00_2874 ==
																							BgL_oclassz00_2880);
																						if (BgL__ortest_1115z00_2884)
																							{	/* Reduce/cond.scm 128 */
																								BgL_res1949z00_2879 =
																									BgL__ortest_1115z00_2884;
																							}
																						else
																							{	/* Reduce/cond.scm 128 */
																								long BgL_odepthz00_2885;

																								{	/* Reduce/cond.scm 128 */
																									obj_t BgL_arg1804z00_2886;

																									BgL_arg1804z00_2886 =
																										(BgL_oclassz00_2880);
																									BgL_odepthz00_2885 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2886);
																								}
																								if ((2L < BgL_odepthz00_2885))
																									{	/* Reduce/cond.scm 128 */
																										obj_t BgL_arg1802z00_2887;

																										{	/* Reduce/cond.scm 128 */
																											obj_t BgL_arg1803z00_2888;

																											BgL_arg1803z00_2888 =
																												(BgL_oclassz00_2880);
																											BgL_arg1802z00_2887 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2888,
																												2L);
																										}
																										BgL_res1949z00_2879 =
																											(BgL_arg1802z00_2887 ==
																											BgL_classz00_2874);
																									}
																								else
																									{	/* Reduce/cond.scm 128 */
																										BgL_res1949z00_2879 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2105z00_3724 =
																					BgL_res1949z00_2879;
																			}
																	}
																else
																	{	/* Reduce/cond.scm 128 */
																		BgL_test2105z00_3724 = ((bool_t) 0);
																	}
															}
														else
															{	/* Reduce/cond.scm 128 */
																BgL_test2105z00_3724 = ((bool_t) 0);
															}
													}
													if (BgL_test2105z00_3724)
														{	/* Reduce/cond.scm 128 */
															if (
																(((obj_t)
																		(((BgL_varz00_bglt) COBJECT(
																					((BgL_varz00_bglt) BgL_n1z00_2827)))->
																			BgL_variablez00)) ==
																	((obj_t) (((BgL_varz00_bglt)
																				COBJECT(((BgL_varz00_bglt)
																						BgL_n2z00_2828)))->
																			BgL_variablez00))))
																{	/* Reduce/cond.scm 129 */
																	BgL_trivz00_2889 = CNST_TABLE_REF(6);
																}
															else
																{	/* Reduce/cond.scm 129 */
																	BgL_trivz00_2889 = BFALSE;
																}
														}
													else
														{	/* Reduce/cond.scm 128 */
															BgL_trivz00_2889 = BFALSE;
														}
												}
										}
									}
								else
									{	/* Reduce/cond.scm 140 */
										bool_t BgL_test2116z00_3779;

										{	/* Reduce/cond.scm 140 */
											BgL_variablez00_bglt BgL_arg1575z00_2823;

											BgL_arg1575z00_2823 =
												(((BgL_varz00_bglt) COBJECT(
														(((BgL_appz00_bglt) COBJECT(BgL_nodez00_2780))->
															BgL_funz00)))->BgL_variablez00);
											BgL_test2116z00_3779 =
												(((obj_t) BgL_arg1575z00_2823) ==
												BGl_za2z42zd3fxza2z91zzreduce_condz00);
										}
										if (BgL_test2116z00_3779)
											{	/* Reduce/cond.scm 141 */
												obj_t BgL_arg1564z00_2824;
												obj_t BgL_arg1565z00_2825;

												BgL_arg1564z00_2824 =
													CAR(
													(((BgL_appz00_bglt) COBJECT(BgL_nodez00_2780))->
														BgL_argsz00));
												{	/* Reduce/cond.scm 141 */
													obj_t BgL_pairz00_2826;

													BgL_pairz00_2826 =
														(((BgL_appz00_bglt) COBJECT(BgL_nodez00_2780))->
														BgL_argsz00);
													BgL_arg1565z00_2825 = CAR(CDR(BgL_pairz00_2826));
												}
												{
													obj_t BgL_n2z00_3790;
													obj_t BgL_n1z00_3789;

													BgL_n1z00_3789 = BgL_arg1564z00_2824;
													BgL_n2z00_3790 = BgL_arg1565z00_2825;
													BgL_n2z00_2828 = BgL_n2z00_3790;
													BgL_n1z00_2827 = BgL_n1z00_3789;
													goto BgL_eqzd2atomzf3z21_2779;
												}
											}
										else
											{	/* Reduce/cond.scm 140 */
												BgL_trivz00_2889 = BFALSE;
											}
									}
							}
						else
							{	/* Reduce/cond.scm 135 */
								BgL_trivz00_2889 = BFALSE;
							}
					}
					if (CBOOL(BgL_trivz00_2889))
						{	/* Reduce/cond.scm 150 */
							BgL_literalz00_bglt BgL_new1113z00_2890;

							{	/* Reduce/cond.scm 150 */
								BgL_literalz00_bglt BgL_new1111z00_2891;

								BgL_new1111z00_2891 =
									((BgL_literalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_literalz00_bgl))));
								{	/* Reduce/cond.scm 150 */
									long BgL_arg1378z00_2892;

									BgL_arg1378z00_2892 =
										BGL_CLASS_NUM(BGl_literalz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1111z00_2891),
										BgL_arg1378z00_2892);
								}
								{	/* Reduce/cond.scm 150 */
									BgL_objectz00_bglt BgL_tmpz00_3798;

									BgL_tmpz00_3798 = ((BgL_objectz00_bglt) BgL_new1111z00_2891);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3798, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1111z00_2891);
								BgL_new1113z00_2890 = BgL_new1111z00_2891;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1113z00_2890)))->BgL_locz00) =
								((obj_t) BFALSE), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1113z00_2890)))->BgL_typez00) =
								((BgL_typez00_bglt) (((BgL_nodez00_bglt)
											COBJECT(((BgL_nodez00_bglt) ((BgL_appz00_bglt)
														BgL_nodez00_2597))))->BgL_typez00)), BUNSPEC);
							((((BgL_atomz00_bglt) COBJECT(((BgL_atomz00_bglt)
												BgL_new1113z00_2890)))->BgL_valuez00) =
								((obj_t) BBOOL((BgL_trivz00_2889 == CNST_TABLE_REF(6)))),
								BUNSPEC);
							return ((BgL_nodez00_bglt) BgL_new1113z00_2890);
						}
					else
						{	/* Reduce/cond.scm 148 */
							return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2597));
						}
				}
			}
		}

	}



/* &node-cond!-sync1285 */
	BgL_nodez00_bglt BGl_z62nodezd2condz12zd2sync1285z70zzreduce_condz00(obj_t
		BgL_envz00_2598, obj_t BgL_nodez00_2599)
	{
		{	/* Reduce/cond.scm 112 */
			{
				BgL_nodez00_bglt BgL_auxz00_3817;

				{	/* Reduce/cond.scm 114 */
					BgL_nodez00_bglt BgL_arg1371z00_2894;

					BgL_arg1371z00_2894 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2599)))->BgL_mutexz00);
					BgL_auxz00_3817 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1371z00_2894);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2599)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3817), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3823;

				{	/* Reduce/cond.scm 115 */
					BgL_nodez00_bglt BgL_arg1375z00_2895;

					BgL_arg1375z00_2895 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2599)))->BgL_prelockz00);
					BgL_auxz00_3823 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1375z00_2895);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2599)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3823), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3829;

				{	/* Reduce/cond.scm 116 */
					BgL_nodez00_bglt BgL_arg1376z00_2896;

					BgL_arg1376z00_2896 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2599)))->BgL_bodyz00);
					BgL_auxz00_3829 =
						BGl_nodezd2condz12zc0zzreduce_condz00(BgL_arg1376z00_2896);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2599)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3829), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2599));
		}

	}



/* &node-cond!-sequence1283 */
	BgL_nodez00_bglt BGl_z62nodezd2condz12zd2sequence1283z70zzreduce_condz00(obj_t
		BgL_envz00_2600, obj_t BgL_nodez00_2601)
	{
		{	/* Reduce/cond.scm 104 */
			BGl_nodezd2condza2z12z62zzreduce_condz00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2601)))->BgL_nodesz00));
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_2601));
		}

	}



/* &node-cond!-closure1281 */
	BgL_nodez00_bglt BGl_z62nodezd2condz12zd2closure1281z70zzreduce_condz00(obj_t
		BgL_envz00_2602, obj_t BgL_nodez00_2603)
	{
		{	/* Reduce/cond.scm 98 */
			return ((BgL_nodez00_bglt) ((BgL_closurez00_bglt) BgL_nodez00_2603));
		}

	}



/* &node-cond!-var1279 */
	BgL_nodez00_bglt BGl_z62nodezd2condz12zd2var1279z70zzreduce_condz00(obj_t
		BgL_envz00_2604, obj_t BgL_nodez00_2605)
	{
		{	/* Reduce/cond.scm 92 */
			return ((BgL_nodez00_bglt) ((BgL_varz00_bglt) BgL_nodez00_2605));
		}

	}



/* &node-cond!-kwote1276 */
	BgL_nodez00_bglt BGl_z62nodezd2condz12zd2kwote1276z70zzreduce_condz00(obj_t
		BgL_envz00_2606, obj_t BgL_nodez00_2607)
	{
		{	/* Reduce/cond.scm 86 */
			return ((BgL_nodez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_2607));
		}

	}



/* &node-cond!-atom1274 */
	BgL_nodez00_bglt BGl_z62nodezd2condz12zd2atom1274z70zzreduce_condz00(obj_t
		BgL_envz00_2608, obj_t BgL_nodez00_2609)
	{
		{	/* Reduce/cond.scm 80 */
			return ((BgL_nodez00_bglt) ((BgL_atomz00_bglt) BgL_nodez00_2609));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzreduce_condz00(void)
	{
		{	/* Reduce/cond.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1996z00zzreduce_condz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1996z00zzreduce_condz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1996z00zzreduce_condz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1996z00zzreduce_condz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1996z00zzreduce_condz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1996z00zzreduce_condz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1996z00zzreduce_condz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1996z00zzreduce_condz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1996z00zzreduce_condz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1996z00zzreduce_condz00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1996z00zzreduce_condz00));
			return
				BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string1996z00zzreduce_condz00));
		}

	}

#ifdef __cplusplus
}
#endif
