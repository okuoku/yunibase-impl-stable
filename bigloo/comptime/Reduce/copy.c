/*===========================================================================*/
/*   (Reduce/copy.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Reduce/copy.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_REDUCE_COPY_TYPE_DEFINITIONS
#define BGL_REDUCE_COPY_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_ftypez00;
	}                 *BgL_vlengthz00_bglt;

	typedef struct BgL_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_classz00;
	}                    *BgL_instanceofz00_bglt;

	typedef struct BgL_castzd2nullzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                     *BgL_castzd2nullzd2_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_REDUCE_COPY_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62reducezd2copyz12za2zzreduce_copyz00(obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_returnz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2sync1296z70zzreduce_copyz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzreduce_copyz00 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2boxzd2ref1350za2zzreduce_copyz00(obj_t, obj_t);
	extern obj_t BGl_vlengthz00zzast_nodez00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	extern BgL_nodez00_bglt BGl_alphatiza7eza7zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t, BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62copyablezf3zd2var1320z43zzreduce_copyz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzreduce_copyz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_z62copyablezf3zd2conditiona1332z43zzreduce_copyz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62copyablezf3zd2cast1326z43zzreduce_copyz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzreduce_copyz00(void);
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2closure1292z70zzreduce_copyz00(obj_t, obj_t);
	extern obj_t BGl_za2cellza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2condition1308z70zzreduce_copyz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzreduce_copyz00(void);
	static obj_t BGl_z62copyablezf31315z91zzreduce_copyz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2kwote1288z70zzreduce_copyz00(obj_t, obj_t);
	static obj_t BGl_z62copyablezf3zd2fail1334z43zzreduce_copyz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2extern1302z70zzreduce_copyz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzreduce_copyz00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2switch1312z70zzreduce_copyz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2return1344z70zzreduce_copyz00(obj_t, obj_t);
	static bool_t
		BGl_cellzd2typezd2lesszd2specificzf3z21zzreduce_copyz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2boxzd2setz121348zb0zzreduce_copyz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2makezd2box1346za2zzreduce_copyz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2appzd2ly1298za2zzreduce_copyz00(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern bool_t BGl_typezd2lesszd2specificzf3zf3zztype_miscz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2letzd2fun1314za2zzreduce_copyz00(obj_t, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2atom1286z70zzreduce_copyz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_reducezd2copyz12zc0zzreduce_copyz00(obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2jumpzd2exzd2i1340z70zzreduce_copyz00(obj_t, obj_t);
	static obj_t BGl_z62copyablezf3z91zzreduce_copyz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzreduce_copyz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_alphatiza7eza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2setzd2exzd2it1338z70zzreduce_copyz00(obj_t, obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static long BGl_za2copyzd2removedza2zd2zzreduce_copyz00 = 0L;
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzreduce_copyz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzreduce_copyz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzreduce_copyz00(void);
	static obj_t BGl_copyablezf3zf3zzreduce_copyz00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzreduce_copyz00(void);
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2cast1304z70zzreduce_copyz00(obj_t, obj_t);
	extern obj_t BGl_retblockz00zzast_nodez00;
	static obj_t BGl_z62copyablezf3zd2vlength1324z43zzreduce_copyz00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2setq1306z70zzreduce_copyz00(obj_t, obj_t);
	extern obj_t BGl_instanceofz00zzast_nodez00;
	static obj_t BGl_nodezd2copyza2z12z62zzreduce_copyz00(obj_t);
	static obj_t BGl_z62copyablezf3zd2atom1318z43zzreduce_copyz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62copyablezf3zd2instanceof1330z43zzreduce_copyz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2fail1310z70zzreduce_copyz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2copyz121283za2zzreduce_copyz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_nodez00_bglt);
	extern obj_t BGl_castzd2nullzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2var1290z70zzreduce_copyz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2sequence1294z70zzreduce_copyz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2funcall1300z70zzreduce_copyz00(obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62nodezd2copyz12za2zzreduce_copyz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2app1352z70zzreduce_copyz00(obj_t, obj_t);
	static obj_t BGl_z62copyablezf3zd2castzd2null1328z91zzreduce_copyz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2letzd2var1336za2zzreduce_copyz00(obj_t, obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t BGl_z62copyablezf3zd2sequence1322z43zzreduce_copyz00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2retblock1342z70zzreduce_copyz00(obj_t, obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1900z00zzreduce_copyz00,
		BgL_bgl_za762copyableza7f3za7d1919za7,
		BGl_z62copyablezf3zd2sequence1322z43zzreduce_copyz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1901z00zzreduce_copyz00,
		BgL_bgl_za762copyableza7f3za7d1920za7,
		BGl_z62copyablezf3zd2vlength1324z43zzreduce_copyz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1902z00zzreduce_copyz00,
		BgL_bgl_za762copyableza7f3za7d1921za7,
		BGl_z62copyablezf3zd2cast1326z43zzreduce_copyz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1903z00zzreduce_copyz00,
		BgL_bgl_za762copyableza7f3za7d1922za7,
		BGl_z62copyablezf3zd2castzd2null1328z91zzreduce_copyz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1904z00zzreduce_copyz00,
		BgL_bgl_za762copyableza7f3za7d1923za7,
		BGl_z62copyablezf3zd2instanceof1330z43zzreduce_copyz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1905z00zzreduce_copyz00,
		BgL_bgl_za762copyableza7f3za7d1924za7,
		BGl_z62copyablezf3zd2conditiona1332z43zzreduce_copyz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1906z00zzreduce_copyz00,
		BgL_bgl_za762copyableza7f3za7d1925za7,
		BGl_z62copyablezf3zd2fail1334z43zzreduce_copyz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1907z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711926za7,
		BGl_z62nodezd2copyz12zd2letzd2var1336za2zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1908z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711927za7,
		BGl_z62nodezd2copyz12zd2setzd2exzd2it1338z70zzreduce_copyz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1909z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711928za7,
		BGl_z62nodezd2copyz12zd2jumpzd2exzd2i1340z70zzreduce_copyz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1916z00zzreduce_copyz00,
		BgL_bgl_string1916za700za7za7r1929za7, "reduce_copy", 11);
	      DEFINE_STRING(BGl_string1917z00zzreduce_copyz00,
		BgL_bgl_string1917za700za7za7r1930za7, "read node-copy!1283 done ", 25);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1910z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711931za7,
		BGl_z62nodezd2copyz12zd2retblock1342z70zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1911z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711932za7,
		BGl_z62nodezd2copyz12zd2return1344z70zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1912z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711933za7,
		BGl_z62nodezd2copyz12zd2makezd2box1346za2zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1913z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711934za7,
		BGl_z62nodezd2copyz12zd2boxzd2setz121348zb0zzreduce_copyz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1914z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711935za7,
		BGl_z62nodezd2copyz12zd2boxzd2ref1350za2zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1915z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711936za7,
		BGl_z62nodezd2copyz12zd2app1352z70zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_copyablezf3zd2envz21zzreduce_copyz00,
		BgL_bgl_za762copyableza7f3za791937za7,
		BGl_z62copyablezf3z91zzreduce_copyz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1874z00zzreduce_copyz00,
		BgL_bgl_string1874za700za7za7r1938za7, "      copy propagation       ", 29);
	      DEFINE_STRING(BGl_string1875z00zzreduce_copyz00,
		BgL_bgl_string1875za700za7za7r1939za7, "(removed: ", 10);
	      DEFINE_STRING(BGl_string1877z00zzreduce_copyz00,
		BgL_bgl_string1877za700za7za7r1940za7, "node-copy!1283", 14);
	      DEFINE_STRING(BGl_string1879z00zzreduce_copyz00,
		BgL_bgl_string1879za700za7za7r1941za7, "copyable?1315", 13);
	      DEFINE_STRING(BGl_string1880z00zzreduce_copyz00,
		BgL_bgl_string1880za700za7za7r1942za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1882z00zzreduce_copyz00,
		BgL_bgl_string1882za700za7za7r1943za7, "node-copy!", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1876z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711944za7,
		BGl_z62nodezd2copyz121283za2zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1878z00zzreduce_copyz00,
		BgL_bgl_za762copyableza7f3131945z00,
		BGl_z62copyablezf31315z91zzreduce_copyz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1881z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711946za7,
		BGl_z62nodezd2copyz12zd2atom1286z70zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1883z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711947za7,
		BGl_z62nodezd2copyz12zd2kwote1288z70zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1884z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711948za7,
		BGl_z62nodezd2copyz12zd2var1290z70zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1885z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711949za7,
		BGl_z62nodezd2copyz12zd2closure1292z70zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1886z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711950za7,
		BGl_z62nodezd2copyz12zd2sequence1294z70zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1887z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711951za7,
		BGl_z62nodezd2copyz12zd2sync1296z70zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1888z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711952za7,
		BGl_z62nodezd2copyz12zd2appzd2ly1298za2zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1889z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711953za7,
		BGl_z62nodezd2copyz12zd2funcall1300z70zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1898z00zzreduce_copyz00,
		BgL_bgl_string1898za700za7za7r1954za7, "copyable?", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1890z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711955za7,
		BGl_z62nodezd2copyz12zd2extern1302z70zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1891z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711956za7,
		BGl_z62nodezd2copyz12zd2cast1304z70zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1892z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711957za7,
		BGl_z62nodezd2copyz12zd2setq1306z70zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1893z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711958za7,
		BGl_z62nodezd2copyz12zd2condition1308z70zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1894z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711959za7,
		BGl_z62nodezd2copyz12zd2fail1310z70zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1895z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711960za7,
		BGl_z62nodezd2copyz12zd2switch1312z70zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1896z00zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711961za7,
		BGl_z62nodezd2copyz12zd2letzd2fun1314za2zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1897z00zzreduce_copyz00,
		BgL_bgl_za762copyableza7f3za7d1962za7,
		BGl_z62copyablezf3zd2atom1318z43zzreduce_copyz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1899z00zzreduce_copyz00,
		BgL_bgl_za762copyableza7f3za7d1963za7,
		BGl_z62copyablezf3zd2var1320z43zzreduce_copyz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_reducezd2copyz12zd2envz12zzreduce_copyz00,
		BgL_bgl_za762reduceza7d2copy1964z00,
		BGl_z62reducezd2copyz12za2zzreduce_copyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
		BgL_bgl_za762nodeza7d2copyza711965za7,
		BGl_z62nodezd2copyz12za2zzreduce_copyz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzreduce_copyz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzreduce_copyz00(long
		BgL_checksumz00_2416, char *BgL_fromz00_2417)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzreduce_copyz00))
				{
					BGl_requirezd2initializa7ationz75zzreduce_copyz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzreduce_copyz00();
					BGl_libraryzd2moduleszd2initz00zzreduce_copyz00();
					BGl_cnstzd2initzd2zzreduce_copyz00();
					BGl_importedzd2moduleszd2initz00zzreduce_copyz00();
					BGl_genericzd2initzd2zzreduce_copyz00();
					BGl_methodzd2initzd2zzreduce_copyz00();
					return BGl_toplevelzd2initzd2zzreduce_copyz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzreduce_copyz00(void)
	{
		{	/* Reduce/copy.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "reduce_copy");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "reduce_copy");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "reduce_copy");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "reduce_copy");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "reduce_copy");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"reduce_copy");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "reduce_copy");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "reduce_copy");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"reduce_copy");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"reduce_copy");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzreduce_copyz00(void)
	{
		{	/* Reduce/copy.scm 15 */
			{	/* Reduce/copy.scm 15 */
				obj_t BgL_cportz00_2242;

				{	/* Reduce/copy.scm 15 */
					obj_t BgL_stringz00_2249;

					BgL_stringz00_2249 = BGl_string1917z00zzreduce_copyz00;
					{	/* Reduce/copy.scm 15 */
						obj_t BgL_startz00_2250;

						BgL_startz00_2250 = BINT(0L);
						{	/* Reduce/copy.scm 15 */
							obj_t BgL_endz00_2251;

							BgL_endz00_2251 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2249)));
							{	/* Reduce/copy.scm 15 */

								BgL_cportz00_2242 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2249, BgL_startz00_2250, BgL_endz00_2251);
				}}}}
				{
					long BgL_iz00_2243;

					BgL_iz00_2243 = 2L;
				BgL_loopz00_2244:
					if ((BgL_iz00_2243 == -1L))
						{	/* Reduce/copy.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Reduce/copy.scm 15 */
							{	/* Reduce/copy.scm 15 */
								obj_t BgL_arg1918z00_2245;

								{	/* Reduce/copy.scm 15 */

									{	/* Reduce/copy.scm 15 */
										obj_t BgL_locationz00_2247;

										BgL_locationz00_2247 = BBOOL(((bool_t) 0));
										{	/* Reduce/copy.scm 15 */

											BgL_arg1918z00_2245 =
												BGl_readz00zz__readerz00(BgL_cportz00_2242,
												BgL_locationz00_2247);
										}
									}
								}
								{	/* Reduce/copy.scm 15 */
									int BgL_tmpz00_2447;

									BgL_tmpz00_2447 = (int) (BgL_iz00_2243);
									CNST_TABLE_SET(BgL_tmpz00_2447, BgL_arg1918z00_2245);
							}}
							{	/* Reduce/copy.scm 15 */
								int BgL_auxz00_2248;

								BgL_auxz00_2248 = (int) ((BgL_iz00_2243 - 1L));
								{
									long BgL_iz00_2452;

									BgL_iz00_2452 = (long) (BgL_auxz00_2248);
									BgL_iz00_2243 = BgL_iz00_2452;
									goto BgL_loopz00_2244;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzreduce_copyz00(void)
	{
		{	/* Reduce/copy.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzreduce_copyz00(void)
	{
		{	/* Reduce/copy.scm 15 */
			BGl_za2copyzd2removedza2zd2zzreduce_copyz00 = 0L;
			return BUNSPEC;
		}

	}



/* reduce-copy! */
	BGL_EXPORTED_DEF obj_t BGl_reducezd2copyz12zc0zzreduce_copyz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Reduce/copy.scm 35 */
			{	/* Reduce/copy.scm 36 */
				obj_t BgL_list1365z00_1401;

				BgL_list1365z00_1401 =
					MAKE_YOUNG_PAIR(BGl_string1874z00zzreduce_copyz00, BNIL);
				BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1365z00_1401);
			}
			BGl_za2copyzd2removedza2zd2zzreduce_copyz00 = 0L;
			{
				obj_t BgL_l1258z00_1403;

				BgL_l1258z00_1403 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31366ze3z87_1404:
				if (PAIRP(BgL_l1258z00_1403))
					{	/* Reduce/copy.scm 38 */
						{	/* Reduce/copy.scm 39 */
							obj_t BgL_globalz00_1406;

							BgL_globalz00_1406 = CAR(BgL_l1258z00_1403);
							{	/* Reduce/copy.scm 39 */
								BgL_valuez00_bglt BgL_funz00_1407;

								BgL_funz00_1407 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_1406))))->
									BgL_valuez00);
								{	/* Reduce/copy.scm 39 */
									obj_t BgL_nodez00_1408;

									BgL_nodez00_1408 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_funz00_1407)))->BgL_bodyz00);
									{	/* Reduce/copy.scm 40 */

										{	/* Reduce/copy.scm 41 */
											BgL_nodez00_bglt BgL_arg1370z00_1409;

											BgL_arg1370z00_1409 =
												BGl_nodezd2copyz12zc0zzreduce_copyz00(
												((BgL_nodez00_bglt) BgL_nodez00_1408));
											((((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_funz00_1407)))->
													BgL_bodyz00) =
												((obj_t) ((obj_t) BgL_arg1370z00_1409)), BUNSPEC);
										}
										BUNSPEC;
									}
								}
							}
						}
						{
							obj_t BgL_l1258z00_2471;

							BgL_l1258z00_2471 = CDR(BgL_l1258z00_1403);
							BgL_l1258z00_1403 = BgL_l1258z00_2471;
							goto BgL_zc3z04anonymousza31366ze3z87_1404;
						}
					}
				else
					{	/* Reduce/copy.scm 38 */
						((bool_t) 1);
					}
			}
			{	/* Reduce/copy.scm 44 */
				obj_t BgL_list1372z00_1412;

				{	/* Reduce/copy.scm 44 */
					obj_t BgL_arg1375z00_1413;

					{	/* Reduce/copy.scm 44 */
						obj_t BgL_arg1376z00_1414;

						{	/* Reduce/copy.scm 44 */
							obj_t BgL_arg1377z00_1415;

							BgL_arg1377z00_1415 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
							BgL_arg1376z00_1414 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ')')),
								BgL_arg1377z00_1415);
						}
						BgL_arg1375z00_1413 =
							MAKE_YOUNG_PAIR(BINT(BGl_za2copyzd2removedza2zd2zzreduce_copyz00),
							BgL_arg1376z00_1414);
					}
					BgL_list1372z00_1412 =
						MAKE_YOUNG_PAIR(BGl_string1875z00zzreduce_copyz00,
						BgL_arg1375z00_1413);
				}
				BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1372z00_1412);
			}
			return BgL_globalsz00_3;
		}

	}



/* &reduce-copy! */
	obj_t BGl_z62reducezd2copyz12za2zzreduce_copyz00(obj_t BgL_envz00_2120,
		obj_t BgL_globalsz00_2121)
	{
		{	/* Reduce/copy.scm 35 */
			return BGl_reducezd2copyz12zc0zzreduce_copyz00(BgL_globalsz00_2121);
		}

	}



/* cell-type-less-specific? */
	bool_t
		BGl_cellzd2typezd2lesszd2specificzf3z21zzreduce_copyz00(BgL_typez00_bglt
		BgL_ty1z00_41, BgL_typez00_bglt BgL_ty2z00_42)
	{
		{	/* Reduce/copy.scm 315 */
			{	/* Reduce/copy.scm 316 */
				bool_t BgL__ortest_1132z00_1416;

				BgL__ortest_1132z00_1416 =
					BGl_typezd2lesszd2specificzf3zf3zztype_miscz00(BgL_ty1z00_41,
					BgL_ty2z00_42);
				if (BgL__ortest_1132z00_1416)
					{	/* Reduce/copy.scm 316 */
						return BgL__ortest_1132z00_1416;
					}
				else
					{	/* Reduce/copy.scm 316 */
						if ((((obj_t) BgL_ty2z00_42) == BGl_za2objza2z00zztype_cachez00))
							{	/* Reduce/copy.scm 317 */
								return
									(((obj_t) BgL_ty1z00_41) == BGl_za2cellza2z00zztype_cachez00);
							}
						else
							{	/* Reduce/copy.scm 317 */
								return ((bool_t) 0);
							}
					}
			}
		}

	}



/* node-copy*! */
	obj_t BGl_nodezd2copyza2z12z62zzreduce_copyz00(obj_t BgL_nodeza2za2_51)
	{
		{	/* Reduce/copy.scm 404 */
			{
				obj_t BgL_nodeza2za2_1419;

				BgL_nodeza2za2_1419 = BgL_nodeza2za2_51;
			BgL_zc3z04anonymousza31378ze3z87_1420:
				if (NULLP(BgL_nodeza2za2_1419))
					{	/* Reduce/copy.scm 406 */
						return CNST_TABLE_REF(0);
					}
				else
					{	/* Reduce/copy.scm 406 */
						{	/* Reduce/copy.scm 409 */
							BgL_nodez00_bglt BgL_arg1408z00_1422;

							{	/* Reduce/copy.scm 409 */
								obj_t BgL_arg1410z00_1423;

								BgL_arg1410z00_1423 = CAR(((obj_t) BgL_nodeza2za2_1419));
								BgL_arg1408z00_1422 =
									BGl_nodezd2copyz12zc0zzreduce_copyz00(
									((BgL_nodez00_bglt) BgL_arg1410z00_1423));
							}
							{	/* Reduce/copy.scm 409 */
								obj_t BgL_auxz00_2499;
								obj_t BgL_tmpz00_2497;

								BgL_auxz00_2499 = ((obj_t) BgL_arg1408z00_1422);
								BgL_tmpz00_2497 = ((obj_t) BgL_nodeza2za2_1419);
								SET_CAR(BgL_tmpz00_2497, BgL_auxz00_2499);
							}
						}
						{	/* Reduce/copy.scm 410 */
							obj_t BgL_arg1421z00_1424;

							BgL_arg1421z00_1424 = CDR(((obj_t) BgL_nodeza2za2_1419));
							{
								obj_t BgL_nodeza2za2_2504;

								BgL_nodeza2za2_2504 = BgL_arg1421z00_1424;
								BgL_nodeza2za2_1419 = BgL_nodeza2za2_2504;
								goto BgL_zc3z04anonymousza31378ze3z87_1420;
							}
						}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzreduce_copyz00(void)
	{
		{	/* Reduce/copy.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzreduce_copyz00(void)
	{
		{	/* Reduce/copy.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_proc1876z00zzreduce_copyz00, BGl_nodez00zzast_nodez00,
				BGl_string1877z00zzreduce_copyz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_copyablezf3zd2envz21zzreduce_copyz00,
				BGl_proc1878z00zzreduce_copyz00, BFALSE,
				BGl_string1879z00zzreduce_copyz00);
		}

	}



/* &copyable?1315 */
	obj_t BGl_z62copyablezf31315z91zzreduce_copyz00(obj_t BgL_envz00_2124,
		obj_t BgL_nodez00_2125, obj_t BgL_vz00_2126)
	{
		{	/* Reduce/copy.scm 194 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &node-copy!1283 */
	obj_t BGl_z62nodezd2copyz121283za2zzreduce_copyz00(obj_t BgL_envz00_2127,
		obj_t BgL_nodez00_2128)
	{
		{	/* Reduce/copy.scm 55 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(1),
				BGl_string1880z00zzreduce_copyz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2128)));
		}

	}



/* node-copy! */
	BgL_nodez00_bglt BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_nodez00_bglt
		BgL_nodez00_4)
	{
		{	/* Reduce/copy.scm 55 */
			{	/* Reduce/copy.scm 55 */
				obj_t BgL_method1284z00_1435;

				{	/* Reduce/copy.scm 55 */
					obj_t BgL_res1865z00_1966;

					{	/* Reduce/copy.scm 55 */
						long BgL_objzd2classzd2numz00_1937;

						BgL_objzd2classzd2numz00_1937 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_4));
						{	/* Reduce/copy.scm 55 */
							obj_t BgL_arg1811z00_1938;

							BgL_arg1811z00_1938 =
								PROCEDURE_REF(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
								(int) (1L));
							{	/* Reduce/copy.scm 55 */
								int BgL_offsetz00_1941;

								BgL_offsetz00_1941 = (int) (BgL_objzd2classzd2numz00_1937);
								{	/* Reduce/copy.scm 55 */
									long BgL_offsetz00_1942;

									BgL_offsetz00_1942 =
										((long) (BgL_offsetz00_1941) - OBJECT_TYPE);
									{	/* Reduce/copy.scm 55 */
										long BgL_modz00_1943;

										BgL_modz00_1943 =
											(BgL_offsetz00_1942 >> (int) ((long) ((int) (4L))));
										{	/* Reduce/copy.scm 55 */
											long BgL_restz00_1945;

											BgL_restz00_1945 =
												(BgL_offsetz00_1942 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Reduce/copy.scm 55 */

												{	/* Reduce/copy.scm 55 */
													obj_t BgL_bucketz00_1947;

													BgL_bucketz00_1947 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1938), BgL_modz00_1943);
													BgL_res1865z00_1966 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1947), BgL_restz00_1945);
					}}}}}}}}
					BgL_method1284z00_1435 = BgL_res1865z00_1966;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1284z00_1435, ((obj_t) BgL_nodez00_4)));
			}
		}

	}



/* &node-copy! */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12za2zzreduce_copyz00(obj_t
		BgL_envz00_2129, obj_t BgL_nodez00_2130)
	{
		{	/* Reduce/copy.scm 55 */
			return
				BGl_nodezd2copyz12zc0zzreduce_copyz00(
				((BgL_nodez00_bglt) BgL_nodez00_2130));
		}

	}



/* copyable? */
	obj_t BGl_copyablezf3zf3zzreduce_copyz00(obj_t BgL_nodez00_20,
		obj_t BgL_vz00_21)
	{
		{	/* Reduce/copy.scm 194 */
			if (BGL_OBJECTP(BgL_nodez00_20))
				{	/* Reduce/copy.scm 194 */
					obj_t BgL_method1316z00_1437;

					{	/* Reduce/copy.scm 194 */
						obj_t BgL_res1870z00_1997;

						{	/* Reduce/copy.scm 194 */
							long BgL_objzd2classzd2numz00_1968;

							BgL_objzd2classzd2numz00_1968 =
								BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_20));
							{	/* Reduce/copy.scm 194 */
								obj_t BgL_arg1811z00_1969;

								BgL_arg1811z00_1969 =
									PROCEDURE_REF(BGl_copyablezf3zd2envz21zzreduce_copyz00,
									(int) (1L));
								{	/* Reduce/copy.scm 194 */
									int BgL_offsetz00_1972;

									BgL_offsetz00_1972 = (int) (BgL_objzd2classzd2numz00_1968);
									{	/* Reduce/copy.scm 194 */
										long BgL_offsetz00_1973;

										BgL_offsetz00_1973 =
											((long) (BgL_offsetz00_1972) - OBJECT_TYPE);
										{	/* Reduce/copy.scm 194 */
											long BgL_modz00_1974;

											BgL_modz00_1974 =
												(BgL_offsetz00_1973 >> (int) ((long) ((int) (4L))));
											{	/* Reduce/copy.scm 194 */
												long BgL_restz00_1976;

												BgL_restz00_1976 =
													(BgL_offsetz00_1973 &
													(long) (
														(int) (
															((long) (
																	(int) (
																		(1L <<
																			(int) ((long) ((int) (4L)))))) - 1L))));
												{	/* Reduce/copy.scm 194 */

													{	/* Reduce/copy.scm 194 */
														obj_t BgL_bucketz00_1978;

														BgL_bucketz00_1978 =
															VECTOR_REF(
															((obj_t) BgL_arg1811z00_1969), BgL_modz00_1974);
														BgL_res1870z00_1997 =
															VECTOR_REF(
															((obj_t) BgL_bucketz00_1978), BgL_restz00_1976);
						}}}}}}}}
						BgL_method1316z00_1437 = BgL_res1870z00_1997;
					}
					return
						BGL_PROCEDURE_CALL2(BgL_method1316z00_1437, BgL_nodez00_20,
						BgL_vz00_21);
				}
			else
				{	/* Reduce/copy.scm 194 */
					obj_t BgL_fun1436z00_1438;

					BgL_fun1436z00_1438 =
						PROCEDURE_REF(BGl_copyablezf3zd2envz21zzreduce_copyz00, (int) (0L));
					return
						BGL_PROCEDURE_CALL2(BgL_fun1436z00_1438, BgL_nodez00_20,
						BgL_vz00_21);
				}
		}

	}



/* &copyable? */
	obj_t BGl_z62copyablezf3z91zzreduce_copyz00(obj_t BgL_envz00_2131,
		obj_t BgL_nodez00_2132, obj_t BgL_vz00_2133)
	{
		{	/* Reduce/copy.scm 194 */
			return
				BGl_copyablezf3zf3zzreduce_copyz00(BgL_nodez00_2132, BgL_vz00_2133);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzreduce_copyz00(void)
	{
		{	/* Reduce/copy.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00, BGl_atomz00zzast_nodez00,
				BGl_proc1881z00zzreduce_copyz00, BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00, BGl_kwotez00zzast_nodez00,
				BGl_proc1883z00zzreduce_copyz00, BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00, BGl_varz00zzast_nodez00,
				BGl_proc1884z00zzreduce_copyz00, BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_closurez00zzast_nodez00, BGl_proc1885z00zzreduce_copyz00,
				BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1886z00zzreduce_copyz00,
				BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00, BGl_syncz00zzast_nodez00,
				BGl_proc1887z00zzreduce_copyz00, BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1888z00zzreduce_copyz00,
				BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_funcallz00zzast_nodez00, BGl_proc1889z00zzreduce_copyz00,
				BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_externz00zzast_nodez00, BGl_proc1890z00zzreduce_copyz00,
				BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00, BGl_castz00zzast_nodez00,
				BGl_proc1891z00zzreduce_copyz00, BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00, BGl_setqz00zzast_nodez00,
				BGl_proc1892z00zzreduce_copyz00, BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1893z00zzreduce_copyz00,
				BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00, BGl_failz00zzast_nodez00,
				BGl_proc1894z00zzreduce_copyz00, BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_switchz00zzast_nodez00, BGl_proc1895z00zzreduce_copyz00,
				BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1896z00zzreduce_copyz00,
				BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_copyablezf3zd2envz21zzreduce_copyz00, BGl_atomz00zzast_nodez00,
				BGl_proc1897z00zzreduce_copyz00, BGl_string1898z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_copyablezf3zd2envz21zzreduce_copyz00, BGl_varz00zzast_nodez00,
				BGl_proc1899z00zzreduce_copyz00, BGl_string1898z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_copyablezf3zd2envz21zzreduce_copyz00, BGl_sequencez00zzast_nodez00,
				BGl_proc1900z00zzreduce_copyz00, BGl_string1898z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_copyablezf3zd2envz21zzreduce_copyz00, BGl_vlengthz00zzast_nodez00,
				BGl_proc1901z00zzreduce_copyz00, BGl_string1898z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_copyablezf3zd2envz21zzreduce_copyz00, BGl_castz00zzast_nodez00,
				BGl_proc1902z00zzreduce_copyz00, BGl_string1898z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_copyablezf3zd2envz21zzreduce_copyz00,
				BGl_castzd2nullzd2zzast_nodez00, BGl_proc1903z00zzreduce_copyz00,
				BGl_string1898z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_copyablezf3zd2envz21zzreduce_copyz00,
				BGl_instanceofz00zzast_nodez00, BGl_proc1904z00zzreduce_copyz00,
				BGl_string1898z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_copyablezf3zd2envz21zzreduce_copyz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1905z00zzreduce_copyz00,
				BGl_string1898z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_copyablezf3zd2envz21zzreduce_copyz00, BGl_failz00zzast_nodez00,
				BGl_proc1906z00zzreduce_copyz00, BGl_string1898z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1907z00zzreduce_copyz00,
				BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1908z00zzreduce_copyz00,
				BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1909z00zzreduce_copyz00,
				BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_retblockz00zzast_nodez00, BGl_proc1910z00zzreduce_copyz00,
				BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_returnz00zzast_nodez00, BGl_proc1911z00zzreduce_copyz00,
				BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1912z00zzreduce_copyz00,
				BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1913z00zzreduce_copyz00,
				BGl_string1882z00zzreduce_copyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1914z00zzreduce_copyz00,
				BGl_string1882z00zzreduce_copyz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2copyz12zd2envz12zzreduce_copyz00, BGl_appz00zzast_nodez00,
				BGl_proc1915z00zzreduce_copyz00, BGl_string1882z00zzreduce_copyz00);
		}

	}



/* &node-copy!-app1352 */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12zd2app1352z70zzreduce_copyz00(obj_t
		BgL_envz00_2167, obj_t BgL_nodez00_2168)
	{
		{	/* Reduce/copy.scm 396 */
			BGl_nodezd2copyza2z12z62zzreduce_copyz00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2168)))->BgL_argsz00));
			return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2168));
		}

	}



/* &node-copy!-box-ref1350 */
	BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2boxzd2ref1350za2zzreduce_copyz00(obj_t
		BgL_envz00_2169, obj_t BgL_nodez00_2170)
	{
		{	/* Reduce/copy.scm 381 */
			{	/* Reduce/copy.scm 383 */
				BgL_nodez00_bglt BgL_cpz00_2256;

				{	/* Reduce/copy.scm 383 */
					BgL_varz00_bglt BgL_arg1746z00_2257;

					BgL_arg1746z00_2257 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2170)))->BgL_varz00);
					BgL_cpz00_2256 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(
						((BgL_nodez00_bglt) BgL_arg1746z00_2257));
				}
				{	/* Reduce/copy.scm 384 */
					bool_t BgL_test1973z00_2627;

					{	/* Reduce/copy.scm 384 */
						obj_t BgL_classz00_2258;

						BgL_classz00_2258 = BGl_castz00zzast_nodez00;
						{	/* Reduce/copy.scm 384 */
							BgL_objectz00_bglt BgL_arg1807z00_2259;

							{	/* Reduce/copy.scm 384 */
								obj_t BgL_tmpz00_2628;

								BgL_tmpz00_2628 =
									((obj_t) ((BgL_objectz00_bglt) BgL_cpz00_2256));
								BgL_arg1807z00_2259 = (BgL_objectz00_bglt) (BgL_tmpz00_2628);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Reduce/copy.scm 384 */
									long BgL_idxz00_2260;

									BgL_idxz00_2260 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2259);
									BgL_test1973z00_2627 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2260 + 2L)) == BgL_classz00_2258);
								}
							else
								{	/* Reduce/copy.scm 384 */
									bool_t BgL_res1873z00_2263;

									{	/* Reduce/copy.scm 384 */
										obj_t BgL_oclassz00_2264;

										{	/* Reduce/copy.scm 384 */
											obj_t BgL_arg1815z00_2265;
											long BgL_arg1816z00_2266;

											BgL_arg1815z00_2265 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Reduce/copy.scm 384 */
												long BgL_arg1817z00_2267;

												BgL_arg1817z00_2267 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2259);
												BgL_arg1816z00_2266 =
													(BgL_arg1817z00_2267 - OBJECT_TYPE);
											}
											BgL_oclassz00_2264 =
												VECTOR_REF(BgL_arg1815z00_2265, BgL_arg1816z00_2266);
										}
										{	/* Reduce/copy.scm 384 */
											bool_t BgL__ortest_1115z00_2268;

											BgL__ortest_1115z00_2268 =
												(BgL_classz00_2258 == BgL_oclassz00_2264);
											if (BgL__ortest_1115z00_2268)
												{	/* Reduce/copy.scm 384 */
													BgL_res1873z00_2263 = BgL__ortest_1115z00_2268;
												}
											else
												{	/* Reduce/copy.scm 384 */
													long BgL_odepthz00_2269;

													{	/* Reduce/copy.scm 384 */
														obj_t BgL_arg1804z00_2270;

														BgL_arg1804z00_2270 = (BgL_oclassz00_2264);
														BgL_odepthz00_2269 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2270);
													}
													if ((2L < BgL_odepthz00_2269))
														{	/* Reduce/copy.scm 384 */
															obj_t BgL_arg1802z00_2271;

															{	/* Reduce/copy.scm 384 */
																obj_t BgL_arg1803z00_2272;

																BgL_arg1803z00_2272 = (BgL_oclassz00_2264);
																BgL_arg1802z00_2271 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2272,
																	2L);
															}
															BgL_res1873z00_2263 =
																(BgL_arg1802z00_2271 == BgL_classz00_2258);
														}
													else
														{	/* Reduce/copy.scm 384 */
															BgL_res1873z00_2263 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1973z00_2627 = BgL_res1873z00_2263;
								}
						}
					}
					if (BgL_test1973z00_2627)
						{	/* Reduce/copy.scm 384 */
							((((BgL_boxzd2refzd2_bglt) COBJECT(
											((BgL_boxzd2refzd2_bglt) BgL_nodez00_2170)))->
									BgL_varz00) =
								((BgL_varz00_bglt) ((BgL_varz00_bglt) (((BgL_castz00_bglt)
												COBJECT(((BgL_castz00_bglt) BgL_cpz00_2256)))->
											BgL_argz00))), BUNSPEC);
							((((BgL_castz00_bglt) COBJECT(((BgL_castz00_bglt)
												BgL_cpz00_2256)))->BgL_argz00) =
								((BgL_nodez00_bglt) ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt)
											BgL_nodez00_2170))), BUNSPEC);
							return BgL_cpz00_2256;
						}
					else
						{	/* Reduce/copy.scm 384 */
							((((BgL_boxzd2refzd2_bglt) COBJECT(
											((BgL_boxzd2refzd2_bglt) BgL_nodez00_2170)))->
									BgL_varz00) =
								((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_cpz00_2256)),
								BUNSPEC);
							return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt)
									BgL_nodez00_2170));
						}
				}
			}
		}

	}



/* &node-copy!-box-set!1348 */
	BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2boxzd2setz121348zb0zzreduce_copyz00(obj_t
		BgL_envz00_2171, obj_t BgL_nodez00_2172)
	{
		{	/* Reduce/copy.scm 365 */
			{
				BgL_nodez00_bglt BgL_auxz00_2665;

				{	/* Reduce/copy.scm 367 */
					BgL_nodez00_bglt BgL_arg1737z00_2274;

					BgL_arg1737z00_2274 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2172)))->BgL_valuez00);
					BgL_auxz00_2665 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1737z00_2274);
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2172)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_2665), BUNSPEC);
			}
			{	/* Reduce/copy.scm 368 */
				BgL_nodez00_bglt BgL_cpz00_2275;

				{	/* Reduce/copy.scm 368 */
					BgL_varz00_bglt BgL_arg1739z00_2276;

					BgL_arg1739z00_2276 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2172)))->BgL_varz00);
					BgL_cpz00_2275 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(
						((BgL_nodez00_bglt) BgL_arg1739z00_2276));
				}
				{	/* Reduce/copy.scm 369 */
					bool_t BgL_test1977z00_2675;

					{	/* Reduce/copy.scm 369 */
						obj_t BgL_classz00_2277;

						BgL_classz00_2277 = BGl_castz00zzast_nodez00;
						{	/* Reduce/copy.scm 369 */
							BgL_objectz00_bglt BgL_arg1807z00_2278;

							{	/* Reduce/copy.scm 369 */
								obj_t BgL_tmpz00_2676;

								BgL_tmpz00_2676 =
									((obj_t) ((BgL_objectz00_bglt) BgL_cpz00_2275));
								BgL_arg1807z00_2278 = (BgL_objectz00_bglt) (BgL_tmpz00_2676);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Reduce/copy.scm 369 */
									long BgL_idxz00_2279;

									BgL_idxz00_2279 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2278);
									BgL_test1977z00_2675 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2279 + 2L)) == BgL_classz00_2277);
								}
							else
								{	/* Reduce/copy.scm 369 */
									bool_t BgL_res1872z00_2282;

									{	/* Reduce/copy.scm 369 */
										obj_t BgL_oclassz00_2283;

										{	/* Reduce/copy.scm 369 */
											obj_t BgL_arg1815z00_2284;
											long BgL_arg1816z00_2285;

											BgL_arg1815z00_2284 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Reduce/copy.scm 369 */
												long BgL_arg1817z00_2286;

												BgL_arg1817z00_2286 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2278);
												BgL_arg1816z00_2285 =
													(BgL_arg1817z00_2286 - OBJECT_TYPE);
											}
											BgL_oclassz00_2283 =
												VECTOR_REF(BgL_arg1815z00_2284, BgL_arg1816z00_2285);
										}
										{	/* Reduce/copy.scm 369 */
											bool_t BgL__ortest_1115z00_2287;

											BgL__ortest_1115z00_2287 =
												(BgL_classz00_2277 == BgL_oclassz00_2283);
											if (BgL__ortest_1115z00_2287)
												{	/* Reduce/copy.scm 369 */
													BgL_res1872z00_2282 = BgL__ortest_1115z00_2287;
												}
											else
												{	/* Reduce/copy.scm 369 */
													long BgL_odepthz00_2288;

													{	/* Reduce/copy.scm 369 */
														obj_t BgL_arg1804z00_2289;

														BgL_arg1804z00_2289 = (BgL_oclassz00_2283);
														BgL_odepthz00_2288 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2289);
													}
													if ((2L < BgL_odepthz00_2288))
														{	/* Reduce/copy.scm 369 */
															obj_t BgL_arg1802z00_2290;

															{	/* Reduce/copy.scm 369 */
																obj_t BgL_arg1803z00_2291;

																BgL_arg1803z00_2291 = (BgL_oclassz00_2283);
																BgL_arg1802z00_2290 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2291,
																	2L);
															}
															BgL_res1872z00_2282 =
																(BgL_arg1802z00_2290 == BgL_classz00_2277);
														}
													else
														{	/* Reduce/copy.scm 369 */
															BgL_res1872z00_2282 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1977z00_2675 = BgL_res1872z00_2282;
								}
						}
					}
					if (BgL_test1977z00_2675)
						{	/* Reduce/copy.scm 369 */
							((((BgL_boxzd2setz12zc0_bglt) COBJECT(
											((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2172)))->
									BgL_varz00) =
								((BgL_varz00_bglt) ((BgL_varz00_bglt) (((BgL_castz00_bglt)
												COBJECT(((BgL_castz00_bglt) BgL_cpz00_2275)))->
											BgL_argz00))), BUNSPEC);
							((((BgL_castz00_bglt) COBJECT(((BgL_castz00_bglt)
												BgL_cpz00_2275)))->BgL_argz00) =
								((BgL_nodez00_bglt) ((BgL_nodez00_bglt) (
											(BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2172))), BUNSPEC);
							return BgL_cpz00_2275;
						}
					else
						{	/* Reduce/copy.scm 369 */
							((((BgL_boxzd2setz12zc0_bglt) COBJECT(
											((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2172)))->
									BgL_varz00) =
								((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_cpz00_2275)),
								BUNSPEC);
							return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt)
									BgL_nodez00_2172));
						}
				}
			}
		}

	}



/* &node-copy!-make-box1346 */
	BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2makezd2box1346za2zzreduce_copyz00(obj_t
		BgL_envz00_2173, obj_t BgL_nodez00_2174)
	{
		{	/* Reduce/copy.scm 357 */
			{
				BgL_nodez00_bglt BgL_auxz00_2713;

				{	/* Reduce/copy.scm 359 */
					BgL_nodez00_bglt BgL_arg1736z00_2293;

					BgL_arg1736z00_2293 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2174)))->BgL_valuez00);
					BgL_auxz00_2713 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1736z00_2293);
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2174)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_2713), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2174));
		}

	}



/* &node-copy!-return1344 */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12zd2return1344z70zzreduce_copyz00(obj_t
		BgL_envz00_2175, obj_t BgL_nodez00_2176)
	{
		{	/* Reduce/copy.scm 349 */
			{
				BgL_nodez00_bglt BgL_auxz00_2721;

				{	/* Reduce/copy.scm 351 */
					BgL_nodez00_bglt BgL_arg1735z00_2295;

					BgL_arg1735z00_2295 =
						(((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nodez00_2176)))->BgL_valuez00);
					BgL_auxz00_2721 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1735z00_2295);
				}
				((((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nodez00_2176)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_2721), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_returnz00_bglt) BgL_nodez00_2176));
		}

	}



/* &node-copy!-retblock1342 */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12zd2retblock1342z70zzreduce_copyz00(obj_t
		BgL_envz00_2177, obj_t BgL_nodez00_2178)
	{
		{	/* Reduce/copy.scm 341 */
			{
				BgL_nodez00_bglt BgL_auxz00_2729;

				{	/* Reduce/copy.scm 343 */
					BgL_nodez00_bglt BgL_arg1734z00_2297;

					BgL_arg1734z00_2297 =
						(((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nodez00_2178)))->BgL_bodyz00);
					BgL_auxz00_2729 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1734z00_2297);
				}
				((((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nodez00_2178)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2729), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_retblockz00_bglt) BgL_nodez00_2178));
		}

	}



/* &node-copy!-jump-ex-i1340 */
	BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2jumpzd2exzd2i1340z70zzreduce_copyz00(obj_t
		BgL_envz00_2179, obj_t BgL_nodez00_2180)
	{
		{	/* Reduce/copy.scm 332 */
			{
				BgL_nodez00_bglt BgL_auxz00_2737;

				{	/* Reduce/copy.scm 334 */
					BgL_nodez00_bglt BgL_arg1724z00_2299;

					BgL_arg1724z00_2299 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2180)))->BgL_exitz00);
					BgL_auxz00_2737 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1724z00_2299);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2180)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_2737), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2743;

				{	/* Reduce/copy.scm 335 */
					BgL_nodez00_bglt BgL_arg1733z00_2300;

					BgL_arg1733z00_2300 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2180)))->
						BgL_valuez00);
					BgL_auxz00_2743 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1733z00_2300);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2180)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_2743), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2180));
		}

	}



/* &node-copy!-set-ex-it1338 */
	BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2setzd2exzd2it1338z70zzreduce_copyz00(obj_t
		BgL_envz00_2181, obj_t BgL_nodez00_2182)
	{
		{	/* Reduce/copy.scm 322 */
			{
				BgL_nodez00_bglt BgL_auxz00_2751;

				{	/* Reduce/copy.scm 324 */
					BgL_nodez00_bglt BgL_arg1718z00_2302;

					BgL_arg1718z00_2302 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2182)))->BgL_bodyz00);
					BgL_auxz00_2751 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1718z00_2302);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2182)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2751), BUNSPEC);
			}
			{
				BgL_varz00_bglt BgL_auxz00_2757;

				{	/* Reduce/copy.scm 325 */
					BgL_varz00_bglt BgL_arg1720z00_2303;

					BgL_arg1720z00_2303 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2182)))->BgL_varz00);
					BgL_auxz00_2757 =
						((BgL_varz00_bglt)
						BGl_nodezd2copyz12zc0zzreduce_copyz00(
							((BgL_nodez00_bglt) BgL_arg1720z00_2303)));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2182)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_2757), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2765;

				{	/* Reduce/copy.scm 326 */
					BgL_nodez00_bglt BgL_arg1722z00_2304;

					BgL_arg1722z00_2304 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2182)))->
						BgL_onexitz00);
					BgL_auxz00_2765 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1722z00_2304);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2182)))->
						BgL_onexitz00) = ((BgL_nodez00_bglt) BgL_auxz00_2765), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2182));
		}

	}



/* &node-copy!-let-var1336 */
	BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2letzd2var1336za2zzreduce_copyz00(obj_t
		BgL_envz00_2183, obj_t BgL_nodez00_2184)
	{
		{	/* Reduce/copy.scm 262 */
			{
				obj_t BgL_obindingsz00_2307;
				obj_t BgL_nbindingsz00_2308;

				BgL_obindingsz00_2307 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2184)))->BgL_bindingsz00);
				BgL_nbindingsz00_2308 = BNIL;
			BgL_loopz00_2306:
				if (NULLP(BgL_obindingsz00_2307))
					{	/* Reduce/copy.scm 267 */
						bool_t BgL_test1982z00_2775;

						if (NULLP(BgL_nbindingsz00_2308))
							{	/* Reduce/copy.scm 267 */
								BgL_test1982z00_2775 =
									(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_nodez00_2184)))->
									BgL_removablezf3zf3);
							}
						else
							{	/* Reduce/copy.scm 267 */
								BgL_test1982z00_2775 = ((bool_t) 0);
							}
						if (BgL_test1982z00_2775)
							{	/* Reduce/copy.scm 267 */
								return
									BGl_nodezd2copyz12zc0zzreduce_copyz00(
									(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_nodez00_2184)))->
										BgL_bodyz00));
							}
						else
							{	/* Reduce/copy.scm 267 */
								((((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_nodez00_2184)))->
										BgL_bindingsz00) =
									((obj_t) BgL_nbindingsz00_2308), BUNSPEC);
								{
									BgL_nodez00_bglt BgL_auxz00_2785;

									{	/* Reduce/copy.scm 271 */
										BgL_nodez00_bglt BgL_arg1663z00_2309;

										BgL_arg1663z00_2309 =
											(((BgL_letzd2varzd2_bglt) COBJECT(
													((BgL_letzd2varzd2_bglt) BgL_nodez00_2184)))->
											BgL_bodyz00);
										BgL_auxz00_2785 =
											BGl_nodezd2copyz12zc0zzreduce_copyz00
											(BgL_arg1663z00_2309);
									}
									((((BgL_letzd2varzd2_bglt) COBJECT(
													((BgL_letzd2varzd2_bglt) BgL_nodez00_2184)))->
											BgL_bodyz00) =
										((BgL_nodez00_bglt) BgL_auxz00_2785), BUNSPEC);
								}
								return
									((BgL_nodez00_bglt)
									((BgL_letzd2varzd2_bglt) BgL_nodez00_2184));
							}
					}
				else
					{	/* Reduce/copy.scm 273 */
						obj_t BgL_bindingz00_2310;

						BgL_bindingz00_2310 = CAR(((obj_t) BgL_obindingsz00_2307));
						{	/* Reduce/copy.scm 273 */
							obj_t BgL_varz00_2311;

							BgL_varz00_2311 = CAR(((obj_t) BgL_bindingz00_2310));
							{	/* Reduce/copy.scm 274 */
								BgL_nodez00_bglt BgL_valz00_2312;

								{	/* Reduce/copy.scm 275 */
									obj_t BgL_arg1717z00_2313;

									BgL_arg1717z00_2313 = CDR(((obj_t) BgL_bindingz00_2310));
									BgL_valz00_2312 =
										BGl_nodezd2copyz12zc0zzreduce_copyz00(
										((BgL_nodez00_bglt) BgL_arg1717z00_2313));
								}
								{	/* Reduce/copy.scm 275 */

									{	/* Reduce/copy.scm 276 */
										obj_t BgL_auxz00_2803;
										obj_t BgL_tmpz00_2801;

										BgL_auxz00_2803 = ((obj_t) BgL_valz00_2312);
										BgL_tmpz00_2801 = ((obj_t) BgL_bindingz00_2310);
										SET_CDR(BgL_tmpz00_2801, BgL_auxz00_2803);
									}
									{	/* Reduce/copy.scm 291 */
										bool_t BgL_test1984z00_2806;

										if (
											(((BgL_letzd2varzd2_bglt) COBJECT(
														((BgL_letzd2varzd2_bglt) BgL_nodez00_2184)))->
												BgL_removablezf3zf3))
											{	/* Reduce/copy.scm 291 */
												if (
													((((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt) BgL_varz00_2311)))->
															BgL_accessz00) == CNST_TABLE_REF(2)))
													{	/* Reduce/copy.scm 292 */
														if (CBOOL(BGl_copyablezf3zf3zzreduce_copyz00(
																	((obj_t) BgL_valz00_2312), BgL_varz00_2311)))
															{	/* Reduce/copy.scm 295 */
																BgL_typez00_bglt BgL_arg1710z00_2314;
																BgL_typez00_bglt BgL_arg1711z00_2315;

																BgL_arg1710z00_2314 =
																	(((BgL_nodez00_bglt)
																		COBJECT(BgL_valz00_2312))->BgL_typez00);
																BgL_arg1711z00_2315 =
																	(((BgL_variablez00_bglt)
																		COBJECT(((BgL_variablez00_bglt)
																				BgL_varz00_2311)))->BgL_typez00);
																BgL_test1984z00_2806 =
																	BGl_cellzd2typezd2lesszd2specificzf3z21zzreduce_copyz00
																	(BgL_arg1710z00_2314, BgL_arg1711z00_2315);
															}
														else
															{	/* Reduce/copy.scm 293 */
																BgL_test1984z00_2806 = ((bool_t) 0);
															}
													}
												else
													{	/* Reduce/copy.scm 292 */
														BgL_test1984z00_2806 = ((bool_t) 0);
													}
											}
										else
											{	/* Reduce/copy.scm 291 */
												BgL_test1984z00_2806 = ((bool_t) 0);
											}
										if (BgL_test1984z00_2806)
											{	/* Reduce/copy.scm 291 */
												BGl_za2copyzd2removedza2zd2zzreduce_copyz00 =
													(BGl_za2copyzd2removedza2zd2zzreduce_copyz00 + 1L);
												{	/* Reduce/copy.scm 303 */
													BgL_nodez00_bglt BgL_arg1691z00_2316;

													if (
														(((obj_t)
																(((BgL_nodez00_bglt) COBJECT(BgL_valz00_2312))->
																	BgL_typez00)) ==
															((obj_t) (((BgL_variablez00_bglt)
																		COBJECT(((BgL_variablez00_bglt)
																				BgL_varz00_2311)))->BgL_typez00))))
														{	/* Reduce/copy.scm 303 */
															BgL_arg1691z00_2316 = BgL_valz00_2312;
														}
													else
														{	/* Reduce/copy.scm 305 */
															BgL_castz00_bglt BgL_new1131z00_2317;

															{	/* Reduce/copy.scm 306 */
																BgL_castz00_bglt BgL_new1130z00_2318;

																BgL_new1130z00_2318 =
																	((BgL_castz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_castz00_bgl))));
																{	/* Reduce/copy.scm 306 */
																	long BgL_arg1701z00_2319;

																	BgL_arg1701z00_2319 =
																		BGL_CLASS_NUM(BGl_castz00zzast_nodez00);
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1130z00_2318),
																		BgL_arg1701z00_2319);
																}
																{	/* Reduce/copy.scm 306 */
																	BgL_objectz00_bglt BgL_tmpz00_2835;

																	BgL_tmpz00_2835 =
																		((BgL_objectz00_bglt) BgL_new1130z00_2318);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2835,
																		BFALSE);
																}
																((BgL_objectz00_bglt) BgL_new1130z00_2318);
																BgL_new1131z00_2317 = BgL_new1130z00_2318;
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1131z00_2317)))->BgL_locz00) =
																((obj_t) (((BgL_nodez00_bglt)
																			COBJECT(BgL_valz00_2312))->BgL_locz00)),
																BUNSPEC);
															((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																				BgL_new1131z00_2317)))->BgL_typez00) =
																((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																			COBJECT(((BgL_variablez00_bglt)
																					BgL_varz00_2311)))->BgL_typez00)),
																BUNSPEC);
															((((BgL_castz00_bglt)
																		COBJECT(BgL_new1131z00_2317))->BgL_argz00) =
																((BgL_nodez00_bglt) BgL_valz00_2312), BUNSPEC);
															BgL_arg1691z00_2316 =
																((BgL_nodez00_bglt) BgL_new1131z00_2317);
														}
													((((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt) BgL_varz00_2311)))->
															BgL_fastzd2alphazd2) =
														((obj_t) ((obj_t) BgL_arg1691z00_2316)), BUNSPEC);
												}
												{	/* Reduce/copy.scm 309 */
													obj_t BgL_arg1705z00_2320;

													BgL_arg1705z00_2320 =
														CDR(((obj_t) BgL_obindingsz00_2307));
													{
														obj_t BgL_obindingsz00_2853;

														BgL_obindingsz00_2853 = BgL_arg1705z00_2320;
														BgL_obindingsz00_2307 = BgL_obindingsz00_2853;
														goto BgL_loopz00_2306;
													}
												}
											}
										else
											{	/* Reduce/copy.scm 310 */
												obj_t BgL_arg1708z00_2321;
												obj_t BgL_arg1709z00_2322;

												BgL_arg1708z00_2321 =
													CDR(((obj_t) BgL_obindingsz00_2307));
												BgL_arg1709z00_2322 =
													MAKE_YOUNG_PAIR(BgL_bindingz00_2310,
													BgL_nbindingsz00_2308);
												{
													obj_t BgL_nbindingsz00_2858;
													obj_t BgL_obindingsz00_2857;

													BgL_obindingsz00_2857 = BgL_arg1708z00_2321;
													BgL_nbindingsz00_2858 = BgL_arg1709z00_2322;
													BgL_nbindingsz00_2308 = BgL_nbindingsz00_2858;
													BgL_obindingsz00_2307 = BgL_obindingsz00_2857;
													goto BgL_loopz00_2306;
												}
											}
									}
								}
							}
						}
					}
			}
		}

	}



/* &copyable?-fail1334 */
	obj_t BGl_z62copyablezf3zd2fail1334z43zzreduce_copyz00(obj_t BgL_envz00_2185,
		obj_t BgL_nodez00_2186, obj_t BgL_vz00_2187)
	{
		{	/* Reduce/copy.scm 255 */
			{	/* Reduce/copy.scm 257 */
				obj_t BgL__andtest_1126z00_2324;

				{	/* Reduce/copy.scm 257 */
					BgL_nodez00_bglt BgL_arg1646z00_2325;

					BgL_arg1646z00_2325 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2186)))->BgL_procz00);
					BgL__andtest_1126z00_2324 =
						BGl_copyablezf3zf3zzreduce_copyz00(
						((obj_t) BgL_arg1646z00_2325), BgL_vz00_2187);
				}
				if (CBOOL(BgL__andtest_1126z00_2324))
					{	/* Reduce/copy.scm 257 */
						obj_t BgL__andtest_1127z00_2326;

						{	/* Reduce/copy.scm 257 */
							BgL_nodez00_bglt BgL_arg1642z00_2327;

							BgL_arg1642z00_2327 =
								(((BgL_failz00_bglt) COBJECT(
										((BgL_failz00_bglt) BgL_nodez00_2186)))->BgL_msgz00);
							BgL__andtest_1127z00_2326 =
								BGl_copyablezf3zf3zzreduce_copyz00(
								((obj_t) BgL_arg1642z00_2327), BgL_vz00_2187);
						}
						if (CBOOL(BgL__andtest_1127z00_2326))
							{	/* Reduce/copy.scm 257 */
								BgL_nodez00_bglt BgL_arg1630z00_2328;

								BgL_arg1630z00_2328 =
									(((BgL_failz00_bglt) COBJECT(
											((BgL_failz00_bglt) BgL_nodez00_2186)))->BgL_objz00);
								return
									BGl_copyablezf3zf3zzreduce_copyz00(
									((obj_t) BgL_arg1630z00_2328), BgL_vz00_2187);
							}
						else
							{	/* Reduce/copy.scm 257 */
								return BFALSE;
							}
					}
				else
					{	/* Reduce/copy.scm 257 */
						return BFALSE;
					}
			}
		}

	}



/* &copyable?-conditiona1332 */
	obj_t BGl_z62copyablezf3zd2conditiona1332z43zzreduce_copyz00(obj_t
		BgL_envz00_2188, obj_t BgL_nodez00_2189, obj_t BgL_vz00_2190)
	{
		{	/* Reduce/copy.scm 247 */
			if (
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_vz00_2190)))->BgL_occurrencez00) <=
					1L))
				{	/* Reduce/copy.scm 250 */
					obj_t BgL__andtest_1123z00_2330;

					{	/* Reduce/copy.scm 250 */
						BgL_nodez00_bglt BgL_arg1627z00_2331;

						BgL_arg1627z00_2331 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_2189)))->BgL_testz00);
						BgL__andtest_1123z00_2330 =
							BGl_copyablezf3zf3zzreduce_copyz00(
							((obj_t) BgL_arg1627z00_2331), BgL_vz00_2190);
					}
					if (CBOOL(BgL__andtest_1123z00_2330))
						{	/* Reduce/copy.scm 250 */
							obj_t BgL__andtest_1124z00_2332;

							{	/* Reduce/copy.scm 250 */
								BgL_nodez00_bglt BgL_arg1626z00_2333;

								BgL_arg1626z00_2333 =
									(((BgL_conditionalz00_bglt) COBJECT(
											((BgL_conditionalz00_bglt) BgL_nodez00_2189)))->
									BgL_truez00);
								BgL__andtest_1124z00_2332 =
									BGl_copyablezf3zf3zzreduce_copyz00(((obj_t)
										BgL_arg1626z00_2333), BgL_vz00_2190);
							}
							if (CBOOL(BgL__andtest_1124z00_2332))
								{	/* Reduce/copy.scm 250 */
									BgL_nodez00_bglt BgL_arg1625z00_2334;

									BgL_arg1625z00_2334 =
										(((BgL_conditionalz00_bglt) COBJECT(
												((BgL_conditionalz00_bglt) BgL_nodez00_2189)))->
										BgL_falsez00);
									return BGl_copyablezf3zf3zzreduce_copyz00(((obj_t)
											BgL_arg1625z00_2334), BgL_vz00_2190);
								}
							else
								{	/* Reduce/copy.scm 250 */
									return BFALSE;
								}
						}
					else
						{	/* Reduce/copy.scm 250 */
							return BFALSE;
						}
				}
			else
				{	/* Reduce/copy.scm 249 */
					return BFALSE;
				}
		}

	}



/* &copyable?-instanceof1330 */
	obj_t BGl_z62copyablezf3zd2instanceof1330z43zzreduce_copyz00(obj_t
		BgL_envz00_2191, obj_t BgL_nodez00_2192, obj_t BgL_vz00_2193)
	{
		{	/* Reduce/copy.scm 240 */
			{	/* Reduce/copy.scm 241 */
				bool_t BgL_tmpz00_2897;

				{	/* Reduce/copy.scm 242 */
					obj_t BgL_g1281z00_2336;

					BgL_g1281z00_2336 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_instanceofz00_bglt) BgL_nodez00_2192))))->
						BgL_exprza2za2);
					{
						obj_t BgL_l1279z00_2338;

						BgL_l1279z00_2338 = BgL_g1281z00_2336;
					BgL_zc3z04anonymousza31610ze3z87_2337:
						if (NULLP(BgL_l1279z00_2338))
							{	/* Reduce/copy.scm 242 */
								BgL_tmpz00_2897 = ((bool_t) 1);
							}
						else
							{	/* Reduce/copy.scm 242 */
								obj_t BgL_nvz00_2339;

								{	/* Reduce/copy.scm 242 */
									obj_t BgL_nz00_2340;

									BgL_nz00_2340 = CAR(((obj_t) BgL_l1279z00_2338));
									BgL_nvz00_2339 =
										BGl_copyablezf3zf3zzreduce_copyz00(BgL_nz00_2340,
										BgL_vz00_2193);
								}
								if (CBOOL(BgL_nvz00_2339))
									{	/* Reduce/copy.scm 242 */
										obj_t BgL_arg1613z00_2341;

										BgL_arg1613z00_2341 = CDR(((obj_t) BgL_l1279z00_2338));
										{
											obj_t BgL_l1279z00_2910;

											BgL_l1279z00_2910 = BgL_arg1613z00_2341;
											BgL_l1279z00_2338 = BgL_l1279z00_2910;
											goto BgL_zc3z04anonymousza31610ze3z87_2337;
										}
									}
								else
									{	/* Reduce/copy.scm 242 */
										BgL_tmpz00_2897 = ((bool_t) 0);
									}
							}
					}
				}
				return BBOOL(BgL_tmpz00_2897);
			}
		}

	}



/* &copyable?-cast-null1328 */
	obj_t BGl_z62copyablezf3zd2castzd2null1328z91zzreduce_copyz00(obj_t
		BgL_envz00_2194, obj_t BgL_nodez00_2195, obj_t BgL_vz00_2196)
	{
		{	/* Reduce/copy.scm 233 */
			{	/* Reduce/copy.scm 234 */
				bool_t BgL_tmpz00_2912;

				{	/* Reduce/copy.scm 235 */
					obj_t BgL_g1277z00_2343;

					BgL_g1277z00_2343 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_castzd2nullzd2_bglt) BgL_nodez00_2195))))->
						BgL_exprza2za2);
					{
						obj_t BgL_l1274z00_2345;

						BgL_l1274z00_2345 = BgL_g1277z00_2343;
					BgL_zc3z04anonymousza31606ze3z87_2344:
						if (NULLP(BgL_l1274z00_2345))
							{	/* Reduce/copy.scm 235 */
								BgL_tmpz00_2912 = ((bool_t) 1);
							}
						else
							{	/* Reduce/copy.scm 235 */
								obj_t BgL_nvz00_2346;

								{	/* Reduce/copy.scm 235 */
									obj_t BgL_nz00_2347;

									BgL_nz00_2347 = CAR(((obj_t) BgL_l1274z00_2345));
									BgL_nvz00_2346 =
										BGl_copyablezf3zf3zzreduce_copyz00(BgL_nz00_2347,
										BgL_vz00_2196);
								}
								if (CBOOL(BgL_nvz00_2346))
									{	/* Reduce/copy.scm 235 */
										obj_t BgL_arg1609z00_2348;

										BgL_arg1609z00_2348 = CDR(((obj_t) BgL_l1274z00_2345));
										{
											obj_t BgL_l1274z00_2925;

											BgL_l1274z00_2925 = BgL_arg1609z00_2348;
											BgL_l1274z00_2345 = BgL_l1274z00_2925;
											goto BgL_zc3z04anonymousza31606ze3z87_2344;
										}
									}
								else
									{	/* Reduce/copy.scm 235 */
										BgL_tmpz00_2912 = ((bool_t) 0);
									}
							}
					}
				}
				return BBOOL(BgL_tmpz00_2912);
			}
		}

	}



/* &copyable?-cast1326 */
	obj_t BGl_z62copyablezf3zd2cast1326z43zzreduce_copyz00(obj_t BgL_envz00_2197,
		obj_t BgL_nodez00_2198, obj_t BgL_vz00_2199)
	{
		{	/* Reduce/copy.scm 226 */
			{	/* Reduce/copy.scm 228 */
				BgL_nodez00_bglt BgL_arg1605z00_2350;

				BgL_arg1605z00_2350 =
					(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2198)))->BgL_argz00);
				return
					BGl_copyablezf3zf3zzreduce_copyz00(
					((obj_t) BgL_arg1605z00_2350), BgL_vz00_2199);
			}
		}

	}



/* &copyable?-vlength1324 */
	obj_t BGl_z62copyablezf3zd2vlength1324z43zzreduce_copyz00(obj_t
		BgL_envz00_2200, obj_t BgL_nodez00_2201, obj_t BgL_vz00_2202)
	{
		{	/* Reduce/copy.scm 219 */
			{	/* Reduce/copy.scm 220 */
				bool_t BgL_tmpz00_2931;

				{	/* Reduce/copy.scm 221 */
					obj_t BgL_g1272z00_2352;

					BgL_g1272z00_2352 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_vlengthz00_bglt) BgL_nodez00_2201))))->BgL_exprza2za2);
					{
						obj_t BgL_l1270z00_2354;

						BgL_l1270z00_2354 = BgL_g1272z00_2352;
					BgL_zc3z04anonymousza31596ze3z87_2353:
						if (NULLP(BgL_l1270z00_2354))
							{	/* Reduce/copy.scm 221 */
								BgL_tmpz00_2931 = ((bool_t) 1);
							}
						else
							{	/* Reduce/copy.scm 221 */
								obj_t BgL_nvz00_2355;

								{	/* Reduce/copy.scm 221 */
									obj_t BgL_nz00_2356;

									BgL_nz00_2356 = CAR(((obj_t) BgL_l1270z00_2354));
									BgL_nvz00_2355 =
										BGl_copyablezf3zf3zzreduce_copyz00(BgL_nz00_2356,
										BgL_vz00_2202);
								}
								if (CBOOL(BgL_nvz00_2355))
									{	/* Reduce/copy.scm 221 */
										obj_t BgL_arg1602z00_2357;

										BgL_arg1602z00_2357 = CDR(((obj_t) BgL_l1270z00_2354));
										{
											obj_t BgL_l1270z00_2944;

											BgL_l1270z00_2944 = BgL_arg1602z00_2357;
											BgL_l1270z00_2354 = BgL_l1270z00_2944;
											goto BgL_zc3z04anonymousza31596ze3z87_2353;
										}
									}
								else
									{	/* Reduce/copy.scm 221 */
										BgL_tmpz00_2931 = ((bool_t) 0);
									}
							}
					}
				}
				return BBOOL(BgL_tmpz00_2931);
			}
		}

	}



/* &copyable?-sequence1322 */
	obj_t BGl_z62copyablezf3zd2sequence1322z43zzreduce_copyz00(obj_t
		BgL_envz00_2203, obj_t BgL_nodez00_2204, obj_t BgL_vz00_2205)
	{
		{	/* Reduce/copy.scm 212 */
			{	/* Reduce/copy.scm 213 */
				bool_t BgL_tmpz00_2946;

				{	/* Reduce/copy.scm 214 */
					obj_t BgL_g1268z00_2359;

					BgL_g1268z00_2359 =
						(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_2204)))->BgL_nodesz00);
					{
						obj_t BgL_l1266z00_2361;

						BgL_l1266z00_2361 = BgL_g1268z00_2359;
					BgL_zc3z04anonymousza31592ze3z87_2360:
						if (NULLP(BgL_l1266z00_2361))
							{	/* Reduce/copy.scm 214 */
								BgL_tmpz00_2946 = ((bool_t) 1);
							}
						else
							{	/* Reduce/copy.scm 214 */
								obj_t BgL_nvz00_2362;

								{	/* Reduce/copy.scm 214 */
									obj_t BgL_nz00_2363;

									BgL_nz00_2363 = CAR(((obj_t) BgL_l1266z00_2361));
									BgL_nvz00_2362 =
										BGl_copyablezf3zf3zzreduce_copyz00(BgL_nz00_2363,
										BgL_vz00_2205);
								}
								if (CBOOL(BgL_nvz00_2362))
									{	/* Reduce/copy.scm 214 */
										obj_t BgL_arg1595z00_2364;

										BgL_arg1595z00_2364 = CDR(((obj_t) BgL_l1266z00_2361));
										{
											obj_t BgL_l1266z00_2958;

											BgL_l1266z00_2958 = BgL_arg1595z00_2364;
											BgL_l1266z00_2361 = BgL_l1266z00_2958;
											goto BgL_zc3z04anonymousza31592ze3z87_2360;
										}
									}
								else
									{	/* Reduce/copy.scm 214 */
										BgL_tmpz00_2946 = ((bool_t) 0);
									}
							}
					}
				}
				return BBOOL(BgL_tmpz00_2946);
			}
		}

	}



/* &copyable?-var1320 */
	obj_t BGl_z62copyablezf3zd2var1320z43zzreduce_copyz00(obj_t BgL_envz00_2206,
		obj_t BgL_nodez00_2207, obj_t BgL_vz00_2208)
	{
		{	/* Reduce/copy.scm 206 */
			return
				BBOOL(
				((((BgL_variablez00_bglt) COBJECT(
								(((BgL_varz00_bglt) COBJECT(
											((BgL_varz00_bglt) BgL_nodez00_2207)))->
									BgL_variablez00)))->BgL_accessz00) == CNST_TABLE_REF(2)));
		}

	}



/* &copyable?-atom1318 */
	obj_t BGl_z62copyablezf3zd2atom1318z43zzreduce_copyz00(obj_t BgL_envz00_2209,
		obj_t BgL_nodez00_2210, obj_t BgL_vz00_2211)
	{
		{	/* Reduce/copy.scm 200 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &node-copy!-let-fun1314 */
	BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2letzd2fun1314za2zzreduce_copyz00(obj_t
		BgL_envz00_2212, obj_t BgL_nodez00_2213)
	{
		{	/* Reduce/copy.scm 180 */
			{	/* Reduce/copy.scm 182 */
				obj_t BgL_g1265z00_2368;

				BgL_g1265z00_2368 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2213)))->BgL_localsz00);
				{
					obj_t BgL_l1263z00_2370;

					BgL_l1263z00_2370 = BgL_g1265z00_2368;
				BgL_zc3z04anonymousza31572ze3z87_2369:
					if (PAIRP(BgL_l1263z00_2370))
						{	/* Reduce/copy.scm 182 */
							{	/* Reduce/copy.scm 183 */
								obj_t BgL_localz00_2371;

								BgL_localz00_2371 = CAR(BgL_l1263z00_2370);
								{	/* Reduce/copy.scm 183 */
									BgL_valuez00_bglt BgL_funz00_2372;

									BgL_funz00_2372 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2371))))->
										BgL_valuez00);
									{	/* Reduce/copy.scm 184 */
										BgL_nodez00_bglt BgL_arg1575z00_2373;

										{	/* Reduce/copy.scm 184 */
											obj_t BgL_arg1576z00_2374;

											BgL_arg1576z00_2374 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2372)))->
												BgL_bodyz00);
											BgL_arg1575z00_2373 =
												BGl_nodezd2copyz12zc0zzreduce_copyz00((
													(BgL_nodez00_bglt) BgL_arg1576z00_2374));
										}
										((((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2372)))->
												BgL_bodyz00) =
											((obj_t) ((obj_t) BgL_arg1575z00_2373)), BUNSPEC);
									}
								}
							}
							{
								obj_t BgL_l1263z00_2982;

								BgL_l1263z00_2982 = CDR(BgL_l1263z00_2370);
								BgL_l1263z00_2370 = BgL_l1263z00_2982;
								goto BgL_zc3z04anonymousza31572ze3z87_2369;
							}
						}
					else
						{	/* Reduce/copy.scm 182 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2984;

				{	/* Reduce/copy.scm 186 */
					BgL_nodez00_bglt BgL_arg1585z00_2375;

					BgL_arg1585z00_2375 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2213)))->BgL_bodyz00);
					BgL_auxz00_2984 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1585z00_2375);
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2213)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2984), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2213));
		}

	}



/* &node-copy!-switch1312 */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12zd2switch1312z70zzreduce_copyz00(obj_t
		BgL_envz00_2214, obj_t BgL_nodez00_2215)
	{
		{	/* Reduce/copy.scm 169 */
			{
				BgL_nodez00_bglt BgL_auxz00_2992;

				{	/* Reduce/copy.scm 171 */
					BgL_nodez00_bglt BgL_arg1561z00_2377;

					BgL_arg1561z00_2377 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2215)))->BgL_testz00);
					BgL_auxz00_2992 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1561z00_2377);
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2215)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2992), BUNSPEC);
			}
			{	/* Reduce/copy.scm 172 */
				obj_t BgL_g1262z00_2378;

				BgL_g1262z00_2378 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2215)))->BgL_clausesz00);
				{
					obj_t BgL_l1260z00_2380;

					BgL_l1260z00_2380 = BgL_g1262z00_2378;
				BgL_zc3z04anonymousza31562ze3z87_2379:
					if (PAIRP(BgL_l1260z00_2380))
						{	/* Reduce/copy.scm 172 */
							{	/* Reduce/copy.scm 173 */
								obj_t BgL_clausez00_2381;

								BgL_clausez00_2381 = CAR(BgL_l1260z00_2380);
								{	/* Reduce/copy.scm 173 */
									BgL_nodez00_bglt BgL_arg1564z00_2382;

									{	/* Reduce/copy.scm 173 */
										obj_t BgL_arg1565z00_2383;

										BgL_arg1565z00_2383 = CDR(((obj_t) BgL_clausez00_2381));
										BgL_arg1564z00_2382 =
											BGl_nodezd2copyz12zc0zzreduce_copyz00(
											((BgL_nodez00_bglt) BgL_arg1565z00_2383));
									}
									{	/* Reduce/copy.scm 173 */
										obj_t BgL_auxz00_3009;
										obj_t BgL_tmpz00_3007;

										BgL_auxz00_3009 = ((obj_t) BgL_arg1564z00_2382);
										BgL_tmpz00_3007 = ((obj_t) BgL_clausez00_2381);
										SET_CDR(BgL_tmpz00_3007, BgL_auxz00_3009);
									}
								}
							}
							{
								obj_t BgL_l1260z00_3012;

								BgL_l1260z00_3012 = CDR(BgL_l1260z00_2380);
								BgL_l1260z00_2380 = BgL_l1260z00_3012;
								goto BgL_zc3z04anonymousza31562ze3z87_2379;
							}
						}
					else
						{	/* Reduce/copy.scm 172 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_2215));
		}

	}



/* &node-copy!-fail1310 */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12zd2fail1310z70zzreduce_copyz00(obj_t
		BgL_envz00_2216, obj_t BgL_nodez00_2217)
	{
		{	/* Reduce/copy.scm 159 */
			{
				BgL_nodez00_bglt BgL_auxz00_3016;

				{	/* Reduce/copy.scm 161 */
					BgL_nodez00_bglt BgL_arg1552z00_2385;

					BgL_arg1552z00_2385 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2217)))->BgL_procz00);
					BgL_auxz00_3016 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1552z00_2385);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2217)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3016), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3022;

				{	/* Reduce/copy.scm 162 */
					BgL_nodez00_bglt BgL_arg1553z00_2386;

					BgL_arg1553z00_2386 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2217)))->BgL_msgz00);
					BgL_auxz00_3022 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1553z00_2386);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2217)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3022), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3028;

				{	/* Reduce/copy.scm 163 */
					BgL_nodez00_bglt BgL_arg1559z00_2387;

					BgL_arg1559z00_2387 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2217)))->BgL_objz00);
					BgL_auxz00_3028 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1559z00_2387);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2217)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3028), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_2217));
		}

	}



/* &node-copy!-condition1308 */
	BgL_nodez00_bglt
		BGl_z62nodezd2copyz12zd2condition1308z70zzreduce_copyz00(obj_t
		BgL_envz00_2218, obj_t BgL_nodez00_2219)
	{
		{	/* Reduce/copy.scm 149 */
			{
				BgL_nodez00_bglt BgL_auxz00_3036;

				{	/* Reduce/copy.scm 151 */
					BgL_nodez00_bglt BgL_arg1540z00_2389;

					BgL_arg1540z00_2389 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2219)))->BgL_testz00);
					BgL_auxz00_3036 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1540z00_2389);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2219)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3036), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3042;

				{	/* Reduce/copy.scm 152 */
					BgL_nodez00_bglt BgL_arg1544z00_2390;

					BgL_arg1544z00_2390 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2219)))->BgL_truez00);
					BgL_auxz00_3042 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1544z00_2390);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2219)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3042), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3048;

				{	/* Reduce/copy.scm 153 */
					BgL_nodez00_bglt BgL_arg1546z00_2391;

					BgL_arg1546z00_2391 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2219)))->BgL_falsez00);
					BgL_auxz00_3048 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1546z00_2391);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2219)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3048), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_2219));
		}

	}



/* &node-copy!-setq1306 */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12zd2setq1306z70zzreduce_copyz00(obj_t
		BgL_envz00_2220, obj_t BgL_nodez00_2221)
	{
		{	/* Reduce/copy.scm 140 */
			{
				BgL_nodez00_bglt BgL_auxz00_3056;

				{	/* Reduce/copy.scm 142 */
					BgL_nodez00_bglt BgL_arg1516z00_2393;

					BgL_arg1516z00_2393 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2221)))->BgL_valuez00);
					BgL_auxz00_3056 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1516z00_2393);
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2221)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3056), BUNSPEC);
			}
			{
				BgL_varz00_bglt BgL_auxz00_3062;

				{	/* Reduce/copy.scm 143 */
					BgL_varz00_bglt BgL_arg1535z00_2394;

					BgL_arg1535z00_2394 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2221)))->BgL_varz00);
					BgL_auxz00_3062 =
						((BgL_varz00_bglt)
						BGl_nodezd2copyz12zc0zzreduce_copyz00(
							((BgL_nodez00_bglt) BgL_arg1535z00_2394)));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2221)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3062), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_2221));
		}

	}



/* &node-copy!-cast1304 */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12zd2cast1304z70zzreduce_copyz00(obj_t
		BgL_envz00_2222, obj_t BgL_nodez00_2223)
	{
		{	/* Reduce/copy.scm 132 */
			{
				BgL_nodez00_bglt BgL_auxz00_3072;

				{	/* Reduce/copy.scm 134 */
					BgL_nodez00_bglt BgL_arg1514z00_2396;

					BgL_arg1514z00_2396 =
						(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_2223)))->BgL_argz00);
					BgL_auxz00_3072 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1514z00_2396);
				}
				((((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_2223)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3072), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_2223));
		}

	}



/* &node-copy!-extern1302 */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12zd2extern1302z70zzreduce_copyz00(obj_t
		BgL_envz00_2224, obj_t BgL_nodez00_2225)
	{
		{	/* Reduce/copy.scm 124 */
			BGl_nodezd2copyza2z12z62zzreduce_copyz00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2225)))->BgL_exprza2za2));
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2225));
		}

	}



/* &node-copy!-funcall1300 */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12zd2funcall1300z70zzreduce_copyz00(obj_t
		BgL_envz00_2226, obj_t BgL_nodez00_2227)
	{
		{	/* Reduce/copy.scm 115 */
			{
				BgL_nodez00_bglt BgL_auxz00_3085;

				{	/* Reduce/copy.scm 117 */
					BgL_nodez00_bglt BgL_arg1502z00_2399;

					BgL_arg1502z00_2399 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2227)))->BgL_funz00);
					BgL_auxz00_3085 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1502z00_2399);
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2227)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3085), BUNSPEC);
			}
			BGl_nodezd2copyza2z12z62zzreduce_copyz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2227)))->BgL_argsz00));
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_2227));
		}

	}



/* &node-copy!-app-ly1298 */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12zd2appzd2ly1298za2zzreduce_copyz00(obj_t
		BgL_envz00_2228, obj_t BgL_nodez00_2229)
	{
		{	/* Reduce/copy.scm 106 */
			{
				BgL_nodez00_bglt BgL_auxz00_3096;

				{	/* Reduce/copy.scm 108 */
					BgL_nodez00_bglt BgL_arg1485z00_2401;

					BgL_arg1485z00_2401 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2229)))->BgL_funz00);
					BgL_auxz00_3096 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1485z00_2401);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2229)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3096), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3102;

				{	/* Reduce/copy.scm 109 */
					BgL_nodez00_bglt BgL_arg1489z00_2402;

					BgL_arg1489z00_2402 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2229)))->BgL_argz00);
					BgL_auxz00_3102 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1489z00_2402);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2229)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3102), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2229));
		}

	}



/* &node-copy!-sync1296 */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12zd2sync1296z70zzreduce_copyz00(obj_t
		BgL_envz00_2230, obj_t BgL_nodez00_2231)
	{
		{	/* Reduce/copy.scm 96 */
			{
				BgL_nodez00_bglt BgL_auxz00_3110;

				{	/* Reduce/copy.scm 98 */
					BgL_nodez00_bglt BgL_arg1454z00_2404;

					BgL_arg1454z00_2404 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2231)))->BgL_mutexz00);
					BgL_auxz00_3110 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1454z00_2404);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2231)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3110), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3116;

				{	/* Reduce/copy.scm 99 */
					BgL_nodez00_bglt BgL_arg1472z00_2405;

					BgL_arg1472z00_2405 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2231)))->BgL_prelockz00);
					BgL_auxz00_3116 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1472z00_2405);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2231)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3116), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3122;

				{	/* Reduce/copy.scm 100 */
					BgL_nodez00_bglt BgL_arg1473z00_2406;

					BgL_arg1473z00_2406 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2231)))->BgL_bodyz00);
					BgL_auxz00_3122 =
						BGl_nodezd2copyz12zc0zzreduce_copyz00(BgL_arg1473z00_2406);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2231)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3122), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2231));
		}

	}



/* &node-copy!-sequence1294 */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12zd2sequence1294z70zzreduce_copyz00(obj_t
		BgL_envz00_2232, obj_t BgL_nodez00_2233)
	{
		{	/* Reduce/copy.scm 88 */
			BGl_nodezd2copyza2z12z62zzreduce_copyz00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2233)))->BgL_nodesz00));
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_2233));
		}

	}



/* &node-copy!-closure1292 */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12zd2closure1292z70zzreduce_copyz00(obj_t
		BgL_envz00_2234, obj_t BgL_nodez00_2235)
	{
		{	/* Reduce/copy.scm 82 */
			return ((BgL_nodez00_bglt) ((BgL_closurez00_bglt) BgL_nodez00_2235));
		}

	}



/* &node-copy!-var1290 */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12zd2var1290z70zzreduce_copyz00(obj_t
		BgL_envz00_2236, obj_t BgL_nodez00_2237)
	{
		{	/* Reduce/copy.scm 72 */
			{	/* Reduce/copy.scm 73 */
				BgL_variablez00_bglt BgL_vz00_2410;

				BgL_vz00_2410 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt) BgL_nodez00_2237)))->BgL_variablez00);
				{	/* Reduce/copy.scm 74 */
					obj_t BgL_falphaz00_2411;

					BgL_falphaz00_2411 =
						(((BgL_variablez00_bglt) COBJECT(BgL_vz00_2410))->
						BgL_fastzd2alphazd2);
					if ((BgL_falphaz00_2411 == BUNSPEC))
						{	/* Reduce/copy.scm 75 */
							return ((BgL_nodez00_bglt) ((BgL_varz00_bglt) BgL_nodez00_2237));
						}
					else
						{	/* Reduce/copy.scm 76 */
							obj_t BgL_arg1437z00_2412;
							BgL_nodez00_bglt BgL_arg1448z00_2413;

							BgL_arg1437z00_2412 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_varz00_bglt) BgL_nodez00_2237))))->BgL_locz00);
							BgL_arg1448z00_2413 =
								BGl_nodezd2copyz12zc0zzreduce_copyz00(
								((BgL_nodez00_bglt) BgL_falphaz00_2411));
							return
								BGl_alphatiza7eza7zzast_alphatiza7eza7(BNIL, BNIL,
								BgL_arg1437z00_2412, BgL_arg1448z00_2413);
						}
				}
			}
		}

	}



/* &node-copy!-kwote1288 */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12zd2kwote1288z70zzreduce_copyz00(obj_t
		BgL_envz00_2238, obj_t BgL_nodez00_2239)
	{
		{	/* Reduce/copy.scm 66 */
			return ((BgL_nodez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_2239));
		}

	}



/* &node-copy!-atom1286 */
	BgL_nodez00_bglt BGl_z62nodezd2copyz12zd2atom1286z70zzreduce_copyz00(obj_t
		BgL_envz00_2240, obj_t BgL_nodez00_2241)
	{
		{	/* Reduce/copy.scm 60 */
			return ((BgL_nodez00_bglt) ((BgL_atomz00_bglt) BgL_nodez00_2241));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzreduce_copyz00(void)
	{
		{	/* Reduce/copy.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1916z00zzreduce_copyz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1916z00zzreduce_copyz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1916z00zzreduce_copyz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1916z00zzreduce_copyz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1916z00zzreduce_copyz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1916z00zzreduce_copyz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1916z00zzreduce_copyz00));
			BGl_modulezd2initializa7ationz75zztype_miscz00(49975086L,
				BSTRING_TO_STRING(BGl_string1916z00zzreduce_copyz00));
			BGl_modulezd2initializa7ationz75zzcoerce_coercez00(361167184L,
				BSTRING_TO_STRING(BGl_string1916z00zzreduce_copyz00));
			BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string1916z00zzreduce_copyz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1916z00zzreduce_copyz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1916z00zzreduce_copyz00));
			BGl_modulezd2initializa7ationz75zzast_alphatiza7eza7(53595773L,
				BSTRING_TO_STRING(BGl_string1916z00zzreduce_copyz00));
			return
				BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string1916z00zzreduce_copyz00));
		}

	}

#ifdef __cplusplus
}
#endif
