/*===========================================================================*/
/*   (Reduce/cse.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Reduce/cse.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_REDUCE_CSE_TYPE_DEFINITIONS
#define BGL_REDUCE_CSE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_REDUCE_CSE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62nodezd2csez12zd2retblock1354z70zzreduce_csez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_nodezd2csez12zc0zzreduce_csez00(BgL_nodez00_bglt,
		obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_returnz00zzast_nodez00;
	static obj_t BGl_z62nodezd2csez12zd2closure1326z70zzreduce_csez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2csez12zd2letzd2var1366za2zzreduce_csez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzreduce_csez00 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62nodezd2csez12zd2boxzd2ref1362za2zzreduce_csez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static long BGl_za2csezd2removedza2zd2zzreduce_csez00 = 0L;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzreduce_csez00(void);
	static obj_t BGl_z62nodezd2csez12zd2kwote1322z70zzreduce_csez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzreduce_csez00(void);
	BGL_EXPORTED_DECL obj_t BGl_reducezd2csez12zc0zzreduce_csez00(obj_t);
	static obj_t BGl_objectzd2initzd2zzreduce_csez00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t
		BGl_z62nodezd2csez12zd2jumpzd2exzd2it1352z70zzreduce_csez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	static obj_t BGl_z62nodezd2csez12zd2appzd2ly1332za2zzreduce_csez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzreduce_csez00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzreduce_csez00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62nodezd2csez12zd2atom1320z70zzreduce_csez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2csez12zd2return1356z70zzreduce_csez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2csez12zd2extern1336z70zzreduce_csez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62reducezd2csez12za2zzreduce_csez00(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2csez12zd2switch1346z70zzreduce_csez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2csez12zd2makezd2box1358za2zzreduce_csez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62nodezd2csez12zd2boxzd2setz121360zb0zzreduce_csez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzreduce_csez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzreduce_samez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_z62nodezd2csez12zd2letzd2fun1348za2zzreduce_csez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2csez121317za2zzreduce_csez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static obj_t BGl_z62nodezd2csez12zd2setzd2exzd2it1350z70zzreduce_csez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62nodezd2csez12zd2var1324z70zzreduce_csez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzreduce_csez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzreduce_csez00(void);
	static obj_t BGl_z62nodezd2csez12zd2sequence1328z70zzreduce_csez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzreduce_csez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzreduce_csez00(void);
	extern bool_t BGl_samezd2nodezf3z21zzreduce_samez00(BgL_nodez00_bglt,
		BgL_nodez00_bglt, obj_t);
	extern bool_t BGl_sidezd2effectzf3z21zzeffect_effectz00(BgL_nodez00_bglt);
	extern obj_t BGl_retblockz00zzast_nodez00;
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2csez12zd2cast1338z70zzreduce_csez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_variablez00zzast_varz00;
	extern obj_t BGl_switchz00zzast_nodez00;
	static obj_t BGl_z62nodezd2csez12zd2setq1340z70zzreduce_csez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62nodezd2csez12za2zzreduce_csez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62nodezd2csez12zd2sync1330z70zzreduce_csez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2csez12zd2fail1344z70zzreduce_csez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t BGl_z62nodezd2csez12zd2app1364z70zzreduce_csez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2csez12zd2funcall1334z70zzreduce_csez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t BGl_nodezd2cseza2z12z62zzreduce_csez00(obj_t, obj_t);
	static obj_t BGl_findzd2stackzd2zzreduce_csez00(BgL_nodez00_bglt, obj_t);
	static obj_t BGl_z62nodezd2csez12zd2conditiona1342z70zzreduce_csez00(obj_t,
		obj_t, obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_reducezd2csez12zd2envz12zzreduce_csez00,
		BgL_bgl_za762reduceza7d2cseza71885za7,
		BGl_z62reducezd2csez12za2zzreduce_csez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1852z00zzreduce_csez00,
		BgL_bgl_string1852za700za7za7r1886za7, "      cse                    ", 29);
	      DEFINE_STRING(BGl_string1853z00zzreduce_csez00,
		BgL_bgl_string1853za700za7za7r1887za7, "(removed: ", 10);
	      DEFINE_STRING(BGl_string1855z00zzreduce_csez00,
		BgL_bgl_string1855za700za7za7r1888za7, "node-cse!1317", 13);
	      DEFINE_STRING(BGl_string1856z00zzreduce_csez00,
		BgL_bgl_string1856za700za7za7r1889za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1858z00zzreduce_csez00,
		BgL_bgl_string1858za700za7za7r1890za7, "node-cse!", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1854z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121891za7,
		BGl_z62nodezd2csez121317za2zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1857z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121892za7,
		BGl_z62nodezd2csez12zd2atom1320z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1859z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121893za7,
		BGl_z62nodezd2csez12zd2kwote1322z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1860z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121894za7,
		BGl_z62nodezd2csez12zd2var1324z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1861z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121895za7,
		BGl_z62nodezd2csez12zd2closure1326z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1862z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121896za7,
		BGl_z62nodezd2csez12zd2sequence1328z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1863z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121897za7,
		BGl_z62nodezd2csez12zd2sync1330z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1864z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121898za7,
		BGl_z62nodezd2csez12zd2appzd2ly1332za2zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1865z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121899za7,
		BGl_z62nodezd2csez12zd2funcall1334z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1866z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121900za7,
		BGl_z62nodezd2csez12zd2extern1336z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1867z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121901za7,
		BGl_z62nodezd2csez12zd2cast1338z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1868z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121902za7,
		BGl_z62nodezd2csez12zd2setq1340z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1869z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121903za7,
		BGl_z62nodezd2csez12zd2conditiona1342z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1870z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121904za7,
		BGl_z62nodezd2csez12zd2fail1344z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1871z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121905za7,
		BGl_z62nodezd2csez12zd2switch1346z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1872z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121906za7,
		BGl_z62nodezd2csez12zd2letzd2fun1348za2zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1873z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121907za7,
		BGl_z62nodezd2csez12zd2setzd2exzd2it1350z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1874z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121908za7,
		BGl_z62nodezd2csez12zd2jumpzd2exzd2it1352z70zzreduce_csez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1875z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121909za7,
		BGl_z62nodezd2csez12zd2retblock1354z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1882z00zzreduce_csez00,
		BgL_bgl_string1882za700za7za7r1910za7, "reduce_cse", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1876z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121911za7,
		BGl_z62nodezd2csez12zd2return1356z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1883z00zzreduce_csez00,
		BgL_bgl_string1883za700za7za7r1912za7, "call read node-cse!1317 ", 24);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1877z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121913za7,
		BGl_z62nodezd2csez12zd2makezd2box1358za2zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1878z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121914za7,
		BGl_z62nodezd2csez12zd2boxzd2setz121360zb0zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1879z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121915za7,
		BGl_z62nodezd2csez12zd2boxzd2ref1362za2zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1880z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121916za7,
		BGl_z62nodezd2csez12zd2app1364z70zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1881z00zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121917za7,
		BGl_z62nodezd2csez12zd2letzd2var1366za2zzreduce_csez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_nodezd2csez12zd2envz12zzreduce_csez00,
		BgL_bgl_za762nodeza7d2cseza7121918za7,
		BGl_z62nodezd2csez12za2zzreduce_csez00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzreduce_csez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzreduce_csez00(long
		BgL_checksumz00_2706, char *BgL_fromz00_2707)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzreduce_csez00))
				{
					BGl_requirezd2initializa7ationz75zzreduce_csez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzreduce_csez00();
					BGl_libraryzd2moduleszd2initz00zzreduce_csez00();
					BGl_cnstzd2initzd2zzreduce_csez00();
					BGl_importedzd2moduleszd2initz00zzreduce_csez00();
					BGl_genericzd2initzd2zzreduce_csez00();
					BGl_methodzd2initzd2zzreduce_csez00();
					return BGl_toplevelzd2initzd2zzreduce_csez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzreduce_csez00(void)
	{
		{	/* Reduce/cse.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "reduce_cse");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "reduce_cse");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "reduce_cse");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "reduce_cse");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "reduce_cse");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"reduce_cse");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "reduce_cse");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "reduce_cse");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"reduce_cse");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "reduce_cse");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"reduce_cse");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"reduce_cse");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "reduce_cse");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzreduce_csez00(void)
	{
		{	/* Reduce/cse.scm 15 */
			{	/* Reduce/cse.scm 15 */
				obj_t BgL_cportz00_2475;

				{	/* Reduce/cse.scm 15 */
					obj_t BgL_stringz00_2482;

					BgL_stringz00_2482 = BGl_string1883z00zzreduce_csez00;
					{	/* Reduce/cse.scm 15 */
						obj_t BgL_startz00_2483;

						BgL_startz00_2483 = BINT(0L);
						{	/* Reduce/cse.scm 15 */
							obj_t BgL_endz00_2484;

							BgL_endz00_2484 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2482)));
							{	/* Reduce/cse.scm 15 */

								BgL_cportz00_2475 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2482, BgL_startz00_2483, BgL_endz00_2484);
				}}}}
				{
					long BgL_iz00_2476;

					BgL_iz00_2476 = 2L;
				BgL_loopz00_2477:
					if ((BgL_iz00_2476 == -1L))
						{	/* Reduce/cse.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Reduce/cse.scm 15 */
							{	/* Reduce/cse.scm 15 */
								obj_t BgL_arg1884z00_2478;

								{	/* Reduce/cse.scm 15 */

									{	/* Reduce/cse.scm 15 */
										obj_t BgL_locationz00_2480;

										BgL_locationz00_2480 = BBOOL(((bool_t) 0));
										{	/* Reduce/cse.scm 15 */

											BgL_arg1884z00_2478 =
												BGl_readz00zz__readerz00(BgL_cportz00_2475,
												BgL_locationz00_2480);
										}
									}
								}
								{	/* Reduce/cse.scm 15 */
									int BgL_tmpz00_2740;

									BgL_tmpz00_2740 = (int) (BgL_iz00_2476);
									CNST_TABLE_SET(BgL_tmpz00_2740, BgL_arg1884z00_2478);
							}}
							{	/* Reduce/cse.scm 15 */
								int BgL_auxz00_2481;

								BgL_auxz00_2481 = (int) ((BgL_iz00_2476 - 1L));
								{
									long BgL_iz00_2745;

									BgL_iz00_2745 = (long) (BgL_auxz00_2481);
									BgL_iz00_2476 = BgL_iz00_2745;
									goto BgL_loopz00_2477;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzreduce_csez00(void)
	{
		{	/* Reduce/cse.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzreduce_csez00(void)
	{
		{	/* Reduce/cse.scm 15 */
			BGl_za2csezd2removedza2zd2zzreduce_csez00 = 0L;
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzreduce_csez00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1409;

				BgL_headz00_1409 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1410;
					obj_t BgL_tailz00_1411;

					BgL_prevz00_1410 = BgL_headz00_1409;
					BgL_tailz00_1411 = BgL_l1z00_1;
				BgL_loopz00_1412:
					if (PAIRP(BgL_tailz00_1411))
						{
							obj_t BgL_newzd2prevzd2_1414;

							BgL_newzd2prevzd2_1414 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1411), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1410, BgL_newzd2prevzd2_1414);
							{
								obj_t BgL_tailz00_2755;
								obj_t BgL_prevz00_2754;

								BgL_prevz00_2754 = BgL_newzd2prevzd2_1414;
								BgL_tailz00_2755 = CDR(BgL_tailz00_1411);
								BgL_tailz00_1411 = BgL_tailz00_2755;
								BgL_prevz00_1410 = BgL_prevz00_2754;
								goto BgL_loopz00_1412;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1409);
				}
			}
		}

	}



/* reduce-cse! */
	BGL_EXPORTED_DEF obj_t BGl_reducezd2csez12zc0zzreduce_csez00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Reduce/cse.scm 34 */
			{	/* Reduce/cse.scm 35 */
				obj_t BgL_list1372z00_1417;

				BgL_list1372z00_1417 =
					MAKE_YOUNG_PAIR(BGl_string1852z00zzreduce_csez00, BNIL);
				BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1372z00_1417);
			}
			BGl_za2csezd2removedza2zd2zzreduce_csez00 = 0L;
			{
				obj_t BgL_l1257z00_1419;

				BgL_l1257z00_1419 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31373ze3z87_1420:
				if (PAIRP(BgL_l1257z00_1419))
					{	/* Reduce/cse.scm 37 */
						{	/* Reduce/cse.scm 38 */
							obj_t BgL_globalz00_1422;

							BgL_globalz00_1422 = CAR(BgL_l1257z00_1419);
							{	/* Reduce/cse.scm 38 */
								BgL_valuez00_bglt BgL_funz00_1423;

								BgL_funz00_1423 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_1422))))->
									BgL_valuez00);
								{	/* Reduce/cse.scm 38 */
									obj_t BgL_nodez00_1424;

									BgL_nodez00_1424 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_funz00_1423)))->BgL_bodyz00);
									{	/* Reduce/cse.scm 39 */

										{	/* Reduce/cse.scm 40 */
											obj_t BgL_arg1408z00_1425;

											{	/* Reduce/cse.scm 40 */
												obj_t BgL__z00_1426;

												BgL__z00_1426 =
													BGl_nodezd2csez12zc0zzreduce_csez00(
													((BgL_nodez00_bglt) BgL_nodez00_1424), BNIL);
												{	/* Reduce/cse.scm 41 */
													obj_t BgL_nodez00_1427;

													{	/* Reduce/cse.scm 41 */
														obj_t BgL_tmpz00_2009;

														{	/* Reduce/cse.scm 41 */
															int BgL_tmpz00_2771;

															BgL_tmpz00_2771 = (int) (1L);
															BgL_tmpz00_2009 =
																BGL_MVALUES_VAL(BgL_tmpz00_2771);
														}
														{	/* Reduce/cse.scm 41 */
															int BgL_tmpz00_2774;

															BgL_tmpz00_2774 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_2774, BUNSPEC);
														}
														BgL_nodez00_1427 = BgL_tmpz00_2009;
													}
													BgL_arg1408z00_1425 = BgL_nodez00_1427;
											}}
											((((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_funz00_1423)))->
													BgL_bodyz00) =
												((obj_t) BgL_arg1408z00_1425), BUNSPEC);
										} BUNSPEC;
						}}}}
						{
							obj_t BgL_l1257z00_2779;

							BgL_l1257z00_2779 = CDR(BgL_l1257z00_1419);
							BgL_l1257z00_1419 = BgL_l1257z00_2779;
							goto BgL_zc3z04anonymousza31373ze3z87_1420;
						}
					}
				else
					{	/* Reduce/cse.scm 37 */
						((bool_t) 1);
					}
			}
			{	/* Reduce/cse.scm 45 */
				obj_t BgL_list1411z00_1430;

				{	/* Reduce/cse.scm 45 */
					obj_t BgL_arg1421z00_1431;

					{	/* Reduce/cse.scm 45 */
						obj_t BgL_arg1422z00_1432;

						{	/* Reduce/cse.scm 45 */
							obj_t BgL_arg1434z00_1433;

							BgL_arg1434z00_1433 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
							BgL_arg1422z00_1432 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ')')),
								BgL_arg1434z00_1433);
						}
						BgL_arg1421z00_1431 =
							MAKE_YOUNG_PAIR(BINT(BGl_za2csezd2removedza2zd2zzreduce_csez00),
							BgL_arg1422z00_1432);
					}
					BgL_list1411z00_1430 =
						MAKE_YOUNG_PAIR(BGl_string1853z00zzreduce_csez00,
						BgL_arg1421z00_1431);
				}
				BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1411z00_1430);
			}
			return BgL_globalsz00_3;
		}

	}



/* &reduce-cse! */
	obj_t BGl_z62reducezd2csez12za2zzreduce_csez00(obj_t BgL_envz00_2369,
		obj_t BgL_globalsz00_2370)
	{
		{	/* Reduce/cse.scm 34 */
			return BGl_reducezd2csez12zc0zzreduce_csez00(BgL_globalsz00_2370);
		}

	}



/* node-cse*! */
	obj_t BGl_nodezd2cseza2z12z62zzreduce_csez00(obj_t BgL_nodeza2za2_54,
		obj_t BgL_stackz00_55)
	{
		{	/* Reduce/cse.scm 383 */
			{
				obj_t BgL_nodeza2za2_1435;
				bool_t BgL_resetz00_1436;
				obj_t BgL_stackz00_1437;

				BgL_nodeza2za2_1435 = BgL_nodeza2za2_54;
				BgL_resetz00_1436 = ((bool_t) 0);
				BgL_stackz00_1437 = BgL_stackz00_55;
			BgL_zc3z04anonymousza31435ze3z87_1438:
				if (NULLP(BgL_nodeza2za2_1435))
					{	/* Reduce/cse.scm 388 */
						return BBOOL(BgL_resetz00_1436);
					}
				else
					{	/* Reduce/cse.scm 388 */
						if (NULLP(CDR(((obj_t) BgL_nodeza2za2_1435))))
							{	/* Reduce/cse.scm 391 */
								obj_t BgL_resetz72z72_1442;

								{	/* Reduce/cse.scm 392 */
									obj_t BgL_arg1448z00_1445;

									BgL_arg1448z00_1445 = CAR(((obj_t) BgL_nodeza2za2_1435));
									BgL_resetz72z72_1442 =
										BGl_nodezd2csez12zc0zzreduce_csez00(
										((BgL_nodez00_bglt) BgL_arg1448z00_1445),
										BgL_stackz00_1437);
								}
								{	/* Reduce/cse.scm 392 */
									obj_t BgL_nodez00_1443;

									{	/* Reduce/cse.scm 393 */
										obj_t BgL_tmpz00_2014;

										{	/* Reduce/cse.scm 393 */
											int BgL_tmpz00_2802;

											BgL_tmpz00_2802 = (int) (1L);
											BgL_tmpz00_2014 = BGL_MVALUES_VAL(BgL_tmpz00_2802);
										}
										{	/* Reduce/cse.scm 393 */
											int BgL_tmpz00_2805;

											BgL_tmpz00_2805 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2805, BUNSPEC);
										}
										BgL_nodez00_1443 = BgL_tmpz00_2014;
									}
									{	/* Reduce/cse.scm 393 */
										obj_t BgL_tmpz00_2808;

										BgL_tmpz00_2808 = ((obj_t) BgL_nodeza2za2_1435);
										SET_CAR(BgL_tmpz00_2808, BgL_nodez00_1443);
									}
									if (BgL_resetz00_1436)
										{	/* Reduce/cse.scm 394 */
											return BBOOL(BgL_resetz00_1436);
										}
									else
										{	/* Reduce/cse.scm 394 */
											return BgL_resetz72z72_1442;
										}
								}
							}
						else
							{	/* Reduce/cse.scm 396 */
								obj_t BgL_resetz72z72_1446;

								{	/* Reduce/cse.scm 397 */
									obj_t BgL_arg1472z00_1451;

									BgL_arg1472z00_1451 = CAR(((obj_t) BgL_nodeza2za2_1435));
									BgL_resetz72z72_1446 =
										BGl_nodezd2csez12zc0zzreduce_csez00(
										((BgL_nodez00_bglt) BgL_arg1472z00_1451),
										BgL_stackz00_1437);
								}
								{	/* Reduce/cse.scm 397 */
									obj_t BgL_nodez00_1447;

									{	/* Reduce/cse.scm 398 */
										obj_t BgL_tmpz00_2017;

										{	/* Reduce/cse.scm 398 */
											int BgL_tmpz00_2817;

											BgL_tmpz00_2817 = (int) (1L);
											BgL_tmpz00_2017 = BGL_MVALUES_VAL(BgL_tmpz00_2817);
										}
										{	/* Reduce/cse.scm 398 */
											int BgL_tmpz00_2820;

											BgL_tmpz00_2820 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2820, BUNSPEC);
										}
										BgL_nodez00_1447 = BgL_tmpz00_2017;
									}
									{	/* Reduce/cse.scm 398 */
										obj_t BgL_tmpz00_2823;

										BgL_tmpz00_2823 = ((obj_t) BgL_nodeza2za2_1435);
										SET_CAR(BgL_tmpz00_2823, BgL_nodez00_1447);
									}
									{	/* Reduce/cse.scm 399 */
										bool_t BgL_test1926z00_2826;

										if (BgL_resetz00_1436)
											{	/* Reduce/cse.scm 399 */
												BgL_test1926z00_2826 = ((bool_t) 1);
											}
										else
											{	/* Reduce/cse.scm 399 */
												BgL_test1926z00_2826 = CBOOL(BgL_resetz72z72_1446);
											}
										if (BgL_test1926z00_2826)
											{	/* Reduce/cse.scm 400 */
												obj_t BgL_arg1453z00_1449;

												BgL_arg1453z00_1449 =
													CDR(((obj_t) BgL_nodeza2za2_1435));
												{
													obj_t BgL_stackz00_2833;
													bool_t BgL_resetz00_2832;
													obj_t BgL_nodeza2za2_2831;

													BgL_nodeza2za2_2831 = BgL_arg1453z00_1449;
													BgL_resetz00_2832 = ((bool_t) 1);
													BgL_stackz00_2833 = BNIL;
													BgL_stackz00_1437 = BgL_stackz00_2833;
													BgL_resetz00_1436 = BgL_resetz00_2832;
													BgL_nodeza2za2_1435 = BgL_nodeza2za2_2831;
													goto BgL_zc3z04anonymousza31435ze3z87_1438;
												}
											}
										else
											{	/* Reduce/cse.scm 401 */
												obj_t BgL_arg1454z00_1450;

												BgL_arg1454z00_1450 =
													CDR(((obj_t) BgL_nodeza2za2_1435));
												{
													bool_t BgL_resetz00_2837;
													obj_t BgL_nodeza2za2_2836;

													BgL_nodeza2za2_2836 = BgL_arg1454z00_1450;
													BgL_resetz00_2837 = ((bool_t) 0);
													BgL_resetz00_1436 = BgL_resetz00_2837;
													BgL_nodeza2za2_1435 = BgL_nodeza2za2_2836;
													goto BgL_zc3z04anonymousza31435ze3z87_1438;
												}
											}
									}
								}
							}
					}
			}
		}

	}



/* find-stack */
	obj_t BGl_findzd2stackzd2zzreduce_csez00(BgL_nodez00_bglt BgL_nodez00_66,
		obj_t BgL_stackz00_67)
	{
		{	/* Reduce/cse.scm 415 */
			{
				obj_t BgL_stackz00_1466;

				BgL_stackz00_1466 = BgL_stackz00_67;
			BgL_zc3z04anonymousza31510ze3z87_1467:
				if (NULLP(BgL_stackz00_1466))
					{	/* Reduce/cse.scm 419 */
						return BFALSE;
					}
				else
					{	/* Reduce/cse.scm 421 */
						bool_t BgL_test1929z00_2840;

						{	/* Reduce/cse.scm 421 */
							obj_t BgL_arg1540z00_1474;

							{	/* Reduce/cse.scm 406 */
								obj_t BgL_sz00_2029;

								BgL_sz00_2029 = CAR(((obj_t) BgL_stackz00_1466));
								BgL_arg1540z00_1474 = STRUCT_REF(BgL_sz00_2029, (int) (0L));
							}
							BgL_test1929z00_2840 =
								BGl_samezd2nodezf3z21zzreduce_samez00(BgL_nodez00_66,
								((BgL_nodez00_bglt) BgL_arg1540z00_1474), BNIL);
						}
						if (BgL_test1929z00_2840)
							{	/* Reduce/cse.scm 406 */
								obj_t BgL_sz00_2031;

								BgL_sz00_2031 = CAR(((obj_t) BgL_stackz00_1466));
								return STRUCT_REF(BgL_sz00_2031, (int) (1L));
							}
						else
							{	/* Reduce/cse.scm 424 */
								obj_t BgL_arg1535z00_1473;

								BgL_arg1535z00_1473 = CDR(((obj_t) BgL_stackz00_1466));
								{
									obj_t BgL_stackz00_2853;

									BgL_stackz00_2853 = BgL_arg1535z00_1473;
									BgL_stackz00_1466 = BgL_stackz00_2853;
									goto BgL_zc3z04anonymousza31510ze3z87_1467;
								}
							}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzreduce_csez00(void)
	{
		{	/* Reduce/cse.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzreduce_csez00(void)
	{
		{	/* Reduce/cse.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00,
				BGl_proc1854z00zzreduce_csez00, BGl_nodez00zzast_nodez00,
				BGl_string1855z00zzreduce_csez00);
		}

	}



/* &node-cse!1317 */
	obj_t BGl_z62nodezd2csez121317za2zzreduce_csez00(obj_t BgL_envz00_2372,
		obj_t BgL_nodez00_2373, obj_t BgL_stackz00_2374)
	{
		{	/* Reduce/cse.scm 58 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
				BGl_string1856z00zzreduce_csez00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2373)));
		}

	}



/* node-cse! */
	BGL_EXPORTED_DEF obj_t BGl_nodezd2csez12zc0zzreduce_csez00(BgL_nodez00_bglt
		BgL_nodez00_4, obj_t BgL_stackz00_5)
	{
		{	/* Reduce/cse.scm 58 */
			{	/* Reduce/cse.scm 58 */
				obj_t BgL_method1318z00_1482;

				{	/* Reduce/cse.scm 58 */
					obj_t BgL_res1844z00_2063;

					{	/* Reduce/cse.scm 58 */
						long BgL_objzd2classzd2numz00_2034;

						BgL_objzd2classzd2numz00_2034 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_4));
						{	/* Reduce/cse.scm 58 */
							obj_t BgL_arg1811z00_2035;

							BgL_arg1811z00_2035 =
								PROCEDURE_REF(BGl_nodezd2csez12zd2envz12zzreduce_csez00,
								(int) (1L));
							{	/* Reduce/cse.scm 58 */
								int BgL_offsetz00_2038;

								BgL_offsetz00_2038 = (int) (BgL_objzd2classzd2numz00_2034);
								{	/* Reduce/cse.scm 58 */
									long BgL_offsetz00_2039;

									BgL_offsetz00_2039 =
										((long) (BgL_offsetz00_2038) - OBJECT_TYPE);
									{	/* Reduce/cse.scm 58 */
										long BgL_modz00_2040;

										BgL_modz00_2040 =
											(BgL_offsetz00_2039 >> (int) ((long) ((int) (4L))));
										{	/* Reduce/cse.scm 58 */
											long BgL_restz00_2042;

											BgL_restz00_2042 =
												(BgL_offsetz00_2039 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Reduce/cse.scm 58 */

												{	/* Reduce/cse.scm 58 */
													obj_t BgL_bucketz00_2044;

													BgL_bucketz00_2044 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2035), BgL_modz00_2040);
													BgL_res1844z00_2063 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2044), BgL_restz00_2042);
					}}}}}}}}
					BgL_method1318z00_1482 = BgL_res1844z00_2063;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1318z00_1482,
					((obj_t) BgL_nodez00_4), BgL_stackz00_5);
			}
		}

	}



/* &node-cse! */
	obj_t BGl_z62nodezd2csez12za2zzreduce_csez00(obj_t BgL_envz00_2375,
		obj_t BgL_nodez00_2376, obj_t BgL_stackz00_2377)
	{
		{	/* Reduce/cse.scm 58 */
			return
				BGl_nodezd2csez12zc0zzreduce_csez00(
				((BgL_nodez00_bglt) BgL_nodez00_2376), BgL_stackz00_2377);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzreduce_csez00(void)
	{
		{	/* Reduce/cse.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00, BGl_atomz00zzast_nodez00,
				BGl_proc1857z00zzreduce_csez00, BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00, BGl_kwotez00zzast_nodez00,
				BGl_proc1859z00zzreduce_csez00, BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00, BGl_varz00zzast_nodez00,
				BGl_proc1860z00zzreduce_csez00, BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00, BGl_closurez00zzast_nodez00,
				BGl_proc1861z00zzreduce_csez00, BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00,
				BGl_sequencez00zzast_nodez00, BGl_proc1862z00zzreduce_csez00,
				BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00, BGl_syncz00zzast_nodez00,
				BGl_proc1863z00zzreduce_csez00, BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1864z00zzreduce_csez00,
				BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00, BGl_funcallz00zzast_nodez00,
				BGl_proc1865z00zzreduce_csez00, BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00, BGl_externz00zzast_nodez00,
				BGl_proc1866z00zzreduce_csez00, BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00, BGl_castz00zzast_nodez00,
				BGl_proc1867z00zzreduce_csez00, BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00, BGl_setqz00zzast_nodez00,
				BGl_proc1868z00zzreduce_csez00, BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1869z00zzreduce_csez00,
				BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00, BGl_failz00zzast_nodez00,
				BGl_proc1870z00zzreduce_csez00, BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00, BGl_switchz00zzast_nodez00,
				BGl_proc1871z00zzreduce_csez00, BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1872z00zzreduce_csez00,
				BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1873z00zzreduce_csez00,
				BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1874z00zzreduce_csez00,
				BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00,
				BGl_retblockz00zzast_nodez00, BGl_proc1875z00zzreduce_csez00,
				BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00, BGl_returnz00zzast_nodez00,
				BGl_proc1876z00zzreduce_csez00, BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1877z00zzreduce_csez00,
				BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1878z00zzreduce_csez00,
				BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1879z00zzreduce_csez00,
				BGl_string1858z00zzreduce_csez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00, BGl_appz00zzast_nodez00,
				BGl_proc1880z00zzreduce_csez00, BGl_string1858z00zzreduce_csez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2csez12zd2envz12zzreduce_csez00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1881z00zzreduce_csez00,
				BGl_string1858z00zzreduce_csez00);
		}

	}



/* &node-cse!-let-var1366 */
	obj_t BGl_z62nodezd2csez12zd2letzd2var1366za2zzreduce_csez00(obj_t
		BgL_envz00_2402, obj_t BgL_nodez00_2403, obj_t BgL_stackz00_2404)
	{
		{	/* Reduce/cse.scm 328 */
			{
				obj_t BgL_bindingsz00_2489;
				bool_t BgL_resetz00_2490;
				obj_t BgL_extendz00_2491;

				BgL_bindingsz00_2489 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2403)))->BgL_bindingsz00);
				BgL_resetz00_2490 = ((bool_t) 0);
				BgL_extendz00_2491 = BNIL;
			BgL_loopz00_2488:
				if (NULLP(BgL_bindingsz00_2489))
					{	/* Reduce/cse.scm 334 */
						obj_t BgL_stackz72z72_2492;

						if (BgL_resetz00_2490)
							{	/* Reduce/cse.scm 334 */
								BgL_stackz72z72_2492 = BNIL;
							}
						else
							{	/* Reduce/cse.scm 334 */
								BgL_stackz72z72_2492 =
									BGl_appendzd221011zd2zzreduce_csez00(BgL_extendz00_2491,
									BgL_stackz00_2404);
							}
						{	/* Reduce/cse.scm 335 */
							obj_t BgL_resetz72z72_2493;

							BgL_resetz72z72_2493 =
								BGl_nodezd2csez12zc0zzreduce_csez00(
								(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_nodez00_2403)))->
									BgL_bodyz00), BgL_stackz72z72_2492);
							{	/* Reduce/cse.scm 336 */
								obj_t BgL_nbodyz00_2494;

								{	/* Reduce/cse.scm 337 */
									obj_t BgL_tmpz00_2495;

									{	/* Reduce/cse.scm 337 */
										int BgL_tmpz00_2923;

										BgL_tmpz00_2923 = (int) (1L);
										BgL_tmpz00_2495 = BGL_MVALUES_VAL(BgL_tmpz00_2923);
									}
									{	/* Reduce/cse.scm 337 */
										int BgL_tmpz00_2926;

										BgL_tmpz00_2926 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_2926, BUNSPEC);
									}
									BgL_nbodyz00_2494 = BgL_tmpz00_2495;
								}
								((((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_nodez00_2403)))->
										BgL_bodyz00) =
									((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nbodyz00_2494)),
									BUNSPEC);
								{	/* Reduce/cse.scm 339 */
									bool_t BgL_test1932z00_2932;

									if (BgL_resetz00_2490)
										{	/* Reduce/cse.scm 339 */
											BgL_test1932z00_2932 = ((bool_t) 1);
										}
									else
										{	/* Reduce/cse.scm 339 */
											BgL_test1932z00_2932 = CBOOL(BgL_resetz72z72_2493);
										}
									if (BgL_test1932z00_2932)
										{	/* Reduce/cse.scm 339 */
											{	/* Reduce/cse.scm 340 */
												int BgL_tmpz00_2935;

												BgL_tmpz00_2935 = (int) (2L);
												BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2935);
											}
											{	/* Reduce/cse.scm 340 */
												obj_t BgL_auxz00_2940;
												int BgL_tmpz00_2938;

												BgL_auxz00_2940 =
													((obj_t) ((BgL_letzd2varzd2_bglt) BgL_nodez00_2403));
												BgL_tmpz00_2938 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_2938, BgL_auxz00_2940);
											}
											return BTRUE;
										}
									else
										{	/* Reduce/cse.scm 339 */
											if (BGl_sidezd2effectzf3z21zzeffect_effectz00(
													((BgL_nodez00_bglt)
														((BgL_letzd2varzd2_bglt) BgL_nodez00_2403))))
												{	/* Reduce/cse.scm 341 */
													{	/* Reduce/cse.scm 342 */
														int BgL_tmpz00_2948;

														BgL_tmpz00_2948 = (int) (2L);
														BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2948);
													}
													{	/* Reduce/cse.scm 342 */
														obj_t BgL_auxz00_2953;
														int BgL_tmpz00_2951;

														BgL_auxz00_2953 =
															((obj_t)
															((BgL_letzd2varzd2_bglt) BgL_nodez00_2403));
														BgL_tmpz00_2951 = (int) (1L);
														BGL_MVALUES_VAL_SET(BgL_tmpz00_2951,
															BgL_auxz00_2953);
													}
													return BFALSE;
												}
											else
												{	/* Reduce/cse.scm 344 */
													obj_t BgL_previousz00_2496;

													BgL_previousz00_2496 =
														BGl_findzd2stackzd2zzreduce_csez00(
														((BgL_nodez00_bglt)
															((BgL_letzd2varzd2_bglt) BgL_nodez00_2403)),
														BgL_stackz00_2404);
													{	/* Reduce/cse.scm 346 */
														bool_t BgL_test1935z00_2960;

														{	/* Reduce/cse.scm 346 */
															obj_t BgL_classz00_2497;

															BgL_classz00_2497 = BGl_variablez00zzast_varz00;
															if (BGL_OBJECTP(BgL_previousz00_2496))
																{	/* Reduce/cse.scm 346 */
																	BgL_objectz00_bglt BgL_arg1807z00_2498;

																	BgL_arg1807z00_2498 =
																		(BgL_objectz00_bglt) (BgL_previousz00_2496);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Reduce/cse.scm 346 */
																			long BgL_idxz00_2499;

																			BgL_idxz00_2499 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2498);
																			BgL_test1935z00_2960 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2499 + 1L)) ==
																				BgL_classz00_2497);
																		}
																	else
																		{	/* Reduce/cse.scm 346 */
																			bool_t BgL_res1846z00_2502;

																			{	/* Reduce/cse.scm 346 */
																				obj_t BgL_oclassz00_2503;

																				{	/* Reduce/cse.scm 346 */
																					obj_t BgL_arg1815z00_2504;
																					long BgL_arg1816z00_2505;

																					BgL_arg1815z00_2504 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Reduce/cse.scm 346 */
																						long BgL_arg1817z00_2506;

																						BgL_arg1817z00_2506 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2498);
																						BgL_arg1816z00_2505 =
																							(BgL_arg1817z00_2506 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2503 =
																						VECTOR_REF(BgL_arg1815z00_2504,
																						BgL_arg1816z00_2505);
																				}
																				{	/* Reduce/cse.scm 346 */
																					bool_t BgL__ortest_1115z00_2507;

																					BgL__ortest_1115z00_2507 =
																						(BgL_classz00_2497 ==
																						BgL_oclassz00_2503);
																					if (BgL__ortest_1115z00_2507)
																						{	/* Reduce/cse.scm 346 */
																							BgL_res1846z00_2502 =
																								BgL__ortest_1115z00_2507;
																						}
																					else
																						{	/* Reduce/cse.scm 346 */
																							long BgL_odepthz00_2508;

																							{	/* Reduce/cse.scm 346 */
																								obj_t BgL_arg1804z00_2509;

																								BgL_arg1804z00_2509 =
																									(BgL_oclassz00_2503);
																								BgL_odepthz00_2508 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2509);
																							}
																							if ((1L < BgL_odepthz00_2508))
																								{	/* Reduce/cse.scm 346 */
																									obj_t BgL_arg1802z00_2510;

																									{	/* Reduce/cse.scm 346 */
																										obj_t BgL_arg1803z00_2511;

																										BgL_arg1803z00_2511 =
																											(BgL_oclassz00_2503);
																										BgL_arg1802z00_2510 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2511, 1L);
																									}
																									BgL_res1846z00_2502 =
																										(BgL_arg1802z00_2510 ==
																										BgL_classz00_2497);
																								}
																							else
																								{	/* Reduce/cse.scm 346 */
																									BgL_res1846z00_2502 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test1935z00_2960 =
																				BgL_res1846z00_2502;
																		}
																}
															else
																{	/* Reduce/cse.scm 346 */
																	BgL_test1935z00_2960 = ((bool_t) 0);
																}
														}
														if (BgL_test1935z00_2960)
															{	/* Reduce/cse.scm 346 */
																BGl_za2csezd2removedza2zd2zzreduce_csez00 =
																	(1L +
																	BGl_za2csezd2removedza2zd2zzreduce_csez00);
																{	/* Reduce/cse.scm 350 */
																	BgL_refz00_bglt BgL_val1_1314z00_2512;

																	{	/* Reduce/cse.scm 350 */
																		BgL_refz00_bglt BgL_new1141z00_2513;

																		{	/* Reduce/cse.scm 353 */
																			BgL_refz00_bglt BgL_new1140z00_2514;

																			BgL_new1140z00_2514 =
																				((BgL_refz00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_refz00_bgl))));
																			{	/* Reduce/cse.scm 353 */
																				long BgL_arg1688z00_2515;

																				BgL_arg1688z00_2515 =
																					BGL_CLASS_NUM
																					(BGl_refz00zzast_nodez00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1140z00_2514),
																					BgL_arg1688z00_2515);
																			}
																			{	/* Reduce/cse.scm 353 */
																				BgL_objectz00_bglt BgL_tmpz00_2988;

																				BgL_tmpz00_2988 =
																					((BgL_objectz00_bglt)
																					BgL_new1140z00_2514);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2988,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1140z00_2514);
																			BgL_new1141z00_2513 = BgL_new1140z00_2514;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1141z00_2513)))->
																				BgL_locz00) =
																			((obj_t) (((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt) (
																									(BgL_letzd2varzd2_bglt)
																									BgL_nodez00_2403))))->
																					BgL_locz00)), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1141z00_2513)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt) (
																									(BgL_letzd2varzd2_bglt)
																									BgL_nodez00_2403))))->
																					BgL_typez00)), BUNSPEC);
																		((((BgL_varz00_bglt)
																					COBJECT(((BgL_varz00_bglt)
																							BgL_new1141z00_2513)))->
																				BgL_variablez00) =
																			((BgL_variablez00_bglt) (
																					(BgL_variablez00_bglt)
																					BgL_previousz00_2496)), BUNSPEC);
																		BgL_val1_1314z00_2512 = BgL_new1141z00_2513;
																	}
																	{	/* Reduce/cse.scm 350 */
																		int BgL_tmpz00_3005;

																		BgL_tmpz00_3005 = (int) (2L);
																		BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3005);
																	}
																	{	/* Reduce/cse.scm 350 */
																		obj_t BgL_auxz00_3010;
																		int BgL_tmpz00_3008;

																		BgL_auxz00_3010 =
																			((obj_t) BgL_val1_1314z00_2512);
																		BgL_tmpz00_3008 = (int) (1L);
																		BGL_MVALUES_VAL_SET(BgL_tmpz00_3008,
																			BgL_auxz00_3010);
																	}
																	return BgL_stackz72z72_2492;
																}
															}
														else
															{	/* Reduce/cse.scm 346 */
																{	/* Reduce/cse.scm 355 */
																	int BgL_tmpz00_3013;

																	BgL_tmpz00_3013 = (int) (2L);
																	BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3013);
																}
																{	/* Reduce/cse.scm 355 */
																	obj_t BgL_auxz00_3018;
																	int BgL_tmpz00_3016;

																	BgL_auxz00_3018 =
																		((obj_t)
																		((BgL_letzd2varzd2_bglt) BgL_nodez00_2403));
																	BgL_tmpz00_3016 = (int) (1L);
																	BGL_MVALUES_VAL_SET(BgL_tmpz00_3016,
																		BgL_auxz00_3018);
																}
																return BFALSE;
															}
													}
												}
										}
								}
							}
						}
					}
				else
					{	/* Reduce/cse.scm 356 */
						obj_t BgL_bindingz00_2516;

						BgL_bindingz00_2516 = CAR(((obj_t) BgL_bindingsz00_2489));
						{	/* Reduce/cse.scm 357 */
							obj_t BgL_varz00_2517;
							obj_t BgL_valz00_2518;

							BgL_varz00_2517 = CAR(((obj_t) BgL_bindingz00_2516));
							BgL_valz00_2518 = CDR(((obj_t) BgL_bindingz00_2516));
							{	/* Reduce/cse.scm 359 */
								obj_t BgL_resetz72z72_2519;

								BgL_resetz72z72_2519 =
									BGl_nodezd2csez12zc0zzreduce_csez00(
									((BgL_nodez00_bglt) BgL_valz00_2518), BgL_stackz00_2404);
								{	/* Reduce/cse.scm 360 */
									obj_t BgL_nvalz00_2520;

									{	/* Reduce/cse.scm 361 */
										obj_t BgL_tmpz00_2521;

										{	/* Reduce/cse.scm 361 */
											int BgL_tmpz00_3030;

											BgL_tmpz00_3030 = (int) (1L);
											BgL_tmpz00_2521 = BGL_MVALUES_VAL(BgL_tmpz00_3030);
										}
										{	/* Reduce/cse.scm 361 */
											int BgL_tmpz00_3033;

											BgL_tmpz00_3033 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_3033, BUNSPEC);
										}
										BgL_nvalz00_2520 = BgL_tmpz00_2521;
									}
									{	/* Reduce/cse.scm 361 */
										obj_t BgL_tmpz00_3036;

										BgL_tmpz00_3036 = ((obj_t) BgL_bindingz00_2516);
										SET_CDR(BgL_tmpz00_3036, BgL_nvalz00_2520);
									}
									{	/* Reduce/cse.scm 363 */
										bool_t BgL_test1940z00_3039;

										if (BgL_resetz00_2490)
											{	/* Reduce/cse.scm 363 */
												BgL_test1940z00_3039 = ((bool_t) 1);
											}
										else
											{	/* Reduce/cse.scm 363 */
												BgL_test1940z00_3039 = CBOOL(BgL_resetz72z72_2519);
											}
										if (BgL_test1940z00_3039)
											{	/* Reduce/cse.scm 364 */
												obj_t BgL_arg1691z00_2522;

												BgL_arg1691z00_2522 =
													CDR(((obj_t) BgL_bindingsz00_2489));
												{
													obj_t BgL_extendz00_3046;
													bool_t BgL_resetz00_3045;
													obj_t BgL_bindingsz00_3044;

													BgL_bindingsz00_3044 = BgL_arg1691z00_2522;
													BgL_resetz00_3045 = ((bool_t) 1);
													BgL_extendz00_3046 = BNIL;
													BgL_extendz00_2491 = BgL_extendz00_3046;
													BgL_resetz00_2490 = BgL_resetz00_3045;
													BgL_bindingsz00_2489 = BgL_bindingsz00_3044;
													goto BgL_loopz00_2488;
												}
											}
										else
											{	/* Reduce/cse.scm 363 */
												if (
													((((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_varz00_2517))))->
															BgL_accessz00) == CNST_TABLE_REF(1)))
													{	/* Reduce/cse.scm 371 */
														bool_t BgL_test1943z00_3053;

														{	/* Reduce/cse.scm 371 */
															bool_t BgL_test1944z00_3054;

															{	/* Reduce/cse.scm 371 */
																obj_t BgL_classz00_2523;

																BgL_classz00_2523 = BGl_appz00zzast_nodez00;
																if (BGL_OBJECTP(BgL_valz00_2518))
																	{	/* Reduce/cse.scm 371 */
																		BgL_objectz00_bglt BgL_arg1807z00_2524;

																		BgL_arg1807z00_2524 =
																			(BgL_objectz00_bglt) (BgL_valz00_2518);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Reduce/cse.scm 371 */
																				long BgL_idxz00_2525;

																				BgL_idxz00_2525 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2524);
																				BgL_test1944z00_3054 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2525 + 3L)) ==
																					BgL_classz00_2523);
																			}
																		else
																			{	/* Reduce/cse.scm 371 */
																				bool_t BgL_res1847z00_2528;

																				{	/* Reduce/cse.scm 371 */
																					obj_t BgL_oclassz00_2529;

																					{	/* Reduce/cse.scm 371 */
																						obj_t BgL_arg1815z00_2530;
																						long BgL_arg1816z00_2531;

																						BgL_arg1815z00_2530 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Reduce/cse.scm 371 */
																							long BgL_arg1817z00_2532;

																							BgL_arg1817z00_2532 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2524);
																							BgL_arg1816z00_2531 =
																								(BgL_arg1817z00_2532 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2529 =
																							VECTOR_REF(BgL_arg1815z00_2530,
																							BgL_arg1816z00_2531);
																					}
																					{	/* Reduce/cse.scm 371 */
																						bool_t BgL__ortest_1115z00_2533;

																						BgL__ortest_1115z00_2533 =
																							(BgL_classz00_2523 ==
																							BgL_oclassz00_2529);
																						if (BgL__ortest_1115z00_2533)
																							{	/* Reduce/cse.scm 371 */
																								BgL_res1847z00_2528 =
																									BgL__ortest_1115z00_2533;
																							}
																						else
																							{	/* Reduce/cse.scm 371 */
																								long BgL_odepthz00_2534;

																								{	/* Reduce/cse.scm 371 */
																									obj_t BgL_arg1804z00_2535;

																									BgL_arg1804z00_2535 =
																										(BgL_oclassz00_2529);
																									BgL_odepthz00_2534 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2535);
																								}
																								if ((3L < BgL_odepthz00_2534))
																									{	/* Reduce/cse.scm 371 */
																										obj_t BgL_arg1802z00_2536;

																										{	/* Reduce/cse.scm 371 */
																											obj_t BgL_arg1803z00_2537;

																											BgL_arg1803z00_2537 =
																												(BgL_oclassz00_2529);
																											BgL_arg1802z00_2536 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2537,
																												3L);
																										}
																										BgL_res1847z00_2528 =
																											(BgL_arg1802z00_2536 ==
																											BgL_classz00_2523);
																									}
																								else
																									{	/* Reduce/cse.scm 371 */
																										BgL_res1847z00_2528 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test1944z00_3054 =
																					BgL_res1847z00_2528;
																			}
																	}
																else
																	{	/* Reduce/cse.scm 371 */
																		BgL_test1944z00_3054 = ((bool_t) 0);
																	}
															}
															if (BgL_test1944z00_3054)
																{	/* Reduce/cse.scm 371 */
																	BgL_test1943z00_3053 = ((bool_t) 1);
																}
															else
																{	/* Reduce/cse.scm 371 */
																	obj_t BgL_classz00_2538;

																	BgL_classz00_2538 =
																		BGl_letzd2varzd2zzast_nodez00;
																	if (BGL_OBJECTP(BgL_valz00_2518))
																		{	/* Reduce/cse.scm 371 */
																			BgL_objectz00_bglt BgL_arg1807z00_2539;

																			BgL_arg1807z00_2539 =
																				(BgL_objectz00_bglt) (BgL_valz00_2518);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Reduce/cse.scm 371 */
																					long BgL_idxz00_2540;

																					BgL_idxz00_2540 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2539);
																					BgL_test1943z00_3053 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2540 + 3L)) ==
																						BgL_classz00_2538);
																				}
																			else
																				{	/* Reduce/cse.scm 371 */
																					bool_t BgL_res1848z00_2543;

																					{	/* Reduce/cse.scm 371 */
																						obj_t BgL_oclassz00_2544;

																						{	/* Reduce/cse.scm 371 */
																							obj_t BgL_arg1815z00_2545;
																							long BgL_arg1816z00_2546;

																							BgL_arg1815z00_2545 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Reduce/cse.scm 371 */
																								long BgL_arg1817z00_2547;

																								BgL_arg1817z00_2547 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2539);
																								BgL_arg1816z00_2546 =
																									(BgL_arg1817z00_2547 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2544 =
																								VECTOR_REF(BgL_arg1815z00_2545,
																								BgL_arg1816z00_2546);
																						}
																						{	/* Reduce/cse.scm 371 */
																							bool_t BgL__ortest_1115z00_2548;

																							BgL__ortest_1115z00_2548 =
																								(BgL_classz00_2538 ==
																								BgL_oclassz00_2544);
																							if (BgL__ortest_1115z00_2548)
																								{	/* Reduce/cse.scm 371 */
																									BgL_res1848z00_2543 =
																										BgL__ortest_1115z00_2548;
																								}
																							else
																								{	/* Reduce/cse.scm 371 */
																									long BgL_odepthz00_2549;

																									{	/* Reduce/cse.scm 371 */
																										obj_t BgL_arg1804z00_2550;

																										BgL_arg1804z00_2550 =
																											(BgL_oclassz00_2544);
																										BgL_odepthz00_2549 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2550);
																									}
																									if ((3L < BgL_odepthz00_2549))
																										{	/* Reduce/cse.scm 371 */
																											obj_t BgL_arg1802z00_2551;

																											{	/* Reduce/cse.scm 371 */
																												obj_t
																													BgL_arg1803z00_2552;
																												BgL_arg1803z00_2552 =
																													(BgL_oclassz00_2544);
																												BgL_arg1802z00_2551 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2552,
																													3L);
																											}
																											BgL_res1848z00_2543 =
																												(BgL_arg1802z00_2551 ==
																												BgL_classz00_2538);
																										}
																									else
																										{	/* Reduce/cse.scm 371 */
																											BgL_res1848z00_2543 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test1943z00_3053 =
																						BgL_res1848z00_2543;
																				}
																		}
																	else
																		{	/* Reduce/cse.scm 371 */
																			BgL_test1943z00_3053 = ((bool_t) 0);
																		}
																}
														}
														if (BgL_test1943z00_3053)
															{	/* Reduce/cse.scm 372 */
																obj_t BgL_arg1699z00_2553;
																obj_t BgL_arg1700z00_2554;

																BgL_arg1699z00_2553 =
																	CDR(((obj_t) BgL_bindingsz00_2489));
																{	/* Reduce/cse.scm 374 */
																	obj_t BgL_arg1701z00_2555;

																	{	/* Reduce/cse.scm 406 */
																		obj_t BgL_newz00_2556;

																		BgL_newz00_2556 =
																			create_struct(CNST_TABLE_REF(2),
																			(int) (2L));
																		{	/* Reduce/cse.scm 406 */
																			int BgL_tmpz00_3104;

																			BgL_tmpz00_3104 = (int) (1L);
																			STRUCT_SET(BgL_newz00_2556,
																				BgL_tmpz00_3104, BgL_varz00_2517);
																		}
																		{	/* Reduce/cse.scm 406 */
																			int BgL_tmpz00_3107;

																			BgL_tmpz00_3107 = (int) (0L);
																			STRUCT_SET(BgL_newz00_2556,
																				BgL_tmpz00_3107, BgL_valz00_2518);
																		}
																		BgL_arg1701z00_2555 = BgL_newz00_2556;
																	}
																	BgL_arg1700z00_2554 =
																		MAKE_YOUNG_PAIR(BgL_arg1701z00_2555,
																		BgL_extendz00_2491);
																}
																{
																	obj_t BgL_extendz00_3113;
																	bool_t BgL_resetz00_3112;
																	obj_t BgL_bindingsz00_3111;

																	BgL_bindingsz00_3111 = BgL_arg1699z00_2553;
																	BgL_resetz00_3112 = ((bool_t) 0);
																	BgL_extendz00_3113 = BgL_arg1700z00_2554;
																	BgL_extendz00_2491 = BgL_extendz00_3113;
																	BgL_resetz00_2490 = BgL_resetz00_3112;
																	BgL_bindingsz00_2489 = BgL_bindingsz00_3111;
																	goto BgL_loopz00_2488;
																}
															}
														else
															{	/* Reduce/cse.scm 376 */
																obj_t BgL_arg1702z00_2557;

																BgL_arg1702z00_2557 =
																	CDR(((obj_t) BgL_bindingsz00_2489));
																{
																	bool_t BgL_resetz00_3117;
																	obj_t BgL_bindingsz00_3116;

																	BgL_bindingsz00_3116 = BgL_arg1702z00_2557;
																	BgL_resetz00_3117 = ((bool_t) 0);
																	BgL_resetz00_2490 = BgL_resetz00_3117;
																	BgL_bindingsz00_2489 = BgL_bindingsz00_3116;
																	goto BgL_loopz00_2488;
																}
															}
													}
												else
													{	/* Reduce/cse.scm 368 */
														obj_t BgL_arg1703z00_2558;

														BgL_arg1703z00_2558 =
															CDR(((obj_t) BgL_bindingsz00_2489));
														{
															bool_t BgL_resetz00_3121;
															obj_t BgL_bindingsz00_3120;

															BgL_bindingsz00_3120 = BgL_arg1703z00_2558;
															BgL_resetz00_3121 = ((bool_t) 0);
															BgL_resetz00_2490 = BgL_resetz00_3121;
															BgL_bindingsz00_2489 = BgL_bindingsz00_3120;
															goto BgL_loopz00_2488;
														}
													}
											}
									}
								}
							}
						}
					}
			}
		}

	}



/* &node-cse!-app1364 */
	obj_t BGl_z62nodezd2csez12zd2app1364z70zzreduce_csez00(obj_t BgL_envz00_2405,
		obj_t BgL_nodez00_2406, obj_t BgL_stackz00_2407)
	{
		{	/* Reduce/cse.scm 302 */
			{	/* Reduce/cse.scm 304 */
				obj_t BgL_resetz00_2560;

				BgL_resetz00_2560 =
					BGl_nodezd2cseza2z12z62zzreduce_csez00(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2406)))->BgL_argsz00),
					BgL_stackz00_2407);
				{	/* Reduce/cse.scm 305 */
					bool_t BgL_test1953z00_3127;

					if (CBOOL(BgL_resetz00_2560))
						{	/* Reduce/cse.scm 305 */
							BgL_test1953z00_3127 = ((bool_t) 1);
						}
					else
						{	/* Reduce/cse.scm 305 */
							BgL_test1953z00_3127 =
								BGl_sidezd2effectzf3z21zzeffect_effectz00(
								((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2406)));
						}
					if (BgL_test1953z00_3127)
						{	/* Reduce/cse.scm 305 */
							{	/* Reduce/cse.scm 309 */
								int BgL_tmpz00_3133;

								BgL_tmpz00_3133 = (int) (2L);
								BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3133);
							}
							{	/* Reduce/cse.scm 309 */
								obj_t BgL_auxz00_3138;
								int BgL_tmpz00_3136;

								BgL_auxz00_3138 =
									((obj_t) ((BgL_appz00_bglt) BgL_nodez00_2406));
								BgL_tmpz00_3136 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_3136, BgL_auxz00_3138);
							}
							return BTRUE;
						}
					else
						{	/* Reduce/cse.scm 310 */
							obj_t BgL_previousz00_2561;

							BgL_previousz00_2561 =
								BGl_findzd2stackzd2zzreduce_csez00(
								((BgL_nodez00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_2406)), BgL_stackz00_2407);
							{	/* Reduce/cse.scm 312 */
								bool_t BgL_test1955z00_3145;

								{	/* Reduce/cse.scm 312 */
									obj_t BgL_classz00_2562;

									BgL_classz00_2562 = BGl_variablez00zzast_varz00;
									if (BGL_OBJECTP(BgL_previousz00_2561))
										{	/* Reduce/cse.scm 312 */
											BgL_objectz00_bglt BgL_arg1807z00_2563;

											BgL_arg1807z00_2563 =
												(BgL_objectz00_bglt) (BgL_previousz00_2561);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Reduce/cse.scm 312 */
													long BgL_idxz00_2564;

													BgL_idxz00_2564 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2563);
													BgL_test1955z00_3145 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2564 + 1L)) == BgL_classz00_2562);
												}
											else
												{	/* Reduce/cse.scm 312 */
													bool_t BgL_res1845z00_2567;

													{	/* Reduce/cse.scm 312 */
														obj_t BgL_oclassz00_2568;

														{	/* Reduce/cse.scm 312 */
															obj_t BgL_arg1815z00_2569;
															long BgL_arg1816z00_2570;

															BgL_arg1815z00_2569 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Reduce/cse.scm 312 */
																long BgL_arg1817z00_2571;

																BgL_arg1817z00_2571 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2563);
																BgL_arg1816z00_2570 =
																	(BgL_arg1817z00_2571 - OBJECT_TYPE);
															}
															BgL_oclassz00_2568 =
																VECTOR_REF(BgL_arg1815z00_2569,
																BgL_arg1816z00_2570);
														}
														{	/* Reduce/cse.scm 312 */
															bool_t BgL__ortest_1115z00_2572;

															BgL__ortest_1115z00_2572 =
																(BgL_classz00_2562 == BgL_oclassz00_2568);
															if (BgL__ortest_1115z00_2572)
																{	/* Reduce/cse.scm 312 */
																	BgL_res1845z00_2567 =
																		BgL__ortest_1115z00_2572;
																}
															else
																{	/* Reduce/cse.scm 312 */
																	long BgL_odepthz00_2573;

																	{	/* Reduce/cse.scm 312 */
																		obj_t BgL_arg1804z00_2574;

																		BgL_arg1804z00_2574 = (BgL_oclassz00_2568);
																		BgL_odepthz00_2573 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2574);
																	}
																	if ((1L < BgL_odepthz00_2573))
																		{	/* Reduce/cse.scm 312 */
																			obj_t BgL_arg1802z00_2575;

																			{	/* Reduce/cse.scm 312 */
																				obj_t BgL_arg1803z00_2576;

																				BgL_arg1803z00_2576 =
																					(BgL_oclassz00_2568);
																				BgL_arg1802z00_2575 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2576, 1L);
																			}
																			BgL_res1845z00_2567 =
																				(BgL_arg1802z00_2575 ==
																				BgL_classz00_2562);
																		}
																	else
																		{	/* Reduce/cse.scm 312 */
																			BgL_res1845z00_2567 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1955z00_3145 = BgL_res1845z00_2567;
												}
										}
									else
										{	/* Reduce/cse.scm 312 */
											BgL_test1955z00_3145 = ((bool_t) 0);
										}
								}
								if (BgL_test1955z00_3145)
									{	/* Reduce/cse.scm 312 */
										BGl_za2csezd2removedza2zd2zzreduce_csez00 =
											(1L + BGl_za2csezd2removedza2zd2zzreduce_csez00);
										{	/* Reduce/cse.scm 315 */
											BgL_refz00_bglt BgL_val1_1306z00_2577;

											{	/* Reduce/cse.scm 315 */
												BgL_refz00_bglt BgL_new1137z00_2578;

												{	/* Reduce/cse.scm 318 */
													BgL_refz00_bglt BgL_new1136z00_2579;

													BgL_new1136z00_2579 =
														((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_refz00_bgl))));
													{	/* Reduce/cse.scm 318 */
														long BgL_arg1675z00_2580;

														BgL_arg1675z00_2580 =
															BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1136z00_2579),
															BgL_arg1675z00_2580);
													}
													{	/* Reduce/cse.scm 318 */
														BgL_objectz00_bglt BgL_tmpz00_3173;

														BgL_tmpz00_3173 =
															((BgL_objectz00_bglt) BgL_new1136z00_2579);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3173, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1136z00_2579);
													BgL_new1137z00_2578 = BgL_new1136z00_2579;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1137z00_2578)))->
														BgL_locz00) =
													((obj_t) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) ((BgL_appz00_bglt)
																			BgL_nodez00_2406))))->BgL_locz00)),
													BUNSPEC);
												((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																	BgL_new1137z00_2578)))->BgL_typez00) =
													((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) ((BgL_appz00_bglt)
																			BgL_nodez00_2406))))->BgL_typez00)),
													BUNSPEC);
												((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																	BgL_new1137z00_2578)))->BgL_variablez00) =
													((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
															BgL_previousz00_2561)), BUNSPEC);
												BgL_val1_1306z00_2577 = BgL_new1137z00_2578;
											}
											{	/* Reduce/cse.scm 315 */
												int BgL_tmpz00_3190;

												BgL_tmpz00_3190 = (int) (2L);
												BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3190);
											}
											{	/* Reduce/cse.scm 315 */
												obj_t BgL_auxz00_3195;
												int BgL_tmpz00_3193;

												BgL_auxz00_3195 = ((obj_t) BgL_val1_1306z00_2577);
												BgL_tmpz00_3193 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_3193, BgL_auxz00_3195);
											}
											return BgL_stackz00_2407;
										}
									}
								else
									{	/* Reduce/cse.scm 312 */
										{	/* Reduce/cse.scm 320 */
											int BgL_tmpz00_3198;

											BgL_tmpz00_3198 = (int) (2L);
											BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3198);
										}
										{	/* Reduce/cse.scm 320 */
											obj_t BgL_auxz00_3203;
											int BgL_tmpz00_3201;

											BgL_auxz00_3203 =
												((obj_t) ((BgL_appz00_bglt) BgL_nodez00_2406));
											BgL_tmpz00_3201 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_3201, BgL_auxz00_3203);
										}
										return BFALSE;
									}
							}
						}
				}
			}
		}

	}



/* &node-cse!-box-ref1362 */
	obj_t BGl_z62nodezd2csez12zd2boxzd2ref1362za2zzreduce_csez00(obj_t
		BgL_envz00_2408, obj_t BgL_nodez00_2409, obj_t BgL_stackz00_2410)
	{
		{	/* Reduce/cse.scm 296 */
			{	/* Reduce/cse.scm 297 */
				int BgL_tmpz00_3207;

				BgL_tmpz00_3207 = (int) (2L);
				BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3207);
			}
			{	/* Reduce/cse.scm 297 */
				obj_t BgL_auxz00_3212;
				int BgL_tmpz00_3210;

				BgL_auxz00_3212 = ((obj_t) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2409));
				BgL_tmpz00_3210 = (int) (1L);
				BGL_MVALUES_VAL_SET(BgL_tmpz00_3210, BgL_auxz00_3212);
			}
			return BFALSE;
		}

	}



/* &node-cse!-box-set!1360 */
	obj_t BGl_z62nodezd2csez12zd2boxzd2setz121360zb0zzreduce_csez00(obj_t
		BgL_envz00_2411, obj_t BgL_nodez00_2412, obj_t BgL_stackz00_2413)
	{
		{	/* Reduce/cse.scm 286 */
			{	/* Reduce/cse.scm 288 */
				obj_t BgL_resetz00_2583;

				BgL_resetz00_2583 =
					BGl_nodezd2csez12zc0zzreduce_csez00(
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2412)))->BgL_valuez00),
					BgL_stackz00_2413);
				{	/* Reduce/cse.scm 289 */
					obj_t BgL_nvaluez00_2584;

					{	/* Reduce/cse.scm 290 */
						obj_t BgL_tmpz00_2585;

						{	/* Reduce/cse.scm 290 */
							int BgL_tmpz00_3219;

							BgL_tmpz00_3219 = (int) (1L);
							BgL_tmpz00_2585 = BGL_MVALUES_VAL(BgL_tmpz00_3219);
						}
						{	/* Reduce/cse.scm 290 */
							int BgL_tmpz00_3222;

							BgL_tmpz00_3222 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3222, BUNSPEC);
						}
						BgL_nvaluez00_2584 = BgL_tmpz00_2585;
					}
					((((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2412)))->
							BgL_valuez00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nvaluez00_2584)),
						BUNSPEC);
					{	/* Reduce/cse.scm 291 */
						int BgL_tmpz00_3228;

						BgL_tmpz00_3228 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3228);
					}
					{	/* Reduce/cse.scm 291 */
						obj_t BgL_auxz00_3233;
						int BgL_tmpz00_3231;

						BgL_auxz00_3233 =
							((obj_t) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2412));
						BgL_tmpz00_3231 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_3231, BgL_auxz00_3233);
					}
					return BgL_resetz00_2583;
				}
			}
		}

	}



/* &node-cse!-make-box1358 */
	obj_t BGl_z62nodezd2csez12zd2makezd2box1358za2zzreduce_csez00(obj_t
		BgL_envz00_2414, obj_t BgL_nodez00_2415, obj_t BgL_stackz00_2416)
	{
		{	/* Reduce/cse.scm 276 */
			{	/* Reduce/cse.scm 278 */
				obj_t BgL_resetz00_2587;

				BgL_resetz00_2587 =
					BGl_nodezd2csez12zc0zzreduce_csez00(
					(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2415)))->BgL_valuez00),
					BgL_stackz00_2416);
				{	/* Reduce/cse.scm 279 */
					obj_t BgL_nvaluez00_2588;

					{	/* Reduce/cse.scm 280 */
						obj_t BgL_tmpz00_2589;

						{	/* Reduce/cse.scm 280 */
							int BgL_tmpz00_3240;

							BgL_tmpz00_3240 = (int) (1L);
							BgL_tmpz00_2589 = BGL_MVALUES_VAL(BgL_tmpz00_3240);
						}
						{	/* Reduce/cse.scm 280 */
							int BgL_tmpz00_3243;

							BgL_tmpz00_3243 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3243, BUNSPEC);
						}
						BgL_nvaluez00_2588 = BgL_tmpz00_2589;
					}
					((((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_2415)))->BgL_valuez00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nvaluez00_2588)),
						BUNSPEC);
					{	/* Reduce/cse.scm 281 */
						int BgL_tmpz00_3249;

						BgL_tmpz00_3249 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3249);
					}
					{	/* Reduce/cse.scm 281 */
						obj_t BgL_auxz00_3254;
						int BgL_tmpz00_3252;

						BgL_auxz00_3254 =
							((obj_t) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2415));
						BgL_tmpz00_3252 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_3252, BgL_auxz00_3254);
					}
					return BgL_resetz00_2587;
				}
			}
		}

	}



/* &node-cse!-return1356 */
	obj_t BGl_z62nodezd2csez12zd2return1356z70zzreduce_csez00(obj_t
		BgL_envz00_2417, obj_t BgL_nodez00_2418, obj_t BgL_stackz00_2419)
	{
		{	/* Reduce/cse.scm 266 */
			{	/* Reduce/cse.scm 268 */
				obj_t BgL_resetz00_2591;

				BgL_resetz00_2591 =
					BGl_nodezd2csez12zc0zzreduce_csez00(
					(((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nodez00_2418)))->BgL_valuez00),
					BgL_stackz00_2419);
				{	/* Reduce/cse.scm 269 */
					obj_t BgL_nvaluez00_2592;

					{	/* Reduce/cse.scm 270 */
						obj_t BgL_tmpz00_2593;

						{	/* Reduce/cse.scm 270 */
							int BgL_tmpz00_3261;

							BgL_tmpz00_3261 = (int) (1L);
							BgL_tmpz00_2593 = BGL_MVALUES_VAL(BgL_tmpz00_3261);
						}
						{	/* Reduce/cse.scm 270 */
							int BgL_tmpz00_3264;

							BgL_tmpz00_3264 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3264, BUNSPEC);
						}
						BgL_nvaluez00_2592 = BgL_tmpz00_2593;
					}
					((((BgL_returnz00_bglt) COBJECT(
									((BgL_returnz00_bglt) BgL_nodez00_2418)))->BgL_valuez00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nvaluez00_2592)),
						BUNSPEC);
					{	/* Reduce/cse.scm 271 */
						int BgL_tmpz00_3270;

						BgL_tmpz00_3270 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3270);
					}
					{	/* Reduce/cse.scm 271 */
						obj_t BgL_auxz00_3275;
						int BgL_tmpz00_3273;

						BgL_auxz00_3275 = ((obj_t) ((BgL_returnz00_bglt) BgL_nodez00_2418));
						BgL_tmpz00_3273 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_3273, BgL_auxz00_3275);
					}
					return BgL_resetz00_2591;
				}
			}
		}

	}



/* &node-cse!-retblock1354 */
	obj_t BGl_z62nodezd2csez12zd2retblock1354z70zzreduce_csez00(obj_t
		BgL_envz00_2420, obj_t BgL_nodez00_2421, obj_t BgL_stackz00_2422)
	{
		{	/* Reduce/cse.scm 256 */
			{	/* Reduce/cse.scm 258 */
				obj_t BgL_resetz00_2595;

				BgL_resetz00_2595 =
					BGl_nodezd2csez12zc0zzreduce_csez00(
					(((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nodez00_2421)))->BgL_bodyz00),
					BgL_stackz00_2422);
				{	/* Reduce/cse.scm 259 */
					obj_t BgL_nbodyz00_2596;

					{	/* Reduce/cse.scm 260 */
						obj_t BgL_tmpz00_2597;

						{	/* Reduce/cse.scm 260 */
							int BgL_tmpz00_3282;

							BgL_tmpz00_3282 = (int) (1L);
							BgL_tmpz00_2597 = BGL_MVALUES_VAL(BgL_tmpz00_3282);
						}
						{	/* Reduce/cse.scm 260 */
							int BgL_tmpz00_3285;

							BgL_tmpz00_3285 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3285, BUNSPEC);
						}
						BgL_nbodyz00_2596 = BgL_tmpz00_2597;
					}
					((((BgL_retblockz00_bglt) COBJECT(
									((BgL_retblockz00_bglt) BgL_nodez00_2421)))->BgL_bodyz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nbodyz00_2596)),
						BUNSPEC);
					{	/* Reduce/cse.scm 261 */
						int BgL_tmpz00_3291;

						BgL_tmpz00_3291 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3291);
					}
					{	/* Reduce/cse.scm 261 */
						obj_t BgL_auxz00_3296;
						int BgL_tmpz00_3294;

						BgL_auxz00_3296 =
							((obj_t) ((BgL_retblockz00_bglt) BgL_nodez00_2421));
						BgL_tmpz00_3294 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_3294, BgL_auxz00_3296);
					}
					return BgL_resetz00_2595;
				}
			}
		}

	}



/* &node-cse!-jump-ex-it1352 */
	obj_t BGl_z62nodezd2csez12zd2jumpzd2exzd2it1352z70zzreduce_csez00(obj_t
		BgL_envz00_2423, obj_t BgL_nodez00_2424, obj_t BgL_stackz00_2425)
	{
		{	/* Reduce/cse.scm 242 */
			{	/* Reduce/cse.scm 244 */
				obj_t BgL_resetz00_2599;

				BgL_resetz00_2599 =
					BGl_nodezd2csez12zc0zzreduce_csez00(
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2424)))->BgL_exitz00),
					BgL_stackz00_2425);
				{	/* Reduce/cse.scm 245 */
					obj_t BgL_nexitz00_2600;

					{	/* Reduce/cse.scm 246 */
						obj_t BgL_tmpz00_2601;

						{	/* Reduce/cse.scm 246 */
							int BgL_tmpz00_3303;

							BgL_tmpz00_3303 = (int) (1L);
							BgL_tmpz00_2601 = BGL_MVALUES_VAL(BgL_tmpz00_3303);
						}
						{	/* Reduce/cse.scm 246 */
							int BgL_tmpz00_3306;

							BgL_tmpz00_3306 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3306, BUNSPEC);
						}
						BgL_nexitz00_2600 = BgL_tmpz00_2601;
					}
					((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2424)))->
							BgL_exitz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nexitz00_2600)),
						BUNSPEC);
					{	/* Reduce/cse.scm 247 */
						obj_t BgL_stackz72z72_2602;

						if (CBOOL(BgL_resetz00_2599))
							{	/* Reduce/cse.scm 247 */
								BgL_stackz72z72_2602 = BNIL;
							}
						else
							{	/* Reduce/cse.scm 247 */
								BgL_stackz72z72_2602 = BgL_stackz00_2425;
							}
						{	/* Reduce/cse.scm 248 */
							obj_t BgL_resetz72z72_2603;

							BgL_resetz72z72_2603 =
								BGl_nodezd2csez12zc0zzreduce_csez00(
								(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
											((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2424)))->
									BgL_valuez00), BgL_stackz72z72_2602);
							{	/* Reduce/cse.scm 249 */
								obj_t BgL_nvaluez00_2604;

								{	/* Reduce/cse.scm 250 */
									obj_t BgL_tmpz00_2605;

									{	/* Reduce/cse.scm 250 */
										int BgL_tmpz00_3317;

										BgL_tmpz00_3317 = (int) (1L);
										BgL_tmpz00_2605 = BGL_MVALUES_VAL(BgL_tmpz00_3317);
									}
									{	/* Reduce/cse.scm 250 */
										int BgL_tmpz00_3320;

										BgL_tmpz00_3320 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_3320, BUNSPEC);
									}
									BgL_nvaluez00_2604 = BgL_tmpz00_2605;
								}
								((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
												((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2424)))->
										BgL_valuez00) =
									((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nvaluez00_2604)),
									BUNSPEC);
								{	/* Reduce/cse.scm 251 */
									obj_t BgL_val0_1291z00_2606;

									if (CBOOL(BgL_resetz00_2599))
										{	/* Reduce/cse.scm 251 */
											BgL_val0_1291z00_2606 = BgL_resetz00_2599;
										}
									else
										{	/* Reduce/cse.scm 251 */
											BgL_val0_1291z00_2606 = BgL_resetz72z72_2603;
										}
									{	/* Reduce/cse.scm 251 */
										int BgL_tmpz00_3328;

										BgL_tmpz00_3328 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3328);
									}
									{	/* Reduce/cse.scm 251 */
										obj_t BgL_auxz00_3333;
										int BgL_tmpz00_3331;

										BgL_auxz00_3333 =
											((obj_t) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2424));
										BgL_tmpz00_3331 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_3331, BgL_auxz00_3333);
									}
									return BgL_val0_1291z00_2606;
								}
							}
						}
					}
				}
			}
		}

	}



/* &node-cse!-set-ex-it1350 */
	obj_t BGl_z62nodezd2csez12zd2setzd2exzd2it1350z70zzreduce_csez00(obj_t
		BgL_envz00_2426, obj_t BgL_nodez00_2427, obj_t BgL_stackz00_2428)
	{
		{	/* Reduce/cse.scm 229 */
			{	/* Reduce/cse.scm 231 */
				obj_t BgL_resetz00_2608;

				BgL_resetz00_2608 =
					BGl_nodezd2csez12zc0zzreduce_csez00(
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2427)))->BgL_bodyz00),
					BNIL);
				{	/* Reduce/cse.scm 232 */
					obj_t BgL_nbodyz00_2609;

					{	/* Reduce/cse.scm 233 */
						obj_t BgL_tmpz00_2610;

						{	/* Reduce/cse.scm 233 */
							int BgL_tmpz00_3340;

							BgL_tmpz00_3340 = (int) (1L);
							BgL_tmpz00_2610 = BGL_MVALUES_VAL(BgL_tmpz00_3340);
						}
						{	/* Reduce/cse.scm 233 */
							int BgL_tmpz00_3343;

							BgL_tmpz00_3343 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3343, BUNSPEC);
						}
						BgL_nbodyz00_2609 = BgL_tmpz00_2610;
					}
					((((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2427)))->
							BgL_bodyz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nbodyz00_2609)),
						BUNSPEC);
					{	/* Reduce/cse.scm 234 */
						obj_t BgL_resetz72z72_2611;

						BgL_resetz72z72_2611 =
							BGl_nodezd2csez12zc0zzreduce_csez00(
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
										((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2427)))->
								BgL_onexitz00), BNIL);
						{	/* Reduce/cse.scm 235 */
							obj_t BgL_nonexitz00_2612;

							{	/* Reduce/cse.scm 236 */
								obj_t BgL_tmpz00_2613;

								{	/* Reduce/cse.scm 236 */
									int BgL_tmpz00_3352;

									BgL_tmpz00_3352 = (int) (1L);
									BgL_tmpz00_2613 = BGL_MVALUES_VAL(BgL_tmpz00_3352);
								}
								{	/* Reduce/cse.scm 236 */
									int BgL_tmpz00_3355;

									BgL_tmpz00_3355 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3355, BUNSPEC);
								}
								BgL_nonexitz00_2612 = BgL_tmpz00_2613;
							}
							((((BgL_setzd2exzd2itz00_bglt) COBJECT(
											((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2427)))->
									BgL_onexitz00) =
								((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nonexitz00_2612)),
								BUNSPEC);
							{	/* Reduce/cse.scm 237 */
								obj_t BgL_val0_1289z00_2614;

								if (CBOOL(BgL_resetz00_2608))
									{	/* Reduce/cse.scm 237 */
										BgL_val0_1289z00_2614 = BgL_resetz00_2608;
									}
								else
									{	/* Reduce/cse.scm 237 */
										BgL_val0_1289z00_2614 = BgL_resetz72z72_2611;
									}
								{	/* Reduce/cse.scm 237 */
									int BgL_tmpz00_3363;

									BgL_tmpz00_3363 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3363);
								}
								{	/* Reduce/cse.scm 237 */
									obj_t BgL_auxz00_3368;
									int BgL_tmpz00_3366;

									BgL_auxz00_3368 =
										((obj_t) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2427));
									BgL_tmpz00_3366 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3366, BgL_auxz00_3368);
								}
								return BgL_val0_1289z00_2614;
							}
						}
					}
				}
			}
		}

	}



/* &node-cse!-let-fun1348 */
	obj_t BGl_z62nodezd2csez12zd2letzd2fun1348za2zzreduce_csez00(obj_t
		BgL_envz00_2429, obj_t BgL_nodez00_2430, obj_t BgL_stackz00_2431)
	{
		{	/* Reduce/cse.scm 210 */
			{	/* Reduce/cse.scm 212 */
				obj_t BgL_resetz00_2616;

				BgL_resetz00_2616 =
					BGl_nodezd2csez12zc0zzreduce_csez00(
					(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2430)))->BgL_bodyz00),
					BgL_stackz00_2431);
				{	/* Reduce/cse.scm 213 */
					obj_t BgL_nbodyz00_2617;

					{	/* Reduce/cse.scm 214 */
						obj_t BgL_tmpz00_2618;

						{	/* Reduce/cse.scm 214 */
							int BgL_tmpz00_3375;

							BgL_tmpz00_3375 = (int) (1L);
							BgL_tmpz00_2618 = BGL_MVALUES_VAL(BgL_tmpz00_3375);
						}
						{	/* Reduce/cse.scm 214 */
							int BgL_tmpz00_3378;

							BgL_tmpz00_3378 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3378, BUNSPEC);
						}
						BgL_nbodyz00_2617 = BgL_tmpz00_2618;
					}
					((((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nodez00_2430)))->BgL_bodyz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nbodyz00_2617)),
						BUNSPEC);
					{
						obj_t BgL_localsz00_2620;
						obj_t BgL_resetz00_2621;

						BgL_localsz00_2620 =
							(((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nodez00_2430)))->BgL_localsz00);
						BgL_resetz00_2621 = BgL_resetz00_2616;
					BgL_loopz00_2619:
						if (NULLP(BgL_localsz00_2620))
							{	/* Reduce/cse.scm 217 */
								{	/* Reduce/cse.scm 218 */
									int BgL_tmpz00_3386;

									BgL_tmpz00_3386 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3386);
								}
								{	/* Reduce/cse.scm 218 */
									obj_t BgL_auxz00_3391;
									int BgL_tmpz00_3389;

									BgL_auxz00_3391 =
										((obj_t) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2430));
									BgL_tmpz00_3389 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3389, BgL_auxz00_3391);
								}
								return BgL_resetz00_2621;
							}
						else
							{	/* Reduce/cse.scm 219 */
								obj_t BgL_localz00_2622;

								BgL_localz00_2622 = CAR(((obj_t) BgL_localsz00_2620));
								{	/* Reduce/cse.scm 219 */
									BgL_valuez00_bglt BgL_sfunz00_2623;

									BgL_sfunz00_2623 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2622))))->
										BgL_valuez00);
									{	/* Reduce/cse.scm 220 */

										{	/* Reduce/cse.scm 221 */
											obj_t BgL_resetz72z72_2624;

											{	/* Reduce/cse.scm 222 */
												obj_t BgL_arg1626z00_2625;

												BgL_arg1626z00_2625 =
													(((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_sfunz00_2623)))->
													BgL_bodyz00);
												BgL_resetz72z72_2624 =
													BGl_nodezd2csez12zc0zzreduce_csez00((
														(BgL_nodez00_bglt) BgL_arg1626z00_2625), BNIL);
											}
											{	/* Reduce/cse.scm 222 */
												obj_t BgL_nbodyz00_2626;

												{	/* Reduce/cse.scm 223 */
													obj_t BgL_tmpz00_2627;

													{	/* Reduce/cse.scm 223 */
														int BgL_tmpz00_3404;

														BgL_tmpz00_3404 = (int) (1L);
														BgL_tmpz00_2627 = BGL_MVALUES_VAL(BgL_tmpz00_3404);
													}
													{	/* Reduce/cse.scm 223 */
														int BgL_tmpz00_3407;

														BgL_tmpz00_3407 = (int) (1L);
														BGL_MVALUES_VAL_SET(BgL_tmpz00_3407, BUNSPEC);
													}
													BgL_nbodyz00_2626 = BgL_tmpz00_2627;
												}
												((((BgL_sfunz00_bglt) COBJECT(
																((BgL_sfunz00_bglt) BgL_sfunz00_2623)))->
														BgL_bodyz00) =
													((obj_t) BgL_nbodyz00_2626), BUNSPEC);
												{	/* Reduce/cse.scm 224 */
													obj_t BgL_arg1616z00_2628;
													obj_t BgL_arg1625z00_2629;

													BgL_arg1616z00_2628 =
														CDR(((obj_t) BgL_localsz00_2620));
													if (CBOOL(BgL_resetz00_2621))
														{	/* Reduce/cse.scm 224 */
															BgL_arg1625z00_2629 = BgL_resetz00_2621;
														}
													else
														{	/* Reduce/cse.scm 224 */
															BgL_arg1625z00_2629 = BgL_resetz72z72_2624;
														}
													{
														obj_t BgL_resetz00_3417;
														obj_t BgL_localsz00_3416;

														BgL_localsz00_3416 = BgL_arg1616z00_2628;
														BgL_resetz00_3417 = BgL_arg1625z00_2629;
														BgL_resetz00_2621 = BgL_resetz00_3417;
														BgL_localsz00_2620 = BgL_localsz00_3416;
														goto BgL_loopz00_2619;
													}
												}
											}
										}
									}
								}
							}
					}
				}
			}
		}

	}



/* &node-cse!-switch1346 */
	obj_t BGl_z62nodezd2csez12zd2switch1346z70zzreduce_csez00(obj_t
		BgL_envz00_2432, obj_t BgL_nodez00_2433, obj_t BgL_stackz00_2434)
	{
		{	/* Reduce/cse.scm 191 */
			{	/* Reduce/cse.scm 193 */
				obj_t BgL_resetz00_2631;

				BgL_resetz00_2631 =
					BGl_nodezd2csez12zc0zzreduce_csez00(
					(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2433)))->BgL_testz00),
					BgL_stackz00_2434);
				{	/* Reduce/cse.scm 194 */
					obj_t BgL_ntestz00_2632;

					{	/* Reduce/cse.scm 195 */
						obj_t BgL_tmpz00_2633;

						{	/* Reduce/cse.scm 195 */
							int BgL_tmpz00_3423;

							BgL_tmpz00_3423 = (int) (1L);
							BgL_tmpz00_2633 = BGL_MVALUES_VAL(BgL_tmpz00_3423);
						}
						{	/* Reduce/cse.scm 195 */
							int BgL_tmpz00_3426;

							BgL_tmpz00_3426 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3426, BUNSPEC);
						}
						BgL_ntestz00_2632 = BgL_tmpz00_2633;
					}
					((((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_2433)))->BgL_testz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_ntestz00_2632)),
						BUNSPEC);
					{
						obj_t BgL_clausesz00_2635;
						obj_t BgL_resetz00_2636;

						BgL_clausesz00_2635 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_2433)))->BgL_clausesz00);
						BgL_resetz00_2636 = BgL_resetz00_2631;
					BgL_loopz00_2634:
						if (NULLP(BgL_clausesz00_2635))
							{	/* Reduce/cse.scm 199 */
								{	/* Reduce/cse.scm 200 */
									int BgL_tmpz00_3434;

									BgL_tmpz00_3434 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3434);
								}
								{	/* Reduce/cse.scm 200 */
									obj_t BgL_auxz00_3439;
									int BgL_tmpz00_3437;

									BgL_auxz00_3439 =
										((obj_t) ((BgL_switchz00_bglt) BgL_nodez00_2433));
									BgL_tmpz00_3437 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3437, BgL_auxz00_3439);
								}
								return BgL_resetz00_2636;
							}
						else
							{	/* Reduce/cse.scm 201 */
								obj_t BgL_clausez00_2637;

								BgL_clausez00_2637 = CAR(((obj_t) BgL_clausesz00_2635));
								{	/* Reduce/cse.scm 202 */
									obj_t BgL_resetz72z72_2638;

									{	/* Reduce/cse.scm 203 */
										obj_t BgL_arg1609z00_2639;

										BgL_arg1609z00_2639 = CDR(((obj_t) BgL_clausez00_2637));
										BgL_resetz72z72_2638 =
											BGl_nodezd2csez12zc0zzreduce_csez00(
											((BgL_nodez00_bglt) BgL_arg1609z00_2639),
											BgL_stackz00_2434);
									}
									{	/* Reduce/cse.scm 203 */
										obj_t BgL_nclausez00_2640;

										{	/* Reduce/cse.scm 204 */
											obj_t BgL_tmpz00_2641;

											{	/* Reduce/cse.scm 204 */
												int BgL_tmpz00_3449;

												BgL_tmpz00_3449 = (int) (1L);
												BgL_tmpz00_2641 = BGL_MVALUES_VAL(BgL_tmpz00_3449);
											}
											{	/* Reduce/cse.scm 204 */
												int BgL_tmpz00_3452;

												BgL_tmpz00_3452 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_3452, BUNSPEC);
											}
											BgL_nclausez00_2640 = BgL_tmpz00_2641;
										}
										{	/* Reduce/cse.scm 204 */
											obj_t BgL_tmpz00_3455;

											BgL_tmpz00_3455 = ((obj_t) BgL_clausez00_2637);
											SET_CDR(BgL_tmpz00_3455, BgL_nclausez00_2640);
										}
										{	/* Reduce/cse.scm 205 */
											obj_t BgL_arg1605z00_2642;
											obj_t BgL_arg1606z00_2643;

											BgL_arg1605z00_2642 = CDR(((obj_t) BgL_clausesz00_2635));
											if (CBOOL(BgL_resetz00_2636))
												{	/* Reduce/cse.scm 205 */
													BgL_arg1606z00_2643 = BgL_resetz00_2636;
												}
											else
												{	/* Reduce/cse.scm 205 */
													BgL_arg1606z00_2643 = BgL_resetz72z72_2638;
												}
											{
												obj_t BgL_resetz00_3463;
												obj_t BgL_clausesz00_3462;

												BgL_clausesz00_3462 = BgL_arg1605z00_2642;
												BgL_resetz00_3463 = BgL_arg1606z00_2643;
												BgL_resetz00_2636 = BgL_resetz00_3463;
												BgL_clausesz00_2635 = BgL_clausesz00_3462;
												goto BgL_loopz00_2634;
											}
										}
									}
								}
							}
					}
				}
			}
		}

	}



/* &node-cse!-fail1344 */
	obj_t BGl_z62nodezd2csez12zd2fail1344z70zzreduce_csez00(obj_t BgL_envz00_2435,
		obj_t BgL_nodez00_2436, obj_t BgL_stackz00_2437)
	{
		{	/* Reduce/cse.scm 173 */
			{	/* Reduce/cse.scm 175 */
				obj_t BgL_resetz00_2645;

				BgL_resetz00_2645 =
					BGl_nodezd2csez12zc0zzreduce_csez00(
					(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2436)))->BgL_procz00),
					BgL_stackz00_2437);
				{	/* Reduce/cse.scm 176 */
					obj_t BgL_nprocz00_2646;

					{	/* Reduce/cse.scm 177 */
						obj_t BgL_tmpz00_2647;

						{	/* Reduce/cse.scm 177 */
							int BgL_tmpz00_3469;

							BgL_tmpz00_3469 = (int) (1L);
							BgL_tmpz00_2647 = BGL_MVALUES_VAL(BgL_tmpz00_3469);
						}
						{	/* Reduce/cse.scm 177 */
							int BgL_tmpz00_3472;

							BgL_tmpz00_3472 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3472, BUNSPEC);
						}
						BgL_nprocz00_2646 = BgL_tmpz00_2647;
					}
					((((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_2436)))->BgL_procz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nprocz00_2646)),
						BUNSPEC);
					{	/* Reduce/cse.scm 178 */
						obj_t BgL_stackz72z72_2648;

						if (CBOOL(BgL_resetz00_2645))
							{	/* Reduce/cse.scm 178 */
								BgL_stackz72z72_2648 = BNIL;
							}
						else
							{	/* Reduce/cse.scm 178 */
								BgL_stackz72z72_2648 = BgL_stackz00_2437;
							}
						{	/* Reduce/cse.scm 179 */
							obj_t BgL_resetz72z72_2649;

							BgL_resetz72z72_2649 =
								BGl_nodezd2csez12zc0zzreduce_csez00(
								(((BgL_failz00_bglt) COBJECT(
											((BgL_failz00_bglt) BgL_nodez00_2436)))->BgL_msgz00),
								BgL_stackz72z72_2648);
							{	/* Reduce/cse.scm 180 */
								obj_t BgL_nmsgz00_2650;

								{	/* Reduce/cse.scm 181 */
									obj_t BgL_tmpz00_2651;

									{	/* Reduce/cse.scm 181 */
										int BgL_tmpz00_3483;

										BgL_tmpz00_3483 = (int) (1L);
										BgL_tmpz00_2651 = BGL_MVALUES_VAL(BgL_tmpz00_3483);
									}
									{	/* Reduce/cse.scm 181 */
										int BgL_tmpz00_3486;

										BgL_tmpz00_3486 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_3486, BUNSPEC);
									}
									BgL_nmsgz00_2650 = BgL_tmpz00_2651;
								}
								((((BgL_failz00_bglt) COBJECT(
												((BgL_failz00_bglt) BgL_nodez00_2436)))->BgL_msgz00) =
									((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nmsgz00_2650)),
									BUNSPEC);
								{	/* Reduce/cse.scm 182 */
									obj_t BgL_stackz72z72z00_2652;

									if (CBOOL(BgL_resetz72z72_2649))
										{	/* Reduce/cse.scm 182 */
											BgL_stackz72z72z00_2652 = BNIL;
										}
									else
										{	/* Reduce/cse.scm 182 */
											BgL_stackz72z72z00_2652 = BgL_stackz72z72_2648;
										}
									{	/* Reduce/cse.scm 183 */
										obj_t BgL_resetz72z72z00_2653;

										BgL_resetz72z72z00_2653 =
											BGl_nodezd2csez12zc0zzreduce_csez00(
											(((BgL_failz00_bglt) COBJECT(
														((BgL_failz00_bglt) BgL_nodez00_2436)))->
												BgL_objz00), BgL_stackz72z72z00_2652);
										{	/* Reduce/cse.scm 184 */
											obj_t BgL_nobjz00_2654;

											{	/* Reduce/cse.scm 185 */
												obj_t BgL_tmpz00_2655;

												{	/* Reduce/cse.scm 185 */
													int BgL_tmpz00_3497;

													BgL_tmpz00_3497 = (int) (1L);
													BgL_tmpz00_2655 = BGL_MVALUES_VAL(BgL_tmpz00_3497);
												}
												{	/* Reduce/cse.scm 185 */
													int BgL_tmpz00_3500;

													BgL_tmpz00_3500 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_3500, BUNSPEC);
												}
												BgL_nobjz00_2654 = BgL_tmpz00_2655;
											}
											((((BgL_failz00_bglt) COBJECT(
															((BgL_failz00_bglt) BgL_nodez00_2436)))->
													BgL_objz00) =
												((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
														BgL_nobjz00_2654)), BUNSPEC);
											{	/* Reduce/cse.scm 186 */
												obj_t BgL_val0_1283z00_2656;

												if (CBOOL(BgL_resetz00_2645))
													{	/* Reduce/cse.scm 186 */
														BgL_val0_1283z00_2656 = BgL_resetz00_2645;
													}
												else
													{	/* Reduce/cse.scm 186 */
														if (CBOOL(BgL_resetz72z72_2649))
															{	/* Reduce/cse.scm 186 */
																BgL_val0_1283z00_2656 = BgL_resetz72z72_2649;
															}
														else
															{	/* Reduce/cse.scm 186 */
																BgL_val0_1283z00_2656 = BgL_resetz72z72z00_2653;
															}
													}
												{	/* Reduce/cse.scm 186 */
													int BgL_tmpz00_3510;

													BgL_tmpz00_3510 = (int) (2L);
													BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3510);
												}
												{	/* Reduce/cse.scm 186 */
													obj_t BgL_auxz00_3515;
													int BgL_tmpz00_3513;

													BgL_auxz00_3515 =
														((obj_t) ((BgL_failz00_bglt) BgL_nodez00_2436));
													BgL_tmpz00_3513 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_3513, BgL_auxz00_3515);
												}
												return BgL_val0_1283z00_2656;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &node-cse!-conditiona1342 */
	obj_t BGl_z62nodezd2csez12zd2conditiona1342z70zzreduce_csez00(obj_t
		BgL_envz00_2438, obj_t BgL_nodez00_2439, obj_t BgL_stackz00_2440)
	{
		{	/* Reduce/cse.scm 156 */
			{	/* Reduce/cse.scm 158 */
				obj_t BgL_resetz00_2658;

				BgL_resetz00_2658 =
					BGl_nodezd2csez12zc0zzreduce_csez00(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2439)))->BgL_testz00),
					BgL_stackz00_2440);
				{	/* Reduce/cse.scm 159 */
					obj_t BgL_ntestz00_2659;

					{	/* Reduce/cse.scm 160 */
						obj_t BgL_tmpz00_2660;

						{	/* Reduce/cse.scm 160 */
							int BgL_tmpz00_3522;

							BgL_tmpz00_3522 = (int) (1L);
							BgL_tmpz00_2660 = BGL_MVALUES_VAL(BgL_tmpz00_3522);
						}
						{	/* Reduce/cse.scm 160 */
							int BgL_tmpz00_3525;

							BgL_tmpz00_3525 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3525, BUNSPEC);
						}
						BgL_ntestz00_2659 = BgL_tmpz00_2660;
					}
					((((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_2439)))->BgL_testz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_ntestz00_2659)),
						BUNSPEC);
					{	/* Reduce/cse.scm 161 */
						obj_t BgL_stackz72z72_2661;

						if (CBOOL(BgL_resetz00_2658))
							{	/* Reduce/cse.scm 161 */
								BgL_stackz72z72_2661 = BNIL;
							}
						else
							{	/* Reduce/cse.scm 161 */
								BgL_stackz72z72_2661 = BgL_stackz00_2440;
							}
						{	/* Reduce/cse.scm 162 */
							obj_t BgL_resetz72z72_2662;

							BgL_resetz72z72_2662 =
								BGl_nodezd2csez12zc0zzreduce_csez00(
								(((BgL_conditionalz00_bglt) COBJECT(
											((BgL_conditionalz00_bglt) BgL_nodez00_2439)))->
									BgL_truez00), BgL_stackz72z72_2661);
							{	/* Reduce/cse.scm 163 */
								obj_t BgL_ntruez00_2663;

								{	/* Reduce/cse.scm 164 */
									obj_t BgL_tmpz00_2664;

									{	/* Reduce/cse.scm 164 */
										int BgL_tmpz00_3536;

										BgL_tmpz00_3536 = (int) (1L);
										BgL_tmpz00_2664 = BGL_MVALUES_VAL(BgL_tmpz00_3536);
									}
									{	/* Reduce/cse.scm 164 */
										int BgL_tmpz00_3539;

										BgL_tmpz00_3539 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_3539, BUNSPEC);
									}
									BgL_ntruez00_2663 = BgL_tmpz00_2664;
								}
								((((BgL_conditionalz00_bglt) COBJECT(
												((BgL_conditionalz00_bglt) BgL_nodez00_2439)))->
										BgL_truez00) =
									((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_ntruez00_2663)),
									BUNSPEC);
								{	/* Reduce/cse.scm 165 */
									obj_t BgL_resetz72z72z00_2665;

									BgL_resetz72z72z00_2665 =
										BGl_nodezd2csez12zc0zzreduce_csez00(
										(((BgL_conditionalz00_bglt) COBJECT(
													((BgL_conditionalz00_bglt) BgL_nodez00_2439)))->
											BgL_falsez00), BgL_stackz72z72_2661);
									{	/* Reduce/cse.scm 166 */
										obj_t BgL_nfalsez00_2666;

										{	/* Reduce/cse.scm 167 */
											obj_t BgL_tmpz00_2667;

											{	/* Reduce/cse.scm 167 */
												int BgL_tmpz00_3548;

												BgL_tmpz00_3548 = (int) (1L);
												BgL_tmpz00_2667 = BGL_MVALUES_VAL(BgL_tmpz00_3548);
											}
											{	/* Reduce/cse.scm 167 */
												int BgL_tmpz00_3551;

												BgL_tmpz00_3551 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_3551, BUNSPEC);
											}
											BgL_nfalsez00_2666 = BgL_tmpz00_2667;
										}
										((((BgL_conditionalz00_bglt) COBJECT(
														((BgL_conditionalz00_bglt) BgL_nodez00_2439)))->
												BgL_falsez00) =
											((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
													BgL_nfalsez00_2666)), BUNSPEC);
										{	/* Reduce/cse.scm 168 */
											obj_t BgL_val0_1281z00_2668;

											if (CBOOL(BgL_resetz00_2658))
												{	/* Reduce/cse.scm 168 */
													BgL_val0_1281z00_2668 = BgL_resetz00_2658;
												}
											else
												{	/* Reduce/cse.scm 168 */
													if (CBOOL(BgL_resetz72z72_2662))
														{	/* Reduce/cse.scm 168 */
															BgL_val0_1281z00_2668 = BgL_resetz72z72_2662;
														}
													else
														{	/* Reduce/cse.scm 168 */
															BgL_val0_1281z00_2668 = BgL_resetz72z72z00_2665;
														}
												}
											{	/* Reduce/cse.scm 168 */
												int BgL_tmpz00_3561;

												BgL_tmpz00_3561 = (int) (2L);
												BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3561);
											}
											{	/* Reduce/cse.scm 168 */
												obj_t BgL_auxz00_3566;
												int BgL_tmpz00_3564;

												BgL_auxz00_3566 =
													((obj_t)
													((BgL_conditionalz00_bglt) BgL_nodez00_2439));
												BgL_tmpz00_3564 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_3564, BgL_auxz00_3566);
											}
											return BgL_val0_1281z00_2668;
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &node-cse!-setq1340 */
	obj_t BGl_z62nodezd2csez12zd2setq1340z70zzreduce_csez00(obj_t BgL_envz00_2441,
		obj_t BgL_nodez00_2442, obj_t BgL_stackz00_2443)
	{
		{	/* Reduce/cse.scm 146 */
			{	/* Reduce/cse.scm 148 */
				obj_t BgL_resetz00_2670;

				BgL_resetz00_2670 =
					BGl_nodezd2csez12zc0zzreduce_csez00(
					(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2442)))->BgL_valuez00),
					BgL_stackz00_2443);
				{	/* Reduce/cse.scm 149 */
					obj_t BgL_nvaluez00_2671;

					{	/* Reduce/cse.scm 150 */
						obj_t BgL_tmpz00_2672;

						{	/* Reduce/cse.scm 150 */
							int BgL_tmpz00_3573;

							BgL_tmpz00_3573 = (int) (1L);
							BgL_tmpz00_2672 = BGL_MVALUES_VAL(BgL_tmpz00_3573);
						}
						{	/* Reduce/cse.scm 150 */
							int BgL_tmpz00_3576;

							BgL_tmpz00_3576 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3576, BUNSPEC);
						}
						BgL_nvaluez00_2671 = BgL_tmpz00_2672;
					}
					((((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nodez00_2442)))->BgL_valuez00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nvaluez00_2671)),
						BUNSPEC);
					{	/* Reduce/cse.scm 151 */
						int BgL_tmpz00_3582;

						BgL_tmpz00_3582 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3582);
					}
					{	/* Reduce/cse.scm 151 */
						obj_t BgL_auxz00_3587;
						int BgL_tmpz00_3585;

						BgL_auxz00_3587 = ((obj_t) ((BgL_setqz00_bglt) BgL_nodez00_2442));
						BgL_tmpz00_3585 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_3585, BgL_auxz00_3587);
					}
					return BgL_resetz00_2670;
				}
			}
		}

	}



/* &node-cse!-cast1338 */
	obj_t BGl_z62nodezd2csez12zd2cast1338z70zzreduce_csez00(obj_t BgL_envz00_2444,
		obj_t BgL_nodez00_2445, obj_t BgL_stackz00_2446)
	{
		{	/* Reduce/cse.scm 136 */
			{	/* Reduce/cse.scm 138 */
				obj_t BgL_resetz00_2674;

				BgL_resetz00_2674 =
					BGl_nodezd2csez12zc0zzreduce_csez00(
					(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_2445)))->BgL_argz00),
					BgL_stackz00_2446);
				{	/* Reduce/cse.scm 139 */
					obj_t BgL_nargz00_2675;

					{	/* Reduce/cse.scm 140 */
						obj_t BgL_tmpz00_2676;

						{	/* Reduce/cse.scm 140 */
							int BgL_tmpz00_3594;

							BgL_tmpz00_3594 = (int) (1L);
							BgL_tmpz00_2676 = BGL_MVALUES_VAL(BgL_tmpz00_3594);
						}
						{	/* Reduce/cse.scm 140 */
							int BgL_tmpz00_3597;

							BgL_tmpz00_3597 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3597, BUNSPEC);
						}
						BgL_nargz00_2675 = BgL_tmpz00_2676;
					}
					((((BgL_castz00_bglt) COBJECT(
									((BgL_castz00_bglt) BgL_nodez00_2445)))->BgL_argz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nargz00_2675)),
						BUNSPEC);
					{	/* Reduce/cse.scm 141 */
						int BgL_tmpz00_3603;

						BgL_tmpz00_3603 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3603);
					}
					{	/* Reduce/cse.scm 141 */
						obj_t BgL_auxz00_3608;
						int BgL_tmpz00_3606;

						BgL_auxz00_3608 = ((obj_t) ((BgL_castz00_bglt) BgL_nodez00_2445));
						BgL_tmpz00_3606 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_3606, BgL_auxz00_3608);
					}
					return BgL_resetz00_2674;
				}
			}
		}

	}



/* &node-cse!-extern1336 */
	obj_t BGl_z62nodezd2csez12zd2extern1336z70zzreduce_csez00(obj_t
		BgL_envz00_2447, obj_t BgL_nodez00_2448, obj_t BgL_stackz00_2449)
	{
		{	/* Reduce/cse.scm 128 */
			{	/* Reduce/cse.scm 130 */
				obj_t BgL_resetz72z72_2678;

				BgL_resetz72z72_2678 =
					BGl_nodezd2cseza2z12z62zzreduce_csez00(
					(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_2448)))->BgL_exprza2za2),
					BgL_stackz00_2449);
				{	/* Reduce/cse.scm 131 */
					int BgL_tmpz00_3615;

					BgL_tmpz00_3615 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3615);
				}
				{	/* Reduce/cse.scm 131 */
					obj_t BgL_auxz00_3620;
					int BgL_tmpz00_3618;

					BgL_auxz00_3620 = ((obj_t) ((BgL_externz00_bglt) BgL_nodez00_2448));
					BgL_tmpz00_3618 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_3618, BgL_auxz00_3620);
				}
				return BgL_resetz72z72_2678;
			}
		}

	}



/* &node-cse!-funcall1334 */
	obj_t BGl_z62nodezd2csez12zd2funcall1334z70zzreduce_csez00(obj_t
		BgL_envz00_2450, obj_t BgL_nodez00_2451, obj_t BgL_stackz00_2452)
	{
		{	/* Reduce/cse.scm 120 */
			{	/* Reduce/cse.scm 122 */
				obj_t BgL_resetz72z72_2680;

				BgL_resetz72z72_2680 =
					BGl_nodezd2cseza2z12z62zzreduce_csez00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2451)))->BgL_argsz00),
					BgL_stackz00_2452);
				{	/* Reduce/cse.scm 123 */
					int BgL_tmpz00_3627;

					BgL_tmpz00_3627 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3627);
				}
				{	/* Reduce/cse.scm 123 */
					obj_t BgL_auxz00_3632;
					int BgL_tmpz00_3630;

					BgL_auxz00_3632 = ((obj_t) ((BgL_funcallz00_bglt) BgL_nodez00_2451));
					BgL_tmpz00_3630 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_3630, BgL_auxz00_3632);
				}
				return BgL_resetz72z72_2680;
			}
		}

	}



/* &node-cse!-app-ly1332 */
	obj_t BGl_z62nodezd2csez12zd2appzd2ly1332za2zzreduce_csez00(obj_t
		BgL_envz00_2453, obj_t BgL_nodez00_2454, obj_t BgL_stackz00_2455)
	{
		{	/* Reduce/cse.scm 107 */
			{	/* Reduce/cse.scm 109 */
				obj_t BgL_resetz00_2682;

				BgL_resetz00_2682 =
					BGl_nodezd2csez12zc0zzreduce_csez00(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2454)))->BgL_funz00),
					BgL_stackz00_2455);
				{	/* Reduce/cse.scm 110 */
					obj_t BgL_nfunz00_2683;

					{	/* Reduce/cse.scm 111 */
						obj_t BgL_tmpz00_2684;

						{	/* Reduce/cse.scm 111 */
							int BgL_tmpz00_3639;

							BgL_tmpz00_3639 = (int) (1L);
							BgL_tmpz00_2684 = BGL_MVALUES_VAL(BgL_tmpz00_3639);
						}
						{	/* Reduce/cse.scm 111 */
							int BgL_tmpz00_3642;

							BgL_tmpz00_3642 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3642, BUNSPEC);
						}
						BgL_nfunz00_2683 = BgL_tmpz00_2684;
					}
					((((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_2454)))->BgL_funz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nfunz00_2683)),
						BUNSPEC);
					{	/* Reduce/cse.scm 112 */
						obj_t BgL_resetz72z72_2685;

						{	/* Reduce/cse.scm 113 */
							BgL_nodez00_bglt BgL_arg1564z00_2686;
							obj_t BgL_arg1565z00_2687;

							BgL_arg1564z00_2686 =
								(((BgL_appzd2lyzd2_bglt) COBJECT(
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_2454)))->BgL_argz00);
							if (CBOOL(BgL_resetz00_2682))
								{	/* Reduce/cse.scm 113 */
									BgL_arg1565z00_2687 = BNIL;
								}
							else
								{	/* Reduce/cse.scm 113 */
									BgL_arg1565z00_2687 = BgL_stackz00_2455;
								}
							BgL_resetz72z72_2685 =
								BGl_nodezd2csez12zc0zzreduce_csez00(BgL_arg1564z00_2686,
								BgL_arg1565z00_2687);
						}
						{	/* Reduce/cse.scm 113 */
							obj_t BgL_nargz00_2688;

							{	/* Reduce/cse.scm 114 */
								obj_t BgL_tmpz00_2689;

								{	/* Reduce/cse.scm 114 */
									int BgL_tmpz00_3653;

									BgL_tmpz00_3653 = (int) (1L);
									BgL_tmpz00_2689 = BGL_MVALUES_VAL(BgL_tmpz00_3653);
								}
								{	/* Reduce/cse.scm 114 */
									int BgL_tmpz00_3656;

									BgL_tmpz00_3656 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3656, BUNSPEC);
								}
								BgL_nargz00_2688 = BgL_tmpz00_2689;
							}
							((((BgL_appzd2lyzd2_bglt) COBJECT(
											((BgL_appzd2lyzd2_bglt) BgL_nodez00_2454)))->BgL_argz00) =
								((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nargz00_2688)),
								BUNSPEC);
							{	/* Reduce/cse.scm 115 */
								obj_t BgL_val0_1271z00_2690;

								if (CBOOL(BgL_resetz00_2682))
									{	/* Reduce/cse.scm 115 */
										BgL_val0_1271z00_2690 = BgL_resetz00_2682;
									}
								else
									{	/* Reduce/cse.scm 115 */
										BgL_val0_1271z00_2690 = BgL_resetz72z72_2685;
									}
								{	/* Reduce/cse.scm 115 */
									int BgL_tmpz00_3664;

									BgL_tmpz00_3664 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3664);
								}
								{	/* Reduce/cse.scm 115 */
									obj_t BgL_auxz00_3669;
									int BgL_tmpz00_3667;

									BgL_auxz00_3669 =
										((obj_t) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2454));
									BgL_tmpz00_3667 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3667, BgL_auxz00_3669);
								}
								return BgL_val0_1271z00_2690;
							}
						}
					}
				}
			}
		}

	}



/* &node-cse!-sync1330 */
	obj_t BGl_z62nodezd2csez12zd2sync1330z70zzreduce_csez00(obj_t BgL_envz00_2456,
		obj_t BgL_nodez00_2457, obj_t BgL_stackz00_2458)
	{
		{	/* Reduce/cse.scm 94 */
			{	/* Reduce/cse.scm 96 */
				obj_t BgL_resetmz00_2692;

				BgL_resetmz00_2692 =
					BGl_nodezd2csez12zc0zzreduce_csez00(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2457)))->BgL_mutexz00),
					BgL_stackz00_2458);
				{	/* Reduce/cse.scm 97 */
					obj_t BgL_nmutexz00_2693;

					{	/* Reduce/cse.scm 98 */
						obj_t BgL_tmpz00_2694;

						{	/* Reduce/cse.scm 98 */
							int BgL_tmpz00_3676;

							BgL_tmpz00_3676 = (int) (1L);
							BgL_tmpz00_2694 = BGL_MVALUES_VAL(BgL_tmpz00_3676);
						}
						{	/* Reduce/cse.scm 98 */
							int BgL_tmpz00_3679;

							BgL_tmpz00_3679 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3679, BUNSPEC);
						}
						BgL_nmutexz00_2693 = BgL_tmpz00_2694;
					}
					((((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_2457)))->BgL_mutexz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nmutexz00_2693)),
						BUNSPEC);
					{	/* Reduce/cse.scm 99 */
						obj_t BgL_resetpz00_2695;

						BgL_resetpz00_2695 =
							BGl_nodezd2csez12zc0zzreduce_csez00(
							(((BgL_syncz00_bglt) COBJECT(
										((BgL_syncz00_bglt) BgL_nodez00_2457)))->BgL_prelockz00),
							BgL_stackz00_2458);
						{	/* Reduce/cse.scm 100 */
							obj_t BgL_nprelockz00_2696;

							{	/* Reduce/cse.scm 101 */
								obj_t BgL_tmpz00_2697;

								{	/* Reduce/cse.scm 101 */
									int BgL_tmpz00_3688;

									BgL_tmpz00_3688 = (int) (1L);
									BgL_tmpz00_2697 = BGL_MVALUES_VAL(BgL_tmpz00_3688);
								}
								{	/* Reduce/cse.scm 101 */
									int BgL_tmpz00_3691;

									BgL_tmpz00_3691 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3691, BUNSPEC);
								}
								BgL_nprelockz00_2696 = BgL_tmpz00_2697;
							}
							((((BgL_syncz00_bglt) COBJECT(
											((BgL_syncz00_bglt) BgL_nodez00_2457)))->BgL_prelockz00) =
								((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nprelockz00_2696)),
								BUNSPEC);
							{	/* Reduce/cse.scm 102 */
								obj_t BgL_val0_1269z00_2698;

								{	/* Reduce/cse.scm 102 */
									obj_t BgL__ortest_1108z00_2699;

									BgL__ortest_1108z00_2699 =
										BGl_nodezd2csez12zc0zzreduce_csez00(
										(((BgL_syncz00_bglt) COBJECT(
													((BgL_syncz00_bglt) BgL_nodez00_2457)))->BgL_bodyz00),
										BgL_stackz00_2458);
									if (CBOOL(BgL__ortest_1108z00_2699))
										{	/* Reduce/cse.scm 102 */
											BgL_val0_1269z00_2698 = BgL__ortest_1108z00_2699;
										}
									else
										{	/* Reduce/cse.scm 102 */
											if (CBOOL(BgL_resetmz00_2692))
												{	/* Reduce/cse.scm 102 */
													BgL_val0_1269z00_2698 = BgL_resetmz00_2692;
												}
											else
												{	/* Reduce/cse.scm 102 */
													BgL_val0_1269z00_2698 = BgL_resetpz00_2695;
												}
										}
								}
								{	/* Reduce/cse.scm 102 */
									int BgL_tmpz00_3704;

									BgL_tmpz00_3704 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3704);
								}
								{	/* Reduce/cse.scm 102 */
									obj_t BgL_auxz00_3709;
									int BgL_tmpz00_3707;

									BgL_auxz00_3709 =
										((obj_t) ((BgL_syncz00_bglt) BgL_nodez00_2457));
									BgL_tmpz00_3707 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3707, BgL_auxz00_3709);
								}
								return BgL_val0_1269z00_2698;
							}
						}
					}
				}
			}
		}

	}



/* &node-cse!-sequence1328 */
	obj_t BGl_z62nodezd2csez12zd2sequence1328z70zzreduce_csez00(obj_t
		BgL_envz00_2459, obj_t BgL_nodez00_2460, obj_t BgL_stackz00_2461)
	{
		{	/* Reduce/cse.scm 87 */
			{	/* Reduce/cse.scm 89 */
				obj_t BgL_val0_1267z00_2701;

				BgL_val0_1267z00_2701 =
					BGl_nodezd2cseza2z12z62zzreduce_csez00(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_2460)))->BgL_nodesz00),
					BgL_stackz00_2461);
				{	/* Reduce/cse.scm 89 */
					int BgL_tmpz00_3716;

					BgL_tmpz00_3716 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3716);
				}
				{	/* Reduce/cse.scm 89 */
					obj_t BgL_auxz00_3721;
					int BgL_tmpz00_3719;

					BgL_auxz00_3721 = ((obj_t) ((BgL_sequencez00_bglt) BgL_nodez00_2460));
					BgL_tmpz00_3719 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_3719, BgL_auxz00_3721);
				}
				return BgL_val0_1267z00_2701;
			}
		}

	}



/* &node-cse!-closure1326 */
	obj_t BGl_z62nodezd2csez12zd2closure1326z70zzreduce_csez00(obj_t
		BgL_envz00_2462, obj_t BgL_nodez00_2463, obj_t BgL_stackz00_2464)
	{
		{	/* Reduce/cse.scm 81 */
			{	/* Reduce/cse.scm 82 */
				int BgL_tmpz00_3725;

				BgL_tmpz00_3725 = (int) (2L);
				BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3725);
			}
			{	/* Reduce/cse.scm 82 */
				obj_t BgL_auxz00_3730;
				int BgL_tmpz00_3728;

				BgL_auxz00_3730 = ((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_2463));
				BgL_tmpz00_3728 = (int) (1L);
				BGL_MVALUES_VAL_SET(BgL_tmpz00_3728, BgL_auxz00_3730);
			}
			return BFALSE;
		}

	}



/* &node-cse!-var1324 */
	obj_t BGl_z62nodezd2csez12zd2var1324z70zzreduce_csez00(obj_t BgL_envz00_2465,
		obj_t BgL_nodez00_2466, obj_t BgL_stackz00_2467)
	{
		{	/* Reduce/cse.scm 75 */
			{	/* Reduce/cse.scm 76 */
				int BgL_tmpz00_3734;

				BgL_tmpz00_3734 = (int) (2L);
				BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3734);
			}
			{	/* Reduce/cse.scm 76 */
				obj_t BgL_auxz00_3739;
				int BgL_tmpz00_3737;

				BgL_auxz00_3739 = ((obj_t) ((BgL_varz00_bglt) BgL_nodez00_2466));
				BgL_tmpz00_3737 = (int) (1L);
				BGL_MVALUES_VAL_SET(BgL_tmpz00_3737, BgL_auxz00_3739);
			}
			return BFALSE;
		}

	}



/* &node-cse!-kwote1322 */
	obj_t BGl_z62nodezd2csez12zd2kwote1322z70zzreduce_csez00(obj_t
		BgL_envz00_2468, obj_t BgL_nodez00_2469, obj_t BgL_stackz00_2470)
	{
		{	/* Reduce/cse.scm 69 */
			{	/* Reduce/cse.scm 70 */
				int BgL_tmpz00_3743;

				BgL_tmpz00_3743 = (int) (2L);
				BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3743);
			}
			{	/* Reduce/cse.scm 70 */
				obj_t BgL_auxz00_3748;
				int BgL_tmpz00_3746;

				BgL_auxz00_3748 = ((obj_t) ((BgL_kwotez00_bglt) BgL_nodez00_2469));
				BgL_tmpz00_3746 = (int) (1L);
				BGL_MVALUES_VAL_SET(BgL_tmpz00_3746, BgL_auxz00_3748);
			}
			return BFALSE;
		}

	}



/* &node-cse!-atom1320 */
	obj_t BGl_z62nodezd2csez12zd2atom1320z70zzreduce_csez00(obj_t BgL_envz00_2471,
		obj_t BgL_nodez00_2472, obj_t BgL_stackz00_2473)
	{
		{	/* Reduce/cse.scm 63 */
			{	/* Reduce/cse.scm 64 */
				int BgL_tmpz00_3752;

				BgL_tmpz00_3752 = (int) (2L);
				BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3752);
			}
			{	/* Reduce/cse.scm 64 */
				obj_t BgL_auxz00_3757;
				int BgL_tmpz00_3755;

				BgL_auxz00_3757 = ((obj_t) ((BgL_atomz00_bglt) BgL_nodez00_2472));
				BgL_tmpz00_3755 = (int) (1L);
				BGL_MVALUES_VAL_SET(BgL_tmpz00_3755, BgL_auxz00_3757);
			}
			return BFALSE;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzreduce_csez00(void)
	{
		{	/* Reduce/cse.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1882z00zzreduce_csez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1882z00zzreduce_csez00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1882z00zzreduce_csez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1882z00zzreduce_csez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1882z00zzreduce_csez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1882z00zzreduce_csez00));
			BGl_modulezd2initializa7ationz75zzcoerce_coercez00(361167184L,
				BSTRING_TO_STRING(BGl_string1882z00zzreduce_csez00));
			BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string1882z00zzreduce_csez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1882z00zzreduce_csez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1882z00zzreduce_csez00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string1882z00zzreduce_csez00));
			return
				BGl_modulezd2initializa7ationz75zzreduce_samez00(284483356L,
				BSTRING_TO_STRING(BGl_string1882z00zzreduce_csez00));
		}

	}

#ifdef __cplusplus
}
#endif
