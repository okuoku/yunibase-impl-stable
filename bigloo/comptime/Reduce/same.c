/*===========================================================================*/
/*   (Reduce/same.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Reduce/same.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_REDUCE_SAME_TYPE_DEFINITIONS
#define BGL_REDUCE_SAME_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                *BgL_pragmaz00_bglt;

	typedef struct BgL_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_getfieldz00_bglt;

	typedef struct BgL_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_setfieldz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_REDUCE_SAME_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static bool_t BGl_samezd2nodeza2zf3z83zzreduce_samez00(obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzreduce_samez00 = BUNSPEC;
	static obj_t BGl_z62samezd2nodezf3zd2valloc1308z91zzreduce_samez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62samezd2nodezf3zd2condition1312z91zzreduce_samez00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_z62samezd2nodezf3zd2app1298z91zzreduce_samez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzreduce_samez00(void);
	static obj_t BGl_z62samezd2nodezf3zd2letzd2var1316z43zzreduce_samez00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_z62samezd2nodezf3zd2cast1310z91zzreduce_samez00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzreduce_samez00(void);
	extern obj_t BGl_getfieldz00zzast_nodez00;
	static obj_t BGl_objectzd2initzd2zzreduce_samez00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	static obj_t BGl_z62samezd2nodezf3zd2fail1314z91zzreduce_samez00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62samezd2nodezf3zd2extern1300z91zzreduce_samez00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62samezd2nodezf3zd2funcall1296z91zzreduce_samez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzreduce_samez00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62samezd2nodezf3zd2sequence1290z91zzreduce_samez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62samezd2nodezf3zd2appzd2ly1294z43zzreduce_samez00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t BGl_z62samezd2nodezf3zd2sync1292z91zzreduce_samez00(obj_t, obj_t,
		obj_t, obj_t);
	static bool_t BGl_aliasedzf3zf3zzreduce_samez00(BgL_variablez00_bglt,
		BgL_variablez00_bglt, obj_t);
	extern obj_t BGl_pragmaz00zzast_nodez00;
	static obj_t BGl_z62samezd2nodezf3zd2getfield1304z91zzreduce_samez00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzreduce_samez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzreduce_samez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzreduce_samez00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_z62samezd2nodezf3zd2pragma1302z91zzreduce_samez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzreduce_samez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzreduce_samez00(void);
	BGL_EXPORTED_DECL bool_t
		BGl_samezd2nodezf3z21zzreduce_samez00(BgL_nodez00_bglt, BgL_nodez00_bglt,
		obj_t);
	extern obj_t BGl_vallocz00zzast_nodez00;
	static obj_t BGl_z62samezd2nodezf3zd2setfield1306z91zzreduce_samez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62samezd2nodezf3z43zzreduce_samez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62samezd2nodezf31281z43zzreduce_samez00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_setfieldz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t BGl_z62samezd2nodezf3zd2var1288z91zzreduce_samez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62samezd2nodezf3zd2atom1284z91zzreduce_samez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62samezd2nodezf3zd2kwote1286z91zzreduce_samez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t __cnst[1];


	   
		 
		DEFINE_STRING(BGl_string1955z00zzreduce_samez00,
		BgL_bgl_string1955za700za7za7r1977za7, "same-node?1281", 14);
	      DEFINE_STRING(BGl_string1957z00zzreduce_samez00,
		BgL_bgl_string1957za700za7za7r1978za7, "same-node?", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1954z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1979za7,
		BGl_z62samezd2nodezf31281z43zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1956z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1980za7,
		BGl_z62samezd2nodezf3zd2atom1284z91zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1958z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1981za7,
		BGl_z62samezd2nodezf3zd2kwote1286z91zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1959z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1982za7,
		BGl_z62samezd2nodezf3zd2var1288z91zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1960z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1983za7,
		BGl_z62samezd2nodezf3zd2sequence1290z91zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1961z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1984za7,
		BGl_z62samezd2nodezf3zd2sync1292z91zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1962z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1985za7,
		BGl_z62samezd2nodezf3zd2appzd2ly1294z43zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1963z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1986za7,
		BGl_z62samezd2nodezf3zd2funcall1296z91zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1964z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1987za7,
		BGl_z62samezd2nodezf3zd2app1298z91zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1965z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1988za7,
		BGl_z62samezd2nodezf3zd2extern1300z91zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1966z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1989za7,
		BGl_z62samezd2nodezf3zd2pragma1302z91zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1967z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1990za7,
		BGl_z62samezd2nodezf3zd2getfield1304z91zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1974z00zzreduce_samez00,
		BgL_bgl_string1974za700za7za7r1991za7, "reduce_same", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1968z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1992za7,
		BGl_z62samezd2nodezf3zd2setfield1306z91zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1975z00zzreduce_samez00,
		BgL_bgl_string1975za700za7za7r1993za7, "read ", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1969z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1994za7,
		BGl_z62samezd2nodezf3zd2valloc1308z91zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1970z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1995za7,
		BGl_z62samezd2nodezf3zd2cast1310z91zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1971z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1996za7,
		BGl_z62samezd2nodezf3zd2condition1312z91zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1972z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1997za7,
		BGl_z62samezd2nodezf3zd2fail1314z91zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1973z00zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1998za7,
		BGl_z62samezd2nodezf3zd2letzd2var1316z43zzreduce_samez00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00,
		BgL_bgl_za762sameza7d2nodeza7f1999za7,
		BGl_z62samezd2nodezf3z43zzreduce_samez00, 0L, BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzreduce_samez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzreduce_samez00(long
		BgL_checksumz00_3682, char *BgL_fromz00_3683)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzreduce_samez00))
				{
					BGl_requirezd2initializa7ationz75zzreduce_samez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzreduce_samez00();
					BGl_libraryzd2moduleszd2initz00zzreduce_samez00();
					BGl_cnstzd2initzd2zzreduce_samez00();
					BGl_importedzd2moduleszd2initz00zzreduce_samez00();
					BGl_genericzd2initzd2zzreduce_samez00();
					BGl_methodzd2initzd2zzreduce_samez00();
					return BGl_toplevelzd2initzd2zzreduce_samez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzreduce_samez00(void)
	{
		{	/* Reduce/same.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"reduce_same");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "reduce_same");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "reduce_same");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "reduce_same");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"reduce_same");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "reduce_same");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"reduce_same");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "reduce_same");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "reduce_same");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"reduce_same");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzreduce_samez00(void)
	{
		{	/* Reduce/same.scm 15 */
			{	/* Reduce/same.scm 15 */
				obj_t BgL_cportz00_3304;

				{	/* Reduce/same.scm 15 */
					obj_t BgL_stringz00_3311;

					BgL_stringz00_3311 = BGl_string1975z00zzreduce_samez00;
					{	/* Reduce/same.scm 15 */
						obj_t BgL_startz00_3312;

						BgL_startz00_3312 = BINT(0L);
						{	/* Reduce/same.scm 15 */
							obj_t BgL_endz00_3313;

							BgL_endz00_3313 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3311)));
							{	/* Reduce/same.scm 15 */

								BgL_cportz00_3304 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3311, BgL_startz00_3312, BgL_endz00_3313);
				}}}}
				{
					long BgL_iz00_3305;

					BgL_iz00_3305 = 0L;
				BgL_loopz00_3306:
					if ((BgL_iz00_3305 == -1L))
						{	/* Reduce/same.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Reduce/same.scm 15 */
							{	/* Reduce/same.scm 15 */
								obj_t BgL_arg1976z00_3307;

								{	/* Reduce/same.scm 15 */

									{	/* Reduce/same.scm 15 */
										obj_t BgL_locationz00_3309;

										BgL_locationz00_3309 = BBOOL(((bool_t) 0));
										{	/* Reduce/same.scm 15 */

											BgL_arg1976z00_3307 =
												BGl_readz00zz__readerz00(BgL_cportz00_3304,
												BgL_locationz00_3309);
										}
									}
								}
								{	/* Reduce/same.scm 15 */
									int BgL_tmpz00_3713;

									BgL_tmpz00_3713 = (int) (BgL_iz00_3305);
									CNST_TABLE_SET(BgL_tmpz00_3713, BgL_arg1976z00_3307);
							}}
							{	/* Reduce/same.scm 15 */
								int BgL_auxz00_3310;

								BgL_auxz00_3310 = (int) ((BgL_iz00_3305 - 1L));
								{
									long BgL_iz00_3718;

									BgL_iz00_3718 = (long) (BgL_auxz00_3310);
									BgL_iz00_3305 = BgL_iz00_3718;
									goto BgL_loopz00_3306;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzreduce_samez00(void)
	{
		{	/* Reduce/same.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzreduce_samez00(void)
	{
		{	/* Reduce/same.scm 15 */
			return BUNSPEC;
		}

	}



/* same-node*? */
	bool_t BGl_samezd2nodeza2zf3z83zzreduce_samez00(obj_t BgL_node1z00_57,
		obj_t BgL_node2z00_58, obj_t BgL_aliasz00_59)
	{
		{	/* Reduce/same.scm 227 */
		BGl_samezd2nodeza2zf3z83zzreduce_samez00:
			if (NULLP(BgL_node1z00_57))
				{	/* Reduce/same.scm 229 */
					return NULLP(BgL_node2z00_58);
				}
			else
				{	/* Reduce/same.scm 229 */
					if (NULLP(BgL_node2z00_58))
						{	/* Reduce/same.scm 231 */
							return ((bool_t) 0);
						}
					else
						{	/* Reduce/same.scm 233 */
							bool_t BgL_test2004z00_3726;

							{	/* Reduce/same.scm 233 */
								obj_t BgL_arg1327z00_1419;
								obj_t BgL_arg1328z00_1420;

								BgL_arg1327z00_1419 = CAR(((obj_t) BgL_node1z00_57));
								BgL_arg1328z00_1420 = CAR(((obj_t) BgL_node2z00_58));
								BgL_test2004z00_3726 =
									BGl_samezd2nodezf3z21zzreduce_samez00(
									((BgL_nodez00_bglt) BgL_arg1327z00_1419),
									((BgL_nodez00_bglt) BgL_arg1328z00_1420), BgL_aliasz00_59);
							}
							if (BgL_test2004z00_3726)
								{	/* Reduce/same.scm 234 */
									obj_t BgL_arg1325z00_1417;
									obj_t BgL_arg1326z00_1418;

									BgL_arg1325z00_1417 = CDR(((obj_t) BgL_node1z00_57));
									BgL_arg1326z00_1418 = CDR(((obj_t) BgL_node2z00_58));
									{
										obj_t BgL_node2z00_3739;
										obj_t BgL_node1z00_3738;

										BgL_node1z00_3738 = BgL_arg1325z00_1417;
										BgL_node2z00_3739 = BgL_arg1326z00_1418;
										BgL_node2z00_58 = BgL_node2z00_3739;
										BgL_node1z00_57 = BgL_node1z00_3738;
										goto BGl_samezd2nodeza2zf3z83zzreduce_samez00;
									}
								}
							else
								{	/* Reduce/same.scm 233 */
									return ((bool_t) 0);
								}
						}
				}
		}

	}



/* aliased? */
	bool_t BGl_aliasedzf3zf3zzreduce_samez00(BgL_variablez00_bglt BgL_var1z00_60,
		BgL_variablez00_bglt BgL_var2z00_61, obj_t BgL_aliasz00_62)
	{
		{	/* Reduce/same.scm 241 */
			{
				obj_t BgL_aliasz00_1422;

				BgL_aliasz00_1422 = BgL_aliasz00_62;
			BgL_zc3z04anonymousza31329ze3z87_1423:
				if (NULLP(BgL_aliasz00_1422))
					{	/* Reduce/same.scm 244 */
						return ((bool_t) 0);
					}
				else
					{	/* Reduce/same.scm 246 */
						bool_t BgL_test2006z00_3742;

						{	/* Reduce/same.scm 246 */
							obj_t BgL_tmpz00_3743;

							{	/* Reduce/same.scm 246 */
								obj_t BgL_pairz00_1926;

								BgL_pairz00_1926 = CAR(((obj_t) BgL_aliasz00_1422));
								BgL_tmpz00_3743 = CAR(BgL_pairz00_1926);
							}
							BgL_test2006z00_3742 =
								(BgL_tmpz00_3743 == ((obj_t) BgL_var1z00_60));
						}
						if (BgL_test2006z00_3742)
							{	/* Reduce/same.scm 247 */
								bool_t BgL_test2007z00_3749;

								{	/* Reduce/same.scm 247 */
									obj_t BgL_tmpz00_3750;

									{	/* Reduce/same.scm 247 */
										obj_t BgL_pairz00_1928;

										BgL_pairz00_1928 = CAR(((obj_t) BgL_aliasz00_1422));
										BgL_tmpz00_3750 = CDR(BgL_pairz00_1928);
									}
									BgL_test2007z00_3749 =
										(BgL_tmpz00_3750 == ((obj_t) BgL_var2z00_61));
								}
								if (BgL_test2007z00_3749)
									{	/* Reduce/same.scm 247 */
										return ((bool_t) 1);
									}
								else
									{
										obj_t BgL_aliasz00_3756;

										BgL_aliasz00_3756 = CDR(((obj_t) BgL_aliasz00_1422));
										BgL_aliasz00_1422 = BgL_aliasz00_3756;
										goto BgL_zc3z04anonymousza31329ze3z87_1423;
									}
							}
						else
							{	/* Reduce/same.scm 250 */
								bool_t BgL_test2008z00_3759;

								{	/* Reduce/same.scm 250 */
									obj_t BgL_tmpz00_3760;

									{	/* Reduce/same.scm 250 */
										obj_t BgL_pairz00_1931;

										BgL_pairz00_1931 = CAR(((obj_t) BgL_aliasz00_1422));
										BgL_tmpz00_3760 = CAR(BgL_pairz00_1931);
									}
									BgL_test2008z00_3759 =
										(BgL_tmpz00_3760 == ((obj_t) BgL_var2z00_61));
								}
								if (BgL_test2008z00_3759)
									{	/* Reduce/same.scm 251 */
										bool_t BgL_test2009z00_3766;

										{	/* Reduce/same.scm 251 */
											obj_t BgL_tmpz00_3767;

											{	/* Reduce/same.scm 251 */
												obj_t BgL_pairz00_1933;

												BgL_pairz00_1933 = CAR(((obj_t) BgL_aliasz00_1422));
												BgL_tmpz00_3767 = CDR(BgL_pairz00_1933);
											}
											BgL_test2009z00_3766 =
												(BgL_tmpz00_3767 == ((obj_t) BgL_var1z00_60));
										}
										if (BgL_test2009z00_3766)
											{	/* Reduce/same.scm 251 */
												return ((bool_t) 1);
											}
										else
											{
												obj_t BgL_aliasz00_3773;

												BgL_aliasz00_3773 = CDR(((obj_t) BgL_aliasz00_1422));
												BgL_aliasz00_1422 = BgL_aliasz00_3773;
												goto BgL_zc3z04anonymousza31329ze3z87_1423;
											}
									}
								else
									{
										obj_t BgL_aliasz00_3776;

										BgL_aliasz00_3776 = CDR(((obj_t) BgL_aliasz00_1422));
										BgL_aliasz00_1422 = BgL_aliasz00_3776;
										goto BgL_zc3z04anonymousza31329ze3z87_1423;
									}
							}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzreduce_samez00(void)
	{
		{	/* Reduce/same.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzreduce_samez00(void)
	{
		{	/* Reduce/same.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00,
				BGl_proc1954z00zzreduce_samez00, BGl_nodez00zzast_nodez00,
				BGl_string1955z00zzreduce_samez00);
		}

	}



/* &same-node?1281 */
	obj_t BGl_z62samezd2nodezf31281z43zzreduce_samez00(obj_t BgL_envz00_3211,
		obj_t BgL_node1z00_3212, obj_t BgL_node2z00_3213, obj_t BgL_aliasz00_3214)
	{
		{	/* Reduce/same.scm 26 */
			return BBOOL(((bool_t) 0));
		}

	}



/* same-node? */
	BGL_EXPORTED_DEF bool_t BGl_samezd2nodezf3z21zzreduce_samez00(BgL_nodez00_bglt
		BgL_node1z00_3, BgL_nodez00_bglt BgL_node2z00_4, obj_t BgL_aliasz00_5)
	{
		{	/* Reduce/same.scm 26 */
			{	/* Reduce/same.scm 26 */
				obj_t BgL_method1282z00_1455;

				{	/* Reduce/same.scm 26 */
					obj_t BgL_res1921z00_1966;

					{	/* Reduce/same.scm 26 */
						long BgL_objzd2classzd2numz00_1937;

						BgL_objzd2classzd2numz00_1937 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_node1z00_3));
						{	/* Reduce/same.scm 26 */
							obj_t BgL_arg1811z00_1938;

							BgL_arg1811z00_1938 =
								PROCEDURE_REF(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00,
								(int) (1L));
							{	/* Reduce/same.scm 26 */
								int BgL_offsetz00_1941;

								BgL_offsetz00_1941 = (int) (BgL_objzd2classzd2numz00_1937);
								{	/* Reduce/same.scm 26 */
									long BgL_offsetz00_1942;

									BgL_offsetz00_1942 =
										((long) (BgL_offsetz00_1941) - OBJECT_TYPE);
									{	/* Reduce/same.scm 26 */
										long BgL_modz00_1943;

										BgL_modz00_1943 =
											(BgL_offsetz00_1942 >> (int) ((long) ((int) (4L))));
										{	/* Reduce/same.scm 26 */
											long BgL_restz00_1945;

											BgL_restz00_1945 =
												(BgL_offsetz00_1942 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Reduce/same.scm 26 */

												{	/* Reduce/same.scm 26 */
													obj_t BgL_bucketz00_1947;

													BgL_bucketz00_1947 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1938), BgL_modz00_1943);
													BgL_res1921z00_1966 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1947), BgL_restz00_1945);
					}}}}}}}}
					BgL_method1282z00_1455 = BgL_res1921z00_1966;
				}
				return
					CBOOL(BGL_PROCEDURE_CALL3(BgL_method1282z00_1455,
						((obj_t) BgL_node1z00_3),
						((obj_t) BgL_node2z00_4), BgL_aliasz00_5));
			}
		}

	}



/* &same-node? */
	obj_t BGl_z62samezd2nodezf3z43zzreduce_samez00(obj_t BgL_envz00_3215,
		obj_t BgL_node1z00_3216, obj_t BgL_node2z00_3217, obj_t BgL_aliasz00_3218)
	{
		{	/* Reduce/same.scm 26 */
			return
				BBOOL(BGl_samezd2nodezf3z21zzreduce_samez00(
					((BgL_nodez00_bglt) BgL_node1z00_3216),
					((BgL_nodez00_bglt) BgL_node2z00_3217), BgL_aliasz00_3218));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzreduce_samez00(void)
	{
		{	/* Reduce/same.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00, BGl_atomz00zzast_nodez00,
				BGl_proc1956z00zzreduce_samez00, BGl_string1957z00zzreduce_samez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00, BGl_kwotez00zzast_nodez00,
				BGl_proc1958z00zzreduce_samez00, BGl_string1957z00zzreduce_samez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00, BGl_varz00zzast_nodez00,
				BGl_proc1959z00zzreduce_samez00, BGl_string1957z00zzreduce_samez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00,
				BGl_sequencez00zzast_nodez00, BGl_proc1960z00zzreduce_samez00,
				BGl_string1957z00zzreduce_samez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00, BGl_syncz00zzast_nodez00,
				BGl_proc1961z00zzreduce_samez00, BGl_string1957z00zzreduce_samez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1962z00zzreduce_samez00,
				BGl_string1957z00zzreduce_samez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00,
				BGl_funcallz00zzast_nodez00, BGl_proc1963z00zzreduce_samez00,
				BGl_string1957z00zzreduce_samez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00, BGl_appz00zzast_nodez00,
				BGl_proc1964z00zzreduce_samez00, BGl_string1957z00zzreduce_samez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00,
				BGl_externz00zzast_nodez00, BGl_proc1965z00zzreduce_samez00,
				BGl_string1957z00zzreduce_samez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00,
				BGl_pragmaz00zzast_nodez00, BGl_proc1966z00zzreduce_samez00,
				BGl_string1957z00zzreduce_samez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00,
				BGl_getfieldz00zzast_nodez00, BGl_proc1967z00zzreduce_samez00,
				BGl_string1957z00zzreduce_samez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00,
				BGl_setfieldz00zzast_nodez00, BGl_proc1968z00zzreduce_samez00,
				BGl_string1957z00zzreduce_samez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00,
				BGl_vallocz00zzast_nodez00, BGl_proc1969z00zzreduce_samez00,
				BGl_string1957z00zzreduce_samez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00, BGl_castz00zzast_nodez00,
				BGl_proc1970z00zzreduce_samez00, BGl_string1957z00zzreduce_samez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1971z00zzreduce_samez00,
				BGl_string1957z00zzreduce_samez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00, BGl_failz00zzast_nodez00,
				BGl_proc1972z00zzreduce_samez00, BGl_string1957z00zzreduce_samez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_samezd2nodezf3zd2envzf3zzreduce_samez00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1973z00zzreduce_samez00,
				BGl_string1957z00zzreduce_samez00);
		}

	}



/* &same-node?-let-var1316 */
	obj_t BGl_z62samezd2nodezf3zd2letzd2var1316z43zzreduce_samez00(obj_t
		BgL_envz00_3236, obj_t BgL_nodez00_3237, obj_t BgL_node2z00_3238,
		obj_t BgL_aliasz00_3239)
	{
		{	/* Reduce/same.scm 193 */
			{	/* Tools/trace.sch 53 */
				bool_t BgL_tmpz00_3836;

				{	/* Tools/trace.sch 53 */
					bool_t BgL_test2010z00_3837;

					{	/* Tools/trace.sch 53 */
						obj_t BgL_classz00_3321;

						BgL_classz00_3321 = BGl_letzd2varzd2zzast_nodez00;
						{	/* Tools/trace.sch 53 */
							BgL_objectz00_bglt BgL_arg1807z00_3322;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_tmpz00_3838;

								BgL_tmpz00_3838 =
									((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3238));
								BgL_arg1807z00_3322 = (BgL_objectz00_bglt) (BgL_tmpz00_3838);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Tools/trace.sch 53 */
									long BgL_idxz00_3323;

									BgL_idxz00_3323 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3322);
									BgL_test2010z00_3837 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3323 + 3L)) == BgL_classz00_3321);
								}
							else
								{	/* Tools/trace.sch 53 */
									bool_t BgL_res1937z00_3326;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_oclassz00_3327;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1815z00_3328;
											long BgL_arg1816z00_3329;

											BgL_arg1815z00_3328 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Tools/trace.sch 53 */
												long BgL_arg1817z00_3330;

												BgL_arg1817z00_3330 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3322);
												BgL_arg1816z00_3329 =
													(BgL_arg1817z00_3330 - OBJECT_TYPE);
											}
											BgL_oclassz00_3327 =
												VECTOR_REF(BgL_arg1815z00_3328, BgL_arg1816z00_3329);
										}
										{	/* Tools/trace.sch 53 */
											bool_t BgL__ortest_1115z00_3331;

											BgL__ortest_1115z00_3331 =
												(BgL_classz00_3321 == BgL_oclassz00_3327);
											if (BgL__ortest_1115z00_3331)
												{	/* Tools/trace.sch 53 */
													BgL_res1937z00_3326 = BgL__ortest_1115z00_3331;
												}
											else
												{	/* Tools/trace.sch 53 */
													long BgL_odepthz00_3332;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg1804z00_3333;

														BgL_arg1804z00_3333 = (BgL_oclassz00_3327);
														BgL_odepthz00_3332 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3333);
													}
													if ((3L < BgL_odepthz00_3332))
														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg1802z00_3334;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg1803z00_3335;

																BgL_arg1803z00_3335 = (BgL_oclassz00_3327);
																BgL_arg1802z00_3334 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3335,
																	3L);
															}
															BgL_res1937z00_3326 =
																(BgL_arg1802z00_3334 == BgL_classz00_3321);
														}
													else
														{	/* Tools/trace.sch 53 */
															BgL_res1937z00_3326 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2010z00_3837 = BgL_res1937z00_3326;
								}
						}
					}
					if (BgL_test2010z00_3837)
						{	/* Tools/trace.sch 53 */
							if (
								(((obj_t)
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_letzd2varzd2_bglt) BgL_nodez00_3237))))->
											BgL_typez00)) ==
									((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_node2z00_3238)))->BgL_typez00))))
								{	/* Tools/trace.sch 53 */
									if (
										(bgl_list_length(
												(((BgL_letzd2varzd2_bglt) COBJECT(
															((BgL_letzd2varzd2_bglt) BgL_nodez00_3237)))->
													BgL_bindingsz00)) ==
											bgl_list_length((((BgL_letzd2varzd2_bglt)
														COBJECT(((BgL_letzd2varzd2_bglt) ((BgL_nodez00_bglt)
																	BgL_node2z00_3238))))->BgL_bindingsz00))))
										{	/* Tools/trace.sch 53 */
											obj_t BgL_newzd2aliaszd2_3338;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_arg1834z00_3339;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_ll1275z00_3340;
													obj_t BgL_ll1276z00_3341;

													BgL_ll1275z00_3340 =
														(((BgL_letzd2varzd2_bglt) COBJECT(
																((BgL_letzd2varzd2_bglt) BgL_nodez00_3237)))->
														BgL_bindingsz00);
													BgL_ll1276z00_3341 =
														(((BgL_letzd2varzd2_bglt)
															COBJECT(((BgL_letzd2varzd2_bglt) (
																		(BgL_nodez00_bglt) BgL_node2z00_3238))))->
														BgL_bindingsz00);
													if (NULLP(BgL_ll1275z00_3340))
														{	/* Tools/trace.sch 53 */
															BgL_arg1834z00_3339 = BNIL;
														}
													else
														{	/* Tools/trace.sch 53 */
															obj_t BgL_head1277z00_3342;

															BgL_head1277z00_3342 =
																MAKE_YOUNG_PAIR(BNIL, BNIL);
															{
																obj_t BgL_ll1275z00_3344;
																obj_t BgL_ll1276z00_3345;
																obj_t BgL_tail1278z00_3346;

																BgL_ll1275z00_3344 = BgL_ll1275z00_3340;
																BgL_ll1276z00_3345 = BgL_ll1276z00_3341;
																BgL_tail1278z00_3346 = BgL_head1277z00_3342;
															BgL_zc3z04anonymousza31836ze3z87_3343:
																if (NULLP(BgL_ll1275z00_3344))
																	{	/* Tools/trace.sch 53 */
																		BgL_arg1834z00_3339 =
																			CDR(BgL_head1277z00_3342);
																	}
																else
																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_newtail1279z00_3347;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1840z00_3348;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_b1z00_3349;
																				obj_t BgL_b2z00_3350;

																				BgL_b1z00_3349 =
																					CAR(((obj_t) BgL_ll1275z00_3344));
																				BgL_b2z00_3350 =
																					CAR(((obj_t) BgL_ll1276z00_3345));
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1842z00_3351;
																					obj_t BgL_arg1843z00_3352;

																					BgL_arg1842z00_3351 =
																						CAR(((obj_t) BgL_b1z00_3349));
																					BgL_arg1843z00_3352 =
																						CAR(((obj_t) BgL_b2z00_3350));
																					BgL_arg1840z00_3348 =
																						MAKE_YOUNG_PAIR(BgL_arg1842z00_3351,
																						BgL_arg1843z00_3352);
																				}
																			}
																			BgL_newtail1279z00_3347 =
																				MAKE_YOUNG_PAIR(BgL_arg1840z00_3348,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1278z00_3346,
																			BgL_newtail1279z00_3347);
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1838z00_3353;
																			obj_t BgL_arg1839z00_3354;

																			BgL_arg1838z00_3353 =
																				CDR(((obj_t) BgL_ll1275z00_3344));
																			BgL_arg1839z00_3354 =
																				CDR(((obj_t) BgL_ll1276z00_3345));
																			{
																				obj_t BgL_tail1278z00_3907;
																				obj_t BgL_ll1276z00_3906;
																				obj_t BgL_ll1275z00_3905;

																				BgL_ll1275z00_3905 =
																					BgL_arg1838z00_3353;
																				BgL_ll1276z00_3906 =
																					BgL_arg1839z00_3354;
																				BgL_tail1278z00_3907 =
																					BgL_newtail1279z00_3347;
																				BgL_tail1278z00_3346 =
																					BgL_tail1278z00_3907;
																				BgL_ll1276z00_3345 = BgL_ll1276z00_3906;
																				BgL_ll1275z00_3344 = BgL_ll1275z00_3905;
																				goto
																					BgL_zc3z04anonymousza31836ze3z87_3343;
																			}
																		}
																	}
															}
														}
												}
												BgL_newzd2aliaszd2_3338 =
													BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1834z00_3339, BgL_aliasz00_3239);
											}
											{	/* Tools/trace.sch 53 */
												bool_t BgL_test2018z00_3909;

												{	/* Tools/trace.sch 53 */
													BgL_nodez00_bglt BgL_arg1832z00_3355;
													BgL_nodez00_bglt BgL_arg1833z00_3356;

													BgL_arg1832z00_3355 =
														(((BgL_letzd2varzd2_bglt) COBJECT(
																((BgL_letzd2varzd2_bglt) BgL_nodez00_3237)))->
														BgL_bodyz00);
													BgL_arg1833z00_3356 =
														(((BgL_letzd2varzd2_bglt)
															COBJECT(((BgL_letzd2varzd2_bglt) (
																		(BgL_nodez00_bglt) BgL_node2z00_3238))))->
														BgL_bodyz00);
													BgL_test2018z00_3909 =
														BGl_samezd2nodezf3z21zzreduce_samez00
														(BgL_arg1832z00_3355, BgL_arg1833z00_3356,
														BgL_newzd2aliaszd2_3338);
												}
												if (BgL_test2018z00_3909)
													{	/* Tools/trace.sch 53 */
														obj_t BgL_g1159z00_3357;
														obj_t BgL_g1160z00_3358;

														BgL_g1159z00_3357 =
															(((BgL_letzd2varzd2_bglt) COBJECT(
																	((BgL_letzd2varzd2_bglt) BgL_nodez00_3237)))->
															BgL_bindingsz00);
														BgL_g1160z00_3358 =
															(((BgL_letzd2varzd2_bglt)
																COBJECT(((BgL_letzd2varzd2_bglt) (
																			(BgL_nodez00_bglt) BgL_node2z00_3238))))->
															BgL_bindingsz00);
														{
															obj_t BgL_bindings1z00_3360;
															obj_t BgL_bindings2z00_3361;

															BgL_bindings1z00_3360 = BgL_g1159z00_3357;
															BgL_bindings2z00_3361 = BgL_g1160z00_3358;
														BgL_loopz00_3359:
															if (NULLP(BgL_bindings1z00_3360))
																{	/* Tools/trace.sch 53 */
																	BgL_tmpz00_3836 = ((bool_t) 1);
																}
															else
																{	/* Tools/trace.sch 53 */
																	bool_t BgL_test2020z00_3923;

																	{	/* Tools/trace.sch 53 */
																		bool_t BgL_test2021z00_3924;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_tmpz00_3925;

																			{
																				BgL_variablez00_bglt BgL_auxz00_3926;

																				{
																					BgL_localz00_bglt BgL_auxz00_3927;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_pairz00_3362;

																						BgL_pairz00_3362 =
																							CAR(
																							((obj_t) BgL_bindings1z00_3360));
																						BgL_auxz00_3927 =
																							((BgL_localz00_bglt)
																							CAR(BgL_pairz00_3362));
																					}
																					BgL_auxz00_3926 =
																						((BgL_variablez00_bglt)
																						BgL_auxz00_3927);
																				}
																				BgL_tmpz00_3925 =
																					(((BgL_variablez00_bglt)
																						COBJECT(BgL_auxz00_3926))->
																					BgL_accessz00);
																			}
																			BgL_test2021z00_3924 =
																				(BgL_tmpz00_3925 == CNST_TABLE_REF(0));
																		}
																		if (BgL_test2021z00_3924)
																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_tmpz00_3936;

																				{
																					BgL_variablez00_bglt BgL_auxz00_3937;

																					{
																						BgL_localz00_bglt BgL_auxz00_3938;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_pairz00_3363;

																							BgL_pairz00_3363 =
																								CAR(
																								((obj_t)
																									BgL_bindings2z00_3361));
																							BgL_auxz00_3938 =
																								((BgL_localz00_bglt)
																								CAR(BgL_pairz00_3363));
																						}
																						BgL_auxz00_3937 =
																							((BgL_variablez00_bglt)
																							BgL_auxz00_3938);
																					}
																					BgL_tmpz00_3936 =
																						(((BgL_variablez00_bglt)
																							COBJECT(BgL_auxz00_3937))->
																						BgL_accessz00);
																				}
																				BgL_test2020z00_3923 =
																					(BgL_tmpz00_3936 ==
																					CNST_TABLE_REF(0));
																			}
																		else
																			{	/* Tools/trace.sch 53 */
																				BgL_test2020z00_3923 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test2020z00_3923)
																		{	/* Tools/trace.sch 53 */
																			bool_t BgL_test2022z00_3947;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1798z00_3364;
																				obj_t BgL_arg1799z00_3365;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_pairz00_3366;

																					BgL_pairz00_3366 =
																						CAR(
																						((obj_t) BgL_bindings1z00_3360));
																					BgL_arg1798z00_3364 =
																						CDR(BgL_pairz00_3366);
																				}
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_pairz00_3367;

																					BgL_pairz00_3367 =
																						CAR(
																						((obj_t) BgL_bindings2z00_3361));
																					BgL_arg1799z00_3365 =
																						CDR(BgL_pairz00_3367);
																				}
																				BgL_test2022z00_3947 =
																					BGl_samezd2nodezf3z21zzreduce_samez00(
																					((BgL_nodez00_bglt)
																						BgL_arg1798z00_3364),
																					((BgL_nodez00_bglt)
																						BgL_arg1799z00_3365),
																					BgL_aliasz00_3239);
																			}
																			if (BgL_test2022z00_3947)
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1773z00_3368;
																					obj_t BgL_arg1775z00_3369;

																					BgL_arg1773z00_3368 =
																						CDR(
																						((obj_t) BgL_bindings1z00_3360));
																					BgL_arg1775z00_3369 =
																						CDR(
																						((obj_t) BgL_bindings2z00_3361));
																					{
																						obj_t BgL_bindings2z00_3962;
																						obj_t BgL_bindings1z00_3961;

																						BgL_bindings1z00_3961 =
																							BgL_arg1773z00_3368;
																						BgL_bindings2z00_3962 =
																							BgL_arg1775z00_3369;
																						BgL_bindings2z00_3361 =
																							BgL_bindings2z00_3962;
																						BgL_bindings1z00_3360 =
																							BgL_bindings1z00_3961;
																						goto BgL_loopz00_3359;
																					}
																				}
																			else
																				{	/* Tools/trace.sch 53 */
																					BgL_tmpz00_3836 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Tools/trace.sch 53 */
																			BgL_tmpz00_3836 = ((bool_t) 0);
																		}
																}
														}
													}
												else
													{	/* Tools/trace.sch 53 */
														BgL_tmpz00_3836 = ((bool_t) 0);
													}
											}
										}
									else
										{	/* Tools/trace.sch 53 */
											BgL_tmpz00_3836 = ((bool_t) 0);
										}
								}
							else
								{	/* Tools/trace.sch 53 */
									BgL_tmpz00_3836 = ((bool_t) 0);
								}
						}
					else
						{	/* Tools/trace.sch 53 */
							BgL_tmpz00_3836 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_3836);
			}
		}

	}



/* &same-node?-fail1314 */
	obj_t BGl_z62samezd2nodezf3zd2fail1314z91zzreduce_samez00(obj_t
		BgL_envz00_3240, obj_t BgL_nodez00_3241, obj_t BgL_node2z00_3242,
		obj_t BgL_aliasz00_3243)
	{
		{	/* Reduce/same.scm 180 */
			{	/* Tools/trace.sch 53 */
				bool_t BgL_tmpz00_3964;

				{	/* Tools/trace.sch 53 */
					bool_t BgL_test2023z00_3965;

					{	/* Tools/trace.sch 53 */
						obj_t BgL_classz00_3372;

						BgL_classz00_3372 = BGl_failz00zzast_nodez00;
						{	/* Tools/trace.sch 53 */
							BgL_objectz00_bglt BgL_arg1807z00_3373;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_tmpz00_3966;

								BgL_tmpz00_3966 =
									((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3242));
								BgL_arg1807z00_3373 = (BgL_objectz00_bglt) (BgL_tmpz00_3966);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Tools/trace.sch 53 */
									long BgL_idxz00_3374;

									BgL_idxz00_3374 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3373);
									BgL_test2023z00_3965 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3374 + 2L)) == BgL_classz00_3372);
								}
							else
								{	/* Tools/trace.sch 53 */
									bool_t BgL_res1936z00_3377;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_oclassz00_3378;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1815z00_3379;
											long BgL_arg1816z00_3380;

											BgL_arg1815z00_3379 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Tools/trace.sch 53 */
												long BgL_arg1817z00_3381;

												BgL_arg1817z00_3381 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3373);
												BgL_arg1816z00_3380 =
													(BgL_arg1817z00_3381 - OBJECT_TYPE);
											}
											BgL_oclassz00_3378 =
												VECTOR_REF(BgL_arg1815z00_3379, BgL_arg1816z00_3380);
										}
										{	/* Tools/trace.sch 53 */
											bool_t BgL__ortest_1115z00_3382;

											BgL__ortest_1115z00_3382 =
												(BgL_classz00_3372 == BgL_oclassz00_3378);
											if (BgL__ortest_1115z00_3382)
												{	/* Tools/trace.sch 53 */
													BgL_res1936z00_3377 = BgL__ortest_1115z00_3382;
												}
											else
												{	/* Tools/trace.sch 53 */
													long BgL_odepthz00_3383;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg1804z00_3384;

														BgL_arg1804z00_3384 = (BgL_oclassz00_3378);
														BgL_odepthz00_3383 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3384);
													}
													if ((2L < BgL_odepthz00_3383))
														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg1802z00_3385;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg1803z00_3386;

																BgL_arg1803z00_3386 = (BgL_oclassz00_3378);
																BgL_arg1802z00_3385 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3386,
																	2L);
															}
															BgL_res1936z00_3377 =
																(BgL_arg1802z00_3385 == BgL_classz00_3372);
														}
													else
														{	/* Tools/trace.sch 53 */
															BgL_res1936z00_3377 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2023z00_3965 = BgL_res1936z00_3377;
								}
						}
					}
					if (BgL_test2023z00_3965)
						{	/* Tools/trace.sch 53 */
							if (
								(((obj_t)
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_failz00_bglt) BgL_nodez00_3241))))->
											BgL_typez00)) ==
									((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_node2z00_3242)))->BgL_typez00))))
								{	/* Tools/trace.sch 53 */
									bool_t BgL_test2028z00_3998;

									{	/* Tools/trace.sch 53 */
										BgL_nodez00_bglt BgL_arg1735z00_3387;
										BgL_nodez00_bglt BgL_arg1736z00_3388;

										BgL_arg1735z00_3387 =
											(((BgL_failz00_bglt) COBJECT(
													((BgL_failz00_bglt) BgL_nodez00_3241)))->BgL_msgz00);
										BgL_arg1736z00_3388 =
											(((BgL_failz00_bglt) COBJECT(
													((BgL_failz00_bglt)
														((BgL_nodez00_bglt) BgL_node2z00_3242))))->
											BgL_msgz00);
										BgL_test2028z00_3998 =
											BGl_samezd2nodezf3z21zzreduce_samez00(BgL_arg1735z00_3387,
											BgL_arg1736z00_3388, BgL_aliasz00_3243);
									}
									if (BgL_test2028z00_3998)
										{	/* Tools/trace.sch 53 */
											bool_t BgL_test2029z00_4005;

											{	/* Tools/trace.sch 53 */
												BgL_nodez00_bglt BgL_arg1733z00_3389;
												BgL_nodez00_bglt BgL_arg1734z00_3390;

												BgL_arg1733z00_3389 =
													(((BgL_failz00_bglt) COBJECT(
															((BgL_failz00_bglt) BgL_nodez00_3241)))->
													BgL_procz00);
												BgL_arg1734z00_3390 =
													(((BgL_failz00_bglt)
														COBJECT(((BgL_failz00_bglt) ((BgL_nodez00_bglt)
																	BgL_node2z00_3242))))->BgL_procz00);
												BgL_test2029z00_4005 =
													BGl_samezd2nodezf3z21zzreduce_samez00
													(BgL_arg1733z00_3389, BgL_arg1734z00_3390,
													BgL_aliasz00_3243);
											}
											if (BgL_test2029z00_4005)
												{	/* Tools/trace.sch 53 */
													BgL_nodez00_bglt BgL_arg1722z00_3391;
													BgL_nodez00_bglt BgL_arg1724z00_3392;

													BgL_arg1722z00_3391 =
														(((BgL_failz00_bglt) COBJECT(
																((BgL_failz00_bglt) BgL_nodez00_3241)))->
														BgL_objz00);
													BgL_arg1724z00_3392 =
														(((BgL_failz00_bglt)
															COBJECT(((BgL_failz00_bglt) ((BgL_nodez00_bglt)
																		BgL_node2z00_3242))))->BgL_objz00);
													BgL_tmpz00_3964 =
														BGl_samezd2nodezf3z21zzreduce_samez00
														(BgL_arg1722z00_3391, BgL_arg1724z00_3392,
														BgL_aliasz00_3243);
												}
											else
												{	/* Tools/trace.sch 53 */
													BgL_tmpz00_3964 = ((bool_t) 0);
												}
										}
									else
										{	/* Tools/trace.sch 53 */
											BgL_tmpz00_3964 = ((bool_t) 0);
										}
								}
							else
								{	/* Tools/trace.sch 53 */
									BgL_tmpz00_3964 = ((bool_t) 0);
								}
						}
					else
						{	/* Tools/trace.sch 53 */
							BgL_tmpz00_3964 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_3964);
			}
		}

	}



/* &same-node?-condition1312 */
	obj_t BGl_z62samezd2nodezf3zd2condition1312z91zzreduce_samez00(obj_t
		BgL_envz00_3244, obj_t BgL_nodez00_3245, obj_t BgL_node2z00_3246,
		obj_t BgL_aliasz00_3247)
	{
		{	/* Reduce/same.scm 167 */
			{	/* Tools/trace.sch 53 */
				bool_t BgL_tmpz00_4019;

				{	/* Tools/trace.sch 53 */
					bool_t BgL_test2030z00_4020;

					{	/* Tools/trace.sch 53 */
						obj_t BgL_classz00_3395;

						BgL_classz00_3395 = BGl_conditionalz00zzast_nodez00;
						{	/* Tools/trace.sch 53 */
							BgL_objectz00_bglt BgL_arg1807z00_3396;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_tmpz00_4021;

								BgL_tmpz00_4021 =
									((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3246));
								BgL_arg1807z00_3396 = (BgL_objectz00_bglt) (BgL_tmpz00_4021);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Tools/trace.sch 53 */
									long BgL_idxz00_3397;

									BgL_idxz00_3397 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3396);
									BgL_test2030z00_4020 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3397 + 3L)) == BgL_classz00_3395);
								}
							else
								{	/* Tools/trace.sch 53 */
									bool_t BgL_res1935z00_3400;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_oclassz00_3401;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1815z00_3402;
											long BgL_arg1816z00_3403;

											BgL_arg1815z00_3402 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Tools/trace.sch 53 */
												long BgL_arg1817z00_3404;

												BgL_arg1817z00_3404 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3396);
												BgL_arg1816z00_3403 =
													(BgL_arg1817z00_3404 - OBJECT_TYPE);
											}
											BgL_oclassz00_3401 =
												VECTOR_REF(BgL_arg1815z00_3402, BgL_arg1816z00_3403);
										}
										{	/* Tools/trace.sch 53 */
											bool_t BgL__ortest_1115z00_3405;

											BgL__ortest_1115z00_3405 =
												(BgL_classz00_3395 == BgL_oclassz00_3401);
											if (BgL__ortest_1115z00_3405)
												{	/* Tools/trace.sch 53 */
													BgL_res1935z00_3400 = BgL__ortest_1115z00_3405;
												}
											else
												{	/* Tools/trace.sch 53 */
													long BgL_odepthz00_3406;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg1804z00_3407;

														BgL_arg1804z00_3407 = (BgL_oclassz00_3401);
														BgL_odepthz00_3406 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3407);
													}
													if ((3L < BgL_odepthz00_3406))
														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg1802z00_3408;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg1803z00_3409;

																BgL_arg1803z00_3409 = (BgL_oclassz00_3401);
																BgL_arg1802z00_3408 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3409,
																	3L);
															}
															BgL_res1935z00_3400 =
																(BgL_arg1802z00_3408 == BgL_classz00_3395);
														}
													else
														{	/* Tools/trace.sch 53 */
															BgL_res1935z00_3400 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2030z00_4020 = BgL_res1935z00_3400;
								}
						}
					}
					if (BgL_test2030z00_4020)
						{	/* Tools/trace.sch 53 */
							if (
								(((obj_t)
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_conditionalz00_bglt) BgL_nodez00_3245))))->
											BgL_typez00)) ==
									((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_node2z00_3246)))->BgL_typez00))))
								{	/* Tools/trace.sch 53 */
									bool_t BgL_test2035z00_4053;

									{	/* Tools/trace.sch 53 */
										BgL_nodez00_bglt BgL_arg1714z00_3410;
										BgL_nodez00_bglt BgL_arg1717z00_3411;

										BgL_arg1714z00_3410 =
											(((BgL_conditionalz00_bglt) COBJECT(
													((BgL_conditionalz00_bglt) BgL_nodez00_3245)))->
											BgL_falsez00);
										BgL_arg1717z00_3411 =
											(((BgL_conditionalz00_bglt)
												COBJECT(((BgL_conditionalz00_bglt) ((BgL_nodez00_bglt)
															BgL_node2z00_3246))))->BgL_falsez00);
										BgL_test2035z00_4053 =
											BGl_samezd2nodezf3z21zzreduce_samez00(BgL_arg1714z00_3410,
											BgL_arg1717z00_3411, BgL_aliasz00_3247);
									}
									if (BgL_test2035z00_4053)
										{	/* Tools/trace.sch 53 */
											bool_t BgL_test2036z00_4060;

											{	/* Tools/trace.sch 53 */
												BgL_nodez00_bglt BgL_arg1710z00_3412;
												BgL_nodez00_bglt BgL_arg1711z00_3413;

												BgL_arg1710z00_3412 =
													(((BgL_conditionalz00_bglt) COBJECT(
															((BgL_conditionalz00_bglt) BgL_nodez00_3245)))->
													BgL_truez00);
												BgL_arg1711z00_3413 =
													(((BgL_conditionalz00_bglt)
														COBJECT(((BgL_conditionalz00_bglt) (
																	(BgL_nodez00_bglt) BgL_node2z00_3246))))->
													BgL_truez00);
												BgL_test2036z00_4060 =
													BGl_samezd2nodezf3z21zzreduce_samez00
													(BgL_arg1710z00_3412, BgL_arg1711z00_3413,
													BgL_aliasz00_3247);
											}
											if (BgL_test2036z00_4060)
												{	/* Tools/trace.sch 53 */
													BgL_nodez00_bglt BgL_arg1708z00_3414;
													BgL_nodez00_bglt BgL_arg1709z00_3415;

													BgL_arg1708z00_3414 =
														(((BgL_conditionalz00_bglt) COBJECT(
																((BgL_conditionalz00_bglt) BgL_nodez00_3245)))->
														BgL_testz00);
													BgL_arg1709z00_3415 =
														(((BgL_conditionalz00_bglt)
															COBJECT(((BgL_conditionalz00_bglt) (
																		(BgL_nodez00_bglt) BgL_node2z00_3246))))->
														BgL_testz00);
													BgL_tmpz00_4019 =
														BGl_samezd2nodezf3z21zzreduce_samez00
														(BgL_arg1708z00_3414, BgL_arg1709z00_3415,
														BgL_aliasz00_3247);
												}
											else
												{	/* Tools/trace.sch 53 */
													BgL_tmpz00_4019 = ((bool_t) 0);
												}
										}
									else
										{	/* Tools/trace.sch 53 */
											BgL_tmpz00_4019 = ((bool_t) 0);
										}
								}
							else
								{	/* Tools/trace.sch 53 */
									BgL_tmpz00_4019 = ((bool_t) 0);
								}
						}
					else
						{	/* Tools/trace.sch 53 */
							BgL_tmpz00_4019 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_4019);
			}
		}

	}



/* &same-node?-cast1310 */
	obj_t BGl_z62samezd2nodezf3zd2cast1310z91zzreduce_samez00(obj_t
		BgL_envz00_3248, obj_t BgL_nodez00_3249, obj_t BgL_node2z00_3250,
		obj_t BgL_aliasz00_3251)
	{
		{	/* Reduce/same.scm 159 */
			{	/* Reduce/same.scm 160 */
				bool_t BgL_tmpz00_4074;

				{	/* Reduce/same.scm 160 */
					bool_t BgL_test2037z00_4075;

					{	/* Reduce/same.scm 160 */
						obj_t BgL_classz00_3418;

						BgL_classz00_3418 = BGl_castz00zzast_nodez00;
						{	/* Reduce/same.scm 160 */
							BgL_objectz00_bglt BgL_arg1807z00_3419;

							{	/* Reduce/same.scm 160 */
								obj_t BgL_tmpz00_4076;

								BgL_tmpz00_4076 =
									((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3250));
								BgL_arg1807z00_3419 = (BgL_objectz00_bglt) (BgL_tmpz00_4076);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Reduce/same.scm 160 */
									long BgL_idxz00_3420;

									BgL_idxz00_3420 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3419);
									BgL_test2037z00_4075 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3420 + 2L)) == BgL_classz00_3418);
								}
							else
								{	/* Reduce/same.scm 160 */
									bool_t BgL_res1934z00_3423;

									{	/* Reduce/same.scm 160 */
										obj_t BgL_oclassz00_3424;

										{	/* Reduce/same.scm 160 */
											obj_t BgL_arg1815z00_3425;
											long BgL_arg1816z00_3426;

											BgL_arg1815z00_3425 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Reduce/same.scm 160 */
												long BgL_arg1817z00_3427;

												BgL_arg1817z00_3427 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3419);
												BgL_arg1816z00_3426 =
													(BgL_arg1817z00_3427 - OBJECT_TYPE);
											}
											BgL_oclassz00_3424 =
												VECTOR_REF(BgL_arg1815z00_3425, BgL_arg1816z00_3426);
										}
										{	/* Reduce/same.scm 160 */
											bool_t BgL__ortest_1115z00_3428;

											BgL__ortest_1115z00_3428 =
												(BgL_classz00_3418 == BgL_oclassz00_3424);
											if (BgL__ortest_1115z00_3428)
												{	/* Reduce/same.scm 160 */
													BgL_res1934z00_3423 = BgL__ortest_1115z00_3428;
												}
											else
												{	/* Reduce/same.scm 160 */
													long BgL_odepthz00_3429;

													{	/* Reduce/same.scm 160 */
														obj_t BgL_arg1804z00_3430;

														BgL_arg1804z00_3430 = (BgL_oclassz00_3424);
														BgL_odepthz00_3429 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3430);
													}
													if ((2L < BgL_odepthz00_3429))
														{	/* Reduce/same.scm 160 */
															obj_t BgL_arg1802z00_3431;

															{	/* Reduce/same.scm 160 */
																obj_t BgL_arg1803z00_3432;

																BgL_arg1803z00_3432 = (BgL_oclassz00_3424);
																BgL_arg1802z00_3431 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3432,
																	2L);
															}
															BgL_res1934z00_3423 =
																(BgL_arg1802z00_3431 == BgL_classz00_3418);
														}
													else
														{	/* Reduce/same.scm 160 */
															BgL_res1934z00_3423 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2037z00_4075 = BgL_res1934z00_3423;
								}
						}
					}
					if (BgL_test2037z00_4075)
						{	/* Reduce/same.scm 160 */
							if (
								(((obj_t)
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_castz00_bglt) BgL_nodez00_3249))))->
											BgL_typez00)) ==
									((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_node2z00_3250)))->BgL_typez00))))
								{	/* Reduce/same.scm 162 */
									BgL_nodez00_bglt BgL_arg1701z00_3433;
									BgL_nodez00_bglt BgL_arg1702z00_3434;

									BgL_arg1701z00_3433 =
										(((BgL_castz00_bglt) COBJECT(
												((BgL_castz00_bglt) BgL_nodez00_3249)))->BgL_argz00);
									BgL_arg1702z00_3434 =
										(((BgL_castz00_bglt) COBJECT(
												((BgL_castz00_bglt)
													((BgL_nodez00_bglt) BgL_node2z00_3250))))->
										BgL_argz00);
									BgL_tmpz00_4074 =
										BGl_samezd2nodezf3z21zzreduce_samez00(BgL_arg1701z00_3433,
										BgL_arg1702z00_3434, BgL_aliasz00_3251);
								}
							else
								{	/* Reduce/same.scm 161 */
									BgL_tmpz00_4074 = ((bool_t) 0);
								}
						}
					else
						{	/* Reduce/same.scm 160 */
							BgL_tmpz00_4074 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_4074);
			}
		}

	}



/* &same-node?-valloc1308 */
	obj_t BGl_z62samezd2nodezf3zd2valloc1308z91zzreduce_samez00(obj_t
		BgL_envz00_3252, obj_t BgL_nodez00_3253, obj_t BgL_node2z00_3254,
		obj_t BgL_aliasz00_3255)
	{
		{	/* Reduce/same.scm 153 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &same-node?-setfield1306 */
	obj_t BGl_z62samezd2nodezf3zd2setfield1306z91zzreduce_samez00(obj_t
		BgL_envz00_3256, obj_t BgL_nodez00_3257, obj_t BgL_node2z00_3258,
		obj_t BgL_aliasz00_3259)
	{
		{	/* Reduce/same.scm 145 */
			{

				{	/* Reduce/same.scm 146 */
					bool_t BgL_test2042z00_4116;

					{	/* Reduce/same.scm 146 */
						obj_t BgL_classz00_3441;

						BgL_classz00_3441 = BGl_setfieldz00zzast_nodez00;
						{	/* Reduce/same.scm 146 */
							BgL_objectz00_bglt BgL_arg1807z00_3442;

							{	/* Reduce/same.scm 146 */
								obj_t BgL_tmpz00_4117;

								BgL_tmpz00_4117 =
									((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3258));
								BgL_arg1807z00_3442 = (BgL_objectz00_bglt) (BgL_tmpz00_4117);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Reduce/same.scm 146 */
									long BgL_idxz00_3443;

									BgL_idxz00_3443 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3442);
									BgL_test2042z00_4116 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3443 + 5L)) == BgL_classz00_3441);
								}
							else
								{	/* Reduce/same.scm 146 */
									bool_t BgL_res1933z00_3446;

									{	/* Reduce/same.scm 146 */
										obj_t BgL_oclassz00_3447;

										{	/* Reduce/same.scm 146 */
											obj_t BgL_arg1815z00_3448;
											long BgL_arg1816z00_3449;

											BgL_arg1815z00_3448 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Reduce/same.scm 146 */
												long BgL_arg1817z00_3450;

												BgL_arg1817z00_3450 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3442);
												BgL_arg1816z00_3449 =
													(BgL_arg1817z00_3450 - OBJECT_TYPE);
											}
											BgL_oclassz00_3447 =
												VECTOR_REF(BgL_arg1815z00_3448, BgL_arg1816z00_3449);
										}
										{	/* Reduce/same.scm 146 */
											bool_t BgL__ortest_1115z00_3451;

											BgL__ortest_1115z00_3451 =
												(BgL_classz00_3441 == BgL_oclassz00_3447);
											if (BgL__ortest_1115z00_3451)
												{	/* Reduce/same.scm 146 */
													BgL_res1933z00_3446 = BgL__ortest_1115z00_3451;
												}
											else
												{	/* Reduce/same.scm 146 */
													long BgL_odepthz00_3452;

													{	/* Reduce/same.scm 146 */
														obj_t BgL_arg1804z00_3453;

														BgL_arg1804z00_3453 = (BgL_oclassz00_3447);
														BgL_odepthz00_3452 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3453);
													}
													if ((5L < BgL_odepthz00_3452))
														{	/* Reduce/same.scm 146 */
															obj_t BgL_arg1802z00_3454;

															{	/* Reduce/same.scm 146 */
																obj_t BgL_arg1803z00_3455;

																BgL_arg1803z00_3455 = (BgL_oclassz00_3447);
																BgL_arg1802z00_3454 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3455,
																	5L);
															}
															BgL_res1933z00_3446 =
																(BgL_arg1802z00_3454 == BgL_classz00_3441);
														}
													else
														{	/* Reduce/same.scm 146 */
															BgL_res1933z00_3446 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2042z00_4116 = BgL_res1933z00_3446;
								}
						}
					}
					if (BgL_test2042z00_4116)
						{	/* Reduce/same.scm 147 */
							bool_t BgL_test2046z00_4140;

							{	/* Reduce/same.scm 147 */
								obj_t BgL_arg1699z00_3456;
								obj_t BgL_arg1700z00_3457;

								BgL_arg1699z00_3456 =
									(((BgL_setfieldz00_bglt) COBJECT(
											((BgL_setfieldz00_bglt) BgL_nodez00_3257)))->
									BgL_fnamez00);
								BgL_arg1700z00_3457 =
									(((BgL_setfieldz00_bglt)
										COBJECT(((BgL_setfieldz00_bglt) ((BgL_nodez00_bglt)
													BgL_node2z00_3258))))->BgL_fnamez00);
								{	/* Reduce/same.scm 147 */
									long BgL_l1z00_3458;

									BgL_l1z00_3458 = STRING_LENGTH(BgL_arg1699z00_3456);
									if ((BgL_l1z00_3458 == STRING_LENGTH(BgL_arg1700z00_3457)))
										{	/* Reduce/same.scm 147 */
											int BgL_arg1282z00_3459;

											{	/* Reduce/same.scm 147 */
												char *BgL_auxz00_4152;
												char *BgL_tmpz00_4150;

												BgL_auxz00_4152 =
													BSTRING_TO_STRING(BgL_arg1700z00_3457);
												BgL_tmpz00_4150 =
													BSTRING_TO_STRING(BgL_arg1699z00_3456);
												BgL_arg1282z00_3459 =
													memcmp(BgL_tmpz00_4150, BgL_auxz00_4152,
													BgL_l1z00_3458);
											}
											BgL_test2046z00_4140 =
												((long) (BgL_arg1282z00_3459) == 0L);
										}
									else
										{	/* Reduce/same.scm 147 */
											BgL_test2046z00_4140 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test2046z00_4140)
								{	/* Reduce/same.scm 147 */
									{	/* Reduce/same.scm 145 */
										obj_t BgL_nextzd2method1305zd2_3440;

										BgL_nextzd2method1305zd2_3440 =
											BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
											((BgL_objectz00_bglt)
												((BgL_setfieldz00_bglt) BgL_nodez00_3257)),
											BGl_samezd2nodezf3zd2envzf3zzreduce_samez00,
											BGl_setfieldz00zzast_nodez00);
										return BGL_PROCEDURE_CALL3(BgL_nextzd2method1305zd2_3440,
											((obj_t) ((BgL_setfieldz00_bglt) BgL_nodez00_3257)),
											((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3258)),
											BgL_aliasz00_3259);
									}
								}
							else
								{	/* Reduce/same.scm 147 */
									return BFALSE;
								}
						}
					else
						{	/* Reduce/same.scm 146 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &same-node?-getfield1304 */
	obj_t BGl_z62samezd2nodezf3zd2getfield1304z91zzreduce_samez00(obj_t
		BgL_envz00_3260, obj_t BgL_nodez00_3261, obj_t BgL_node2z00_3262,
		obj_t BgL_aliasz00_3263)
	{
		{	/* Reduce/same.scm 137 */
			{

				{	/* Reduce/same.scm 138 */
					bool_t BgL_test2048z00_4170;

					{	/* Reduce/same.scm 138 */
						obj_t BgL_classz00_3464;

						BgL_classz00_3464 = BGl_getfieldz00zzast_nodez00;
						{	/* Reduce/same.scm 138 */
							BgL_objectz00_bglt BgL_arg1807z00_3465;

							{	/* Reduce/same.scm 138 */
								obj_t BgL_tmpz00_4171;

								BgL_tmpz00_4171 =
									((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3262));
								BgL_arg1807z00_3465 = (BgL_objectz00_bglt) (BgL_tmpz00_4171);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Reduce/same.scm 138 */
									long BgL_idxz00_3466;

									BgL_idxz00_3466 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3465);
									BgL_test2048z00_4170 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3466 + 5L)) == BgL_classz00_3464);
								}
							else
								{	/* Reduce/same.scm 138 */
									bool_t BgL_res1932z00_3469;

									{	/* Reduce/same.scm 138 */
										obj_t BgL_oclassz00_3470;

										{	/* Reduce/same.scm 138 */
											obj_t BgL_arg1815z00_3471;
											long BgL_arg1816z00_3472;

											BgL_arg1815z00_3471 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Reduce/same.scm 138 */
												long BgL_arg1817z00_3473;

												BgL_arg1817z00_3473 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3465);
												BgL_arg1816z00_3472 =
													(BgL_arg1817z00_3473 - OBJECT_TYPE);
											}
											BgL_oclassz00_3470 =
												VECTOR_REF(BgL_arg1815z00_3471, BgL_arg1816z00_3472);
										}
										{	/* Reduce/same.scm 138 */
											bool_t BgL__ortest_1115z00_3474;

											BgL__ortest_1115z00_3474 =
												(BgL_classz00_3464 == BgL_oclassz00_3470);
											if (BgL__ortest_1115z00_3474)
												{	/* Reduce/same.scm 138 */
													BgL_res1932z00_3469 = BgL__ortest_1115z00_3474;
												}
											else
												{	/* Reduce/same.scm 138 */
													long BgL_odepthz00_3475;

													{	/* Reduce/same.scm 138 */
														obj_t BgL_arg1804z00_3476;

														BgL_arg1804z00_3476 = (BgL_oclassz00_3470);
														BgL_odepthz00_3475 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3476);
													}
													if ((5L < BgL_odepthz00_3475))
														{	/* Reduce/same.scm 138 */
															obj_t BgL_arg1802z00_3477;

															{	/* Reduce/same.scm 138 */
																obj_t BgL_arg1803z00_3478;

																BgL_arg1803z00_3478 = (BgL_oclassz00_3470);
																BgL_arg1802z00_3477 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3478,
																	5L);
															}
															BgL_res1932z00_3469 =
																(BgL_arg1802z00_3477 == BgL_classz00_3464);
														}
													else
														{	/* Reduce/same.scm 138 */
															BgL_res1932z00_3469 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2048z00_4170 = BgL_res1932z00_3469;
								}
						}
					}
					if (BgL_test2048z00_4170)
						{	/* Reduce/same.scm 139 */
							bool_t BgL_test2052z00_4194;

							{	/* Reduce/same.scm 139 */
								obj_t BgL_arg1691z00_3479;
								obj_t BgL_arg1692z00_3480;

								BgL_arg1691z00_3479 =
									(((BgL_getfieldz00_bglt) COBJECT(
											((BgL_getfieldz00_bglt) BgL_nodez00_3261)))->
									BgL_fnamez00);
								BgL_arg1692z00_3480 =
									(((BgL_getfieldz00_bglt)
										COBJECT(((BgL_getfieldz00_bglt) ((BgL_nodez00_bglt)
													BgL_node2z00_3262))))->BgL_fnamez00);
								{	/* Reduce/same.scm 139 */
									long BgL_l1z00_3481;

									BgL_l1z00_3481 = STRING_LENGTH(BgL_arg1691z00_3479);
									if ((BgL_l1z00_3481 == STRING_LENGTH(BgL_arg1692z00_3480)))
										{	/* Reduce/same.scm 139 */
											int BgL_arg1282z00_3482;

											{	/* Reduce/same.scm 139 */
												char *BgL_auxz00_4206;
												char *BgL_tmpz00_4204;

												BgL_auxz00_4206 =
													BSTRING_TO_STRING(BgL_arg1692z00_3480);
												BgL_tmpz00_4204 =
													BSTRING_TO_STRING(BgL_arg1691z00_3479);
												BgL_arg1282z00_3482 =
													memcmp(BgL_tmpz00_4204, BgL_auxz00_4206,
													BgL_l1z00_3481);
											}
											BgL_test2052z00_4194 =
												((long) (BgL_arg1282z00_3482) == 0L);
										}
									else
										{	/* Reduce/same.scm 139 */
											BgL_test2052z00_4194 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test2052z00_4194)
								{	/* Reduce/same.scm 139 */
									{	/* Reduce/same.scm 137 */
										obj_t BgL_nextzd2method1303zd2_3463;

										BgL_nextzd2method1303zd2_3463 =
											BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
											((BgL_objectz00_bglt)
												((BgL_getfieldz00_bglt) BgL_nodez00_3261)),
											BGl_samezd2nodezf3zd2envzf3zzreduce_samez00,
											BGl_getfieldz00zzast_nodez00);
										return BGL_PROCEDURE_CALL3(BgL_nextzd2method1303zd2_3463,
											((obj_t) ((BgL_getfieldz00_bglt) BgL_nodez00_3261)),
											((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3262)),
											BgL_aliasz00_3263);
									}
								}
							else
								{	/* Reduce/same.scm 139 */
									return BFALSE;
								}
						}
					else
						{	/* Reduce/same.scm 138 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &same-node?-pragma1302 */
	obj_t BGl_z62samezd2nodezf3zd2pragma1302z91zzreduce_samez00(obj_t
		BgL_envz00_3264, obj_t BgL_nodez00_3265, obj_t BgL_node2z00_3266,
		obj_t BgL_aliasz00_3267)
	{
		{	/* Reduce/same.scm 128 */
			{	/* Reduce/same.scm 129 */
				bool_t BgL_tmpz00_4224;

				{	/* Reduce/same.scm 129 */
					bool_t BgL_test2054z00_4225;

					{	/* Reduce/same.scm 129 */
						obj_t BgL_classz00_3485;

						BgL_classz00_3485 = BGl_pragmaz00zzast_nodez00;
						{	/* Reduce/same.scm 129 */
							BgL_objectz00_bglt BgL_arg1807z00_3486;

							{	/* Reduce/same.scm 129 */
								obj_t BgL_tmpz00_4226;

								BgL_tmpz00_4226 =
									((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3266));
								BgL_arg1807z00_3486 = (BgL_objectz00_bglt) (BgL_tmpz00_4226);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Reduce/same.scm 129 */
									long BgL_idxz00_3487;

									BgL_idxz00_3487 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3486);
									BgL_test2054z00_4225 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3487 + 4L)) == BgL_classz00_3485);
								}
							else
								{	/* Reduce/same.scm 129 */
									bool_t BgL_res1931z00_3490;

									{	/* Reduce/same.scm 129 */
										obj_t BgL_oclassz00_3491;

										{	/* Reduce/same.scm 129 */
											obj_t BgL_arg1815z00_3492;
											long BgL_arg1816z00_3493;

											BgL_arg1815z00_3492 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Reduce/same.scm 129 */
												long BgL_arg1817z00_3494;

												BgL_arg1817z00_3494 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3486);
												BgL_arg1816z00_3493 =
													(BgL_arg1817z00_3494 - OBJECT_TYPE);
											}
											BgL_oclassz00_3491 =
												VECTOR_REF(BgL_arg1815z00_3492, BgL_arg1816z00_3493);
										}
										{	/* Reduce/same.scm 129 */
											bool_t BgL__ortest_1115z00_3495;

											BgL__ortest_1115z00_3495 =
												(BgL_classz00_3485 == BgL_oclassz00_3491);
											if (BgL__ortest_1115z00_3495)
												{	/* Reduce/same.scm 129 */
													BgL_res1931z00_3490 = BgL__ortest_1115z00_3495;
												}
											else
												{	/* Reduce/same.scm 129 */
													long BgL_odepthz00_3496;

													{	/* Reduce/same.scm 129 */
														obj_t BgL_arg1804z00_3497;

														BgL_arg1804z00_3497 = (BgL_oclassz00_3491);
														BgL_odepthz00_3496 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3497);
													}
													if ((4L < BgL_odepthz00_3496))
														{	/* Reduce/same.scm 129 */
															obj_t BgL_arg1802z00_3498;

															{	/* Reduce/same.scm 129 */
																obj_t BgL_arg1803z00_3499;

																BgL_arg1803z00_3499 = (BgL_oclassz00_3491);
																BgL_arg1802z00_3498 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3499,
																	4L);
															}
															BgL_res1931z00_3490 =
																(BgL_arg1802z00_3498 == BgL_classz00_3485);
														}
													else
														{	/* Reduce/same.scm 129 */
															BgL_res1931z00_3490 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2054z00_4225 = BgL_res1931z00_3490;
								}
						}
					}
					if (BgL_test2054z00_4225)
						{	/* Reduce/same.scm 129 */
							if (
								(((obj_t)
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_pragmaz00_bglt) BgL_nodez00_3265))))->
											BgL_typez00)) ==
									((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_node2z00_3266)))->BgL_typez00))))
								{	/* Reduce/same.scm 131 */
									bool_t BgL_test2059z00_4258;

									{	/* Reduce/same.scm 131 */
										obj_t BgL_arg1678z00_3500;
										obj_t BgL_arg1681z00_3501;

										BgL_arg1678z00_3500 =
											(((BgL_pragmaz00_bglt) COBJECT(
													((BgL_pragmaz00_bglt) BgL_nodez00_3265)))->
											BgL_formatz00);
										BgL_arg1681z00_3501 =
											(((BgL_pragmaz00_bglt)
												COBJECT(((BgL_pragmaz00_bglt) ((BgL_nodez00_bglt)
															BgL_node2z00_3266))))->BgL_formatz00);
										{	/* Reduce/same.scm 131 */
											long BgL_l1z00_3502;

											BgL_l1z00_3502 = STRING_LENGTH(BgL_arg1678z00_3500);
											if (
												(BgL_l1z00_3502 == STRING_LENGTH(BgL_arg1681z00_3501)))
												{	/* Reduce/same.scm 131 */
													int BgL_arg1282z00_3503;

													{	/* Reduce/same.scm 131 */
														char *BgL_auxz00_4270;
														char *BgL_tmpz00_4268;

														BgL_auxz00_4270 =
															BSTRING_TO_STRING(BgL_arg1681z00_3501);
														BgL_tmpz00_4268 =
															BSTRING_TO_STRING(BgL_arg1678z00_3500);
														BgL_arg1282z00_3503 =
															memcmp(BgL_tmpz00_4268, BgL_auxz00_4270,
															BgL_l1z00_3502);
													}
													BgL_test2059z00_4258 =
														((long) (BgL_arg1282z00_3503) == 0L);
												}
											else
												{	/* Reduce/same.scm 131 */
													BgL_test2059z00_4258 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test2059z00_4258)
										{	/* Reduce/same.scm 132 */
											obj_t BgL_arg1663z00_3504;
											obj_t BgL_arg1675z00_3505;

											BgL_arg1663z00_3504 =
												(((BgL_externz00_bglt) COBJECT(
														((BgL_externz00_bglt)
															((BgL_pragmaz00_bglt) BgL_nodez00_3265))))->
												BgL_exprza2za2);
											BgL_arg1675z00_3505 =
												(((BgL_externz00_bglt)
													COBJECT(((BgL_externz00_bglt) ((BgL_pragmaz00_bglt) (
																	(BgL_nodez00_bglt) BgL_node2z00_3266)))))->
												BgL_exprza2za2);
											BgL_tmpz00_4224 =
												BGl_samezd2nodeza2zf3z83zzreduce_samez00
												(BgL_arg1663z00_3504, BgL_arg1675z00_3505,
												BgL_aliasz00_3267);
										}
									else
										{	/* Reduce/same.scm 131 */
											BgL_tmpz00_4224 = ((bool_t) 0);
										}
								}
							else
								{	/* Reduce/same.scm 130 */
									BgL_tmpz00_4224 = ((bool_t) 0);
								}
						}
					else
						{	/* Reduce/same.scm 129 */
							BgL_tmpz00_4224 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_4224);
			}
		}

	}



/* &same-node?-extern1300 */
	obj_t BGl_z62samezd2nodezf3zd2extern1300z91zzreduce_samez00(obj_t
		BgL_envz00_3268, obj_t BgL_nodez00_3269, obj_t BgL_node2z00_3270,
		obj_t BgL_aliasz00_3271)
	{
		{	/* Reduce/same.scm 117 */
			{	/* Tools/trace.sch 53 */
				bool_t BgL_tmpz00_4284;

				{	/* Tools/trace.sch 53 */
					bool_t BgL_test2061z00_4285;

					{	/* Tools/trace.sch 53 */
						obj_t BgL_classz00_3508;

						BgL_classz00_3508 = BGl_externz00zzast_nodez00;
						{	/* Tools/trace.sch 53 */
							BgL_objectz00_bglt BgL_arg1807z00_3509;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_tmpz00_4286;

								BgL_tmpz00_4286 =
									((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3270));
								BgL_arg1807z00_3509 = (BgL_objectz00_bglt) (BgL_tmpz00_4286);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Tools/trace.sch 53 */
									long BgL_idxz00_3510;

									BgL_idxz00_3510 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3509);
									BgL_test2061z00_4285 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3510 + 3L)) == BgL_classz00_3508);
								}
							else
								{	/* Tools/trace.sch 53 */
									bool_t BgL_res1930z00_3513;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_oclassz00_3514;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1815z00_3515;
											long BgL_arg1816z00_3516;

											BgL_arg1815z00_3515 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Tools/trace.sch 53 */
												long BgL_arg1817z00_3517;

												BgL_arg1817z00_3517 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3509);
												BgL_arg1816z00_3516 =
													(BgL_arg1817z00_3517 - OBJECT_TYPE);
											}
											BgL_oclassz00_3514 =
												VECTOR_REF(BgL_arg1815z00_3515, BgL_arg1816z00_3516);
										}
										{	/* Tools/trace.sch 53 */
											bool_t BgL__ortest_1115z00_3518;

											BgL__ortest_1115z00_3518 =
												(BgL_classz00_3508 == BgL_oclassz00_3514);
											if (BgL__ortest_1115z00_3518)
												{	/* Tools/trace.sch 53 */
													BgL_res1930z00_3513 = BgL__ortest_1115z00_3518;
												}
											else
												{	/* Tools/trace.sch 53 */
													long BgL_odepthz00_3519;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg1804z00_3520;

														BgL_arg1804z00_3520 = (BgL_oclassz00_3514);
														BgL_odepthz00_3519 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3520);
													}
													if ((3L < BgL_odepthz00_3519))
														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg1802z00_3521;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg1803z00_3522;

																BgL_arg1803z00_3522 = (BgL_oclassz00_3514);
																BgL_arg1802z00_3521 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3522,
																	3L);
															}
															BgL_res1930z00_3513 =
																(BgL_arg1802z00_3521 == BgL_classz00_3508);
														}
													else
														{	/* Tools/trace.sch 53 */
															BgL_res1930z00_3513 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2061z00_4285 = BgL_res1930z00_3513;
								}
						}
					}
					if (BgL_test2061z00_4285)
						{	/* Tools/trace.sch 53 */
							if (
								(((obj_t)
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_externz00_bglt) BgL_nodez00_3269))))->
											BgL_typez00)) ==
									((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_node2z00_3270)))->BgL_typez00))))
								{	/* Tools/trace.sch 53 */
									obj_t BgL_arg1650z00_3523;
									obj_t BgL_arg1651z00_3524;

									BgL_arg1650z00_3523 =
										(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt) BgL_nodez00_3269)))->
										BgL_exprza2za2);
									BgL_arg1651z00_3524 =
										(((BgL_externz00_bglt)
											COBJECT(((BgL_externz00_bglt) ((BgL_nodez00_bglt)
														BgL_node2z00_3270))))->BgL_exprza2za2);
									BgL_tmpz00_4284 =
										BGl_samezd2nodeza2zf3z83zzreduce_samez00
										(BgL_arg1650z00_3523, BgL_arg1651z00_3524,
										BgL_aliasz00_3271);
								}
							else
								{	/* Tools/trace.sch 53 */
									BgL_tmpz00_4284 = ((bool_t) 0);
								}
						}
					else
						{	/* Tools/trace.sch 53 */
							BgL_tmpz00_4284 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_4284);
			}
		}

	}



/* &same-node?-app1298 */
	obj_t BGl_z62samezd2nodezf3zd2app1298z91zzreduce_samez00(obj_t
		BgL_envz00_3272, obj_t BgL_nodez00_3273, obj_t BgL_node2z00_3274,
		obj_t BgL_aliasz00_3275)
	{
		{	/* Reduce/same.scm 108 */
			{	/* Reduce/same.scm 109 */
				bool_t BgL_tmpz00_4325;

				{	/* Reduce/same.scm 109 */
					bool_t BgL_test2066z00_4326;

					{	/* Reduce/same.scm 109 */
						obj_t BgL_classz00_3527;

						BgL_classz00_3527 = BGl_appz00zzast_nodez00;
						{	/* Reduce/same.scm 109 */
							BgL_objectz00_bglt BgL_arg1807z00_3528;

							{	/* Reduce/same.scm 109 */
								obj_t BgL_tmpz00_4327;

								BgL_tmpz00_4327 =
									((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3274));
								BgL_arg1807z00_3528 = (BgL_objectz00_bglt) (BgL_tmpz00_4327);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Reduce/same.scm 109 */
									long BgL_idxz00_3529;

									BgL_idxz00_3529 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3528);
									BgL_test2066z00_4326 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3529 + 3L)) == BgL_classz00_3527);
								}
							else
								{	/* Reduce/same.scm 109 */
									bool_t BgL_res1929z00_3532;

									{	/* Reduce/same.scm 109 */
										obj_t BgL_oclassz00_3533;

										{	/* Reduce/same.scm 109 */
											obj_t BgL_arg1815z00_3534;
											long BgL_arg1816z00_3535;

											BgL_arg1815z00_3534 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Reduce/same.scm 109 */
												long BgL_arg1817z00_3536;

												BgL_arg1817z00_3536 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3528);
												BgL_arg1816z00_3535 =
													(BgL_arg1817z00_3536 - OBJECT_TYPE);
											}
											BgL_oclassz00_3533 =
												VECTOR_REF(BgL_arg1815z00_3534, BgL_arg1816z00_3535);
										}
										{	/* Reduce/same.scm 109 */
											bool_t BgL__ortest_1115z00_3537;

											BgL__ortest_1115z00_3537 =
												(BgL_classz00_3527 == BgL_oclassz00_3533);
											if (BgL__ortest_1115z00_3537)
												{	/* Reduce/same.scm 109 */
													BgL_res1929z00_3532 = BgL__ortest_1115z00_3537;
												}
											else
												{	/* Reduce/same.scm 109 */
													long BgL_odepthz00_3538;

													{	/* Reduce/same.scm 109 */
														obj_t BgL_arg1804z00_3539;

														BgL_arg1804z00_3539 = (BgL_oclassz00_3533);
														BgL_odepthz00_3538 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3539);
													}
													if ((3L < BgL_odepthz00_3538))
														{	/* Reduce/same.scm 109 */
															obj_t BgL_arg1802z00_3540;

															{	/* Reduce/same.scm 109 */
																obj_t BgL_arg1803z00_3541;

																BgL_arg1803z00_3541 = (BgL_oclassz00_3533);
																BgL_arg1802z00_3540 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3541,
																	3L);
															}
															BgL_res1929z00_3532 =
																(BgL_arg1802z00_3540 == BgL_classz00_3527);
														}
													else
														{	/* Reduce/same.scm 109 */
															BgL_res1929z00_3532 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2066z00_4326 = BgL_res1929z00_3532;
								}
						}
					}
					if (BgL_test2066z00_4326)
						{	/* Reduce/same.scm 109 */
							if (
								(((obj_t)
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_appz00_bglt) BgL_nodez00_3273))))->
											BgL_typez00)) ==
									((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_node2z00_3274)))->BgL_typez00))))
								{	/* Reduce/same.scm 111 */
									bool_t BgL_test2071z00_4359;

									{	/* Reduce/same.scm 111 */
										BgL_varz00_bglt BgL_arg1629z00_3542;
										BgL_varz00_bglt BgL_arg1630z00_3543;

										BgL_arg1629z00_3542 =
											(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt) BgL_nodez00_3273)))->BgL_funz00);
										BgL_arg1630z00_3543 =
											(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt)
														((BgL_nodez00_bglt) BgL_node2z00_3274))))->
											BgL_funz00);
										BgL_test2071z00_4359 =
											BGl_samezd2nodezf3z21zzreduce_samez00(((BgL_nodez00_bglt)
												BgL_arg1629z00_3542),
											((BgL_nodez00_bglt) BgL_arg1630z00_3543),
											BgL_aliasz00_3275);
									}
									if (BgL_test2071z00_4359)
										{	/* Reduce/same.scm 112 */
											obj_t BgL_arg1626z00_3544;
											obj_t BgL_arg1627z00_3545;

											BgL_arg1626z00_3544 =
												(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_nodez00_3273)))->
												BgL_argsz00);
											BgL_arg1627z00_3545 =
												(((BgL_appz00_bglt)
													COBJECT(((BgL_appz00_bglt) ((BgL_nodez00_bglt)
																BgL_node2z00_3274))))->BgL_argsz00);
											BgL_tmpz00_4325 =
												BGl_samezd2nodeza2zf3z83zzreduce_samez00
												(BgL_arg1626z00_3544, BgL_arg1627z00_3545,
												BgL_aliasz00_3275);
										}
									else
										{	/* Reduce/same.scm 111 */
											BgL_tmpz00_4325 = ((bool_t) 0);
										}
								}
							else
								{	/* Reduce/same.scm 110 */
									BgL_tmpz00_4325 = ((bool_t) 0);
								}
						}
					else
						{	/* Reduce/same.scm 109 */
							BgL_tmpz00_4325 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_4325);
			}
		}

	}



/* &same-node?-funcall1296 */
	obj_t BGl_z62samezd2nodezf3zd2funcall1296z91zzreduce_samez00(obj_t
		BgL_envz00_3276, obj_t BgL_nodez00_3277, obj_t BgL_node2z00_3278,
		obj_t BgL_aliasz00_3279)
	{
		{	/* Reduce/same.scm 99 */
			{	/* Reduce/same.scm 100 */
				bool_t BgL_tmpz00_4375;

				{	/* Reduce/same.scm 100 */
					bool_t BgL_test2072z00_4376;

					{	/* Reduce/same.scm 100 */
						obj_t BgL_classz00_3548;

						BgL_classz00_3548 = BGl_funcallz00zzast_nodez00;
						{	/* Reduce/same.scm 100 */
							BgL_objectz00_bglt BgL_arg1807z00_3549;

							{	/* Reduce/same.scm 100 */
								obj_t BgL_tmpz00_4377;

								BgL_tmpz00_4377 =
									((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3278));
								BgL_arg1807z00_3549 = (BgL_objectz00_bglt) (BgL_tmpz00_4377);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Reduce/same.scm 100 */
									long BgL_idxz00_3550;

									BgL_idxz00_3550 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3549);
									BgL_test2072z00_4376 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3550 + 2L)) == BgL_classz00_3548);
								}
							else
								{	/* Reduce/same.scm 100 */
									bool_t BgL_res1928z00_3553;

									{	/* Reduce/same.scm 100 */
										obj_t BgL_oclassz00_3554;

										{	/* Reduce/same.scm 100 */
											obj_t BgL_arg1815z00_3555;
											long BgL_arg1816z00_3556;

											BgL_arg1815z00_3555 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Reduce/same.scm 100 */
												long BgL_arg1817z00_3557;

												BgL_arg1817z00_3557 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3549);
												BgL_arg1816z00_3556 =
													(BgL_arg1817z00_3557 - OBJECT_TYPE);
											}
											BgL_oclassz00_3554 =
												VECTOR_REF(BgL_arg1815z00_3555, BgL_arg1816z00_3556);
										}
										{	/* Reduce/same.scm 100 */
											bool_t BgL__ortest_1115z00_3558;

											BgL__ortest_1115z00_3558 =
												(BgL_classz00_3548 == BgL_oclassz00_3554);
											if (BgL__ortest_1115z00_3558)
												{	/* Reduce/same.scm 100 */
													BgL_res1928z00_3553 = BgL__ortest_1115z00_3558;
												}
											else
												{	/* Reduce/same.scm 100 */
													long BgL_odepthz00_3559;

													{	/* Reduce/same.scm 100 */
														obj_t BgL_arg1804z00_3560;

														BgL_arg1804z00_3560 = (BgL_oclassz00_3554);
														BgL_odepthz00_3559 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3560);
													}
													if ((2L < BgL_odepthz00_3559))
														{	/* Reduce/same.scm 100 */
															obj_t BgL_arg1802z00_3561;

															{	/* Reduce/same.scm 100 */
																obj_t BgL_arg1803z00_3562;

																BgL_arg1803z00_3562 = (BgL_oclassz00_3554);
																BgL_arg1802z00_3561 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3562,
																	2L);
															}
															BgL_res1928z00_3553 =
																(BgL_arg1802z00_3561 == BgL_classz00_3548);
														}
													else
														{	/* Reduce/same.scm 100 */
															BgL_res1928z00_3553 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2072z00_4376 = BgL_res1928z00_3553;
								}
						}
					}
					if (BgL_test2072z00_4376)
						{	/* Reduce/same.scm 100 */
							if (
								(((obj_t)
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_funcallz00_bglt) BgL_nodez00_3277))))->
											BgL_typez00)) ==
									((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_node2z00_3278)))->BgL_typez00))))
								{	/* Reduce/same.scm 102 */
									bool_t BgL_test2077z00_4409;

									{	/* Reduce/same.scm 102 */
										BgL_nodez00_bglt BgL_arg1613z00_3563;
										BgL_nodez00_bglt BgL_arg1615z00_3564;

										BgL_arg1613z00_3563 =
											(((BgL_funcallz00_bglt) COBJECT(
													((BgL_funcallz00_bglt) BgL_nodez00_3277)))->
											BgL_funz00);
										BgL_arg1615z00_3564 =
											(((BgL_funcallz00_bglt)
												COBJECT(((BgL_funcallz00_bglt) ((BgL_nodez00_bglt)
															BgL_node2z00_3278))))->BgL_funz00);
										BgL_test2077z00_4409 =
											BGl_samezd2nodezf3z21zzreduce_samez00(BgL_arg1613z00_3563,
											BgL_arg1615z00_3564, BgL_aliasz00_3279);
									}
									if (BgL_test2077z00_4409)
										{	/* Reduce/same.scm 103 */
											obj_t BgL_arg1609z00_3565;
											obj_t BgL_arg1611z00_3566;

											BgL_arg1609z00_3565 =
												(((BgL_funcallz00_bglt) COBJECT(
														((BgL_funcallz00_bglt) BgL_nodez00_3277)))->
												BgL_argsz00);
											BgL_arg1611z00_3566 =
												(((BgL_funcallz00_bglt)
													COBJECT(((BgL_funcallz00_bglt) ((BgL_nodez00_bglt)
																BgL_node2z00_3278))))->BgL_argsz00);
											BgL_tmpz00_4375 =
												BGl_samezd2nodeza2zf3z83zzreduce_samez00
												(BgL_arg1609z00_3565, BgL_arg1611z00_3566,
												BgL_aliasz00_3279);
										}
									else
										{	/* Reduce/same.scm 102 */
											BgL_tmpz00_4375 = ((bool_t) 0);
										}
								}
							else
								{	/* Reduce/same.scm 101 */
									BgL_tmpz00_4375 = ((bool_t) 0);
								}
						}
					else
						{	/* Reduce/same.scm 100 */
							BgL_tmpz00_4375 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_4375);
			}
		}

	}



/* &same-node?-app-ly1294 */
	obj_t BGl_z62samezd2nodezf3zd2appzd2ly1294z43zzreduce_samez00(obj_t
		BgL_envz00_3280, obj_t BgL_nodez00_3281, obj_t BgL_node2z00_3282,
		obj_t BgL_aliasz00_3283)
	{
		{	/* Reduce/same.scm 90 */
			{	/* Reduce/same.scm 91 */
				bool_t BgL_tmpz00_4423;

				{	/* Reduce/same.scm 91 */
					bool_t BgL_test2078z00_4424;

					{	/* Reduce/same.scm 91 */
						obj_t BgL_classz00_3569;

						BgL_classz00_3569 = BGl_appzd2lyzd2zzast_nodez00;
						{	/* Reduce/same.scm 91 */
							BgL_objectz00_bglt BgL_arg1807z00_3570;

							{	/* Reduce/same.scm 91 */
								obj_t BgL_tmpz00_4425;

								BgL_tmpz00_4425 =
									((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3282));
								BgL_arg1807z00_3570 = (BgL_objectz00_bglt) (BgL_tmpz00_4425);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Reduce/same.scm 91 */
									long BgL_idxz00_3571;

									BgL_idxz00_3571 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3570);
									BgL_test2078z00_4424 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3571 + 2L)) == BgL_classz00_3569);
								}
							else
								{	/* Reduce/same.scm 91 */
									bool_t BgL_res1927z00_3574;

									{	/* Reduce/same.scm 91 */
										obj_t BgL_oclassz00_3575;

										{	/* Reduce/same.scm 91 */
											obj_t BgL_arg1815z00_3576;
											long BgL_arg1816z00_3577;

											BgL_arg1815z00_3576 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Reduce/same.scm 91 */
												long BgL_arg1817z00_3578;

												BgL_arg1817z00_3578 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3570);
												BgL_arg1816z00_3577 =
													(BgL_arg1817z00_3578 - OBJECT_TYPE);
											}
											BgL_oclassz00_3575 =
												VECTOR_REF(BgL_arg1815z00_3576, BgL_arg1816z00_3577);
										}
										{	/* Reduce/same.scm 91 */
											bool_t BgL__ortest_1115z00_3579;

											BgL__ortest_1115z00_3579 =
												(BgL_classz00_3569 == BgL_oclassz00_3575);
											if (BgL__ortest_1115z00_3579)
												{	/* Reduce/same.scm 91 */
													BgL_res1927z00_3574 = BgL__ortest_1115z00_3579;
												}
											else
												{	/* Reduce/same.scm 91 */
													long BgL_odepthz00_3580;

													{	/* Reduce/same.scm 91 */
														obj_t BgL_arg1804z00_3581;

														BgL_arg1804z00_3581 = (BgL_oclassz00_3575);
														BgL_odepthz00_3580 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3581);
													}
													if ((2L < BgL_odepthz00_3580))
														{	/* Reduce/same.scm 91 */
															obj_t BgL_arg1802z00_3582;

															{	/* Reduce/same.scm 91 */
																obj_t BgL_arg1803z00_3583;

																BgL_arg1803z00_3583 = (BgL_oclassz00_3575);
																BgL_arg1802z00_3582 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3583,
																	2L);
															}
															BgL_res1927z00_3574 =
																(BgL_arg1802z00_3582 == BgL_classz00_3569);
														}
													else
														{	/* Reduce/same.scm 91 */
															BgL_res1927z00_3574 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2078z00_4424 = BgL_res1927z00_3574;
								}
						}
					}
					if (BgL_test2078z00_4424)
						{	/* Reduce/same.scm 91 */
							if (
								(((obj_t)
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_appzd2lyzd2_bglt) BgL_nodez00_3281))))->
											BgL_typez00)) ==
									((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_node2z00_3282)))->BgL_typez00))))
								{	/* Reduce/same.scm 93 */
									bool_t BgL_test2083z00_4457;

									{	/* Reduce/same.scm 93 */
										BgL_nodez00_bglt BgL_arg1595z00_3584;
										BgL_nodez00_bglt BgL_arg1602z00_3585;

										BgL_arg1595z00_3584 =
											(((BgL_appzd2lyzd2_bglt) COBJECT(
													((BgL_appzd2lyzd2_bglt) BgL_nodez00_3281)))->
											BgL_funz00);
										BgL_arg1602z00_3585 =
											(((BgL_appzd2lyzd2_bglt)
												COBJECT(((BgL_appzd2lyzd2_bglt) ((BgL_nodez00_bglt)
															BgL_node2z00_3282))))->BgL_funz00);
										BgL_test2083z00_4457 =
											BGl_samezd2nodezf3z21zzreduce_samez00(BgL_arg1595z00_3584,
											BgL_arg1602z00_3585, BgL_aliasz00_3283);
									}
									if (BgL_test2083z00_4457)
										{	/* Reduce/same.scm 94 */
											BgL_nodez00_bglt BgL_arg1593z00_3586;
											BgL_nodez00_bglt BgL_arg1594z00_3587;

											BgL_arg1593z00_3586 =
												(((BgL_appzd2lyzd2_bglt) COBJECT(
														((BgL_appzd2lyzd2_bglt) BgL_nodez00_3281)))->
												BgL_argz00);
											BgL_arg1594z00_3587 =
												(((BgL_appzd2lyzd2_bglt)
													COBJECT(((BgL_appzd2lyzd2_bglt) ((BgL_nodez00_bglt)
																BgL_node2z00_3282))))->BgL_argz00);
											BgL_tmpz00_4423 =
												BGl_samezd2nodezf3z21zzreduce_samez00
												(BgL_arg1593z00_3586, BgL_arg1594z00_3587,
												BgL_aliasz00_3283);
										}
									else
										{	/* Reduce/same.scm 93 */
											BgL_tmpz00_4423 = ((bool_t) 0);
										}
								}
							else
								{	/* Reduce/same.scm 92 */
									BgL_tmpz00_4423 = ((bool_t) 0);
								}
						}
					else
						{	/* Reduce/same.scm 91 */
							BgL_tmpz00_4423 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_4423);
			}
		}

	}



/* &same-node?-sync1292 */
	obj_t BGl_z62samezd2nodezf3zd2sync1292z91zzreduce_samez00(obj_t
		BgL_envz00_3284, obj_t BgL_nodez00_3285, obj_t BgL_node2z00_3286,
		obj_t BgL_aliasz00_3287)
	{
		{	/* Reduce/same.scm 80 */
			{	/* Reduce/same.scm 81 */
				bool_t BgL_tmpz00_4471;

				{	/* Reduce/same.scm 81 */
					bool_t BgL_test2084z00_4472;

					{	/* Reduce/same.scm 81 */
						obj_t BgL_classz00_3590;

						BgL_classz00_3590 = BGl_syncz00zzast_nodez00;
						{	/* Reduce/same.scm 81 */
							BgL_objectz00_bglt BgL_arg1807z00_3591;

							{	/* Reduce/same.scm 81 */
								obj_t BgL_tmpz00_4473;

								BgL_tmpz00_4473 =
									((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3286));
								BgL_arg1807z00_3591 = (BgL_objectz00_bglt) (BgL_tmpz00_4473);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Reduce/same.scm 81 */
									long BgL_idxz00_3592;

									BgL_idxz00_3592 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3591);
									BgL_test2084z00_4472 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3592 + 2L)) == BgL_classz00_3590);
								}
							else
								{	/* Reduce/same.scm 81 */
									bool_t BgL_res1926z00_3595;

									{	/* Reduce/same.scm 81 */
										obj_t BgL_oclassz00_3596;

										{	/* Reduce/same.scm 81 */
											obj_t BgL_arg1815z00_3597;
											long BgL_arg1816z00_3598;

											BgL_arg1815z00_3597 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Reduce/same.scm 81 */
												long BgL_arg1817z00_3599;

												BgL_arg1817z00_3599 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3591);
												BgL_arg1816z00_3598 =
													(BgL_arg1817z00_3599 - OBJECT_TYPE);
											}
											BgL_oclassz00_3596 =
												VECTOR_REF(BgL_arg1815z00_3597, BgL_arg1816z00_3598);
										}
										{	/* Reduce/same.scm 81 */
											bool_t BgL__ortest_1115z00_3600;

											BgL__ortest_1115z00_3600 =
												(BgL_classz00_3590 == BgL_oclassz00_3596);
											if (BgL__ortest_1115z00_3600)
												{	/* Reduce/same.scm 81 */
													BgL_res1926z00_3595 = BgL__ortest_1115z00_3600;
												}
											else
												{	/* Reduce/same.scm 81 */
													long BgL_odepthz00_3601;

													{	/* Reduce/same.scm 81 */
														obj_t BgL_arg1804z00_3602;

														BgL_arg1804z00_3602 = (BgL_oclassz00_3596);
														BgL_odepthz00_3601 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3602);
													}
													if ((2L < BgL_odepthz00_3601))
														{	/* Reduce/same.scm 81 */
															obj_t BgL_arg1802z00_3603;

															{	/* Reduce/same.scm 81 */
																obj_t BgL_arg1803z00_3604;

																BgL_arg1803z00_3604 = (BgL_oclassz00_3596);
																BgL_arg1802z00_3603 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3604,
																	2L);
															}
															BgL_res1926z00_3595 =
																(BgL_arg1802z00_3603 == BgL_classz00_3590);
														}
													else
														{	/* Reduce/same.scm 81 */
															BgL_res1926z00_3595 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2084z00_4472 = BgL_res1926z00_3595;
								}
						}
					}
					if (BgL_test2084z00_4472)
						{	/* Reduce/same.scm 81 */
							if (
								(((obj_t)
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_syncz00_bglt) BgL_nodez00_3285))))->
											BgL_typez00)) ==
									((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_node2z00_3286)))->BgL_typez00))))
								{	/* Reduce/same.scm 83 */
									bool_t BgL_test2089z00_4505;

									{	/* Reduce/same.scm 83 */
										BgL_nodez00_bglt BgL_arg1584z00_3605;
										BgL_nodez00_bglt BgL_arg1585z00_3606;

										BgL_arg1584z00_3605 =
											(((BgL_syncz00_bglt) COBJECT(
													((BgL_syncz00_bglt) BgL_nodez00_3285)))->
											BgL_mutexz00);
										BgL_arg1585z00_3606 =
											(((BgL_syncz00_bglt)
												COBJECT(((BgL_syncz00_bglt) ((BgL_nodez00_bglt)
															BgL_node2z00_3286))))->BgL_mutexz00);
										BgL_test2089z00_4505 =
											BGl_samezd2nodezf3z21zzreduce_samez00(BgL_arg1584z00_3605,
											BgL_arg1585z00_3606, BgL_aliasz00_3287);
									}
									if (BgL_test2089z00_4505)
										{	/* Reduce/same.scm 84 */
											bool_t BgL_test2090z00_4512;

											{	/* Reduce/same.scm 84 */
												BgL_nodez00_bglt BgL_arg1575z00_3607;
												BgL_nodez00_bglt BgL_arg1576z00_3608;

												BgL_arg1575z00_3607 =
													(((BgL_syncz00_bglt) COBJECT(
															((BgL_syncz00_bglt) BgL_nodez00_3285)))->
													BgL_prelockz00);
												BgL_arg1576z00_3608 =
													(((BgL_syncz00_bglt)
														COBJECT(((BgL_syncz00_bglt) ((BgL_nodez00_bglt)
																	BgL_node2z00_3286))))->BgL_prelockz00);
												BgL_test2090z00_4512 =
													BGl_samezd2nodezf3z21zzreduce_samez00
													(BgL_arg1575z00_3607, BgL_arg1576z00_3608,
													BgL_aliasz00_3287);
											}
											if (BgL_test2090z00_4512)
												{	/* Reduce/same.scm 85 */
													BgL_nodez00_bglt BgL_arg1571z00_3609;
													BgL_nodez00_bglt BgL_arg1573z00_3610;

													BgL_arg1571z00_3609 =
														(((BgL_syncz00_bglt) COBJECT(
																((BgL_syncz00_bglt) BgL_nodez00_3285)))->
														BgL_bodyz00);
													BgL_arg1573z00_3610 =
														(((BgL_syncz00_bglt)
															COBJECT(((BgL_syncz00_bglt) ((BgL_nodez00_bglt)
																		BgL_node2z00_3286))))->BgL_bodyz00);
													BgL_tmpz00_4471 =
														BGl_samezd2nodeza2zf3z83zzreduce_samez00(((obj_t)
															BgL_arg1571z00_3609),
														((obj_t) BgL_arg1573z00_3610), BgL_aliasz00_3287);
												}
											else
												{	/* Reduce/same.scm 84 */
													BgL_tmpz00_4471 = ((bool_t) 0);
												}
										}
									else
										{	/* Reduce/same.scm 83 */
											BgL_tmpz00_4471 = ((bool_t) 0);
										}
								}
							else
								{	/* Reduce/same.scm 82 */
									BgL_tmpz00_4471 = ((bool_t) 0);
								}
						}
					else
						{	/* Reduce/same.scm 81 */
							BgL_tmpz00_4471 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_4471);
			}
		}

	}



/* &same-node?-sequence1290 */
	obj_t BGl_z62samezd2nodezf3zd2sequence1290z91zzreduce_samez00(obj_t
		BgL_envz00_3288, obj_t BgL_nodez00_3289, obj_t BgL_node2z00_3290,
		obj_t BgL_aliasz00_3291)
	{
		{	/* Reduce/same.scm 72 */
			{	/* Reduce/same.scm 73 */
				bool_t BgL_tmpz00_4528;

				{	/* Reduce/same.scm 73 */
					bool_t BgL_test2091z00_4529;

					{	/* Reduce/same.scm 73 */
						obj_t BgL_classz00_3613;

						BgL_classz00_3613 = BGl_sequencez00zzast_nodez00;
						{	/* Reduce/same.scm 73 */
							BgL_objectz00_bglt BgL_arg1807z00_3614;

							{	/* Reduce/same.scm 73 */
								obj_t BgL_tmpz00_4530;

								BgL_tmpz00_4530 =
									((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3290));
								BgL_arg1807z00_3614 = (BgL_objectz00_bglt) (BgL_tmpz00_4530);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Reduce/same.scm 73 */
									long BgL_idxz00_3615;

									BgL_idxz00_3615 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3614);
									BgL_test2091z00_4529 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3615 + 3L)) == BgL_classz00_3613);
								}
							else
								{	/* Reduce/same.scm 73 */
									bool_t BgL_res1925z00_3618;

									{	/* Reduce/same.scm 73 */
										obj_t BgL_oclassz00_3619;

										{	/* Reduce/same.scm 73 */
											obj_t BgL_arg1815z00_3620;
											long BgL_arg1816z00_3621;

											BgL_arg1815z00_3620 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Reduce/same.scm 73 */
												long BgL_arg1817z00_3622;

												BgL_arg1817z00_3622 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3614);
												BgL_arg1816z00_3621 =
													(BgL_arg1817z00_3622 - OBJECT_TYPE);
											}
											BgL_oclassz00_3619 =
												VECTOR_REF(BgL_arg1815z00_3620, BgL_arg1816z00_3621);
										}
										{	/* Reduce/same.scm 73 */
											bool_t BgL__ortest_1115z00_3623;

											BgL__ortest_1115z00_3623 =
												(BgL_classz00_3613 == BgL_oclassz00_3619);
											if (BgL__ortest_1115z00_3623)
												{	/* Reduce/same.scm 73 */
													BgL_res1925z00_3618 = BgL__ortest_1115z00_3623;
												}
											else
												{	/* Reduce/same.scm 73 */
													long BgL_odepthz00_3624;

													{	/* Reduce/same.scm 73 */
														obj_t BgL_arg1804z00_3625;

														BgL_arg1804z00_3625 = (BgL_oclassz00_3619);
														BgL_odepthz00_3624 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3625);
													}
													if ((3L < BgL_odepthz00_3624))
														{	/* Reduce/same.scm 73 */
															obj_t BgL_arg1802z00_3626;

															{	/* Reduce/same.scm 73 */
																obj_t BgL_arg1803z00_3627;

																BgL_arg1803z00_3627 = (BgL_oclassz00_3619);
																BgL_arg1802z00_3626 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3627,
																	3L);
															}
															BgL_res1925z00_3618 =
																(BgL_arg1802z00_3626 == BgL_classz00_3613);
														}
													else
														{	/* Reduce/same.scm 73 */
															BgL_res1925z00_3618 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2091z00_4529 = BgL_res1925z00_3618;
								}
						}
					}
					if (BgL_test2091z00_4529)
						{	/* Reduce/same.scm 73 */
							if (
								(((obj_t)
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_sequencez00_bglt) BgL_nodez00_3289))))->
											BgL_typez00)) ==
									((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_node2z00_3290)))->BgL_typez00))))
								{	/* Reduce/same.scm 75 */
									obj_t BgL_arg1559z00_3628;
									obj_t BgL_arg1561z00_3629;

									BgL_arg1559z00_3628 =
										(((BgL_sequencez00_bglt) COBJECT(
												((BgL_sequencez00_bglt) BgL_nodez00_3289)))->
										BgL_nodesz00);
									BgL_arg1561z00_3629 =
										(((BgL_sequencez00_bglt)
											COBJECT(((BgL_sequencez00_bglt) ((BgL_nodez00_bglt)
														BgL_node2z00_3290))))->BgL_nodesz00);
									BgL_tmpz00_4528 =
										BGl_samezd2nodeza2zf3z83zzreduce_samez00
										(BgL_arg1559z00_3628, BgL_arg1561z00_3629,
										BgL_aliasz00_3291);
								}
							else
								{	/* Reduce/same.scm 74 */
									BgL_tmpz00_4528 = ((bool_t) 0);
								}
						}
					else
						{	/* Reduce/same.scm 73 */
							BgL_tmpz00_4528 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_4528);
			}
		}

	}



/* &same-node?-var1288 */
	obj_t BGl_z62samezd2nodezf3zd2var1288z91zzreduce_samez00(obj_t
		BgL_envz00_3292, obj_t BgL_nodez00_3293, obj_t BgL_node2z00_3294,
		obj_t BgL_aliasz00_3295)
	{
		{	/* Reduce/same.scm 59 */
			{	/* Tools/trace.sch 53 */
				bool_t BgL_tmpz00_4569;

				{	/* Tools/trace.sch 53 */
					bool_t BgL_test2096z00_4570;

					{	/* Tools/trace.sch 53 */
						obj_t BgL_classz00_3632;

						BgL_classz00_3632 = BGl_varz00zzast_nodez00;
						{	/* Tools/trace.sch 53 */
							BgL_objectz00_bglt BgL_arg1807z00_3633;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_tmpz00_4571;

								BgL_tmpz00_4571 =
									((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3294));
								BgL_arg1807z00_3633 = (BgL_objectz00_bglt) (BgL_tmpz00_4571);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Tools/trace.sch 53 */
									long BgL_idxz00_3634;

									BgL_idxz00_3634 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3633);
									BgL_test2096z00_4570 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3634 + 2L)) == BgL_classz00_3632);
								}
							else
								{	/* Tools/trace.sch 53 */
									bool_t BgL_res1924z00_3637;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_oclassz00_3638;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1815z00_3639;
											long BgL_arg1816z00_3640;

											BgL_arg1815z00_3639 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Tools/trace.sch 53 */
												long BgL_arg1817z00_3641;

												BgL_arg1817z00_3641 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3633);
												BgL_arg1816z00_3640 =
													(BgL_arg1817z00_3641 - OBJECT_TYPE);
											}
											BgL_oclassz00_3638 =
												VECTOR_REF(BgL_arg1815z00_3639, BgL_arg1816z00_3640);
										}
										{	/* Tools/trace.sch 53 */
											bool_t BgL__ortest_1115z00_3642;

											BgL__ortest_1115z00_3642 =
												(BgL_classz00_3632 == BgL_oclassz00_3638);
											if (BgL__ortest_1115z00_3642)
												{	/* Tools/trace.sch 53 */
													BgL_res1924z00_3637 = BgL__ortest_1115z00_3642;
												}
											else
												{	/* Tools/trace.sch 53 */
													long BgL_odepthz00_3643;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg1804z00_3644;

														BgL_arg1804z00_3644 = (BgL_oclassz00_3638);
														BgL_odepthz00_3643 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3644);
													}
													if ((2L < BgL_odepthz00_3643))
														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg1802z00_3645;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg1803z00_3646;

																BgL_arg1803z00_3646 = (BgL_oclassz00_3638);
																BgL_arg1802z00_3645 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3646,
																	2L);
															}
															BgL_res1924z00_3637 =
																(BgL_arg1802z00_3645 == BgL_classz00_3632);
														}
													else
														{	/* Tools/trace.sch 53 */
															BgL_res1924z00_3637 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2096z00_4570 = BgL_res1924z00_3637;
								}
						}
					}
					if (BgL_test2096z00_4570)
						{	/* Tools/trace.sch 53 */
							if (
								((((BgL_variablez00_bglt) COBJECT(
												(((BgL_varz00_bglt) COBJECT(
															((BgL_varz00_bglt) BgL_nodez00_3293)))->
													BgL_variablez00)))->BgL_accessz00) ==
									CNST_TABLE_REF(0)))
								{	/* Tools/trace.sch 53 */
									if (
										(((obj_t)
												(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																((BgL_varz00_bglt) BgL_nodez00_3293))))->
													BgL_typez00)) ==
											((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_node2z00_3294)))->BgL_typez00))))
										{	/* Tools/trace.sch 53 */
											bool_t BgL__ortest_1118z00_3647;

											BgL__ortest_1118z00_3647 =
												(
												((obj_t)
													(((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_nodez00_3293)))->
														BgL_variablez00)) ==
												((obj_t) (((BgL_varz00_bglt)
															COBJECT(((BgL_varz00_bglt) ((BgL_nodez00_bglt)
																		BgL_node2z00_3294))))->BgL_variablez00)));
											if (BgL__ortest_1118z00_3647)
												{	/* Tools/trace.sch 53 */
													BgL_tmpz00_4569 = BgL__ortest_1118z00_3647;
												}
											else
												{	/* Tools/trace.sch 53 */
													BgL_tmpz00_4569 =
														BGl_aliasedzf3zf3zzreduce_samez00(
														(((BgL_varz00_bglt) COBJECT(
																	((BgL_varz00_bglt) BgL_nodez00_3293)))->
															BgL_variablez00),
														(((BgL_varz00_bglt)
																COBJECT(((BgL_varz00_bglt) ((BgL_nodez00_bglt)
																			BgL_node2z00_3294))))->BgL_variablez00),
														BgL_aliasz00_3295);
												}
										}
									else
										{	/* Tools/trace.sch 53 */
											BgL_tmpz00_4569 = ((bool_t) 0);
										}
								}
							else
								{	/* Tools/trace.sch 53 */
									BgL_tmpz00_4569 = ((bool_t) 0);
								}
						}
					else
						{	/* Tools/trace.sch 53 */
							BgL_tmpz00_4569 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_4569);
			}
		}

	}



/* &same-node?-kwote1286 */
	obj_t BGl_z62samezd2nodezf3zd2kwote1286z91zzreduce_samez00(obj_t
		BgL_envz00_3296, obj_t BgL_nodez00_3297, obj_t BgL_node2z00_3298,
		obj_t BgL_aliasz00_3299)
	{
		{	/* Reduce/same.scm 48 */
			{	/* Tools/trace.sch 53 */
				bool_t BgL_tmpz00_4625;

				{	/* Tools/trace.sch 53 */
					bool_t BgL_test2103z00_4626;

					{	/* Tools/trace.sch 53 */
						obj_t BgL_classz00_3650;

						BgL_classz00_3650 = BGl_kwotez00zzast_nodez00;
						{	/* Tools/trace.sch 53 */
							BgL_objectz00_bglt BgL_arg1807z00_3651;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_tmpz00_4627;

								BgL_tmpz00_4627 =
									((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3298));
								BgL_arg1807z00_3651 = (BgL_objectz00_bglt) (BgL_tmpz00_4627);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Tools/trace.sch 53 */
									long BgL_idxz00_3652;

									BgL_idxz00_3652 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3651);
									BgL_test2103z00_4626 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3652 + 2L)) == BgL_classz00_3650);
								}
							else
								{	/* Tools/trace.sch 53 */
									bool_t BgL_res1923z00_3655;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_oclassz00_3656;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1815z00_3657;
											long BgL_arg1816z00_3658;

											BgL_arg1815z00_3657 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Tools/trace.sch 53 */
												long BgL_arg1817z00_3659;

												BgL_arg1817z00_3659 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3651);
												BgL_arg1816z00_3658 =
													(BgL_arg1817z00_3659 - OBJECT_TYPE);
											}
											BgL_oclassz00_3656 =
												VECTOR_REF(BgL_arg1815z00_3657, BgL_arg1816z00_3658);
										}
										{	/* Tools/trace.sch 53 */
											bool_t BgL__ortest_1115z00_3660;

											BgL__ortest_1115z00_3660 =
												(BgL_classz00_3650 == BgL_oclassz00_3656);
											if (BgL__ortest_1115z00_3660)
												{	/* Tools/trace.sch 53 */
													BgL_res1923z00_3655 = BgL__ortest_1115z00_3660;
												}
											else
												{	/* Tools/trace.sch 53 */
													long BgL_odepthz00_3661;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg1804z00_3662;

														BgL_arg1804z00_3662 = (BgL_oclassz00_3656);
														BgL_odepthz00_3661 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3662);
													}
													if ((2L < BgL_odepthz00_3661))
														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg1802z00_3663;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg1803z00_3664;

																BgL_arg1803z00_3664 = (BgL_oclassz00_3656);
																BgL_arg1802z00_3663 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3664,
																	2L);
															}
															BgL_res1923z00_3655 =
																(BgL_arg1802z00_3663 == BgL_classz00_3650);
														}
													else
														{	/* Tools/trace.sch 53 */
															BgL_res1923z00_3655 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2103z00_4626 = BgL_res1923z00_3655;
								}
						}
					}
					if (BgL_test2103z00_4626)
						{	/* Tools/trace.sch 53 */
							if (
								(((obj_t)
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_kwotez00_bglt) BgL_nodez00_3297))))->
											BgL_typez00)) ==
									((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_node2z00_3298)))->BgL_typez00))))
								{	/* Tools/trace.sch 53 */
									BgL_tmpz00_4625 =
										BGl_equalzf3zf3zz__r4_equivalence_6_2z00(
										(((BgL_kwotez00_bglt) COBJECT(
													((BgL_kwotez00_bglt) BgL_nodez00_3297)))->
											BgL_valuez00),
										(((BgL_kwotez00_bglt)
												COBJECT(((BgL_kwotez00_bglt) ((BgL_nodez00_bglt)
															BgL_node2z00_3298))))->BgL_valuez00));
								}
							else
								{	/* Tools/trace.sch 53 */
									BgL_tmpz00_4625 = ((bool_t) 0);
								}
						}
					else
						{	/* Tools/trace.sch 53 */
							BgL_tmpz00_4625 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_4625);
			}
		}

	}



/* &same-node?-atom1284 */
	obj_t BGl_z62samezd2nodezf3zd2atom1284z91zzreduce_samez00(obj_t
		BgL_envz00_3300, obj_t BgL_nodez00_3301, obj_t BgL_node2z00_3302,
		obj_t BgL_aliasz00_3303)
	{
		{	/* Reduce/same.scm 32 */
			{	/* Tools/trace.sch 53 */
				bool_t BgL_tmpz00_4666;

				{	/* Tools/trace.sch 53 */
					bool_t BgL_test2108z00_4667;

					{	/* Tools/trace.sch 53 */
						obj_t BgL_classz00_3667;

						BgL_classz00_3667 = BGl_atomz00zzast_nodez00;
						{	/* Tools/trace.sch 53 */
							BgL_objectz00_bglt BgL_arg1807z00_3668;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_tmpz00_4668;

								BgL_tmpz00_4668 =
									((obj_t) ((BgL_nodez00_bglt) BgL_node2z00_3302));
								BgL_arg1807z00_3668 = (BgL_objectz00_bglt) (BgL_tmpz00_4668);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Tools/trace.sch 53 */
									long BgL_idxz00_3669;

									BgL_idxz00_3669 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3668);
									BgL_test2108z00_4667 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3669 + 2L)) == BgL_classz00_3667);
								}
							else
								{	/* Tools/trace.sch 53 */
									bool_t BgL_res1922z00_3672;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_oclassz00_3673;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1815z00_3674;
											long BgL_arg1816z00_3675;

											BgL_arg1815z00_3674 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Tools/trace.sch 53 */
												long BgL_arg1817z00_3676;

												BgL_arg1817z00_3676 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3668);
												BgL_arg1816z00_3675 =
													(BgL_arg1817z00_3676 - OBJECT_TYPE);
											}
											BgL_oclassz00_3673 =
												VECTOR_REF(BgL_arg1815z00_3674, BgL_arg1816z00_3675);
										}
										{	/* Tools/trace.sch 53 */
											bool_t BgL__ortest_1115z00_3677;

											BgL__ortest_1115z00_3677 =
												(BgL_classz00_3667 == BgL_oclassz00_3673);
											if (BgL__ortest_1115z00_3677)
												{	/* Tools/trace.sch 53 */
													BgL_res1922z00_3672 = BgL__ortest_1115z00_3677;
												}
											else
												{	/* Tools/trace.sch 53 */
													long BgL_odepthz00_3678;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg1804z00_3679;

														BgL_arg1804z00_3679 = (BgL_oclassz00_3673);
														BgL_odepthz00_3678 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3679);
													}
													if ((2L < BgL_odepthz00_3678))
														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg1802z00_3680;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg1803z00_3681;

																BgL_arg1803z00_3681 = (BgL_oclassz00_3673);
																BgL_arg1802z00_3680 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3681,
																	2L);
															}
															BgL_res1922z00_3672 =
																(BgL_arg1802z00_3680 == BgL_classz00_3667);
														}
													else
														{	/* Tools/trace.sch 53 */
															BgL_res1922z00_3672 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2108z00_4667 = BgL_res1922z00_3672;
								}
						}
					}
					if (BgL_test2108z00_4667)
						{	/* Tools/trace.sch 53 */
							if (
								(((obj_t)
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_atomz00_bglt) BgL_nodez00_3301))))->
											BgL_typez00)) ==
									((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_node2z00_3302)))->BgL_typez00))))
								{	/* Tools/trace.sch 53 */
									bool_t BgL_test2113z00_4700;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_tmpz00_4701;

										BgL_tmpz00_4701 =
											(((BgL_atomz00_bglt) COBJECT(
													((BgL_atomz00_bglt) BgL_nodez00_3301)))->
											BgL_valuez00);
										BgL_test2113z00_4700 = REALP(BgL_tmpz00_4701);
									}
									if (BgL_test2113z00_4700)
										{	/* Tools/trace.sch 53 */
											bool_t BgL_test2114z00_4705;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_tmpz00_4706;

												BgL_tmpz00_4706 =
													(((BgL_atomz00_bglt) COBJECT(
															((BgL_atomz00_bglt)
																((BgL_nodez00_bglt) BgL_node2z00_3302))))->
													BgL_valuez00);
												BgL_test2114z00_4705 = REALP(BgL_tmpz00_4706);
											}
											if (BgL_test2114z00_4705)
												{	/* Tools/trace.sch 53 */
													if (
														(REAL_TO_DOUBLE(
																(((BgL_atomz00_bglt) COBJECT(
																			((BgL_atomz00_bglt) BgL_nodez00_3301)))->
																	BgL_valuez00)) ==
															REAL_TO_DOUBLE((((BgL_atomz00_bglt)
																		COBJECT(((BgL_atomz00_bglt) (
																					(BgL_nodez00_bglt)
																					BgL_node2z00_3302))))->
																	BgL_valuez00))))
														{	/* Tools/trace.sch 53 */
															BgL_tmpz00_4666 =
																(BGL_SIGNBIT(REAL_TO_DOUBLE(
																		(((BgL_atomz00_bglt) COBJECT(
																					((BgL_atomz00_bglt)
																						BgL_nodez00_3301)))->
																			BgL_valuez00))) ==
																BGL_SIGNBIT(REAL_TO_DOUBLE((((BgL_atomz00_bglt)
																				COBJECT(((BgL_atomz00_bglt) (
																							(BgL_nodez00_bglt)
																							BgL_node2z00_3302))))->
																			BgL_valuez00))));
														}
													else
														{	/* Tools/trace.sch 53 */
															BgL_tmpz00_4666 = ((bool_t) 0);
														}
												}
											else
												{	/* Tools/trace.sch 53 */
													BgL_tmpz00_4666 = ((bool_t) 0);
												}
										}
									else
										{	/* Tools/trace.sch 53 */
											BgL_tmpz00_4666 =
												BGl_equalzf3zf3zz__r4_equivalence_6_2z00(
												(((BgL_atomz00_bglt) COBJECT(
															((BgL_atomz00_bglt) BgL_nodez00_3301)))->
													BgL_valuez00),
												(((BgL_atomz00_bglt)
														COBJECT(((BgL_atomz00_bglt) ((BgL_nodez00_bglt)
																	BgL_node2z00_3302))))->BgL_valuez00));
										}
								}
							else
								{	/* Tools/trace.sch 53 */
									BgL_tmpz00_4666 = ((bool_t) 0);
								}
						}
					else
						{	/* Tools/trace.sch 53 */
							BgL_tmpz00_4666 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_4666);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzreduce_samez00(void)
	{
		{	/* Reduce/same.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1974z00zzreduce_samez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1974z00zzreduce_samez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1974z00zzreduce_samez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1974z00zzreduce_samez00));
			return
				BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1974z00zzreduce_samez00));
		}

	}

#ifdef __cplusplus
}
#endif
