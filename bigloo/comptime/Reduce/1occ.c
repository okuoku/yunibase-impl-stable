/*===========================================================================*/
/*   (Reduce/1occ.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Reduce/1occ.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_REDUCE_1OCC_TYPE_DEFINITIONS
#define BGL_REDUCE_1OCC_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_privatez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                 *BgL_privatez00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_REDUCE_1OCC_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_nodezd21occz12zc0zzreduce_1occz00(BgL_nodez00_bglt, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_returnz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzreduce_1occz00 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	static obj_t BGl_z62nodezd21occz12za2zzreduce_1occz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62nodezd21occz12zd2cast1344z70zzreduce_1occz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t
		BGl_z62nodezd21occz12zd2boxzd2setz121368zb0zzreduce_1occz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzreduce_1occz00(void);
	static obj_t BGl_z62nodezd21occz12zd2makezd2box1366za2zzreduce_1occz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd21occz12zd2setq1346z70zzreduce_1occz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_z62reducezd21occz12za2zzreduce_1occz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd21occz12zd2sync1334z70zzreduce_1occz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzreduce_1occz00(void);
	static obj_t BGl_z62nodezd21occz12zd2fail1350z70zzreduce_1occz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzreduce_1occz00(void);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	static obj_t BGl_z62nodezd21occz121321za2zzreduce_1occz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_z62nodezd21occz12zd2funcall1338z70zzreduce_1occz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd21occz12zd2appzd2ly1336za2zzreduce_1occz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_nodezd21occza2z12z62zzreduce_1occz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd21occz12zd2letzd2var1356za2zzreduce_1occz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd21occz12zd2condition1348z70zzreduce_1occz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzreduce_1occz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzreduce_1occz00(void);
	static obj_t BGl_z62nodezd21occz12zd2sequence1332z70zzreduce_1occz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd21occz12zd2closure1330z70zzreduce_1occz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62nodezd21occz12zd2return1364z70zzreduce_1occz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd21occz12zd2extern1340z70zzreduce_1occz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd21occz12zd2boxzd2ref1371za2zzreduce_1occz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t BGl_z62nodezd21occz12zd2switch1352z70zzreduce_1occz00(obj_t,
		obj_t, obj_t);
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t
		BGl_nodezd21occzd2letzd2varz12zc0zzreduce_1occz00(BgL_letzd2varzd2_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern bool_t BGl_typezd2lesszd2specificzf3zf3zztype_miscz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzreduce_1occz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_occurz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_z62nodezd21occz12zd2var1328z70zzreduce_1occz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62nodezd21occz12zd2jumpzd2exzd2i1360z70zzreduce_1occz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t
		BGl_z62nodezd21occz12zd2setzd2exzd2it1358z70zzreduce_1occz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	static obj_t BGl_z62nodezd21occz12zd2atom1324z70zzreduce_1occz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_kwotez00zzast_nodez00;
	static obj_t BGl_z62nodezd21occz12zd2retblock1362z70zzreduce_1occz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	extern obj_t BGl_occurzd2varzd2zzast_occurz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzreduce_1occz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzreduce_1occz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzreduce_1occz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzreduce_1occz00(void);
	extern bool_t BGl_sidezd2effectzf3z21zzeffect_effectz00(BgL_nodez00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_reducezd21occz12zc0zzreduce_1occz00(obj_t);
	extern obj_t BGl_retblockz00zzast_nodez00;
	static obj_t BGl_z62nodezd21occz12zd2kwote1326z70zzreduce_1occz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_privatez00zzast_nodez00;
	static long BGl_za2variablezd2removedza2zd2zzreduce_1occz00 = 0L;
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd21occz12zd2private1342z70zzreduce_1occz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62nodezd21occz12zd2app1374z70zzreduce_1occz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nodezd21occz12zd2letzd2fun1354za2zzreduce_1occz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_reducezd21occz12zd2envz12zzreduce_1occz00,
		BgL_bgl_za762reduceza7d21occ1946z00,
		BGl_z62reducezd21occz12za2zzreduce_1occz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711947za7,
		BGl_z62nodezd21occz12za2zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1912z00zzreduce_1occz00,
		BgL_bgl_string1912za700za7za7r1948za7, "      single occurrence", 23);
	      DEFINE_STRING(BGl_string1913z00zzreduce_1occz00,
		BgL_bgl_string1913za700za7za7r1949za7, "      (removed: ", 16);
	      DEFINE_STRING(BGl_string1915z00zzreduce_1occz00,
		BgL_bgl_string1915za700za7za7r1950za7, "node-1occ!1321", 14);
	      DEFINE_STRING(BGl_string1916z00zzreduce_1occz00,
		BgL_bgl_string1916za700za7za7r1951za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1918z00zzreduce_1occz00,
		BgL_bgl_string1918za700za7za7r1952za7, "node-1occ!", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1914z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711953za7,
		BGl_z62nodezd21occz121321za2zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1917z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711954za7,
		BGl_z62nodezd21occz12zd2atom1324z70zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1919z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711955za7,
		BGl_z62nodezd21occz12zd2kwote1326z70zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1920z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711956za7,
		BGl_z62nodezd21occz12zd2var1328z70zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1921z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711957za7,
		BGl_z62nodezd21occz12zd2closure1330z70zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1922z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711958za7,
		BGl_z62nodezd21occz12zd2sequence1332z70zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1923z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711959za7,
		BGl_z62nodezd21occz12zd2sync1334z70zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1924z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711960za7,
		BGl_z62nodezd21occz12zd2appzd2ly1336za2zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1925z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711961za7,
		BGl_z62nodezd21occz12zd2funcall1338z70zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1926z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711962za7,
		BGl_z62nodezd21occz12zd2extern1340z70zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1927z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711963za7,
		BGl_z62nodezd21occz12zd2private1342z70zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1928z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711964za7,
		BGl_z62nodezd21occz12zd2cast1344z70zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1929z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711965za7,
		BGl_z62nodezd21occz12zd2setq1346z70zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1930z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711966za7,
		BGl_z62nodezd21occz12zd2condition1348z70zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1931z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711967za7,
		BGl_z62nodezd21occz12zd2fail1350z70zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1932z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711968za7,
		BGl_z62nodezd21occz12zd2switch1352z70zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1933z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711969za7,
		BGl_z62nodezd21occz12zd2letzd2fun1354za2zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1934z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711970za7,
		BGl_z62nodezd21occz12zd2letzd2var1356za2zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1935z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711971za7,
		BGl_z62nodezd21occz12zd2setzd2exzd2it1358z70zzreduce_1occz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1936z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711972za7,
		BGl_z62nodezd21occz12zd2jumpzd2exzd2i1360z70zzreduce_1occz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string1943z00zzreduce_1occz00,
		BgL_bgl_string1943za700za7za7r1973za7, "reduce_1occ", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1937z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711974za7,
		BGl_z62nodezd21occz12zd2retblock1362z70zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1944z00zzreduce_1occz00,
		BgL_bgl_string1944za700za7za7r1975za7, "never node-1occ!1321 read ", 26);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1938z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711976za7,
		BGl_z62nodezd21occz12zd2return1364z70zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1939z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711977za7,
		BGl_z62nodezd21occz12zd2makezd2box1366za2zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1940z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711978za7,
		BGl_z62nodezd21occz12zd2boxzd2setz121368zb0zzreduce_1occz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1941z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711979za7,
		BGl_z62nodezd21occz12zd2boxzd2ref1371za2zzreduce_1occz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1942z00zzreduce_1occz00,
		BgL_bgl_za762nodeza7d21occza711980za7,
		BGl_z62nodezd21occz12zd2app1374z70zzreduce_1occz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzreduce_1occz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzreduce_1occz00(long
		BgL_checksumz00_2632, char *BgL_fromz00_2633)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzreduce_1occz00))
				{
					BGl_requirezd2initializa7ationz75zzreduce_1occz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzreduce_1occz00();
					BGl_libraryzd2moduleszd2initz00zzreduce_1occz00();
					BGl_cnstzd2initzd2zzreduce_1occz00();
					BGl_importedzd2moduleszd2initz00zzreduce_1occz00();
					BGl_genericzd2initzd2zzreduce_1occz00();
					BGl_methodzd2initzd2zzreduce_1occz00();
					return BGl_toplevelzd2initzd2zzreduce_1occz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzreduce_1occz00(void)
	{
		{	/* Reduce/1occ.scm 16 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "reduce_1occ");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"reduce_1occ");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "reduce_1occ");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "reduce_1occ");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "reduce_1occ");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "reduce_1occ");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"reduce_1occ");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "reduce_1occ");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "reduce_1occ");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"reduce_1occ");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"reduce_1occ");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzreduce_1occz00(void)
	{
		{	/* Reduce/1occ.scm 16 */
			{	/* Reduce/1occ.scm 16 */
				obj_t BgL_cportz00_2447;

				{	/* Reduce/1occ.scm 16 */
					obj_t BgL_stringz00_2454;

					BgL_stringz00_2454 = BGl_string1944z00zzreduce_1occz00;
					{	/* Reduce/1occ.scm 16 */
						obj_t BgL_startz00_2455;

						BgL_startz00_2455 = BINT(0L);
						{	/* Reduce/1occ.scm 16 */
							obj_t BgL_endz00_2456;

							BgL_endz00_2456 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2454)));
							{	/* Reduce/1occ.scm 16 */

								BgL_cportz00_2447 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2454, BgL_startz00_2455, BgL_endz00_2456);
				}}}}
				{
					long BgL_iz00_2448;

					BgL_iz00_2448 = 2L;
				BgL_loopz00_2449:
					if ((BgL_iz00_2448 == -1L))
						{	/* Reduce/1occ.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Reduce/1occ.scm 16 */
							{	/* Reduce/1occ.scm 16 */
								obj_t BgL_arg1945z00_2450;

								{	/* Reduce/1occ.scm 16 */

									{	/* Reduce/1occ.scm 16 */
										obj_t BgL_locationz00_2452;

										BgL_locationz00_2452 = BBOOL(((bool_t) 0));
										{	/* Reduce/1occ.scm 16 */

											BgL_arg1945z00_2450 =
												BGl_readz00zz__readerz00(BgL_cportz00_2447,
												BgL_locationz00_2452);
										}
									}
								}
								{	/* Reduce/1occ.scm 16 */
									int BgL_tmpz00_2664;

									BgL_tmpz00_2664 = (int) (BgL_iz00_2448);
									CNST_TABLE_SET(BgL_tmpz00_2664, BgL_arg1945z00_2450);
							}}
							{	/* Reduce/1occ.scm 16 */
								int BgL_auxz00_2453;

								BgL_auxz00_2453 = (int) ((BgL_iz00_2448 - 1L));
								{
									long BgL_iz00_2669;

									BgL_iz00_2669 = (long) (BgL_auxz00_2453);
									BgL_iz00_2448 = BgL_iz00_2669;
									goto BgL_loopz00_2449;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzreduce_1occz00(void)
	{
		{	/* Reduce/1occ.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzreduce_1occz00(void)
	{
		{	/* Reduce/1occ.scm 16 */
			BGl_za2variablezd2removedza2zd2zzreduce_1occz00 = 0L;
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzreduce_1occz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1401;

				BgL_headz00_1401 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1402;
					obj_t BgL_tailz00_1403;

					BgL_prevz00_1402 = BgL_headz00_1401;
					BgL_tailz00_1403 = BgL_l1z00_1;
				BgL_loopz00_1404:
					if (PAIRP(BgL_tailz00_1403))
						{
							obj_t BgL_newzd2prevzd2_1406;

							BgL_newzd2prevzd2_1406 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1403), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1402, BgL_newzd2prevzd2_1406);
							{
								obj_t BgL_tailz00_2679;
								obj_t BgL_prevz00_2678;

								BgL_prevz00_2678 = BgL_newzd2prevzd2_1406;
								BgL_tailz00_2679 = CDR(BgL_tailz00_1403);
								BgL_tailz00_1403 = BgL_tailz00_2679;
								BgL_prevz00_1402 = BgL_prevz00_2678;
								goto BgL_loopz00_1404;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1401);
				}
			}
		}

	}



/* reduce-1occ! */
	BGL_EXPORTED_DEF obj_t BGl_reducezd21occz12zc0zzreduce_1occz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Reduce/1occ.scm 37 */
			{	/* Reduce/1occ.scm 38 */
				obj_t BgL_list1411z00_1409;

				BgL_list1411z00_1409 =
					MAKE_YOUNG_PAIR(BGl_string1912z00zzreduce_1occz00, BNIL);
				BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1411z00_1409);
			}
			BGl_occurzd2varzd2zzast_occurz00(BgL_globalsz00_3);
			BGl_za2variablezd2removedza2zd2zzreduce_1occz00 = 0L;
			{
				obj_t BgL_l1263z00_1411;

				BgL_l1263z00_1411 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31412ze3z87_1412:
				if (PAIRP(BgL_l1263z00_1411))
					{	/* Reduce/1occ.scm 44 */
						{	/* Reduce/1occ.scm 45 */
							obj_t BgL_globalz00_1414;

							BgL_globalz00_1414 = CAR(BgL_l1263z00_1411);
							{	/* Reduce/1occ.scm 45 */
								BgL_valuez00_bglt BgL_funz00_1415;

								BgL_funz00_1415 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_1414))))->
									BgL_valuez00);
								{	/* Reduce/1occ.scm 45 */
									obj_t BgL_nodez00_1416;

									BgL_nodez00_1416 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_funz00_1415)))->BgL_bodyz00);
									{	/* Reduce/1occ.scm 46 */

										{	/* Reduce/1occ.scm 47 */
											obj_t BgL_arg1421z00_1417;

											{	/* Reduce/1occ.scm 47 */
												obj_t BgL__z00_1418;

												BgL__z00_1418 =
													BGl_nodezd21occz12zc0zzreduce_1occz00(
													((BgL_nodez00_bglt) BgL_nodez00_1416), BNIL);
												{	/* Reduce/1occ.scm 48 */
													obj_t BgL_nodez00_1419;

													{	/* Reduce/1occ.scm 48 */
														obj_t BgL_tmpz00_2071;

														{	/* Reduce/1occ.scm 48 */
															int BgL_tmpz00_2696;

															BgL_tmpz00_2696 = (int) (1L);
															BgL_tmpz00_2071 =
																BGL_MVALUES_VAL(BgL_tmpz00_2696);
														}
														{	/* Reduce/1occ.scm 48 */
															int BgL_tmpz00_2699;

															BgL_tmpz00_2699 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_2699, BUNSPEC);
														}
														BgL_nodez00_1419 = BgL_tmpz00_2071;
													}
													BgL_arg1421z00_1417 = BgL_nodez00_1419;
											}}
											((((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_funz00_1415)))->
													BgL_bodyz00) =
												((obj_t) BgL_arg1421z00_1417), BUNSPEC);
										} BUNSPEC;
						}}}}
						{
							obj_t BgL_l1263z00_2704;

							BgL_l1263z00_2704 = CDR(BgL_l1263z00_1411);
							BgL_l1263z00_1411 = BgL_l1263z00_2704;
							goto BgL_zc3z04anonymousza31412ze3z87_1412;
						}
					}
				else
					{	/* Reduce/1occ.scm 44 */
						((bool_t) 1);
					}
			}
			{	/* Reduce/1occ.scm 52 */
				obj_t BgL_list1423z00_1422;

				{	/* Reduce/1occ.scm 52 */
					obj_t BgL_arg1434z00_1423;

					{	/* Reduce/1occ.scm 52 */
						obj_t BgL_arg1437z00_1424;

						{	/* Reduce/1occ.scm 52 */
							obj_t BgL_arg1448z00_1425;

							BgL_arg1448z00_1425 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
							BgL_arg1437z00_1424 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ')')),
								BgL_arg1448z00_1425);
						}
						BgL_arg1434z00_1423 =
							MAKE_YOUNG_PAIR(BINT
							(BGl_za2variablezd2removedza2zd2zzreduce_1occz00),
							BgL_arg1437z00_1424);
					}
					BgL_list1423z00_1422 =
						MAKE_YOUNG_PAIR(BGl_string1913z00zzreduce_1occz00,
						BgL_arg1434z00_1423);
				}
				BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1423z00_1422);
			}
			return BgL_globalsz00_3;
		}

	}



/* &reduce-1occ! */
	obj_t BGl_z62reducezd21occz12za2zzreduce_1occz00(obj_t BgL_envz00_2338,
		obj_t BgL_globalsz00_2339)
	{
		{	/* Reduce/1occ.scm 37 */
			return BGl_reducezd21occz12zc0zzreduce_1occz00(BgL_globalsz00_2339);
		}

	}



/* node-1occ-let-var! */
	obj_t BGl_nodezd21occzd2letzd2varz12zc0zzreduce_1occz00(BgL_letzd2varzd2_bglt
		BgL_nodez00_40, obj_t BgL_1zd2expza2z70_41)
	{
		{	/* Reduce/1occ.scm 279 */
			{
				obj_t BgL_obindingsz00_1430;
				bool_t BgL_resetz00_1431;
				obj_t BgL_extendz00_1432;

				BgL_obindingsz00_1430 =
					(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_40))->BgL_bindingsz00);
				BgL_resetz00_1431 = ((bool_t) 0);
				BgL_extendz00_1432 = BNIL;
			BgL_zc3z04anonymousza31454ze3z87_1433:
				if (NULLP(BgL_obindingsz00_1430))
					{	/* Reduce/1occ.scm 285 */
						bool_t BgL_test1986z00_2718;

						if (BgL_resetz00_1431)
							{	/* Reduce/1occ.scm 285 */
								BgL_test1986z00_2718 = ((bool_t) 1);
							}
						else
							{	/* Reduce/1occ.scm 285 */
								BgL_test1986z00_2718 =
									BGl_sidezd2effectzf3z21zzeffect_effectz00(
									(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_40))->
										BgL_bodyz00));
							}
						if (BgL_test1986z00_2718)
							{	/* Reduce/1occ.scm 286 */
								obj_t BgL_resetz72z72_1437;

								BgL_resetz72z72_1437 =
									BGl_nodezd21occz12zc0zzreduce_1occz00(
									(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_40))->
										BgL_bodyz00), BNIL);
								{	/* Reduce/1occ.scm 287 */
									obj_t BgL_nbodyz00_1438;

									{	/* Reduce/1occ.scm 288 */
										obj_t BgL_tmpz00_2074;

										{	/* Reduce/1occ.scm 288 */
											int BgL_tmpz00_2724;

											BgL_tmpz00_2724 = (int) (1L);
											BgL_tmpz00_2074 = BGL_MVALUES_VAL(BgL_tmpz00_2724);
										}
										{	/* Reduce/1occ.scm 288 */
											int BgL_tmpz00_2727;

											BgL_tmpz00_2727 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2727, BUNSPEC);
										}
										BgL_nbodyz00_1438 = BgL_tmpz00_2074;
									}
									((((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_40))->
											BgL_bodyz00) =
										((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nbodyz00_1438)),
										BUNSPEC);
									{	/* Reduce/1occ.scm 289 */
										int BgL_tmpz00_2732;

										BgL_tmpz00_2732 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2732);
									}
									{	/* Reduce/1occ.scm 289 */
										obj_t BgL_auxz00_2737;
										int BgL_tmpz00_2735;

										BgL_auxz00_2737 = ((obj_t) BgL_nodez00_40);
										BgL_tmpz00_2735 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_2735, BgL_auxz00_2737);
									}
									return BTRUE;
								}
							}
						else
							{	/* Reduce/1occ.scm 290 */
								obj_t BgL_1zd2expza2z72z02_1442;

								BgL_1zd2expza2z72z02_1442 =
									BGl_appendzd221011zd2zzreduce_1occz00(BgL_extendz00_1432,
									BgL_1zd2expza2z70_41);
								{	/* Reduce/1occ.scm 291 */
									obj_t BgL_resetz72z72_1443;

									BgL_resetz72z72_1443 =
										BGl_nodezd21occz12zc0zzreduce_1occz00(
										(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_40))->
											BgL_bodyz00), BgL_1zd2expza2z72z02_1442);
									{	/* Reduce/1occ.scm 292 */
										obj_t BgL_nbodyz00_1444;

										{	/* Reduce/1occ.scm 293 */
											obj_t BgL_tmpz00_2075;

											{	/* Reduce/1occ.scm 293 */
												int BgL_tmpz00_2743;

												BgL_tmpz00_2743 = (int) (1L);
												BgL_tmpz00_2075 = BGL_MVALUES_VAL(BgL_tmpz00_2743);
											}
											{	/* Reduce/1occ.scm 293 */
												int BgL_tmpz00_2746;

												BgL_tmpz00_2746 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_2746, BUNSPEC);
											}
											BgL_nbodyz00_1444 = BgL_tmpz00_2075;
										}
										((((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_40))->
												BgL_bodyz00) =
											((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
													BgL_nbodyz00_1444)), BUNSPEC);
										{
											obj_t BgL_obindingsz00_1448;
											obj_t BgL_nbindingsz00_1449;

											BgL_obindingsz00_1448 =
												(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_40))->
												BgL_bindingsz00);
											BgL_nbindingsz00_1449 = BNIL;
										BgL_zc3z04anonymousza31490ze3z87_1450:
											if (NULLP(BgL_obindingsz00_1448))
												{	/* Reduce/1occ.scm 300 */
													bool_t BgL_test1989z00_2753;

													if (
														(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_40))->
															BgL_removablezf3zf3))
														{	/* Reduce/1occ.scm 300 */
															BgL_test1989z00_2753 =
																NULLP(BgL_nbindingsz00_1449);
														}
													else
														{	/* Reduce/1occ.scm 300 */
															BgL_test1989z00_2753 = ((bool_t) 0);
														}
													if (BgL_test1989z00_2753)
														{	/* Reduce/1occ.scm 300 */
															{	/* Reduce/1occ.scm 305 */
																BgL_nodez00_bglt BgL_val1_1300z00_1455;

																BgL_val1_1300z00_1455 =
																	(((BgL_letzd2varzd2_bglt)
																		COBJECT(BgL_nodez00_40))->BgL_bodyz00);
																{	/* Reduce/1occ.scm 305 */
																	int BgL_tmpz00_2758;

																	BgL_tmpz00_2758 = (int) (2L);
																	BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2758);
																}
																{	/* Reduce/1occ.scm 305 */
																	obj_t BgL_auxz00_2763;
																	int BgL_tmpz00_2761;

																	BgL_auxz00_2763 =
																		((obj_t) BgL_val1_1300z00_1455);
																	BgL_tmpz00_2761 = (int) (1L);
																	BGL_MVALUES_VAL_SET(BgL_tmpz00_2761,
																		BgL_auxz00_2763);
																}
																return BgL_resetz72z72_1443;
															}
														}
													else
														{	/* Reduce/1occ.scm 300 */
															((((BgL_letzd2varzd2_bglt)
																		COBJECT(BgL_nodez00_40))->BgL_bindingsz00) =
																((obj_t)
																	bgl_reverse_bang(BgL_nbindingsz00_1449)),
																BUNSPEC);
															{	/* Reduce/1occ.scm 308 */
																int BgL_tmpz00_2768;

																BgL_tmpz00_2768 = (int) (2L);
																BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2768);
															}
															{	/* Reduce/1occ.scm 308 */
																obj_t BgL_auxz00_2773;
																int BgL_tmpz00_2771;

																BgL_auxz00_2773 = ((obj_t) BgL_nodez00_40);
																BgL_tmpz00_2771 = (int) (1L);
																BGL_MVALUES_VAL_SET(BgL_tmpz00_2771,
																	BgL_auxz00_2773);
															}
															return BgL_resetz72z72_1443;
														}
												}
											else
												{	/* Reduce/1occ.scm 309 */
													bool_t BgL_test1991z00_2776;

													{	/* Reduce/1occ.scm 309 */
														obj_t BgL_varz00_1471;
														obj_t BgL_valz00_1472;

														{	/* Reduce/1occ.scm 309 */
															obj_t BgL_pairz00_2077;

															BgL_pairz00_2077 =
																CAR(((obj_t) BgL_obindingsz00_1448));
															BgL_varz00_1471 = CAR(BgL_pairz00_2077);
														}
														{	/* Reduce/1occ.scm 310 */
															obj_t BgL_pairz00_2079;

															BgL_pairz00_2079 =
																CAR(((obj_t) BgL_obindingsz00_1448));
															BgL_valz00_1472 = CDR(BgL_pairz00_2079);
														}
														if (
															((((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					BgL_varz00_1471))))->
																	BgL_occurrencez00) == 0L))
															{	/* Reduce/1occ.scm 311 */
																if (BGl_sidezd2effectzf3z21zzeffect_effectz00(
																		((BgL_nodez00_bglt) BgL_valz00_1472)))
																	{	/* Reduce/1occ.scm 312 */
																		BgL_test1991z00_2776 = ((bool_t) 0);
																	}
																else
																	{	/* Reduce/1occ.scm 312 */
																		BgL_test1991z00_2776 = ((bool_t) 1);
																	}
															}
														else
															{	/* Reduce/1occ.scm 311 */
																BgL_test1991z00_2776 = ((bool_t) 0);
															}
													}
													if (BgL_test1991z00_2776)
														{	/* Reduce/1occ.scm 309 */
															{	/* Reduce/1occ.scm 316 */
																obj_t BgL_arg1516z00_1467;

																BgL_arg1516z00_1467 =
																	CDR(((obj_t) BgL_obindingsz00_1448));
																{
																	obj_t BgL_obindingsz00_2793;

																	BgL_obindingsz00_2793 = BgL_arg1516z00_1467;
																	BgL_obindingsz00_1448 = BgL_obindingsz00_2793;
																	goto BgL_zc3z04anonymousza31490ze3z87_1450;
																}
															}
														}
													else
														{	/* Reduce/1occ.scm 318 */
															obj_t BgL_arg1535z00_1468;
															obj_t BgL_arg1540z00_1469;

															BgL_arg1535z00_1468 =
																CDR(((obj_t) BgL_obindingsz00_1448));
															{	/* Reduce/1occ.scm 319 */
																obj_t BgL_arg1544z00_1470;

																BgL_arg1544z00_1470 =
																	CAR(((obj_t) BgL_obindingsz00_1448));
																BgL_arg1540z00_1469 =
																	MAKE_YOUNG_PAIR(BgL_arg1544z00_1470,
																	BgL_nbindingsz00_1449);
															}
															{
																obj_t BgL_nbindingsz00_2800;
																obj_t BgL_obindingsz00_2799;

																BgL_obindingsz00_2799 = BgL_arg1535z00_1468;
																BgL_nbindingsz00_2800 = BgL_arg1540z00_1469;
																BgL_nbindingsz00_1449 = BgL_nbindingsz00_2800;
																BgL_obindingsz00_1448 = BgL_obindingsz00_2799;
																goto BgL_zc3z04anonymousza31490ze3z87_1450;
															}
														}
												}
										}
									}
								}
							}
					}
				else
					{	/* Reduce/1occ.scm 320 */
						obj_t BgL_bindingz00_1481;

						BgL_bindingz00_1481 = CAR(((obj_t) BgL_obindingsz00_1430));
						{	/* Reduce/1occ.scm 321 */
							obj_t BgL_varz00_1482;
							obj_t BgL_valz00_1483;

							BgL_varz00_1482 = CAR(((obj_t) BgL_bindingz00_1481));
							BgL_valz00_1483 = CDR(((obj_t) BgL_bindingz00_1481));
							{	/* Reduce/1occ.scm 323 */
								obj_t BgL_resetz72z72_1484;

								BgL_resetz72z72_1484 =
									BGl_nodezd21occz12zc0zzreduce_1occz00(
									((BgL_nodez00_bglt) BgL_valz00_1483), BgL_1zd2expza2z70_41);
								{	/* Reduce/1occ.scm 324 */
									obj_t BgL_nvalz00_1485;

									{	/* Reduce/1occ.scm 325 */
										obj_t BgL_tmpz00_2088;

										{	/* Reduce/1occ.scm 325 */
											int BgL_tmpz00_2810;

											BgL_tmpz00_2810 = (int) (1L);
											BgL_tmpz00_2088 = BGL_MVALUES_VAL(BgL_tmpz00_2810);
										}
										{	/* Reduce/1occ.scm 325 */
											int BgL_tmpz00_2813;

											BgL_tmpz00_2813 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2813, BUNSPEC);
										}
										BgL_nvalz00_1485 = BgL_tmpz00_2088;
									}
									{	/* Reduce/1occ.scm 325 */
										obj_t BgL_tmpz00_2816;

										BgL_tmpz00_2816 = ((obj_t) BgL_bindingz00_1481);
										SET_CDR(BgL_tmpz00_2816, BgL_nvalz00_1485);
									}
									{	/* Reduce/1occ.scm 327 */
										bool_t BgL_test1994z00_2819;

										if (BgL_resetz00_1431)
											{	/* Reduce/1occ.scm 327 */
												BgL_test1994z00_2819 = ((bool_t) 1);
											}
										else
											{	/* Reduce/1occ.scm 327 */
												BgL_test1994z00_2819 = CBOOL(BgL_resetz72z72_1484);
											}
										if (BgL_test1994z00_2819)
											{	/* Reduce/1occ.scm 328 */
												obj_t BgL_arg1564z00_1487;

												BgL_arg1564z00_1487 =
													CDR(((obj_t) BgL_obindingsz00_1430));
												{
													obj_t BgL_extendz00_2826;
													bool_t BgL_resetz00_2825;
													obj_t BgL_obindingsz00_2824;

													BgL_obindingsz00_2824 = BgL_arg1564z00_1487;
													BgL_resetz00_2825 = ((bool_t) 1);
													BgL_extendz00_2826 = BNIL;
													BgL_extendz00_1432 = BgL_extendz00_2826;
													BgL_resetz00_1431 = BgL_resetz00_2825;
													BgL_obindingsz00_1430 = BgL_obindingsz00_2824;
													goto BgL_zc3z04anonymousza31454ze3z87_1433;
												}
											}
										else
											{	/* Reduce/1occ.scm 327 */
												if (
													((((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_varz00_1482))))->
															BgL_accessz00) == CNST_TABLE_REF(0)))
													{	/* Reduce/1occ.scm 331 */
														bool_t BgL_test1997z00_2833;

														if (
															((((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					BgL_varz00_1482))))->
																	BgL_occurrencez00) == 1L))
															{	/* Reduce/1occ.scm 331 */
																if (
																	((((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_localz00_bglt)
																							BgL_varz00_1482))))->
																			BgL_accessz00) == CNST_TABLE_REF(0)))
																	{	/* Reduce/1occ.scm 332 */
																		if (BGl_sidezd2effectzf3z21zzeffect_effectz00(((BgL_nodez00_bglt) BgL_valz00_1483)))
																			{	/* Reduce/1occ.scm 333 */
																				BgL_test1997z00_2833 = ((bool_t) 0);
																			}
																		else
																			{	/* Reduce/1occ.scm 334 */
																				BgL_typez00_bglt BgL_arg1602z00_1506;
																				BgL_typez00_bglt BgL_arg1605z00_1507;

																				BgL_arg1602z00_1506 =
																					(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_localz00_bglt)
																									BgL_varz00_1482))))->
																					BgL_typez00);
																				BgL_arg1605z00_1507 =
																					BGl_getzd2typezd2zztype_typeofz00((
																						(BgL_nodez00_bglt) BgL_valz00_1483),
																					((bool_t) 0));
																				BgL_test1997z00_2833 =
																					BGl_typezd2lesszd2specificzf3zf3zztype_miscz00
																					(BgL_arg1602z00_1506,
																					BgL_arg1605z00_1507);
																			}
																	}
																else
																	{	/* Reduce/1occ.scm 332 */
																		BgL_test1997z00_2833 = ((bool_t) 0);
																	}
															}
														else
															{	/* Reduce/1occ.scm 331 */
																BgL_test1997z00_2833 = ((bool_t) 0);
															}
														if (BgL_test1997z00_2833)
															{	/* Reduce/1occ.scm 331 */
																{	/* Reduce/1occ.scm 338 */
																	obj_t BgL_arg1593z00_1500;
																	obj_t BgL_arg1594z00_1501;

																	BgL_arg1593z00_1500 =
																		CDR(((obj_t) BgL_obindingsz00_1430));
																	BgL_arg1594z00_1501 =
																		MAKE_YOUNG_PAIR(BgL_bindingz00_1481,
																		BgL_extendz00_1432);
																	{
																		obj_t BgL_extendz00_2859;
																		bool_t BgL_resetz00_2858;
																		obj_t BgL_obindingsz00_2857;

																		BgL_obindingsz00_2857 = BgL_arg1593z00_1500;
																		BgL_resetz00_2858 = ((bool_t) 0);
																		BgL_extendz00_2859 = BgL_arg1594z00_1501;
																		BgL_extendz00_1432 = BgL_extendz00_2859;
																		BgL_resetz00_1431 = BgL_resetz00_2858;
																		BgL_obindingsz00_1430 =
																			BgL_obindingsz00_2857;
																		goto BgL_zc3z04anonymousza31454ze3z87_1433;
																	}
																}
															}
														else
															{	/* Reduce/1occ.scm 340 */
																obj_t BgL_arg1595z00_1502;

																BgL_arg1595z00_1502 =
																	CDR(((obj_t) BgL_obindingsz00_1430));
																{
																	bool_t BgL_resetz00_2863;
																	obj_t BgL_obindingsz00_2862;

																	BgL_obindingsz00_2862 = BgL_arg1595z00_1502;
																	BgL_resetz00_2863 = ((bool_t) 0);
																	BgL_resetz00_1431 = BgL_resetz00_2863;
																	BgL_obindingsz00_1430 = BgL_obindingsz00_2862;
																	goto BgL_zc3z04anonymousza31454ze3z87_1433;
																}
															}
													}
												else
													{	/* Reduce/1occ.scm 330 */
														obj_t BgL_arg1611z00_1510;

														BgL_arg1611z00_1510 =
															CDR(((obj_t) BgL_obindingsz00_1430));
														{
															bool_t BgL_resetz00_2867;
															obj_t BgL_obindingsz00_2866;

															BgL_obindingsz00_2866 = BgL_arg1611z00_1510;
															BgL_resetz00_2867 = ((bool_t) 0);
															BgL_resetz00_1431 = BgL_resetz00_2867;
															BgL_obindingsz00_1430 = BgL_obindingsz00_2866;
															goto BgL_zc3z04anonymousza31454ze3z87_1433;
														}
													}
											}
									}
								}
							}
						}
					}
			}
		}

	}



/* node-1occ*! */
	obj_t BGl_nodezd21occza2z12z62zzreduce_1occz00(obj_t BgL_nodeza2za2_58,
		obj_t BgL_1zd2expza2z70_59)
	{
		{	/* Reduce/1occ.scm 431 */
			{
				obj_t BgL_nodeza2za2_1514;
				bool_t BgL_resetz00_1515;
				obj_t BgL_1zd2expza2z70_1516;

				BgL_nodeza2za2_1514 = BgL_nodeza2za2_58;
				BgL_resetz00_1515 = ((bool_t) 0);
				BgL_1zd2expza2z70_1516 = BgL_1zd2expza2z70_59;
			BgL_zc3z04anonymousza31614ze3z87_1517:
				if (NULLP(BgL_nodeza2za2_1514))
					{	/* Reduce/1occ.scm 436 */
						return BBOOL(BgL_resetz00_1515);
					}
				else
					{	/* Reduce/1occ.scm 436 */
						if (NULLP(CDR(((obj_t) BgL_nodeza2za2_1514))))
							{	/* Reduce/1occ.scm 439 */
								obj_t BgL_resetz72z72_1521;

								{	/* Reduce/1occ.scm 440 */
									obj_t BgL_arg1625z00_1524;

									BgL_arg1625z00_1524 = CAR(((obj_t) BgL_nodeza2za2_1514));
									BgL_resetz72z72_1521 =
										BGl_nodezd21occz12zc0zzreduce_1occz00(
										((BgL_nodez00_bglt) BgL_arg1625z00_1524),
										BgL_1zd2expza2z70_1516);
								}
								{	/* Reduce/1occ.scm 440 */
									obj_t BgL_nodez00_1522;

									{	/* Reduce/1occ.scm 441 */
										obj_t BgL_tmpz00_2101;

										{	/* Reduce/1occ.scm 441 */
											int BgL_tmpz00_2880;

											BgL_tmpz00_2880 = (int) (1L);
											BgL_tmpz00_2101 = BGL_MVALUES_VAL(BgL_tmpz00_2880);
										}
										{	/* Reduce/1occ.scm 441 */
											int BgL_tmpz00_2883;

											BgL_tmpz00_2883 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2883, BUNSPEC);
										}
										BgL_nodez00_1522 = BgL_tmpz00_2101;
									}
									{	/* Reduce/1occ.scm 441 */
										obj_t BgL_tmpz00_2886;

										BgL_tmpz00_2886 = ((obj_t) BgL_nodeza2za2_1514);
										SET_CAR(BgL_tmpz00_2886, BgL_nodez00_1522);
									}
									if (BgL_resetz00_1515)
										{	/* Reduce/1occ.scm 442 */
											return BBOOL(BgL_resetz00_1515);
										}
									else
										{	/* Reduce/1occ.scm 442 */
											return BgL_resetz72z72_1521;
										}
								}
							}
						else
							{	/* Reduce/1occ.scm 444 */
								obj_t BgL_resetz72z72_1525;

								{	/* Reduce/1occ.scm 445 */
									obj_t BgL_arg1630z00_1530;

									BgL_arg1630z00_1530 = CAR(((obj_t) BgL_nodeza2za2_1514));
									BgL_resetz72z72_1525 =
										BGl_nodezd21occz12zc0zzreduce_1occz00(
										((BgL_nodez00_bglt) BgL_arg1630z00_1530),
										BgL_1zd2expza2z70_1516);
								}
								{	/* Reduce/1occ.scm 445 */
									obj_t BgL_nodez00_1526;

									{	/* Reduce/1occ.scm 446 */
										obj_t BgL_tmpz00_2104;

										{	/* Reduce/1occ.scm 446 */
											int BgL_tmpz00_2895;

											BgL_tmpz00_2895 = (int) (1L);
											BgL_tmpz00_2104 = BGL_MVALUES_VAL(BgL_tmpz00_2895);
										}
										{	/* Reduce/1occ.scm 446 */
											int BgL_tmpz00_2898;

											BgL_tmpz00_2898 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2898, BUNSPEC);
										}
										BgL_nodez00_1526 = BgL_tmpz00_2104;
									}
									{	/* Reduce/1occ.scm 446 */
										obj_t BgL_tmpz00_2901;

										BgL_tmpz00_2901 = ((obj_t) BgL_nodeza2za2_1514);
										SET_CAR(BgL_tmpz00_2901, BgL_nodez00_1526);
									}
									{	/* Reduce/1occ.scm 447 */
										bool_t BgL_test2004z00_2904;

										if (BgL_resetz00_1515)
											{	/* Reduce/1occ.scm 447 */
												BgL_test2004z00_2904 = ((bool_t) 1);
											}
										else
											{	/* Reduce/1occ.scm 447 */
												BgL_test2004z00_2904 = CBOOL(BgL_resetz72z72_1525);
											}
										if (BgL_test2004z00_2904)
											{	/* Reduce/1occ.scm 448 */
												obj_t BgL_arg1627z00_1528;

												BgL_arg1627z00_1528 =
													CDR(((obj_t) BgL_nodeza2za2_1514));
												{
													obj_t BgL_1zd2expza2z70_2911;
													bool_t BgL_resetz00_2910;
													obj_t BgL_nodeza2za2_2909;

													BgL_nodeza2za2_2909 = BgL_arg1627z00_1528;
													BgL_resetz00_2910 = ((bool_t) 1);
													BgL_1zd2expza2z70_2911 = BNIL;
													BgL_1zd2expza2z70_1516 = BgL_1zd2expza2z70_2911;
													BgL_resetz00_1515 = BgL_resetz00_2910;
													BgL_nodeza2za2_1514 = BgL_nodeza2za2_2909;
													goto BgL_zc3z04anonymousza31614ze3z87_1517;
												}
											}
										else
											{	/* Reduce/1occ.scm 449 */
												obj_t BgL_arg1629z00_1529;

												BgL_arg1629z00_1529 =
													CDR(((obj_t) BgL_nodeza2za2_1514));
												{
													bool_t BgL_resetz00_2915;
													obj_t BgL_nodeza2za2_2914;

													BgL_nodeza2za2_2914 = BgL_arg1629z00_1529;
													BgL_resetz00_2915 = ((bool_t) 0);
													BgL_resetz00_1515 = BgL_resetz00_2915;
													BgL_nodeza2za2_1514 = BgL_nodeza2za2_2914;
													goto BgL_zc3z04anonymousza31614ze3z87_1517;
												}
											}
									}
								}
							}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzreduce_1occz00(void)
	{
		{	/* Reduce/1occ.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzreduce_1occz00(void)
	{
		{	/* Reduce/1occ.scm 16 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_proc1914z00zzreduce_1occz00, BGl_nodez00zzast_nodez00,
				BGl_string1915z00zzreduce_1occz00);
		}

	}



/* &node-1occ!1321 */
	obj_t BGl_z62nodezd21occz121321za2zzreduce_1occz00(obj_t BgL_envz00_2341,
		obj_t BgL_nodez00_2342, obj_t BgL_1zd2expza2z70_2343)
	{
		{	/* Reduce/1occ.scm 67 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(1),
				BGl_string1916z00zzreduce_1occz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2342)));
		}

	}



/* node-1occ! */
	obj_t BGl_nodezd21occz12zc0zzreduce_1occz00(BgL_nodez00_bglt BgL_nodez00_4,
		obj_t BgL_1zd2expza2z70_5)
	{
		{	/* Reduce/1occ.scm 67 */
			{	/* Reduce/1occ.scm 67 */
				obj_t BgL_method1322z00_1538;

				{	/* Reduce/1occ.scm 67 */
					obj_t BgL_res1907z00_2138;

					{	/* Reduce/1occ.scm 67 */
						long BgL_objzd2classzd2numz00_2109;

						BgL_objzd2classzd2numz00_2109 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_4));
						{	/* Reduce/1occ.scm 67 */
							obj_t BgL_arg1811z00_2110;

							BgL_arg1811z00_2110 =
								PROCEDURE_REF(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
								(int) (1L));
							{	/* Reduce/1occ.scm 67 */
								int BgL_offsetz00_2113;

								BgL_offsetz00_2113 = (int) (BgL_objzd2classzd2numz00_2109);
								{	/* Reduce/1occ.scm 67 */
									long BgL_offsetz00_2114;

									BgL_offsetz00_2114 =
										((long) (BgL_offsetz00_2113) - OBJECT_TYPE);
									{	/* Reduce/1occ.scm 67 */
										long BgL_modz00_2115;

										BgL_modz00_2115 =
											(BgL_offsetz00_2114 >> (int) ((long) ((int) (4L))));
										{	/* Reduce/1occ.scm 67 */
											long BgL_restz00_2117;

											BgL_restz00_2117 =
												(BgL_offsetz00_2114 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Reduce/1occ.scm 67 */

												{	/* Reduce/1occ.scm 67 */
													obj_t BgL_bucketz00_2119;

													BgL_bucketz00_2119 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2110), BgL_modz00_2115);
													BgL_res1907z00_2138 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2119), BgL_restz00_2117);
					}}}}}}}}
					BgL_method1322z00_1538 = BgL_res1907z00_2138;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1322z00_1538,
					((obj_t) BgL_nodez00_4), BgL_1zd2expza2z70_5);
			}
		}

	}



/* &node-1occ! */
	obj_t BGl_z62nodezd21occz12za2zzreduce_1occz00(obj_t BgL_envz00_2344,
		obj_t BgL_nodez00_2345, obj_t BgL_1zd2expza2z70_2346)
	{
		{	/* Reduce/1occ.scm 67 */
			return
				BGl_nodezd21occz12zc0zzreduce_1occz00(
				((BgL_nodez00_bglt) BgL_nodez00_2345), BgL_1zd2expza2z70_2346);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzreduce_1occz00(void)
	{
		{	/* Reduce/1occ.scm 16 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00, BGl_atomz00zzast_nodez00,
				BGl_proc1917z00zzreduce_1occz00, BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00, BGl_kwotez00zzast_nodez00,
				BGl_proc1919z00zzreduce_1occz00, BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00, BGl_varz00zzast_nodez00,
				BGl_proc1920z00zzreduce_1occz00, BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_closurez00zzast_nodez00, BGl_proc1921z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1922z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00, BGl_syncz00zzast_nodez00,
				BGl_proc1923z00zzreduce_1occz00, BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1924z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_funcallz00zzast_nodez00, BGl_proc1925z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_externz00zzast_nodez00, BGl_proc1926z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_privatez00zzast_nodez00, BGl_proc1927z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00, BGl_castz00zzast_nodez00,
				BGl_proc1928z00zzreduce_1occz00, BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00, BGl_setqz00zzast_nodez00,
				BGl_proc1929z00zzreduce_1occz00, BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1930z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00, BGl_failz00zzast_nodez00,
				BGl_proc1931z00zzreduce_1occz00, BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_switchz00zzast_nodez00, BGl_proc1932z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1933z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1934z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1935z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1936z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_retblockz00zzast_nodez00, BGl_proc1937z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_returnz00zzast_nodez00, BGl_proc1938z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1939z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1940z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1941z00zzreduce_1occz00,
				BGl_string1918z00zzreduce_1occz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd21occz12zd2envz12zzreduce_1occz00, BGl_appz00zzast_nodez00,
				BGl_proc1942z00zzreduce_1occz00, BGl_string1918z00zzreduce_1occz00);
		}

	}



/* &node-1occ!-app1374 */
	obj_t BGl_z62nodezd21occz12zd2app1374z70zzreduce_1occz00(obj_t
		BgL_envz00_2372, obj_t BgL_nodez00_2373, obj_t BgL_1zd2expza2z70_2374)
	{
		{	/* Reduce/1occ.scm 418 */
			{	/* Reduce/1occ.scm 419 */
				bool_t BgL_tmpz00_2979;

				{	/* Reduce/1occ.scm 420 */
					obj_t BgL_resetz00_2460;

					BgL_resetz00_2460 =
						BGl_nodezd21occza2z12z62zzreduce_1occz00(
						(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nodez00_2373)))->BgL_argsz00),
						BgL_1zd2expza2z70_2374);
					{	/* Reduce/1occ.scm 421 */
						bool_t BgL_test2006z00_2983;

						if (CBOOL(BgL_resetz00_2460))
							{	/* Reduce/1occ.scm 421 */
								BgL_test2006z00_2983 = ((bool_t) 1);
							}
						else
							{	/* Reduce/1occ.scm 421 */
								BgL_test2006z00_2983 =
									BGl_sidezd2effectzf3z21zzeffect_effectz00(
									((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2373)));
							}
						if (BgL_test2006z00_2983)
							{	/* Reduce/1occ.scm 421 */
								{	/* Reduce/1occ.scm 425 */
									int BgL_tmpz00_2989;

									BgL_tmpz00_2989 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2989);
								}
								{	/* Reduce/1occ.scm 425 */
									obj_t BgL_auxz00_2994;
									int BgL_tmpz00_2992;

									BgL_auxz00_2994 =
										((obj_t) ((BgL_appz00_bglt) BgL_nodez00_2373));
									BgL_tmpz00_2992 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_2992, BgL_auxz00_2994);
								}
								BgL_tmpz00_2979 = ((bool_t) 1);
							}
						else
							{	/* Reduce/1occ.scm 421 */
								{	/* Reduce/1occ.scm 426 */
									int BgL_tmpz00_2998;

									BgL_tmpz00_2998 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2998);
								}
								{	/* Reduce/1occ.scm 426 */
									obj_t BgL_auxz00_3003;
									int BgL_tmpz00_3001;

									BgL_auxz00_3003 =
										((obj_t) ((BgL_appz00_bglt) BgL_nodez00_2373));
									BgL_tmpz00_3001 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3001, BgL_auxz00_3003);
								}
								BgL_tmpz00_2979 = ((bool_t) 0);
				}}}
				return BBOOL(BgL_tmpz00_2979);
			}
		}

	}



/* &node-1occ!-box-ref1371 */
	obj_t BGl_z62nodezd21occz12zd2boxzd2ref1371za2zzreduce_1occz00(obj_t
		BgL_envz00_2375, obj_t BgL_nodez00_2376, obj_t BgL_1zd2expza2z70_2377)
	{
		{	/* Reduce/1occ.scm 412 */
			{	/* Reduce/1occ.scm 413 */
				bool_t BgL_tmpz00_3008;

				{	/* Reduce/1occ.scm 413 */
					int BgL_tmpz00_3009;

					BgL_tmpz00_3009 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3009);
				}
				{	/* Reduce/1occ.scm 413 */
					obj_t BgL_auxz00_3014;
					int BgL_tmpz00_3012;

					BgL_auxz00_3014 =
						((obj_t) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2376));
					BgL_tmpz00_3012 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_3012, BgL_auxz00_3014);
				}
				BgL_tmpz00_3008 = ((bool_t) 0);
				return BBOOL(BgL_tmpz00_3008);
			}
		}

	}



/* &node-1occ!-box-set!1368 */
	obj_t BGl_z62nodezd21occz12zd2boxzd2setz121368zb0zzreduce_1occz00(obj_t
		BgL_envz00_2378, obj_t BgL_nodez00_2379, obj_t BgL_1zd2expza2z70_2380)
	{
		{	/* Reduce/1occ.scm 402 */
			{	/* Reduce/1occ.scm 404 */
				obj_t BgL_resetz00_2463;

				BgL_resetz00_2463 =
					BGl_nodezd21occz12zc0zzreduce_1occz00(
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2379)))->BgL_valuez00),
					BgL_1zd2expza2z70_2380);
				{	/* Reduce/1occ.scm 405 */
					obj_t BgL_nvaluez00_2464;

					{	/* Reduce/1occ.scm 406 */
						obj_t BgL_tmpz00_2465;

						{	/* Reduce/1occ.scm 406 */
							int BgL_tmpz00_3022;

							BgL_tmpz00_3022 = (int) (1L);
							BgL_tmpz00_2465 = BGL_MVALUES_VAL(BgL_tmpz00_3022);
						}
						{	/* Reduce/1occ.scm 406 */
							int BgL_tmpz00_3025;

							BgL_tmpz00_3025 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3025, BUNSPEC);
						}
						BgL_nvaluez00_2464 = BgL_tmpz00_2465;
					}
					((((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2379)))->
							BgL_valuez00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nvaluez00_2464)),
						BUNSPEC);
					{	/* Reduce/1occ.scm 407 */
						int BgL_tmpz00_3031;

						BgL_tmpz00_3031 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3031);
					}
					{	/* Reduce/1occ.scm 407 */
						obj_t BgL_auxz00_3036;
						int BgL_tmpz00_3034;

						BgL_auxz00_3036 =
							((obj_t) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2379));
						BgL_tmpz00_3034 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_3034, BgL_auxz00_3036);
					}
					return BgL_resetz00_2463;
				}
			}
		}

	}



/* &node-1occ!-make-box1366 */
	obj_t BGl_z62nodezd21occz12zd2makezd2box1366za2zzreduce_1occz00(obj_t
		BgL_envz00_2381, obj_t BgL_nodez00_2382, obj_t BgL_1zd2expza2z70_2383)
	{
		{	/* Reduce/1occ.scm 392 */
			{	/* Reduce/1occ.scm 394 */
				obj_t BgL_resetz00_2467;

				BgL_resetz00_2467 =
					BGl_nodezd21occz12zc0zzreduce_1occz00(
					(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2382)))->BgL_valuez00),
					BgL_1zd2expza2z70_2383);
				{	/* Reduce/1occ.scm 395 */
					obj_t BgL_nvaluez00_2468;

					{	/* Reduce/1occ.scm 396 */
						obj_t BgL_tmpz00_2469;

						{	/* Reduce/1occ.scm 396 */
							int BgL_tmpz00_3043;

							BgL_tmpz00_3043 = (int) (1L);
							BgL_tmpz00_2469 = BGL_MVALUES_VAL(BgL_tmpz00_3043);
						}
						{	/* Reduce/1occ.scm 396 */
							int BgL_tmpz00_3046;

							BgL_tmpz00_3046 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3046, BUNSPEC);
						}
						BgL_nvaluez00_2468 = BgL_tmpz00_2469;
					}
					((((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_2382)))->BgL_valuez00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nvaluez00_2468)),
						BUNSPEC);
					{	/* Reduce/1occ.scm 397 */
						int BgL_tmpz00_3052;

						BgL_tmpz00_3052 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3052);
					}
					{	/* Reduce/1occ.scm 397 */
						obj_t BgL_auxz00_3057;
						int BgL_tmpz00_3055;

						BgL_auxz00_3057 =
							((obj_t) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2382));
						BgL_tmpz00_3055 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_3055, BgL_auxz00_3057);
					}
					return BgL_resetz00_2467;
				}
			}
		}

	}



/* &node-1occ!-return1364 */
	obj_t BGl_z62nodezd21occz12zd2return1364z70zzreduce_1occz00(obj_t
		BgL_envz00_2384, obj_t BgL_nodez00_2385, obj_t BgL_1zd2expza2z70_2386)
	{
		{	/* Reduce/1occ.scm 382 */
			{	/* Reduce/1occ.scm 384 */
				obj_t BgL_resetz00_2471;

				BgL_resetz00_2471 =
					BGl_nodezd21occz12zc0zzreduce_1occz00(
					(((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nodez00_2385)))->BgL_valuez00),
					BgL_1zd2expza2z70_2386);
				{	/* Reduce/1occ.scm 385 */
					obj_t BgL_nvaluez00_2472;

					{	/* Reduce/1occ.scm 386 */
						obj_t BgL_tmpz00_2473;

						{	/* Reduce/1occ.scm 386 */
							int BgL_tmpz00_3064;

							BgL_tmpz00_3064 = (int) (1L);
							BgL_tmpz00_2473 = BGL_MVALUES_VAL(BgL_tmpz00_3064);
						}
						{	/* Reduce/1occ.scm 386 */
							int BgL_tmpz00_3067;

							BgL_tmpz00_3067 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3067, BUNSPEC);
						}
						BgL_nvaluez00_2472 = BgL_tmpz00_2473;
					}
					((((BgL_returnz00_bglt) COBJECT(
									((BgL_returnz00_bglt) BgL_nodez00_2385)))->BgL_valuez00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nvaluez00_2472)),
						BUNSPEC);
					{	/* Reduce/1occ.scm 387 */
						int BgL_tmpz00_3073;

						BgL_tmpz00_3073 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3073);
					}
					{	/* Reduce/1occ.scm 387 */
						obj_t BgL_auxz00_3078;
						int BgL_tmpz00_3076;

						BgL_auxz00_3078 = ((obj_t) ((BgL_returnz00_bglt) BgL_nodez00_2385));
						BgL_tmpz00_3076 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_3076, BgL_auxz00_3078);
					}
					return BgL_resetz00_2471;
				}
			}
		}

	}



/* &node-1occ!-retblock1362 */
	obj_t BGl_z62nodezd21occz12zd2retblock1362z70zzreduce_1occz00(obj_t
		BgL_envz00_2387, obj_t BgL_nodez00_2388, obj_t BgL_1zd2expza2z70_2389)
	{
		{	/* Reduce/1occ.scm 372 */
			{	/* Reduce/1occ.scm 374 */
				obj_t BgL_resetz00_2475;

				BgL_resetz00_2475 =
					BGl_nodezd21occz12zc0zzreduce_1occz00(
					(((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nodez00_2388)))->BgL_bodyz00),
					BgL_1zd2expza2z70_2389);
				{	/* Reduce/1occ.scm 375 */
					obj_t BgL_nbodyz00_2476;

					{	/* Reduce/1occ.scm 376 */
						obj_t BgL_tmpz00_2477;

						{	/* Reduce/1occ.scm 376 */
							int BgL_tmpz00_3085;

							BgL_tmpz00_3085 = (int) (1L);
							BgL_tmpz00_2477 = BGL_MVALUES_VAL(BgL_tmpz00_3085);
						}
						{	/* Reduce/1occ.scm 376 */
							int BgL_tmpz00_3088;

							BgL_tmpz00_3088 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3088, BUNSPEC);
						}
						BgL_nbodyz00_2476 = BgL_tmpz00_2477;
					}
					((((BgL_retblockz00_bglt) COBJECT(
									((BgL_retblockz00_bglt) BgL_nodez00_2388)))->BgL_bodyz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nbodyz00_2476)),
						BUNSPEC);
					{	/* Reduce/1occ.scm 377 */
						int BgL_tmpz00_3094;

						BgL_tmpz00_3094 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3094);
					}
					{	/* Reduce/1occ.scm 377 */
						obj_t BgL_auxz00_3099;
						int BgL_tmpz00_3097;

						BgL_auxz00_3099 =
							((obj_t) ((BgL_retblockz00_bglt) BgL_nodez00_2388));
						BgL_tmpz00_3097 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_3097, BgL_auxz00_3099);
					}
					return BgL_resetz00_2475;
				}
			}
		}

	}



/* &node-1occ!-jump-ex-i1360 */
	obj_t BGl_z62nodezd21occz12zd2jumpzd2exzd2i1360z70zzreduce_1occz00(obj_t
		BgL_envz00_2390, obj_t BgL_nodez00_2391, obj_t BgL_1zd2expza2z70_2392)
	{
		{	/* Reduce/1occ.scm 358 */
			{	/* Reduce/1occ.scm 360 */
				obj_t BgL_resetz00_2479;

				BgL_resetz00_2479 =
					BGl_nodezd21occz12zc0zzreduce_1occz00(
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2391)))->BgL_exitz00),
					BgL_1zd2expza2z70_2392);
				{	/* Reduce/1occ.scm 361 */
					obj_t BgL_nexitz00_2480;

					{	/* Reduce/1occ.scm 362 */
						obj_t BgL_tmpz00_2481;

						{	/* Reduce/1occ.scm 362 */
							int BgL_tmpz00_3106;

							BgL_tmpz00_3106 = (int) (1L);
							BgL_tmpz00_2481 = BGL_MVALUES_VAL(BgL_tmpz00_3106);
						}
						{	/* Reduce/1occ.scm 362 */
							int BgL_tmpz00_3109;

							BgL_tmpz00_3109 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3109, BUNSPEC);
						}
						BgL_nexitz00_2480 = BgL_tmpz00_2481;
					}
					((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2391)))->
							BgL_exitz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nexitz00_2480)),
						BUNSPEC);
					{	/* Reduce/1occ.scm 363 */
						obj_t BgL_1zd2expza2z72z02_2482;

						if (CBOOL(BgL_resetz00_2479))
							{	/* Reduce/1occ.scm 363 */
								BgL_1zd2expza2z72z02_2482 = BNIL;
							}
						else
							{	/* Reduce/1occ.scm 363 */
								BgL_1zd2expza2z72z02_2482 = BgL_1zd2expza2z70_2392;
							}
						{	/* Reduce/1occ.scm 364 */
							obj_t BgL_resetz72z72_2483;

							BgL_resetz72z72_2483 =
								BGl_nodezd21occz12zc0zzreduce_1occz00(
								(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
											((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2391)))->
									BgL_valuez00), BgL_1zd2expza2z72z02_2482);
							{	/* Reduce/1occ.scm 365 */
								obj_t BgL_nvaluez00_2484;

								{	/* Reduce/1occ.scm 366 */
									obj_t BgL_tmpz00_2485;

									{	/* Reduce/1occ.scm 366 */
										int BgL_tmpz00_3120;

										BgL_tmpz00_3120 = (int) (1L);
										BgL_tmpz00_2485 = BGL_MVALUES_VAL(BgL_tmpz00_3120);
									}
									{	/* Reduce/1occ.scm 366 */
										int BgL_tmpz00_3123;

										BgL_tmpz00_3123 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_3123, BUNSPEC);
									}
									BgL_nvaluez00_2484 = BgL_tmpz00_2485;
								}
								((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
												((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2391)))->
										BgL_valuez00) =
									((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nvaluez00_2484)),
									BUNSPEC);
								{	/* Reduce/1occ.scm 367 */
									obj_t BgL_val0_1305z00_2486;

									if (CBOOL(BgL_resetz00_2479))
										{	/* Reduce/1occ.scm 367 */
											BgL_val0_1305z00_2486 = BgL_resetz00_2479;
										}
									else
										{	/* Reduce/1occ.scm 367 */
											BgL_val0_1305z00_2486 = BgL_resetz72z72_2483;
										}
									{	/* Reduce/1occ.scm 367 */
										int BgL_tmpz00_3131;

										BgL_tmpz00_3131 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3131);
									}
									{	/* Reduce/1occ.scm 367 */
										obj_t BgL_auxz00_3136;
										int BgL_tmpz00_3134;

										BgL_auxz00_3136 =
											((obj_t) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2391));
										BgL_tmpz00_3134 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_3134, BgL_auxz00_3136);
									}
									return BgL_val0_1305z00_2486;
								}
							}
						}
					}
				}
			}
		}

	}



/* &node-1occ!-set-ex-it1358 */
	obj_t BGl_z62nodezd21occz12zd2setzd2exzd2it1358z70zzreduce_1occz00(obj_t
		BgL_envz00_2393, obj_t BgL_nodez00_2394, obj_t BgL_1zd2expza2z70_2395)
	{
		{	/* Reduce/1occ.scm 345 */
			{	/* Reduce/1occ.scm 347 */
				obj_t BgL_resetz00_2488;

				BgL_resetz00_2488 =
					BGl_nodezd21occz12zc0zzreduce_1occz00(
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2394)))->BgL_bodyz00),
					BgL_1zd2expza2z70_2395);
				{	/* Reduce/1occ.scm 348 */
					obj_t BgL_nbodyz00_2489;

					{	/* Reduce/1occ.scm 349 */
						obj_t BgL_tmpz00_2490;

						{	/* Reduce/1occ.scm 349 */
							int BgL_tmpz00_3143;

							BgL_tmpz00_3143 = (int) (1L);
							BgL_tmpz00_2490 = BGL_MVALUES_VAL(BgL_tmpz00_3143);
						}
						{	/* Reduce/1occ.scm 349 */
							int BgL_tmpz00_3146;

							BgL_tmpz00_3146 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3146, BUNSPEC);
						}
						BgL_nbodyz00_2489 = BgL_tmpz00_2490;
					}
					((((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2394)))->
							BgL_bodyz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nbodyz00_2489)),
						BUNSPEC);
					{	/* Reduce/1occ.scm 350 */
						obj_t BgL_resetz72z72_2491;

						BgL_resetz72z72_2491 =
							BGl_nodezd21occz12zc0zzreduce_1occz00(
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
										((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2394)))->
								BgL_onexitz00), BgL_1zd2expza2z70_2395);
						{	/* Reduce/1occ.scm 351 */
							obj_t BgL_nonexitz00_2492;

							{	/* Reduce/1occ.scm 352 */
								obj_t BgL_tmpz00_2493;

								{	/* Reduce/1occ.scm 352 */
									int BgL_tmpz00_3155;

									BgL_tmpz00_3155 = (int) (1L);
									BgL_tmpz00_2493 = BGL_MVALUES_VAL(BgL_tmpz00_3155);
								}
								{	/* Reduce/1occ.scm 352 */
									int BgL_tmpz00_3158;

									BgL_tmpz00_3158 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3158, BUNSPEC);
								}
								BgL_nonexitz00_2492 = BgL_tmpz00_2493;
							}
							((((BgL_setzd2exzd2itz00_bglt) COBJECT(
											((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2394)))->
									BgL_onexitz00) =
								((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nonexitz00_2492)),
								BUNSPEC);
							{	/* Reduce/1occ.scm 353 */
								obj_t BgL_val0_1303z00_2494;

								if (CBOOL(BgL_resetz00_2488))
									{	/* Reduce/1occ.scm 353 */
										BgL_val0_1303z00_2494 = BgL_resetz00_2488;
									}
								else
									{	/* Reduce/1occ.scm 353 */
										BgL_val0_1303z00_2494 = BgL_resetz72z72_2491;
									}
								{	/* Reduce/1occ.scm 353 */
									int BgL_tmpz00_3166;

									BgL_tmpz00_3166 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3166);
								}
								{	/* Reduce/1occ.scm 353 */
									obj_t BgL_auxz00_3171;
									int BgL_tmpz00_3169;

									BgL_auxz00_3171 =
										((obj_t) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2394));
									BgL_tmpz00_3169 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3169, BgL_auxz00_3171);
								}
								return BgL_val0_1303z00_2494;
							}
						}
					}
				}
			}
		}

	}



/* &node-1occ!-let-var1356 */
	obj_t BGl_z62nodezd21occz12zd2letzd2var1356za2zzreduce_1occz00(obj_t
		BgL_envz00_2396, obj_t BgL_nodez00_2397, obj_t BgL_1zd2expza2z70_2398)
	{
		{	/* Reduce/1occ.scm 253 */
			{	/* Reduce/1occ.scm 259 */
				bool_t BgL_test2011z00_3175;

				{	/* Reduce/1occ.scm 259 */
					bool_t BgL_test2012z00_3176;

					{	/* Reduce/1occ.scm 259 */
						obj_t BgL_tmpz00_3177;

						BgL_tmpz00_3177 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_2397)))->
							BgL_bindingsz00);
						BgL_test2012z00_3176 = PAIRP(BgL_tmpz00_3177);
					}
					if (BgL_test2012z00_3176)
						{	/* Reduce/1occ.scm 259 */
							if (NULLP(CDR(
										(((BgL_letzd2varzd2_bglt) COBJECT(
													((BgL_letzd2varzd2_bglt) BgL_nodez00_2397)))->
											BgL_bindingsz00))))
								{	/* Reduce/1occ.scm 260 */
									if (
										(BBOOL(
												(((BgL_letzd2varzd2_bglt) COBJECT(
															((BgL_letzd2varzd2_bglt) BgL_nodez00_2397)))->
													BgL_removablezf3zf3)) == CNST_TABLE_REF(2)))
										{	/* Reduce/1occ.scm 261 */
											BgL_test2011z00_3175 = ((bool_t) 0);
										}
									else
										{	/* Reduce/1occ.scm 262 */
											obj_t BgL_varz00_2496;

											{	/* Reduce/1occ.scm 262 */
												obj_t BgL_pairz00_2497;

												BgL_pairz00_2497 =
													(((BgL_letzd2varzd2_bglt) COBJECT(
															((BgL_letzd2varzd2_bglt) BgL_nodez00_2397)))->
													BgL_bindingsz00);
												BgL_varz00_2496 = CAR(CAR(BgL_pairz00_2497));
											}
											if (
												((((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_varz00_2496))))->
														BgL_occurrencez00) == 1L))
												{	/* Reduce/1occ.scm 264 */
													bool_t BgL_test2016z00_3201;

													{	/* Reduce/1occ.scm 264 */
														BgL_typez00_bglt BgL_arg1799z00_2498;

														BgL_arg1799z00_2498 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt) BgL_varz00_2496)))->
															BgL_typez00);
														BgL_test2016z00_3201 =
															(((obj_t) BgL_arg1799z00_2498) ==
															BGl_za2boolza2z00zztype_cachez00);
													}
													if (BgL_test2016z00_3201)
														{	/* Reduce/1occ.scm 265 */
															bool_t BgL_test2017z00_3206;

															{	/* Reduce/1occ.scm 265 */
																BgL_nodez00_bglt BgL_arg1798z00_2499;

																BgL_arg1798z00_2499 =
																	(((BgL_letzd2varzd2_bglt) COBJECT(
																			((BgL_letzd2varzd2_bglt)
																				BgL_nodez00_2397)))->BgL_bodyz00);
																{	/* Reduce/1occ.scm 265 */
																	obj_t BgL_classz00_2500;

																	BgL_classz00_2500 =
																		BGl_conditionalz00zzast_nodez00;
																	{	/* Reduce/1occ.scm 265 */
																		BgL_objectz00_bglt BgL_arg1807z00_2501;

																		{	/* Reduce/1occ.scm 265 */
																			obj_t BgL_tmpz00_3209;

																			BgL_tmpz00_3209 =
																				((obj_t)
																				((BgL_objectz00_bglt)
																					BgL_arg1798z00_2499));
																			BgL_arg1807z00_2501 =
																				(BgL_objectz00_bglt) (BgL_tmpz00_3209);
																		}
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Reduce/1occ.scm 265 */
																				long BgL_idxz00_2502;

																				BgL_idxz00_2502 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2501);
																				BgL_test2017z00_3206 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2502 + 3L)) ==
																					BgL_classz00_2500);
																			}
																		else
																			{	/* Reduce/1occ.scm 265 */
																				bool_t BgL_res1908z00_2505;

																				{	/* Reduce/1occ.scm 265 */
																					obj_t BgL_oclassz00_2506;

																					{	/* Reduce/1occ.scm 265 */
																						obj_t BgL_arg1815z00_2507;
																						long BgL_arg1816z00_2508;

																						BgL_arg1815z00_2507 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Reduce/1occ.scm 265 */
																							long BgL_arg1817z00_2509;

																							BgL_arg1817z00_2509 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2501);
																							BgL_arg1816z00_2508 =
																								(BgL_arg1817z00_2509 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2506 =
																							VECTOR_REF(BgL_arg1815z00_2507,
																							BgL_arg1816z00_2508);
																					}
																					{	/* Reduce/1occ.scm 265 */
																						bool_t BgL__ortest_1115z00_2510;

																						BgL__ortest_1115z00_2510 =
																							(BgL_classz00_2500 ==
																							BgL_oclassz00_2506);
																						if (BgL__ortest_1115z00_2510)
																							{	/* Reduce/1occ.scm 265 */
																								BgL_res1908z00_2505 =
																									BgL__ortest_1115z00_2510;
																							}
																						else
																							{	/* Reduce/1occ.scm 265 */
																								long BgL_odepthz00_2511;

																								{	/* Reduce/1occ.scm 265 */
																									obj_t BgL_arg1804z00_2512;

																									BgL_arg1804z00_2512 =
																										(BgL_oclassz00_2506);
																									BgL_odepthz00_2511 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2512);
																								}
																								if ((3L < BgL_odepthz00_2511))
																									{	/* Reduce/1occ.scm 265 */
																										obj_t BgL_arg1802z00_2513;

																										{	/* Reduce/1occ.scm 265 */
																											obj_t BgL_arg1803z00_2514;

																											BgL_arg1803z00_2514 =
																												(BgL_oclassz00_2506);
																											BgL_arg1802z00_2513 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2514,
																												3L);
																										}
																										BgL_res1908z00_2505 =
																											(BgL_arg1802z00_2513 ==
																											BgL_classz00_2500);
																									}
																								else
																									{	/* Reduce/1occ.scm 265 */
																										BgL_res1908z00_2505 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2017z00_3206 =
																					BgL_res1908z00_2505;
																			}
																	}
																}
															}
															if (BgL_test2017z00_3206)
																{	/* Reduce/1occ.scm 266 */
																	BgL_conditionalz00_bglt BgL_i1131z00_2515;

																	BgL_i1131z00_2515 =
																		((BgL_conditionalz00_bglt)
																		(((BgL_letzd2varzd2_bglt) COBJECT(
																					((BgL_letzd2varzd2_bglt)
																						BgL_nodez00_2397)))->BgL_bodyz00));
																	{	/* Reduce/1occ.scm 267 */
																		bool_t BgL_test2021z00_3235;

																		{	/* Reduce/1occ.scm 267 */
																			BgL_nodez00_bglt BgL_arg1775z00_2516;

																			BgL_arg1775z00_2516 =
																				(((BgL_conditionalz00_bglt)
																					COBJECT(BgL_i1131z00_2515))->
																				BgL_testz00);
																			{	/* Reduce/1occ.scm 267 */
																				obj_t BgL_classz00_2517;

																				BgL_classz00_2517 =
																					BGl_varz00zzast_nodez00;
																				{	/* Reduce/1occ.scm 267 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_2518;
																					{	/* Reduce/1occ.scm 267 */
																						obj_t BgL_tmpz00_3237;

																						BgL_tmpz00_3237 =
																							((obj_t)
																							((BgL_objectz00_bglt)
																								BgL_arg1775z00_2516));
																						BgL_arg1807z00_2518 =
																							(BgL_objectz00_bglt)
																							(BgL_tmpz00_3237);
																					}
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Reduce/1occ.scm 267 */
																							long BgL_idxz00_2519;

																							BgL_idxz00_2519 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_2518);
																							BgL_test2021z00_3235 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_2519 + 2L)) ==
																								BgL_classz00_2517);
																						}
																					else
																						{	/* Reduce/1occ.scm 267 */
																							bool_t BgL_res1909z00_2522;

																							{	/* Reduce/1occ.scm 267 */
																								obj_t BgL_oclassz00_2523;

																								{	/* Reduce/1occ.scm 267 */
																									obj_t BgL_arg1815z00_2524;
																									long BgL_arg1816z00_2525;

																									BgL_arg1815z00_2524 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Reduce/1occ.scm 267 */
																										long BgL_arg1817z00_2526;

																										BgL_arg1817z00_2526 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_2518);
																										BgL_arg1816z00_2525 =
																											(BgL_arg1817z00_2526 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_2523 =
																										VECTOR_REF
																										(BgL_arg1815z00_2524,
																										BgL_arg1816z00_2525);
																								}
																								{	/* Reduce/1occ.scm 267 */
																									bool_t
																										BgL__ortest_1115z00_2527;
																									BgL__ortest_1115z00_2527 =
																										(BgL_classz00_2517 ==
																										BgL_oclassz00_2523);
																									if (BgL__ortest_1115z00_2527)
																										{	/* Reduce/1occ.scm 267 */
																											BgL_res1909z00_2522 =
																												BgL__ortest_1115z00_2527;
																										}
																									else
																										{	/* Reduce/1occ.scm 267 */
																											long BgL_odepthz00_2528;

																											{	/* Reduce/1occ.scm 267 */
																												obj_t
																													BgL_arg1804z00_2529;
																												BgL_arg1804z00_2529 =
																													(BgL_oclassz00_2523);
																												BgL_odepthz00_2528 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_2529);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_2528))
																												{	/* Reduce/1occ.scm 267 */
																													obj_t
																														BgL_arg1802z00_2530;
																													{	/* Reduce/1occ.scm 267 */
																														obj_t
																															BgL_arg1803z00_2531;
																														BgL_arg1803z00_2531
																															=
																															(BgL_oclassz00_2523);
																														BgL_arg1802z00_2530
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_2531,
																															2L);
																													}
																													BgL_res1909z00_2522 =
																														(BgL_arg1802z00_2530
																														==
																														BgL_classz00_2517);
																												}
																											else
																												{	/* Reduce/1occ.scm 267 */
																													BgL_res1909z00_2522 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test2021z00_3235 =
																								BgL_res1909z00_2522;
																						}
																				}
																			}
																		}
																		if (BgL_test2021z00_3235)
																			{	/* Reduce/1occ.scm 267 */
																				BgL_test2011z00_3175 =
																					(
																					((obj_t)
																						(((BgL_varz00_bglt) COBJECT(
																									((BgL_varz00_bglt)
																										(((BgL_conditionalz00_bglt)
																												COBJECT
																												(BgL_i1131z00_2515))->
																											BgL_testz00))))->
																							BgL_variablez00)) ==
																					BgL_varz00_2496);
																			}
																		else
																			{	/* Reduce/1occ.scm 267 */
																				BgL_test2011z00_3175 = ((bool_t) 0);
																			}
																	}
																}
															else
																{	/* Reduce/1occ.scm 265 */
																	BgL_test2011z00_3175 = ((bool_t) 0);
																}
														}
													else
														{	/* Reduce/1occ.scm 264 */
															BgL_test2011z00_3175 = ((bool_t) 0);
														}
												}
											else
												{	/* Reduce/1occ.scm 263 */
													BgL_test2011z00_3175 = ((bool_t) 0);
												}
										}
								}
							else
								{	/* Reduce/1occ.scm 260 */
									BgL_test2011z00_3175 = ((bool_t) 0);
								}
						}
					else
						{	/* Reduce/1occ.scm 259 */
							BgL_test2011z00_3175 = ((bool_t) 0);
						}
				}
				if (BgL_test2011z00_3175)
					{	/* Reduce/1occ.scm 270 */
						BgL_conditionalz00_bglt BgL_i1133z00_2532;

						BgL_i1133z00_2532 =
							((BgL_conditionalz00_bglt)
							(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_nodez00_2397)))->BgL_bodyz00));
						{
							BgL_nodez00_bglt BgL_auxz00_3268;

							{	/* Reduce/1occ.scm 271 */
								obj_t BgL_pairz00_2533;

								BgL_pairz00_2533 =
									(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_nodez00_2397)))->
									BgL_bindingsz00);
								{	/* Reduce/1occ.scm 271 */
									obj_t BgL_pairz00_2534;

									BgL_pairz00_2534 = CAR(BgL_pairz00_2533);
									BgL_auxz00_3268 = ((BgL_nodez00_bglt) CDR(BgL_pairz00_2534));
								}
							}
							((((BgL_conditionalz00_bglt) COBJECT(BgL_i1133z00_2532))->
									BgL_testz00) = ((BgL_nodez00_bglt) BgL_auxz00_3268), BUNSPEC);
						}
						((((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_nodez00_2397)))->
								BgL_bindingsz00) = ((obj_t) BNIL), BUNSPEC);
						return
							BGl_nodezd21occz12zc0zzreduce_1occz00((((BgL_letzd2varzd2_bglt)
									COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_2397)))->
								BgL_bodyz00), BgL_1zd2expza2z70_2398);
					}
				else
					{	/* Reduce/1occ.scm 259 */
						return
							BGl_nodezd21occzd2letzd2varz12zc0zzreduce_1occz00(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2397),
							BgL_1zd2expza2z70_2398);
					}
			}
		}

	}



/* &node-1occ!-let-fun1354 */
	obj_t BGl_z62nodezd21occz12zd2letzd2fun1354za2zzreduce_1occz00(obj_t
		BgL_envz00_2399, obj_t BgL_nodez00_2400, obj_t BgL_1zd2expza2z70_2401)
	{
		{	/* Reduce/1occ.scm 234 */
			{	/* Reduce/1occ.scm 236 */
				obj_t BgL_resetz00_2536;

				BgL_resetz00_2536 =
					BGl_nodezd21occz12zc0zzreduce_1occz00(
					(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2400)))->BgL_bodyz00),
					BgL_1zd2expza2z70_2401);
				{	/* Reduce/1occ.scm 237 */
					obj_t BgL_nbodyz00_2537;

					{	/* Reduce/1occ.scm 238 */
						obj_t BgL_tmpz00_2538;

						{	/* Reduce/1occ.scm 238 */
							int BgL_tmpz00_3285;

							BgL_tmpz00_3285 = (int) (1L);
							BgL_tmpz00_2538 = BGL_MVALUES_VAL(BgL_tmpz00_3285);
						}
						{	/* Reduce/1occ.scm 238 */
							int BgL_tmpz00_3288;

							BgL_tmpz00_3288 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3288, BUNSPEC);
						}
						BgL_nbodyz00_2537 = BgL_tmpz00_2538;
					}
					((((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nodez00_2400)))->BgL_bodyz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nbodyz00_2537)),
						BUNSPEC);
					{
						obj_t BgL_localsz00_2540;
						obj_t BgL_resetz00_2541;

						BgL_localsz00_2540 =
							(((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nodez00_2400)))->BgL_localsz00);
						BgL_resetz00_2541 = BgL_resetz00_2536;
					BgL_loopz00_2539:
						if (NULLP(BgL_localsz00_2540))
							{	/* Reduce/1occ.scm 241 */
								{	/* Reduce/1occ.scm 242 */
									int BgL_tmpz00_3296;

									BgL_tmpz00_3296 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3296);
								}
								{	/* Reduce/1occ.scm 242 */
									obj_t BgL_auxz00_3301;
									int BgL_tmpz00_3299;

									BgL_auxz00_3301 =
										((obj_t) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2400));
									BgL_tmpz00_3299 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3299, BgL_auxz00_3301);
								}
								return BgL_resetz00_2541;
							}
						else
							{	/* Reduce/1occ.scm 243 */
								obj_t BgL_localz00_2542;

								BgL_localz00_2542 = CAR(((obj_t) BgL_localsz00_2540));
								{	/* Reduce/1occ.scm 243 */
									BgL_valuez00_bglt BgL_sfunz00_2543;

									BgL_sfunz00_2543 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2542))))->
										BgL_valuez00);
									{	/* Reduce/1occ.scm 244 */

										{	/* Reduce/1occ.scm 245 */
											obj_t BgL_resetz72z72_2544;

											{	/* Reduce/1occ.scm 246 */
												obj_t BgL_arg1735z00_2545;

												BgL_arg1735z00_2545 =
													(((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_sfunz00_2543)))->
													BgL_bodyz00);
												BgL_resetz72z72_2544 =
													BGl_nodezd21occz12zc0zzreduce_1occz00((
														(BgL_nodez00_bglt) BgL_arg1735z00_2545), BNIL);
											}
											{	/* Reduce/1occ.scm 246 */
												obj_t BgL_nbodyz00_2546;

												{	/* Reduce/1occ.scm 247 */
													obj_t BgL_tmpz00_2547;

													{	/* Reduce/1occ.scm 247 */
														int BgL_tmpz00_3314;

														BgL_tmpz00_3314 = (int) (1L);
														BgL_tmpz00_2547 = BGL_MVALUES_VAL(BgL_tmpz00_3314);
													}
													{	/* Reduce/1occ.scm 247 */
														int BgL_tmpz00_3317;

														BgL_tmpz00_3317 = (int) (1L);
														BGL_MVALUES_VAL_SET(BgL_tmpz00_3317, BUNSPEC);
													}
													BgL_nbodyz00_2546 = BgL_tmpz00_2547;
												}
												((((BgL_sfunz00_bglt) COBJECT(
																((BgL_sfunz00_bglt) BgL_sfunz00_2543)))->
														BgL_bodyz00) =
													((obj_t) BgL_nbodyz00_2546), BUNSPEC);
												{	/* Reduce/1occ.scm 248 */
													obj_t BgL_arg1733z00_2548;
													obj_t BgL_arg1734z00_2549;

													BgL_arg1733z00_2548 =
														CDR(((obj_t) BgL_localsz00_2540));
													if (CBOOL(BgL_resetz00_2541))
														{	/* Reduce/1occ.scm 248 */
															BgL_arg1734z00_2549 = BgL_resetz00_2541;
														}
													else
														{	/* Reduce/1occ.scm 248 */
															BgL_arg1734z00_2549 = BgL_resetz72z72_2544;
														}
													{
														obj_t BgL_resetz00_3327;
														obj_t BgL_localsz00_3326;

														BgL_localsz00_3326 = BgL_arg1733z00_2548;
														BgL_resetz00_3327 = BgL_arg1734z00_2549;
														BgL_resetz00_2541 = BgL_resetz00_3327;
														BgL_localsz00_2540 = BgL_localsz00_3326;
														goto BgL_loopz00_2539;
													}
												}
											}
										}
									}
								}
							}
					}
				}
			}
		}

	}



/* &node-1occ!-switch1352 */
	obj_t BGl_z62nodezd21occz12zd2switch1352z70zzreduce_1occz00(obj_t
		BgL_envz00_2402, obj_t BgL_nodez00_2403, obj_t BgL_1zd2expza2z70_2404)
	{
		{	/* Reduce/1occ.scm 215 */
			{	/* Reduce/1occ.scm 217 */
				obj_t BgL_resetz00_2551;

				BgL_resetz00_2551 =
					BGl_nodezd21occz12zc0zzreduce_1occz00(
					(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2403)))->BgL_testz00),
					BgL_1zd2expza2z70_2404);
				{	/* Reduce/1occ.scm 218 */
					obj_t BgL_ntestz00_2552;

					{	/* Reduce/1occ.scm 219 */
						obj_t BgL_tmpz00_2553;

						{	/* Reduce/1occ.scm 219 */
							int BgL_tmpz00_3333;

							BgL_tmpz00_3333 = (int) (1L);
							BgL_tmpz00_2553 = BGL_MVALUES_VAL(BgL_tmpz00_3333);
						}
						{	/* Reduce/1occ.scm 219 */
							int BgL_tmpz00_3336;

							BgL_tmpz00_3336 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3336, BUNSPEC);
						}
						BgL_ntestz00_2552 = BgL_tmpz00_2553;
					}
					((((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_2403)))->BgL_testz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_ntestz00_2552)),
						BUNSPEC);
					{
						obj_t BgL_clausesz00_2555;
						obj_t BgL_resetz00_2556;

						BgL_clausesz00_2555 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_2403)))->BgL_clausesz00);
						BgL_resetz00_2556 = BgL_resetz00_2551;
					BgL_loopz00_2554:
						if (NULLP(BgL_clausesz00_2555))
							{	/* Reduce/1occ.scm 223 */
								{	/* Reduce/1occ.scm 224 */
									int BgL_tmpz00_3344;

									BgL_tmpz00_3344 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3344);
								}
								{	/* Reduce/1occ.scm 224 */
									obj_t BgL_auxz00_3349;
									int BgL_tmpz00_3347;

									BgL_auxz00_3349 =
										((obj_t) ((BgL_switchz00_bglt) BgL_nodez00_2403));
									BgL_tmpz00_3347 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3347, BgL_auxz00_3349);
								}
								return BgL_resetz00_2556;
							}
						else
							{	/* Reduce/1occ.scm 225 */
								obj_t BgL_clausez00_2557;

								BgL_clausez00_2557 = CAR(((obj_t) BgL_clausesz00_2555));
								{	/* Reduce/1occ.scm 226 */
									obj_t BgL_resetz72z72_2558;

									{	/* Reduce/1occ.scm 227 */
										obj_t BgL_arg1718z00_2559;

										BgL_arg1718z00_2559 = CDR(((obj_t) BgL_clausez00_2557));
										BgL_resetz72z72_2558 =
											BGl_nodezd21occz12zc0zzreduce_1occz00(
											((BgL_nodez00_bglt) BgL_arg1718z00_2559),
											BgL_1zd2expza2z70_2404);
									}
									{	/* Reduce/1occ.scm 227 */
										obj_t BgL_nclausez00_2560;

										{	/* Reduce/1occ.scm 228 */
											obj_t BgL_tmpz00_2561;

											{	/* Reduce/1occ.scm 228 */
												int BgL_tmpz00_3359;

												BgL_tmpz00_3359 = (int) (1L);
												BgL_tmpz00_2561 = BGL_MVALUES_VAL(BgL_tmpz00_3359);
											}
											{	/* Reduce/1occ.scm 228 */
												int BgL_tmpz00_3362;

												BgL_tmpz00_3362 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_3362, BUNSPEC);
											}
											BgL_nclausez00_2560 = BgL_tmpz00_2561;
										}
										{	/* Reduce/1occ.scm 228 */
											obj_t BgL_tmpz00_3365;

											BgL_tmpz00_3365 = ((obj_t) BgL_clausez00_2557);
											SET_CDR(BgL_tmpz00_3365, BgL_nclausez00_2560);
										}
										{	/* Reduce/1occ.scm 229 */
											obj_t BgL_arg1714z00_2562;
											obj_t BgL_arg1717z00_2563;

											BgL_arg1714z00_2562 = CDR(((obj_t) BgL_clausesz00_2555));
											if (CBOOL(BgL_resetz00_2556))
												{	/* Reduce/1occ.scm 229 */
													BgL_arg1717z00_2563 = BgL_resetz00_2556;
												}
											else
												{	/* Reduce/1occ.scm 229 */
													BgL_arg1717z00_2563 = BgL_resetz72z72_2558;
												}
											{
												obj_t BgL_resetz00_3373;
												obj_t BgL_clausesz00_3372;

												BgL_clausesz00_3372 = BgL_arg1714z00_2562;
												BgL_resetz00_3373 = BgL_arg1717z00_2563;
												BgL_resetz00_2556 = BgL_resetz00_3373;
												BgL_clausesz00_2555 = BgL_clausesz00_3372;
												goto BgL_loopz00_2554;
											}
										}
									}
								}
							}
					}
				}
			}
		}

	}



/* &node-1occ!-fail1350 */
	obj_t BGl_z62nodezd21occz12zd2fail1350z70zzreduce_1occz00(obj_t
		BgL_envz00_2405, obj_t BgL_nodez00_2406, obj_t BgL_1zd2expza2z70_2407)
	{
		{	/* Reduce/1occ.scm 197 */
			{	/* Reduce/1occ.scm 199 */
				obj_t BgL_resetz00_2565;

				BgL_resetz00_2565 =
					BGl_nodezd21occz12zc0zzreduce_1occz00(
					(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2406)))->BgL_procz00),
					BgL_1zd2expza2z70_2407);
				{	/* Reduce/1occ.scm 200 */
					obj_t BgL_nprocz00_2566;

					{	/* Reduce/1occ.scm 201 */
						obj_t BgL_tmpz00_2567;

						{	/* Reduce/1occ.scm 201 */
							int BgL_tmpz00_3379;

							BgL_tmpz00_3379 = (int) (1L);
							BgL_tmpz00_2567 = BGL_MVALUES_VAL(BgL_tmpz00_3379);
						}
						{	/* Reduce/1occ.scm 201 */
							int BgL_tmpz00_3382;

							BgL_tmpz00_3382 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3382, BUNSPEC);
						}
						BgL_nprocz00_2566 = BgL_tmpz00_2567;
					}
					((((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_2406)))->BgL_procz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nprocz00_2566)),
						BUNSPEC);
					{	/* Reduce/1occ.scm 202 */
						obj_t BgL_1zd2expza2z72z02_2568;

						if (CBOOL(BgL_resetz00_2565))
							{	/* Reduce/1occ.scm 202 */
								BgL_1zd2expza2z72z02_2568 = BNIL;
							}
						else
							{	/* Reduce/1occ.scm 202 */
								BgL_1zd2expza2z72z02_2568 = BgL_1zd2expza2z70_2407;
							}
						{	/* Reduce/1occ.scm 203 */
							obj_t BgL_resetz72z72_2569;

							BgL_resetz72z72_2569 =
								BGl_nodezd21occz12zc0zzreduce_1occz00(
								(((BgL_failz00_bglt) COBJECT(
											((BgL_failz00_bglt) BgL_nodez00_2406)))->BgL_msgz00),
								BgL_1zd2expza2z72z02_2568);
							{	/* Reduce/1occ.scm 204 */
								obj_t BgL_nmsgz00_2570;

								{	/* Reduce/1occ.scm 205 */
									obj_t BgL_tmpz00_2571;

									{	/* Reduce/1occ.scm 205 */
										int BgL_tmpz00_3393;

										BgL_tmpz00_3393 = (int) (1L);
										BgL_tmpz00_2571 = BGL_MVALUES_VAL(BgL_tmpz00_3393);
									}
									{	/* Reduce/1occ.scm 205 */
										int BgL_tmpz00_3396;

										BgL_tmpz00_3396 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_3396, BUNSPEC);
									}
									BgL_nmsgz00_2570 = BgL_tmpz00_2571;
								}
								((((BgL_failz00_bglt) COBJECT(
												((BgL_failz00_bglt) BgL_nodez00_2406)))->BgL_msgz00) =
									((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nmsgz00_2570)),
									BUNSPEC);
								{	/* Reduce/1occ.scm 206 */
									obj_t BgL_1zd2expza2z72z72z70_2572;

									if (CBOOL(BgL_resetz72z72_2569))
										{	/* Reduce/1occ.scm 206 */
											BgL_1zd2expza2z72z72z70_2572 = BNIL;
										}
									else
										{	/* Reduce/1occ.scm 206 */
											BgL_1zd2expza2z72z72z70_2572 = BgL_1zd2expza2z72z02_2568;
										}
									{	/* Reduce/1occ.scm 207 */
										obj_t BgL_resetz72z72z00_2573;

										BgL_resetz72z72z00_2573 =
											BGl_nodezd21occz12zc0zzreduce_1occz00(
											(((BgL_failz00_bglt) COBJECT(
														((BgL_failz00_bglt) BgL_nodez00_2406)))->
												BgL_objz00), BgL_1zd2expza2z72z72z70_2572);
										{	/* Reduce/1occ.scm 208 */
											obj_t BgL_nobjz00_2574;

											{	/* Reduce/1occ.scm 209 */
												obj_t BgL_tmpz00_2575;

												{	/* Reduce/1occ.scm 209 */
													int BgL_tmpz00_3407;

													BgL_tmpz00_3407 = (int) (1L);
													BgL_tmpz00_2575 = BGL_MVALUES_VAL(BgL_tmpz00_3407);
												}
												{	/* Reduce/1occ.scm 209 */
													int BgL_tmpz00_3410;

													BgL_tmpz00_3410 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_3410, BUNSPEC);
												}
												BgL_nobjz00_2574 = BgL_tmpz00_2575;
											}
											((((BgL_failz00_bglt) COBJECT(
															((BgL_failz00_bglt) BgL_nodez00_2406)))->
													BgL_objz00) =
												((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
														BgL_nobjz00_2574)), BUNSPEC);
											{	/* Reduce/1occ.scm 210 */
												obj_t BgL_val0_1291z00_2576;

												if (CBOOL(BgL_resetz00_2565))
													{	/* Reduce/1occ.scm 210 */
														BgL_val0_1291z00_2576 = BgL_resetz00_2565;
													}
												else
													{	/* Reduce/1occ.scm 210 */
														if (CBOOL(BgL_resetz72z72_2569))
															{	/* Reduce/1occ.scm 210 */
																BgL_val0_1291z00_2576 = BgL_resetz72z72_2569;
															}
														else
															{	/* Reduce/1occ.scm 210 */
																BgL_val0_1291z00_2576 = BgL_resetz72z72z00_2573;
															}
													}
												{	/* Reduce/1occ.scm 210 */
													int BgL_tmpz00_3420;

													BgL_tmpz00_3420 = (int) (2L);
													BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3420);
												}
												{	/* Reduce/1occ.scm 210 */
													obj_t BgL_auxz00_3425;
													int BgL_tmpz00_3423;

													BgL_auxz00_3425 =
														((obj_t) ((BgL_failz00_bglt) BgL_nodez00_2406));
													BgL_tmpz00_3423 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_3423, BgL_auxz00_3425);
												}
												return BgL_val0_1291z00_2576;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &node-1occ!-condition1348 */
	obj_t BGl_z62nodezd21occz12zd2condition1348z70zzreduce_1occz00(obj_t
		BgL_envz00_2408, obj_t BgL_nodez00_2409, obj_t BgL_1zd2expza2z70_2410)
	{
		{	/* Reduce/1occ.scm 180 */
			{	/* Reduce/1occ.scm 182 */
				obj_t BgL_resetz00_2578;

				BgL_resetz00_2578 =
					BGl_nodezd21occz12zc0zzreduce_1occz00(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2409)))->BgL_testz00),
					BgL_1zd2expza2z70_2410);
				{	/* Reduce/1occ.scm 183 */
					obj_t BgL_ntestz00_2579;

					{	/* Reduce/1occ.scm 184 */
						obj_t BgL_tmpz00_2580;

						{	/* Reduce/1occ.scm 184 */
							int BgL_tmpz00_3432;

							BgL_tmpz00_3432 = (int) (1L);
							BgL_tmpz00_2580 = BGL_MVALUES_VAL(BgL_tmpz00_3432);
						}
						{	/* Reduce/1occ.scm 184 */
							int BgL_tmpz00_3435;

							BgL_tmpz00_3435 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3435, BUNSPEC);
						}
						BgL_ntestz00_2579 = BgL_tmpz00_2580;
					}
					((((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_2409)))->BgL_testz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_ntestz00_2579)),
						BUNSPEC);
					{	/* Reduce/1occ.scm 185 */
						obj_t BgL_1zd2expza2z72z02_2581;

						if (CBOOL(BgL_resetz00_2578))
							{	/* Reduce/1occ.scm 185 */
								BgL_1zd2expza2z72z02_2581 = BNIL;
							}
						else
							{	/* Reduce/1occ.scm 185 */
								BgL_1zd2expza2z72z02_2581 = BgL_1zd2expza2z70_2410;
							}
						{	/* Reduce/1occ.scm 186 */
							obj_t BgL_resetz72z72_2582;

							BgL_resetz72z72_2582 =
								BGl_nodezd21occz12zc0zzreduce_1occz00(
								(((BgL_conditionalz00_bglt) COBJECT(
											((BgL_conditionalz00_bglt) BgL_nodez00_2409)))->
									BgL_truez00), BgL_1zd2expza2z72z02_2581);
							{	/* Reduce/1occ.scm 187 */
								obj_t BgL_ntruez00_2583;

								{	/* Reduce/1occ.scm 188 */
									obj_t BgL_tmpz00_2584;

									{	/* Reduce/1occ.scm 188 */
										int BgL_tmpz00_3446;

										BgL_tmpz00_3446 = (int) (1L);
										BgL_tmpz00_2584 = BGL_MVALUES_VAL(BgL_tmpz00_3446);
									}
									{	/* Reduce/1occ.scm 188 */
										int BgL_tmpz00_3449;

										BgL_tmpz00_3449 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_3449, BUNSPEC);
									}
									BgL_ntruez00_2583 = BgL_tmpz00_2584;
								}
								((((BgL_conditionalz00_bglt) COBJECT(
												((BgL_conditionalz00_bglt) BgL_nodez00_2409)))->
										BgL_truez00) =
									((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_ntruez00_2583)),
									BUNSPEC);
								{	/* Reduce/1occ.scm 189 */
									obj_t BgL_resetz72z72z00_2585;

									BgL_resetz72z72z00_2585 =
										BGl_nodezd21occz12zc0zzreduce_1occz00(
										(((BgL_conditionalz00_bglt) COBJECT(
													((BgL_conditionalz00_bglt) BgL_nodez00_2409)))->
											BgL_falsez00), BgL_1zd2expza2z72z02_2581);
									{	/* Reduce/1occ.scm 190 */
										obj_t BgL_nfalsez00_2586;

										{	/* Reduce/1occ.scm 191 */
											obj_t BgL_tmpz00_2587;

											{	/* Reduce/1occ.scm 191 */
												int BgL_tmpz00_3458;

												BgL_tmpz00_3458 = (int) (1L);
												BgL_tmpz00_2587 = BGL_MVALUES_VAL(BgL_tmpz00_3458);
											}
											{	/* Reduce/1occ.scm 191 */
												int BgL_tmpz00_3461;

												BgL_tmpz00_3461 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_3461, BUNSPEC);
											}
											BgL_nfalsez00_2586 = BgL_tmpz00_2587;
										}
										((((BgL_conditionalz00_bglt) COBJECT(
														((BgL_conditionalz00_bglt) BgL_nodez00_2409)))->
												BgL_falsez00) =
											((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
													BgL_nfalsez00_2586)), BUNSPEC);
										{	/* Reduce/1occ.scm 192 */
											obj_t BgL_val0_1289z00_2588;

											if (CBOOL(BgL_resetz00_2578))
												{	/* Reduce/1occ.scm 192 */
													BgL_val0_1289z00_2588 = BgL_resetz00_2578;
												}
											else
												{	/* Reduce/1occ.scm 192 */
													if (CBOOL(BgL_resetz72z72_2582))
														{	/* Reduce/1occ.scm 192 */
															BgL_val0_1289z00_2588 = BgL_resetz72z72_2582;
														}
													else
														{	/* Reduce/1occ.scm 192 */
															BgL_val0_1289z00_2588 = BgL_resetz72z72z00_2585;
														}
												}
											{	/* Reduce/1occ.scm 192 */
												int BgL_tmpz00_3471;

												BgL_tmpz00_3471 = (int) (2L);
												BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3471);
											}
											{	/* Reduce/1occ.scm 192 */
												obj_t BgL_auxz00_3476;
												int BgL_tmpz00_3474;

												BgL_auxz00_3476 =
													((obj_t)
													((BgL_conditionalz00_bglt) BgL_nodez00_2409));
												BgL_tmpz00_3474 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_3474, BgL_auxz00_3476);
											}
											return BgL_val0_1289z00_2588;
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &node-1occ!-setq1346 */
	obj_t BGl_z62nodezd21occz12zd2setq1346z70zzreduce_1occz00(obj_t
		BgL_envz00_2411, obj_t BgL_nodez00_2412, obj_t BgL_1zd2expza2z70_2413)
	{
		{	/* Reduce/1occ.scm 170 */
			{	/* Reduce/1occ.scm 172 */
				obj_t BgL_resetz00_2590;

				BgL_resetz00_2590 =
					BGl_nodezd21occz12zc0zzreduce_1occz00(
					(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2412)))->BgL_valuez00),
					BgL_1zd2expza2z70_2413);
				{	/* Reduce/1occ.scm 173 */
					obj_t BgL_nvaluez00_2591;

					{	/* Reduce/1occ.scm 174 */
						obj_t BgL_tmpz00_2592;

						{	/* Reduce/1occ.scm 174 */
							int BgL_tmpz00_3483;

							BgL_tmpz00_3483 = (int) (1L);
							BgL_tmpz00_2592 = BGL_MVALUES_VAL(BgL_tmpz00_3483);
						}
						{	/* Reduce/1occ.scm 174 */
							int BgL_tmpz00_3486;

							BgL_tmpz00_3486 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3486, BUNSPEC);
						}
						BgL_nvaluez00_2591 = BgL_tmpz00_2592;
					}
					((((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nodez00_2412)))->BgL_valuez00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nvaluez00_2591)),
						BUNSPEC);
					{	/* Reduce/1occ.scm 175 */
						int BgL_tmpz00_3492;

						BgL_tmpz00_3492 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3492);
					}
					{	/* Reduce/1occ.scm 175 */
						obj_t BgL_auxz00_3497;
						int BgL_tmpz00_3495;

						BgL_auxz00_3497 = ((obj_t) ((BgL_setqz00_bglt) BgL_nodez00_2412));
						BgL_tmpz00_3495 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_3495, BgL_auxz00_3497);
					}
					return BgL_resetz00_2590;
				}
			}
		}

	}



/* &node-1occ!-cast1344 */
	obj_t BGl_z62nodezd21occz12zd2cast1344z70zzreduce_1occz00(obj_t
		BgL_envz00_2414, obj_t BgL_nodez00_2415, obj_t BgL_1zd2expza2z70_2416)
	{
		{	/* Reduce/1occ.scm 160 */
			{	/* Reduce/1occ.scm 162 */
				obj_t BgL_resetz00_2594;

				BgL_resetz00_2594 =
					BGl_nodezd21occz12zc0zzreduce_1occz00(
					(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_2415)))->BgL_argz00),
					BgL_1zd2expza2z70_2416);
				{	/* Reduce/1occ.scm 163 */
					obj_t BgL_nargz00_2595;

					{	/* Reduce/1occ.scm 164 */
						obj_t BgL_tmpz00_2596;

						{	/* Reduce/1occ.scm 164 */
							int BgL_tmpz00_3504;

							BgL_tmpz00_3504 = (int) (1L);
							BgL_tmpz00_2596 = BGL_MVALUES_VAL(BgL_tmpz00_3504);
						}
						{	/* Reduce/1occ.scm 164 */
							int BgL_tmpz00_3507;

							BgL_tmpz00_3507 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3507, BUNSPEC);
						}
						BgL_nargz00_2595 = BgL_tmpz00_2596;
					}
					((((BgL_castz00_bglt) COBJECT(
									((BgL_castz00_bglt) BgL_nodez00_2415)))->BgL_argz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nargz00_2595)),
						BUNSPEC);
					{	/* Reduce/1occ.scm 165 */
						int BgL_tmpz00_3513;

						BgL_tmpz00_3513 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3513);
					}
					{	/* Reduce/1occ.scm 165 */
						obj_t BgL_auxz00_3518;
						int BgL_tmpz00_3516;

						BgL_auxz00_3518 = ((obj_t) ((BgL_castz00_bglt) BgL_nodez00_2415));
						BgL_tmpz00_3516 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_3516, BgL_auxz00_3518);
					}
					return BgL_resetz00_2594;
				}
			}
		}

	}



/* &node-1occ!-private1342 */
	obj_t BGl_z62nodezd21occz12zd2private1342z70zzreduce_1occz00(obj_t
		BgL_envz00_2417, obj_t BgL_nodez00_2418, obj_t BgL_1zd2expza2z70_2419)
	{
		{	/* Reduce/1occ.scm 153 */
			{	/* Reduce/1occ.scm 155 */
				obj_t BgL_val0_1283z00_2598;

				{	/* Reduce/1occ.scm 155 */
					obj_t BgL_arg1692z00_2599;

					BgL_arg1692z00_2599 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_privatez00_bglt) BgL_nodez00_2418))))->BgL_exprza2za2);
					BgL_val0_1283z00_2598 =
						BGl_nodezd21occza2z12z62zzreduce_1occz00(BgL_arg1692z00_2599,
						BgL_1zd2expza2z70_2419);
				}
				{	/* Reduce/1occ.scm 155 */
					int BgL_tmpz00_3526;

					BgL_tmpz00_3526 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3526);
				}
				{	/* Reduce/1occ.scm 155 */
					obj_t BgL_auxz00_3531;
					int BgL_tmpz00_3529;

					BgL_auxz00_3531 = ((obj_t) ((BgL_privatez00_bglt) BgL_nodez00_2418));
					BgL_tmpz00_3529 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_3529, BgL_auxz00_3531);
				}
				return BgL_val0_1283z00_2598;
			}
		}

	}



/* &node-1occ!-extern1340 */
	obj_t BGl_z62nodezd21occz12zd2extern1340z70zzreduce_1occz00(obj_t
		BgL_envz00_2420, obj_t BgL_nodez00_2421, obj_t BgL_1zd2expza2z70_2422)
	{
		{	/* Reduce/1occ.scm 147 */
			{	/* Reduce/1occ.scm 148 */
				bool_t BgL_tmpz00_3535;

				{	/* Reduce/1occ.scm 148 */
					bool_t BgL_val0_1281z00_2601;

					BgL_val0_1281z00_2601 =
						BGl_sidezd2effectzf3z21zzeffect_effectz00(
						((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2421)));
					{	/* Reduce/1occ.scm 148 */
						int BgL_tmpz00_3539;

						BgL_tmpz00_3539 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3539);
					}
					{	/* Reduce/1occ.scm 148 */
						obj_t BgL_auxz00_3544;
						int BgL_tmpz00_3542;

						BgL_auxz00_3544 = ((obj_t) ((BgL_externz00_bglt) BgL_nodez00_2421));
						BgL_tmpz00_3542 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_3542, BgL_auxz00_3544);
					}
					BgL_tmpz00_3535 = BgL_val0_1281z00_2601;
				}
				return BBOOL(BgL_tmpz00_3535);
			}
		}

	}



/* &node-1occ!-funcall1338 */
	obj_t BGl_z62nodezd21occz12zd2funcall1338z70zzreduce_1occz00(obj_t
		BgL_envz00_2423, obj_t BgL_nodez00_2424, obj_t BgL_1zd2expza2z70_2425)
	{
		{	/* Reduce/1occ.scm 136 */
			{	/* Reduce/1occ.scm 138 */
				obj_t BgL_resetz72z72_2603;

				BgL_resetz72z72_2603 =
					BGl_nodezd21occza2z12z62zzreduce_1occz00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2424)))->BgL_argsz00),
					BgL_1zd2expza2z70_2425);
				{	/* Reduce/1occ.scm 139 */
					int BgL_tmpz00_3552;

					BgL_tmpz00_3552 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3552);
				}
				{	/* Reduce/1occ.scm 139 */
					obj_t BgL_auxz00_3557;
					int BgL_tmpz00_3555;

					BgL_auxz00_3557 = ((obj_t) ((BgL_funcallz00_bglt) BgL_nodez00_2424));
					BgL_tmpz00_3555 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_3555, BgL_auxz00_3557);
				}
				return BgL_resetz72z72_2603;
			}
		}

	}



/* &node-1occ!-app-ly1336 */
	obj_t BGl_z62nodezd21occz12zd2appzd2ly1336za2zzreduce_1occz00(obj_t
		BgL_envz00_2426, obj_t BgL_nodez00_2427, obj_t BgL_1zd2expza2z70_2428)
	{
		{	/* Reduce/1occ.scm 123 */
			{	/* Reduce/1occ.scm 125 */
				obj_t BgL_resetz00_2605;

				BgL_resetz00_2605 =
					BGl_nodezd21occz12zc0zzreduce_1occz00(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2427)))->BgL_funz00),
					BgL_1zd2expza2z70_2428);
				{	/* Reduce/1occ.scm 126 */
					obj_t BgL_nfunz00_2606;

					{	/* Reduce/1occ.scm 127 */
						obj_t BgL_tmpz00_2607;

						{	/* Reduce/1occ.scm 127 */
							int BgL_tmpz00_3564;

							BgL_tmpz00_3564 = (int) (1L);
							BgL_tmpz00_2607 = BGL_MVALUES_VAL(BgL_tmpz00_3564);
						}
						{	/* Reduce/1occ.scm 127 */
							int BgL_tmpz00_3567;

							BgL_tmpz00_3567 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3567, BUNSPEC);
						}
						BgL_nfunz00_2606 = BgL_tmpz00_2607;
					}
					((((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_2427)))->BgL_funz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nfunz00_2606)),
						BUNSPEC);
					{	/* Reduce/1occ.scm 128 */
						obj_t BgL_resetz72z72_2608;

						{	/* Reduce/1occ.scm 129 */
							BgL_nodez00_bglt BgL_arg1681z00_2609;
							obj_t BgL_arg1688z00_2610;

							BgL_arg1681z00_2609 =
								(((BgL_appzd2lyzd2_bglt) COBJECT(
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_2427)))->BgL_argz00);
							if (CBOOL(BgL_resetz00_2605))
								{	/* Reduce/1occ.scm 129 */
									BgL_arg1688z00_2610 = BNIL;
								}
							else
								{	/* Reduce/1occ.scm 129 */
									BgL_arg1688z00_2610 = BgL_1zd2expza2z70_2428;
								}
							BgL_resetz72z72_2608 =
								BGl_nodezd21occz12zc0zzreduce_1occz00(BgL_arg1681z00_2609,
								BgL_arg1688z00_2610);
						}
						{	/* Reduce/1occ.scm 129 */
							obj_t BgL_nargz00_2611;

							{	/* Reduce/1occ.scm 130 */
								obj_t BgL_tmpz00_2612;

								{	/* Reduce/1occ.scm 130 */
									int BgL_tmpz00_3578;

									BgL_tmpz00_3578 = (int) (1L);
									BgL_tmpz00_2612 = BGL_MVALUES_VAL(BgL_tmpz00_3578);
								}
								{	/* Reduce/1occ.scm 130 */
									int BgL_tmpz00_3581;

									BgL_tmpz00_3581 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3581, BUNSPEC);
								}
								BgL_nargz00_2611 = BgL_tmpz00_2612;
							}
							((((BgL_appzd2lyzd2_bglt) COBJECT(
											((BgL_appzd2lyzd2_bglt) BgL_nodez00_2427)))->BgL_argz00) =
								((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nargz00_2611)),
								BUNSPEC);
							{	/* Reduce/1occ.scm 131 */
								obj_t BgL_val0_1277z00_2613;

								if (CBOOL(BgL_resetz00_2605))
									{	/* Reduce/1occ.scm 131 */
										BgL_val0_1277z00_2613 = BgL_resetz00_2605;
									}
								else
									{	/* Reduce/1occ.scm 131 */
										BgL_val0_1277z00_2613 = BgL_resetz72z72_2608;
									}
								{	/* Reduce/1occ.scm 131 */
									int BgL_tmpz00_3589;

									BgL_tmpz00_3589 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3589);
								}
								{	/* Reduce/1occ.scm 131 */
									obj_t BgL_auxz00_3594;
									int BgL_tmpz00_3592;

									BgL_auxz00_3594 =
										((obj_t) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2427));
									BgL_tmpz00_3592 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3592, BgL_auxz00_3594);
								}
								return BgL_val0_1277z00_2613;
							}
						}
					}
				}
			}
		}

	}



/* &node-1occ!-sync1334 */
	obj_t BGl_z62nodezd21occz12zd2sync1334z70zzreduce_1occz00(obj_t
		BgL_envz00_2429, obj_t BgL_nodez00_2430, obj_t BgL_1zd2expza2z70_2431)
	{
		{	/* Reduce/1occ.scm 110 */
			{	/* Reduce/1occ.scm 112 */
				obj_t BgL_resetmz00_2615;

				BgL_resetmz00_2615 =
					BGl_nodezd21occz12zc0zzreduce_1occz00(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2430)))->BgL_mutexz00),
					BgL_1zd2expza2z70_2431);
				{	/* Reduce/1occ.scm 113 */
					obj_t BgL_nmutexz00_2616;

					{	/* Reduce/1occ.scm 114 */
						obj_t BgL_tmpz00_2617;

						{	/* Reduce/1occ.scm 114 */
							int BgL_tmpz00_3601;

							BgL_tmpz00_3601 = (int) (1L);
							BgL_tmpz00_2617 = BGL_MVALUES_VAL(BgL_tmpz00_3601);
						}
						{	/* Reduce/1occ.scm 114 */
							int BgL_tmpz00_3604;

							BgL_tmpz00_3604 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3604, BUNSPEC);
						}
						BgL_nmutexz00_2616 = BgL_tmpz00_2617;
					}
					((((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_2430)))->BgL_mutexz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nmutexz00_2616)),
						BUNSPEC);
					{	/* Reduce/1occ.scm 115 */
						obj_t BgL_resetpz00_2618;

						BgL_resetpz00_2618 =
							BGl_nodezd21occz12zc0zzreduce_1occz00(
							(((BgL_syncz00_bglt) COBJECT(
										((BgL_syncz00_bglt) BgL_nodez00_2430)))->BgL_prelockz00),
							BgL_1zd2expza2z70_2431);
						{	/* Reduce/1occ.scm 116 */
							obj_t BgL_nprelockz00_2619;

							{	/* Reduce/1occ.scm 117 */
								obj_t BgL_tmpz00_2620;

								{	/* Reduce/1occ.scm 117 */
									int BgL_tmpz00_3613;

									BgL_tmpz00_3613 = (int) (1L);
									BgL_tmpz00_2620 = BGL_MVALUES_VAL(BgL_tmpz00_3613);
								}
								{	/* Reduce/1occ.scm 117 */
									int BgL_tmpz00_3616;

									BgL_tmpz00_3616 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3616, BUNSPEC);
								}
								BgL_nprelockz00_2619 = BgL_tmpz00_2620;
							}
							((((BgL_syncz00_bglt) COBJECT(
											((BgL_syncz00_bglt) BgL_nodez00_2430)))->BgL_prelockz00) =
								((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nprelockz00_2619)),
								BUNSPEC);
							{	/* Reduce/1occ.scm 118 */
								obj_t BgL_val0_1275z00_2621;

								{	/* Reduce/1occ.scm 118 */
									obj_t BgL__ortest_1108z00_2622;

									BgL__ortest_1108z00_2622 =
										BGl_nodezd21occz12zc0zzreduce_1occz00(
										(((BgL_syncz00_bglt) COBJECT(
													((BgL_syncz00_bglt) BgL_nodez00_2430)))->BgL_bodyz00),
										BgL_1zd2expza2z70_2431);
									if (CBOOL(BgL__ortest_1108z00_2622))
										{	/* Reduce/1occ.scm 118 */
											BgL_val0_1275z00_2621 = BgL__ortest_1108z00_2622;
										}
									else
										{	/* Reduce/1occ.scm 118 */
											if (CBOOL(BgL_resetmz00_2615))
												{	/* Reduce/1occ.scm 118 */
													BgL_val0_1275z00_2621 = BgL_resetmz00_2615;
												}
											else
												{	/* Reduce/1occ.scm 118 */
													BgL_val0_1275z00_2621 = BgL_resetpz00_2618;
												}
										}
								}
								{	/* Reduce/1occ.scm 118 */
									int BgL_tmpz00_3629;

									BgL_tmpz00_3629 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3629);
								}
								{	/* Reduce/1occ.scm 118 */
									obj_t BgL_auxz00_3634;
									int BgL_tmpz00_3632;

									BgL_auxz00_3634 =
										((obj_t) ((BgL_syncz00_bglt) BgL_nodez00_2430));
									BgL_tmpz00_3632 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3632, BgL_auxz00_3634);
								}
								return BgL_val0_1275z00_2621;
							}
						}
					}
				}
			}
		}

	}



/* &node-1occ!-sequence1332 */
	obj_t BGl_z62nodezd21occz12zd2sequence1332z70zzreduce_1occz00(obj_t
		BgL_envz00_2432, obj_t BgL_nodez00_2433, obj_t BgL_1zd2expza2z70_2434)
	{
		{	/* Reduce/1occ.scm 103 */
			{	/* Reduce/1occ.scm 105 */
				obj_t BgL_val0_1273z00_2624;

				BgL_val0_1273z00_2624 =
					BGl_nodezd21occza2z12z62zzreduce_1occz00(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_2433)))->BgL_nodesz00),
					BgL_1zd2expza2z70_2434);
				{	/* Reduce/1occ.scm 105 */
					int BgL_tmpz00_3641;

					BgL_tmpz00_3641 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3641);
				}
				{	/* Reduce/1occ.scm 105 */
					obj_t BgL_auxz00_3646;
					int BgL_tmpz00_3644;

					BgL_auxz00_3646 = ((obj_t) ((BgL_sequencez00_bglt) BgL_nodez00_2433));
					BgL_tmpz00_3644 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_3644, BgL_auxz00_3646);
				}
				return BgL_val0_1273z00_2624;
			}
		}

	}



/* &node-1occ!-closure1330 */
	obj_t BGl_z62nodezd21occz12zd2closure1330z70zzreduce_1occz00(obj_t
		BgL_envz00_2435, obj_t BgL_nodez00_2436, obj_t BgL_1zd2expza2z70_2437)
	{
		{	/* Reduce/1occ.scm 97 */
			{	/* Reduce/1occ.scm 98 */
				bool_t BgL_tmpz00_3650;

				{	/* Reduce/1occ.scm 98 */
					int BgL_tmpz00_3651;

					BgL_tmpz00_3651 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3651);
				}
				{	/* Reduce/1occ.scm 98 */
					obj_t BgL_auxz00_3656;
					int BgL_tmpz00_3654;

					BgL_auxz00_3656 = ((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_2436));
					BgL_tmpz00_3654 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_3654, BgL_auxz00_3656);
				}
				BgL_tmpz00_3650 = ((bool_t) 0);
				return BBOOL(BgL_tmpz00_3650);
			}
		}

	}



/* &node-1occ!-var1328 */
	obj_t BGl_z62nodezd21occz12zd2var1328z70zzreduce_1occz00(obj_t
		BgL_envz00_2438, obj_t BgL_nodez00_2439, obj_t BgL_1zd2expza2z70_2440)
	{
		{	/* Reduce/1occ.scm 84 */
			{	/* Reduce/1occ.scm 85 */
				BgL_variablez00_bglt BgL_vz00_2627;

				BgL_vz00_2627 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt) BgL_nodez00_2439)))->BgL_variablez00);
				{	/* Reduce/1occ.scm 86 */
					obj_t BgL_falphaz00_2628;

					BgL_falphaz00_2628 =
						BGl_assqz00zz__r4_pairs_and_lists_6_3z00(
						((obj_t) BgL_vz00_2627), BgL_1zd2expza2z70_2440);
					if (PAIRP(BgL_falphaz00_2628))
						{	/* Reduce/1occ.scm 87 */
							((((BgL_variablez00_bglt) COBJECT(BgL_vz00_2627))->
									BgL_occurrencez00) =
								((long) ((((BgL_variablez00_bglt) COBJECT(BgL_vz00_2627))->
											BgL_occurrencez00) - 1L)), BUNSPEC);
							BGl_za2variablezd2removedza2zd2zzreduce_1occz00 =
								(BGl_za2variablezd2removedza2zd2zzreduce_1occz00 + 1L);
							{	/* Reduce/1occ.scm 91 */
								obj_t BgL_arg1654z00_2629;

								BgL_arg1654z00_2629 = CDR(BgL_falphaz00_2628);
								return
									BGl_nodezd21occz12zc0zzreduce_1occz00(
									((BgL_nodez00_bglt) BgL_arg1654z00_2629),
									BgL_1zd2expza2z70_2440);
							}
						}
					else
						{	/* Reduce/1occ.scm 87 */
							{	/* Reduce/1occ.scm 92 */
								int BgL_tmpz00_3674;

								BgL_tmpz00_3674 = (int) (2L);
								BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3674);
							}
							{	/* Reduce/1occ.scm 92 */
								obj_t BgL_auxz00_3679;
								int BgL_tmpz00_3677;

								BgL_auxz00_3679 =
									((obj_t) ((BgL_varz00_bglt) BgL_nodez00_2439));
								BgL_tmpz00_3677 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_3677, BgL_auxz00_3679);
							}
							return BFALSE;
						}
				}
			}
		}

	}



/* &node-1occ!-kwote1326 */
	obj_t BGl_z62nodezd21occz12zd2kwote1326z70zzreduce_1occz00(obj_t
		BgL_envz00_2441, obj_t BgL_nodez00_2442, obj_t BgL_1zd2expza2z70_2443)
	{
		{	/* Reduce/1occ.scm 78 */
			{	/* Reduce/1occ.scm 79 */
				bool_t BgL_tmpz00_3683;

				{	/* Reduce/1occ.scm 79 */
					int BgL_tmpz00_3684;

					BgL_tmpz00_3684 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3684);
				}
				{	/* Reduce/1occ.scm 79 */
					obj_t BgL_auxz00_3689;
					int BgL_tmpz00_3687;

					BgL_auxz00_3689 = ((obj_t) ((BgL_kwotez00_bglt) BgL_nodez00_2442));
					BgL_tmpz00_3687 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_3687, BgL_auxz00_3689);
				}
				BgL_tmpz00_3683 = ((bool_t) 0);
				return BBOOL(BgL_tmpz00_3683);
			}
		}

	}



/* &node-1occ!-atom1324 */
	obj_t BGl_z62nodezd21occz12zd2atom1324z70zzreduce_1occz00(obj_t
		BgL_envz00_2444, obj_t BgL_nodez00_2445, obj_t BgL_1zd2expza2z70_2446)
	{
		{	/* Reduce/1occ.scm 72 */
			{	/* Reduce/1occ.scm 73 */
				bool_t BgL_tmpz00_3694;

				{	/* Reduce/1occ.scm 73 */
					int BgL_tmpz00_3695;

					BgL_tmpz00_3695 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3695);
				}
				{	/* Reduce/1occ.scm 73 */
					obj_t BgL_auxz00_3700;
					int BgL_tmpz00_3698;

					BgL_auxz00_3700 = ((obj_t) ((BgL_atomz00_bglt) BgL_nodez00_2445));
					BgL_tmpz00_3698 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_3698, BgL_auxz00_3700);
				}
				BgL_tmpz00_3694 = ((bool_t) 0);
				return BBOOL(BgL_tmpz00_3694);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzreduce_1occz00(void)
	{
		{	/* Reduce/1occ.scm 16 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1943z00zzreduce_1occz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1943z00zzreduce_1occz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1943z00zzreduce_1occz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1943z00zzreduce_1occz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1943z00zzreduce_1occz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1943z00zzreduce_1occz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1943z00zzreduce_1occz00));
			BGl_modulezd2initializa7ationz75zztype_miscz00(49975086L,
				BSTRING_TO_STRING(BGl_string1943z00zzreduce_1occz00));
			BGl_modulezd2initializa7ationz75zzcoerce_coercez00(361167184L,
				BSTRING_TO_STRING(BGl_string1943z00zzreduce_1occz00));
			BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string1943z00zzreduce_1occz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1943z00zzreduce_1occz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1943z00zzreduce_1occz00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string1943z00zzreduce_1occz00));
			BGl_modulezd2initializa7ationz75zzast_occurz00(282085879L,
				BSTRING_TO_STRING(BGl_string1943z00zzreduce_1occz00));
			return
				BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1943z00zzreduce_1occz00));
		}

	}

#ifdef __cplusplus
}
#endif
