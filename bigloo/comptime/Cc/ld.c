/*===========================================================================*/
/*   (Cc/ld.scm)                                                             */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cc/ld.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CC_LD_TYPE_DEFINITIONS
#define BGL_CC_LD_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_CC_LD_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2destza2z00zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t BGl_libraryzd2suffixeszd2zzcc_ldz00(void);
	static obj_t BGl_requirezd2initializa7ationz75zzcc_ldz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_makezd2sharedzd2libzd2namezd2zz__osz00(obj_t, obj_t);
	static obj_t BGl_profilezd2gczd2debugzd2libraryzd2suffixz00zzcc_ldz00(void);
	extern obj_t BGl_za2stripza2z00zzengine_paramz00;
	extern obj_t BGl_za2staticzd2allzd2bigloozf3za2zf3zzengine_paramz00;
	extern obj_t BGl_za2cczd2optionsza2zd2zzengine_paramz00;
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	extern obj_t BGl_za2doublezd2ldzd2libszf3za2zf3zzengine_paramz00;
	extern obj_t BGl_za2ldzd2relativeza2zd2zzengine_paramz00;
	static obj_t BGl_libraryzd2ze3oszd2fileze3zzcc_ldz00(obj_t, obj_t, obj_t,
		bool_t, bool_t);
	extern obj_t BGl_za2gczd2customzf3za2z21zzengine_paramz00;
	extern obj_t BGl_za2passza2z00zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t BGl_ldz00zzcc_ldz00(obj_t, bool_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	extern obj_t BGl_za2ldzd2optionsza2zd2zzengine_paramz00;
	static obj_t BGl_genericzd2initzd2zzcc_ldz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzcc_ldz00(void);
	BGL_IMPORT obj_t BGl_appendz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62libraryzd2suffixeszb0zzcc_ldz00(obj_t);
	extern obj_t BGl_za2sawza2z00zzengine_paramz00;
	extern obj_t BGl_za2additionalzd2bigloozd2librariesza2z00zzengine_paramz00;
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_userzd2warningzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_win32zd2ldzd2zzcc_ldz00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzcc_ldz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcc_ldz00(void);
	extern obj_t BGl_unixzd2filenamezd2zzcc_execz00(obj_t);
	extern obj_t BGl_za2ccza2z00zzengine_paramz00;
	static obj_t BGl_zc3z04anonymousza32059ze3ze70z60zzcc_ldz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31979ze3ze5zzcc_ldz00(obj_t);
	extern obj_t BGl_execz00zzcc_execz00(obj_t, bool_t, obj_t);
	BGL_IMPORT obj_t bgl_close_input_port(obj_t);
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_za2ldzd2debugzd2optionza2z00zzengine_paramz00;
	extern obj_t BGl_za2gczd2libza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2ldzd2optimzd2flagsza2z00zzengine_paramz00;
	extern obj_t BGl_za2staticzd2bigloozf3za2z21zzengine_paramz00;
	extern obj_t BGl_za2ldzd2postzd2optionsza2z00zzengine_paramz00;
	BGL_IMPORT obj_t bgl_append2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2multizd2threadedzd2gczf3za2zf3zzengine_paramz00;
	static obj_t BGl_z62zc3z04anonymousza32327ze3ze5zzcc_ldz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcc_ldz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_evalz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_configurez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcc_execz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__libraryz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__rgcz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__processz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__configurez00(long,
		char *);
	static obj_t BGl_zc3z04anonymousza32087ze3ze70z60zzcc_ldz00(obj_t);
	BGL_IMPORT obj_t BGl_makezd2staticzd2libzd2namezd2zz__osz00(obj_t, obj_t);
	extern obj_t BGl_za2profilezd2libraryza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_za2withzd2filesza2zd2zzengine_paramz00;
	extern obj_t BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00;
	extern obj_t BGl_za2libzd2dirza2zd2zzengine_paramz00;
	extern obj_t BGl_za2bigloozd2libza2zd2zzengine_paramz00;
	extern obj_t BGl_za2unsafezd2libraryza2zd2zzengine_paramz00;
	static obj_t BGl_cnstzd2initzd2zzcc_ldz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcc_ldz00(void);
	extern obj_t BGl_za2bigloozd2userzd2libza2z00zzengine_paramz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzcc_ldz00(void);
	BGL_IMPORT obj_t BGl_prefixz00zz__osz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzcc_ldz00(void);
	static obj_t BGl_z62ldz62zzcc_ldz00(obj_t, obj_t, obj_t);
	static obj_t BGl_unixzd2ldzd2zzcc_ldz00(obj_t, bool_t);
	extern obj_t BGl_za2ozd2filesza2zd2zzengine_paramz00;
	extern obj_t BGl_za2cflagszd2rpathza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_bigloozd2configzd2zz__configurez00(obj_t);
	static obj_t BGl_defaultzd2sonameze70z35zzcc_ldz00(obj_t);
	extern obj_t BGl_stringza2zd2ze3stringz93zztools_miscz00(obj_t);
	BGL_IMPORT obj_t
		BGl_libraryzd2translationzd2tablezd2addz12zc0zz__libraryz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_getzd2evalzd2librariesz00zzmodule_evalz00(void);
	extern obj_t BGl_za2ldzd2ozd2optionza2z00zzengine_paramz00;
	BGL_IMPORT obj_t BGl_runzd2processzd2zz__processz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
	static obj_t BGl_loopze70ze7zzcc_ldz00(obj_t);
	static obj_t BGl_loopze71ze7zzcc_ldz00(obj_t);
	BGL_IMPORT obj_t BGl_libraryzd2filezd2namez00zz__libraryz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT bool_t rgc_fill_buffer(obj_t);
	BGL_IMPORT obj_t
		BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_stringzd2splitzd2charz00zztools_miscz00(obj_t, obj_t);
	extern obj_t BGl_za2czd2debugza2zd2zzengine_paramz00;
	static obj_t BGl_zc3z04anonymousza31172ze3ze70z60zzcc_ldz00(obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t BGl_libraryzd2threadzd2suffixz00zzcc_ldz00(obj_t);
	static obj_t BGl_libraryzd2evalzd2suffixesz00zzcc_ldz00(void);
	static obj_t __cnst[27];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ldzd2envzd2zzcc_ldz00,
		BgL_bgl_za762ldza762za7za7cc_ldza72372za7, BGl_z62ldz62zzcc_ldz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2339z00zzcc_ldz00,
		BgL_bgl_string2339za700za7za7c2373za7, "unix", 4);
	      DEFINE_STRING(BGl_string2340z00zzcc_ldz00,
		BgL_bgl_string2340za700za7za7c2374za7, "mingw", 5);
	      DEFINE_STRING(BGl_string2341z00zzcc_ldz00,
		BgL_bgl_string2341za700za7za7c2375za7, "win32", 5);
	      DEFINE_STRING(BGl_string2342z00zzcc_ldz00,
		BgL_bgl_string2342za700za7za7c2376za7, "ld", 2);
	      DEFINE_STRING(BGl_string2343z00zzcc_ldz00,
		BgL_bgl_string2343za700za7za7c2377za7, "Unknown os", 10);
	      DEFINE_STRING(BGl_string2344z00zzcc_ldz00,
		BgL_bgl_string2344za700za7za7c2378za7, "_mt", 3);
	      DEFINE_STRING(BGl_string2345z00zzcc_ldz00,
		BgL_bgl_string2345za700za7za7c2379za7, "saw", 3);
	      DEFINE_STRING(BGl_string2346z00zzcc_ldz00,
		BgL_bgl_string2346za700za7za7c2380za7, "_p", 2);
	      DEFINE_STRING(BGl_string2347z00zzcc_ldz00,
		BgL_bgl_string2347za700za7za7c2381za7, "", 0);
	      DEFINE_STRING(BGl_string2348z00zzcc_ldz00,
		BgL_bgl_string2348za700za7za7c2382za7, "-l", 2);
	      DEFINE_STRING(BGl_string2349z00zzcc_ldz00,
		BgL_bgl_string2349za700za7za7c2383za7, "Can't find any `~a'", 19);
	      DEFINE_STRING(BGl_string2350z00zzcc_ldz00,
		BgL_bgl_string2350za700za7za7c2384za7, "bigloo", 6);
	      DEFINE_STRING(BGl_string2351z00zzcc_ldz00,
		BgL_bgl_string2351za700za7za7c2385za7, "Can't find library \"~a\" in path",
		31);
	      DEFINE_STRING(BGl_string2352z00zzcc_ldz00,
		BgL_bgl_string2352za700za7za7c2386za7, " ~( )", 5);
	      DEFINE_STRING(BGl_string2353z00zzcc_ldz00,
		BgL_bgl_string2353za700za7za7c2387za7, ")", 1);
	      DEFINE_STRING(BGl_string2354z00zzcc_ldz00,
		BgL_bgl_string2354za700za7za7c2388za7, "   . ld (", 9);
	      DEFINE_STRING(BGl_string2355z00zzcc_ldz00,
		BgL_bgl_string2355za700za7za7c2389za7, "regular-grammar", 15);
	      DEFINE_STRING(BGl_string2356z00zzcc_ldz00,
		BgL_bgl_string2356za700za7za7c2390za7, "Illegal match", 13);
	      DEFINE_STRING(BGl_string2357z00zzcc_ldz00,
		BgL_bgl_string2357za700za7za7c2391za7, " ", 1);
	      DEFINE_STRING(BGl_string2358z00zzcc_ldz00,
		BgL_bgl_string2358za700za7za7c2392za7, ".", 1);
	      DEFINE_STRING(BGl_string2359z00zzcc_ldz00,
		BgL_bgl_string2359za700za7za7c2393za7, "~( )", 4);
	      DEFINE_STRING(BGl_string2360z00zzcc_ldz00,
		BgL_bgl_string2360za700za7za7c2394za7, "-L", 2);
	      DEFINE_STRING(BGl_string2361z00zzcc_ldz00,
		BgL_bgl_string2361za700za7za7c2395za7, "      [", 7);
	      DEFINE_STRING(BGl_string2362z00zzcc_ldz00,
		BgL_bgl_string2362za700za7za7c2396za7, "out", 3);
	      DEFINE_STRING(BGl_string2363z00zzcc_ldz00,
		BgL_bgl_string2363za700za7za7c2397za7, "bigloo_mt", 9);
	      DEFINE_STRING(BGl_string2364z00zzcc_ldz00,
		BgL_bgl_string2364za700za7za7c2398za7, "/link", 5);
	      DEFINE_STRING(BGl_string2365z00zzcc_ldz00,
		BgL_bgl_string2365za700za7za7c2399za7, "[", 1);
	      DEFINE_STRING(BGl_string2366z00zzcc_ldz00,
		BgL_bgl_string2366za700za7za7c2400za7, "]", 1);
	      DEFINE_STRING(BGl_string2367z00zzcc_ldz00,
		BgL_bgl_string2367za700za7za7c2401za7, "      ", 6);
	      DEFINE_STRING(BGl_string2368z00zzcc_ldz00,
		BgL_bgl_string2368za700za7za7c2402za7, "/LIBPATH:", 9);
	      DEFINE_STRING(BGl_string2369z00zzcc_ldz00,
		BgL_bgl_string2369za700za7za7c2403za7, "cc_ld", 5);
	      DEFINE_STRING(BGl_string2370z00zzcc_ldz00,
		BgL_bgl_string2370za700za7za7c2404za7,
		"(:wait #t) shared-lib-suffix dlopen-lib non-custom-gc-directory c-pic-flag c-strip-flag c-linker-soname-option c-linker-shared-option shared-link-option static-link-option have-shared-library c-ld so c-compiler-rpath (\"_es\") (\"_eu\" \"_es\") (\"_ep\" \"_es\") (\"_s\") (\"_saw_s\" \"_s\") (\"_saw_s\") (\"_u\" \"_s\") (\"_saw_u\" \"_u\" \"_saw_s\") (\"_saw_u\" \"_saw_s\") (\"_p\" \"_s\") (\"_saw_p\" \"_p\" \"_saw_s\" \"_s\") (\"_saw_p\" \"_saw_s\") gc ",
		409);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_libraryzd2suffixeszd2envz00zzcc_ldz00,
		BgL_bgl_za762libraryza7d2suf2405z00, BGl_z62libraryzd2suffixeszb0zzcc_ldz00,
		0L, BUNSPEC, 0);
	BGL_IMPORT obj_t BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcc_ldz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcc_ldz00(long
		BgL_checksumz00_1977, char *BgL_fromz00_1978)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcc_ldz00))
				{
					BGl_requirezd2initializa7ationz75zzcc_ldz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcc_ldz00();
					BGl_libraryzd2moduleszd2initz00zzcc_ldz00();
					BGl_cnstzd2initzd2zzcc_ldz00();
					BGl_importedzd2moduleszd2initz00zzcc_ldz00();
					return BGl_methodzd2initzd2zzcc_ldz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcc_ldz00(void)
	{
		{	/* Cc/ld.scm 15 */
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "cc_ld");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "cc_ld");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cc_ld");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cc_ld");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cc_ld");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cc_ld");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cc_ld");
			BGl_modulezd2initializa7ationz75zz__configurez00(0L, "cc_ld");
			BGl_modulezd2initializa7ationz75zz__libraryz00(0L, "cc_ld");
			BGl_modulezd2initializa7ationz75zz__processz00(0L, "cc_ld");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "cc_ld");
			BGl_modulezd2initializa7ationz75zz__rgcz00(0L, "cc_ld");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "cc_ld");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "cc_ld");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L, "cc_ld");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcc_ldz00(void)
	{
		{	/* Cc/ld.scm 15 */
			{	/* Cc/ld.scm 15 */
				obj_t BgL_cportz00_1966;

				{	/* Cc/ld.scm 15 */
					obj_t BgL_stringz00_1973;

					BgL_stringz00_1973 = BGl_string2370z00zzcc_ldz00;
					{	/* Cc/ld.scm 15 */
						obj_t BgL_startz00_1974;

						BgL_startz00_1974 = BINT(0L);
						{	/* Cc/ld.scm 15 */
							obj_t BgL_endz00_1975;

							BgL_endz00_1975 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1973)));
							{	/* Cc/ld.scm 15 */

								BgL_cportz00_1966 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1973, BgL_startz00_1974, BgL_endz00_1975);
				}}}}
				{
					long BgL_iz00_1967;

					BgL_iz00_1967 = 26L;
				BgL_loopz00_1968:
					if ((BgL_iz00_1967 == -1L))
						{	/* Cc/ld.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cc/ld.scm 15 */
							{	/* Cc/ld.scm 15 */
								obj_t BgL_arg2371z00_1969;

								{	/* Cc/ld.scm 15 */

									{	/* Cc/ld.scm 15 */
										obj_t BgL_locationz00_1971;

										BgL_locationz00_1971 = BBOOL(((bool_t) 0));
										{	/* Cc/ld.scm 15 */

											BgL_arg2371z00_1969 =
												BGl_readz00zz__readerz00(BgL_cportz00_1966,
												BgL_locationz00_1971);
										}
									}
								}
								{	/* Cc/ld.scm 15 */
									int BgL_tmpz00_2011;

									BgL_tmpz00_2011 = (int) (BgL_iz00_1967);
									CNST_TABLE_SET(BgL_tmpz00_2011, BgL_arg2371z00_1969);
							}}
							{	/* Cc/ld.scm 15 */
								int BgL_auxz00_1972;

								BgL_auxz00_1972 = (int) ((BgL_iz00_1967 - 1L));
								{
									long BgL_iz00_2016;

									BgL_iz00_2016 = (long) (BgL_auxz00_1972);
									BgL_iz00_1967 = BgL_iz00_2016;
									goto BgL_loopz00_1968;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcc_ldz00(void)
	{
		{	/* Cc/ld.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzcc_ldz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_111;

				BgL_headz00_111 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_112;
					obj_t BgL_tailz00_113;

					BgL_prevz00_112 = BgL_headz00_111;
					BgL_tailz00_113 = BgL_l1z00_1;
				BgL_loopz00_114:
					if (PAIRP(BgL_tailz00_113))
						{
							obj_t BgL_newzd2prevzd2_116;

							BgL_newzd2prevzd2_116 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_113), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_112, BgL_newzd2prevzd2_116);
							{
								obj_t BgL_tailz00_2026;
								obj_t BgL_prevz00_2025;

								BgL_prevz00_2025 = BgL_newzd2prevzd2_116;
								BgL_tailz00_2026 = CDR(BgL_tailz00_113);
								BgL_tailz00_113 = BgL_tailz00_2026;
								BgL_prevz00_112 = BgL_prevz00_2025;
								goto BgL_loopz00_114;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_111);
				}
			}
		}

	}



/* ld */
	BGL_EXPORTED_DEF obj_t BGl_ldz00zzcc_ldz00(obj_t BgL_namez00_3,
		bool_t BgL_needzd2tozd2returnz00_4)
	{
		{	/* Cc/ld.scm 30 */
			{	/* Cc/ld.scm 32 */
				bool_t BgL_test2409z00_2029;

				{	/* Cc/ld.scm 32 */
					obj_t BgL_string1z00_1409;

					BgL_string1z00_1409 = string_to_bstring(OS_CLASS);
					{	/* Cc/ld.scm 32 */
						long BgL_l1z00_1411;

						BgL_l1z00_1411 = STRING_LENGTH(BgL_string1z00_1409);
						if ((BgL_l1z00_1411 == 4L))
							{	/* Cc/ld.scm 32 */
								int BgL_arg1282z00_1414;

								{	/* Cc/ld.scm 32 */
									char *BgL_auxz00_2036;
									char *BgL_tmpz00_2034;

									BgL_auxz00_2036 =
										BSTRING_TO_STRING(BGl_string2339z00zzcc_ldz00);
									BgL_tmpz00_2034 = BSTRING_TO_STRING(BgL_string1z00_1409);
									BgL_arg1282z00_1414 =
										memcmp(BgL_tmpz00_2034, BgL_auxz00_2036, BgL_l1z00_1411);
								}
								BgL_test2409z00_2029 = ((long) (BgL_arg1282z00_1414) == 0L);
							}
						else
							{	/* Cc/ld.scm 32 */
								BgL_test2409z00_2029 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test2409z00_2029)
					{	/* Cc/ld.scm 32 */
						BGL_TAIL return
							BGl_unixzd2ldzd2zzcc_ldz00(BgL_namez00_3,
							BgL_needzd2tozd2returnz00_4);
					}
				else
					{	/* Cc/ld.scm 34 */
						bool_t BgL_test2411z00_2042;

						{	/* Cc/ld.scm 34 */
							obj_t BgL_string1z00_1420;

							BgL_string1z00_1420 = string_to_bstring(OS_CLASS);
							{	/* Cc/ld.scm 34 */
								long BgL_l1z00_1422;

								BgL_l1z00_1422 = STRING_LENGTH(BgL_string1z00_1420);
								if ((BgL_l1z00_1422 == 5L))
									{	/* Cc/ld.scm 34 */
										int BgL_arg1282z00_1425;

										{	/* Cc/ld.scm 34 */
											char *BgL_auxz00_2049;
											char *BgL_tmpz00_2047;

											BgL_auxz00_2049 =
												BSTRING_TO_STRING(BGl_string2340z00zzcc_ldz00);
											BgL_tmpz00_2047 = BSTRING_TO_STRING(BgL_string1z00_1420);
											BgL_arg1282z00_1425 =
												memcmp(BgL_tmpz00_2047, BgL_auxz00_2049,
												BgL_l1z00_1422);
										}
										BgL_test2411z00_2042 = ((long) (BgL_arg1282z00_1425) == 0L);
									}
								else
									{	/* Cc/ld.scm 34 */
										BgL_test2411z00_2042 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test2411z00_2042)
							{	/* Cc/ld.scm 34 */
								BGL_TAIL return
									BGl_unixzd2ldzd2zzcc_ldz00(BgL_namez00_3,
									BgL_needzd2tozd2returnz00_4);
							}
						else
							{	/* Cc/ld.scm 36 */
								bool_t BgL_test2413z00_2055;

								{	/* Cc/ld.scm 36 */
									obj_t BgL_string1z00_1431;

									BgL_string1z00_1431 = string_to_bstring(OS_CLASS);
									{	/* Cc/ld.scm 36 */
										long BgL_l1z00_1433;

										BgL_l1z00_1433 = STRING_LENGTH(BgL_string1z00_1431);
										if ((BgL_l1z00_1433 == 5L))
											{	/* Cc/ld.scm 36 */
												int BgL_arg1282z00_1436;

												{	/* Cc/ld.scm 36 */
													char *BgL_auxz00_2062;
													char *BgL_tmpz00_2060;

													BgL_auxz00_2062 =
														BSTRING_TO_STRING(BGl_string2341z00zzcc_ldz00);
													BgL_tmpz00_2060 =
														BSTRING_TO_STRING(BgL_string1z00_1431);
													BgL_arg1282z00_1436 =
														memcmp(BgL_tmpz00_2060, BgL_auxz00_2062,
														BgL_l1z00_1433);
												}
												BgL_test2413z00_2055 =
													((long) (BgL_arg1282z00_1436) == 0L);
											}
										else
											{	/* Cc/ld.scm 36 */
												BgL_test2413z00_2055 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test2413z00_2055)
									{	/* Cc/ld.scm 36 */
										return BGl_win32zd2ldzd2zzcc_ldz00(BgL_namez00_3);
									}
								else
									{	/* Cc/ld.scm 36 */
										return
											BGl_userzd2errorzd2zztools_errorz00
											(BGl_string2342z00zzcc_ldz00, BGl_string2343z00zzcc_ldz00,
											string_to_bstring(OS_CLASS), BNIL);
									}
							}
					}
			}
		}

	}



/* &ld */
	obj_t BGl_z62ldz62zzcc_ldz00(obj_t BgL_envz00_1953, obj_t BgL_namez00_1954,
		obj_t BgL_needzd2tozd2returnz00_1955)
	{
		{	/* Cc/ld.scm 30 */
			return
				BGl_ldz00zzcc_ldz00(BgL_namez00_1954,
				CBOOL(BgL_needzd2tozd2returnz00_1955));
		}

	}



/* library-thread-suffix */
	obj_t BGl_libraryzd2threadzd2suffixz00zzcc_ldz00(obj_t BgL_suffixesz00_5)
	{
		{	/* Cc/ld.scm 44 */
			if (CBOOL(BGl_za2multizd2threadedzd2gczf3za2zf3zzengine_paramz00))
				{	/* Cc/ld.scm 45 */
					return
						BGl_zc3z04anonymousza31172ze3ze70z60zzcc_ldz00(BgL_suffixesz00_5);
				}
			else
				{	/* Cc/ld.scm 45 */
					return BgL_suffixesz00_5;
				}
		}

	}



/* <@anonymous:1172>~0 */
	obj_t BGl_zc3z04anonymousza31172ze3ze70z60zzcc_ldz00(obj_t BgL_l1114z00_131)
	{
		{	/* Cc/ld.scm 46 */
			if (NULLP(BgL_l1114z00_131))
				{	/* Cc/ld.scm 46 */
					return BNIL;
				}
			else
				{	/* Cc/ld.scm 47 */
					obj_t BgL_arg1182z00_134;
					obj_t BgL_arg1183z00_135;

					{	/* Cc/ld.scm 47 */
						obj_t BgL_sz00_136;

						BgL_sz00_136 = CAR(((obj_t) BgL_l1114z00_131));
						{	/* Cc/ld.scm 47 */
							obj_t BgL_arg1187z00_137;

							BgL_arg1187z00_137 =
								string_append(BgL_sz00_136, BGl_string2344z00zzcc_ldz00);
							{	/* Cc/ld.scm 47 */
								obj_t BgL_list1188z00_138;

								{	/* Cc/ld.scm 47 */
									obj_t BgL_arg1189z00_139;

									BgL_arg1189z00_139 = MAKE_YOUNG_PAIR(BgL_sz00_136, BNIL);
									BgL_list1188z00_138 =
										MAKE_YOUNG_PAIR(BgL_arg1187z00_137, BgL_arg1189z00_139);
								}
								BgL_arg1182z00_134 = BgL_list1188z00_138;
							}
						}
					}
					{	/* Cc/ld.scm 46 */
						obj_t BgL_arg1190z00_140;

						BgL_arg1190z00_140 = CDR(((obj_t) BgL_l1114z00_131));
						BgL_arg1183z00_135 =
							BGl_zc3z04anonymousza31172ze3ze70z60zzcc_ldz00
							(BgL_arg1190z00_140);
					}
					return bgl_append2(BgL_arg1182z00_134, BgL_arg1183z00_135);
				}
		}

	}



/* library-suffixes */
	BGL_EXPORTED_DEF obj_t BGl_libraryzd2suffixeszd2zzcc_ldz00(void)
	{
		{	/* Cc/ld.scm 54 */
			{	/* Cc/ld.scm 57 */
				obj_t BgL_arg1191z00_142;

				if (CBOOL(BGl_za2profilezd2libraryza2zd2zzengine_paramz00))
					{	/* Cc/ld.scm 57 */
						if (CBOOL(BGl_za2sawza2z00zzengine_paramz00))
							{	/* Cc/ld.scm 59 */
								bool_t BgL_test2419z00_2090;

								{	/* Cc/ld.scm 59 */
									obj_t BgL_arg1196z00_145;

									BgL_arg1196z00_145 =
										BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(0));
									{	/* Cc/ld.scm 59 */
										long BgL_l1z00_1447;

										BgL_l1z00_1447 =
											STRING_LENGTH(((obj_t) BgL_arg1196z00_145));
										if ((BgL_l1z00_1447 == 3L))
											{	/* Cc/ld.scm 59 */
												int BgL_arg1282z00_1450;

												{	/* Cc/ld.scm 59 */
													char *BgL_auxz00_2100;
													char *BgL_tmpz00_2097;

													BgL_auxz00_2100 =
														BSTRING_TO_STRING(BGl_string2345z00zzcc_ldz00);
													BgL_tmpz00_2097 =
														BSTRING_TO_STRING(((obj_t) BgL_arg1196z00_145));
													BgL_arg1282z00_1450 =
														memcmp(BgL_tmpz00_2097, BgL_auxz00_2100,
														BgL_l1z00_1447);
												}
												BgL_test2419z00_2090 =
													((long) (BgL_arg1282z00_1450) == 0L);
											}
										else
											{	/* Cc/ld.scm 59 */
												BgL_test2419z00_2090 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test2419z00_2090)
									{	/* Cc/ld.scm 59 */
										BgL_arg1191z00_142 = CNST_TABLE_REF(1);
									}
								else
									{	/* Cc/ld.scm 59 */
										BgL_arg1191z00_142 = CNST_TABLE_REF(2);
									}
							}
						else
							{	/* Cc/ld.scm 58 */
								BgL_arg1191z00_142 = CNST_TABLE_REF(3);
							}
					}
				else
					{	/* Cc/ld.scm 57 */
						if (CBOOL(BGl_za2unsafezd2libraryza2zd2zzengine_paramz00))
							{	/* Cc/ld.scm 63 */
								if (CBOOL(BGl_za2sawza2z00zzengine_paramz00))
									{	/* Cc/ld.scm 65 */
										bool_t BgL_test2423z00_2112;

										{	/* Cc/ld.scm 65 */
											obj_t BgL_arg1199z00_148;

											BgL_arg1199z00_148 =
												BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
												(0));
											{	/* Cc/ld.scm 65 */
												long BgL_l1z00_1458;

												BgL_l1z00_1458 =
													STRING_LENGTH(((obj_t) BgL_arg1199z00_148));
												if ((BgL_l1z00_1458 == 3L))
													{	/* Cc/ld.scm 65 */
														int BgL_arg1282z00_1461;

														{	/* Cc/ld.scm 65 */
															char *BgL_auxz00_2122;
															char *BgL_tmpz00_2119;

															BgL_auxz00_2122 =
																BSTRING_TO_STRING(BGl_string2345z00zzcc_ldz00);
															BgL_tmpz00_2119 =
																BSTRING_TO_STRING(((obj_t) BgL_arg1199z00_148));
															BgL_arg1282z00_1461 =
																memcmp(BgL_tmpz00_2119, BgL_auxz00_2122,
																BgL_l1z00_1458);
														}
														BgL_test2423z00_2112 =
															((long) (BgL_arg1282z00_1461) == 0L);
													}
												else
													{	/* Cc/ld.scm 65 */
														BgL_test2423z00_2112 = ((bool_t) 0);
													}
											}
										}
										if (BgL_test2423z00_2112)
											{	/* Cc/ld.scm 65 */
												BgL_arg1191z00_142 = CNST_TABLE_REF(4);
											}
										else
											{	/* Cc/ld.scm 65 */
												BgL_arg1191z00_142 = CNST_TABLE_REF(5);
											}
									}
								else
									{	/* Cc/ld.scm 64 */
										BgL_arg1191z00_142 = CNST_TABLE_REF(6);
									}
							}
						else
							{	/* Cc/ld.scm 63 */
								if (CBOOL(BGl_za2sawza2z00zzengine_paramz00))
									{	/* Cc/ld.scm 70 */
										bool_t BgL_test2426z00_2132;

										{	/* Cc/ld.scm 70 */
											obj_t BgL_arg1202z00_151;

											BgL_arg1202z00_151 =
												BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
												(0));
											{	/* Cc/ld.scm 70 */
												long BgL_l1z00_1469;

												BgL_l1z00_1469 =
													STRING_LENGTH(((obj_t) BgL_arg1202z00_151));
												if ((BgL_l1z00_1469 == 3L))
													{	/* Cc/ld.scm 70 */
														int BgL_arg1282z00_1472;

														{	/* Cc/ld.scm 70 */
															char *BgL_auxz00_2142;
															char *BgL_tmpz00_2139;

															BgL_auxz00_2142 =
																BSTRING_TO_STRING(BGl_string2345z00zzcc_ldz00);
															BgL_tmpz00_2139 =
																BSTRING_TO_STRING(((obj_t) BgL_arg1202z00_151));
															BgL_arg1282z00_1472 =
																memcmp(BgL_tmpz00_2139, BgL_auxz00_2142,
																BgL_l1z00_1469);
														}
														BgL_test2426z00_2132 =
															((long) (BgL_arg1282z00_1472) == 0L);
													}
												else
													{	/* Cc/ld.scm 70 */
														BgL_test2426z00_2132 = ((bool_t) 0);
													}
											}
										}
										if (BgL_test2426z00_2132)
											{	/* Cc/ld.scm 70 */
												BgL_arg1191z00_142 = CNST_TABLE_REF(7);
											}
										else
											{	/* Cc/ld.scm 70 */
												BgL_arg1191z00_142 = CNST_TABLE_REF(8);
											}
									}
								else
									{	/* Cc/ld.scm 69 */
										BgL_arg1191z00_142 = CNST_TABLE_REF(9);
									}
							}
					}
				return BGl_libraryzd2threadzd2suffixz00zzcc_ldz00(BgL_arg1191z00_142);
			}
		}

	}



/* &library-suffixes */
	obj_t BGl_z62libraryzd2suffixeszb0zzcc_ldz00(obj_t BgL_envz00_1956)
	{
		{	/* Cc/ld.scm 54 */
			return BGl_libraryzd2suffixeszd2zzcc_ldz00();
		}

	}



/* library-eval-suffixes */
	obj_t BGl_libraryzd2evalzd2suffixesz00zzcc_ldz00(void)
	{
		{	/* Cc/ld.scm 79 */
			{	/* Cc/ld.scm 82 */
				obj_t BgL_arg1203z00_152;

				if (CBOOL(BGl_za2profilezd2libraryza2zd2zzengine_paramz00))
					{	/* Cc/ld.scm 82 */
						BgL_arg1203z00_152 = CNST_TABLE_REF(10);
					}
				else
					{	/* Cc/ld.scm 82 */
						if (CBOOL(BGl_za2unsafezd2libraryza2zd2zzengine_paramz00))
							{	/* Cc/ld.scm 83 */
								BgL_arg1203z00_152 = CNST_TABLE_REF(11);
							}
						else
							{	/* Cc/ld.scm 83 */
								BgL_arg1203z00_152 = CNST_TABLE_REF(12);
							}
					}
				return BGl_libraryzd2threadzd2suffixz00zzcc_ldz00(BgL_arg1203z00_152);
			}
		}

	}



/* profile-gc-debug-library-suffix */
	obj_t BGl_profilezd2gczd2debugzd2libraryzd2suffixz00zzcc_ldz00(void)
	{
		{	/* Cc/ld.scm 95 */
			if (CBOOL(BGl_za2profilezd2libraryza2zd2zzengine_paramz00))
				{	/* Cc/ld.scm 97 */
					return BGl_string2346z00zzcc_ldz00;
				}
			else
				{	/* Cc/ld.scm 97 */
					return BGl_string2347z00zzcc_ldz00;
				}
		}

	}



/* library->os-file */
	obj_t BGl_libraryzd2ze3oszd2fileze3zzcc_ldz00(obj_t BgL_libraryz00_6,
		obj_t BgL_suffixesz00_7, obj_t BgL_staticpz00_8, bool_t BgL_forcepz00_9,
		bool_t BgL_foreignpz00_10)
	{
		{	/* Cc/ld.scm 103 */
		BGl_libraryzd2ze3oszd2fileze3zzcc_ldz00:
			{
				obj_t BgL_libraryz00_175;
				obj_t BgL_backendz00_176;

				{	/* Cc/ld.scm 116 */
					obj_t BgL_backendz00_154;

					{	/* Cc/ld.scm 116 */
						obj_t BgL_arg1220z00_174;

						BgL_arg1220z00_174 = BGl_thezd2backendzd2zzbackend_backendz00();
						BgL_backendz00_154 =
							(((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_arg1220z00_174)))->BgL_srfi0z00);
					}
					if (BgL_foreignpz00_10)
						{	/* Cc/ld.scm 118 */
							obj_t BgL_arg1206z00_155;

							{	/* Cc/ld.scm 118 */
								obj_t BgL_arg1455z00_1513;

								BgL_arg1455z00_1513 =
									SYMBOL_TO_STRING(((obj_t) BgL_libraryz00_6));
								BgL_arg1206z00_155 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1455z00_1513);
							}
							BgL_libraryz00_175 = BgL_arg1206z00_155;
							BgL_backendz00_176 = BgL_backendz00_154;
						BgL_zc3z04anonymousza31221ze3z87_177:
							{	/* Cc/ld.scm 107 */
								obj_t BgL_arg1223z00_178;

								{	/* Cc/ld.scm 107 */
									bool_t BgL_test2432z00_2169;

									{	/* Cc/ld.scm 107 */
										bool_t BgL_test2433z00_2170;

										{	/* Cc/ld.scm 107 */
											obj_t BgL_string1z00_1478;

											BgL_string1z00_1478 = string_to_bstring(OS_CLASS);
											{	/* Cc/ld.scm 107 */
												long BgL_l1z00_1480;

												BgL_l1z00_1480 = STRING_LENGTH(BgL_string1z00_1478);
												if ((BgL_l1z00_1480 == 4L))
													{	/* Cc/ld.scm 107 */
														int BgL_arg1282z00_1483;

														{	/* Cc/ld.scm 107 */
															char *BgL_auxz00_2177;
															char *BgL_tmpz00_2175;

															BgL_auxz00_2177 =
																BSTRING_TO_STRING(BGl_string2339z00zzcc_ldz00);
															BgL_tmpz00_2175 =
																BSTRING_TO_STRING(BgL_string1z00_1478);
															BgL_arg1282z00_1483 =
																memcmp(BgL_tmpz00_2175, BgL_auxz00_2177,
																BgL_l1z00_1480);
														}
														BgL_test2433z00_2170 =
															((long) (BgL_arg1282z00_1483) == 0L);
													}
												else
													{	/* Cc/ld.scm 107 */
														BgL_test2433z00_2170 = ((bool_t) 0);
													}
											}
										}
										if (BgL_test2433z00_2170)
											{	/* Cc/ld.scm 107 */
												BgL_test2432z00_2169 = ((bool_t) 1);
											}
										else
											{	/* Cc/ld.scm 108 */
												obj_t BgL_string1z00_1489;

												BgL_string1z00_1489 = string_to_bstring(OS_CLASS);
												{	/* Cc/ld.scm 108 */
													long BgL_l1z00_1491;

													BgL_l1z00_1491 = STRING_LENGTH(BgL_string1z00_1489);
													if ((BgL_l1z00_1491 == 5L))
														{	/* Cc/ld.scm 108 */
															int BgL_arg1282z00_1494;

															{	/* Cc/ld.scm 108 */
																char *BgL_auxz00_2188;
																char *BgL_tmpz00_2186;

																BgL_auxz00_2188 =
																	BSTRING_TO_STRING
																	(BGl_string2340z00zzcc_ldz00);
																BgL_tmpz00_2186 =
																	BSTRING_TO_STRING(BgL_string1z00_1489);
																BgL_arg1282z00_1494 =
																	memcmp(BgL_tmpz00_2186, BgL_auxz00_2188,
																	BgL_l1z00_1491);
															}
															BgL_test2432z00_2169 =
																((long) (BgL_arg1282z00_1494) == 0L);
														}
													else
														{	/* Cc/ld.scm 108 */
															BgL_test2432z00_2169 = ((bool_t) 0);
														}
												}
											}
									}
									if (BgL_test2432z00_2169)
										{	/* Cc/ld.scm 107 */
											BgL_arg1223z00_178 = BGl_string2348z00zzcc_ldz00;
										}
									else
										{	/* Cc/ld.scm 110 */
											bool_t BgL_test2436z00_2193;

											{	/* Cc/ld.scm 110 */
												obj_t BgL_string1z00_1500;

												BgL_string1z00_1500 = string_to_bstring(OS_CLASS);
												{	/* Cc/ld.scm 110 */
													long BgL_l1z00_1502;

													BgL_l1z00_1502 = STRING_LENGTH(BgL_string1z00_1500);
													if ((BgL_l1z00_1502 == 5L))
														{	/* Cc/ld.scm 110 */
															int BgL_arg1282z00_1505;

															{	/* Cc/ld.scm 110 */
																char *BgL_auxz00_2200;
																char *BgL_tmpz00_2198;

																BgL_auxz00_2200 =
																	BSTRING_TO_STRING
																	(BGl_string2341z00zzcc_ldz00);
																BgL_tmpz00_2198 =
																	BSTRING_TO_STRING(BgL_string1z00_1500);
																BgL_arg1282z00_1505 =
																	memcmp(BgL_tmpz00_2198, BgL_auxz00_2200,
																	BgL_l1z00_1502);
															}
															BgL_test2436z00_2193 =
																((long) (BgL_arg1282z00_1505) == 0L);
														}
													else
														{	/* Cc/ld.scm 110 */
															BgL_test2436z00_2193 = ((bool_t) 0);
														}
												}
											}
											if (BgL_test2436z00_2193)
												{	/* Cc/ld.scm 110 */
													BgL_arg1223z00_178 = BGl_string2347z00zzcc_ldz00;
												}
											else
												{	/* Cc/ld.scm 110 */
													BgL_arg1223z00_178 =
														BGl_userzd2errorzd2zztools_errorz00
														(BGl_string2342z00zzcc_ldz00,
														BGl_string2343z00zzcc_ldz00,
														string_to_bstring(OS_CLASS), BNIL);
												}
										}
								}
								return string_append(BgL_arg1223z00_178, BgL_libraryz00_175);
							}
						}
					else
						{
							obj_t BgL_ssz00_157;

							BgL_ssz00_157 = BgL_suffixesz00_7;
						BgL_zc3z04anonymousza31207ze3z87_158:
							if (NULLP(BgL_ssz00_157))
								{	/* Cc/ld.scm 120 */
									if (CBOOL(BgL_staticpz00_8))
										{	/* Cc/ld.scm 123 */
											obj_t BgL_arg1209z00_160;

											{	/* Cc/ld.scm 123 */
												obj_t BgL_list1210z00_161;

												BgL_list1210z00_161 =
													MAKE_YOUNG_PAIR(BgL_libraryz00_6, BNIL);
												BgL_arg1209z00_160 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string2349z00zzcc_ldz00, BgL_list1210z00_161);
											}
											return
												BGl_errorz00zz__errorz00(BGl_string2350z00zzcc_ldz00,
												BgL_arg1209z00_160,
												BGl_za2libzd2dirza2zd2zzengine_paramz00);
										}
									else
										{
											obj_t BgL_staticpz00_2215;

											BgL_staticpz00_2215 = BTRUE;
											BgL_staticpz00_8 = BgL_staticpz00_2215;
											goto BGl_libraryzd2ze3oszd2fileze3zzcc_ldz00;
										}
								}
							else
								{	/* Cc/ld.scm 126 */
									obj_t BgL_lnamez00_162;

									{	/* Cc/ld.scm 126 */
										obj_t BgL_arg1219z00_172;

										BgL_arg1219z00_172 = CAR(((obj_t) BgL_ssz00_157));
										BgL_lnamez00_162 =
											BGl_libraryzd2filezd2namez00zz__libraryz00
											(BgL_libraryz00_6, BgL_arg1219z00_172,
											BgL_backendz00_154);
									}
									{	/* Cc/ld.scm 126 */
										obj_t BgL_fnamez00_163;

										if (CBOOL(BgL_staticpz00_8))
											{	/* Cc/ld.scm 127 */
												BgL_fnamez00_163 =
													BGl_makezd2staticzd2libzd2namezd2zz__osz00
													(BgL_lnamez00_162, BgL_backendz00_154);
											}
										else
											{	/* Cc/ld.scm 127 */
												BgL_fnamez00_163 =
													BGl_makezd2sharedzd2libzd2namezd2zz__osz00
													(BgL_lnamez00_162, BgL_backendz00_154);
											}
										{	/* Cc/ld.scm 127 */
											obj_t BgL_namez00_164;

											BgL_namez00_164 =
												BGl_findzd2filezf2pathz20zz__osz00(BgL_fnamez00_163,
												BGl_za2libzd2dirza2zd2zzengine_paramz00);
											{	/* Cc/ld.scm 131 */

												if (STRINGP(BgL_namez00_164))
													{	/* Cc/ld.scm 133 */
														bool_t BgL_test2442z00_2226;

														if (CBOOL
															(BGl_za2ldzd2relativeza2zd2zzengine_paramz00))
															{	/* Cc/ld.scm 133 */
																if (CBOOL
																	(BGl_za2profilezd2libraryza2zd2zzengine_paramz00))
																	{	/* Cc/ld.scm 134 */
																		BgL_test2442z00_2226 = ((bool_t) 0);
																	}
																else
																	{	/* Cc/ld.scm 135 */
																		bool_t BgL_test2445z00_2231;

																		if (CBOOL(BgL_staticpz00_8))
																			{	/* Cc/ld.scm 135 */
																				BgL_test2445z00_2231 = ((bool_t) 1);
																			}
																		else
																			{	/* Cc/ld.scm 135 */
																				if (CBOOL
																					(BGl_za2staticzd2allzd2bigloozf3za2zf3zzengine_paramz00))
																					{	/* Cc/ld.scm 135 */
																						BgL_test2445z00_2231 = ((bool_t) 1);
																					}
																				else
																					{	/* Cc/ld.scm 135 */
																						BgL_test2445z00_2231 =
																							CBOOL
																							(BGl_za2staticzd2bigloozf3za2z21zzengine_paramz00);
																					}
																			}
																		if (BgL_test2445z00_2231)
																			{	/* Cc/ld.scm 135 */
																				BgL_test2442z00_2226 = ((bool_t) 0);
																			}
																		else
																			{	/* Cc/ld.scm 135 */
																				BgL_test2442z00_2226 = ((bool_t) 1);
																			}
																	}
															}
														else
															{	/* Cc/ld.scm 133 */
																BgL_test2442z00_2226 = ((bool_t) 0);
															}
														if (BgL_test2442z00_2226)
															{
																obj_t BgL_backendz00_2238;
																obj_t BgL_libraryz00_2237;

																BgL_libraryz00_2237 = BgL_lnamez00_162;
																BgL_backendz00_2238 = BgL_backendz00_154;
																BgL_backendz00_176 = BgL_backendz00_2238;
																BgL_libraryz00_175 = BgL_libraryz00_2237;
																goto BgL_zc3z04anonymousza31221ze3z87_177;
															}
														else
															{	/* Cc/ld.scm 133 */
																return BgL_namez00_164;
															}
													}
												else
													{	/* Cc/ld.scm 132 */
														{	/* Cc/ld.scm 142 */
															obj_t BgL_arg1215z00_169;

															{	/* Cc/ld.scm 142 */
																obj_t BgL_list1216z00_170;

																BgL_list1216z00_170 =
																	MAKE_YOUNG_PAIR(BgL_fnamez00_163, BNIL);
																BgL_arg1215z00_169 =
																	BGl_formatz00zz__r4_output_6_10_3z00
																	(BGl_string2351z00zzcc_ldz00,
																	BgL_list1216z00_170);
															}
															BGl_userzd2warningzd2zztools_errorz00
																(BGl_string2342z00zzcc_ldz00,
																BgL_arg1215z00_169,
																BGl_za2libzd2dirza2zd2zzengine_paramz00);
														}
														{	/* Cc/ld.scm 144 */
															obj_t BgL_arg1218z00_171;

															BgL_arg1218z00_171 = CDR(((obj_t) BgL_ssz00_157));
															{
																obj_t BgL_ssz00_2244;

																BgL_ssz00_2244 = BgL_arg1218z00_171;
																BgL_ssz00_157 = BgL_ssz00_2244;
																goto BgL_zc3z04anonymousza31207ze3z87_158;
															}
														}
													}
											}
										}
									}
								}
						}
				}
			}
		}

	}



/* unix-ld */
	obj_t BGl_unixzd2ldzd2zzcc_ldz00(obj_t BgL_namez00_11,
		bool_t BgL_needzd2tozd2returnz00_12)
	{
		{	/* Cc/ld.scm 149 */
			{

				{	/* Cc/ld.scm 173 */
					obj_t BgL_compz00_195;

					if ((BGl_za2passza2z00zzengine_paramz00 == CNST_TABLE_REF(14)))
						{	/* Cc/ld.scm 173 */
							BgL_compz00_195 =
								BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(15));
						}
					else
						{	/* Cc/ld.scm 173 */
							BgL_compz00_195 = BGl_za2ccza2z00zzengine_paramz00;
						}
					{	/* Cc/ld.scm 177 */
						obj_t BgL_list1239z00_196;

						{	/* Cc/ld.scm 177 */
							obj_t BgL_arg1242z00_197;

							{	/* Cc/ld.scm 177 */
								obj_t BgL_arg1244z00_198;

								{	/* Cc/ld.scm 177 */
									obj_t BgL_arg1248z00_199;

									BgL_arg1248z00_199 =
										MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
									BgL_arg1244z00_198 =
										MAKE_YOUNG_PAIR(BGl_string2353z00zzcc_ldz00,
										BgL_arg1248z00_199);
								}
								BgL_arg1242z00_197 =
									MAKE_YOUNG_PAIR(BgL_compz00_195, BgL_arg1244z00_198);
							}
							BgL_list1239z00_196 =
								MAKE_YOUNG_PAIR(BGl_string2354z00zzcc_ldz00,
								BgL_arg1242z00_197);
						}
						BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1239z00_196);
					}
					{	/* Cc/ld.scm 179 */
						obj_t BgL_staticpz00_200;

						{	/* Cc/ld.scm 179 */
							bool_t BgL__ortest_1048z00_388;

							if (CBOOL(BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
										(16))))
								{	/* Cc/ld.scm 179 */
									BgL__ortest_1048z00_388 = ((bool_t) 0);
								}
							else
								{	/* Cc/ld.scm 179 */
									BgL__ortest_1048z00_388 = ((bool_t) 1);
								}
							if (BgL__ortest_1048z00_388)
								{	/* Cc/ld.scm 179 */
									BgL_staticpz00_200 = BBOOL(BgL__ortest_1048z00_388);
								}
							else
								{	/* Cc/ld.scm 180 */
									obj_t BgL_port1049z00_389;

									{	/* Cc/ld.scm 180 */
										obj_t BgL_stringz00_813;

										BgL_stringz00_813 =
											BGl_za2ldzd2optionsza2zd2zzengine_paramz00;
										{	/* Cc/ld.scm 180 */
											long BgL_endz00_815;

											BgL_endz00_815 =
												STRING_LENGTH(((obj_t) BgL_stringz00_813));
											{	/* Cc/ld.scm 180 */

												BgL_port1049z00_389 =
													BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
													(BgL_stringz00_813, BINT(0L), BINT(BgL_endz00_815));
									}}}
									{	/* Cc/ld.scm 180 */
										obj_t BgL_exitd1050z00_390;

										BgL_exitd1050z00_390 = BGL_EXITD_TOP_AS_OBJ();
										{	/* Cc/ld.scm 180 */
											obj_t BgL_zc3z04anonymousza31979ze3z87_1957;

											BgL_zc3z04anonymousza31979ze3z87_1957 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31979ze3ze5zzcc_ldz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31979ze3z87_1957,
												(int) (0L), BgL_port1049z00_389);
											{	/* Cc/ld.scm 180 */
												obj_t BgL_arg1828z00_1530;

												{	/* Cc/ld.scm 180 */
													obj_t BgL_arg1829z00_1531;

													BgL_arg1829z00_1531 =
														BGL_EXITD_PROTECT(BgL_exitd1050z00_390);
													BgL_arg1828z00_1530 =
														MAKE_YOUNG_PAIR
														(BgL_zc3z04anonymousza31979ze3z87_1957,
														BgL_arg1829z00_1531);
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1050z00_390,
													BgL_arg1828z00_1530);
												BUNSPEC;
											}
											{	/* Cc/ld.scm 180 */
												obj_t BgL_tmp1052z00_392;

												{
													obj_t BgL_iportz00_434;
													long BgL_lastzd2matchzd2_435;
													long BgL_forwardz00_436;
													long BgL_bufposz00_437;
													obj_t BgL_iportz00_451;
													long BgL_lastzd2matchzd2_452;
													long BgL_forwardz00_453;
													long BgL_bufposz00_454;
													obj_t BgL_iportz00_472;
													long BgL_lastzd2matchzd2_473;
													long BgL_forwardz00_474;
													long BgL_bufposz00_475;
													obj_t BgL_iportz00_488;
													long BgL_lastzd2matchzd2_489;
													long BgL_forwardz00_490;
													long BgL_bufposz00_491;
													obj_t BgL_iportz00_508;
													long BgL_lastzd2matchzd2_509;
													long BgL_forwardz00_510;
													long BgL_bufposz00_511;
													obj_t BgL_iportz00_528;
													long BgL_lastzd2matchzd2_529;
													long BgL_forwardz00_530;
													long BgL_bufposz00_531;
													obj_t BgL_iportz00_548;
													long BgL_lastzd2matchzd2_549;
													long BgL_forwardz00_550;
													long BgL_bufposz00_551;
													obj_t BgL_iportz00_568;
													long BgL_lastzd2matchzd2_569;
													long BgL_forwardz00_570;
													long BgL_bufposz00_571;
													obj_t BgL_iportz00_588;
													long BgL_lastzd2matchzd2_589;
													long BgL_forwardz00_590;
													long BgL_bufposz00_591;
													obj_t BgL_iportz00_608;
													long BgL_lastzd2matchzd2_609;
													long BgL_forwardz00_610;
													long BgL_bufposz00_611;
													obj_t BgL_iportz00_631;
													long BgL_lastzd2matchzd2_632;
													long BgL_forwardz00_633;
													long BgL_bufposz00_634;

													RGC_START_MATCH(BgL_port1049z00_389);
													{	/* Cc/ld.scm 180 */
														long BgL_matchz00_765;

														{	/* Cc/ld.scm 180 */
															long BgL_arg1977z00_770;
															long BgL_arg1978z00_771;

															BgL_arg1977z00_770 =
																RGC_BUFFER_FORWARD(BgL_port1049z00_389);
															BgL_arg1978z00_771 =
																RGC_BUFFER_BUFPOS(BgL_port1049z00_389);
															BgL_iportz00_631 = BgL_port1049z00_389;
															BgL_lastzd2matchzd2_632 = 1L;
															BgL_forwardz00_633 = BgL_arg1977z00_770;
															BgL_bufposz00_634 = BgL_arg1978z00_771;
														BgL_zc3z04anonymousza31875ze3z87_635:
															if ((BgL_forwardz00_633 == BgL_bufposz00_634))
																{	/* Cc/ld.scm 180 */
																	if (rgc_fill_buffer(BgL_iportz00_631))
																		{	/* Cc/ld.scm 180 */
																			long BgL_arg1878z00_638;
																			long BgL_arg1879z00_639;

																			BgL_arg1878z00_638 =
																				RGC_BUFFER_FORWARD(BgL_iportz00_631);
																			BgL_arg1879z00_639 =
																				RGC_BUFFER_BUFPOS(BgL_iportz00_631);
																			{
																				long BgL_bufposz00_2287;
																				long BgL_forwardz00_2286;

																				BgL_forwardz00_2286 =
																					BgL_arg1878z00_638;
																				BgL_bufposz00_2287 = BgL_arg1879z00_639;
																				BgL_bufposz00_634 = BgL_bufposz00_2287;
																				BgL_forwardz00_633 =
																					BgL_forwardz00_2286;
																				goto
																					BgL_zc3z04anonymousza31875ze3z87_635;
																			}
																		}
																	else
																		{	/* Cc/ld.scm 180 */
																			BgL_matchz00_765 =
																				BgL_lastzd2matchzd2_632;
																		}
																}
															else
																{	/* Cc/ld.scm 180 */
																	int BgL_curz00_640;

																	BgL_curz00_640 =
																		RGC_BUFFER_GET_CHAR(BgL_iportz00_631,
																		BgL_forwardz00_633);
																	{	/* Cc/ld.scm 180 */

																		if (((long) (BgL_curz00_640) == 45L))
																			{	/* Cc/ld.scm 180 */
																				BgL_iportz00_451 = BgL_iportz00_631;
																				BgL_lastzd2matchzd2_452 =
																					BgL_lastzd2matchzd2_632;
																				BgL_forwardz00_453 =
																					(1L + BgL_forwardz00_633);
																				BgL_bufposz00_454 = BgL_bufposz00_634;
																			BgL_zc3z04anonymousza31689ze3z87_455:
																				{	/* Cc/ld.scm 180 */
																					long BgL_newzd2matchzd2_456;

																					RGC_STOP_MATCH(BgL_iportz00_451,
																						BgL_forwardz00_453);
																					BgL_newzd2matchzd2_456 = 1L;
																					if (
																						(BgL_forwardz00_453 ==
																							BgL_bufposz00_454))
																						{	/* Cc/ld.scm 180 */
																							if (rgc_fill_buffer
																								(BgL_iportz00_451))
																								{	/* Cc/ld.scm 180 */
																									long BgL_arg1692z00_459;
																									long BgL_arg1699z00_460;

																									BgL_arg1692z00_459 =
																										RGC_BUFFER_FORWARD
																										(BgL_iportz00_451);
																									BgL_arg1699z00_460 =
																										RGC_BUFFER_BUFPOS
																										(BgL_iportz00_451);
																									{
																										long BgL_bufposz00_2300;
																										long BgL_forwardz00_2299;

																										BgL_forwardz00_2299 =
																											BgL_arg1692z00_459;
																										BgL_bufposz00_2300 =
																											BgL_arg1699z00_460;
																										BgL_bufposz00_454 =
																											BgL_bufposz00_2300;
																										BgL_forwardz00_453 =
																											BgL_forwardz00_2299;
																										goto
																											BgL_zc3z04anonymousza31689ze3z87_455;
																									}
																								}
																							else
																								{	/* Cc/ld.scm 180 */
																									BgL_matchz00_765 =
																										BgL_newzd2matchzd2_456;
																								}
																						}
																					else
																						{	/* Cc/ld.scm 180 */
																							int BgL_curz00_461;

																							BgL_curz00_461 =
																								RGC_BUFFER_GET_CHAR
																								(BgL_iportz00_451,
																								BgL_forwardz00_453);
																							{	/* Cc/ld.scm 180 */

																								if (
																									((long) (BgL_curz00_461) ==
																										115L))
																									{	/* Cc/ld.scm 180 */
																										BgL_iportz00_508 =
																											BgL_iportz00_451;
																										BgL_lastzd2matchzd2_509 =
																											BgL_newzd2matchzd2_456;
																										BgL_forwardz00_510 =
																											(1L + BgL_forwardz00_453);
																										BgL_bufposz00_511 =
																											BgL_bufposz00_454;
																									BgL_zc3z04anonymousza31747ze3z87_512:
																										if (
																											(BgL_forwardz00_510 ==
																												BgL_bufposz00_511))
																											{	/* Cc/ld.scm 180 */
																												if (rgc_fill_buffer
																													(BgL_iportz00_508))
																													{	/* Cc/ld.scm 180 */
																														long
																															BgL_arg1751z00_515;
																														long
																															BgL_arg1752z00_516;
																														BgL_arg1751z00_515 =
																															RGC_BUFFER_FORWARD
																															(BgL_iportz00_508);
																														BgL_arg1752z00_516 =
																															RGC_BUFFER_BUFPOS
																															(BgL_iportz00_508);
																														{
																															long
																																BgL_bufposz00_2312;
																															long
																																BgL_forwardz00_2311;
																															BgL_forwardz00_2311
																																=
																																BgL_arg1751z00_515;
																															BgL_bufposz00_2312
																																=
																																BgL_arg1752z00_516;
																															BgL_bufposz00_511
																																=
																																BgL_bufposz00_2312;
																															BgL_forwardz00_510
																																=
																																BgL_forwardz00_2311;
																															goto
																																BgL_zc3z04anonymousza31747ze3z87_512;
																														}
																													}
																												else
																													{	/* Cc/ld.scm 180 */
																														BgL_matchz00_765 =
																															BgL_lastzd2matchzd2_509;
																													}
																											}
																										else
																											{	/* Cc/ld.scm 180 */
																												int BgL_curz00_517;

																												BgL_curz00_517 =
																													RGC_BUFFER_GET_CHAR
																													(BgL_iportz00_508,
																													BgL_forwardz00_510);
																												{	/* Cc/ld.scm 180 */

																													if (
																														((long)
																															(BgL_curz00_517)
																															== 116L))
																														{	/* Cc/ld.scm 180 */
																															BgL_iportz00_528 =
																																BgL_iportz00_508;
																															BgL_lastzd2matchzd2_529
																																=
																																BgL_lastzd2matchzd2_509;
																															BgL_forwardz00_530
																																=
																																(1L +
																																BgL_forwardz00_510);
																															BgL_bufposz00_531
																																=
																																BgL_bufposz00_511;
																														BgL_zc3z04anonymousza31768ze3z87_532:
																															if (
																																(BgL_forwardz00_530
																																	==
																																	BgL_bufposz00_531))
																																{	/* Cc/ld.scm 180 */
																																	if (rgc_fill_buffer(BgL_iportz00_528))
																																		{	/* Cc/ld.scm 180 */
																																			long
																																				BgL_arg1771z00_535;
																																			long
																																				BgL_arg1773z00_536;
																																			BgL_arg1771z00_535
																																				=
																																				RGC_BUFFER_FORWARD
																																				(BgL_iportz00_528);
																																			BgL_arg1773z00_536
																																				=
																																				RGC_BUFFER_BUFPOS
																																				(BgL_iportz00_528);
																																			{
																																				long
																																					BgL_bufposz00_2324;
																																				long
																																					BgL_forwardz00_2323;
																																				BgL_forwardz00_2323
																																					=
																																					BgL_arg1771z00_535;
																																				BgL_bufposz00_2324
																																					=
																																					BgL_arg1773z00_536;
																																				BgL_bufposz00_531
																																					=
																																					BgL_bufposz00_2324;
																																				BgL_forwardz00_530
																																					=
																																					BgL_forwardz00_2323;
																																				goto
																																					BgL_zc3z04anonymousza31768ze3z87_532;
																																			}
																																		}
																																	else
																																		{	/* Cc/ld.scm 180 */
																																			BgL_matchz00_765
																																				=
																																				BgL_lastzd2matchzd2_529;
																																		}
																																}
																															else
																																{	/* Cc/ld.scm 180 */
																																	int
																																		BgL_curz00_537;
																																	BgL_curz00_537
																																		=
																																		RGC_BUFFER_GET_CHAR
																																		(BgL_iportz00_528,
																																		BgL_forwardz00_530);
																																	{	/* Cc/ld.scm 180 */

																																		if (
																																			((long)
																																				(BgL_curz00_537)
																																				== 97L))
																																			{	/* Cc/ld.scm 180 */
																																				BgL_iportz00_548
																																					=
																																					BgL_iportz00_528;
																																				BgL_lastzd2matchzd2_549
																																					=
																																					BgL_lastzd2matchzd2_529;
																																				BgL_forwardz00_550
																																					=
																																					(1L +
																																					BgL_forwardz00_530);
																																				BgL_bufposz00_551
																																					=
																																					BgL_bufposz00_531;
																																			BgL_zc3z04anonymousza31806ze3z87_552:
																																				if (
																																					(BgL_forwardz00_550
																																						==
																																						BgL_bufposz00_551))
																																					{	/* Cc/ld.scm 180 */
																																						if (rgc_fill_buffer(BgL_iportz00_548))
																																							{	/* Cc/ld.scm 180 */
																																								long
																																									BgL_arg1812z00_555;
																																								long
																																									BgL_arg1820z00_556;
																																								BgL_arg1812z00_555
																																									=
																																									RGC_BUFFER_FORWARD
																																									(BgL_iportz00_548);
																																								BgL_arg1820z00_556
																																									=
																																									RGC_BUFFER_BUFPOS
																																									(BgL_iportz00_548);
																																								{
																																									long
																																										BgL_bufposz00_2336;
																																									long
																																										BgL_forwardz00_2335;
																																									BgL_forwardz00_2335
																																										=
																																										BgL_arg1812z00_555;
																																									BgL_bufposz00_2336
																																										=
																																										BgL_arg1820z00_556;
																																									BgL_bufposz00_551
																																										=
																																										BgL_bufposz00_2336;
																																									BgL_forwardz00_550
																																										=
																																										BgL_forwardz00_2335;
																																									goto
																																										BgL_zc3z04anonymousza31806ze3z87_552;
																																								}
																																							}
																																						else
																																							{	/* Cc/ld.scm 180 */
																																								BgL_matchz00_765
																																									=
																																									BgL_lastzd2matchzd2_549;
																																							}
																																					}
																																				else
																																					{	/* Cc/ld.scm 180 */
																																						int
																																							BgL_curz00_557;
																																						BgL_curz00_557
																																							=
																																							RGC_BUFFER_GET_CHAR
																																							(BgL_iportz00_548,
																																							BgL_forwardz00_550);
																																						{	/* Cc/ld.scm 180 */

																																							if (((long) (BgL_curz00_557) == 116L))
																																								{	/* Cc/ld.scm 180 */
																																									BgL_iportz00_568
																																										=
																																										BgL_iportz00_548;
																																									BgL_lastzd2matchzd2_569
																																										=
																																										BgL_lastzd2matchzd2_549;
																																									BgL_forwardz00_570
																																										=
																																										(1L
																																										+
																																										BgL_forwardz00_550);
																																									BgL_bufposz00_571
																																										=
																																										BgL_bufposz00_551;
																																								BgL_zc3z04anonymousza31836ze3z87_572:
																																									if ((BgL_forwardz00_570 == BgL_bufposz00_571))
																																										{	/* Cc/ld.scm 180 */
																																											if (rgc_fill_buffer(BgL_iportz00_568))
																																												{	/* Cc/ld.scm 180 */
																																													long
																																														BgL_arg1839z00_575;
																																													long
																																														BgL_arg1840z00_576;
																																													BgL_arg1839z00_575
																																														=
																																														RGC_BUFFER_FORWARD
																																														(BgL_iportz00_568);
																																													BgL_arg1840z00_576
																																														=
																																														RGC_BUFFER_BUFPOS
																																														(BgL_iportz00_568);
																																													{
																																														long
																																															BgL_bufposz00_2348;
																																														long
																																															BgL_forwardz00_2347;
																																														BgL_forwardz00_2347
																																															=
																																															BgL_arg1839z00_575;
																																														BgL_bufposz00_2348
																																															=
																																															BgL_arg1840z00_576;
																																														BgL_bufposz00_571
																																															=
																																															BgL_bufposz00_2348;
																																														BgL_forwardz00_570
																																															=
																																															BgL_forwardz00_2347;
																																														goto
																																															BgL_zc3z04anonymousza31836ze3z87_572;
																																													}
																																												}
																																											else
																																												{	/* Cc/ld.scm 180 */
																																													BgL_matchz00_765
																																														=
																																														BgL_lastzd2matchzd2_569;
																																												}
																																										}
																																									else
																																										{	/* Cc/ld.scm 180 */
																																											int
																																												BgL_curz00_577;
																																											BgL_curz00_577
																																												=
																																												RGC_BUFFER_GET_CHAR
																																												(BgL_iportz00_568,
																																												BgL_forwardz00_570);
																																											{	/* Cc/ld.scm 180 */

																																												if (((long) (BgL_curz00_577) == 105L))
																																													{	/* Cc/ld.scm 180 */
																																														BgL_iportz00_588
																																															=
																																															BgL_iportz00_568;
																																														BgL_lastzd2matchzd2_589
																																															=
																																															BgL_lastzd2matchzd2_569;
																																														BgL_forwardz00_590
																																															=
																																															(1L
																																															+
																																															BgL_forwardz00_570);
																																														BgL_bufposz00_591
																																															=
																																															BgL_bufposz00_571;
																																													BgL_zc3z04anonymousza31849ze3z87_592:
																																														if ((BgL_forwardz00_590 == BgL_bufposz00_591))
																																															{	/* Cc/ld.scm 180 */
																																																if (rgc_fill_buffer(BgL_iportz00_588))
																																																	{	/* Cc/ld.scm 180 */
																																																		long
																																																			BgL_arg1852z00_595;
																																																		long
																																																			BgL_arg1853z00_596;
																																																		BgL_arg1852z00_595
																																																			=
																																																			RGC_BUFFER_FORWARD
																																																			(BgL_iportz00_588);
																																																		BgL_arg1853z00_596
																																																			=
																																																			RGC_BUFFER_BUFPOS
																																																			(BgL_iportz00_588);
																																																		{
																																																			long
																																																				BgL_bufposz00_2360;
																																																			long
																																																				BgL_forwardz00_2359;
																																																			BgL_forwardz00_2359
																																																				=
																																																				BgL_arg1852z00_595;
																																																			BgL_bufposz00_2360
																																																				=
																																																				BgL_arg1853z00_596;
																																																			BgL_bufposz00_591
																																																				=
																																																				BgL_bufposz00_2360;
																																																			BgL_forwardz00_590
																																																				=
																																																				BgL_forwardz00_2359;
																																																			goto
																																																				BgL_zc3z04anonymousza31849ze3z87_592;
																																																		}
																																																	}
																																																else
																																																	{	/* Cc/ld.scm 180 */
																																																		BgL_matchz00_765
																																																			=
																																																			BgL_lastzd2matchzd2_589;
																																																	}
																																															}
																																														else
																																															{	/* Cc/ld.scm 180 */
																																																int
																																																	BgL_curz00_597;
																																																BgL_curz00_597
																																																	=
																																																	RGC_BUFFER_GET_CHAR
																																																	(BgL_iportz00_588,
																																																	BgL_forwardz00_590);
																																																{	/* Cc/ld.scm 180 */

																																																	if (((long) (BgL_curz00_597) == 99L))
																																																		{	/* Cc/ld.scm 180 */
																																																			BgL_iportz00_608
																																																				=
																																																				BgL_iportz00_588;
																																																			BgL_lastzd2matchzd2_609
																																																				=
																																																				BgL_lastzd2matchzd2_589;
																																																			BgL_forwardz00_610
																																																				=
																																																				(1L
																																																				+
																																																				BgL_forwardz00_590);
																																																			BgL_bufposz00_611
																																																				=
																																																				BgL_bufposz00_591;
																																																		BgL_zc3z04anonymousza31863ze3z87_612:
																																																			{	/* Cc/ld.scm 180 */
																																																				long
																																																					BgL_newzd2matchzd2_613;
																																																				RGC_STOP_MATCH
																																																					(BgL_iportz00_608,
																																																					BgL_forwardz00_610);
																																																				BgL_newzd2matchzd2_613
																																																					=
																																																					0L;
																																																				if ((BgL_forwardz00_610 == BgL_bufposz00_611))
																																																					{	/* Cc/ld.scm 180 */
																																																						if (rgc_fill_buffer(BgL_iportz00_608))
																																																							{	/* Cc/ld.scm 180 */
																																																								long
																																																									BgL_arg1866z00_616;
																																																								long
																																																									BgL_arg1868z00_617;
																																																								BgL_arg1866z00_616
																																																									=
																																																									RGC_BUFFER_FORWARD
																																																									(BgL_iportz00_608);
																																																								BgL_arg1868z00_617
																																																									=
																																																									RGC_BUFFER_BUFPOS
																																																									(BgL_iportz00_608);
																																																								{
																																																									long
																																																										BgL_bufposz00_2373;
																																																									long
																																																										BgL_forwardz00_2372;
																																																									BgL_forwardz00_2372
																																																										=
																																																										BgL_arg1866z00_616;
																																																									BgL_bufposz00_2373
																																																										=
																																																										BgL_arg1868z00_617;
																																																									BgL_bufposz00_611
																																																										=
																																																										BgL_bufposz00_2373;
																																																									BgL_forwardz00_610
																																																										=
																																																										BgL_forwardz00_2372;
																																																									goto
																																																										BgL_zc3z04anonymousza31863ze3z87_612;
																																																								}
																																																							}
																																																						else
																																																							{	/* Cc/ld.scm 180 */
																																																								BgL_matchz00_765
																																																									=
																																																									BgL_newzd2matchzd2_613;
																																																							}
																																																					}
																																																				else
																																																					{	/* Cc/ld.scm 180 */
																																																						int
																																																							BgL_curz00_618;
																																																						BgL_curz00_618
																																																							=
																																																							RGC_BUFFER_GET_CHAR
																																																							(BgL_iportz00_608,
																																																							BgL_forwardz00_610);
																																																						{	/* Cc/ld.scm 180 */

																																																							if (((long) (BgL_curz00_618) == 45L))
																																																								{	/* Cc/ld.scm 180 */
																																																									BgL_iportz00_488
																																																										=
																																																										BgL_iportz00_608;
																																																									BgL_lastzd2matchzd2_489
																																																										=
																																																										BgL_newzd2matchzd2_613;
																																																									BgL_forwardz00_490
																																																										=
																																																										(1L
																																																										+
																																																										BgL_forwardz00_610);
																																																									BgL_bufposz00_491
																																																										=
																																																										BgL_bufposz00_611;
																																																								BgL_zc3z04anonymousza31725ze3z87_492:
																																																									if ((BgL_forwardz00_490 == BgL_bufposz00_491))
																																																										{	/* Cc/ld.scm 180 */
																																																											if (rgc_fill_buffer(BgL_iportz00_488))
																																																												{	/* Cc/ld.scm 180 */
																																																													long
																																																														BgL_arg1733z00_495;
																																																													long
																																																														BgL_arg1734z00_496;
																																																													BgL_arg1733z00_495
																																																														=
																																																														RGC_BUFFER_FORWARD
																																																														(BgL_iportz00_488);
																																																													BgL_arg1734z00_496
																																																														=
																																																														RGC_BUFFER_BUFPOS
																																																														(BgL_iportz00_488);
																																																													{
																																																														long
																																																															BgL_bufposz00_2385;
																																																														long
																																																															BgL_forwardz00_2384;
																																																														BgL_forwardz00_2384
																																																															=
																																																															BgL_arg1733z00_495;
																																																														BgL_bufposz00_2385
																																																															=
																																																															BgL_arg1734z00_496;
																																																														BgL_bufposz00_491
																																																															=
																																																															BgL_bufposz00_2385;
																																																														BgL_forwardz00_490
																																																															=
																																																															BgL_forwardz00_2384;
																																																														goto
																																																															BgL_zc3z04anonymousza31725ze3z87_492;
																																																													}
																																																												}
																																																											else
																																																												{	/* Cc/ld.scm 180 */
																																																													BgL_matchz00_765
																																																														=
																																																														BgL_lastzd2matchzd2_489;
																																																												}
																																																										}
																																																									else
																																																										{	/* Cc/ld.scm 180 */
																																																											int
																																																												BgL_curz00_497;
																																																											BgL_curz00_497
																																																												=
																																																												RGC_BUFFER_GET_CHAR
																																																												(BgL_iportz00_488,
																																																												BgL_forwardz00_490);
																																																											{	/* Cc/ld.scm 180 */

																																																												if (((long) (BgL_curz00_497) == 115L))
																																																													{
																																																														long
																																																															BgL_bufposz00_2394;
																																																														long
																																																															BgL_forwardz00_2392;
																																																														long
																																																															BgL_lastzd2matchzd2_2391;
																																																														obj_t
																																																															BgL_iportz00_2390;
																																																														BgL_iportz00_2390
																																																															=
																																																															BgL_iportz00_488;
																																																														BgL_lastzd2matchzd2_2391
																																																															=
																																																															BgL_lastzd2matchzd2_489;
																																																														BgL_forwardz00_2392
																																																															=
																																																															(1L
																																																															+
																																																															BgL_forwardz00_490);
																																																														BgL_bufposz00_2394
																																																															=
																																																															BgL_bufposz00_491;
																																																														BgL_bufposz00_511
																																																															=
																																																															BgL_bufposz00_2394;
																																																														BgL_forwardz00_510
																																																															=
																																																															BgL_forwardz00_2392;
																																																														BgL_lastzd2matchzd2_509
																																																															=
																																																															BgL_lastzd2matchzd2_2391;
																																																														BgL_iportz00_508
																																																															=
																																																															BgL_iportz00_2390;
																																																														goto
																																																															BgL_zc3z04anonymousza31747ze3z87_512;
																																																													}
																																																												else
																																																													{	/* Cc/ld.scm 180 */
																																																														if (((long) (BgL_curz00_497) == 45L))
																																																															{
																																																																long
																																																																	BgL_forwardz00_2398;
																																																																BgL_forwardz00_2398
																																																																	=
																																																																	(1L
																																																																	+
																																																																	BgL_forwardz00_490);
																																																																BgL_forwardz00_490
																																																																	=
																																																																	BgL_forwardz00_2398;
																																																																goto
																																																																	BgL_zc3z04anonymousza31725ze3z87_492;
																																																															}
																																																														else
																																																															{	/* Cc/ld.scm 180 */
																																																																bool_t
																																																																	BgL_test2479z00_2400;
																																																																if (((long) (BgL_curz00_497) == 10L))
																																																																	{	/* Cc/ld.scm 180 */
																																																																		BgL_test2479z00_2400
																																																																			=
																																																																			(
																																																																			(bool_t)
																																																																			1);
																																																																	}
																																																																else
																																																																	{	/* Cc/ld.scm 180 */
																																																																		if (((long) (BgL_curz00_497) == 45L))
																																																																			{	/* Cc/ld.scm 180 */
																																																																				BgL_test2479z00_2400
																																																																					=
																																																																					(
																																																																					(bool_t)
																																																																					1);
																																																																			}
																																																																		else
																																																																			{	/* Cc/ld.scm 180 */
																																																																				BgL_test2479z00_2400
																																																																					=
																																																																					(
																																																																					(long)
																																																																					(BgL_curz00_497)
																																																																					==
																																																																					115L);
																																																																	}}
																																																																if (BgL_test2479z00_2400)
																																																																	{	/* Cc/ld.scm 180 */
																																																																		BgL_matchz00_765
																																																																			=
																																																																			BgL_lastzd2matchzd2_489;
																																																																	}
																																																																else
																																																																	{	/* Cc/ld.scm 180 */
																																																																		BgL_iportz00_472
																																																																			=
																																																																			BgL_iportz00_488;
																																																																		BgL_lastzd2matchzd2_473
																																																																			=
																																																																			BgL_lastzd2matchzd2_489;
																																																																		BgL_forwardz00_474
																																																																			=
																																																																			(1L
																																																																			+
																																																																			BgL_forwardz00_490);
																																																																		BgL_bufposz00_475
																																																																			=
																																																																			BgL_bufposz00_491;
																																																																	BgL_zc3z04anonymousza31709ze3z87_476:
																																																																		if ((BgL_forwardz00_474 == BgL_bufposz00_475))
																																																																			{	/* Cc/ld.scm 180 */
																																																																				if (rgc_fill_buffer(BgL_iportz00_472))
																																																																					{	/* Cc/ld.scm 180 */
																																																																						long
																																																																							BgL_arg1714z00_479;
																																																																						long
																																																																							BgL_arg1717z00_480;
																																																																						BgL_arg1714z00_479
																																																																							=
																																																																							RGC_BUFFER_FORWARD
																																																																							(BgL_iportz00_472);
																																																																						BgL_arg1717z00_480
																																																																							=
																																																																							RGC_BUFFER_BUFPOS
																																																																							(BgL_iportz00_472);
																																																																						{
																																																																							long
																																																																								BgL_bufposz00_2416;
																																																																							long
																																																																								BgL_forwardz00_2415;
																																																																							BgL_forwardz00_2415
																																																																								=
																																																																								BgL_arg1714z00_479;
																																																																							BgL_bufposz00_2416
																																																																								=
																																																																								BgL_arg1717z00_480;
																																																																							BgL_bufposz00_475
																																																																								=
																																																																								BgL_bufposz00_2416;
																																																																							BgL_forwardz00_474
																																																																								=
																																																																								BgL_forwardz00_2415;
																																																																							goto
																																																																								BgL_zc3z04anonymousza31709ze3z87_476;
																																																																						}
																																																																					}
																																																																				else
																																																																					{	/* Cc/ld.scm 180 */
																																																																						BgL_matchz00_765
																																																																							=
																																																																							BgL_lastzd2matchzd2_473;
																																																																					}
																																																																			}
																																																																		else
																																																																			{	/* Cc/ld.scm 180 */
																																																																				int
																																																																					BgL_curz00_481;
																																																																				BgL_curz00_481
																																																																					=
																																																																					RGC_BUFFER_GET_CHAR
																																																																					(BgL_iportz00_472,
																																																																					BgL_forwardz00_474);
																																																																				{	/* Cc/ld.scm 180 */

																																																																					if (((long) (BgL_curz00_481) == 45L))
																																																																						{
																																																																							long
																																																																								BgL_bufposz00_2425;
																																																																							long
																																																																								BgL_forwardz00_2423;
																																																																							long
																																																																								BgL_lastzd2matchzd2_2422;
																																																																							obj_t
																																																																								BgL_iportz00_2421;
																																																																							BgL_iportz00_2421
																																																																								=
																																																																								BgL_iportz00_472;
																																																																							BgL_lastzd2matchzd2_2422
																																																																								=
																																																																								BgL_lastzd2matchzd2_473;
																																																																							BgL_forwardz00_2423
																																																																								=
																																																																								(1L
																																																																								+
																																																																								BgL_forwardz00_474);
																																																																							BgL_bufposz00_2425
																																																																								=
																																																																								BgL_bufposz00_475;
																																																																							BgL_bufposz00_491
																																																																								=
																																																																								BgL_bufposz00_2425;
																																																																							BgL_forwardz00_490
																																																																								=
																																																																								BgL_forwardz00_2423;
																																																																							BgL_lastzd2matchzd2_489
																																																																								=
																																																																								BgL_lastzd2matchzd2_2422;
																																																																							BgL_iportz00_488
																																																																								=
																																																																								BgL_iportz00_2421;
																																																																							goto
																																																																								BgL_zc3z04anonymousza31725ze3z87_492;
																																																																						}
																																																																					else
																																																																						{	/* Cc/ld.scm 180 */
																																																																							bool_t
																																																																								BgL_test2485z00_2426;
																																																																							if (((long) (BgL_curz00_481) == 10L))
																																																																								{	/* Cc/ld.scm 180 */
																																																																									BgL_test2485z00_2426
																																																																										=
																																																																										(
																																																																										(bool_t)
																																																																										1);
																																																																								}
																																																																							else
																																																																								{	/* Cc/ld.scm 180 */
																																																																									BgL_test2485z00_2426
																																																																										=
																																																																										(
																																																																										(long)
																																																																										(BgL_curz00_481)
																																																																										==
																																																																										45L);
																																																																								}
																																																																							if (BgL_test2485z00_2426)
																																																																								{	/* Cc/ld.scm 180 */
																																																																									BgL_matchz00_765
																																																																										=
																																																																										BgL_lastzd2matchzd2_473;
																																																																								}
																																																																							else
																																																																								{
																																																																									long
																																																																										BgL_forwardz00_2432;
																																																																									BgL_forwardz00_2432
																																																																										=
																																																																										(1L
																																																																										+
																																																																										BgL_forwardz00_474);
																																																																									BgL_forwardz00_474
																																																																										=
																																																																										BgL_forwardz00_2432;
																																																																									goto
																																																																										BgL_zc3z04anonymousza31709ze3z87_476;
																																																																								}
																																																																						}
																																																																				}
																																																																			}
																																																																	}
																																																															}
																																																													}
																																																											}
																																																										}
																																																								}
																																																							else
																																																								{	/* Cc/ld.scm 180 */
																																																									bool_t
																																																										BgL_test2487z00_2436;
																																																									if (((long) (BgL_curz00_618) == 10L))
																																																										{	/* Cc/ld.scm 180 */
																																																											BgL_test2487z00_2436
																																																												=
																																																												(
																																																												(bool_t)
																																																												1);
																																																										}
																																																									else
																																																										{	/* Cc/ld.scm 180 */
																																																											BgL_test2487z00_2436
																																																												=
																																																												(
																																																												(long)
																																																												(BgL_curz00_618)
																																																												==
																																																												45L);
																																																										}
																																																									if (BgL_test2487z00_2436)
																																																										{	/* Cc/ld.scm 180 */
																																																											BgL_matchz00_765
																																																												=
																																																												BgL_newzd2matchzd2_613;
																																																										}
																																																									else
																																																										{
																																																											long
																																																												BgL_bufposz00_2446;
																																																											long
																																																												BgL_forwardz00_2444;
																																																											long
																																																												BgL_lastzd2matchzd2_2443;
																																																											obj_t
																																																												BgL_iportz00_2442;
																																																											BgL_iportz00_2442
																																																												=
																																																												BgL_iportz00_608;
																																																											BgL_lastzd2matchzd2_2443
																																																												=
																																																												BgL_newzd2matchzd2_613;
																																																											BgL_forwardz00_2444
																																																												=
																																																												(1L
																																																												+
																																																												BgL_forwardz00_610);
																																																											BgL_bufposz00_2446
																																																												=
																																																												BgL_bufposz00_611;
																																																											BgL_bufposz00_475
																																																												=
																																																												BgL_bufposz00_2446;
																																																											BgL_forwardz00_474
																																																												=
																																																												BgL_forwardz00_2444;
																																																											BgL_lastzd2matchzd2_473
																																																												=
																																																												BgL_lastzd2matchzd2_2443;
																																																											BgL_iportz00_472
																																																												=
																																																												BgL_iportz00_2442;
																																																											goto
																																																												BgL_zc3z04anonymousza31709ze3z87_476;
																																																										}
																																																								}
																																																						}
																																																					}
																																																			}
																																																		}
																																																	else
																																																		{	/* Cc/ld.scm 180 */
																																																			if (((long) (BgL_curz00_597) == 45L))
																																																				{
																																																					long
																																																						BgL_bufposz00_2455;
																																																					long
																																																						BgL_forwardz00_2453;
																																																					long
																																																						BgL_lastzd2matchzd2_2452;
																																																					obj_t
																																																						BgL_iportz00_2451;
																																																					BgL_iportz00_2451
																																																						=
																																																						BgL_iportz00_588;
																																																					BgL_lastzd2matchzd2_2452
																																																						=
																																																						BgL_lastzd2matchzd2_589;
																																																					BgL_forwardz00_2453
																																																						=
																																																						(1L
																																																						+
																																																						BgL_forwardz00_590);
																																																					BgL_bufposz00_2455
																																																						=
																																																						BgL_bufposz00_591;
																																																					BgL_bufposz00_491
																																																						=
																																																						BgL_bufposz00_2455;
																																																					BgL_forwardz00_490
																																																						=
																																																						BgL_forwardz00_2453;
																																																					BgL_lastzd2matchzd2_489
																																																						=
																																																						BgL_lastzd2matchzd2_2452;
																																																					BgL_iportz00_488
																																																						=
																																																						BgL_iportz00_2451;
																																																					goto
																																																						BgL_zc3z04anonymousza31725ze3z87_492;
																																																				}
																																																			else
																																																				{	/* Cc/ld.scm 180 */
																																																					bool_t
																																																						BgL_test2490z00_2456;
																																																					if (((long) (BgL_curz00_597) == 10L))
																																																						{	/* Cc/ld.scm 180 */
																																																							BgL_test2490z00_2456
																																																								=
																																																								(
																																																								(bool_t)
																																																								1);
																																																						}
																																																					else
																																																						{	/* Cc/ld.scm 180 */
																																																							if (((long) (BgL_curz00_597) == 45L))
																																																								{	/* Cc/ld.scm 180 */
																																																									BgL_test2490z00_2456
																																																										=
																																																										(
																																																										(bool_t)
																																																										1);
																																																								}
																																																							else
																																																								{	/* Cc/ld.scm 180 */
																																																									BgL_test2490z00_2456
																																																										=
																																																										(
																																																										(long)
																																																										(BgL_curz00_597)
																																																										==
																																																										99L);
																																																						}}
																																																					if (BgL_test2490z00_2456)
																																																						{	/* Cc/ld.scm 180 */
																																																							BgL_matchz00_765
																																																								=
																																																								BgL_lastzd2matchzd2_589;
																																																						}
																																																					else
																																																						{
																																																							long
																																																								BgL_bufposz00_2469;
																																																							long
																																																								BgL_forwardz00_2467;
																																																							long
																																																								BgL_lastzd2matchzd2_2466;
																																																							obj_t
																																																								BgL_iportz00_2465;
																																																							BgL_iportz00_2465
																																																								=
																																																								BgL_iportz00_588;
																																																							BgL_lastzd2matchzd2_2466
																																																								=
																																																								BgL_lastzd2matchzd2_589;
																																																							BgL_forwardz00_2467
																																																								=
																																																								(1L
																																																								+
																																																								BgL_forwardz00_590);
																																																							BgL_bufposz00_2469
																																																								=
																																																								BgL_bufposz00_591;
																																																							BgL_bufposz00_475
																																																								=
																																																								BgL_bufposz00_2469;
																																																							BgL_forwardz00_474
																																																								=
																																																								BgL_forwardz00_2467;
																																																							BgL_lastzd2matchzd2_473
																																																								=
																																																								BgL_lastzd2matchzd2_2466;
																																																							BgL_iportz00_472
																																																								=
																																																								BgL_iportz00_2465;
																																																							goto
																																																								BgL_zc3z04anonymousza31709ze3z87_476;
																																																						}
																																																				}
																																																		}
																																																}
																																															}
																																													}
																																												else
																																													{	/* Cc/ld.scm 180 */
																																														if (((long) (BgL_curz00_577) == 45L))
																																															{
																																																long
																																																	BgL_bufposz00_2478;
																																																long
																																																	BgL_forwardz00_2476;
																																																long
																																																	BgL_lastzd2matchzd2_2475;
																																																obj_t
																																																	BgL_iportz00_2474;
																																																BgL_iportz00_2474
																																																	=
																																																	BgL_iportz00_568;
																																																BgL_lastzd2matchzd2_2475
																																																	=
																																																	BgL_lastzd2matchzd2_569;
																																																BgL_forwardz00_2476
																																																	=
																																																	(1L
																																																	+
																																																	BgL_forwardz00_570);
																																																BgL_bufposz00_2478
																																																	=
																																																	BgL_bufposz00_571;
																																																BgL_bufposz00_491
																																																	=
																																																	BgL_bufposz00_2478;
																																																BgL_forwardz00_490
																																																	=
																																																	BgL_forwardz00_2476;
																																																BgL_lastzd2matchzd2_489
																																																	=
																																																	BgL_lastzd2matchzd2_2475;
																																																BgL_iportz00_488
																																																	=
																																																	BgL_iportz00_2474;
																																																goto
																																																	BgL_zc3z04anonymousza31725ze3z87_492;
																																															}
																																														else
																																															{	/* Cc/ld.scm 180 */
																																																bool_t
																																																	BgL_test2494z00_2479;
																																																if (((long) (BgL_curz00_577) == 10L))
																																																	{	/* Cc/ld.scm 180 */
																																																		BgL_test2494z00_2479
																																																			=
																																																			(
																																																			(bool_t)
																																																			1);
																																																	}
																																																else
																																																	{	/* Cc/ld.scm 180 */
																																																		if (((long) (BgL_curz00_577) == 45L))
																																																			{	/* Cc/ld.scm 180 */
																																																				BgL_test2494z00_2479
																																																					=
																																																					(
																																																					(bool_t)
																																																					1);
																																																			}
																																																		else
																																																			{	/* Cc/ld.scm 180 */
																																																				BgL_test2494z00_2479
																																																					=
																																																					(
																																																					(long)
																																																					(BgL_curz00_577)
																																																					==
																																																					105L);
																																																	}}
																																																if (BgL_test2494z00_2479)
																																																	{	/* Cc/ld.scm 180 */
																																																		BgL_matchz00_765
																																																			=
																																																			BgL_lastzd2matchzd2_569;
																																																	}
																																																else
																																																	{
																																																		long
																																																			BgL_bufposz00_2492;
																																																		long
																																																			BgL_forwardz00_2490;
																																																		long
																																																			BgL_lastzd2matchzd2_2489;
																																																		obj_t
																																																			BgL_iportz00_2488;
																																																		BgL_iportz00_2488
																																																			=
																																																			BgL_iportz00_568;
																																																		BgL_lastzd2matchzd2_2489
																																																			=
																																																			BgL_lastzd2matchzd2_569;
																																																		BgL_forwardz00_2490
																																																			=
																																																			(1L
																																																			+
																																																			BgL_forwardz00_570);
																																																		BgL_bufposz00_2492
																																																			=
																																																			BgL_bufposz00_571;
																																																		BgL_bufposz00_475
																																																			=
																																																			BgL_bufposz00_2492;
																																																		BgL_forwardz00_474
																																																			=
																																																			BgL_forwardz00_2490;
																																																		BgL_lastzd2matchzd2_473
																																																			=
																																																			BgL_lastzd2matchzd2_2489;
																																																		BgL_iportz00_472
																																																			=
																																																			BgL_iportz00_2488;
																																																		goto
																																																			BgL_zc3z04anonymousza31709ze3z87_476;
																																																	}
																																															}
																																													}
																																											}
																																										}
																																								}
																																							else
																																								{	/* Cc/ld.scm 180 */
																																									if (((long) (BgL_curz00_557) == 45L))
																																										{
																																											long
																																												BgL_bufposz00_2501;
																																											long
																																												BgL_forwardz00_2499;
																																											long
																																												BgL_lastzd2matchzd2_2498;
																																											obj_t
																																												BgL_iportz00_2497;
																																											BgL_iportz00_2497
																																												=
																																												BgL_iportz00_548;
																																											BgL_lastzd2matchzd2_2498
																																												=
																																												BgL_lastzd2matchzd2_549;
																																											BgL_forwardz00_2499
																																												=
																																												(1L
																																												+
																																												BgL_forwardz00_550);
																																											BgL_bufposz00_2501
																																												=
																																												BgL_bufposz00_551;
																																											BgL_bufposz00_491
																																												=
																																												BgL_bufposz00_2501;
																																											BgL_forwardz00_490
																																												=
																																												BgL_forwardz00_2499;
																																											BgL_lastzd2matchzd2_489
																																												=
																																												BgL_lastzd2matchzd2_2498;
																																											BgL_iportz00_488
																																												=
																																												BgL_iportz00_2497;
																																											goto
																																												BgL_zc3z04anonymousza31725ze3z87_492;
																																										}
																																									else
																																										{	/* Cc/ld.scm 180 */
																																											bool_t
																																												BgL_test2498z00_2502;
																																											if (((long) (BgL_curz00_557) == 10L))
																																												{	/* Cc/ld.scm 180 */
																																													BgL_test2498z00_2502
																																														=
																																														(
																																														(bool_t)
																																														1);
																																												}
																																											else
																																												{	/* Cc/ld.scm 180 */
																																													if (((long) (BgL_curz00_557) == 45L))
																																														{	/* Cc/ld.scm 180 */
																																															BgL_test2498z00_2502
																																																=
																																																(
																																																(bool_t)
																																																1);
																																														}
																																													else
																																														{	/* Cc/ld.scm 180 */
																																															BgL_test2498z00_2502
																																																=
																																																(
																																																(long)
																																																(BgL_curz00_557)
																																																==
																																																116L);
																																												}}
																																											if (BgL_test2498z00_2502)
																																												{	/* Cc/ld.scm 180 */
																																													BgL_matchz00_765
																																														=
																																														BgL_lastzd2matchzd2_549;
																																												}
																																											else
																																												{
																																													long
																																														BgL_bufposz00_2515;
																																													long
																																														BgL_forwardz00_2513;
																																													long
																																														BgL_lastzd2matchzd2_2512;
																																													obj_t
																																														BgL_iportz00_2511;
																																													BgL_iportz00_2511
																																														=
																																														BgL_iportz00_548;
																																													BgL_lastzd2matchzd2_2512
																																														=
																																														BgL_lastzd2matchzd2_549;
																																													BgL_forwardz00_2513
																																														=
																																														(1L
																																														+
																																														BgL_forwardz00_550);
																																													BgL_bufposz00_2515
																																														=
																																														BgL_bufposz00_551;
																																													BgL_bufposz00_475
																																														=
																																														BgL_bufposz00_2515;
																																													BgL_forwardz00_474
																																														=
																																														BgL_forwardz00_2513;
																																													BgL_lastzd2matchzd2_473
																																														=
																																														BgL_lastzd2matchzd2_2512;
																																													BgL_iportz00_472
																																														=
																																														BgL_iportz00_2511;
																																													goto
																																														BgL_zc3z04anonymousza31709ze3z87_476;
																																												}
																																										}
																																								}
																																						}
																																					}
																																			}
																																		else
																																			{	/* Cc/ld.scm 180 */
																																				if (
																																					((long) (BgL_curz00_537) == 45L))
																																					{
																																						long
																																							BgL_bufposz00_2524;
																																						long
																																							BgL_forwardz00_2522;
																																						long
																																							BgL_lastzd2matchzd2_2521;
																																						obj_t
																																							BgL_iportz00_2520;
																																						BgL_iportz00_2520
																																							=
																																							BgL_iportz00_528;
																																						BgL_lastzd2matchzd2_2521
																																							=
																																							BgL_lastzd2matchzd2_529;
																																						BgL_forwardz00_2522
																																							=
																																							(1L
																																							+
																																							BgL_forwardz00_530);
																																						BgL_bufposz00_2524
																																							=
																																							BgL_bufposz00_531;
																																						BgL_bufposz00_491
																																							=
																																							BgL_bufposz00_2524;
																																						BgL_forwardz00_490
																																							=
																																							BgL_forwardz00_2522;
																																						BgL_lastzd2matchzd2_489
																																							=
																																							BgL_lastzd2matchzd2_2521;
																																						BgL_iportz00_488
																																							=
																																							BgL_iportz00_2520;
																																						goto
																																							BgL_zc3z04anonymousza31725ze3z87_492;
																																					}
																																				else
																																					{	/* Cc/ld.scm 180 */
																																						bool_t
																																							BgL_test2502z00_2525;
																																						if (
																																							((long) (BgL_curz00_537) == 10L))
																																							{	/* Cc/ld.scm 180 */
																																								BgL_test2502z00_2525
																																									=
																																									(
																																									(bool_t)
																																									1);
																																							}
																																						else
																																							{	/* Cc/ld.scm 180 */
																																								if (((long) (BgL_curz00_537) == 45L))
																																									{	/* Cc/ld.scm 180 */
																																										BgL_test2502z00_2525
																																											=
																																											(
																																											(bool_t)
																																											1);
																																									}
																																								else
																																									{	/* Cc/ld.scm 180 */
																																										BgL_test2502z00_2525
																																											=
																																											(
																																											(long)
																																											(BgL_curz00_537)
																																											==
																																											97L);
																																							}}
																																						if (BgL_test2502z00_2525)
																																							{	/* Cc/ld.scm 180 */
																																								BgL_matchz00_765
																																									=
																																									BgL_lastzd2matchzd2_529;
																																							}
																																						else
																																							{
																																								long
																																									BgL_bufposz00_2538;
																																								long
																																									BgL_forwardz00_2536;
																																								long
																																									BgL_lastzd2matchzd2_2535;
																																								obj_t
																																									BgL_iportz00_2534;
																																								BgL_iportz00_2534
																																									=
																																									BgL_iportz00_528;
																																								BgL_lastzd2matchzd2_2535
																																									=
																																									BgL_lastzd2matchzd2_529;
																																								BgL_forwardz00_2536
																																									=
																																									(1L
																																									+
																																									BgL_forwardz00_530);
																																								BgL_bufposz00_2538
																																									=
																																									BgL_bufposz00_531;
																																								BgL_bufposz00_475
																																									=
																																									BgL_bufposz00_2538;
																																								BgL_forwardz00_474
																																									=
																																									BgL_forwardz00_2536;
																																								BgL_lastzd2matchzd2_473
																																									=
																																									BgL_lastzd2matchzd2_2535;
																																								BgL_iportz00_472
																																									=
																																									BgL_iportz00_2534;
																																								goto
																																									BgL_zc3z04anonymousza31709ze3z87_476;
																																							}
																																					}
																																			}
																																	}
																																}
																														}
																													else
																														{	/* Cc/ld.scm 180 */
																															if (
																																((long)
																																	(BgL_curz00_517)
																																	== 45L))
																																{
																																	long
																																		BgL_bufposz00_2547;
																																	long
																																		BgL_forwardz00_2545;
																																	long
																																		BgL_lastzd2matchzd2_2544;
																																	obj_t
																																		BgL_iportz00_2543;
																																	BgL_iportz00_2543
																																		=
																																		BgL_iportz00_508;
																																	BgL_lastzd2matchzd2_2544
																																		=
																																		BgL_lastzd2matchzd2_509;
																																	BgL_forwardz00_2545
																																		=
																																		(1L +
																																		BgL_forwardz00_510);
																																	BgL_bufposz00_2547
																																		=
																																		BgL_bufposz00_511;
																																	BgL_bufposz00_491
																																		=
																																		BgL_bufposz00_2547;
																																	BgL_forwardz00_490
																																		=
																																		BgL_forwardz00_2545;
																																	BgL_lastzd2matchzd2_489
																																		=
																																		BgL_lastzd2matchzd2_2544;
																																	BgL_iportz00_488
																																		=
																																		BgL_iportz00_2543;
																																	goto
																																		BgL_zc3z04anonymousza31725ze3z87_492;
																																}
																															else
																																{	/* Cc/ld.scm 180 */
																																	bool_t
																																		BgL_test2506z00_2548;
																																	if (((long)
																																			(BgL_curz00_517)
																																			== 10L))
																																		{	/* Cc/ld.scm 180 */
																																			BgL_test2506z00_2548
																																				=
																																				(
																																				(bool_t)
																																				1);
																																		}
																																	else
																																		{	/* Cc/ld.scm 180 */
																																			if (
																																				((long)
																																					(BgL_curz00_517)
																																					==
																																					45L))
																																				{	/* Cc/ld.scm 180 */
																																					BgL_test2506z00_2548
																																						=
																																						(
																																						(bool_t)
																																						1);
																																				}
																																			else
																																				{	/* Cc/ld.scm 180 */
																																					BgL_test2506z00_2548
																																						=
																																						(
																																						(long)
																																						(BgL_curz00_517)
																																						==
																																						116L);
																																		}}
																																	if (BgL_test2506z00_2548)
																																		{	/* Cc/ld.scm 180 */
																																			BgL_matchz00_765
																																				=
																																				BgL_lastzd2matchzd2_509;
																																		}
																																	else
																																		{
																																			long
																																				BgL_bufposz00_2561;
																																			long
																																				BgL_forwardz00_2559;
																																			long
																																				BgL_lastzd2matchzd2_2558;
																																			obj_t
																																				BgL_iportz00_2557;
																																			BgL_iportz00_2557
																																				=
																																				BgL_iportz00_508;
																																			BgL_lastzd2matchzd2_2558
																																				=
																																				BgL_lastzd2matchzd2_509;
																																			BgL_forwardz00_2559
																																				=
																																				(1L +
																																				BgL_forwardz00_510);
																																			BgL_bufposz00_2561
																																				=
																																				BgL_bufposz00_511;
																																			BgL_bufposz00_475
																																				=
																																				BgL_bufposz00_2561;
																																			BgL_forwardz00_474
																																				=
																																				BgL_forwardz00_2559;
																																			BgL_lastzd2matchzd2_473
																																				=
																																				BgL_lastzd2matchzd2_2558;
																																			BgL_iportz00_472
																																				=
																																				BgL_iportz00_2557;
																																			goto
																																				BgL_zc3z04anonymousza31709ze3z87_476;
																																		}
																																}
																														}
																												}
																											}
																									}
																								else
																									{	/* Cc/ld.scm 180 */
																										if (
																											((long) (BgL_curz00_461)
																												== 45L))
																											{
																												long BgL_bufposz00_2570;
																												long
																													BgL_forwardz00_2568;
																												long
																													BgL_lastzd2matchzd2_2567;
																												obj_t BgL_iportz00_2566;

																												BgL_iportz00_2566 =
																													BgL_iportz00_451;
																												BgL_lastzd2matchzd2_2567
																													=
																													BgL_newzd2matchzd2_456;
																												BgL_forwardz00_2568 =
																													(1L +
																													BgL_forwardz00_453);
																												BgL_bufposz00_2570 =
																													BgL_bufposz00_454;
																												BgL_bufposz00_491 =
																													BgL_bufposz00_2570;
																												BgL_forwardz00_490 =
																													BgL_forwardz00_2568;
																												BgL_lastzd2matchzd2_489
																													=
																													BgL_lastzd2matchzd2_2567;
																												BgL_iportz00_488 =
																													BgL_iportz00_2566;
																												goto
																													BgL_zc3z04anonymousza31725ze3z87_492;
																											}
																										else
																											{	/* Cc/ld.scm 180 */
																												bool_t
																													BgL_test2510z00_2571;
																												if (((long)
																														(BgL_curz00_461) ==
																														10L))
																													{	/* Cc/ld.scm 180 */
																														BgL_test2510z00_2571
																															= ((bool_t) 1);
																													}
																												else
																													{	/* Cc/ld.scm 180 */
																														if (
																															((long)
																																(BgL_curz00_461)
																																== 45L))
																															{	/* Cc/ld.scm 180 */
																																BgL_test2510z00_2571
																																	=
																																	((bool_t) 1);
																															}
																														else
																															{	/* Cc/ld.scm 180 */
																																BgL_test2510z00_2571
																																	=
																																	((long)
																																	(BgL_curz00_461)
																																	== 115L);
																													}}
																												if (BgL_test2510z00_2571)
																													{	/* Cc/ld.scm 180 */
																														BgL_matchz00_765 =
																															BgL_newzd2matchzd2_456;
																													}
																												else
																													{
																														long
																															BgL_bufposz00_2584;
																														long
																															BgL_forwardz00_2582;
																														long
																															BgL_lastzd2matchzd2_2581;
																														obj_t
																															BgL_iportz00_2580;
																														BgL_iportz00_2580 =
																															BgL_iportz00_451;
																														BgL_lastzd2matchzd2_2581
																															=
																															BgL_newzd2matchzd2_456;
																														BgL_forwardz00_2582
																															=
																															(1L +
																															BgL_forwardz00_453);
																														BgL_bufposz00_2584 =
																															BgL_bufposz00_454;
																														BgL_bufposz00_475 =
																															BgL_bufposz00_2584;
																														BgL_forwardz00_474 =
																															BgL_forwardz00_2582;
																														BgL_lastzd2matchzd2_473
																															=
																															BgL_lastzd2matchzd2_2581;
																														BgL_iportz00_472 =
																															BgL_iportz00_2580;
																														goto
																															BgL_zc3z04anonymousza31709ze3z87_476;
																													}
																											}
																									}
																							}
																						}
																				}
																			}
																		else
																			{	/* Cc/ld.scm 180 */
																				if (((long) (BgL_curz00_640) == 10L))
																					{	/* Cc/ld.scm 180 */
																						long BgL_arg1884z00_644;

																						BgL_arg1884z00_644 =
																							(1L + BgL_forwardz00_633);
																						{	/* Cc/ld.scm 180 */
																							long BgL_newzd2matchzd2_1693;

																							RGC_STOP_MATCH(BgL_iportz00_631,
																								BgL_arg1884z00_644);
																							BgL_newzd2matchzd2_1693 = 1L;
																							BgL_matchz00_765 =
																								BgL_newzd2matchzd2_1693;
																					}}
																				else
																					{	/* Cc/ld.scm 180 */
																						BgL_iportz00_434 = BgL_iportz00_631;
																						BgL_lastzd2matchzd2_435 =
																							BgL_lastzd2matchzd2_632;
																						BgL_forwardz00_436 =
																							(1L + BgL_forwardz00_633);
																						BgL_bufposz00_437 =
																							BgL_bufposz00_634;
																					BgL_zc3z04anonymousza31665ze3z87_438:
																						{	/* Cc/ld.scm 180 */
																							long BgL_newzd2matchzd2_439;

																							RGC_STOP_MATCH(BgL_iportz00_434,
																								BgL_forwardz00_436);
																							BgL_newzd2matchzd2_439 = 1L;
																							if (
																								(BgL_forwardz00_436 ==
																									BgL_bufposz00_437))
																								{	/* Cc/ld.scm 180 */
																									if (rgc_fill_buffer
																										(BgL_iportz00_434))
																										{	/* Cc/ld.scm 180 */
																											long BgL_arg1675z00_442;
																											long BgL_arg1678z00_443;

																											BgL_arg1675z00_442 =
																												RGC_BUFFER_FORWARD
																												(BgL_iportz00_434);
																											BgL_arg1678z00_443 =
																												RGC_BUFFER_BUFPOS
																												(BgL_iportz00_434);
																											{
																												long BgL_bufposz00_2599;
																												long
																													BgL_forwardz00_2598;
																												BgL_forwardz00_2598 =
																													BgL_arg1675z00_442;
																												BgL_bufposz00_2599 =
																													BgL_arg1678z00_443;
																												BgL_bufposz00_437 =
																													BgL_bufposz00_2599;
																												BgL_forwardz00_436 =
																													BgL_forwardz00_2598;
																												goto
																													BgL_zc3z04anonymousza31665ze3z87_438;
																											}
																										}
																									else
																										{	/* Cc/ld.scm 180 */
																											BgL_matchz00_765 =
																												BgL_newzd2matchzd2_439;
																										}
																								}
																							else
																								{	/* Cc/ld.scm 180 */
																									int BgL_curz00_444;

																									BgL_curz00_444 =
																										RGC_BUFFER_GET_CHAR
																										(BgL_iportz00_434,
																										BgL_forwardz00_436);
																									{	/* Cc/ld.scm 180 */

																										if (
																											((long) (BgL_curz00_444)
																												== 45L))
																											{
																												long BgL_bufposz00_2608;
																												long
																													BgL_forwardz00_2606;
																												long
																													BgL_lastzd2matchzd2_2605;
																												obj_t BgL_iportz00_2604;

																												BgL_iportz00_2604 =
																													BgL_iportz00_434;
																												BgL_lastzd2matchzd2_2605
																													=
																													BgL_newzd2matchzd2_439;
																												BgL_forwardz00_2606 =
																													(1L +
																													BgL_forwardz00_436);
																												BgL_bufposz00_2608 =
																													BgL_bufposz00_437;
																												BgL_bufposz00_491 =
																													BgL_bufposz00_2608;
																												BgL_forwardz00_490 =
																													BgL_forwardz00_2606;
																												BgL_lastzd2matchzd2_489
																													=
																													BgL_lastzd2matchzd2_2605;
																												BgL_iportz00_488 =
																													BgL_iportz00_2604;
																												goto
																													BgL_zc3z04anonymousza31725ze3z87_492;
																											}
																										else
																											{	/* Cc/ld.scm 180 */
																												bool_t
																													BgL_test2517z00_2609;
																												if (((long)
																														(BgL_curz00_444) ==
																														10L))
																													{	/* Cc/ld.scm 180 */
																														BgL_test2517z00_2609
																															= ((bool_t) 1);
																													}
																												else
																													{	/* Cc/ld.scm 180 */
																														BgL_test2517z00_2609
																															=
																															((long)
																															(BgL_curz00_444)
																															== 45L);
																													}
																												if (BgL_test2517z00_2609)
																													{	/* Cc/ld.scm 180 */
																														BgL_matchz00_765 =
																															BgL_newzd2matchzd2_439;
																													}
																												else
																													{
																														long
																															BgL_bufposz00_2619;
																														long
																															BgL_forwardz00_2617;
																														long
																															BgL_lastzd2matchzd2_2616;
																														obj_t
																															BgL_iportz00_2615;
																														BgL_iportz00_2615 =
																															BgL_iportz00_434;
																														BgL_lastzd2matchzd2_2616
																															=
																															BgL_newzd2matchzd2_439;
																														BgL_forwardz00_2617
																															=
																															(1L +
																															BgL_forwardz00_436);
																														BgL_bufposz00_2619 =
																															BgL_bufposz00_437;
																														BgL_bufposz00_475 =
																															BgL_bufposz00_2619;
																														BgL_forwardz00_474 =
																															BgL_forwardz00_2617;
																														BgL_lastzd2matchzd2_473
																															=
																															BgL_lastzd2matchzd2_2616;
																														BgL_iportz00_472 =
																															BgL_iportz00_2615;
																														goto
																															BgL_zc3z04anonymousza31709ze3z87_476;
																													}
																											}
																									}
																								}
																						}
																					}
																			}
																	}
																}
														}
														RGC_SET_FILEPOS(BgL_port1049z00_389);
														{

															switch (BgL_matchz00_765)
																{
																case 1L:

																	BgL_tmp1052z00_392 = BFALSE;
																	break;
																case 0L:

																	BgL_tmp1052z00_392 = BTRUE;
																	break;
																default:
																	BgL_tmp1052z00_392 =
																		BGl_errorz00zz__errorz00
																		(BGl_string2355z00zzcc_ldz00,
																		BGl_string2356z00zzcc_ldz00,
																		BINT(BgL_matchz00_765));
																}
														}
													}
												}
												{	/* Cc/ld.scm 180 */
													bool_t BgL_test2519z00_2625;

													{	/* Cc/ld.scm 180 */
														obj_t BgL_arg1827z00_1702;

														BgL_arg1827z00_1702 =
															BGL_EXITD_PROTECT(BgL_exitd1050z00_390);
														BgL_test2519z00_2625 = PAIRP(BgL_arg1827z00_1702);
													}
													if (BgL_test2519z00_2625)
														{	/* Cc/ld.scm 180 */
															obj_t BgL_arg1825z00_1703;

															{	/* Cc/ld.scm 180 */
																obj_t BgL_arg1826z00_1704;

																BgL_arg1826z00_1704 =
																	BGL_EXITD_PROTECT(BgL_exitd1050z00_390);
																BgL_arg1825z00_1703 =
																	CDR(((obj_t) BgL_arg1826z00_1704));
															}
															BGL_EXITD_PROTECT_SET(BgL_exitd1050z00_390,
																BgL_arg1825z00_1703);
															BUNSPEC;
														}
													else
														{	/* Cc/ld.scm 180 */
															BFALSE;
														}
												}
												bgl_close_input_port(BgL_port1049z00_389);
												BgL_staticpz00_200 = BgL_tmp1052z00_392;
											}
										}
									}
								}
						}
						if (CBOOL(BgL_staticpz00_200))
							{	/* Cc/ld.scm 185 */
								obj_t BgL_arg1249z00_201;

								BgL_arg1249z00_201 =
									BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(17));
								BGl_za2ldzd2optionsza2zd2zzengine_paramz00 =
									string_append_3(BgL_arg1249z00_201,
									BGl_string2357z00zzcc_ldz00,
									BGl_za2ldzd2optionsza2zd2zzengine_paramz00);
							}
						else
							{	/* Cc/ld.scm 187 */
								obj_t BgL_arg1252z00_202;

								BgL_arg1252z00_202 =
									BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(18));
								BGl_za2ldzd2optionsza2zd2zzengine_paramz00 =
									string_append_3(BgL_arg1252z00_202,
									BGl_string2357z00zzcc_ldz00,
									BGl_za2ldzd2optionsza2zd2zzengine_paramz00);
							}
						if ((BGl_za2passza2z00zzengine_paramz00 == CNST_TABLE_REF(14)))
							{	/* Cc/ld.scm 190 */
								{	/* Cc/ld.scm 192 */
									obj_t BgL_arg1268z00_203;

									BgL_arg1268z00_203 =
										BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(19));
									BgL_compz00_195 =
										string_append_3(BgL_compz00_195,
										BGl_string2357z00zzcc_ldz00, BgL_arg1268z00_203);
								}
								{	/* Cc/ld.scm 193 */
									obj_t BgL_sonameoptz00_204;

									BgL_sonameoptz00_204 =
										BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(20));
									{	/* Cc/ld.scm 195 */
										obj_t BgL_arg1272z00_205;

										{	/* Cc/ld.scm 195 */
											obj_t BgL_arg1284z00_206;

											BgL_arg1284z00_206 =
												BGl_defaultzd2sonameze70z35zzcc_ldz00
												(BGl_za2srczd2filesza2zd2zzengine_paramz00);
											{	/* Cc/ld.scm 195 */
												obj_t BgL_list1285z00_207;

												BgL_list1285z00_207 =
													MAKE_YOUNG_PAIR(BgL_arg1284z00_206, BNIL);
												BgL_arg1272z00_205 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BgL_sonameoptz00_204, BgL_list1285z00_207);
											}
										}
										BGl_za2ldzd2optionsza2zd2zzengine_paramz00 =
											string_append_3(BgL_arg1272z00_205,
											BGl_string2357z00zzcc_ldz00,
											BGl_za2ldzd2optionsza2zd2zzengine_paramz00);
									}
								}
							}
						else
							{	/* Cc/ld.scm 190 */
								BFALSE;
							}
						{	/* Cc/ld.scm 198 */
							obj_t BgL_destz00_208;

							if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
								{	/* Cc/ld.scm 199 */
									BgL_destz00_208 = BGl_za2destza2z00zzengine_paramz00;
								}
							else
								{	/* Cc/ld.scm 199 */
									if (
										(BGl_za2passza2z00zzengine_paramz00 == CNST_TABLE_REF(14)))
										{	/* Cc/ld.scm 200 */
											BgL_destz00_208 =
												BGl_defaultzd2sonameze70z35zzcc_ldz00
												(BGl_za2srczd2filesza2zd2zzengine_paramz00);
										}
									else
										{	/* Cc/ld.scm 200 */
											BgL_destz00_208 = string_to_bstring(BGL_DEFAULT_A_OUT);
										}
								}
							{	/* Cc/ld.scm 198 */
								obj_t BgL_bigloozd2libzd2_209;

								{	/* Cc/ld.scm 204 */
									obj_t BgL_arg1661z00_383;
									obj_t BgL_arg1663z00_384;

									BgL_arg1661z00_383 = BGl_libraryzd2suffixeszd2zzcc_ldz00();
									{	/* Cc/ld.scm 205 */
										obj_t BgL__ortest_1079z00_385;

										BgL__ortest_1079z00_385 =
											BGl_za2staticzd2allzd2bigloozf3za2zf3zzengine_paramz00;
										if (CBOOL(BgL__ortest_1079z00_385))
											{	/* Cc/ld.scm 205 */
												BgL_arg1663z00_384 = BgL__ortest_1079z00_385;
											}
										else
											{	/* Cc/ld.scm 205 */
												obj_t BgL__ortest_1080z00_386;

												BgL__ortest_1080z00_386 =
													BGl_za2staticzd2bigloozf3za2z21zzengine_paramz00;
												if (CBOOL(BgL__ortest_1080z00_386))
													{	/* Cc/ld.scm 205 */
														BgL_arg1663z00_384 = BgL__ortest_1080z00_386;
													}
												else
													{	/* Cc/ld.scm 205 */
														BgL_arg1663z00_384 = BgL_staticpz00_200;
													}
											}
									}
									BgL_bigloozd2libzd2_209 =
										BGl_libraryzd2ze3oszd2fileze3zzcc_ldz00
										(BGl_za2bigloozd2libza2zd2zzengine_paramz00,
										BgL_arg1661z00_383, BgL_arg1663z00_384, ((bool_t) 0),
										((bool_t) 0));
								}
								{	/* Cc/ld.scm 203 */
									obj_t BgL_gczd2libzd2_210;

									{	/* Cc/ld.scm 211 */
										obj_t BgL_arg1642z00_374;
										obj_t BgL_arg1646z00_375;
										bool_t BgL_arg1650z00_376;

										{	/* Cc/ld.scm 211 */
											obj_t BgL_arg1651z00_377;

											BgL_arg1651z00_377 =
												BGl_profilezd2gczd2debugzd2libraryzd2suffixz00zzcc_ldz00
												();
											{	/* Cc/ld.scm 211 */
												obj_t BgL_list1652z00_378;

												{	/* Cc/ld.scm 211 */
													obj_t BgL_arg1654z00_379;

													BgL_arg1654z00_379 =
														MAKE_YOUNG_PAIR(BGl_string2347z00zzcc_ldz00, BNIL);
													BgL_list1652z00_378 =
														MAKE_YOUNG_PAIR(BgL_arg1651z00_377,
														BgL_arg1654z00_379);
												}
												BgL_arg1642z00_374 = BgL_list1652z00_378;
											}
										}
										{	/* Cc/ld.scm 213 */
											obj_t BgL__ortest_1081z00_380;

											BgL__ortest_1081z00_380 =
												BGl_za2profilezd2libraryza2zd2zzengine_paramz00;
											if (CBOOL(BgL__ortest_1081z00_380))
												{	/* Cc/ld.scm 213 */
													BgL_arg1646z00_375 = BgL__ortest_1081z00_380;
												}
											else
												{	/* Cc/ld.scm 213 */
													obj_t BgL__ortest_1082z00_381;

													BgL__ortest_1082z00_381 =
														BGl_za2staticzd2allzd2bigloozf3za2zf3zzengine_paramz00;
													if (CBOOL(BgL__ortest_1082z00_381))
														{	/* Cc/ld.scm 213 */
															BgL_arg1646z00_375 = BgL__ortest_1082z00_381;
														}
													else
														{	/* Cc/ld.scm 213 */
															obj_t BgL__ortest_1083z00_382;

															BgL__ortest_1083z00_382 =
																BGl_za2staticzd2bigloozf3za2z21zzengine_paramz00;
															if (CBOOL(BgL__ortest_1083z00_382))
																{	/* Cc/ld.scm 213 */
																	BgL_arg1646z00_375 = BgL__ortest_1083z00_382;
																}
															else
																{	/* Cc/ld.scm 213 */
																	BgL_arg1646z00_375 = BgL_staticpz00_200;
																}
														}
												}
										}
										if (CBOOL(BGl_za2gczd2customzf3za2z21zzengine_paramz00))
											{	/* Cc/ld.scm 218 */
												BgL_arg1650z00_376 = ((bool_t) 0);
											}
										else
											{	/* Cc/ld.scm 218 */
												BgL_arg1650z00_376 = ((bool_t) 1);
											}
										BgL_gczd2libzd2_210 =
											BGl_libraryzd2ze3oszd2fileze3zzcc_ldz00
											(BGl_za2gczd2libza2zd2zzengine_paramz00,
											BgL_arg1642z00_374, BgL_arg1646z00_375, ((bool_t) 0),
											BgL_arg1650z00_376);
									}
									{	/* Cc/ld.scm 210 */
										obj_t BgL_evalzd2libszd2_211;

										{	/* Cc/ld.scm 220 */
											obj_t BgL_g1085z00_360;

											BgL_g1085z00_360 =
												BGl_getzd2evalzd2librariesz00zzmodule_evalz00();
											{
												obj_t BgL_libz00_362;
												obj_t BgL_resz00_363;

												BgL_libz00_362 = BgL_g1085z00_360;
												BgL_resz00_363 = BGl_string2347z00zzcc_ldz00;
											BgL_zc3z04anonymousza31614ze3z87_364:
												if (NULLP(BgL_libz00_362))
													{	/* Cc/ld.scm 222 */
														BgL_evalzd2libszd2_211 = BgL_resz00_363;
													}
												else
													{	/* Cc/ld.scm 224 */
														obj_t BgL_arg1616z00_366;
														obj_t BgL_arg1625z00_367;

														BgL_arg1616z00_366 = CDR(((obj_t) BgL_libz00_362));
														{	/* Cc/ld.scm 227 */
															obj_t BgL_arg1626z00_368;

															{	/* Cc/ld.scm 227 */
																obj_t BgL_arg1627z00_369;
																obj_t BgL_arg1629z00_370;
																obj_t BgL_arg1630z00_371;

																BgL_arg1627z00_369 =
																	CAR(((obj_t) BgL_libz00_362));
																BgL_arg1629z00_370 =
																	BGl_libraryzd2evalzd2suffixesz00zzcc_ldz00();
																{	/* Cc/ld.scm 229 */
																	obj_t BgL__ortest_1086z00_372;

																	BgL__ortest_1086z00_372 =
																		BGl_za2staticzd2allzd2bigloozf3za2zf3zzengine_paramz00;
																	if (CBOOL(BgL__ortest_1086z00_372))
																		{	/* Cc/ld.scm 229 */
																			BgL_arg1630z00_371 =
																				BgL__ortest_1086z00_372;
																		}
																	else
																		{	/* Cc/ld.scm 229 */
																			BgL_arg1630z00_371 = BgL_staticpz00_200;
																		}
																}
																BgL_arg1626z00_368 =
																	BGl_libraryzd2ze3oszd2fileze3zzcc_ldz00
																	(BgL_arg1627z00_369, BgL_arg1629z00_370,
																	BgL_arg1630z00_371, ((bool_t) 0),
																	((bool_t) 0));
															}
															BgL_arg1625z00_367 =
																string_append_3(BgL_arg1626z00_368,
																BGl_string2357z00zzcc_ldz00, BgL_resz00_363);
														}
														{
															obj_t BgL_resz00_2691;
															obj_t BgL_libz00_2690;

															BgL_libz00_2690 = BgL_arg1616z00_366;
															BgL_resz00_2691 = BgL_arg1625z00_367;
															BgL_resz00_363 = BgL_resz00_2691;
															BgL_libz00_362 = BgL_libz00_2690;
															goto BgL_zc3z04anonymousza31614ze3z87_364;
														}
													}
											}
										}
										{	/* Cc/ld.scm 220 */
											obj_t BgL_addzd2libszd2_212;

											{	/* Cc/ld.scm 234 */
												obj_t BgL_g1087z00_344;

												{	/* Cc/ld.scm 235 */
													obj_t BgL_g1945z00_358;

													BgL_g1945z00_358 =
														BGl_za2additionalzd2bigloozd2librariesza2z00zzengine_paramz00;
													{	/* Cc/ld.scm 234 */

														BgL_g1087z00_344 =
															BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00
															(BgL_g1945z00_358,
															BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00);
													}
												}
												{
													obj_t BgL_libz00_346;
													obj_t BgL_resz00_347;

													BgL_libz00_346 = BgL_g1087z00_344;
													BgL_resz00_347 = BGl_string2347z00zzcc_ldz00;
												BgL_zc3z04anonymousza31594ze3z87_348:
													if (NULLP(BgL_libz00_346))
														{	/* Cc/ld.scm 237 */
															BgL_addzd2libszd2_212 = BgL_resz00_347;
														}
													else
														{	/* Cc/ld.scm 239 */
															obj_t BgL_arg1602z00_350;
															obj_t BgL_arg1605z00_351;

															BgL_arg1602z00_350 =
																CDR(((obj_t) BgL_libz00_346));
															{	/* Cc/ld.scm 242 */
																obj_t BgL_arg1606z00_352;

																{	/* Cc/ld.scm 242 */
																	obj_t BgL_arg1609z00_353;
																	obj_t BgL_arg1611z00_354;
																	obj_t BgL_arg1613z00_355;

																	BgL_arg1609z00_353 =
																		CAR(((obj_t) BgL_libz00_346));
																	BgL_arg1611z00_354 =
																		BGl_libraryzd2suffixeszd2zzcc_ldz00();
																	{	/* Cc/ld.scm 244 */
																		obj_t BgL__ortest_1088z00_356;

																		BgL__ortest_1088z00_356 =
																			BGl_za2staticzd2allzd2bigloozf3za2zf3zzengine_paramz00;
																		if (CBOOL(BgL__ortest_1088z00_356))
																			{	/* Cc/ld.scm 244 */
																				BgL_arg1613z00_355 =
																					BgL__ortest_1088z00_356;
																			}
																		else
																			{	/* Cc/ld.scm 244 */
																				BgL_arg1613z00_355 = BgL_staticpz00_200;
																			}
																	}
																	BgL_arg1606z00_352 =
																		BGl_libraryzd2ze3oszd2fileze3zzcc_ldz00
																		(BgL_arg1609z00_353, BgL_arg1611z00_354,
																		BgL_arg1613z00_355, ((bool_t) 0),
																		((bool_t) 0));
																}
																BgL_arg1605z00_351 =
																	string_append_3(BgL_arg1606z00_352,
																	BGl_string2357z00zzcc_ldz00, BgL_resz00_347);
															}
															{
																obj_t BgL_resz00_2705;
																obj_t BgL_libz00_2704;

																BgL_libz00_2704 = BgL_arg1602z00_350;
																BgL_resz00_2705 = BgL_arg1605z00_351;
																BgL_resz00_347 = BgL_resz00_2705;
																BgL_libz00_346 = BgL_libz00_2704;
																goto BgL_zc3z04anonymousza31594ze3z87_348;
															}
														}
												}
											}
											{	/* Cc/ld.scm 234 */
												obj_t BgL_otherzd2libszd2_213;

												{	/* Cc/ld.scm 249 */
													obj_t BgL_g1089z00_334;

													BgL_g1089z00_334 =
														bgl_reverse
														(BGl_za2bigloozd2userzd2libza2z00zzengine_paramz00);
													{
														obj_t BgL_libz00_336;
														obj_t BgL_resz00_337;

														BgL_libz00_336 = BgL_g1089z00_334;
														BgL_resz00_337 = BGl_string2347z00zzcc_ldz00;
													BgL_zc3z04anonymousza31586ze3z87_338:
														if (NULLP(BgL_libz00_336))
															{	/* Cc/ld.scm 251 */
																BgL_otherzd2libszd2_213 = BgL_resz00_337;
															}
														else
															{	/* Cc/ld.scm 253 */
																obj_t BgL_arg1589z00_340;
																obj_t BgL_arg1591z00_341;

																BgL_arg1589z00_340 =
																	CDR(((obj_t) BgL_libz00_336));
																{	/* Cc/ld.scm 254 */
																	obj_t BgL_arg1593z00_342;

																	BgL_arg1593z00_342 =
																		CAR(((obj_t) BgL_libz00_336));
																	BgL_arg1591z00_341 =
																		string_append_3(BgL_arg1593z00_342,
																		BGl_string2357z00zzcc_ldz00,
																		BgL_resz00_337);
																}
																{
																	obj_t BgL_resz00_2715;
																	obj_t BgL_libz00_2714;

																	BgL_libz00_2714 = BgL_arg1589z00_340;
																	BgL_resz00_2715 = BgL_arg1591z00_341;
																	BgL_resz00_337 = BgL_resz00_2715;
																	BgL_libz00_336 = BgL_libz00_2714;
																	goto BgL_zc3z04anonymousza31586ze3z87_338;
																}
															}
													}
												}
												{	/* Cc/ld.scm 249 */
													obj_t BgL_ldzd2argszd2_214;

													{	/* Cc/ld.scm 257 */
														obj_t BgL_arg1307z00_220;
														obj_t BgL_arg1308z00_221;
														obj_t BgL_arg1310z00_222;
														obj_t BgL_arg1311z00_223;
														obj_t BgL_arg1312z00_224;
														obj_t BgL_arg1314z00_225;
														obj_t BgL_arg1315z00_226;
														obj_t BgL_arg1316z00_227;
														obj_t BgL_arg1317z00_228;
														obj_t BgL_arg1318z00_229;
														obj_t BgL_arg1319z00_230;
														obj_t BgL_arg1320z00_231;
														obj_t BgL_arg1321z00_232;
														obj_t BgL_arg1322z00_233;
														obj_t BgL_arg1323z00_234;

														{	/* Cc/ld.scm 257 */
															obj_t BgL_list1435z00_270;

															{	/* Cc/ld.scm 257 */
																obj_t BgL_arg1437z00_271;

																{	/* Cc/ld.scm 257 */
																	obj_t BgL_arg1448z00_272;

																	BgL_arg1448z00_272 =
																		MAKE_YOUNG_PAIR
																		(BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00,
																		BNIL);
																	BgL_arg1437z00_271 =
																		MAKE_YOUNG_PAIR(BGl_string2358z00zzcc_ldz00,
																		BgL_arg1448z00_272);
																}
																BgL_list1435z00_270 =
																	MAKE_YOUNG_PAIR(BgL_namez00_11,
																	BgL_arg1437z00_271);
															}
															BgL_arg1307z00_220 =
																BGl_unixzd2filenamezd2zzcc_execz00
																(BgL_list1435z00_270);
														}
														{	/* Cc/ld.scm 259 */
															obj_t BgL_arg1453z00_273;

															{	/* Cc/ld.scm 259 */
																obj_t BgL_l1122z00_274;

																BgL_l1122z00_274 =
																	BGl_za2withzd2filesza2zd2zzengine_paramz00;
																if (NULLP(BgL_l1122z00_274))
																	{	/* Cc/ld.scm 259 */
																		BgL_arg1453z00_273 = BNIL;
																	}
																else
																	{	/* Cc/ld.scm 259 */
																		obj_t BgL_head1124z00_276;

																		{	/* Cc/ld.scm 259 */
																			obj_t BgL_arg1489z00_289;

																			{	/* Cc/ld.scm 259 */
																				obj_t BgL_arg1502z00_290;

																				BgL_arg1502z00_290 =
																					CAR(((obj_t) BgL_l1122z00_274));
																				{	/* Cc/ld.scm 259 */
																					obj_t BgL_list1503z00_291;

																					BgL_list1503z00_291 =
																						MAKE_YOUNG_PAIR(BgL_arg1502z00_290,
																						BNIL);
																					BgL_arg1489z00_289 =
																						BGl_unixzd2filenamezd2zzcc_execz00
																						(BgL_list1503z00_291);
																				}
																			}
																			BgL_head1124z00_276 =
																				MAKE_YOUNG_PAIR(BgL_arg1489z00_289,
																				BNIL);
																		}
																		{	/* Cc/ld.scm 259 */
																			obj_t BgL_g1129z00_277;

																			BgL_g1129z00_277 =
																				CDR(((obj_t) BgL_l1122z00_274));
																			{
																				obj_t BgL_l1122z00_279;
																				obj_t BgL_tail1125z00_280;

																				BgL_l1122z00_279 = BgL_g1129z00_277;
																				BgL_tail1125z00_280 =
																					BgL_head1124z00_276;
																			BgL_zc3z04anonymousza31456ze3z87_281:
																				if (NULLP(BgL_l1122z00_279))
																					{	/* Cc/ld.scm 259 */
																						BgL_arg1453z00_273 =
																							BgL_head1124z00_276;
																					}
																				else
																					{	/* Cc/ld.scm 259 */
																						obj_t BgL_newtail1126z00_283;

																						{	/* Cc/ld.scm 259 */
																							obj_t BgL_arg1473z00_285;

																							{	/* Cc/ld.scm 259 */
																								obj_t BgL_arg1485z00_286;

																								BgL_arg1485z00_286 =
																									CAR(
																									((obj_t) BgL_l1122z00_279));
																								{	/* Cc/ld.scm 259 */
																									obj_t BgL_list1486z00_287;

																									BgL_list1486z00_287 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1485z00_286, BNIL);
																									BgL_arg1473z00_285 =
																										BGl_unixzd2filenamezd2zzcc_execz00
																										(BgL_list1486z00_287);
																								}
																							}
																							BgL_newtail1126z00_283 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1473z00_285, BNIL);
																						}
																						SET_CDR(BgL_tail1125z00_280,
																							BgL_newtail1126z00_283);
																						{	/* Cc/ld.scm 259 */
																							obj_t BgL_arg1472z00_284;

																							BgL_arg1472z00_284 =
																								CDR(((obj_t) BgL_l1122z00_279));
																							{
																								obj_t BgL_tail1125z00_2740;
																								obj_t BgL_l1122z00_2739;

																								BgL_l1122z00_2739 =
																									BgL_arg1472z00_284;
																								BgL_tail1125z00_2740 =
																									BgL_newtail1126z00_283;
																								BgL_tail1125z00_280 =
																									BgL_tail1125z00_2740;
																								BgL_l1122z00_279 =
																									BgL_l1122z00_2739;
																								goto
																									BgL_zc3z04anonymousza31456ze3z87_281;
																							}
																						}
																					}
																			}
																		}
																	}
															}
															BgL_arg1308z00_221 =
																BGl_stringza2zd2ze3stringz93zztools_miscz00
																(BgL_arg1453z00_273);
														}
														{	/* Cc/ld.scm 261 */
															obj_t BgL_arg1509z00_292;

															{	/* Cc/ld.scm 261 */
																obj_t BgL_l1130z00_293;

																BgL_l1130z00_293 =
																	BGl_za2ozd2filesza2zd2zzengine_paramz00;
																if (NULLP(BgL_l1130z00_293))
																	{	/* Cc/ld.scm 261 */
																		BgL_arg1509z00_292 = BNIL;
																	}
																else
																	{	/* Cc/ld.scm 261 */
																		obj_t BgL_head1132z00_295;

																		{	/* Cc/ld.scm 261 */
																			obj_t BgL_arg1535z00_308;

																			{	/* Cc/ld.scm 261 */
																				obj_t BgL_arg1540z00_309;

																				BgL_arg1540z00_309 =
																					CAR(((obj_t) BgL_l1130z00_293));
																				{	/* Cc/ld.scm 261 */
																					obj_t BgL_list1541z00_310;

																					BgL_list1541z00_310 =
																						MAKE_YOUNG_PAIR(BgL_arg1540z00_309,
																						BNIL);
																					BgL_arg1535z00_308 =
																						BGl_unixzd2filenamezd2zzcc_execz00
																						(BgL_list1541z00_310);
																				}
																			}
																			BgL_head1132z00_295 =
																				MAKE_YOUNG_PAIR(BgL_arg1535z00_308,
																				BNIL);
																		}
																		{	/* Cc/ld.scm 261 */
																			obj_t BgL_g1135z00_296;

																			BgL_g1135z00_296 =
																				CDR(((obj_t) BgL_l1130z00_293));
																			{
																				obj_t BgL_l1130z00_298;
																				obj_t BgL_tail1133z00_299;

																				BgL_l1130z00_298 = BgL_g1135z00_296;
																				BgL_tail1133z00_299 =
																					BgL_head1132z00_295;
																			BgL_zc3z04anonymousza31511ze3z87_300:
																				if (NULLP(BgL_l1130z00_298))
																					{	/* Cc/ld.scm 261 */
																						BgL_arg1509z00_292 =
																							BgL_head1132z00_295;
																					}
																				else
																					{	/* Cc/ld.scm 261 */
																						obj_t BgL_newtail1134z00_302;

																						{	/* Cc/ld.scm 261 */
																							obj_t BgL_arg1514z00_304;

																							{	/* Cc/ld.scm 261 */
																								obj_t BgL_arg1516z00_305;

																								BgL_arg1516z00_305 =
																									CAR(
																									((obj_t) BgL_l1130z00_298));
																								{	/* Cc/ld.scm 261 */
																									obj_t BgL_list1517z00_306;

																									BgL_list1517z00_306 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1516z00_305, BNIL);
																									BgL_arg1514z00_304 =
																										BGl_unixzd2filenamezd2zzcc_execz00
																										(BgL_list1517z00_306);
																								}
																							}
																							BgL_newtail1134z00_302 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1514z00_304, BNIL);
																						}
																						SET_CDR(BgL_tail1133z00_299,
																							BgL_newtail1134z00_302);
																						{	/* Cc/ld.scm 261 */
																							obj_t BgL_arg1513z00_303;

																							BgL_arg1513z00_303 =
																								CDR(((obj_t) BgL_l1130z00_298));
																							{
																								obj_t BgL_tail1133z00_2762;
																								obj_t BgL_l1130z00_2761;

																								BgL_l1130z00_2761 =
																									BgL_arg1513z00_303;
																								BgL_tail1133z00_2762 =
																									BgL_newtail1134z00_302;
																								BgL_tail1133z00_299 =
																									BgL_tail1133z00_2762;
																								BgL_l1130z00_298 =
																									BgL_l1130z00_2761;
																								goto
																									BgL_zc3z04anonymousza31511ze3z87_300;
																							}
																						}
																					}
																			}
																		}
																	}
															}
															BgL_arg1310z00_222 =
																BGl_stringza2zd2ze3stringz93zztools_miscz00
																(BgL_arg1509z00_292);
														}
														{	/* Cc/ld.scm 263 */
															obj_t BgL_list1542z00_311;

															BgL_list1542z00_311 =
																MAKE_YOUNG_PAIR(BgL_destz00_208, BNIL);
															BgL_arg1311z00_223 =
																BGl_unixzd2filenamezd2zzcc_execz00
																(BgL_list1542z00_311);
														}
														{	/* Cc/ld.scm 265 */
															obj_t BgL_list1543z00_312;

															BgL_list1543z00_312 =
																MAKE_YOUNG_PAIR
																(BGl_za2cczd2optionsza2zd2zzengine_paramz00,
																BNIL);
															BgL_arg1312z00_224 =
																BGl_formatz00zz__r4_output_6_10_3z00
																(BGl_string2359z00zzcc_ldz00,
																BgL_list1543z00_312);
														}
														{	/* Cc/ld.scm 152 */
															obj_t BgL_rpathfmtz00_828;

															BgL_rpathfmtz00_828 =
																BGl_bigloozd2configzd2zz__configurez00
																(CNST_TABLE_REF(13));
															if ((STRING_LENGTH(((obj_t) BgL_rpathfmtz00_828))
																	== 0L))
																{	/* Cc/ld.scm 153 */
																	BgL_arg1314z00_225 =
																		BGl_string2347z00zzcc_ldz00;
																}
															else
																{	/* Cc/ld.scm 156 */
																	obj_t BgL_arg1990z00_830;

																	{	/* Cc/ld.scm 156 */
																		obj_t BgL_l1115z00_832;

																		{	/* Cc/ld.scm 158 */
																			obj_t BgL_g1945z00_846;

																			BgL_g1945z00_846 =
																				BGl_za2cflagszd2rpathza2zd2zzengine_paramz00;
																			{	/* Cc/ld.scm 158 */

																				BgL_l1115z00_832 =
																					BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00
																					(BgL_g1945z00_846,
																					BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00);
																			}
																		}
																		if (NULLP(BgL_l1115z00_832))
																			{	/* Cc/ld.scm 156 */
																				BgL_arg1990z00_830 = BNIL;
																			}
																		else
																			{	/* Cc/ld.scm 156 */
																				obj_t BgL_head1117z00_834;

																				BgL_head1117z00_834 =
																					MAKE_YOUNG_PAIR(BNIL, BNIL);
																				{
																					obj_t BgL_l1115z00_836;
																					obj_t BgL_tail1118z00_837;

																					BgL_l1115z00_836 = BgL_l1115z00_832;
																					BgL_tail1118z00_837 =
																						BgL_head1117z00_834;
																				BgL_zc3z04anonymousza31993ze3z87_838:
																					if (NULLP(BgL_l1115z00_836))
																						{	/* Cc/ld.scm 156 */
																							BgL_arg1990z00_830 =
																								CDR(BgL_head1117z00_834);
																						}
																					else
																						{	/* Cc/ld.scm 156 */
																							obj_t BgL_newtail1119z00_840;

																							{	/* Cc/ld.scm 156 */
																								obj_t BgL_arg1996z00_842;

																								{	/* Cc/ld.scm 156 */
																									obj_t BgL_pathz00_843;

																									BgL_pathz00_843 =
																										CAR(
																										((obj_t) BgL_l1115z00_836));
																									{	/* Cc/ld.scm 157 */
																										obj_t BgL_list1997z00_844;

																										BgL_list1997z00_844 =
																											MAKE_YOUNG_PAIR
																											(BgL_pathz00_843, BNIL);
																										BgL_arg1996z00_842 =
																											BGl_formatz00zz__r4_output_6_10_3z00
																											(BgL_rpathfmtz00_828,
																											BgL_list1997z00_844);
																									}
																								}
																								BgL_newtail1119z00_840 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1996z00_842, BNIL);
																							}
																							SET_CDR(BgL_tail1118z00_837,
																								BgL_newtail1119z00_840);
																							{	/* Cc/ld.scm 156 */
																								obj_t BgL_arg1995z00_841;

																								BgL_arg1995z00_841 =
																									CDR(
																									((obj_t) BgL_l1115z00_836));
																								{
																									obj_t BgL_tail1118z00_2790;
																									obj_t BgL_l1115z00_2789;

																									BgL_l1115z00_2789 =
																										BgL_arg1995z00_841;
																									BgL_tail1118z00_2790 =
																										BgL_newtail1119z00_840;
																									BgL_tail1118z00_837 =
																										BgL_tail1118z00_2790;
																									BgL_l1115z00_836 =
																										BgL_l1115z00_2789;
																									goto
																										BgL_zc3z04anonymousza31993ze3z87_838;
																								}
																							}
																						}
																				}
																			}
																	}
																	{	/* Cc/ld.scm 155 */
																		obj_t BgL_list1991z00_831;

																		BgL_list1991z00_831 =
																			MAKE_YOUNG_PAIR(BgL_arg1990z00_830, BNIL);
																		BgL_arg1314z00_225 =
																			BGl_formatz00zz__r4_output_6_10_3z00
																			(BGl_string2352z00zzcc_ldz00,
																			BgL_list1991z00_831);
																	}
																}
														}
														{	/* Cc/ld.scm 269 */
															bool_t BgL_test2542z00_2793;

															if (CBOOL
																(BGl_za2czd2debugza2zd2zzengine_paramz00))
																{	/* Cc/ld.scm 269 */
																	BgL_test2542z00_2793 = ((bool_t) 1);
																}
															else
																{	/* Cc/ld.scm 269 */
																	BgL_test2542z00_2793 =
																		(
																		(long)
																		CINT
																		(BGl_za2bdbzd2debugza2zd2zzengine_paramz00)
																		> 0L);
																}
															if (BgL_test2542z00_2793)
																{	/* Cc/ld.scm 269 */
																	BgL_arg1315z00_226 =
																		string_append(BGl_string2357z00zzcc_ldz00,
																		BGl_za2ldzd2debugzd2optionza2z00zzengine_paramz00);
																}
															else
																{	/* Cc/ld.scm 269 */
																	BgL_arg1315z00_226 =
																		BGl_string2347z00zzcc_ldz00;
																}
														}
														{	/* Cc/ld.scm 273 */
															bool_t BgL_test2544z00_2799;

															if (CBOOL(BGl_za2stripza2z00zzengine_paramz00))
																{	/* Cc/ld.scm 273 */
																	if (
																		(BGl_za2passza2z00zzengine_paramz00 ==
																			CNST_TABLE_REF(14)))
																		{	/* Cc/ld.scm 273 */
																			BgL_test2544z00_2799 = ((bool_t) 0);
																		}
																	else
																		{	/* Cc/ld.scm 273 */
																			BgL_test2544z00_2799 = ((bool_t) 1);
																		}
																}
															else
																{	/* Cc/ld.scm 273 */
																	BgL_test2544z00_2799 = ((bool_t) 0);
																}
															if (BgL_test2544z00_2799)
																{	/* Cc/ld.scm 273 */
																	BgL_arg1316z00_227 =
																		string_append(BGl_string2357z00zzcc_ldz00,
																		BGl_bigloozd2configzd2zz__configurez00
																		(CNST_TABLE_REF(21)));
																}
															else
																{	/* Cc/ld.scm 273 */
																	BgL_arg1316z00_227 =
																		BGl_string2347z00zzcc_ldz00;
																}
														}
														BgL_arg1317z00_228 =
															string_append(BGl_string2357z00zzcc_ldz00,
															BGl_bigloozd2configzd2zz__configurez00
															(CNST_TABLE_REF(22)));
														BgL_arg1318z00_229 =
															BGl_loopze71ze7zzcc_ldz00
															(BGl_za2libzd2dirza2zd2zzengine_paramz00);
														{	/* Cc/ld.scm 288 */
															obj_t BgL_sz00_329;

															BgL_sz00_329 =
																BGl_bigloozd2configzd2zz__configurez00
																(CNST_TABLE_REF(23));
															if ((STRING_LENGTH(((obj_t) BgL_sz00_329)) > 0L))
																{	/* Cc/ld.scm 289 */
																	BgL_arg1319z00_230 =
																		string_append(BGl_string2360z00zzcc_ldz00,
																		BgL_sz00_329);
																}
															else
																{	/* Cc/ld.scm 289 */
																	BgL_arg1319z00_230 =
																		BGl_string2347z00zzcc_ldz00;
																}
														}
														if (CBOOL(BGl_za2czd2debugza2zd2zzengine_paramz00))
															{	/* Cc/ld.scm 293 */
																BgL_arg1320z00_231 =
																	BGl_string2347z00zzcc_ldz00;
															}
														else
															{	/* Cc/ld.scm 293 */
																BgL_arg1320z00_231 =
																	string_append(BGl_string2357z00zzcc_ldz00,
																	BGl_za2ldzd2optimzd2flagsza2z00zzengine_paramz00);
															}
														BgL_arg1321z00_232 =
															BGl_bigloozd2configzd2zz__configurez00
															(CNST_TABLE_REF(24));
														if (CBOOL
															(BGl_za2doublezd2ldzd2libszf3za2zf3zzengine_paramz00))
															{	/* Cc/ld.scm 309 */
																BgL_arg1322z00_233 = BgL_addzd2libszd2_212;
															}
														else
															{	/* Cc/ld.scm 309 */
																BgL_arg1322z00_233 =
																	BGl_string2347z00zzcc_ldz00;
															}
														{	/* Cc/ld.scm 311 */
															obj_t BgL_list1585z00_333;

															BgL_list1585z00_333 =
																MAKE_YOUNG_PAIR
																(BGl_za2ldzd2postzd2optionsza2z00zzengine_paramz00,
																BNIL);
															BgL_arg1323z00_234 =
																BGl_formatz00zz__r4_output_6_10_3z00
																(BGl_string2359z00zzcc_ldz00,
																BgL_list1585z00_333);
														}
														{	/* Cc/ld.scm 255 */
															obj_t BgL_list1324z00_235;

															{	/* Cc/ld.scm 255 */
																obj_t BgL_arg1325z00_236;

																{	/* Cc/ld.scm 255 */
																	obj_t BgL_arg1326z00_237;

																	{	/* Cc/ld.scm 255 */
																		obj_t BgL_arg1327z00_238;

																		{	/* Cc/ld.scm 255 */
																			obj_t BgL_arg1328z00_239;

																			{	/* Cc/ld.scm 255 */
																				obj_t BgL_arg1329z00_240;

																				{	/* Cc/ld.scm 255 */
																					obj_t BgL_arg1331z00_241;

																					{	/* Cc/ld.scm 255 */
																						obj_t BgL_arg1332z00_242;

																						{	/* Cc/ld.scm 255 */
																							obj_t BgL_arg1333z00_243;

																							{	/* Cc/ld.scm 255 */
																								obj_t BgL_arg1335z00_244;

																								{	/* Cc/ld.scm 255 */
																									obj_t BgL_arg1339z00_245;

																									{	/* Cc/ld.scm 255 */
																										obj_t BgL_arg1340z00_246;

																										{	/* Cc/ld.scm 255 */
																											obj_t BgL_arg1342z00_247;

																											{	/* Cc/ld.scm 255 */
																												obj_t
																													BgL_arg1343z00_248;
																												{	/* Cc/ld.scm 255 */
																													obj_t
																														BgL_arg1346z00_249;
																													{	/* Cc/ld.scm 255 */
																														obj_t
																															BgL_arg1348z00_250;
																														{	/* Cc/ld.scm 255 */
																															obj_t
																																BgL_arg1349z00_251;
																															{	/* Cc/ld.scm 255 */
																																obj_t
																																	BgL_arg1351z00_252;
																																{	/* Cc/ld.scm 255 */
																																	obj_t
																																		BgL_arg1352z00_253;
																																	{	/* Cc/ld.scm 255 */
																																		obj_t
																																			BgL_arg1361z00_254;
																																		{	/* Cc/ld.scm 255 */
																																			obj_t
																																				BgL_arg1364z00_255;
																																			{	/* Cc/ld.scm 255 */
																																				obj_t
																																					BgL_arg1367z00_256;
																																				{	/* Cc/ld.scm 255 */
																																					obj_t
																																						BgL_arg1370z00_257;
																																					{	/* Cc/ld.scm 255 */
																																						obj_t
																																							BgL_arg1371z00_258;
																																						{	/* Cc/ld.scm 255 */
																																							obj_t
																																								BgL_arg1375z00_259;
																																							{	/* Cc/ld.scm 255 */
																																								obj_t
																																									BgL_arg1376z00_260;
																																								{	/* Cc/ld.scm 255 */
																																									obj_t
																																										BgL_arg1377z00_261;
																																									{	/* Cc/ld.scm 255 */
																																										obj_t
																																											BgL_arg1378z00_262;
																																										{	/* Cc/ld.scm 255 */
																																											obj_t
																																												BgL_arg1379z00_263;
																																											{	/* Cc/ld.scm 255 */
																																												obj_t
																																													BgL_arg1380z00_264;
																																												{	/* Cc/ld.scm 255 */
																																													obj_t
																																														BgL_arg1408z00_265;
																																													{	/* Cc/ld.scm 255 */
																																														obj_t
																																															BgL_arg1410z00_266;
																																														{	/* Cc/ld.scm 255 */
																																															obj_t
																																																BgL_arg1421z00_267;
																																															{	/* Cc/ld.scm 255 */
																																																obj_t
																																																	BgL_arg1422z00_268;
																																																{	/* Cc/ld.scm 255 */
																																																	obj_t
																																																		BgL_arg1434z00_269;
																																																	BgL_arg1434z00_269
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BgL_arg1323z00_234,
																																																		BNIL);
																																																	BgL_arg1422z00_268
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BGl_string2357z00zzcc_ldz00,
																																																		BgL_arg1434z00_269);
																																																}
																																																BgL_arg1421z00_267
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BgL_arg1322z00_233,
																																																	BgL_arg1422z00_268);
																																															}
																																															BgL_arg1410z00_266
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BGl_string2357z00zzcc_ldz00,
																																																BgL_arg1421z00_267);
																																														}
																																														BgL_arg1408z00_265
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_otherzd2libszd2_213,
																																															BgL_arg1410z00_266);
																																													}
																																													BgL_arg1380z00_264
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BGl_string2357z00zzcc_ldz00,
																																														BgL_arg1408z00_265);
																																												}
																																												BgL_arg1379z00_263
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg1321z00_232,
																																													BgL_arg1380z00_264);
																																											}
																																											BgL_arg1378z00_262
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_string2357z00zzcc_ldz00,
																																												BgL_arg1379z00_263);
																																										}
																																										BgL_arg1377z00_261
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_gczd2libzd2_210,
																																											BgL_arg1378z00_262);
																																									}
																																									BgL_arg1376z00_260
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_string2357z00zzcc_ldz00,
																																										BgL_arg1377z00_261);
																																								}
																																								BgL_arg1375z00_259
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_bigloozd2libzd2_209,
																																									BgL_arg1376z00_260);
																																							}
																																							BgL_arg1371z00_258
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_string2357z00zzcc_ldz00,
																																								BgL_arg1375z00_259);
																																						}
																																						BgL_arg1370z00_257
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_addzd2libszd2_212,
																																							BgL_arg1371z00_258);
																																					}
																																					BgL_arg1367z00_256
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_string2357z00zzcc_ldz00,
																																						BgL_arg1370z00_257);
																																				}
																																				BgL_arg1364z00_255
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_evalzd2libszd2_211,
																																					BgL_arg1367z00_256);
																																			}
																																			BgL_arg1361z00_254
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_string2357z00zzcc_ldz00,
																																				BgL_arg1364z00_255);
																																		}
																																		BgL_arg1352z00_253
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1320z00_231,
																																			BgL_arg1361z00_254);
																																	}
																																	BgL_arg1351z00_252
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1319z00_230,
																																		BgL_arg1352z00_253);
																																}
																																BgL_arg1349z00_251
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1318z00_229,
																																	BgL_arg1351z00_252);
																															}
																															BgL_arg1348z00_250
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_string2357z00zzcc_ldz00,
																																BgL_arg1349z00_251);
																														}
																														BgL_arg1346z00_249 =
																															MAKE_YOUNG_PAIR
																															(BGl_za2ldzd2optionsza2zd2zzengine_paramz00,
																															BgL_arg1348z00_250);
																													}
																													BgL_arg1343z00_248 =
																														MAKE_YOUNG_PAIR
																														(BGl_string2357z00zzcc_ldz00,
																														BgL_arg1346z00_249);
																												}
																												BgL_arg1342z00_247 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1317z00_228,
																													BgL_arg1343z00_248);
																											}
																											BgL_arg1340z00_246 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1316z00_227,
																												BgL_arg1342z00_247);
																										}
																										BgL_arg1339z00_245 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1315z00_226,
																											BgL_arg1340z00_246);
																									}
																									BgL_arg1335z00_244 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1314z00_225,
																										BgL_arg1339z00_245);
																								}
																								BgL_arg1333z00_243 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1312z00_224,
																									BgL_arg1335z00_244);
																							}
																							BgL_arg1332z00_242 =
																								MAKE_YOUNG_PAIR
																								(BGl_string2357z00zzcc_ldz00,
																								BgL_arg1333z00_243);
																						}
																						BgL_arg1331z00_241 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1311z00_223,
																							BgL_arg1332z00_242);
																					}
																					BgL_arg1329z00_240 =
																						MAKE_YOUNG_PAIR
																						(BGl_za2ldzd2ozd2optionza2z00zzengine_paramz00,
																						BgL_arg1331z00_241);
																				}
																				BgL_arg1328z00_239 =
																					MAKE_YOUNG_PAIR
																					(BGl_string2357z00zzcc_ldz00,
																					BgL_arg1329z00_240);
																			}
																			BgL_arg1327z00_238 =
																				MAKE_YOUNG_PAIR(BgL_arg1310z00_222,
																				BgL_arg1328z00_239);
																		}
																		BgL_arg1326z00_237 =
																			MAKE_YOUNG_PAIR(BgL_arg1308z00_221,
																			BgL_arg1327z00_238);
																	}
																	BgL_arg1325z00_236 =
																		MAKE_YOUNG_PAIR(BGl_string2357z00zzcc_ldz00,
																		BgL_arg1326z00_237);
																}
																BgL_list1324z00_235 =
																	MAKE_YOUNG_PAIR(BgL_arg1307z00_220,
																	BgL_arg1325z00_236);
															}
															BgL_ldzd2argszd2_214 =
																BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																(BgL_list1324z00_235);
														}
													}
													{	/* Cc/ld.scm 255 */
														obj_t BgL_cmdz00_215;

														BgL_cmdz00_215 =
															string_append_3(BgL_compz00_195,
															BGl_string2357z00zzcc_ldz00,
															BgL_ldzd2argszd2_214);
														{	/* Cc/ld.scm 312 */

															{	/* Cc/ld.scm 313 */
																obj_t BgL_list1286z00_216;

																{	/* Cc/ld.scm 313 */
																	obj_t BgL_arg1304z00_217;

																	{	/* Cc/ld.scm 313 */
																		obj_t BgL_arg1305z00_218;

																		{	/* Cc/ld.scm 313 */
																			obj_t BgL_arg1306z00_219;

																			BgL_arg1306z00_219 =
																				MAKE_YOUNG_PAIR(BCHAR(((unsigned char)
																						10)), BNIL);
																			BgL_arg1305z00_218 =
																				MAKE_YOUNG_PAIR(BCHAR(((unsigned char)
																						']')), BgL_arg1306z00_219);
																		}
																		BgL_arg1304z00_217 =
																			MAKE_YOUNG_PAIR(BgL_cmdz00_215,
																			BgL_arg1305z00_218);
																	}
																	BgL_list1286z00_216 =
																		MAKE_YOUNG_PAIR(BGl_string2361z00zzcc_ldz00,
																		BgL_arg1304z00_217);
																}
																BGl_verbosez00zztools_speekz00(BINT(2L),
																	BgL_list1286z00_216);
															}
															return
																BGl_execz00zzcc_execz00(BgL_cmdz00_215,
																BgL_needzd2tozd2returnz00_12,
																BGl_string2342z00zzcc_ldz00);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* default-soname~0 */
	obj_t BGl_defaultzd2sonameze70z35zzcc_ldz00(obj_t BgL_filesz00_817)
	{
		{	/* Cc/ld.scm 170 */
			{	/* Cc/ld.scm 161 */
				obj_t BgL_namez00_819;

				if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
					{	/* Cc/ld.scm 162 */
						BgL_namez00_819 = BGl_za2destza2z00zzengine_paramz00;
					}
				else
					{	/* Cc/ld.scm 162 */
						if (PAIRP(BgL_filesz00_817))
							{	/* Cc/ld.scm 163 */
								BgL_namez00_819 = CAR(BgL_filesz00_817);
							}
						else
							{	/* Cc/ld.scm 163 */
								BgL_namez00_819 = BGl_string2362z00zzcc_ldz00;
							}
					}
				{	/* Cc/ld.scm 166 */
					obj_t BgL_arg1982z00_820;
					obj_t BgL_arg1983z00_821;

					{	/* Cc/ld.scm 166 */
						obj_t BgL_arg1984z00_822;

						if (STRINGP(BgL_namez00_819))
							{	/* Cc/ld.scm 166 */
								BgL_arg1984z00_822 = BgL_namez00_819;
							}
						else
							{	/* Cc/ld.scm 168 */
								obj_t BgL_arg1455z00_1527;

								BgL_arg1455z00_1527 =
									SYMBOL_TO_STRING(((obj_t) BgL_namez00_819));
								BgL_arg1984z00_822 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1455z00_1527);
							}
						BgL_arg1982z00_820 = BGl_prefixz00zz__osz00(BgL_arg1984z00_822);
					}
					BgL_arg1983z00_821 =
						BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(25));
					return
						string_append_3(BgL_arg1982z00_820, BGl_string2358z00zzcc_ldz00,
						BgL_arg1983z00_821);
				}
			}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zzcc_ldz00(obj_t BgL_pathz00_318)
	{
		{	/* Cc/ld.scm 280 */
			if (NULLP(BgL_pathz00_318))
				{	/* Cc/ld.scm 281 */
					return BGl_string2347z00zzcc_ldz00;
				}
			else
				{	/* Cc/ld.scm 284 */
					obj_t BgL_arg1559z00_321;
					obj_t BgL_arg1561z00_322;

					BgL_arg1559z00_321 = CAR(((obj_t) BgL_pathz00_318));
					{	/* Cc/ld.scm 286 */
						obj_t BgL_arg1573z00_327;

						BgL_arg1573z00_327 = CDR(((obj_t) BgL_pathz00_318));
						BgL_arg1561z00_322 = BGl_loopze71ze7zzcc_ldz00(BgL_arg1573z00_327);
					}
					{	/* Cc/ld.scm 283 */
						obj_t BgL_list1562z00_323;

						{	/* Cc/ld.scm 283 */
							obj_t BgL_arg1564z00_324;

							{	/* Cc/ld.scm 283 */
								obj_t BgL_arg1565z00_325;

								{	/* Cc/ld.scm 283 */
									obj_t BgL_arg1571z00_326;

									BgL_arg1571z00_326 =
										MAKE_YOUNG_PAIR(BgL_arg1561z00_322, BNIL);
									BgL_arg1565z00_325 =
										MAKE_YOUNG_PAIR(BGl_string2357z00zzcc_ldz00,
										BgL_arg1571z00_326);
								}
								BgL_arg1564z00_324 =
									MAKE_YOUNG_PAIR(BgL_arg1559z00_321, BgL_arg1565z00_325);
							}
							BgL_list1562z00_323 =
								MAKE_YOUNG_PAIR(BGl_string2360z00zzcc_ldz00,
								BgL_arg1564z00_324);
						}
						BGL_TAIL return
							BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1562z00_323);
					}
				}
		}

	}



/* &<@anonymous:1979> */
	obj_t BGl_z62zc3z04anonymousza31979ze3ze5zzcc_ldz00(obj_t BgL_envz00_1958)
	{
		{	/* Cc/ld.scm 180 */
			{	/* Cc/ld.scm 180 */
				obj_t BgL_port1049z00_1959;

				BgL_port1049z00_1959 =
					((obj_t) PROCEDURE_REF(BgL_envz00_1958, (int) (0L)));
				return bgl_close_input_port(BgL_port1049z00_1959);
			}
		}

	}



/* win32-ld */
	obj_t BGl_win32zd2ldzd2zzcc_ldz00(obj_t BgL_namez00_13)
	{
		{	/* Cc/ld.scm 319 */
			{	/* Cc/ld.scm 320 */
				obj_t BgL_list1998z00_849;

				{	/* Cc/ld.scm 320 */
					obj_t BgL_arg1999z00_850;

					{	/* Cc/ld.scm 320 */
						obj_t BgL_arg2000z00_851;

						{	/* Cc/ld.scm 320 */
							obj_t BgL_arg2001z00_852;

							BgL_arg2001z00_852 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
							BgL_arg2000z00_851 =
								MAKE_YOUNG_PAIR(BGl_string2353z00zzcc_ldz00,
								BgL_arg2001z00_852);
						}
						BgL_arg1999z00_850 =
							MAKE_YOUNG_PAIR(BGl_za2ccza2z00zzengine_paramz00,
							BgL_arg2000z00_851);
					}
					BgL_list1998z00_849 =
						MAKE_YOUNG_PAIR(BGl_string2354z00zzcc_ldz00, BgL_arg1999z00_850);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1998z00_849);
			}
			{	/* Cc/ld.scm 322 */
				obj_t BgL_staticpz00_853;

				{	/* Cc/ld.scm 323 */
					bool_t BgL__ortest_1090z00_976;

					if (CBOOL(BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(16))))
						{	/* Cc/ld.scm 323 */
							BgL__ortest_1090z00_976 = ((bool_t) 0);
						}
					else
						{	/* Cc/ld.scm 323 */
							BgL__ortest_1090z00_976 = ((bool_t) 1);
						}
					if (BgL__ortest_1090z00_976)
						{	/* Cc/ld.scm 323 */
							BgL_staticpz00_853 = BBOOL(BgL__ortest_1090z00_976);
						}
					else
						{	/* Cc/ld.scm 324 */
							obj_t BgL_port1091z00_977;

							{	/* Cc/ld.scm 324 */
								obj_t BgL_stringz00_1401;

								BgL_stringz00_1401 = BGl_za2ldzd2optionsza2zd2zzengine_paramz00;
								{	/* Cc/ld.scm 324 */
									long BgL_endz00_1403;

									BgL_endz00_1403 = STRING_LENGTH(((obj_t) BgL_stringz00_1401));
									{	/* Cc/ld.scm 324 */

										BgL_port1091z00_977 =
											BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
											(BgL_stringz00_1401, BINT(0L), BINT(BgL_endz00_1403));
							}}}
							{	/* Cc/ld.scm 324 */
								obj_t BgL_exitd1092z00_978;

								BgL_exitd1092z00_978 = BGL_EXITD_TOP_AS_OBJ();
								{	/* Cc/ld.scm 324 */
									obj_t BgL_zc3z04anonymousza32327ze3z87_1963;

									BgL_zc3z04anonymousza32327ze3z87_1963 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza32327ze3ze5zzcc_ldz00, (int) (0L),
										(int) (1L));
									PROCEDURE_SET(BgL_zc3z04anonymousza32327ze3z87_1963,
										(int) (0L), BgL_port1091z00_977);
									{	/* Cc/ld.scm 324 */
										obj_t BgL_arg1828z00_1732;

										{	/* Cc/ld.scm 324 */
											obj_t BgL_arg1829z00_1733;

											BgL_arg1829z00_1733 =
												BGL_EXITD_PROTECT(BgL_exitd1092z00_978);
											BgL_arg1828z00_1732 =
												MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza32327ze3z87_1963,
												BgL_arg1829z00_1733);
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1092z00_978,
											BgL_arg1828z00_1732);
										BUNSPEC;
									}
									{	/* Cc/ld.scm 324 */
										obj_t BgL_tmp1094z00_980;

										{
											obj_t BgL_iportz00_1022;
											long BgL_lastzd2matchzd2_1023;
											long BgL_forwardz00_1024;
											long BgL_bufposz00_1025;
											obj_t BgL_iportz00_1039;
											long BgL_lastzd2matchzd2_1040;
											long BgL_forwardz00_1041;
											long BgL_bufposz00_1042;
											obj_t BgL_iportz00_1060;
											long BgL_lastzd2matchzd2_1061;
											long BgL_forwardz00_1062;
											long BgL_bufposz00_1063;
											obj_t BgL_iportz00_1076;
											long BgL_lastzd2matchzd2_1077;
											long BgL_forwardz00_1078;
											long BgL_bufposz00_1079;
											obj_t BgL_iportz00_1096;
											long BgL_lastzd2matchzd2_1097;
											long BgL_forwardz00_1098;
											long BgL_bufposz00_1099;
											obj_t BgL_iportz00_1116;
											long BgL_lastzd2matchzd2_1117;
											long BgL_forwardz00_1118;
											long BgL_bufposz00_1119;
											obj_t BgL_iportz00_1136;
											long BgL_lastzd2matchzd2_1137;
											long BgL_forwardz00_1138;
											long BgL_bufposz00_1139;
											obj_t BgL_iportz00_1156;
											long BgL_lastzd2matchzd2_1157;
											long BgL_forwardz00_1158;
											long BgL_bufposz00_1159;
											obj_t BgL_iportz00_1176;
											long BgL_lastzd2matchzd2_1177;
											long BgL_forwardz00_1178;
											long BgL_bufposz00_1179;
											obj_t BgL_iportz00_1196;
											long BgL_lastzd2matchzd2_1197;
											long BgL_forwardz00_1198;
											long BgL_bufposz00_1199;
											obj_t BgL_iportz00_1219;
											long BgL_lastzd2matchzd2_1220;
											long BgL_forwardz00_1221;
											long BgL_bufposz00_1222;

											RGC_START_MATCH(BgL_port1091z00_977);
											{	/* Cc/ld.scm 324 */
												long BgL_matchz00_1353;

												{	/* Cc/ld.scm 324 */
													long BgL_arg2325z00_1358;
													long BgL_arg2326z00_1359;

													BgL_arg2325z00_1358 =
														RGC_BUFFER_FORWARD(BgL_port1091z00_977);
													BgL_arg2326z00_1359 =
														RGC_BUFFER_BUFPOS(BgL_port1091z00_977);
													BgL_iportz00_1219 = BgL_port1091z00_977;
													BgL_lastzd2matchzd2_1220 = 1L;
													BgL_forwardz00_1221 = BgL_arg2325z00_1358;
													BgL_bufposz00_1222 = BgL_arg2326z00_1359;
												BgL_zc3z04anonymousza32227ze3z87_1223:
													if ((BgL_forwardz00_1221 == BgL_bufposz00_1222))
														{	/* Cc/ld.scm 324 */
															if (rgc_fill_buffer(BgL_iportz00_1219))
																{	/* Cc/ld.scm 324 */
																	long BgL_arg2230z00_1226;
																	long BgL_arg2231z00_1227;

																	BgL_arg2230z00_1226 =
																		RGC_BUFFER_FORWARD(BgL_iportz00_1219);
																	BgL_arg2231z00_1227 =
																		RGC_BUFFER_BUFPOS(BgL_iportz00_1219);
																	{
																		long BgL_bufposz00_2941;
																		long BgL_forwardz00_2940;

																		BgL_forwardz00_2940 = BgL_arg2230z00_1226;
																		BgL_bufposz00_2941 = BgL_arg2231z00_1227;
																		BgL_bufposz00_1222 = BgL_bufposz00_2941;
																		BgL_forwardz00_1221 = BgL_forwardz00_2940;
																		goto BgL_zc3z04anonymousza32227ze3z87_1223;
																	}
																}
															else
																{	/* Cc/ld.scm 324 */
																	BgL_matchz00_1353 = BgL_lastzd2matchzd2_1220;
																}
														}
													else
														{	/* Cc/ld.scm 324 */
															int BgL_curz00_1228;

															BgL_curz00_1228 =
																RGC_BUFFER_GET_CHAR(BgL_iportz00_1219,
																BgL_forwardz00_1221);
															{	/* Cc/ld.scm 324 */

																if (((long) (BgL_curz00_1228) == 45L))
																	{	/* Cc/ld.scm 324 */
																		BgL_iportz00_1039 = BgL_iportz00_1219;
																		BgL_lastzd2matchzd2_1040 =
																			BgL_lastzd2matchzd2_1220;
																		BgL_forwardz00_1041 =
																			(1L + BgL_forwardz00_1221);
																		BgL_bufposz00_1042 = BgL_bufposz00_1222;
																	BgL_zc3z04anonymousza32112ze3z87_1043:
																		{	/* Cc/ld.scm 324 */
																			long BgL_newzd2matchzd2_1044;

																			RGC_STOP_MATCH(BgL_iportz00_1039,
																				BgL_forwardz00_1041);
																			BgL_newzd2matchzd2_1044 = 1L;
																			if (
																				(BgL_forwardz00_1041 ==
																					BgL_bufposz00_1042))
																				{	/* Cc/ld.scm 324 */
																					if (rgc_fill_buffer
																						(BgL_iportz00_1039))
																						{	/* Cc/ld.scm 324 */
																							long BgL_arg2115z00_1047;
																							long BgL_arg2116z00_1048;

																							BgL_arg2115z00_1047 =
																								RGC_BUFFER_FORWARD
																								(BgL_iportz00_1039);
																							BgL_arg2116z00_1048 =
																								RGC_BUFFER_BUFPOS
																								(BgL_iportz00_1039);
																							{
																								long BgL_bufposz00_2954;
																								long BgL_forwardz00_2953;

																								BgL_forwardz00_2953 =
																									BgL_arg2115z00_1047;
																								BgL_bufposz00_2954 =
																									BgL_arg2116z00_1048;
																								BgL_bufposz00_1042 =
																									BgL_bufposz00_2954;
																								BgL_forwardz00_1041 =
																									BgL_forwardz00_2953;
																								goto
																									BgL_zc3z04anonymousza32112ze3z87_1043;
																							}
																						}
																					else
																						{	/* Cc/ld.scm 324 */
																							BgL_matchz00_1353 =
																								BgL_newzd2matchzd2_1044;
																						}
																				}
																			else
																				{	/* Cc/ld.scm 324 */
																					int BgL_curz00_1049;

																					BgL_curz00_1049 =
																						RGC_BUFFER_GET_CHAR
																						(BgL_iportz00_1039,
																						BgL_forwardz00_1041);
																					{	/* Cc/ld.scm 324 */

																						if (
																							((long) (BgL_curz00_1049) ==
																								115L))
																							{	/* Cc/ld.scm 324 */
																								BgL_iportz00_1096 =
																									BgL_iportz00_1039;
																								BgL_lastzd2matchzd2_1097 =
																									BgL_newzd2matchzd2_1044;
																								BgL_forwardz00_1098 =
																									(1L + BgL_forwardz00_1041);
																								BgL_bufposz00_1099 =
																									BgL_bufposz00_1042;
																							BgL_zc3z04anonymousza32150ze3z87_1100:
																								if (
																									(BgL_forwardz00_1098 ==
																										BgL_bufposz00_1099))
																									{	/* Cc/ld.scm 324 */
																										if (rgc_fill_buffer
																											(BgL_iportz00_1096))
																											{	/* Cc/ld.scm 324 */
																												long
																													BgL_arg2154z00_1103;
																												long
																													BgL_arg2155z00_1104;
																												BgL_arg2154z00_1103 =
																													RGC_BUFFER_FORWARD
																													(BgL_iportz00_1096);
																												BgL_arg2155z00_1104 =
																													RGC_BUFFER_BUFPOS
																													(BgL_iportz00_1096);
																												{
																													long
																														BgL_bufposz00_2966;
																													long
																														BgL_forwardz00_2965;
																													BgL_forwardz00_2965 =
																														BgL_arg2154z00_1103;
																													BgL_bufposz00_2966 =
																														BgL_arg2155z00_1104;
																													BgL_bufposz00_1099 =
																														BgL_bufposz00_2966;
																													BgL_forwardz00_1098 =
																														BgL_forwardz00_2965;
																													goto
																														BgL_zc3z04anonymousza32150ze3z87_1100;
																												}
																											}
																										else
																											{	/* Cc/ld.scm 324 */
																												BgL_matchz00_1353 =
																													BgL_lastzd2matchzd2_1097;
																											}
																									}
																								else
																									{	/* Cc/ld.scm 324 */
																										int BgL_curz00_1105;

																										BgL_curz00_1105 =
																											RGC_BUFFER_GET_CHAR
																											(BgL_iportz00_1096,
																											BgL_forwardz00_1098);
																										{	/* Cc/ld.scm 324 */

																											if (
																												((long)
																													(BgL_curz00_1105) ==
																													116L))
																												{	/* Cc/ld.scm 324 */
																													BgL_iportz00_1116 =
																														BgL_iportz00_1096;
																													BgL_lastzd2matchzd2_1117
																														=
																														BgL_lastzd2matchzd2_1097;
																													BgL_forwardz00_1118 =
																														(1L +
																														BgL_forwardz00_1098);
																													BgL_bufposz00_1119 =
																														BgL_bufposz00_1099;
																												BgL_zc3z04anonymousza32164ze3z87_1120:
																													if (
																														(BgL_forwardz00_1118
																															==
																															BgL_bufposz00_1119))
																														{	/* Cc/ld.scm 324 */
																															if (rgc_fill_buffer(BgL_iportz00_1116))
																																{	/* Cc/ld.scm 324 */
																																	long
																																		BgL_arg2167z00_1123;
																																	long
																																		BgL_arg2168z00_1124;
																																	BgL_arg2167z00_1123
																																		=
																																		RGC_BUFFER_FORWARD
																																		(BgL_iportz00_1116);
																																	BgL_arg2168z00_1124
																																		=
																																		RGC_BUFFER_BUFPOS
																																		(BgL_iportz00_1116);
																																	{
																																		long
																																			BgL_bufposz00_2978;
																																		long
																																			BgL_forwardz00_2977;
																																		BgL_forwardz00_2977
																																			=
																																			BgL_arg2167z00_1123;
																																		BgL_bufposz00_2978
																																			=
																																			BgL_arg2168z00_1124;
																																		BgL_bufposz00_1119
																																			=
																																			BgL_bufposz00_2978;
																																		BgL_forwardz00_1118
																																			=
																																			BgL_forwardz00_2977;
																																		goto
																																			BgL_zc3z04anonymousza32164ze3z87_1120;
																																	}
																																}
																															else
																																{	/* Cc/ld.scm 324 */
																																	BgL_matchz00_1353
																																		=
																																		BgL_lastzd2matchzd2_1117;
																																}
																														}
																													else
																														{	/* Cc/ld.scm 324 */
																															int
																																BgL_curz00_1125;
																															BgL_curz00_1125 =
																																RGC_BUFFER_GET_CHAR
																																(BgL_iportz00_1116,
																																BgL_forwardz00_1118);
																															{	/* Cc/ld.scm 324 */

																																if (
																																	((long)
																																		(BgL_curz00_1125)
																																		== 97L))
																																	{	/* Cc/ld.scm 324 */
																																		BgL_iportz00_1136
																																			=
																																			BgL_iportz00_1116;
																																		BgL_lastzd2matchzd2_1137
																																			=
																																			BgL_lastzd2matchzd2_1117;
																																		BgL_forwardz00_1138
																																			=
																																			(1L +
																																			BgL_forwardz00_1118);
																																		BgL_bufposz00_1139
																																			=
																																			BgL_bufposz00_1119;
																																	BgL_zc3z04anonymousza32177ze3z87_1140:
																																		if (
																																			(BgL_forwardz00_1138
																																				==
																																				BgL_bufposz00_1139))
																																			{	/* Cc/ld.scm 324 */
																																				if (rgc_fill_buffer(BgL_iportz00_1136))
																																					{	/* Cc/ld.scm 324 */
																																						long
																																							BgL_arg2180z00_1143;
																																						long
																																							BgL_arg2181z00_1144;
																																						BgL_arg2180z00_1143
																																							=
																																							RGC_BUFFER_FORWARD
																																							(BgL_iportz00_1136);
																																						BgL_arg2181z00_1144
																																							=
																																							RGC_BUFFER_BUFPOS
																																							(BgL_iportz00_1136);
																																						{
																																							long
																																								BgL_bufposz00_2990;
																																							long
																																								BgL_forwardz00_2989;
																																							BgL_forwardz00_2989
																																								=
																																								BgL_arg2180z00_1143;
																																							BgL_bufposz00_2990
																																								=
																																								BgL_arg2181z00_1144;
																																							BgL_bufposz00_1139
																																								=
																																								BgL_bufposz00_2990;
																																							BgL_forwardz00_1138
																																								=
																																								BgL_forwardz00_2989;
																																							goto
																																								BgL_zc3z04anonymousza32177ze3z87_1140;
																																						}
																																					}
																																				else
																																					{	/* Cc/ld.scm 324 */
																																						BgL_matchz00_1353
																																							=
																																							BgL_lastzd2matchzd2_1137;
																																					}
																																			}
																																		else
																																			{	/* Cc/ld.scm 324 */
																																				int
																																					BgL_curz00_1145;
																																				BgL_curz00_1145
																																					=
																																					RGC_BUFFER_GET_CHAR
																																					(BgL_iportz00_1136,
																																					BgL_forwardz00_1138);
																																				{	/* Cc/ld.scm 324 */

																																					if (
																																						((long) (BgL_curz00_1145) == 116L))
																																						{	/* Cc/ld.scm 324 */
																																							BgL_iportz00_1156
																																								=
																																								BgL_iportz00_1136;
																																							BgL_lastzd2matchzd2_1157
																																								=
																																								BgL_lastzd2matchzd2_1137;
																																							BgL_forwardz00_1158
																																								=
																																								(1L
																																								+
																																								BgL_forwardz00_1138);
																																							BgL_bufposz00_1159
																																								=
																																								BgL_bufposz00_1139;
																																						BgL_zc3z04anonymousza32190ze3z87_1160:
																																							if ((BgL_forwardz00_1158 == BgL_bufposz00_1159))
																																								{	/* Cc/ld.scm 324 */
																																									if (rgc_fill_buffer(BgL_iportz00_1156))
																																										{	/* Cc/ld.scm 324 */
																																											long
																																												BgL_arg2193z00_1163;
																																											long
																																												BgL_arg2194z00_1164;
																																											BgL_arg2193z00_1163
																																												=
																																												RGC_BUFFER_FORWARD
																																												(BgL_iportz00_1156);
																																											BgL_arg2194z00_1164
																																												=
																																												RGC_BUFFER_BUFPOS
																																												(BgL_iportz00_1156);
																																											{
																																												long
																																													BgL_bufposz00_3002;
																																												long
																																													BgL_forwardz00_3001;
																																												BgL_forwardz00_3001
																																													=
																																													BgL_arg2193z00_1163;
																																												BgL_bufposz00_3002
																																													=
																																													BgL_arg2194z00_1164;
																																												BgL_bufposz00_1159
																																													=
																																													BgL_bufposz00_3002;
																																												BgL_forwardz00_1158
																																													=
																																													BgL_forwardz00_3001;
																																												goto
																																													BgL_zc3z04anonymousza32190ze3z87_1160;
																																											}
																																										}
																																									else
																																										{	/* Cc/ld.scm 324 */
																																											BgL_matchz00_1353
																																												=
																																												BgL_lastzd2matchzd2_1157;
																																										}
																																								}
																																							else
																																								{	/* Cc/ld.scm 324 */
																																									int
																																										BgL_curz00_1165;
																																									BgL_curz00_1165
																																										=
																																										RGC_BUFFER_GET_CHAR
																																										(BgL_iportz00_1156,
																																										BgL_forwardz00_1158);
																																									{	/* Cc/ld.scm 324 */

																																										if (((long) (BgL_curz00_1165) == 105L))
																																											{	/* Cc/ld.scm 324 */
																																												BgL_iportz00_1176
																																													=
																																													BgL_iportz00_1156;
																																												BgL_lastzd2matchzd2_1177
																																													=
																																													BgL_lastzd2matchzd2_1157;
																																												BgL_forwardz00_1178
																																													=
																																													(1L
																																													+
																																													BgL_forwardz00_1158);
																																												BgL_bufposz00_1179
																																													=
																																													BgL_bufposz00_1159;
																																											BgL_zc3z04anonymousza32203ze3z87_1180:
																																												if ((BgL_forwardz00_1178 == BgL_bufposz00_1179))
																																													{	/* Cc/ld.scm 324 */
																																														if (rgc_fill_buffer(BgL_iportz00_1176))
																																															{	/* Cc/ld.scm 324 */
																																																long
																																																	BgL_arg2206z00_1183;
																																																long
																																																	BgL_arg2207z00_1184;
																																																BgL_arg2206z00_1183
																																																	=
																																																	RGC_BUFFER_FORWARD
																																																	(BgL_iportz00_1176);
																																																BgL_arg2207z00_1184
																																																	=
																																																	RGC_BUFFER_BUFPOS
																																																	(BgL_iportz00_1176);
																																																{
																																																	long
																																																		BgL_bufposz00_3014;
																																																	long
																																																		BgL_forwardz00_3013;
																																																	BgL_forwardz00_3013
																																																		=
																																																		BgL_arg2206z00_1183;
																																																	BgL_bufposz00_3014
																																																		=
																																																		BgL_arg2207z00_1184;
																																																	BgL_bufposz00_1179
																																																		=
																																																		BgL_bufposz00_3014;
																																																	BgL_forwardz00_1178
																																																		=
																																																		BgL_forwardz00_3013;
																																																	goto
																																																		BgL_zc3z04anonymousza32203ze3z87_1180;
																																																}
																																															}
																																														else
																																															{	/* Cc/ld.scm 324 */
																																																BgL_matchz00_1353
																																																	=
																																																	BgL_lastzd2matchzd2_1177;
																																															}
																																													}
																																												else
																																													{	/* Cc/ld.scm 324 */
																																														int
																																															BgL_curz00_1185;
																																														BgL_curz00_1185
																																															=
																																															RGC_BUFFER_GET_CHAR
																																															(BgL_iportz00_1176,
																																															BgL_forwardz00_1178);
																																														{	/* Cc/ld.scm 324 */

																																															if (((long) (BgL_curz00_1185) == 99L))
																																																{	/* Cc/ld.scm 324 */
																																																	BgL_iportz00_1196
																																																		=
																																																		BgL_iportz00_1176;
																																																	BgL_lastzd2matchzd2_1197
																																																		=
																																																		BgL_lastzd2matchzd2_1177;
																																																	BgL_forwardz00_1198
																																																		=
																																																		(1L
																																																		+
																																																		BgL_forwardz00_1178);
																																																	BgL_bufposz00_1199
																																																		=
																																																		BgL_bufposz00_1179;
																																																BgL_zc3z04anonymousza32216ze3z87_1200:
																																																	{	/* Cc/ld.scm 324 */
																																																		long
																																																			BgL_newzd2matchzd2_1201;
																																																		RGC_STOP_MATCH
																																																			(BgL_iportz00_1196,
																																																			BgL_forwardz00_1198);
																																																		BgL_newzd2matchzd2_1201
																																																			=
																																																			0L;
																																																		if ((BgL_forwardz00_1198 == BgL_bufposz00_1199))
																																																			{	/* Cc/ld.scm 324 */
																																																				if (rgc_fill_buffer(BgL_iportz00_1196))
																																																					{	/* Cc/ld.scm 324 */
																																																						long
																																																							BgL_arg2219z00_1204;
																																																						long
																																																							BgL_arg2220z00_1205;
																																																						BgL_arg2219z00_1204
																																																							=
																																																							RGC_BUFFER_FORWARD
																																																							(BgL_iportz00_1196);
																																																						BgL_arg2220z00_1205
																																																							=
																																																							RGC_BUFFER_BUFPOS
																																																							(BgL_iportz00_1196);
																																																						{
																																																							long
																																																								BgL_bufposz00_3027;
																																																							long
																																																								BgL_forwardz00_3026;
																																																							BgL_forwardz00_3026
																																																								=
																																																								BgL_arg2219z00_1204;
																																																							BgL_bufposz00_3027
																																																								=
																																																								BgL_arg2220z00_1205;
																																																							BgL_bufposz00_1199
																																																								=
																																																								BgL_bufposz00_3027;
																																																							BgL_forwardz00_1198
																																																								=
																																																								BgL_forwardz00_3026;
																																																							goto
																																																								BgL_zc3z04anonymousza32216ze3z87_1200;
																																																						}
																																																					}
																																																				else
																																																					{	/* Cc/ld.scm 324 */
																																																						BgL_matchz00_1353
																																																							=
																																																							BgL_newzd2matchzd2_1201;
																																																					}
																																																			}
																																																		else
																																																			{	/* Cc/ld.scm 324 */
																																																				int
																																																					BgL_curz00_1206;
																																																				BgL_curz00_1206
																																																					=
																																																					RGC_BUFFER_GET_CHAR
																																																					(BgL_iportz00_1196,
																																																					BgL_forwardz00_1198);
																																																				{	/* Cc/ld.scm 324 */

																																																					if (((long) (BgL_curz00_1206) == 45L))
																																																						{	/* Cc/ld.scm 324 */
																																																							BgL_iportz00_1076
																																																								=
																																																								BgL_iportz00_1196;
																																																							BgL_lastzd2matchzd2_1077
																																																								=
																																																								BgL_newzd2matchzd2_1201;
																																																							BgL_forwardz00_1078
																																																								=
																																																								(1L
																																																								+
																																																								BgL_forwardz00_1198);
																																																							BgL_bufposz00_1079
																																																								=
																																																								BgL_bufposz00_1199;
																																																						BgL_zc3z04anonymousza32136ze3z87_1080:
																																																							if ((BgL_forwardz00_1078 == BgL_bufposz00_1079))
																																																								{	/* Cc/ld.scm 324 */
																																																									if (rgc_fill_buffer(BgL_iportz00_1076))
																																																										{	/* Cc/ld.scm 324 */
																																																											long
																																																												BgL_arg2139z00_1083;
																																																											long
																																																												BgL_arg2141z00_1084;
																																																											BgL_arg2139z00_1083
																																																												=
																																																												RGC_BUFFER_FORWARD
																																																												(BgL_iportz00_1076);
																																																											BgL_arg2141z00_1084
																																																												=
																																																												RGC_BUFFER_BUFPOS
																																																												(BgL_iportz00_1076);
																																																											{
																																																												long
																																																													BgL_bufposz00_3039;
																																																												long
																																																													BgL_forwardz00_3038;
																																																												BgL_forwardz00_3038
																																																													=
																																																													BgL_arg2139z00_1083;
																																																												BgL_bufposz00_3039
																																																													=
																																																													BgL_arg2141z00_1084;
																																																												BgL_bufposz00_1079
																																																													=
																																																													BgL_bufposz00_3039;
																																																												BgL_forwardz00_1078
																																																													=
																																																													BgL_forwardz00_3038;
																																																												goto
																																																													BgL_zc3z04anonymousza32136ze3z87_1080;
																																																											}
																																																										}
																																																									else
																																																										{	/* Cc/ld.scm 324 */
																																																											BgL_matchz00_1353
																																																												=
																																																												BgL_lastzd2matchzd2_1077;
																																																										}
																																																								}
																																																							else
																																																								{	/* Cc/ld.scm 324 */
																																																									int
																																																										BgL_curz00_1085;
																																																									BgL_curz00_1085
																																																										=
																																																										RGC_BUFFER_GET_CHAR
																																																										(BgL_iportz00_1076,
																																																										BgL_forwardz00_1078);
																																																									{	/* Cc/ld.scm 324 */

																																																										if (((long) (BgL_curz00_1085) == 115L))
																																																											{
																																																												long
																																																													BgL_bufposz00_3048;
																																																												long
																																																													BgL_forwardz00_3046;
																																																												long
																																																													BgL_lastzd2matchzd2_3045;
																																																												obj_t
																																																													BgL_iportz00_3044;
																																																												BgL_iportz00_3044
																																																													=
																																																													BgL_iportz00_1076;
																																																												BgL_lastzd2matchzd2_3045
																																																													=
																																																													BgL_lastzd2matchzd2_1077;
																																																												BgL_forwardz00_3046
																																																													=
																																																													(1L
																																																													+
																																																													BgL_forwardz00_1078);
																																																												BgL_bufposz00_3048
																																																													=
																																																													BgL_bufposz00_1079;
																																																												BgL_bufposz00_1099
																																																													=
																																																													BgL_bufposz00_3048;
																																																												BgL_forwardz00_1098
																																																													=
																																																													BgL_forwardz00_3046;
																																																												BgL_lastzd2matchzd2_1097
																																																													=
																																																													BgL_lastzd2matchzd2_3045;
																																																												BgL_iportz00_1096
																																																													=
																																																													BgL_iportz00_3044;
																																																												goto
																																																													BgL_zc3z04anonymousza32150ze3z87_1100;
																																																											}
																																																										else
																																																											{	/* Cc/ld.scm 324 */
																																																												if (((long) (BgL_curz00_1085) == 45L))
																																																													{
																																																														long
																																																															BgL_forwardz00_3052;
																																																														BgL_forwardz00_3052
																																																															=
																																																															(1L
																																																															+
																																																															BgL_forwardz00_1078);
																																																														BgL_forwardz00_1078
																																																															=
																																																															BgL_forwardz00_3052;
																																																														goto
																																																															BgL_zc3z04anonymousza32136ze3z87_1080;
																																																													}
																																																												else
																																																													{	/* Cc/ld.scm 324 */
																																																														bool_t
																																																															BgL_test2584z00_3054;
																																																														if (((long) (BgL_curz00_1085) == 10L))
																																																															{	/* Cc/ld.scm 324 */
																																																																BgL_test2584z00_3054
																																																																	=
																																																																	(
																																																																	(bool_t)
																																																																	1);
																																																															}
																																																														else
																																																															{	/* Cc/ld.scm 324 */
																																																																if (((long) (BgL_curz00_1085) == 45L))
																																																																	{	/* Cc/ld.scm 324 */
																																																																		BgL_test2584z00_3054
																																																																			=
																																																																			(
																																																																			(bool_t)
																																																																			1);
																																																																	}
																																																																else
																																																																	{	/* Cc/ld.scm 324 */
																																																																		BgL_test2584z00_3054
																																																																			=
																																																																			(
																																																																			(long)
																																																																			(BgL_curz00_1085)
																																																																			==
																																																																			115L);
																																																															}}
																																																														if (BgL_test2584z00_3054)
																																																															{	/* Cc/ld.scm 324 */
																																																																BgL_matchz00_1353
																																																																	=
																																																																	BgL_lastzd2matchzd2_1077;
																																																															}
																																																														else
																																																															{	/* Cc/ld.scm 324 */
																																																																BgL_iportz00_1060
																																																																	=
																																																																	BgL_iportz00_1076;
																																																																BgL_lastzd2matchzd2_1061
																																																																	=
																																																																	BgL_lastzd2matchzd2_1077;
																																																																BgL_forwardz00_1062
																																																																	=
																																																																	(1L
																																																																	+
																																																																	BgL_forwardz00_1078);
																																																																BgL_bufposz00_1063
																																																																	=
																																																																	BgL_bufposz00_1079;
																																																															BgL_zc3z04anonymousza32125ze3z87_1064:
																																																																if ((BgL_forwardz00_1062 == BgL_bufposz00_1063))
																																																																	{	/* Cc/ld.scm 324 */
																																																																		if (rgc_fill_buffer(BgL_iportz00_1060))
																																																																			{	/* Cc/ld.scm 324 */
																																																																				long
																																																																					BgL_arg2129z00_1067;
																																																																				long
																																																																					BgL_arg2130z00_1068;
																																																																				BgL_arg2129z00_1067
																																																																					=
																																																																					RGC_BUFFER_FORWARD
																																																																					(BgL_iportz00_1060);
																																																																				BgL_arg2130z00_1068
																																																																					=
																																																																					RGC_BUFFER_BUFPOS
																																																																					(BgL_iportz00_1060);
																																																																				{
																																																																					long
																																																																						BgL_bufposz00_3070;
																																																																					long
																																																																						BgL_forwardz00_3069;
																																																																					BgL_forwardz00_3069
																																																																						=
																																																																						BgL_arg2129z00_1067;
																																																																					BgL_bufposz00_3070
																																																																						=
																																																																						BgL_arg2130z00_1068;
																																																																					BgL_bufposz00_1063
																																																																						=
																																																																						BgL_bufposz00_3070;
																																																																					BgL_forwardz00_1062
																																																																						=
																																																																						BgL_forwardz00_3069;
																																																																					goto
																																																																						BgL_zc3z04anonymousza32125ze3z87_1064;
																																																																				}
																																																																			}
																																																																		else
																																																																			{	/* Cc/ld.scm 324 */
																																																																				BgL_matchz00_1353
																																																																					=
																																																																					BgL_lastzd2matchzd2_1061;
																																																																			}
																																																																	}
																																																																else
																																																																	{	/* Cc/ld.scm 324 */
																																																																		int
																																																																			BgL_curz00_1069;
																																																																		BgL_curz00_1069
																																																																			=
																																																																			RGC_BUFFER_GET_CHAR
																																																																			(BgL_iportz00_1060,
																																																																			BgL_forwardz00_1062);
																																																																		{	/* Cc/ld.scm 324 */

																																																																			if (((long) (BgL_curz00_1069) == 45L))
																																																																				{
																																																																					long
																																																																						BgL_bufposz00_3079;
																																																																					long
																																																																						BgL_forwardz00_3077;
																																																																					long
																																																																						BgL_lastzd2matchzd2_3076;
																																																																					obj_t
																																																																						BgL_iportz00_3075;
																																																																					BgL_iportz00_3075
																																																																						=
																																																																						BgL_iportz00_1060;
																																																																					BgL_lastzd2matchzd2_3076
																																																																						=
																																																																						BgL_lastzd2matchzd2_1061;
																																																																					BgL_forwardz00_3077
																																																																						=
																																																																						(1L
																																																																						+
																																																																						BgL_forwardz00_1062);
																																																																					BgL_bufposz00_3079
																																																																						=
																																																																						BgL_bufposz00_1063;
																																																																					BgL_bufposz00_1079
																																																																						=
																																																																						BgL_bufposz00_3079;
																																																																					BgL_forwardz00_1078
																																																																						=
																																																																						BgL_forwardz00_3077;
																																																																					BgL_lastzd2matchzd2_1077
																																																																						=
																																																																						BgL_lastzd2matchzd2_3076;
																																																																					BgL_iportz00_1076
																																																																						=
																																																																						BgL_iportz00_3075;
																																																																					goto
																																																																						BgL_zc3z04anonymousza32136ze3z87_1080;
																																																																				}
																																																																			else
																																																																				{	/* Cc/ld.scm 324 */
																																																																					bool_t
																																																																						BgL_test2590z00_3080;
																																																																					if (((long) (BgL_curz00_1069) == 10L))
																																																																						{	/* Cc/ld.scm 324 */
																																																																							BgL_test2590z00_3080
																																																																								=
																																																																								(
																																																																								(bool_t)
																																																																								1);
																																																																						}
																																																																					else
																																																																						{	/* Cc/ld.scm 324 */
																																																																							BgL_test2590z00_3080
																																																																								=
																																																																								(
																																																																								(long)
																																																																								(BgL_curz00_1069)
																																																																								==
																																																																								45L);
																																																																						}
																																																																					if (BgL_test2590z00_3080)
																																																																						{	/* Cc/ld.scm 324 */
																																																																							BgL_matchz00_1353
																																																																								=
																																																																								BgL_lastzd2matchzd2_1061;
																																																																						}
																																																																					else
																																																																						{
																																																																							long
																																																																								BgL_forwardz00_3086;
																																																																							BgL_forwardz00_3086
																																																																								=
																																																																								(1L
																																																																								+
																																																																								BgL_forwardz00_1062);
																																																																							BgL_forwardz00_1062
																																																																								=
																																																																								BgL_forwardz00_3086;
																																																																							goto
																																																																								BgL_zc3z04anonymousza32125ze3z87_1064;
																																																																						}
																																																																				}
																																																																		}
																																																																	}
																																																															}
																																																													}
																																																											}
																																																									}
																																																								}
																																																						}
																																																					else
																																																						{	/* Cc/ld.scm 324 */
																																																							bool_t
																																																								BgL_test2592z00_3090;
																																																							if (((long) (BgL_curz00_1206) == 10L))
																																																								{	/* Cc/ld.scm 324 */
																																																									BgL_test2592z00_3090
																																																										=
																																																										(
																																																										(bool_t)
																																																										1);
																																																								}
																																																							else
																																																								{	/* Cc/ld.scm 324 */
																																																									BgL_test2592z00_3090
																																																										=
																																																										(
																																																										(long)
																																																										(BgL_curz00_1206)
																																																										==
																																																										45L);
																																																								}
																																																							if (BgL_test2592z00_3090)
																																																								{	/* Cc/ld.scm 324 */
																																																									BgL_matchz00_1353
																																																										=
																																																										BgL_newzd2matchzd2_1201;
																																																								}
																																																							else
																																																								{
																																																									long
																																																										BgL_bufposz00_3100;
																																																									long
																																																										BgL_forwardz00_3098;
																																																									long
																																																										BgL_lastzd2matchzd2_3097;
																																																									obj_t
																																																										BgL_iportz00_3096;
																																																									BgL_iportz00_3096
																																																										=
																																																										BgL_iportz00_1196;
																																																									BgL_lastzd2matchzd2_3097
																																																										=
																																																										BgL_newzd2matchzd2_1201;
																																																									BgL_forwardz00_3098
																																																										=
																																																										(1L
																																																										+
																																																										BgL_forwardz00_1198);
																																																									BgL_bufposz00_3100
																																																										=
																																																										BgL_bufposz00_1199;
																																																									BgL_bufposz00_1063
																																																										=
																																																										BgL_bufposz00_3100;
																																																									BgL_forwardz00_1062
																																																										=
																																																										BgL_forwardz00_3098;
																																																									BgL_lastzd2matchzd2_1061
																																																										=
																																																										BgL_lastzd2matchzd2_3097;
																																																									BgL_iportz00_1060
																																																										=
																																																										BgL_iportz00_3096;
																																																									goto
																																																										BgL_zc3z04anonymousza32125ze3z87_1064;
																																																								}
																																																						}
																																																				}
																																																			}
																																																	}
																																																}
																																															else
																																																{	/* Cc/ld.scm 324 */
																																																	if (((long) (BgL_curz00_1185) == 45L))
																																																		{
																																																			long
																																																				BgL_bufposz00_3109;
																																																			long
																																																				BgL_forwardz00_3107;
																																																			long
																																																				BgL_lastzd2matchzd2_3106;
																																																			obj_t
																																																				BgL_iportz00_3105;
																																																			BgL_iportz00_3105
																																																				=
																																																				BgL_iportz00_1176;
																																																			BgL_lastzd2matchzd2_3106
																																																				=
																																																				BgL_lastzd2matchzd2_1177;
																																																			BgL_forwardz00_3107
																																																				=
																																																				(1L
																																																				+
																																																				BgL_forwardz00_1178);
																																																			BgL_bufposz00_3109
																																																				=
																																																				BgL_bufposz00_1179;
																																																			BgL_bufposz00_1079
																																																				=
																																																				BgL_bufposz00_3109;
																																																			BgL_forwardz00_1078
																																																				=
																																																				BgL_forwardz00_3107;
																																																			BgL_lastzd2matchzd2_1077
																																																				=
																																																				BgL_lastzd2matchzd2_3106;
																																																			BgL_iportz00_1076
																																																				=
																																																				BgL_iportz00_3105;
																																																			goto
																																																				BgL_zc3z04anonymousza32136ze3z87_1080;
																																																		}
																																																	else
																																																		{	/* Cc/ld.scm 324 */
																																																			bool_t
																																																				BgL_test2595z00_3110;
																																																			if (((long) (BgL_curz00_1185) == 10L))
																																																				{	/* Cc/ld.scm 324 */
																																																					BgL_test2595z00_3110
																																																						=
																																																						(
																																																						(bool_t)
																																																						1);
																																																				}
																																																			else
																																																				{	/* Cc/ld.scm 324 */
																																																					if (((long) (BgL_curz00_1185) == 45L))
																																																						{	/* Cc/ld.scm 324 */
																																																							BgL_test2595z00_3110
																																																								=
																																																								(
																																																								(bool_t)
																																																								1);
																																																						}
																																																					else
																																																						{	/* Cc/ld.scm 324 */
																																																							BgL_test2595z00_3110
																																																								=
																																																								(
																																																								(long)
																																																								(BgL_curz00_1185)
																																																								==
																																																								99L);
																																																				}}
																																																			if (BgL_test2595z00_3110)
																																																				{	/* Cc/ld.scm 324 */
																																																					BgL_matchz00_1353
																																																						=
																																																						BgL_lastzd2matchzd2_1177;
																																																				}
																																																			else
																																																				{
																																																					long
																																																						BgL_bufposz00_3123;
																																																					long
																																																						BgL_forwardz00_3121;
																																																					long
																																																						BgL_lastzd2matchzd2_3120;
																																																					obj_t
																																																						BgL_iportz00_3119;
																																																					BgL_iportz00_3119
																																																						=
																																																						BgL_iportz00_1176;
																																																					BgL_lastzd2matchzd2_3120
																																																						=
																																																						BgL_lastzd2matchzd2_1177;
																																																					BgL_forwardz00_3121
																																																						=
																																																						(1L
																																																						+
																																																						BgL_forwardz00_1178);
																																																					BgL_bufposz00_3123
																																																						=
																																																						BgL_bufposz00_1179;
																																																					BgL_bufposz00_1063
																																																						=
																																																						BgL_bufposz00_3123;
																																																					BgL_forwardz00_1062
																																																						=
																																																						BgL_forwardz00_3121;
																																																					BgL_lastzd2matchzd2_1061
																																																						=
																																																						BgL_lastzd2matchzd2_3120;
																																																					BgL_iportz00_1060
																																																						=
																																																						BgL_iportz00_3119;
																																																					goto
																																																						BgL_zc3z04anonymousza32125ze3z87_1064;
																																																				}
																																																		}
																																																}
																																														}
																																													}
																																											}
																																										else
																																											{	/* Cc/ld.scm 324 */
																																												if (((long) (BgL_curz00_1165) == 45L))
																																													{
																																														long
																																															BgL_bufposz00_3132;
																																														long
																																															BgL_forwardz00_3130;
																																														long
																																															BgL_lastzd2matchzd2_3129;
																																														obj_t
																																															BgL_iportz00_3128;
																																														BgL_iportz00_3128
																																															=
																																															BgL_iportz00_1156;
																																														BgL_lastzd2matchzd2_3129
																																															=
																																															BgL_lastzd2matchzd2_1157;
																																														BgL_forwardz00_3130
																																															=
																																															(1L
																																															+
																																															BgL_forwardz00_1158);
																																														BgL_bufposz00_3132
																																															=
																																															BgL_bufposz00_1159;
																																														BgL_bufposz00_1079
																																															=
																																															BgL_bufposz00_3132;
																																														BgL_forwardz00_1078
																																															=
																																															BgL_forwardz00_3130;
																																														BgL_lastzd2matchzd2_1077
																																															=
																																															BgL_lastzd2matchzd2_3129;
																																														BgL_iportz00_1076
																																															=
																																															BgL_iportz00_3128;
																																														goto
																																															BgL_zc3z04anonymousza32136ze3z87_1080;
																																													}
																																												else
																																													{	/* Cc/ld.scm 324 */
																																														bool_t
																																															BgL_test2599z00_3133;
																																														if (((long) (BgL_curz00_1165) == 10L))
																																															{	/* Cc/ld.scm 324 */
																																																BgL_test2599z00_3133
																																																	=
																																																	(
																																																	(bool_t)
																																																	1);
																																															}
																																														else
																																															{	/* Cc/ld.scm 324 */
																																																if (((long) (BgL_curz00_1165) == 45L))
																																																	{	/* Cc/ld.scm 324 */
																																																		BgL_test2599z00_3133
																																																			=
																																																			(
																																																			(bool_t)
																																																			1);
																																																	}
																																																else
																																																	{	/* Cc/ld.scm 324 */
																																																		BgL_test2599z00_3133
																																																			=
																																																			(
																																																			(long)
																																																			(BgL_curz00_1165)
																																																			==
																																																			105L);
																																															}}
																																														if (BgL_test2599z00_3133)
																																															{	/* Cc/ld.scm 324 */
																																																BgL_matchz00_1353
																																																	=
																																																	BgL_lastzd2matchzd2_1157;
																																															}
																																														else
																																															{
																																																long
																																																	BgL_bufposz00_3146;
																																																long
																																																	BgL_forwardz00_3144;
																																																long
																																																	BgL_lastzd2matchzd2_3143;
																																																obj_t
																																																	BgL_iportz00_3142;
																																																BgL_iportz00_3142
																																																	=
																																																	BgL_iportz00_1156;
																																																BgL_lastzd2matchzd2_3143
																																																	=
																																																	BgL_lastzd2matchzd2_1157;
																																																BgL_forwardz00_3144
																																																	=
																																																	(1L
																																																	+
																																																	BgL_forwardz00_1158);
																																																BgL_bufposz00_3146
																																																	=
																																																	BgL_bufposz00_1159;
																																																BgL_bufposz00_1063
																																																	=
																																																	BgL_bufposz00_3146;
																																																BgL_forwardz00_1062
																																																	=
																																																	BgL_forwardz00_3144;
																																																BgL_lastzd2matchzd2_1061
																																																	=
																																																	BgL_lastzd2matchzd2_3143;
																																																BgL_iportz00_1060
																																																	=
																																																	BgL_iportz00_3142;
																																																goto
																																																	BgL_zc3z04anonymousza32125ze3z87_1064;
																																															}
																																													}
																																											}
																																									}
																																								}
																																						}
																																					else
																																						{	/* Cc/ld.scm 324 */
																																							if (((long) (BgL_curz00_1145) == 45L))
																																								{
																																									long
																																										BgL_bufposz00_3155;
																																									long
																																										BgL_forwardz00_3153;
																																									long
																																										BgL_lastzd2matchzd2_3152;
																																									obj_t
																																										BgL_iportz00_3151;
																																									BgL_iportz00_3151
																																										=
																																										BgL_iportz00_1136;
																																									BgL_lastzd2matchzd2_3152
																																										=
																																										BgL_lastzd2matchzd2_1137;
																																									BgL_forwardz00_3153
																																										=
																																										(1L
																																										+
																																										BgL_forwardz00_1138);
																																									BgL_bufposz00_3155
																																										=
																																										BgL_bufposz00_1139;
																																									BgL_bufposz00_1079
																																										=
																																										BgL_bufposz00_3155;
																																									BgL_forwardz00_1078
																																										=
																																										BgL_forwardz00_3153;
																																									BgL_lastzd2matchzd2_1077
																																										=
																																										BgL_lastzd2matchzd2_3152;
																																									BgL_iportz00_1076
																																										=
																																										BgL_iportz00_3151;
																																									goto
																																										BgL_zc3z04anonymousza32136ze3z87_1080;
																																								}
																																							else
																																								{	/* Cc/ld.scm 324 */
																																									bool_t
																																										BgL_test2603z00_3156;
																																									if (((long) (BgL_curz00_1145) == 10L))
																																										{	/* Cc/ld.scm 324 */
																																											BgL_test2603z00_3156
																																												=
																																												(
																																												(bool_t)
																																												1);
																																										}
																																									else
																																										{	/* Cc/ld.scm 324 */
																																											if (((long) (BgL_curz00_1145) == 45L))
																																												{	/* Cc/ld.scm 324 */
																																													BgL_test2603z00_3156
																																														=
																																														(
																																														(bool_t)
																																														1);
																																												}
																																											else
																																												{	/* Cc/ld.scm 324 */
																																													BgL_test2603z00_3156
																																														=
																																														(
																																														(long)
																																														(BgL_curz00_1145)
																																														==
																																														116L);
																																										}}
																																									if (BgL_test2603z00_3156)
																																										{	/* Cc/ld.scm 324 */
																																											BgL_matchz00_1353
																																												=
																																												BgL_lastzd2matchzd2_1137;
																																										}
																																									else
																																										{
																																											long
																																												BgL_bufposz00_3169;
																																											long
																																												BgL_forwardz00_3167;
																																											long
																																												BgL_lastzd2matchzd2_3166;
																																											obj_t
																																												BgL_iportz00_3165;
																																											BgL_iportz00_3165
																																												=
																																												BgL_iportz00_1136;
																																											BgL_lastzd2matchzd2_3166
																																												=
																																												BgL_lastzd2matchzd2_1137;
																																											BgL_forwardz00_3167
																																												=
																																												(1L
																																												+
																																												BgL_forwardz00_1138);
																																											BgL_bufposz00_3169
																																												=
																																												BgL_bufposz00_1139;
																																											BgL_bufposz00_1063
																																												=
																																												BgL_bufposz00_3169;
																																											BgL_forwardz00_1062
																																												=
																																												BgL_forwardz00_3167;
																																											BgL_lastzd2matchzd2_1061
																																												=
																																												BgL_lastzd2matchzd2_3166;
																																											BgL_iportz00_1060
																																												=
																																												BgL_iportz00_3165;
																																											goto
																																												BgL_zc3z04anonymousza32125ze3z87_1064;
																																										}
																																								}
																																						}
																																				}
																																			}
																																	}
																																else
																																	{	/* Cc/ld.scm 324 */
																																		if (
																																			((long)
																																				(BgL_curz00_1125)
																																				== 45L))
																																			{
																																				long
																																					BgL_bufposz00_3178;
																																				long
																																					BgL_forwardz00_3176;
																																				long
																																					BgL_lastzd2matchzd2_3175;
																																				obj_t
																																					BgL_iportz00_3174;
																																				BgL_iportz00_3174
																																					=
																																					BgL_iportz00_1116;
																																				BgL_lastzd2matchzd2_3175
																																					=
																																					BgL_lastzd2matchzd2_1117;
																																				BgL_forwardz00_3176
																																					=
																																					(1L +
																																					BgL_forwardz00_1118);
																																				BgL_bufposz00_3178
																																					=
																																					BgL_bufposz00_1119;
																																				BgL_bufposz00_1079
																																					=
																																					BgL_bufposz00_3178;
																																				BgL_forwardz00_1078
																																					=
																																					BgL_forwardz00_3176;
																																				BgL_lastzd2matchzd2_1077
																																					=
																																					BgL_lastzd2matchzd2_3175;
																																				BgL_iportz00_1076
																																					=
																																					BgL_iportz00_3174;
																																				goto
																																					BgL_zc3z04anonymousza32136ze3z87_1080;
																																			}
																																		else
																																			{	/* Cc/ld.scm 324 */
																																				bool_t
																																					BgL_test2607z00_3179;
																																				if ((
																																						(long)
																																						(BgL_curz00_1125)
																																						==
																																						10L))
																																					{	/* Cc/ld.scm 324 */
																																						BgL_test2607z00_3179
																																							=
																																							(
																																							(bool_t)
																																							1);
																																					}
																																				else
																																					{	/* Cc/ld.scm 324 */
																																						if (
																																							((long) (BgL_curz00_1125) == 45L))
																																							{	/* Cc/ld.scm 324 */
																																								BgL_test2607z00_3179
																																									=
																																									(
																																									(bool_t)
																																									1);
																																							}
																																						else
																																							{	/* Cc/ld.scm 324 */
																																								BgL_test2607z00_3179
																																									=
																																									(
																																									(long)
																																									(BgL_curz00_1125)
																																									==
																																									97L);
																																					}}
																																				if (BgL_test2607z00_3179)
																																					{	/* Cc/ld.scm 324 */
																																						BgL_matchz00_1353
																																							=
																																							BgL_lastzd2matchzd2_1117;
																																					}
																																				else
																																					{
																																						long
																																							BgL_bufposz00_3192;
																																						long
																																							BgL_forwardz00_3190;
																																						long
																																							BgL_lastzd2matchzd2_3189;
																																						obj_t
																																							BgL_iportz00_3188;
																																						BgL_iportz00_3188
																																							=
																																							BgL_iportz00_1116;
																																						BgL_lastzd2matchzd2_3189
																																							=
																																							BgL_lastzd2matchzd2_1117;
																																						BgL_forwardz00_3190
																																							=
																																							(1L
																																							+
																																							BgL_forwardz00_1118);
																																						BgL_bufposz00_3192
																																							=
																																							BgL_bufposz00_1119;
																																						BgL_bufposz00_1063
																																							=
																																							BgL_bufposz00_3192;
																																						BgL_forwardz00_1062
																																							=
																																							BgL_forwardz00_3190;
																																						BgL_lastzd2matchzd2_1061
																																							=
																																							BgL_lastzd2matchzd2_3189;
																																						BgL_iportz00_1060
																																							=
																																							BgL_iportz00_3188;
																																						goto
																																							BgL_zc3z04anonymousza32125ze3z87_1064;
																																					}
																																			}
																																	}
																															}
																														}
																												}
																											else
																												{	/* Cc/ld.scm 324 */
																													if (
																														((long)
																															(BgL_curz00_1105)
																															== 45L))
																														{
																															long
																																BgL_bufposz00_3201;
																															long
																																BgL_forwardz00_3199;
																															long
																																BgL_lastzd2matchzd2_3198;
																															obj_t
																																BgL_iportz00_3197;
																															BgL_iportz00_3197
																																=
																																BgL_iportz00_1096;
																															BgL_lastzd2matchzd2_3198
																																=
																																BgL_lastzd2matchzd2_1097;
																															BgL_forwardz00_3199
																																=
																																(1L +
																																BgL_forwardz00_1098);
																															BgL_bufposz00_3201
																																=
																																BgL_bufposz00_1099;
																															BgL_bufposz00_1079
																																=
																																BgL_bufposz00_3201;
																															BgL_forwardz00_1078
																																=
																																BgL_forwardz00_3199;
																															BgL_lastzd2matchzd2_1077
																																=
																																BgL_lastzd2matchzd2_3198;
																															BgL_iportz00_1076
																																=
																																BgL_iportz00_3197;
																															goto
																																BgL_zc3z04anonymousza32136ze3z87_1080;
																														}
																													else
																														{	/* Cc/ld.scm 324 */
																															bool_t
																																BgL_test2611z00_3202;
																															if (((long)
																																	(BgL_curz00_1105)
																																	== 10L))
																																{	/* Cc/ld.scm 324 */
																																	BgL_test2611z00_3202
																																		=
																																		((bool_t)
																																		1);
																																}
																															else
																																{	/* Cc/ld.scm 324 */
																																	if (
																																		((long)
																																			(BgL_curz00_1105)
																																			== 45L))
																																		{	/* Cc/ld.scm 324 */
																																			BgL_test2611z00_3202
																																				=
																																				(
																																				(bool_t)
																																				1);
																																		}
																																	else
																																		{	/* Cc/ld.scm 324 */
																																			BgL_test2611z00_3202
																																				=
																																				((long)
																																				(BgL_curz00_1105)
																																				==
																																				116L);
																																}}
																															if (BgL_test2611z00_3202)
																																{	/* Cc/ld.scm 324 */
																																	BgL_matchz00_1353
																																		=
																																		BgL_lastzd2matchzd2_1097;
																																}
																															else
																																{
																																	long
																																		BgL_bufposz00_3215;
																																	long
																																		BgL_forwardz00_3213;
																																	long
																																		BgL_lastzd2matchzd2_3212;
																																	obj_t
																																		BgL_iportz00_3211;
																																	BgL_iportz00_3211
																																		=
																																		BgL_iportz00_1096;
																																	BgL_lastzd2matchzd2_3212
																																		=
																																		BgL_lastzd2matchzd2_1097;
																																	BgL_forwardz00_3213
																																		=
																																		(1L +
																																		BgL_forwardz00_1098);
																																	BgL_bufposz00_3215
																																		=
																																		BgL_bufposz00_1099;
																																	BgL_bufposz00_1063
																																		=
																																		BgL_bufposz00_3215;
																																	BgL_forwardz00_1062
																																		=
																																		BgL_forwardz00_3213;
																																	BgL_lastzd2matchzd2_1061
																																		=
																																		BgL_lastzd2matchzd2_3212;
																																	BgL_iportz00_1060
																																		=
																																		BgL_iportz00_3211;
																																	goto
																																		BgL_zc3z04anonymousza32125ze3z87_1064;
																																}
																														}
																												}
																										}
																									}
																							}
																						else
																							{	/* Cc/ld.scm 324 */
																								if (
																									((long) (BgL_curz00_1049) ==
																										45L))
																									{
																										long BgL_bufposz00_3224;
																										long BgL_forwardz00_3222;
																										long
																											BgL_lastzd2matchzd2_3221;
																										obj_t BgL_iportz00_3220;

																										BgL_iportz00_3220 =
																											BgL_iportz00_1039;
																										BgL_lastzd2matchzd2_3221 =
																											BgL_newzd2matchzd2_1044;
																										BgL_forwardz00_3222 =
																											(1L +
																											BgL_forwardz00_1041);
																										BgL_bufposz00_3224 =
																											BgL_bufposz00_1042;
																										BgL_bufposz00_1079 =
																											BgL_bufposz00_3224;
																										BgL_forwardz00_1078 =
																											BgL_forwardz00_3222;
																										BgL_lastzd2matchzd2_1077 =
																											BgL_lastzd2matchzd2_3221;
																										BgL_iportz00_1076 =
																											BgL_iportz00_3220;
																										goto
																											BgL_zc3z04anonymousza32136ze3z87_1080;
																									}
																								else
																									{	/* Cc/ld.scm 324 */
																										bool_t BgL_test2615z00_3225;

																										if (
																											((long) (BgL_curz00_1049)
																												== 10L))
																											{	/* Cc/ld.scm 324 */
																												BgL_test2615z00_3225 =
																													((bool_t) 1);
																											}
																										else
																											{	/* Cc/ld.scm 324 */
																												if (
																													((long)
																														(BgL_curz00_1049) ==
																														45L))
																													{	/* Cc/ld.scm 324 */
																														BgL_test2615z00_3225
																															= ((bool_t) 1);
																													}
																												else
																													{	/* Cc/ld.scm 324 */
																														BgL_test2615z00_3225
																															=
																															((long)
																															(BgL_curz00_1049)
																															== 115L);
																											}}
																										if (BgL_test2615z00_3225)
																											{	/* Cc/ld.scm 324 */
																												BgL_matchz00_1353 =
																													BgL_newzd2matchzd2_1044;
																											}
																										else
																											{
																												long BgL_bufposz00_3238;
																												long
																													BgL_forwardz00_3236;
																												long
																													BgL_lastzd2matchzd2_3235;
																												obj_t BgL_iportz00_3234;

																												BgL_iportz00_3234 =
																													BgL_iportz00_1039;
																												BgL_lastzd2matchzd2_3235
																													=
																													BgL_newzd2matchzd2_1044;
																												BgL_forwardz00_3236 =
																													(1L +
																													BgL_forwardz00_1041);
																												BgL_bufposz00_3238 =
																													BgL_bufposz00_1042;
																												BgL_bufposz00_1063 =
																													BgL_bufposz00_3238;
																												BgL_forwardz00_1062 =
																													BgL_forwardz00_3236;
																												BgL_lastzd2matchzd2_1061
																													=
																													BgL_lastzd2matchzd2_3235;
																												BgL_iportz00_1060 =
																													BgL_iportz00_3234;
																												goto
																													BgL_zc3z04anonymousza32125ze3z87_1064;
																											}
																									}
																							}
																					}
																				}
																		}
																	}
																else
																	{	/* Cc/ld.scm 324 */
																		if (((long) (BgL_curz00_1228) == 10L))
																			{	/* Cc/ld.scm 324 */
																				long BgL_arg2235z00_1232;

																				BgL_arg2235z00_1232 =
																					(1L + BgL_forwardz00_1221);
																				{	/* Cc/ld.scm 324 */
																					long BgL_newzd2matchzd2_1895;

																					RGC_STOP_MATCH(BgL_iportz00_1219,
																						BgL_arg2235z00_1232);
																					BgL_newzd2matchzd2_1895 = 1L;
																					BgL_matchz00_1353 =
																						BgL_newzd2matchzd2_1895;
																			}}
																		else
																			{	/* Cc/ld.scm 324 */
																				BgL_iportz00_1022 = BgL_iportz00_1219;
																				BgL_lastzd2matchzd2_1023 =
																					BgL_lastzd2matchzd2_1220;
																				BgL_forwardz00_1024 =
																					(1L + BgL_forwardz00_1221);
																				BgL_bufposz00_1025 = BgL_bufposz00_1222;
																			BgL_zc3z04anonymousza32102ze3z87_1026:
																				{	/* Cc/ld.scm 324 */
																					long BgL_newzd2matchzd2_1027;

																					RGC_STOP_MATCH(BgL_iportz00_1022,
																						BgL_forwardz00_1024);
																					BgL_newzd2matchzd2_1027 = 1L;
																					if (
																						(BgL_forwardz00_1024 ==
																							BgL_bufposz00_1025))
																						{	/* Cc/ld.scm 324 */
																							if (rgc_fill_buffer
																								(BgL_iportz00_1022))
																								{	/* Cc/ld.scm 324 */
																									long BgL_arg2105z00_1030;
																									long BgL_arg2106z00_1031;

																									BgL_arg2105z00_1030 =
																										RGC_BUFFER_FORWARD
																										(BgL_iportz00_1022);
																									BgL_arg2106z00_1031 =
																										RGC_BUFFER_BUFPOS
																										(BgL_iportz00_1022);
																									{
																										long BgL_bufposz00_3253;
																										long BgL_forwardz00_3252;

																										BgL_forwardz00_3252 =
																											BgL_arg2105z00_1030;
																										BgL_bufposz00_3253 =
																											BgL_arg2106z00_1031;
																										BgL_bufposz00_1025 =
																											BgL_bufposz00_3253;
																										BgL_forwardz00_1024 =
																											BgL_forwardz00_3252;
																										goto
																											BgL_zc3z04anonymousza32102ze3z87_1026;
																									}
																								}
																							else
																								{	/* Cc/ld.scm 324 */
																									BgL_matchz00_1353 =
																										BgL_newzd2matchzd2_1027;
																								}
																						}
																					else
																						{	/* Cc/ld.scm 324 */
																							int BgL_curz00_1032;

																							BgL_curz00_1032 =
																								RGC_BUFFER_GET_CHAR
																								(BgL_iportz00_1022,
																								BgL_forwardz00_1024);
																							{	/* Cc/ld.scm 324 */

																								if (
																									((long) (BgL_curz00_1032) ==
																										45L))
																									{
																										long BgL_bufposz00_3262;
																										long BgL_forwardz00_3260;
																										long
																											BgL_lastzd2matchzd2_3259;
																										obj_t BgL_iportz00_3258;

																										BgL_iportz00_3258 =
																											BgL_iportz00_1022;
																										BgL_lastzd2matchzd2_3259 =
																											BgL_newzd2matchzd2_1027;
																										BgL_forwardz00_3260 =
																											(1L +
																											BgL_forwardz00_1024);
																										BgL_bufposz00_3262 =
																											BgL_bufposz00_1025;
																										BgL_bufposz00_1079 =
																											BgL_bufposz00_3262;
																										BgL_forwardz00_1078 =
																											BgL_forwardz00_3260;
																										BgL_lastzd2matchzd2_1077 =
																											BgL_lastzd2matchzd2_3259;
																										BgL_iportz00_1076 =
																											BgL_iportz00_3258;
																										goto
																											BgL_zc3z04anonymousza32136ze3z87_1080;
																									}
																								else
																									{	/* Cc/ld.scm 324 */
																										bool_t BgL_test2622z00_3263;

																										if (
																											((long) (BgL_curz00_1032)
																												== 10L))
																											{	/* Cc/ld.scm 324 */
																												BgL_test2622z00_3263 =
																													((bool_t) 1);
																											}
																										else
																											{	/* Cc/ld.scm 324 */
																												BgL_test2622z00_3263 =
																													(
																													(long)
																													(BgL_curz00_1032) ==
																													45L);
																											}
																										if (BgL_test2622z00_3263)
																											{	/* Cc/ld.scm 324 */
																												BgL_matchz00_1353 =
																													BgL_newzd2matchzd2_1027;
																											}
																										else
																											{
																												long BgL_bufposz00_3273;
																												long
																													BgL_forwardz00_3271;
																												long
																													BgL_lastzd2matchzd2_3270;
																												obj_t BgL_iportz00_3269;

																												BgL_iportz00_3269 =
																													BgL_iportz00_1022;
																												BgL_lastzd2matchzd2_3270
																													=
																													BgL_newzd2matchzd2_1027;
																												BgL_forwardz00_3271 =
																													(1L +
																													BgL_forwardz00_1024);
																												BgL_bufposz00_3273 =
																													BgL_bufposz00_1025;
																												BgL_bufposz00_1063 =
																													BgL_bufposz00_3273;
																												BgL_forwardz00_1062 =
																													BgL_forwardz00_3271;
																												BgL_lastzd2matchzd2_1061
																													=
																													BgL_lastzd2matchzd2_3270;
																												BgL_iportz00_1060 =
																													BgL_iportz00_3269;
																												goto
																													BgL_zc3z04anonymousza32125ze3z87_1064;
																											}
																									}
																							}
																						}
																				}
																			}
																	}
															}
														}
												}
												RGC_SET_FILEPOS(BgL_port1091z00_977);
												{

													switch (BgL_matchz00_1353)
														{
														case 1L:

															BgL_tmp1094z00_980 = BFALSE;
															break;
														case 0L:

															BgL_tmp1094z00_980 = BTRUE;
															break;
														default:
															BgL_tmp1094z00_980 =
																BGl_errorz00zz__errorz00
																(BGl_string2355z00zzcc_ldz00,
																BGl_string2356z00zzcc_ldz00,
																BINT(BgL_matchz00_1353));
														}
												}
											}
										}
										{	/* Cc/ld.scm 324 */
											bool_t BgL_test2624z00_3279;

											{	/* Cc/ld.scm 324 */
												obj_t BgL_arg1827z00_1904;

												BgL_arg1827z00_1904 =
													BGL_EXITD_PROTECT(BgL_exitd1092z00_978);
												BgL_test2624z00_3279 = PAIRP(BgL_arg1827z00_1904);
											}
											if (BgL_test2624z00_3279)
												{	/* Cc/ld.scm 324 */
													obj_t BgL_arg1825z00_1905;

													{	/* Cc/ld.scm 324 */
														obj_t BgL_arg1826z00_1906;

														BgL_arg1826z00_1906 =
															BGL_EXITD_PROTECT(BgL_exitd1092z00_978);
														BgL_arg1825z00_1905 =
															CDR(((obj_t) BgL_arg1826z00_1906));
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1092z00_978,
														BgL_arg1825z00_1905);
													BUNSPEC;
												}
											else
												{	/* Cc/ld.scm 324 */
													BFALSE;
												}
										}
										bgl_close_input_port(BgL_port1091z00_977);
										BgL_staticpz00_853 = BgL_tmp1094z00_980;
									}
								}
							}
						}
				}
				if (CBOOL(BgL_staticpz00_853))
					{	/* Cc/ld.scm 330 */
						obj_t BgL_arg2002z00_854;

						BgL_arg2002z00_854 =
							BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(17));
						BGl_za2ldzd2optionsza2zd2zzengine_paramz00 =
							string_append_3(BgL_arg2002z00_854, BGl_string2357z00zzcc_ldz00,
							BGl_za2ldzd2optionsza2zd2zzengine_paramz00);
					}
				else
					{	/* Cc/ld.scm 332 */
						obj_t BgL_arg2003z00_855;

						BgL_arg2003z00_855 =
							BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(18));
						BGl_za2ldzd2optionsza2zd2zzengine_paramz00 =
							string_append_3(BgL_arg2003z00_855, BGl_string2357z00zzcc_ldz00,
							BGl_za2ldzd2optionsza2zd2zzengine_paramz00);
					}
				if (CBOOL(BGl_za2multizd2threadedzd2gczf3za2zf3zzengine_paramz00))
					{	/* Cc/ld.scm 336 */
						BGl_libraryzd2translationzd2tablezd2addz12zc0zz__libraryz00
							(BGl_za2bigloozd2libza2zd2zzengine_paramz00,
							BGl_string2363z00zzcc_ldz00, BNIL);
					}
				else
					{	/* Cc/ld.scm 336 */
						BFALSE;
					}
				{	/* Cc/ld.scm 338 */
					obj_t BgL_destz00_857;

					if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
						{	/* Cc/ld.scm 338 */
							BgL_destz00_857 = BGl_za2destza2z00zzengine_paramz00;
						}
					else
						{	/* Cc/ld.scm 338 */
							BgL_destz00_857 = string_to_bstring(BGL_DEFAULT_A_OUT);
						}
					{	/* Cc/ld.scm 338 */
						obj_t BgL_bigloozd2libzd2_858;

						{	/* Cc/ld.scm 343 */
							obj_t BgL_arg2100z00_974;

							BgL_arg2100z00_974 = BGl_libraryzd2suffixeszd2zzcc_ldz00();
							BgL_bigloozd2libzd2_858 =
								BGl_libraryzd2ze3oszd2fileze3zzcc_ldz00
								(BGl_za2bigloozd2libza2zd2zzengine_paramz00, BgL_arg2100z00_974,
								BTRUE, ((bool_t) 0), ((bool_t) 0));
						}
						{	/* Cc/ld.scm 342 */
							obj_t BgL_addzd2libszd2_859;

							{
								obj_t BgL_libz00_962;
								obj_t BgL_resz00_963;

								BgL_libz00_962 =
									BGl_za2additionalzd2bigloozd2librariesza2z00zzengine_paramz00;
								BgL_resz00_963 = BNIL;
							BgL_zc3z04anonymousza32092ze3z87_964:
								if (NULLP(BgL_libz00_962))
									{	/* Cc/ld.scm 348 */
										BgL_addzd2libszd2_859 = BgL_resz00_963;
									}
								else
									{	/* Cc/ld.scm 350 */
										obj_t BgL_arg2094z00_966;
										obj_t BgL_arg2095z00_967;

										BgL_arg2094z00_966 = CDR(((obj_t) BgL_libz00_962));
										{	/* Cc/ld.scm 353 */
											obj_t BgL_arg2096z00_968;

											{	/* Cc/ld.scm 353 */
												obj_t BgL_arg2097z00_969;
												obj_t BgL_arg2098z00_970;
												obj_t BgL_arg2099z00_971;

												BgL_arg2097z00_969 = CAR(((obj_t) BgL_libz00_962));
												BgL_arg2098z00_970 =
													BGl_libraryzd2suffixeszd2zzcc_ldz00();
												{	/* Cc/ld.scm 355 */
													obj_t BgL__ortest_1112z00_972;

													BgL__ortest_1112z00_972 =
														BGl_za2staticzd2allzd2bigloozf3za2zf3zzengine_paramz00;
													if (CBOOL(BgL__ortest_1112z00_972))
														{	/* Cc/ld.scm 355 */
															BgL_arg2099z00_971 = BgL__ortest_1112z00_972;
														}
													else
														{	/* Cc/ld.scm 355 */
															BgL_arg2099z00_971 = BgL_staticpz00_853;
														}
												}
												BgL_arg2096z00_968 =
													BGl_libraryzd2ze3oszd2fileze3zzcc_ldz00
													(BgL_arg2097z00_969, BgL_arg2098z00_970,
													BgL_arg2099z00_971, ((bool_t) 0), ((bool_t) 0));
											}
											BgL_arg2095z00_967 =
												MAKE_YOUNG_PAIR(BgL_arg2096z00_968, BgL_resz00_963);
										}
										{
											obj_t BgL_resz00_3315;
											obj_t BgL_libz00_3314;

											BgL_libz00_3314 = BgL_arg2094z00_966;
											BgL_resz00_3315 = BgL_arg2095z00_967;
											BgL_resz00_963 = BgL_resz00_3315;
											BgL_libz00_962 = BgL_libz00_3314;
											goto BgL_zc3z04anonymousza32092ze3z87_964;
										}
									}
							}
							{	/* Cc/ld.scm 346 */
								obj_t BgL_otherzd2libszd2_860;

								BgL_otherzd2libszd2_860 =
									BGl_za2bigloozd2userzd2libza2z00zzengine_paramz00;
								{	/* Cc/ld.scm 358 */
									obj_t BgL_ldzd2argszd2_861;

									{	/* Cc/ld.scm 361 */
										obj_t BgL_arg2019z00_883;
										obj_t BgL_arg2020z00_884;
										obj_t BgL_arg2021z00_885;
										obj_t BgL_arg2022z00_886;
										obj_t BgL_arg2024z00_887;
										obj_t BgL_arg2025z00_888;
										obj_t BgL_arg2026z00_889;
										obj_t BgL_arg2027z00_890;
										obj_t BgL_arg2029z00_891;
										obj_t BgL_arg2030z00_892;
										obj_t BgL_arg2031z00_893;
										obj_t BgL_arg2033z00_894;
										obj_t BgL_arg2034z00_895;

										{	/* Cc/ld.scm 361 */
											obj_t BgL_arg2055z00_913;

											BgL_arg2055z00_913 =
												string_append_3(BgL_namez00_13,
												BGl_string2358z00zzcc_ldz00,
												BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00);
											{	/* Cc/ld.scm 361 */
												obj_t BgL_list2056z00_914;

												BgL_list2056z00_914 =
													MAKE_YOUNG_PAIR(BgL_arg2055z00_913, BNIL);
												BgL_arg2019z00_883 = BgL_list2056z00_914;
											}
										}
										{	/* Cc/ld.scm 369 */
											obj_t BgL_arg2057z00_915;

											BgL_arg2057z00_915 =
												string_append
												(BGl_za2ldzd2ozd2optionza2z00zzengine_paramz00,
												BgL_destz00_857);
											{	/* Cc/ld.scm 369 */
												obj_t BgL_list2058z00_916;

												BgL_list2058z00_916 =
													MAKE_YOUNG_PAIR(BgL_arg2057z00_915, BNIL);
												BgL_arg2020z00_884 = BgL_list2058z00_916;
											}
										}
										BgL_arg2021z00_885 =
											BGl_zc3z04anonymousza32059ze3ze70z60zzcc_ldz00
											(BGl_za2cczd2optionsza2zd2zzengine_paramz00);
										{	/* Cc/ld.scm 375 */
											obj_t BgL_list2064z00_926;

											BgL_list2064z00_926 =
												MAKE_YOUNG_PAIR(BGl_string2364z00zzcc_ldz00, BNIL);
											BgL_arg2022z00_886 = BgL_list2064z00_926;
										}
										{	/* Cc/ld.scm 377 */
											bool_t BgL_test2630z00_3322;

											if (CBOOL(BGl_za2czd2debugza2zd2zzengine_paramz00))
												{	/* Cc/ld.scm 377 */
													BgL_test2630z00_3322 = ((bool_t) 1);
												}
											else
												{	/* Cc/ld.scm 377 */
													BgL_test2630z00_3322 =
														(
														(long)
														CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) >
														0L);
												}
											if (BgL_test2630z00_3322)
												{	/* Cc/ld.scm 377 */
													BgL_arg2024z00_887 =
														BGl_stringzd2splitzd2charz00zztools_miscz00
														(BGl_za2ldzd2debugzd2optionza2z00zzengine_paramz00,
														BCHAR(((unsigned char) ' ')));
												}
											else
												{	/* Cc/ld.scm 377 */
													BgL_arg2024z00_887 = BNIL;
												}
										}
										if (CBOOL(BGl_za2czd2debugza2zd2zzengine_paramz00))
											{	/* Cc/ld.scm 381 */
												BgL_arg2025z00_888 = BNIL;
											}
										else
											{	/* Cc/ld.scm 381 */
												BgL_arg2025z00_888 =
													BGl_stringzd2splitzd2charz00zztools_miscz00
													(BGl_za2ldzd2optimzd2flagsza2z00zzengine_paramz00,
													BCHAR(((unsigned char) ' ')));
											}
										{	/* Cc/ld.scm 385 */
											bool_t BgL_test2633z00_3333;

											if (CBOOL(BGl_za2stripza2z00zzengine_paramz00))
												{	/* Cc/ld.scm 386 */
													bool_t BgL_test2635z00_3336;

													{	/* Cc/ld.scm 386 */
														obj_t BgL_arg2074z00_935;

														BgL_arg2074z00_935 =
															BGl_bigloozd2configzd2zz__configurez00
															(CNST_TABLE_REF(21));
														{	/* Cc/ld.scm 386 */
															long BgL_l1z00_1920;

															BgL_l1z00_1920 =
																STRING_LENGTH(((obj_t) BgL_arg2074z00_935));
															if ((BgL_l1z00_1920 == 0L))
																{	/* Cc/ld.scm 386 */
																	int BgL_arg1282z00_1923;

																	{	/* Cc/ld.scm 386 */
																		char *BgL_auxz00_3346;
																		char *BgL_tmpz00_3343;

																		BgL_auxz00_3346 =
																			BSTRING_TO_STRING
																			(BGl_string2347z00zzcc_ldz00);
																		BgL_tmpz00_3343 =
																			BSTRING_TO_STRING(((obj_t)
																				BgL_arg2074z00_935));
																		BgL_arg1282z00_1923 =
																			memcmp(BgL_tmpz00_3343, BgL_auxz00_3346,
																			BgL_l1z00_1920);
																	}
																	BgL_test2635z00_3336 =
																		((long) (BgL_arg1282z00_1923) == 0L);
																}
															else
																{	/* Cc/ld.scm 386 */
																	BgL_test2635z00_3336 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test2635z00_3336)
														{	/* Cc/ld.scm 386 */
															BgL_test2633z00_3333 = ((bool_t) 0);
														}
													else
														{	/* Cc/ld.scm 386 */
															BgL_test2633z00_3333 = ((bool_t) 1);
														}
												}
											else
												{	/* Cc/ld.scm 385 */
													BgL_test2633z00_3333 = ((bool_t) 0);
												}
											if (BgL_test2633z00_3333)
												{	/* Cc/ld.scm 387 */
													obj_t BgL_arg2072z00_932;

													BgL_arg2072z00_932 =
														BGl_bigloozd2configzd2zz__configurez00
														(CNST_TABLE_REF(21));
													{	/* Cc/ld.scm 387 */
														obj_t BgL_list2073z00_933;

														BgL_list2073z00_933 =
															MAKE_YOUNG_PAIR(BgL_arg2072z00_932, BNIL);
														BgL_arg2026z00_889 = BgL_list2073z00_933;
													}
												}
											else
												{	/* Cc/ld.scm 385 */
													BgL_arg2026z00_889 = BNIL;
												}
										}
										BgL_arg2027z00_890 =
											BGl_stringzd2splitzd2charz00zztools_miscz00
											(BGl_za2ldzd2optionsza2zd2zzengine_paramz00,
											BCHAR(((unsigned char) ' ')));
										BgL_arg2029z00_891 =
											BGl_loopze70ze7zzcc_ldz00
											(BGl_za2libzd2dirza2zd2zzengine_paramz00);
										{	/* Cc/ld.scm 400 */
											obj_t BgL_list2081z00_945;

											BgL_list2081z00_945 =
												MAKE_YOUNG_PAIR(BgL_bigloozd2libzd2_858, BNIL);
											BgL_arg2030z00_892 = BgL_list2081z00_945;
										}
										{	/* Cc/ld.scm 402 */
											bool_t BgL_test2637z00_3358;

											{	/* Cc/ld.scm 402 */
												obj_t BgL_arg2086z00_950;

												BgL_arg2086z00_950 =
													BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
													(24));
												{	/* Cc/ld.scm 402 */
													long BgL_l1z00_1935;

													BgL_l1z00_1935 =
														STRING_LENGTH(((obj_t) BgL_arg2086z00_950));
													if ((BgL_l1z00_1935 == 0L))
														{	/* Cc/ld.scm 402 */
															int BgL_arg1282z00_1938;

															{	/* Cc/ld.scm 402 */
																char *BgL_auxz00_3368;
																char *BgL_tmpz00_3365;

																BgL_auxz00_3368 =
																	BSTRING_TO_STRING
																	(BGl_string2347z00zzcc_ldz00);
																BgL_tmpz00_3365 =
																	BSTRING_TO_STRING(((obj_t)
																		BgL_arg2086z00_950));
																BgL_arg1282z00_1938 =
																	memcmp(BgL_tmpz00_3365, BgL_auxz00_3368,
																	BgL_l1z00_1935);
															}
															BgL_test2637z00_3358 =
																((long) (BgL_arg1282z00_1938) == 0L);
														}
													else
														{	/* Cc/ld.scm 402 */
															BgL_test2637z00_3358 = ((bool_t) 0);
														}
												}
											}
											if (BgL_test2637z00_3358)
												{	/* Cc/ld.scm 402 */
													BgL_arg2031z00_893 = BNIL;
												}
											else
												{	/* Cc/ld.scm 404 */
													obj_t BgL_arg2084z00_948;

													BgL_arg2084z00_948 =
														BGl_bigloozd2configzd2zz__configurez00
														(CNST_TABLE_REF(24));
													{	/* Cc/ld.scm 404 */
														obj_t BgL_list2085z00_949;

														BgL_list2085z00_949 =
															MAKE_YOUNG_PAIR(BgL_arg2084z00_948, BNIL);
														BgL_arg2031z00_893 = BgL_list2085z00_949;
													}
												}
										}
										if (CBOOL
											(BGl_za2doublezd2ldzd2libszf3za2zf3zzengine_paramz00))
											{	/* Cc/ld.scm 408 */
												BgL_arg2033z00_894 = BgL_addzd2libszd2_859;
											}
										else
											{	/* Cc/ld.scm 408 */
												BgL_arg2033z00_894 = BNIL;
											}
										BgL_arg2034z00_895 =
											BGl_zc3z04anonymousza32087ze3ze70z60zzcc_ldz00
											(BGl_za2ldzd2postzd2optionsza2z00zzengine_paramz00);
										{	/* Cc/ld.scm 359 */
											obj_t BgL_list2035z00_896;

											{	/* Cc/ld.scm 359 */
												obj_t BgL_arg2036z00_897;

												{	/* Cc/ld.scm 359 */
													obj_t BgL_arg2037z00_898;

													{	/* Cc/ld.scm 359 */
														obj_t BgL_arg2038z00_899;

														{	/* Cc/ld.scm 359 */
															obj_t BgL_arg2039z00_900;

															{	/* Cc/ld.scm 359 */
																obj_t BgL_arg2040z00_901;

																{	/* Cc/ld.scm 359 */
																	obj_t BgL_arg2041z00_902;

																	{	/* Cc/ld.scm 359 */
																		obj_t BgL_arg2042z00_903;

																		{	/* Cc/ld.scm 359 */
																			obj_t BgL_arg2044z00_904;

																			{	/* Cc/ld.scm 359 */
																				obj_t BgL_arg2045z00_905;

																				{	/* Cc/ld.scm 359 */
																					obj_t BgL_arg2046z00_906;

																					{	/* Cc/ld.scm 359 */
																						obj_t BgL_arg2047z00_907;

																						{	/* Cc/ld.scm 359 */
																							obj_t BgL_arg2048z00_908;

																							{	/* Cc/ld.scm 359 */
																								obj_t BgL_arg2049z00_909;

																								{	/* Cc/ld.scm 359 */
																									obj_t BgL_arg2050z00_910;

																									{	/* Cc/ld.scm 359 */
																										obj_t BgL_arg2051z00_911;

																										{	/* Cc/ld.scm 359 */
																											obj_t BgL_arg2052z00_912;

																											BgL_arg2052z00_912 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg2034z00_895,
																												BNIL);
																											BgL_arg2051z00_911 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg2033z00_894,
																												BgL_arg2052z00_912);
																										}
																										BgL_arg2050z00_910 =
																											MAKE_YOUNG_PAIR
																											(BgL_otherzd2libszd2_860,
																											BgL_arg2051z00_911);
																									}
																									BgL_arg2049z00_909 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2031z00_893,
																										BgL_arg2050z00_910);
																								}
																								BgL_arg2048z00_908 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg2030z00_892,
																									BgL_arg2049z00_909);
																							}
																							BgL_arg2047z00_907 =
																								MAKE_YOUNG_PAIR
																								(BgL_addzd2libszd2_859,
																								BgL_arg2048z00_908);
																						}
																						BgL_arg2046z00_906 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2029z00_891,
																							BgL_arg2047z00_907);
																					}
																					BgL_arg2045z00_905 =
																						MAKE_YOUNG_PAIR(BgL_arg2027z00_890,
																						BgL_arg2046z00_906);
																				}
																				BgL_arg2044z00_904 =
																					MAKE_YOUNG_PAIR(BgL_arg2026z00_889,
																					BgL_arg2045z00_905);
																			}
																			BgL_arg2042z00_903 =
																				MAKE_YOUNG_PAIR(BgL_arg2025z00_888,
																				BgL_arg2044z00_904);
																		}
																		BgL_arg2041z00_902 =
																			MAKE_YOUNG_PAIR(BgL_arg2024z00_887,
																			BgL_arg2042z00_903);
																	}
																	BgL_arg2040z00_901 =
																		MAKE_YOUNG_PAIR(BgL_arg2022z00_886,
																		BgL_arg2041z00_902);
																}
																BgL_arg2039z00_900 =
																	MAKE_YOUNG_PAIR(BgL_arg2021z00_885,
																	BgL_arg2040z00_901);
															}
															BgL_arg2038z00_899 =
																MAKE_YOUNG_PAIR(BgL_arg2020z00_884,
																BgL_arg2039z00_900);
														}
														BgL_arg2037z00_898 =
															MAKE_YOUNG_PAIR
															(BGl_za2ozd2filesza2zd2zzengine_paramz00,
															BgL_arg2038z00_899);
													}
													BgL_arg2036z00_897 =
														MAKE_YOUNG_PAIR
														(BGl_za2withzd2filesza2zd2zzengine_paramz00,
														BgL_arg2037z00_898);
												}
												BgL_list2035z00_896 =
													MAKE_YOUNG_PAIR(BgL_arg2019z00_883,
													BgL_arg2036z00_897);
											}
											BgL_ldzd2argszd2_861 =
												BGl_appendz00zz__r4_pairs_and_lists_6_3z00
												(BgL_list2035z00_896);
										}
									}
									{	/* Cc/ld.scm 359 */

										{	/* Cc/ld.scm 413 */
											obj_t BgL_arg2006z00_862;

											{	/* Cc/ld.scm 413 */
												obj_t BgL_l1142z00_866;

												BgL_l1142z00_866 =
													MAKE_YOUNG_PAIR(BGl_za2ccza2z00zzengine_paramz00,
													BgL_ldzd2argszd2_861);
												{	/* Cc/ld.scm 413 */
													obj_t BgL_head1144z00_868;

													BgL_head1144z00_868 = MAKE_YOUNG_PAIR(BNIL, BNIL);
													{
														obj_t BgL_l1142z00_870;
														obj_t BgL_tail1145z00_871;

														BgL_l1142z00_870 = BgL_l1142z00_866;
														BgL_tail1145z00_871 = BgL_head1144z00_868;
													BgL_zc3z04anonymousza32011ze3z87_872:
														if (NULLP(BgL_l1142z00_870))
															{	/* Cc/ld.scm 413 */
																BgL_arg2006z00_862 = CDR(BgL_head1144z00_868);
															}
														else
															{	/* Cc/ld.scm 413 */
																obj_t BgL_newtail1146z00_874;

																{	/* Cc/ld.scm 413 */
																	obj_t BgL_arg2014z00_876;

																	{	/* Cc/ld.scm 413 */
																		obj_t BgL_strz00_877;

																		BgL_strz00_877 =
																			CAR(((obj_t) BgL_l1142z00_870));
																		BgL_arg2014z00_876 =
																			string_append_3
																			(BGl_string2365z00zzcc_ldz00,
																			BgL_strz00_877,
																			BGl_string2366z00zzcc_ldz00);
																	}
																	BgL_newtail1146z00_874 =
																		MAKE_YOUNG_PAIR(BgL_arg2014z00_876, BNIL);
																}
																SET_CDR(BgL_tail1145z00_871,
																	BgL_newtail1146z00_874);
																{	/* Cc/ld.scm 413 */
																	obj_t BgL_arg2013z00_875;

																	BgL_arg2013z00_875 =
																		CDR(((obj_t) BgL_l1142z00_870));
																	{
																		obj_t BgL_tail1145z00_3410;
																		obj_t BgL_l1142z00_3409;

																		BgL_l1142z00_3409 = BgL_arg2013z00_875;
																		BgL_tail1145z00_3410 =
																			BgL_newtail1146z00_874;
																		BgL_tail1145z00_871 = BgL_tail1145z00_3410;
																		BgL_l1142z00_870 = BgL_l1142z00_3409;
																		goto BgL_zc3z04anonymousza32011ze3z87_872;
																	}
																}
															}
													}
												}
											}
											{	/* Cc/ld.scm 413 */
												obj_t BgL_list2007z00_863;

												{	/* Cc/ld.scm 413 */
													obj_t BgL_arg2008z00_864;

													{	/* Cc/ld.scm 413 */
														obj_t BgL_arg2009z00_865;

														BgL_arg2009z00_865 =
															MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)),
															BNIL);
														BgL_arg2008z00_864 =
															MAKE_YOUNG_PAIR(BgL_arg2006z00_862,
															BgL_arg2009z00_865);
													}
													BgL_list2007z00_863 =
														MAKE_YOUNG_PAIR(BGl_string2367z00zzcc_ldz00,
														BgL_arg2008z00_864);
												}
												BGl_verbosez00zztools_speekz00(BINT(2L),
													BgL_list2007z00_863);
										}}
										{	/* Cc/ld.scm 417 */
											obj_t BgL_runner2018z00_882;

											{	/* Cc/ld.scm 417 */
												obj_t BgL_arg2015z00_879;

												BgL_arg2015z00_879 =
													BGl_appendzd221011zd2zzcc_ldz00(BgL_ldzd2argszd2_861,
													CNST_TABLE_REF(26));
												{	/* Cc/ld.scm 417 */
													obj_t BgL_list2016z00_880;

													BgL_list2016z00_880 =
														MAKE_YOUNG_PAIR(BgL_arg2015z00_879, BNIL);
													BgL_runner2018z00_882 =
														BGl_consza2za2zz__r4_pairs_and_lists_6_3z00
														(BGl_za2ccza2z00zzengine_paramz00,
														BgL_list2016z00_880);
											}}
											{	/* Cc/ld.scm 417 */
												obj_t BgL_aux2017z00_881;

												BgL_aux2017z00_881 = CAR(BgL_runner2018z00_882);
												BgL_runner2018z00_882 = CDR(BgL_runner2018z00_882);
												return
													BGl_runzd2processzd2zz__processz00(BgL_aux2017z00_881,
													BgL_runner2018z00_882);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* <@anonymous:2059>~0 */
	obj_t BGl_zc3z04anonymousza32059ze3ze70z60zzcc_ldz00(obj_t BgL_l1139z00_918)
	{
		{	/* Cc/ld.scm 371 */
			if (NULLP(BgL_l1139z00_918))
				{	/* Cc/ld.scm 371 */
					return BNIL;
				}
			else
				{	/* Cc/ld.scm 372 */
					obj_t BgL_arg2061z00_921;
					obj_t BgL_arg2062z00_922;

					{	/* Cc/ld.scm 372 */
						obj_t BgL_oz00_923;

						BgL_oz00_923 = CAR(((obj_t) BgL_l1139z00_918));
						BgL_arg2061z00_921 =
							BGl_stringzd2splitzd2charz00zztools_miscz00(BgL_oz00_923,
							BCHAR(((unsigned char) ' ')));
					}
					{	/* Cc/ld.scm 371 */
						obj_t BgL_arg2063z00_924;

						BgL_arg2063z00_924 = CDR(((obj_t) BgL_l1139z00_918));
						BgL_arg2062z00_922 =
							BGl_zc3z04anonymousza32059ze3ze70z60zzcc_ldz00
							(BgL_arg2063z00_924);
					}
					return bgl_append2(BgL_arg2061z00_921, BgL_arg2062z00_922);
				}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzcc_ldz00(obj_t BgL_pathz00_937)
	{
		{	/* Cc/ld.scm 392 */
			if (NULLP(BgL_pathz00_937))
				{	/* Cc/ld.scm 393 */
					return BNIL;
				}
			else
				{	/* Cc/ld.scm 395 */
					obj_t BgL_arg2077z00_940;
					obj_t BgL_arg2078z00_941;

					{	/* Cc/ld.scm 395 */
						obj_t BgL_arg2079z00_942;

						BgL_arg2079z00_942 = CAR(((obj_t) BgL_pathz00_937));
						BgL_arg2077z00_940 =
							string_append(BGl_string2368z00zzcc_ldz00, BgL_arg2079z00_942);
					}
					{	/* Cc/ld.scm 396 */
						obj_t BgL_arg2080z00_943;

						BgL_arg2080z00_943 = CDR(((obj_t) BgL_pathz00_937));
						BgL_arg2078z00_941 = BGl_loopze70ze7zzcc_ldz00(BgL_arg2080z00_943);
					}
					return MAKE_YOUNG_PAIR(BgL_arg2077z00_940, BgL_arg2078z00_941);
				}
		}

	}



/* <@anonymous:2087>~0 */
	obj_t BGl_zc3z04anonymousza32087ze3ze70z60zzcc_ldz00(obj_t BgL_l1141z00_952)
	{
		{	/* Cc/ld.scm 410 */
			if (NULLP(BgL_l1141z00_952))
				{	/* Cc/ld.scm 410 */
					return BNIL;
				}
			else
				{	/* Cc/ld.scm 411 */
					obj_t BgL_arg2089z00_955;
					obj_t BgL_arg2090z00_956;

					{	/* Cc/ld.scm 411 */
						obj_t BgL_oz00_957;

						BgL_oz00_957 = CAR(((obj_t) BgL_l1141z00_952));
						BgL_arg2089z00_955 =
							BGl_stringzd2splitzd2charz00zztools_miscz00(BgL_oz00_957,
							BCHAR(((unsigned char) ' ')));
					}
					{	/* Cc/ld.scm 410 */
						obj_t BgL_arg2091z00_958;

						BgL_arg2091z00_958 = CDR(((obj_t) BgL_l1141z00_952));
						BgL_arg2090z00_956 =
							BGl_zc3z04anonymousza32087ze3ze70z60zzcc_ldz00
							(BgL_arg2091z00_958);
					}
					return bgl_append2(BgL_arg2089z00_955, BgL_arg2090z00_956);
				}
		}

	}



/* &<@anonymous:2327> */
	obj_t BGl_z62zc3z04anonymousza32327ze3ze5zzcc_ldz00(obj_t BgL_envz00_1964)
	{
		{	/* Cc/ld.scm 324 */
			{	/* Cc/ld.scm 324 */
				obj_t BgL_port1091z00_1965;

				BgL_port1091z00_1965 =
					((obj_t) PROCEDURE_REF(BgL_envz00_1964, (int) (0L)));
				return bgl_close_input_port(BgL_port1091z00_1965);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcc_ldz00(void)
	{
		{	/* Cc/ld.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcc_ldz00(void)
	{
		{	/* Cc/ld.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcc_ldz00(void)
	{
		{	/* Cc/ld.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcc_ldz00(void)
	{
		{	/* Cc/ld.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2369z00zzcc_ldz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2369z00zzcc_ldz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string2369z00zzcc_ldz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2369z00zzcc_ldz00));
			BGl_modulezd2initializa7ationz75zzcc_execz00(367900553L,
				BSTRING_TO_STRING(BGl_string2369z00zzcc_ldz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2369z00zzcc_ldz00));
			BGl_modulezd2initializa7ationz75zzengine_configurez00(272817175L,
				BSTRING_TO_STRING(BGl_string2369z00zzcc_ldz00));
			return
				BGl_modulezd2initializa7ationz75zzmodule_evalz00(428236825L,
				BSTRING_TO_STRING(BGl_string2369z00zzcc_ldz00));
		}

	}

#ifdef __cplusplus
}
#endif
