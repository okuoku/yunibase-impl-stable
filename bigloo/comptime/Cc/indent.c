/*===========================================================================*/
/*   (Cc/indent.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cc/indent.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CC_INDENT_TYPE_DEFINITIONS
#define BGL_CC_INDENT_TYPE_DEFINITIONS
#endif													// BGL_CC_INDENT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzcc_indentz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_indentz00zzcc_indentz00(obj_t);
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	BGL_IMPORT obj_t BGl_systemz00zz__osz00(obj_t);
	static obj_t BGl_genericzd2initzd2zzcc_indentz00(void);
	static obj_t BGl_objectzd2initzd2zzcc_indentz00(void);
	BGL_IMPORT obj_t BGl_basenamez00zz__osz00(obj_t);
	static obj_t BGl_methodzd2initzd2zzcc_indentz00(void);
	static obj_t BGl_z62indentz62zzcc_indentz00(obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcc_indentz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT bool_t fexists(char *);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcc_indentz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcc_indentz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzcc_indentz00(void);
	extern obj_t BGl_za2indentza2z00zzengine_paramz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_indentzd2envzd2zzcc_indentz00,
		BgL_bgl_za762indentza762za7za7cc1061z00, BGl_z62indentz62zzcc_indentz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1050z00zzcc_indentz00,
		BgL_bgl_string1050za700za7za7c1062za7, ")", 1);
	      DEFINE_STRING(BGl_string1051z00zzcc_indentz00,
		BgL_bgl_string1051za700za7za7c1063za7, "   . indent (", 13);
	      DEFINE_STRING(BGl_string1052z00zzcc_indentz00,
		BgL_bgl_string1052za700za7za7c1064za7, ".cc ", 4);
	      DEFINE_STRING(BGl_string1053z00zzcc_indentz00,
		BgL_bgl_string1053za700za7za7c1065za7, ".c > ", 5);
	      DEFINE_STRING(BGl_string1054z00zzcc_indentz00,
		BgL_bgl_string1054za700za7za7c1066za7, " ", 1);
	      DEFINE_STRING(BGl_string1055z00zzcc_indentz00,
		BgL_bgl_string1055za700za7za7c1067za7, "      [", 7);
	      DEFINE_STRING(BGl_string1056z00zzcc_indentz00,
		BgL_bgl_string1056za700za7za7c1068za7, "Non nul value returned -- ", 26);
	      DEFINE_STRING(BGl_string1057z00zzcc_indentz00,
		BgL_bgl_string1057za700za7za7c1069za7, ".cc", 3);
	      DEFINE_STRING(BGl_string1058z00zzcc_indentz00,
		BgL_bgl_string1058za700za7za7c1070za7, ".c", 2);
	      DEFINE_STRING(BGl_string1059z00zzcc_indentz00,
		BgL_bgl_string1059za700za7za7c1071za7, "        mv ", 11);
	      DEFINE_STRING(BGl_string1060z00zzcc_indentz00,
		BgL_bgl_string1060za700za7za7c1072za7, "cc_indent", 9);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcc_indentz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcc_indentz00(long
		BgL_checksumz00_63, char *BgL_fromz00_64)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcc_indentz00))
				{
					BGl_requirezd2initializa7ationz75zzcc_indentz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcc_indentz00();
					BGl_libraryzd2moduleszd2initz00zzcc_indentz00();
					BGl_importedzd2moduleszd2initz00zzcc_indentz00();
					return BGl_methodzd2initzd2zzcc_indentz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcc_indentz00(void)
	{
		{	/* Cc/indent.scm 15 */
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cc_indent");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "cc_indent");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cc_indent");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cc_indent");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cc_indent");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cc_indent");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cc_indent");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcc_indentz00(void)
	{
		{	/* Cc/indent.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* indent */
	BGL_EXPORTED_DEF obj_t BGl_indentz00zzcc_indentz00(obj_t BgL_namez00_3)
	{
		{	/* Cc/indent.scm 23 */
			{	/* Cc/indent.scm 24 */
				bool_t BgL_test1074z00_80;

				if (STRINGP(BgL_namez00_3))
					{	/* Cc/indent.scm 24 */
						if (STRINGP(BGl_za2indentza2z00zzengine_paramz00))
							{	/* Cc/indent.scm 25 */
								BgL_test1074z00_80 =
									(STRING_LENGTH(BGl_za2indentza2z00zzengine_paramz00) > 0L);
							}
						else
							{	/* Cc/indent.scm 25 */
								BgL_test1074z00_80 = ((bool_t) 0);
							}
					}
				else
					{	/* Cc/indent.scm 24 */
						BgL_test1074z00_80 = ((bool_t) 0);
					}
				if (BgL_test1074z00_80)
					{	/* Cc/indent.scm 24 */
						{	/* Cc/indent.scm 28 */
							obj_t BgL_arg1019z00_16;

							BgL_arg1019z00_16 =
								BGl_basenamez00zz__osz00(BGl_za2indentza2z00zzengine_paramz00);
							{	/* Cc/indent.scm 28 */
								obj_t BgL_list1020z00_17;

								{	/* Cc/indent.scm 28 */
									obj_t BgL_arg1021z00_18;

									{	/* Cc/indent.scm 28 */
										obj_t BgL_arg1022z00_19;

										{	/* Cc/indent.scm 28 */
											obj_t BgL_arg1023z00_20;

											BgL_arg1023z00_20 =
												MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
											BgL_arg1022z00_19 =
												MAKE_YOUNG_PAIR(BGl_string1050z00zzcc_indentz00,
												BgL_arg1023z00_20);
										}
										BgL_arg1021z00_18 =
											MAKE_YOUNG_PAIR(BgL_arg1019z00_16, BgL_arg1022z00_19);
									}
									BgL_list1020z00_17 =
										MAKE_YOUNG_PAIR(BGl_string1051z00zzcc_indentz00,
										BgL_arg1021z00_18);
								}
								BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1020z00_17);
						}}
						{	/* Cc/indent.scm 29 */
							obj_t BgL_cmdz00_21;

							{	/* Cc/indent.scm 29 */
								obj_t BgL_list1043z00_42;

								{	/* Cc/indent.scm 29 */
									obj_t BgL_arg1044z00_43;

									{	/* Cc/indent.scm 29 */
										obj_t BgL_arg1045z00_44;

										{	/* Cc/indent.scm 29 */
											obj_t BgL_arg1046z00_45;

											{	/* Cc/indent.scm 29 */
												obj_t BgL_arg1047z00_46;

												{	/* Cc/indent.scm 29 */
													obj_t BgL_arg1048z00_47;

													BgL_arg1048z00_47 =
														MAKE_YOUNG_PAIR(BGl_string1052z00zzcc_indentz00,
														BNIL);
													BgL_arg1047z00_46 =
														MAKE_YOUNG_PAIR(BgL_namez00_3, BgL_arg1048z00_47);
												}
												BgL_arg1046z00_45 =
													MAKE_YOUNG_PAIR(BGl_string1053z00zzcc_indentz00,
													BgL_arg1047z00_46);
											}
											BgL_arg1045z00_44 =
												MAKE_YOUNG_PAIR(BgL_namez00_3, BgL_arg1046z00_45);
										}
										BgL_arg1044z00_43 =
											MAKE_YOUNG_PAIR(BGl_string1054z00zzcc_indentz00,
											BgL_arg1045z00_44);
									}
									BgL_list1043z00_42 =
										MAKE_YOUNG_PAIR(BGl_za2indentza2z00zzengine_paramz00,
										BgL_arg1044z00_43);
								}
								BgL_cmdz00_21 =
									BGl_stringzd2appendzd2zz__r4_strings_6_7z00
									(BgL_list1043z00_42);
							}
							{	/* Cc/indent.scm 32 */
								obj_t BgL_list1024z00_22;

								{	/* Cc/indent.scm 32 */
									obj_t BgL_arg1025z00_23;

									{	/* Cc/indent.scm 32 */
										obj_t BgL_arg1026z00_24;

										{	/* Cc/indent.scm 32 */
											obj_t BgL_arg1027z00_25;

											BgL_arg1027z00_25 =
												MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
											BgL_arg1026z00_24 =
												MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ']')),
												BgL_arg1027z00_25);
										}
										BgL_arg1025z00_23 =
											MAKE_YOUNG_PAIR(BgL_cmdz00_21, BgL_arg1026z00_24);
									}
									BgL_list1024z00_22 =
										MAKE_YOUNG_PAIR(BGl_string1055z00zzcc_indentz00,
										BgL_arg1025z00_23);
								}
								BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1024z00_22);
							}
							{	/* Cc/indent.scm 33 */
								obj_t BgL_resz00_26;

								{	/* Cc/indent.scm 33 */
									obj_t BgL_list1034z00_31;

									BgL_list1034z00_31 = MAKE_YOUNG_PAIR(BgL_cmdz00_21, BNIL);
									BgL_resz00_26 = BGl_systemz00zz__osz00(BgL_list1034z00_31);
								}
								if (((long) CINT(BgL_resz00_26) == 0L))
									{	/* Cc/indent.scm 34 */
										BFALSE;
									}
								else
									{	/* Cc/indent.scm 35 */
										obj_t BgL_list1029z00_28;

										{	/* Cc/indent.scm 35 */
											obj_t BgL_arg1030z00_29;

											{	/* Cc/indent.scm 35 */
												obj_t BgL_arg1033z00_30;

												BgL_arg1033z00_30 =
													MAKE_YOUNG_PAIR(BgL_resz00_26, BNIL);
												BgL_arg1030z00_29 =
													MAKE_YOUNG_PAIR(BGl_string1056z00zzcc_indentz00,
													BgL_arg1033z00_30);
											}
											BgL_list1029z00_28 =
												MAKE_YOUNG_PAIR(BGl_za2indentza2z00zzengine_paramz00,
												BgL_arg1030z00_29);
										}
										BGl_warningz00zz__errorz00(BgL_list1029z00_28);
									}
							}
							{	/* Cc/indent.scm 36 */
								bool_t BgL_test1078z00_119;

								{	/* Cc/indent.scm 36 */
									obj_t BgL_arg1042z00_41;

									BgL_arg1042z00_41 =
										string_append(BgL_namez00_3,
										BGl_string1057z00zzcc_indentz00);
									BgL_test1078z00_119 =
										fexists(BSTRING_TO_STRING(BgL_arg1042z00_41));
								}
								if (BgL_test1078z00_119)
									{	/* Cc/indent.scm 37 */
										obj_t BgL_snamez00_34;
										obj_t BgL_dnamez00_35;

										BgL_snamez00_34 =
											string_append(BgL_namez00_3,
											BGl_string1057z00zzcc_indentz00);
										BgL_dnamez00_35 =
											string_append(BgL_namez00_3,
											BGl_string1058z00zzcc_indentz00);
										{	/* Cc/indent.scm 39 */
											obj_t BgL_list1037z00_36;

											{	/* Cc/indent.scm 39 */
												obj_t BgL_arg1038z00_37;

												{	/* Cc/indent.scm 39 */
													obj_t BgL_arg1039z00_38;

													{	/* Cc/indent.scm 39 */
														obj_t BgL_arg1040z00_39;

														{	/* Cc/indent.scm 39 */
															obj_t BgL_arg1041z00_40;

															BgL_arg1041z00_40 =
																MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)),
																BNIL);
															BgL_arg1040z00_39 =
																MAKE_YOUNG_PAIR(BgL_dnamez00_35,
																BgL_arg1041z00_40);
														}
														BgL_arg1039z00_38 =
															MAKE_YOUNG_PAIR(BGl_string1054z00zzcc_indentz00,
															BgL_arg1040z00_39);
													}
													BgL_arg1038z00_37 =
														MAKE_YOUNG_PAIR(BgL_snamez00_34, BgL_arg1039z00_38);
												}
												BgL_list1037z00_36 =
													MAKE_YOUNG_PAIR(BGl_string1059z00zzcc_indentz00,
													BgL_arg1038z00_37);
											}
											BGl_verbosez00zztools_speekz00(BINT(2L),
												BgL_list1037z00_36);
										}
										{	/* Cc/indent.scm 40 */
											char *BgL_string1z00_56;
											char *BgL_string2z00_57;

											BgL_string1z00_56 = BSTRING_TO_STRING(BgL_snamez00_34);
											BgL_string2z00_57 = BSTRING_TO_STRING(BgL_dnamez00_35);
											{	/* Cc/indent.scm 40 */
												int BgL_arg1263z00_59;

												BgL_arg1263z00_59 =
													rename(BgL_string1z00_56, BgL_string2z00_57);
												return BBOOL(((long) (BgL_arg1263z00_59) == 0L));
									}}}
								else
									{	/* Cc/indent.scm 36 */
										return BFALSE;
									}
							}
						}
					}
				else
					{	/* Cc/indent.scm 24 */
						return BFALSE;
					}
			}
		}

	}



/* &indent */
	obj_t BGl_z62indentz62zzcc_indentz00(obj_t BgL_envz00_60,
		obj_t BgL_namez00_61)
	{
		{	/* Cc/indent.scm 23 */
			return BGl_indentz00zzcc_indentz00(BgL_namez00_61);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcc_indentz00(void)
	{
		{	/* Cc/indent.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcc_indentz00(void)
	{
		{	/* Cc/indent.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcc_indentz00(void)
	{
		{	/* Cc/indent.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcc_indentz00(void)
	{
		{	/* Cc/indent.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1060z00zzcc_indentz00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1060z00zzcc_indentz00));
		}

	}

#ifdef __cplusplus
}
#endif
