/*===========================================================================*/
/*   (Cc/cc.scm)                                                             */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cc/cc.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CC_CC_TYPE_DEFINITIONS
#define BGL_CC_CC_TYPE_DEFINITIONS
#endif													// BGL_CC_CC_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_win32zd2cczd2zzcc_ccz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcc_ccz00 = BUNSPEC;
	extern obj_t BGl_za2cczd2optionsza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_md5sumz00zz__md5z00(obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcc_ccz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzcc_ccz00(void);
	BGL_IMPORT obj_t BGl_appendz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_IMPORT obj_t BGl_dirnamez00zz__osz00(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_basenamez00zz__osz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzcc_ccz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcc_ccz00(void);
	extern obj_t BGl_unixzd2filenamezd2zzcc_execz00(obj_t);
	extern obj_t BGl_za2ccza2z00zzengine_paramz00;
	extern obj_t BGl_za2cczd2ozd2optionza2z00zzengine_paramz00;
	extern obj_t BGl_execz00zzcc_execz00(obj_t, bool_t, obj_t);
	BGL_IMPORT obj_t bgl_bignum_sub(obj_t, obj_t);
	static obj_t BGl_z62ccz62zzcc_ccz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	static obj_t BGl_unixzd2cczd2zzcc_ccz00(obj_t, obj_t, bool_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2cflagsza2z00zzengine_paramz00;
	BGL_IMPORT obj_t bgl_append2(obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza31554ze3ze70z60zzcc_ccz00(obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcc_ccz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcc_execz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__md5z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__processz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__configurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_za2bigloozd2argsza2zd2zzengine_paramz00;
	extern obj_t BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00;
	extern obj_t BGl_za2libzd2dirza2zd2zzengine_paramz00;
	static obj_t BGl_cnstzd2initzd2zzcc_ccz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcc_ccz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcc_ccz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzcc_ccz00(void);
	BGL_IMPORT obj_t bigloo_mangle(obj_t);
	extern obj_t BGl_za2cczd2moveza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_bigloozd2configzd2zz__configurez00(obj_t);
	BGL_IMPORT obj_t BGl_pwdz00zz__osz00(void);
	static obj_t BGl_commandzd2linezd2ze3stringze3zzcc_ccz00(obj_t);
	BGL_IMPORT bool_t bgl_directoryp(char *);
	extern obj_t BGl_za2czd2debugzd2optionza2z00zzengine_paramz00;
	BGL_IMPORT obj_t BGl_runzd2processzd2zz__processz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ccz00zzcc_ccz00(obj_t, obj_t, bool_t);
	static obj_t BGl_loopze70ze7zzcc_ccz00(obj_t);
	static obj_t BGl_loopze71ze7zzcc_ccz00(obj_t);
	static obj_t BGl_loopze72ze7zzcc_ccz00(obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_za2rmzd2tmpzd2filesza2z00zzengine_paramz00;
	static obj_t BGl_mingwzd2cczd2zzcc_ccz00(obj_t, obj_t, bool_t);
	extern obj_t BGl_stringzd2splitzd2charz00zztools_miscz00(obj_t, obj_t);
	extern obj_t BGl_za2czd2debugza2zd2zzengine_paramz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[7];


	   
		 
		DEFINE_STRING(BGl_string1629z00zzcc_ccz00,
		BgL_bgl_string1629za700za7za7c1655za7, "unix", 4);
	      DEFINE_STRING(BGl_string1630z00zzcc_ccz00,
		BgL_bgl_string1630za700za7za7c1656za7, "win32", 5);
	      DEFINE_STRING(BGl_string1631z00zzcc_ccz00,
		BgL_bgl_string1631za700za7za7c1657za7, "mingw", 5);
	      DEFINE_STRING(BGl_string1632z00zzcc_ccz00,
		BgL_bgl_string1632za700za7za7c1658za7, "cc", 2);
	      DEFINE_STRING(BGl_string1633z00zzcc_ccz00,
		BgL_bgl_string1633za700za7za7c1659za7, "Unknown os", 10);
	      DEFINE_STRING(BGl_string1634z00zzcc_ccz00,
		BgL_bgl_string1634za700za7za7c1660za7, "~a", 2);
	      DEFINE_STRING(BGl_string1635z00zzcc_ccz00,
		BgL_bgl_string1635za700za7za7c1661za7, ")", 1);
	      DEFINE_STRING(BGl_string1636z00zzcc_ccz00,
		BgL_bgl_string1636za700za7za7c1662za7, "   . cc (", 9);
	      DEFINE_STRING(BGl_string1637z00zzcc_ccz00,
		BgL_bgl_string1637za700za7za7c1663za7, ".", 1);
	      DEFINE_STRING(BGl_string1638z00zzcc_ccz00,
		BgL_bgl_string1638za700za7za7c1664za7, "", 0);
	      DEFINE_STRING(BGl_string1639z00zzcc_ccz00,
		BgL_bgl_string1639za700za7za7c1665za7, "~( )", 4);
	      DEFINE_STRING(BGl_string1640z00zzcc_ccz00,
		BgL_bgl_string1640za700za7za7c1666za7, " ", 1);
	      DEFINE_STRING(BGl_string1641z00zzcc_ccz00,
		BgL_bgl_string1641za700za7za7c1667za7, ".c", 2);
	      DEFINE_STRING(BGl_string1642z00zzcc_ccz00,
		BgL_bgl_string1642za700za7za7c1668za7, " -I. ", 5);
	      DEFINE_STRING(BGl_string1643z00zzcc_ccz00,
		BgL_bgl_string1643za700za7za7c1669za7, " -c ", 4);
	      DEFINE_STRING(BGl_string1644z00zzcc_ccz00,
		BgL_bgl_string1644za700za7za7c1670za7, "&& ", 3);
	      DEFINE_STRING(BGl_string1645z00zzcc_ccz00,
		BgL_bgl_string1645za700za7za7c1671za7, " 2>&1 >/dev/null ", 17);
	      DEFINE_STRING(BGl_string1646z00zzcc_ccz00,
		BgL_bgl_string1646za700za7za7c1672za7, "      [", 7);
	      DEFINE_STRING(BGl_string1647z00zzcc_ccz00,
		BgL_bgl_string1647za700za7za7c1673za7, "can't process cc on stdout", 26);
	      DEFINE_STRING(BGl_string1648z00zzcc_ccz00,
		BgL_bgl_string1648za700za7za7c1674za7, "-I", 2);
	      DEFINE_STRING(BGl_string1649z00zzcc_ccz00,
		BgL_bgl_string1649za700za7za7c1675za7, ".c ", 3);
	      DEFINE_STRING(BGl_string1650z00zzcc_ccz00,
		BgL_bgl_string1650za700za7za7c1676za7, "      ", 6);
	      DEFINE_STRING(BGl_string1651z00zzcc_ccz00,
		BgL_bgl_string1651za700za7za7c1677za7, "cc_cc", 5);
	      DEFINE_STRING(BGl_string1652z00zzcc_ccz00,
		BgL_bgl_string1652za700za7za7c1678za7,
		"(:wait #t) (\"-c\") (\"-I.\") shell-mv shell-rm c-nan-flag c-pic-flag ",
		66);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cczd2envzd2zzcc_ccz00,
		BgL_bgl_za762ccza762za7za7cc_ccza71679za7, BGl_z62ccz62zzcc_ccz00, 0L,
		BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcc_ccz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcc_ccz00(long
		BgL_checksumz00_480, char *BgL_fromz00_481)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcc_ccz00))
				{
					BGl_requirezd2initializa7ationz75zzcc_ccz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcc_ccz00();
					BGl_libraryzd2moduleszd2initz00zzcc_ccz00();
					BGl_cnstzd2initzd2zzcc_ccz00();
					BGl_importedzd2moduleszd2initz00zzcc_ccz00();
					return BGl_methodzd2initzd2zzcc_ccz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcc_ccz00(void)
	{
		{	/* Cc/cc.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "cc_cc");
			BGl_modulezd2initializa7ationz75zz__md5z00(0L, "cc_cc");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cc_cc");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cc_cc");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "cc_cc");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cc_cc");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cc_cc");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cc_cc");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "cc_cc");
			BGl_modulezd2initializa7ationz75zz__configurez00(0L, "cc_cc");
			BGl_modulezd2initializa7ationz75zz__processz00(0L, "cc_cc");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L, "cc_cc");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "cc_cc");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "cc_cc");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L, "cc_cc");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcc_ccz00(void)
	{
		{	/* Cc/cc.scm 15 */
			{	/* Cc/cc.scm 15 */
				obj_t BgL_cportz00_468;

				{	/* Cc/cc.scm 15 */
					obj_t BgL_stringz00_475;

					BgL_stringz00_475 = BGl_string1652z00zzcc_ccz00;
					{	/* Cc/cc.scm 15 */
						obj_t BgL_startz00_476;

						BgL_startz00_476 = BINT(0L);
						{	/* Cc/cc.scm 15 */
							obj_t BgL_endz00_477;

							BgL_endz00_477 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_475)));
							{	/* Cc/cc.scm 15 */

								BgL_cportz00_468 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_475, BgL_startz00_476, BgL_endz00_477);
				}}}}
				{
					long BgL_iz00_469;

					BgL_iz00_469 = 6L;
				BgL_loopz00_470:
					if ((BgL_iz00_469 == -1L))
						{	/* Cc/cc.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cc/cc.scm 15 */
							{	/* Cc/cc.scm 15 */
								obj_t BgL_arg1654z00_471;

								{	/* Cc/cc.scm 15 */

									{	/* Cc/cc.scm 15 */
										obj_t BgL_locationz00_473;

										BgL_locationz00_473 = BBOOL(((bool_t) 0));
										{	/* Cc/cc.scm 15 */

											BgL_arg1654z00_471 =
												BGl_readz00zz__readerz00(BgL_cportz00_468,
												BgL_locationz00_473);
										}
									}
								}
								{	/* Cc/cc.scm 15 */
									int BgL_tmpz00_514;

									BgL_tmpz00_514 = (int) (BgL_iz00_469);
									CNST_TABLE_SET(BgL_tmpz00_514, BgL_arg1654z00_471);
							}}
							{	/* Cc/cc.scm 15 */
								int BgL_auxz00_474;

								BgL_auxz00_474 = (int) ((BgL_iz00_469 - 1L));
								{
									long BgL_iz00_519;

									BgL_iz00_519 = (long) (BgL_auxz00_474);
									BgL_iz00_469 = BgL_iz00_519;
									goto BgL_loopz00_470;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcc_ccz00(void)
	{
		{	/* Cc/cc.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzcc_ccz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_24;

				BgL_headz00_24 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_25;
					obj_t BgL_tailz00_26;

					BgL_prevz00_25 = BgL_headz00_24;
					BgL_tailz00_26 = BgL_l1z00_1;
				BgL_loopz00_27:
					if (PAIRP(BgL_tailz00_26))
						{
							obj_t BgL_newzd2prevzd2_29;

							BgL_newzd2prevzd2_29 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_26), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_25, BgL_newzd2prevzd2_29);
							{
								obj_t BgL_tailz00_529;
								obj_t BgL_prevz00_528;

								BgL_prevz00_528 = BgL_newzd2prevzd2_29;
								BgL_tailz00_529 = CDR(BgL_tailz00_26);
								BgL_tailz00_26 = BgL_tailz00_529;
								BgL_prevz00_25 = BgL_prevz00_528;
								goto BgL_loopz00_27;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_24);
				}
			}
		}

	}



/* cc */
	BGL_EXPORTED_DEF obj_t BGl_ccz00zzcc_ccz00(obj_t BgL_namez00_3,
		obj_t BgL_onamez00_4, bool_t BgL_needzd2tozd2returnz00_5)
	{
		{	/* Cc/cc.scm 27 */
			{	/* Cc/cc.scm 29 */
				bool_t BgL_test1684z00_532;

				{	/* Cc/cc.scm 29 */
					obj_t BgL_string1z00_308;

					BgL_string1z00_308 = string_to_bstring(OS_CLASS);
					{	/* Cc/cc.scm 29 */
						long BgL_l1z00_310;

						BgL_l1z00_310 = STRING_LENGTH(BgL_string1z00_308);
						if ((BgL_l1z00_310 == 4L))
							{	/* Cc/cc.scm 29 */
								int BgL_arg1282z00_313;

								{	/* Cc/cc.scm 29 */
									char *BgL_auxz00_539;
									char *BgL_tmpz00_537;

									BgL_auxz00_539 =
										BSTRING_TO_STRING(BGl_string1629z00zzcc_ccz00);
									BgL_tmpz00_537 = BSTRING_TO_STRING(BgL_string1z00_308);
									BgL_arg1282z00_313 =
										memcmp(BgL_tmpz00_537, BgL_auxz00_539, BgL_l1z00_310);
								}
								BgL_test1684z00_532 = ((long) (BgL_arg1282z00_313) == 0L);
							}
						else
							{	/* Cc/cc.scm 29 */
								BgL_test1684z00_532 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test1684z00_532)
					{	/* Cc/cc.scm 29 */
						BGL_TAIL return
							BGl_unixzd2cczd2zzcc_ccz00(BgL_namez00_3, BgL_onamez00_4,
							BgL_needzd2tozd2returnz00_5);
					}
				else
					{	/* Cc/cc.scm 31 */
						bool_t BgL_test1686z00_545;

						{	/* Cc/cc.scm 31 */
							obj_t BgL_string1z00_319;

							BgL_string1z00_319 = string_to_bstring(OS_CLASS);
							{	/* Cc/cc.scm 31 */
								long BgL_l1z00_321;

								BgL_l1z00_321 = STRING_LENGTH(BgL_string1z00_319);
								if ((BgL_l1z00_321 == 5L))
									{	/* Cc/cc.scm 31 */
										int BgL_arg1282z00_324;

										{	/* Cc/cc.scm 31 */
											char *BgL_auxz00_552;
											char *BgL_tmpz00_550;

											BgL_auxz00_552 =
												BSTRING_TO_STRING(BGl_string1630z00zzcc_ccz00);
											BgL_tmpz00_550 = BSTRING_TO_STRING(BgL_string1z00_319);
											BgL_arg1282z00_324 =
												memcmp(BgL_tmpz00_550, BgL_auxz00_552, BgL_l1z00_321);
										}
										BgL_test1686z00_545 = ((long) (BgL_arg1282z00_324) == 0L);
									}
								else
									{	/* Cc/cc.scm 31 */
										BgL_test1686z00_545 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test1686z00_545)
							{	/* Cc/cc.scm 31 */
								return
									BGl_win32zd2cczd2zzcc_ccz00(BgL_namez00_3, BgL_onamez00_4);
							}
						else
							{	/* Cc/cc.scm 33 */
								bool_t BgL_test1688z00_558;

								{	/* Cc/cc.scm 33 */
									obj_t BgL_string1z00_330;

									BgL_string1z00_330 = string_to_bstring(OS_CLASS);
									{	/* Cc/cc.scm 33 */
										long BgL_l1z00_332;

										BgL_l1z00_332 = STRING_LENGTH(BgL_string1z00_330);
										if ((BgL_l1z00_332 == 5L))
											{	/* Cc/cc.scm 33 */
												int BgL_arg1282z00_335;

												{	/* Cc/cc.scm 33 */
													char *BgL_auxz00_565;
													char *BgL_tmpz00_563;

													BgL_auxz00_565 =
														BSTRING_TO_STRING(BGl_string1631z00zzcc_ccz00);
													BgL_tmpz00_563 =
														BSTRING_TO_STRING(BgL_string1z00_330);
													BgL_arg1282z00_335 =
														memcmp(BgL_tmpz00_563, BgL_auxz00_565,
														BgL_l1z00_332);
												}
												BgL_test1688z00_558 =
													((long) (BgL_arg1282z00_335) == 0L);
											}
										else
											{	/* Cc/cc.scm 33 */
												BgL_test1688z00_558 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test1688z00_558)
									{	/* Cc/cc.scm 33 */
										BGL_TAIL return
											BGl_mingwzd2cczd2zzcc_ccz00(BgL_namez00_3, BgL_onamez00_4,
											BgL_needzd2tozd2returnz00_5);
									}
								else
									{	/* Cc/cc.scm 33 */
										return
											BGl_userzd2errorzd2zztools_errorz00
											(BGl_string1632z00zzcc_ccz00, BGl_string1633z00zzcc_ccz00,
											string_to_bstring(OS_CLASS), BNIL);
									}
							}
					}
			}
		}

	}



/* &cc */
	obj_t BGl_z62ccz62zzcc_ccz00(obj_t BgL_envz00_464, obj_t BgL_namez00_465,
		obj_t BgL_onamez00_466, obj_t BgL_needzd2tozd2returnz00_467)
	{
		{	/* Cc/cc.scm 27 */
			return
				BGl_ccz00zzcc_ccz00(BgL_namez00_465, BgL_onamez00_466,
				CBOOL(BgL_needzd2tozd2returnz00_467));
		}

	}



/* command-line->string */
	obj_t BGl_commandzd2linezd2ze3stringze3zzcc_ccz00(obj_t BgL_lz00_6)
	{
		{	/* Cc/cc.scm 41 */
			{	/* Cc/cc.scm 42 */
				obj_t BgL_arg1080z00_43;

				{	/* Cc/cc.scm 42 */
					obj_t BgL_list1081z00_44;

					BgL_list1081z00_44 = MAKE_YOUNG_PAIR(BgL_lz00_6, BNIL);
					BgL_arg1080z00_43 =
						BGl_formatz00zz__r4_output_6_10_3z00(BGl_string1634z00zzcc_ccz00,
						BgL_list1081z00_44);
				}
				BGL_TAIL return BGl_md5sumz00zz__md5z00(BgL_arg1080z00_43);
			}
		}

	}



/* unix-cc */
	obj_t BGl_unixzd2cczd2zzcc_ccz00(obj_t BgL_namez00_7, obj_t BgL_onamez00_8,
		bool_t BgL_needzd2tozd2returnz00_9)
	{
		{	/* Cc/cc.scm 47 */
			{	/* Cc/cc.scm 48 */
				obj_t BgL_list1082z00_45;

				{	/* Cc/cc.scm 48 */
					obj_t BgL_arg1083z00_46;

					{	/* Cc/cc.scm 48 */
						obj_t BgL_arg1084z00_47;

						{	/* Cc/cc.scm 48 */
							obj_t BgL_arg1085z00_48;

							BgL_arg1085z00_48 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
							BgL_arg1084z00_47 =
								MAKE_YOUNG_PAIR(BGl_string1635z00zzcc_ccz00, BgL_arg1085z00_48);
						}
						BgL_arg1083z00_46 =
							MAKE_YOUNG_PAIR(BGl_za2ccza2z00zzengine_paramz00,
							BgL_arg1084z00_47);
					}
					BgL_list1082z00_45 =
						MAKE_YOUNG_PAIR(BGl_string1636z00zzcc_ccz00, BgL_arg1083z00_46);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1082z00_45);
			}
			if (STRINGP(BgL_namez00_7))
				{	/* Cc/cc.scm 53 */
					obj_t BgL_objnamez00_50;

					if (STRINGP(BgL_onamez00_8))
						{	/* Cc/cc.scm 53 */
							BgL_objnamez00_50 = BgL_onamez00_8;
						}
					else
						{	/* Cc/cc.scm 53 */
							BgL_objnamez00_50 = BgL_namez00_7;
						}
					{	/* Cc/cc.scm 53 */
						obj_t BgL_basenamez00_51;

						BgL_basenamez00_51 = BGl_basenamez00zz__osz00(BgL_objnamez00_50);
						{	/* Cc/cc.scm 54 */
							bool_t BgL_needzd2ozd2_52;

							{	/* Cc/cc.scm 55 */
								bool_t BgL_test1692z00_590;

								{	/* Cc/cc.scm 55 */
									long BgL_l1z00_343;

									BgL_l1z00_343 = STRING_LENGTH(BgL_basenamez00_51);
									if ((BgL_l1z00_343 == STRING_LENGTH(BgL_objnamez00_50)))
										{	/* Cc/cc.scm 55 */
											int BgL_arg1282z00_346;

											{	/* Cc/cc.scm 55 */
												char *BgL_auxz00_597;
												char *BgL_tmpz00_595;

												BgL_auxz00_597 = BSTRING_TO_STRING(BgL_objnamez00_50);
												BgL_tmpz00_595 = BSTRING_TO_STRING(BgL_basenamez00_51);
												BgL_arg1282z00_346 =
													memcmp(BgL_tmpz00_595, BgL_auxz00_597, BgL_l1z00_343);
											}
											BgL_test1692z00_590 = ((long) (BgL_arg1282z00_346) == 0L);
										}
									else
										{	/* Cc/cc.scm 55 */
											BgL_test1692z00_590 = ((bool_t) 0);
										}
								}
								if (BgL_test1692z00_590)
									{	/* Cc/cc.scm 55 */
										BgL_needzd2ozd2_52 = ((bool_t) 0);
									}
								else
									{	/* Cc/cc.scm 56 */
										bool_t BgL_test1694z00_602;

										{	/* Cc/cc.scm 56 */
											obj_t BgL_arg1239z00_150;
											obj_t BgL_arg1242z00_151;

											BgL_arg1239z00_150 = BGl_pwdz00zz__osz00();
											BgL_arg1242z00_151 =
												BGl_dirnamez00zz__osz00(BgL_objnamez00_50);
											{	/* Cc/cc.scm 56 */
												long BgL_l1z00_354;

												BgL_l1z00_354 =
													STRING_LENGTH(((obj_t) BgL_arg1239z00_150));
												if (
													(BgL_l1z00_354 == STRING_LENGTH(BgL_arg1242z00_151)))
													{	/* Cc/cc.scm 56 */
														int BgL_arg1282z00_357;

														{	/* Cc/cc.scm 56 */
															char *BgL_auxz00_613;
															char *BgL_tmpz00_610;

															BgL_auxz00_613 =
																BSTRING_TO_STRING(BgL_arg1242z00_151);
															BgL_tmpz00_610 =
																BSTRING_TO_STRING(((obj_t) BgL_arg1239z00_150));
															BgL_arg1282z00_357 =
																memcmp(BgL_tmpz00_610, BgL_auxz00_613,
																BgL_l1z00_354);
														}
														BgL_test1694z00_602 =
															((long) (BgL_arg1282z00_357) == 0L);
													}
												else
													{	/* Cc/cc.scm 56 */
														BgL_test1694z00_602 = ((bool_t) 0);
													}
											}
										}
										if (BgL_test1694z00_602)
											{	/* Cc/cc.scm 56 */
												BgL_needzd2ozd2_52 = ((bool_t) 0);
											}
										else
											{	/* Cc/cc.scm 57 */
												bool_t BgL_test1696z00_618;

												{	/* Cc/cc.scm 57 */
													obj_t BgL_arg1238z00_149;

													BgL_arg1238z00_149 =
														BGl_dirnamez00zz__osz00(BgL_objnamez00_50);
													if ((1L == STRING_LENGTH(BgL_arg1238z00_149)))
														{	/* Cc/cc.scm 57 */
															int BgL_arg1282z00_368;

															{	/* Cc/cc.scm 57 */
																char *BgL_auxz00_625;
																char *BgL_tmpz00_623;

																BgL_auxz00_625 =
																	BSTRING_TO_STRING(BgL_arg1238z00_149);
																BgL_tmpz00_623 =
																	BSTRING_TO_STRING
																	(BGl_string1637z00zzcc_ccz00);
																BgL_arg1282z00_368 =
																	memcmp(BgL_tmpz00_623, BgL_auxz00_625, 1L);
															}
															BgL_test1696z00_618 =
																((long) (BgL_arg1282z00_368) == 0L);
														}
													else
														{	/* Cc/cc.scm 57 */
															BgL_test1696z00_618 = ((bool_t) 0);
														}
												}
												if (BgL_test1696z00_618)
													{	/* Cc/cc.scm 57 */
														BgL_needzd2ozd2_52 = ((bool_t) 0);
													}
												else
													{	/* Cc/cc.scm 57 */
														BgL_needzd2ozd2_52 = ((bool_t) 1);
													}
											}
									}
							}
							{	/* Cc/cc.scm 55 */
								obj_t BgL_needmvz00_53;

								if (BgL_needzd2ozd2_52)
									{	/* Cc/cc.scm 58 */
										obj_t BgL__ortest_1051z00_144;

										BgL__ortest_1051z00_144 =
											BGl_za2cczd2moveza2zd2zzengine_paramz00;
										if (CBOOL(BgL__ortest_1051z00_144))
											{	/* Cc/cc.scm 58 */
												BgL_needmvz00_53 = BgL__ortest_1051z00_144;
											}
										else
											{	/* Cc/cc.scm 58 */
												obj_t BgL_string1z00_374;

												BgL_string1z00_374 =
													BGl_za2cczd2ozd2optionza2z00zzengine_paramz00;
												{	/* Cc/cc.scm 58 */
													long BgL_l1z00_376;

													BgL_l1z00_376 = STRING_LENGTH(BgL_string1z00_374);
													if ((BgL_l1z00_376 == 0L))
														{	/* Cc/cc.scm 58 */
															int BgL_arg1282z00_379;

															{	/* Cc/cc.scm 58 */
																char *BgL_auxz00_638;
																char *BgL_tmpz00_636;

																BgL_auxz00_638 =
																	BSTRING_TO_STRING
																	(BGl_string1638z00zzcc_ccz00);
																BgL_tmpz00_636 =
																	BSTRING_TO_STRING(BgL_string1z00_374);
																BgL_arg1282z00_379 =
																	memcmp(BgL_tmpz00_636, BgL_auxz00_638,
																	BgL_l1z00_376);
															}
															BgL_needmvz00_53 =
																BBOOL(((long) (BgL_arg1282z00_379) == 0L));
														}
													else
														{	/* Cc/cc.scm 58 */
															BgL_needmvz00_53 = BFALSE;
														}
												}
											}
									}
								else
									{	/* Cc/cc.scm 58 */
										BgL_needmvz00_53 = BFALSE;
									}
								{	/* Cc/cc.scm 58 */
									obj_t BgL_objz00_54;

									if (BgL_needzd2ozd2_52)
										{	/* Cc/cc.scm 60 */
											if (CBOOL(BgL_needmvz00_53))
												{	/* Cc/cc.scm 65 */
													obj_t BgL_arg1225z00_132;

													{	/* Cc/cc.scm 65 */
														obj_t BgL_arg1227z00_134;
														obj_t BgL_arg1228z00_135;

														{	/* Cc/cc.scm 65 */
															obj_t BgL_arg1233z00_140;

															{	/* Cc/cc.scm 65 */
																obj_t BgL_symbolz00_385;

																BgL_symbolz00_385 =
																	BGl_za2moduleza2z00zzmodule_modulez00;
																{	/* Cc/cc.scm 65 */
																	obj_t BgL_arg1455z00_386;

																	BgL_arg1455z00_386 =
																		SYMBOL_TO_STRING(BgL_symbolz00_385);
																	BgL_arg1233z00_140 =
																		BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																		(BgL_arg1455z00_386);
																}
															}
															BgL_arg1227z00_134 =
																bigloo_mangle(BgL_arg1233z00_140);
														}
														BgL_arg1228z00_135 =
															BGl_commandzd2linezd2ze3stringze3zzcc_ccz00
															(BGl_za2bigloozd2argsza2zd2zzengine_paramz00);
														{	/* Cc/cc.scm 64 */
															obj_t BgL_list1229z00_136;

															{	/* Cc/cc.scm 64 */
																obj_t BgL_arg1230z00_137;

																{	/* Cc/cc.scm 64 */
																	obj_t BgL_arg1231z00_138;

																	{	/* Cc/cc.scm 64 */
																		obj_t BgL_arg1232z00_139;

																		BgL_arg1232z00_139 =
																			MAKE_YOUNG_PAIR
																			(BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00,
																			BNIL);
																		BgL_arg1231z00_138 =
																			MAKE_YOUNG_PAIR
																			(BGl_string1637z00zzcc_ccz00,
																			BgL_arg1232z00_139);
																	}
																	BgL_arg1230z00_137 =
																		MAKE_YOUNG_PAIR(BgL_arg1228z00_135,
																		BgL_arg1231z00_138);
																}
																BgL_list1229z00_136 =
																	MAKE_YOUNG_PAIR(BgL_arg1227z00_134,
																	BgL_arg1230z00_137);
															}
															BgL_arg1225z00_132 =
																BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																(BgL_list1229z00_136);
														}
													}
													{	/* Cc/cc.scm 63 */
														obj_t BgL_list1226z00_133;

														BgL_list1226z00_133 =
															MAKE_YOUNG_PAIR(BgL_arg1225z00_132, BNIL);
														BgL_objz00_54 =
															BGl_unixzd2filenamezd2zzcc_execz00
															(BgL_list1226z00_133);
													}
												}
											else
												{	/* Cc/cc.scm 71 */
													obj_t BgL_arg1234z00_141;

													BgL_arg1234z00_141 =
														string_append_3(BgL_objnamez00_50,
														BGl_string1637z00zzcc_ccz00,
														BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00);
													{	/* Cc/cc.scm 70 */
														obj_t BgL_list1235z00_142;

														BgL_list1235z00_142 =
															MAKE_YOUNG_PAIR(BgL_arg1234z00_141, BNIL);
														BgL_objz00_54 =
															BGl_unixzd2filenamezd2zzcc_execz00
															(BgL_list1235z00_142);
													}
												}
										}
									else
										{	/* Cc/cc.scm 60 */
											BgL_objz00_54 = BGl_string1638z00zzcc_ccz00;
										}
									{	/* Cc/cc.scm 59 */
										obj_t BgL_ccz00_55;

										{	/* Cc/cc.scm 76 */
											obj_t BgL_arg1145z00_84;
											obj_t BgL_arg1148z00_85;
											obj_t BgL_arg1149z00_86;
											obj_t BgL_arg1152z00_87;
											obj_t BgL_arg1153z00_88;
											obj_t BgL_arg1154z00_89;
											obj_t BgL_arg1157z00_90;

											{	/* Cc/cc.scm 76 */
												obj_t BgL_list1200z00_110;

												BgL_list1200z00_110 =
													MAKE_YOUNG_PAIR
													(BGl_za2cczd2optionsza2zd2zzengine_paramz00, BNIL);
												BgL_arg1145z00_84 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string1639z00zzcc_ccz00, BgL_list1200z00_110);
											}
											BgL_arg1148z00_85 =
												string_append(BGl_string1640z00zzcc_ccz00,
												BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
													(0)));
											BgL_arg1149z00_86 =
												string_append(BGl_string1640z00zzcc_ccz00,
												BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
													(1)));
											if (BgL_needzd2ozd2_52)
												{	/* Cc/cc.scm 84 */
													BgL_arg1152z00_87 =
														BGl_za2cczd2ozd2optionza2z00zzengine_paramz00;
												}
											else
												{	/* Cc/cc.scm 84 */
													BgL_arg1152z00_87 = BGl_string1638z00zzcc_ccz00;
												}
											BgL_arg1153z00_88 =
												BGl_loopze72ze7zzcc_ccz00
												(BGl_za2libzd2dirza2zd2zzengine_paramz00);
											{	/* Cc/cc.scm 99 */
												bool_t BgL_test1705z00_671;

												if (CBOOL(BGl_za2czd2debugza2zd2zzengine_paramz00))
													{	/* Cc/cc.scm 99 */
														BgL_test1705z00_671 = ((bool_t) 1);
													}
												else
													{	/* Cc/cc.scm 99 */
														BgL_test1705z00_671 =
															(
															(long)
															CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) >
															0L);
													}
												if (BgL_test1705z00_671)
													{	/* Cc/cc.scm 99 */
														BgL_arg1154z00_89 =
															string_append(BGl_string1640z00zzcc_ccz00,
															BGl_za2czd2debugzd2optionza2z00zzengine_paramz00);
													}
												else
													{	/* Cc/cc.scm 99 */
														BgL_arg1154z00_89 = BGl_string1638z00zzcc_ccz00;
													}
											}
											{	/* Cc/cc.scm 102 */
												obj_t BgL_list1222z00_130;

												{	/* Cc/cc.scm 102 */
													obj_t BgL_arg1223z00_131;

													BgL_arg1223z00_131 =
														MAKE_YOUNG_PAIR(BGl_string1641z00zzcc_ccz00, BNIL);
													BgL_list1222z00_130 =
														MAKE_YOUNG_PAIR(BgL_namez00_7, BgL_arg1223z00_131);
												}
												BgL_arg1157z00_90 =
													BGl_unixzd2filenamezd2zzcc_execz00
													(BgL_list1222z00_130);
											}
											{	/* Cc/cc.scm 74 */
												obj_t BgL_list1158z00_91;

												{	/* Cc/cc.scm 74 */
													obj_t BgL_arg1162z00_92;

													{	/* Cc/cc.scm 74 */
														obj_t BgL_arg1164z00_93;

														{	/* Cc/cc.scm 74 */
															obj_t BgL_arg1166z00_94;

															{	/* Cc/cc.scm 74 */
																obj_t BgL_arg1171z00_95;

																{	/* Cc/cc.scm 74 */
																	obj_t BgL_arg1172z00_96;

																	{	/* Cc/cc.scm 74 */
																		obj_t BgL_arg1182z00_97;

																		{	/* Cc/cc.scm 74 */
																			obj_t BgL_arg1183z00_98;

																			{	/* Cc/cc.scm 74 */
																				obj_t BgL_arg1187z00_99;

																				{	/* Cc/cc.scm 74 */
																					obj_t BgL_arg1188z00_100;

																					{	/* Cc/cc.scm 74 */
																						obj_t BgL_arg1189z00_101;

																						{	/* Cc/cc.scm 74 */
																							obj_t BgL_arg1190z00_102;

																							{	/* Cc/cc.scm 74 */
																								obj_t BgL_arg1191z00_103;

																								{	/* Cc/cc.scm 74 */
																									obj_t BgL_arg1193z00_104;

																									{	/* Cc/cc.scm 74 */
																										obj_t BgL_arg1194z00_105;

																										{	/* Cc/cc.scm 74 */
																											obj_t BgL_arg1196z00_106;

																											{	/* Cc/cc.scm 74 */
																												obj_t
																													BgL_arg1197z00_107;
																												{	/* Cc/cc.scm 74 */
																													obj_t
																														BgL_arg1198z00_108;
																													{	/* Cc/cc.scm 74 */
																														obj_t
																															BgL_arg1199z00_109;
																														BgL_arg1199z00_109 =
																															MAKE_YOUNG_PAIR
																															(BGl_string1640z00zzcc_ccz00,
																															BNIL);
																														BgL_arg1198z00_108 =
																															MAKE_YOUNG_PAIR
																															(BgL_arg1157z00_90,
																															BgL_arg1199z00_109);
																													}
																													BgL_arg1197z00_107 =
																														MAKE_YOUNG_PAIR
																														(BGl_string1640z00zzcc_ccz00,
																														BgL_arg1198z00_108);
																												}
																												BgL_arg1196z00_106 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1154z00_89,
																													BgL_arg1197z00_107);
																											}
																											BgL_arg1194z00_105 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1153z00_88,
																												BgL_arg1196z00_106);
																										}
																										BgL_arg1193z00_104 =
																											MAKE_YOUNG_PAIR
																											(BGl_string1642z00zzcc_ccz00,
																											BgL_arg1194z00_105);
																									}
																									BgL_arg1191z00_103 =
																										MAKE_YOUNG_PAIR
																										(BgL_objz00_54,
																										BgL_arg1193z00_104);
																								}
																								BgL_arg1190z00_102 =
																									MAKE_YOUNG_PAIR
																									(BGl_string1640z00zzcc_ccz00,
																									BgL_arg1191z00_103);
																							}
																							BgL_arg1189z00_101 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1152z00_87,
																								BgL_arg1190z00_102);
																						}
																						BgL_arg1188z00_100 =
																							MAKE_YOUNG_PAIR
																							(BGl_string1643z00zzcc_ccz00,
																							BgL_arg1189z00_101);
																					}
																					BgL_arg1187z00_99 =
																						MAKE_YOUNG_PAIR
																						(BGl_string1640z00zzcc_ccz00,
																						BgL_arg1188z00_100);
																				}
																				BgL_arg1183z00_98 =
																					MAKE_YOUNG_PAIR(BgL_arg1149z00_86,
																					BgL_arg1187z00_99);
																			}
																			BgL_arg1182z00_97 =
																				MAKE_YOUNG_PAIR(BgL_arg1148z00_85,
																				BgL_arg1183z00_98);
																		}
																		BgL_arg1172z00_96 =
																			MAKE_YOUNG_PAIR
																			(BGl_string1640z00zzcc_ccz00,
																			BgL_arg1182z00_97);
																	}
																	BgL_arg1171z00_95 =
																		MAKE_YOUNG_PAIR
																		(BGl_za2cflagsza2z00zzengine_paramz00,
																		BgL_arg1172z00_96);
																}
																BgL_arg1166z00_94 =
																	MAKE_YOUNG_PAIR(BGl_string1640z00zzcc_ccz00,
																	BgL_arg1171z00_95);
															}
															BgL_arg1164z00_93 =
																MAKE_YOUNG_PAIR(BgL_arg1145z00_84,
																BgL_arg1166z00_94);
														}
														BgL_arg1162z00_92 =
															MAKE_YOUNG_PAIR(BGl_string1640z00zzcc_ccz00,
															BgL_arg1164z00_93);
													}
													BgL_list1158z00_91 =
														MAKE_YOUNG_PAIR(BGl_za2ccza2z00zzengine_paramz00,
														BgL_arg1162z00_92);
												}
												BgL_ccz00_55 =
													BGl_stringzd2appendzd2zz__r4_strings_6_7z00
													(BgL_list1158z00_91);
											}
										}
										{	/* Cc/cc.scm 74 */
											obj_t BgL_rmzd2csrczd2_56;

											if (CBOOL(BGl_za2rmzd2tmpzd2filesza2z00zzengine_paramz00))
												{	/* Cc/cc.scm 105 */
													obj_t BgL_arg1131z00_75;
													obj_t BgL_arg1132z00_76;

													BgL_arg1131z00_75 =
														BGl_bigloozd2configzd2zz__configurez00
														(CNST_TABLE_REF(2));
													{	/* Cc/cc.scm 107 */
														obj_t BgL_list1142z00_82;

														{	/* Cc/cc.scm 107 */
															obj_t BgL_arg1143z00_83;

															BgL_arg1143z00_83 =
																MAKE_YOUNG_PAIR(BGl_string1641z00zzcc_ccz00,
																BNIL);
															BgL_list1142z00_82 =
																MAKE_YOUNG_PAIR(BgL_namez00_7,
																BgL_arg1143z00_83);
														}
														BgL_arg1132z00_76 =
															BGl_unixzd2filenamezd2zzcc_execz00
															(BgL_list1142z00_82);
													}
													{	/* Cc/cc.scm 104 */
														obj_t BgL_list1133z00_77;

														{	/* Cc/cc.scm 104 */
															obj_t BgL_arg1137z00_78;

															{	/* Cc/cc.scm 104 */
																obj_t BgL_arg1138z00_79;

																{	/* Cc/cc.scm 104 */
																	obj_t BgL_arg1140z00_80;

																	{	/* Cc/cc.scm 104 */
																		obj_t BgL_arg1141z00_81;

																		BgL_arg1141z00_81 =
																			MAKE_YOUNG_PAIR
																			(BGl_string1640z00zzcc_ccz00, BNIL);
																		BgL_arg1140z00_80 =
																			MAKE_YOUNG_PAIR(BgL_arg1132z00_76,
																			BgL_arg1141z00_81);
																	}
																	BgL_arg1138z00_79 =
																		MAKE_YOUNG_PAIR(BGl_string1640z00zzcc_ccz00,
																		BgL_arg1140z00_80);
																}
																BgL_arg1137z00_78 =
																	MAKE_YOUNG_PAIR(BgL_arg1131z00_75,
																	BgL_arg1138z00_79);
															}
															BgL_list1133z00_77 =
																MAKE_YOUNG_PAIR(BGl_string1644z00zzcc_ccz00,
																BgL_arg1137z00_78);
														}
														BgL_rmzd2csrczd2_56 =
															BGl_stringzd2appendzd2zz__r4_strings_6_7z00
															(BgL_list1133z00_77);
													}
												}
											else
												{	/* Cc/cc.scm 103 */
													BgL_rmzd2csrczd2_56 = BGl_string1638z00zzcc_ccz00;
												}
											{	/* Cc/cc.scm 103 */
												obj_t BgL_mvzd2objzd2_57;

												if (CBOOL(BgL_needmvz00_53))
													{	/* Cc/cc.scm 111 */
														obj_t BgL_arg1097z00_63;
														obj_t BgL_arg1102z00_64;

														BgL_arg1097z00_63 =
															BGl_bigloozd2configzd2zz__configurez00
															(CNST_TABLE_REF(3));
														{	/* Cc/cc.scm 115 */
															obj_t BgL_list1126z00_72;

															{	/* Cc/cc.scm 115 */
																obj_t BgL_arg1127z00_73;

																{	/* Cc/cc.scm 115 */
																	obj_t BgL_arg1129z00_74;

																	BgL_arg1129z00_74 =
																		MAKE_YOUNG_PAIR
																		(BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00,
																		BNIL);
																	BgL_arg1127z00_73 =
																		MAKE_YOUNG_PAIR(BGl_string1637z00zzcc_ccz00,
																		BgL_arg1129z00_74);
																}
																BgL_list1126z00_72 =
																	MAKE_YOUNG_PAIR(BgL_objnamez00_50,
																	BgL_arg1127z00_73);
															}
															BgL_arg1102z00_64 =
																BGl_unixzd2filenamezd2zzcc_execz00
																(BgL_list1126z00_72);
														}
														{	/* Cc/cc.scm 110 */
															obj_t BgL_list1103z00_65;

															{	/* Cc/cc.scm 110 */
																obj_t BgL_arg1104z00_66;

																{	/* Cc/cc.scm 110 */
																	obj_t BgL_arg1114z00_67;

																	{	/* Cc/cc.scm 110 */
																		obj_t BgL_arg1115z00_68;

																		{	/* Cc/cc.scm 110 */
																			obj_t BgL_arg1122z00_69;

																			{	/* Cc/cc.scm 110 */
																				obj_t BgL_arg1123z00_70;

																				{	/* Cc/cc.scm 110 */
																					obj_t BgL_arg1125z00_71;

																					BgL_arg1125z00_71 =
																						MAKE_YOUNG_PAIR
																						(BGl_string1645z00zzcc_ccz00, BNIL);
																					BgL_arg1123z00_70 =
																						MAKE_YOUNG_PAIR(BgL_arg1102z00_64,
																						BgL_arg1125z00_71);
																				}
																				BgL_arg1122z00_69 =
																					MAKE_YOUNG_PAIR
																					(BGl_string1640z00zzcc_ccz00,
																					BgL_arg1123z00_70);
																			}
																			BgL_arg1115z00_68 =
																				MAKE_YOUNG_PAIR(BgL_objz00_54,
																				BgL_arg1122z00_69);
																		}
																		BgL_arg1114z00_67 =
																			MAKE_YOUNG_PAIR
																			(BGl_string1640z00zzcc_ccz00,
																			BgL_arg1115z00_68);
																	}
																	BgL_arg1104z00_66 =
																		MAKE_YOUNG_PAIR(BgL_arg1097z00_63,
																		BgL_arg1114z00_67);
																}
																BgL_list1103z00_65 =
																	MAKE_YOUNG_PAIR(BGl_string1644z00zzcc_ccz00,
																	BgL_arg1104z00_66);
															}
															BgL_mvzd2objzd2_57 =
																BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																(BgL_list1103z00_65);
														}
													}
												else
													{	/* Cc/cc.scm 109 */
														BgL_mvzd2objzd2_57 = BGl_string1638z00zzcc_ccz00;
													}
												{	/* Cc/cc.scm 109 */
													obj_t BgL_cmdz00_58;

													BgL_cmdz00_58 =
														string_append_3(BgL_ccz00_55, BgL_mvzd2objzd2_57,
														BgL_rmzd2csrczd2_56);
													{	/* Cc/cc.scm 120 */

														{	/* Cc/cc.scm 121 */
															obj_t BgL_list1087z00_59;

															{	/* Cc/cc.scm 121 */
																obj_t BgL_arg1088z00_60;

																{	/* Cc/cc.scm 121 */
																	obj_t BgL_arg1090z00_61;

																	{	/* Cc/cc.scm 121 */
																		obj_t BgL_arg1092z00_62;

																		BgL_arg1092z00_62 =
																			MAKE_YOUNG_PAIR(BCHAR(((unsigned char)
																					10)), BNIL);
																		BgL_arg1090z00_61 =
																			MAKE_YOUNG_PAIR(BCHAR(((unsigned char)
																					']')), BgL_arg1092z00_62);
																	}
																	BgL_arg1088z00_60 =
																		MAKE_YOUNG_PAIR(BgL_cmdz00_58,
																		BgL_arg1090z00_61);
																}
																BgL_list1087z00_59 =
																	MAKE_YOUNG_PAIR(BGl_string1646z00zzcc_ccz00,
																	BgL_arg1088z00_60);
															}
															BGl_verbosez00zztools_speekz00(BINT(2L),
																BgL_list1087z00_59);
														}
														return
															BGl_execz00zzcc_execz00(BgL_cmdz00_58,
															BgL_needzd2tozd2returnz00_9,
															BGl_string1632z00zzcc_ccz00);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			else
				{	/* Cc/cc.scm 50 */
					return
						BGl_errorz00zz__errorz00(BGl_string1632z00zzcc_ccz00,
						BGl_string1647z00zzcc_ccz00, BgL_namez00_7);
				}
		}

	}



/* loop~2 */
	obj_t BGl_loopze72ze7zzcc_ccz00(obj_t BgL_pathz00_114)
	{
		{	/* Cc/cc.scm 88 */
		BGl_loopze72ze7zzcc_ccz00:
			if (NULLP(BgL_pathz00_114))
				{	/* Cc/cc.scm 90 */
					return BGl_string1638z00zzcc_ccz00;
				}
			else
				{	/* Cc/cc.scm 92 */
					bool_t BgL_test1714z00_742;

					{	/* Cc/cc.scm 92 */
						obj_t BgL_arg1220z00_127;

						BgL_arg1220z00_127 = CAR(((obj_t) BgL_pathz00_114));
						BgL_test1714z00_742 =
							bgl_directoryp(BSTRING_TO_STRING(BgL_arg1220z00_127));
					}
					if (BgL_test1714z00_742)
						{	/* Cc/cc.scm 94 */
							obj_t BgL_arg1208z00_119;
							obj_t BgL_arg1209z00_120;

							BgL_arg1208z00_119 = CAR(((obj_t) BgL_pathz00_114));
							{	/* Cc/cc.scm 96 */
								obj_t BgL_arg1218z00_125;

								BgL_arg1218z00_125 = CDR(((obj_t) BgL_pathz00_114));
								BgL_arg1209z00_120 =
									BGl_loopze72ze7zzcc_ccz00(BgL_arg1218z00_125);
							}
							{	/* Cc/cc.scm 93 */
								obj_t BgL_list1210z00_121;

								{	/* Cc/cc.scm 93 */
									obj_t BgL_arg1212z00_122;

									{	/* Cc/cc.scm 93 */
										obj_t BgL_arg1215z00_123;

										{	/* Cc/cc.scm 93 */
											obj_t BgL_arg1216z00_124;

											BgL_arg1216z00_124 =
												MAKE_YOUNG_PAIR(BgL_arg1209z00_120, BNIL);
											BgL_arg1215z00_123 =
												MAKE_YOUNG_PAIR(BGl_string1640z00zzcc_ccz00,
												BgL_arg1216z00_124);
										}
										BgL_arg1212z00_122 =
											MAKE_YOUNG_PAIR(BgL_arg1208z00_119, BgL_arg1215z00_123);
									}
									BgL_list1210z00_121 =
										MAKE_YOUNG_PAIR(BGl_string1648z00zzcc_ccz00,
										BgL_arg1212z00_122);
								}
								BGL_TAIL return
									BGl_stringzd2appendzd2zz__r4_strings_6_7z00
									(BgL_list1210z00_121);
							}
						}
					else
						{	/* Cc/cc.scm 98 */
							obj_t BgL_arg1219z00_126;

							BgL_arg1219z00_126 = CDR(((obj_t) BgL_pathz00_114));
							{
								obj_t BgL_pathz00_759;

								BgL_pathz00_759 = BgL_arg1219z00_126;
								BgL_pathz00_114 = BgL_pathz00_759;
								goto BGl_loopze72ze7zzcc_ccz00;
							}
						}
				}
		}

	}



/* mingw-cc */
	obj_t BGl_mingwzd2cczd2zzcc_ccz00(obj_t BgL_namez00_10, obj_t BgL_onamez00_11,
		bool_t BgL_needzd2tozd2returnz00_12)
	{
		{	/* Cc/cc.scm 127 */
			{	/* Cc/cc.scm 128 */
				obj_t BgL_list1244z00_153;

				{	/* Cc/cc.scm 128 */
					obj_t BgL_arg1248z00_154;

					{	/* Cc/cc.scm 128 */
						obj_t BgL_arg1249z00_155;

						{	/* Cc/cc.scm 128 */
							obj_t BgL_arg1252z00_156;

							BgL_arg1252z00_156 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
							BgL_arg1249z00_155 =
								MAKE_YOUNG_PAIR(BGl_string1635z00zzcc_ccz00,
								BgL_arg1252z00_156);
						}
						BgL_arg1248z00_154 =
							MAKE_YOUNG_PAIR(BGl_za2ccza2z00zzengine_paramz00,
							BgL_arg1249z00_155);
					}
					BgL_list1244z00_153 =
						MAKE_YOUNG_PAIR(BGl_string1636z00zzcc_ccz00, BgL_arg1248z00_154);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1244z00_153);
			}
			if (STRINGP(BgL_namez00_10))
				{	/* Cc/cc.scm 133 */
					obj_t BgL_onamez00_158;

					if (STRINGP(BgL_onamez00_11))
						{	/* Cc/cc.scm 133 */
							BgL_onamez00_158 = BgL_onamez00_11;
						}
					else
						{	/* Cc/cc.scm 133 */
							BgL_onamez00_158 = BgL_namez00_10;
						}
					{	/* Cc/cc.scm 133 */
						obj_t BgL_destzd2objzd2_159;

						if (CBOOL(BGl_za2cczd2moveza2zd2zzengine_paramz00))
							{	/* Cc/cc.scm 134 */
								BgL_destzd2objzd2_159 = BGl_string1638z00zzcc_ccz00;
							}
						else
							{	/* Cc/cc.scm 137 */
								obj_t BgL_arg1370z00_222;

								{	/* Cc/cc.scm 137 */
									obj_t BgL_list1372z00_224;

									{	/* Cc/cc.scm 137 */
										obj_t BgL_arg1375z00_225;

										{	/* Cc/cc.scm 137 */
											obj_t BgL_arg1376z00_226;

											{	/* Cc/cc.scm 137 */
												obj_t BgL_arg1377z00_227;

												BgL_arg1377z00_227 =
													MAKE_YOUNG_PAIR
													(BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00,
													BNIL);
												BgL_arg1376z00_226 =
													MAKE_YOUNG_PAIR(BGl_string1637z00zzcc_ccz00,
													BgL_arg1377z00_227);
											}
											BgL_arg1375z00_225 =
												MAKE_YOUNG_PAIR(BgL_onamez00_158, BgL_arg1376z00_226);
										}
										BgL_list1372z00_224 =
											MAKE_YOUNG_PAIR
											(BGl_za2cczd2ozd2optionza2z00zzengine_paramz00,
											BgL_arg1375z00_225);
									}
									BgL_arg1370z00_222 =
										BGl_stringzd2appendzd2zz__r4_strings_6_7z00
										(BgL_list1372z00_224);
								}
								{	/* Cc/cc.scm 136 */
									obj_t BgL_list1371z00_223;

									BgL_list1371z00_223 =
										MAKE_YOUNG_PAIR(BgL_arg1370z00_222, BNIL);
									BgL_destzd2objzd2_159 =
										BGl_unixzd2filenamezd2zzcc_execz00(BgL_list1371z00_223);
								}
							}
						{	/* Cc/cc.scm 134 */
							obj_t BgL_ccz00_160;

							{	/* Cc/cc.scm 141 */
								obj_t BgL_arg1320z00_189;
								obj_t BgL_arg1321z00_190;
								obj_t BgL_arg1322z00_191;
								obj_t BgL_arg1323z00_192;

								{	/* Cc/cc.scm 141 */
									obj_t BgL_list1343z00_206;

									BgL_list1343z00_206 =
										MAKE_YOUNG_PAIR(BGl_za2cczd2optionsza2zd2zzengine_paramz00,
										BNIL);
									BgL_arg1320z00_189 =
										BGl_formatz00zz__r4_output_6_10_3z00
										(BGl_string1639z00zzcc_ccz00, BgL_list1343z00_206);
								}
								BgL_arg1321z00_190 =
									BGl_loopze71ze7zzcc_ccz00
									(BGl_za2libzd2dirza2zd2zzengine_paramz00);
								{	/* Cc/cc.scm 154 */
									bool_t BgL_test1719z00_783;

									if (CBOOL(BGl_za2czd2debugza2zd2zzengine_paramz00))
										{	/* Cc/cc.scm 154 */
											BgL_test1719z00_783 = ((bool_t) 1);
										}
									else
										{	/* Cc/cc.scm 154 */
											BgL_test1719z00_783 =
												(
												(long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) >
												0L);
										}
									if (BgL_test1719z00_783)
										{	/* Cc/cc.scm 154 */
											BgL_arg1322z00_191 =
												string_append(BGl_string1640z00zzcc_ccz00,
												BGl_za2czd2debugzd2optionza2z00zzengine_paramz00);
										}
									else
										{	/* Cc/cc.scm 154 */
											BgL_arg1322z00_191 = BGl_string1638z00zzcc_ccz00;
										}
								}
								{	/* Cc/cc.scm 157 */
									obj_t BgL_list1366z00_220;

									{	/* Cc/cc.scm 157 */
										obj_t BgL_arg1367z00_221;

										BgL_arg1367z00_221 =
											MAKE_YOUNG_PAIR(BGl_string1641z00zzcc_ccz00, BNIL);
										BgL_list1366z00_220 =
											MAKE_YOUNG_PAIR(BgL_namez00_10, BgL_arg1367z00_221);
									}
									BgL_arg1323z00_192 =
										BGl_unixzd2filenamezd2zzcc_execz00(BgL_list1366z00_220);
								}
								{	/* Cc/cc.scm 139 */
									obj_t BgL_list1324z00_193;

									{	/* Cc/cc.scm 139 */
										obj_t BgL_arg1325z00_194;

										{	/* Cc/cc.scm 139 */
											obj_t BgL_arg1326z00_195;

											{	/* Cc/cc.scm 139 */
												obj_t BgL_arg1327z00_196;

												{	/* Cc/cc.scm 139 */
													obj_t BgL_arg1328z00_197;

													{	/* Cc/cc.scm 139 */
														obj_t BgL_arg1329z00_198;

														{	/* Cc/cc.scm 139 */
															obj_t BgL_arg1331z00_199;

															{	/* Cc/cc.scm 139 */
																obj_t BgL_arg1332z00_200;

																{	/* Cc/cc.scm 139 */
																	obj_t BgL_arg1333z00_201;

																	{	/* Cc/cc.scm 139 */
																		obj_t BgL_arg1335z00_202;

																		{	/* Cc/cc.scm 139 */
																			obj_t BgL_arg1339z00_203;

																			{	/* Cc/cc.scm 139 */
																				obj_t BgL_arg1340z00_204;

																				{	/* Cc/cc.scm 139 */
																					obj_t BgL_arg1342z00_205;

																					BgL_arg1342z00_205 =
																						MAKE_YOUNG_PAIR
																						(BGl_string1640z00zzcc_ccz00, BNIL);
																					BgL_arg1340z00_204 =
																						MAKE_YOUNG_PAIR(BgL_arg1323z00_192,
																						BgL_arg1342z00_205);
																				}
																				BgL_arg1339z00_203 =
																					MAKE_YOUNG_PAIR
																					(BGl_string1640z00zzcc_ccz00,
																					BgL_arg1340z00_204);
																			}
																			BgL_arg1335z00_202 =
																				MAKE_YOUNG_PAIR(BgL_arg1322z00_191,
																				BgL_arg1339z00_203);
																		}
																		BgL_arg1333z00_201 =
																			MAKE_YOUNG_PAIR(BgL_arg1321z00_190,
																			BgL_arg1335z00_202);
																	}
																	BgL_arg1332z00_200 =
																		MAKE_YOUNG_PAIR(BGl_string1642z00zzcc_ccz00,
																		BgL_arg1333z00_201);
																}
																BgL_arg1331z00_199 =
																	MAKE_YOUNG_PAIR(BgL_destzd2objzd2_159,
																	BgL_arg1332z00_200);
															}
															BgL_arg1329z00_198 =
																MAKE_YOUNG_PAIR(BGl_string1643z00zzcc_ccz00,
																BgL_arg1331z00_199);
														}
														BgL_arg1328z00_197 =
															MAKE_YOUNG_PAIR
															(BGl_za2cflagsza2z00zzengine_paramz00,
															BgL_arg1329z00_198);
													}
													BgL_arg1327z00_196 =
														MAKE_YOUNG_PAIR(BGl_string1640z00zzcc_ccz00,
														BgL_arg1328z00_197);
												}
												BgL_arg1326z00_195 =
													MAKE_YOUNG_PAIR(BgL_arg1320z00_189,
													BgL_arg1327z00_196);
											}
											BgL_arg1325z00_194 =
												MAKE_YOUNG_PAIR(BGl_string1640z00zzcc_ccz00,
												BgL_arg1326z00_195);
										}
										BgL_list1324z00_193 =
											MAKE_YOUNG_PAIR(BGl_za2ccza2z00zzengine_paramz00,
											BgL_arg1325z00_194);
									}
									BgL_ccz00_160 =
										BGl_stringzd2appendzd2zz__r4_strings_6_7z00
										(BgL_list1324z00_193);
								}
							}
							{	/* Cc/cc.scm 139 */
								obj_t BgL_basenamez00_161;

								BgL_basenamez00_161 =
									BGl_basenamez00zz__osz00(BgL_onamez00_158);
								{	/* Cc/cc.scm 160 */
									obj_t BgL_cmdz00_164;

									BgL_cmdz00_164 =
										string_append_3(BgL_ccz00_160, BGl_string1638z00zzcc_ccz00,
										BGl_string1638z00zzcc_ccz00);
									{	/* Cc/cc.scm 161 */

										{	/* Cc/cc.scm 162 */
											obj_t BgL_port1054z00_165;

											{	/* Cc/cc.scm 162 */
												obj_t BgL_tmpz00_808;

												BgL_tmpz00_808 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_port1054z00_165 =
													BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_808);
											}
											bgl_display_obj(BgL_cmdz00_164, BgL_port1054z00_165);
											bgl_display_char(((unsigned char) 10),
												BgL_port1054z00_165);
										}
										{	/* Cc/cc.scm 163 */
											obj_t BgL_list1255z00_166;

											{	/* Cc/cc.scm 163 */
												obj_t BgL_arg1268z00_167;

												{	/* Cc/cc.scm 163 */
													obj_t BgL_arg1272z00_168;

													{	/* Cc/cc.scm 163 */
														obj_t BgL_arg1284z00_169;

														BgL_arg1284z00_169 =
															MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)),
															BNIL);
														BgL_arg1272z00_168 =
															MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ']')),
															BgL_arg1284z00_169);
													}
													BgL_arg1268z00_167 =
														MAKE_YOUNG_PAIR(BgL_cmdz00_164, BgL_arg1272z00_168);
												}
												BgL_list1255z00_166 =
													MAKE_YOUNG_PAIR(BGl_string1646z00zzcc_ccz00,
													BgL_arg1268z00_167);
											}
											BGl_verbosez00zztools_speekz00(BINT(2L),
												BgL_list1255z00_166);
										}
										BGl_execz00zzcc_execz00(BgL_cmdz00_164,
											BgL_needzd2tozd2returnz00_12,
											BGl_string1632z00zzcc_ccz00);
										{	/* Cc/cc.scm 165 */
											bool_t BgL_test1721z00_822;

											if (CBOOL(BGl_za2cczd2moveza2zd2zzengine_paramz00))
												{	/* Cc/cc.scm 166 */
													bool_t BgL_test1724z00_825;

													{	/* Cc/cc.scm 166 */
														long BgL_l1z00_400;

														BgL_l1z00_400 = STRING_LENGTH(BgL_basenamez00_161);
														if (
															(BgL_l1z00_400 ==
																STRING_LENGTH(BgL_onamez00_158)))
															{	/* Cc/cc.scm 166 */
																int BgL_arg1282z00_403;

																{	/* Cc/cc.scm 166 */
																	char *BgL_auxz00_832;
																	char *BgL_tmpz00_830;

																	BgL_auxz00_832 =
																		BSTRING_TO_STRING(BgL_onamez00_158);
																	BgL_tmpz00_830 =
																		BSTRING_TO_STRING(BgL_basenamez00_161);
																	BgL_arg1282z00_403 =
																		memcmp(BgL_tmpz00_830, BgL_auxz00_832,
																		BgL_l1z00_400);
																}
																BgL_test1724z00_825 =
																	((long) (BgL_arg1282z00_403) == 0L);
															}
														else
															{	/* Cc/cc.scm 166 */
																BgL_test1724z00_825 = ((bool_t) 0);
															}
													}
													if (BgL_test1724z00_825)
														{	/* Cc/cc.scm 166 */
															BgL_test1721z00_822 = ((bool_t) 0);
														}
													else
														{	/* Cc/cc.scm 167 */
															bool_t BgL_test1726z00_837;

															{	/* Cc/cc.scm 167 */
																obj_t BgL_arg1317z00_186;
																obj_t BgL_arg1318z00_187;

																BgL_arg1317z00_186 = BGl_pwdz00zz__osz00();
																BgL_arg1318z00_187 =
																	BGl_dirnamez00zz__osz00(BgL_onamez00_158);
																{	/* Cc/cc.scm 167 */
																	long BgL_l1z00_411;

																	BgL_l1z00_411 =
																		STRING_LENGTH(((obj_t) BgL_arg1317z00_186));
																	if (
																		(BgL_l1z00_411 ==
																			STRING_LENGTH(BgL_arg1318z00_187)))
																		{	/* Cc/cc.scm 167 */
																			int BgL_arg1282z00_414;

																			{	/* Cc/cc.scm 167 */
																				char *BgL_auxz00_848;
																				char *BgL_tmpz00_845;

																				BgL_auxz00_848 =
																					BSTRING_TO_STRING(BgL_arg1318z00_187);
																				BgL_tmpz00_845 =
																					BSTRING_TO_STRING(
																					((obj_t) BgL_arg1317z00_186));
																				BgL_arg1282z00_414 =
																					memcmp(BgL_tmpz00_845, BgL_auxz00_848,
																					BgL_l1z00_411);
																			}
																			BgL_test1726z00_837 =
																				((long) (BgL_arg1282z00_414) == 0L);
																		}
																	else
																		{	/* Cc/cc.scm 167 */
																			BgL_test1726z00_837 = ((bool_t) 0);
																		}
																}
															}
															if (BgL_test1726z00_837)
																{	/* Cc/cc.scm 167 */
																	BgL_test1721z00_822 = ((bool_t) 0);
																}
															else
																{	/* Cc/cc.scm 168 */
																	bool_t BgL_test1728z00_853;

																	{	/* Cc/cc.scm 168 */
																		obj_t BgL_arg1316z00_185;

																		BgL_arg1316z00_185 =
																			BGl_dirnamez00zz__osz00(BgL_onamez00_158);
																		if (
																			(1L == STRING_LENGTH(BgL_arg1316z00_185)))
																			{	/* Cc/cc.scm 168 */
																				int BgL_arg1282z00_425;

																				{	/* Cc/cc.scm 168 */
																					char *BgL_auxz00_860;
																					char *BgL_tmpz00_858;

																					BgL_auxz00_860 =
																						BSTRING_TO_STRING
																						(BgL_arg1316z00_185);
																					BgL_tmpz00_858 =
																						BSTRING_TO_STRING
																						(BGl_string1637z00zzcc_ccz00);
																					BgL_arg1282z00_425 =
																						memcmp(BgL_tmpz00_858,
																						BgL_auxz00_860, 1L);
																				}
																				BgL_test1728z00_853 =
																					((long) (BgL_arg1282z00_425) == 0L);
																			}
																		else
																			{	/* Cc/cc.scm 168 */
																				BgL_test1728z00_853 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test1728z00_853)
																		{	/* Cc/cc.scm 168 */
																			BgL_test1721z00_822 = ((bool_t) 0);
																		}
																	else
																		{	/* Cc/cc.scm 168 */
																			BgL_test1721z00_822 = ((bool_t) 1);
																		}
																}
														}
												}
											else
												{	/* Cc/cc.scm 165 */
													BgL_test1721z00_822 = ((bool_t) 0);
												}
											if (BgL_test1721z00_822)
												{	/* Cc/cc.scm 169 */
													obj_t BgL_arg1314z00_180;
													obj_t BgL_arg1315z00_181;

													BgL_arg1314z00_180 =
														string_append_3(BgL_basenamez00_161,
														BGl_string1637z00zzcc_ccz00,
														BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00);
													BgL_arg1315z00_181 =
														string_append_3(BgL_onamez00_158,
														BGl_string1637z00zzcc_ccz00,
														BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00);
													{	/* Cc/cc.scm 169 */
														char *BgL_string1z00_431;
														char *BgL_string2z00_432;

														BgL_string1z00_431 =
															BSTRING_TO_STRING(BgL_arg1314z00_180);
														BgL_string2z00_432 =
															BSTRING_TO_STRING(BgL_arg1315z00_181);
														{	/* Cc/cc.scm 169 */
															int BgL_arg1263z00_434;

															BgL_arg1263z00_434 =
																rename(BgL_string1z00_431, BgL_string2z00_432);
															((long) (BgL_arg1263z00_434) == 0L);
												}}}
											else
												{	/* Cc/cc.scm 165 */
													((bool_t) 0);
												}
										}
										if (CBOOL(BGl_za2rmzd2tmpzd2filesza2z00zzengine_paramz00))
											{	/* Cc/cc.scm 172 */
												obj_t BgL_arg1319z00_188;

												BgL_arg1319z00_188 =
													string_append(BgL_namez00_10,
													BGl_string1649z00zzcc_ccz00);
												{	/* Cc/cc.scm 172 */
													char *BgL_stringz00_435;

													BgL_stringz00_435 =
														BSTRING_TO_STRING(BgL_arg1319z00_188);
													if (unlink(BgL_stringz00_435))
														{	/* Cc/cc.scm 172 */
															return BFALSE;
														}
													else
														{	/* Cc/cc.scm 172 */
															return BTRUE;
														}
												}
											}
										else
											{	/* Cc/cc.scm 171 */
												return BFALSE;
											}
									}
								}
							}
						}
					}
				}
			else
				{	/* Cc/cc.scm 130 */
					return
						BGl_errorz00zz__errorz00(BGl_string1632z00zzcc_ccz00,
						BGl_string1647z00zzcc_ccz00, BgL_namez00_10);
				}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zzcc_ccz00(obj_t BgL_pathz00_208)
	{
		{	/* Cc/cc.scm 147 */
			if (NULLP(BgL_pathz00_208))
				{	/* Cc/cc.scm 148 */
					return BGl_string1638z00zzcc_ccz00;
				}
			else
				{	/* Cc/cc.scm 151 */
					obj_t BgL_arg1346z00_211;
					obj_t BgL_arg1348z00_212;

					BgL_arg1346z00_211 = CAR(((obj_t) BgL_pathz00_208));
					{	/* Cc/cc.scm 153 */
						obj_t BgL_arg1364z00_217;

						BgL_arg1364z00_217 = CDR(((obj_t) BgL_pathz00_208));
						BgL_arg1348z00_212 = BGl_loopze71ze7zzcc_ccz00(BgL_arg1364z00_217);
					}
					{	/* Cc/cc.scm 150 */
						obj_t BgL_list1349z00_213;

						{	/* Cc/cc.scm 150 */
							obj_t BgL_arg1351z00_214;

							{	/* Cc/cc.scm 150 */
								obj_t BgL_arg1352z00_215;

								{	/* Cc/cc.scm 150 */
									obj_t BgL_arg1361z00_216;

									BgL_arg1361z00_216 =
										MAKE_YOUNG_PAIR(BgL_arg1348z00_212, BNIL);
									BgL_arg1352z00_215 =
										MAKE_YOUNG_PAIR(BGl_string1640z00zzcc_ccz00,
										BgL_arg1361z00_216);
								}
								BgL_arg1351z00_214 =
									MAKE_YOUNG_PAIR(BgL_arg1346z00_211, BgL_arg1352z00_215);
							}
							BgL_list1349z00_213 =
								MAKE_YOUNG_PAIR(BGl_string1648z00zzcc_ccz00,
								BgL_arg1351z00_214);
						}
						BGL_TAIL return
							BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1349z00_213);
					}
				}
		}

	}



/* win32-cc */
	obj_t BGl_win32zd2cczd2zzcc_ccz00(obj_t BgL_namez00_13, obj_t BgL_onamez00_14)
	{
		{	/* Cc/cc.scm 177 */
			{	/* Cc/cc.scm 178 */
				obj_t BgL_list1383z00_229;

				{	/* Cc/cc.scm 178 */
					obj_t BgL_arg1408z00_230;

					{	/* Cc/cc.scm 178 */
						obj_t BgL_arg1410z00_231;

						{	/* Cc/cc.scm 178 */
							obj_t BgL_arg1421z00_232;

							BgL_arg1421z00_232 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
							BgL_arg1410z00_231 =
								MAKE_YOUNG_PAIR(BGl_string1635z00zzcc_ccz00,
								BgL_arg1421z00_232);
						}
						BgL_arg1408z00_230 =
							MAKE_YOUNG_PAIR(BGl_za2ccza2z00zzengine_paramz00,
							BgL_arg1410z00_231);
					}
					BgL_list1383z00_229 =
						MAKE_YOUNG_PAIR(BGl_string1636z00zzcc_ccz00, BgL_arg1408z00_230);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1383z00_229);
			}
			if (STRINGP(BgL_namez00_13))
				{	/* Cc/cc.scm 183 */
					obj_t BgL_onamez00_234;

					if (STRINGP(BgL_onamez00_14))
						{	/* Cc/cc.scm 183 */
							BgL_onamez00_234 = BgL_onamez00_14;
						}
					else
						{	/* Cc/cc.scm 183 */
							BgL_onamez00_234 = BgL_namez00_13;
						}
					{	/* Cc/cc.scm 183 */
						obj_t BgL_cczd2argszd2_235;

						{	/* Cc/cc.scm 184 */
							obj_t BgL_arg1473z00_245;
							obj_t BgL_arg1485z00_246;
							obj_t BgL_arg1489z00_247;
							obj_t BgL_arg1502z00_248;
							obj_t BgL_arg1509z00_249;
							obj_t BgL_arg1513z00_250;

							BgL_arg1473z00_245 =
								BGl_zc3z04anonymousza31554ze3ze70z60zzcc_ccz00
								(BGl_za2cczd2optionsza2zd2zzengine_paramz00);
							BgL_arg1485z00_246 =
								BGl_stringzd2splitzd2charz00zztools_miscz00
								(BGl_za2cflagsza2z00zzengine_paramz00,
								BCHAR(((unsigned char) ' ')));
							BgL_arg1489z00_247 =
								BGl_loopze70ze7zzcc_ccz00
								(BGl_za2libzd2dirza2zd2zzengine_paramz00);
							{	/* Cc/cc.scm 195 */
								bool_t BgL_test1737z00_906;

								if (CBOOL(BGl_za2czd2debugza2zd2zzengine_paramz00))
									{	/* Cc/cc.scm 195 */
										BgL_test1737z00_906 = ((bool_t) 1);
									}
								else
									{	/* Cc/cc.scm 195 */
										BgL_test1737z00_906 =
											(
											(long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) >
											0L);
									}
								if (BgL_test1737z00_906)
									{	/* Cc/cc.scm 195 */
										BgL_arg1502z00_248 =
											BGl_stringzd2splitzd2charz00zztools_miscz00
											(BGl_za2czd2debugzd2optionza2z00zzengine_paramz00,
											BCHAR(((unsigned char) ' ')));
									}
								else
									{	/* Cc/cc.scm 195 */
										BgL_arg1502z00_248 = BNIL;
									}
							}
							{	/* Cc/cc.scm 198 */
								obj_t BgL_arg1584z00_278;

								BgL_arg1584z00_278 =
									string_append(BgL_namez00_13, BGl_string1641z00zzcc_ccz00);
								{	/* Cc/cc.scm 198 */
									obj_t BgL_list1585z00_279;

									BgL_list1585z00_279 =
										MAKE_YOUNG_PAIR(BgL_arg1584z00_278, BNIL);
									BgL_arg1509z00_249 = BgL_list1585z00_279;
								}
							}
							{	/* Cc/cc.scm 199 */
								bool_t BgL_test1739z00_915;

								{	/* Cc/cc.scm 200 */
									unsigned char BgL_arg1615z00_295;

									{	/* Cc/cc.scm 200 */
										obj_t BgL_arg1616z00_296;

										{	/* Cc/cc.scm 200 */
											long BgL_a1057z00_297;

											BgL_a1057z00_297 =
												STRING_LENGTH
												(BGl_za2cczd2ozd2optionza2z00zzengine_paramz00);
											{	/* Cc/cc.scm 200 */

												{	/* Cc/cc.scm 200 */
													obj_t BgL_za71za7_444;
													obj_t BgL_za72za7_445;

													BgL_za71za7_444 = BINT(BgL_a1057z00_297);
													BgL_za72za7_445 = BINT(1L);
													{	/* Cc/cc.scm 200 */
														obj_t BgL_tmpz00_446;

														BgL_tmpz00_446 = BINT(0L);
														if (BGL_SUBFX_OV(BgL_za71za7_444, BgL_za72za7_445,
																BgL_tmpz00_446))
															{	/* Cc/cc.scm 200 */
																BgL_arg1616z00_296 =
																	bgl_bignum_sub(bgl_long_to_bignum(
																		(long) CINT(BgL_za71za7_444)),
																	bgl_long_to_bignum(
																		(long) CINT(BgL_za72za7_445)));
															}
														else
															{	/* Cc/cc.scm 200 */
																BgL_arg1616z00_296 = BgL_tmpz00_446;
															}
													}
												}
											}
										}
										BgL_arg1615z00_295 =
											STRING_REF(BGl_za2cczd2ozd2optionza2z00zzengine_paramz00,
											(long) CINT(BgL_arg1616z00_296));
									}
									BgL_test1739z00_915 =
										(BgL_arg1615z00_295 == ((unsigned char) ' '));
								}
								if (BgL_test1739z00_915)
									{	/* Cc/cc.scm 203 */
										obj_t BgL_arg1595z00_286;

										BgL_arg1595z00_286 =
											string_append_3(BgL_onamez00_234,
											BGl_string1637z00zzcc_ccz00,
											BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00);
										{	/* Cc/cc.scm 202 */
											obj_t BgL_list1596z00_287;

											{	/* Cc/cc.scm 202 */
												obj_t BgL_arg1602z00_288;

												BgL_arg1602z00_288 =
													MAKE_YOUNG_PAIR(BgL_arg1595z00_286, BNIL);
												BgL_list1596z00_287 =
													MAKE_YOUNG_PAIR
													(BGl_za2cczd2ozd2optionza2z00zzengine_paramz00,
													BgL_arg1602z00_288);
											}
											BgL_arg1513z00_250 = BgL_list1596z00_287;
										}
									}
								else
									{	/* Cc/cc.scm 204 */
										obj_t BgL_arg1605z00_289;

										{	/* Cc/cc.scm 204 */
											obj_t BgL_list1607z00_291;

											{	/* Cc/cc.scm 204 */
												obj_t BgL_arg1609z00_292;

												{	/* Cc/cc.scm 204 */
													obj_t BgL_arg1611z00_293;

													{	/* Cc/cc.scm 204 */
														obj_t BgL_arg1613z00_294;

														BgL_arg1613z00_294 =
															MAKE_YOUNG_PAIR
															(BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00,
															BNIL);
														BgL_arg1611z00_293 =
															MAKE_YOUNG_PAIR(BGl_string1637z00zzcc_ccz00,
															BgL_arg1613z00_294);
													}
													BgL_arg1609z00_292 =
														MAKE_YOUNG_PAIR(BgL_onamez00_234,
														BgL_arg1611z00_293);
												}
												BgL_list1607z00_291 =
													MAKE_YOUNG_PAIR
													(BGl_za2cczd2ozd2optionza2z00zzengine_paramz00,
													BgL_arg1609z00_292);
											}
											BgL_arg1605z00_289 =
												BGl_stringzd2appendzd2zz__r4_strings_6_7z00
												(BgL_list1607z00_291);
										}
										{	/* Cc/cc.scm 204 */
											obj_t BgL_list1606z00_290;

											BgL_list1606z00_290 =
												MAKE_YOUNG_PAIR(BgL_arg1605z00_289, BNIL);
											BgL_arg1513z00_250 = BgL_list1606z00_290;
										}
									}
							}
							{	/* Cc/cc.scm 184 */
								obj_t BgL_list1514z00_251;

								{	/* Cc/cc.scm 184 */
									obj_t BgL_arg1516z00_252;

									{	/* Cc/cc.scm 184 */
										obj_t BgL_arg1535z00_253;

										{	/* Cc/cc.scm 184 */
											obj_t BgL_arg1540z00_254;

											{	/* Cc/cc.scm 184 */
												obj_t BgL_arg1544z00_255;

												{	/* Cc/cc.scm 184 */
													obj_t BgL_arg1546z00_256;

													{	/* Cc/cc.scm 184 */
														obj_t BgL_arg1552z00_257;

														{	/* Cc/cc.scm 184 */
															obj_t BgL_arg1553z00_258;

															BgL_arg1553z00_258 =
																MAKE_YOUNG_PAIR(BgL_arg1513z00_250, BNIL);
															BgL_arg1552z00_257 =
																MAKE_YOUNG_PAIR(BgL_arg1509z00_249,
																BgL_arg1553z00_258);
														}
														BgL_arg1546z00_256 =
															MAKE_YOUNG_PAIR(BgL_arg1502z00_248,
															BgL_arg1552z00_257);
													}
													BgL_arg1544z00_255 =
														MAKE_YOUNG_PAIR(BgL_arg1489z00_247,
														BgL_arg1546z00_256);
												}
												BgL_arg1540z00_254 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
													BgL_arg1544z00_255);
											}
											BgL_arg1535z00_253 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(5), BgL_arg1540z00_254);
										}
										BgL_arg1516z00_252 =
											MAKE_YOUNG_PAIR(BgL_arg1485z00_246, BgL_arg1535z00_253);
									}
									BgL_list1514z00_251 =
										MAKE_YOUNG_PAIR(BgL_arg1473z00_245, BgL_arg1516z00_252);
								}
								BgL_cczd2argszd2_235 =
									BGl_appendz00zz__r4_pairs_and_lists_6_3z00
									(BgL_list1514z00_251);
							}
						}
						{	/* Cc/cc.scm 184 */

							{	/* Cc/cc.scm 205 */
								obj_t BgL_arg1434z00_236;

								BgL_arg1434z00_236 =
									MAKE_YOUNG_PAIR(BGl_za2ccza2z00zzengine_paramz00,
									BgL_cczd2argszd2_235);
								{	/* Cc/cc.scm 205 */
									obj_t BgL_list1435z00_237;

									{	/* Cc/cc.scm 205 */
										obj_t BgL_arg1437z00_238;

										{	/* Cc/cc.scm 205 */
											obj_t BgL_arg1448z00_239;

											BgL_arg1448z00_239 =
												MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
											BgL_arg1437z00_238 =
												MAKE_YOUNG_PAIR(BgL_arg1434z00_236, BgL_arg1448z00_239);
										}
										BgL_list1435z00_237 =
											MAKE_YOUNG_PAIR(BGl_string1650z00zzcc_ccz00,
											BgL_arg1437z00_238);
									}
									BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1435z00_237);
							}}
							{	/* Cc/cc.scm 206 */
								obj_t BgL_runner1456z00_243;

								{	/* Cc/cc.scm 206 */
									obj_t BgL_arg1453z00_240;

									BgL_arg1453z00_240 =
										BGl_appendzd221011zd2zzcc_ccz00(BgL_cczd2argszd2_235,
										CNST_TABLE_REF(6));
									{	/* Cc/cc.scm 206 */
										obj_t BgL_list1454z00_241;

										BgL_list1454z00_241 =
											MAKE_YOUNG_PAIR(BgL_arg1453z00_240, BNIL);
										BgL_runner1456z00_243 =
											BGl_consza2za2zz__r4_pairs_and_lists_6_3z00
											(BGl_za2ccza2z00zzengine_paramz00, BgL_list1454z00_241);
								}}
								{	/* Cc/cc.scm 206 */
									obj_t BgL_aux1455z00_242;

									BgL_aux1455z00_242 = CAR(BgL_runner1456z00_243);
									BgL_runner1456z00_243 = CDR(BgL_runner1456z00_243);
									BGl_runzd2processzd2zz__processz00(BgL_aux1455z00_242,
										BgL_runner1456z00_243);
							}}
							if (CBOOL(BGl_za2rmzd2tmpzd2filesza2z00zzengine_paramz00))
								{	/* Cc/cc.scm 208 */
									obj_t BgL_arg1472z00_244;

									BgL_arg1472z00_244 =
										string_append(BgL_namez00_13, BGl_string1649z00zzcc_ccz00);
									{	/* Cc/cc.scm 208 */
										char *BgL_stringz00_462;

										BgL_stringz00_462 = BSTRING_TO_STRING(BgL_arg1472z00_244);
										if (unlink(BgL_stringz00_462))
											{	/* Cc/cc.scm 208 */
												return BFALSE;
											}
										else
											{	/* Cc/cc.scm 208 */
												return BTRUE;
											}
									}
								}
							else
								{	/* Cc/cc.scm 207 */
									return BFALSE;
								}
						}
					}
				}
			else
				{	/* Cc/cc.scm 180 */
					return
						BGl_errorz00zz__errorz00(BGl_string1632z00zzcc_ccz00,
						BGl_string1647z00zzcc_ccz00, BgL_namez00_13);
				}
		}

	}



/* <@anonymous:1554>~0 */
	obj_t BGl_zc3z04anonymousza31554ze3ze70z60zzcc_ccz00(obj_t BgL_l1056z00_260)
	{
		{	/* Cc/cc.scm 184 */
			if (NULLP(BgL_l1056z00_260))
				{	/* Cc/cc.scm 184 */
					return BNIL;
				}
			else
				{	/* Cc/cc.scm 185 */
					obj_t BgL_arg1559z00_263;
					obj_t BgL_arg1561z00_264;

					{	/* Cc/cc.scm 185 */
						obj_t BgL_oz00_265;

						BgL_oz00_265 = CAR(((obj_t) BgL_l1056z00_260));
						BgL_arg1559z00_263 =
							BGl_stringzd2splitzd2charz00zztools_miscz00(BgL_oz00_265,
							BCHAR(((unsigned char) ' ')));
					}
					{	/* Cc/cc.scm 184 */
						obj_t BgL_arg1564z00_266;

						BgL_arg1564z00_266 = CDR(((obj_t) BgL_l1056z00_260));
						BgL_arg1561z00_264 =
							BGl_zc3z04anonymousza31554ze3ze70z60zzcc_ccz00
							(BgL_arg1564z00_266);
					}
					return bgl_append2(BgL_arg1559z00_263, BgL_arg1561z00_264);
				}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzcc_ccz00(obj_t BgL_pathz00_269)
	{
		{	/* Cc/cc.scm 190 */
			if (NULLP(BgL_pathz00_269))
				{	/* Cc/cc.scm 191 */
					return BNIL;
				}
			else
				{	/* Cc/cc.scm 193 */
					obj_t BgL_arg1571z00_272;
					obj_t BgL_arg1573z00_273;

					{	/* Cc/cc.scm 193 */
						obj_t BgL_arg1575z00_274;

						BgL_arg1575z00_274 = CAR(((obj_t) BgL_pathz00_269));
						BgL_arg1571z00_272 =
							string_append(BGl_string1648z00zzcc_ccz00, BgL_arg1575z00_274);
					}
					{	/* Cc/cc.scm 194 */
						obj_t BgL_arg1576z00_275;

						BgL_arg1576z00_275 = CDR(((obj_t) BgL_pathz00_269));
						BgL_arg1573z00_273 = BGl_loopze70ze7zzcc_ccz00(BgL_arg1576z00_275);
					}
					return MAKE_YOUNG_PAIR(BgL_arg1571z00_272, BgL_arg1573z00_273);
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcc_ccz00(void)
	{
		{	/* Cc/cc.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcc_ccz00(void)
	{
		{	/* Cc/cc.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcc_ccz00(void)
	{
		{	/* Cc/cc.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcc_ccz00(void)
	{
		{	/* Cc/cc.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1651z00zzcc_ccz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1651z00zzcc_ccz00));
			BGl_modulezd2initializa7ationz75zzcc_execz00(367900553L,
				BSTRING_TO_STRING(BGl_string1651z00zzcc_ccz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1651z00zzcc_ccz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1651z00zzcc_ccz00));
			return
				BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1651z00zzcc_ccz00));
		}

	}

#ifdef __cplusplus
}
#endif
