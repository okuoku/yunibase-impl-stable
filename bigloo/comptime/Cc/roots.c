/*===========================================================================*/
/*   (Cc/roots.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cc/roots.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CC_ROOTS_TYPE_DEFINITIONS
#define BGL_CC_ROOTS_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;


#endif													// BGL_CC_ROOTS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzcc_rootsz00 = BUNSPEC;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t BGl_makezd2gczd2rootszd2unitzd2zzcc_rootsz00(void);
	static obj_t BGl_toplevelzd2initzd2zzcc_rootsz00(void);
	extern obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcc_rootsz00(void);
	static obj_t BGl_objectzd2initzd2zzcc_rootsz00(void);
	static obj_t BGl_z62zc3z04anonymousza31134ze3ze5zzcc_rootsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t BGl_gczd2rootszd2emitz00zzcc_rootsz00(obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcc_rootsz00(void);
	static bool_t BGl_iszd2localzd2gczd2rootzf3z21zzcc_rootsz00(obj_t);
	static obj_t
		BGl_emitzd2gczd2registrationz00zzcc_rootsz00(BgL_variablez00_bglt, obj_t);
	extern obj_t BGl_svarz00zzast_varz00;
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcc_rootsz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static obj_t BGl_z62makezd2gczd2rootszd2unitzb0zzcc_rootsz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzcc_rootsz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcc_rootsz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcc_rootsz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzcc_rootsz00(void);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62gczd2rootszd2emitz62zzcc_rootsz00(obj_t, obj_t);
	static obj_t __cnst[4];


	   
		 
		DEFINE_STRING(BGl_string1192z00zzcc_rootsz00,
		BgL_bgl_string1192za700za7za7c1207za7, "/* GC roots registration */\n", 28);
	      DEFINE_STRING(BGl_string1193z00zzcc_rootsz00,
		BgL_bgl_string1193za700za7za7c1208za7,
		"static obj_t bgl_gc_roots_register() {\n", 39);
	      DEFINE_STRING(BGl_string1194z00zzcc_rootsz00,
		BgL_bgl_string1194za700za7za7c1209za7, "#if defined( BGL_GC_ROOTS )\n", 28);
	      DEFINE_STRING(BGl_string1195z00zzcc_rootsz00,
		BgL_bgl_string1195za700za7za7c1210za7,
		"#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))\n",
		107);
	      DEFINE_STRING(BGl_string1196z00zzcc_rootsz00,
		BgL_bgl_string1196za700za7za7c1211za7,
		"void *roots_min = (void*)ULONG_MAX, *roots_max = 0;\n", 52);
	      DEFINE_STRING(BGl_string1197z00zzcc_rootsz00,
		BgL_bgl_string1197za700za7za7c1212za7, "#undef ADD_ROOT\n", 16);
	      DEFINE_STRING(BGl_string1198z00zzcc_rootsz00,
		BgL_bgl_string1198za700za7za7c1213za7,
		"if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );\n",
		73);
	      DEFINE_STRING(BGl_string1199z00zzcc_rootsz00,
		BgL_bgl_string1199za700za7za7c1214za7, "#endif\n", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_gczd2rootszd2emitzd2envzd2zzcc_rootsz00,
		BgL_bgl_za762gcza7d2rootsza7d21215za7,
		BGl_z62gczd2rootszd2emitz62zzcc_rootsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2gczd2rootszd2unitzd2envz00zzcc_rootsz00,
		BgL_bgl_za762makeza7d2gcza7d2r1216za7,
		BGl_z62makezd2gczd2rootszd2unitzb0zzcc_rootsz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1200z00zzcc_rootsz00,
		BgL_bgl_string1200za700za7za7c1217za7, "return BUNSPEC;\n", 16);
	      DEFINE_STRING(BGl_string1201z00zzcc_rootsz00,
		BgL_bgl_string1201za700za7za7c1218za7, "}\n\n", 3);
	      DEFINE_STRING(BGl_string1202z00zzcc_rootsz00,
		BgL_bgl_string1202za700za7za7c1219za7, "ADD_ROOT( (void *)(&", 20);
	      DEFINE_STRING(BGl_string1203z00zzcc_rootsz00,
		BgL_bgl_string1203za700za7za7c1220za7, ") );\n", 5);
	      DEFINE_STRING(BGl_string1204z00zzcc_rootsz00,
		BgL_bgl_string1204za700za7za7c1221za7, "cc_roots", 8);
	      DEFINE_STRING(BGl_string1205z00zzcc_rootsz00,
		BgL_bgl_string1205za700za7za7c1222za7,
		"unit gc-roots begin (begin (pragma::obj :srfi bigloo-c \"bgl_gc_roots_register()\")) ",
		83);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcc_rootsz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcc_rootsz00(long
		BgL_checksumz00_788, char *BgL_fromz00_789)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcc_rootsz00))
				{
					BGl_requirezd2initializa7ationz75zzcc_rootsz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcc_rootsz00();
					BGl_libraryzd2moduleszd2initz00zzcc_rootsz00();
					BGl_cnstzd2initzd2zzcc_rootsz00();
					BGl_importedzd2moduleszd2initz00zzcc_rootsz00();
					return BGl_toplevelzd2initzd2zzcc_rootsz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcc_rootsz00(void)
	{
		{	/* Cc/roots.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "cc_roots");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cc_roots");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cc_roots");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cc_roots");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cc_roots");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "cc_roots");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cc_roots");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cc_roots");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "cc_roots");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "cc_roots");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcc_rootsz00(void)
	{
		{	/* Cc/roots.scm 15 */
			{	/* Cc/roots.scm 15 */
				obj_t BgL_cportz00_777;

				{	/* Cc/roots.scm 15 */
					obj_t BgL_stringz00_784;

					BgL_stringz00_784 = BGl_string1205z00zzcc_rootsz00;
					{	/* Cc/roots.scm 15 */
						obj_t BgL_startz00_785;

						BgL_startz00_785 = BINT(0L);
						{	/* Cc/roots.scm 15 */
							obj_t BgL_endz00_786;

							BgL_endz00_786 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_784)));
							{	/* Cc/roots.scm 15 */

								BgL_cportz00_777 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_784, BgL_startz00_785, BgL_endz00_786);
				}}}}
				{
					long BgL_iz00_778;

					BgL_iz00_778 = 3L;
				BgL_loopz00_779:
					if ((BgL_iz00_778 == -1L))
						{	/* Cc/roots.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cc/roots.scm 15 */
							{	/* Cc/roots.scm 15 */
								obj_t BgL_arg1206z00_780;

								{	/* Cc/roots.scm 15 */

									{	/* Cc/roots.scm 15 */
										obj_t BgL_locationz00_782;

										BgL_locationz00_782 = BBOOL(((bool_t) 0));
										{	/* Cc/roots.scm 15 */

											BgL_arg1206z00_780 =
												BGl_readz00zz__readerz00(BgL_cportz00_777,
												BgL_locationz00_782);
										}
									}
								}
								{	/* Cc/roots.scm 15 */
									int BgL_tmpz00_817;

									BgL_tmpz00_817 = (int) (BgL_iz00_778);
									CNST_TABLE_SET(BgL_tmpz00_817, BgL_arg1206z00_780);
							}}
							{	/* Cc/roots.scm 15 */
								int BgL_auxz00_783;

								BgL_auxz00_783 = (int) ((BgL_iz00_778 - 1L));
								{
									long BgL_iz00_822;

									BgL_iz00_822 = (long) (BgL_auxz00_783);
									BgL_iz00_778 = BgL_iz00_822;
									goto BgL_loopz00_779;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcc_rootsz00(void)
	{
		{	/* Cc/roots.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcc_rootsz00(void)
	{
		{	/* Cc/roots.scm 15 */
			return BUNSPEC;
		}

	}



/* make-gc-roots-unit */
	BGL_EXPORTED_DEF obj_t BGl_makezd2gczd2rootszd2unitzd2zzcc_rootsz00(void)
	{
		{	/* Cc/roots.scm 32 */
			{	/* Cc/roots.scm 35 */
				obj_t BgL_arg1126z00_588;

				{	/* Cc/roots.scm 35 */
					bool_t BgL_test1225z00_825;

					{	/* Cc/roots.scm 35 */
						obj_t BgL_arg1131z00_592;

						BgL_arg1131z00_592 = BGl_thezd2backendzd2zzbackend_backendz00();
						BgL_test1225z00_825 =
							(((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_arg1131z00_592)))->
							BgL_pragmazd2supportzd2);
					}
					if (BgL_test1225z00_825)
						{	/* Cc/roots.scm 35 */
							BgL_arg1126z00_588 = CNST_TABLE_REF(0);
						}
					else
						{	/* Cc/roots.scm 37 */
							obj_t BgL_arg1129z00_591;

							BgL_arg1129z00_591 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
							BgL_arg1126z00_588 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(1), BgL_arg1129z00_591);
						}
				}
				{	/* Cc/roots.scm 33 */
					obj_t BgL_idz00_668;

					BgL_idz00_668 = CNST_TABLE_REF(2);
					{	/* Cc/roots.scm 33 */
						obj_t BgL_newz00_669;

						BgL_newz00_669 = create_struct(CNST_TABLE_REF(3), (int) (5L));
						{	/* Cc/roots.scm 33 */
							int BgL_tmpz00_837;

							BgL_tmpz00_837 = (int) (4L);
							STRUCT_SET(BgL_newz00_669, BgL_tmpz00_837, BFALSE);
						}
						{	/* Cc/roots.scm 33 */
							int BgL_tmpz00_840;

							BgL_tmpz00_840 = (int) (3L);
							STRUCT_SET(BgL_newz00_669, BgL_tmpz00_840, BTRUE);
						}
						{	/* Cc/roots.scm 33 */
							int BgL_tmpz00_843;

							BgL_tmpz00_843 = (int) (2L);
							STRUCT_SET(BgL_newz00_669, BgL_tmpz00_843, BgL_arg1126z00_588);
						}
						{	/* Cc/roots.scm 33 */
							obj_t BgL_auxz00_848;
							int BgL_tmpz00_846;

							BgL_auxz00_848 = BINT(-1L);
							BgL_tmpz00_846 = (int) (1L);
							STRUCT_SET(BgL_newz00_669, BgL_tmpz00_846, BgL_auxz00_848);
						}
						{	/* Cc/roots.scm 33 */
							int BgL_tmpz00_851;

							BgL_tmpz00_851 = (int) (0L);
							STRUCT_SET(BgL_newz00_669, BgL_tmpz00_851, BgL_idz00_668);
						}
						return BgL_newz00_669;
					}
				}
			}
		}

	}



/* &make-gc-roots-unit */
	obj_t BGl_z62makezd2gczd2rootszd2unitzb0zzcc_rootsz00(obj_t BgL_envz00_769)
	{
		{	/* Cc/roots.scm 32 */
			return BGl_makezd2gczd2rootszd2unitzd2zzcc_rootsz00();
		}

	}



/* gc-roots-emit */
	BGL_EXPORTED_DEF obj_t BGl_gczd2rootszd2emitz00zzcc_rootsz00(obj_t
		BgL_portz00_25)
	{
		{	/* Cc/roots.scm 44 */
			bgl_display_string(BGl_string1192z00zzcc_rootsz00, BgL_portz00_25);
			bgl_display_string(BGl_string1193z00zzcc_rootsz00, BgL_portz00_25);
			bgl_display_string(BGl_string1194z00zzcc_rootsz00, BgL_portz00_25);
			bgl_display_string(BGl_string1195z00zzcc_rootsz00, BgL_portz00_25);
			bgl_display_string(BGl_string1196z00zzcc_rootsz00, BgL_portz00_25);
			{	/* Cc/roots.scm 52 */
				obj_t BgL_zc3z04anonymousza31134ze3z87_770;

				BgL_zc3z04anonymousza31134ze3z87_770 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31134ze3ze5zzcc_rootsz00,
					(int) (1L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31134ze3z87_770,
					(int) (0L), BgL_portz00_25);
				BGl_forzd2eachzd2globalz12z12zzast_envz00
					(BgL_zc3z04anonymousza31134ze3z87_770, BNIL);
			}
			bgl_display_string(BGl_string1197z00zzcc_rootsz00, BgL_portz00_25);
			bgl_display_string(BGl_string1198z00zzcc_rootsz00, BgL_portz00_25);
			bgl_display_string(BGl_string1199z00zzcc_rootsz00, BgL_portz00_25);
			bgl_display_string(BGl_string1200z00zzcc_rootsz00, BgL_portz00_25);
			return bgl_display_string(BGl_string1201z00zzcc_rootsz00, BgL_portz00_25);
		}

	}



/* &gc-roots-emit */
	obj_t BGl_z62gczd2rootszd2emitz62zzcc_rootsz00(obj_t BgL_envz00_771,
		obj_t BgL_portz00_772)
	{
		{	/* Cc/roots.scm 44 */
			return BGl_gczd2rootszd2emitz00zzcc_rootsz00(BgL_portz00_772);
		}

	}



/* &<@anonymous:1134> */
	obj_t BGl_z62zc3z04anonymousza31134ze3ze5zzcc_rootsz00(obj_t BgL_envz00_773,
		obj_t BgL_globalz00_775)
	{
		{	/* Cc/roots.scm 51 */
			{	/* Cc/roots.scm 52 */
				obj_t BgL_portz00_774;

				BgL_portz00_774 = ((obj_t) PROCEDURE_REF(BgL_envz00_773, (int) (0L)));
				if (BGl_iszd2localzd2gczd2rootzf3z21zzcc_rootsz00(BgL_globalz00_775))
					{	/* Cc/roots.scm 52 */
						return
							BGl_emitzd2gczd2registrationz00zzcc_rootsz00(
							((BgL_variablez00_bglt) BgL_globalz00_775), BgL_portz00_774);
					}
				else
					{	/* Cc/roots.scm 52 */
						return BFALSE;
					}
			}
		}

	}



/* is-local-gc-root? */
	bool_t BGl_iszd2localzd2gczd2rootzf3z21zzcc_rootsz00(obj_t BgL_globalz00_26)
	{
		{	/* Cc/roots.scm 63 */
			{	/* Cc/roots.scm 64 */
				bool_t BgL_test1227z00_879;

				{	/* Cc/roots.scm 64 */
					BgL_valuez00_bglt BgL_arg1148z00_610;

					BgL_arg1148z00_610 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_globalz00_26))))->BgL_valuez00);
					{	/* Cc/roots.scm 64 */
						obj_t BgL_classz00_696;

						BgL_classz00_696 = BGl_svarz00zzast_varz00;
						{	/* Cc/roots.scm 64 */
							BgL_objectz00_bglt BgL_arg1807z00_698;

							{	/* Cc/roots.scm 64 */
								obj_t BgL_tmpz00_883;

								BgL_tmpz00_883 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg1148z00_610));
								BgL_arg1807z00_698 = (BgL_objectz00_bglt) (BgL_tmpz00_883);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cc/roots.scm 64 */
									long BgL_idxz00_704;

									BgL_idxz00_704 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_698);
									BgL_test1227z00_879 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_704 + 2L)) == BgL_classz00_696);
								}
							else
								{	/* Cc/roots.scm 64 */
									bool_t BgL_res1190z00_729;

									{	/* Cc/roots.scm 64 */
										obj_t BgL_oclassz00_712;

										{	/* Cc/roots.scm 64 */
											obj_t BgL_arg1815z00_720;
											long BgL_arg1816z00_721;

											BgL_arg1815z00_720 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cc/roots.scm 64 */
												long BgL_arg1817z00_722;

												BgL_arg1817z00_722 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_698);
												BgL_arg1816z00_721 = (BgL_arg1817z00_722 - OBJECT_TYPE);
											}
											BgL_oclassz00_712 =
												VECTOR_REF(BgL_arg1815z00_720, BgL_arg1816z00_721);
										}
										{	/* Cc/roots.scm 64 */
											bool_t BgL__ortest_1115z00_713;

											BgL__ortest_1115z00_713 =
												(BgL_classz00_696 == BgL_oclassz00_712);
											if (BgL__ortest_1115z00_713)
												{	/* Cc/roots.scm 64 */
													BgL_res1190z00_729 = BgL__ortest_1115z00_713;
												}
											else
												{	/* Cc/roots.scm 64 */
													long BgL_odepthz00_714;

													{	/* Cc/roots.scm 64 */
														obj_t BgL_arg1804z00_715;

														BgL_arg1804z00_715 = (BgL_oclassz00_712);
														BgL_odepthz00_714 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_715);
													}
													if ((2L < BgL_odepthz00_714))
														{	/* Cc/roots.scm 64 */
															obj_t BgL_arg1802z00_717;

															{	/* Cc/roots.scm 64 */
																obj_t BgL_arg1803z00_718;

																BgL_arg1803z00_718 = (BgL_oclassz00_712);
																BgL_arg1802z00_717 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_718,
																	2L);
															}
															BgL_res1190z00_729 =
																(BgL_arg1802z00_717 == BgL_classz00_696);
														}
													else
														{	/* Cc/roots.scm 64 */
															BgL_res1190z00_729 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1227z00_879 = BgL_res1190z00_729;
								}
						}
					}
				}
				if (BgL_test1227z00_879)
					{	/* Cc/roots.scm 66 */
						bool_t BgL_test1231z00_906;

						if (
							(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_globalz00_26)))->
								BgL_userzf3zf3))
							{	/* Cc/roots.scm 66 */
								if (CBOOL(
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_globalz00_26)))->
											BgL_namez00)))
									{	/* Cc/roots.scm 66 */
										BgL_typez00_bglt BgL_arg1145z00_609;

										BgL_arg1145z00_609 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_globalz00_26)))->
											BgL_typez00);
										BgL_test1231z00_906 =
											BGl_bigloozd2typezf3z21zztype_typez00(BgL_arg1145z00_609);
									}
								else
									{	/* Cc/roots.scm 66 */
										BgL_test1231z00_906 = ((bool_t) 0);
									}
							}
						else
							{	/* Cc/roots.scm 66 */
								BgL_test1231z00_906 = ((bool_t) 0);
							}
						if (BgL_test1231z00_906)
							{	/* Cc/roots.scm 67 */
								obj_t BgL_arg1143z00_606;

								BgL_arg1143z00_606 =
									(((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt) BgL_globalz00_26)))->BgL_modulez00);
								return
									(BgL_arg1143z00_606 == BGl_za2moduleza2z00zzmodule_modulez00);
							}
						else
							{	/* Cc/roots.scm 66 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* Cc/roots.scm 64 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* emit-gc-registration */
	obj_t BGl_emitzd2gczd2registrationz00zzcc_rootsz00(BgL_variablez00_bglt
		BgL_globalz00_27, obj_t BgL_portz00_28)
	{
		{	/* Cc/roots.scm 72 */
			bgl_display_string(BGl_string1202z00zzcc_rootsz00, BgL_portz00_28);
			bgl_display_obj(
				(((BgL_variablez00_bglt) COBJECT(BgL_globalz00_27))->BgL_namez00),
				BgL_portz00_28);
			return bgl_display_string(BGl_string1203z00zzcc_rootsz00, BgL_portz00_28);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcc_rootsz00(void)
	{
		{	/* Cc/roots.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcc_rootsz00(void)
	{
		{	/* Cc/roots.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcc_rootsz00(void)
	{
		{	/* Cc/roots.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcc_rootsz00(void)
	{
		{	/* Cc/roots.scm 15 */
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1204z00zzcc_rootsz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1204z00zzcc_rootsz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1204z00zzcc_rootsz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1204z00zzcc_rootsz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1204z00zzcc_rootsz00));
			return
				BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1204z00zzcc_rootsz00));
		}

	}

#ifdef __cplusplus
}
#endif
