/*===========================================================================*/
/*   (Fail/walk.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Fail/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_FAIL_WALK_TYPE_DEFINITIONS
#define BGL_FAIL_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_FAIL_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2funcall1269z70zzfail_walkz00(obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_z62failzd2walkz12za2zzfail_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2letzd2var1283za2zzfail_walkz00(obj_t, obj_t);
	extern obj_t BGl_locationzd2fullzd2fnamez00zztools_locationz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzfail_walkz00 = BUNSPEC;
	static obj_t BGl_za2errorza2z00zzfail_walkz00 = BUNSPEC;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2kwote1257z70zzfail_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2boxzd2ref1293za2zzfail_walkz00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzfail_walkz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzfail_walkz00(void);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	static obj_t BGl_objectzd2initzd2zzfail_walkz00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2appzd2ly1267za2zzfail_walkz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31331ze3ze5zzfail_walkz00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2atom1255z70zzfail_walkz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzfail_walkz00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2condition1275z70zzfail_walkz00(obj_t, obj_t);
	static obj_t BGl_failzd2nodeza2z12z62zzfail_walkz00(obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_failzd2nodez12zc0zzfail_walkz00(BgL_nodez00_bglt);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62failzd2nodez121252za2zzfail_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2extern1271z70zzfail_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2switch1279z70zzfail_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62failzd2nodez12za2zzfail_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2boxzd2setz121295zb0zzfail_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2makezd2box1291za2zzfail_walkz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzfail_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2var1259z70zzfail_walkz00(obj_t, obj_t);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	extern obj_t BGl_nodez00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2letzd2fun1281za2zzfail_walkz00(obj_t, obj_t);
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_failzd2funz12zc0zzfail_walkz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzfail_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzfail_walkz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzfail_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzfail_walkz00(void);
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2jumpzd2exzd2i1289z70zzfail_walkz00(obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2app1265z70zzfail_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2setzd2exzd2it1287z70zzfail_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2sequence1261z70zzfail_walkz00(obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2cast1285z70zzfail_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2setq1273z70zzfail_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2sync1263z70zzfail_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2fail1277z70zzfail_walkz00(obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_failzd2walkz12zc0zzfail_walkz00(obj_t);
	static obj_t __cnst[9];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_failzd2walkz12zd2envz12zzfail_walkz00,
		BgL_bgl_za762failza7d2walkza711897za7,
		BGl_z62failzd2walkz12za2zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_failzd2nodez12zd2envz12zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711898za7,
		BGl_z62failzd2nodez12za2zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1859z00zzfail_walkz00,
		BgL_bgl_string1859za700za7za7f1899za7, "Fail", 4);
	      DEFINE_STRING(BGl_string1860z00zzfail_walkz00,
		BgL_bgl_string1860za700za7za7f1900za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1862z00zzfail_walkz00,
		BgL_bgl_string1862za700za7za7f1901za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1863z00zzfail_walkz00,
		BgL_bgl_string1863za700za7za7f1902za7, " error", 6);
	      DEFINE_STRING(BGl_string1864z00zzfail_walkz00,
		BgL_bgl_string1864za700za7za7f1903za7, "s", 1);
	      DEFINE_STRING(BGl_string1865z00zzfail_walkz00,
		BgL_bgl_string1865za700za7za7f1904za7, "", 0);
	      DEFINE_STRING(BGl_string1866z00zzfail_walkz00,
		BgL_bgl_string1866za700za7za7f1905za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1867z00zzfail_walkz00,
		BgL_bgl_string1867za700za7za7f1906za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1869z00zzfail_walkz00,
		BgL_bgl_string1869za700za7za7f1907za7, "fail-node!1252", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1861z00zzfail_walkz00,
		BgL_bgl_za762za7c3za704anonymo1908za7,
		BGl_z62zc3z04anonymousza31331ze3ze5zzfail_walkz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1870z00zzfail_walkz00,
		BgL_bgl_string1870za700za7za7f1909za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1872z00zzfail_walkz00,
		BgL_bgl_string1872za700za7za7f1910za7, "fail-node!", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1868z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711911za7,
		BGl_z62failzd2nodez121252za2zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1871z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711912za7,
		BGl_z62failzd2nodez12zd2atom1255z70zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1873z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711913za7,
		BGl_z62failzd2nodez12zd2kwote1257z70zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1874z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711914za7,
		BGl_z62failzd2nodez12zd2var1259z70zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1875z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711915za7,
		BGl_z62failzd2nodez12zd2sequence1261z70zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1876z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711916za7,
		BGl_z62failzd2nodez12zd2sync1263z70zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1877z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711917za7,
		BGl_z62failzd2nodez12zd2app1265z70zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1878z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711918za7,
		BGl_z62failzd2nodez12zd2appzd2ly1267za2zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1879z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711919za7,
		BGl_z62failzd2nodez12zd2funcall1269z70zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1880z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711920za7,
		BGl_z62failzd2nodez12zd2extern1271z70zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1881z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711921za7,
		BGl_z62failzd2nodez12zd2setq1273z70zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1882z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711922za7,
		BGl_z62failzd2nodez12zd2condition1275z70zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1883z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711923za7,
		BGl_z62failzd2nodez12zd2fail1277z70zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1884z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711924za7,
		BGl_z62failzd2nodez12zd2switch1279z70zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1885z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711925za7,
		BGl_z62failzd2nodez12zd2letzd2fun1281za2zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1886z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711926za7,
		BGl_z62failzd2nodez12zd2letzd2var1283za2zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1893z00zzfail_walkz00,
		BgL_bgl_string1893za700za7za7f1927za7, "fail_walk", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1887z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711928za7,
		BGl_z62failzd2nodez12zd2cast1285z70zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1894z00zzfail_walkz00,
		BgL_bgl_string1894za700za7za7f1929za7,
		"value error/location location fail-node!1252 done error __error pass-started ((lambda () (set! *error* (find-global (quote error) (quote __error))))) ",
		150);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1888z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711930za7,
		BGl_z62failzd2nodez12zd2setzd2exzd2it1287z70zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1889z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711931za7,
		BGl_z62failzd2nodez12zd2jumpzd2exzd2i1289z70zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1890z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711932za7,
		BGl_z62failzd2nodez12zd2makezd2box1291za2zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1891z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711933za7,
		BGl_z62failzd2nodez12zd2boxzd2ref1293za2zzfail_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1892z00zzfail_walkz00,
		BgL_bgl_za762failza7d2nodeza711934za7,
		BGl_z62failzd2nodez12zd2boxzd2setz121295zb0zzfail_walkz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzfail_walkz00));
		     ADD_ROOT((void *) (&BGl_za2errorza2z00zzfail_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzfail_walkz00(long
		BgL_checksumz00_2122, char *BgL_fromz00_2123)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzfail_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzfail_walkz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzfail_walkz00();
					BGl_libraryzd2moduleszd2initz00zzfail_walkz00();
					BGl_cnstzd2initzd2zzfail_walkz00();
					BGl_importedzd2moduleszd2initz00zzfail_walkz00();
					BGl_genericzd2initzd2zzfail_walkz00();
					BGl_methodzd2initzd2zzfail_walkz00();
					return BGl_toplevelzd2initzd2zzfail_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzfail_walkz00(void)
	{
		{	/* Fail/walk.scm 18 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "fail_walk");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "fail_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"fail_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "fail_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "fail_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "fail_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "fail_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "fail_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "fail_walk");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "fail_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"fail_walk");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "fail_walk");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "fail_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzfail_walkz00(void)
	{
		{	/* Fail/walk.scm 18 */
			{	/* Fail/walk.scm 18 */
				obj_t BgL_cportz00_2024;

				{	/* Fail/walk.scm 18 */
					obj_t BgL_stringz00_2031;

					BgL_stringz00_2031 = BGl_string1894z00zzfail_walkz00;
					{	/* Fail/walk.scm 18 */
						obj_t BgL_startz00_2032;

						BgL_startz00_2032 = BINT(0L);
						{	/* Fail/walk.scm 18 */
							obj_t BgL_endz00_2033;

							BgL_endz00_2033 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2031)));
							{	/* Fail/walk.scm 18 */

								BgL_cportz00_2024 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2031, BgL_startz00_2032, BgL_endz00_2033);
				}}}}
				{
					long BgL_iz00_2025;

					BgL_iz00_2025 = 8L;
				BgL_loopz00_2026:
					if ((BgL_iz00_2025 == -1L))
						{	/* Fail/walk.scm 18 */
							return BUNSPEC;
						}
					else
						{	/* Fail/walk.scm 18 */
							{	/* Fail/walk.scm 18 */
								obj_t BgL_arg1896z00_2027;

								{	/* Fail/walk.scm 18 */

									{	/* Fail/walk.scm 18 */
										obj_t BgL_locationz00_2029;

										BgL_locationz00_2029 = BBOOL(((bool_t) 0));
										{	/* Fail/walk.scm 18 */

											BgL_arg1896z00_2027 =
												BGl_readz00zz__readerz00(BgL_cportz00_2024,
												BgL_locationz00_2029);
										}
									}
								}
								{	/* Fail/walk.scm 18 */
									int BgL_tmpz00_2156;

									BgL_tmpz00_2156 = (int) (BgL_iz00_2025);
									CNST_TABLE_SET(BgL_tmpz00_2156, BgL_arg1896z00_2027);
							}}
							{	/* Fail/walk.scm 18 */
								int BgL_auxz00_2030;

								BgL_auxz00_2030 = (int) ((BgL_iz00_2025 - 1L));
								{
									long BgL_iz00_2161;

									BgL_iz00_2161 = (long) (BgL_auxz00_2030);
									BgL_iz00_2025 = BgL_iz00_2161;
									goto BgL_loopz00_2026;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzfail_walkz00(void)
	{
		{	/* Fail/walk.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzfail_walkz00(void)
	{
		{	/* Fail/walk.scm 18 */
			BGl_za2errorza2z00zzfail_walkz00 = BUNSPEC;
			return BUNSPEC;
		}

	}



/* fail-walk! */
	BGL_EXPORTED_DEF obj_t BGl_failzd2walkz12zc0zzfail_walkz00(obj_t
		BgL_globalsz00_17)
	{
		{	/* Fail/walk.scm 39 */
			{	/* Fail/walk.scm 40 */
				obj_t BgL_list1318z00_1403;

				{	/* Fail/walk.scm 40 */
					obj_t BgL_arg1319z00_1404;

					{	/* Fail/walk.scm 40 */
						obj_t BgL_arg1320z00_1405;

						BgL_arg1320z00_1405 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1319z00_1404 =
							MAKE_YOUNG_PAIR(BGl_string1859z00zzfail_walkz00,
							BgL_arg1320z00_1405);
					}
					BgL_list1318z00_1403 =
						MAKE_YOUNG_PAIR(BGl_string1860z00zzfail_walkz00,
						BgL_arg1319z00_1404);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1318z00_1403);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1859z00zzfail_walkz00;
			{	/* Fail/walk.scm 40 */
				obj_t BgL_g1110z00_1406;
				obj_t BgL_g1111z00_1407;

				{	/* Fail/walk.scm 40 */
					obj_t BgL_list1330z00_1421;

					BgL_list1330z00_1421 =
						MAKE_YOUNG_PAIR(BGl_proc1861z00zzfail_walkz00, BNIL);
					BgL_g1110z00_1406 = BgL_list1330z00_1421;
				}
				BgL_g1111z00_1407 = CNST_TABLE_REF(0);
				{
					obj_t BgL_hooksz00_1409;
					obj_t BgL_hnamesz00_1410;

					BgL_hooksz00_1409 = BgL_g1110z00_1406;
					BgL_hnamesz00_1410 = BgL_g1111z00_1407;
				BgL_zc3z04anonymousza31321ze3z87_1411:
					if (NULLP(BgL_hooksz00_1409))
						{	/* Fail/walk.scm 40 */
							CNST_TABLE_REF(1);
						}
					else
						{	/* Fail/walk.scm 40 */
							bool_t BgL_test1938z00_2176;

							{	/* Fail/walk.scm 40 */
								obj_t BgL_fun1328z00_1418;

								BgL_fun1328z00_1418 = CAR(((obj_t) BgL_hooksz00_1409));
								BgL_test1938z00_2176 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1328z00_1418));
							}
							if (BgL_test1938z00_2176)
								{	/* Fail/walk.scm 40 */
									obj_t BgL_arg1325z00_1415;
									obj_t BgL_arg1326z00_1416;

									BgL_arg1325z00_1415 = CDR(((obj_t) BgL_hooksz00_1409));
									BgL_arg1326z00_1416 = CDR(((obj_t) BgL_hnamesz00_1410));
									{
										obj_t BgL_hnamesz00_2188;
										obj_t BgL_hooksz00_2187;

										BgL_hooksz00_2187 = BgL_arg1325z00_1415;
										BgL_hnamesz00_2188 = BgL_arg1326z00_1416;
										BgL_hnamesz00_1410 = BgL_hnamesz00_2188;
										BgL_hooksz00_1409 = BgL_hooksz00_2187;
										goto BgL_zc3z04anonymousza31321ze3z87_1411;
									}
								}
							else
								{	/* Fail/walk.scm 40 */
									obj_t BgL_arg1327z00_1417;

									BgL_arg1327z00_1417 = CAR(((obj_t) BgL_hnamesz00_1410));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1859z00zzfail_walkz00,
										BGl_string1862z00zzfail_walkz00, BgL_arg1327z00_1417);
								}
						}
				}
			}
			{
				obj_t BgL_l1240z00_1426;

				BgL_l1240z00_1426 = BgL_globalsz00_17;
			BgL_zc3z04anonymousza31333ze3z87_1427:
				if (PAIRP(BgL_l1240z00_1426))
					{	/* Fail/walk.scm 43 */
						BGl_failzd2funz12zc0zzfail_walkz00(CAR(BgL_l1240z00_1426));
						{
							obj_t BgL_l1240z00_2196;

							BgL_l1240z00_2196 = CDR(BgL_l1240z00_1426);
							BgL_l1240z00_1426 = BgL_l1240z00_2196;
							goto BgL_zc3z04anonymousza31333ze3z87_1427;
						}
					}
				else
					{	/* Fail/walk.scm 43 */
						((bool_t) 1);
					}
			}
			if (
				((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) > 0L))
				{	/* Fail/walk.scm 44 */
					{	/* Fail/walk.scm 44 */
						obj_t BgL_port1242z00_1434;

						{	/* Fail/walk.scm 44 */
							obj_t BgL_tmpz00_2201;

							BgL_tmpz00_2201 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1242z00_1434 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2201);
						}
						bgl_display_obj(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
							BgL_port1242z00_1434);
						bgl_display_string(BGl_string1863z00zzfail_walkz00,
							BgL_port1242z00_1434);
						{	/* Fail/walk.scm 44 */
							obj_t BgL_arg1342z00_1435;

							{	/* Fail/walk.scm 44 */
								bool_t BgL_test1941z00_2206;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
									{	/* Fail/walk.scm 44 */
										if (INTEGERP
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Fail/walk.scm 44 */
												BgL_test1941z00_2206 =
													(
													(long)
													CINT
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
													> 1L);
											}
										else
											{	/* Fail/walk.scm 44 */
												BgL_test1941z00_2206 =
													BGl_2ze3ze3zz__r4_numbers_6_5z00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BINT(1L));
											}
									}
								else
									{	/* Fail/walk.scm 44 */
										BgL_test1941z00_2206 = ((bool_t) 0);
									}
								if (BgL_test1941z00_2206)
									{	/* Fail/walk.scm 44 */
										BgL_arg1342z00_1435 = BGl_string1864z00zzfail_walkz00;
									}
								else
									{	/* Fail/walk.scm 44 */
										BgL_arg1342z00_1435 = BGl_string1865z00zzfail_walkz00;
									}
							}
							bgl_display_obj(BgL_arg1342z00_1435, BgL_port1242z00_1434);
						}
						bgl_display_string(BGl_string1866z00zzfail_walkz00,
							BgL_port1242z00_1434);
						bgl_display_char(((unsigned char) 10), BgL_port1242z00_1434);
					}
					{	/* Fail/walk.scm 44 */
						obj_t BgL_list1346z00_1439;

						BgL_list1346z00_1439 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
						BGL_TAIL return BGl_exitz00zz__errorz00(BgL_list1346z00_1439);
					}
				}
			else
				{	/* Fail/walk.scm 44 */
					obj_t BgL_g1112z00_1440;

					BgL_g1112z00_1440 = BNIL;
					{
						obj_t BgL_hooksz00_1443;
						obj_t BgL_hnamesz00_1444;

						BgL_hooksz00_1443 = BgL_g1112z00_1440;
						BgL_hnamesz00_1444 = BNIL;
					BgL_zc3z04anonymousza31347ze3z87_1445:
						if (NULLP(BgL_hooksz00_1443))
							{	/* Fail/walk.scm 44 */
								return BgL_globalsz00_17;
							}
						else
							{	/* Fail/walk.scm 44 */
								bool_t BgL_test1945z00_2223;

								{	/* Fail/walk.scm 44 */
									obj_t BgL_fun1362z00_1452;

									BgL_fun1362z00_1452 = CAR(((obj_t) BgL_hooksz00_1443));
									BgL_test1945z00_2223 =
										CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1362z00_1452));
								}
								if (BgL_test1945z00_2223)
									{	/* Fail/walk.scm 44 */
										obj_t BgL_arg1351z00_1449;
										obj_t BgL_arg1352z00_1450;

										BgL_arg1351z00_1449 = CDR(((obj_t) BgL_hooksz00_1443));
										BgL_arg1352z00_1450 = CDR(((obj_t) BgL_hnamesz00_1444));
										{
											obj_t BgL_hnamesz00_2235;
											obj_t BgL_hooksz00_2234;

											BgL_hooksz00_2234 = BgL_arg1351z00_1449;
											BgL_hnamesz00_2235 = BgL_arg1352z00_1450;
											BgL_hnamesz00_1444 = BgL_hnamesz00_2235;
											BgL_hooksz00_1443 = BgL_hooksz00_2234;
											goto BgL_zc3z04anonymousza31347ze3z87_1445;
										}
									}
								else
									{	/* Fail/walk.scm 44 */
										obj_t BgL_arg1361z00_1451;

										BgL_arg1361z00_1451 = CAR(((obj_t) BgL_hnamesz00_1444));
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_za2currentzd2passza2zd2zzengine_passz00,
											BGl_string1867z00zzfail_walkz00, BgL_arg1361z00_1451);
									}
							}
					}
				}
		}

	}



/* &fail-walk! */
	obj_t BGl_z62failzd2walkz12za2zzfail_walkz00(obj_t BgL_envz00_1952,
		obj_t BgL_globalsz00_1953)
	{
		{	/* Fail/walk.scm 39 */
			return BGl_failzd2walkz12zc0zzfail_walkz00(BgL_globalsz00_1953);
		}

	}



/* &<@anonymous:1331> */
	obj_t BGl_z62zc3z04anonymousza31331ze3ze5zzfail_walkz00(obj_t BgL_envz00_1954)
	{
		{	/* Fail/walk.scm 41 */
			{	/* Fail/walk.scm 42 */
				obj_t BgL_list1332z00_2035;

				BgL_list1332z00_2035 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BNIL);
				return (BGl_za2errorza2z00zzfail_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(3),
						BgL_list1332z00_2035), BUNSPEC);
			}
		}

	}



/* fail-fun! */
	obj_t BGl_failzd2funz12zc0zzfail_walkz00(obj_t BgL_varz00_18)
	{
		{	/* Fail/walk.scm 49 */
			{	/* Fail/walk.scm 51 */
				BgL_valuez00_bglt BgL_funz00_1455;

				BgL_funz00_1455 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_18)))->BgL_valuez00);
				{	/* Fail/walk.scm 51 */
					obj_t BgL_bodyz00_1456;

					BgL_bodyz00_1456 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1455)))->BgL_bodyz00);
					{	/* Fail/walk.scm 53 */

						{	/* Fail/walk.scm 54 */
							obj_t BgL_arg1364z00_1458;

							BgL_arg1364z00_1458 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_varz00_18)))->BgL_idz00);
							BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1364z00_1458);
						}
						{	/* Fail/walk.scm 55 */
							BgL_nodez00_bglt BgL_arg1367z00_1459;

							BgL_arg1367z00_1459 =
								BGl_failzd2nodez12zc0zzfail_walkz00(
								((BgL_nodez00_bglt) BgL_bodyz00_1456));
							((((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt) BgL_funz00_1455)))->BgL_bodyz00) =
								((obj_t) ((obj_t) BgL_arg1367z00_1459)), BUNSPEC);
						}
						return BGl_leavezd2functionzd2zztools_errorz00();
					}
				}
			}
		}

	}



/* fail-node*! */
	obj_t BGl_failzd2nodeza2z12z62zzfail_walkz00(obj_t BgL_nodeza2za2_41)
	{
		{	/* Fail/walk.scm 256 */
		BGl_failzd2nodeza2z12z62zzfail_walkz00:
			if (NULLP(BgL_nodeza2za2_41))
				{	/* Fail/walk.scm 257 */
					return CNST_TABLE_REF(4);
				}
			else
				{	/* Fail/walk.scm 257 */
					{	/* Fail/walk.scm 260 */
						BgL_nodez00_bglt BgL_arg1370z00_1461;

						{	/* Fail/walk.scm 260 */
							obj_t BgL_arg1371z00_1462;

							BgL_arg1371z00_1462 = CAR(((obj_t) BgL_nodeza2za2_41));
							BgL_arg1370z00_1461 =
								BGl_failzd2nodez12zc0zzfail_walkz00(
								((BgL_nodez00_bglt) BgL_arg1371z00_1462));
						}
						{	/* Fail/walk.scm 260 */
							obj_t BgL_auxz00_2266;
							obj_t BgL_tmpz00_2264;

							BgL_auxz00_2266 = ((obj_t) BgL_arg1370z00_1461);
							BgL_tmpz00_2264 = ((obj_t) BgL_nodeza2za2_41);
							SET_CAR(BgL_tmpz00_2264, BgL_auxz00_2266);
						}
					}
					{	/* Fail/walk.scm 261 */
						obj_t BgL_arg1375z00_1463;

						BgL_arg1375z00_1463 = CDR(((obj_t) BgL_nodeza2za2_41));
						{
							obj_t BgL_nodeza2za2_2271;

							BgL_nodeza2za2_2271 = BgL_arg1375z00_1463;
							BgL_nodeza2za2_41 = BgL_nodeza2za2_2271;
							goto BGl_failzd2nodeza2z12z62zzfail_walkz00;
						}
					}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzfail_walkz00(void)
	{
		{	/* Fail/walk.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzfail_walkz00(void)
	{
		{	/* Fail/walk.scm 18 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00,
				BGl_proc1868z00zzfail_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string1869z00zzfail_walkz00);
		}

	}



/* &fail-node!1252 */
	obj_t BGl_z62failzd2nodez121252za2zzfail_walkz00(obj_t BgL_envz00_1956,
		obj_t BgL_nodez00_1957)
	{
		{	/* Fail/walk.scm 61 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(5),
				BGl_string1870z00zzfail_walkz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_1957)));
		}

	}



/* fail-node! */
	BgL_nodez00_bglt BGl_failzd2nodez12zc0zzfail_walkz00(BgL_nodez00_bglt
		BgL_nodez00_19)
	{
		{	/* Fail/walk.scm 61 */
			{	/* Fail/walk.scm 61 */
				obj_t BgL_method1253z00_1468;

				{	/* Fail/walk.scm 61 */
					obj_t BgL_res1856z00_1893;

					{	/* Fail/walk.scm 61 */
						long BgL_objzd2classzd2numz00_1864;

						BgL_objzd2classzd2numz00_1864 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_19));
						{	/* Fail/walk.scm 61 */
							obj_t BgL_arg1811z00_1865;

							BgL_arg1811z00_1865 =
								PROCEDURE_REF(BGl_failzd2nodez12zd2envz12zzfail_walkz00,
								(int) (1L));
							{	/* Fail/walk.scm 61 */
								int BgL_offsetz00_1868;

								BgL_offsetz00_1868 = (int) (BgL_objzd2classzd2numz00_1864);
								{	/* Fail/walk.scm 61 */
									long BgL_offsetz00_1869;

									BgL_offsetz00_1869 =
										((long) (BgL_offsetz00_1868) - OBJECT_TYPE);
									{	/* Fail/walk.scm 61 */
										long BgL_modz00_1870;

										BgL_modz00_1870 =
											(BgL_offsetz00_1869 >> (int) ((long) ((int) (4L))));
										{	/* Fail/walk.scm 61 */
											long BgL_restz00_1872;

											BgL_restz00_1872 =
												(BgL_offsetz00_1869 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Fail/walk.scm 61 */

												{	/* Fail/walk.scm 61 */
													obj_t BgL_bucketz00_1874;

													BgL_bucketz00_1874 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1865), BgL_modz00_1870);
													BgL_res1856z00_1893 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1874), BgL_restz00_1872);
					}}}}}}}}
					BgL_method1253z00_1468 = BgL_res1856z00_1893;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1253z00_1468,
						((obj_t) BgL_nodez00_19)));
			}
		}

	}



/* &fail-node! */
	BgL_nodez00_bglt BGl_z62failzd2nodez12za2zzfail_walkz00(obj_t BgL_envz00_1958,
		obj_t BgL_nodez00_1959)
	{
		{	/* Fail/walk.scm 61 */
			return
				BGl_failzd2nodez12zc0zzfail_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_1959));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzfail_walkz00(void)
	{
		{	/* Fail/walk.scm 18 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00, BGl_atomz00zzast_nodez00,
				BGl_proc1871z00zzfail_walkz00, BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00, BGl_kwotez00zzast_nodez00,
				BGl_proc1873z00zzfail_walkz00, BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00, BGl_varz00zzast_nodez00,
				BGl_proc1874z00zzfail_walkz00, BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1875z00zzfail_walkz00,
				BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc1876z00zzfail_walkz00, BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc1877z00zzfail_walkz00, BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1878z00zzfail_walkz00,
				BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc1879z00zzfail_walkz00, BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc1880z00zzfail_walkz00, BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc1881z00zzfail_walkz00, BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1882z00zzfail_walkz00,
				BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc1883z00zzfail_walkz00, BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc1884z00zzfail_walkz00, BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1885z00zzfail_walkz00,
				BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1886z00zzfail_walkz00,
				BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc1887z00zzfail_walkz00, BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1888z00zzfail_walkz00,
				BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1889z00zzfail_walkz00,
				BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1890z00zzfail_walkz00,
				BGl_string1872z00zzfail_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1891z00zzfail_walkz00,
				BGl_string1872z00zzfail_walkz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failzd2nodez12zd2envz12zzfail_walkz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1892z00zzfail_walkz00,
				BGl_string1872z00zzfail_walkz00);
		}

	}



/* &fail-node!-box-set!1295 */
	BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2boxzd2setz121295zb0zzfail_walkz00(obj_t
		BgL_envz00_1981, obj_t BgL_nodez00_1982)
	{
		{	/* Fail/walk.scm 248 */
			{
				BgL_nodez00_bglt BgL_auxz00_2331;

				{	/* Fail/walk.scm 250 */
					BgL_nodez00_bglt BgL_arg1720z00_2038;

					BgL_arg1720z00_2038 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_1982)))->BgL_valuez00);
					BgL_auxz00_2331 =
						BGl_failzd2nodez12zc0zzfail_walkz00(BgL_arg1720z00_2038);
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_1982)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_2331), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_1982));
		}

	}



/* &fail-node!-box-ref1293 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2boxzd2ref1293za2zzfail_walkz00(obj_t
		BgL_envz00_1983, obj_t BgL_nodez00_1984)
	{
		{	/* Fail/walk.scm 242 */
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_1984));
		}

	}



/* &fail-node!-make-box1291 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2makezd2box1291za2zzfail_walkz00(obj_t
		BgL_envz00_1985, obj_t BgL_nodez00_1986)
	{
		{	/* Fail/walk.scm 235 */
			((((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_1986)))->BgL_valuez00) =
				((BgL_nodez00_bglt)
					BGl_failzd2nodez12zc0zzfail_walkz00((((BgL_makezd2boxzd2_bglt)
								COBJECT(((BgL_makezd2boxzd2_bglt) BgL_nodez00_1986)))->
							BgL_valuez00))), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_1986));
		}

	}



/* &fail-node!-jump-ex-i1289 */
	BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2jumpzd2exzd2i1289z70zzfail_walkz00(obj_t
		BgL_envz00_1987, obj_t BgL_nodez00_1988)
	{
		{	/* Fail/walk.scm 226 */
			{
				BgL_nodez00_bglt BgL_auxz00_2348;

				{	/* Fail/walk.scm 228 */
					BgL_nodez00_bglt BgL_arg1711z00_2042;

					BgL_arg1711z00_2042 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_1988)))->BgL_exitz00);
					BgL_auxz00_2348 =
						BGl_failzd2nodez12zc0zzfail_walkz00(BgL_arg1711z00_2042);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_1988)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_2348), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2354;

				{	/* Fail/walk.scm 229 */
					BgL_nodez00_bglt BgL_arg1714z00_2043;

					BgL_arg1714z00_2043 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_1988)))->
						BgL_valuez00);
					BgL_auxz00_2354 =
						BGl_failzd2nodez12zc0zzfail_walkz00(BgL_arg1714z00_2043);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_1988)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_2354), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_1988));
		}

	}



/* &fail-node!-set-ex-it1287 */
	BgL_nodez00_bglt
		BGl_z62failzd2nodez12zd2setzd2exzd2it1287z70zzfail_walkz00(obj_t
		BgL_envz00_1989, obj_t BgL_nodez00_1990)
	{
		{	/* Fail/walk.scm 218 */
			((((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_1990)))->BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_failzd2nodez12zc0zzfail_walkz00((((BgL_setzd2exzd2itz00_bglt)
								COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_1990)))->
							BgL_bodyz00))), BUNSPEC);
			((((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
								BgL_nodez00_1990)))->BgL_onexitz00) =
				((BgL_nodez00_bglt)
					BGl_failzd2nodez12zc0zzfail_walkz00((((BgL_setzd2exzd2itz00_bglt)
								COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_1990)))->
							BgL_onexitz00))), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt)
					BgL_nodez00_1990));
		}

	}



/* &fail-node!-cast1285 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2cast1285z70zzfail_walkz00(obj_t
		BgL_envz00_1991, obj_t BgL_nodez00_1992)
	{
		{	/* Fail/walk.scm 210 */
			{
				BgL_nodez00_bglt BgL_auxz00_2374;

				{	/* Fail/walk.scm 212 */
					BgL_nodez00_bglt BgL_arg1703z00_2046;

					BgL_arg1703z00_2046 =
						(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_1992)))->BgL_argz00);
					BgL_auxz00_2374 =
						BGl_failzd2nodez12zc0zzfail_walkz00(BgL_arg1703z00_2046);
				}
				((((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_1992)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2374), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_1992));
		}

	}



/* &fail-node!-let-var1283 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2letzd2var1283za2zzfail_walkz00(obj_t
		BgL_envz00_1993, obj_t BgL_nodez00_1994)
	{
		{	/* Fail/walk.scm 199 */
			{	/* Fail/walk.scm 201 */
				obj_t BgL_g1251z00_2048;

				BgL_g1251z00_2048 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_1994)))->BgL_bindingsz00);
				{
					obj_t BgL_l1249z00_2050;

					BgL_l1249z00_2050 = BgL_g1251z00_2048;
				BgL_zc3z04anonymousza31692ze3z87_2049:
					if (PAIRP(BgL_l1249z00_2050))
						{	/* Fail/walk.scm 201 */
							{	/* Fail/walk.scm 202 */
								obj_t BgL_bindingz00_2051;

								BgL_bindingz00_2051 = CAR(BgL_l1249z00_2050);
								{	/* Fail/walk.scm 202 */
									BgL_nodez00_bglt BgL_arg1699z00_2052;

									{	/* Fail/walk.scm 202 */
										obj_t BgL_arg1700z00_2053;

										BgL_arg1700z00_2053 = CDR(((obj_t) BgL_bindingz00_2051));
										BgL_arg1699z00_2052 =
											BGl_failzd2nodez12zc0zzfail_walkz00(
											((BgL_nodez00_bglt) BgL_arg1700z00_2053));
									}
									{	/* Fail/walk.scm 202 */
										obj_t BgL_auxz00_2393;
										obj_t BgL_tmpz00_2391;

										BgL_auxz00_2393 = ((obj_t) BgL_arg1699z00_2052);
										BgL_tmpz00_2391 = ((obj_t) BgL_bindingz00_2051);
										SET_CDR(BgL_tmpz00_2391, BgL_auxz00_2393);
									}
								}
							}
							{
								obj_t BgL_l1249z00_2396;

								BgL_l1249z00_2396 = CDR(BgL_l1249z00_2050);
								BgL_l1249z00_2050 = BgL_l1249z00_2396;
								goto BgL_zc3z04anonymousza31692ze3z87_2049;
							}
						}
					else
						{	/* Fail/walk.scm 201 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2398;

				{	/* Fail/walk.scm 204 */
					BgL_nodez00_bglt BgL_arg1702z00_2054;

					BgL_arg1702z00_2054 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_1994)))->BgL_bodyz00);
					BgL_auxz00_2398 =
						BGl_failzd2nodez12zc0zzfail_walkz00(BgL_arg1702z00_2054);
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_1994)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2398), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_1994));
		}

	}



/* &fail-node!-let-fun1281 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2letzd2fun1281za2zzfail_walkz00(obj_t
		BgL_envz00_1995, obj_t BgL_nodez00_1996)
	{
		{	/* Fail/walk.scm 190 */
			{	/* Fail/walk.scm 192 */
				obj_t BgL_g1248z00_2056;

				BgL_g1248z00_2056 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_1996)))->BgL_localsz00);
				{
					obj_t BgL_l1246z00_2058;

					BgL_l1246z00_2058 = BgL_g1248z00_2056;
				BgL_zc3z04anonymousza31682ze3z87_2057:
					if (PAIRP(BgL_l1246z00_2058))
						{	/* Fail/walk.scm 192 */
							BGl_failzd2funz12zc0zzfail_walkz00(CAR(BgL_l1246z00_2058));
							{
								obj_t BgL_l1246z00_2412;

								BgL_l1246z00_2412 = CDR(BgL_l1246z00_2058);
								BgL_l1246z00_2058 = BgL_l1246z00_2412;
								goto BgL_zc3z04anonymousza31682ze3z87_2057;
							}
						}
					else
						{	/* Fail/walk.scm 192 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2414;

				{	/* Fail/walk.scm 193 */
					BgL_nodez00_bglt BgL_arg1691z00_2059;

					BgL_arg1691z00_2059 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_1996)))->BgL_bodyz00);
					BgL_auxz00_2414 =
						BGl_failzd2nodez12zc0zzfail_walkz00(BgL_arg1691z00_2059);
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_1996)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2414), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_1996));
		}

	}



/* &fail-node!-switch1279 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2switch1279z70zzfail_walkz00(obj_t
		BgL_envz00_1997, obj_t BgL_nodez00_1998)
	{
		{	/* Fail/walk.scm 179 */
			{
				BgL_nodez00_bglt BgL_auxz00_2422;

				{	/* Fail/walk.scm 181 */
					BgL_nodez00_bglt BgL_arg1663z00_2061;

					BgL_arg1663z00_2061 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_1998)))->BgL_testz00);
					BgL_auxz00_2422 =
						BGl_failzd2nodez12zc0zzfail_walkz00(BgL_arg1663z00_2061);
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_1998)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2422), BUNSPEC);
			}
			{	/* Fail/walk.scm 182 */
				obj_t BgL_g1245z00_2062;

				BgL_g1245z00_2062 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_1998)))->BgL_clausesz00);
				{
					obj_t BgL_l1243z00_2064;

					BgL_l1243z00_2064 = BgL_g1245z00_2062;
				BgL_zc3z04anonymousza31664ze3z87_2063:
					if (PAIRP(BgL_l1243z00_2064))
						{	/* Fail/walk.scm 182 */
							{	/* Fail/walk.scm 183 */
								obj_t BgL_clausez00_2065;

								BgL_clausez00_2065 = CAR(BgL_l1243z00_2064);
								{	/* Fail/walk.scm 183 */
									BgL_nodez00_bglt BgL_arg1675z00_2066;

									{	/* Fail/walk.scm 183 */
										obj_t BgL_arg1678z00_2067;

										BgL_arg1678z00_2067 = CDR(((obj_t) BgL_clausez00_2065));
										BgL_arg1675z00_2066 =
											BGl_failzd2nodez12zc0zzfail_walkz00(
											((BgL_nodez00_bglt) BgL_arg1678z00_2067));
									}
									{	/* Fail/walk.scm 183 */
										obj_t BgL_auxz00_2439;
										obj_t BgL_tmpz00_2437;

										BgL_auxz00_2439 = ((obj_t) BgL_arg1675z00_2066);
										BgL_tmpz00_2437 = ((obj_t) BgL_clausez00_2065);
										SET_CDR(BgL_tmpz00_2437, BgL_auxz00_2439);
									}
								}
							}
							{
								obj_t BgL_l1243z00_2442;

								BgL_l1243z00_2442 = CDR(BgL_l1243z00_2064);
								BgL_l1243z00_2064 = BgL_l1243z00_2442;
								goto BgL_zc3z04anonymousza31664ze3z87_2063;
							}
						}
					else
						{	/* Fail/walk.scm 182 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_1998));
		}

	}



/* &fail-node!-fail1277 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2fail1277z70zzfail_walkz00(obj_t
		BgL_envz00_1999, obj_t BgL_nodez00_2000)
	{
		{	/* Fail/walk.scm 160 */
			{
				BgL_nodez00_bglt BgL_auxz00_2446;

				{	/* Fail/walk.scm 162 */
					BgL_nodez00_bglt BgL_arg1602z00_2069;

					BgL_arg1602z00_2069 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2000)))->BgL_procz00);
					BgL_auxz00_2446 =
						BGl_failzd2nodez12zc0zzfail_walkz00(BgL_arg1602z00_2069);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2000)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2446), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2452;

				{	/* Fail/walk.scm 163 */
					BgL_nodez00_bglt BgL_arg1605z00_2070;

					BgL_arg1605z00_2070 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2000)))->BgL_msgz00);
					BgL_auxz00_2452 =
						BGl_failzd2nodez12zc0zzfail_walkz00(BgL_arg1605z00_2070);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2000)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2452), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2458;

				{	/* Fail/walk.scm 164 */
					BgL_nodez00_bglt BgL_arg1606z00_2071;

					BgL_arg1606z00_2071 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2000)))->BgL_objz00);
					BgL_auxz00_2458 =
						BGl_failzd2nodez12zc0zzfail_walkz00(BgL_arg1606z00_2071);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2000)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2458), BUNSPEC);
			}
			{	/* Fail/walk.scm 165 */
				bool_t BgL_test1950z00_2464;

				{	/* Fail/walk.scm 165 */
					obj_t BgL_arg1661z00_2072;

					BgL_arg1661z00_2072 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_failz00_bglt) BgL_nodez00_2000))))->BgL_locz00);
					if (STRUCTP(BgL_arg1661z00_2072))
						{	/* Fail/walk.scm 165 */
							BgL_test1950z00_2464 =
								(STRUCT_KEY(BgL_arg1661z00_2072) == CNST_TABLE_REF(6));
						}
					else
						{	/* Fail/walk.scm 165 */
							BgL_test1950z00_2464 = ((bool_t) 0);
						}
				}
				if (BgL_test1950z00_2464)
					{	/* Fail/walk.scm 166 */
						obj_t BgL_arg1611z00_2073;
						obj_t BgL_arg1613z00_2074;

						{	/* Fail/walk.scm 166 */
							obj_t BgL_arg1615z00_2075;

							{	/* Fail/walk.scm 166 */
								BgL_nodez00_bglt BgL_arg1616z00_2076;
								obj_t BgL_arg1625z00_2077;

								BgL_arg1616z00_2076 =
									(((BgL_failz00_bglt) COBJECT(
											((BgL_failz00_bglt) BgL_nodez00_2000)))->BgL_procz00);
								{	/* Fail/walk.scm 166 */
									BgL_nodez00_bglt BgL_arg1626z00_2078;
									obj_t BgL_arg1627z00_2079;

									BgL_arg1626z00_2078 =
										(((BgL_failz00_bglt) COBJECT(
												((BgL_failz00_bglt) BgL_nodez00_2000)))->BgL_msgz00);
									{	/* Fail/walk.scm 166 */
										BgL_nodez00_bglt BgL_arg1629z00_2080;
										obj_t BgL_arg1630z00_2081;

										BgL_arg1629z00_2080 =
											(((BgL_failz00_bglt) COBJECT(
													((BgL_failz00_bglt) BgL_nodez00_2000)))->BgL_objz00);
										{	/* Fail/walk.scm 169 */
											obj_t BgL_arg1642z00_2082;
											obj_t BgL_arg1646z00_2083;

											{	/* Fail/walk.scm 169 */
												obj_t BgL_arg1650z00_2084;

												BgL_arg1650z00_2084 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																((BgL_failz00_bglt) BgL_nodez00_2000))))->
													BgL_locz00);
												BgL_arg1642z00_2082 =
													BGl_locationzd2fullzd2fnamez00zztools_locationz00
													(BgL_arg1650z00_2084);
											}
											{	/* Fail/walk.scm 170 */
												obj_t BgL_arg1651z00_2085;

												{	/* Fail/walk.scm 170 */
													obj_t BgL_sz00_2086;

													BgL_sz00_2086 =
														(((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt)
																	((BgL_failz00_bglt) BgL_nodez00_2000))))->
														BgL_locz00);
													BgL_arg1651z00_2085 =
														STRUCT_REF(BgL_sz00_2086, (int) (1L));
												}
												BgL_arg1646z00_2083 =
													MAKE_YOUNG_PAIR(BgL_arg1651z00_2085, BNIL);
											}
											BgL_arg1630z00_2081 =
												MAKE_YOUNG_PAIR(BgL_arg1642z00_2082,
												BgL_arg1646z00_2083);
										}
										BgL_arg1627z00_2079 =
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_arg1629z00_2080), BgL_arg1630z00_2081);
									}
									BgL_arg1625z00_2077 =
										MAKE_YOUNG_PAIR(
										((obj_t) BgL_arg1626z00_2078), BgL_arg1627z00_2079);
								}
								BgL_arg1615z00_2075 =
									MAKE_YOUNG_PAIR(
									((obj_t) BgL_arg1616z00_2076), BgL_arg1625z00_2077);
							}
							BgL_arg1611z00_2073 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1615z00_2075);
						}
						BgL_arg1613z00_2074 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_failz00_bglt) BgL_nodez00_2000))))->BgL_locz00);
						return
							BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg1611z00_2073, BNIL,
							BgL_arg1613z00_2074, CNST_TABLE_REF(8));
					}
				else
					{	/* Fail/walk.scm 165 */
						return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_2000));
					}
			}
		}

	}



/* &fail-node!-condition1275 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2condition1275z70zzfail_walkz00(obj_t
		BgL_envz00_2001, obj_t BgL_nodez00_2002)
	{
		{	/* Fail/walk.scm 150 */
			{
				BgL_nodez00_bglt BgL_auxz00_2505;

				{	/* Fail/walk.scm 152 */
					BgL_nodez00_bglt BgL_arg1593z00_2088;

					BgL_arg1593z00_2088 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2002)))->BgL_testz00);
					BgL_auxz00_2505 =
						BGl_failzd2nodez12zc0zzfail_walkz00(BgL_arg1593z00_2088);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2002)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2505), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2511;

				{	/* Fail/walk.scm 153 */
					BgL_nodez00_bglt BgL_arg1594z00_2089;

					BgL_arg1594z00_2089 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2002)))->BgL_truez00);
					BgL_auxz00_2511 =
						BGl_failzd2nodez12zc0zzfail_walkz00(BgL_arg1594z00_2089);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2002)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_2511), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2517;

				{	/* Fail/walk.scm 154 */
					BgL_nodez00_bglt BgL_arg1595z00_2090;

					BgL_arg1595z00_2090 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2002)))->BgL_falsez00);
					BgL_auxz00_2517 =
						BGl_failzd2nodez12zc0zzfail_walkz00(BgL_arg1595z00_2090);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2002)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_2517), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_2002));
		}

	}



/* &fail-node!-setq1273 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2setq1273z70zzfail_walkz00(obj_t
		BgL_envz00_2003, obj_t BgL_nodez00_2004)
	{
		{	/* Fail/walk.scm 143 */
			((((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2004)))->BgL_valuez00) =
				((BgL_nodez00_bglt)
					BGl_failzd2nodez12zc0zzfail_walkz00((((BgL_setqz00_bglt)
								COBJECT(((BgL_setqz00_bglt) BgL_nodez00_2004)))->
							BgL_valuez00))), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_2004));
		}

	}



/* &fail-node!-extern1271 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2extern1271z70zzfail_walkz00(obj_t
		BgL_envz00_2005, obj_t BgL_nodez00_2006)
	{
		{	/* Fail/walk.scm 136 */
			BGl_failzd2nodeza2z12z62zzfail_walkz00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2006)))->BgL_exprza2za2));
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2006));
		}

	}



/* &fail-node!-funcall1269 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2funcall1269z70zzfail_walkz00(obj_t
		BgL_envz00_2007, obj_t BgL_nodez00_2008)
	{
		{	/* Fail/walk.scm 127 */
			{
				BgL_nodez00_bglt BgL_auxz00_2537;

				{	/* Fail/walk.scm 129 */
					BgL_nodez00_bglt BgL_arg1576z00_2094;

					BgL_arg1576z00_2094 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2008)))->BgL_funz00);
					BgL_auxz00_2537 =
						BGl_failzd2nodez12zc0zzfail_walkz00(BgL_arg1576z00_2094);
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2008)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2537), BUNSPEC);
			}
			BGl_failzd2nodeza2z12z62zzfail_walkz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2008)))->BgL_argsz00));
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_2008));
		}

	}



/* &fail-node!-app-ly1267 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2appzd2ly1267za2zzfail_walkz00(obj_t
		BgL_envz00_2009, obj_t BgL_nodez00_2010)
	{
		{	/* Fail/walk.scm 118 */
			{
				BgL_nodez00_bglt BgL_auxz00_2548;

				{	/* Fail/walk.scm 120 */
					BgL_nodez00_bglt BgL_arg1573z00_2096;

					BgL_arg1573z00_2096 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2010)))->BgL_funz00);
					BgL_auxz00_2548 =
						BGl_failzd2nodez12zc0zzfail_walkz00(BgL_arg1573z00_2096);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2010)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2548), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2554;

				{	/* Fail/walk.scm 121 */
					BgL_nodez00_bglt BgL_arg1575z00_2097;

					BgL_arg1575z00_2097 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2010)))->BgL_argz00);
					BgL_auxz00_2554 =
						BGl_failzd2nodez12zc0zzfail_walkz00(BgL_arg1575z00_2097);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2010)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2554), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2010));
		}

	}



/* &fail-node!-app1265 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2app1265z70zzfail_walkz00(obj_t
		BgL_envz00_2011, obj_t BgL_nodez00_2012)
	{
		{	/* Fail/walk.scm 100 */
			BGl_failzd2nodeza2z12z62zzfail_walkz00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2012)))->BgL_argsz00));
			{	/* Fail/walk.scm 103 */
				bool_t BgL_test1952z00_2565;

				{	/* Fail/walk.scm 103 */
					bool_t BgL_test1953z00_2566;

					{	/* Fail/walk.scm 103 */
						obj_t BgL_arg1571z00_2099;

						BgL_arg1571z00_2099 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_2012))))->BgL_locz00);
						if (STRUCTP(BgL_arg1571z00_2099))
							{	/* Fail/walk.scm 103 */
								BgL_test1953z00_2566 =
									(STRUCT_KEY(BgL_arg1571z00_2099) == CNST_TABLE_REF(6));
							}
						else
							{	/* Fail/walk.scm 103 */
								BgL_test1953z00_2566 = ((bool_t) 0);
							}
					}
					if (BgL_test1953z00_2566)
						{	/* Fail/walk.scm 104 */
							BgL_variablez00_bglt BgL_arg1564z00_2100;

							BgL_arg1564z00_2100 =
								(((BgL_varz00_bglt) COBJECT(
										(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt) BgL_nodez00_2012)))->
											BgL_funz00)))->BgL_variablez00);
							BgL_test1952z00_2565 =
								(((obj_t) BgL_arg1564z00_2100) ==
								BGl_za2errorza2z00zzfail_walkz00);
						}
					else
						{	/* Fail/walk.scm 103 */
							BgL_test1952z00_2565 = ((bool_t) 0);
						}
				}
				if (BgL_test1952z00_2565)
					{	/* Fail/walk.scm 105 */
						obj_t BgL_arg1472z00_2101;
						obj_t BgL_arg1473z00_2102;

						{	/* Fail/walk.scm 105 */
							obj_t BgL_arg1485z00_2103;

							{	/* Fail/walk.scm 105 */
								obj_t BgL_arg1489z00_2104;
								obj_t BgL_arg1502z00_2105;

								BgL_arg1489z00_2104 =
									CAR(
									(((BgL_appz00_bglt) COBJECT(
												((BgL_appz00_bglt) BgL_nodez00_2012)))->BgL_argsz00));
								{	/* Fail/walk.scm 106 */
									obj_t BgL_arg1513z00_2106;
									obj_t BgL_arg1514z00_2107;

									{	/* Fail/walk.scm 106 */
										obj_t BgL_pairz00_2108;

										BgL_pairz00_2108 =
											(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt) BgL_nodez00_2012)))->BgL_argsz00);
										BgL_arg1513z00_2106 = CAR(CDR(BgL_pairz00_2108));
									}
									{	/* Fail/walk.scm 107 */
										obj_t BgL_arg1535z00_2109;
										obj_t BgL_arg1540z00_2110;

										{	/* Fail/walk.scm 107 */
											obj_t BgL_pairz00_2111;

											BgL_pairz00_2111 =
												(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_nodez00_2012)))->
												BgL_argsz00);
											BgL_arg1535z00_2109 = CAR(CDR(CDR(BgL_pairz00_2111)));
										}
										{	/* Fail/walk.scm 108 */
											obj_t BgL_arg1546z00_2112;
											obj_t BgL_arg1552z00_2113;

											{	/* Fail/walk.scm 108 */
												obj_t BgL_arg1553z00_2114;

												BgL_arg1553z00_2114 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																((BgL_appz00_bglt) BgL_nodez00_2012))))->
													BgL_locz00);
												BgL_arg1546z00_2112 =
													BGl_locationzd2fullzd2fnamez00zztools_locationz00
													(BgL_arg1553z00_2114);
											}
											{	/* Fail/walk.scm 109 */
												obj_t BgL_arg1559z00_2115;

												{	/* Fail/walk.scm 109 */
													obj_t BgL_sz00_2116;

													BgL_sz00_2116 =
														(((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt)
																	((BgL_appz00_bglt) BgL_nodez00_2012))))->
														BgL_locz00);
													BgL_arg1559z00_2115 =
														STRUCT_REF(BgL_sz00_2116, (int) (1L));
												}
												BgL_arg1552z00_2113 =
													MAKE_YOUNG_PAIR(BgL_arg1559z00_2115, BNIL);
											}
											BgL_arg1540z00_2110 =
												MAKE_YOUNG_PAIR(BgL_arg1546z00_2112,
												BgL_arg1552z00_2113);
										}
										BgL_arg1514z00_2107 =
											MAKE_YOUNG_PAIR(BgL_arg1535z00_2109, BgL_arg1540z00_2110);
									}
									BgL_arg1502z00_2105 =
										MAKE_YOUNG_PAIR(BgL_arg1513z00_2106, BgL_arg1514z00_2107);
								}
								BgL_arg1485z00_2103 =
									MAKE_YOUNG_PAIR(BgL_arg1489z00_2104, BgL_arg1502z00_2105);
							}
							BgL_arg1472z00_2101 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1485z00_2103);
						}
						BgL_arg1473z00_2102 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_2012))))->BgL_locz00);
						return
							BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg1472z00_2101, BNIL,
							BgL_arg1473z00_2102, CNST_TABLE_REF(8));
					}
				else
					{	/* Fail/walk.scm 103 */
						return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2012));
					}
			}
		}

	}



/* &fail-node!-sync1263 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2sync1263z70zzfail_walkz00(obj_t
		BgL_envz00_2013, obj_t BgL_nodez00_2014)
	{
		{	/* Fail/walk.scm 91 */
			((((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2014)))->BgL_mutexz00) =
				((BgL_nodez00_bglt)
					BGl_failzd2nodez12zc0zzfail_walkz00((((BgL_syncz00_bglt)
								COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2014)))->
							BgL_mutexz00))), BUNSPEC);
			((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2014)))->
					BgL_prelockz00) =
				((BgL_nodez00_bglt)
					BGl_failzd2nodez12zc0zzfail_walkz00((((BgL_syncz00_bglt)
								COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2014)))->
							BgL_prelockz00))), BUNSPEC);
			((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2014)))->
					BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_failzd2nodez12zc0zzfail_walkz00((((BgL_syncz00_bglt)
								COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2014)))->BgL_bodyz00))),
				BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2014));
		}

	}



/* &fail-node!-sequence1261 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2sequence1261z70zzfail_walkz00(obj_t
		BgL_envz00_2015, obj_t BgL_nodez00_2016)
	{
		{	/* Fail/walk.scm 84 */
			BGl_failzd2nodeza2z12z62zzfail_walkz00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2016)))->BgL_nodesz00));
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_2016));
		}

	}



/* &fail-node!-var1259 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2var1259z70zzfail_walkz00(obj_t
		BgL_envz00_2017, obj_t BgL_nodez00_2018)
	{
		{	/* Fail/walk.scm 78 */
			return ((BgL_nodez00_bglt) ((BgL_varz00_bglt) BgL_nodez00_2018));
		}

	}



/* &fail-node!-kwote1257 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2kwote1257z70zzfail_walkz00(obj_t
		BgL_envz00_2019, obj_t BgL_nodez00_2020)
	{
		{	/* Fail/walk.scm 72 */
			return ((BgL_nodez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_2020));
		}

	}



/* &fail-node!-atom1255 */
	BgL_nodez00_bglt BGl_z62failzd2nodez12zd2atom1255z70zzfail_walkz00(obj_t
		BgL_envz00_2021, obj_t BgL_nodez00_2022)
	{
		{	/* Fail/walk.scm 66 */
			return ((BgL_nodez00_bglt) ((BgL_atomz00_bglt) BgL_nodez00_2022));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzfail_walkz00(void)
	{
		{	/* Fail/walk.scm 18 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1893z00zzfail_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1893z00zzfail_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1893z00zzfail_walkz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1893z00zzfail_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1893z00zzfail_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1893z00zzfail_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1893z00zzfail_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1893z00zzfail_walkz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1893z00zzfail_walkz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1893z00zzfail_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1893z00zzfail_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
