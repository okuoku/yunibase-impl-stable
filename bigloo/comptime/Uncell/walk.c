/*===========================================================================*/
/*   (Uncell/walk.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Uncell/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_UNCELL_WALK_TYPE_DEFINITIONS
#define BGL_UNCELL_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_localzf2infozf2_bgl
	{
		bool_t BgL_escapez00;
	}                      *BgL_localzf2infozf2_bglt;


#endif													// BGL_UNCELL_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_setqz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzuncell_walkz00 = BUNSPEC;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static obj_t BGl_z62markzd2cellzd2letzd2var1289zb0zzuncell_walkz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static BgL_nodez00_bglt BGl_uncellz12z12zzuncell_walkz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzuncell_walkz00(void);
	static obj_t BGl_z62markzd2cellzd2boxzd2ref1291zb0zzuncell_walkz00(obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62uncellz121294z70zzuncell_walkz00(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzuncell_walkz00(void);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzuncell_walkz00(void);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	extern obj_t BGl_typez00zztype_typez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzuncell_walkz00(void);
	static obj_t BGl_z62uncellzd2walkz12za2zzuncell_walkz00(obj_t, obj_t);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static obj_t BGl_uncellzd2funz12zc0zzuncell_walkz00(obj_t);
	static obj_t BGl_localzf2infozf2zzuncell_walkz00 = BUNSPEC;
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza31409ze3ze5zzuncell_walkz00(obj_t,
		obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62uncellz12zd2boxzd2setz121301z62zzuncell_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	static obj_t BGl_z62markzd2cellzb0zzuncell_walkz00(obj_t, obj_t);
	extern obj_t BGl_walk0z00zzast_walkz00(BgL_nodez00_bglt, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzuncell_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static BgL_nodez00_bglt BGl_z62uncellz12z70zzuncell_walkz00(obj_t, obj_t);
	static obj_t BGl_z62markzd2cell1284zb0zzuncell_walkz00(obj_t, obj_t);
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	static BgL_localz00_bglt BGl_z62lambda1410z62zzuncell_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62uncellz12zd2letzd2var1297z70zzuncell_walkz00(obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_markzd2cellzd2zzuncell_walkz00(BgL_nodez00_bglt);
	static obj_t BGl_cnstzd2initzd2zzuncell_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzuncell_walkz00(void);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzuncell_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzuncell_walkz00(void);
	static BgL_nodez00_bglt
		BGl_z62uncellz12zd2boxzd2ref1299z70zzuncell_walkz00(obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_walk0z12z12zzast_walkz00(BgL_nodez00_bglt, obj_t);
	static obj_t BGl_z62markzd2cellzd2var1287z62zzuncell_walkz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31457ze3ze5zzuncell_walkz00(obj_t);
	static obj_t BGl_z62lambda1455z62zzuncell_walkz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1456z62zzuncell_walkz00(obj_t, obj_t, obj_t);
	static BgL_localz00_bglt BGl_z62lambda1378z62zzuncell_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_localz00_bglt BGl_z62lambda1381z62zzuncell_walkz00(obj_t, obj_t);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_valuez00zzast_varz00;
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	static obj_t BGl_z62markzd2cellzd2boxzd2setz121293za2zzuncell_walkz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_uncellzd2walkz12zc0zzuncell_walkz00(obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[7];


	   
		 
		DEFINE_STRING(BGl_string1815z00zzuncell_walkz00,
		BgL_bgl_string1815za700za7za7u1846za7, "Uncell", 6);
	      DEFINE_STRING(BGl_string1816z00zzuncell_walkz00,
		BgL_bgl_string1816za700za7za7u1847za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1817z00zzuncell_walkz00,
		BgL_bgl_string1817za700za7za7u1848za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1818z00zzuncell_walkz00,
		BgL_bgl_string1818za700za7za7u1849za7, " error", 6);
	      DEFINE_STRING(BGl_string1819z00zzuncell_walkz00,
		BgL_bgl_string1819za700za7za7u1850za7, "s", 1);
	      DEFINE_STRING(BGl_string1820z00zzuncell_walkz00,
		BgL_bgl_string1820za700za7za7u1851za7, "", 0);
	      DEFINE_STRING(BGl_string1821z00zzuncell_walkz00,
		BgL_bgl_string1821za700za7za7u1852za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1822z00zzuncell_walkz00,
		BgL_bgl_string1822za700za7za7u1853za7, "failure during postlude hook", 28);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1823z00zzuncell_walkz00,
		BgL_bgl_za762za7c3za704anonymo1854za7,
		BGl_z62zc3z04anonymousza31457ze3ze5zzuncell_walkz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1824z00zzuncell_walkz00,
		BgL_bgl_za762lambda1456za7621855z00, BGl_z62lambda1456z62zzuncell_walkz00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1831z00zzuncell_walkz00,
		BgL_bgl_string1831za700za7za7u1856za7, "mark-cell1284", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1825z00zzuncell_walkz00,
		BgL_bgl_za762lambda1455za7621857z00, BGl_z62lambda1455z62zzuncell_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1826z00zzuncell_walkz00,
		BgL_bgl_za762lambda1410za7621858z00, BGl_z62lambda1410z62zzuncell_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1833z00zzuncell_walkz00,
		BgL_bgl_string1833za700za7za7u1859za7, "uncell!1294", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1827z00zzuncell_walkz00,
		BgL_bgl_za762za7c3za704anonymo1860za7,
		BGl_z62zc3z04anonymousza31409ze3ze5zzuncell_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1828z00zzuncell_walkz00,
		BgL_bgl_za762lambda1381za7621861z00, BGl_z62lambda1381z62zzuncell_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1835z00zzuncell_walkz00,
		BgL_bgl_string1835za700za7za7u1862za7, "mark-cell", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1829z00zzuncell_walkz00,
		BgL_bgl_za762lambda1378za7621863z00, BGl_z62lambda1378z62zzuncell_walkz00,
		0L, BUNSPEC, 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1830z00zzuncell_walkz00,
		BgL_bgl_za762markza7d2cell121864z00,
		BGl_z62markzd2cell1284zb0zzuncell_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1832z00zzuncell_walkz00,
		BgL_bgl_za762uncellza71212941865z00,
		BGl_z62uncellz121294z70zzuncell_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1840z00zzuncell_walkz00,
		BgL_bgl_string1840za700za7za7u1866za7, "uncell!::node", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1834z00zzuncell_walkz00,
		BgL_bgl_za762markza7d2cellza7d1867za7,
		BGl_z62markzd2cellzd2var1287z62zzuncell_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1836z00zzuncell_walkz00,
		BgL_bgl_za762markza7d2cellza7d1868za7,
		BGl_z62markzd2cellzd2letzd2var1289zb0zzuncell_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1843z00zzuncell_walkz00,
		BgL_bgl_string1843za700za7za7u1869za7, "uncell_walk", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1837z00zzuncell_walkz00,
		BgL_bgl_za762markza7d2cellza7d1870za7,
		BGl_z62markzd2cellzd2boxzd2ref1291zb0zzuncell_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1844z00zzuncell_walkz00,
		BgL_bgl_string1844za700za7za7u1871za7,
		"read _ uncell_walk local/info bool escape pass-started ", 55);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1838z00zzuncell_walkz00,
		BgL_bgl_za762markza7d2cellza7d1872za7,
		BGl_z62markzd2cellzd2boxzd2setz121293za2zzuncell_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1839z00zzuncell_walkz00,
		BgL_bgl_za762uncellza712za7d2l1873za7,
		BGl_z62uncellz12zd2letzd2var1297z70zzuncell_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1841z00zzuncell_walkz00,
		BgL_bgl_za762uncellza712za7d2b1874za7,
		BGl_z62uncellz12zd2boxzd2ref1299z70zzuncell_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1842z00zzuncell_walkz00,
		BgL_bgl_za762uncellza712za7d2b1875za7,
		BGl_z62uncellz12zd2boxzd2setz121301z62zzuncell_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_uncellz12zd2envzc0zzuncell_walkz00,
		BgL_bgl_za762uncellza712za770za71876z00,
		BGl_z62uncellz12z70zzuncell_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_markzd2cellzd2envz00zzuncell_walkz00,
		BgL_bgl_za762markza7d2cellza7b1877za7,
		BGl_z62markzd2cellzb0zzuncell_walkz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_uncellzd2walkz12zd2envz12zzuncell_walkz00,
		BgL_bgl_za762uncellza7d2walk1878z00,
		BGl_z62uncellzd2walkz12za2zzuncell_walkz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzuncell_walkz00));
		     ADD_ROOT((void *) (&BGl_localzf2infozf2zzuncell_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzuncell_walkz00(long
		BgL_checksumz00_2448, char *BgL_fromz00_2449)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzuncell_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzuncell_walkz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzuncell_walkz00();
					BGl_libraryzd2moduleszd2initz00zzuncell_walkz00();
					BGl_cnstzd2initzd2zzuncell_walkz00();
					BGl_importedzd2moduleszd2initz00zzuncell_walkz00();
					BGl_objectzd2initzd2zzuncell_walkz00();
					BGl_genericzd2initzd2zzuncell_walkz00();
					BGl_methodzd2initzd2zzuncell_walkz00();
					return BGl_toplevelzd2initzd2zzuncell_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzuncell_walkz00(void)
	{
		{	/* Uncell/walk.scm 16 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "uncell_walk");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"uncell_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"uncell_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "uncell_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "uncell_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "uncell_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "uncell_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "uncell_walk");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"uncell_walk");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "uncell_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "uncell_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"uncell_walk");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "uncell_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzuncell_walkz00(void)
	{
		{	/* Uncell/walk.scm 16 */
			{	/* Uncell/walk.scm 16 */
				obj_t BgL_cportz00_2327;

				{	/* Uncell/walk.scm 16 */
					obj_t BgL_stringz00_2334;

					BgL_stringz00_2334 = BGl_string1844z00zzuncell_walkz00;
					{	/* Uncell/walk.scm 16 */
						obj_t BgL_startz00_2335;

						BgL_startz00_2335 = BINT(0L);
						{	/* Uncell/walk.scm 16 */
							obj_t BgL_endz00_2336;

							BgL_endz00_2336 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2334)));
							{	/* Uncell/walk.scm 16 */

								BgL_cportz00_2327 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2334, BgL_startz00_2335, BgL_endz00_2336);
				}}}}
				{
					long BgL_iz00_2328;

					BgL_iz00_2328 = 6L;
				BgL_loopz00_2329:
					if ((BgL_iz00_2328 == -1L))
						{	/* Uncell/walk.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Uncell/walk.scm 16 */
							{	/* Uncell/walk.scm 16 */
								obj_t BgL_arg1845z00_2330;

								{	/* Uncell/walk.scm 16 */

									{	/* Uncell/walk.scm 16 */
										obj_t BgL_locationz00_2332;

										BgL_locationz00_2332 = BBOOL(((bool_t) 0));
										{	/* Uncell/walk.scm 16 */

											BgL_arg1845z00_2330 =
												BGl_readz00zz__readerz00(BgL_cportz00_2327,
												BgL_locationz00_2332);
										}
									}
								}
								{	/* Uncell/walk.scm 16 */
									int BgL_tmpz00_2483;

									BgL_tmpz00_2483 = (int) (BgL_iz00_2328);
									CNST_TABLE_SET(BgL_tmpz00_2483, BgL_arg1845z00_2330);
							}}
							{	/* Uncell/walk.scm 16 */
								int BgL_auxz00_2333;

								BgL_auxz00_2333 = (int) ((BgL_iz00_2328 - 1L));
								{
									long BgL_iz00_2488;

									BgL_iz00_2488 = (long) (BgL_auxz00_2333);
									BgL_iz00_2328 = BgL_iz00_2488;
									goto BgL_loopz00_2329;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzuncell_walkz00(void)
	{
		{	/* Uncell/walk.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzuncell_walkz00(void)
	{
		{	/* Uncell/walk.scm 16 */
			return BUNSPEC;
		}

	}



/* uncell-walk! */
	BGL_EXPORTED_DEF obj_t BGl_uncellzd2walkz12zc0zzuncell_walkz00(obj_t
		BgL_globalsz00_17)
	{
		{	/* Uncell/walk.scm 41 */
			{	/* Uncell/walk.scm 42 */
				obj_t BgL_list1320z00_1486;

				{	/* Uncell/walk.scm 42 */
					obj_t BgL_arg1321z00_1487;

					{	/* Uncell/walk.scm 42 */
						obj_t BgL_arg1322z00_1488;

						BgL_arg1322z00_1488 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1321z00_1487 =
							MAKE_YOUNG_PAIR(BGl_string1815z00zzuncell_walkz00,
							BgL_arg1322z00_1488);
					}
					BgL_list1320z00_1486 =
						MAKE_YOUNG_PAIR(BGl_string1816z00zzuncell_walkz00,
						BgL_arg1321z00_1487);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1320z00_1486);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1815z00zzuncell_walkz00;
			{	/* Uncell/walk.scm 42 */
				obj_t BgL_g1138z00_1489;

				BgL_g1138z00_1489 = BNIL;
				{
					obj_t BgL_hooksz00_1492;
					obj_t BgL_hnamesz00_1493;

					BgL_hooksz00_1492 = BgL_g1138z00_1489;
					BgL_hnamesz00_1493 = BNIL;
				BgL_zc3z04anonymousza31323ze3z87_1494:
					if (NULLP(BgL_hooksz00_1492))
						{	/* Uncell/walk.scm 42 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Uncell/walk.scm 42 */
							bool_t BgL_test1882z00_2501;

							{	/* Uncell/walk.scm 42 */
								obj_t BgL_fun1332z00_1501;

								BgL_fun1332z00_1501 = CAR(((obj_t) BgL_hooksz00_1492));
								BgL_test1882z00_2501 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1332z00_1501));
							}
							if (BgL_test1882z00_2501)
								{	/* Uncell/walk.scm 42 */
									obj_t BgL_arg1328z00_1498;
									obj_t BgL_arg1329z00_1499;

									BgL_arg1328z00_1498 = CDR(((obj_t) BgL_hooksz00_1492));
									BgL_arg1329z00_1499 = CDR(((obj_t) BgL_hnamesz00_1493));
									{
										obj_t BgL_hnamesz00_2513;
										obj_t BgL_hooksz00_2512;

										BgL_hooksz00_2512 = BgL_arg1328z00_1498;
										BgL_hnamesz00_2513 = BgL_arg1329z00_1499;
										BgL_hnamesz00_1493 = BgL_hnamesz00_2513;
										BgL_hooksz00_1492 = BgL_hooksz00_2512;
										goto BgL_zc3z04anonymousza31323ze3z87_1494;
									}
								}
							else
								{	/* Uncell/walk.scm 42 */
									obj_t BgL_arg1331z00_1500;

									BgL_arg1331z00_1500 = CAR(((obj_t) BgL_hnamesz00_1493));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1815z00zzuncell_walkz00,
										BGl_string1817z00zzuncell_walkz00, BgL_arg1331z00_1500);
								}
						}
				}
			}
			{
				obj_t BgL_l1273z00_1505;

				BgL_l1273z00_1505 = BgL_globalsz00_17;
			BgL_zc3z04anonymousza31334ze3z87_1506:
				if (PAIRP(BgL_l1273z00_1505))
					{	/* Uncell/walk.scm 43 */
						BGl_uncellzd2funz12zc0zzuncell_walkz00(CAR(BgL_l1273z00_1505));
						{
							obj_t BgL_l1273z00_2521;

							BgL_l1273z00_2521 = CDR(BgL_l1273z00_1505);
							BgL_l1273z00_1505 = BgL_l1273z00_2521;
							goto BgL_zc3z04anonymousza31334ze3z87_1506;
						}
					}
				else
					{	/* Uncell/walk.scm 43 */
						((bool_t) 1);
					}
			}
			if (
				((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) > 0L))
				{	/* Uncell/walk.scm 44 */
					{	/* Uncell/walk.scm 44 */
						obj_t BgL_port1275z00_1513;

						{	/* Uncell/walk.scm 44 */
							obj_t BgL_tmpz00_2526;

							BgL_tmpz00_2526 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1275z00_1513 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2526);
						}
						bgl_display_obj(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
							BgL_port1275z00_1513);
						bgl_display_string(BGl_string1818z00zzuncell_walkz00,
							BgL_port1275z00_1513);
						{	/* Uncell/walk.scm 44 */
							obj_t BgL_arg1342z00_1514;

							{	/* Uncell/walk.scm 44 */
								bool_t BgL_test1885z00_2531;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
									{	/* Uncell/walk.scm 44 */
										if (INTEGERP
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Uncell/walk.scm 44 */
												BgL_test1885z00_2531 =
													(
													(long)
													CINT
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
													> 1L);
											}
										else
											{	/* Uncell/walk.scm 44 */
												BgL_test1885z00_2531 =
													BGl_2ze3ze3zz__r4_numbers_6_5z00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BINT(1L));
											}
									}
								else
									{	/* Uncell/walk.scm 44 */
										BgL_test1885z00_2531 = ((bool_t) 0);
									}
								if (BgL_test1885z00_2531)
									{	/* Uncell/walk.scm 44 */
										BgL_arg1342z00_1514 = BGl_string1819z00zzuncell_walkz00;
									}
								else
									{	/* Uncell/walk.scm 44 */
										BgL_arg1342z00_1514 = BGl_string1820z00zzuncell_walkz00;
									}
							}
							bgl_display_obj(BgL_arg1342z00_1514, BgL_port1275z00_1513);
						}
						bgl_display_string(BGl_string1821z00zzuncell_walkz00,
							BgL_port1275z00_1513);
						bgl_display_char(((unsigned char) 10), BgL_port1275z00_1513);
					}
					{	/* Uncell/walk.scm 44 */
						obj_t BgL_list1346z00_1518;

						BgL_list1346z00_1518 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
						BGL_TAIL return BGl_exitz00zz__errorz00(BgL_list1346z00_1518);
					}
				}
			else
				{	/* Uncell/walk.scm 44 */
					obj_t BgL_g1140z00_1519;

					BgL_g1140z00_1519 = BNIL;
					{
						obj_t BgL_hooksz00_1522;
						obj_t BgL_hnamesz00_1523;

						BgL_hooksz00_1522 = BgL_g1140z00_1519;
						BgL_hnamesz00_1523 = BNIL;
					BgL_zc3z04anonymousza31347ze3z87_1524:
						if (NULLP(BgL_hooksz00_1522))
							{	/* Uncell/walk.scm 44 */
								return BgL_globalsz00_17;
							}
						else
							{	/* Uncell/walk.scm 44 */
								bool_t BgL_test1889z00_2548;

								{	/* Uncell/walk.scm 44 */
									obj_t BgL_fun1362z00_1531;

									BgL_fun1362z00_1531 = CAR(((obj_t) BgL_hooksz00_1522));
									BgL_test1889z00_2548 =
										CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1362z00_1531));
								}
								if (BgL_test1889z00_2548)
									{	/* Uncell/walk.scm 44 */
										obj_t BgL_arg1351z00_1528;
										obj_t BgL_arg1352z00_1529;

										BgL_arg1351z00_1528 = CDR(((obj_t) BgL_hooksz00_1522));
										BgL_arg1352z00_1529 = CDR(((obj_t) BgL_hnamesz00_1523));
										{
											obj_t BgL_hnamesz00_2560;
											obj_t BgL_hooksz00_2559;

											BgL_hooksz00_2559 = BgL_arg1351z00_1528;
											BgL_hnamesz00_2560 = BgL_arg1352z00_1529;
											BgL_hnamesz00_1523 = BgL_hnamesz00_2560;
											BgL_hooksz00_1522 = BgL_hooksz00_2559;
											goto BgL_zc3z04anonymousza31347ze3z87_1524;
										}
									}
								else
									{	/* Uncell/walk.scm 44 */
										obj_t BgL_arg1361z00_1530;

										BgL_arg1361z00_1530 = CAR(((obj_t) BgL_hnamesz00_1523));
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_za2currentzd2passza2zd2zzengine_passz00,
											BGl_string1822z00zzuncell_walkz00, BgL_arg1361z00_1530);
									}
							}
					}
				}
		}

	}



/* &uncell-walk! */
	obj_t BGl_z62uncellzd2walkz12za2zzuncell_walkz00(obj_t BgL_envz00_2259,
		obj_t BgL_globalsz00_2260)
	{
		{	/* Uncell/walk.scm 41 */
			return BGl_uncellzd2walkz12zc0zzuncell_walkz00(BgL_globalsz00_2260);
		}

	}



/* uncell-fun! */
	obj_t BGl_uncellzd2funz12zc0zzuncell_walkz00(obj_t BgL_varz00_18)
	{
		{	/* Uncell/walk.scm 49 */
			{	/* Uncell/walk.scm 50 */
				obj_t BgL_arg1364z00_1534;

				BgL_arg1364z00_1534 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_18)))->BgL_idz00);
				BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1364z00_1534);
			}
			{	/* Uncell/walk.scm 51 */
				BgL_valuez00_bglt BgL_funz00_1535;

				BgL_funz00_1535 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_18)))->BgL_valuez00);
				{	/* Uncell/walk.scm 51 */
					obj_t BgL_bodyz00_1536;

					BgL_bodyz00_1536 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1535)))->BgL_bodyz00);
					{	/* Uncell/walk.scm 52 */

						BGl_markzd2cellzd2zzuncell_walkz00(
							((BgL_nodez00_bglt) BgL_bodyz00_1536));
						BGl_uncellz12z12zzuncell_walkz00(
							((BgL_nodez00_bglt) BgL_bodyz00_1536));
						BGl_leavezd2functionzd2zztools_errorz00();
						return BgL_varz00_18;
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzuncell_walkz00(void)
	{
		{	/* Uncell/walk.scm 16 */
			{	/* Uncell/walk.scm 35 */
				obj_t BgL_arg1376z00_1541;
				obj_t BgL_arg1377z00_1542;

				{	/* Uncell/walk.scm 35 */
					obj_t BgL_v1282z00_1577;

					BgL_v1282z00_1577 = create_vector(1L);
					{	/* Uncell/walk.scm 35 */
						obj_t BgL_arg1437z00_1578;

						BgL_arg1437z00_1578 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc1825z00zzuncell_walkz00, BGl_proc1824z00zzuncell_walkz00,
							((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc1823z00zzuncell_walkz00, CNST_TABLE_REF(2));
						VECTOR_SET(BgL_v1282z00_1577, 0L, BgL_arg1437z00_1578);
					}
					BgL_arg1376z00_1541 = BgL_v1282z00_1577;
				}
				{	/* Uncell/walk.scm 35 */
					obj_t BgL_v1283z00_1591;

					BgL_v1283z00_1591 = create_vector(0L);
					BgL_arg1377z00_1542 = BgL_v1283z00_1591;
				}
				return (BGl_localzf2infozf2zzuncell_walkz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(3),
						CNST_TABLE_REF(4), BGl_localz00zzast_varz00, 1375L,
						BGl_proc1829z00zzuncell_walkz00, BGl_proc1828z00zzuncell_walkz00,
						BFALSE, BGl_proc1827z00zzuncell_walkz00,
						BGl_proc1826z00zzuncell_walkz00, BgL_arg1376z00_1541,
						BgL_arg1377z00_1542), BUNSPEC);
			}
		}

	}



/* &lambda1410 */
	BgL_localz00_bglt BGl_z62lambda1410z62zzuncell_walkz00(obj_t BgL_envz00_2268,
		obj_t BgL_o1123z00_2269)
	{
		{	/* Uncell/walk.scm 35 */
			{	/* Uncell/walk.scm 35 */
				long BgL_arg1421z00_2339;

				{	/* Uncell/walk.scm 35 */
					obj_t BgL_arg1422z00_2340;

					{	/* Uncell/walk.scm 35 */
						obj_t BgL_arg1434z00_2341;

						{	/* Uncell/walk.scm 35 */
							obj_t BgL_arg1815z00_2342;
							long BgL_arg1816z00_2343;

							BgL_arg1815z00_2342 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Uncell/walk.scm 35 */
								long BgL_arg1817z00_2344;

								BgL_arg1817z00_2344 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_o1123z00_2269)));
								BgL_arg1816z00_2343 = (BgL_arg1817z00_2344 - OBJECT_TYPE);
							}
							BgL_arg1434z00_2341 =
								VECTOR_REF(BgL_arg1815z00_2342, BgL_arg1816z00_2343);
						}
						BgL_arg1422z00_2340 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1434z00_2341);
					}
					{	/* Uncell/walk.scm 35 */
						obj_t BgL_tmpz00_2593;

						BgL_tmpz00_2593 = ((obj_t) BgL_arg1422z00_2340);
						BgL_arg1421z00_2339 = BGL_CLASS_NUM(BgL_tmpz00_2593);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_localz00_bglt) BgL_o1123z00_2269)), BgL_arg1421z00_2339);
			}
			{	/* Uncell/walk.scm 35 */
				BgL_objectz00_bglt BgL_tmpz00_2599;

				BgL_tmpz00_2599 =
					((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1123z00_2269));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2599, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1123z00_2269));
			return ((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1123z00_2269));
		}

	}



/* &<@anonymous:1409> */
	obj_t BGl_z62zc3z04anonymousza31409ze3ze5zzuncell_walkz00(obj_t
		BgL_envz00_2270, obj_t BgL_new1122z00_2271)
	{
		{	/* Uncell/walk.scm 35 */
			{
				BgL_localz00_bglt BgL_auxz00_2607;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1122z00_2271))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(5)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_2271))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_2615;

					{	/* Uncell/walk.scm 35 */
						obj_t BgL_classz00_2346;

						BgL_classz00_2346 = BGl_typez00zztype_typez00;
						{	/* Uncell/walk.scm 35 */
							obj_t BgL__ortest_1117z00_2347;

							BgL__ortest_1117z00_2347 = BGL_CLASS_NIL(BgL_classz00_2346);
							if (CBOOL(BgL__ortest_1117z00_2347))
								{	/* Uncell/walk.scm 35 */
									BgL_auxz00_2615 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_2347);
								}
							else
								{	/* Uncell/walk.scm 35 */
									BgL_auxz00_2615 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2346));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1122z00_2271))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_2615), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_2625;

					{	/* Uncell/walk.scm 35 */
						obj_t BgL_classz00_2348;

						BgL_classz00_2348 = BGl_valuez00zzast_varz00;
						{	/* Uncell/walk.scm 35 */
							obj_t BgL__ortest_1117z00_2349;

							BgL__ortest_1117z00_2349 = BGL_CLASS_NIL(BgL_classz00_2348);
							if (CBOOL(BgL__ortest_1117z00_2349))
								{	/* Uncell/walk.scm 35 */
									BgL_auxz00_2625 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_2349);
								}
							else
								{	/* Uncell/walk.scm 35 */
									BgL_auxz00_2625 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2348));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1122z00_2271))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_2625), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1122z00_2271))))->BgL_accessz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_2271))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_2271))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_2271))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_2271))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_2271))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_2271))))->BgL_keyz00) =
					((long) 0L), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_2271))))->BgL_valzd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_2271))))->BgL_volatilez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_localzf2infozf2_bglt BgL_auxz00_2662;

					{
						obj_t BgL_auxz00_2663;

						{	/* Uncell/walk.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_2664;

							BgL_tmpz00_2664 =
								((BgL_objectz00_bglt)
								((BgL_localz00_bglt) BgL_new1122z00_2271));
							BgL_auxz00_2663 = BGL_OBJECT_WIDENING(BgL_tmpz00_2664);
						}
						BgL_auxz00_2662 = ((BgL_localzf2infozf2_bglt) BgL_auxz00_2663);
					}
					((((BgL_localzf2infozf2_bglt) COBJECT(BgL_auxz00_2662))->
							BgL_escapez00) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				BgL_auxz00_2607 = ((BgL_localz00_bglt) BgL_new1122z00_2271);
				return ((obj_t) BgL_auxz00_2607);
			}
		}

	}



/* &lambda1381 */
	BgL_localz00_bglt BGl_z62lambda1381z62zzuncell_walkz00(obj_t BgL_envz00_2272,
		obj_t BgL_o1118z00_2273)
	{
		{	/* Uncell/walk.scm 35 */
			{	/* Uncell/walk.scm 35 */
				BgL_localzf2infozf2_bglt BgL_wide1120z00_2351;

				BgL_wide1120z00_2351 =
					((BgL_localzf2infozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_localzf2infozf2_bgl))));
				{	/* Uncell/walk.scm 35 */
					obj_t BgL_auxz00_2677;
					BgL_objectz00_bglt BgL_tmpz00_2673;

					BgL_auxz00_2677 = ((obj_t) BgL_wide1120z00_2351);
					BgL_tmpz00_2673 =
						((BgL_objectz00_bglt)
						((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1118z00_2273)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2673, BgL_auxz00_2677);
				}
				((BgL_objectz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1118z00_2273)));
				{	/* Uncell/walk.scm 35 */
					long BgL_arg1408z00_2352;

					BgL_arg1408z00_2352 =
						BGL_CLASS_NUM(BGl_localzf2infozf2zzuncell_walkz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_localz00_bglt)
								((BgL_localz00_bglt) BgL_o1118z00_2273))), BgL_arg1408z00_2352);
				}
				return
					((BgL_localz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1118z00_2273)));
			}
		}

	}



/* &lambda1378 */
	BgL_localz00_bglt BGl_z62lambda1378z62zzuncell_walkz00(obj_t BgL_envz00_2274,
		obj_t BgL_id1104z00_2275, obj_t BgL_name1105z00_2276,
		obj_t BgL_type1106z00_2277, obj_t BgL_value1107z00_2278,
		obj_t BgL_access1108z00_2279, obj_t BgL_fastzd2alpha1109zd2_2280,
		obj_t BgL_removable1110z00_2281, obj_t BgL_occurrence1111z00_2282,
		obj_t BgL_occurrencew1112z00_2283, obj_t BgL_userzf31113zf3_2284,
		obj_t BgL_key1114z00_2285, obj_t BgL_valzd2noescape1115zd2_2286,
		obj_t BgL_volatile1116z00_2287, obj_t BgL_escape1117z00_2288)
	{
		{	/* Uncell/walk.scm 35 */
			{	/* Uncell/walk.scm 35 */
				long BgL_occurrence1111z00_2356;
				long BgL_occurrencew1112z00_2357;
				bool_t BgL_userzf31113zf3_2358;
				long BgL_key1114z00_2359;
				bool_t BgL_volatile1116z00_2360;
				bool_t BgL_escape1117z00_2361;

				BgL_occurrence1111z00_2356 = (long) CINT(BgL_occurrence1111z00_2282);
				BgL_occurrencew1112z00_2357 = (long) CINT(BgL_occurrencew1112z00_2283);
				BgL_userzf31113zf3_2358 = CBOOL(BgL_userzf31113zf3_2284);
				BgL_key1114z00_2359 = (long) CINT(BgL_key1114z00_2285);
				BgL_volatile1116z00_2360 = CBOOL(BgL_volatile1116z00_2287);
				BgL_escape1117z00_2361 = CBOOL(BgL_escape1117z00_2288);
				{	/* Uncell/walk.scm 35 */
					BgL_localz00_bglt BgL_new1157z00_2362;

					{	/* Uncell/walk.scm 35 */
						BgL_localz00_bglt BgL_tmp1155z00_2363;
						BgL_localzf2infozf2_bglt BgL_wide1156z00_2364;

						{
							BgL_localz00_bglt BgL_auxz00_2697;

							{	/* Uncell/walk.scm 35 */
								BgL_localz00_bglt BgL_new1154z00_2365;

								BgL_new1154z00_2365 =
									((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_localz00_bgl))));
								{	/* Uncell/walk.scm 35 */
									long BgL_arg1380z00_2366;

									BgL_arg1380z00_2366 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1154z00_2365),
										BgL_arg1380z00_2366);
								}
								{	/* Uncell/walk.scm 35 */
									BgL_objectz00_bglt BgL_tmpz00_2702;

									BgL_tmpz00_2702 = ((BgL_objectz00_bglt) BgL_new1154z00_2365);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2702, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1154z00_2365);
								BgL_auxz00_2697 = BgL_new1154z00_2365;
							}
							BgL_tmp1155z00_2363 = ((BgL_localz00_bglt) BgL_auxz00_2697);
						}
						BgL_wide1156z00_2364 =
							((BgL_localzf2infozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_localzf2infozf2_bgl))));
						{	/* Uncell/walk.scm 35 */
							obj_t BgL_auxz00_2710;
							BgL_objectz00_bglt BgL_tmpz00_2708;

							BgL_auxz00_2710 = ((obj_t) BgL_wide1156z00_2364);
							BgL_tmpz00_2708 = ((BgL_objectz00_bglt) BgL_tmp1155z00_2363);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2708, BgL_auxz00_2710);
						}
						((BgL_objectz00_bglt) BgL_tmp1155z00_2363);
						{	/* Uncell/walk.scm 35 */
							long BgL_arg1379z00_2367;

							BgL_arg1379z00_2367 =
								BGL_CLASS_NUM(BGl_localzf2infozf2zzuncell_walkz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1155z00_2363),
								BgL_arg1379z00_2367);
						}
						BgL_new1157z00_2362 = ((BgL_localz00_bglt) BgL_tmp1155z00_2363);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1157z00_2362)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1104z00_2275)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1157z00_2362)))->BgL_namez00) =
						((obj_t) BgL_name1105z00_2276), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1157z00_2362)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1106z00_2277)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1157z00_2362)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1107z00_2278)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1157z00_2362)))->BgL_accessz00) =
						((obj_t) BgL_access1108z00_2279), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1157z00_2362)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1109zd2_2280), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1157z00_2362)))->BgL_removablez00) =
						((obj_t) BgL_removable1110z00_2281), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1157z00_2362)))->BgL_occurrencez00) =
						((long) BgL_occurrence1111z00_2356), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1157z00_2362)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1112z00_2357), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1157z00_2362)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31113zf3_2358), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1157z00_2362)))->BgL_keyz00) =
						((long) BgL_key1114z00_2359), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1157z00_2362)))->BgL_valzd2noescapezd2) =
						((obj_t) BgL_valzd2noescape1115zd2_2286), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1157z00_2362)))->BgL_volatilez00) =
						((bool_t) BgL_volatile1116z00_2360), BUNSPEC);
					{
						BgL_localzf2infozf2_bglt BgL_auxz00_2747;

						{
							obj_t BgL_auxz00_2748;

							{	/* Uncell/walk.scm 35 */
								BgL_objectz00_bglt BgL_tmpz00_2749;

								BgL_tmpz00_2749 = ((BgL_objectz00_bglt) BgL_new1157z00_2362);
								BgL_auxz00_2748 = BGL_OBJECT_WIDENING(BgL_tmpz00_2749);
							}
							BgL_auxz00_2747 = ((BgL_localzf2infozf2_bglt) BgL_auxz00_2748);
						}
						((((BgL_localzf2infozf2_bglt) COBJECT(BgL_auxz00_2747))->
								BgL_escapez00) = ((bool_t) BgL_escape1117z00_2361), BUNSPEC);
					}
					return BgL_new1157z00_2362;
				}
			}
		}

	}



/* &<@anonymous:1457> */
	obj_t BGl_z62zc3z04anonymousza31457ze3ze5zzuncell_walkz00(obj_t
		BgL_envz00_2289)
	{
		{	/* Uncell/walk.scm 35 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1456 */
	obj_t BGl_z62lambda1456z62zzuncell_walkz00(obj_t BgL_envz00_2290,
		obj_t BgL_oz00_2291, obj_t BgL_vz00_2292)
	{
		{	/* Uncell/walk.scm 35 */
			{	/* Uncell/walk.scm 35 */
				bool_t BgL_vz00_2369;

				BgL_vz00_2369 = CBOOL(BgL_vz00_2292);
				{
					BgL_localzf2infozf2_bglt BgL_auxz00_2756;

					{
						obj_t BgL_auxz00_2757;

						{	/* Uncell/walk.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_2758;

							BgL_tmpz00_2758 =
								((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_2291));
							BgL_auxz00_2757 = BGL_OBJECT_WIDENING(BgL_tmpz00_2758);
						}
						BgL_auxz00_2756 = ((BgL_localzf2infozf2_bglt) BgL_auxz00_2757);
					}
					return
						((((BgL_localzf2infozf2_bglt) COBJECT(BgL_auxz00_2756))->
							BgL_escapez00) = ((bool_t) BgL_vz00_2369), BUNSPEC);
				}
			}
		}

	}



/* &lambda1455 */
	obj_t BGl_z62lambda1455z62zzuncell_walkz00(obj_t BgL_envz00_2293,
		obj_t BgL_oz00_2294)
	{
		{	/* Uncell/walk.scm 35 */
			{	/* Uncell/walk.scm 35 */
				bool_t BgL_tmpz00_2764;

				{
					BgL_localzf2infozf2_bglt BgL_auxz00_2765;

					{
						obj_t BgL_auxz00_2766;

						{	/* Uncell/walk.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_2767;

							BgL_tmpz00_2767 =
								((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_2294));
							BgL_auxz00_2766 = BGL_OBJECT_WIDENING(BgL_tmpz00_2767);
						}
						BgL_auxz00_2765 = ((BgL_localzf2infozf2_bglt) BgL_auxz00_2766);
					}
					BgL_tmpz00_2764 =
						(((BgL_localzf2infozf2_bglt) COBJECT(BgL_auxz00_2765))->
						BgL_escapez00);
				}
				return BBOOL(BgL_tmpz00_2764);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzuncell_walkz00(void)
	{
		{	/* Uncell/walk.scm 16 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_markzd2cellzd2envz00zzuncell_walkz00,
				BGl_proc1830z00zzuncell_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string1831z00zzuncell_walkz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_uncellz12zd2envzc0zzuncell_walkz00,
				BGl_proc1832z00zzuncell_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string1833z00zzuncell_walkz00);
		}

	}



/* &uncell!1294 */
	obj_t BGl_z62uncellz121294z70zzuncell_walkz00(obj_t BgL_envz00_2301,
		obj_t BgL_nodez00_2302)
	{
		{	/* Uncell/walk.scm 104 */
			return
				((obj_t)
				BGl_walk0z12z12zzast_walkz00(
					((BgL_nodez00_bglt) BgL_nodez00_2302),
					BGl_uncellz12zd2envzc0zzuncell_walkz00));
		}

	}



/* &mark-cell1284 */
	obj_t BGl_z62markzd2cell1284zb0zzuncell_walkz00(obj_t BgL_envz00_2303,
		obj_t BgL_nodez00_2304)
	{
		{	/* Uncell/walk.scm 64 */
			return
				BGl_walk0z00zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_2304),
				BGl_markzd2cellzd2envz00zzuncell_walkz00);
		}

	}



/* mark-cell */
	obj_t BGl_markzd2cellzd2zzuncell_walkz00(BgL_nodez00_bglt BgL_nodez00_19)
	{
		{	/* Uncell/walk.scm 64 */
			{	/* Uncell/walk.scm 64 */
				obj_t BgL_method1285z00_1626;

				{	/* Uncell/walk.scm 64 */
					obj_t BgL_res1806z00_2050;

					{	/* Uncell/walk.scm 64 */
						long BgL_objzd2classzd2numz00_2021;

						BgL_objzd2classzd2numz00_2021 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_19));
						{	/* Uncell/walk.scm 64 */
							obj_t BgL_arg1811z00_2022;

							BgL_arg1811z00_2022 =
								PROCEDURE_REF(BGl_markzd2cellzd2envz00zzuncell_walkz00,
								(int) (1L));
							{	/* Uncell/walk.scm 64 */
								int BgL_offsetz00_2025;

								BgL_offsetz00_2025 = (int) (BgL_objzd2classzd2numz00_2021);
								{	/* Uncell/walk.scm 64 */
									long BgL_offsetz00_2026;

									BgL_offsetz00_2026 =
										((long) (BgL_offsetz00_2025) - OBJECT_TYPE);
									{	/* Uncell/walk.scm 64 */
										long BgL_modz00_2027;

										BgL_modz00_2027 =
											(BgL_offsetz00_2026 >> (int) ((long) ((int) (4L))));
										{	/* Uncell/walk.scm 64 */
											long BgL_restz00_2029;

											BgL_restz00_2029 =
												(BgL_offsetz00_2026 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Uncell/walk.scm 64 */

												{	/* Uncell/walk.scm 64 */
													obj_t BgL_bucketz00_2031;

													BgL_bucketz00_2031 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2022), BgL_modz00_2027);
													BgL_res1806z00_2050 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2031), BgL_restz00_2029);
					}}}}}}}}
					BgL_method1285z00_1626 = BgL_res1806z00_2050;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1285z00_1626, ((obj_t) BgL_nodez00_19));
			}
		}

	}



/* &mark-cell */
	obj_t BGl_z62markzd2cellzb0zzuncell_walkz00(obj_t BgL_envz00_2299,
		obj_t BgL_nodez00_2300)
	{
		{	/* Uncell/walk.scm 64 */
			return
				BGl_markzd2cellzd2zzuncell_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_2300));
		}

	}



/* uncell! */
	BgL_nodez00_bglt BGl_uncellz12z12zzuncell_walkz00(BgL_nodez00_bglt
		BgL_nodez00_24)
	{
		{	/* Uncell/walk.scm 104 */
			{	/* Uncell/walk.scm 104 */
				obj_t BgL_method1295z00_1627;

				{	/* Uncell/walk.scm 104 */
					obj_t BgL_res1811z00_2081;

					{	/* Uncell/walk.scm 104 */
						long BgL_objzd2classzd2numz00_2052;

						BgL_objzd2classzd2numz00_2052 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_24));
						{	/* Uncell/walk.scm 104 */
							obj_t BgL_arg1811z00_2053;

							BgL_arg1811z00_2053 =
								PROCEDURE_REF(BGl_uncellz12zd2envzc0zzuncell_walkz00,
								(int) (1L));
							{	/* Uncell/walk.scm 104 */
								int BgL_offsetz00_2056;

								BgL_offsetz00_2056 = (int) (BgL_objzd2classzd2numz00_2052);
								{	/* Uncell/walk.scm 104 */
									long BgL_offsetz00_2057;

									BgL_offsetz00_2057 =
										((long) (BgL_offsetz00_2056) - OBJECT_TYPE);
									{	/* Uncell/walk.scm 104 */
										long BgL_modz00_2058;

										BgL_modz00_2058 =
											(BgL_offsetz00_2057 >> (int) ((long) ((int) (4L))));
										{	/* Uncell/walk.scm 104 */
											long BgL_restz00_2060;

											BgL_restz00_2060 =
												(BgL_offsetz00_2057 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Uncell/walk.scm 104 */

												{	/* Uncell/walk.scm 104 */
													obj_t BgL_bucketz00_2062;

													BgL_bucketz00_2062 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2053), BgL_modz00_2058);
													BgL_res1811z00_2081 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2062), BgL_restz00_2060);
					}}}}}}}}
					BgL_method1295z00_1627 = BgL_res1811z00_2081;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1295z00_1627,
						((obj_t) BgL_nodez00_24)));
			}
		}

	}



/* &uncell! */
	BgL_nodez00_bglt BGl_z62uncellz12z70zzuncell_walkz00(obj_t BgL_envz00_2296,
		obj_t BgL_nodez00_2297)
	{
		{	/* Uncell/walk.scm 104 */
			return
				BGl_uncellz12z12zzuncell_walkz00(((BgL_nodez00_bglt) BgL_nodez00_2297));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzuncell_walkz00(void)
	{
		{	/* Uncell/walk.scm 16 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_markzd2cellzd2envz00zzuncell_walkz00, BGl_varz00zzast_nodez00,
				BGl_proc1834z00zzuncell_walkz00, BGl_string1835z00zzuncell_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_markzd2cellzd2envz00zzuncell_walkz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1836z00zzuncell_walkz00,
				BGl_string1835z00zzuncell_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_markzd2cellzd2envz00zzuncell_walkz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1837z00zzuncell_walkz00,
				BGl_string1835z00zzuncell_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_markzd2cellzd2envz00zzuncell_walkz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1838z00zzuncell_walkz00,
				BGl_string1835z00zzuncell_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncellz12zd2envzc0zzuncell_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc1839z00zzuncell_walkz00, BGl_string1840z00zzuncell_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncellz12zd2envzc0zzuncell_walkz00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc1841z00zzuncell_walkz00, BGl_string1840z00zzuncell_walkz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncellz12zd2envzc0zzuncell_walkz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1842z00zzuncell_walkz00,
				BGl_string1840z00zzuncell_walkz00);
		}

	}



/* &uncell!-box-set!1301 */
	BgL_nodez00_bglt BGl_z62uncellz12zd2boxzd2setz121301z62zzuncell_walkz00(obj_t
		BgL_envz00_2312, obj_t BgL_nodez00_2313)
	{
		{	/* Uncell/walk.scm 137 */
			{	/* Uncell/walk.scm 138 */
				BgL_variablez00_bglt BgL_varz00_2374;

				BgL_varz00_2374 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
										((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2313)))->
								BgL_varz00)))->BgL_variablez00);
				{	/* Uncell/walk.scm 139 */
					bool_t BgL_test1892z00_2856;

					{	/* Uncell/walk.scm 139 */
						obj_t BgL_classz00_2375;

						BgL_classz00_2375 = BGl_localzf2infozf2zzuncell_walkz00;
						{	/* Uncell/walk.scm 139 */
							obj_t BgL_oclassz00_2376;

							{	/* Uncell/walk.scm 139 */
								obj_t BgL_arg1815z00_2377;
								long BgL_arg1816z00_2378;

								BgL_arg1815z00_2377 = (BGl_za2classesza2z00zz__objectz00);
								{	/* Uncell/walk.scm 139 */
									long BgL_arg1817z00_2379;

									BgL_arg1817z00_2379 =
										BGL_OBJECT_CLASS_NUM(
										((BgL_objectz00_bglt) BgL_varz00_2374));
									BgL_arg1816z00_2378 = (BgL_arg1817z00_2379 - OBJECT_TYPE);
								}
								BgL_oclassz00_2376 =
									VECTOR_REF(BgL_arg1815z00_2377, BgL_arg1816z00_2378);
							}
							BgL_test1892z00_2856 = (BgL_oclassz00_2376 == BgL_classz00_2375);
					}}
					if (BgL_test1892z00_2856)
						{	/* Uncell/walk.scm 141 */
							bool_t BgL_test1893z00_2863;

							{
								BgL_localzf2infozf2_bglt BgL_auxz00_2864;

								{
									obj_t BgL_auxz00_2865;

									{	/* Uncell/walk.scm 141 */
										BgL_objectz00_bglt BgL_tmpz00_2866;

										BgL_tmpz00_2866 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt) BgL_varz00_2374));
										BgL_auxz00_2865 = BGL_OBJECT_WIDENING(BgL_tmpz00_2866);
									}
									BgL_auxz00_2864 =
										((BgL_localzf2infozf2_bglt) BgL_auxz00_2865);
								}
								BgL_test1893z00_2863 =
									(((BgL_localzf2infozf2_bglt) COBJECT(BgL_auxz00_2864))->
									BgL_escapez00);
							}
							if (BgL_test1893z00_2863)
								{	/* Uncell/walk.scm 141 */
									return
										BGl_walk0z12z12zzast_walkz00(
										((BgL_nodez00_bglt)
											((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2313)),
										BGl_uncellz12zd2envzc0zzuncell_walkz00);
								}
							else
								{	/* Uncell/walk.scm 143 */
									BgL_setqz00_bglt BgL_new1153z00_2380;

									{	/* Uncell/walk.scm 143 */
										BgL_setqz00_bglt BgL_new1152z00_2381;

										BgL_new1152z00_2381 =
											((BgL_setqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_setqz00_bgl))));
										{	/* Uncell/walk.scm 143 */
											long BgL_arg1646z00_2382;

											BgL_arg1646z00_2382 =
												BGL_CLASS_NUM(BGl_setqz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1152z00_2381),
												BgL_arg1646z00_2382);
										}
										{	/* Uncell/walk.scm 143 */
											BgL_objectz00_bglt BgL_tmpz00_2879;

											BgL_tmpz00_2879 =
												((BgL_objectz00_bglt) BgL_new1152z00_2381);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2879, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1152z00_2381);
										BgL_new1153z00_2380 = BgL_new1152z00_2381;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1153z00_2380)))->
											BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1153z00_2380)))->BgL_typez00) =
										((BgL_typez00_bglt) ((BgL_typez00_bglt)
												BGl_za2unspecza2z00zztype_cachez00)), BUNSPEC);
									((((BgL_setqz00_bglt) COBJECT(BgL_new1153z00_2380))->
											BgL_varz00) =
										((BgL_varz00_bglt) (((BgL_boxzd2setz12zc0_bglt)
													COBJECT(((BgL_boxzd2setz12zc0_bglt)
															BgL_nodez00_2313)))->BgL_varz00)), BUNSPEC);
									{
										BgL_nodez00_bglt BgL_auxz00_2891;

										{	/* Uncell/walk.scm 146 */
											BgL_nodez00_bglt BgL_arg1642z00_2383;

											BgL_arg1642z00_2383 =
												(((BgL_boxzd2setz12zc0_bglt) COBJECT(
														((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2313)))->
												BgL_valuez00);
											BgL_auxz00_2891 =
												BGl_uncellz12z12zzuncell_walkz00(BgL_arg1642z00_2383);
										}
										((((BgL_setqz00_bglt) COBJECT(BgL_new1153z00_2380))->
												BgL_valuez00) =
											((BgL_nodez00_bglt) BgL_auxz00_2891), BUNSPEC);
									}
									return ((BgL_nodez00_bglt) BgL_new1153z00_2380);
								}
						}
					else
						{	/* Uncell/walk.scm 139 */
							return
								BGl_walk0z12z12zzast_walkz00(
								((BgL_nodez00_bglt)
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2313)),
								BGl_uncellz12zd2envzc0zzuncell_walkz00);
						}
				}
			}
		}

	}



/* &uncell!-box-ref1299 */
	BgL_nodez00_bglt BGl_z62uncellz12zd2boxzd2ref1299z70zzuncell_walkz00(obj_t
		BgL_envz00_2314, obj_t BgL_nodez00_2315)
	{
		{	/* Uncell/walk.scm 125 */
			{	/* Uncell/walk.scm 126 */
				BgL_variablez00_bglt BgL_varz00_2385;

				BgL_varz00_2385 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_boxzd2refzd2_bglt) COBJECT(
										((BgL_boxzd2refzd2_bglt) BgL_nodez00_2315)))->
								BgL_varz00)))->BgL_variablez00);
				{	/* Uncell/walk.scm 127 */
					bool_t BgL_test1894z00_2903;

					{	/* Uncell/walk.scm 127 */
						obj_t BgL_classz00_2386;

						BgL_classz00_2386 = BGl_localzf2infozf2zzuncell_walkz00;
						{	/* Uncell/walk.scm 127 */
							obj_t BgL_oclassz00_2387;

							{	/* Uncell/walk.scm 127 */
								obj_t BgL_arg1815z00_2388;
								long BgL_arg1816z00_2389;

								BgL_arg1815z00_2388 = (BGl_za2classesza2z00zz__objectz00);
								{	/* Uncell/walk.scm 127 */
									long BgL_arg1817z00_2390;

									BgL_arg1817z00_2390 =
										BGL_OBJECT_CLASS_NUM(
										((BgL_objectz00_bglt) BgL_varz00_2385));
									BgL_arg1816z00_2389 = (BgL_arg1817z00_2390 - OBJECT_TYPE);
								}
								BgL_oclassz00_2387 =
									VECTOR_REF(BgL_arg1815z00_2388, BgL_arg1816z00_2389);
							}
							BgL_test1894z00_2903 = (BgL_oclassz00_2387 == BgL_classz00_2386);
					}}
					if (BgL_test1894z00_2903)
						{	/* Uncell/walk.scm 129 */
							bool_t BgL_test1895z00_2910;

							{
								BgL_localzf2infozf2_bglt BgL_auxz00_2911;

								{
									obj_t BgL_auxz00_2912;

									{	/* Uncell/walk.scm 129 */
										BgL_objectz00_bglt BgL_tmpz00_2913;

										BgL_tmpz00_2913 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt) BgL_varz00_2385));
										BgL_auxz00_2912 = BGL_OBJECT_WIDENING(BgL_tmpz00_2913);
									}
									BgL_auxz00_2911 =
										((BgL_localzf2infozf2_bglt) BgL_auxz00_2912);
								}
								BgL_test1895z00_2910 =
									(((BgL_localzf2infozf2_bglt) COBJECT(BgL_auxz00_2911))->
									BgL_escapez00);
							}
							if (BgL_test1895z00_2910)
								{	/* Uncell/walk.scm 129 */
									return
										((BgL_nodez00_bglt)
										((BgL_boxzd2refzd2_bglt) BgL_nodez00_2315));
								}
							else
								{	/* Uncell/walk.scm 129 */
									return
										((BgL_nodez00_bglt)
										(((BgL_boxzd2refzd2_bglt) COBJECT(
													((BgL_boxzd2refzd2_bglt) BgL_nodez00_2315)))->
											BgL_varz00));
								}
						}
					else
						{	/* Uncell/walk.scm 127 */
							return
								((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2315));
						}
				}
			}
		}

	}



/* &uncell!-let-var1297 */
	BgL_nodez00_bglt BGl_z62uncellz12zd2letzd2var1297z70zzuncell_walkz00(obj_t
		BgL_envz00_2316, obj_t BgL_nodez00_2317)
	{
		{	/* Uncell/walk.scm 110 */
			{	/* Uncell/walk.scm 112 */
				obj_t BgL_g1281z00_2392;

				BgL_g1281z00_2392 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2317)))->BgL_bindingsz00);
				{
					obj_t BgL_l1279z00_2394;

					BgL_l1279z00_2394 = BgL_g1281z00_2392;
				BgL_zc3z04anonymousza31575ze3z87_2393:
					if (PAIRP(BgL_l1279z00_2394))
						{	/* Uncell/walk.scm 112 */
							{	/* Uncell/walk.scm 113 */
								obj_t BgL_bindingz00_2395;

								BgL_bindingz00_2395 = CAR(BgL_l1279z00_2394);
								{	/* Uncell/walk.scm 113 */
									bool_t BgL_test1897z00_2931;

									if (
										((((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															CAR(
																((obj_t) BgL_bindingz00_2395)))))->
												BgL_accessz00) == CNST_TABLE_REF(6)))
										{	/* Uncell/walk.scm 114 */
											obj_t BgL_arg1606z00_2396;

											BgL_arg1606z00_2396 = CDR(((obj_t) BgL_bindingz00_2395));
											{	/* Uncell/walk.scm 114 */
												obj_t BgL_classz00_2397;

												BgL_classz00_2397 = BGl_makezd2boxzd2zzast_nodez00;
												if (BGL_OBJECTP(BgL_arg1606z00_2396))
													{	/* Uncell/walk.scm 114 */
														BgL_objectz00_bglt BgL_arg1807z00_2398;

														BgL_arg1807z00_2398 =
															(BgL_objectz00_bglt) (BgL_arg1606z00_2396);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Uncell/walk.scm 114 */
																long BgL_idxz00_2399;

																BgL_idxz00_2399 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_2398);
																BgL_test1897z00_2931 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_2399 + 3L)) ==
																	BgL_classz00_2397);
															}
														else
															{	/* Uncell/walk.scm 114 */
																bool_t BgL_res1813z00_2402;

																{	/* Uncell/walk.scm 114 */
																	obj_t BgL_oclassz00_2403;

																	{	/* Uncell/walk.scm 114 */
																		obj_t BgL_arg1815z00_2404;
																		long BgL_arg1816z00_2405;

																		BgL_arg1815z00_2404 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Uncell/walk.scm 114 */
																			long BgL_arg1817z00_2406;

																			BgL_arg1817z00_2406 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_2398);
																			BgL_arg1816z00_2405 =
																				(BgL_arg1817z00_2406 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_2403 =
																			VECTOR_REF(BgL_arg1815z00_2404,
																			BgL_arg1816z00_2405);
																	}
																	{	/* Uncell/walk.scm 114 */
																		bool_t BgL__ortest_1115z00_2407;

																		BgL__ortest_1115z00_2407 =
																			(BgL_classz00_2397 == BgL_oclassz00_2403);
																		if (BgL__ortest_1115z00_2407)
																			{	/* Uncell/walk.scm 114 */
																				BgL_res1813z00_2402 =
																					BgL__ortest_1115z00_2407;
																			}
																		else
																			{	/* Uncell/walk.scm 114 */
																				long BgL_odepthz00_2408;

																				{	/* Uncell/walk.scm 114 */
																					obj_t BgL_arg1804z00_2409;

																					BgL_arg1804z00_2409 =
																						(BgL_oclassz00_2403);
																					BgL_odepthz00_2408 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_2409);
																				}
																				if ((3L < BgL_odepthz00_2408))
																					{	/* Uncell/walk.scm 114 */
																						obj_t BgL_arg1802z00_2410;

																						{	/* Uncell/walk.scm 114 */
																							obj_t BgL_arg1803z00_2411;

																							BgL_arg1803z00_2411 =
																								(BgL_oclassz00_2403);
																							BgL_arg1802z00_2410 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_2411, 3L);
																						}
																						BgL_res1813z00_2402 =
																							(BgL_arg1802z00_2410 ==
																							BgL_classz00_2397);
																					}
																				else
																					{	/* Uncell/walk.scm 114 */
																						BgL_res1813z00_2402 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test1897z00_2931 = BgL_res1813z00_2402;
															}
													}
												else
													{	/* Uncell/walk.scm 114 */
														BgL_test1897z00_2931 = ((bool_t) 0);
													}
											}
										}
									else
										{	/* Uncell/walk.scm 113 */
											BgL_test1897z00_2931 = ((bool_t) 0);
										}
									if (BgL_test1897z00_2931)
										{	/* Uncell/walk.scm 115 */
											BgL_localz00_bglt BgL_i1149z00_2412;

											BgL_i1149z00_2412 =
												((BgL_localz00_bglt)
												CAR(((obj_t) BgL_bindingz00_2395)));
											{	/* Uncell/walk.scm 116 */
												bool_t BgL_test1903z00_2966;

												{
													BgL_localzf2infozf2_bglt BgL_auxz00_2967;

													{
														obj_t BgL_auxz00_2968;

														{	/* Uncell/walk.scm 116 */
															BgL_objectz00_bglt BgL_tmpz00_2969;

															BgL_tmpz00_2969 =
																((BgL_objectz00_bglt) BgL_i1149z00_2412);
															BgL_auxz00_2968 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2969);
														}
														BgL_auxz00_2967 =
															((BgL_localzf2infozf2_bglt) BgL_auxz00_2968);
													}
													BgL_test1903z00_2966 =
														(((BgL_localzf2infozf2_bglt)
															COBJECT(BgL_auxz00_2967))->BgL_escapez00);
												}
												if (BgL_test1903z00_2966)
													{	/* Uncell/walk.scm 116 */
														BFALSE;
													}
												else
													{	/* Uncell/walk.scm 116 */
														{	/* Uncell/walk.scm 117 */
															BgL_nodez00_bglt BgL_arg1602z00_2413;

															BgL_arg1602z00_2413 =
																(((BgL_makezd2boxzd2_bglt) COBJECT(
																		((BgL_makezd2boxzd2_bglt)
																			CDR(
																				((obj_t) BgL_bindingz00_2395)))))->
																BgL_valuez00);
															{	/* Uncell/walk.scm 117 */
																obj_t BgL_auxz00_2980;
																obj_t BgL_tmpz00_2978;

																BgL_auxz00_2980 = ((obj_t) BgL_arg1602z00_2413);
																BgL_tmpz00_2978 = ((obj_t) BgL_bindingz00_2395);
																SET_CDR(BgL_tmpz00_2978, BgL_auxz00_2980);
															}
														}
														((((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			BgL_i1149z00_2412)))->BgL_typez00) =
															((BgL_typez00_bglt) ((BgL_typez00_bglt)
																	BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
													}
											}
										}
									else
										{	/* Uncell/walk.scm 113 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1279z00_2986;

								BgL_l1279z00_2986 = CDR(BgL_l1279z00_2394);
								BgL_l1279z00_2394 = BgL_l1279z00_2986;
								goto BgL_zc3z04anonymousza31575ze3z87_2393;
							}
						}
					else
						{	/* Uncell/walk.scm 112 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_walk0z12z12zzast_walkz00(
				((BgL_nodez00_bglt)
					((BgL_letzd2varzd2_bglt) BgL_nodez00_2317)),
				BGl_uncellz12zd2envzc0zzuncell_walkz00);
		}

	}



/* &mark-cell-box-set!1293 */
	obj_t BGl_z62markzd2cellzd2boxzd2setz121293za2zzuncell_walkz00(obj_t
		BgL_envz00_2318, obj_t BgL_nodez00_2319)
	{
		{	/* Uncell/walk.scm 96 */
			return
				BGl_markzd2cellzd2zzuncell_walkz00(
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2319)))->BgL_valuez00));
		}

	}



/* &mark-cell-box-ref1291 */
	obj_t BGl_z62markzd2cellzd2boxzd2ref1291zb0zzuncell_walkz00(obj_t
		BgL_envz00_2320, obj_t BgL_nodez00_2321)
	{
		{	/* Uncell/walk.scm 90 */
			return ((obj_t) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2321));
		}

	}



/* &mark-cell-let-var1289 */
	obj_t BGl_z62markzd2cellzd2letzd2var1289zb0zzuncell_walkz00(obj_t
		BgL_envz00_2322, obj_t BgL_nodez00_2323)
	{
		{	/* Uncell/walk.scm 78 */
			{	/* Uncell/walk.scm 80 */
				obj_t BgL_g1278z00_2417;

				BgL_g1278z00_2417 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2323)))->BgL_bindingsz00);
				{
					obj_t BgL_l1276z00_2419;

					BgL_l1276z00_2419 = BgL_g1278z00_2417;
				BgL_zc3z04anonymousza31506ze3z87_2418:
					if (PAIRP(BgL_l1276z00_2419))
						{	/* Uncell/walk.scm 80 */
							{	/* Uncell/walk.scm 81 */
								obj_t BgL_bindingz00_2420;

								BgL_bindingz00_2420 = CAR(BgL_l1276z00_2419);
								{	/* Uncell/walk.scm 81 */
									bool_t BgL_test1905z00_3001;

									if (
										((((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															CAR(
																((obj_t) BgL_bindingz00_2420)))))->
												BgL_accessz00) == CNST_TABLE_REF(6)))
										{	/* Uncell/walk.scm 82 */
											obj_t BgL_arg1546z00_2421;

											BgL_arg1546z00_2421 = CDR(((obj_t) BgL_bindingz00_2420));
											{	/* Uncell/walk.scm 82 */
												obj_t BgL_classz00_2422;

												BgL_classz00_2422 = BGl_makezd2boxzd2zzast_nodez00;
												if (BGL_OBJECTP(BgL_arg1546z00_2421))
													{	/* Uncell/walk.scm 82 */
														BgL_objectz00_bglt BgL_arg1807z00_2423;

														BgL_arg1807z00_2423 =
															(BgL_objectz00_bglt) (BgL_arg1546z00_2421);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Uncell/walk.scm 82 */
																long BgL_idxz00_2424;

																BgL_idxz00_2424 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_2423);
																BgL_test1905z00_3001 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_2424 + 3L)) ==
																	BgL_classz00_2422);
															}
														else
															{	/* Uncell/walk.scm 82 */
																bool_t BgL_res1812z00_2427;

																{	/* Uncell/walk.scm 82 */
																	obj_t BgL_oclassz00_2428;

																	{	/* Uncell/walk.scm 82 */
																		obj_t BgL_arg1815z00_2429;
																		long BgL_arg1816z00_2430;

																		BgL_arg1815z00_2429 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Uncell/walk.scm 82 */
																			long BgL_arg1817z00_2431;

																			BgL_arg1817z00_2431 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_2423);
																			BgL_arg1816z00_2430 =
																				(BgL_arg1817z00_2431 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_2428 =
																			VECTOR_REF(BgL_arg1815z00_2429,
																			BgL_arg1816z00_2430);
																	}
																	{	/* Uncell/walk.scm 82 */
																		bool_t BgL__ortest_1115z00_2432;

																		BgL__ortest_1115z00_2432 =
																			(BgL_classz00_2422 == BgL_oclassz00_2428);
																		if (BgL__ortest_1115z00_2432)
																			{	/* Uncell/walk.scm 82 */
																				BgL_res1812z00_2427 =
																					BgL__ortest_1115z00_2432;
																			}
																		else
																			{	/* Uncell/walk.scm 82 */
																				long BgL_odepthz00_2433;

																				{	/* Uncell/walk.scm 82 */
																					obj_t BgL_arg1804z00_2434;

																					BgL_arg1804z00_2434 =
																						(BgL_oclassz00_2428);
																					BgL_odepthz00_2433 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_2434);
																				}
																				if ((3L < BgL_odepthz00_2433))
																					{	/* Uncell/walk.scm 82 */
																						obj_t BgL_arg1802z00_2435;

																						{	/* Uncell/walk.scm 82 */
																							obj_t BgL_arg1803z00_2436;

																							BgL_arg1803z00_2436 =
																								(BgL_oclassz00_2428);
																							BgL_arg1802z00_2435 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_2436, 3L);
																						}
																						BgL_res1812z00_2427 =
																							(BgL_arg1802z00_2435 ==
																							BgL_classz00_2422);
																					}
																				else
																					{	/* Uncell/walk.scm 82 */
																						BgL_res1812z00_2427 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test1905z00_3001 = BgL_res1812z00_2427;
															}
													}
												else
													{	/* Uncell/walk.scm 82 */
														BgL_test1905z00_3001 = ((bool_t) 0);
													}
											}
										}
									else
										{	/* Uncell/walk.scm 81 */
											BgL_test1905z00_3001 = ((bool_t) 0);
										}
									if (BgL_test1905z00_3001)
										{	/* Uncell/walk.scm 83 */
											BgL_localz00_bglt BgL_tmp1144z00_2437;

											BgL_tmp1144z00_2437 =
												((BgL_localz00_bglt)
												CAR(((obj_t) BgL_bindingz00_2420)));
											{	/* Uncell/walk.scm 83 */
												BgL_localzf2infozf2_bglt BgL_wide1146z00_2438;

												BgL_wide1146z00_2438 =
													((BgL_localzf2infozf2_bglt)
													BOBJECT(GC_MALLOC(sizeof(struct
																BgL_localzf2infozf2_bgl))));
												{	/* Uncell/walk.scm 83 */
													obj_t BgL_auxz00_3040;
													BgL_objectz00_bglt BgL_tmpz00_3037;

													BgL_auxz00_3040 = ((obj_t) BgL_wide1146z00_2438);
													BgL_tmpz00_3037 =
														((BgL_objectz00_bglt)
														((BgL_localz00_bglt) BgL_tmp1144z00_2437));
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3037,
														BgL_auxz00_3040);
												}
												((BgL_objectz00_bglt)
													((BgL_localz00_bglt) BgL_tmp1144z00_2437));
												{	/* Uncell/walk.scm 83 */
													long BgL_arg1544z00_2439;

													BgL_arg1544z00_2439 =
														BGL_CLASS_NUM(BGl_localzf2infozf2zzuncell_walkz00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt)
															((BgL_localz00_bglt) BgL_tmp1144z00_2437)),
														BgL_arg1544z00_2439);
												}
												((BgL_localz00_bglt)
													((BgL_localz00_bglt) BgL_tmp1144z00_2437));
											}
											{
												BgL_localzf2infozf2_bglt BgL_auxz00_3051;

												{
													obj_t BgL_auxz00_3052;

													{	/* Uncell/walk.scm 83 */
														BgL_objectz00_bglt BgL_tmpz00_3053;

														BgL_tmpz00_3053 =
															((BgL_objectz00_bglt)
															((BgL_localz00_bglt) BgL_tmp1144z00_2437));
														BgL_auxz00_3052 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3053);
													}
													BgL_auxz00_3051 =
														((BgL_localzf2infozf2_bglt) BgL_auxz00_3052);
												}
												((((BgL_localzf2infozf2_bglt)
															COBJECT(BgL_auxz00_3051))->BgL_escapez00) =
													((bool_t) ((bool_t) 0)), BUNSPEC);
											}
											((obj_t) ((BgL_localz00_bglt) BgL_tmp1144z00_2437));
										}
									else
										{	/* Uncell/walk.scm 81 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1276z00_3061;

								BgL_l1276z00_3061 = CDR(BgL_l1276z00_2419);
								BgL_l1276z00_2419 = BgL_l1276z00_3061;
								goto BgL_zc3z04anonymousza31506ze3z87_2418;
							}
						}
					else
						{	/* Uncell/walk.scm 80 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_walk0z00zzast_walkz00(
				((BgL_nodez00_bglt)
					((BgL_letzd2varzd2_bglt) BgL_nodez00_2323)),
				BGl_markzd2cellzd2envz00zzuncell_walkz00);
		}

	}



/* &mark-cell-var1287 */
	obj_t BGl_z62markzd2cellzd2var1287z62zzuncell_walkz00(obj_t BgL_envz00_2324,
		obj_t BgL_nodez00_2325)
	{
		{	/* Uncell/walk.scm 70 */
			{	/* Uncell/walk.scm 71 */
				bool_t BgL_test1911z00_3066;

				{	/* Uncell/walk.scm 71 */
					BgL_variablez00_bglt BgL_arg1502z00_2441;

					BgL_arg1502z00_2441 =
						(((BgL_varz00_bglt) COBJECT(
								((BgL_varz00_bglt) BgL_nodez00_2325)))->BgL_variablez00);
					{	/* Uncell/walk.scm 71 */
						obj_t BgL_classz00_2442;

						BgL_classz00_2442 = BGl_localzf2infozf2zzuncell_walkz00;
						{	/* Uncell/walk.scm 71 */
							obj_t BgL_oclassz00_2443;

							{	/* Uncell/walk.scm 71 */
								obj_t BgL_arg1815z00_2444;
								long BgL_arg1816z00_2445;

								BgL_arg1815z00_2444 = (BGl_za2classesza2z00zz__objectz00);
								{	/* Uncell/walk.scm 71 */
									long BgL_arg1817z00_2446;

									BgL_arg1817z00_2446 =
										BGL_OBJECT_CLASS_NUM(
										((BgL_objectz00_bglt) BgL_arg1502z00_2441));
									BgL_arg1816z00_2445 = (BgL_arg1817z00_2446 - OBJECT_TYPE);
								}
								BgL_oclassz00_2443 =
									VECTOR_REF(BgL_arg1815z00_2444, BgL_arg1816z00_2445);
							}
							BgL_test1911z00_3066 = (BgL_oclassz00_2443 == BgL_classz00_2442);
				}}}
				if (BgL_test1911z00_3066)
					{	/* Uncell/walk.scm 72 */
						BgL_localz00_bglt BgL_i1142z00_2447;

						BgL_i1142z00_2447 =
							((BgL_localz00_bglt)
							(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_nodez00_2325)))->BgL_variablez00));
						{
							BgL_localzf2infozf2_bglt BgL_auxz00_3078;

							{
								obj_t BgL_auxz00_3079;

								{	/* Uncell/walk.scm 73 */
									BgL_objectz00_bglt BgL_tmpz00_3080;

									BgL_tmpz00_3080 = ((BgL_objectz00_bglt) BgL_i1142z00_2447);
									BgL_auxz00_3079 = BGL_OBJECT_WIDENING(BgL_tmpz00_3080);
								}
								BgL_auxz00_3078 = ((BgL_localzf2infozf2_bglt) BgL_auxz00_3079);
							}
							return
								((((BgL_localzf2infozf2_bglt) COBJECT(BgL_auxz00_3078))->
									BgL_escapez00) = ((bool_t) ((bool_t) 1)), BUNSPEC);
						}
					}
				else
					{	/* Uncell/walk.scm 71 */
						return BFALSE;
					}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzuncell_walkz00(void)
	{
		{	/* Uncell/walk.scm 16 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zzast_walkz00(343174225L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1843z00zzuncell_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
