/*===========================================================================*/
/*   (Tailc/walk.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Tailc/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TAILC_WALK_TYPE_DEFINITIONS
#define BGL_TAILC_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_localzd2tailzd2_bgl
	{
	}                      *BgL_localzd2tailzd2_bglt;


#endif													// BGL_TAILC_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62getzd2tailzd2callszd2seque1315zb0zztailc_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_globalzd2ze3localz12z23zztailc_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_tailczd2walkz12zc0zztailc_walkz00(obj_t);
	static obj_t BGl_z62getzd2tailzd2callszd2sync1317zb0zztailc_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zztailc_walkz00 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	extern obj_t BGl_funz00zzast_varz00;
	static obj_t BGl_z62getzd2tailzd2callszd2switc1323zb0zztailc_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_toplevelzd2initzd2zztailc_walkz00(void);
	static obj_t BGl_getzd2tailzd2callsz00zztailc_walkz00(BgL_nodez00_bglt,
		BgL_variablez00_bglt, obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zztailc_walkz00(void);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	static obj_t BGl_objectzd2initzd2zztailc_walkz00(void);
	static obj_t BGl_z62getzd2tailzd2calls1310z62zztailc_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_typez00zztype_typez00;
	extern BgL_localz00_bglt BGl_clonezd2localzd2zzast_localz00(BgL_localz00_bglt,
		BgL_valuez00_bglt);
	static obj_t BGl_tailczd2globalzd2zztailc_walkz00(obj_t);
	static obj_t BGl_z62tailczd2walkz12za2zztailc_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	extern BgL_localz00_bglt BGl_makezd2localzd2sfunz00zzast_localz00(obj_t,
		BgL_typez00_bglt, BgL_sfunz00_bglt);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztailc_walkz00(void);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static obj_t BGl_localzd2tailzd2zztailc_walkz00 = BUNSPEC;
	static obj_t BGl_z62getzd2tailzd2callszd2condi1321zb0zztailc_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_svarz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztailc_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	static obj_t BGl_z62getzd2tailzd2callszd2letzd2f1325z62zztailc_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_cnstzd2initzd2zztailc_walkz00(void);
	static obj_t BGl_z62getzd2tailzd2callszd2letzd2v1327z62zztailc_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zztailc_walkz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zztailc_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zztailc_walkz00(void);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_valuez00zzast_varz00;
	static obj_t BGl_z62zc3z04anonymousza31565ze3ze5zztailc_walkz00(obj_t, obj_t);
	static BgL_localz00_bglt BGl_z62lambda1553z62zztailc_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static obj_t BGl_z62getzd2tailzd2callszd2closu1313zb0zztailc_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62getzd2tailzd2callszd2app1319zb0zztailc_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static BgL_localz00_bglt BGl_z62lambda1562z62zztailc_walkz00(obj_t, obj_t);
	static BgL_localz00_bglt BGl_z62lambda1566z62zztailc_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t BGl_z62getzd2tailzd2callsz62zztailc_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t __cnst[4];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tailczd2walkz12zd2envz12zztailc_walkz00,
		BgL_bgl_za762tailcza7d2walkza71809za7,
		BGl_z62tailczd2walkz12za2zztailc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1802z00zztailc_walkz00,
		BgL_bgl_string1802za700za7za7t1810za7, "Unexpected closure", 18);
	      DEFINE_STRING(BGl_string1803z00zztailc_walkz00,
		BgL_bgl_string1803za700za7za7t1811za7, "tailc_walk", 10);
	      DEFINE_STRING(BGl_string1804z00zztailc_walkz00,
		BgL_bgl_string1804za700za7za7t1812za7,
		"_ tailc_walk local-tail pass-started ", 37);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1800z00zztailc_walkz00,
		BgL_bgl_za762getza7d2tailza7d21813za7,
		BGl_z62getzd2tailzd2callszd2letzd2f1325z62zztailc_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1801z00zztailc_walkz00,
		BgL_bgl_za762getza7d2tailza7d21814za7,
		BGl_z62getzd2tailzd2callszd2letzd2v1327z62zztailc_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1781z00zztailc_walkz00,
		BgL_bgl_string1781za700za7za7t1815za7, "Tailc", 5);
	      DEFINE_STRING(BGl_string1782z00zztailc_walkz00,
		BgL_bgl_string1782za700za7za7t1816za7, "   . ", 5);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_getzd2tailzd2callszd2envzd2zztailc_walkz00,
		BgL_bgl_za762getza7d2tailza7d21817za7,
		BGl_z62getzd2tailzd2callsz62zztailc_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1783z00zztailc_walkz00,
		BgL_bgl_string1783za700za7za7t1818za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1784z00zztailc_walkz00,
		BgL_bgl_string1784za700za7za7t1819za7, " self tail call(s)", 18);
	      DEFINE_STRING(BGl_string1785z00zztailc_walkz00,
		BgL_bgl_string1785za700za7za7t1820za7, ": ", 2);
	      DEFINE_STRING(BGl_string1786z00zztailc_walkz00,
		BgL_bgl_string1786za700za7za7t1821za7, "        ", 8);
	      DEFINE_STRING(BGl_string1792z00zztailc_walkz00,
		BgL_bgl_string1792za700za7za7t1822za7, "get-tail-calls1310", 18);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1787z00zztailc_walkz00,
		BgL_bgl_za762lambda1566za7621823z00, BGl_z62lambda1566z62zztailc_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1794z00zztailc_walkz00,
		BgL_bgl_string1794za700za7za7t1824za7, "get-tail-calls", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1788z00zztailc_walkz00,
		BgL_bgl_za762za7c3za704anonymo1825za7,
		BGl_z62zc3z04anonymousza31565ze3ze5zztailc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1789z00zztailc_walkz00,
		BgL_bgl_za762lambda1562za7621826z00, BGl_z62lambda1562z62zztailc_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1790z00zztailc_walkz00,
		BgL_bgl_za762lambda1553za7621827z00, BGl_z62lambda1553z62zztailc_walkz00,
		0L, BUNSPEC, 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1791z00zztailc_walkz00,
		BgL_bgl_za762getza7d2tailza7d21828za7,
		BGl_z62getzd2tailzd2calls1310z62zztailc_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1793z00zztailc_walkz00,
		BgL_bgl_za762getza7d2tailza7d21829za7,
		BGl_z62getzd2tailzd2callszd2closu1313zb0zztailc_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1795z00zztailc_walkz00,
		BgL_bgl_za762getza7d2tailza7d21830za7,
		BGl_z62getzd2tailzd2callszd2seque1315zb0zztailc_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1796z00zztailc_walkz00,
		BgL_bgl_za762getza7d2tailza7d21831za7,
		BGl_z62getzd2tailzd2callszd2sync1317zb0zztailc_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1797z00zztailc_walkz00,
		BgL_bgl_za762getza7d2tailza7d21832za7,
		BGl_z62getzd2tailzd2callszd2app1319zb0zztailc_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1798z00zztailc_walkz00,
		BgL_bgl_za762getza7d2tailza7d21833za7,
		BGl_z62getzd2tailzd2callszd2condi1321zb0zztailc_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1799z00zztailc_walkz00,
		BgL_bgl_za762getza7d2tailza7d21834za7,
		BGl_z62getzd2tailzd2callszd2switc1323zb0zztailc_walkz00, 0L, BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztailc_walkz00));
		     ADD_ROOT((void *) (&BGl_localzd2tailzd2zztailc_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztailc_walkz00(long
		BgL_checksumz00_2255, char *BgL_fromz00_2256)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztailc_walkz00))
				{
					BGl_requirezd2initializa7ationz75zztailc_walkz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztailc_walkz00();
					BGl_libraryzd2moduleszd2initz00zztailc_walkz00();
					BGl_cnstzd2initzd2zztailc_walkz00();
					BGl_importedzd2moduleszd2initz00zztailc_walkz00();
					BGl_objectzd2initzd2zztailc_walkz00();
					BGl_genericzd2initzd2zztailc_walkz00();
					BGl_methodzd2initzd2zztailc_walkz00();
					return BGl_toplevelzd2initzd2zztailc_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztailc_walkz00(void)
	{
		{	/* Tailc/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "tailc_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "tailc_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "tailc_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"tailc_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "tailc_walk");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "tailc_walk");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"tailc_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "tailc_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"tailc_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztailc_walkz00(void)
	{
		{	/* Tailc/walk.scm 15 */
			{	/* Tailc/walk.scm 15 */
				obj_t BgL_cportz00_2162;

				{	/* Tailc/walk.scm 15 */
					obj_t BgL_stringz00_2169;

					BgL_stringz00_2169 = BGl_string1804z00zztailc_walkz00;
					{	/* Tailc/walk.scm 15 */
						obj_t BgL_startz00_2170;

						BgL_startz00_2170 = BINT(0L);
						{	/* Tailc/walk.scm 15 */
							obj_t BgL_endz00_2171;

							BgL_endz00_2171 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2169)));
							{	/* Tailc/walk.scm 15 */

								BgL_cportz00_2162 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2169, BgL_startz00_2170, BgL_endz00_2171);
				}}}}
				{
					long BgL_iz00_2163;

					BgL_iz00_2163 = 3L;
				BgL_loopz00_2164:
					if ((BgL_iz00_2163 == -1L))
						{	/* Tailc/walk.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Tailc/walk.scm 15 */
							{	/* Tailc/walk.scm 15 */
								obj_t BgL_arg1805z00_2165;

								{	/* Tailc/walk.scm 15 */

									{	/* Tailc/walk.scm 15 */
										obj_t BgL_locationz00_2167;

										BgL_locationz00_2167 = BBOOL(((bool_t) 0));
										{	/* Tailc/walk.scm 15 */

											BgL_arg1805z00_2165 =
												BGl_readz00zz__readerz00(BgL_cportz00_2162,
												BgL_locationz00_2167);
										}
									}
								}
								{	/* Tailc/walk.scm 15 */
									int BgL_tmpz00_2286;

									BgL_tmpz00_2286 = (int) (BgL_iz00_2163);
									CNST_TABLE_SET(BgL_tmpz00_2286, BgL_arg1805z00_2165);
							}}
							{	/* Tailc/walk.scm 15 */
								int BgL_auxz00_2168;

								BgL_auxz00_2168 = (int) ((BgL_iz00_2163 - 1L));
								{
									long BgL_iz00_2291;

									BgL_iz00_2291 = (long) (BgL_auxz00_2168);
									BgL_iz00_2163 = BgL_iz00_2291;
									goto BgL_loopz00_2164;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztailc_walkz00(void)
	{
		{	/* Tailc/walk.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztailc_walkz00(void)
	{
		{	/* Tailc/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* tailc-walk! */
	BGL_EXPORTED_DEF obj_t BGl_tailczd2walkz12zc0zztailc_walkz00(obj_t
		BgL_globalsz00_48)
	{
		{	/* Tailc/walk.scm 32 */
			{	/* Tailc/walk.scm 33 */
				obj_t BgL_list1334z00_1434;

				{	/* Tailc/walk.scm 33 */
					obj_t BgL_arg1335z00_1435;

					{	/* Tailc/walk.scm 33 */
						obj_t BgL_arg1339z00_1436;

						BgL_arg1339z00_1436 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1335z00_1435 =
							MAKE_YOUNG_PAIR(BGl_string1781z00zztailc_walkz00,
							BgL_arg1339z00_1436);
					}
					BgL_list1334z00_1434 =
						MAKE_YOUNG_PAIR(BGl_string1782z00zztailc_walkz00,
						BgL_arg1335z00_1435);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1334z00_1434);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1781z00zztailc_walkz00;
			{	/* Tailc/walk.scm 33 */
				obj_t BgL_g1130z00_1437;

				BgL_g1130z00_1437 = BNIL;
				{
					obj_t BgL_hooksz00_1440;
					obj_t BgL_hnamesz00_1441;

					BgL_hooksz00_1440 = BgL_g1130z00_1437;
					BgL_hnamesz00_1441 = BNIL;
				BgL_zc3z04anonymousza31340ze3z87_1442:
					if (NULLP(BgL_hooksz00_1440))
						{	/* Tailc/walk.scm 33 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Tailc/walk.scm 33 */
							bool_t BgL_test1838z00_2304;

							{	/* Tailc/walk.scm 33 */
								obj_t BgL_fun1351z00_1449;

								BgL_fun1351z00_1449 = CAR(((obj_t) BgL_hooksz00_1440));
								BgL_test1838z00_2304 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1351z00_1449));
							}
							if (BgL_test1838z00_2304)
								{	/* Tailc/walk.scm 33 */
									obj_t BgL_arg1346z00_1446;
									obj_t BgL_arg1348z00_1447;

									BgL_arg1346z00_1446 = CDR(((obj_t) BgL_hooksz00_1440));
									BgL_arg1348z00_1447 = CDR(((obj_t) BgL_hnamesz00_1441));
									{
										obj_t BgL_hnamesz00_2316;
										obj_t BgL_hooksz00_2315;

										BgL_hooksz00_2315 = BgL_arg1346z00_1446;
										BgL_hnamesz00_2316 = BgL_arg1348z00_1447;
										BgL_hnamesz00_1441 = BgL_hnamesz00_2316;
										BgL_hooksz00_1440 = BgL_hooksz00_2315;
										goto BgL_zc3z04anonymousza31340ze3z87_1442;
									}
								}
							else
								{	/* Tailc/walk.scm 33 */
									obj_t BgL_arg1349z00_1448;

									BgL_arg1349z00_1448 = CAR(((obj_t) BgL_hnamesz00_1441));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1781z00zztailc_walkz00,
										BGl_string1783z00zztailc_walkz00, BgL_arg1349z00_1448);
								}
						}
				}
			}
			{
				obj_t BgL_l1294z00_1453;

				BgL_l1294z00_1453 = BgL_globalsz00_48;
			BgL_zc3z04anonymousza31353ze3z87_1454:
				if (PAIRP(BgL_l1294z00_1453))
					{	/* Tailc/walk.scm 34 */
						BGl_tailczd2globalzd2zztailc_walkz00(CAR(BgL_l1294z00_1453));
						{
							obj_t BgL_l1294z00_2324;

							BgL_l1294z00_2324 = CDR(BgL_l1294z00_1453);
							BgL_l1294z00_1453 = BgL_l1294z00_2324;
							goto BgL_zc3z04anonymousza31353ze3z87_1454;
						}
					}
				else
					{	/* Tailc/walk.scm 34 */
						((bool_t) 1);
					}
			}
			return BgL_globalsz00_48;
		}

	}



/* &tailc-walk! */
	obj_t BGl_z62tailczd2walkz12za2zztailc_walkz00(obj_t BgL_envz00_2087,
		obj_t BgL_globalsz00_2088)
	{
		{	/* Tailc/walk.scm 32 */
			return BGl_tailczd2walkz12zc0zztailc_walkz00(BgL_globalsz00_2088);
		}

	}



/* tailc-global */
	obj_t BGl_tailczd2globalzd2zztailc_walkz00(obj_t BgL_globalz00_49)
	{
		{	/* Tailc/walk.scm 40 */
			{	/* Tailc/walk.scm 41 */
				obj_t BgL_arg1367z00_1459;

				BgL_arg1367z00_1459 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_globalz00_49))))->BgL_idz00);
				BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1367z00_1459);
			}
			{	/* Tailc/walk.scm 42 */
				obj_t BgL_callsz00_1460;

				{	/* Tailc/walk.scm 42 */
					obj_t BgL_arg1380z00_1470;

					BgL_arg1380z00_1470 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt)
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_globalz00_49))))->
										BgL_valuez00))))->BgL_bodyz00);
					BgL_callsz00_1460 =
						BGl_getzd2tailzd2callsz00zztailc_walkz00(((BgL_nodez00_bglt)
							BgL_arg1380z00_1470), ((BgL_variablez00_bglt) BgL_globalz00_49),
						BNIL);
				}
				if (NULLP(BgL_callsz00_1460))
					{	/* Tailc/walk.scm 43 */
						BFALSE;
					}
				else
					{	/* Tailc/walk.scm 43 */
						{	/* Tailc/walk.scm 44 */
							obj_t BgL_arg1370z00_1462;
							long BgL_arg1371z00_1463;

							BgL_arg1370z00_1462 =
								BGl_shapez00zztools_shapez00(BgL_globalz00_49);
							BgL_arg1371z00_1463 = bgl_list_length(BgL_callsz00_1460);
							{	/* Tailc/walk.scm 44 */
								obj_t BgL_list1372z00_1464;

								{	/* Tailc/walk.scm 44 */
									obj_t BgL_arg1375z00_1465;

									{	/* Tailc/walk.scm 44 */
										obj_t BgL_arg1376z00_1466;

										{	/* Tailc/walk.scm 44 */
											obj_t BgL_arg1377z00_1467;

											{	/* Tailc/walk.scm 44 */
												obj_t BgL_arg1378z00_1468;

												{	/* Tailc/walk.scm 44 */
													obj_t BgL_arg1379z00_1469;

													BgL_arg1379z00_1469 =
														MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
													BgL_arg1378z00_1468 =
														MAKE_YOUNG_PAIR(BGl_string1784z00zztailc_walkz00,
														BgL_arg1379z00_1469);
												}
												BgL_arg1377z00_1467 =
													MAKE_YOUNG_PAIR(BINT(BgL_arg1371z00_1463),
													BgL_arg1378z00_1468);
											}
											BgL_arg1376z00_1466 =
												MAKE_YOUNG_PAIR(BGl_string1785z00zztailc_walkz00,
												BgL_arg1377z00_1467);
										}
										BgL_arg1375z00_1465 =
											MAKE_YOUNG_PAIR(BgL_arg1370z00_1462, BgL_arg1376z00_1466);
									}
									BgL_list1372z00_1464 =
										MAKE_YOUNG_PAIR(BGl_string1786z00zztailc_walkz00,
										BgL_arg1375z00_1465);
								}
								BGl_verbosez00zztools_speekz00(BINT(3L), BgL_list1372z00_1464);
						}}
						BGl_globalzd2ze3localz12z23zztailc_walkz00(BgL_globalz00_49,
							BgL_callsz00_1460);
			}}
			return BGl_leavezd2functionzd2zztools_errorz00();
		}

	}



/* global->local! */
	obj_t BGl_globalzd2ze3localz12z23zztailc_walkz00(obj_t BgL_globalz00_50,
		obj_t BgL_callsz00_51)
	{
		{	/* Tailc/walk.scm 52 */
			{	/* Tailc/walk.scm 53 */
				BgL_valuez00_bglt BgL_gfunz00_1472;

				BgL_gfunz00_1472 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_globalz00_50))))->BgL_valuez00);
				{	/* Tailc/walk.scm 53 */
					BgL_typez00_bglt BgL_gtypez00_1473;

					BgL_gtypez00_1473 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_globalz00_50))))->BgL_typez00);
					{	/* Tailc/walk.scm 54 */
						obj_t BgL_lbodyz00_1474;

						BgL_lbodyz00_1474 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_gfunz00_1472)))->BgL_bodyz00);
						{	/* Tailc/walk.scm 55 */
							obj_t BgL_largsz00_1475;

							BgL_largsz00_1475 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt) BgL_gfunz00_1472)))->BgL_argsz00);
							{	/* Tailc/walk.scm 56 */
								obj_t BgL_gargsz00_1476;

								if (NULLP(BgL_largsz00_1475))
									{	/* Tailc/walk.scm 57 */
										BgL_gargsz00_1476 = BNIL;
									}
								else
									{	/* Tailc/walk.scm 57 */
										obj_t BgL_head1298z00_1537;

										BgL_head1298z00_1537 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1296z00_1539;
											obj_t BgL_tail1299z00_1540;

											BgL_l1296z00_1539 = BgL_largsz00_1475;
											BgL_tail1299z00_1540 = BgL_head1298z00_1537;
										BgL_zc3z04anonymousza31491ze3z87_1541:
											if (NULLP(BgL_l1296z00_1539))
												{	/* Tailc/walk.scm 57 */
													BgL_gargsz00_1476 = CDR(BgL_head1298z00_1537);
												}
											else
												{	/* Tailc/walk.scm 57 */
													obj_t BgL_newtail1300z00_1543;

													{	/* Tailc/walk.scm 57 */
														BgL_localz00_bglt BgL_arg1509z00_1545;

														{	/* Tailc/walk.scm 57 */
															obj_t BgL_oldz00_1546;

															BgL_oldz00_1546 =
																CAR(((obj_t) BgL_l1296z00_1539));
															{	/* Tailc/walk.scm 59 */
																BgL_svarz00_bglt BgL_arg1513z00_1547;

																{	/* Tailc/walk.scm 59 */
																	BgL_svarz00_bglt BgL_duplicated1135z00_1548;
																	BgL_svarz00_bglt BgL_new1133z00_1549;

																	BgL_duplicated1135z00_1548 =
																		((BgL_svarz00_bglt)
																		(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_localz00_bglt)
																							BgL_oldz00_1546))))->
																			BgL_valuez00));
																	{	/* Tailc/walk.scm 59 */
																		BgL_svarz00_bglt BgL_new1136z00_1550;

																		BgL_new1136z00_1550 =
																			((BgL_svarz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_svarz00_bgl))));
																		{	/* Tailc/walk.scm 59 */
																			long BgL_arg1514z00_1551;

																			BgL_arg1514z00_1551 =
																				BGL_CLASS_NUM(BGl_svarz00zzast_varz00);
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1136z00_1550),
																				BgL_arg1514z00_1551);
																		}
																		{	/* Tailc/walk.scm 59 */
																			BgL_objectz00_bglt BgL_tmpz00_2381;

																			BgL_tmpz00_2381 =
																				((BgL_objectz00_bglt)
																				BgL_new1136z00_1550);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2381,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1136z00_1550);
																		BgL_new1133z00_1549 = BgL_new1136z00_1550;
																	}
																	((((BgL_svarz00_bglt)
																				COBJECT(BgL_new1133z00_1549))->
																			BgL_locz00) =
																		((obj_t) (((BgL_svarz00_bglt)
																					COBJECT(BgL_duplicated1135z00_1548))->
																				BgL_locz00)), BUNSPEC);
																	BgL_arg1513z00_1547 = BgL_new1133z00_1549;
																}
																BgL_arg1509z00_1545 =
																	BGl_clonezd2localzd2zzast_localz00(
																	((BgL_localz00_bglt) BgL_oldz00_1546),
																	((BgL_valuez00_bglt) BgL_arg1513z00_1547));
														}}
														BgL_newtail1300z00_1543 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg1509z00_1545), BNIL);
													}
													SET_CDR(BgL_tail1299z00_1540,
														BgL_newtail1300z00_1543);
													{	/* Tailc/walk.scm 57 */
														obj_t BgL_arg1502z00_1544;

														BgL_arg1502z00_1544 =
															CDR(((obj_t) BgL_l1296z00_1539));
														{
															obj_t BgL_tail1299z00_2396;
															obj_t BgL_l1296z00_2395;

															BgL_l1296z00_2395 = BgL_arg1502z00_1544;
															BgL_tail1299z00_2396 = BgL_newtail1300z00_1543;
															BgL_tail1299z00_1540 = BgL_tail1299z00_2396;
															BgL_l1296z00_1539 = BgL_l1296z00_2395;
															goto BgL_zc3z04anonymousza31491ze3z87_1541;
														}
													}
												}
										}
									}
								{	/* Tailc/walk.scm 57 */
									BgL_sfunz00_bglt BgL_lfunz00_1477;

									{	/* Tailc/walk.scm 62 */
										BgL_sfunz00_bglt BgL_new1137z00_1522;

										{	/* Tailc/walk.scm 63 */
											BgL_sfunz00_bglt BgL_new1150z00_1533;

											BgL_new1150z00_1533 =
												((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_sfunz00_bgl))));
											{	/* Tailc/walk.scm 63 */
												long BgL_arg1489z00_1534;

												BgL_arg1489z00_1534 =
													BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1150z00_1533),
													BgL_arg1489z00_1534);
											}
											{	/* Tailc/walk.scm 63 */
												BgL_objectz00_bglt BgL_tmpz00_2401;

												BgL_tmpz00_2401 =
													((BgL_objectz00_bglt) BgL_new1150z00_1533);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2401, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1150z00_1533);
											BgL_new1137z00_1522 = BgL_new1150z00_1533;
										}
										((((BgL_funz00_bglt) COBJECT(
														((BgL_funz00_bglt) BgL_new1137z00_1522)))->
												BgL_arityz00) =
											((long) (((BgL_funz00_bglt)
														COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
																	BgL_gfunz00_1472))))->BgL_arityz00)),
											BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1137z00_1522)))->BgL_sidezd2effectzd2) =
											((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																BgL_gfunz00_1472)))->BgL_sidezd2effectzd2)),
											BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1137z00_1522)))->BgL_predicatezd2ofzd2) =
											((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																BgL_gfunz00_1472)))->BgL_predicatezd2ofzd2)),
											BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1137z00_1522)))->
												BgL_stackzd2allocatorzd2) =
											((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																BgL_gfunz00_1472)))->BgL_stackzd2allocatorzd2)),
											BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1137z00_1522)))->BgL_topzf3zf3) =
											((bool_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																BgL_gfunz00_1472)))->BgL_topzf3zf3)), BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1137z00_1522)))->BgL_thezd2closurezd2) =
											((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																BgL_gfunz00_1472)))->BgL_thezd2closurezd2)),
											BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1137z00_1522)))->BgL_effectz00) =
											((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																BgL_gfunz00_1472)))->BgL_effectz00)), BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1137z00_1522)))->BgL_failsafez00) =
											((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																BgL_gfunz00_1472)))->BgL_failsafez00)),
											BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1137z00_1522)))->BgL_argszd2noescapezd2) =
											((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																BgL_gfunz00_1472)))->BgL_argszd2noescapezd2)),
											BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1137z00_1522)))->BgL_argszd2retescapezd2) =
											((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																BgL_gfunz00_1472)))->BgL_argszd2retescapezd2)),
											BUNSPEC);
										((((BgL_sfunz00_bglt) COBJECT(BgL_new1137z00_1522))->
												BgL_propertyz00) =
											((obj_t) (((BgL_sfunz00_bglt)
														COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																	BgL_gfunz00_1472))))->BgL_propertyz00)),
											BUNSPEC);
										((((BgL_sfunz00_bglt) COBJECT(BgL_new1137z00_1522))->
												BgL_argsz00) = ((obj_t) BgL_largsz00_1475), BUNSPEC);
										((((BgL_sfunz00_bglt) COBJECT(BgL_new1137z00_1522))->
												BgL_argszd2namezd2) =
											((obj_t) (((BgL_sfunz00_bglt)
														COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																	BgL_gfunz00_1472))))->BgL_argszd2namezd2)),
											BUNSPEC);
										((((BgL_sfunz00_bglt) COBJECT(BgL_new1137z00_1522))->
												BgL_bodyz00) = ((obj_t) BgL_lbodyz00_1474), BUNSPEC);
										((((BgL_sfunz00_bglt) COBJECT(BgL_new1137z00_1522))->
												BgL_classz00) =
											((obj_t) (((BgL_sfunz00_bglt)
														COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																	BgL_gfunz00_1472))))->BgL_classz00)),
											BUNSPEC);
										((((BgL_sfunz00_bglt) COBJECT(BgL_new1137z00_1522))->
												BgL_dssslzd2keywordszd2) =
											((obj_t) (((BgL_sfunz00_bglt)
														COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																	BgL_gfunz00_1472))))->
													BgL_dssslzd2keywordszd2)), BUNSPEC);
										((((BgL_sfunz00_bglt) COBJECT(BgL_new1137z00_1522))->
												BgL_locz00) =
											((obj_t) (((BgL_sfunz00_bglt)
														COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																	BgL_gfunz00_1472))))->BgL_locz00)), BUNSPEC);
										((((BgL_sfunz00_bglt) COBJECT(BgL_new1137z00_1522))->
												BgL_optionalsz00) =
											((obj_t) (((BgL_sfunz00_bglt)
														COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																	BgL_gfunz00_1472))))->BgL_optionalsz00)),
											BUNSPEC);
										((((BgL_sfunz00_bglt) COBJECT(BgL_new1137z00_1522))->
												BgL_keysz00) =
											((obj_t) (((BgL_sfunz00_bglt)
														COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																	BgL_gfunz00_1472))))->BgL_keysz00)), BUNSPEC);
										((((BgL_sfunz00_bglt) COBJECT(BgL_new1137z00_1522))->
												BgL_thezd2closurezd2globalz00) =
											((obj_t) (((BgL_sfunz00_bglt)
														COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																	BgL_gfunz00_1472))))->
													BgL_thezd2closurezd2globalz00)), BUNSPEC);
										((((BgL_sfunz00_bglt) COBJECT(BgL_new1137z00_1522))->
												BgL_strengthz00) =
											((obj_t) (((BgL_sfunz00_bglt)
														COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																	BgL_gfunz00_1472))))->BgL_strengthz00)),
											BUNSPEC);
										((((BgL_sfunz00_bglt) COBJECT(BgL_new1137z00_1522))->
												BgL_stackablez00) =
											((obj_t) (((BgL_sfunz00_bglt)
														COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																	BgL_gfunz00_1472))))->BgL_stackablez00)),
											BUNSPEC);
										BgL_lfunz00_1477 = BgL_new1137z00_1522;
									}
									{	/* Tailc/walk.scm 62 */
										BgL_localz00_bglt BgL_localz00_1478;

										{	/* Tailc/walk.scm 66 */
											obj_t BgL_arg1485z00_1520;

											BgL_arg1485z00_1520 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_globalz00_bglt) BgL_globalz00_50))))->
												BgL_idz00);
											BgL_localz00_1478 =
												BGl_makezd2localzd2sfunz00zzast_localz00
												(BgL_arg1485z00_1520, BgL_gtypez00_1473,
												BgL_lfunz00_1477);
										}
										{	/* Tailc/walk.scm 66 */
											obj_t BgL_locz00_1479;

											BgL_locz00_1479 =
												(((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_lbodyz00_1474)))->
												BgL_locz00);
											{	/* Tailc/walk.scm 67 */
												BgL_letzd2funzd2_bglt BgL_gbodyz00_1480;

												{	/* Tailc/walk.scm 68 */
													BgL_letzd2funzd2_bglt BgL_new1152z00_1494;

													{	/* Tailc/walk.scm 69 */
														BgL_letzd2funzd2_bglt BgL_new1151z00_1518;

														BgL_new1151z00_1518 =
															((BgL_letzd2funzd2_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_letzd2funzd2_bgl))));
														{	/* Tailc/walk.scm 69 */
															long BgL_arg1473z00_1519;

															BgL_arg1473z00_1519 =
																BGL_CLASS_NUM(BGl_letzd2funzd2zzast_nodez00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1151z00_1518),
																BgL_arg1473z00_1519);
														}
														{	/* Tailc/walk.scm 69 */
															BgL_objectz00_bglt BgL_tmpz00_2498;

															BgL_tmpz00_2498 =
																((BgL_objectz00_bglt) BgL_new1151z00_1518);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2498, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1151z00_1518);
														BgL_new1152z00_1494 = BgL_new1151z00_1518;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1152z00_1494)))->
															BgL_locz00) = ((obj_t) BgL_locz00_1479), BUNSPEC);
													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_new1152z00_1494)))->BgL_typez00) =
														((BgL_typez00_bglt) BgL_gtypez00_1473), BUNSPEC);
													((((BgL_nodezf2effectzf2_bglt)
																COBJECT(((BgL_nodezf2effectzf2_bglt)
																		BgL_new1152z00_1494)))->
															BgL_sidezd2effectzd2) =
														((obj_t) BUNSPEC), BUNSPEC);
													((((BgL_nodezf2effectzf2_bglt)
																COBJECT(((BgL_nodezf2effectzf2_bglt)
																		BgL_new1152z00_1494)))->BgL_keyz00) =
														((obj_t) BINT(-1L)), BUNSPEC);
													{
														obj_t BgL_auxz00_2511;

														{	/* Tailc/walk.scm 71 */
															obj_t BgL_list1423z00_1495;

															BgL_list1423z00_1495 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_localz00_1478), BNIL);
															BgL_auxz00_2511 = BgL_list1423z00_1495;
														}
														((((BgL_letzd2funzd2_bglt)
																	COBJECT(BgL_new1152z00_1494))->
																BgL_localsz00) =
															((obj_t) BgL_auxz00_2511), BUNSPEC);
													}
													{
														BgL_nodez00_bglt BgL_auxz00_2515;

														{	/* Tailc/walk.scm 72 */
															BgL_appz00_bglt BgL_new1154z00_1496;

															{	/* Tailc/walk.scm 73 */
																BgL_appz00_bglt BgL_new1153z00_1516;

																BgL_new1153z00_1516 =
																	((BgL_appz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_appz00_bgl))));
																{	/* Tailc/walk.scm 73 */
																	long BgL_arg1472z00_1517;

																	{	/* Tailc/walk.scm 73 */
																		obj_t BgL_classz00_1899;

																		BgL_classz00_1899 = BGl_appz00zzast_nodez00;
																		BgL_arg1472z00_1517 =
																			BGL_CLASS_NUM(BgL_classz00_1899);
																	}
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1153z00_1516),
																		BgL_arg1472z00_1517);
																}
																{	/* Tailc/walk.scm 73 */
																	BgL_objectz00_bglt BgL_tmpz00_2520;

																	BgL_tmpz00_2520 =
																		((BgL_objectz00_bglt) BgL_new1153z00_1516);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2520,
																		BFALSE);
																}
																((BgL_objectz00_bglt) BgL_new1153z00_1516);
																BgL_new1154z00_1496 = BgL_new1153z00_1516;
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1154z00_1496)))->BgL_locz00) =
																((obj_t) BgL_locz00_1479), BUNSPEC);
															((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																				BgL_new1154z00_1496)))->BgL_typez00) =
																((BgL_typez00_bglt) BgL_gtypez00_1473),
																BUNSPEC);
															((((BgL_nodezf2effectzf2_bglt)
																		COBJECT(((BgL_nodezf2effectzf2_bglt)
																				BgL_new1154z00_1496)))->
																	BgL_sidezd2effectzd2) =
																((obj_t) BUNSPEC), BUNSPEC);
															((((BgL_nodezf2effectzf2_bglt)
																		COBJECT(((BgL_nodezf2effectzf2_bglt)
																				BgL_new1154z00_1496)))->BgL_keyz00) =
																((obj_t) BINT(-1L)), BUNSPEC);
															{
																BgL_varz00_bglt BgL_auxz00_2533;

																{	/* Tailc/walk.scm 75 */
																	BgL_refz00_bglt BgL_new1157z00_1497;

																	{	/* Tailc/walk.scm 76 */
																		BgL_refz00_bglt BgL_new1156z00_1498;

																		BgL_new1156z00_1498 =
																			((BgL_refz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_refz00_bgl))));
																		{	/* Tailc/walk.scm 76 */
																			long BgL_arg1434z00_1499;

																			{	/* Tailc/walk.scm 76 */
																				obj_t BgL_classz00_1903;

																				BgL_classz00_1903 =
																					BGl_refz00zzast_nodez00;
																				BgL_arg1434z00_1499 =
																					BGL_CLASS_NUM(BgL_classz00_1903);
																			}
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1156z00_1498),
																				BgL_arg1434z00_1499);
																		}
																		{	/* Tailc/walk.scm 76 */
																			BgL_objectz00_bglt BgL_tmpz00_2538;

																			BgL_tmpz00_2538 =
																				((BgL_objectz00_bglt)
																				BgL_new1156z00_1498);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2538,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1156z00_1498);
																		BgL_new1157z00_1497 = BgL_new1156z00_1498;
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1157z00_1497)))->
																			BgL_locz00) =
																		((obj_t) BgL_locz00_1479), BUNSPEC);
																	((((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt)
																						BgL_new1157z00_1497)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) BgL_gtypez00_1473),
																		BUNSPEC);
																	((((BgL_varz00_bglt)
																				COBJECT(((BgL_varz00_bglt)
																						BgL_new1157z00_1497)))->
																			BgL_variablez00) =
																		((BgL_variablez00_bglt) (
																				(BgL_variablez00_bglt)
																				BgL_localz00_1478)), BUNSPEC);
																	BgL_auxz00_2533 =
																		((BgL_varz00_bglt) BgL_new1157z00_1497);
																}
																((((BgL_appz00_bglt)
																			COBJECT(BgL_new1154z00_1496))->
																		BgL_funz00) =
																	((BgL_varz00_bglt) BgL_auxz00_2533), BUNSPEC);
															}
															{
																obj_t BgL_auxz00_2551;

																if (NULLP(BgL_gargsz00_1476))
																	{	/* Tailc/walk.scm 79 */
																		BgL_auxz00_2551 = BNIL;
																	}
																else
																	{	/* Tailc/walk.scm 79 */
																		obj_t BgL_head1303z00_1502;

																		BgL_head1303z00_1502 =
																			MAKE_YOUNG_PAIR(BNIL, BNIL);
																		{
																			obj_t BgL_l1301z00_1504;
																			obj_t BgL_tail1304z00_1505;

																			BgL_l1301z00_1504 = BgL_gargsz00_1476;
																			BgL_tail1304z00_1505 =
																				BgL_head1303z00_1502;
																		BgL_zc3z04anonymousza31436ze3z87_1506:
																			if (NULLP(BgL_l1301z00_1504))
																				{	/* Tailc/walk.scm 79 */
																					BgL_auxz00_2551 =
																						CDR(BgL_head1303z00_1502);
																				}
																			else
																				{	/* Tailc/walk.scm 79 */
																					obj_t BgL_newtail1305z00_1508;

																					{	/* Tailc/walk.scm 79 */
																						BgL_refz00_bglt BgL_arg1453z00_1510;

																						{	/* Tailc/walk.scm 79 */
																							obj_t BgL_vz00_1511;

																							BgL_vz00_1511 =
																								CAR(
																								((obj_t) BgL_l1301z00_1504));
																							{	/* Tailc/walk.scm 80 */
																								BgL_refz00_bglt
																									BgL_new1159z00_1512;
																								{	/* Tailc/walk.scm 83 */
																									BgL_refz00_bglt
																										BgL_new1158z00_1513;
																									BgL_new1158z00_1513 =
																										((BgL_refz00_bglt)
																										BOBJECT(GC_MALLOC(sizeof
																												(struct
																													BgL_refz00_bgl))));
																									{	/* Tailc/walk.scm 83 */
																										long BgL_arg1454z00_1514;

																										{	/* Tailc/walk.scm 83 */
																											obj_t BgL_classz00_1909;

																											BgL_classz00_1909 =
																												BGl_refz00zzast_nodez00;
																											BgL_arg1454z00_1514 =
																												BGL_CLASS_NUM
																												(BgL_classz00_1909);
																										}
																										BGL_OBJECT_CLASS_NUM_SET(
																											((BgL_objectz00_bglt)
																												BgL_new1158z00_1513),
																											BgL_arg1454z00_1514);
																									}
																									{	/* Tailc/walk.scm 83 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_2564;
																										BgL_tmpz00_2564 =
																											((BgL_objectz00_bglt)
																											BgL_new1158z00_1513);
																										BGL_OBJECT_WIDENING_SET
																											(BgL_tmpz00_2564, BFALSE);
																									}
																									((BgL_objectz00_bglt)
																										BgL_new1158z00_1513);
																									BgL_new1159z00_1512 =
																										BgL_new1158z00_1513;
																								}
																								((((BgL_nodez00_bglt) COBJECT(
																												((BgL_nodez00_bglt)
																													BgL_new1159z00_1512)))->
																										BgL_locz00) =
																									((obj_t) BgL_locz00_1479),
																									BUNSPEC);
																								((((BgL_nodez00_bglt)
																											COBJECT((
																													(BgL_nodez00_bglt)
																													BgL_new1159z00_1512)))->
																										BgL_typez00) =
																									((BgL_typez00_bglt) ((
																												(BgL_variablez00_bglt)
																												COBJECT((
																														(BgL_variablez00_bglt)
																														((BgL_localz00_bglt)
																															BgL_vz00_1511))))->
																											BgL_typez00)), BUNSPEC);
																								((((BgL_varz00_bglt)
																											COBJECT(((BgL_varz00_bglt)
																													BgL_new1159z00_1512)))->
																										BgL_variablez00) =
																									((BgL_variablez00_bglt) (
																											(BgL_variablez00_bglt)
																											BgL_vz00_1511)), BUNSPEC);
																								BgL_arg1453z00_1510 =
																									BgL_new1159z00_1512;
																						}}
																						BgL_newtail1305z00_1508 =
																							MAKE_YOUNG_PAIR(
																							((obj_t) BgL_arg1453z00_1510),
																							BNIL);
																					}
																					SET_CDR(BgL_tail1304z00_1505,
																						BgL_newtail1305z00_1508);
																					{	/* Tailc/walk.scm 79 */
																						obj_t BgL_arg1448z00_1509;

																						BgL_arg1448z00_1509 =
																							CDR(((obj_t) BgL_l1301z00_1504));
																						{
																							obj_t BgL_tail1304z00_2584;
																							obj_t BgL_l1301z00_2583;

																							BgL_l1301z00_2583 =
																								BgL_arg1448z00_1509;
																							BgL_tail1304z00_2584 =
																								BgL_newtail1305z00_1508;
																							BgL_tail1304z00_1505 =
																								BgL_tail1304z00_2584;
																							BgL_l1301z00_1504 =
																								BgL_l1301z00_2583;
																							goto
																								BgL_zc3z04anonymousza31436ze3z87_1506;
																						}
																					}
																				}
																		}
																	}
																((((BgL_appz00_bglt)
																			COBJECT(BgL_new1154z00_1496))->
																		BgL_argsz00) =
																	((obj_t) BgL_auxz00_2551), BUNSPEC);
															}
															((((BgL_appz00_bglt)
																		COBJECT(BgL_new1154z00_1496))->
																	BgL_stackablez00) =
																((obj_t) BFALSE), BUNSPEC);
															BgL_auxz00_2515 =
																((BgL_nodez00_bglt) BgL_new1154z00_1496);
														}
														((((BgL_letzd2funzd2_bglt)
																	COBJECT(BgL_new1152z00_1494))->BgL_bodyz00) =
															((BgL_nodez00_bglt) BgL_auxz00_2515), BUNSPEC);
													}
													BgL_gbodyz00_1480 = BgL_new1152z00_1494;
												}
												{	/* Tailc/walk.scm 68 */

													((((BgL_sfunz00_bglt) COBJECT(
																	((BgL_sfunz00_bglt) BgL_gfunz00_1472)))->
															BgL_argsz00) =
														((obj_t) BgL_gargsz00_1476), BUNSPEC);
													((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
																		BgL_gfunz00_1472)))->BgL_bodyz00) =
														((obj_t) ((obj_t) BgL_gbodyz00_1480)), BUNSPEC);
													{
														obj_t BgL_l1306z00_1482;

														BgL_l1306z00_1482 = BgL_callsz00_51;
													BgL_zc3z04anonymousza31409ze3z87_1483:
														if (PAIRP(BgL_l1306z00_1482))
															{	/* Tailc/walk.scm 89 */
																{	/* Tailc/walk.scm 90 */
																	obj_t BgL_callz00_1485;

																	BgL_callz00_1485 = CAR(BgL_l1306z00_1482);
																	{	/* Tailc/walk.scm 90 */
																		obj_t BgL_locz00_1486;

																		BgL_locz00_1486 =
																			(((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_callz00_1485)))->BgL_locz00);
																		{	/* Tailc/walk.scm 90 */
																			BgL_refz00_bglt BgL_varz00_1487;

																			{	/* Tailc/walk.scm 91 */
																				BgL_refz00_bglt BgL_new1161z00_1489;

																				{	/* Tailc/walk.scm 94 */
																					BgL_refz00_bglt BgL_new1160z00_1490;

																					BgL_new1160z00_1490 =
																						((BgL_refz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_refz00_bgl))));
																					{	/* Tailc/walk.scm 94 */
																						long BgL_arg1421z00_1491;

																						BgL_arg1421z00_1491 =
																							BGL_CLASS_NUM
																							(BGl_refz00zzast_nodez00);
																						BGL_OBJECT_CLASS_NUM_SET((
																								(BgL_objectz00_bglt)
																								BgL_new1160z00_1490),
																							BgL_arg1421z00_1491);
																					}
																					{	/* Tailc/walk.scm 94 */
																						BgL_objectz00_bglt BgL_tmpz00_2603;

																						BgL_tmpz00_2603 =
																							((BgL_objectz00_bglt)
																							BgL_new1160z00_1490);
																						BGL_OBJECT_WIDENING_SET
																							(BgL_tmpz00_2603, BFALSE);
																					}
																					((BgL_objectz00_bglt)
																						BgL_new1160z00_1490);
																					BgL_new1161z00_1489 =
																						BgL_new1160z00_1490;
																				}
																				((((BgL_nodez00_bglt) COBJECT(
																								((BgL_nodez00_bglt)
																									BgL_new1161z00_1489)))->
																						BgL_locz00) =
																					((obj_t) BgL_locz00_1486), BUNSPEC);
																				((((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt)
																									BgL_new1161z00_1489)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) ((
																								(BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										BgL_localz00_1478)))->
																							BgL_typez00)), BUNSPEC);
																				((((BgL_varz00_bglt)
																							COBJECT(((BgL_varz00_bglt)
																									BgL_new1161z00_1489)))->
																						BgL_variablez00) =
																					((BgL_variablez00_bglt) (
																							(BgL_variablez00_bglt)
																							BgL_localz00_1478)), BUNSPEC);
																				BgL_varz00_1487 = BgL_new1161z00_1489;
																			}
																			{	/* Tailc/walk.scm 91 */

																				((((BgL_appz00_bglt) COBJECT(
																								((BgL_appz00_bglt)
																									BgL_callz00_1485)))->
																						BgL_funz00) =
																					((BgL_varz00_bglt) ((BgL_varz00_bglt)
																							BgL_varz00_1487)), BUNSPEC);
																}}}}
																{
																	obj_t BgL_l1306z00_2619;

																	BgL_l1306z00_2619 = CDR(BgL_l1306z00_1482);
																	BgL_l1306z00_1482 = BgL_l1306z00_2619;
																	goto BgL_zc3z04anonymousza31409ze3z87_1483;
																}
															}
														else
															{	/* Tailc/walk.scm 89 */
																((bool_t) 1);
															}
													}
													return BGl_funz00zzast_varz00;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztailc_walkz00(void)
	{
		{	/* Tailc/walk.scm 15 */
			{	/* Tailc/walk.scm 26 */
				obj_t BgL_arg1546z00_1557;
				obj_t BgL_arg1552z00_1558;

				{	/* Tailc/walk.scm 26 */
					obj_t BgL_v1308z00_1592;

					BgL_v1308z00_1592 = create_vector(0L);
					BgL_arg1546z00_1557 = BgL_v1308z00_1592;
				}
				{	/* Tailc/walk.scm 26 */
					obj_t BgL_v1309z00_1593;

					BgL_v1309z00_1593 = create_vector(0L);
					BgL_arg1552z00_1558 = BgL_v1309z00_1593;
				}
				return (BGl_localzd2tailzd2zztailc_walkz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(1),
						CNST_TABLE_REF(2), BGl_localz00zzast_varz00, 43644L,
						BGl_proc1790z00zztailc_walkz00, BGl_proc1789z00zztailc_walkz00,
						BFALSE, BGl_proc1788z00zztailc_walkz00,
						BGl_proc1787z00zztailc_walkz00, BgL_arg1546z00_1557,
						BgL_arg1552z00_1558), BUNSPEC);
			}
		}

	}



/* &lambda1566 */
	BgL_localz00_bglt BGl_z62lambda1566z62zztailc_walkz00(obj_t BgL_envz00_2093,
		obj_t BgL_o1120z00_2094)
	{
		{	/* Tailc/walk.scm 26 */
			{	/* Tailc/walk.scm 26 */
				long BgL_arg1571z00_2174;

				{	/* Tailc/walk.scm 26 */
					obj_t BgL_arg1573z00_2175;

					{	/* Tailc/walk.scm 26 */
						obj_t BgL_arg1575z00_2176;

						{	/* Tailc/walk.scm 26 */
							obj_t BgL_arg1815z00_2177;
							long BgL_arg1816z00_2178;

							BgL_arg1815z00_2177 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Tailc/walk.scm 26 */
								long BgL_arg1817z00_2179;

								BgL_arg1817z00_2179 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_o1120z00_2094)));
								BgL_arg1816z00_2178 = (BgL_arg1817z00_2179 - OBJECT_TYPE);
							}
							BgL_arg1575z00_2176 =
								VECTOR_REF(BgL_arg1815z00_2177, BgL_arg1816z00_2178);
						}
						BgL_arg1573z00_2175 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1575z00_2176);
					}
					{	/* Tailc/walk.scm 26 */
						obj_t BgL_tmpz00_2633;

						BgL_tmpz00_2633 = ((obj_t) BgL_arg1573z00_2175);
						BgL_arg1571z00_2174 = BGL_CLASS_NUM(BgL_tmpz00_2633);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_localz00_bglt) BgL_o1120z00_2094)), BgL_arg1571z00_2174);
			}
			{	/* Tailc/walk.scm 26 */
				BgL_objectz00_bglt BgL_tmpz00_2639;

				BgL_tmpz00_2639 =
					((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1120z00_2094));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2639, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1120z00_2094));
			return ((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1120z00_2094));
		}

	}



/* &<@anonymous:1565> */
	obj_t BGl_z62zc3z04anonymousza31565ze3ze5zztailc_walkz00(obj_t
		BgL_envz00_2095, obj_t BgL_new1119z00_2096)
	{
		{	/* Tailc/walk.scm 26 */
			{
				BgL_localz00_bglt BgL_auxz00_2647;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1119z00_2096))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(3)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2096))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_2655;

					{	/* Tailc/walk.scm 26 */
						obj_t BgL_classz00_2181;

						BgL_classz00_2181 = BGl_typez00zztype_typez00;
						{	/* Tailc/walk.scm 26 */
							obj_t BgL__ortest_1117z00_2182;

							BgL__ortest_1117z00_2182 = BGL_CLASS_NIL(BgL_classz00_2181);
							if (CBOOL(BgL__ortest_1117z00_2182))
								{	/* Tailc/walk.scm 26 */
									BgL_auxz00_2655 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_2182);
								}
							else
								{	/* Tailc/walk.scm 26 */
									BgL_auxz00_2655 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2181));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1119z00_2096))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_2655), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_2665;

					{	/* Tailc/walk.scm 26 */
						obj_t BgL_classz00_2183;

						BgL_classz00_2183 = BGl_valuez00zzast_varz00;
						{	/* Tailc/walk.scm 26 */
							obj_t BgL__ortest_1117z00_2184;

							BgL__ortest_1117z00_2184 = BGL_CLASS_NIL(BgL_classz00_2183);
							if (CBOOL(BgL__ortest_1117z00_2184))
								{	/* Tailc/walk.scm 26 */
									BgL_auxz00_2665 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_2184);
								}
							else
								{	/* Tailc/walk.scm 26 */
									BgL_auxz00_2665 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2183));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1119z00_2096))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_2665), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1119z00_2096))))->BgL_accessz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2096))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2096))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2096))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2096))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2096))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2096))))->BgL_keyz00) =
					((long) 0L), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2096))))->BgL_valzd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2096))))->BgL_volatilez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				BgL_auxz00_2647 = ((BgL_localz00_bglt) BgL_new1119z00_2096);
				return ((obj_t) BgL_auxz00_2647);
			}
		}

	}



/* &lambda1562 */
	BgL_localz00_bglt BGl_z62lambda1562z62zztailc_walkz00(obj_t BgL_envz00_2097,
		obj_t BgL_o1116z00_2098)
	{
		{	/* Tailc/walk.scm 26 */
			{	/* Tailc/walk.scm 26 */
				BgL_localzd2tailzd2_bglt BgL_wide1118z00_2186;

				BgL_wide1118z00_2186 =
					((BgL_localzd2tailzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_localzd2tailzd2_bgl))));
				{	/* Tailc/walk.scm 26 */
					obj_t BgL_auxz00_2709;
					BgL_objectz00_bglt BgL_tmpz00_2705;

					BgL_auxz00_2709 = ((obj_t) BgL_wide1118z00_2186);
					BgL_tmpz00_2705 =
						((BgL_objectz00_bglt)
						((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1116z00_2098)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2705, BgL_auxz00_2709);
				}
				((BgL_objectz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1116z00_2098)));
				{	/* Tailc/walk.scm 26 */
					long BgL_arg1564z00_2187;

					BgL_arg1564z00_2187 =
						BGL_CLASS_NUM(BGl_localzd2tailzd2zztailc_walkz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_localz00_bglt)
								((BgL_localz00_bglt) BgL_o1116z00_2098))), BgL_arg1564z00_2187);
				}
				return
					((BgL_localz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1116z00_2098)));
			}
		}

	}



/* &lambda1553 */
	BgL_localz00_bglt BGl_z62lambda1553z62zztailc_walkz00(obj_t BgL_envz00_2099,
		obj_t BgL_id1103z00_2100, obj_t BgL_name1104z00_2101,
		obj_t BgL_type1105z00_2102, obj_t BgL_value1106z00_2103,
		obj_t BgL_access1107z00_2104, obj_t BgL_fastzd2alpha1108zd2_2105,
		obj_t BgL_removable1109z00_2106, obj_t BgL_occurrence1110z00_2107,
		obj_t BgL_occurrencew1111z00_2108, obj_t BgL_userzf31112zf3_2109,
		obj_t BgL_key1113z00_2110, obj_t BgL_valzd2noescape1114zd2_2111,
		obj_t BgL_volatile1115z00_2112)
	{
		{	/* Tailc/walk.scm 26 */
			{	/* Tailc/walk.scm 26 */
				long BgL_occurrence1110z00_2191;
				long BgL_occurrencew1111z00_2192;
				bool_t BgL_userzf31112zf3_2193;
				long BgL_key1113z00_2194;
				bool_t BgL_volatile1115z00_2195;

				BgL_occurrence1110z00_2191 = (long) CINT(BgL_occurrence1110z00_2107);
				BgL_occurrencew1111z00_2192 = (long) CINT(BgL_occurrencew1111z00_2108);
				BgL_userzf31112zf3_2193 = CBOOL(BgL_userzf31112zf3_2109);
				BgL_key1113z00_2194 = (long) CINT(BgL_key1113z00_2110);
				BgL_volatile1115z00_2195 = CBOOL(BgL_volatile1115z00_2112);
				{	/* Tailc/walk.scm 26 */
					BgL_localz00_bglt BgL_new1177z00_2196;

					{	/* Tailc/walk.scm 26 */
						BgL_localz00_bglt BgL_tmp1175z00_2197;
						BgL_localzd2tailzd2_bglt BgL_wide1176z00_2198;

						{
							BgL_localz00_bglt BgL_auxz00_2728;

							{	/* Tailc/walk.scm 26 */
								BgL_localz00_bglt BgL_new1174z00_2199;

								BgL_new1174z00_2199 =
									((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_localz00_bgl))));
								{	/* Tailc/walk.scm 26 */
									long BgL_arg1561z00_2200;

									BgL_arg1561z00_2200 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1174z00_2199),
										BgL_arg1561z00_2200);
								}
								{	/* Tailc/walk.scm 26 */
									BgL_objectz00_bglt BgL_tmpz00_2733;

									BgL_tmpz00_2733 = ((BgL_objectz00_bglt) BgL_new1174z00_2199);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2733, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1174z00_2199);
								BgL_auxz00_2728 = BgL_new1174z00_2199;
							}
							BgL_tmp1175z00_2197 = ((BgL_localz00_bglt) BgL_auxz00_2728);
						}
						BgL_wide1176z00_2198 =
							((BgL_localzd2tailzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_localzd2tailzd2_bgl))));
						{	/* Tailc/walk.scm 26 */
							obj_t BgL_auxz00_2741;
							BgL_objectz00_bglt BgL_tmpz00_2739;

							BgL_auxz00_2741 = ((obj_t) BgL_wide1176z00_2198);
							BgL_tmpz00_2739 = ((BgL_objectz00_bglt) BgL_tmp1175z00_2197);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2739, BgL_auxz00_2741);
						}
						((BgL_objectz00_bglt) BgL_tmp1175z00_2197);
						{	/* Tailc/walk.scm 26 */
							long BgL_arg1559z00_2201;

							BgL_arg1559z00_2201 =
								BGL_CLASS_NUM(BGl_localzd2tailzd2zztailc_walkz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1175z00_2197),
								BgL_arg1559z00_2201);
						}
						BgL_new1177z00_2196 = ((BgL_localz00_bglt) BgL_tmp1175z00_2197);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1177z00_2196)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1103z00_2100)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1177z00_2196)))->BgL_namez00) =
						((obj_t) BgL_name1104z00_2101), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1177z00_2196)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1105z00_2102)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1177z00_2196)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1106z00_2103)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1177z00_2196)))->BgL_accessz00) =
						((obj_t) BgL_access1107z00_2104), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1177z00_2196)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1108zd2_2105), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1177z00_2196)))->BgL_removablez00) =
						((obj_t) BgL_removable1109z00_2106), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1177z00_2196)))->BgL_occurrencez00) =
						((long) BgL_occurrence1110z00_2191), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1177z00_2196)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1111z00_2192), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1177z00_2196)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31112zf3_2193), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1177z00_2196)))->BgL_keyz00) =
						((long) BgL_key1113z00_2194), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1177z00_2196)))->BgL_valzd2noescapezd2) =
						((obj_t) BgL_valzd2noescape1114zd2_2111), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1177z00_2196)))->BgL_volatilez00) =
						((bool_t) BgL_volatile1115z00_2195), BUNSPEC);
					return BgL_new1177z00_2196;
				}
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztailc_walkz00(void)
	{
		{	/* Tailc/walk.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_getzd2tailzd2callszd2envzd2zztailc_walkz00,
				BGl_proc1791z00zztailc_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string1792z00zztailc_walkz00);
		}

	}



/* &get-tail-calls1310 */
	obj_t BGl_z62getzd2tailzd2calls1310z62zztailc_walkz00(obj_t BgL_envz00_2114,
		obj_t BgL_nz00_2115, obj_t BgL_hz00_2116, obj_t BgL_tailsz00_2117)
	{
		{	/* Tailc/walk.scm 106 */
			return ((obj_t) BgL_tailsz00_2117);
		}

	}



/* get-tail-calls */
	obj_t BGl_getzd2tailzd2callsz00zztailc_walkz00(BgL_nodez00_bglt BgL_nz00_52,
		BgL_variablez00_bglt BgL_hz00_53, obj_t BgL_tailsz00_54)
	{
		{	/* Tailc/walk.scm 106 */
			{	/* Tailc/walk.scm 106 */
				obj_t BgL_method1311z00_1604;

				{	/* Tailc/walk.scm 106 */
					obj_t BgL_res1778z00_1985;

					{	/* Tailc/walk.scm 106 */
						long BgL_objzd2classzd2numz00_1956;

						BgL_objzd2classzd2numz00_1956 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_52));
						{	/* Tailc/walk.scm 106 */
							obj_t BgL_arg1811z00_1957;

							BgL_arg1811z00_1957 =
								PROCEDURE_REF(BGl_getzd2tailzd2callszd2envzd2zztailc_walkz00,
								(int) (1L));
							{	/* Tailc/walk.scm 106 */
								int BgL_offsetz00_1960;

								BgL_offsetz00_1960 = (int) (BgL_objzd2classzd2numz00_1956);
								{	/* Tailc/walk.scm 106 */
									long BgL_offsetz00_1961;

									BgL_offsetz00_1961 =
										((long) (BgL_offsetz00_1960) - OBJECT_TYPE);
									{	/* Tailc/walk.scm 106 */
										long BgL_modz00_1962;

										BgL_modz00_1962 =
											(BgL_offsetz00_1961 >> (int) ((long) ((int) (4L))));
										{	/* Tailc/walk.scm 106 */
											long BgL_restz00_1964;

											BgL_restz00_1964 =
												(BgL_offsetz00_1961 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Tailc/walk.scm 106 */

												{	/* Tailc/walk.scm 106 */
													obj_t BgL_bucketz00_1966;

													BgL_bucketz00_1966 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1957), BgL_modz00_1962);
													BgL_res1778z00_1985 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1966), BgL_restz00_1964);
					}}}}}}}}
					BgL_method1311z00_1604 = BgL_res1778z00_1985;
				}
				return
					BGL_PROCEDURE_CALL3(BgL_method1311z00_1604,
					((obj_t) BgL_nz00_52), ((obj_t) BgL_hz00_53), BgL_tailsz00_54);
			}
		}

	}



/* &get-tail-calls */
	obj_t BGl_z62getzd2tailzd2callsz62zztailc_walkz00(obj_t BgL_envz00_2118,
		obj_t BgL_nz00_2119, obj_t BgL_hz00_2120, obj_t BgL_tailsz00_2121)
	{
		{	/* Tailc/walk.scm 106 */
			return
				BGl_getzd2tailzd2callsz00zztailc_walkz00(
				((BgL_nodez00_bglt) BgL_nz00_2119),
				((BgL_variablez00_bglt) BgL_hz00_2120), BgL_tailsz00_2121);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztailc_walkz00(void)
	{
		{	/* Tailc/walk.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2tailzd2callszd2envzd2zztailc_walkz00,
				BGl_closurez00zzast_nodez00, BGl_proc1793z00zztailc_walkz00,
				BGl_string1794z00zztailc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2tailzd2callszd2envzd2zztailc_walkz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1795z00zztailc_walkz00,
				BGl_string1794z00zztailc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2tailzd2callszd2envzd2zztailc_walkz00,
				BGl_syncz00zzast_nodez00, BGl_proc1796z00zztailc_walkz00,
				BGl_string1794z00zztailc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2tailzd2callszd2envzd2zztailc_walkz00,
				BGl_appz00zzast_nodez00, BGl_proc1797z00zztailc_walkz00,
				BGl_string1794z00zztailc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2tailzd2callszd2envzd2zztailc_walkz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1798z00zztailc_walkz00,
				BGl_string1794z00zztailc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2tailzd2callszd2envzd2zztailc_walkz00,
				BGl_switchz00zzast_nodez00, BGl_proc1799z00zztailc_walkz00,
				BGl_string1794z00zztailc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2tailzd2callszd2envzd2zztailc_walkz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1800z00zztailc_walkz00,
				BGl_string1794z00zztailc_walkz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2tailzd2callszd2envzd2zztailc_walkz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1801z00zztailc_walkz00,
				BGl_string1794z00zztailc_walkz00);
		}

	}



/* &get-tail-calls-let-v1327 */
	obj_t BGl_z62getzd2tailzd2callszd2letzd2v1327z62zztailc_walkz00(obj_t
		BgL_envz00_2130, obj_t BgL_nodez00_2131, obj_t BgL_hostz00_2132,
		obj_t BgL_tailsz00_2133)
	{
		{	/* Tailc/walk.scm 179 */
			{	/* Tailc/walk.scm 181 */
				BgL_nodez00_bglt BgL_arg1642z00_2206;

				BgL_arg1642z00_2206 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2131)))->BgL_bodyz00);
				return
					BGl_getzd2tailzd2callsz00zztailc_walkz00(BgL_arg1642z00_2206,
					((BgL_variablez00_bglt) BgL_hostz00_2132), BgL_tailsz00_2133);
			}
		}

	}



/* &get-tail-calls-let-f1325 */
	obj_t BGl_z62getzd2tailzd2callszd2letzd2f1325z62zztailc_walkz00(obj_t
		BgL_envz00_2134, obj_t BgL_nodez00_2135, obj_t BgL_hostz00_2136,
		obj_t BgL_tailsz00_2137)
	{
		{	/* Tailc/walk.scm 169 */
			{	/* Tailc/walk.scm 174 */
				BgL_nodez00_bglt BgL_arg1630z00_2208;

				BgL_arg1630z00_2208 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2135)))->BgL_bodyz00);
				return
					BGl_getzd2tailzd2callsz00zztailc_walkz00(BgL_arg1630z00_2208,
					((BgL_variablez00_bglt) BgL_hostz00_2136), BgL_tailsz00_2137);
			}
		}

	}



/* &get-tail-calls-switc1323 */
	obj_t BGl_z62getzd2tailzd2callszd2switc1323zb0zztailc_walkz00(obj_t
		BgL_envz00_2138, obj_t BgL_nodez00_2139, obj_t BgL_hostz00_2140,
		obj_t BgL_tailsz00_2141)
	{
		{	/* Tailc/walk.scm 157 */
			{	/* Tailc/walk.scm 159 */
				obj_t BgL_res1806z00_2218;

				{	/* Tailc/walk.scm 159 */
					obj_t BgL_g1171z00_2210;

					BgL_g1171z00_2210 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2139)))->BgL_clausesz00);
					{
						obj_t BgL_clausesz00_2212;
						obj_t BgL_tailsz00_2213;

						BgL_clausesz00_2212 = BgL_g1171z00_2210;
						BgL_tailsz00_2213 = BgL_tailsz00_2141;
					BgL_liipz00_2211:
						if (NULLP(BgL_clausesz00_2212))
							{	/* Tailc/walk.scm 161 */
								BgL_res1806z00_2218 = BgL_tailsz00_2213;
							}
						else
							{	/* Tailc/walk.scm 163 */
								obj_t BgL_arg1625z00_2214;
								obj_t BgL_arg1626z00_2215;

								BgL_arg1625z00_2214 = CDR(((obj_t) BgL_clausesz00_2212));
								{	/* Tailc/walk.scm 164 */
									obj_t BgL_arg1627z00_2216;

									{	/* Tailc/walk.scm 164 */
										obj_t BgL_pairz00_2217;

										BgL_pairz00_2217 = CAR(((obj_t) BgL_clausesz00_2212));
										BgL_arg1627z00_2216 = CDR(BgL_pairz00_2217);
									}
									BgL_arg1626z00_2215 =
										BGl_getzd2tailzd2callsz00zztailc_walkz00(
										((BgL_nodez00_bglt) BgL_arg1627z00_2216),
										((BgL_variablez00_bglt) BgL_hostz00_2140),
										BgL_tailsz00_2213);
								}
								{
									obj_t BgL_tailsz00_2845;
									obj_t BgL_clausesz00_2844;

									BgL_clausesz00_2844 = BgL_arg1625z00_2214;
									BgL_tailsz00_2845 = BgL_arg1626z00_2215;
									BgL_tailsz00_2213 = BgL_tailsz00_2845;
									BgL_clausesz00_2212 = BgL_clausesz00_2844;
									goto BgL_liipz00_2211;
								}
							}
					}
				}
				return BgL_res1806z00_2218;
			}
		}

	}



/* &get-tail-calls-condi1321 */
	obj_t BGl_z62getzd2tailzd2callszd2condi1321zb0zztailc_walkz00(obj_t
		BgL_envz00_2142, obj_t BgL_nodez00_2143, obj_t BgL_hostz00_2144,
		obj_t BgL_tailsz00_2145)
	{
		{	/* Tailc/walk.scm 150 */
			{	/* Tailc/walk.scm 152 */
				BgL_nodez00_bglt BgL_arg1611z00_2220;
				obj_t BgL_arg1613z00_2221;

				BgL_arg1611z00_2220 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2143)))->BgL_truez00);
				{	/* Tailc/walk.scm 152 */
					BgL_nodez00_bglt BgL_arg1615z00_2222;

					BgL_arg1615z00_2222 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2143)))->BgL_falsez00);
					BgL_arg1613z00_2221 =
						BGl_getzd2tailzd2callsz00zztailc_walkz00(BgL_arg1615z00_2222,
						((BgL_variablez00_bglt) BgL_hostz00_2144), BgL_tailsz00_2145);
				}
				return
					BGl_getzd2tailzd2callsz00zztailc_walkz00(BgL_arg1611z00_2220,
					((BgL_variablez00_bglt) BgL_hostz00_2144), BgL_arg1613z00_2221);
			}
		}

	}



/* &get-tail-calls-app1319 */
	obj_t BGl_z62getzd2tailzd2callszd2app1319zb0zztailc_walkz00(obj_t
		BgL_envz00_2146, obj_t BgL_nodez00_2147, obj_t BgL_hostz00_2148,
		obj_t BgL_tailsz00_2149)
	{
		{	/* Tailc/walk.scm 133 */
			{	/* Tailc/walk.scm 135 */
				BgL_variablez00_bglt BgL_calleez00_2224;

				BgL_calleez00_2224 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_2147)))->BgL_funz00)))->
					BgL_variablez00);
				if ((((obj_t) BgL_calleez00_2224) == BgL_hostz00_2148))
					{	/* Tailc/walk.scm 137 */
						return
							MAKE_YOUNG_PAIR(
							((obj_t)
								((BgL_appz00_bglt) BgL_nodez00_2147)), BgL_tailsz00_2149);
					}
				else
					{	/* Tailc/walk.scm 139 */
						bool_t BgL_test1850z00_2863;

						{	/* Tailc/walk.scm 139 */
							obj_t BgL_classz00_2225;

							BgL_classz00_2225 = BGl_localzd2tailzd2zztailc_walkz00;
							{	/* Tailc/walk.scm 139 */
								obj_t BgL_oclassz00_2226;

								{	/* Tailc/walk.scm 139 */
									obj_t BgL_arg1815z00_2227;
									long BgL_arg1816z00_2228;

									BgL_arg1815z00_2227 = (BGl_za2classesza2z00zz__objectz00);
									{	/* Tailc/walk.scm 139 */
										long BgL_arg1817z00_2229;

										BgL_arg1817z00_2229 =
											BGL_OBJECT_CLASS_NUM(
											((BgL_objectz00_bglt) BgL_calleez00_2224));
										BgL_arg1816z00_2228 = (BgL_arg1817z00_2229 - OBJECT_TYPE);
									}
									BgL_oclassz00_2226 =
										VECTOR_REF(BgL_arg1815z00_2227, BgL_arg1816z00_2228);
								}
								BgL_test1850z00_2863 =
									(BgL_oclassz00_2226 == BgL_classz00_2225);
						}}
						if (BgL_test1850z00_2863)
							{	/* Tailc/walk.scm 139 */
								return BgL_tailsz00_2149;
							}
						else
							{	/* Tailc/walk.scm 141 */
								bool_t BgL_test1851z00_2870;

								{	/* Tailc/walk.scm 141 */
									obj_t BgL_classz00_2230;

									BgL_classz00_2230 = BGl_localz00zzast_varz00;
									{	/* Tailc/walk.scm 141 */
										BgL_objectz00_bglt BgL_arg1807z00_2231;

										{	/* Tailc/walk.scm 141 */
											obj_t BgL_tmpz00_2871;

											BgL_tmpz00_2871 =
												((obj_t) ((BgL_objectz00_bglt) BgL_calleez00_2224));
											BgL_arg1807z00_2231 =
												(BgL_objectz00_bglt) (BgL_tmpz00_2871);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Tailc/walk.scm 141 */
												long BgL_idxz00_2232;

												BgL_idxz00_2232 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2231);
												BgL_test1851z00_2870 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2232 + 2L)) == BgL_classz00_2230);
											}
										else
											{	/* Tailc/walk.scm 141 */
												bool_t BgL_res1779z00_2235;

												{	/* Tailc/walk.scm 141 */
													obj_t BgL_oclassz00_2236;

													{	/* Tailc/walk.scm 141 */
														obj_t BgL_arg1815z00_2237;
														long BgL_arg1816z00_2238;

														BgL_arg1815z00_2237 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Tailc/walk.scm 141 */
															long BgL_arg1817z00_2239;

															BgL_arg1817z00_2239 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2231);
															BgL_arg1816z00_2238 =
																(BgL_arg1817z00_2239 - OBJECT_TYPE);
														}
														BgL_oclassz00_2236 =
															VECTOR_REF(BgL_arg1815z00_2237,
															BgL_arg1816z00_2238);
													}
													{	/* Tailc/walk.scm 141 */
														bool_t BgL__ortest_1115z00_2240;

														BgL__ortest_1115z00_2240 =
															(BgL_classz00_2230 == BgL_oclassz00_2236);
														if (BgL__ortest_1115z00_2240)
															{	/* Tailc/walk.scm 141 */
																BgL_res1779z00_2235 = BgL__ortest_1115z00_2240;
															}
														else
															{	/* Tailc/walk.scm 141 */
																long BgL_odepthz00_2241;

																{	/* Tailc/walk.scm 141 */
																	obj_t BgL_arg1804z00_2242;

																	BgL_arg1804z00_2242 = (BgL_oclassz00_2236);
																	BgL_odepthz00_2241 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2242);
																}
																if ((2L < BgL_odepthz00_2241))
																	{	/* Tailc/walk.scm 141 */
																		obj_t BgL_arg1802z00_2243;

																		{	/* Tailc/walk.scm 141 */
																			obj_t BgL_arg1803z00_2244;

																			BgL_arg1803z00_2244 =
																				(BgL_oclassz00_2236);
																			BgL_arg1802z00_2243 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2244, 2L);
																		}
																		BgL_res1779z00_2235 =
																			(BgL_arg1802z00_2243 ==
																			BgL_classz00_2230);
																	}
																else
																	{	/* Tailc/walk.scm 141 */
																		BgL_res1779z00_2235 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1851z00_2870 = BgL_res1779z00_2235;
											}
									}
								}
								if (BgL_test1851z00_2870)
									{	/* Tailc/walk.scm 141 */
										{	/* Tailc/walk.scm 142 */
											BgL_localzd2tailzd2_bglt BgL_wide1167z00_2245;

											BgL_wide1167z00_2245 =
												((BgL_localzd2tailzd2_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_localzd2tailzd2_bgl))));
											{	/* Tailc/walk.scm 142 */
												obj_t BgL_auxz00_2899;
												BgL_objectz00_bglt BgL_tmpz00_2895;

												BgL_auxz00_2899 = ((obj_t) BgL_wide1167z00_2245);
												BgL_tmpz00_2895 =
													((BgL_objectz00_bglt)
													((BgL_localz00_bglt)
														((BgL_localz00_bglt) BgL_calleez00_2224)));
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2895,
													BgL_auxz00_2899);
											}
											((BgL_objectz00_bglt)
												((BgL_localz00_bglt)
													((BgL_localz00_bglt) BgL_calleez00_2224)));
											{	/* Tailc/walk.scm 142 */
												long BgL_arg1602z00_2246;

												BgL_arg1602z00_2246 =
													BGL_CLASS_NUM(BGl_localzd2tailzd2zztailc_walkz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt)
														((BgL_localz00_bglt)
															((BgL_localz00_bglt) BgL_calleez00_2224))),
													BgL_arg1602z00_2246);
											}
											((BgL_localz00_bglt)
												((BgL_localz00_bglt)
													((BgL_localz00_bglt) BgL_calleez00_2224)));
										}
										((BgL_localz00_bglt)
											((BgL_localz00_bglt) BgL_calleez00_2224));
										{	/* Tailc/walk.scm 143 */
											obj_t BgL_arg1605z00_2247;

											BgL_arg1605z00_2247 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt)
															(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt)
																				BgL_calleez00_2224))))->
																BgL_valuez00))))->BgL_bodyz00);
											return
												BGl_getzd2tailzd2callsz00zztailc_walkz00((
													(BgL_nodez00_bglt) BgL_arg1605z00_2247),
												((BgL_variablez00_bglt) BgL_hostz00_2148),
												BgL_tailsz00_2149);
										}
									}
								else
									{	/* Tailc/walk.scm 141 */
										return BgL_tailsz00_2149;
									}
							}
					}
			}
		}

	}



/* &get-tail-calls-sync1317 */
	obj_t BGl_z62getzd2tailzd2callszd2sync1317zb0zztailc_walkz00(obj_t
		BgL_envz00_2150, obj_t BgL_nodez00_2151, obj_t BgL_hostz00_2152,
		obj_t BgL_tailsz00_2153)
	{
		{	/* Tailc/walk.scm 127 */
			return ((obj_t) BgL_tailsz00_2153);
		}

	}



/* &get-tail-calls-seque1315 */
	obj_t BGl_z62getzd2tailzd2callszd2seque1315zb0zztailc_walkz00(obj_t
		BgL_envz00_2154, obj_t BgL_nodez00_2155, obj_t BgL_hostz00_2156,
		obj_t BgL_tailsz00_2157)
	{
		{	/* Tailc/walk.scm 118 */
			if (NULLP(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_2155)))->BgL_nodesz00)))
				{	/* Tailc/walk.scm 120 */
					return BgL_tailsz00_2157;
				}
			else
				{	/* Tailc/walk.scm 122 */
					obj_t BgL_arg1589z00_2251;

					BgL_arg1589z00_2251 =
						CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
							(((BgL_sequencez00_bglt) COBJECT(
										((BgL_sequencez00_bglt) BgL_nodez00_2155)))->
								BgL_nodesz00)));
					return BGl_getzd2tailzd2callsz00zztailc_walkz00(((BgL_nodez00_bglt)
							BgL_arg1589z00_2251), ((BgL_variablez00_bglt) BgL_hostz00_2156),
						BgL_tailsz00_2157);
				}
		}

	}



/* &get-tail-calls-closu1313 */
	obj_t BGl_z62getzd2tailzd2callszd2closu1313zb0zztailc_walkz00(obj_t
		BgL_envz00_2158, obj_t BgL_nodez00_2159, obj_t BgL_hostz00_2160,
		obj_t BgL_tailsz00_2161)
	{
		{	/* Tailc/walk.scm 112 */
			{	/* Tailc/walk.scm 113 */
				obj_t BgL_res1808z00_2254;

				{	/* Tailc/walk.scm 113 */
					obj_t BgL_arg1584z00_2253;

					BgL_arg1584z00_2253 =
						BGl_shapez00zztools_shapez00(
						((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_2159)));
					BgL_res1808z00_2254 =
						BGl_internalzd2errorzd2zztools_errorz00
						(BGl_string1794z00zztailc_walkz00, BGl_string1802z00zztailc_walkz00,
						BgL_arg1584z00_2253);
				}
				return BgL_res1808z00_2254;
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztailc_walkz00(void)
	{
		{	/* Tailc/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1803z00zztailc_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1803z00zztailc_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1803z00zztailc_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1803z00zztailc_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1803z00zztailc_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1803z00zztailc_walkz00));
			BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string1803z00zztailc_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1803z00zztailc_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1803z00zztailc_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
