/*===========================================================================*/
/*   (Jas/peep.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Jas/peep.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_JAS_PEEP_TYPE_DEFINITIONS
#define BGL_JAS_PEEP_TYPE_DEFINITIONS
#endif													// BGL_JAS_PEEP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzjas_peepz00 = BUNSPEC;
	static bool_t BGl_hugezf3zf3zzjas_peepz00(obj_t);
	static obj_t BGl_branchzf3zf3zzjas_peepz00(obj_t);
	static bool_t BGl_store2zf3zf3zzjas_peepz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzjas_peepz00(void);
	static obj_t BGl_zc3z04anonymousza32450ze3ze70z60zzjas_peepz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_walkzd2atzd2labze70ze7zzjas_peepz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzjas_peepz00(void);
	static obj_t BGl_objectzd2initzd2zzjas_peepz00(void);
	BGL_IMPORT obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_walkze70ze7zzjas_peepz00(obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_peepz00zzjas_peepz00 = BUNSPEC;
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	static obj_t BGl_nocontzf3zf3zzjas_peepz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2jaszd2peepholeza2zd2zzjas_peepz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t make_vector(long, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzjas_peepz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzjas_peepz00(void);
	static bool_t BGl_load2zf3zf3zzjas_peepz00(obj_t);
	static obj_t BGl_singlezd2labzf3z21zzjas_peepz00(obj_t);
	static obj_t BGl_simplematchz00zzjas_peepz00(obj_t);
	BGL_EXPORTED_DEF obj_t
		BGl_za2za2lastzd2numberzd2ofzd2localsza2za2zd2zzjas_peepz00 = BUNSPEC;
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	static obj_t BGl_removesequencez00zzjas_peepz00(obj_t);
	static obj_t BGl_z62zc3z04peepza31226ze3ze5zzjas_peepz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzjas_peepz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_stackz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_classfilez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	static bool_t BGl_loadzf3zf3zzjas_peepz00(obj_t);
	extern obj_t BGl_nbpushz00zzjas_stackz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzjas_peepz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzjas_peepz00(void);
	static obj_t BGl_walkzd2fromze70z35zzjas_peepz00(obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzjas_peepz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzjas_peepz00(void);
	static obj_t BGl_keeplocalsz00zzjas_peepz00(obj_t, long, long, obj_t);
	static long BGl_packlocalsz00zzjas_peepz00(obj_t, long, long, obj_t);
	static obj_t BGl_collectze70ze7zzjas_peepz00(obj_t, obj_t, obj_t, long);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_countzd2insze70z35zzjas_peepz00(obj_t, obj_t);
	static obj_t BGl_removebranchz00zzjas_peepz00(obj_t);
	static obj_t BGl_countzd2labze70z35zzjas_peepz00(obj_t, obj_t);
	static bool_t BGl_storezf3zf3zzjas_peepz00(obj_t);
	extern obj_t BGl_nbpopz00zzjas_stackz00(obj_t, obj_t);
	static obj_t __cnst[9];


	   
		 
		DEFINE_STRING(BGl_string2605z00zzjas_peepz00,
		BgL_bgl_string2605za700za7za7j2608za7, "jas_peep", 8);
	      DEFINE_STRING(BGl_string2606z00zzjas_peepz00,
		BgL_bgl_string2606za700za7za7j2609za7,
		"done begin (4) (3) h dummy ok (198 199) (167 191) ", 50);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2604z00zzjas_peepz00,
		BgL_bgl_za762za7c3za704peepza7a32610z00,
		BGl_z62zc3z04peepza31226ze3ze5zzjas_peepz00, 0L, BUNSPEC, 4);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzjas_peepz00));
		     ADD_ROOT((void *) (&BGl_peepz00zzjas_peepz00));
		     ADD_ROOT((void *) (&BGl_za2jaszd2peepholeza2zd2zzjas_peepz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2za2lastzd2numberzd2ofzd2localsza2za2zd2zzjas_peepz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzjas_peepz00(long
		BgL_checksumz00_2679, char *BgL_fromz00_2680)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzjas_peepz00))
				{
					BGl_requirezd2initializa7ationz75zzjas_peepz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzjas_peepz00();
					BGl_libraryzd2moduleszd2initz00zzjas_peepz00();
					BGl_cnstzd2initzd2zzjas_peepz00();
					BGl_importedzd2moduleszd2initz00zzjas_peepz00();
					return BGl_toplevelzd2initzd2zzjas_peepz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzjas_peepz00(void)
	{
		{	/* Jas/peep.scm 1 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"jas_peep");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "jas_peep");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "jas_peep");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "jas_peep");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "jas_peep");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "jas_peep");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "jas_peep");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"jas_peep");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzjas_peepz00(void)
	{
		{	/* Jas/peep.scm 1 */
			{	/* Jas/peep.scm 1 */
				obj_t BgL_cportz00_2667;

				{	/* Jas/peep.scm 1 */
					obj_t BgL_stringz00_2674;

					BgL_stringz00_2674 = BGl_string2606z00zzjas_peepz00;
					{	/* Jas/peep.scm 1 */
						obj_t BgL_startz00_2675;

						BgL_startz00_2675 = BINT(0L);
						{	/* Jas/peep.scm 1 */
							obj_t BgL_endz00_2676;

							BgL_endz00_2676 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2674)));
							{	/* Jas/peep.scm 1 */

								BgL_cportz00_2667 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2674, BgL_startz00_2675, BgL_endz00_2676);
				}}}}
				{
					long BgL_iz00_2668;

					BgL_iz00_2668 = 8L;
				BgL_loopz00_2669:
					if ((BgL_iz00_2668 == -1L))
						{	/* Jas/peep.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* Jas/peep.scm 1 */
							{	/* Jas/peep.scm 1 */
								obj_t BgL_arg2607z00_2670;

								{	/* Jas/peep.scm 1 */

									{	/* Jas/peep.scm 1 */
										obj_t BgL_locationz00_2672;

										BgL_locationz00_2672 = BBOOL(((bool_t) 0));
										{	/* Jas/peep.scm 1 */

											BgL_arg2607z00_2670 =
												BGl_readz00zz__readerz00(BgL_cportz00_2667,
												BgL_locationz00_2672);
										}
									}
								}
								{	/* Jas/peep.scm 1 */
									int BgL_tmpz00_2706;

									BgL_tmpz00_2706 = (int) (BgL_iz00_2668);
									CNST_TABLE_SET(BgL_tmpz00_2706, BgL_arg2607z00_2670);
							}}
							{	/* Jas/peep.scm 1 */
								int BgL_auxz00_2673;

								BgL_auxz00_2673 = (int) ((BgL_iz00_2668 - 1L));
								{
									long BgL_iz00_2711;

									BgL_iz00_2711 = (long) (BgL_auxz00_2673);
									BgL_iz00_2668 = BgL_iz00_2711;
									goto BgL_loopz00_2669;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzjas_peepz00(void)
	{
		{	/* Jas/peep.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzjas_peepz00(void)
	{
		{	/* Jas/peep.scm 1 */
			BGl_za2jaszd2peepholeza2zd2zzjas_peepz00 = BTRUE;
			BGl_za2za2lastzd2numberzd2ofzd2localsza2za2zd2zzjas_peepz00 = BINT(0L);
			return (BGl_peepz00zzjas_peepz00 = BGl_proc2604z00zzjas_peepz00, BUNSPEC);
		}

	}



/* &<@peep:1226> */
	obj_t BGl_z62zc3z04peepza31226ze3ze5zzjas_peepz00(obj_t BgL_envz00_2652,
		obj_t BgL_classfilez00_2653, obj_t BgL_paramz00_2654,
		obj_t BgL_localsz00_2655, obj_t BgL_codez00_2656)
	{
		{	/* Jas/peep.scm 9 */
			BGl_za2za2lastzd2numberzd2ofzd2localsza2za2zd2zzjas_peepz00 =
				BINT(
				(bgl_list_length(BgL_paramz00_2654) +
					bgl_list_length(BgL_localsz00_2655)));
			if (CBOOL(BGl_za2jaszd2peepholeza2zd2zzjas_peepz00))
				{	/* Jas/peep.scm 12 */
					obj_t BgL_c1z00_2678;

					BgL_c1z00_2678 =
						BGl_keeplocalsz00zzjas_peepz00(BgL_classfilez00_2653,
						bgl_list_length(BgL_paramz00_2654),
						bgl_list_length(BgL_localsz00_2655), BgL_codez00_2656);
					BGl_simplematchz00zzjas_peepz00(BgL_c1z00_2678);
					BGl_packlocalsz00zzjas_peepz00(BgL_classfilez00_2653,
						bgl_list_length(BgL_paramz00_2654),
						bgl_list_length(BgL_localsz00_2655), BgL_c1z00_2678);
					BGl_removebranchz00zzjas_peepz00(BgL_c1z00_2678);
					BGl_removesequencez00zzjas_peepz00(BgL_c1z00_2678);
					return BgL_c1z00_2678;
				}
			else
				{	/* Jas/peep.scm 11 */
					return BgL_codez00_2656;
				}
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzjas_peepz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_283;

				BgL_headz00_283 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_284;
					obj_t BgL_tailz00_285;

					BgL_prevz00_284 = BgL_headz00_283;
					BgL_tailz00_285 = BgL_l1z00_1;
				BgL_loopz00_286:
					if (PAIRP(BgL_tailz00_285))
						{
							obj_t BgL_newzd2prevzd2_288;

							BgL_newzd2prevzd2_288 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_285), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_284, BgL_newzd2prevzd2_288);
							{
								obj_t BgL_tailz00_2737;
								obj_t BgL_prevz00_2736;

								BgL_prevz00_2736 = BgL_newzd2prevzd2_288;
								BgL_tailz00_2737 = CDR(BgL_tailz00_285);
								BgL_tailz00_285 = BgL_tailz00_2737;
								BgL_prevz00_284 = BgL_prevz00_2736;
								goto BgL_loopz00_286;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_283);
				}
			}
		}

	}



/* load? */
	bool_t BGl_loadzf3zf3zzjas_peepz00(obj_t BgL_insz00_3)
	{
		{	/* Jas/peep.scm 21 */
			{	/* Jas/peep.scm 22 */
				obj_t BgL_opcodez00_291;

				BgL_opcodez00_291 = CAR(((obj_t) BgL_insz00_3));
				if (((long) CINT(BgL_opcodez00_291) >= 21L))
					{	/* Jas/peep.scm 23 */
						return ((long) CINT(BgL_opcodez00_291) <= 25L);
					}
				else
					{	/* Jas/peep.scm 23 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* load2? */
	bool_t BGl_load2zf3zf3zzjas_peepz00(obj_t BgL_insz00_4)
	{
		{	/* Jas/peep.scm 25 */
			{	/* Jas/peep.scm 26 */
				obj_t BgL_opcodez00_293;

				BgL_opcodez00_293 = CAR(BgL_insz00_4);
				{	/* Jas/peep.scm 27 */
					bool_t BgL__ortest_1058z00_294;

					BgL__ortest_1058z00_294 = ((long) CINT(BgL_opcodez00_293) == 22L);
					if (BgL__ortest_1058z00_294)
						{	/* Jas/peep.scm 27 */
							return BgL__ortest_1058z00_294;
						}
					else
						{	/* Jas/peep.scm 27 */
							return ((long) CINT(BgL_opcodez00_293) == 24L);
		}}}}

	}



/* store? */
	bool_t BGl_storezf3zf3zzjas_peepz00(obj_t BgL_insz00_5)
	{
		{	/* Jas/peep.scm 29 */
			{	/* Jas/peep.scm 30 */
				obj_t BgL_opcodez00_295;

				BgL_opcodez00_295 = CAR(((obj_t) BgL_insz00_5));
				if (((long) CINT(BgL_opcodez00_295) >= 54L))
					{	/* Jas/peep.scm 31 */
						return ((long) CINT(BgL_opcodez00_295) <= 58L);
					}
				else
					{	/* Jas/peep.scm 31 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* store2? */
	bool_t BGl_store2zf3zf3zzjas_peepz00(obj_t BgL_insz00_6)
	{
		{	/* Jas/peep.scm 33 */
			{	/* Jas/peep.scm 34 */
				obj_t BgL_opcodez00_297;

				BgL_opcodez00_297 = CAR(BgL_insz00_6);
				{	/* Jas/peep.scm 35 */
					bool_t BgL__ortest_1060z00_298;

					BgL__ortest_1060z00_298 = ((long) CINT(BgL_opcodez00_297) == 55L);
					if (BgL__ortest_1060z00_298)
						{	/* Jas/peep.scm 35 */
							return BgL__ortest_1060z00_298;
						}
					else
						{	/* Jas/peep.scm 35 */
							return ((long) CINT(BgL_opcodez00_297) == 57L);
		}}}}

	}



/* nocont? */
	obj_t BGl_nocontzf3zf3zzjas_peepz00(obj_t BgL_insz00_10)
	{
		{	/* Jas/peep.scm 49 */
			{	/* Jas/peep.scm 50 */
				obj_t BgL_opcodez00_302;

				BgL_opcodez00_302 = CAR(((obj_t) BgL_insz00_10));
				{	/* Jas/peep.scm 51 */
					bool_t BgL_test2619z00_2768;

					if (((long) CINT(BgL_opcodez00_302) >= 169L))
						{	/* Jas/peep.scm 51 */
							BgL_test2619z00_2768 = ((long) CINT(BgL_opcodez00_302) <= 177L);
						}
					else
						{	/* Jas/peep.scm 51 */
							BgL_test2619z00_2768 = ((bool_t) 0);
						}
					if (BgL_test2619z00_2768)
						{	/* Jas/peep.scm 51 */
							return BTRUE;
						}
					else
						{	/* Jas/peep.scm 51 */
							return
								BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_opcodez00_302,
								CNST_TABLE_REF(0));
						}
				}
			}
		}

	}



/* huge? */
	bool_t BGl_hugezf3zf3zzjas_peepz00(obj_t BgL_insz00_11)
	{
		{	/* Jas/peep.scm 55 */
			{	/* Jas/peep.scm 56 */
				obj_t BgL_opcodez00_306;

				BgL_opcodez00_306 = CAR(((obj_t) BgL_insz00_11));
				{	/* Jas/peep.scm 57 */
					bool_t BgL__ortest_1061z00_307;

					BgL__ortest_1061z00_307 = ((long) CINT(BgL_opcodez00_306) == 170L);
					if (BgL__ortest_1061z00_307)
						{	/* Jas/peep.scm 57 */
							return BgL__ortest_1061z00_307;
						}
					else
						{	/* Jas/peep.scm 57 */
							return ((long) CINT(BgL_opcodez00_306) == 171L);
		}}}}

	}



/* single-lab? */
	obj_t BGl_singlezd2labzf3z21zzjas_peepz00(obj_t BgL_insz00_12)
	{
		{	/* Jas/peep.scm 59 */
			{	/* Jas/peep.scm 60 */
				obj_t BgL_opcodez00_308;

				BgL_opcodez00_308 = CAR(BgL_insz00_12);
				{	/* Jas/peep.scm 61 */
					bool_t BgL_test2622z00_2784;

					if (((long) CINT(BgL_opcodez00_308) >= 153L))
						{	/* Jas/peep.scm 61 */
							BgL_test2622z00_2784 = ((long) CINT(BgL_opcodez00_308) <= 168L);
						}
					else
						{	/* Jas/peep.scm 61 */
							BgL_test2622z00_2784 = ((bool_t) 0);
						}
					if (BgL_test2622z00_2784)
						{	/* Jas/peep.scm 61 */
							return BTRUE;
						}
					else
						{	/* Jas/peep.scm 61 */
							return
								BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_opcodez00_308,
								CNST_TABLE_REF(1));
						}
				}
			}
		}

	}



/* branch? */
	obj_t BGl_branchzf3zf3zzjas_peepz00(obj_t BgL_insz00_13)
	{
		{	/* Jas/peep.scm 65 */
			{	/* Jas/peep.scm 66 */
				obj_t BgL_opcodez00_312;

				BgL_opcodez00_312 = CAR(((obj_t) BgL_insz00_13));
				{	/* Jas/peep.scm 67 */
					bool_t BgL_test2624z00_2794;

					if (((long) CINT(BgL_opcodez00_312) >= 153L))
						{	/* Jas/peep.scm 67 */
							BgL_test2624z00_2794 = ((long) CINT(BgL_opcodez00_312) <= 171L);
						}
					else
						{	/* Jas/peep.scm 67 */
							BgL_test2624z00_2794 = ((bool_t) 0);
						}
					if (BgL_test2624z00_2794)
						{	/* Jas/peep.scm 67 */
							return BTRUE;
						}
					else
						{	/* Jas/peep.scm 67 */
							return
								BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_opcodez00_312,
								CNST_TABLE_REF(1));
						}
				}
			}
		}

	}



/* keeplocals */
	obj_t BGl_keeplocalsz00zzjas_peepz00(obj_t BgL_classfilez00_14,
		long BgL_nparamz00_15, long BgL_nlocalsz00_16, obj_t BgL_codez00_17)
	{
		{	/* Jas/peep.scm 74 */
			{	/* Jas/peep.scm 75 */
				long BgL_nz00_316;

				BgL_nz00_316 = (BgL_nparamz00_15 + BgL_nlocalsz00_16);
				{	/* Jas/peep.scm 75 */
					obj_t BgL_wz00_317;

					BgL_wz00_317 = make_vector(BgL_nz00_316, BINT(0L));
					{	/* Jas/peep.scm 76 */
						obj_t BgL_rz00_318;

						BgL_rz00_318 = make_vector(BgL_nz00_316, BINT(0L));
						{	/* Jas/peep.scm 77 */
							obj_t BgL_candidatez00_319;

							BgL_candidatez00_319 = make_vector(BgL_nz00_316, BTRUE);
							{	/* Jas/peep.scm 78 */

								{
									obj_t BgL_pz00_399;
									obj_t BgL_lz00_400;
									obj_t BgL_pz00_388;
									obj_t BgL_lz00_389;
									obj_t BgL_vz00_364;
									obj_t BgL_pz00_365;
									obj_t BgL_lz00_366;
									long BgL_spz00_367;
									long BgL_iz00_356;
									obj_t BgL_insz00_345;
									long BgL_iz00_336;

									BgL_iz00_336 = 0L;
								BgL_zc3z04anonymousza31249ze3z87_337:
									if ((BgL_iz00_336 < BgL_nparamz00_15))
										{	/* Jas/peep.scm 80 */
											VECTOR_SET(BgL_wz00_317, BgL_iz00_336, BINT(1L));
											{
												long BgL_iz00_2812;

												BgL_iz00_2812 = (BgL_iz00_336 + 1L);
												BgL_iz00_336 = BgL_iz00_2812;
												goto BgL_zc3z04anonymousza31249ze3z87_337;
											}
										}
									else
										{	/* Jas/peep.scm 80 */
											CNST_TABLE_REF(2);
										}
									{
										obj_t BgL_l1205z00_328;

										BgL_l1205z00_328 = BgL_codez00_17;
									BgL_zc3z04anonymousza31244ze3z87_329:
										if (PAIRP(BgL_l1205z00_328))
											{	/* Jas/peep.scm 132 */
												{	/* Jas/peep.scm 132 */
													obj_t BgL_xz00_331;

													BgL_xz00_331 = CAR(BgL_l1205z00_328);
													if (PAIRP(BgL_xz00_331))
														{	/* Jas/peep.scm 132 */
															BgL_insz00_345 = BgL_xz00_331;
															if (BGl_loadzf3zf3zzjas_peepz00(BgL_insz00_345))
																{	/* Jas/peep.scm 85 */
																	obj_t BgL_arg1284z00_348;

																	BgL_arg1284z00_348 = CAR(CDR(BgL_insz00_345));
																	{	/* Jas/peep.scm 83 */
																		long BgL_arg1268z00_1770;

																		{	/* Jas/peep.scm 83 */
																			long BgL_arg1272z00_1771;

																			{	/* Jas/peep.scm 83 */
																				long BgL_kz00_1773;

																				BgL_kz00_1773 =
																					(long) CINT(BgL_arg1284z00_348);
																				BgL_arg1272z00_1771 =
																					(long) CINT(VECTOR_REF(BgL_rz00_318,
																						BgL_kz00_1773));
																			}
																			BgL_arg1268z00_1770 =
																				(1L + BgL_arg1272z00_1771);
																		}
																		VECTOR_SET(BgL_rz00_318,
																			(long) CINT(BgL_arg1284z00_348),
																			BINT(BgL_arg1268z00_1770));
																}}
															else
																{	/* Jas/peep.scm 85 */
																	if (BGl_storezf3zf3zzjas_peepz00
																		(BgL_insz00_345))
																		{	/* Jas/peep.scm 86 */
																			obj_t BgL_arg1304z00_350;

																			BgL_arg1304z00_350 =
																				CAR(CDR(BgL_insz00_345));
																			{	/* Jas/peep.scm 83 */
																				long BgL_arg1268z00_1781;

																				{	/* Jas/peep.scm 83 */
																					long BgL_arg1272z00_1782;

																					{	/* Jas/peep.scm 83 */
																						long BgL_kz00_1784;

																						BgL_kz00_1784 =
																							(long) CINT(BgL_arg1304z00_350);
																						BgL_arg1272z00_1782 =
																							(long)
																							CINT(VECTOR_REF(BgL_wz00_317,
																								BgL_kz00_1784));
																					}
																					BgL_arg1268z00_1781 =
																						(1L + BgL_arg1272z00_1782);
																				}
																				VECTOR_SET(BgL_wz00_317,
																					(long) CINT(BgL_arg1304z00_350),
																					BINT(BgL_arg1268z00_1781));
																		}}
																	else
																		{	/* Jas/peep.scm 86 */
																			if (
																				((long) CINT(CAR(BgL_insz00_345)) ==
																					169L))
																				{	/* Jas/peep.scm 87 */
																					obj_t BgL_arg1306z00_352;

																					BgL_arg1306z00_352 =
																						CAR(CDR(BgL_insz00_345));
																					{	/* Jas/peep.scm 83 */
																						long BgL_arg1268z00_1795;

																						{	/* Jas/peep.scm 83 */
																							long BgL_arg1272z00_1796;

																							{	/* Jas/peep.scm 83 */
																								long BgL_kz00_1798;

																								BgL_kz00_1798 =
																									(long)
																									CINT(BgL_arg1306z00_352);
																								BgL_arg1272z00_1796 =
																									(long)
																									CINT(VECTOR_REF(BgL_rz00_318,
																										BgL_kz00_1798));
																							}
																							BgL_arg1268z00_1795 =
																								(1L + BgL_arg1272z00_1796);
																						}
																						VECTOR_SET(BgL_rz00_318,
																							(long) CINT(BgL_arg1306z00_352),
																							BINT(BgL_arg1268z00_1795));
																				}}
																			else
																				{	/* Jas/peep.scm 87 */
																					if (
																						((long) CINT(CAR(BgL_insz00_345)) ==
																							132L))
																						{	/* Jas/peep.scm 88 */
																							{	/* Jas/peep.scm 89 */
																								obj_t BgL_arg1308z00_354;

																								BgL_arg1308z00_354 =
																									CAR(CDR(BgL_insz00_345));
																								{	/* Jas/peep.scm 83 */
																									long BgL_arg1268z00_1809;

																									{	/* Jas/peep.scm 83 */
																										long BgL_arg1272z00_1810;

																										{	/* Jas/peep.scm 83 */
																											long BgL_kz00_1812;

																											BgL_kz00_1812 =
																												(long)
																												CINT
																												(BgL_arg1308z00_354);
																											BgL_arg1272z00_1810 =
																												(long)
																												CINT(VECTOR_REF
																												(BgL_wz00_317,
																													BgL_kz00_1812));
																										}
																										BgL_arg1268z00_1809 =
																											(1L +
																											BgL_arg1272z00_1810);
																									}
																									VECTOR_SET(BgL_wz00_317,
																										(long)
																										CINT(BgL_arg1308z00_354),
																										BINT(BgL_arg1268z00_1809));
																							}}
																							{	/* Jas/peep.scm 90 */
																								obj_t BgL_arg1310z00_355;

																								BgL_arg1310z00_355 =
																									CAR(CDR(BgL_insz00_345));
																								{	/* Jas/peep.scm 83 */
																									long BgL_arg1268z00_1820;

																									{	/* Jas/peep.scm 83 */
																										long BgL_arg1272z00_1821;

																										{	/* Jas/peep.scm 83 */
																											long BgL_kz00_1823;

																											BgL_kz00_1823 =
																												(long)
																												CINT
																												(BgL_arg1310z00_355);
																											BgL_arg1272z00_1821 =
																												(long)
																												CINT(VECTOR_REF
																												(BgL_rz00_318,
																													BgL_kz00_1823));
																										}
																										BgL_arg1268z00_1820 =
																											(1L +
																											BgL_arg1272z00_1821);
																									}
																									VECTOR_SET(BgL_rz00_318,
																										(long)
																										CINT(BgL_arg1310z00_355),
																										BINT(BgL_arg1268z00_1820));
																						}}}
																					else
																						{	/* Jas/peep.scm 88 */
																							BFALSE;
																						}
																				}
																		}
																}
														}
													else
														{	/* Jas/peep.scm 132 */
															BFALSE;
														}
												}
												{
													obj_t BgL_l1205z00_2877;

													BgL_l1205z00_2877 = CDR(BgL_l1205z00_328);
													BgL_l1205z00_328 = BgL_l1205z00_2877;
													goto BgL_zc3z04anonymousza31244ze3z87_329;
												}
											}
										else
											{	/* Jas/peep.scm 132 */
												((bool_t) 1);
											}
									}
									BgL_iz00_356 = 0L;
								BgL_zc3z04anonymousza31311ze3z87_357:
									if ((BgL_iz00_356 < BgL_nz00_316))
										{	/* Jas/peep.scm 92 */
											{	/* Jas/peep.scm 95 */
												bool_t BgL_arg1314z00_359;

												if (
													((long) CINT(VECTOR_REF(BgL_wz00_317,
																BgL_iz00_356)) == 1L))
													{	/* Jas/peep.scm 95 */
														BgL_arg1314z00_359 =
															(
															(long) CINT(VECTOR_REF(BgL_rz00_318,
																	BgL_iz00_356)) == 1L);
													}
												else
													{	/* Jas/peep.scm 95 */
														BgL_arg1314z00_359 = ((bool_t) 0);
													}
												VECTOR_SET(BgL_candidatez00_319, BgL_iz00_356,
													BBOOL(BgL_arg1314z00_359));
											}
											{
												long BgL_iz00_2890;

												BgL_iz00_2890 = (BgL_iz00_356 + 1L);
												BgL_iz00_356 = BgL_iz00_2890;
												goto BgL_zc3z04anonymousza31311ze3z87_357;
											}
										}
									else
										{	/* Jas/peep.scm 92 */
											CNST_TABLE_REF(2);
										}
									{	/* Jas/peep.scm 134 */
										obj_t BgL_hookz00_335;

										BgL_hookz00_335 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_codez00_17);
										BgL_pz00_399 = BgL_hookz00_335;
										BgL_lz00_400 = BgL_codez00_17;
									BgL_zc3z04anonymousza31343ze3z87_401:
										if (NULLP(BgL_lz00_400))
											{	/* Jas/peep.scm 127 */
												CNST_TABLE_REF(2);
											}
										else
											{	/* Jas/peep.scm 128 */
												bool_t BgL_test2636z00_2898;

												{	/* Jas/peep.scm 128 */
													obj_t BgL_tmpz00_2899;

													BgL_tmpz00_2899 = CAR(((obj_t) BgL_lz00_400));
													BgL_test2636z00_2898 = SYMBOLP(BgL_tmpz00_2899);
												}
												if (BgL_test2636z00_2898)
													{	/* Jas/peep.scm 128 */
														obj_t BgL_arg1348z00_405;

														BgL_arg1348z00_405 = CDR(((obj_t) BgL_lz00_400));
														{
															obj_t BgL_lz00_2906;
															obj_t BgL_pz00_2905;

															BgL_pz00_2905 = BgL_lz00_400;
															BgL_lz00_2906 = BgL_arg1348z00_405;
															BgL_lz00_400 = BgL_lz00_2906;
															BgL_pz00_399 = BgL_pz00_2905;
															goto BgL_zc3z04anonymousza31343ze3z87_401;
														}
													}
												else
													{	/* Jas/peep.scm 129 */
														bool_t BgL_test2637z00_2907;

														BgL_pz00_388 = BgL_pz00_399;
														BgL_lz00_389 = BgL_lz00_400;
														{	/* Jas/peep.scm 113 */
															obj_t BgL_insz00_391;

															BgL_insz00_391 = CAR(((obj_t) BgL_lz00_389));
															if (BGl_storezf3zf3zzjas_peepz00(BgL_insz00_391))
																{	/* Jas/peep.scm 115 */
																	obj_t BgL_vz00_393;

																	{	/* Jas/peep.scm 115 */
																		obj_t BgL_pairz00_1857;

																		BgL_pairz00_1857 =
																			CDR(((obj_t) BgL_insz00_391));
																		BgL_vz00_393 = CAR(BgL_pairz00_1857);
																	}
																	{	/* Jas/peep.scm 116 */
																		bool_t BgL_test2639z00_2915;

																		{	/* Jas/peep.scm 116 */
																			long BgL_kz00_1859;

																			BgL_kz00_1859 = (long) CINT(BgL_vz00_393);
																			BgL_test2639z00_2915 =
																				CBOOL(VECTOR_REF(BgL_candidatez00_319,
																					BgL_kz00_1859));
																		}
																		if (BgL_test2639z00_2915)
																			{	/* Jas/peep.scm 117 */
																				obj_t BgL_ppz00_395;

																				{	/* Jas/peep.scm 117 */
																					obj_t BgL_arg1342z00_398;

																					BgL_arg1342z00_398 =
																						CDR(((obj_t) BgL_lz00_389));
																					BgL_vz00_364 = BgL_vz00_393;
																					BgL_pz00_365 = BgL_lz00_389;
																					BgL_lz00_366 = BgL_arg1342z00_398;
																					BgL_spz00_367 = 0L;
																				BgL_zc3z04anonymousza31318ze3z87_368:
																					{	/* Jas/peep.scm 100 */
																						bool_t BgL_test2640z00_2921;

																						if (NULLP(BgL_lz00_366))
																							{	/* Jas/peep.scm 100 */
																								BgL_test2640z00_2921 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Jas/peep.scm 100 */
																								obj_t BgL_tmpz00_2924;

																								BgL_tmpz00_2924 =
																									CAR(((obj_t) BgL_lz00_366));
																								BgL_test2640z00_2921 =
																									SYMBOLP(BgL_tmpz00_2924);
																							}
																						if (BgL_test2640z00_2921)
																							{	/* Jas/peep.scm 100 */
																								BgL_ppz00_395 = BFALSE;
																							}
																						else
																							{	/* Jas/peep.scm 102 */
																								obj_t BgL_insz00_372;

																								BgL_insz00_372 =
																									CAR(((obj_t) BgL_lz00_366));
																								{	/* Jas/peep.scm 103 */
																									bool_t BgL_test2642z00_2930;

																									if (BGl_loadzf3zf3zzjas_peepz00(BgL_insz00_372))
																										{	/* Jas/peep.scm 103 */
																											long BgL_tmpz00_2933;

																											{	/* Jas/peep.scm 103 */
																												obj_t BgL_pairz00_1843;

																												BgL_pairz00_1843 =
																													CDR(
																													((obj_t)
																														BgL_insz00_372));
																												BgL_tmpz00_2933 =
																													(long)
																													CINT(CAR
																													(BgL_pairz00_1843));
																											}
																											BgL_test2642z00_2930 =
																												(BgL_tmpz00_2933 ==
																												(long)
																												CINT(BgL_vz00_364));
																										}
																									else
																										{	/* Jas/peep.scm 103 */
																											BgL_test2642z00_2930 =
																												((bool_t) 0);
																										}
																									if (BgL_test2642z00_2930)
																										{	/* Jas/peep.scm 103 */
																											if ((BgL_spz00_367 == 0L))
																												{	/* Jas/peep.scm 104 */
																													BgL_ppz00_395 =
																														BgL_pz00_365;
																												}
																											else
																												{	/* Jas/peep.scm 104 */
																													BgL_ppz00_395 =
																														BFALSE;
																												}
																										}
																									else
																										{	/* Jas/peep.scm 105 */
																											long BgL_downz00_377;

																											{	/* Jas/peep.scm 105 */
																												obj_t
																													BgL_arg1331z00_383;
																												BgL_arg1331z00_383 =
																													BGl_nbpopz00zzjas_stackz00
																													(BgL_classfilez00_14,
																													BgL_insz00_372);
																												BgL_downz00_377 =
																													(BgL_spz00_367 -
																													(long)
																													CINT
																													(BgL_arg1331z00_383));
																											}
																											{	/* Jas/peep.scm 105 */
																												long BgL_upz00_378;

																												{	/* Jas/peep.scm 106 */
																													obj_t
																														BgL_arg1329z00_382;
																													BgL_arg1329z00_382 =
																														BGl_nbpushz00zzjas_stackz00
																														(BgL_classfilez00_14,
																														BgL_insz00_372);
																													BgL_upz00_378 =
																														(BgL_downz00_377 +
																														(long)
																														CINT
																														(BgL_arg1329z00_382));
																												}
																												{	/* Jas/peep.scm 106 */

																													if (
																														(BgL_downz00_377 <
																															0L))
																														{	/* Jas/peep.scm 107 */
																															BgL_ppz00_395 =
																																BFALSE;
																														}
																													else
																														{	/* Jas/peep.scm 107 */
																															if (CBOOL
																																(BGl_branchzf3zf3zzjas_peepz00
																																	(BgL_insz00_372)))
																																{	/* Jas/peep.scm 109 */
																																	BgL_ppz00_395
																																		= BFALSE;
																																}
																															else
																																{	/* Jas/peep.scm 111 */
																																	obj_t
																																		BgL_arg1328z00_381;
																																	BgL_arg1328z00_381
																																		=
																																		CDR(((obj_t)
																																			BgL_lz00_366));
																																	{
																																		long
																																			BgL_spz00_2957;
																																		obj_t
																																			BgL_lz00_2956;
																																		obj_t
																																			BgL_pz00_2955;
																																		BgL_pz00_2955
																																			=
																																			BgL_lz00_366;
																																		BgL_lz00_2956
																																			=
																																			BgL_arg1328z00_381;
																																		BgL_spz00_2957
																																			=
																																			BgL_upz00_378;
																																		BgL_spz00_367
																																			=
																																			BgL_spz00_2957;
																																		BgL_lz00_366
																																			=
																																			BgL_lz00_2956;
																																		BgL_pz00_365
																																			=
																																			BgL_pz00_2955;
																																		goto
																																			BgL_zc3z04anonymousza31318ze3z87_368;
																																	}
																																}
																														}
																												}
																											}
																										}
																								}
																							}
																					}
																				}
																				if (CBOOL(BgL_ppz00_395))
																					{	/* Jas/peep.scm 118 */
																						{	/* Jas/peep.scm 120 */
																							obj_t BgL_arg1339z00_396;

																							{	/* Jas/peep.scm 120 */
																								obj_t BgL_pairz00_1864;

																								BgL_pairz00_1864 =
																									CDR(((obj_t) BgL_ppz00_395));
																								BgL_arg1339z00_396 =
																									CDR(BgL_pairz00_1864);
																							}
																							{	/* Jas/peep.scm 120 */
																								obj_t BgL_tmpz00_2963;

																								BgL_tmpz00_2963 =
																									((obj_t) BgL_ppz00_395);
																								SET_CDR(BgL_tmpz00_2963,
																									BgL_arg1339z00_396);
																							}
																						}
																						{	/* Jas/peep.scm 121 */
																							obj_t BgL_arg1340z00_397;

																							BgL_arg1340z00_397 =
																								CDR(((obj_t) BgL_lz00_389));
																							{	/* Jas/peep.scm 121 */
																								obj_t BgL_tmpz00_2968;

																								BgL_tmpz00_2968 =
																									((obj_t) BgL_pz00_388);
																								SET_CDR(BgL_tmpz00_2968,
																									BgL_arg1340z00_397);
																							}
																						}
																						BgL_test2637z00_2907 = ((bool_t) 1);
																					}
																				else
																					{	/* Jas/peep.scm 118 */
																						BgL_test2637z00_2907 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Jas/peep.scm 116 */
																				BgL_test2637z00_2907 = ((bool_t) 0);
																			}
																	}
																}
															else
																{	/* Jas/peep.scm 114 */
																	BgL_test2637z00_2907 = ((bool_t) 0);
																}
														}
														if (BgL_test2637z00_2907)
															{	/* Jas/peep.scm 129 */
																obj_t BgL_arg1351z00_407;

																BgL_arg1351z00_407 =
																	CDR(((obj_t) BgL_pz00_399));
																{
																	obj_t BgL_lz00_2973;

																	BgL_lz00_2973 = BgL_arg1351z00_407;
																	BgL_lz00_400 = BgL_lz00_2973;
																	goto BgL_zc3z04anonymousza31343ze3z87_401;
																}
															}
														else
															{	/* Jas/peep.scm 130 */
																obj_t BgL_arg1352z00_408;

																BgL_arg1352z00_408 =
																	CDR(((obj_t) BgL_lz00_400));
																{
																	obj_t BgL_lz00_2977;
																	obj_t BgL_pz00_2976;

																	BgL_pz00_2976 = BgL_lz00_400;
																	BgL_lz00_2977 = BgL_arg1352z00_408;
																	BgL_lz00_400 = BgL_lz00_2977;
																	BgL_pz00_399 = BgL_pz00_2976;
																	goto BgL_zc3z04anonymousza31343ze3z87_401;
																}
															}
													}
											}
										return CDR(BgL_hookz00_335);
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* packlocals */
	long BGl_packlocalsz00zzjas_peepz00(obj_t BgL_classfilez00_18,
		long BgL_nparamz00_19, long BgL_nlocalsz00_20, obj_t BgL_codez00_21)
	{
		{	/* Jas/peep.scm 141 */
			{	/* Jas/peep.scm 142 */
				long BgL_nz00_417;

				BgL_nz00_417 = (BgL_nparamz00_19 + BgL_nlocalsz00_20);
				{	/* Jas/peep.scm 142 */
					obj_t BgL_accessz00_418;

					BgL_accessz00_418 = make_vector(BgL_nz00_417, BFALSE);
					{	/* Jas/peep.scm 143 */
						obj_t BgL_mappingz00_419;

						BgL_mappingz00_419 = make_vector(BgL_nz00_417, BINT(0L));
						{	/* Jas/peep.scm 144 */

							{
								obj_t BgL_lz00_481;
								obj_t BgL_insz00_461;
								long BgL_freez00_453;
								long BgL_iz00_454;
								obj_t BgL_insz00_438;
								long BgL_iz00_434;

								BgL_iz00_434 = 0L;
							BgL_zc3z04anonymousza31368ze3z87_435:
								if ((BgL_iz00_434 < BgL_nparamz00_19))
									{	/* Jas/peep.scm 146 */
										VECTOR_SET(BgL_accessz00_418, BgL_iz00_434, BTRUE);
										{
											long BgL_iz00_2986;

											BgL_iz00_2986 = (BgL_iz00_434 + 1L);
											BgL_iz00_434 = BgL_iz00_2986;
											goto BgL_zc3z04anonymousza31368ze3z87_435;
										}
									}
								else
									{	/* Jas/peep.scm 146 */
										CNST_TABLE_REF(2);
									}
								{
									obj_t BgL_l1207z00_426;

									BgL_l1207z00_426 = BgL_codez00_21;
								BgL_zc3z04anonymousza31362ze3z87_427:
									if (PAIRP(BgL_l1207z00_426))
										{	/* Jas/peep.scm 181 */
											{	/* Jas/peep.scm 181 */
												obj_t BgL_xz00_429;

												BgL_xz00_429 = CAR(BgL_l1207z00_426);
												if (PAIRP(BgL_xz00_429))
													{	/* Jas/peep.scm 181 */
														BgL_insz00_438 = BgL_xz00_429;
														{	/* Jas/peep.scm 150 */
															bool_t BgL_test2651z00_2994;

															if (BGl_loadzf3zf3zzjas_peepz00(BgL_insz00_438))
																{	/* Jas/peep.scm 150 */
																	BgL_test2651z00_2994 = ((bool_t) 1);
																}
															else
																{	/* Jas/peep.scm 150 */
																	if (BGl_storezf3zf3zzjas_peepz00
																		(BgL_insz00_438))
																		{	/* Jas/peep.scm 150 */
																			BgL_test2651z00_2994 = ((bool_t) 1);
																		}
																	else
																		{	/* Jas/peep.scm 150 */
																			if (
																				((long) CINT(CAR(BgL_insz00_438)) ==
																					169L))
																				{	/* Jas/peep.scm 150 */
																					BgL_test2651z00_2994 = ((bool_t) 1);
																				}
																			else
																				{	/* Jas/peep.scm 150 */
																					BgL_test2651z00_2994 =
																						(
																						(long) CINT(CAR(BgL_insz00_438)) ==
																						132L);
																}}}
															if (BgL_test2651z00_2994)
																{	/* Jas/peep.scm 150 */
																	{	/* Jas/peep.scm 152 */
																		obj_t BgL_arg1408z00_444;

																		BgL_arg1408z00_444 =
																			CAR(CDR(BgL_insz00_438));
																		VECTOR_SET(BgL_accessz00_418,
																			(long) CINT(BgL_arg1408z00_444), BTRUE);
																	}
																	{	/* Jas/peep.scm 153 */
																		bool_t BgL_test2655z00_3010;

																		if (BGl_load2zf3zf3zzjas_peepz00
																			(BgL_insz00_438))
																			{	/* Jas/peep.scm 153 */
																				BgL_test2655z00_3010 = ((bool_t) 1);
																			}
																		else
																			{	/* Jas/peep.scm 153 */
																				BgL_test2655z00_3010 =
																					BGl_store2zf3zf3zzjas_peepz00
																					(BgL_insz00_438);
																			}
																		if (BgL_test2655z00_3010)
																			{	/* Jas/peep.scm 154 */
																				long BgL_arg1421z00_447;

																				{	/* Jas/peep.scm 154 */
																					long BgL_tmpz00_3014;

																					{	/* Jas/peep.scm 154 */
																						obj_t BgL_pairz00_1897;

																						BgL_pairz00_1897 =
																							CDR(BgL_insz00_438);
																						BgL_tmpz00_3014 =
																							(long)
																							CINT(CAR(BgL_pairz00_1897));
																					}
																					BgL_arg1421z00_447 =
																						(BgL_tmpz00_3014 + 1L);
																				}
																				VECTOR_SET(BgL_accessz00_418,
																					BgL_arg1421z00_447, BTRUE);
																			}
																		else
																			{	/* Jas/peep.scm 153 */
																				BFALSE;
																			}
																	}
																}
															else
																{	/* Jas/peep.scm 150 */
																	BFALSE;
																}
														}
													}
												else
													{	/* Jas/peep.scm 181 */
														BFALSE;
													}
											}
											{
												obj_t BgL_l1207z00_3020;

												BgL_l1207z00_3020 = CDR(BgL_l1207z00_426);
												BgL_l1207z00_426 = BgL_l1207z00_3020;
												goto BgL_zc3z04anonymousza31362ze3z87_427;
											}
										}
									else
										{	/* Jas/peep.scm 181 */
											((bool_t) 1);
										}
								}
								{	/* Jas/peep.scm 182 */
									long BgL_reallyzd2usedzd2_433;

									BgL_freez00_453 = 0L;
									BgL_iz00_454 = 0L;
								BgL_zc3z04anonymousza31423ze3z87_455:
									if ((BgL_iz00_454 < BgL_nz00_417))
										{	/* Jas/peep.scm 156 */
											if (CBOOL(VECTOR_REF(BgL_accessz00_418, BgL_iz00_454)))
												{	/* Jas/peep.scm 157 */
													VECTOR_SET(BgL_mappingz00_419, BgL_iz00_454,
														BINT(BgL_freez00_453));
													{
														long BgL_iz00_3031;
														long BgL_freez00_3029;

														BgL_freez00_3029 = (BgL_freez00_453 + 1L);
														BgL_iz00_3031 = (BgL_iz00_454 + 1L);
														BgL_iz00_454 = BgL_iz00_3031;
														BgL_freez00_453 = BgL_freez00_3029;
														goto BgL_zc3z04anonymousza31423ze3z87_455;
													}
												}
											else
												{	/* Jas/peep.scm 157 */
													VECTOR_SET(BgL_mappingz00_419, BgL_iz00_454,
														BINT(-1L));
													{
														long BgL_iz00_3035;

														BgL_iz00_3035 = (BgL_iz00_454 + 1L);
														BgL_iz00_454 = BgL_iz00_3035;
														goto BgL_zc3z04anonymousza31423ze3z87_455;
													}
												}
										}
									else
										{	/* Jas/peep.scm 156 */
											BgL_reallyzd2usedzd2_433 = BgL_freez00_453;
										}
									BGl_za2za2lastzd2numberzd2ofzd2localsza2za2zd2zzjas_peepz00 =
										BINT(BgL_reallyzd2usedzd2_433);
									BgL_lz00_481 = BgL_codez00_21;
								BgL_zc3z04anonymousza31514ze3z87_482:
									if (NULLP(BgL_lz00_481))
										{	/* Jas/peep.scm 176 */
											CNST_TABLE_REF(2);
										}
									else
										{	/* Jas/peep.scm 177 */
											bool_t BgL_test2660z00_3041;

											{	/* Jas/peep.scm 177 */
												obj_t BgL_tmpz00_3042;

												BgL_tmpz00_3042 = CAR(((obj_t) BgL_lz00_481));
												BgL_test2660z00_3041 = SYMBOLP(BgL_tmpz00_3042);
											}
											if (BgL_test2660z00_3041)
												{	/* Jas/peep.scm 177 */
													obj_t BgL_arg1535z00_486;

													BgL_arg1535z00_486 = CDR(((obj_t) BgL_lz00_481));
													{
														obj_t BgL_lz00_3048;

														BgL_lz00_3048 = BgL_arg1535z00_486;
														BgL_lz00_481 = BgL_lz00_3048;
														goto BgL_zc3z04anonymousza31514ze3z87_482;
													}
												}
											else
												{	/* Jas/peep.scm 177 */
													{	/* Jas/peep.scm 178 */
														obj_t BgL_arg1540z00_487;

														BgL_arg1540z00_487 = CAR(((obj_t) BgL_lz00_481));
														BgL_insz00_461 = BgL_arg1540z00_487;
														{	/* Jas/peep.scm 167 */
															bool_t BgL_test2661z00_3051;

															if (BGl_loadzf3zf3zzjas_peepz00(BgL_insz00_461))
																{	/* Jas/peep.scm 167 */
																	BgL_test2661z00_3051 = ((bool_t) 1);
																}
															else
																{	/* Jas/peep.scm 167 */
																	if (BGl_storezf3zf3zzjas_peepz00
																		(BgL_insz00_461))
																		{	/* Jas/peep.scm 167 */
																			BgL_test2661z00_3051 = ((bool_t) 1);
																		}
																	else
																		{	/* Jas/peep.scm 167 */
																			if (
																				((long) CINT(CAR(
																							((obj_t) BgL_insz00_461))) ==
																					169L))
																				{	/* Jas/peep.scm 167 */
																					BgL_test2661z00_3051 = ((bool_t) 1);
																				}
																			else
																				{	/* Jas/peep.scm 167 */
																					BgL_test2661z00_3051 =
																						(
																						(long) CINT(CAR(
																								((obj_t) BgL_insz00_461))) ==
																						132L);
																}}}
															if (BgL_test2661z00_3051)
																{	/* Jas/peep.scm 168 */
																	obj_t BgL_arg1472z00_467;
																	long BgL_arg1473z00_468;

																	BgL_arg1472z00_467 =
																		CDR(((obj_t) BgL_insz00_461));
																	{	/* Jas/peep.scm 168 */
																		obj_t BgL_arg1485z00_469;

																		{	/* Jas/peep.scm 168 */
																			obj_t BgL_pairz00_1922;

																			BgL_pairz00_1922 =
																				CDR(((obj_t) BgL_insz00_461));
																			BgL_arg1485z00_469 =
																				CAR(BgL_pairz00_1922);
																		}
																		{	/* Jas/peep.scm 168 */
																			long BgL_kz00_1924;

																			BgL_kz00_1924 =
																				(long) CINT(BgL_arg1485z00_469);
																			BgL_arg1473z00_468 =
																				(long)
																				CINT(VECTOR_REF(BgL_mappingz00_419,
																					BgL_kz00_1924));
																	}}
																	{	/* Jas/peep.scm 168 */
																		obj_t BgL_auxz00_3075;
																		obj_t BgL_tmpz00_3073;

																		BgL_auxz00_3075 = BINT(BgL_arg1473z00_468);
																		BgL_tmpz00_3073 =
																			((obj_t) BgL_arg1472z00_467);
																		SET_CAR(BgL_tmpz00_3073, BgL_auxz00_3075);
																}}
															else
																{	/* Jas/peep.scm 167 */
																	if (
																		(CAR(
																				((obj_t) BgL_insz00_461)) ==
																			BINT(205L)))
																		{	/* Jas/peep.scm 170 */
																			obj_t BgL_adrz00_472;

																			{	/* Jas/peep.scm 170 */
																				obj_t BgL_pairz00_1935;

																				{	/* Jas/peep.scm 170 */
																					obj_t BgL_pairz00_1934;

																					{	/* Jas/peep.scm 170 */
																						obj_t BgL_pairz00_1933;

																						{	/* Jas/peep.scm 170 */
																							obj_t BgL_pairz00_1932;

																							BgL_pairz00_1932 =
																								CDR(((obj_t) BgL_insz00_461));
																							BgL_pairz00_1933 =
																								CDR(BgL_pairz00_1932);
																						}
																						BgL_pairz00_1934 =
																							CDR(BgL_pairz00_1933);
																					}
																					BgL_pairz00_1935 =
																						CDR(BgL_pairz00_1934);
																				}
																				BgL_adrz00_472 = CDR(BgL_pairz00_1935);
																			}
																			{	/* Jas/peep.scm 171 */
																				obj_t BgL_ivarz00_473;

																				BgL_ivarz00_473 =
																					CAR(((obj_t) BgL_adrz00_472));
																				{	/* Jas/peep.scm 172 */
																					bool_t BgL_test2666z00_3091;

																					{	/* Jas/peep.scm 172 */
																						long BgL_kz00_1938;

																						BgL_kz00_1938 =
																							(long) CINT(BgL_ivarz00_473);
																						BgL_test2666z00_3091 =
																							CBOOL(VECTOR_REF
																							(BgL_accessz00_418,
																								BgL_kz00_1938));
																					}
																					if (BgL_test2666z00_3091)
																						{	/* Jas/peep.scm 173 */
																							long BgL_arg1502z00_475;

																							{	/* Jas/peep.scm 173 */
																								long BgL_kz00_1940;

																								BgL_kz00_1940 =
																									(long) CINT(BgL_ivarz00_473);
																								BgL_arg1502z00_475 =
																									(long)
																									CINT(VECTOR_REF
																									(BgL_mappingz00_419,
																										BgL_kz00_1940));
																							}
																							{	/* Jas/peep.scm 173 */
																								obj_t BgL_auxz00_3100;
																								obj_t BgL_tmpz00_3098;

																								BgL_auxz00_3100 =
																									BINT(BgL_arg1502z00_475);
																								BgL_tmpz00_3098 =
																									((obj_t) BgL_adrz00_472);
																								SET_CAR(BgL_tmpz00_3098,
																									BgL_auxz00_3100);
																						}}
																					else
																						{	/* Jas/peep.scm 174 */
																							obj_t BgL_auxz00_3105;
																							obj_t BgL_tmpz00_3103;

																							BgL_auxz00_3105 = BINT(204L);
																							BgL_tmpz00_3103 =
																								((obj_t) BgL_insz00_461);
																							SET_CAR(BgL_tmpz00_3103,
																								BgL_auxz00_3105);
																						}
																				}
																			}
																		}
																	else
																		{	/* Jas/peep.scm 169 */
																			BFALSE;
																		}
																}
														}
													}
													{	/* Jas/peep.scm 179 */
														obj_t BgL_arg1544z00_488;

														BgL_arg1544z00_488 = CDR(((obj_t) BgL_lz00_481));
														{
															obj_t BgL_lz00_3110;

															BgL_lz00_3110 = BgL_arg1544z00_488;
															BgL_lz00_481 = BgL_lz00_3110;
															goto BgL_zc3z04anonymousza31514ze3z87_482;
														}
													}
												}
										}
									return BgL_reallyzd2usedzd2_433;
								}
							}
						}
					}
				}
			}
		}

	}



/* simplematch */
	obj_t BGl_simplematchz00zzjas_peepz00(obj_t BgL_codez00_22)
	{
		{	/* Jas/peep.scm 190 */
			{
				obj_t BgL_lz00_496;

				BgL_lz00_496 = BgL_codez00_22;
			BgL_zc3z04anonymousza31547ze3z87_497:
				{
					obj_t BgL_labz00_503;
					obj_t BgL_truez00_499;
					obj_t BgL_retestz00_500;
					obj_t BgL_labz00_501;

					if (NULLP(BgL_lz00_496))
						{	/* Jas/peep.scm 222 */
							return CNST_TABLE_REF(2);
						}
					else
						{	/* Jas/peep.scm 222 */
							if (PAIRP(BgL_lz00_496))
								{	/* Jas/peep.scm 222 */
									obj_t BgL_carzd2122zd2_518;
									obj_t BgL_cdrzd2123zd2_519;

									BgL_carzd2122zd2_518 = CAR(((obj_t) BgL_lz00_496));
									BgL_cdrzd2123zd2_519 = CDR(((obj_t) BgL_lz00_496));
									if (PAIRP(BgL_carzd2122zd2_518))
										{	/* Jas/peep.scm 222 */
											obj_t BgL_cdrzd2126zd2_521;

											BgL_cdrzd2126zd2_521 = CDR(BgL_carzd2122zd2_518);
											if ((CAR(BgL_carzd2122zd2_518) == BINT(153L)))
												{	/* Jas/peep.scm 222 */
													if (PAIRP(BgL_cdrzd2126zd2_521))
														{	/* Jas/peep.scm 222 */
															obj_t BgL_truez00_525;

															BgL_truez00_525 = CAR(BgL_cdrzd2126zd2_521);
															if (NULLP(CDR(BgL_cdrzd2126zd2_521)))
																{	/* Jas/peep.scm 222 */
																	if (PAIRP(BgL_cdrzd2123zd2_519))
																		{	/* Jas/peep.scm 222 */
																			obj_t BgL_carzd2134zd2_529;
																			obj_t BgL_cdrzd2135zd2_530;

																			BgL_carzd2134zd2_529 =
																				CAR(BgL_cdrzd2123zd2_519);
																			BgL_cdrzd2135zd2_530 =
																				CDR(BgL_cdrzd2123zd2_519);
																			if (PAIRP(BgL_carzd2134zd2_529))
																				{	/* Jas/peep.scm 222 */
																					if (
																						(CAR(BgL_carzd2134zd2_529) ==
																							BINT(3L)))
																						{	/* Jas/peep.scm 222 */
																							if (NULLP(CDR
																									(BgL_carzd2134zd2_529)))
																								{	/* Jas/peep.scm 222 */
																									if (PAIRP
																										(BgL_cdrzd2135zd2_530))
																										{	/* Jas/peep.scm 222 */
																											obj_t
																												BgL_carzd2141zd2_537;
																											obj_t
																												BgL_cdrzd2142zd2_538;
																											BgL_carzd2141zd2_537 =
																												CAR
																												(BgL_cdrzd2135zd2_530);
																											BgL_cdrzd2142zd2_538 =
																												CDR
																												(BgL_cdrzd2135zd2_530);
																											if (PAIRP
																												(BgL_carzd2141zd2_537))
																												{	/* Jas/peep.scm 222 */
																													obj_t
																														BgL_cdrzd2145zd2_540;
																													BgL_cdrzd2145zd2_540 =
																														CDR
																														(BgL_carzd2141zd2_537);
																													if ((CAR
																															(BgL_carzd2141zd2_537)
																															== BINT(167L)))
																														{	/* Jas/peep.scm 222 */
																															if (PAIRP
																																(BgL_cdrzd2145zd2_540))
																																{	/* Jas/peep.scm 222 */
																																	obj_t
																																		BgL_retestz00_544;
																																	BgL_retestz00_544
																																		=
																																		CAR
																																		(BgL_cdrzd2145zd2_540);
																																	if (NULLP(CDR
																																			(BgL_cdrzd2145zd2_540)))
																																		{	/* Jas/peep.scm 222 */
																																			if (PAIRP
																																				(BgL_cdrzd2142zd2_538))
																																				{	/* Jas/peep.scm 222 */
																																					obj_t
																																						BgL_cdrzd2154zd2_548;
																																					BgL_cdrzd2154zd2_548
																																						=
																																						CDR
																																						(BgL_cdrzd2142zd2_538);
																																					if (
																																						(BgL_truez00_525
																																							==
																																							CAR
																																							(BgL_cdrzd2142zd2_538)))
																																						{	/* Jas/peep.scm 222 */
																																							if (PAIRP(BgL_cdrzd2154zd2_548))
																																								{	/* Jas/peep.scm 222 */
																																									obj_t
																																										BgL_carzd2158zd2_552;
																																									obj_t
																																										BgL_cdrzd2159zd2_553;
																																									BgL_carzd2158zd2_552
																																										=
																																										CAR
																																										(BgL_cdrzd2154zd2_548);
																																									BgL_cdrzd2159zd2_553
																																										=
																																										CDR
																																										(BgL_cdrzd2154zd2_548);
																																									if (PAIRP(BgL_carzd2158zd2_552))
																																										{	/* Jas/peep.scm 222 */
																																											if ((CAR(BgL_carzd2158zd2_552) == BINT(4L)))
																																												{	/* Jas/peep.scm 222 */
																																													if (NULLP(CDR(BgL_carzd2158zd2_552)))
																																														{	/* Jas/peep.scm 222 */
																																															if (PAIRP(BgL_cdrzd2159zd2_553))
																																																{	/* Jas/peep.scm 222 */
																																																	obj_t
																																																		BgL_cdrzd2165zd2_560;
																																																	BgL_cdrzd2165zd2_560
																																																		=
																																																		CDR
																																																		(BgL_cdrzd2159zd2_553);
																																																	if ((BgL_retestz00_544 == CAR(BgL_cdrzd2159zd2_553)))
																																																		{	/* Jas/peep.scm 222 */
																																																			if (PAIRP(BgL_cdrzd2165zd2_560))
																																																				{	/* Jas/peep.scm 222 */
																																																					obj_t
																																																						BgL_carzd2168zd2_564;
																																																					BgL_carzd2168zd2_564
																																																						=
																																																						CAR
																																																						(BgL_cdrzd2165zd2_560);
																																																					if (PAIRP(BgL_carzd2168zd2_564))
																																																						{	/* Jas/peep.scm 222 */
																																																							obj_t
																																																								BgL_cdrzd2172zd2_566;
																																																							BgL_cdrzd2172zd2_566
																																																								=
																																																								CDR
																																																								(BgL_carzd2168zd2_564);
																																																							if ((CAR(BgL_carzd2168zd2_564) == BINT(153L)))
																																																								{	/* Jas/peep.scm 222 */
																																																									if (PAIRP(BgL_cdrzd2172zd2_566))
																																																										{	/* Jas/peep.scm 222 */
																																																											if (NULLP(CDR(BgL_cdrzd2172zd2_566)))
																																																												{	/* Jas/peep.scm 222 */
																																																													BgL_truez00_499
																																																														=
																																																														BgL_truez00_525;
																																																													BgL_retestz00_500
																																																														=
																																																														BgL_retestz00_544;
																																																													BgL_labz00_501
																																																														=
																																																														CAR
																																																														(BgL_cdrzd2172zd2_566);
																																																													{	/* Jas/peep.scm 196 */
																																																														obj_t
																																																															BgL_pz00_1287;
																																																														obj_t
																																																															BgL_cz00_1288;
																																																														{	/* Jas/peep.scm 196 */
																																																															obj_t
																																																																BgL_pairz00_1955;
																																																															{	/* Jas/peep.scm 196 */
																																																																obj_t
																																																																	BgL_pairz00_1954;
																																																																{	/* Jas/peep.scm 196 */
																																																																	obj_t
																																																																		BgL_pairz00_1953;
																																																																	BgL_pairz00_1953
																																																																		=
																																																																		CDR
																																																																		(
																																																																		((obj_t) BgL_lz00_496));
																																																																	BgL_pairz00_1954
																																																																		=
																																																																		CDR
																																																																		(BgL_pairz00_1953);
																																																																}
																																																																BgL_pairz00_1955
																																																																	=
																																																																	CDR
																																																																	(BgL_pairz00_1954);
																																																															}
																																																															BgL_pz00_1287
																																																																=
																																																																CDR
																																																																(CDR
																																																																(CDR
																																																																	(BgL_pairz00_1955)));
																																																														}
																																																														BgL_cz00_1288
																																																															=
																																																															BGl_gensymz00zz__r4_symbols_6_4z00
																																																															(CNST_TABLE_REF
																																																															(4));
																																																														{	/* Jas/peep.scm 197 */
																																																															obj_t
																																																																BgL_arg2385z00_1289;
																																																															{	/* Jas/peep.scm 197 */
																																																																obj_t
																																																																	BgL_list2386z00_1290;
																																																																{	/* Jas/peep.scm 197 */
																																																																	obj_t
																																																																		BgL_arg2387z00_1291;
																																																																	BgL_arg2387z00_1291
																																																																		=
																																																																		MAKE_YOUNG_PAIR
																																																																		(BgL_labz00_501,
																																																																		BNIL);
																																																																	BgL_list2386z00_1290
																																																																		=
																																																																		MAKE_YOUNG_PAIR
																																																																		(BINT
																																																																		(154L),
																																																																		BgL_arg2387z00_1291);
																																																																}
																																																																BgL_arg2385z00_1289
																																																																	=
																																																																	BgL_list2386z00_1290;
																																																															}
																																																															{	/* Jas/peep.scm 197 */
																																																																obj_t
																																																																	BgL_tmpz00_3215;
																																																																BgL_tmpz00_3215
																																																																	=
																																																																	(
																																																																	(obj_t)
																																																																	BgL_lz00_496);
																																																																SET_CAR
																																																																	(BgL_tmpz00_3215,
																																																																	BgL_arg2385z00_1289);
																																																															}
																																																														}
																																																														{	/* Jas/peep.scm 198 */
																																																															obj_t
																																																																BgL_arg2388z00_1292;
																																																															obj_t
																																																																BgL_arg2389z00_1293;
																																																															BgL_arg2388z00_1292
																																																																=
																																																																CDR
																																																																(
																																																																((obj_t) BgL_lz00_496));
																																																															{	/* Jas/peep.scm 198 */
																																																																obj_t
																																																																	BgL_list2390z00_1294;
																																																																{	/* Jas/peep.scm 198 */
																																																																	obj_t
																																																																		BgL_arg2391z00_1295;
																																																																	BgL_arg2391z00_1295
																																																																		=
																																																																		MAKE_YOUNG_PAIR
																																																																		(BgL_cz00_1288,
																																																																		BNIL);
																																																																	BgL_list2390z00_1294
																																																																		=
																																																																		MAKE_YOUNG_PAIR
																																																																		(BINT
																																																																		(167L),
																																																																		BgL_arg2391z00_1295);
																																																																}
																																																																BgL_arg2389z00_1293
																																																																	=
																																																																	BgL_list2390z00_1294;
																																																															}
																																																															{	/* Jas/peep.scm 198 */
																																																																obj_t
																																																																	BgL_tmpz00_3223;
																																																																BgL_tmpz00_3223
																																																																	=
																																																																	(
																																																																	(obj_t)
																																																																	BgL_arg2388z00_1292);
																																																																SET_CAR
																																																																	(BgL_tmpz00_3223,
																																																																	BgL_arg2389z00_1293);
																																																															}
																																																														}
																																																														{	/* Jas/peep.scm 199 */
																																																															obj_t
																																																																BgL_arg2392z00_1296;
																																																															{	/* Jas/peep.scm 199 */
																																																																obj_t
																																																																	BgL_arg2393z00_1297;
																																																																BgL_arg2393z00_1297
																																																																	=
																																																																	CDR
																																																																	(
																																																																	((obj_t) BgL_pz00_1287));
																																																																BgL_arg2392z00_1296
																																																																	=
																																																																	MAKE_YOUNG_PAIR
																																																																	(BgL_cz00_1288,
																																																																	BgL_arg2393z00_1297);
																																																															}
																																																															{	/* Jas/peep.scm 199 */
																																																																obj_t
																																																																	BgL_tmpz00_3229;
																																																																BgL_tmpz00_3229
																																																																	=
																																																																	(
																																																																	(obj_t)
																																																																	BgL_pz00_1287);
																																																																SET_CDR
																																																																	(BgL_tmpz00_3229,
																																																																	BgL_arg2392z00_1296);
																																																															}
																																																														}
																																																														{
																																																															obj_t
																																																																BgL_lz00_3232;
																																																															BgL_lz00_3232
																																																																=
																																																																BgL_pz00_1287;
																																																															BgL_lz00_496
																																																																=
																																																																BgL_lz00_3232;
																																																															goto
																																																																BgL_zc3z04anonymousza31547ze3z87_497;
																																																														}
																																																													}
																																																												}
																																																											else
																																																												{	/* Jas/peep.scm 222 */
																																																												BgL_tagzd2109zd2_514:
																																																													{	/* Jas/peep.scm 222 */
																																																														obj_t
																																																															BgL_arg2408z00_1309;
																																																														BgL_arg2408z00_1309
																																																															=
																																																															CDR
																																																															(
																																																															((obj_t) BgL_lz00_496));
																																																														{
																																																															obj_t
																																																																BgL_lz00_3236;
																																																															BgL_lz00_3236
																																																																=
																																																																BgL_arg2408z00_1309;
																																																															BgL_lz00_496
																																																																=
																																																																BgL_lz00_3236;
																																																															goto
																																																																BgL_zc3z04anonymousza31547ze3z87_497;
																																																														}
																																																													}
																																																												}
																																																										}
																																																									else
																																																										{	/* Jas/peep.scm 222 */
																																																											goto
																																																												BgL_tagzd2109zd2_514;
																																																										}
																																																								}
																																																							else
																																																								{	/* Jas/peep.scm 222 */
																																																									goto
																																																										BgL_tagzd2109zd2_514;
																																																								}
																																																						}
																																																					else
																																																						{	/* Jas/peep.scm 222 */
																																																							goto
																																																								BgL_tagzd2109zd2_514;
																																																						}
																																																				}
																																																			else
																																																				{	/* Jas/peep.scm 222 */
																																																					goto
																																																						BgL_tagzd2109zd2_514;
																																																				}
																																																		}
																																																	else
																																																		{	/* Jas/peep.scm 222 */
																																																			goto
																																																				BgL_tagzd2109zd2_514;
																																																		}
																																																}
																																															else
																																																{	/* Jas/peep.scm 222 */
																																																	goto
																																																		BgL_tagzd2109zd2_514;
																																																}
																																														}
																																													else
																																														{	/* Jas/peep.scm 222 */
																																															goto
																																																BgL_tagzd2109zd2_514;
																																														}
																																												}
																																											else
																																												{	/* Jas/peep.scm 222 */
																																													goto
																																														BgL_tagzd2109zd2_514;
																																												}
																																										}
																																									else
																																										{	/* Jas/peep.scm 222 */
																																											goto
																																												BgL_tagzd2109zd2_514;
																																										}
																																								}
																																							else
																																								{	/* Jas/peep.scm 222 */
																																									goto
																																										BgL_tagzd2109zd2_514;
																																								}
																																						}
																																					else
																																						{	/* Jas/peep.scm 222 */
																																							goto
																																								BgL_tagzd2109zd2_514;
																																						}
																																				}
																																			else
																																				{	/* Jas/peep.scm 222 */
																																					goto
																																						BgL_tagzd2109zd2_514;
																																				}
																																		}
																																	else
																																		{	/* Jas/peep.scm 222 */
																																			goto
																																				BgL_tagzd2109zd2_514;
																																		}
																																}
																															else
																																{	/* Jas/peep.scm 222 */
																																	goto
																																		BgL_tagzd2109zd2_514;
																																}
																														}
																													else
																														{	/* Jas/peep.scm 222 */
																															goto
																																BgL_tagzd2109zd2_514;
																														}
																												}
																											else
																												{	/* Jas/peep.scm 222 */
																													goto
																														BgL_tagzd2109zd2_514;
																												}
																										}
																									else
																										{	/* Jas/peep.scm 222 */
																											goto BgL_tagzd2109zd2_514;
																										}
																								}
																							else
																								{	/* Jas/peep.scm 222 */
																									goto BgL_tagzd2109zd2_514;
																								}
																						}
																					else
																						{	/* Jas/peep.scm 222 */
																							obj_t BgL_carzd2397zd2_583;
																							obj_t BgL_cdrzd2398zd2_584;

																							BgL_carzd2397zd2_583 =
																								CAR(
																								((obj_t) BgL_cdrzd2123zd2_519));
																							BgL_cdrzd2398zd2_584 =
																								CDR(
																								((obj_t) BgL_cdrzd2123zd2_519));
																							if (
																								(CAR(
																										((obj_t)
																											BgL_carzd2397zd2_583)) ==
																									BINT(4L)))
																								{	/* Jas/peep.scm 222 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_carzd2397zd2_583))))
																										{	/* Jas/peep.scm 222 */
																											if (PAIRP
																												(BgL_cdrzd2398zd2_584))
																												{	/* Jas/peep.scm 222 */
																													obj_t
																														BgL_carzd2402zd2_590;
																													BgL_carzd2402zd2_590 =
																														CAR
																														(BgL_cdrzd2398zd2_584);
																													if (PAIRP
																														(BgL_carzd2402zd2_590))
																														{	/* Jas/peep.scm 222 */
																															obj_t
																																BgL_cdrzd2406zd2_592;
																															BgL_cdrzd2406zd2_592
																																=
																																CDR
																																(BgL_carzd2402zd2_590);
																															if ((CAR
																																	(BgL_carzd2402zd2_590)
																																	==
																																	BINT(153L)))
																																{	/* Jas/peep.scm 222 */
																																	if (PAIRP
																																		(BgL_cdrzd2406zd2_592))
																																		{	/* Jas/peep.scm 222 */
																																			if (NULLP
																																				(CDR
																																					(BgL_cdrzd2406zd2_592)))
																																				{	/* Jas/peep.scm 222 */
																																					{	/* Jas/peep.scm 205 */
																																						obj_t
																																							BgL_tmpz00_3265;
																																						BgL_tmpz00_3265
																																							=
																																							CDR
																																							(CDR
																																							(CDR
																																								(BgL_lz00_496)));
																																						SET_CDR
																																							(BgL_lz00_496,
																																							BgL_tmpz00_3265);
																																					}
																																					{

																																						goto
																																							BgL_zc3z04anonymousza31547ze3z87_497;
																																					}
																																				}
																																			else
																																				{	/* Jas/peep.scm 222 */
																																					goto
																																						BgL_tagzd2109zd2_514;
																																				}
																																		}
																																	else
																																		{	/* Jas/peep.scm 222 */
																																			goto
																																				BgL_tagzd2109zd2_514;
																																		}
																																}
																															else
																																{	/* Jas/peep.scm 222 */
																																	goto
																																		BgL_tagzd2109zd2_514;
																																}
																														}
																													else
																														{	/* Jas/peep.scm 222 */
																															goto
																																BgL_tagzd2109zd2_514;
																														}
																												}
																											else
																												{	/* Jas/peep.scm 222 */
																													goto
																														BgL_tagzd2109zd2_514;
																												}
																										}
																									else
																										{	/* Jas/peep.scm 222 */
																											goto BgL_tagzd2109zd2_514;
																										}
																								}
																							else
																								{	/* Jas/peep.scm 222 */
																									obj_t BgL_cdrzd2441zd2_604;

																									BgL_cdrzd2441zd2_604 =
																										CDR(
																										((obj_t)
																											BgL_cdrzd2123zd2_519));
																									{	/* Jas/peep.scm 222 */
																										bool_t BgL_test2702z00_3272;

																										{	/* Jas/peep.scm 222 */
																											obj_t BgL_tmpz00_3273;

																											{	/* Jas/peep.scm 222 */
																												obj_t BgL_pairz00_2068;

																												BgL_pairz00_2068 =
																													CAR(
																													((obj_t)
																														BgL_cdrzd2123zd2_519));
																												BgL_tmpz00_3273 =
																													CAR(BgL_pairz00_2068);
																											}
																											BgL_test2702z00_3272 =
																												(BgL_tmpz00_3273 ==
																												BINT(178L));
																										}
																										if (BgL_test2702z00_3272)
																											{	/* Jas/peep.scm 222 */
																												if (PAIRP
																													(BgL_cdrzd2441zd2_604))
																													{	/* Jas/peep.scm 222 */
																														obj_t
																															BgL_carzd2444zd2_609;
																														BgL_carzd2444zd2_609
																															=
																															CAR
																															(BgL_cdrzd2441zd2_604);
																														if (PAIRP
																															(BgL_carzd2444zd2_609))
																															{	/* Jas/peep.scm 222 */
																																if (
																																	(CAR
																																		(BgL_carzd2444zd2_609)
																																		==
																																		BINT(87L)))
																																	{	/* Jas/peep.scm 222 */
																																		if (NULLP
																																			(CDR
																																				(BgL_carzd2444zd2_609)))
																																			{	/* Jas/peep.scm 222 */
																																				{	/* Jas/peep.scm 208 */
																																					obj_t
																																						BgL_tmpz00_3291;
																																					BgL_tmpz00_3291
																																						=
																																						CDR
																																						(CDR
																																						(CDR
																																							(BgL_lz00_496)));
																																					SET_CDR
																																						(BgL_lz00_496,
																																						BgL_tmpz00_3291);
																																				}
																																				{

																																					goto
																																						BgL_zc3z04anonymousza31547ze3z87_497;
																																				}
																																			}
																																		else
																																			{	/* Jas/peep.scm 222 */
																																				goto
																																					BgL_tagzd2109zd2_514;
																																			}
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		obj_t
																																			BgL_carzd2459zd2_617;
																																		BgL_carzd2459zd2_617
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd2441zd2_604));
																																		if ((CAR((
																																						(obj_t)
																																						BgL_carzd2459zd2_617))
																																				==
																																				BINT
																																				(88L)))
																																			{	/* Jas/peep.scm 222 */
																																				if (NULLP(CDR(((obj_t) BgL_carzd2459zd2_617))))
																																					{	/* Jas/peep.scm 222 */
																																						{	/* Jas/peep.scm 211 */
																																							obj_t
																																								BgL_tmpz00_3307;
																																							BgL_tmpz00_3307
																																								=
																																								CDR
																																								(CDR
																																								(CDR
																																									(BgL_lz00_496)));
																																							SET_CDR
																																								(BgL_lz00_496,
																																								BgL_tmpz00_3307);
																																						}
																																						{

																																							goto
																																								BgL_zc3z04anonymousza31547ze3z87_497;
																																						}
																																					}
																																				else
																																					{	/* Jas/peep.scm 222 */
																																						goto
																																							BgL_tagzd2109zd2_514;
																																					}
																																			}
																																		else
																																			{	/* Jas/peep.scm 222 */
																																				goto
																																					BgL_tagzd2109zd2_514;
																																			}
																																	}
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																goto
																																	BgL_tagzd2109zd2_514;
																															}
																													}
																												else
																													{	/* Jas/peep.scm 222 */
																														goto
																															BgL_tagzd2109zd2_514;
																													}
																											}
																										else
																											{	/* Jas/peep.scm 222 */
																												obj_t
																													BgL_carzd2469zd2_628;
																												BgL_carzd2469zd2_628 =
																													CAR(((obj_t)
																														BgL_lz00_496));
																												if (PAIRP
																													(BgL_carzd2469zd2_628))
																													{	/* Jas/peep.scm 222 */
																														if (
																															(CAR
																																(BgL_carzd2469zd2_628)
																																== BINT(9L)))
																															{	/* Jas/peep.scm 222 */
																																if (NULLP(CDR
																																		(BgL_carzd2469zd2_628)))
																																	{	/* Jas/peep.scm 222 */
																																		obj_t
																																			BgL_carzd2473zd2_634;
																																		BgL_carzd2473zd2_634
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd2123zd2_519));
																																		if ((CAR((
																																						(obj_t)
																																						BgL_carzd2473zd2_634))
																																				==
																																				BINT
																																				(136L)))
																																			{	/* Jas/peep.scm 222 */
																																				if (NULLP(CDR(((obj_t) BgL_carzd2473zd2_634))))
																																					{	/* Jas/peep.scm 222 */
																																					BgL_tagzd2107zd2_512:
																																						{	/* Jas/peep.scm 215 */
																																							obj_t
																																								BgL_objz00_1995;
																																							BgL_objz00_1995
																																								=
																																								CNST_TABLE_REF
																																								(5);
																																							{	/* Jas/peep.scm 215 */
																																								obj_t
																																									BgL_tmpz00_3335;
																																								BgL_tmpz00_3335
																																									=
																																									(
																																									(obj_t)
																																									BgL_lz00_496);
																																								SET_CAR
																																									(BgL_tmpz00_3335,
																																									BgL_objz00_1995);
																																							}
																																						}
																																						{	/* Jas/peep.scm 216 */
																																							obj_t
																																								BgL_arg2403z00_1305;
																																							{	/* Jas/peep.scm 216 */
																																								obj_t
																																									BgL_pairz00_1999;
																																								BgL_pairz00_1999
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_lz00_496));
																																								BgL_arg2403z00_1305
																																									=
																																									CDR
																																									(BgL_pairz00_1999);
																																							}
																																							{	/* Jas/peep.scm 216 */
																																								obj_t
																																									BgL_tmpz00_3341;
																																								BgL_tmpz00_3341
																																									=
																																									(
																																									(obj_t)
																																									BgL_lz00_496);
																																								SET_CDR
																																									(BgL_tmpz00_3341,
																																									BgL_arg2403z00_1305);
																																							}
																																						}
																																						{	/* Jas/peep.scm 217 */
																																							obj_t
																																								BgL_arg2404z00_1306;
																																							{	/* Jas/peep.scm 217 */
																																								obj_t
																																									BgL_pairz00_2004;
																																								BgL_pairz00_2004
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_lz00_496));
																																								BgL_arg2404z00_1306
																																									=
																																									CDR
																																									(BgL_pairz00_2004);
																																							}
																																							{
																																								obj_t
																																									BgL_lz00_3347;
																																								BgL_lz00_3347
																																									=
																																									BgL_arg2404z00_1306;
																																								BgL_lz00_496
																																									=
																																									BgL_lz00_3347;
																																								goto
																																									BgL_zc3z04anonymousza31547ze3z87_497;
																																							}
																																						}
																																					}
																																				else
																																					{	/* Jas/peep.scm 222 */
																																						goto
																																							BgL_tagzd2109zd2_514;
																																					}
																																			}
																																		else
																																			{	/* Jas/peep.scm 222 */
																																				goto
																																					BgL_tagzd2109zd2_514;
																																			}
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2109zd2_514;
																																	}
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																if (
																																	(CAR(
																																			((obj_t)
																																				BgL_carzd2469zd2_628))
																																		==
																																		BINT(10L)))
																																	{	/* Jas/peep.scm 222 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_carzd2469zd2_628))))
																																			{	/* Jas/peep.scm 222 */
																																				obj_t
																																					BgL_carzd2481zd2_648;
																																				{	/* Jas/peep.scm 222 */
																																					obj_t
																																						BgL_pairz00_2106;
																																					BgL_pairz00_2106
																																						=
																																						CDR(
																																						((obj_t) BgL_lz00_496));
																																					BgL_carzd2481zd2_648
																																						=
																																						CAR
																																						(BgL_pairz00_2106);
																																				}
																																				if (
																																					(CAR(
																																							((obj_t) BgL_carzd2481zd2_648)) == BINT(136L)))
																																					{	/* Jas/peep.scm 222 */
																																						if (NULLP(CDR(((obj_t) BgL_carzd2481zd2_648))))
																																							{	/* Jas/peep.scm 222 */
																																							BgL_tagzd2108zd2_513:
																																								{	/* Jas/peep.scm 219 */
																																									obj_t
																																										BgL_objz00_2006;
																																									BgL_objz00_2006
																																										=
																																										CNST_TABLE_REF
																																										(6);
																																									{	/* Jas/peep.scm 219 */
																																										obj_t
																																											BgL_tmpz00_3370;
																																										BgL_tmpz00_3370
																																											=
																																											(
																																											(obj_t)
																																											BgL_lz00_496);
																																										SET_CAR
																																											(BgL_tmpz00_3370,
																																											BgL_objz00_2006);
																																									}
																																								}
																																								{	/* Jas/peep.scm 220 */
																																									obj_t
																																										BgL_arg2405z00_1307;
																																									{	/* Jas/peep.scm 220 */
																																										obj_t
																																											BgL_pairz00_2010;
																																										BgL_pairz00_2010
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_lz00_496));
																																										BgL_arg2405z00_1307
																																											=
																																											CDR
																																											(BgL_pairz00_2010);
																																									}
																																									{	/* Jas/peep.scm 220 */
																																										obj_t
																																											BgL_tmpz00_3376;
																																										BgL_tmpz00_3376
																																											=
																																											(
																																											(obj_t)
																																											BgL_lz00_496);
																																										SET_CDR
																																											(BgL_tmpz00_3376,
																																											BgL_arg2405z00_1307);
																																									}
																																								}
																																								{	/* Jas/peep.scm 221 */
																																									obj_t
																																										BgL_arg2407z00_1308;
																																									{	/* Jas/peep.scm 221 */
																																										obj_t
																																											BgL_pairz00_2015;
																																										BgL_pairz00_2015
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_lz00_496));
																																										BgL_arg2407z00_1308
																																											=
																																											CDR
																																											(BgL_pairz00_2015);
																																									}
																																									{
																																										obj_t
																																											BgL_lz00_3382;
																																										BgL_lz00_3382
																																											=
																																											BgL_arg2407z00_1308;
																																										BgL_lz00_496
																																											=
																																											BgL_lz00_3382;
																																										goto
																																											BgL_zc3z04anonymousza31547ze3z87_497;
																																									}
																																								}
																																							}
																																						else
																																							{	/* Jas/peep.scm 222 */
																																								goto
																																									BgL_tagzd2109zd2_514;
																																							}
																																					}
																																				else
																																					{	/* Jas/peep.scm 222 */
																																						goto
																																							BgL_tagzd2109zd2_514;
																																					}
																																			}
																																		else
																																			{	/* Jas/peep.scm 222 */
																																				goto
																																					BgL_tagzd2109zd2_514;
																																			}
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2109zd2_514;
																																	}
																															}
																													}
																												else
																													{	/* Jas/peep.scm 222 */
																														goto
																															BgL_tagzd2109zd2_514;
																													}
																											}
																									}
																								}
																						}
																				}
																			else
																				{	/* Jas/peep.scm 222 */
																					goto BgL_tagzd2109zd2_514;
																				}
																		}
																	else
																		{	/* Jas/peep.scm 222 */
																			goto BgL_tagzd2109zd2_514;
																		}
																}
															else
																{	/* Jas/peep.scm 222 */
																	if (PAIRP(BgL_cdrzd2123zd2_519))
																		{	/* Jas/peep.scm 222 */
																			obj_t BgL_carzd2526zd2_681;
																			obj_t BgL_cdrzd2527zd2_682;

																			BgL_carzd2526zd2_681 =
																				CAR(BgL_cdrzd2123zd2_519);
																			BgL_cdrzd2527zd2_682 =
																				CDR(BgL_cdrzd2123zd2_519);
																			if (PAIRP(BgL_carzd2526zd2_681))
																				{	/* Jas/peep.scm 222 */
																					if (
																						(CAR(BgL_carzd2526zd2_681) ==
																							BINT(4L)))
																						{	/* Jas/peep.scm 222 */
																							if (NULLP(CDR
																									(BgL_carzd2526zd2_681)))
																								{	/* Jas/peep.scm 222 */
																									if (PAIRP
																										(BgL_cdrzd2527zd2_682))
																										{	/* Jas/peep.scm 222 */
																											obj_t
																												BgL_carzd2531zd2_689;
																											BgL_carzd2531zd2_689 =
																												CAR
																												(BgL_cdrzd2527zd2_682);
																											if (PAIRP
																												(BgL_carzd2531zd2_689))
																												{	/* Jas/peep.scm 222 */
																													obj_t
																														BgL_cdrzd2535zd2_691;
																													BgL_cdrzd2535zd2_691 =
																														CDR
																														(BgL_carzd2531zd2_689);
																													if ((CAR
																															(BgL_carzd2531zd2_689)
																															== BINT(153L)))
																														{	/* Jas/peep.scm 222 */
																															if (PAIRP
																																(BgL_cdrzd2535zd2_691))
																																{	/* Jas/peep.scm 222 */
																																	if (NULLP(CDR
																																			(BgL_cdrzd2535zd2_691)))
																																		{	/* Jas/peep.scm 222 */
																																			{	/* Jas/peep.scm 205 */
																																				obj_t
																																					BgL_tmpz00_3411;
																																				BgL_tmpz00_3411
																																					=
																																					CDR
																																					(CDR
																																					(CDR
																																						(BgL_lz00_496)));
																																				SET_CDR
																																					(BgL_lz00_496,
																																					BgL_tmpz00_3411);
																																			}
																																			{

																																				goto
																																					BgL_zc3z04anonymousza31547ze3z87_497;
																																			}
																																		}
																																	else
																																		{	/* Jas/peep.scm 222 */
																																			goto
																																				BgL_tagzd2109zd2_514;
																																		}
																																}
																															else
																																{	/* Jas/peep.scm 222 */
																																	goto
																																		BgL_tagzd2109zd2_514;
																																}
																														}
																													else
																														{	/* Jas/peep.scm 222 */
																															goto
																																BgL_tagzd2109zd2_514;
																														}
																												}
																											else
																												{	/* Jas/peep.scm 222 */
																													goto
																														BgL_tagzd2109zd2_514;
																												}
																										}
																									else
																										{	/* Jas/peep.scm 222 */
																											goto BgL_tagzd2109zd2_514;
																										}
																								}
																							else
																								{	/* Jas/peep.scm 222 */
																									goto BgL_tagzd2109zd2_514;
																								}
																						}
																					else
																						{	/* Jas/peep.scm 222 */
																							obj_t BgL_cdrzd2570zd2_703;

																							BgL_cdrzd2570zd2_703 =
																								CDR(
																								((obj_t) BgL_cdrzd2123zd2_519));
																							{	/* Jas/peep.scm 222 */
																								bool_t BgL_test2727z00_3418;

																								{	/* Jas/peep.scm 222 */
																									obj_t BgL_tmpz00_3419;

																									{	/* Jas/peep.scm 222 */
																										obj_t BgL_pairz00_2138;

																										BgL_pairz00_2138 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd2123zd2_519));
																										BgL_tmpz00_3419 =
																											CAR(BgL_pairz00_2138);
																									}
																									BgL_test2727z00_3418 =
																										(BgL_tmpz00_3419 ==
																										BINT(178L));
																								}
																								if (BgL_test2727z00_3418)
																									{	/* Jas/peep.scm 222 */
																										if (PAIRP
																											(BgL_cdrzd2570zd2_703))
																											{	/* Jas/peep.scm 222 */
																												obj_t
																													BgL_carzd2573zd2_708;
																												BgL_carzd2573zd2_708 =
																													CAR
																													(BgL_cdrzd2570zd2_703);
																												if (PAIRP
																													(BgL_carzd2573zd2_708))
																													{	/* Jas/peep.scm 222 */
																														if (
																															(CAR
																																(BgL_carzd2573zd2_708)
																																== BINT(87L)))
																															{	/* Jas/peep.scm 222 */
																																if (NULLP(CDR
																																		(BgL_carzd2573zd2_708)))
																																	{	/* Jas/peep.scm 222 */
																																		{	/* Jas/peep.scm 208 */
																																			obj_t
																																				BgL_tmpz00_3437;
																																			BgL_tmpz00_3437
																																				=
																																				CDR(CDR
																																				(CDR
																																					(BgL_lz00_496)));
																																			SET_CDR
																																				(BgL_lz00_496,
																																				BgL_tmpz00_3437);
																																		}
																																		{

																																			goto
																																				BgL_zc3z04anonymousza31547ze3z87_497;
																																		}
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2109zd2_514;
																																	}
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																obj_t
																																	BgL_carzd2588zd2_716;
																																BgL_carzd2588zd2_716
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd2570zd2_703));
																																if ((CAR((
																																				(obj_t)
																																				BgL_carzd2588zd2_716))
																																		==
																																		BINT(88L)))
																																	{	/* Jas/peep.scm 222 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_carzd2588zd2_716))))
																																			{	/* Jas/peep.scm 222 */
																																				{	/* Jas/peep.scm 211 */
																																					obj_t
																																						BgL_tmpz00_3453;
																																					BgL_tmpz00_3453
																																						=
																																						CDR
																																						(CDR
																																						(CDR
																																							(BgL_lz00_496)));
																																					SET_CDR
																																						(BgL_lz00_496,
																																						BgL_tmpz00_3453);
																																				}
																																				{

																																					goto
																																						BgL_zc3z04anonymousza31547ze3z87_497;
																																				}
																																			}
																																		else
																																			{	/* Jas/peep.scm 222 */
																																				goto
																																					BgL_tagzd2109zd2_514;
																																			}
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2109zd2_514;
																																	}
																															}
																													}
																												else
																													{	/* Jas/peep.scm 222 */
																														goto
																															BgL_tagzd2109zd2_514;
																													}
																											}
																										else
																											{	/* Jas/peep.scm 222 */
																												goto
																													BgL_tagzd2109zd2_514;
																											}
																									}
																								else
																									{	/* Jas/peep.scm 222 */
																										obj_t BgL_carzd2598zd2_727;

																										BgL_carzd2598zd2_727 =
																											CAR(
																											((obj_t) BgL_lz00_496));
																										if (PAIRP
																											(BgL_carzd2598zd2_727))
																											{	/* Jas/peep.scm 222 */
																												if (
																													(CAR
																														(BgL_carzd2598zd2_727)
																														== BINT(9L)))
																													{	/* Jas/peep.scm 222 */
																														if (NULLP(CDR
																																(BgL_carzd2598zd2_727)))
																															{	/* Jas/peep.scm 222 */
																																obj_t
																																	BgL_carzd2602zd2_733;
																																BgL_carzd2602zd2_733
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd2123zd2_519));
																																if ((CAR((
																																				(obj_t)
																																				BgL_carzd2602zd2_733))
																																		==
																																		BINT(136L)))
																																	{	/* Jas/peep.scm 222 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_carzd2602zd2_733))))
																																			{	/* Jas/peep.scm 222 */
																																				goto
																																					BgL_tagzd2107zd2_512;
																																			}
																																		else
																																			{	/* Jas/peep.scm 222 */
																																				goto
																																					BgL_tagzd2109zd2_514;
																																			}
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2109zd2_514;
																																	}
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																goto
																																	BgL_tagzd2109zd2_514;
																															}
																													}
																												else
																													{	/* Jas/peep.scm 222 */
																														if (
																															(CAR(
																																	((obj_t)
																																		BgL_carzd2598zd2_727))
																																== BINT(10L)))
																															{	/* Jas/peep.scm 222 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_carzd2598zd2_727))))
																																	{	/* Jas/peep.scm 222 */
																																		obj_t
																																			BgL_carzd2610zd2_747;
																																		{	/* Jas/peep.scm 222 */
																																			obj_t
																																				BgL_pairz00_2176;
																																			BgL_pairz00_2176
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_lz00_496));
																																			BgL_carzd2610zd2_747
																																				=
																																				CAR
																																				(BgL_pairz00_2176);
																																		}
																																		if (
																																			(CAR(
																																					((obj_t) BgL_carzd2610zd2_747)) == BINT(136L)))
																																			{	/* Jas/peep.scm 222 */
																																				if (NULLP(CDR(((obj_t) BgL_carzd2610zd2_747))))
																																					{	/* Jas/peep.scm 222 */
																																						goto
																																							BgL_tagzd2108zd2_513;
																																					}
																																				else
																																					{	/* Jas/peep.scm 222 */
																																						goto
																																							BgL_tagzd2109zd2_514;
																																					}
																																			}
																																		else
																																			{	/* Jas/peep.scm 222 */
																																				goto
																																					BgL_tagzd2109zd2_514;
																																			}
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2109zd2_514;
																																	}
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																goto
																																	BgL_tagzd2109zd2_514;
																															}
																													}
																											}
																										else
																											{	/* Jas/peep.scm 222 */
																												goto
																													BgL_tagzd2109zd2_514;
																											}
																									}
																							}
																						}
																				}
																			else
																				{	/* Jas/peep.scm 222 */
																					goto BgL_tagzd2109zd2_514;
																				}
																		}
																	else
																		{	/* Jas/peep.scm 222 */
																			goto BgL_tagzd2109zd2_514;
																		}
																}
														}
													else
														{	/* Jas/peep.scm 222 */
															if (PAIRP(BgL_cdrzd2123zd2_519))
																{	/* Jas/peep.scm 222 */
																	obj_t BgL_carzd2641zd2_780;
																	obj_t BgL_cdrzd2642zd2_781;

																	BgL_carzd2641zd2_780 =
																		CAR(BgL_cdrzd2123zd2_519);
																	BgL_cdrzd2642zd2_781 =
																		CDR(BgL_cdrzd2123zd2_519);
																	if (PAIRP(BgL_carzd2641zd2_780))
																		{	/* Jas/peep.scm 222 */
																			if (
																				(CAR(BgL_carzd2641zd2_780) == BINT(4L)))
																				{	/* Jas/peep.scm 222 */
																					if (NULLP(CDR(BgL_carzd2641zd2_780)))
																						{	/* Jas/peep.scm 222 */
																							if (PAIRP(BgL_cdrzd2642zd2_781))
																								{	/* Jas/peep.scm 222 */
																									obj_t BgL_carzd2646zd2_788;

																									BgL_carzd2646zd2_788 =
																										CAR(BgL_cdrzd2642zd2_781);
																									if (PAIRP
																										(BgL_carzd2646zd2_788))
																										{	/* Jas/peep.scm 222 */
																											obj_t
																												BgL_cdrzd2650zd2_790;
																											BgL_cdrzd2650zd2_790 =
																												CDR
																												(BgL_carzd2646zd2_788);
																											if ((CAR
																													(BgL_carzd2646zd2_788)
																													== BINT(153L)))
																												{	/* Jas/peep.scm 222 */
																													if (PAIRP
																														(BgL_cdrzd2650zd2_790))
																														{	/* Jas/peep.scm 222 */
																															if (NULLP(CDR
																																	(BgL_cdrzd2650zd2_790)))
																																{	/* Jas/peep.scm 222 */
																																	{	/* Jas/peep.scm 205 */
																																		obj_t
																																			BgL_tmpz00_3529;
																																		BgL_tmpz00_3529
																																			=
																																			CDR(CDR
																																			(CDR
																																				(BgL_lz00_496)));
																																		SET_CDR
																																			(BgL_lz00_496,
																																			BgL_tmpz00_3529);
																																	}
																																	{

																																		goto
																																			BgL_zc3z04anonymousza31547ze3z87_497;
																																	}
																																}
																															else
																																{	/* Jas/peep.scm 222 */
																																	goto
																																		BgL_tagzd2109zd2_514;
																																}
																														}
																													else
																														{	/* Jas/peep.scm 222 */
																															goto
																																BgL_tagzd2109zd2_514;
																														}
																												}
																											else
																												{	/* Jas/peep.scm 222 */
																													goto
																														BgL_tagzd2109zd2_514;
																												}
																										}
																									else
																										{	/* Jas/peep.scm 222 */
																											goto BgL_tagzd2109zd2_514;
																										}
																								}
																							else
																								{	/* Jas/peep.scm 222 */
																									goto BgL_tagzd2109zd2_514;
																								}
																						}
																					else
																						{	/* Jas/peep.scm 222 */
																							goto BgL_tagzd2109zd2_514;
																						}
																				}
																			else
																				{	/* Jas/peep.scm 222 */
																					obj_t BgL_cdrzd2685zd2_802;

																					BgL_cdrzd2685zd2_802 =
																						CDR(((obj_t) BgL_cdrzd2123zd2_519));
																					{	/* Jas/peep.scm 222 */
																						bool_t BgL_test2752z00_3536;

																						{	/* Jas/peep.scm 222 */
																							obj_t BgL_tmpz00_3537;

																							{	/* Jas/peep.scm 222 */
																								obj_t BgL_pairz00_2208;

																								BgL_pairz00_2208 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd2123zd2_519));
																								BgL_tmpz00_3537 =
																									CAR(BgL_pairz00_2208);
																							}
																							BgL_test2752z00_3536 =
																								(BgL_tmpz00_3537 == BINT(178L));
																						}
																						if (BgL_test2752z00_3536)
																							{	/* Jas/peep.scm 222 */
																								if (PAIRP(BgL_cdrzd2685zd2_802))
																									{	/* Jas/peep.scm 222 */
																										obj_t BgL_carzd2688zd2_807;

																										BgL_carzd2688zd2_807 =
																											CAR(BgL_cdrzd2685zd2_802);
																										if (PAIRP
																											(BgL_carzd2688zd2_807))
																											{	/* Jas/peep.scm 222 */
																												if (
																													(CAR
																														(BgL_carzd2688zd2_807)
																														== BINT(87L)))
																													{	/* Jas/peep.scm 222 */
																														if (NULLP(CDR
																																(BgL_carzd2688zd2_807)))
																															{	/* Jas/peep.scm 222 */
																																{	/* Jas/peep.scm 208 */
																																	obj_t
																																		BgL_tmpz00_3555;
																																	BgL_tmpz00_3555
																																		=
																																		CDR(CDR(CDR
																																			(BgL_lz00_496)));
																																	SET_CDR
																																		(BgL_lz00_496,
																																		BgL_tmpz00_3555);
																																}
																																{

																																	goto
																																		BgL_zc3z04anonymousza31547ze3z87_497;
																																}
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																goto
																																	BgL_tagzd2109zd2_514;
																															}
																													}
																												else
																													{	/* Jas/peep.scm 222 */
																														obj_t
																															BgL_carzd2703zd2_815;
																														BgL_carzd2703zd2_815
																															=
																															CAR(((obj_t)
																																BgL_cdrzd2685zd2_802));
																														if ((CAR(((obj_t)
																																		BgL_carzd2703zd2_815))
																																== BINT(88L)))
																															{	/* Jas/peep.scm 222 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_carzd2703zd2_815))))
																																	{	/* Jas/peep.scm 222 */
																																		{	/* Jas/peep.scm 211 */
																																			obj_t
																																				BgL_tmpz00_3571;
																																			BgL_tmpz00_3571
																																				=
																																				CDR(CDR
																																				(CDR
																																					(BgL_lz00_496)));
																																			SET_CDR
																																				(BgL_lz00_496,
																																				BgL_tmpz00_3571);
																																		}
																																		{

																																			goto
																																				BgL_zc3z04anonymousza31547ze3z87_497;
																																		}
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2109zd2_514;
																																	}
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																goto
																																	BgL_tagzd2109zd2_514;
																															}
																													}
																											}
																										else
																											{	/* Jas/peep.scm 222 */
																												goto
																													BgL_tagzd2109zd2_514;
																											}
																									}
																								else
																									{	/* Jas/peep.scm 222 */
																										goto BgL_tagzd2109zd2_514;
																									}
																							}
																						else
																							{	/* Jas/peep.scm 222 */
																								obj_t BgL_carzd2713zd2_826;

																								BgL_carzd2713zd2_826 =
																									CAR(((obj_t) BgL_lz00_496));
																								if (PAIRP(BgL_carzd2713zd2_826))
																									{	/* Jas/peep.scm 222 */
																										if (
																											(CAR(BgL_carzd2713zd2_826)
																												== BINT(9L)))
																											{	/* Jas/peep.scm 222 */
																												if (NULLP(CDR
																														(BgL_carzd2713zd2_826)))
																													{	/* Jas/peep.scm 222 */
																														obj_t
																															BgL_carzd2717zd2_832;
																														BgL_carzd2717zd2_832
																															=
																															CAR(((obj_t)
																																BgL_cdrzd2123zd2_519));
																														if ((CAR(((obj_t)
																																		BgL_carzd2717zd2_832))
																																== BINT(136L)))
																															{	/* Jas/peep.scm 222 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_carzd2717zd2_832))))
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2107zd2_512;
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2109zd2_514;
																																	}
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																goto
																																	BgL_tagzd2109zd2_514;
																															}
																													}
																												else
																													{	/* Jas/peep.scm 222 */
																														goto
																															BgL_tagzd2109zd2_514;
																													}
																											}
																										else
																											{	/* Jas/peep.scm 222 */
																												if (
																													(CAR(
																															((obj_t)
																																BgL_carzd2713zd2_826))
																														== BINT(10L)))
																													{	/* Jas/peep.scm 222 */
																														if (NULLP(CDR(
																																	((obj_t)
																																		BgL_carzd2713zd2_826))))
																															{	/* Jas/peep.scm 222 */
																																obj_t
																																	BgL_carzd2725zd2_846;
																																{	/* Jas/peep.scm 222 */
																																	obj_t
																																		BgL_pairz00_2246;
																																	BgL_pairz00_2246
																																		=
																																		CDR(((obj_t)
																																			BgL_lz00_496));
																																	BgL_carzd2725zd2_846
																																		=
																																		CAR
																																		(BgL_pairz00_2246);
																																}
																																if (
																																	(CAR(
																																			((obj_t)
																																				BgL_carzd2725zd2_846))
																																		==
																																		BINT(136L)))
																																	{	/* Jas/peep.scm 222 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_carzd2725zd2_846))))
																																			{	/* Jas/peep.scm 222 */
																																				goto
																																					BgL_tagzd2108zd2_513;
																																			}
																																		else
																																			{	/* Jas/peep.scm 222 */
																																				goto
																																					BgL_tagzd2109zd2_514;
																																			}
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2109zd2_514;
																																	}
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																goto
																																	BgL_tagzd2109zd2_514;
																															}
																													}
																												else
																													{	/* Jas/peep.scm 222 */
																														goto
																															BgL_tagzd2109zd2_514;
																													}
																											}
																									}
																								else
																									{	/* Jas/peep.scm 222 */
																										goto BgL_tagzd2109zd2_514;
																									}
																							}
																					}
																				}
																		}
																	else
																		{	/* Jas/peep.scm 222 */
																			goto BgL_tagzd2109zd2_514;
																		}
																}
															else
																{	/* Jas/peep.scm 222 */
																	goto BgL_tagzd2109zd2_514;
																}
														}
												}
											else
												{	/* Jas/peep.scm 222 */
													if ((CAR(((obj_t) BgL_carzd2122zd2_518)) == BINT(3L)))
														{	/* Jas/peep.scm 222 */
															if (NULLP(CDR(((obj_t) BgL_carzd2122zd2_518))))
																{	/* Jas/peep.scm 222 */
																	if (PAIRP(BgL_cdrzd2123zd2_519))
																		{	/* Jas/peep.scm 222 */
																			obj_t BgL_carzd2753zd2_883;

																			BgL_carzd2753zd2_883 =
																				CAR(BgL_cdrzd2123zd2_519);
																			if (PAIRP(BgL_carzd2753zd2_883))
																				{	/* Jas/peep.scm 222 */
																					obj_t BgL_cdrzd2757zd2_885;

																					BgL_cdrzd2757zd2_885 =
																						CDR(BgL_carzd2753zd2_883);
																					if (
																						(CAR(BgL_carzd2753zd2_883) ==
																							BINT(153L)))
																						{	/* Jas/peep.scm 222 */
																							if (PAIRP(BgL_cdrzd2757zd2_885))
																								{	/* Jas/peep.scm 222 */
																									if (NULLP(CDR
																											(BgL_cdrzd2757zd2_885)))
																										{	/* Jas/peep.scm 222 */
																											BgL_labz00_503 =
																												CAR
																												(BgL_cdrzd2757zd2_885);
																											{	/* Jas/peep.scm 202 */
																												obj_t
																													BgL_arg2396z00_1299;
																												{	/* Jas/peep.scm 202 */
																													obj_t
																														BgL_arg2397z00_1300;
																													BgL_arg2397z00_1300 =
																														MAKE_YOUNG_PAIR
																														(BgL_labz00_503,
																														BNIL);
																													BgL_arg2396z00_1299 =
																														MAKE_YOUNG_PAIR(BINT
																														(167L),
																														BgL_arg2397z00_1300);
																												}
																												{	/* Jas/peep.scm 202 */
																													obj_t BgL_tmpz00_3646;

																													BgL_tmpz00_3646 =
																														((obj_t)
																														BgL_lz00_496);
																													SET_CAR
																														(BgL_tmpz00_3646,
																														BgL_arg2396z00_1299);
																												}
																											}
																											{	/* Jas/peep.scm 203 */
																												obj_t
																													BgL_arg2398z00_1301;
																												BgL_arg2398z00_1301 =
																													CDR(((obj_t)
																														BgL_lz00_496));
																												{
																													obj_t BgL_lz00_3651;

																													BgL_lz00_3651 =
																														BgL_arg2398z00_1301;
																													BgL_lz00_496 =
																														BgL_lz00_3651;
																													goto
																														BgL_zc3z04anonymousza31547ze3z87_497;
																												}
																											}
																										}
																									else
																										{	/* Jas/peep.scm 222 */
																											goto BgL_tagzd2109zd2_514;
																										}
																								}
																							else
																								{	/* Jas/peep.scm 222 */
																									goto BgL_tagzd2109zd2_514;
																								}
																						}
																					else
																						{	/* Jas/peep.scm 222 */
																							obj_t BgL_carzd2786zd2_894;
																							obj_t BgL_cdrzd2787zd2_895;

																							BgL_carzd2786zd2_894 =
																								CAR(
																								((obj_t) BgL_cdrzd2123zd2_519));
																							BgL_cdrzd2787zd2_895 =
																								CDR(
																								((obj_t) BgL_cdrzd2123zd2_519));
																							if (
																								(CAR(
																										((obj_t)
																											BgL_carzd2786zd2_894)) ==
																									BINT(4L)))
																								{	/* Jas/peep.scm 222 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_carzd2786zd2_894))))
																										{	/* Jas/peep.scm 222 */
																											if (PAIRP
																												(BgL_cdrzd2787zd2_895))
																												{	/* Jas/peep.scm 222 */
																													obj_t
																														BgL_carzd2791zd2_901;
																													BgL_carzd2791zd2_901 =
																														CAR
																														(BgL_cdrzd2787zd2_895);
																													if (PAIRP
																														(BgL_carzd2791zd2_901))
																														{	/* Jas/peep.scm 222 */
																															obj_t
																																BgL_cdrzd2795zd2_903;
																															BgL_cdrzd2795zd2_903
																																=
																																CDR
																																(BgL_carzd2791zd2_901);
																															if ((CAR
																																	(BgL_carzd2791zd2_901)
																																	==
																																	BINT(153L)))
																																{	/* Jas/peep.scm 222 */
																																	if (PAIRP
																																		(BgL_cdrzd2795zd2_903))
																																		{	/* Jas/peep.scm 222 */
																																			if (NULLP
																																				(CDR
																																					(BgL_cdrzd2795zd2_903)))
																																				{	/* Jas/peep.scm 222 */
																																					{	/* Jas/peep.scm 205 */
																																						obj_t
																																							BgL_tmpz00_3681;
																																						BgL_tmpz00_3681
																																							=
																																							CDR
																																							(CDR
																																							(CDR
																																								(BgL_lz00_496)));
																																						SET_CDR
																																							(BgL_lz00_496,
																																							BgL_tmpz00_3681);
																																					}
																																					{

																																						goto
																																							BgL_zc3z04anonymousza31547ze3z87_497;
																																					}
																																				}
																																			else
																																				{	/* Jas/peep.scm 222 */
																																					goto
																																						BgL_tagzd2109zd2_514;
																																				}
																																		}
																																	else
																																		{	/* Jas/peep.scm 222 */
																																			goto
																																				BgL_tagzd2109zd2_514;
																																		}
																																}
																															else
																																{	/* Jas/peep.scm 222 */
																																	goto
																																		BgL_tagzd2109zd2_514;
																																}
																														}
																													else
																														{	/* Jas/peep.scm 222 */
																															goto
																																BgL_tagzd2109zd2_514;
																														}
																												}
																											else
																												{	/* Jas/peep.scm 222 */
																													goto
																														BgL_tagzd2109zd2_514;
																												}
																										}
																									else
																										{	/* Jas/peep.scm 222 */
																											goto BgL_tagzd2109zd2_514;
																										}
																								}
																							else
																								{	/* Jas/peep.scm 222 */
																									bool_t BgL_test2782z00_3686;

																									{	/* Jas/peep.scm 222 */
																										obj_t BgL_tmpz00_3687;

																										{	/* Jas/peep.scm 222 */
																											obj_t BgL_pairz00_2287;

																											BgL_pairz00_2287 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd2123zd2_519));
																											BgL_tmpz00_3687 =
																												CAR(BgL_pairz00_2287);
																										}
																										BgL_test2782z00_3686 =
																											(BgL_tmpz00_3687 ==
																											BINT(178L));
																									}
																									if (BgL_test2782z00_3686)
																										{	/* Jas/peep.scm 222 */
																											if (PAIRP
																												(BgL_cdrzd2787zd2_895))
																												{	/* Jas/peep.scm 222 */
																													obj_t
																														BgL_carzd2833zd2_920;
																													BgL_carzd2833zd2_920 =
																														CAR
																														(BgL_cdrzd2787zd2_895);
																													if (PAIRP
																														(BgL_carzd2833zd2_920))
																														{	/* Jas/peep.scm 222 */
																															if (
																																(CAR
																																	(BgL_carzd2833zd2_920)
																																	== BINT(87L)))
																																{	/* Jas/peep.scm 222 */
																																	if (NULLP(CDR
																																			(BgL_carzd2833zd2_920)))
																																		{	/* Jas/peep.scm 222 */
																																			{	/* Jas/peep.scm 208 */
																																				obj_t
																																					BgL_tmpz00_3705;
																																				BgL_tmpz00_3705
																																					=
																																					CDR
																																					(CDR
																																					(CDR
																																						(BgL_lz00_496)));
																																				SET_CDR
																																					(BgL_lz00_496,
																																					BgL_tmpz00_3705);
																																			}
																																			{

																																				goto
																																					BgL_zc3z04anonymousza31547ze3z87_497;
																																			}
																																		}
																																	else
																																		{	/* Jas/peep.scm 222 */
																																			goto
																																				BgL_tagzd2109zd2_514;
																																		}
																																}
																															else
																																{	/* Jas/peep.scm 222 */
																																	obj_t
																																		BgL_carzd2848zd2_928;
																																	{	/* Jas/peep.scm 222 */
																																		obj_t
																																			BgL_pairz00_2302;
																																		{	/* Jas/peep.scm 222 */
																																			obj_t
																																				BgL_pairz00_2301;
																																			BgL_pairz00_2301
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_lz00_496));
																																			BgL_pairz00_2302
																																				=
																																				CDR
																																				(BgL_pairz00_2301);
																																		}
																																		BgL_carzd2848zd2_928
																																			=
																																			CAR
																																			(BgL_pairz00_2302);
																																	}
																																	if (
																																		(CAR(
																																				((obj_t)
																																					BgL_carzd2848zd2_928))
																																			==
																																			BINT
																																			(88L)))
																																		{	/* Jas/peep.scm 222 */
																																			if (NULLP
																																				(CDR(((obj_t) BgL_carzd2848zd2_928))))
																																				{	/* Jas/peep.scm 222 */
																																					{	/* Jas/peep.scm 211 */
																																						obj_t
																																							BgL_tmpz00_3723;
																																						BgL_tmpz00_3723
																																							=
																																							CDR
																																							(CDR
																																							(CDR
																																								(BgL_lz00_496)));
																																						SET_CDR
																																							(BgL_lz00_496,
																																							BgL_tmpz00_3723);
																																					}
																																					{

																																						goto
																																							BgL_zc3z04anonymousza31547ze3z87_497;
																																					}
																																				}
																																			else
																																				{	/* Jas/peep.scm 222 */
																																					goto
																																						BgL_tagzd2109zd2_514;
																																				}
																																		}
																																	else
																																		{	/* Jas/peep.scm 222 */
																																			goto
																																				BgL_tagzd2109zd2_514;
																																		}
																																}
																														}
																													else
																														{	/* Jas/peep.scm 222 */
																															goto
																																BgL_tagzd2109zd2_514;
																														}
																												}
																											else
																												{	/* Jas/peep.scm 222 */
																													goto
																														BgL_tagzd2109zd2_514;
																												}
																										}
																									else
																										{	/* Jas/peep.scm 222 */
																											obj_t
																												BgL_carzd2858zd2_939;
																											BgL_carzd2858zd2_939 =
																												CAR(((obj_t)
																													BgL_lz00_496));
																											if (PAIRP
																												(BgL_carzd2858zd2_939))
																												{	/* Jas/peep.scm 222 */
																													if (
																														(CAR
																															(BgL_carzd2858zd2_939)
																															== BINT(9L)))
																														{	/* Jas/peep.scm 222 */
																															if (NULLP(CDR
																																	(BgL_carzd2858zd2_939)))
																																{	/* Jas/peep.scm 222 */
																																	obj_t
																																		BgL_carzd2862zd2_945;
																																	{	/* Jas/peep.scm 222 */
																																		obj_t
																																			BgL_pairz00_2318;
																																		BgL_pairz00_2318
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_lz00_496));
																																		BgL_carzd2862zd2_945
																																			=
																																			CAR
																																			(BgL_pairz00_2318);
																																	}
																																	if (
																																		(CAR(
																																				((obj_t)
																																					BgL_carzd2862zd2_945))
																																			==
																																			BINT
																																			(136L)))
																																		{	/* Jas/peep.scm 222 */
																																			if (NULLP
																																				(CDR(((obj_t) BgL_carzd2862zd2_945))))
																																				{	/* Jas/peep.scm 222 */
																																					goto
																																						BgL_tagzd2107zd2_512;
																																				}
																																			else
																																				{	/* Jas/peep.scm 222 */
																																					goto
																																						BgL_tagzd2109zd2_514;
																																				}
																																		}
																																	else
																																		{	/* Jas/peep.scm 222 */
																																			goto
																																				BgL_tagzd2109zd2_514;
																																		}
																																}
																															else
																																{	/* Jas/peep.scm 222 */
																																	goto
																																		BgL_tagzd2109zd2_514;
																																}
																														}
																													else
																														{	/* Jas/peep.scm 222 */
																															if (
																																(CAR(
																																		((obj_t)
																																			BgL_carzd2858zd2_939))
																																	== BINT(10L)))
																																{	/* Jas/peep.scm 222 */
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_carzd2858zd2_939))))
																																		{	/* Jas/peep.scm 222 */
																																			obj_t
																																				BgL_carzd2870zd2_959;
																																			{	/* Jas/peep.scm 222 */
																																				obj_t
																																					BgL_pairz00_2325;
																																				BgL_pairz00_2325
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_lz00_496));
																																				BgL_carzd2870zd2_959
																																					=
																																					CAR
																																					(BgL_pairz00_2325);
																																			}
																																			if (
																																				(CAR(
																																						((obj_t) BgL_carzd2870zd2_959)) == BINT(136L)))
																																				{	/* Jas/peep.scm 222 */
																																					if (NULLP(CDR(((obj_t) BgL_carzd2870zd2_959))))
																																						{	/* Jas/peep.scm 222 */
																																							goto
																																								BgL_tagzd2108zd2_513;
																																						}
																																					else
																																						{	/* Jas/peep.scm 222 */
																																							goto
																																								BgL_tagzd2109zd2_514;
																																						}
																																				}
																																			else
																																				{	/* Jas/peep.scm 222 */
																																					goto
																																						BgL_tagzd2109zd2_514;
																																				}
																																		}
																																	else
																																		{	/* Jas/peep.scm 222 */
																																			goto
																																				BgL_tagzd2109zd2_514;
																																		}
																																}
																															else
																																{	/* Jas/peep.scm 222 */
																																	goto
																																		BgL_tagzd2109zd2_514;
																																}
																														}
																												}
																											else
																												{	/* Jas/peep.scm 222 */
																													goto
																														BgL_tagzd2109zd2_514;
																												}
																										}
																								}
																						}
																				}
																			else
																				{	/* Jas/peep.scm 222 */
																					goto BgL_tagzd2109zd2_514;
																				}
																		}
																	else
																		{	/* Jas/peep.scm 222 */
																			goto BgL_tagzd2109zd2_514;
																		}
																}
															else
																{	/* Jas/peep.scm 222 */
																	if (PAIRP(BgL_cdrzd2123zd2_519))
																		{	/* Jas/peep.scm 222 */
																			obj_t BgL_carzd2906zd2_992;
																			obj_t BgL_cdrzd2907zd2_993;

																			BgL_carzd2906zd2_992 =
																				CAR(BgL_cdrzd2123zd2_519);
																			BgL_cdrzd2907zd2_993 =
																				CDR(BgL_cdrzd2123zd2_519);
																			if (PAIRP(BgL_carzd2906zd2_992))
																				{	/* Jas/peep.scm 222 */
																					if (
																						(CAR(BgL_carzd2906zd2_992) ==
																							BINT(4L)))
																						{	/* Jas/peep.scm 222 */
																							if (NULLP(CDR
																									(BgL_carzd2906zd2_992)))
																								{	/* Jas/peep.scm 222 */
																									if (PAIRP
																										(BgL_cdrzd2907zd2_993))
																										{	/* Jas/peep.scm 222 */
																											obj_t
																												BgL_carzd2911zd2_1000;
																											BgL_carzd2911zd2_1000 =
																												CAR
																												(BgL_cdrzd2907zd2_993);
																											if (PAIRP
																												(BgL_carzd2911zd2_1000))
																												{	/* Jas/peep.scm 222 */
																													obj_t
																														BgL_cdrzd2915zd2_1002;
																													BgL_cdrzd2915zd2_1002
																														=
																														CDR
																														(BgL_carzd2911zd2_1000);
																													if ((CAR
																															(BgL_carzd2911zd2_1000)
																															== BINT(153L)))
																														{	/* Jas/peep.scm 222 */
																															if (PAIRP
																																(BgL_cdrzd2915zd2_1002))
																																{	/* Jas/peep.scm 222 */
																																	if (NULLP(CDR
																																			(BgL_cdrzd2915zd2_1002)))
																																		{	/* Jas/peep.scm 222 */
																																			{	/* Jas/peep.scm 205 */
																																				obj_t
																																					BgL_tmpz00_3800;
																																				BgL_tmpz00_3800
																																					=
																																					CDR
																																					(CDR
																																					(CDR
																																						(BgL_lz00_496)));
																																				SET_CDR
																																					(BgL_lz00_496,
																																					BgL_tmpz00_3800);
																																			}
																																			{

																																				goto
																																					BgL_zc3z04anonymousza31547ze3z87_497;
																																			}
																																		}
																																	else
																																		{	/* Jas/peep.scm 222 */
																																			goto
																																				BgL_tagzd2109zd2_514;
																																		}
																																}
																															else
																																{	/* Jas/peep.scm 222 */
																																	goto
																																		BgL_tagzd2109zd2_514;
																																}
																														}
																													else
																														{	/* Jas/peep.scm 222 */
																															goto
																																BgL_tagzd2109zd2_514;
																														}
																												}
																											else
																												{	/* Jas/peep.scm 222 */
																													goto
																														BgL_tagzd2109zd2_514;
																												}
																										}
																									else
																										{	/* Jas/peep.scm 222 */
																											goto BgL_tagzd2109zd2_514;
																										}
																								}
																							else
																								{	/* Jas/peep.scm 222 */
																									goto BgL_tagzd2109zd2_514;
																								}
																						}
																					else
																						{	/* Jas/peep.scm 222 */
																							obj_t BgL_cdrzd2950zd2_1014;

																							BgL_cdrzd2950zd2_1014 =
																								CDR(
																								((obj_t) BgL_cdrzd2123zd2_519));
																							{	/* Jas/peep.scm 222 */
																								bool_t BgL_test2807z00_3807;

																								{	/* Jas/peep.scm 222 */
																									obj_t BgL_tmpz00_3808;

																									{	/* Jas/peep.scm 222 */
																										obj_t BgL_pairz00_2357;

																										BgL_pairz00_2357 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd2123zd2_519));
																										BgL_tmpz00_3808 =
																											CAR(BgL_pairz00_2357);
																									}
																									BgL_test2807z00_3807 =
																										(BgL_tmpz00_3808 ==
																										BINT(178L));
																								}
																								if (BgL_test2807z00_3807)
																									{	/* Jas/peep.scm 222 */
																										if (PAIRP
																											(BgL_cdrzd2950zd2_1014))
																											{	/* Jas/peep.scm 222 */
																												obj_t
																													BgL_carzd2953zd2_1019;
																												BgL_carzd2953zd2_1019 =
																													CAR
																													(BgL_cdrzd2950zd2_1014);
																												if (PAIRP
																													(BgL_carzd2953zd2_1019))
																													{	/* Jas/peep.scm 222 */
																														if (
																															(CAR
																																(BgL_carzd2953zd2_1019)
																																== BINT(87L)))
																															{	/* Jas/peep.scm 222 */
																																if (NULLP(CDR
																																		(BgL_carzd2953zd2_1019)))
																																	{	/* Jas/peep.scm 222 */
																																		{	/* Jas/peep.scm 208 */
																																			obj_t
																																				BgL_tmpz00_3826;
																																			BgL_tmpz00_3826
																																				=
																																				CDR(CDR
																																				(CDR
																																					(BgL_lz00_496)));
																																			SET_CDR
																																				(BgL_lz00_496,
																																				BgL_tmpz00_3826);
																																		}
																																		{

																																			goto
																																				BgL_zc3z04anonymousza31547ze3z87_497;
																																		}
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2109zd2_514;
																																	}
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																obj_t
																																	BgL_carzd2968zd2_1027;
																																{	/* Jas/peep.scm 222 */
																																	obj_t
																																		BgL_pairz00_2372;
																																	{	/* Jas/peep.scm 222 */
																																		obj_t
																																			BgL_pairz00_2371;
																																		BgL_pairz00_2371
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_lz00_496));
																																		BgL_pairz00_2372
																																			=
																																			CDR
																																			(BgL_pairz00_2371);
																																	}
																																	BgL_carzd2968zd2_1027
																																		=
																																		CAR
																																		(BgL_pairz00_2372);
																																}
																																if (
																																	(CAR(
																																			((obj_t)
																																				BgL_carzd2968zd2_1027))
																																		==
																																		BINT(88L)))
																																	{	/* Jas/peep.scm 222 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_carzd2968zd2_1027))))
																																			{	/* Jas/peep.scm 222 */
																																				{	/* Jas/peep.scm 211 */
																																					obj_t
																																						BgL_tmpz00_3844;
																																					BgL_tmpz00_3844
																																						=
																																						CDR
																																						(CDR
																																						(CDR
																																							(BgL_lz00_496)));
																																					SET_CDR
																																						(BgL_lz00_496,
																																						BgL_tmpz00_3844);
																																				}
																																				{

																																					goto
																																						BgL_zc3z04anonymousza31547ze3z87_497;
																																				}
																																			}
																																		else
																																			{	/* Jas/peep.scm 222 */
																																				goto
																																					BgL_tagzd2109zd2_514;
																																			}
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2109zd2_514;
																																	}
																															}
																													}
																												else
																													{	/* Jas/peep.scm 222 */
																														goto
																															BgL_tagzd2109zd2_514;
																													}
																											}
																										else
																											{	/* Jas/peep.scm 222 */
																												goto
																													BgL_tagzd2109zd2_514;
																											}
																									}
																								else
																									{	/* Jas/peep.scm 222 */
																										obj_t BgL_carzd2978zd2_1038;

																										BgL_carzd2978zd2_1038 =
																											CAR(
																											((obj_t) BgL_lz00_496));
																										if (PAIRP
																											(BgL_carzd2978zd2_1038))
																											{	/* Jas/peep.scm 222 */
																												if (
																													(CAR
																														(BgL_carzd2978zd2_1038)
																														== BINT(9L)))
																													{	/* Jas/peep.scm 222 */
																														if (NULLP(CDR
																																(BgL_carzd2978zd2_1038)))
																															{	/* Jas/peep.scm 222 */
																																obj_t
																																	BgL_carzd2982zd2_1044;
																																{	/* Jas/peep.scm 222 */
																																	obj_t
																																		BgL_pairz00_2388;
																																	BgL_pairz00_2388
																																		=
																																		CDR(((obj_t)
																																			BgL_lz00_496));
																																	BgL_carzd2982zd2_1044
																																		=
																																		CAR
																																		(BgL_pairz00_2388);
																																}
																																if (
																																	(CAR(
																																			((obj_t)
																																				BgL_carzd2982zd2_1044))
																																		==
																																		BINT(136L)))
																																	{	/* Jas/peep.scm 222 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_carzd2982zd2_1044))))
																																			{	/* Jas/peep.scm 222 */
																																				goto
																																					BgL_tagzd2107zd2_512;
																																			}
																																		else
																																			{	/* Jas/peep.scm 222 */
																																				goto
																																					BgL_tagzd2109zd2_514;
																																			}
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2109zd2_514;
																																	}
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																goto
																																	BgL_tagzd2109zd2_514;
																															}
																													}
																												else
																													{	/* Jas/peep.scm 222 */
																														if (
																															(CAR(
																																	((obj_t)
																																		BgL_carzd2978zd2_1038))
																																== BINT(10L)))
																															{	/* Jas/peep.scm 222 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_carzd2978zd2_1038))))
																																	{	/* Jas/peep.scm 222 */
																																		obj_t
																																			BgL_carzd2990zd2_1058;
																																		{	/* Jas/peep.scm 222 */
																																			obj_t
																																				BgL_pairz00_2395;
																																			BgL_pairz00_2395
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_lz00_496));
																																			BgL_carzd2990zd2_1058
																																				=
																																				CAR
																																				(BgL_pairz00_2395);
																																		}
																																		if (
																																			(CAR(
																																					((obj_t) BgL_carzd2990zd2_1058)) == BINT(136L)))
																																			{	/* Jas/peep.scm 222 */
																																				if (NULLP(CDR(((obj_t) BgL_carzd2990zd2_1058))))
																																					{	/* Jas/peep.scm 222 */
																																						goto
																																							BgL_tagzd2108zd2_513;
																																					}
																																				else
																																					{	/* Jas/peep.scm 222 */
																																						goto
																																							BgL_tagzd2109zd2_514;
																																					}
																																			}
																																		else
																																			{	/* Jas/peep.scm 222 */
																																				goto
																																					BgL_tagzd2109zd2_514;
																																			}
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2109zd2_514;
																																	}
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																goto
																																	BgL_tagzd2109zd2_514;
																															}
																													}
																											}
																										else
																											{	/* Jas/peep.scm 222 */
																												goto
																													BgL_tagzd2109zd2_514;
																											}
																									}
																							}
																						}
																				}
																			else
																				{	/* Jas/peep.scm 222 */
																					goto BgL_tagzd2109zd2_514;
																				}
																		}
																	else
																		{	/* Jas/peep.scm 222 */
																			goto BgL_tagzd2109zd2_514;
																		}
																}
														}
													else
														{	/* Jas/peep.scm 222 */
															if (PAIRP(BgL_cdrzd2123zd2_519))
																{	/* Jas/peep.scm 222 */
																	obj_t BgL_carzd21018zd2_1091;
																	obj_t BgL_cdrzd21019zd2_1092;

																	BgL_carzd21018zd2_1091 =
																		CAR(BgL_cdrzd2123zd2_519);
																	BgL_cdrzd21019zd2_1092 =
																		CDR(BgL_cdrzd2123zd2_519);
																	if (PAIRP(BgL_carzd21018zd2_1091))
																		{	/* Jas/peep.scm 222 */
																			if (
																				(CAR(BgL_carzd21018zd2_1091) ==
																					BINT(4L)))
																				{	/* Jas/peep.scm 222 */
																					if (NULLP(CDR
																							(BgL_carzd21018zd2_1091)))
																						{	/* Jas/peep.scm 222 */
																							if (PAIRP(BgL_cdrzd21019zd2_1092))
																								{	/* Jas/peep.scm 222 */
																									obj_t BgL_carzd21023zd2_1099;

																									BgL_carzd21023zd2_1099 =
																										CAR(BgL_cdrzd21019zd2_1092);
																									if (PAIRP
																										(BgL_carzd21023zd2_1099))
																										{	/* Jas/peep.scm 222 */
																											obj_t
																												BgL_cdrzd21027zd2_1101;
																											BgL_cdrzd21027zd2_1101 =
																												CDR
																												(BgL_carzd21023zd2_1099);
																											if ((CAR
																													(BgL_carzd21023zd2_1099)
																													== BINT(153L)))
																												{	/* Jas/peep.scm 222 */
																													if (PAIRP
																														(BgL_cdrzd21027zd2_1101))
																														{	/* Jas/peep.scm 222 */
																															if (NULLP(CDR
																																	(BgL_cdrzd21027zd2_1101)))
																																{	/* Jas/peep.scm 222 */
																																	{	/* Jas/peep.scm 205 */
																																		obj_t
																																			BgL_tmpz00_3921;
																																		BgL_tmpz00_3921
																																			=
																																			CDR(CDR
																																			(CDR
																																				(BgL_lz00_496)));
																																		SET_CDR
																																			(BgL_lz00_496,
																																			BgL_tmpz00_3921);
																																	}
																																	{

																																		goto
																																			BgL_zc3z04anonymousza31547ze3z87_497;
																																	}
																																}
																															else
																																{	/* Jas/peep.scm 222 */
																																	goto
																																		BgL_tagzd2109zd2_514;
																																}
																														}
																													else
																														{	/* Jas/peep.scm 222 */
																															goto
																																BgL_tagzd2109zd2_514;
																														}
																												}
																											else
																												{	/* Jas/peep.scm 222 */
																													goto
																														BgL_tagzd2109zd2_514;
																												}
																										}
																									else
																										{	/* Jas/peep.scm 222 */
																											goto BgL_tagzd2109zd2_514;
																										}
																								}
																							else
																								{	/* Jas/peep.scm 222 */
																									goto BgL_tagzd2109zd2_514;
																								}
																						}
																					else
																						{	/* Jas/peep.scm 222 */
																							goto BgL_tagzd2109zd2_514;
																						}
																				}
																			else
																				{	/* Jas/peep.scm 222 */
																					obj_t BgL_cdrzd21062zd2_1113;

																					BgL_cdrzd21062zd2_1113 =
																						CDR(((obj_t) BgL_cdrzd2123zd2_519));
																					{	/* Jas/peep.scm 222 */
																						bool_t BgL_test2832z00_3928;

																						{	/* Jas/peep.scm 222 */
																							obj_t BgL_tmpz00_3929;

																							{	/* Jas/peep.scm 222 */
																								obj_t BgL_pairz00_2427;

																								BgL_pairz00_2427 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd2123zd2_519));
																								BgL_tmpz00_3929 =
																									CAR(BgL_pairz00_2427);
																							}
																							BgL_test2832z00_3928 =
																								(BgL_tmpz00_3929 == BINT(178L));
																						}
																						if (BgL_test2832z00_3928)
																							{	/* Jas/peep.scm 222 */
																								if (PAIRP
																									(BgL_cdrzd21062zd2_1113))
																									{	/* Jas/peep.scm 222 */
																										obj_t
																											BgL_carzd21065zd2_1118;
																										BgL_carzd21065zd2_1118 =
																											CAR
																											(BgL_cdrzd21062zd2_1113);
																										if (PAIRP
																											(BgL_carzd21065zd2_1118))
																											{	/* Jas/peep.scm 222 */
																												if (
																													(CAR
																														(BgL_carzd21065zd2_1118)
																														== BINT(87L)))
																													{	/* Jas/peep.scm 222 */
																														if (NULLP(CDR
																																(BgL_carzd21065zd2_1118)))
																															{	/* Jas/peep.scm 222 */
																																{	/* Jas/peep.scm 208 */
																																	obj_t
																																		BgL_tmpz00_3947;
																																	BgL_tmpz00_3947
																																		=
																																		CDR(CDR(CDR
																																			(BgL_lz00_496)));
																																	SET_CDR
																																		(BgL_lz00_496,
																																		BgL_tmpz00_3947);
																																}
																																{

																																	goto
																																		BgL_zc3z04anonymousza31547ze3z87_497;
																																}
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																goto
																																	BgL_tagzd2109zd2_514;
																															}
																													}
																												else
																													{	/* Jas/peep.scm 222 */
																														obj_t
																															BgL_carzd21080zd2_1126;
																														{	/* Jas/peep.scm 222 */
																															obj_t
																																BgL_pairz00_2442;
																															{	/* Jas/peep.scm 222 */
																																obj_t
																																	BgL_pairz00_2441;
																																BgL_pairz00_2441
																																	=
																																	CDR(((obj_t)
																																		BgL_lz00_496));
																																BgL_pairz00_2442
																																	=
																																	CDR
																																	(BgL_pairz00_2441);
																															}
																															BgL_carzd21080zd2_1126
																																=
																																CAR
																																(BgL_pairz00_2442);
																														}
																														if (
																															(CAR(
																																	((obj_t)
																																		BgL_carzd21080zd2_1126))
																																== BINT(88L)))
																															{	/* Jas/peep.scm 222 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_carzd21080zd2_1126))))
																																	{	/* Jas/peep.scm 222 */
																																		{	/* Jas/peep.scm 211 */
																																			obj_t
																																				BgL_tmpz00_3965;
																																			BgL_tmpz00_3965
																																				=
																																				CDR(CDR
																																				(CDR
																																					(BgL_lz00_496)));
																																			SET_CDR
																																				(BgL_lz00_496,
																																				BgL_tmpz00_3965);
																																		}
																																		{

																																			goto
																																				BgL_zc3z04anonymousza31547ze3z87_497;
																																		}
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2109zd2_514;
																																	}
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																goto
																																	BgL_tagzd2109zd2_514;
																															}
																													}
																											}
																										else
																											{	/* Jas/peep.scm 222 */
																												goto
																													BgL_tagzd2109zd2_514;
																											}
																									}
																								else
																									{	/* Jas/peep.scm 222 */
																										goto BgL_tagzd2109zd2_514;
																									}
																							}
																						else
																							{	/* Jas/peep.scm 222 */
																								obj_t BgL_carzd21090zd2_1137;

																								BgL_carzd21090zd2_1137 =
																									CAR(((obj_t) BgL_lz00_496));
																								if (PAIRP
																									(BgL_carzd21090zd2_1137))
																									{	/* Jas/peep.scm 222 */
																										if (
																											(CAR
																												(BgL_carzd21090zd2_1137)
																												== BINT(9L)))
																											{	/* Jas/peep.scm 222 */
																												if (NULLP(CDR
																														(BgL_carzd21090zd2_1137)))
																													{	/* Jas/peep.scm 222 */
																														obj_t
																															BgL_carzd21094zd2_1143;
																														{	/* Jas/peep.scm 222 */
																															obj_t
																																BgL_pairz00_2458;
																															BgL_pairz00_2458 =
																																CDR(((obj_t)
																																	BgL_lz00_496));
																															BgL_carzd21094zd2_1143
																																=
																																CAR
																																(BgL_pairz00_2458);
																														}
																														if (
																															(CAR(
																																	((obj_t)
																																		BgL_carzd21094zd2_1143))
																																== BINT(136L)))
																															{	/* Jas/peep.scm 222 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_carzd21094zd2_1143))))
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2107zd2_512;
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2109zd2_514;
																																	}
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																goto
																																	BgL_tagzd2109zd2_514;
																															}
																													}
																												else
																													{	/* Jas/peep.scm 222 */
																														goto
																															BgL_tagzd2109zd2_514;
																													}
																											}
																										else
																											{	/* Jas/peep.scm 222 */
																												if (
																													(CAR(
																															((obj_t)
																																BgL_carzd21090zd2_1137))
																														== BINT(10L)))
																													{	/* Jas/peep.scm 222 */
																														if (NULLP(CDR(
																																	((obj_t)
																																		BgL_carzd21090zd2_1137))))
																															{	/* Jas/peep.scm 222 */
																																obj_t
																																	BgL_carzd21102zd2_1157;
																																{	/* Jas/peep.scm 222 */
																																	obj_t
																																		BgL_pairz00_2465;
																																	BgL_pairz00_2465
																																		=
																																		CDR(((obj_t)
																																			BgL_lz00_496));
																																	BgL_carzd21102zd2_1157
																																		=
																																		CAR
																																		(BgL_pairz00_2465);
																																}
																																if (
																																	(CAR(
																																			((obj_t)
																																				BgL_carzd21102zd2_1157))
																																		==
																																		BINT(136L)))
																																	{	/* Jas/peep.scm 222 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_carzd21102zd2_1157))))
																																			{	/* Jas/peep.scm 222 */
																																				goto
																																					BgL_tagzd2108zd2_513;
																																			}
																																		else
																																			{	/* Jas/peep.scm 222 */
																																				goto
																																					BgL_tagzd2109zd2_514;
																																			}
																																	}
																																else
																																	{	/* Jas/peep.scm 222 */
																																		goto
																																			BgL_tagzd2109zd2_514;
																																	}
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																goto
																																	BgL_tagzd2109zd2_514;
																															}
																													}
																												else
																													{	/* Jas/peep.scm 222 */
																														goto
																															BgL_tagzd2109zd2_514;
																													}
																											}
																									}
																								else
																									{	/* Jas/peep.scm 222 */
																										goto BgL_tagzd2109zd2_514;
																									}
																							}
																					}
																				}
																		}
																	else
																		{	/* Jas/peep.scm 222 */
																			goto BgL_tagzd2109zd2_514;
																		}
																}
															else
																{	/* Jas/peep.scm 222 */
																	goto BgL_tagzd2109zd2_514;
																}
														}
												}
										}
									else
										{	/* Jas/peep.scm 222 */
											if (PAIRP(BgL_cdrzd2123zd2_519))
												{	/* Jas/peep.scm 222 */
													obj_t BgL_carzd21133zd2_1191;
													obj_t BgL_cdrzd21134zd2_1192;

													BgL_carzd21133zd2_1191 = CAR(BgL_cdrzd2123zd2_519);
													BgL_cdrzd21134zd2_1192 = CDR(BgL_cdrzd2123zd2_519);
													if (PAIRP(BgL_carzd21133zd2_1191))
														{	/* Jas/peep.scm 222 */
															if ((CAR(BgL_carzd21133zd2_1191) == BINT(4L)))
																{	/* Jas/peep.scm 222 */
																	if (NULLP(CDR(BgL_carzd21133zd2_1191)))
																		{	/* Jas/peep.scm 222 */
																			if (PAIRP(BgL_cdrzd21134zd2_1192))
																				{	/* Jas/peep.scm 222 */
																					obj_t BgL_carzd21138zd2_1199;

																					BgL_carzd21138zd2_1199 =
																						CAR(BgL_cdrzd21134zd2_1192);
																					if (PAIRP(BgL_carzd21138zd2_1199))
																						{	/* Jas/peep.scm 222 */
																							obj_t BgL_cdrzd21142zd2_1201;

																							BgL_cdrzd21142zd2_1201 =
																								CDR(BgL_carzd21138zd2_1199);
																							if (
																								(CAR(BgL_carzd21138zd2_1199) ==
																									BINT(153L)))
																								{	/* Jas/peep.scm 222 */
																									if (PAIRP
																										(BgL_cdrzd21142zd2_1201))
																										{	/* Jas/peep.scm 222 */
																											if (NULLP(CDR
																													(BgL_cdrzd21142zd2_1201)))
																												{	/* Jas/peep.scm 222 */
																													{	/* Jas/peep.scm 205 */
																														obj_t
																															BgL_tmpz00_4042;
																														BgL_tmpz00_4042 =
																															CDR(CDR(CDR
																																(BgL_lz00_496)));
																														SET_CDR
																															(BgL_lz00_496,
																															BgL_tmpz00_4042);
																													}
																													{

																														goto
																															BgL_zc3z04anonymousza31547ze3z87_497;
																													}
																												}
																											else
																												{	/* Jas/peep.scm 222 */
																													goto
																														BgL_tagzd2109zd2_514;
																												}
																										}
																									else
																										{	/* Jas/peep.scm 222 */
																											goto BgL_tagzd2109zd2_514;
																										}
																								}
																							else
																								{	/* Jas/peep.scm 222 */
																									goto BgL_tagzd2109zd2_514;
																								}
																						}
																					else
																						{	/* Jas/peep.scm 222 */
																							goto BgL_tagzd2109zd2_514;
																						}
																				}
																			else
																				{	/* Jas/peep.scm 222 */
																					goto BgL_tagzd2109zd2_514;
																				}
																		}
																	else
																		{	/* Jas/peep.scm 222 */
																			goto BgL_tagzd2109zd2_514;
																		}
																}
															else
																{	/* Jas/peep.scm 222 */
																	obj_t BgL_cdrzd21191zd2_1213;

																	BgL_cdrzd21191zd2_1213 =
																		CDR(((obj_t) BgL_cdrzd2123zd2_519));
																	{	/* Jas/peep.scm 222 */
																		bool_t BgL_test2857z00_4049;

																		{	/* Jas/peep.scm 222 */
																			obj_t BgL_tmpz00_4050;

																			{	/* Jas/peep.scm 222 */
																				obj_t BgL_pairz00_2497;

																				BgL_pairz00_2497 =
																					CAR(((obj_t) BgL_cdrzd2123zd2_519));
																				BgL_tmpz00_4050 = CAR(BgL_pairz00_2497);
																			}
																			BgL_test2857z00_4049 =
																				(BgL_tmpz00_4050 == BINT(178L));
																		}
																		if (BgL_test2857z00_4049)
																			{	/* Jas/peep.scm 222 */
																				if (PAIRP(BgL_cdrzd21191zd2_1213))
																					{	/* Jas/peep.scm 222 */
																						obj_t BgL_carzd21194zd2_1218;

																						BgL_carzd21194zd2_1218 =
																							CAR(BgL_cdrzd21191zd2_1213);
																						if (PAIRP(BgL_carzd21194zd2_1218))
																							{	/* Jas/peep.scm 222 */
																								if (
																									(CAR(BgL_carzd21194zd2_1218)
																										== BINT(87L)))
																									{	/* Jas/peep.scm 222 */
																										if (NULLP(CDR
																												(BgL_carzd21194zd2_1218)))
																											{	/* Jas/peep.scm 222 */
																												{	/* Jas/peep.scm 208 */
																													obj_t BgL_tmpz00_4068;

																													BgL_tmpz00_4068 =
																														CDR(CDR(CDR
																															(BgL_lz00_496)));
																													SET_CDR(BgL_lz00_496,
																														BgL_tmpz00_4068);
																												}
																												{

																													goto
																														BgL_zc3z04anonymousza31547ze3z87_497;
																												}
																											}
																										else
																											{	/* Jas/peep.scm 222 */
																												goto
																													BgL_tagzd2109zd2_514;
																											}
																									}
																								else
																									{	/* Jas/peep.scm 222 */
																										obj_t
																											BgL_carzd21212zd2_1226;
																										BgL_carzd21212zd2_1226 =
																											CAR(((obj_t)
																												BgL_cdrzd21191zd2_1213));
																										if ((CAR(((obj_t)
																														BgL_carzd21212zd2_1226))
																												== BINT(88L)))
																											{	/* Jas/peep.scm 222 */
																												if (NULLP(CDR(
																															((obj_t)
																																BgL_carzd21212zd2_1226))))
																													{	/* Jas/peep.scm 222 */
																														{	/* Jas/peep.scm 211 */
																															obj_t
																																BgL_tmpz00_4084;
																															BgL_tmpz00_4084 =
																																CDR(CDR(CDR
																																	(BgL_lz00_496)));
																															SET_CDR
																																(BgL_lz00_496,
																																BgL_tmpz00_4084);
																														}
																														{

																															goto
																																BgL_zc3z04anonymousza31547ze3z87_497;
																														}
																													}
																												else
																													{	/* Jas/peep.scm 222 */
																														goto
																															BgL_tagzd2109zd2_514;
																													}
																											}
																										else
																											{	/* Jas/peep.scm 222 */
																												goto
																													BgL_tagzd2109zd2_514;
																											}
																									}
																							}
																						else
																							{	/* Jas/peep.scm 222 */
																								goto BgL_tagzd2109zd2_514;
																							}
																					}
																				else
																					{	/* Jas/peep.scm 222 */
																						goto BgL_tagzd2109zd2_514;
																					}
																			}
																		else
																			{	/* Jas/peep.scm 222 */
																				obj_t BgL_carzd21225zd2_1237;

																				BgL_carzd21225zd2_1237 =
																					CAR(((obj_t) BgL_lz00_496));
																				if (PAIRP(BgL_carzd21225zd2_1237))
																					{	/* Jas/peep.scm 222 */
																						if (
																							(CAR(BgL_carzd21225zd2_1237) ==
																								BINT(9L)))
																							{	/* Jas/peep.scm 222 */
																								if (NULLP(CDR
																										(BgL_carzd21225zd2_1237)))
																									{	/* Jas/peep.scm 222 */
																										obj_t
																											BgL_carzd21229zd2_1243;
																										BgL_carzd21229zd2_1243 =
																											CAR(((obj_t)
																												BgL_cdrzd2123zd2_519));
																										if ((CAR(((obj_t)
																														BgL_carzd21229zd2_1243))
																												== BINT(136L)))
																											{	/* Jas/peep.scm 222 */
																												if (NULLP(CDR(
																															((obj_t)
																																BgL_carzd21229zd2_1243))))
																													{	/* Jas/peep.scm 222 */
																														goto
																															BgL_tagzd2107zd2_512;
																													}
																												else
																													{	/* Jas/peep.scm 222 */
																														goto
																															BgL_tagzd2109zd2_514;
																													}
																											}
																										else
																											{	/* Jas/peep.scm 222 */
																												goto
																													BgL_tagzd2109zd2_514;
																											}
																									}
																								else
																									{	/* Jas/peep.scm 222 */
																										goto BgL_tagzd2109zd2_514;
																									}
																							}
																						else
																							{	/* Jas/peep.scm 222 */
																								if (
																									(CAR(
																											((obj_t)
																												BgL_carzd21225zd2_1237))
																										== BINT(10L)))
																									{	/* Jas/peep.scm 222 */
																										if (NULLP(CDR(
																													((obj_t)
																														BgL_carzd21225zd2_1237))))
																											{	/* Jas/peep.scm 222 */
																												obj_t
																													BgL_carzd21237zd2_1257;
																												{	/* Jas/peep.scm 222 */
																													obj_t
																														BgL_pairz00_2535;
																													BgL_pairz00_2535 =
																														CDR(((obj_t)
																															BgL_lz00_496));
																													BgL_carzd21237zd2_1257
																														=
																														CAR
																														(BgL_pairz00_2535);
																												}
																												if (
																													(CAR(
																															((obj_t)
																																BgL_carzd21237zd2_1257))
																														== BINT(136L)))
																													{	/* Jas/peep.scm 222 */
																														if (NULLP(CDR(
																																	((obj_t)
																																		BgL_carzd21237zd2_1257))))
																															{	/* Jas/peep.scm 222 */
																																goto
																																	BgL_tagzd2108zd2_513;
																															}
																														else
																															{	/* Jas/peep.scm 222 */
																																goto
																																	BgL_tagzd2109zd2_514;
																															}
																													}
																												else
																													{	/* Jas/peep.scm 222 */
																														goto
																															BgL_tagzd2109zd2_514;
																													}
																											}
																										else
																											{	/* Jas/peep.scm 222 */
																												goto
																													BgL_tagzd2109zd2_514;
																											}
																									}
																								else
																									{	/* Jas/peep.scm 222 */
																										goto BgL_tagzd2109zd2_514;
																									}
																							}
																					}
																				else
																					{	/* Jas/peep.scm 222 */
																						goto BgL_tagzd2109zd2_514;
																					}
																			}
																	}
																}
														}
													else
														{	/* Jas/peep.scm 222 */
															goto BgL_tagzd2109zd2_514;
														}
												}
											else
												{	/* Jas/peep.scm 222 */
													goto BgL_tagzd2109zd2_514;
												}
										}
								}
							else
								{	/* Jas/peep.scm 222 */
									goto BgL_tagzd2109zd2_514;
								}
						}
				}
			}
		}

	}



/* removebranch */
	obj_t BGl_removebranchz00zzjas_peepz00(obj_t BgL_codez00_23)
	{
		{	/* Jas/peep.scm 228 */
			{	/* Jas/peep.scm 229 */
				struct bgl_cell BgL_box2873_2665z00;
				obj_t BgL_donez00_2665;

				BgL_donez00_2665 = MAKE_CELL_STACK(BNIL, BgL_box2873_2665z00);
				return
					BGl_walkze70ze7zzjas_peepz00(BgL_codez00_23, BgL_donez00_2665,
					CNST_TABLE_REF(7), BgL_codez00_23);
			}
		}

	}



/* <@anonymous:2450>~0 */
	obj_t BGl_zc3z04anonymousza32450ze3ze70z60zzjas_peepz00(obj_t
		BgL_head1211z00_2657, obj_t BgL_l1209z00_1369, obj_t BgL_tail1212z00_1370)
	{
		{	/* Jas/peep.scm 253 */
		BGl_zc3z04anonymousza32450ze3ze70z60zzjas_peepz00:
			if (NULLP(BgL_l1209z00_1369))
				{	/* Jas/peep.scm 253 */
					return CDR(BgL_head1211z00_2657);
				}
			else
				{	/* Jas/peep.scm 253 */
					obj_t BgL_newtail1213z00_1373;

					{	/* Jas/peep.scm 253 */
						obj_t BgL_arg2453z00_1375;

						{	/* Jas/peep.scm 253 */
							obj_t BgL_xz00_1376;

							BgL_xz00_1376 = CAR(((obj_t) BgL_l1209z00_1369));
							BgL_arg2453z00_1375 =
								BGl_appendzd221011zd2zzjas_peepz00(BgL_xz00_1376, BNIL);
						}
						BgL_newtail1213z00_1373 =
							MAKE_YOUNG_PAIR(BgL_arg2453z00_1375, BNIL);
					}
					SET_CDR(BgL_tail1212z00_1370, BgL_newtail1213z00_1373);
					{	/* Jas/peep.scm 253 */
						obj_t BgL_arg2452z00_1374;

						BgL_arg2452z00_1374 = CDR(((obj_t) BgL_l1209z00_1369));
						{
							obj_t BgL_tail1212z00_4145;
							obj_t BgL_l1209z00_4144;

							BgL_l1209z00_4144 = BgL_arg2452z00_1374;
							BgL_tail1212z00_4145 = BgL_newtail1213z00_1373;
							BgL_tail1212z00_1370 = BgL_tail1212z00_4145;
							BgL_l1209z00_1369 = BgL_l1209z00_4144;
							goto BGl_zc3z04anonymousza32450ze3ze70z60zzjas_peepz00;
						}
					}
				}
		}

	}



/* collect~0 */
	obj_t BGl_collectze70ze7zzjas_peepz00(obj_t BgL_lz00_1316,
		obj_t BgL_fromz00_1317, obj_t BgL_rz00_1318, long BgL_nz00_1319)
	{
		{	/* Jas/peep.scm 235 */
		BGl_collectze70ze7zzjas_peepz00:
			{	/* Jas/peep.scm 231 */
				bool_t BgL_test2875z00_4146;

				if (NULLP(BgL_lz00_1316))
					{	/* Jas/peep.scm 231 */
						BgL_test2875z00_4146 = ((bool_t) 1);
					}
				else
					{	/* Jas/peep.scm 231 */
						if ((BgL_nz00_1319 > 5L))
							{	/* Jas/peep.scm 231 */
								BgL_test2875z00_4146 = ((bool_t) 1);
							}
						else
							{	/* Jas/peep.scm 231 */
								BgL_test2875z00_4146 =
									(CAR(((obj_t) BgL_lz00_1316)) == BgL_fromz00_1317);
							}
					}
				if (BgL_test2875z00_4146)
					{	/* Jas/peep.scm 231 */
						return BFALSE;
					}
				else
					{	/* Jas/peep.scm 232 */
						bool_t BgL_test2878z00_4154;

						{	/* Jas/peep.scm 232 */
							obj_t BgL_tmpz00_4155;

							BgL_tmpz00_4155 = CAR(((obj_t) BgL_lz00_1316));
							BgL_test2878z00_4154 = SYMBOLP(BgL_tmpz00_4155);
						}
						if (BgL_test2878z00_4154)
							{	/* Jas/peep.scm 232 */
								obj_t BgL_arg2417z00_1327;

								BgL_arg2417z00_1327 = CDR(((obj_t) BgL_lz00_1316));
								{
									obj_t BgL_lz00_4161;

									BgL_lz00_4161 = BgL_arg2417z00_1327;
									BgL_lz00_1316 = BgL_lz00_4161;
									goto BGl_collectze70ze7zzjas_peepz00;
								}
							}
						else
							{	/* Jas/peep.scm 232 */
								if (BGl_hugezf3zf3zzjas_peepz00(CAR(((obj_t) BgL_lz00_1316))))
									{	/* Jas/peep.scm 233 */
										return BFALSE;
									}
								else
									{	/* Jas/peep.scm 233 */
										if (CBOOL(BGl_nocontzf3zf3zzjas_peepz00(CAR(
														((obj_t) BgL_lz00_1316)))))
											{	/* Jas/peep.scm 234 */
												obj_t BgL_arg2423z00_1332;

												{	/* Jas/peep.scm 234 */
													obj_t BgL_arg2424z00_1333;

													BgL_arg2424z00_1333 = CAR(((obj_t) BgL_lz00_1316));
													BgL_arg2423z00_1332 =
														MAKE_YOUNG_PAIR(BgL_arg2424z00_1333, BgL_rz00_1318);
												}
												return bgl_reverse_bang(BgL_arg2423z00_1332);
											}
										else
											{	/* Jas/peep.scm 235 */
												obj_t BgL_arg2425z00_1334;
												obj_t BgL_arg2426z00_1335;
												long BgL_arg2428z00_1336;

												BgL_arg2425z00_1334 = CDR(((obj_t) BgL_lz00_1316));
												{	/* Jas/peep.scm 235 */
													obj_t BgL_arg2429z00_1337;

													BgL_arg2429z00_1337 = CAR(((obj_t) BgL_lz00_1316));
													BgL_arg2426z00_1335 =
														MAKE_YOUNG_PAIR(BgL_arg2429z00_1337, BgL_rz00_1318);
												}
												BgL_arg2428z00_1336 = (BgL_nz00_1319 + 1L);
												{
													long BgL_nz00_4183;
													obj_t BgL_rz00_4182;
													obj_t BgL_lz00_4181;

													BgL_lz00_4181 = BgL_arg2425z00_1334;
													BgL_rz00_4182 = BgL_arg2426z00_1335;
													BgL_nz00_4183 = BgL_arg2428z00_1336;
													BgL_nz00_1319 = BgL_nz00_4183;
													BgL_rz00_1318 = BgL_rz00_4182;
													BgL_lz00_1316 = BgL_lz00_4181;
													goto BGl_collectze70ze7zzjas_peepz00;
												}
											}
									}
							}
					}
			}
		}

	}



/* walk~0 */
	obj_t BGl_walkze70ze7zzjas_peepz00(obj_t BgL_codez00_2659,
		obj_t BgL_donez00_2658, obj_t BgL_fromz00_1352, obj_t BgL_lz00_1353)
	{
		{	/* Jas/peep.scm 258 */
		BGl_walkze70ze7zzjas_peepz00:
			if (NULLP(BgL_lz00_1353))
				{	/* Jas/peep.scm 244 */
					return CNST_TABLE_REF(8);
				}
			else
				{	/* Jas/peep.scm 245 */
					bool_t BgL_test2882z00_4187;

					{	/* Jas/peep.scm 245 */
						obj_t BgL_tmpz00_4188;

						BgL_tmpz00_4188 = CAR(((obj_t) BgL_lz00_1353));
						BgL_test2882z00_4187 = SYMBOLP(BgL_tmpz00_4188);
					}
					if (BgL_test2882z00_4187)
						{	/* Jas/peep.scm 246 */
							obj_t BgL_arg2445z00_1358;
							obj_t BgL_arg2446z00_1359;

							BgL_arg2445z00_1358 = CAR(((obj_t) BgL_lz00_1353));
							BgL_arg2446z00_1359 = CDR(((obj_t) BgL_lz00_1353));
							BGL_TAIL return
								BGl_walkzd2fromze70z35zzjas_peepz00(BgL_donez00_2658,
								BgL_codez00_2659, BgL_arg2445z00_1358, BgL_arg2446z00_1359);
						}
					else
						{	/* Jas/peep.scm 247 */
							bool_t BgL_test2883z00_4197;

							{	/* Jas/peep.scm 39 */
								long BgL_tmpz00_4198;

								{	/* Jas/peep.scm 38 */
									obj_t BgL_pairz00_2561;

									BgL_pairz00_2561 = CAR(((obj_t) BgL_lz00_1353));
									BgL_tmpz00_4198 = (long) CINT(CAR(BgL_pairz00_2561));
								}
								BgL_test2883z00_4197 = (BgL_tmpz00_4198 == 167L);
							}
							if (BgL_test2883z00_4197)
								{	/* Jas/peep.scm 248 */
									obj_t BgL_labz00_1362;
									obj_t BgL_nextz00_1363;

									{	/* Jas/peep.scm 248 */
										obj_t BgL_pairz00_2568;

										{	/* Jas/peep.scm 248 */
											obj_t BgL_pairz00_2567;

											BgL_pairz00_2567 = CAR(((obj_t) BgL_lz00_1353));
											BgL_pairz00_2568 = CDR(BgL_pairz00_2567);
										}
										BgL_labz00_1362 = CAR(BgL_pairz00_2568);
									}
									BgL_nextz00_1363 = CDR(((obj_t) BgL_lz00_1353));
									BGl_walkzd2atzd2labze70ze7zzjas_peepz00(BgL_codez00_2659,
										BgL_donez00_2658, BgL_labz00_1362);
									{	/* Jas/peep.scm 251 */
										obj_t BgL_dupz00_1364;

										BgL_dupz00_1364 =
											BGl_collectze70ze7zzjas_peepz00
											(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_labz00_1362,
												BgL_codez00_2659), BgL_fromz00_1352, BNIL, 0L);
										if (CBOOL(BgL_dupz00_1364))
											{	/* Jas/peep.scm 252 */
												{	/* Jas/peep.scm 253 */
													obj_t BgL_l1209z00_1365;

													BgL_l1209z00_1365 = BgL_dupz00_1364;
													if (NULLP(BgL_l1209z00_1365))
														{	/* Jas/peep.scm 253 */
															BgL_dupz00_1364 = BNIL;
														}
													else
														{	/* Jas/peep.scm 253 */
															obj_t BgL_head1211z00_1367;

															BgL_head1211z00_1367 =
																MAKE_YOUNG_PAIR(BNIL, BNIL);
															BgL_dupz00_1364 =
																BGl_zc3z04anonymousza32450ze3ze70z60zzjas_peepz00
																(BgL_head1211z00_1367, BgL_l1209z00_1365,
																BgL_head1211z00_1367);
														}
												}
												{	/* Jas/peep.scm 255 */
													obj_t BgL_arg2455z00_1378;

													BgL_arg2455z00_1378 = CAR(BgL_dupz00_1364);
													{	/* Jas/peep.scm 255 */
														obj_t BgL_tmpz00_4220;

														BgL_tmpz00_4220 = ((obj_t) BgL_lz00_1353);
														SET_CAR(BgL_tmpz00_4220, BgL_arg2455z00_1378);
													}
												}
												{	/* Jas/peep.scm 256 */
													obj_t BgL_arg2456z00_1379;

													BgL_arg2456z00_1379 =
														BGl_appendzd221011zd2zzjas_peepz00(CDR
														(BgL_dupz00_1364), BgL_nextz00_1363);
													{	/* Jas/peep.scm 256 */
														obj_t BgL_tmpz00_4225;

														BgL_tmpz00_4225 = ((obj_t) BgL_lz00_1353);
														SET_CDR(BgL_tmpz00_4225, BgL_arg2456z00_1379);
													}
												}
											}
										else
											{	/* Jas/peep.scm 252 */
												BFALSE;
											}
										{
											obj_t BgL_lz00_4228;

											BgL_lz00_4228 = BgL_nextz00_1363;
											BgL_lz00_1353 = BgL_lz00_4228;
											goto BGl_walkze70ze7zzjas_peepz00;
										}
									}
								}
							else
								{	/* Jas/peep.scm 258 */
									obj_t BgL_arg2459z00_1382;

									BgL_arg2459z00_1382 = CDR(((obj_t) BgL_lz00_1353));
									{
										obj_t BgL_lz00_4231;

										BgL_lz00_4231 = BgL_arg2459z00_1382;
										BgL_lz00_1353 = BgL_lz00_4231;
										goto BGl_walkze70ze7zzjas_peepz00;
									}
								}
						}
				}
		}

	}



/* walk-from~0 */
	obj_t BGl_walkzd2fromze70z35zzjas_peepz00(obj_t BgL_donez00_2661,
		obj_t BgL_codez00_2660, obj_t BgL_fromz00_1348, obj_t BgL_lz00_1349)
	{
		{	/* Jas/peep.scm 242 */
			if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_fromz00_1348,
						((obj_t) ((obj_t) CELL_REF(BgL_donez00_2661))))))
				{	/* Jas/peep.scm 239 */
					return CNST_TABLE_REF(8);
				}
			else
				{	/* Jas/peep.scm 239 */
					{	/* Jas/peep.scm 241 */
						obj_t BgL_auxz00_2662;

						BgL_auxz00_2662 =
							MAKE_YOUNG_PAIR(BgL_fromz00_1348,
							((obj_t) ((obj_t) CELL_REF(BgL_donez00_2661))));
						CELL_SET(BgL_donez00_2661, BgL_auxz00_2662);
					}
					BGL_TAIL return
						BGl_walkze70ze7zzjas_peepz00(BgL_codez00_2660, BgL_donez00_2661,
						BgL_fromz00_1348, BgL_lz00_1349);
				}
		}

	}



/* walk-at-lab~0 */
	obj_t BGl_walkzd2atzd2labze70ze7zzjas_peepz00(obj_t BgL_codez00_2664,
		obj_t BgL_donez00_2663, obj_t BgL_labz00_1344)
	{
		{	/* Jas/peep.scm 237 */
			{	/* Jas/peep.scm 237 */
				obj_t BgL_arg2437z00_1346;

				BgL_arg2437z00_1346 =
					CDR(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_labz00_1344,
						BgL_codez00_2664));
				return BGl_walkzd2fromze70z35zzjas_peepz00(BgL_donez00_2663,
					BgL_codez00_2664, BgL_labz00_1344, BgL_arg2437z00_1346);
			}
		}

	}



/* removesequence */
	obj_t BGl_removesequencez00zzjas_peepz00(obj_t BgL_codez00_24)
	{
		{	/* Jas/peep.scm 264 */
			{
				obj_t BgL_codez00_1458;

				{	/* Jas/peep.scm 294 */
					obj_t BgL_envz00_1392;

					BgL_codez00_1458 = BgL_codez00_24;
					{
						obj_t BgL_codez00_1462;
						obj_t BgL_accz00_1463;

						BgL_codez00_1462 = BgL_codez00_1458;
						BgL_accz00_1463 = BNIL;
					BgL_zc3z04anonymousza32514ze3z87_1464:
						if (NULLP(BgL_codez00_1462))
							{	/* Jas/peep.scm 268 */
								BgL_envz00_1392 = BgL_accz00_1463;
							}
						else
							{	/* Jas/peep.scm 269 */
								bool_t BgL_test2888z00_4245;

								{	/* Jas/peep.scm 269 */
									obj_t BgL_tmpz00_4246;

									BgL_tmpz00_4246 = CAR(((obj_t) BgL_codez00_1462));
									BgL_test2888z00_4245 = SYMBOLP(BgL_tmpz00_4246);
								}
								if (BgL_test2888z00_4245)
									{	/* Jas/peep.scm 270 */
										obj_t BgL_arg2518z00_1468;
										obj_t BgL_arg2519z00_1469;

										BgL_arg2518z00_1468 = CDR(((obj_t) BgL_codez00_1462));
										{	/* Jas/peep.scm 270 */
											obj_t BgL_arg2521z00_1470;

											{	/* Jas/peep.scm 270 */
												obj_t BgL_arg2524z00_1471;

												BgL_arg2524z00_1471 = CAR(((obj_t) BgL_codez00_1462));
												BgL_arg2521z00_1470 =
													MAKE_YOUNG_PAIR(BgL_arg2524z00_1471, BINT(0L));
											}
											BgL_arg2519z00_1469 =
												MAKE_YOUNG_PAIR(BgL_arg2521z00_1470, BgL_accz00_1463);
										}
										{
											obj_t BgL_accz00_4258;
											obj_t BgL_codez00_4257;

											BgL_codez00_4257 = BgL_arg2518z00_1468;
											BgL_accz00_4258 = BgL_arg2519z00_1469;
											BgL_accz00_1463 = BgL_accz00_4258;
											BgL_codez00_1462 = BgL_codez00_4257;
											goto BgL_zc3z04anonymousza32514ze3z87_1464;
										}
									}
								else
									{	/* Jas/peep.scm 272 */
										obj_t BgL_arg2525z00_1472;

										BgL_arg2525z00_1472 = CDR(((obj_t) BgL_codez00_1462));
										{
											obj_t BgL_codez00_4261;

											BgL_codez00_4261 = BgL_arg2525z00_1472;
											BgL_codez00_1462 = BgL_codez00_4261;
											goto BgL_zc3z04anonymousza32514ze3z87_1464;
										}
									}
							}
					}
					{
						obj_t BgL_prevz00_1403;
						obj_t BgL_codez00_1404;
						obj_t BgL_prevz00_1424;
						obj_t BgL_codez00_1425;

						{
							obj_t BgL_l1224z00_1396;

							BgL_l1224z00_1396 = BgL_codez00_24;
						BgL_zc3z04anonymousza32462ze3z87_1397:
							if (PAIRP(BgL_l1224z00_1396))
								{	/* Jas/peep.scm 313 */
									BGl_countzd2insze70z35zzjas_peepz00(CAR(BgL_l1224z00_1396),
										BgL_envz00_1392);
									{
										obj_t BgL_l1224z00_4266;

										BgL_l1224z00_4266 = CDR(BgL_l1224z00_1396);
										BgL_l1224z00_1396 = BgL_l1224z00_4266;
										goto BgL_zc3z04anonymousza32462ze3z87_1397;
									}
								}
							else
								{	/* Jas/peep.scm 313 */
									((bool_t) 1);
								}
						}
						{	/* Jas/peep.scm 314 */
							obj_t BgL_arg2466z00_1402;

							BgL_arg2466z00_1402 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_codez00_24);
							BgL_prevz00_1424 = BgL_arg2466z00_1402;
							BgL_codez00_1425 = BgL_codez00_24;
						BgL_zc3z04anonymousza32484ze3z87_1426:
							if (NULLP(BgL_codez00_1425))
								{	/* Jas/peep.scm 305 */
									return CNST_TABLE_REF(2);
								}
							else
								{	/* Jas/peep.scm 306 */
									bool_t BgL_test2891z00_4273;

									{	/* Jas/peep.scm 306 */
										bool_t BgL_test2892z00_4274;

										{	/* Jas/peep.scm 306 */
											obj_t BgL_tmpz00_4275;

											BgL_tmpz00_4275 = CAR(((obj_t) BgL_codez00_1425));
											BgL_test2892z00_4274 = SYMBOLP(BgL_tmpz00_4275);
										}
										if (BgL_test2892z00_4274)
											{	/* Jas/peep.scm 307 */
												obj_t BgL_b1223z00_1451;

												{	/* Jas/peep.scm 307 */
													obj_t BgL_pairz00_2639;

													BgL_pairz00_2639 =
														BGl_assqz00zz__r4_pairs_and_lists_6_3z00(CAR(
															((obj_t) BgL_codez00_1425)), BgL_envz00_1392);
													BgL_b1223z00_1451 = CDR(BgL_pairz00_2639);
												}
												{	/* Jas/peep.scm 307 */

													if (INTEGERP(BgL_b1223z00_1451))
														{	/* Jas/peep.scm 307 */
															BgL_test2891z00_4273 =
																(0L == (long) CINT(BgL_b1223z00_1451));
														}
													else
														{	/* Jas/peep.scm 307 */
															BgL_test2891z00_4273 =
																BGl_2zd3zd3zz__r4_numbers_6_5z00(BINT(0L),
																BgL_b1223z00_1451);
														}
												}
											}
										else
											{	/* Jas/peep.scm 306 */
												BgL_test2891z00_4273 = ((bool_t) 0);
											}
									}
									if (BgL_test2891z00_4273)
										{	/* Jas/peep.scm 306 */
											{	/* Jas/peep.scm 308 */
												obj_t BgL_arg2495z00_1437;

												BgL_arg2495z00_1437 = CDR(((obj_t) BgL_codez00_1425));
												{	/* Jas/peep.scm 308 */
													obj_t BgL_tmpz00_4291;

													BgL_tmpz00_4291 = ((obj_t) BgL_prevz00_1424);
													SET_CDR(BgL_tmpz00_4291, BgL_arg2495z00_1437);
												}
											}
											{	/* Jas/peep.scm 309 */
												obj_t BgL_arg2497z00_1438;

												BgL_arg2497z00_1438 = CDR(((obj_t) BgL_codez00_1425));
												{
													obj_t BgL_codez00_4296;

													BgL_codez00_4296 = BgL_arg2497z00_1438;
													BgL_codez00_1425 = BgL_codez00_4296;
													goto BgL_zc3z04anonymousza32484ze3z87_1426;
												}
											}
										}
									else
										{	/* Jas/peep.scm 310 */
											bool_t BgL_test2894z00_4297;

											{	/* Jas/peep.scm 310 */
												bool_t BgL_test2895z00_4298;

												{	/* Jas/peep.scm 310 */
													obj_t BgL_tmpz00_4299;

													BgL_tmpz00_4299 = CAR(((obj_t) BgL_codez00_1425));
													BgL_test2895z00_4298 = PAIRP(BgL_tmpz00_4299);
												}
												if (BgL_test2895z00_4298)
													{	/* Jas/peep.scm 310 */
														BgL_test2894z00_4297 =
															CBOOL(BGl_nocontzf3zf3zzjas_peepz00(CAR(
																	((obj_t) BgL_codez00_1425))));
													}
												else
													{	/* Jas/peep.scm 310 */
														BgL_test2894z00_4297 = ((bool_t) 0);
													}
											}
											if (BgL_test2894z00_4297)
												{	/* Jas/peep.scm 311 */
													obj_t BgL_arg2505z00_1444;

													BgL_arg2505z00_1444 = CDR(((obj_t) BgL_codez00_1425));
													BgL_prevz00_1403 = BgL_codez00_1425;
													BgL_codez00_1404 = BgL_arg2505z00_1444;
												BgL_zc3z04anonymousza32467ze3z87_1405:
													if (NULLP(BgL_codez00_1404))
														{	/* Jas/peep.scm 297 */
															obj_t BgL_tmpz00_4311;

															BgL_tmpz00_4311 = ((obj_t) BgL_prevz00_1403);
															return SET_CDR(BgL_tmpz00_4311, BNIL);
														}
													else
														{	/* Jas/peep.scm 298 */
															bool_t BgL_test2897z00_4314;

															{	/* Jas/peep.scm 298 */
																obj_t BgL_tmpz00_4315;

																BgL_tmpz00_4315 =
																	CAR(((obj_t) BgL_codez00_1404));
																BgL_test2897z00_4314 = SYMBOLP(BgL_tmpz00_4315);
															}
															if (BgL_test2897z00_4314)
																{	/* Jas/peep.scm 299 */
																	bool_t BgL_test2898z00_4319;

																	{	/* Jas/peep.scm 299 */
																		obj_t BgL_b1221z00_1418;

																		{	/* Jas/peep.scm 299 */
																			obj_t BgL_pairz00_2630;

																			BgL_pairz00_2630 =
																				BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																				(CAR(((obj_t) BgL_codez00_1404)),
																				BgL_envz00_1392);
																			BgL_b1221z00_1418 = CDR(BgL_pairz00_2630);
																		}
																		{	/* Jas/peep.scm 299 */

																			if (INTEGERP(BgL_b1221z00_1418))
																				{	/* Jas/peep.scm 299 */
																					BgL_test2898z00_4319 =
																						(0L ==
																						(long) CINT(BgL_b1221z00_1418));
																				}
																			else
																				{	/* Jas/peep.scm 299 */
																					BgL_test2898z00_4319 =
																						BGl_2zd3zd3zz__r4_numbers_6_5z00
																						(BINT(0L), BgL_b1221z00_1418);
																				}
																		}
																	}
																	if (BgL_test2898z00_4319)
																		{	/* Jas/peep.scm 300 */
																			obj_t BgL_arg2476z00_1415;

																			BgL_arg2476z00_1415 =
																				CDR(((obj_t) BgL_codez00_1404));
																			{
																				obj_t BgL_codez00_4332;

																				BgL_codez00_4332 = BgL_arg2476z00_1415;
																				BgL_codez00_1404 = BgL_codez00_4332;
																				goto
																					BgL_zc3z04anonymousza32467ze3z87_1405;
																			}
																		}
																	else
																		{	/* Jas/peep.scm 299 */
																			{	/* Jas/peep.scm 301 */
																				obj_t BgL_tmpz00_4333;

																				BgL_tmpz00_4333 =
																					((obj_t) BgL_prevz00_1403);
																				SET_CDR(BgL_tmpz00_4333,
																					BgL_codez00_1404);
																			}
																			{	/* Jas/peep.scm 302 */
																				obj_t BgL_arg2479z00_1416;

																				BgL_arg2479z00_1416 =
																					CDR(((obj_t) BgL_codez00_1404));
																				{
																					obj_t BgL_codez00_4339;
																					obj_t BgL_prevz00_4338;

																					BgL_prevz00_4338 = BgL_codez00_1404;
																					BgL_codez00_4339 =
																						BgL_arg2479z00_1416;
																					BgL_codez00_1425 = BgL_codez00_4339;
																					BgL_prevz00_1424 = BgL_prevz00_4338;
																					goto
																						BgL_zc3z04anonymousza32484ze3z87_1426;
																				}
																			}
																		}
																}
															else
																{	/* Jas/peep.scm 303 */
																	obj_t BgL_arg2482z00_1422;

																	BgL_arg2482z00_1422 =
																		CDR(((obj_t) BgL_codez00_1404));
																	{
																		obj_t BgL_codez00_4342;

																		BgL_codez00_4342 = BgL_arg2482z00_1422;
																		BgL_codez00_1404 = BgL_codez00_4342;
																		goto BgL_zc3z04anonymousza32467ze3z87_1405;
																	}
																}
														}
												}
											else
												{	/* Jas/peep.scm 312 */
													obj_t BgL_arg2506z00_1445;

													BgL_arg2506z00_1445 = CDR(((obj_t) BgL_codez00_1425));
													{
														obj_t BgL_codez00_4346;
														obj_t BgL_prevz00_4345;

														BgL_prevz00_4345 = BgL_codez00_1425;
														BgL_codez00_4346 = BgL_arg2506z00_1445;
														BgL_codez00_1425 = BgL_codez00_4346;
														BgL_prevz00_1424 = BgL_prevz00_4345;
														goto BgL_zc3z04anonymousza32484ze3z87_1426;
													}
												}
										}
								}
						}
					}
				}
			}
		}

	}



/* count-lab~0 */
	obj_t BGl_countzd2labze70z35zzjas_peepz00(obj_t BgL_labz00_1475,
		obj_t BgL_envz00_1476)
	{
		{	/* Jas/peep.scm 275 */
			{	/* Jas/peep.scm 274 */
				obj_t BgL_slotz00_1478;

				BgL_slotz00_1478 =
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_labz00_1475,
					BgL_envz00_1476);
				{	/* Jas/peep.scm 275 */
					obj_t BgL_arg2528z00_1479;

					{	/* Jas/peep.scm 275 */
						obj_t BgL_b1215z00_1481;

						BgL_b1215z00_1481 = CDR(((obj_t) BgL_slotz00_1478));
						{	/* Jas/peep.scm 275 */

							if (INTEGERP(BgL_b1215z00_1481))
								{	/* Jas/peep.scm 275 */
									obj_t BgL_za71za7_2584;

									BgL_za71za7_2584 = BINT(1L);
									{	/* Jas/peep.scm 275 */
										obj_t BgL_tmpz00_2586;

										BgL_tmpz00_2586 = BINT(0L);
										if (BGL_ADDFX_OV(BgL_za71za7_2584, BgL_b1215z00_1481,
												BgL_tmpz00_2586))
											{	/* Jas/peep.scm 275 */
												BgL_arg2528z00_1479 =
													bgl_bignum_add(bgl_long_to_bignum(
														(long) CINT(BgL_za71za7_2584)),
													bgl_long_to_bignum((long) CINT(BgL_b1215z00_1481)));
											}
										else
											{	/* Jas/peep.scm 275 */
												BgL_arg2528z00_1479 = BgL_tmpz00_2586;
											}
									}
								}
							else
								{	/* Jas/peep.scm 275 */
									BgL_arg2528z00_1479 =
										BGl_2zb2zb2zz__r4_numbers_6_5z00(BINT(1L),
										BgL_b1215z00_1481);
								}
						}
					}
					{	/* Jas/peep.scm 275 */
						obj_t BgL_tmpz00_4363;

						BgL_tmpz00_4363 = ((obj_t) BgL_slotz00_1478);
						return SET_CDR(BgL_tmpz00_4363, BgL_arg2528z00_1479);
					}
				}
			}
		}

	}



/* count-ins~0 */
	obj_t BGl_countzd2insze70z35zzjas_peepz00(obj_t BgL_insz00_1483,
		obj_t BgL_envz00_1484)
	{
		{	/* Jas/peep.scm 293 */
			{	/* Jas/peep.scm 277 */
				bool_t BgL_test2902z00_4366;

				if (PAIRP(BgL_insz00_1483))
					{	/* Jas/peep.scm 277 */
						BgL_test2902z00_4366 =
							CBOOL(BGl_singlezd2labzf3z21zzjas_peepz00(BgL_insz00_1483));
					}
				else
					{	/* Jas/peep.scm 277 */
						BgL_test2902z00_4366 = ((bool_t) 0);
					}
				if (BgL_test2902z00_4366)
					{	/* Jas/peep.scm 277 */
						BGL_TAIL return
							BGl_countzd2labze70z35zzjas_peepz00(CAR(CDR(BgL_insz00_1483)),
							BgL_envz00_1484);
					}
				else
					{
						obj_t BgL_defz00_1489;
						obj_t BgL_labsz00_1490;
						obj_t BgL_defz00_1492;
						obj_t BgL_tablez00_1493;

						if (PAIRP(BgL_insz00_1483))
							{	/* Jas/peep.scm 279 */
								obj_t BgL_cdrzd21286zd2_1504;

								BgL_cdrzd21286zd2_1504 = CDR(((obj_t) BgL_insz00_1483));
								if ((CAR(((obj_t) BgL_insz00_1483)) == BINT(170L)))
									{	/* Jas/peep.scm 279 */
										if (PAIRP(BgL_cdrzd21286zd2_1504))
											{	/* Jas/peep.scm 279 */
												obj_t BgL_cdrzd21290zd2_1508;

												BgL_cdrzd21290zd2_1508 = CDR(BgL_cdrzd21286zd2_1504);
												if (PAIRP(BgL_cdrzd21290zd2_1508))
													{	/* Jas/peep.scm 279 */
														obj_t BgL_arg2540z00_1510;
														obj_t BgL_arg2542z00_1511;

														BgL_arg2540z00_1510 = CAR(BgL_cdrzd21286zd2_1504);
														BgL_arg2542z00_1511 = CDR(BgL_cdrzd21290zd2_1508);
														{	/* Jas/peep.scm 279 */
															bool_t BgL_tmpz00_4390;

															BgL_defz00_1489 = BgL_arg2540z00_1510;
															BgL_labsz00_1490 = BgL_arg2542z00_1511;
															BGl_countzd2labze70z35zzjas_peepz00
																(BgL_defz00_1489, BgL_envz00_1484);
															{
																obj_t BgL_l1216z00_1547;

																BgL_l1216z00_1547 = BgL_labsz00_1490;
															BgL_zc3z04anonymousza32579ze3z87_1548:
																if (PAIRP(BgL_l1216z00_1547))
																	{	/* Jas/peep.scm 282 */
																		BGl_countzd2labze70z35zzjas_peepz00(CAR
																			(BgL_l1216z00_1547), BgL_envz00_1484);
																		{
																			obj_t BgL_l1216z00_4396;

																			BgL_l1216z00_4396 =
																				CDR(BgL_l1216z00_1547);
																			BgL_l1216z00_1547 = BgL_l1216z00_4396;
																			goto
																				BgL_zc3z04anonymousza32579ze3z87_1548;
																		}
																	}
																else
																	{	/* Jas/peep.scm 282 */
																		BgL_tmpz00_4390 = ((bool_t) 1);
																	}
															}
															return BBOOL(BgL_tmpz00_4390);
														}
													}
												else
													{	/* Jas/peep.scm 279 */
														return BFALSE;
													}
											}
										else
											{	/* Jas/peep.scm 279 */
												return BFALSE;
											}
									}
								else
									{	/* Jas/peep.scm 279 */
										if ((CAR(((obj_t) BgL_insz00_1483)) == BINT(171L)))
											{	/* Jas/peep.scm 279 */
												if (PAIRP(BgL_cdrzd21286zd2_1504))
													{	/* Jas/peep.scm 279 */
														obj_t BgL_arg2546z00_1516;
														obj_t BgL_arg2548z00_1517;

														BgL_arg2546z00_1516 = CAR(BgL_cdrzd21286zd2_1504);
														BgL_arg2548z00_1517 = CDR(BgL_cdrzd21286zd2_1504);
														{	/* Jas/peep.scm 279 */
															bool_t BgL_tmpz00_4408;

															BgL_defz00_1492 = BgL_arg2546z00_1516;
															BgL_tablez00_1493 = BgL_arg2548z00_1517;
															BGl_countzd2labze70z35zzjas_peepz00
																(BgL_defz00_1492, BgL_envz00_1484);
															{
																obj_t BgL_l1218z00_1554;

																BgL_l1218z00_1554 = BgL_tablez00_1493;
															BgL_zc3z04anonymousza32585ze3z87_1555:
																if (PAIRP(BgL_l1218z00_1554))
																	{	/* Jas/peep.scm 285 */
																		{	/* Jas/peep.scm 285 */
																			obj_t BgL_slotz00_1557;

																			BgL_slotz00_1557 = CAR(BgL_l1218z00_1554);
																			{	/* Jas/peep.scm 285 */
																				obj_t BgL_arg2587z00_1558;

																				BgL_arg2587z00_1558 =
																					CDR(((obj_t) BgL_slotz00_1557));
																				BGl_countzd2labze70z35zzjas_peepz00
																					(BgL_arg2587z00_1558,
																					BgL_envz00_1484);
																			}
																		}
																		{
																			obj_t BgL_l1218z00_4416;

																			BgL_l1218z00_4416 =
																				CDR(BgL_l1218z00_1554);
																			BgL_l1218z00_1554 = BgL_l1218z00_4416;
																			goto
																				BgL_zc3z04anonymousza32585ze3z87_1555;
																		}
																	}
																else
																	{	/* Jas/peep.scm 285 */
																		BgL_tmpz00_4408 = ((bool_t) 1);
																	}
															}
															return BBOOL(BgL_tmpz00_4408);
														}
													}
												else
													{	/* Jas/peep.scm 279 */
														return BFALSE;
													}
											}
										else
											{	/* Jas/peep.scm 279 */
												if ((CAR(((obj_t) BgL_insz00_1483)) == BINT(205L)))
													{	/* Jas/peep.scm 279 */
														if (PAIRP(BgL_cdrzd21286zd2_1504))
															{	/* Jas/peep.scm 279 */
																obj_t BgL_cdrzd21368zd2_1522;

																BgL_cdrzd21368zd2_1522 =
																	CDR(BgL_cdrzd21286zd2_1504);
																if (PAIRP(BgL_cdrzd21368zd2_1522))
																	{	/* Jas/peep.scm 279 */
																		obj_t BgL_arg2553z00_1524;
																		obj_t BgL_arg2554z00_1525;

																		BgL_arg2553z00_1524 =
																			CAR(BgL_cdrzd21286zd2_1504);
																		BgL_arg2554z00_1525 =
																			CAR(BgL_cdrzd21368zd2_1522);
																		BGl_countzd2labze70z35zzjas_peepz00
																			(BgL_arg2553z00_1524, BgL_envz00_1484);
																		BGL_TAIL return
																			BGl_countzd2labze70z35zzjas_peepz00
																			(BgL_arg2554z00_1525, BgL_envz00_1484);
																	}
																else
																	{	/* Jas/peep.scm 279 */
																		return BFALSE;
																	}
															}
														else
															{	/* Jas/peep.scm 279 */
																return BFALSE;
															}
													}
												else
													{	/* Jas/peep.scm 279 */
														if ((CAR(((obj_t) BgL_insz00_1483)) == BINT(202L)))
															{	/* Jas/peep.scm 279 */
																if (PAIRP(BgL_cdrzd21286zd2_1504))
																	{	/* Jas/peep.scm 279 */
																		obj_t BgL_cdrzd21398zd2_1530;

																		BgL_cdrzd21398zd2_1530 =
																			CDR(BgL_cdrzd21286zd2_1504);
																		if (PAIRP(BgL_cdrzd21398zd2_1530))
																			{	/* Jas/peep.scm 279 */
																				obj_t BgL_cdrzd21403zd2_1532;

																				BgL_cdrzd21403zd2_1532 =
																					CDR(BgL_cdrzd21398zd2_1530);
																				if (PAIRP(BgL_cdrzd21403zd2_1532))
																					{	/* Jas/peep.scm 279 */
																						obj_t BgL_cdrzd21407zd2_1534;

																						BgL_cdrzd21407zd2_1534 =
																							CDR(BgL_cdrzd21403zd2_1532);
																						if (PAIRP(BgL_cdrzd21407zd2_1534))
																							{	/* Jas/peep.scm 279 */
																								if (NULLP(CDR
																										(BgL_cdrzd21407zd2_1534)))
																									{	/* Jas/peep.scm 279 */
																										obj_t BgL_arg2563z00_1538;
																										obj_t BgL_arg2564z00_1539;
																										obj_t BgL_arg2565z00_1540;

																										BgL_arg2563z00_1538 =
																											CAR
																											(BgL_cdrzd21286zd2_1504);
																										BgL_arg2564z00_1539 =
																											CAR
																											(BgL_cdrzd21398zd2_1530);
																										BgL_arg2565z00_1540 =
																											CAR
																											(BgL_cdrzd21403zd2_1532);
																										BGl_countzd2labze70z35zzjas_peepz00
																											(BgL_arg2563z00_1538,
																											BgL_envz00_1484);
																										BGl_countzd2labze70z35zzjas_peepz00
																											(BgL_arg2564z00_1539,
																											BgL_envz00_1484);
																										BGL_TAIL return
																											BGl_countzd2labze70z35zzjas_peepz00
																											(BgL_arg2565z00_1540,
																											BgL_envz00_1484);
																									}
																								else
																									{	/* Jas/peep.scm 279 */
																										return BFALSE;
																									}
																							}
																						else
																							{	/* Jas/peep.scm 279 */
																								return BFALSE;
																							}
																					}
																				else
																					{	/* Jas/peep.scm 279 */
																						return BFALSE;
																					}
																			}
																		else
																			{	/* Jas/peep.scm 279 */
																				return BFALSE;
																			}
																	}
																else
																	{	/* Jas/peep.scm 279 */
																		return BFALSE;
																	}
															}
														else
															{	/* Jas/peep.scm 279 */
																return BFALSE;
															}
													}
											}
									}
							}
						else
							{	/* Jas/peep.scm 279 */
								return BFALSE;
							}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzjas_peepz00(void)
	{
		{	/* Jas/peep.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzjas_peepz00(void)
	{
		{	/* Jas/peep.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzjas_peepz00(void)
	{
		{	/* Jas/peep.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzjas_peepz00(void)
	{
		{	/* Jas/peep.scm 1 */
			BGl_modulezd2initializa7ationz75zzjas_classfilez00(135094074L,
				BSTRING_TO_STRING(BGl_string2605z00zzjas_peepz00));
			return
				BGl_modulezd2initializa7ationz75zzjas_stackz00(163304304L,
				BSTRING_TO_STRING(BGl_string2605z00zzjas_peepz00));
		}

	}

#ifdef __cplusplus
}
#endif
