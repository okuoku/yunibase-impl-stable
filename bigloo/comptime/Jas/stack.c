/*===========================================================================*/
/*   (Jas/stack.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Jas/stack.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_JAS_STACK_TYPE_DEFINITIONS
#define BGL_JAS_STACK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_jastypez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_codez00;
		obj_t BgL_vectz00;
	}                 *BgL_jastypez00_bglt;

	typedef struct BgL_jasfunz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_codez00;
		obj_t BgL_vectz00;
		struct BgL_jastypez00_bgl *BgL_tretz00;
		obj_t BgL_targsz00;
	}                *BgL_jasfunz00_bglt;

	typedef struct BgL_fieldzd2orzd2methodz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}                             *BgL_fieldzd2orzd2methodz00_bglt;

	typedef struct BgL_fieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}               *BgL_fieldz00_bglt;

	typedef struct BgL_methodz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}                *BgL_methodz00_bglt;

	typedef struct BgL_classfilez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_currentzd2methodzd2;
		obj_t BgL_globalsz00;
		obj_t BgL_poolz00;
		obj_t BgL_poolzd2siza7ez75;
		obj_t BgL_pooledzd2nameszd2;
		obj_t BgL_flagsz00;
		obj_t BgL_mez00;
		obj_t BgL_superz00;
		obj_t BgL_interfacesz00;
		obj_t BgL_fieldsz00;
		obj_t BgL_methodsz00;
		obj_t BgL_attributesz00;
	}                   *BgL_classfilez00_bglt;


#endif													// BGL_JAS_STACK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzjas_stackz00 = BUNSPEC;
	extern obj_t BGl_jaszd2warningzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t, obj_t);
	static obj_t BGl_z62nbpopz62zzjas_stackz00(obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzjas_stackz00(void);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_stkzd2analyseze70z35zzjas_stackz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzjas_stackz00(void);
	static obj_t BGl_objectzd2initzd2zzjas_stackz00(void);
	static obj_t BGl_z62stkzd2analysiszb0zzjas_stackz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_makezd2stkzd2envz00zzjas_stackz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzjas_stackz00(void);
	BGL_IMPORT obj_t bgl_bignum_sub(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	BGL_IMPORT obj_t BGl_zb2zb2zz__r4_numbers_6_5z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzjas_stackz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_classfilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_libz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern int BGl_typezd2siza7ez75zzjas_classfilez00(BgL_jastypez00_bglt);
	static obj_t BGl_za2popza2z00zzjas_stackz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_nbpushz00zzjas_stackz00(obj_t, obj_t);
	static obj_t BGl_za2pushza2z00zzjas_stackz00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zzjas_stackz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzjas_stackz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzjas_stackz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzjas_stackz00(void);
	extern obj_t BGl_jaszd2errorzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_2maxz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stkzd2analysiszd2zzjas_stackz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_maxz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_nbpopz00zzjas_stackz00(obj_t, obj_t);
	static obj_t BGl_z62nbpushz62zzjas_stackz00(obj_t, obj_t, obj_t);
	static obj_t __cnst[4];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_stkzd2analysiszd2envz00zzjas_stackz00,
		BgL_bgl_za762stkza7d2analysi1572z00, BGl_z62stkzd2analysiszb0zzjas_stackz00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1560z00zzjas_stackz00,
		BgL_bgl_string1560za700za7za7j1573za7, "bad cop for pop", 15);
	      DEFINE_STRING(BGl_string1561z00zzjas_stackz00,
		BgL_bgl_string1561za700za7za7j1574za7, "bad cop for push", 16);
	      DEFINE_STRING(BGl_string1562z00zzjas_stackz00,
		BgL_bgl_string1562za700za7za7j1575za7, "stack not empty", 15);
	      DEFINE_STRING(BGl_string1563z00zzjas_stackz00,
		BgL_bgl_string1563za700za7za7j1576za7, "return unreachable", 18);
	      DEFINE_STRING(BGl_string1564z00zzjas_stackz00,
		BgL_bgl_string1564za700za7za7j1577za7, "different stack for label", 25);
	      DEFINE_STRING(BGl_string1565z00zzjas_stackz00,
		BgL_bgl_string1565za700za7za7j1578za7, "empty stack", 11);
	      DEFINE_STRING(BGl_string1566z00zzjas_stackz00,
		BgL_bgl_string1566za700za7za7j1579za7, "jas_stack", 9);
	      DEFINE_STRING(BGl_string1567z00zzjas_stackz00,
		BgL_bgl_string1567za700za7za7j1580za7,
		"#z1 * #(0 1 1 1 1 1 1 1 1 2 2 1 1 1 2 2 1 1 1 1 2 1 2 1 2 1 1 1 1 1 2 2 2 2 1 1 1 1 2 2 2 2 1 1 1 1 1 2 1 2 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 3 4 4 5 6 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 0 2 1 2 1 1 2 1 2 2 1 2 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 * 0 * 0 * * * * 0 1 1 1 1 0 1 1 0 0 * 1 0 0 0 1 0 0 0 0) #(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 2 2 2 2 2 2 2 1 2 1 2 1 1 1 1 1 2 2 2 2 1 1 1 1 2 2 2 2 1 1 1 1 3 4 3 4 3 3 3 3 1 2 1 2 3 2 3 4 2 2 4 2 4 2 4 2 4 2 4 2 4 2 4 2 4 2 4 2 4 1 2 1 2 2 3 2 3 2 3 2 4 2 4 2 4 0 1 1 1 2 2 2 1 1 1 2 2 2 1 1 1 4 2 2 4 4 1 1 1 1 1 1 2 2 2 2 2 2 2 2 0 0 0 1 1 1 2 1 2 1 0 0 * 1 * * * * * 0 0 1 1 1 1 1 1 1 1 * * 1 1 0 0 0 0 0 0) ",
		836);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_nbpushzd2envzd2zzjas_stackz00,
		BgL_bgl_za762nbpushza762za7za7ja1581z00, BGl_z62nbpushz62zzjas_stackz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_nbpopzd2envzd2zzjas_stackz00,
		BgL_bgl_za762nbpopza762za7za7jas1582z00, BGl_z62nbpopz62zzjas_stackz00, 0L,
		BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzjas_stackz00));
		     ADD_ROOT((void *) (&BGl_za2popza2z00zzjas_stackz00));
		     ADD_ROOT((void *) (&BGl_za2pushza2z00zzjas_stackz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzjas_stackz00(long
		BgL_checksumz00_732, char *BgL_fromz00_733)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzjas_stackz00))
				{
					BGl_requirezd2initializa7ationz75zzjas_stackz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzjas_stackz00();
					BGl_libraryzd2moduleszd2initz00zzjas_stackz00();
					BGl_cnstzd2initzd2zzjas_stackz00();
					BGl_importedzd2moduleszd2initz00zzjas_stackz00();
					return BGl_toplevelzd2initzd2zzjas_stackz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzjas_stackz00(void)
	{
		{	/* Jas/stack.scm 1 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"jas_stack");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "jas_stack");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "jas_stack");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "jas_stack");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "jas_stack");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "jas_stack");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"jas_stack");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "jas_stack");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzjas_stackz00(void)
	{
		{	/* Jas/stack.scm 1 */
			{	/* Jas/stack.scm 1 */
				obj_t BgL_cportz00_721;

				{	/* Jas/stack.scm 1 */
					obj_t BgL_stringz00_728;

					BgL_stringz00_728 = BGl_string1567z00zzjas_stackz00;
					{	/* Jas/stack.scm 1 */
						obj_t BgL_startz00_729;

						BgL_startz00_729 = BINT(0L);
						{	/* Jas/stack.scm 1 */
							obj_t BgL_endz00_730;

							BgL_endz00_730 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_728)));
							{	/* Jas/stack.scm 1 */

								BgL_cportz00_721 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_728, BgL_startz00_729, BgL_endz00_730);
				}}}}
				{
					long BgL_iz00_722;

					BgL_iz00_722 = 3L;
				BgL_loopz00_723:
					if ((BgL_iz00_722 == -1L))
						{	/* Jas/stack.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* Jas/stack.scm 1 */
							{	/* Jas/stack.scm 1 */
								obj_t BgL_arg1571z00_724;

								{	/* Jas/stack.scm 1 */

									{	/* Jas/stack.scm 1 */
										obj_t BgL_locationz00_726;

										BgL_locationz00_726 = BBOOL(((bool_t) 0));
										{	/* Jas/stack.scm 1 */

											BgL_arg1571z00_724 =
												BGl_readz00zz__readerz00(BgL_cportz00_721,
												BgL_locationz00_726);
										}
									}
								}
								{	/* Jas/stack.scm 1 */
									int BgL_tmpz00_759;

									BgL_tmpz00_759 = (int) (BgL_iz00_722);
									CNST_TABLE_SET(BgL_tmpz00_759, BgL_arg1571z00_724);
							}}
							{	/* Jas/stack.scm 1 */
								int BgL_auxz00_727;

								BgL_auxz00_727 = (int) ((BgL_iz00_722 - 1L));
								{
									long BgL_iz00_764;

									BgL_iz00_764 = (long) (BgL_auxz00_727);
									BgL_iz00_722 = BgL_iz00_764;
									goto BgL_loopz00_723;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzjas_stackz00(void)
	{
		{	/* Jas/stack.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzjas_stackz00(void)
	{
		{	/* Jas/stack.scm 1 */
			BGl_za2popza2z00zzjas_stackz00 = CNST_TABLE_REF(0);
			return (BGl_za2pushza2z00zzjas_stackz00 = CNST_TABLE_REF(1), BUNSPEC);
		}

	}



/* nbpop */
	BGL_EXPORTED_DEF obj_t BGl_nbpopz00zzjas_stackz00(obj_t BgL_classfilez00_3,
		obj_t BgL_insz00_4)
	{
		{	/* Jas/stack.scm 25 */
			{	/* Jas/stack.scm 26 */
				obj_t BgL_copz00_264;

				BgL_copz00_264 = CAR(((obj_t) BgL_insz00_4));
				{	/* Jas/stack.scm 26 */
					obj_t BgL_argsz00_265;

					BgL_argsz00_265 = CDR(((obj_t) BgL_insz00_4));
					{	/* Jas/stack.scm 26 */
						obj_t BgL_nz00_266;

						{	/* Jas/stack.scm 26 */
							obj_t BgL_vectorz00_585;
							long BgL_kz00_586;

							BgL_vectorz00_585 = CNST_TABLE_REF(0);
							BgL_kz00_586 = (long) CINT(BgL_copz00_264);
							BgL_nz00_266 = VECTOR_REF(BgL_vectorz00_585, BgL_kz00_586);
						}
						{	/* Jas/stack.scm 26 */

							if ((BgL_nz00_266 == CNST_TABLE_REF(2)))
								{

									if (INTEGERP(BgL_copz00_264))
										{	/* Jas/stack.scm 28 */
											switch ((long) CINT(BgL_copz00_264))
												{
												case 179L:
												case 181L:

													{	/* Jas/stack.scm 30 */
														int BgL_nz00_269;

														{	/* Jas/stack.scm 30 */
															obj_t BgL_arg1229z00_272;

															BgL_arg1229z00_272 =
																(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
																		((BgL_fieldzd2orzd2methodz00_bglt)
																			((BgL_fieldz00_bglt)
																				CAR(
																					((obj_t) BgL_argsz00_265))))))->
																BgL_typez00);
															BgL_nz00_269 =
																BGl_typezd2siza7ez75zzjas_classfilez00((
																	(BgL_jastypez00_bglt) BgL_arg1229z00_272));
														}
														switch ((long) CINT(BgL_copz00_264))
															{
															case 179L:

																return BINT(BgL_nz00_269);
																break;
															case 181L:

																return BINT(((long) (BgL_nz00_269) + 1L));
																break;
															default:
																return BUNSPEC;
															}
													}
													break;
												case 182L:
												case 183L:
												case 184L:
												case 185L:

													{	/* Jas/stack.scm 35 */
														BgL_jasfunz00_bglt BgL_i1061z00_274;

														BgL_i1061z00_274 =
															((BgL_jasfunz00_bglt)
															(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
																		((BgL_fieldzd2orzd2methodz00_bglt)
																			((BgL_methodz00_bglt)
																				CAR(
																					((obj_t) BgL_argsz00_265))))))->
																BgL_typez00));
														{	/* Jas/stack.scm 36 */
															obj_t BgL_nz00_275;

															{	/* Jas/stack.scm 36 */
																obj_t BgL_runner1245z00_293;

																{	/* Jas/stack.scm 36 */
																	obj_t BgL_l1207z00_277;

																	BgL_l1207z00_277 =
																		(((BgL_jasfunz00_bglt)
																			COBJECT(BgL_i1061z00_274))->BgL_targsz00);
																	if (NULLP(BgL_l1207z00_277))
																		{	/* Jas/stack.scm 36 */
																			BgL_runner1245z00_293 = BNIL;
																		}
																	else
																		{	/* Jas/stack.scm 36 */
																			obj_t BgL_head1209z00_279;

																			{	/* Jas/stack.scm 36 */
																				int BgL_arg1242z00_291;

																				{	/* Jas/stack.scm 36 */
																					obj_t BgL_arg1244z00_292;

																					BgL_arg1244z00_292 =
																						CAR(((obj_t) BgL_l1207z00_277));
																					BgL_arg1242z00_291 =
																						BGl_typezd2siza7ez75zzjas_classfilez00
																						(((BgL_jastypez00_bglt)
																							BgL_arg1244z00_292));
																				}
																				BgL_head1209z00_279 =
																					MAKE_YOUNG_PAIR(BINT
																					(BgL_arg1242z00_291), BNIL);
																			}
																			{	/* Jas/stack.scm 36 */
																				obj_t BgL_g1214z00_280;

																				BgL_g1214z00_280 =
																					CDR(((obj_t) BgL_l1207z00_277));
																				{
																					obj_t BgL_l1207z00_282;
																					obj_t BgL_tail1210z00_283;

																					BgL_l1207z00_282 = BgL_g1214z00_280;
																					BgL_tail1210z00_283 =
																						BgL_head1209z00_279;
																				BgL_zc3z04anonymousza31233ze3z87_284:
																					if (NULLP(BgL_l1207z00_282))
																						{	/* Jas/stack.scm 36 */
																							BgL_runner1245z00_293 =
																								BgL_head1209z00_279;
																						}
																					else
																						{	/* Jas/stack.scm 36 */
																							obj_t BgL_newtail1211z00_286;

																							{	/* Jas/stack.scm 36 */
																								int BgL_arg1238z00_288;

																								{	/* Jas/stack.scm 36 */
																									obj_t BgL_arg1239z00_289;

																									BgL_arg1239z00_289 =
																										CAR(
																										((obj_t) BgL_l1207z00_282));
																									BgL_arg1238z00_288 =
																										BGl_typezd2siza7ez75zzjas_classfilez00
																										(((BgL_jastypez00_bglt)
																											BgL_arg1239z00_289));
																								}
																								BgL_newtail1211z00_286 =
																									MAKE_YOUNG_PAIR(BINT
																									(BgL_arg1238z00_288), BNIL);
																							}
																							SET_CDR(BgL_tail1210z00_283,
																								BgL_newtail1211z00_286);
																							{	/* Jas/stack.scm 36 */
																								obj_t BgL_arg1236z00_287;

																								BgL_arg1236z00_287 =
																									CDR(
																									((obj_t) BgL_l1207z00_282));
																								{
																									obj_t BgL_tail1210z00_823;
																									obj_t BgL_l1207z00_822;

																									BgL_l1207z00_822 =
																										BgL_arg1236z00_287;
																									BgL_tail1210z00_823 =
																										BgL_newtail1211z00_286;
																									BgL_tail1210z00_283 =
																										BgL_tail1210z00_823;
																									BgL_l1207z00_282 =
																										BgL_l1207z00_822;
																									goto
																										BgL_zc3z04anonymousza31233ze3z87_284;
																								}
																							}
																						}
																				}
																			}
																		}
																}
																BgL_nz00_275 =
																	BGl_zb2zb2zz__r4_numbers_6_5z00
																	(BgL_runner1245z00_293);
															}
															if (((long) CINT(BgL_copz00_264) == 184L))
																{	/* Jas/stack.scm 37 */
																	return BgL_nz00_275;
																}
															else
																{	/* Jas/stack.scm 37 */
																	return ADDFX(BgL_nz00_275, BINT(1L));
																}
														}
													}
													break;
												case 197L:

													{	/* Jas/stack.scm 39 */
														obj_t BgL_pairz00_604;

														BgL_pairz00_604 = CDR(((obj_t) BgL_argsz00_265));
														return CAR(BgL_pairz00_604);
													}
													break;
												default:
												BgL_case_else1057z00_267:
													return
														BGl_jaszd2errorzd2zzjas_classfilez00(
														((BgL_classfilez00_bglt) BgL_classfilez00_3),
														BGl_string1560z00zzjas_stackz00, BgL_copz00_264);
												}
										}
									else
										{	/* Jas/stack.scm 28 */
											goto BgL_case_else1057z00_267;
										}
								}
							else
								{	/* Jas/stack.scm 27 */
									return BgL_nz00_266;
								}
						}
					}
				}
			}
		}

	}



/* &nbpop */
	obj_t BGl_z62nbpopz62zzjas_stackz00(obj_t BgL_envz00_705,
		obj_t BgL_classfilez00_706, obj_t BgL_insz00_707)
	{
		{	/* Jas/stack.scm 25 */
			return BGl_nbpopz00zzjas_stackz00(BgL_classfilez00_706, BgL_insz00_707);
		}

	}



/* nbpush */
	BGL_EXPORTED_DEF obj_t BGl_nbpushz00zzjas_stackz00(obj_t BgL_classfilez00_5,
		obj_t BgL_insz00_6)
	{
		{	/* Jas/stack.scm 58 */
			{	/* Jas/stack.scm 59 */
				obj_t BgL_copz00_295;

				BgL_copz00_295 = CAR(((obj_t) BgL_insz00_6));
				{	/* Jas/stack.scm 59 */
					obj_t BgL_argsz00_296;

					BgL_argsz00_296 = CDR(((obj_t) BgL_insz00_6));
					{	/* Jas/stack.scm 59 */
						obj_t BgL_nz00_297;

						{	/* Jas/stack.scm 59 */
							obj_t BgL_vectorz00_607;
							long BgL_kz00_608;

							BgL_vectorz00_607 = CNST_TABLE_REF(1);
							BgL_kz00_608 = (long) CINT(BgL_copz00_295);
							BgL_nz00_297 = VECTOR_REF(BgL_vectorz00_607, BgL_kz00_608);
						}
						{	/* Jas/stack.scm 59 */

							if ((BgL_nz00_297 == CNST_TABLE_REF(2)))
								{

									if (INTEGERP(BgL_copz00_295))
										{	/* Jas/stack.scm 61 */
											switch ((long) CINT(BgL_copz00_295))
												{
												case 178L:
												case 180L:

													{	/* Jas/stack.scm 63 */
														obj_t BgL_arg1249z00_300;

														BgL_arg1249z00_300 =
															(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
																	((BgL_fieldzd2orzd2methodz00_bglt)
																		((BgL_fieldz00_bglt)
																			CAR(
																				((obj_t) BgL_argsz00_296))))))->
															BgL_typez00);
														return
															BINT(BGl_typezd2siza7ez75zzjas_classfilez00((
																	(BgL_jastypez00_bglt) BgL_arg1249z00_300)));
													}
													break;
												case 182L:
												case 183L:
												case 184L:
												case 185L:

													{	/* Jas/stack.scm 65 */
														BgL_jasfunz00_bglt BgL_i1064z00_302;

														BgL_i1064z00_302 =
															((BgL_jasfunz00_bglt)
															(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
																		((BgL_fieldzd2orzd2methodz00_bglt)
																			((BgL_methodz00_bglt)
																				CAR(
																					((obj_t) BgL_argsz00_296))))))->
																BgL_typez00));
														{	/* Jas/stack.scm 66 */
															BgL_jastypez00_bglt BgL_arg1268z00_303;

															BgL_arg1268z00_303 =
																(((BgL_jasfunz00_bglt)
																	COBJECT(BgL_i1064z00_302))->BgL_tretz00);
															return
																BINT(BGl_typezd2siza7ez75zzjas_classfilez00
																(BgL_arg1268z00_303));
														}
													}
													break;
												default:
												BgL_case_else1062z00_298:
													return
														BGl_jaszd2errorzd2zzjas_classfilez00(
														((BgL_classfilez00_bglt) BgL_classfilez00_5),
														BGl_string1561z00zzjas_stackz00, BgL_copz00_295);
												}
										}
									else
										{	/* Jas/stack.scm 61 */
											goto BgL_case_else1062z00_298;
										}
								}
							else
								{	/* Jas/stack.scm 60 */
									return BgL_nz00_297;
								}
						}
					}
				}
			}
		}

	}



/* &nbpush */
	obj_t BGl_z62nbpushz62zzjas_stackz00(obj_t BgL_envz00_708,
		obj_t BgL_classfilez00_709, obj_t BgL_insz00_710)
	{
		{	/* Jas/stack.scm 58 */
			return BGl_nbpushz00zzjas_stackz00(BgL_classfilez00_709, BgL_insz00_710);
		}

	}



/* stk-analysis */
	BGL_EXPORTED_DEF obj_t BGl_stkzd2analysiszd2zzjas_stackz00(obj_t
		BgL_classfilez00_7, obj_t BgL_codez00_8)
	{
		{	/* Jas/stack.scm 70 */
			return
				BGl_stkzd2analyseze70z35zzjas_stackz00
				(BGl_makezd2stkzd2envz00zzjas_stackz00(BgL_codez00_8),
				BgL_classfilez00_7, BgL_codez00_8, BgL_codez00_8, BINT(0L), BINT(0L));
		}

	}



/* stk-analyse~0 */
	obj_t BGl_stkzd2analyseze70z35zzjas_stackz00(obj_t BgL_envz00_720,
		obj_t BgL_classfilez00_719, obj_t BgL_codez00_718, obj_t BgL_lz00_308,
		obj_t BgL_nz00_309, obj_t BgL_maxnz00_310)
	{
		{	/* Jas/stack.scm 93 */
		BGl_stkzd2analyseze70z35zzjas_stackz00:
			{
				obj_t BgL_insz00_339;
				obj_t BgL_lz00_340;
				long BgL_nz00_341;
				obj_t BgL_maxnz00_342;

				if (NULLP(BgL_lz00_308))
					{	/* Jas/stack.scm 73 */
						return
							BGl_jaszd2errorzd2zzjas_classfilez00(
							((BgL_classfilez00_bglt) BgL_classfilez00_719),
							BGl_string1563z00zzjas_stackz00, BgL_lz00_308);
					}
				else
					{	/* Jas/stack.scm 75 */
						bool_t BgL_test1596z00_880;

						{	/* Jas/stack.scm 75 */
							obj_t BgL_tmpz00_881;

							BgL_tmpz00_881 = CAR(((obj_t) BgL_lz00_308));
							BgL_test1596z00_880 = SYMBOLP(BgL_tmpz00_881);
						}
						if (BgL_test1596z00_880)
							{	/* Jas/stack.scm 76 */
								obj_t BgL_slotz00_315;

								BgL_slotz00_315 =
									BGl_assqz00zz__r4_pairs_and_lists_6_3z00(CAR(
										((obj_t) BgL_lz00_308)), BgL_envz00_720);
								{	/* Jas/stack.scm 76 */
									obj_t BgL_nlabz00_316;

									BgL_nlabz00_316 = CDR(((obj_t) BgL_slotz00_315));
									{	/* Jas/stack.scm 76 */

										if (((long) CINT(BgL_nlabz00_316) == -1L))
											{	/* Jas/stack.scm 78 */
												{	/* Jas/stack.scm 79 */
													obj_t BgL_tmpz00_893;

													BgL_tmpz00_893 = ((obj_t) BgL_slotz00_315);
													SET_CDR(BgL_tmpz00_893, BgL_nz00_309);
												}
												{	/* Jas/stack.scm 80 */
													obj_t BgL_arg1304z00_318;

													BgL_arg1304z00_318 = CDR(((obj_t) BgL_lz00_308));
													{
														obj_t BgL_lz00_898;

														BgL_lz00_898 = BgL_arg1304z00_318;
														BgL_lz00_308 = BgL_lz00_898;
														goto BGl_stkzd2analyseze70z35zzjas_stackz00;
													}
												}
											}
										else
											{	/* Jas/stack.scm 78 */
												if (
													((long) CINT(BgL_nz00_309) ==
														(long) CINT(BgL_nlabz00_316)))
													{	/* Jas/stack.scm 81 */
														return BgL_maxnz00_310;
													}
												else
													{	/* Jas/stack.scm 81 */
														{	/* Jas/stack.scm 84 */
															obj_t BgL_arg1306z00_320;

															{	/* Jas/stack.scm 84 */
																obj_t BgL_arg1307z00_321;

																BgL_arg1307z00_321 =
																	CAR(((obj_t) BgL_lz00_308));
																{	/* Jas/stack.scm 84 */
																	obj_t BgL_list1308z00_322;

																	{	/* Jas/stack.scm 84 */
																		obj_t BgL_arg1310z00_323;

																		{	/* Jas/stack.scm 84 */
																			obj_t BgL_arg1311z00_324;

																			BgL_arg1311z00_324 =
																				MAKE_YOUNG_PAIR(BgL_nlabz00_316, BNIL);
																			BgL_arg1310z00_323 =
																				MAKE_YOUNG_PAIR(BgL_nz00_309,
																				BgL_arg1311z00_324);
																		}
																		BgL_list1308z00_322 =
																			MAKE_YOUNG_PAIR(BgL_arg1307z00_321,
																			BgL_arg1310z00_323);
																	}
																	BgL_arg1306z00_320 = BgL_list1308z00_322;
																}
															}
															BGl_jaszd2warningzd2zzjas_classfilez00(
																((BgL_classfilez00_bglt) BgL_classfilez00_719),
																BGl_string1564z00zzjas_stackz00,
																BgL_arg1306z00_320);
														}
														return BgL_maxnz00_310;
													}
											}
									}
								}
							}
						else
							{	/* Jas/stack.scm 87 */
								long BgL_downz00_326;

								{	/* Jas/stack.scm 87 */
									obj_t BgL_arg1319z00_336;

									{	/* Jas/stack.scm 87 */
										obj_t BgL_arg1320z00_337;

										BgL_arg1320z00_337 = CAR(((obj_t) BgL_lz00_308));
										BgL_arg1319z00_336 =
											BGl_nbpopz00zzjas_stackz00(BgL_classfilez00_719,
											BgL_arg1320z00_337);
									}
									BgL_downz00_326 =
										(
										(long) CINT(BgL_nz00_309) -
										(long) CINT(BgL_arg1319z00_336));
								}
								{	/* Jas/stack.scm 87 */
									long BgL_upz00_327;

									{	/* Jas/stack.scm 88 */
										obj_t BgL_arg1317z00_334;

										{	/* Jas/stack.scm 88 */
											obj_t BgL_arg1318z00_335;

											BgL_arg1318z00_335 = CAR(((obj_t) BgL_lz00_308));
											BgL_arg1317z00_334 =
												BGl_nbpushz00zzjas_stackz00(BgL_classfilez00_719,
												BgL_arg1318z00_335);
										}
										BgL_upz00_327 =
											(BgL_downz00_326 + (long) CINT(BgL_arg1317z00_334));
									}
									{	/* Jas/stack.scm 88 */

										if ((BgL_downz00_326 < 0L))
											{	/* Jas/stack.scm 89 */
												BGl_jaszd2warningzd2zzjas_classfilez00(
													((BgL_classfilez00_bglt) BgL_classfilez00_719),
													BGl_string1565z00zzjas_stackz00,
													BINT(BgL_downz00_326));
												BgL_maxnz00_310 =
													SUBFX(BgL_maxnz00_310, BINT(BgL_downz00_326));
											}
										else
											{	/* Jas/stack.scm 89 */
												BFALSE;
											}
										{	/* Jas/stack.scm 93 */
											obj_t BgL_arg1314z00_329;
											obj_t BgL_arg1315z00_330;
											obj_t BgL_arg1316z00_331;

											BgL_arg1314z00_329 = CAR(((obj_t) BgL_lz00_308));
											BgL_arg1315z00_330 = CDR(((obj_t) BgL_lz00_308));
											BgL_arg1316z00_331 =
												BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_upz00_327),
												BgL_maxnz00_310);
											BgL_insz00_339 = BgL_arg1314z00_329;
											BgL_lz00_340 = BgL_arg1315z00_330;
											BgL_nz00_341 = BgL_upz00_327;
											BgL_maxnz00_342 = BgL_arg1316z00_331;
											{
												obj_t BgL_lz00_420;

												{	/* Jas/stack.scm 97 */
													obj_t BgL_aux1066z00_347;

													BgL_aux1066z00_347 = CAR(((obj_t) BgL_insz00_339));
													if (INTEGERP(BgL_aux1066z00_347))
														{	/* Jas/stack.scm 97 */
															switch ((long) CINT(BgL_aux1066z00_347))
																{
																case 153L:
																case 154L:
																case 155L:
																case 156L:
																case 157L:
																case 158L:
																case 159L:
																case 160L:
																case 161L:
																case 162L:
																case 163L:
																case 164L:
																case 165L:
																case 166L:
																case 198L:
																case 199L:

																	{	/* Jas/stack.scm 99 */
																		obj_t BgL_xz00_348;
																		obj_t BgL_yz00_349;

																		BgL_xz00_348 =
																			BGl_stkzd2analyseze70z35zzjas_stackz00
																			(BgL_envz00_720, BgL_classfilez00_719,
																			BgL_codez00_718, BgL_lz00_340,
																			BINT(BgL_nz00_341), BgL_maxnz00_342);
																		{	/* Jas/stack.scm 99 */
																			obj_t BgL_arg1323z00_350;

																			{	/* Jas/stack.scm 99 */
																				obj_t BgL_pairz00_641;

																				BgL_pairz00_641 =
																					CDR(((obj_t) BgL_insz00_339));
																				BgL_arg1323z00_350 =
																					CAR(BgL_pairz00_641);
																			}
																			BgL_yz00_349 =
																				BGl_stkzd2analyseze70z35zzjas_stackz00
																				(BgL_envz00_720, BgL_classfilez00_719,
																				BgL_codez00_718,
																				BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																				(BgL_arg1323z00_350, BgL_codez00_718),
																				BINT(BgL_nz00_341), BgL_maxnz00_342);
																		}
																		return
																			BGl_2maxz00zz__r4_numbers_6_5z00
																			(BgL_xz00_348, BgL_yz00_349);
																	}
																	break;
																case 167L:
																case 200L:

																	{	/* Jas/stack.scm 101 */
																		obj_t BgL_arg1325z00_351;

																		{	/* Jas/stack.scm 101 */
																			obj_t BgL_pairz00_646;

																			BgL_pairz00_646 =
																				CDR(((obj_t) BgL_insz00_339));
																			BgL_arg1325z00_351 = CAR(BgL_pairz00_646);
																		}
																		{
																			obj_t BgL_maxnz00_954;
																			obj_t BgL_nz00_952;
																			obj_t BgL_lz00_950;

																			BgL_lz00_950 =
																				BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																				(BgL_arg1325z00_351, BgL_codez00_718);
																			BgL_nz00_952 = BINT(BgL_nz00_341);
																			BgL_maxnz00_954 = BgL_maxnz00_342;
																			BgL_maxnz00_310 = BgL_maxnz00_954;
																			BgL_nz00_309 = BgL_nz00_952;
																			BgL_lz00_308 = BgL_lz00_950;
																			goto
																				BGl_stkzd2analyseze70z35zzjas_stackz00;
																		}
																	}
																	break;
																case 168L:
																case 201L:

																	{	/* Jas/stack.scm 103 */
																		obj_t BgL_xz00_352;
																		obj_t BgL_yz00_353;

																		{	/* Jas/stack.scm 103 */
																			obj_t BgL_arg1326z00_354;

																			{	/* Jas/stack.scm 103 */
																				obj_t BgL_za71za7_648;

																				BgL_za71za7_648 = BINT(BgL_nz00_341);
																				{	/* Jas/stack.scm 103 */
																					obj_t BgL_tmpz00_650;

																					BgL_tmpz00_650 = BINT(0L);
																					{	/* Jas/stack.scm 103 */
																						bool_t BgL_test1602z00_957;

																						{	/* Jas/stack.scm 103 */
																							obj_t BgL_tmpz00_958;

																							BgL_tmpz00_958 = BINT(1L);
																							BgL_test1602z00_957 =
																								BGL_SUBFX_OV(BgL_za71za7_648,
																								BgL_tmpz00_958, BgL_tmpz00_650);
																						}
																						if (BgL_test1602z00_957)
																							{	/* Jas/stack.scm 103 */
																								BgL_arg1326z00_354 =
																									bgl_bignum_sub
																									(bgl_long_to_bignum((long)
																										CINT(BgL_za71za7_648)),
																									CNST_TABLE_REF(3));
																							}
																						else
																							{	/* Jas/stack.scm 103 */
																								BgL_arg1326z00_354 =
																									BgL_tmpz00_650;
																							}
																					}
																				}
																			}
																			BgL_xz00_352 =
																				BGl_stkzd2analyseze70z35zzjas_stackz00
																				(BgL_envz00_720, BgL_classfilez00_719,
																				BgL_codez00_718, BgL_lz00_340,
																				BgL_arg1326z00_354, BgL_maxnz00_342);
																		}
																		{	/* Jas/stack.scm 104 */
																			obj_t BgL_arg1327z00_355;

																			{	/* Jas/stack.scm 104 */
																				obj_t BgL_pairz00_661;

																				BgL_pairz00_661 =
																					CDR(((obj_t) BgL_insz00_339));
																				BgL_arg1327z00_355 =
																					CAR(BgL_pairz00_661);
																			}
																			BgL_yz00_353 =
																				BGl_stkzd2analyseze70z35zzjas_stackz00
																				(BgL_envz00_720, BgL_classfilez00_719,
																				BgL_codez00_718,
																				BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																				(BgL_arg1327z00_355, BgL_codez00_718),
																				BINT(BgL_nz00_341), BgL_maxnz00_342);
																		}
																		return
																			BGl_2maxz00zz__r4_numbers_6_5z00
																			(BgL_xz00_352, BgL_yz00_353);
																	}
																	break;
																case 169L:
																case 191L:

																	return BgL_maxnz00_342;
																	break;
																case 170L:

																	{
																		obj_t BgL_defz00_356;
																		obj_t BgL_begz00_357;
																		obj_t BgL_labsz00_358;

																		{	/* Jas/stack.scm 108 */
																			obj_t BgL_ezd2102zd2_360;

																			BgL_ezd2102zd2_360 =
																				CDR(((obj_t) BgL_insz00_339));
																			if (PAIRP(BgL_ezd2102zd2_360))
																				{	/* Jas/stack.scm 107 */
																					obj_t BgL_cdrzd2110zd2_362;

																					BgL_cdrzd2110zd2_362 =
																						CDR(BgL_ezd2102zd2_360);
																					if (PAIRP(BgL_cdrzd2110zd2_362))
																						{	/* Jas/stack.scm 107 */
																							BgL_defz00_356 =
																								CAR(BgL_ezd2102zd2_360);
																							BgL_begz00_357 =
																								CAR(BgL_cdrzd2110zd2_362);
																							BgL_labsz00_358 =
																								CDR(BgL_cdrzd2110zd2_362);
																							{	/* Jas/stack.scm 110 */
																								obj_t BgL_runner1354z00_386;

																								{	/* Jas/stack.scm 110 */
																									obj_t BgL_arg1335z00_367;
																									obj_t BgL_arg1339z00_368;

																									BgL_arg1335z00_367 =
																										BGl_stkzd2analyseze70z35zzjas_stackz00
																										(BgL_envz00_720,
																										BgL_classfilez00_719,
																										BgL_codez00_718,
																										BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																										(BgL_defz00_356,
																											BgL_codez00_718),
																										BINT(BgL_nz00_341),
																										BgL_maxnz00_342);
																									if (NULLP(BgL_labsz00_358))
																										{	/* Jas/stack.scm 110 */
																											BgL_arg1339z00_368 = BNIL;
																										}
																									else
																										{	/* Jas/stack.scm 110 */
																											obj_t BgL_head1217z00_371;

																											{	/* Jas/stack.scm 110 */
																												obj_t
																													BgL_arg1351z00_383;
																												{	/* Jas/stack.scm 110 */
																													obj_t
																														BgL_arg1352z00_384;
																													BgL_arg1352z00_384 =
																														CAR(((obj_t)
																															BgL_labsz00_358));
																													BgL_arg1351z00_383 =
																														BGl_stkzd2analyseze70z35zzjas_stackz00
																														(BgL_envz00_720,
																														BgL_classfilez00_719,
																														BgL_codez00_718,
																														BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																														(BgL_arg1352z00_384,
																															BgL_codez00_718),
																														BINT(BgL_nz00_341),
																														BgL_maxnz00_342);
																												}
																												BgL_head1217z00_371 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1351z00_383,
																													BNIL);
																											}
																											{	/* Jas/stack.scm 110 */
																												obj_t BgL_g1220z00_372;

																												BgL_g1220z00_372 =
																													CDR(
																													((obj_t)
																														BgL_labsz00_358));
																												{
																													obj_t
																														BgL_l1215z00_374;
																													obj_t
																														BgL_tail1218z00_375;
																													BgL_l1215z00_374 =
																														BgL_g1220z00_372;
																													BgL_tail1218z00_375 =
																														BgL_head1217z00_371;
																												BgL_zc3z04anonymousza31342ze3z87_376:
																													if (NULLP
																														(BgL_l1215z00_374))
																														{	/* Jas/stack.scm 110 */
																															BgL_arg1339z00_368
																																=
																																BgL_head1217z00_371;
																														}
																													else
																														{	/* Jas/stack.scm 110 */
																															obj_t
																																BgL_newtail1219z00_378;
																															{	/* Jas/stack.scm 110 */
																																obj_t
																																	BgL_arg1348z00_380;
																																{	/* Jas/stack.scm 110 */
																																	obj_t
																																		BgL_arg1349z00_381;
																																	BgL_arg1349z00_381
																																		=
																																		CAR(((obj_t)
																																			BgL_l1215z00_374));
																																	BgL_arg1348z00_380
																																		=
																																		BGl_stkzd2analyseze70z35zzjas_stackz00
																																		(BgL_envz00_720,
																																		BgL_classfilez00_719,
																																		BgL_codez00_718,
																																		BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																																		(BgL_arg1349z00_381,
																																			BgL_codez00_718),
																																		BINT
																																		(BgL_nz00_341),
																																		BgL_maxnz00_342);
																																}
																																BgL_newtail1219z00_378
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1348z00_380,
																																	BNIL);
																															}
																															SET_CDR
																																(BgL_tail1218z00_375,
																																BgL_newtail1219z00_378);
																															{	/* Jas/stack.scm 110 */
																																obj_t
																																	BgL_arg1346z00_379;
																																BgL_arg1346z00_379
																																	=
																																	CDR(((obj_t)
																																		BgL_l1215z00_374));
																																{
																																	obj_t
																																		BgL_tail1218z00_1005;
																																	obj_t
																																		BgL_l1215z00_1004;
																																	BgL_l1215z00_1004
																																		=
																																		BgL_arg1346z00_379;
																																	BgL_tail1218z00_1005
																																		=
																																		BgL_newtail1219z00_378;
																																	BgL_tail1218z00_375
																																		=
																																		BgL_tail1218z00_1005;
																																	BgL_l1215z00_374
																																		=
																																		BgL_l1215z00_1004;
																																	goto
																																		BgL_zc3z04anonymousza31342ze3z87_376;
																																}
																															}
																														}
																												}
																											}
																										}
																									BgL_runner1354z00_386 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1335z00_367,
																										BgL_arg1339z00_368);
																								}
																								{	/* Jas/stack.scm 110 */
																									obj_t BgL_aux1353z00_385;

																									BgL_aux1353z00_385 =
																										CAR(BgL_runner1354z00_386);
																									BgL_runner1354z00_386 =
																										CDR(BgL_runner1354z00_386);
																									return
																										BGl_maxz00zz__r4_numbers_6_5z00
																										(BgL_aux1353z00_385,
																										BgL_runner1354z00_386);
																								}
																							}
																						}
																					else
																						{	/* Jas/stack.scm 107 */
																							return BFALSE;
																						}
																				}
																			else
																				{	/* Jas/stack.scm 107 */
																					return BFALSE;
																				}
																		}
																	}
																	break;
																case 171L:

																	{
																		obj_t BgL_defz00_387;
																		obj_t BgL_tablez00_388;

																		{	/* Jas/stack.scm 112 */
																			obj_t BgL_ezd2119zd2_390;

																			BgL_ezd2119zd2_390 =
																				CDR(((obj_t) BgL_insz00_339));
																			if (PAIRP(BgL_ezd2119zd2_390))
																				{	/* Jas/stack.scm 111 */
																					BgL_defz00_387 =
																						CAR(BgL_ezd2119zd2_390);
																					BgL_tablez00_388 =
																						CDR(BgL_ezd2119zd2_390);
																					{	/* Jas/stack.scm 114 */
																						obj_t BgL_runner1379z00_411;

																						{	/* Jas/stack.scm 114 */
																							obj_t BgL_arg1367z00_394;
																							obj_t BgL_arg1370z00_395;

																							BgL_arg1367z00_394 =
																								BGl_stkzd2analyseze70z35zzjas_stackz00
																								(BgL_envz00_720,
																								BgL_classfilez00_719,
																								BgL_codez00_718,
																								BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																								(BgL_defz00_387,
																									BgL_codez00_718),
																								BINT(BgL_nz00_341),
																								BgL_maxnz00_342);
																							if (NULLP(BgL_tablez00_388))
																								{	/* Jas/stack.scm 115 */
																									BgL_arg1370z00_395 = BNIL;
																								}
																							else
																								{	/* Jas/stack.scm 115 */
																									obj_t BgL_head1223z00_398;

																									BgL_head1223z00_398 =
																										MAKE_YOUNG_PAIR(BNIL, BNIL);
																									{
																										obj_t BgL_l1221z00_400;
																										obj_t BgL_tail1224z00_401;

																										BgL_l1221z00_400 =
																											BgL_tablez00_388;
																										BgL_tail1224z00_401 =
																											BgL_head1223z00_398;
																									BgL_zc3z04anonymousza31372ze3z87_402:
																										if (NULLP
																											(BgL_l1221z00_400))
																											{	/* Jas/stack.scm 115 */
																												BgL_arg1370z00_395 =
																													CDR
																													(BgL_head1223z00_398);
																											}
																										else
																											{	/* Jas/stack.scm 115 */
																												obj_t
																													BgL_newtail1225z00_404;
																												{	/* Jas/stack.scm 115 */
																													obj_t
																														BgL_arg1376z00_406;
																													{	/* Jas/stack.scm 115 */
																														obj_t
																															BgL_slotz00_407;
																														BgL_slotz00_407 =
																															CAR(((obj_t)
																																BgL_l1221z00_400));
																														{	/* Jas/stack.scm 115 */
																															obj_t
																																BgL_arg1377z00_408;
																															BgL_arg1377z00_408
																																=
																																CDR(((obj_t)
																																	BgL_slotz00_407));
																															BgL_arg1376z00_406
																																=
																																BGl_stkzd2analyseze70z35zzjas_stackz00
																																(BgL_envz00_720,
																																BgL_classfilez00_719,
																																BgL_codez00_718,
																																BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																																(BgL_arg1377z00_408,
																																	BgL_codez00_718),
																																BINT
																																(BgL_nz00_341),
																																BgL_maxnz00_342);
																														}
																													}
																													BgL_newtail1225z00_404
																														=
																														MAKE_YOUNG_PAIR
																														(BgL_arg1376z00_406,
																														BNIL);
																												}
																												SET_CDR
																													(BgL_tail1224z00_401,
																													BgL_newtail1225z00_404);
																												{	/* Jas/stack.scm 115 */
																													obj_t
																														BgL_arg1375z00_405;
																													BgL_arg1375z00_405 =
																														CDR(((obj_t)
																															BgL_l1221z00_400));
																													{
																														obj_t
																															BgL_tail1224z00_1038;
																														obj_t
																															BgL_l1221z00_1037;
																														BgL_l1221z00_1037 =
																															BgL_arg1375z00_405;
																														BgL_tail1224z00_1038
																															=
																															BgL_newtail1225z00_404;
																														BgL_tail1224z00_401
																															=
																															BgL_tail1224z00_1038;
																														BgL_l1221z00_400 =
																															BgL_l1221z00_1037;
																														goto
																															BgL_zc3z04anonymousza31372ze3z87_402;
																													}
																												}
																											}
																									}
																								}
																							BgL_runner1379z00_411 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1367z00_394,
																								BgL_arg1370z00_395);
																						}
																						{	/* Jas/stack.scm 114 */
																							obj_t BgL_aux1378z00_410;

																							BgL_aux1378z00_410 =
																								CAR(BgL_runner1379z00_411);
																							BgL_runner1379z00_411 =
																								CDR(BgL_runner1379z00_411);
																							return
																								BGl_maxz00zz__r4_numbers_6_5z00
																								(BgL_aux1378z00_410,
																								BgL_runner1379z00_411);
																						}
																					}
																				}
																			else
																				{	/* Jas/stack.scm 111 */
																					return BFALSE;
																				}
																		}
																	}
																	break;
																case 172L:
																case 173L:
																case 174L:
																case 175L:
																case 176L:
																case 177L:

																	if ((BgL_nz00_341 == 0L))
																		{	/* Jas/stack.scm 118 */
																			BFALSE;
																		}
																	else
																		{	/* Jas/stack.scm 118 */
																			BGl_jaszd2warningzd2zzjas_classfilez00(
																				((BgL_classfilez00_bglt)
																					BgL_classfilez00_719),
																				BGl_string1562z00zzjas_stackz00,
																				BINT(BgL_nz00_341));
																		}
																	return BgL_maxnz00_342;
																	break;
																case 202L:

																	{	/* Jas/stack.scm 121 */
																		obj_t BgL_xz00_413;
																		obj_t BgL_yz00_414;

																		BgL_xz00_413 =
																			BGl_stkzd2analyseze70z35zzjas_stackz00
																			(BgL_envz00_720, BgL_classfilez00_719,
																			BgL_codez00_718, BgL_lz00_340,
																			BINT(BgL_nz00_341), BgL_maxnz00_342);
																		{	/* Jas/stack.scm 121 */
																			obj_t BgL_arg1408z00_415;

																			{	/* Jas/stack.scm 121 */
																				obj_t BgL_auxz00_1052;

																				{	/* Jas/stack.scm 121 */
																					obj_t BgL_pairz00_698;

																					{	/* Jas/stack.scm 121 */
																						obj_t BgL_pairz00_697;

																						{	/* Jas/stack.scm 121 */
																							obj_t BgL_pairz00_696;

																							BgL_pairz00_696 =
																								CDR(((obj_t) BgL_insz00_339));
																							BgL_pairz00_697 =
																								CDR(BgL_pairz00_696);
																						}
																						BgL_pairz00_698 =
																							CDR(BgL_pairz00_697);
																					}
																					BgL_auxz00_1052 =
																						CAR(BgL_pairz00_698);
																				}
																				BgL_arg1408z00_415 =
																					BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																					(BgL_auxz00_1052, BgL_codez00_718);
																			}
																			BgL_yz00_414 =
																				BGl_stkzd2analyseze70z35zzjas_stackz00
																				(BgL_envz00_720, BgL_classfilez00_719,
																				BgL_codez00_718, BgL_arg1408z00_415,
																				BINT(1L), BgL_maxnz00_342);
																		}
																		return
																			BGl_2maxz00zz__r4_numbers_6_5z00
																			(BgL_xz00_413, BgL_yz00_414);
																	}
																	break;
																default:
																	BgL_lz00_420 = BgL_lz00_340;
																BgL_zc3z04anonymousza31422ze3z87_421:
																	{
																		obj_t BgL_maxnz00_1065;
																		obj_t BgL_nz00_1063;
																		obj_t BgL_lz00_1062;

																		BgL_lz00_1062 = BgL_lz00_420;
																		BgL_nz00_1063 = BINT(BgL_nz00_341);
																		BgL_maxnz00_1065 = BgL_maxnz00_342;
																		BgL_maxnz00_310 = BgL_maxnz00_1065;
																		BgL_nz00_309 = BgL_nz00_1063;
																		BgL_lz00_308 = BgL_lz00_1062;
																		goto BGl_stkzd2analyseze70z35zzjas_stackz00;
																	}
																}
														}
													else
														{
															obj_t BgL_lz00_1068;

															BgL_lz00_1068 = BgL_lz00_340;
															BgL_lz00_420 = BgL_lz00_1068;
															goto BgL_zc3z04anonymousza31422ze3z87_421;
														}
												}
											}
										}
									}
								}
							}
					}
			}
		}

	}



/* &stk-analysis */
	obj_t BGl_z62stkzd2analysiszb0zzjas_stackz00(obj_t BgL_envz00_711,
		obj_t BgL_classfilez00_712, obj_t BgL_codez00_713)
	{
		{	/* Jas/stack.scm 70 */
			return
				BGl_stkzd2analysiszd2zzjas_stackz00(BgL_classfilez00_712,
				BgL_codez00_713);
		}

	}



/* make-stk-env */
	obj_t BGl_makezd2stkzd2envz00zzjas_stackz00(obj_t BgL_lz00_9)
	{
		{	/* Jas/stack.scm 125 */
		BGl_makezd2stkzd2envz00zzjas_stackz00:
			if (NULLP(BgL_lz00_9))
				{	/* Jas/stack.scm 126 */
					return BgL_lz00_9;
				}
			else
				{	/* Jas/stack.scm 127 */
					bool_t BgL_test1612z00_1072;

					{	/* Jas/stack.scm 127 */
						obj_t BgL_tmpz00_1073;

						BgL_tmpz00_1073 = CAR(((obj_t) BgL_lz00_9));
						BgL_test1612z00_1072 = SYMBOLP(BgL_tmpz00_1073);
					}
					if (BgL_test1612z00_1072)
						{	/* Jas/stack.scm 127 */
							obj_t BgL_arg1437z00_429;
							obj_t BgL_arg1448z00_430;

							{	/* Jas/stack.scm 127 */
								obj_t BgL_arg1453z00_431;

								BgL_arg1453z00_431 = CAR(((obj_t) BgL_lz00_9));
								BgL_arg1437z00_429 =
									MAKE_YOUNG_PAIR(BgL_arg1453z00_431, BINT(-1L));
							}
							{	/* Jas/stack.scm 127 */
								obj_t BgL_arg1454z00_432;

								BgL_arg1454z00_432 = CDR(((obj_t) BgL_lz00_9));
								BgL_arg1448z00_430 =
									BGl_makezd2stkzd2envz00zzjas_stackz00(BgL_arg1454z00_432);
							}
							return MAKE_YOUNG_PAIR(BgL_arg1437z00_429, BgL_arg1448z00_430);
						}
					else
						{	/* Jas/stack.scm 128 */
							obj_t BgL_arg1472z00_433;

							BgL_arg1472z00_433 = CDR(((obj_t) BgL_lz00_9));
							{
								obj_t BgL_lz00_1087;

								BgL_lz00_1087 = BgL_arg1472z00_433;
								BgL_lz00_9 = BgL_lz00_1087;
								goto BGl_makezd2stkzd2envz00zzjas_stackz00;
							}
						}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzjas_stackz00(void)
	{
		{	/* Jas/stack.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzjas_stackz00(void)
	{
		{	/* Jas/stack.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzjas_stackz00(void)
	{
		{	/* Jas/stack.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzjas_stackz00(void)
	{
		{	/* Jas/stack.scm 1 */
			BGl_modulezd2initializa7ationz75zzjas_libz00(267157509L,
				BSTRING_TO_STRING(BGl_string1566z00zzjas_stackz00));
			return
				BGl_modulezd2initializa7ationz75zzjas_classfilez00(135094074L,
				BSTRING_TO_STRING(BGl_string1566z00zzjas_stackz00));
		}

	}

#ifdef __cplusplus
}
#endif
