/*===========================================================================*/
/*   (Jas/produce.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Jas/produce.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_JAS_PRODUCE_TYPE_DEFINITIONS
#define BGL_JAS_PRODUCE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_fieldzd2orzd2methodz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}                             *BgL_fieldzd2orzd2methodz00_bglt;

	typedef struct BgL_fieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}               *BgL_fieldz00_bglt;

	typedef struct BgL_methodz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}                *BgL_methodz00_bglt;

	typedef struct BgL_attributez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_typez00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_infoz00;
	}                   *BgL_attributez00_bglt;

	typedef struct BgL_classfilez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_currentzd2methodzd2;
		obj_t BgL_globalsz00;
		obj_t BgL_poolz00;
		obj_t BgL_poolzd2siza7ez75;
		obj_t BgL_pooledzd2nameszd2;
		obj_t BgL_flagsz00;
		obj_t BgL_mez00;
		obj_t BgL_superz00;
		obj_t BgL_interfacesz00;
		obj_t BgL_fieldsz00;
		obj_t BgL_methodsz00;
		obj_t BgL_attributesz00;
	}                   *BgL_classfilez00_bglt;


#endif													// BGL_JAS_PRODUCE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzjas_producez00 = BUNSPEC;
	static obj_t BGl_z62producez62zzjas_producez00(obj_t, obj_t, obj_t);
	static obj_t BGl_producezd2cpinfozd2zzjas_producez00(obj_t, obj_t);
	static bool_t BGl_producezd2fieldszd2zzjas_producez00(obj_t, obj_t);
	static bool_t BGl_producezd2poolzd2zzjas_producez00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzjas_producez00(void);
	static bool_t BGl_producezd2methodzd2zzjas_producez00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzjas_producez00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzjas_producez00(void);
	static obj_t BGl_outshortz00zzjas_producez00(obj_t, obj_t);
	static bool_t BGl_producezd2methodszd2zzjas_producez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_producez00zzjas_producez00(obj_t, obj_t);
	static obj_t BGl_producezd2codezd2zzjas_producez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzjas_producez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_classfilez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__binaryz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzjas_producez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzjas_producez00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_outintz00zzjas_producez00(obj_t, obj_t);
	static bool_t BGl_producezd2attributeszd2zzjas_producez00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzjas_producez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzjas_producez00(void);
	static bool_t BGl_producezd2fieldzd2zzjas_producez00(obj_t, obj_t);
	static obj_t BGl_producezd2attributezd2zzjas_producez00(obj_t, obj_t);
	static obj_t BGl_producezd2stringzd2zzjas_producez00(obj_t, obj_t);
	extern obj_t BGl_stringzd2ze3utf8z31zzjas_libz00(obj_t);
	static bool_t BGl_producezd2interfaceszd2zzjas_producez00(obj_t, obj_t);
	static obj_t __cnst[6];


	   
		 
		DEFINE_STRING(BGl_string1574z00zzjas_producez00,
		BgL_bgl_string1574za700za7za7j1585za7, "as", 2);
	      DEFINE_STRING(BGl_string1575z00zzjas_producez00,
		BgL_bgl_string1575za700za7za7j1586za7, "bad cpinfo tag", 14);
	      DEFINE_STRING(BGl_string1576z00zzjas_producez00,
		BgL_bgl_string1576za700za7za7j1587za7, "constant string too long", 24);
	      DEFINE_STRING(BGl_string1577z00zzjas_producez00,
		BgL_bgl_string1577za700za7za7j1588za7, "produce-attribute", 17);
	      DEFINE_STRING(BGl_string1578z00zzjas_producez00,
		BgL_bgl_string1578za700za7za7j1589za7, "bad attribute type", 18);
	      DEFINE_STRING(BGl_string1579z00zzjas_producez00,
		BgL_bgl_string1579za700za7za7j1590za7, "produce-code", 12);
	      DEFINE_STRING(BGl_string1580z00zzjas_producez00,
		BgL_bgl_string1580za700za7za7j1591za7, "bad handler", 11);
	      DEFINE_STRING(BGl_string1581z00zzjas_producez00,
		BgL_bgl_string1581za700za7za7j1592za7, "bad code attribute", 18);
	      DEFINE_STRING(BGl_string1582z00zzjas_producez00,
		BgL_bgl_string1582za700za7za7j1593za7, "jas_produce", 11);
	      DEFINE_STRING(BGl_string1583z00zzjas_producez00,
		BgL_bgl_string1583za700za7za7j1594za7,
		"localvariable linenumber srcfile code bytevector ok ", 52);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_producezd2envzd2zzjas_producez00,
		BgL_bgl_za762produceza762za7za7j1595z00, BGl_z62producez62zzjas_producez00,
		0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzjas_producez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzjas_producez00(long
		BgL_checksumz00_812, char *BgL_fromz00_813)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzjas_producez00))
				{
					BGl_requirezd2initializa7ationz75zzjas_producez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzjas_producez00();
					BGl_libraryzd2moduleszd2initz00zzjas_producez00();
					BGl_cnstzd2initzd2zzjas_producez00();
					BGl_importedzd2moduleszd2initz00zzjas_producez00();
					return BGl_methodzd2initzd2zzjas_producez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzjas_producez00(void)
	{
		{	/* Jas/produce.scm 1 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"jas_produce");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "jas_produce");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "jas_produce");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "jas_produce");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"jas_produce");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"jas_produce");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "jas_produce");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"jas_produce");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "jas_produce");
			BGl_modulezd2initializa7ationz75zz__binaryz00(0L, "jas_produce");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzjas_producez00(void)
	{
		{	/* Jas/produce.scm 1 */
			{	/* Jas/produce.scm 1 */
				obj_t BgL_cportz00_801;

				{	/* Jas/produce.scm 1 */
					obj_t BgL_stringz00_808;

					BgL_stringz00_808 = BGl_string1583z00zzjas_producez00;
					{	/* Jas/produce.scm 1 */
						obj_t BgL_startz00_809;

						BgL_startz00_809 = BINT(0L);
						{	/* Jas/produce.scm 1 */
							obj_t BgL_endz00_810;

							BgL_endz00_810 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_808)));
							{	/* Jas/produce.scm 1 */

								BgL_cportz00_801 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_808, BgL_startz00_809, BgL_endz00_810);
				}}}}
				{
					long BgL_iz00_802;

					BgL_iz00_802 = 5L;
				BgL_loopz00_803:
					if ((BgL_iz00_802 == -1L))
						{	/* Jas/produce.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* Jas/produce.scm 1 */
							{	/* Jas/produce.scm 1 */
								obj_t BgL_arg1584z00_804;

								{	/* Jas/produce.scm 1 */

									{	/* Jas/produce.scm 1 */
										obj_t BgL_locationz00_806;

										BgL_locationz00_806 = BBOOL(((bool_t) 0));
										{	/* Jas/produce.scm 1 */

											BgL_arg1584z00_804 =
												BGl_readz00zz__readerz00(BgL_cportz00_801,
												BgL_locationz00_806);
										}
									}
								}
								{	/* Jas/produce.scm 1 */
									int BgL_tmpz00_841;

									BgL_tmpz00_841 = (int) (BgL_iz00_802);
									CNST_TABLE_SET(BgL_tmpz00_841, BgL_arg1584z00_804);
							}}
							{	/* Jas/produce.scm 1 */
								int BgL_auxz00_807;

								BgL_auxz00_807 = (int) ((BgL_iz00_802 - 1L));
								{
									long BgL_iz00_846;

									BgL_iz00_846 = (long) (BgL_auxz00_807);
									BgL_iz00_802 = BgL_iz00_846;
									goto BgL_loopz00_803;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzjas_producez00(void)
	{
		{	/* Jas/produce.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* outshort */
	obj_t BGl_outshortz00zzjas_producez00(obj_t BgL_outchanz00_7,
		obj_t BgL_nz00_8)
	{
		{	/* Jas/produce.scm 14 */
			{	/* Jas/produce.scm 15 */
				long BgL_arg1232z00_292;

				BgL_arg1232z00_292 = ((long) CINT(BgL_nz00_8) >> (int) (8L));
				{	/* Jas/produce.scm 12 */
					unsigned char BgL_arg1230z00_630;

					BgL_arg1230z00_630 = ((BgL_arg1232z00_292 & 255L));
					BGL_OUTPUT_CHAR(BgL_outchanz00_7, (char) (BgL_arg1230z00_630));
			}}
			{	/* Jas/produce.scm 16 */
				long BgL_arg1233z00_293;

				BgL_arg1233z00_293 = ((long) CINT(BgL_nz00_8) & 255L);
				{	/* Jas/produce.scm 12 */
					unsigned char BgL_arg1230z00_637;

					BgL_arg1230z00_637 = ((BgL_arg1233z00_293 & 255L));
					return BGL_OUTPUT_CHAR(BgL_outchanz00_7, (char) (BgL_arg1230z00_637));
		}}}

	}



/* outint */
	obj_t BGl_outintz00zzjas_producez00(obj_t BgL_outchanz00_9, obj_t BgL_nz00_10)
	{
		{	/* Jas/produce.scm 18 */
			{	/* Jas/produce.scm 19 */
				long BgL_arg1234z00_294;

				BgL_arg1234z00_294 = ((long) CINT(BgL_nz00_10) >> (int) (16L));
				BGl_outshortz00zzjas_producez00(BgL_outchanz00_9,
					BINT(BgL_arg1234z00_294));
			}
			{	/* Jas/produce.scm 20 */
				long BgL_arg1236z00_295;

				BgL_arg1236z00_295 = ((long) CINT(BgL_nz00_10) & 65535L);
				BGL_TAIL return
					BGl_outshortz00zzjas_producez00(BgL_outchanz00_9,
					BINT(BgL_arg1236z00_295));
			}
		}

	}



/* produce */
	BGL_EXPORTED_DEF obj_t BGl_producez00zzjas_producez00(obj_t BgL_outchanz00_11,
		obj_t BgL_classfilez00_12)
	{
		{	/* Jas/produce.scm 22 */
			{	/* Jas/produce.scm 15 */
				long BgL_arg1232z00_645;

				BgL_arg1232z00_645 = (51966L >> (int) (8L));
				{	/* Jas/produce.scm 12 */
					unsigned char BgL_arg1230z00_647;

					BgL_arg1230z00_647 = ((BgL_arg1232z00_645 & 255L));
					BGL_OUTPUT_CHAR(BgL_outchanz00_11, (char) (BgL_arg1230z00_647));
			}}
			{	/* Jas/produce.scm 16 */
				long BgL_arg1233z00_653;

				BgL_arg1233z00_653 = (51966L & 255L);
				{	/* Jas/produce.scm 12 */
					unsigned char BgL_arg1230z00_655;

					BgL_arg1230z00_655 = ((BgL_arg1233z00_653 & 255L));
					BGL_OUTPUT_CHAR(BgL_outchanz00_11, (char) (BgL_arg1230z00_655));
			}}
			{	/* Jas/produce.scm 15 */
				long BgL_arg1232z00_661;

				BgL_arg1232z00_661 = (47806L >> (int) (8L));
				{	/* Jas/produce.scm 12 */
					unsigned char BgL_arg1230z00_663;

					BgL_arg1230z00_663 = ((BgL_arg1232z00_661 & 255L));
					BGL_OUTPUT_CHAR(BgL_outchanz00_11, (char) (BgL_arg1230z00_663));
			}}
			{	/* Jas/produce.scm 16 */
				long BgL_arg1233z00_669;

				BgL_arg1233z00_669 = (47806L & 255L);
				{	/* Jas/produce.scm 12 */
					unsigned char BgL_arg1230z00_671;

					BgL_arg1230z00_671 = ((BgL_arg1233z00_669 & 255L));
					BGL_OUTPUT_CHAR(BgL_outchanz00_11, (char) (BgL_arg1230z00_671));
			}}
			{	/* Jas/produce.scm 15 */
				long BgL_arg1232z00_677;

				BgL_arg1232z00_677 = (3L >> (int) (8L));
				{	/* Jas/produce.scm 12 */
					unsigned char BgL_arg1230z00_679;

					BgL_arg1230z00_679 = ((BgL_arg1232z00_677 & 255L));
					BGL_OUTPUT_CHAR(BgL_outchanz00_11, (char) (BgL_arg1230z00_679));
			}}
			{	/* Jas/produce.scm 16 */
				long BgL_arg1233z00_685;

				BgL_arg1233z00_685 = (3L & 255L);
				{	/* Jas/produce.scm 12 */
					unsigned char BgL_arg1230z00_687;

					BgL_arg1230z00_687 = ((BgL_arg1233z00_685 & 255L));
					BGL_OUTPUT_CHAR(BgL_outchanz00_11, (char) (BgL_arg1230z00_687));
			}}
			{	/* Jas/produce.scm 15 */
				long BgL_arg1232z00_693;

				BgL_arg1232z00_693 = (45L >> (int) (8L));
				{	/* Jas/produce.scm 12 */
					unsigned char BgL_arg1230z00_695;

					BgL_arg1230z00_695 = ((BgL_arg1232z00_693 & 255L));
					BGL_OUTPUT_CHAR(BgL_outchanz00_11, (char) (BgL_arg1230z00_695));
			}}
			{	/* Jas/produce.scm 16 */
				long BgL_arg1233z00_701;

				BgL_arg1233z00_701 = (45L & 255L);
				{	/* Jas/produce.scm 12 */
					unsigned char BgL_arg1230z00_703;

					BgL_arg1230z00_703 = ((BgL_arg1233z00_701 & 255L));
					BGL_OUTPUT_CHAR(BgL_outchanz00_11, (char) (BgL_arg1230z00_703));
			}}
			{	/* Jas/produce.scm 29 */
				obj_t BgL_arg1238z00_297;
				obj_t BgL_arg1239z00_298;

				BgL_arg1238z00_297 =
					(((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_classfilez00_12)))->
					BgL_poolzd2siza7ez75);
				BgL_arg1239z00_298 =
					bgl_reverse((((BgL_classfilez00_bglt) COBJECT(((BgL_classfilez00_bglt)
									BgL_classfilez00_12)))->BgL_poolz00));
				BGl_producezd2poolzd2zzjas_producez00(BgL_outchanz00_11,
					BgL_arg1238z00_297, BgL_arg1239z00_298);
			}
			{	/* Jas/produce.scm 30 */
				obj_t BgL_arg1244z00_300;

				BgL_arg1244z00_300 =
					(((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_classfilez00_12)))->BgL_flagsz00);
				BGl_outshortz00zzjas_producez00(BgL_outchanz00_11, BgL_arg1244z00_300);
			}
			{	/* Jas/produce.scm 31 */
				obj_t BgL_arg1248z00_301;

				BgL_arg1248z00_301 =
					(((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_classfilez00_12)))->BgL_mez00);
				BGl_outshortz00zzjas_producez00(BgL_outchanz00_11, BgL_arg1248z00_301);
			}
			{	/* Jas/produce.scm 32 */
				obj_t BgL_arg1249z00_302;

				BgL_arg1249z00_302 =
					(((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_classfilez00_12)))->BgL_superz00);
				BGl_outshortz00zzjas_producez00(BgL_outchanz00_11, BgL_arg1249z00_302);
			}
			{	/* Jas/produce.scm 33 */
				obj_t BgL_arg1252z00_303;

				BgL_arg1252z00_303 =
					(((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_classfilez00_12)))->
					BgL_interfacesz00);
				BGl_producezd2interfaceszd2zzjas_producez00(BgL_outchanz00_11,
					BgL_arg1252z00_303);
			}
			{	/* Jas/produce.scm 34 */
				obj_t BgL_arg1268z00_304;

				BgL_arg1268z00_304 =
					(((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_classfilez00_12)))->BgL_fieldsz00);
				BGl_producezd2fieldszd2zzjas_producez00(BgL_outchanz00_11,
					BgL_arg1268z00_304);
			}
			{	/* Jas/produce.scm 35 */
				obj_t BgL_arg1272z00_305;

				BgL_arg1272z00_305 =
					(((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_classfilez00_12)))->BgL_methodsz00);
				BGl_producezd2methodszd2zzjas_producez00(BgL_outchanz00_11,
					BgL_arg1272z00_305);
			}
			{	/* Jas/produce.scm 36 */
				obj_t BgL_arg1284z00_306;

				BgL_arg1284z00_306 =
					(((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_classfilez00_12)))->
					BgL_attributesz00);
				return
					BBOOL(BGl_producezd2attributeszd2zzjas_producez00(BgL_outchanz00_11,
						BgL_arg1284z00_306));
			}
		}

	}



/* &produce */
	obj_t BGl_z62producez62zzjas_producez00(obj_t BgL_envz00_798,
		obj_t BgL_outchanz00_799, obj_t BgL_classfilez00_800)
	{
		{	/* Jas/produce.scm 22 */
			return
				BGl_producez00zzjas_producez00(BgL_outchanz00_799,
				BgL_classfilez00_800);
		}

	}



/* produce-pool */
	bool_t BGl_producezd2poolzd2zzjas_producez00(obj_t BgL_outchanz00_13,
		obj_t BgL_siza7eza7_14, obj_t BgL_poolz00_15)
	{
		{	/* Jas/produce.scm 38 */
			BGl_outshortz00zzjas_producez00(BgL_outchanz00_13, BgL_siza7eza7_14);
			{
				obj_t BgL_l1202z00_308;

				BgL_l1202z00_308 = BgL_poolz00_15;
			BgL_zc3z04anonymousza31285ze3z87_309:
				if (PAIRP(BgL_l1202z00_308))
					{	/* Jas/produce.scm 40 */
						BGl_producezd2cpinfozd2zzjas_producez00(BgL_outchanz00_13,
							CAR(BgL_l1202z00_308));
						{
							obj_t BgL_l1202z00_949;

							BgL_l1202z00_949 = CDR(BgL_l1202z00_308);
							BgL_l1202z00_308 = BgL_l1202z00_949;
							goto BgL_zc3z04anonymousza31285ze3z87_309;
						}
					}
				else
					{	/* Jas/produce.scm 40 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* produce-cpinfo */
	obj_t BGl_producezd2cpinfozd2zzjas_producez00(obj_t BgL_outchanz00_16,
		obj_t BgL_cpinfoz00_17)
	{
		{	/* Jas/produce.scm 42 */
			{	/* Jas/produce.scm 43 */
				obj_t BgL_arg1305z00_314;

				BgL_arg1305z00_314 = CAR(((obj_t) BgL_cpinfoz00_17));
				{	/* Jas/produce.scm 12 */
					unsigned char BgL_arg1230z00_712;

					BgL_arg1230z00_712 = (((long) CINT(BgL_arg1305z00_314) & 255L));
					BGL_OUTPUT_CHAR(BgL_outchanz00_16, (char) (BgL_arg1230z00_712));
			}}
			{

				{	/* Jas/produce.scm 44 */
					obj_t BgL_aux1059z00_316;

					BgL_aux1059z00_316 = CAR(((obj_t) BgL_cpinfoz00_17));
					if (INTEGERP(BgL_aux1059z00_316))
						{	/* Jas/produce.scm 44 */
							switch ((long) CINT(BgL_aux1059z00_316))
								{
								case 1L:

									{	/* Jas/produce.scm 46 */
										obj_t BgL_strz00_317;

										{	/* Jas/produce.scm 46 */
											obj_t BgL_arg1308z00_320;

											BgL_arg1308z00_320 = CDR(((obj_t) BgL_cpinfoz00_17));
											BgL_strz00_317 =
												BGl_stringzd2ze3utf8z31zzjas_libz00(BgL_arg1308z00_320);
										}
										{	/* Jas/produce.scm 47 */
											long BgL_nz00_318;

											BgL_nz00_318 = STRING_LENGTH(((obj_t) BgL_strz00_317));
											if ((BgL_nz00_318 > 65535L))
												{	/* Jas/produce.scm 48 */
													BGl_errorz00zz__errorz00
														(BGl_string1574z00zzjas_producez00,
														BGl_string1576z00zzjas_producez00,
														BINT(BgL_nz00_318));
												}
											else
												{	/* Jas/produce.scm 48 */
													BGl_outshortz00zzjas_producez00(BgL_outchanz00_16,
														BINT(BgL_nz00_318));
												}
										}
										BGL_TAIL return
											BGl_producezd2stringzd2zzjas_producez00(BgL_outchanz00_16,
											BgL_strz00_317);
									}
									break;
								case 3L:
								case 4L:
								case 5L:
								case 6L:
								case 7L:
								case 8L:
								case 9L:
								case 10L:
								case 11L:
								case 12L:

									{	/* Jas/produce.scm 53 */
										obj_t BgL_g1208z00_321;

										BgL_g1208z00_321 = CDR(((obj_t) BgL_cpinfoz00_17));
										{
											obj_t BgL_l1204z00_323;

											{	/* Jas/produce.scm 53 */
												bool_t BgL_tmpz00_976;

												BgL_l1204z00_323 = BgL_g1208z00_321;
											BgL_zc3z04anonymousza31309ze3z87_324:
												if (PAIRP(BgL_l1204z00_323))
													{	/* Jas/produce.scm 53 */
														BGl_outshortz00zzjas_producez00(BgL_outchanz00_16,
															CAR(BgL_l1204z00_323));
														{
															obj_t BgL_l1204z00_981;

															BgL_l1204z00_981 = CDR(BgL_l1204z00_323);
															BgL_l1204z00_323 = BgL_l1204z00_981;
															goto BgL_zc3z04anonymousza31309ze3z87_324;
														}
													}
												else
													{	/* Jas/produce.scm 53 */
														BgL_tmpz00_976 = ((bool_t) 1);
													}
												return BBOOL(BgL_tmpz00_976);
											}
										}
									}
									break;
								default:
								BgL_case_else1058z00_315:
									{	/* Jas/produce.scm 54 */
										obj_t BgL_arg1314z00_329;

										BgL_arg1314z00_329 = CAR(((obj_t) BgL_cpinfoz00_17));
										return
											BGl_errorz00zz__errorz00
											(BGl_string1574z00zzjas_producez00,
											BGl_string1575z00zzjas_producez00, BgL_arg1314z00_329);
									}
								}
						}
					else
						{	/* Jas/produce.scm 44 */
							goto BgL_case_else1058z00_315;
						}
				}
			}
		}

	}



/* produce-string */
	obj_t BGl_producezd2stringzd2zzjas_producez00(obj_t BgL_outchanz00_18,
		obj_t BgL_sz00_19)
	{
		{	/* Jas/produce.scm 56 */
			{	/* Jas/produce.scm 57 */
				long BgL_nz00_330;

				BgL_nz00_330 = STRING_LENGTH(((obj_t) BgL_sz00_19));
				{
					long BgL_iz00_332;

					BgL_iz00_332 = 0L;
				BgL_zc3z04anonymousza31315ze3z87_333:
					if ((BgL_iz00_332 == BgL_nz00_330))
						{	/* Jas/produce.scm 59 */
							return CNST_TABLE_REF(0);
						}
					else
						{	/* Jas/produce.scm 59 */
							{	/* Jas/produce.scm 61 */
								long BgL_arg1317z00_335;

								BgL_arg1317z00_335 =
									(STRING_REF(((obj_t) BgL_sz00_19), BgL_iz00_332));
								{	/* Jas/produce.scm 12 */
									unsigned char BgL_arg1230z00_732;

									BgL_arg1230z00_732 = ((BgL_arg1317z00_335 & 255L));
									BGL_OUTPUT_CHAR(BgL_outchanz00_18,
										(char) (BgL_arg1230z00_732));
							}}
							{
								long BgL_iz00_1001;

								BgL_iz00_1001 = (BgL_iz00_332 + 1L);
								BgL_iz00_332 = BgL_iz00_1001;
								goto BgL_zc3z04anonymousza31315ze3z87_333;
							}
						}
				}
			}
		}

	}



/* produce-interfaces */
	bool_t BGl_producezd2interfaceszd2zzjas_producez00(obj_t BgL_outchanz00_20,
		obj_t BgL_interfacesz00_21)
	{
		{	/* Jas/produce.scm 65 */
			{	/* Jas/produce.scm 66 */
				long BgL_arg1320z00_339;

				BgL_arg1320z00_339 = bgl_list_length(BgL_interfacesz00_21);
				BGl_outshortz00zzjas_producez00(BgL_outchanz00_20,
					BINT(BgL_arg1320z00_339));
			}
			{
				obj_t BgL_l1209z00_341;

				BgL_l1209z00_341 = BgL_interfacesz00_21;
			BgL_zc3z04anonymousza31321ze3z87_342:
				if (PAIRP(BgL_l1209z00_341))
					{	/* Jas/produce.scm 67 */
						BGl_outshortz00zzjas_producez00(BgL_outchanz00_20,
							CAR(BgL_l1209z00_341));
						{
							obj_t BgL_l1209z00_1010;

							BgL_l1209z00_1010 = CDR(BgL_l1209z00_341);
							BgL_l1209z00_341 = BgL_l1209z00_1010;
							goto BgL_zc3z04anonymousza31321ze3z87_342;
						}
					}
				else
					{	/* Jas/produce.scm 67 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* produce-fields */
	bool_t BGl_producezd2fieldszd2zzjas_producez00(obj_t BgL_outchanz00_22,
		obj_t BgL_fieldsz00_23)
	{
		{	/* Jas/produce.scm 69 */
			{	/* Jas/produce.scm 70 */
				long BgL_arg1325z00_347;

				BgL_arg1325z00_347 = bgl_list_length(BgL_fieldsz00_23);
				BGl_outshortz00zzjas_producez00(BgL_outchanz00_22,
					BINT(BgL_arg1325z00_347));
			}
			{
				obj_t BgL_l1211z00_349;

				BgL_l1211z00_349 = BgL_fieldsz00_23;
			BgL_zc3z04anonymousza31326ze3z87_350:
				if (PAIRP(BgL_l1211z00_349))
					{	/* Jas/produce.scm 71 */
						BGl_producezd2fieldzd2zzjas_producez00(BgL_outchanz00_22,
							CAR(BgL_l1211z00_349));
						{
							obj_t BgL_l1211z00_1019;

							BgL_l1211z00_1019 = CDR(BgL_l1211z00_349);
							BgL_l1211z00_349 = BgL_l1211z00_1019;
							goto BgL_zc3z04anonymousza31326ze3z87_350;
						}
					}
				else
					{	/* Jas/produce.scm 71 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* produce-field */
	bool_t BGl_producezd2fieldzd2zzjas_producez00(obj_t BgL_outchanz00_24,
		obj_t BgL_fieldz00_25)
	{
		{	/* Jas/produce.scm 73 */
			{	/* Jas/produce.scm 75 */
				obj_t BgL_arg1329z00_356;

				BgL_arg1329z00_356 =
					(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt)
								((BgL_fieldz00_bglt) BgL_fieldz00_25))))->BgL_flagsz00);
				BGl_outshortz00zzjas_producez00(BgL_outchanz00_24, BgL_arg1329z00_356);
			}
			{	/* Jas/produce.scm 76 */
				obj_t BgL_arg1331z00_357;

				BgL_arg1331z00_357 =
					(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt)
								((BgL_fieldz00_bglt) BgL_fieldz00_25))))->BgL_pnamez00);
				BGl_outshortz00zzjas_producez00(BgL_outchanz00_24, BgL_arg1331z00_357);
			}
			{	/* Jas/produce.scm 77 */
				obj_t BgL_arg1332z00_358;

				BgL_arg1332z00_358 =
					(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt)
								((BgL_fieldz00_bglt) BgL_fieldz00_25))))->BgL_descriptorz00);
				BGl_outshortz00zzjas_producez00(BgL_outchanz00_24, BgL_arg1332z00_358);
			}
			{	/* Jas/produce.scm 78 */
				obj_t BgL_arg1333z00_359;

				BgL_arg1333z00_359 =
					(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt)
								((BgL_fieldz00_bglt) BgL_fieldz00_25))))->BgL_attributesz00);
				BGL_TAIL return
					BGl_producezd2attributeszd2zzjas_producez00(BgL_outchanz00_24,
					BgL_arg1333z00_359);
			}
		}

	}



/* produce-methods */
	bool_t BGl_producezd2methodszd2zzjas_producez00(obj_t BgL_outchanz00_26,
		obj_t BgL_methodsz00_27)
	{
		{	/* Jas/produce.scm 80 */
			{	/* Jas/produce.scm 81 */
				long BgL_arg1335z00_360;

				BgL_arg1335z00_360 = bgl_list_length(BgL_methodsz00_27);
				BGl_outshortz00zzjas_producez00(BgL_outchanz00_26,
					BINT(BgL_arg1335z00_360));
			}
			{
				obj_t BgL_l1213z00_362;

				BgL_l1213z00_362 = BgL_methodsz00_27;
			BgL_zc3z04anonymousza31336ze3z87_363:
				if (PAIRP(BgL_l1213z00_362))
					{	/* Jas/produce.scm 82 */
						BGl_producezd2methodzd2zzjas_producez00(BgL_outchanz00_26,
							CAR(BgL_l1213z00_362));
						{
							obj_t BgL_l1213z00_1044;

							BgL_l1213z00_1044 = CDR(BgL_l1213z00_362);
							BgL_l1213z00_362 = BgL_l1213z00_1044;
							goto BgL_zc3z04anonymousza31336ze3z87_363;
						}
					}
				else
					{	/* Jas/produce.scm 82 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* produce-method */
	bool_t BGl_producezd2methodzd2zzjas_producez00(obj_t BgL_outchanz00_28,
		obj_t BgL_methodz00_29)
	{
		{	/* Jas/produce.scm 84 */
			{	/* Jas/produce.scm 86 */
				obj_t BgL_arg1340z00_369;

				BgL_arg1340z00_369 =
					(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt)
								((BgL_methodz00_bglt) BgL_methodz00_29))))->BgL_flagsz00);
				BGl_outshortz00zzjas_producez00(BgL_outchanz00_28, BgL_arg1340z00_369);
			}
			{	/* Jas/produce.scm 87 */
				obj_t BgL_arg1342z00_370;

				BgL_arg1342z00_370 =
					(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt)
								((BgL_methodz00_bglt) BgL_methodz00_29))))->BgL_pnamez00);
				BGl_outshortz00zzjas_producez00(BgL_outchanz00_28, BgL_arg1342z00_370);
			}
			{	/* Jas/produce.scm 88 */
				obj_t BgL_arg1343z00_371;

				BgL_arg1343z00_371 =
					(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt)
								((BgL_methodz00_bglt) BgL_methodz00_29))))->BgL_descriptorz00);
				BGl_outshortz00zzjas_producez00(BgL_outchanz00_28, BgL_arg1343z00_371);
			}
			{	/* Jas/produce.scm 89 */
				obj_t BgL_arg1346z00_372;

				BgL_arg1346z00_372 =
					(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt)
								((BgL_methodz00_bglt) BgL_methodz00_29))))->BgL_attributesz00);
				BGL_TAIL return
					BGl_producezd2attributeszd2zzjas_producez00(BgL_outchanz00_28,
					BgL_arg1346z00_372);
			}
		}

	}



/* produce-attributes */
	bool_t BGl_producezd2attributeszd2zzjas_producez00(obj_t BgL_outchanz00_30,
		obj_t BgL_attributesz00_31)
	{
		{	/* Jas/produce.scm 91 */
			{	/* Jas/produce.scm 92 */
				long BgL_arg1348z00_373;

				BgL_arg1348z00_373 = bgl_list_length(BgL_attributesz00_31);
				BGl_outshortz00zzjas_producez00(BgL_outchanz00_30,
					BINT(BgL_arg1348z00_373));
			}
			{
				obj_t BgL_l1215z00_375;

				BgL_l1215z00_375 = BgL_attributesz00_31;
			BgL_zc3z04anonymousza31349ze3z87_376:
				if (PAIRP(BgL_l1215z00_375))
					{	/* Jas/produce.scm 93 */
						BGl_producezd2attributezd2zzjas_producez00(BgL_outchanz00_30,
							CAR(BgL_l1215z00_375));
						{
							obj_t BgL_l1215z00_1069;

							BgL_l1215z00_1069 = CDR(BgL_l1215z00_375);
							BgL_l1215z00_375 = BgL_l1215z00_1069;
							goto BgL_zc3z04anonymousza31349ze3z87_376;
						}
					}
				else
					{	/* Jas/produce.scm 93 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* produce-attribute */
	obj_t BGl_producezd2attributezd2zzjas_producez00(obj_t BgL_outchanz00_32,
		obj_t BgL_attributez00_33)
	{
		{	/* Jas/produce.scm 96 */
			{	/* Jas/produce.scm 97 */
				obj_t BgL_arg1352z00_381;

				BgL_arg1352z00_381 =
					(((BgL_attributez00_bglt) COBJECT(
							((BgL_attributez00_bglt) BgL_attributez00_33)))->BgL_namez00);
				BGl_outshortz00zzjas_producez00(BgL_outchanz00_32, BgL_arg1352z00_381);
			}
			{	/* Jas/produce.scm 98 */
				obj_t BgL_arg1361z00_382;

				BgL_arg1361z00_382 =
					(((BgL_attributez00_bglt) COBJECT(
							((BgL_attributez00_bglt) BgL_attributez00_33)))->BgL_siza7eza7);
				BGl_outintz00zzjas_producez00(BgL_outchanz00_32, BgL_arg1361z00_382);
			}
			{	/* Jas/produce.scm 99 */
				obj_t BgL_infoz00_383;
				obj_t BgL_typez00_384;

				BgL_infoz00_383 =
					(((BgL_attributez00_bglt) COBJECT(
							((BgL_attributez00_bglt) BgL_attributez00_33)))->BgL_infoz00);
				BgL_typez00_384 =
					(((BgL_attributez00_bglt) COBJECT(
							((BgL_attributez00_bglt) BgL_attributez00_33)))->BgL_typez00);
				if ((BgL_typez00_384 == CNST_TABLE_REF(1)))
					{	/* Jas/produce.scm 100 */
						BGL_TAIL return
							BGl_producezd2stringzd2zzjas_producez00(BgL_outchanz00_32,
							BgL_infoz00_383);
					}
				else
					{	/* Jas/produce.scm 100 */
						if ((BgL_typez00_384 == CNST_TABLE_REF(2)))
							{	/* Jas/produce.scm 100 */
								BGL_TAIL return
									BGl_producezd2codezd2zzjas_producez00(BgL_outchanz00_32,
									BgL_infoz00_383);
							}
						else
							{	/* Jas/produce.scm 100 */
								if ((BgL_typez00_384 == CNST_TABLE_REF(3)))
									{	/* Jas/produce.scm 100 */
										BGL_TAIL return
											BGl_outshortz00zzjas_producez00(BgL_outchanz00_32,
											BgL_infoz00_383);
									}
								else
									{	/* Jas/produce.scm 100 */
										if ((BgL_typez00_384 == CNST_TABLE_REF(4)))
											{	/* Jas/produce.scm 100 */
												{	/* Jas/produce.scm 108 */
													long BgL_arg1367z00_390;

													BgL_arg1367z00_390 = bgl_list_length(BgL_infoz00_383);
													BGl_outshortz00zzjas_producez00(BgL_outchanz00_32,
														BINT(BgL_arg1367z00_390));
												}
												{
													obj_t BgL_l1217z00_392;

													{	/* Jas/produce.scm 109 */
														bool_t BgL_tmpz00_1099;

														BgL_l1217z00_392 = BgL_infoz00_383;
													BgL_zc3z04anonymousza31368ze3z87_393:
														if (PAIRP(BgL_l1217z00_392))
															{	/* Jas/produce.scm 109 */
																{	/* Jas/produce.scm 109 */
																	obj_t BgL_slotz00_395;

																	BgL_slotz00_395 = CAR(BgL_l1217z00_392);
																	{	/* Jas/produce.scm 109 */
																		obj_t BgL_arg1370z00_396;

																		BgL_arg1370z00_396 =
																			CAR(((obj_t) BgL_slotz00_395));
																		BGl_outshortz00zzjas_producez00
																			(BgL_outchanz00_32, BgL_arg1370z00_396);
																	}
																	{	/* Jas/produce.scm 110 */
																		obj_t BgL_arg1371z00_397;

																		BgL_arg1371z00_397 =
																			CDR(((obj_t) BgL_slotz00_395));
																		BGl_outshortz00zzjas_producez00
																			(BgL_outchanz00_32, BgL_arg1371z00_397);
																	}
																}
																{
																	obj_t BgL_l1217z00_1109;

																	BgL_l1217z00_1109 = CDR(BgL_l1217z00_392);
																	BgL_l1217z00_392 = BgL_l1217z00_1109;
																	goto BgL_zc3z04anonymousza31368ze3z87_393;
																}
															}
														else
															{	/* Jas/produce.scm 109 */
																BgL_tmpz00_1099 = ((bool_t) 1);
															}
														return BBOOL(BgL_tmpz00_1099);
													}
												}
											}
										else
											{	/* Jas/produce.scm 100 */
												if ((BgL_typez00_384 == CNST_TABLE_REF(5)))
													{	/* Jas/produce.scm 100 */
														{	/* Jas/produce.scm 113 */
															long BgL_arg1408z00_401;

															BgL_arg1408z00_401 =
																bgl_list_length(BgL_infoz00_383);
															BGl_outshortz00zzjas_producez00(BgL_outchanz00_32,
																BINT(BgL_arg1408z00_401));
														}
														{
															obj_t BgL_l1221z00_403;

															{	/* Jas/produce.scm 114 */
																bool_t BgL_tmpz00_1118;

																BgL_l1221z00_403 = BgL_infoz00_383;
															BgL_zc3z04anonymousza31409ze3z87_404:
																if (PAIRP(BgL_l1221z00_403))
																	{	/* Jas/produce.scm 114 */
																		{	/* Jas/produce.scm 114 */
																			obj_t BgL_slotz00_406;

																			BgL_slotz00_406 = CAR(BgL_l1221z00_403);
																			{
																				obj_t BgL_l1219z00_408;

																				BgL_l1219z00_408 = BgL_slotz00_406;
																			BgL_zc3z04anonymousza31411ze3z87_409:
																				if (PAIRP(BgL_l1219z00_408))
																					{	/* Jas/produce.scm 114 */
																						BGl_outshortz00zzjas_producez00
																							(BgL_outchanz00_32,
																							CAR(BgL_l1219z00_408));
																						{
																							obj_t BgL_l1219z00_1126;

																							BgL_l1219z00_1126 =
																								CDR(BgL_l1219z00_408);
																							BgL_l1219z00_408 =
																								BgL_l1219z00_1126;
																							goto
																								BgL_zc3z04anonymousza31411ze3z87_409;
																						}
																					}
																				else
																					{	/* Jas/produce.scm 114 */
																						((bool_t) 1);
																					}
																			}
																		}
																		{
																			obj_t BgL_l1221z00_1128;

																			BgL_l1221z00_1128 = CDR(BgL_l1221z00_403);
																			BgL_l1221z00_403 = BgL_l1221z00_1128;
																			goto BgL_zc3z04anonymousza31409ze3z87_404;
																		}
																	}
																else
																	{	/* Jas/produce.scm 114 */
																		BgL_tmpz00_1118 = ((bool_t) 1);
																	}
																return BBOOL(BgL_tmpz00_1118);
															}
														}
													}
												else
													{	/* Jas/produce.scm 100 */
														return
															BGl_errorz00zz__errorz00
															(BGl_string1577z00zzjas_producez00,
															BGl_string1578z00zzjas_producez00,
															BgL_typez00_384);
													}
											}
									}
							}
					}
			}
		}

	}



/* produce-code */
	obj_t BGl_producezd2codezd2zzjas_producez00(obj_t BgL_outchanz00_34,
		obj_t BgL_codez00_35)
	{
		{	/* Jas/produce.scm 119 */
			{
				obj_t BgL_maxstkz00_416;
				obj_t BgL_maxlocalz00_417;
				obj_t BgL_bytecodez00_418;
				obj_t BgL_handlersz00_419;
				obj_t BgL_attributesz00_420;

				if (PAIRP(BgL_codez00_35))
					{	/* Jas/produce.scm 120 */
						obj_t BgL_cdrzd2115zd2_425;

						BgL_cdrzd2115zd2_425 = CDR(((obj_t) BgL_codez00_35));
						if (PAIRP(BgL_cdrzd2115zd2_425))
							{	/* Jas/produce.scm 120 */
								obj_t BgL_cdrzd2122zd2_427;

								BgL_cdrzd2122zd2_427 = CDR(BgL_cdrzd2115zd2_425);
								if (PAIRP(BgL_cdrzd2122zd2_427))
									{	/* Jas/produce.scm 120 */
										obj_t BgL_cdrzd2128zd2_429;

										BgL_cdrzd2128zd2_429 = CDR(BgL_cdrzd2122zd2_427);
										if (PAIRP(BgL_cdrzd2128zd2_429))
											{	/* Jas/produce.scm 120 */
												obj_t BgL_arg1434z00_431;
												obj_t BgL_arg1437z00_432;
												obj_t BgL_arg1448z00_433;
												obj_t BgL_arg1453z00_434;
												obj_t BgL_arg1454z00_435;

												BgL_arg1434z00_431 = CAR(((obj_t) BgL_codez00_35));
												BgL_arg1437z00_432 = CAR(BgL_cdrzd2115zd2_425);
												BgL_arg1448z00_433 = CAR(BgL_cdrzd2122zd2_427);
												BgL_arg1453z00_434 = CAR(BgL_cdrzd2128zd2_429);
												BgL_arg1454z00_435 = CDR(BgL_cdrzd2128zd2_429);
												{	/* Jas/produce.scm 120 */
													bool_t BgL_tmpz00_1150;

													BgL_maxstkz00_416 = BgL_arg1434z00_431;
													BgL_maxlocalz00_417 = BgL_arg1437z00_432;
													BgL_bytecodez00_418 = BgL_arg1448z00_433;
													BgL_handlersz00_419 = BgL_arg1453z00_434;
													BgL_attributesz00_420 = BgL_arg1454z00_435;
													BGl_outshortz00zzjas_producez00(BgL_outchanz00_34,
														BgL_maxstkz00_416);
													BGl_outshortz00zzjas_producez00(BgL_outchanz00_34,
														BgL_maxlocalz00_417);
													{	/* Jas/produce.scm 124 */
														long BgL_arg1472z00_436;

														BgL_arg1472z00_436 =
															bgl_list_length(BgL_bytecodez00_418);
														BGl_outintz00zzjas_producez00(BgL_outchanz00_34,
															BINT(BgL_arg1472z00_436));
													}
													{
														obj_t BgL_l1223z00_438;

														BgL_l1223z00_438 = BgL_bytecodez00_418;
													BgL_zc3z04anonymousza31473ze3z87_439:
														if (PAIRP(BgL_l1223z00_438))
															{	/* Jas/produce.scm 125 */
																{	/* Jas/produce.scm 125 */
																	obj_t BgL_bz00_441;

																	BgL_bz00_441 = CAR(BgL_l1223z00_438);
																	{	/* Jas/produce.scm 12 */
																		unsigned char BgL_arg1230z00_769;

																		BgL_arg1230z00_769 =
																			(((long) CINT(BgL_bz00_441) & 255L));
																		BGL_OUTPUT_CHAR(BgL_outchanz00_34,
																			(char) (BgL_arg1230z00_769));
																}}
																{
																	obj_t BgL_l1223z00_1164;

																	BgL_l1223z00_1164 = CDR(BgL_l1223z00_438);
																	BgL_l1223z00_438 = BgL_l1223z00_1164;
																	goto BgL_zc3z04anonymousza31473ze3z87_439;
																}
															}
														else
															{	/* Jas/produce.scm 125 */
																((bool_t) 1);
															}
													}
													{	/* Jas/produce.scm 126 */
														long BgL_arg1489z00_444;

														BgL_arg1489z00_444 =
															bgl_list_length(BgL_handlersz00_419);
														BGl_outshortz00zzjas_producez00(BgL_outchanz00_34,
															BINT(BgL_arg1489z00_444));
													}
													{
														obj_t BgL_l1225z00_446;

														BgL_l1225z00_446 = BgL_handlersz00_419;
													BgL_zc3z04anonymousza31490ze3z87_447:
														if (PAIRP(BgL_l1225z00_446))
															{	/* Jas/produce.scm 127 */
																{	/* Jas/produce.scm 128 */
																	obj_t BgL_hz00_449;

																	BgL_hz00_449 = CAR(BgL_l1225z00_446);
																	{

																		if (PAIRP(BgL_hz00_449))
																			{	/* Jas/produce.scm 128 */
																				obj_t BgL_cdrzd2148zd2_458;

																				BgL_cdrzd2148zd2_458 =
																					CDR(((obj_t) BgL_hz00_449));
																				if (PAIRP(BgL_cdrzd2148zd2_458))
																					{	/* Jas/produce.scm 128 */
																						obj_t BgL_cdrzd2154zd2_460;

																						BgL_cdrzd2154zd2_460 =
																							CDR(BgL_cdrzd2148zd2_458);
																						if (PAIRP(BgL_cdrzd2154zd2_460))
																							{	/* Jas/produce.scm 128 */
																								obj_t BgL_cdrzd2159zd2_462;

																								BgL_cdrzd2159zd2_462 =
																									CDR(BgL_cdrzd2154zd2_460);
																								if (PAIRP(BgL_cdrzd2159zd2_462))
																									{	/* Jas/produce.scm 128 */
																										if (NULLP(CDR
																												(BgL_cdrzd2159zd2_462)))
																											{	/* Jas/produce.scm 128 */
																												obj_t
																													BgL_arg1509z00_466;
																												obj_t
																													BgL_arg1513z00_467;
																												obj_t
																													BgL_arg1514z00_468;
																												obj_t
																													BgL_arg1516z00_469;
																												BgL_arg1509z00_466 =
																													CAR(((obj_t)
																														BgL_hz00_449));
																												BgL_arg1513z00_467 =
																													CAR
																													(BgL_cdrzd2148zd2_458);
																												BgL_arg1514z00_468 =
																													CAR
																													(BgL_cdrzd2154zd2_460);
																												BgL_arg1516z00_469 =
																													CAR
																													(BgL_cdrzd2159zd2_462);
																												BGl_outshortz00zzjas_producez00
																													(BgL_outchanz00_34,
																													BgL_arg1509z00_466);
																												BGl_outshortz00zzjas_producez00
																													(BgL_outchanz00_34,
																													BgL_arg1513z00_467);
																												BGl_outshortz00zzjas_producez00
																													(BgL_outchanz00_34,
																													BgL_arg1514z00_468);
																												BGl_outshortz00zzjas_producez00
																													(BgL_outchanz00_34,
																													BgL_arg1516z00_469);
																											}
																										else
																											{	/* Jas/produce.scm 128 */
																											BgL_tagzd2137zd2_455:
																												BGl_errorz00zz__errorz00
																													(BGl_string1579z00zzjas_producez00,
																													BGl_string1580z00zzjas_producez00,
																													BgL_hz00_449);
																											}
																									}
																								else
																									{	/* Jas/produce.scm 128 */
																										goto BgL_tagzd2137zd2_455;
																									}
																							}
																						else
																							{	/* Jas/produce.scm 128 */
																								goto BgL_tagzd2137zd2_455;
																							}
																					}
																				else
																					{	/* Jas/produce.scm 128 */
																						goto BgL_tagzd2137zd2_455;
																					}
																			}
																		else
																			{	/* Jas/produce.scm 128 */
																				goto BgL_tagzd2137zd2_455;
																			}
																	}
																}
																{
																	obj_t BgL_l1225z00_1197;

																	BgL_l1225z00_1197 = CDR(BgL_l1225z00_446);
																	BgL_l1225z00_446 = BgL_l1225z00_1197;
																	goto BgL_zc3z04anonymousza31490ze3z87_447;
																}
															}
														else
															{	/* Jas/produce.scm 127 */
																((bool_t) 1);
															}
													}
													BgL_tmpz00_1150 =
														BGl_producezd2attributeszd2zzjas_producez00
														(BgL_outchanz00_34, BgL_attributesz00_420);
													return BBOOL(BgL_tmpz00_1150);
												}
											}
										else
											{	/* Jas/produce.scm 120 */
											BgL_tagzd2102zd2_422:
												return
													BGl_errorz00zz__errorz00
													(BGl_string1579z00zzjas_producez00,
													BGl_string1581z00zzjas_producez00, BgL_codez00_35);
											}
									}
								else
									{	/* Jas/produce.scm 120 */
										goto BgL_tagzd2102zd2_422;
									}
							}
						else
							{	/* Jas/produce.scm 120 */
								goto BgL_tagzd2102zd2_422;
							}
					}
				else
					{	/* Jas/produce.scm 120 */
						goto BgL_tagzd2102zd2_422;
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzjas_producez00(void)
	{
		{	/* Jas/produce.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzjas_producez00(void)
	{
		{	/* Jas/produce.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzjas_producez00(void)
	{
		{	/* Jas/produce.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzjas_producez00(void)
	{
		{	/* Jas/produce.scm 1 */
			BGl_modulezd2initializa7ationz75zzjas_classfilez00(135094074L,
				BSTRING_TO_STRING(BGl_string1582z00zzjas_producez00));
			return
				BGl_modulezd2initializa7ationz75zzjas_libz00(267157509L,
				BSTRING_TO_STRING(BGl_string1582z00zzjas_producez00));
		}

	}

#ifdef __cplusplus
}
#endif
