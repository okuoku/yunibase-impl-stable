/*===========================================================================*/
/*   (Jas/wide.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Jas/wide.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_JAS_WIDE_TYPE_DEFINITIONS
#define BGL_JAS_WIDE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_jastypez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_codez00;
		obj_t BgL_vectz00;
	}                 *BgL_jastypez00_bglt;

	typedef struct BgL_jasfunz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_codez00;
		obj_t BgL_vectz00;
		struct BgL_jastypez00_bgl *BgL_tretz00;
		obj_t BgL_targsz00;
	}                *BgL_jasfunz00_bglt;

	typedef struct BgL_classez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_codez00;
		obj_t BgL_vectz00;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_poolz00;
	}                *BgL_classez00_bglt;

	typedef struct BgL_fieldzd2orzd2methodz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}                             *BgL_fieldzd2orzd2methodz00_bglt;

	typedef struct BgL_fieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}               *BgL_fieldz00_bglt;

	typedef struct BgL_methodz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}                *BgL_methodz00_bglt;

	typedef struct BgL_classfilez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_currentzd2methodzd2;
		obj_t BgL_globalsz00;
		obj_t BgL_poolz00;
		obj_t BgL_poolzd2siza7ez75;
		obj_t BgL_pooledzd2nameszd2;
		obj_t BgL_flagsz00;
		obj_t BgL_mez00;
		obj_t BgL_superz00;
		obj_t BgL_interfacesz00;
		obj_t BgL_fieldsz00;
		obj_t BgL_methodsz00;
		obj_t BgL_attributesz00;
	}                   *BgL_classfilez00_bglt;


#endif													// BGL_JAS_WIDE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzjas_widez00 = BUNSPEC;
	extern int BGl_poolzd2doublezd2zzjas_classfilez00(BgL_classfilez00_bglt,
		double);
	extern int
		BGl_poolzd2classzd2byzd2reftypezd2zzjas_classfilez00(BgL_classfilez00_bglt,
		BgL_jastypez00_bglt);
	static obj_t BGl_widez00zzjas_widez00(obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzjas_widez00(void);
	BGL_IMPORT obj_t BGl_absz00zz__r4_numbers_6_5z00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_resolvezd2widezd2zzjas_widez00 = BUNSPEC;
	static obj_t BGl_genericzd2initzd2zzjas_widez00(void);
	static obj_t BGl_objectzd2initzd2zzjas_widez00(void);
	BGL_IMPORT obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern int BGl_poolzd2stringzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t);
	extern int
		BGl_poolzd2interfacezd2methodz00zzjas_classfilez00(BgL_classfilez00_bglt,
		BgL_methodz00_bglt);
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzjas_widez00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzjas_widez00(void);
	extern obj_t BGl_u2z00zzjas_libz00(int);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	BGL_IMPORT obj_t BGl_zb2zb2zz__r4_numbers_6_5z00(obj_t);
	extern int BGl_poolzd2floatzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		float);
	extern int BGl_poolzd2uint32zd2zzjas_classfilez00(BgL_classfilez00_bglt,
		uint32_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzjas_widez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_classfilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_libz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern int BGl_poolzd2methodzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		BgL_methodz00_bglt);
	extern int BGl_typezd2siza7ez75zzjas_classfilez00(BgL_jastypez00_bglt);
	extern int BGl_poolzd2llongzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		BGL_LONGLONG_T);
	extern int BGl_poolzd2longzd2zzjas_classfilez00(BgL_classfilez00_bglt, long);
	extern int BGl_poolzd2uint64zd2zzjas_classfilez00(BgL_classfilez00_bglt,
		uint64_t);
	extern int BGl_poolzd2intzd2zzjas_classfilez00(BgL_classfilez00_bglt, int);
	static obj_t BGl_libraryzd2moduleszd2initz00zzjas_widez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzjas_widez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzjas_widez00(void);
	extern int BGl_poolzd2elongzd2zzjas_classfilez00(BgL_classfilez00_bglt, long);
	extern obj_t BGl_jaszd2errorzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t, obj_t);
	extern int BGl_poolzd2classzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		BgL_classez00_bglt);
	extern int BGl_poolzd2int32zd2zzjas_classfilez00(BgL_classfilez00_bglt,
		int32_t);
	extern int BGl_poolzd2fieldzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		BgL_fieldz00_bglt);
	extern int BGl_poolzd2int64zd2zzjas_classfilez00(BgL_classfilez00_bglt,
		int64_t);
	extern int BGl_poolzd2namezd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t);
	static obj_t BGl_z62zc3z04resolvezd2wideza31210ze3z37zzjas_widez00(obj_t,
		obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1637z00zzjas_widez00,
		BgL_bgl_string1637za700za7za7j1643za7, "bad immediate value (ldc)", 25);
	      DEFINE_STRING(BGl_string1638z00zzjas_widez00,
		BgL_bgl_string1638za700za7za7j1644za7, "bad immediate value (ldc2)", 26);
	      DEFINE_STRING(BGl_string1639z00zzjas_widez00,
		BgL_bgl_string1639za700za7za7j1645za7, "too much locals", 15);
	      DEFINE_STRING(BGl_string1640z00zzjas_widez00,
		BgL_bgl_string1640za700za7za7j1646za7, "too large increment in iinc", 27);
	      DEFINE_STRING(BGl_string1641z00zzjas_widez00,
		BgL_bgl_string1641za700za7za7j1647za7, "bad localvar args", 17);
	      DEFINE_STRING(BGl_string1642z00zzjas_widez00,
		BgL_bgl_string1642za700za7za7j1648za7, "jas_wide", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1636z00zzjas_widez00,
		BgL_bgl_za762za7c3za704resolve1649za7,
		BGl_z62zc3z04resolvezd2wideza31210ze3z37zzjas_widez00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzjas_widez00));
		     ADD_ROOT((void *) (&BGl_resolvezd2widezd2zzjas_widez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzjas_widez00(long
		BgL_checksumz00_691, char *BgL_fromz00_692)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzjas_widez00))
				{
					BGl_requirezd2initializa7ationz75zzjas_widez00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzjas_widez00();
					BGl_libraryzd2moduleszd2initz00zzjas_widez00();
					BGl_importedzd2moduleszd2initz00zzjas_widez00();
					return BGl_toplevelzd2initzd2zzjas_widez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzjas_widez00(void)
	{
		{	/* Jas/wide.scm 1 */
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "jas_wide");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "jas_wide");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"jas_wide");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"jas_wide");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"jas_wide");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "jas_wide");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzjas_widez00(void)
	{
		{	/* Jas/wide.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzjas_widez00(void)
	{
		{	/* Jas/wide.scm 1 */
			return (BGl_resolvezd2widezd2zzjas_widez00 =
				BGl_proc1636z00zzjas_widez00, BUNSPEC);
		}

	}



/* &<@resolve-wide:1210> */
	obj_t BGl_z62zc3z04resolvezd2wideza31210ze3z37zzjas_widez00(obj_t
		BgL_envz00_680, obj_t BgL_classfilez00_681, obj_t BgL_codez00_682)
	{
		{	/* Jas/wide.scm 8 */
			if (NULLP(BgL_codez00_682))
				{	/* Jas/wide.scm 9 */
					return BNIL;
				}
			else
				{	/* Jas/wide.scm 9 */
					obj_t BgL_head1201z00_683;

					BgL_head1201z00_683 = MAKE_YOUNG_PAIR(BNIL, BNIL);
					{
						obj_t BgL_l1199z00_685;
						obj_t BgL_tail1202z00_686;

						BgL_l1199z00_685 = BgL_codez00_682;
						BgL_tail1202z00_686 = BgL_head1201z00_683;
					BgL_zc3z04anonymousza31212ze3z87_684:
						if (NULLP(BgL_l1199z00_685))
							{	/* Jas/wide.scm 9 */
								return CDR(BgL_head1201z00_683);
							}
						else
							{	/* Jas/wide.scm 9 */
								obj_t BgL_newtail1203z00_687;

								{	/* Jas/wide.scm 9 */
									obj_t BgL_arg1216z00_688;

									{	/* Jas/wide.scm 9 */
										obj_t BgL_xz00_689;

										BgL_xz00_689 = CAR(((obj_t) BgL_l1199z00_685));
										if (PAIRP(BgL_xz00_689))
											{	/* Jas/wide.scm 9 */
												BgL_arg1216z00_688 =
													BGl_widez00zzjas_widez00(BgL_classfilez00_681,
													CAR(BgL_xz00_689), CDR(BgL_xz00_689));
											}
										else
											{	/* Jas/wide.scm 9 */
												BgL_arg1216z00_688 = BgL_xz00_689;
											}
									}
									BgL_newtail1203z00_687 =
										MAKE_YOUNG_PAIR(BgL_arg1216z00_688, BNIL);
								}
								SET_CDR(BgL_tail1202z00_686, BgL_newtail1203z00_687);
								{	/* Jas/wide.scm 9 */
									obj_t BgL_arg1215z00_690;

									BgL_arg1215z00_690 = CDR(((obj_t) BgL_l1199z00_685));
									{
										obj_t BgL_tail1202z00_725;
										obj_t BgL_l1199z00_724;

										BgL_l1199z00_724 = BgL_arg1215z00_690;
										BgL_tail1202z00_725 = BgL_newtail1203z00_687;
										BgL_tail1202z00_686 = BgL_tail1202z00_725;
										BgL_l1199z00_685 = BgL_l1199z00_724;
										goto BgL_zc3z04anonymousza31212ze3z87_684;
									}
								}
							}
					}
				}
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzjas_widez00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_271;

				BgL_headz00_271 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_272;
					obj_t BgL_tailz00_273;

					BgL_prevz00_272 = BgL_headz00_271;
					BgL_tailz00_273 = BgL_l1z00_1;
				BgL_loopz00_274:
					if (PAIRP(BgL_tailz00_273))
						{
							obj_t BgL_newzd2prevzd2_276;

							BgL_newzd2prevzd2_276 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_273), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_272, BgL_newzd2prevzd2_276);
							{
								obj_t BgL_tailz00_733;
								obj_t BgL_prevz00_732;

								BgL_prevz00_732 = BgL_newzd2prevzd2_276;
								BgL_tailz00_733 = CDR(BgL_tailz00_273);
								BgL_tailz00_273 = BgL_tailz00_733;
								BgL_prevz00_272 = BgL_prevz00_732;
								goto BgL_loopz00_274;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_271);
				}
			}
		}

	}



/* wide */
	obj_t BGl_widez00zzjas_widez00(obj_t BgL_classfilez00_3, obj_t BgL_copz00_4,
		obj_t BgL_argsz00_5)
	{
		{	/* Jas/wide.scm 12 */
			if (INTEGERP(BgL_copz00_4))
				{	/* Jas/wide.scm 13 */
					switch ((long) CINT(BgL_copz00_4))
						{
						case 17L:

							{	/* Jas/wide.scm 15 */
								obj_t BgL_arg1225z00_281;

								{	/* Jas/wide.scm 15 */
									obj_t BgL_arg1226z00_282;

									BgL_arg1226z00_282 = CAR(((obj_t) BgL_argsz00_5));
									BgL_arg1225z00_281 =
										BGl_u2z00zzjas_libz00(CINT(BgL_arg1226z00_282));
								}
								return MAKE_YOUNG_PAIR(BgL_copz00_4, BgL_arg1225z00_281);
							}
							break;
						case 18L:

							{	/* Jas/wide.scm 17 */
								obj_t BgL_vz00_283;

								BgL_vz00_283 = CAR(((obj_t) BgL_argsz00_5));
								{
									int BgL_nz00_295;

									if (INTEGERP(BgL_vz00_283))
										{	/* Jas/wide.scm 19 */
											int BgL_arg1228z00_286;

											BgL_arg1228z00_286 =
												BGl_poolzd2intzd2zzjas_classfilez00(
												((BgL_classfilez00_bglt) BgL_classfilez00_3),
												CINT(BgL_vz00_283));
											BgL_nz00_295 = BgL_arg1228z00_286;
										BgL_zc3z04anonymousza31237ze3z87_296:
											if (((long) (BgL_nz00_295) < 256L))
												{	/* Jas/wide.scm 18 */
													obj_t BgL_list1240z00_298;

													{	/* Jas/wide.scm 18 */
														obj_t BgL_arg1242z00_299;

														BgL_arg1242z00_299 =
															MAKE_YOUNG_PAIR(BINT(BgL_nz00_295), BNIL);
														BgL_list1240z00_298 =
															MAKE_YOUNG_PAIR(BINT(18L), BgL_arg1242z00_299);
													}
													return BgL_list1240z00_298;
												}
											else
												{	/* Jas/wide.scm 18 */
													return
														MAKE_YOUNG_PAIR(BINT(19L),
														BGl_u2z00zzjas_libz00(BgL_nz00_295));
												}
										}
									else
										{	/* Jas/wide.scm 19 */
											if (BGL_INT32P(BgL_vz00_283))
												{	/* Jas/wide.scm 21 */
													int BgL_arg1230z00_288;

													BgL_arg1230z00_288 =
														BGl_poolzd2int32zd2zzjas_classfilez00(
														((BgL_classfilez00_bglt) BgL_classfilez00_3),
														BGL_BINT32_TO_INT32(BgL_vz00_283));
													{
														int BgL_nz00_765;

														BgL_nz00_765 = BgL_arg1230z00_288;
														BgL_nz00_295 = BgL_nz00_765;
														goto BgL_zc3z04anonymousza31237ze3z87_296;
													}
												}
											else
												{	/* Jas/wide.scm 21 */
													if (BGL_UINT32P(BgL_vz00_283))
														{	/* Jas/wide.scm 22 */
															int BgL_arg1232z00_290;

															BgL_arg1232z00_290 =
																BGl_poolzd2uint32zd2zzjas_classfilez00(
																((BgL_classfilez00_bglt) BgL_classfilez00_3),
																BGL_BUINT32_TO_UINT32(BgL_vz00_283));
															{
																int BgL_nz00_771;

																BgL_nz00_771 = BgL_arg1232z00_290;
																BgL_nz00_295 = BgL_nz00_771;
																goto BgL_zc3z04anonymousza31237ze3z87_296;
															}
														}
													else
														{	/* Jas/wide.scm 22 */
															if (REALP(BgL_vz00_283))
																{	/* Jas/wide.scm 23 */
																	int BgL_arg1234z00_292;

																	BgL_arg1234z00_292 =
																		BGl_poolzd2floatzd2zzjas_classfilez00(
																		((BgL_classfilez00_bglt)
																			BgL_classfilez00_3),
																		(float) (REAL_TO_DOUBLE(BgL_vz00_283)));
																	{
																		int BgL_nz00_778;

																		BgL_nz00_778 = BgL_arg1234z00_292;
																		BgL_nz00_295 = BgL_nz00_778;
																		goto BgL_zc3z04anonymousza31237ze3z87_296;
																	}
																}
															else
																{	/* Jas/wide.scm 23 */
																	if (STRINGP(BgL_vz00_283))
																		{	/* Jas/wide.scm 24 */
																			int BgL_arg1236z00_294;

																			BgL_arg1236z00_294 =
																				BGl_poolzd2stringzd2zzjas_classfilez00(
																				((BgL_classfilez00_bglt)
																					BgL_classfilez00_3), BgL_vz00_283);
																			{
																				int BgL_nz00_783;

																				BgL_nz00_783 = BgL_arg1236z00_294;
																				BgL_nz00_295 = BgL_nz00_783;
																				goto
																					BgL_zc3z04anonymousza31237ze3z87_296;
																			}
																		}
																	else
																		{	/* Jas/wide.scm 24 */
																			return
																				BGl_jaszd2errorzd2zzjas_classfilez00(
																				((BgL_classfilez00_bglt)
																					BgL_classfilez00_3),
																				BGl_string1637z00zzjas_widez00,
																				BgL_vz00_283);
																		}
																}
														}
												}
										}
								}
							}
							break;
						case 20L:

							{	/* Jas/wide.scm 27 */
								obj_t BgL_vz00_302;

								BgL_vz00_302 = CAR(((obj_t) BgL_argsz00_5));
								if (INTEGERP(BgL_vz00_302))
									{	/* Jas/wide.scm 28 */
										obj_t BgL_arg1248z00_304;

										{	/* Jas/wide.scm 28 */
											int BgL_arg1249z00_305;

											BgL_arg1249z00_305 =
												BGl_poolzd2longzd2zzjas_classfilez00(
												((BgL_classfilez00_bglt) BgL_classfilez00_3),
												(long) CINT(BgL_vz00_302));
											BgL_arg1248z00_304 =
												BGl_u2z00zzjas_libz00(BgL_arg1249z00_305);
										}
										return MAKE_YOUNG_PAIR(BINT(20L), BgL_arg1248z00_304);
									}
								else
									{	/* Jas/wide.scm 28 */
										if (REALP(BgL_vz00_302))
											{	/* Jas/wide.scm 29 */
												obj_t BgL_arg1252z00_307;

												{	/* Jas/wide.scm 29 */
													int BgL_arg1268z00_308;

													BgL_arg1268z00_308 =
														BGl_poolzd2doublezd2zzjas_classfilez00(
														((BgL_classfilez00_bglt) BgL_classfilez00_3),
														REAL_TO_DOUBLE(BgL_vz00_302));
													BgL_arg1252z00_307 =
														BGl_u2z00zzjas_libz00(BgL_arg1268z00_308);
												}
												return MAKE_YOUNG_PAIR(BINT(20L), BgL_arg1252z00_307);
											}
										else
											{	/* Jas/wide.scm 29 */
												if (ELONGP(BgL_vz00_302))
													{	/* Jas/wide.scm 30 */
														obj_t BgL_arg1272z00_310;

														{	/* Jas/wide.scm 30 */
															int BgL_arg1284z00_311;

															BgL_arg1284z00_311 =
																BGl_poolzd2elongzd2zzjas_classfilez00(
																((BgL_classfilez00_bglt) BgL_classfilez00_3),
																BELONG_TO_LONG(BgL_vz00_302));
															BgL_arg1272z00_310 =
																BGl_u2z00zzjas_libz00(BgL_arg1284z00_311);
														}
														return
															MAKE_YOUNG_PAIR(BINT(20L), BgL_arg1272z00_310);
													}
												else
													{	/* Jas/wide.scm 30 */
														if (LLONGP(BgL_vz00_302))
															{	/* Jas/wide.scm 31 */
																obj_t BgL_arg1304z00_313;

																{	/* Jas/wide.scm 31 */
																	int BgL_arg1305z00_314;

																	BgL_arg1305z00_314 =
																		BGl_poolzd2llongzd2zzjas_classfilez00(
																		((BgL_classfilez00_bglt)
																			BgL_classfilez00_3),
																		BLLONG_TO_LLONG(BgL_vz00_302));
																	BgL_arg1304z00_313 =
																		BGl_u2z00zzjas_libz00(BgL_arg1305z00_314);
																}
																return
																	MAKE_YOUNG_PAIR(BINT(20L),
																	BgL_arg1304z00_313);
															}
														else
															{	/* Jas/wide.scm 31 */
																if (BGL_INT64P(BgL_vz00_302))
																	{	/* Jas/wide.scm 32 */
																		obj_t BgL_arg1308z00_316;

																		{	/* Jas/wide.scm 32 */
																			int BgL_arg1310z00_317;

																			BgL_arg1310z00_317 =
																				BGl_poolzd2int64zd2zzjas_classfilez00(
																				((BgL_classfilez00_bglt)
																					BgL_classfilez00_3),
																				BGL_BINT64_TO_INT64(BgL_vz00_302));
																			BgL_arg1308z00_316 =
																				BGl_u2z00zzjas_libz00
																				(BgL_arg1310z00_317);
																		}
																		return
																			MAKE_YOUNG_PAIR(BINT(20L),
																			BgL_arg1308z00_316);
																	}
																else
																	{	/* Jas/wide.scm 32 */
																		if (BGL_UINT64P(BgL_vz00_302))
																			{	/* Jas/wide.scm 33 */
																				obj_t BgL_arg1312z00_319;

																				{	/* Jas/wide.scm 33 */
																					int BgL_arg1314z00_320;

																					BgL_arg1314z00_320 =
																						BGl_poolzd2uint64zd2zzjas_classfilez00
																						(((BgL_classfilez00_bglt)
																							BgL_classfilez00_3),
																						BGL_BINT64_TO_INT64(BgL_vz00_302));
																					BgL_arg1312z00_319 =
																						BGl_u2z00zzjas_libz00
																						(BgL_arg1314z00_320);
																				}
																				return
																					MAKE_YOUNG_PAIR(BINT(20L),
																					BgL_arg1312z00_319);
																			}
																		else
																			{	/* Jas/wide.scm 33 */
																				return
																					BGl_jaszd2errorzd2zzjas_classfilez00(
																					((BgL_classfilez00_bglt)
																						BgL_classfilez00_3),
																					BGl_string1638z00zzjas_widez00,
																					BgL_vz00_302);
																			}
																	}
															}
													}
											}
									}
							}
							break;
						case 21L:
						case 22L:
						case 23L:
						case 24L:
						case 25L:
						case 54L:
						case 55L:
						case 56L:
						case 57L:
						case 58L:

							{	/* Jas/wide.scm 36 */
								obj_t BgL_indexz00_321;

								BgL_indexz00_321 = CAR(((obj_t) BgL_argsz00_5));
								{	/* Jas/wide.scm 36 */
									long BgL_basez00_322;

									if (((long) CINT(BgL_copz00_4) > 50L))
										{	/* Jas/wide.scm 37 */
											BgL_basez00_322 = 54L;
										}
									else
										{	/* Jas/wide.scm 37 */
											BgL_basez00_322 = 21L;
										}
									{	/* Jas/wide.scm 37 */
										long BgL_ismallz00_323;

										BgL_ismallz00_323 =
											(
											(BgL_basez00_322 + 5L) +
											(((long) CINT(BgL_copz00_4) - BgL_basez00_322) * 4L));
										{	/* Jas/wide.scm 38 */

											if (((long) CINT(BgL_indexz00_321) < 4L))
												{	/* Jas/wide.scm 39 */
													obj_t BgL_arg1316z00_325;

													if (INTEGERP(BgL_indexz00_321))
														{	/* Jas/wide.scm 39 */
															obj_t BgL_za71za7_615;

															BgL_za71za7_615 = BINT(BgL_ismallz00_323);
															{	/* Jas/wide.scm 39 */
																obj_t BgL_tmpz00_617;

																BgL_tmpz00_617 = BINT(0L);
																if (BGL_ADDFX_OV(BgL_za71za7_615,
																		BgL_indexz00_321, BgL_tmpz00_617))
																	{	/* Jas/wide.scm 39 */
																		BgL_arg1316z00_325 =
																			bgl_bignum_add(bgl_long_to_bignum(
																				(long) CINT(BgL_za71za7_615)),
																			bgl_long_to_bignum(
																				(long) CINT(BgL_indexz00_321)));
																	}
																else
																	{	/* Jas/wide.scm 39 */
																		BgL_arg1316z00_325 = BgL_tmpz00_617;
																	}
															}
														}
													else
														{	/* Jas/wide.scm 39 */
															BgL_arg1316z00_325 =
																BGl_2zb2zb2zz__r4_numbers_6_5z00(BINT
																(BgL_ismallz00_323), BgL_indexz00_321);
														}
													{	/* Jas/wide.scm 39 */
														obj_t BgL_list1317z00_326;

														BgL_list1317z00_326 =
															MAKE_YOUNG_PAIR(BgL_arg1316z00_325, BNIL);
														return BgL_list1317z00_326;
													}
												}
											else
												{	/* Jas/wide.scm 39 */
													if (((long) CINT(BgL_indexz00_321) < 256L))
														{	/* Jas/wide.scm 40 */
															obj_t BgL_list1320z00_329;

															{	/* Jas/wide.scm 40 */
																obj_t BgL_arg1321z00_330;

																BgL_arg1321z00_330 =
																	MAKE_YOUNG_PAIR(BgL_indexz00_321, BNIL);
																BgL_list1320z00_329 =
																	MAKE_YOUNG_PAIR(BgL_copz00_4,
																	BgL_arg1321z00_330);
															}
															return BgL_list1320z00_329;
														}
													else
														{	/* Jas/wide.scm 40 */
															if (((long) CINT(BgL_indexz00_321) < 65536L))
																{	/* Jas/wide.scm 41 */
																	obj_t BgL_arg1323z00_332;

																	BgL_arg1323z00_332 =
																		MAKE_YOUNG_PAIR(BgL_copz00_4,
																		BGl_u2z00zzjas_libz00(CINT
																			(BgL_indexz00_321)));
																	return MAKE_YOUNG_PAIR(BINT(196L),
																		BgL_arg1323z00_332);
																}
															else
																{	/* Jas/wide.scm 41 */
																	return
																		BGl_jaszd2errorzd2zzjas_classfilez00(
																		((BgL_classfilez00_bglt)
																			BgL_classfilez00_3),
																		BGl_string1639z00zzjas_widez00,
																		BgL_indexz00_321);
																}
														}
												}
										}
									}
								}
							}
							break;
						case 132L:

							{	/* Jas/wide.scm 45 */
								obj_t BgL_indexz00_338;
								obj_t BgL_nz00_339;

								BgL_indexz00_338 = CAR(((obj_t) BgL_argsz00_5));
								{	/* Jas/wide.scm 45 */
									obj_t BgL_pairz00_633;

									BgL_pairz00_633 = CDR(((obj_t) BgL_argsz00_5));
									BgL_nz00_339 = CAR(BgL_pairz00_633);
								}
								{	/* Jas/wide.scm 47 */
									bool_t BgL_test1676z00_885;

									if (((long) CINT(BgL_indexz00_338) < 256L))
										{	/* Jas/wide.scm 47 */
											if (((long) CINT(BgL_nz00_339) > -129L))
												{	/* Jas/wide.scm 47 */
													BgL_test1676z00_885 =
														((long) CINT(BgL_nz00_339) < 128L);
												}
											else
												{	/* Jas/wide.scm 47 */
													BgL_test1676z00_885 = ((bool_t) 0);
												}
										}
									else
										{	/* Jas/wide.scm 47 */
											BgL_test1676z00_885 = ((bool_t) 0);
										}
									if (BgL_test1676z00_885)
										{	/* Jas/wide.scm 48 */
											obj_t BgL_list1333z00_343;

											{	/* Jas/wide.scm 48 */
												obj_t BgL_arg1335z00_344;

												{	/* Jas/wide.scm 48 */
													obj_t BgL_arg1339z00_345;

													BgL_arg1339z00_345 =
														MAKE_YOUNG_PAIR(BgL_nz00_339, BNIL);
													BgL_arg1335z00_344 =
														MAKE_YOUNG_PAIR(BgL_indexz00_338,
														BgL_arg1339z00_345);
												}
												BgL_list1333z00_343 =
													MAKE_YOUNG_PAIR(BINT(132L), BgL_arg1335z00_344);
											}
											return BgL_list1333z00_343;
										}
									else
										{	/* Jas/wide.scm 47 */
											if (((long) CINT(BgL_indexz00_338) >= 65536L))
												{	/* Jas/wide.scm 49 */
													return
														BGl_jaszd2errorzd2zzjas_classfilez00(
														((BgL_classfilez00_bglt) BgL_classfilez00_3),
														BGl_string1639z00zzjas_widez00, BgL_indexz00_338);
												}
											else
												{	/* Jas/wide.scm 49 */
													if (
														((long)
															CINT(BGl_absz00zz__r4_numbers_6_5z00
																(BgL_nz00_339)) >= 32768L))
														{	/* Jas/wide.scm 51 */
															return
																BGl_jaszd2errorzd2zzjas_classfilez00(
																((BgL_classfilez00_bglt) BgL_classfilez00_3),
																BGl_string1640z00zzjas_widez00, BgL_nz00_339);
														}
													else
														{	/* Jas/wide.scm 53 */
															obj_t BgL_arg1346z00_349;

															{	/* Jas/wide.scm 53 */
																obj_t BgL_arg1348z00_350;

																BgL_arg1348z00_350 =
																	MAKE_YOUNG_PAIR(BGl_u2z00zzjas_libz00(CINT
																		(BgL_indexz00_338)),
																	BGl_u2z00zzjas_libz00(CINT(BgL_nz00_339)));
																BgL_arg1346z00_349 =
																	MAKE_YOUNG_PAIR(BINT(132L),
																	BgL_arg1348z00_350);
															}
															return
																MAKE_YOUNG_PAIR(BINT(196L), BgL_arg1346z00_349);
														}
												}
										}
								}
							}
							break;
						case 169L:

							{	/* Jas/wide.scm 55 */
								obj_t BgL_indexz00_356;

								BgL_indexz00_356 = CAR(((obj_t) BgL_argsz00_5));
								if (((long) CINT(BgL_indexz00_356) < 256L))
									{	/* Jas/wide.scm 56 */
										obj_t BgL_list1355z00_358;

										{	/* Jas/wide.scm 56 */
											obj_t BgL_arg1361z00_359;

											BgL_arg1361z00_359 =
												MAKE_YOUNG_PAIR(BgL_indexz00_356, BNIL);
											BgL_list1355z00_358 =
												MAKE_YOUNG_PAIR(BINT(169L), BgL_arg1361z00_359);
										}
										return BgL_list1355z00_358;
									}
								else
									{	/* Jas/wide.scm 56 */
										if (((long) CINT(BgL_indexz00_356) < 65536L))
											{	/* Jas/wide.scm 57 */
												obj_t BgL_arg1364z00_361;

												BgL_arg1364z00_361 =
													MAKE_YOUNG_PAIR(BINT(169L),
													BGl_u2z00zzjas_libz00(CINT(BgL_indexz00_356)));
												return MAKE_YOUNG_PAIR(BINT(196L), BgL_arg1364z00_361);
											}
										else
											{	/* Jas/wide.scm 57 */
												return
													BGl_jaszd2errorzd2zzjas_classfilez00(
													((BgL_classfilez00_bglt) BgL_classfilez00_3),
													BGl_string1639z00zzjas_widez00, BgL_indexz00_356);
											}
									}
							}
							break;
						case 178L:
						case 179L:
						case 180L:
						case 181L:

							{	/* Jas/wide.scm 61 */
								obj_t BgL_arg1370z00_363;

								{	/* Jas/wide.scm 61 */
									int BgL_arg1371z00_364;

									{	/* Jas/wide.scm 61 */
										obj_t BgL_arg1375z00_365;

										BgL_arg1375z00_365 = CAR(((obj_t) BgL_argsz00_5));
										BgL_arg1371z00_364 =
											BGl_poolzd2fieldzd2zzjas_classfilez00(
											((BgL_classfilez00_bglt) BgL_classfilez00_3),
											((BgL_fieldz00_bglt) BgL_arg1375z00_365));
									}
									BgL_arg1370z00_363 =
										BGl_u2z00zzjas_libz00(BgL_arg1371z00_364);
								}
								return MAKE_YOUNG_PAIR(BgL_copz00_4, BgL_arg1370z00_363);
							}
							break;
						case 182L:
						case 183L:
						case 184L:

							{	/* Jas/wide.scm 63 */
								obj_t BgL_arg1376z00_366;

								{	/* Jas/wide.scm 63 */
									int BgL_arg1377z00_367;

									{	/* Jas/wide.scm 63 */
										obj_t BgL_arg1378z00_368;

										BgL_arg1378z00_368 = CAR(((obj_t) BgL_argsz00_5));
										BgL_arg1377z00_367 =
											BGl_poolzd2methodzd2zzjas_classfilez00(
											((BgL_classfilez00_bglt) BgL_classfilez00_3),
											((BgL_methodz00_bglt) BgL_arg1378z00_368));
									}
									BgL_arg1376z00_366 =
										BGl_u2z00zzjas_libz00(BgL_arg1377z00_367);
								}
								return MAKE_YOUNG_PAIR(BgL_copz00_4, BgL_arg1376z00_366);
							}
							break;
						case 185L:

							{	/* Jas/wide.scm 65 */
								obj_t BgL_mz00_369;

								BgL_mz00_369 = CAR(((obj_t) BgL_argsz00_5));
								{	/* Jas/wide.scm 66 */
									BgL_jasfunz00_bglt BgL_i1059z00_370;

									BgL_i1059z00_370 =
										((BgL_jasfunz00_bglt)
										(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
													((BgL_fieldzd2orzd2methodz00_bglt)
														((BgL_methodz00_bglt) BgL_mz00_369))))->
											BgL_typez00));
									{	/* Jas/wide.scm 67 */
										obj_t BgL_arg1379z00_371;

										{	/* Jas/wide.scm 67 */
											obj_t BgL_arg1380z00_372;
											obj_t BgL_arg1408z00_373;

											{	/* Jas/wide.scm 67 */
												int BgL_arg1410z00_374;

												BgL_arg1410z00_374 =
													BGl_poolzd2interfacezd2methodz00zzjas_classfilez00(
													((BgL_classfilez00_bglt) BgL_classfilez00_3),
													((BgL_methodz00_bglt) BgL_mz00_369));
												BgL_arg1380z00_372 =
													BGl_u2z00zzjas_libz00(BgL_arg1410z00_374);
											}
											{	/* Jas/wide.scm 68 */
												long BgL_arg1421z00_375;

												{	/* Jas/wide.scm 68 */
													obj_t BgL_arg1437z00_378;

													{	/* Jas/wide.scm 68 */
														obj_t BgL_runner1474z00_395;

														{	/* Jas/wide.scm 68 */
															obj_t BgL_l1204z00_379;

															BgL_l1204z00_379 =
																(((BgL_jasfunz00_bglt)
																	COBJECT(BgL_i1059z00_370))->BgL_targsz00);
															if (NULLP(BgL_l1204z00_379))
																{	/* Jas/wide.scm 68 */
																	BgL_runner1474z00_395 = BNIL;
																}
															else
																{	/* Jas/wide.scm 68 */
																	obj_t BgL_head1206z00_381;

																	{	/* Jas/wide.scm 68 */
																		int BgL_arg1472z00_393;

																		{	/* Jas/wide.scm 68 */
																			obj_t BgL_arg1473z00_394;

																			BgL_arg1473z00_394 =
																				CAR(((obj_t) BgL_l1204z00_379));
																			BgL_arg1472z00_393 =
																				BGl_typezd2siza7ez75zzjas_classfilez00(
																				((BgL_jastypez00_bglt)
																					BgL_arg1473z00_394));
																		}
																		BgL_head1206z00_381 =
																			MAKE_YOUNG_PAIR(BINT(BgL_arg1472z00_393),
																			BNIL);
																	}
																	{	/* Jas/wide.scm 68 */
																		obj_t BgL_g1209z00_382;

																		BgL_g1209z00_382 =
																			CDR(((obj_t) BgL_l1204z00_379));
																		{
																			obj_t BgL_l1204z00_384;
																			obj_t BgL_tail1207z00_385;

																			BgL_l1204z00_384 = BgL_g1209z00_382;
																			BgL_tail1207z00_385 = BgL_head1206z00_381;
																		BgL_zc3z04anonymousza31439ze3z87_386:
																			if (NULLP(BgL_l1204z00_384))
																				{	/* Jas/wide.scm 68 */
																					BgL_runner1474z00_395 =
																						BgL_head1206z00_381;
																				}
																			else
																				{	/* Jas/wide.scm 68 */
																					obj_t BgL_newtail1208z00_388;

																					{	/* Jas/wide.scm 68 */
																						int BgL_arg1453z00_390;

																						{	/* Jas/wide.scm 68 */
																							obj_t BgL_arg1454z00_391;

																							BgL_arg1454z00_391 =
																								CAR(((obj_t) BgL_l1204z00_384));
																							BgL_arg1453z00_390 =
																								BGl_typezd2siza7ez75zzjas_classfilez00
																								(((BgL_jastypez00_bglt)
																									BgL_arg1454z00_391));
																						}
																						BgL_newtail1208z00_388 =
																							MAKE_YOUNG_PAIR(BINT
																							(BgL_arg1453z00_390), BNIL);
																					}
																					SET_CDR(BgL_tail1207z00_385,
																						BgL_newtail1208z00_388);
																					{	/* Jas/wide.scm 68 */
																						obj_t BgL_arg1448z00_389;

																						BgL_arg1448z00_389 =
																							CDR(((obj_t) BgL_l1204z00_384));
																						{
																							obj_t BgL_tail1207z00_984;
																							obj_t BgL_l1204z00_983;

																							BgL_l1204z00_983 =
																								BgL_arg1448z00_389;
																							BgL_tail1207z00_984 =
																								BgL_newtail1208z00_388;
																							BgL_tail1207z00_385 =
																								BgL_tail1207z00_984;
																							BgL_l1204z00_384 =
																								BgL_l1204z00_983;
																							goto
																								BgL_zc3z04anonymousza31439ze3z87_386;
																						}
																					}
																				}
																		}
																	}
																}
														}
														BgL_arg1437z00_378 =
															BGl_zb2zb2zz__r4_numbers_6_5z00
															(BgL_runner1474z00_395);
													}
													BgL_arg1421z00_375 =
														(1L + (long) CINT(BgL_arg1437z00_378));
												}
												{	/* Jas/wide.scm 68 */
													obj_t BgL_list1422z00_376;

													{	/* Jas/wide.scm 68 */
														obj_t BgL_arg1434z00_377;

														BgL_arg1434z00_377 =
															MAKE_YOUNG_PAIR(BINT(0L), BNIL);
														BgL_list1422z00_376 =
															MAKE_YOUNG_PAIR(BINT(BgL_arg1421z00_375),
															BgL_arg1434z00_377);
													}
													BgL_arg1408z00_373 = BgL_list1422z00_376;
											}}
											BgL_arg1379z00_371 =
												BGl_appendzd221011zd2zzjas_widez00(BgL_arg1380z00_372,
												BgL_arg1408z00_373);
										}
										return MAKE_YOUNG_PAIR(BgL_copz00_4, BgL_arg1379z00_371);
									}
								}
							}
							break;
						case 187L:

							{	/* Jas/wide.scm 71 */
								obj_t BgL_arg1485z00_396;

								{	/* Jas/wide.scm 71 */
									int BgL_arg1489z00_397;

									{	/* Jas/wide.scm 71 */
										obj_t BgL_arg1502z00_398;

										BgL_arg1502z00_398 = CAR(((obj_t) BgL_argsz00_5));
										BgL_arg1489z00_397 =
											BGl_poolzd2classzd2zzjas_classfilez00(
											((BgL_classfilez00_bglt) BgL_classfilez00_3),
											((BgL_classez00_bglt) BgL_arg1502z00_398));
									}
									BgL_arg1485z00_396 =
										BGl_u2z00zzjas_libz00(BgL_arg1489z00_397);
								}
								return MAKE_YOUNG_PAIR(BgL_copz00_4, BgL_arg1485z00_396);
							}
							break;
						case 189L:
						case 192L:
						case 193L:

							{	/* Jas/wide.scm 73 */
								obj_t BgL_arg1509z00_399;

								{	/* Jas/wide.scm 73 */
									int BgL_arg1513z00_400;

									{	/* Jas/wide.scm 73 */
										obj_t BgL_arg1514z00_401;

										BgL_arg1514z00_401 = CAR(((obj_t) BgL_argsz00_5));
										BgL_arg1513z00_400 =
											BGl_poolzd2classzd2byzd2reftypezd2zzjas_classfilez00(
											((BgL_classfilez00_bglt) BgL_classfilez00_3),
											((BgL_jastypez00_bglt) BgL_arg1514z00_401));
									}
									BgL_arg1509z00_399 =
										BGl_u2z00zzjas_libz00(BgL_arg1513z00_400);
								}
								return MAKE_YOUNG_PAIR(BgL_copz00_4, BgL_arg1509z00_399);
							}
							break;
						case 197L:

							{	/* Jas/wide.scm 75 */
								obj_t BgL_arg1516z00_402;

								{	/* Jas/wide.scm 75 */
									obj_t BgL_arg1535z00_403;
									obj_t BgL_arg1540z00_404;

									{	/* Jas/wide.scm 75 */
										int BgL_arg1544z00_405;

										{	/* Jas/wide.scm 75 */
											obj_t BgL_arg1546z00_406;

											BgL_arg1546z00_406 = CAR(((obj_t) BgL_argsz00_5));
											BgL_arg1544z00_405 =
												BGl_poolzd2classzd2byzd2reftypezd2zzjas_classfilez00(
												((BgL_classfilez00_bglt) BgL_classfilez00_3),
												((BgL_jastypez00_bglt) BgL_arg1546z00_406));
										}
										BgL_arg1535z00_403 =
											BGl_u2z00zzjas_libz00(BgL_arg1544z00_405);
									}
									{	/* Jas/wide.scm 76 */
										obj_t BgL_arg1552z00_407;

										{	/* Jas/wide.scm 76 */
											obj_t BgL_pairz00_662;

											BgL_pairz00_662 = CDR(((obj_t) BgL_argsz00_5));
											BgL_arg1552z00_407 = CAR(BgL_pairz00_662);
										}
										{	/* Jas/wide.scm 76 */
											obj_t BgL_list1553z00_408;

											BgL_list1553z00_408 =
												MAKE_YOUNG_PAIR(BgL_arg1552z00_407, BNIL);
											BgL_arg1540z00_404 = BgL_list1553z00_408;
									}}
									BgL_arg1516z00_402 =
										BGl_appendzd221011zd2zzjas_widez00(BgL_arg1535z00_403,
										BgL_arg1540z00_404);
								}
								return MAKE_YOUNG_PAIR(BgL_copz00_4, BgL_arg1516z00_402);
							}
							break;
						case 205L:

							{
								obj_t BgL_begz00_409;
								obj_t BgL_endz00_410;
								obj_t BgL_namez00_411;
								obj_t BgL_typez00_412;
								obj_t BgL_indexz00_413;

								if (PAIRP(BgL_argsz00_5))
									{	/* Jas/wide.scm 77 */
										obj_t BgL_cdrzd2115zd2_418;

										BgL_cdrzd2115zd2_418 = CDR(((obj_t) BgL_argsz00_5));
										if (PAIRP(BgL_cdrzd2115zd2_418))
											{	/* Jas/wide.scm 77 */
												obj_t BgL_cdrzd2122zd2_420;

												BgL_cdrzd2122zd2_420 = CDR(BgL_cdrzd2115zd2_418);
												if (PAIRP(BgL_cdrzd2122zd2_420))
													{	/* Jas/wide.scm 77 */
														obj_t BgL_cdrzd2128zd2_422;

														BgL_cdrzd2128zd2_422 = CDR(BgL_cdrzd2122zd2_420);
														if (PAIRP(BgL_cdrzd2128zd2_422))
															{	/* Jas/wide.scm 77 */
																obj_t BgL_cdrzd2133zd2_424;

																BgL_cdrzd2133zd2_424 =
																	CDR(BgL_cdrzd2128zd2_422);
																if (PAIRP(BgL_cdrzd2133zd2_424))
																	{	/* Jas/wide.scm 77 */
																		if (NULLP(CDR(BgL_cdrzd2133zd2_424)))
																			{	/* Jas/wide.scm 77 */
																				obj_t BgL_arg1564z00_428;
																				obj_t BgL_arg1565z00_429;
																				obj_t BgL_arg1571z00_430;
																				obj_t BgL_arg1573z00_431;
																				obj_t BgL_arg1575z00_432;

																				BgL_arg1564z00_428 =
																					CAR(((obj_t) BgL_argsz00_5));
																				BgL_arg1565z00_429 =
																					CAR(BgL_cdrzd2115zd2_418);
																				BgL_arg1571z00_430 =
																					CAR(BgL_cdrzd2122zd2_420);
																				BgL_arg1573z00_431 =
																					CAR(BgL_cdrzd2128zd2_422);
																				BgL_arg1575z00_432 =
																					CAR(BgL_cdrzd2133zd2_424);
																				BgL_begz00_409 = BgL_arg1564z00_428;
																				BgL_endz00_410 = BgL_arg1565z00_429;
																				BgL_namez00_411 = BgL_arg1571z00_430;
																				BgL_typez00_412 = BgL_arg1573z00_431;
																				BgL_indexz00_413 = BgL_arg1575z00_432;
																				{	/* Jas/wide.scm 80 */
																					int BgL_arg1584z00_434;
																					int BgL_arg1585z00_435;

																					BgL_arg1584z00_434 =
																						BGl_poolzd2namezd2zzjas_classfilez00
																						(((BgL_classfilez00_bglt)
																							BgL_classfilez00_3),
																						BgL_namez00_411);
																					{	/* Jas/wide.scm 81 */
																						obj_t BgL_arg1602z00_442;

																						BgL_arg1602z00_442 =
																							(((BgL_jastypez00_bglt) COBJECT(
																									((BgL_jastypez00_bglt)
																										BgL_typez00_412)))->
																							BgL_codez00);
																						BgL_arg1585z00_435 =
																							BGl_poolzd2namezd2zzjas_classfilez00
																							(((BgL_classfilez00_bglt)
																								BgL_classfilez00_3),
																							BgL_arg1602z00_442);
																					}
																					{	/* Jas/wide.scm 80 */
																						obj_t BgL_list1586z00_436;

																						{	/* Jas/wide.scm 80 */
																							obj_t BgL_arg1589z00_437;

																							{	/* Jas/wide.scm 80 */
																								obj_t BgL_arg1591z00_438;

																								{	/* Jas/wide.scm 80 */
																									obj_t BgL_arg1593z00_439;

																									{	/* Jas/wide.scm 80 */
																										obj_t BgL_arg1594z00_440;

																										{	/* Jas/wide.scm 80 */
																											obj_t BgL_arg1595z00_441;

																											BgL_arg1595z00_441 =
																												MAKE_YOUNG_PAIR
																												(BgL_indexz00_413,
																												BNIL);
																											BgL_arg1594z00_440 =
																												MAKE_YOUNG_PAIR(BINT
																												(BgL_arg1585z00_435),
																												BgL_arg1595z00_441);
																										}
																										BgL_arg1593z00_439 =
																											MAKE_YOUNG_PAIR(BINT
																											(BgL_arg1584z00_434),
																											BgL_arg1594z00_440);
																									}
																									BgL_arg1591z00_438 =
																										MAKE_YOUNG_PAIR
																										(BgL_endz00_410,
																										BgL_arg1593z00_439);
																								}
																								BgL_arg1589z00_437 =
																									MAKE_YOUNG_PAIR
																									(BgL_begz00_409,
																									BgL_arg1591z00_438);
																							}
																							BgL_list1586z00_436 =
																								MAKE_YOUNG_PAIR(BgL_copz00_4,
																								BgL_arg1589z00_437);
																						}
																						return BgL_list1586z00_436;
																					}
																				}
																			}
																		else
																			{	/* Jas/wide.scm 77 */
																			BgL_tagzd2102zd2_415:
																				return
																					BGl_jaszd2errorzd2zzjas_classfilez00(
																					((BgL_classfilez00_bglt)
																						BgL_classfilez00_3),
																					BGl_string1641z00zzjas_widez00,
																					BgL_argsz00_5);
																			}
																	}
																else
																	{	/* Jas/wide.scm 77 */
																		goto BgL_tagzd2102zd2_415;
																	}
															}
														else
															{	/* Jas/wide.scm 77 */
																goto BgL_tagzd2102zd2_415;
															}
													}
												else
													{	/* Jas/wide.scm 77 */
														goto BgL_tagzd2102zd2_415;
													}
											}
										else
											{	/* Jas/wide.scm 77 */
												goto BgL_tagzd2102zd2_415;
											}
									}
								else
									{	/* Jas/wide.scm 77 */
										goto BgL_tagzd2102zd2_415;
									}
							}
							break;
						default:
							return MAKE_YOUNG_PAIR(BgL_copz00_4, BgL_argsz00_5);
						}
				}
			else
				{	/* Jas/wide.scm 13 */
					return MAKE_YOUNG_PAIR(BgL_copz00_4, BgL_argsz00_5);
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzjas_widez00(void)
	{
		{	/* Jas/wide.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzjas_widez00(void)
	{
		{	/* Jas/wide.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzjas_widez00(void)
	{
		{	/* Jas/wide.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzjas_widez00(void)
	{
		{	/* Jas/wide.scm 1 */
			BGl_modulezd2initializa7ationz75zzjas_libz00(267157509L,
				BSTRING_TO_STRING(BGl_string1642z00zzjas_widez00));
			return
				BGl_modulezd2initializa7ationz75zzjas_classfilez00(135094074L,
				BSTRING_TO_STRING(BGl_string1642z00zzjas_widez00));
		}

	}

#ifdef __cplusplus
}
#endif
