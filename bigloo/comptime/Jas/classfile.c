/*===========================================================================*/
/*   (Jas/classfile.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Jas/classfile.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_JAS_CLASSFILE_TYPE_DEFINITIONS
#define BGL_JAS_CLASSFILE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_jastypez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_codez00;
		obj_t BgL_vectz00;
	}                 *BgL_jastypez00_bglt;

	typedef struct BgL_basicz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_codez00;
		obj_t BgL_vectz00;
	}               *BgL_basicz00_bglt;

	typedef struct BgL_vectz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_codez00;
		obj_t BgL_vectz00;
		struct BgL_jastypez00_bgl *BgL_typez00;
	}              *BgL_vectz00_bglt;

	typedef struct BgL_jasfunz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_codez00;
		obj_t BgL_vectz00;
		struct BgL_jastypez00_bgl *BgL_tretz00;
		obj_t BgL_targsz00;
	}                *BgL_jasfunz00_bglt;

	typedef struct BgL_classez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_codez00;
		obj_t BgL_vectz00;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_poolz00;
	}                *BgL_classez00_bglt;

	typedef struct BgL_fieldzd2orzd2methodz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}                             *BgL_fieldzd2orzd2methodz00_bglt;

	typedef struct BgL_fieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}               *BgL_fieldz00_bglt;

	typedef struct BgL_methodz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}                *BgL_methodz00_bglt;

	typedef struct BgL_attributez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_typez00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_infoz00;
	}                   *BgL_attributez00_bglt;

	typedef struct BgL_classfilez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_currentzd2methodzd2;
		obj_t BgL_globalsz00;
		obj_t BgL_poolz00;
		obj_t BgL_poolzd2siza7ez75;
		obj_t BgL_pooledzd2nameszd2;
		obj_t BgL_flagsz00;
		obj_t BgL_mez00;
		obj_t BgL_superz00;
		obj_t BgL_interfacesz00;
		obj_t BgL_fieldsz00;
		obj_t BgL_methodsz00;
		obj_t BgL_attributesz00;
	}                   *BgL_classfilez00_bglt;


#endif													// BGL_JAS_CLASSFILE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62classezd2vectzd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62methodzd2attributeszb0zzjas_classfilez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_attributezd2typezd2setz12z12zzjas_classfilez00(BgL_attributez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_basiczd2vectzd2zzjas_classfilez00(BgL_basicz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_attributezd2typezd2zzjas_classfilez00(BgL_attributez00_bglt);
	BGL_EXPORTED_DECL BgL_fieldz00_bglt
		BGl_declaredzd2fieldzd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32010ze3ze5zzjas_classfilez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32002ze3ze5zzjas_classfilez00(obj_t);
	static obj_t BGl_z62JasTypezd2codezb0zzjas_classfilez00(obj_t, obj_t);
	static BgL_jastypez00_bglt BGl_z62makezd2JasTypezb0zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2methodszd2setz12z12zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t);
	static obj_t BGl_z62lambda1930z62zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1931z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31919ze3ze5zzjas_classfilez00(obj_t,
		obj_t);
	static BgL_classez00_bglt BGl_z62lambda1937z62zzjas_classfilez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_classez00_bglt BGl_z62lambda1939z62zzjas_classfilez00(obj_t);
	static obj_t BGl_z62classfilezd2poolzd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62attributezd2siza7ez17zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2poolzd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt, obj_t);
	static obj_t BGl_z62classfilezd2poolzb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t
		BGl_z62classfilezd2pooledzd2nameszd2setz12za2zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2globalszd2zzjas_classfilez00(BgL_classfilez00_bglt);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzjas_classfilez00 = BUNSPEC;
	static obj_t
		BGl_z62fieldzd2orzd2methodzd2descriptorzb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_attributez00zzjas_classfilez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_JasFunzd2targszd2zzjas_classfilez00(BgL_jasfunz00_bglt);
	static BgL_jastypez00_bglt BGl_z62lambda1863z62zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1945z62zzjas_classfilez00(obj_t, obj_t);
	static BgL_jastypez00_bglt BGl_z62lambda1865z62zzjas_classfilez00(obj_t);
	static obj_t BGl_z62lambda1946z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classezd2codezd2setz12z12zzjas_classfilez00(BgL_classez00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_fieldzd2orzd2methodzf3zf3zzjas_classfilez00(obj_t);
	BGL_EXPORTED_DECL int
		BGl_poolzd2doublezd2zzjas_classfilez00(BgL_classfilez00_bglt, double);
	BGL_EXPORTED_DECL obj_t
		BGl_classezd2codezd2zzjas_classfilez00(BgL_classez00_bglt);
	static obj_t BGl_z62methodzf3z91zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62basiczd2vectzb0zzjas_classfilez00(obj_t, obj_t);
	static BgL_vectz00_bglt BGl_z62vectzd2nilzb0zzjas_classfilez00(obj_t);
	static obj_t
		BGl_z62fieldzd2orzd2methodzd2namezd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_poolzd2classzd2byzd2reftypezd2zzjas_classfilez00(BgL_classfilez00_bglt,
		BgL_jastypez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2namezd2zzjas_classfilez00(BgL_fieldz00_bglt);
	static obj_t BGl_z62JasFunzd2tretzd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static obj_t BGl_z62lambda1950z62zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2ownerzd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt, obj_t);
	static obj_t BGl_z62lambda1951z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_IMPORT bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	static obj_t BGl_z62lambda1871z62zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1872z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62poolzd2classzd2byzd2namezb0zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_JasTypezd2vectzd2setz12z12zzjas_classfilez00(BgL_jastypez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vectzd2typezd2setz12z12zzjas_classfilez00(BgL_vectz00_bglt,
		BgL_jastypez00_bglt);
	static obj_t BGl_z62lambda1956z62zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1957z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1877z62zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1878z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2poolzd2siza7ezd2setz12z67zzjas_classfilez00
		(BgL_classfilez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jaszd2warningzd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL BgL_classfilez00_bglt
		BGl_classfilezd2nilzd2zzjas_classfilez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_vectzd2vectzd2setz12z12zzjas_classfilez00(BgL_vectz00_bglt, obj_t);
	static obj_t BGl_z62methodzd2usertypezd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2namezd2zzjas_classfilez00(BgL_methodz00_bglt);
	BGL_EXPORTED_DECL BgL_jasfunz00_bglt
		BGl_makezd2JasFunzd2zzjas_classfilez00(obj_t, obj_t, BgL_jastypez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2flagszd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_attributezd2siza7ezd2setz12zb5zzjas_classfilez00(BgL_attributez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2ownerzd2zzjas_classfilez00(BgL_methodz00_bglt);
	static obj_t BGl_z62classfilezd2fieldszd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	static BgL_fieldzd2orzd2methodz00_bglt
		BGl_z62lambda1964z62zzjas_classfilez00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_fieldzd2orzd2methodz00_bglt
		BGl_z62lambda1966z62zzjas_classfilez00(obj_t);
	static BgL_basicz00_bglt BGl_z62lambda1886z62zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static BgL_basicz00_bglt BGl_z62lambda1888z62zzjas_classfilez00(obj_t);
	static obj_t
		BGl_z62fieldzd2orzd2methodzd2poolzd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62fieldzd2pnamezb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzjas_classfilez00(void);
	static obj_t BGl_z62zc3z04anonymousza31890ze3ze5zzjas_classfilez00(obj_t,
		obj_t);
	static obj_t BGl_z62fieldzd2namezb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2attributeszd2zzjas_classfilez00(BgL_fieldz00_bglt);
	static obj_t BGl_z62lambda1972z62zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1973z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2pnamezd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt);
	static obj_t BGl_z62lambda1977z62zzjas_classfilez00(obj_t, obj_t);
	static BgL_vectz00_bglt BGl_z62lambda1897z62zzjas_classfilez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1978z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_JasFunzd2vectzd2setz12z12zzjas_classfilez00(BgL_jasfunz00_bglt, obj_t);
	static BgL_vectz00_bglt BGl_z62lambda1899z62zzjas_classfilez00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_fieldzf3zf3zzjas_classfilez00(obj_t);
	static obj_t BGl_z62JasTypezd2vectzb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2pooledzd2namesz00zzjas_classfilez00(BgL_classfilez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32120ze3ze5zzjas_classfilez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2usertypezd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_methodz00_bglt
		BGl_makezd2methodzd2zzjas_classfilez00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62methodzd2namezd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2superzd2zzjas_classfilez00(BgL_classfilez00_bglt);
	static obj_t BGl_z62methodzd2descriptorzb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62classfilezd2mezd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62classfilezd2superzb0zzjas_classfilez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31867ze3ze5zzjas_classfilez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2pnamezd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt, obj_t);
	static obj_t
		BGl_z62fieldzd2orzd2methodzd2flagszd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62vectzd2codezd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1982z62zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2flagszd2zzjas_classfilez00(BgL_methodz00_bglt);
	static obj_t BGl_z62lambda1983z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2ownerzd2zzjas_classfilez00(BgL_fieldz00_bglt);
	static obj_t BGl_genericzd2initzd2zzjas_classfilez00(void);
	static obj_t BGl_z62lambda1987z62zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1988z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2namezd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32113ze3ze5zzjas_classfilez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32024ze3ze5zzjas_classfilez00(obj_t);
	static obj_t BGl_z62JasFunzf3z91zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classezd2flagszd2setz12z12zzjas_classfilez00(BgL_classez00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_fieldzd2orzd2methodz00_bglt
		BGl_fieldzd2orzd2methodzd2nilzd2zzjas_classfilez00(void);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62classfilezd2currentzd2methodzd2setz12za2zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62poolzd2classzd2byzd2reftypezb0zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzjas_classfilez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_classezd2vectzd2zzjas_classfilez00(BgL_classez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_attributezd2siza7ez75zzjas_classfilez00(BgL_attributez00_bglt);
	static obj_t BGl_z62lambda1993z62zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2poolzd2zzjas_classfilez00(BgL_classfilez00_bglt);
	static obj_t BGl_z62lambda1994z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62classezf3z91zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62methodzd2poolzd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2poolzd2zzjas_classfilez00(BgL_fieldz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32106ze3ze5zzjas_classfilez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32017ze3ze5zzjas_classfilez00(obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62fieldzd2orzd2methodzd2usertypezb0zzjas_classfilez00(obj_t,
		obj_t);
	static obj_t BGl_z62JasFunzd2targszd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31958ze3ze5zzjas_classfilez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2flagszd2zzjas_classfilez00(BgL_fieldz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2poolzd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt, obj_t);
	static obj_t BGl_z62classezd2codezd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62methodzd2pnamezb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62fieldzd2orzd2methodzf3z91zzjas_classfilez00(obj_t, obj_t);
	static obj_t
		BGl_z62classfilezd2interfaceszd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static long BGl_poolzd2getzd2zzjas_classfilez00(BgL_classfilez00_bglt, long,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_JasFunz00zzjas_classfilez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2poolzd2zzjas_classfilez00(BgL_methodz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2usertypezd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt, obj_t);
	static BgL_jastypez00_bglt BGl_z62aszd2typezb0zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62attributezd2infozd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2typezd2zzjas_classfilez00(BgL_fieldz00_bglt);
	static obj_t
		BGl_poolzd2getzd2specialz12z12zzjas_classfilez00(BgL_classfilez00_bglt,
		long, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2poolzd2setz12z12zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t);
	static obj_t BGl_z62fieldzd2ownerzd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62classfilezd2methodszb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_poolzd2stringzd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t);
	static obj_t BGl_z62fieldzd2orzd2methodzd2pnamezb0zzjas_classfilez00(obj_t,
		obj_t);
	static obj_t BGl_z62JasTypezf3z91zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_poolzd2interfacezd2methodz00zzjas_classfilez00(BgL_classfilez00_bglt,
		BgL_methodz00_bglt);
	static BgL_jastypez00_bglt BGl_z62vectzd2typezb0zzjas_classfilez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2usertypezd2zzjas_classfilez00(BgL_methodz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2typezd2zzjas_classfilez00(BgL_methodz00_bglt);
	static obj_t BGl_z62fieldzd2poolzb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62methodzd2usertypezb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31968ze3ze5zzjas_classfilez00(obj_t,
		obj_t);
	static obj_t BGl_z62methodzd2flagszd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31879ze3ze5zzjas_classfilez00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_methodz00zzjas_classfilez00 = BUNSPEC;
	static obj_t BGl_z62poolzd2doublezb0zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62classezd2codezb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2typezd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_fieldz00_bglt
		BGl_makezd2fieldzd2zzjas_classfilez00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2descriptorzd2zzjas_classfilez00(BgL_fieldz00_bglt);
	static obj_t
		BGl_poolzd2fieldzd2methodzd2localzd2zzjas_classfilez00
		(BgL_classfilez00_bglt, BgL_methodz00_bglt, long);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62classfilezd2methodszd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62fieldzd2typezb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62fieldzd2attributeszb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_basicz00zzjas_classfilez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_JasTypezd2codezd2setz12z12zzjas_classfilez00(BgL_jastypez00_bglt,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzjas_classfilez00(void);
	static obj_t BGl_z62JasFunzd2vectzd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62jaszd2warningzb0zzjas_classfilez00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2descriptorzd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2descriptorzd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32142ze3ze5zzjas_classfilez00(obj_t);
	static obj_t BGl_z62aszd2assignzb0zzjas_classfilez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32037ze3ze5zzjas_classfilez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vectzd2codezd2setz12z12zzjas_classfilez00(BgL_vectz00_bglt, obj_t);
	static obj_t BGl_z62methodzd2namezb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62poolzd2intzb0zzjas_classfilez00(obj_t, obj_t, obj_t);
	static BgL_jasfunz00_bglt BGl_z62makezd2JasFunzb0zzjas_classfilez00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2pnamezd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_JasTypez00zzjas_classfilez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_poolzd2localzd2methodz00zzjas_classfilez00(BgL_classfilez00_bglt,
		BgL_methodz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2interfaceszd2setz12z12zzjas_classfilez00
		(BgL_classfilez00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_vectz00_bglt BGl_vectzd2nilzd2zzjas_classfilez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2descriptorzd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_classez00_bglt
		BGl_classezd2nilzd2zzjas_classfilez00(void);
	static obj_t
		BGl_z62fieldzd2orzd2methodzd2descriptorzd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32135ze3ze5zzjas_classfilez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32127ze3ze5zzjas_classfilez00(obj_t);
	BGL_EXPORTED_DECL BgL_jastypez00_bglt
		BGl_JasFunzd2tretzd2zzjas_classfilez00(BgL_jasfunz00_bglt);
	static BgL_fieldz00_bglt BGl_z62makezd2fieldzb0zzjas_classfilez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classezd2flagszd2zzjas_classfilez00(BgL_classez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2fieldszd2zzjas_classfilez00(BgL_classfilez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31995ze3ze5zzjas_classfilez00(obj_t);
	BGL_EXPORTED_DECL BgL_jastypez00_bglt
		BGl_vectzd2typezd2zzjas_classfilez00(BgL_vectz00_bglt);
	static obj_t BGl_z62attributezd2namezd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_JasFunzd2codezd2setz12z12zzjas_classfilez00(BgL_jasfunz00_bglt, obj_t);
	static obj_t BGl_z62poolzd2interfacezd2methodz62zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2namezd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt, obj_t);
	static obj_t BGl_z62classfilezd2globalszb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t
		BGl_z62fieldzd2orzd2methodzd2typezd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2attributeszd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32047ze3ze5zzjas_classfilez00(obj_t,
		obj_t);
	static BgL_methodz00_bglt BGl_z62makezd2methodzb0zzjas_classfilez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62fieldzd2namezd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2attributeszd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt,
		obj_t);
	static obj_t BGl_z62fieldzd2orzd2methodzd2namezb0zzjas_classfilez00(obj_t,
		obj_t);
	static obj_t BGl_z62classfilezd2mezb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t
		BGl_z62fieldzd2orzd2methodzd2pnamezd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t BGl_z62fieldzd2ownerzb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classezd2namezd2setz12z12zzjas_classfilez00(BgL_classez00_bglt, obj_t);
	static obj_t BGl_z62lambda2000z62zzjas_classfilez00(obj_t, obj_t);
	extern obj_t BGl_w2z00zzjas_libz00(int);
	static obj_t BGl_z62lambda2001z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_w4z00zzjas_libz00(long);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2attributeszd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt, obj_t);
	static obj_t BGl_z62classfilezd2attributeszb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2008z62zzjas_classfilez00(obj_t, obj_t);
	static BgL_classez00_bglt BGl_z62classezd2nilzb0zzjas_classfilez00(obj_t);
	static obj_t BGl_z62lambda2009z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62classfilezd2superzd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62fieldzd2orzd2methodzd2attributeszd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62fieldzd2usertypezb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2flagszd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2ownerzd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt);
	BGL_EXPORTED_DECL int
		BGl_poolzd2floatzd2zzjas_classfilez00(BgL_classfilez00_bglt, float);
	static obj_t BGl_z62JasTypezd2vectzd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62classezd2vectzb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2poolzd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_aszd2assignzd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t, obj_t);
	extern obj_t BGl_w4llongz00zzjas_libz00(BGL_LONGLONG_T);
	static obj_t BGl_z62fieldzf3z91zzjas_classfilez00(obj_t, obj_t);
	static obj_t
		BGl_z62classfilezd2attributeszd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62fieldzd2poolzd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62classfilezd2pooledzd2namesz62zzjas_classfilez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2namezd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt);
	static obj_t BGl_z62lambda2015z62zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2016z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62typezd2siza7ez17zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62fieldzd2descriptorzd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_jasfunz00_bglt
		BGl_JasFunzd2nilzd2zzjas_classfilez00(void);
	static obj_t BGl_z62JasFunzd2targszb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classezd2poolzd2setz12z12zzjas_classfilez00(BgL_classez00_bglt, obj_t);
	static obj_t BGl_z62fieldzd2flagszb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_classez00_bglt
		BGl_makezd2classezd2zzjas_classfilez00(obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_attributezd2infozd2setz12z12zzjas_classfilez00(BgL_attributez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classezd2namezd2zzjas_classfilez00(BgL_classez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2attributeszd2zzjas_classfilez00(BgL_classfilez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2superzd2setz12z12zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t);
	static obj_t BGl_z62lambda2022z62zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2023z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2104z62zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32171ze3ze5zzjas_classfilez00(obj_t);
	static obj_t BGl_z62methodzd2poolzb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2105z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62methodzd2typezd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32058ze3ze5zzjas_classfilez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_poolzd2classzd2byzd2namezd2zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t);
	static obj_t BGl_z62fieldzd2descriptorzb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2flagszd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt);
	static obj_t BGl_z62poolzd2longzb0zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_methodz00_bglt
		BGl_declaredzd2methodzd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t);
	static obj_t BGl_z62poolzd2floatzb0zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62poolzd2stringzb0zzjas_classfilez00(obj_t, obj_t, obj_t);
	static BgL_methodz00_bglt BGl_z62declaredzd2methodzb0zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2typezd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt, obj_t);
	static BgL_attributez00_bglt
		BGl_z62attributezd2nilzb0zzjas_classfilez00(obj_t);
	static obj_t BGl_z62classfilezd2poolzd2siza7ezc5zzjas_classfilez00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2111z62zzjas_classfilez00(obj_t, obj_t);
	static BgL_fieldz00_bglt BGl_z62lambda2031z62zzjas_classfilez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2112z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2usertypezd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_JasFunzd2codezd2zzjas_classfilez00(BgL_jasfunz00_bglt);
	static BgL_fieldz00_bglt BGl_z62lambda2034z62zzjas_classfilez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32164ze3ze5zzjas_classfilez00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_vectzf3zf3zzjas_classfilez00(obj_t);
	static obj_t BGl_z62lambda2118z62zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2ownerzd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt, obj_t);
	static obj_t BGl_z62lambda2119z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62methodzd2typezb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62fieldzd2attributeszd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	static BgL_fieldz00_bglt BGl_z62fieldzd2nilzb0zzjas_classfilez00(obj_t);
	static BgL_jasfunz00_bglt BGl_z62JasFunzd2nilzb0zzjas_classfilez00(obj_t);
	static obj_t BGl_z62methodzd2ownerzb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62methodzd2pnamezd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_methodzf3zf3zzjas_classfilez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_JasTypezd2codezd2zzjas_classfilez00(BgL_jastypez00_bglt);
	BGL_EXPORTED_DECL BgL_jastypez00_bglt
		BGl_makezd2JasTypezd2zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_poolzd2uint32zd2zzjas_classfilez00(BgL_classfilez00_bglt, uint32_t);
	static obj_t BGl_z62fieldzd2orzd2methodzd2poolzb0zzjas_classfilez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2currentzd2methodzd2setz12zc0zzjas_classfilez00
		(BgL_classfilez00_bglt, obj_t);
	static BgL_methodz00_bglt BGl_z62lambda2043z62zzjas_classfilez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32092ze3ze5zzjas_classfilez00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2125z62zzjas_classfilez00(obj_t, obj_t);
	static BgL_methodz00_bglt BGl_z62lambda2045z62zzjas_classfilez00(obj_t);
	static obj_t BGl_z62lambda2126z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32157ze3ze5zzjas_classfilez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32149ze3ze5zzjas_classfilez00(obj_t);
	static obj_t BGl_z62classezd2flagszd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzjas_classfilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_libz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62fieldzd2orzd2methodzd2ownerzb0zzjas_classfilez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_JasTypezf3zf3zzjas_classfilez00(obj_t);
	static obj_t BGl_z62classfilezd2fieldszb0zzjas_classfilez00(obj_t, obj_t);
	extern obj_t BGl_w4elongz00zzjas_libz00(long);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2attributeszd2setz12z12zzjas_classfilez00
		(BgL_classfilez00_bglt, obj_t);
	BGL_EXPORTED_DECL int
		BGl_poolzd2methodzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		BgL_methodz00_bglt);
	BGL_EXPORTED_DECL int
		BGl_typezd2siza7ez75zzjas_classfilez00(BgL_jastypez00_bglt);
	static obj_t BGl_z62JasFunzd2codezd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2133z62zzjas_classfilez00(obj_t, obj_t);
	static BgL_attributez00_bglt BGl_z62lambda2053z62zzjas_classfilez00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2134z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static BgL_attributez00_bglt BGl_z62lambda2056z62zzjas_classfilez00(obj_t);
	static obj_t BGl_z62fieldzd2orzd2methodzd2typezb0zzjas_classfilez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2attributeszd2zzjas_classfilez00(BgL_methodz00_bglt);
	static obj_t BGl_z62classfilezd2currentzd2methodz62zzjas_classfilez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62methodzd2flagszb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_basiczf3zf3zzjas_classfilez00(obj_t);
	BGL_EXPORTED_DECL int
		BGl_poolzd2llongzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		BGL_LONGLONG_T);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2poolzd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt);
	static obj_t BGl_z62attributezd2namezb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62classezd2namezd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2140z62zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_getzd2vectzd2typez00zzjas_classfilez00(BgL_jastypez00_bglt);
	static obj_t BGl_z62lambda2141z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2062z62zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2063z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2usertypezd2zzjas_classfilez00(BgL_fieldz00_bglt);
	static obj_t BGl_z62lambda2147z62zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2148z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2068z62zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_poolzd2longzd2zzjas_classfilez00(BgL_classfilez00_bglt, long);
	static obj_t BGl_z62lambda2069z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static BgL_jastypez00_bglt BGl_z62JasFunzd2tretzb0zzjas_classfilez00(obj_t,
		obj_t);
	static obj_t BGl_z62jaszd2errorzb0zzjas_classfilez00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_classez00zzjas_classfilez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_attributezd2namezd2setz12z12zzjas_classfilez00(BgL_attributez00_bglt,
		obj_t);
	static obj_t BGl_z62fieldzd2flagszd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62fieldzd2orzd2methodzd2flagszb0zzjas_classfilez00(obj_t,
		obj_t);
	static obj_t BGl_z62vectzd2codezb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2globalszd2setz12z12zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classezd2poolzd2zzjas_classfilez00(BgL_classez00_bglt);
	static obj_t BGl_poolzd2getz12zc0zzjas_classfilez00(BgL_classfilez00_bglt,
		long, obj_t);
	static obj_t BGl_z62methodzd2descriptorzd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2typezd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt);
	BGL_EXPORTED_DECL int
		BGl_poolzd2uint64zd2zzjas_classfilez00(BgL_classfilez00_bglt, uint64_t);
	BGL_EXPORTED_DECL int
		BGl_poolzd2intzd2zzjas_classfilez00(BgL_classfilez00_bglt, int);
	static obj_t BGl_z62lambda2155z62zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_fieldz00_bglt
		BGl_fieldzd2nilzd2zzjas_classfilez00(void);
	static obj_t BGl_z62lambda2075z62zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2156z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2076z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62vectzf3z91zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzjas_classfilez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_basiczd2vectzd2setz12z12zzjas_classfilez00(BgL_basicz00_bglt, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzjas_classfilez00(void);
	static obj_t BGl_z62basiczd2vectzd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62classezd2poolzd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_JasFunzd2vectzd2zzjas_classfilez00(BgL_jasfunz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_JasFunzf3zf3zzjas_classfilez00(obj_t);
	static BgL_classfilez00_bglt
		BGl_z62makezd2classfilezb0zzjas_classfilez00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62poolzd2llongzb0zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzjas_classfilez00(void);
	static obj_t BGl_z62classfilezd2interfaceszb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzjas_classfilez00(void);
	static obj_t BGl_z62lambda2080z62zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2081z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2162z62zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2163z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2169z62zzjas_classfilez00(obj_t, obj_t);
	static BgL_classfilez00_bglt BGl_z62lambda2088z62zzjas_classfilez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_classezf3zf3zzjas_classfilez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2pnamezd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt, obj_t);
	static BgL_classez00_bglt BGl_z62declaredzd2classzb0zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_JasTypezd2vectzd2zzjas_classfilez00(BgL_jastypez00_bglt);
	static obj_t BGl_z62JasTypezd2codezd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static BgL_classfilez00_bglt
		BGl_z62classfilezd2nilzb0zzjas_classfilez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31902ze3ze5zzjas_classfilez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_jastypez00_bglt
		BGl_JasTypezd2nilzd2zzjas_classfilez00(void);
	static obj_t BGl_z62lambda2170z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static BgL_fieldzd2orzd2methodz00_bglt
		BGl_z62makezd2fieldzd2orzd2methodzb0zzjas_classfilez00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_classfilez00_bglt BGl_z62lambda2090z62zzjas_classfilez00(obj_t);
	static obj_t BGl_z62fieldzd2usertypezd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2ownerzd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32178ze3ze5zzjas_classfilez00(obj_t);
	static obj_t BGl_z62methodzd2attributeszd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda2176z62zzjas_classfilez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_objectz00zz__objectz00;
	static obj_t BGl_z62lambda2177z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2097z62zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2098z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_poolzd2elongzd2zzjas_classfilez00(BgL_classfilez00_bglt, long);
	static obj_t BGl_z62attributezd2typezd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_jastypez00_bglt
		BGl_aszd2typezd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t);
	static BgL_attributez00_bglt
		BGl_z62makezd2attributezb0zzjas_classfilez00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62poolzd2localzd2methodz62zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2interfaceszd2zzjas_classfilez00(BgL_classfilez00_bglt);
	static BgL_classez00_bglt BGl_z62makezd2classezb0zzjas_classfilez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2typezd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt, obj_t);
	static obj_t BGl_z62classezd2namezb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62classfilezd2flagszd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_vectz00zzjas_classfilez00 = BUNSPEC;
	static obj_t BGl_z62attributezd2infozb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62fieldzd2typezd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jaszd2errorzd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vectzd2codezd2zzjas_classfilez00(BgL_vectz00_bglt);
	BGL_EXPORTED_DECL int
		BGl_poolzd2classzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		BgL_classez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2pooledzd2nameszd2setz12zc0zzjas_classfilez00
		(BgL_classfilez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	static obj_t
		BGl_z62classfilezd2poolzd2siza7ezd2setz12z05zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2poolzd2siza7eza7zzjas_classfilez00(BgL_classfilez00_bglt);
	BGL_EXPORTED_DECL BgL_fieldzd2orzd2methodz00_bglt
		BGl_makezd2fieldzd2orzd2methodzd2zzjas_classfilez00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_jasfunz00_bglt
		BGl_aszd2funtypezd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2descriptorzd2zzjas_classfilez00(BgL_methodz00_bglt);
	static obj_t BGl_z62poolzd2namezb0zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2mezd2setz12z12zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2flagszd2zzjas_classfilez00(BgL_classfilez00_bglt);
	BGL_EXPORTED_DECL BgL_methodz00_bglt
		BGl_methodzd2nilzd2zzjas_classfilez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2attributeszd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_classezd2vectzd2setz12z12zzjas_classfilez00(BgL_classez00_bglt, obj_t);
	static obj_t BGl_z62JasFunzd2codezb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62classfilezd2flagszb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32099ze3ze5zzjas_classfilez00(obj_t);
	static obj_t BGl_z62vectzd2vectzb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_poolzd2int32zd2zzjas_classfilez00(BgL_classfilez00_bglt, int32_t);
	static obj_t
		BGl_z62fieldzd2orzd2methodzd2ownerzd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2pnamezd2zzjas_classfilez00(BgL_methodz00_bglt);
	static obj_t BGl_z62poolzd2elongzb0zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_attributezd2namezd2zzjas_classfilez00(BgL_attributez00_bglt);
	static BgL_vectz00_bglt BGl_z62makezd2vectzb0zzjas_classfilez00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_fieldz00zzjas_classfilez00 = BUNSPEC;
	static obj_t BGl_z62attributezd2typezb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2flagszd2setz12z12zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t);
	static BgL_fieldz00_bglt BGl_z62declaredzd2fieldzb0zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_poolzd2addzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t);
	static obj_t BGl_z62poolzd2uint32zb0zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
	static BgL_basicz00_bglt BGl_z62basiczd2nilzb0zzjas_classfilez00(obj_t);
	static obj_t BGl_z62classezd2flagszb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_fieldzd2orzd2methodz00zzjas_classfilez00 = BUNSPEC;
	static obj_t BGl_z62poolzd2classzb0zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_basiczd2codezd2zzjas_classfilez00(BgL_basicz00_bglt);
	BGL_EXPORTED_DECL BgL_basicz00_bglt
		BGl_makezd2basiczd2zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62poolzd2methodzb0zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_JasFunzd2targszd2setz12z12zzjas_classfilez00(BgL_jasfunz00_bglt, obj_t);
	static BgL_jasfunz00_bglt BGl_z62aszd2funtypezb0zzjas_classfilez00(obj_t,
		obj_t, obj_t, obj_t);
	static BgL_methodz00_bglt BGl_z62methodzd2nilzb0zzjas_classfilez00(obj_t);
	static obj_t
		BGl_z62fieldzd2orzd2methodzd2attributeszb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62attributezd2siza7ezd2setz12zd7zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62poolzd2int32zb0zzjas_classfilez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_classfilez00_bglt
		BGl_makezd2classfilezd2zzjas_classfilez00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2pnamezd2zzjas_classfilez00(BgL_fieldz00_bglt);
	static obj_t BGl_pourquoi_tant_de_hainez00zzjas_classfilez00 = BUNSPEC;
	BGL_EXPORTED_DECL int
		BGl_poolzd2fieldzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		BgL_fieldz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_JasFunzd2tretzd2setz12z12zzjas_classfilez00(BgL_jasfunz00_bglt,
		BgL_jastypez00_bglt);
	static obj_t
		BGl_z62fieldzd2orzd2methodzd2usertypezd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_attributezf3zf3zzjas_classfilez00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_classfilezf3zf3zzjas_classfilez00(obj_t);
	BGL_EXPORTED_DECL BgL_classez00_bglt
		BGl_declaredzd2classzd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2methodszd2zzjas_classfilez00(BgL_classfilez00_bglt);
	extern obj_t BGl_f2z00zzjas_libz00(float);
	extern obj_t BGl_f4z00zzjas_libz00(double);
	BGL_EXPORTED_DECL int
		BGl_poolzd2int64zd2zzjas_classfilez00(BgL_classfilez00_bglt, int64_t);
	static obj_t BGl_z62vectzd2typezd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62fieldzd2pnamezd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_attributez00_bglt
		BGl_attributezd2nilzd2zzjas_classfilez00(void);
	static obj_t BGl_z62classezd2poolzb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_poolzd2namezd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t);
	static obj_t BGl_z62basiczd2codezb0zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62vectzd2vectzd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vectzd2vectzd2zzjas_classfilez00(BgL_vectz00_bglt);
	static BgL_basicz00_bglt BGl_z62makezd2basiczb0zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2currentzd2methodz00zzjas_classfilez00
		(BgL_classfilez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2flagszd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt, obj_t);
	static obj_t BGl_z62poolzd2uint64zb0zzjas_classfilez00(obj_t, obj_t, obj_t);
	static BgL_jastypez00_bglt BGl_z62lambda1907z62zzjas_classfilez00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1908z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62methodzd2ownerzd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_vectz00_bglt BGl_makezd2vectzd2zzjas_classfilez00(obj_t,
		obj_t, BgL_jastypez00_bglt);
	BGL_EXPORTED_DECL BgL_attributez00_bglt
		BGl_makezd2attributezd2zzjas_classfilez00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_basiczd2codezd2setz12z12zzjas_classfilez00(BgL_basicz00_bglt, obj_t);
	static obj_t BGl_z62basiczf3z91zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62basiczd2codezd2setz12z70zzjas_classfilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62JasFunzd2vectzb0zzjas_classfilez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_attributezd2infozd2zzjas_classfilez00(BgL_attributez00_bglt);
	static obj_t BGl_z62poolzd2fieldzb0zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31941ze3ze5zzjas_classfilez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_basicz00_bglt
		BGl_basiczd2nilzd2zzjas_classfilez00(void);
	static BgL_fieldzd2orzd2methodz00_bglt
		BGl_z62fieldzd2orzd2methodzd2nilzb0zzjas_classfilez00(obj_t);
	static BgL_jasfunz00_bglt BGl_z62lambda1915z62zzjas_classfilez00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_jasfunz00_bglt BGl_z62lambda1917z62zzjas_classfilez00(obj_t);
	static obj_t BGl_z62attributezf3z91zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62classfilezf3z91zzjas_classfilez00(obj_t, obj_t);
	static obj_t BGl_z62poolzd2int64zb0zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62classfilezd2globalszd2setz12z70zzjas_classfilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_methodzd2namezd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2usertypezd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_fieldzd2orzd2methodzd2descriptorzd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt);
	static obj_t BGl_basiczd2encodedzd2typez00zzjas_classfilez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2fieldszd2setz12z12zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classfilezd2mezd2zzjas_classfilez00(BgL_classfilez00_bglt);
	static BgL_jastypez00_bglt BGl_z62JasTypezd2nilzb0zzjas_classfilez00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_classfilez00zzjas_classfilez00 = BUNSPEC;
	static BgL_jastypez00_bglt BGl_z62lambda1925z62zzjas_classfilez00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1926z62zzjas_classfilez00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_poolzd2fieldzd2methodz00zzjas_classfilez00(BgL_classfilez00_bglt,
		BgL_fieldzd2orzd2methodz00_bglt, long);
	BGL_IMPORT obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	static obj_t __cnst[51];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_poolzd2interfacezd2methodzd2envzd2zzjas_classfilez00,
		BgL_bgl_za762poolza7d2interf2350z00,
		BGl_z62poolzd2interfacezd2methodz62zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2pooledzd2nameszd2envzd2zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2p2351z00,
		BGl_z62classfilezd2pooledzd2namesz62zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_vectzd2vectzd2envz00zzjas_classfilez00,
		BgL_bgl_za762vectza7d2vectza7b2352za7,
		BGl_z62vectzd2vectzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_basiczd2vectzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762basicza7d2vectza72353za7,
		BGl_z62basiczd2vectzd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_attributezd2namezd2envz00zzjas_classfilez00,
		BgL_bgl_za762attributeza7d2n2354z00,
		BGl_z62attributezd2namezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2210z00zzjas_classfilez00,
		BgL_bgl_string2210za700za7za7j2355za7, "V", 1);
	      DEFINE_STRING(BGl_string2211z00zzjas_classfilez00,
		BgL_bgl_string2211za700za7za7j2356za7, "Z", 1);
	      DEFINE_STRING(BGl_string2212z00zzjas_classfilez00,
		BgL_bgl_string2212za700za7za7j2357za7, "C", 1);
	      DEFINE_STRING(BGl_string2213z00zzjas_classfilez00,
		BgL_bgl_string2213za700za7za7j2358za7, "B", 1);
	      DEFINE_STRING(BGl_string2214z00zzjas_classfilez00,
		BgL_bgl_string2214za700za7za7j2359za7, "S", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2flagszd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2f2360z00,
		BGl_z62classfilezd2flagszd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2215z00zzjas_classfilez00,
		BgL_bgl_string2215za700za7za7j2361za7, "I", 1);
	      DEFINE_STRING(BGl_string2216z00zzjas_classfilez00,
		BgL_bgl_string2216za700za7za7j2362za7, "J", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_poolzd2uint32zd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2uint322363z00,
		BGl_z62poolzd2uint32zb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2217z00zzjas_classfilez00,
		BgL_bgl_string2217za700za7za7j2364za7, "F", 1);
	      DEFINE_STRING(BGl_string2218z00zzjas_classfilez00,
		BgL_bgl_string2218za700za7za7j2365za7, "D", 1);
	      DEFINE_STRING(BGl_string2219z00zzjas_classfilez00,
		BgL_bgl_string2219za700za7za7j2366za7, "[B", 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classezd2flagszd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762classeza7d2flag2367z00,
		BGl_z62classezd2flagszd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_poolzd2floatzd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2floatza72368za7,
		BGl_z62poolzd2floatzb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2attributeszd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2a2369z00,
		BGl_z62classfilezd2attributeszd2setz12z70zzjas_classfilez00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2poolzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2poolza72370za7,
		BGl_z62fieldzd2poolzd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2220z00zzjas_classfilez00,
		BgL_bgl_string2220za700za7za7j2371za7, " WARNING ** : ", 14);
	      DEFINE_STRING(BGl_string2221z00zzjas_classfilez00,
		BgL_bgl_string2221za700za7za7j2372za7, " ", 1);
	      DEFINE_STRING(BGl_string2222z00zzjas_classfilez00,
		BgL_bgl_string2222za700za7za7j2373za7, "bad type", 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_methodzd2namezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762methodza7d2name2374z00,
		BGl_z62methodzd2namezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2223z00zzjas_classfilez00,
		BgL_bgl_string2223za700za7za7j2375za7, "[", 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_basiczd2codezd2envz00zzjas_classfilez00,
		BgL_bgl_za762basicza7d2codeza72376za7,
		BGl_z62basiczd2codezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2224z00zzjas_classfilez00,
		BgL_bgl_string2224za700za7za7j2377za7, ")", 1);
	      DEFINE_STRING(BGl_string2225z00zzjas_classfilez00,
		BgL_bgl_string2225za700za7za7j2378za7, "(", 1);
	      DEFINE_STRING(BGl_string2226z00zzjas_classfilez00,
		BgL_bgl_string2226za700za7za7j2379za7, "redefinition of global", 22);
	      DEFINE_STRING(BGl_string2227z00zzjas_classfilez00,
		BgL_bgl_string2227za700za7za7j2380za7, "undefined global name", 21);
	      DEFINE_STRING(BGl_string2228z00zzjas_classfilez00,
		BgL_bgl_string2228za700za7za7j2381za7, "not a class", 11);
	      DEFINE_STRING(BGl_string2229z00zzjas_classfilez00,
		BgL_bgl_string2229za700za7za7j2382za7, "not a field", 11);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_basiczd2vectzd2envz00zzjas_classfilez00,
		BgL_bgl_za762basicza7d2vectza72383za7,
		BGl_z62basiczd2vectzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_fieldzd2pnamezd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2pname2384z00,
		BGl_z62fieldzd2pnamezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2300z00zzjas_classfilez00,
		BgL_bgl_za762lambda2076za7622385z00, BGl_z62lambda2076z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_aszd2typezd2envz00zzjas_classfilez00,
		BgL_bgl_za762asza7d2typeza7b0za72386z00,
		BGl_z62aszd2typezb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2301z00zzjas_classfilez00,
		BgL_bgl_za762lambda2075za7622387z00, BGl_z62lambda2075z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2302z00zzjas_classfilez00,
		BgL_bgl_za762lambda2081za7622388z00, BGl_z62lambda2081z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2303z00zzjas_classfilez00,
		BgL_bgl_za762lambda2080za7622389z00, BGl_z62lambda2080z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2descriptorzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22390za7,
		BGl_z62fieldzd2orzd2methodzd2descriptorzd2setz12z70zzjas_classfilez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2304z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2391za7,
		BGl_z62zc3z04anonymousza32058ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2305z00zzjas_classfilez00,
		BgL_bgl_za762lambda2056za7622392z00, BGl_z62lambda2056z62zzjas_classfilez00,
		0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2230z00zzjas_classfilez00,
		BgL_bgl_string2230za700za7za7j2393za7, "not a method", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2306z00zzjas_classfilez00,
		BgL_bgl_za762lambda2053za7622394z00, BGl_z62lambda2053z62zzjas_classfilez00,
		0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2307z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2395za7,
		BGl_z62zc3z04anonymousza32099ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2308z00zzjas_classfilez00,
		BgL_bgl_za762lambda2098za7622396z00, BGl_z62lambda2098z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2309z00zzjas_classfilez00,
		BgL_bgl_za762lambda2097za7622397z00, BGl_z62lambda2097z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2methodszd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2m2398z00,
		BGl_z62classfilezd2methodszd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2poolzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2p2399z00,
		BGl_z62classfilezd2poolzd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2attributeszd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2attri2400z00,
		BGl_z62fieldzd2attributeszb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_attributezd2namezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762attributeza7d2n2401z00,
		BGl_z62attributezd2namezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_JasFunzd2vectzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762jasfunza7d2vect2402z00,
		BGl_z62JasFunzd2vectzd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2310z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2403za7,
		BGl_z62zc3z04anonymousza32106ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2311z00zzjas_classfilez00,
		BgL_bgl_za762lambda2105za7622404z00, BGl_z62lambda2105z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2312z00zzjas_classfilez00,
		BgL_bgl_za762lambda2104za7622405z00, BGl_z62lambda2104z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2231z00zzjas_classfilez00,
		BgL_bgl_za762lambda1872za7622406z00, BGl_z62lambda1872z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2313z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2407za7,
		BGl_z62zc3z04anonymousza32113ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2232z00zzjas_classfilez00,
		BgL_bgl_za762lambda1871za7622408z00, BGl_z62lambda1871z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2314z00zzjas_classfilez00,
		BgL_bgl_za762lambda2112za7622409z00, BGl_z62lambda2112z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2233z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2410za7,
		BGl_z62zc3z04anonymousza31879ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2315z00zzjas_classfilez00,
		BgL_bgl_za762lambda2111za7622411z00, BGl_z62lambda2111z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2234z00zzjas_classfilez00,
		BgL_bgl_za762lambda1878za7622412z00, BGl_z62lambda1878z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2316z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2413za7,
		BGl_z62zc3z04anonymousza32120ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2235z00zzjas_classfilez00,
		BgL_bgl_za762lambda1877za7622414z00, BGl_z62lambda1877z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2317z00zzjas_classfilez00,
		BgL_bgl_za762lambda2119za7622415z00, BGl_z62lambda2119z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2236z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2416za7,
		BGl_z62zc3z04anonymousza31867ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2fieldzd2orzd2methodzd2envz00zzjas_classfilez00,
		BgL_bgl_za762makeza7d2fieldza72417za7,
		BGl_z62makezd2fieldzd2orzd2methodzb0zzjas_classfilez00, 0L, BUNSPEC, 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2318z00zzjas_classfilez00,
		BgL_bgl_za762lambda2118za7622418z00, BGl_z62lambda2118z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2237z00zzjas_classfilez00,
		BgL_bgl_za762lambda1865za7622419z00, BGl_z62lambda1865z62zzjas_classfilez00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2319z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2420za7,
		BGl_z62zc3z04anonymousza32127ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2238z00zzjas_classfilez00,
		BgL_bgl_za762lambda1863za7622421z00, BGl_z62lambda1863z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2239z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2422za7,
		BGl_z62zc3z04anonymousza31890ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_methodzd2usertypezd2envz00zzjas_classfilez00,
		BgL_bgl_za762methodza7d2user2423z00,
		BGl_z62methodzd2usertypezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_fieldzd2nilzd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2nilza7b2424za7,
		BGl_z62fieldzd2nilzb0zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2320z00zzjas_classfilez00,
		BgL_bgl_za762lambda2126za7622425z00, BGl_z62lambda2126z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2321z00zzjas_classfilez00,
		BgL_bgl_za762lambda2125za7622426z00, BGl_z62lambda2125z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2240z00zzjas_classfilez00,
		BgL_bgl_za762lambda1888za7622427z00, BGl_z62lambda1888z62zzjas_classfilez00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2322z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2428za7,
		BGl_z62zc3z04anonymousza32135ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2241z00zzjas_classfilez00,
		BgL_bgl_za762lambda1886za7622429z00, BGl_z62lambda1886z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2323z00zzjas_classfilez00,
		BgL_bgl_za762lambda2134za7622430z00, BGl_z62lambda2134z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2242z00zzjas_classfilez00,
		BgL_bgl_za762lambda1908za7622431z00, BGl_z62lambda1908z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2namezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22432za7,
		BGl_z62fieldzd2orzd2methodzd2namezd2setz12z70zzjas_classfilez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2324z00zzjas_classfilez00,
		BgL_bgl_za762lambda2133za7622433z00, BGl_z62lambda2133z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2243z00zzjas_classfilez00,
		BgL_bgl_za762lambda1907za7622434z00, BGl_z62lambda1907z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2325z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2435za7,
		BGl_z62zc3z04anonymousza32142ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2244z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2436za7,
		BGl_z62zc3z04anonymousza31902ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2326z00zzjas_classfilez00,
		BgL_bgl_za762lambda2141za7622437z00, BGl_z62lambda2141z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2245z00zzjas_classfilez00,
		BgL_bgl_za762lambda1899za7622438z00, BGl_z62lambda1899z62zzjas_classfilez00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2327z00zzjas_classfilez00,
		BgL_bgl_za762lambda2140za7622439z00, BGl_z62lambda2140z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2246z00zzjas_classfilez00,
		BgL_bgl_za762lambda1897za7622440z00, BGl_z62lambda1897z62zzjas_classfilez00,
		0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2328z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2441za7,
		BGl_z62zc3z04anonymousza32149ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2247z00zzjas_classfilez00,
		BgL_bgl_za762lambda1926za7622442z00, BGl_z62lambda1926z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2329z00zzjas_classfilez00,
		BgL_bgl_za762lambda2148za7622443z00, BGl_z62lambda2148z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2248z00zzjas_classfilez00,
		BgL_bgl_za762lambda1925za7622444z00, BGl_z62lambda1925z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2attributeszd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22445za7,
		BGl_z62fieldzd2orzd2methodzd2attributeszd2setz12z70zzjas_classfilez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2249z00zzjas_classfilez00,
		BgL_bgl_za762lambda1931za7622446z00, BGl_z62lambda1931z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2fieldzd2envz00zzjas_classfilez00,
		BgL_bgl_za762makeza7d2fieldza72447za7,
		BGl_z62makezd2fieldzb0zzjas_classfilez00, 0L, BUNSPEC, 9);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2nilzd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22448za7,
		BGl_z62fieldzd2orzd2methodzd2nilzb0zzjas_classfilez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_poolzd2classzd2byzd2reftypezd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2classza72449za7,
		BGl_z62poolzd2classzd2byzd2reftypezb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_basiczd2nilzd2envz00zzjas_classfilez00,
		BgL_bgl_za762basicza7d2nilza7b2450za7,
		BGl_z62basiczd2nilzb0zzjas_classfilez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_JasTypezd2vectzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762jastypeza7d2vec2451z00,
		BGl_z62JasTypezd2vectzd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2pnamezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2pname2452z00,
		BGl_z62fieldzd2pnamezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jaszd2warningzd2envz00zzjas_classfilez00,
		BgL_bgl_za762jasza7d2warning2453z00,
		BGl_z62jaszd2warningzb0zzjas_classfilez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2330z00zzjas_classfilez00,
		BgL_bgl_za762lambda2147za7622454z00, BGl_z62lambda2147z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_aszd2assignzd2envz00zzjas_classfilez00,
		BgL_bgl_za762asza7d2assignza7b2455za7,
		BGl_z62aszd2assignzb0zzjas_classfilez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2331z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2456za7,
		BGl_z62zc3z04anonymousza32157ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2250z00zzjas_classfilez00,
		BgL_bgl_za762lambda1930za7622457z00, BGl_z62lambda1930z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_methodzd2descriptorzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762methodza7d2desc2458z00,
		BGl_z62methodzd2descriptorzd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2332z00zzjas_classfilez00,
		BgL_bgl_za762lambda2156za7622459z00, BGl_z62lambda2156z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2251z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2460za7,
		BGl_z62zc3z04anonymousza31919ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2333z00zzjas_classfilez00,
		BgL_bgl_za762lambda2155za7622461z00, BGl_z62lambda2155z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2252z00zzjas_classfilez00,
		BgL_bgl_za762lambda1917za7622462z00, BGl_z62lambda1917z62zzjas_classfilez00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2334z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2463za7,
		BGl_z62zc3z04anonymousza32164ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2253z00zzjas_classfilez00,
		BgL_bgl_za762lambda1915za7622464z00, BGl_z62lambda1915z62zzjas_classfilez00,
		0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2335z00zzjas_classfilez00,
		BgL_bgl_za762lambda2163za7622465z00, BGl_z62lambda2163z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2254z00zzjas_classfilez00,
		BgL_bgl_za762lambda1946za7622466z00, BGl_z62lambda1946z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2336z00zzjas_classfilez00,
		BgL_bgl_za762lambda2162za7622467z00, BGl_z62lambda2162z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2255z00zzjas_classfilez00,
		BgL_bgl_za762lambda1945za7622468z00, BGl_z62lambda1945z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2337z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2469za7,
		BGl_z62zc3z04anonymousza32171ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2256z00zzjas_classfilez00,
		BgL_bgl_za762lambda1951za7622470z00, BGl_z62lambda1951z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2338z00zzjas_classfilez00,
		BgL_bgl_za762lambda2170za7622471z00, BGl_z62lambda2170z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2257z00zzjas_classfilez00,
		BgL_bgl_za762lambda1950za7622472z00, BGl_z62lambda1950z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2namezd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22473za7,
		BGl_z62fieldzd2orzd2methodzd2namezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2339z00zzjas_classfilez00,
		BgL_bgl_za762lambda2169za7622474z00, BGl_z62lambda2169z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2258z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2475za7,
		BGl_z62zc3z04anonymousza31958ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2346z00zzjas_classfilez00,
		BgL_bgl_string2346za700za7za7j2476za7, "", 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2259z00zzjas_classfilez00,
		BgL_bgl_za762lambda1957za7622477z00, BGl_z62lambda1957z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2347z00zzjas_classfilez00,
		BgL_bgl_string2347za700za7za7j2478za7, "jas_classfile", 13);
	      DEFINE_STRING(BGl_string2348z00zzjas_classfilez00,
		BgL_bgl_string2348za700za7za7j2479za7,
		"as classfile methods fields interfaces super me pooled-names pool-size globals current-method attribute info size method field field-or-method attributes descriptor pname usertype owner classe pool name flags JasFun targs tret type basic jas_classfile JasType obj vect bstring code (5 6) function vector jas-global-value (jas-error classfile msg arg) double float long int short byte char boolean void ",
		402);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_methodzd2attributeszd2envz00zzjas_classfilez00,
		BgL_bgl_za762methodza7d2attr2480z00,
		BGl_z62methodzd2attributeszb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_methodzd2ownerzd2envz00zzjas_classfilez00,
		BgL_bgl_za762methodza7d2owne2481z00,
		BGl_z62methodzd2ownerzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_poolzd2elongzd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2elongza72482za7,
		BGl_z62poolzd2elongzb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2classezd2envz00zzjas_classfilez00,
		BgL_bgl_za762makeza7d2classe2483z00,
		BGl_z62makezd2classezb0zzjas_classfilez00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2340z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2484za7,
		BGl_z62zc3z04anonymousza32178ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2341z00zzjas_classfilez00,
		BgL_bgl_za762lambda2177za7622485z00, BGl_z62lambda2177z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2260z00zzjas_classfilez00,
		BgL_bgl_za762lambda1956za7622486z00, BGl_z62lambda1956z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_methodzd2poolzd2envz00zzjas_classfilez00,
		BgL_bgl_za762methodza7d2pool2487z00,
		BGl_z62methodzd2poolzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2usertypezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22488za7,
		BGl_z62fieldzd2orzd2methodzd2usertypezd2setz12z70zzjas_classfilez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2342z00zzjas_classfilez00,
		BgL_bgl_za762lambda2176za7622489z00, BGl_z62lambda2176z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2261z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2490za7,
		BGl_z62zc3z04anonymousza31941ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2343z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2491za7,
		BGl_z62zc3z04anonymousza32092ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2262z00zzjas_classfilez00,
		BgL_bgl_za762lambda1939za7622492z00, BGl_z62lambda1939z62zzjas_classfilez00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2344z00zzjas_classfilez00,
		BgL_bgl_za762lambda2090za7622493z00, BGl_z62lambda2090z62zzjas_classfilez00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2263z00zzjas_classfilez00,
		BgL_bgl_za762lambda1937za7622494z00, BGl_z62lambda1937z62zzjas_classfilez00,
		0L, BUNSPEC, 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_poolzd2fieldzd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2fieldza72495za7,
		BGl_z62poolzd2fieldzb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2345z00zzjas_classfilez00,
		BgL_bgl_za762lambda2088za7622496z00, BGl_z62lambda2088z62zzjas_classfilez00,
		0L, BUNSPEC, 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2264z00zzjas_classfilez00,
		BgL_bgl_za762lambda1973za7622497z00, BGl_z62lambda1973z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2265z00zzjas_classfilez00,
		BgL_bgl_za762lambda1972za7622498z00, BGl_z62lambda1972z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2266z00zzjas_classfilez00,
		BgL_bgl_za762lambda1978za7622499z00, BGl_z62lambda1978z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2267z00zzjas_classfilez00,
		BgL_bgl_za762lambda1977za7622500z00, BGl_z62lambda1977z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2268z00zzjas_classfilez00,
		BgL_bgl_za762lambda1983za7622501z00, BGl_z62lambda1983z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2269z00zzjas_classfilez00,
		BgL_bgl_za762lambda1982za7622502z00, BGl_z62lambda1982z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_attributezf3zd2envz21zzjas_classfilez00,
		BgL_bgl_za762attributeza7f3za72503za7,
		BGl_z62attributezf3z91zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_methodzd2poolzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762methodza7d2pool2504z00,
		BGl_z62methodzd2poolzd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2270z00zzjas_classfilez00,
		BgL_bgl_za762lambda1988za7622505z00, BGl_z62lambda1988z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_methodzd2usertypezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762methodza7d2user2506z00,
		BGl_z62methodzd2usertypezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2271z00zzjas_classfilez00,
		BgL_bgl_za762lambda1987za7622507z00, BGl_z62lambda1987z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2272z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2508za7,
		BGl_z62zc3z04anonymousza31995ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2273z00zzjas_classfilez00,
		BgL_bgl_za762lambda1994za7622509z00, BGl_z62lambda1994z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_aszd2funtypezd2envz00zzjas_classfilez00,
		BgL_bgl_za762asza7d2funtypeza72510za7,
		BGl_z62aszd2funtypezb0zzjas_classfilez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2274z00zzjas_classfilez00,
		BgL_bgl_za762lambda1993za7622511z00, BGl_z62lambda1993z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2275z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2512za7,
		BGl_z62zc3z04anonymousza32002ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2usertypezd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22513za7,
		BGl_z62fieldzd2orzd2methodzd2usertypezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2276z00zzjas_classfilez00,
		BgL_bgl_za762lambda2001za7622514z00, BGl_z62lambda2001z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_methodzd2attributeszd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762methodza7d2attr2515z00,
		BGl_z62methodzd2attributeszd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2277z00zzjas_classfilez00,
		BgL_bgl_za762lambda2000za7622516z00, BGl_z62lambda2000z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_methodzd2typezd2envz00zzjas_classfilez00,
		BgL_bgl_za762methodza7d2type2517z00,
		BGl_z62methodzd2typezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2278z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2518za7,
		BGl_z62zc3z04anonymousza32010ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2279z00zzjas_classfilez00,
		BgL_bgl_za762lambda2009za7622519z00, BGl_z62lambda2009z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2attributeszd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22520za7,
		BGl_z62fieldzd2orzd2methodzd2attributeszb0zzjas_classfilez00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classezd2vectzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762classeza7d2vect2521z00,
		BGl_z62classezd2vectzd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_methodzd2pnamezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762methodza7d2pnam2522z00,
		BGl_z62methodzd2pnamezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_poolzd2doublezd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2double2523z00,
		BGl_z62poolzd2doublezb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2280z00zzjas_classfilez00,
		BgL_bgl_za762lambda2008za7622524z00, BGl_z62lambda2008z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2281z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2525za7,
		BGl_z62zc3z04anonymousza32017ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2282z00zzjas_classfilez00,
		BgL_bgl_za762lambda2016za7622526z00, BGl_z62lambda2016z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2283z00zzjas_classfilez00,
		BgL_bgl_za762lambda2015za7622527z00, BGl_z62lambda2015z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2284z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2528za7,
		BGl_z62zc3z04anonymousza32024ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2currentzd2methodzd2setz12zd2envz12zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2c2529z00,
		BGl_z62classfilezd2currentzd2methodzd2setz12za2zzjas_classfilez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2285z00zzjas_classfilez00,
		BgL_bgl_za762lambda2023za7622530z00, BGl_z62lambda2023z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2286z00zzjas_classfilez00,
		BgL_bgl_za762lambda2022za7622531z00, BGl_z62lambda2022z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2descriptorzd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2descr2532z00,
		BGl_z62fieldzd2descriptorzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2287z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2533za7,
		BGl_z62zc3z04anonymousza31968ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2288z00zzjas_classfilez00,
		BgL_bgl_za762lambda1966za7622534z00, BGl_z62lambda1966z62zzjas_classfilez00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_fieldzd2namezd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2nameza72535za7,
		BGl_z62fieldzd2namezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2289z00zzjas_classfilez00,
		BgL_bgl_za762lambda1964za7622536z00, BGl_z62lambda1964z62zzjas_classfilez00,
		0L, BUNSPEC, 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2290z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2537za7,
		BGl_z62zc3z04anonymousza32037ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2291z00zzjas_classfilez00,
		BgL_bgl_za762lambda2034za7622538z00, BGl_z62lambda2034z62zzjas_classfilez00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2292z00zzjas_classfilez00,
		BgL_bgl_za762lambda2031za7622539z00, BGl_z62lambda2031z62zzjas_classfilez00,
		0L, BUNSPEC, 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2293z00zzjas_classfilez00,
		BgL_bgl_za762za7c3za704anonymo2540za7,
		BGl_z62zc3z04anonymousza32047ze3ze5zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2294z00zzjas_classfilez00,
		BgL_bgl_za762lambda2045za7622541z00, BGl_z62lambda2045z62zzjas_classfilez00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2295z00zzjas_classfilez00,
		BgL_bgl_za762lambda2043za7622542z00, BGl_z62lambda2043z62zzjas_classfilez00,
		0L, BUNSPEC, 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2296z00zzjas_classfilez00,
		BgL_bgl_za762lambda2063za7622543z00, BGl_z62lambda2063z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2297z00zzjas_classfilez00,
		BgL_bgl_za762lambda2062za7622544z00, BGl_z62lambda2062z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2JasFunzd2envz00zzjas_classfilez00,
		BgL_bgl_za762makeza7d2jasfun2545z00,
		BGl_z62makezd2JasFunzb0zzjas_classfilez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2298z00zzjas_classfilez00,
		BgL_bgl_za762lambda2069za7622546z00, BGl_z62lambda2069z62zzjas_classfilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2299z00zzjas_classfilez00,
		BgL_bgl_za762lambda2068za7622547z00, BGl_z62lambda2068z62zzjas_classfilez00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_JasFunzd2nilzd2envz00zzjas_classfilez00,
		BgL_bgl_za762jasfunza7d2nilza72548za7,
		BGl_z62JasFunzd2nilzb0zzjas_classfilez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2pnamezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22549za7,
		BGl_z62fieldzd2orzd2methodzd2pnamezd2setz12z70zzjas_classfilez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_poolzd2localzd2methodzd2envzd2zzjas_classfilez00,
		BgL_bgl_za762poolza7d2localza72550za7,
		BGl_z62poolzd2localzd2methodz62zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2poolzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22551za7,
		BGl_z62fieldzd2orzd2methodzd2poolzd2setz12z70zzjas_classfilez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_attributezd2typezd2envz00zzjas_classfilez00,
		BgL_bgl_za762attributeza7d2t2552z00,
		BGl_z62attributezd2typezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_attributezd2infozd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762attributeza7d2i2553z00,
		BGl_z62attributezd2infozd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2typezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2typeza72554za7,
		BGl_z62fieldzd2typezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_jaszd2errorzd2envz00zzjas_classfilez00,
		BgL_bgl_za762jasza7d2errorza7b2555za7,
		BGl_z62jaszd2errorzb0zzjas_classfilez00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_poolzd2int64zd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2int64za72556za7,
		BGl_z62poolzd2int64zb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_poolzd2classzd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2classza72557za7,
		BGl_z62poolzd2classzb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_attributezd2nilzd2envz00zzjas_classfilez00,
		BgL_bgl_za762attributeza7d2n2558z00,
		BGl_z62attributezd2nilzb0zzjas_classfilez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2poolzd2siza7ezd2envz75zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2p2559z00,
		BGl_z62classfilezd2poolzd2siza7ezc5zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_methodzd2descriptorzd2envz00zzjas_classfilez00,
		BgL_bgl_za762methodza7d2desc2560z00,
		BGl_z62methodzd2descriptorzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_JasFunzd2codezd2envz00zzjas_classfilez00,
		BgL_bgl_za762jasfunza7d2code2561z00,
		BGl_z62JasFunzd2codezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2superzd2envz00zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2s2562z00,
		BGl_z62classfilezd2superzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2currentzd2methodzd2envzd2zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2c2563z00,
		BGl_z62classfilezd2currentzd2methodz62zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classezd2codezd2envz00zzjas_classfilez00,
		BgL_bgl_za762classeza7d2code2564z00,
		BGl_z62classezd2codezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_basiczd2codezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762basicza7d2codeza72565za7,
		BGl_z62basiczd2codezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_JasFunzd2vectzd2envz00zzjas_classfilez00,
		BgL_bgl_za762jasfunza7d2vect2566z00,
		BGl_z62JasFunzd2vectzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_JasFunzf3zd2envz21zzjas_classfilez00,
		BgL_bgl_za762jasfunza7f3za791za72567z00,
		BGl_z62JasFunzf3z91zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2flagszd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2flags2568z00,
		BGl_z62fieldzd2flagszd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2ownerzd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22569za7,
		BGl_z62fieldzd2orzd2methodzd2ownerzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classezd2vectzd2envz00zzjas_classfilez00,
		BgL_bgl_za762classeza7d2vect2570z00,
		BGl_z62classezd2vectzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectzd2typezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762vectza7d2typeza7d2571za7,
		BGl_z62vectzd2typezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_JasTypezf3zd2envz21zzjas_classfilez00,
		BgL_bgl_za762jastypeza7f3za7912572za7,
		BGl_z62JasTypezf3z91zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectzd2vectzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762vectza7d2vectza7d2573za7,
		BGl_z62vectzd2vectzd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2usertypezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2usert2574z00,
		BGl_z62fieldzd2usertypezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2descriptorzd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22575za7,
		BGl_z62fieldzd2orzd2methodzd2descriptorzb0zzjas_classfilez00, 0L, BUNSPEC,
		1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2basiczd2envz00zzjas_classfilez00,
		BgL_bgl_za762makeza7d2basicza72576za7,
		BGl_z62makezd2basiczb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2fieldszd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2f2577z00,
		BGl_z62classfilezd2fieldszd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2interfaceszd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2i2578z00,
		BGl_z62classfilezd2interfaceszd2setz12z70zzjas_classfilez00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2poolzd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22579za7,
		BGl_z62fieldzd2orzd2methodzd2poolzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_JasFunzd2tretzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762jasfunza7d2tret2580z00,
		BGl_z62JasFunzd2tretzd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classfilezf3zd2envz21zzjas_classfilez00,
		BgL_bgl_za762classfileza7f3za72581za7,
		BGl_z62classfilezf3z91zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_poolzd2longzd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2longza7b2582za7,
		BGl_z62poolzd2longzb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_vectzd2typezd2envz00zzjas_classfilez00,
		BgL_bgl_za762vectza7d2typeza7b2583za7,
		BGl_z62vectzd2typezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_fieldzf3zd2envz21zzjas_classfilez00,
		BgL_bgl_za762fieldza7f3za791za7za72584za7,
		BGl_z62fieldzf3z91zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_JasFunzd2codezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762jasfunza7d2code2585z00,
		BGl_z62JasFunzd2codezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2typezd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22586za7,
		BGl_z62fieldzd2orzd2methodzd2typezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_methodzd2flagszd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762methodza7d2flag2587z00,
		BGl_z62methodzd2flagszd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_methodzd2flagszd2envz00zzjas_classfilez00,
		BgL_bgl_za762methodza7d2flag2588z00,
		BGl_z62methodzd2flagszb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2vectzd2envz00zzjas_classfilez00,
		BgL_bgl_za762makeza7d2vectza7b2589za7,
		BGl_z62makezd2vectzb0zzjas_classfilez00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_methodzf3zd2envz21zzjas_classfilez00,
		BgL_bgl_za762methodza7f3za791za72590z00,
		BGl_z62methodzf3z91zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_JasTypezd2codezd2envz00zzjas_classfilez00,
		BgL_bgl_za762jastypeza7d2cod2591z00,
		BGl_z62JasTypezd2codezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_methodzd2typezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762methodza7d2type2592z00,
		BGl_z62methodzd2typezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_attributezd2infozd2envz00zzjas_classfilez00,
		BgL_bgl_za762attributeza7d2i2593z00,
		BGl_z62attributezd2infozb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_JasTypezd2vectzd2envz00zzjas_classfilez00,
		BgL_bgl_za762jastypeza7d2vec2594z00,
		BGl_z62JasTypezd2vectzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_JasTypezd2codezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762jastypeza7d2cod2595z00,
		BGl_z62JasTypezd2codezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_poolzd2methodzd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2method2596z00,
		BGl_z62poolzd2methodzb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2descriptorzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2descr2597z00,
		BGl_z62fieldzd2descriptorzd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_fieldzd2poolzd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2poolza72598za7,
		BGl_z62fieldzd2poolzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2flagszd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22599za7,
		BGl_z62fieldzd2orzd2methodzd2flagszd2setz12z70zzjas_classfilez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classfilezd2mezd2envz00zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2m2600z00,
		BGl_z62classfilezd2mezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_attributezd2typezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762attributeza7d2t2601z00,
		BGl_z62attributezd2typezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_vectzd2nilzd2envz00zzjas_classfilez00,
		BgL_bgl_za762vectza7d2nilza7b02602za7,
		BGl_z62vectzd2nilzb0zzjas_classfilez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_declaredzd2fieldzd2envz00zzjas_classfilez00,
		BgL_bgl_za762declaredza7d2fi2603z00,
		BGl_z62declaredzd2fieldzb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2interfaceszd2envz00zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2i2604z00,
		BGl_z62classfilezd2interfaceszb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classezd2namezd2envz00zzjas_classfilez00,
		BgL_bgl_za762classeza7d2name2605z00,
		BGl_z62classezd2namezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_fieldzd2typezd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2typeza72606za7,
		BGl_z62fieldzd2typezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2fieldszd2envz00zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2f2607z00,
		BGl_z62classfilezd2fieldszb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2typezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22608za7,
		BGl_z62fieldzd2orzd2methodzd2typezd2setz12z70zzjas_classfilez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2methodzd2envz00zzjas_classfilez00,
		BgL_bgl_za762makeza7d2method2609z00,
		BGl_z62makezd2methodzb0zzjas_classfilez00, 0L, BUNSPEC, 9);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2mezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2m2610z00,
		BGl_z62classfilezd2mezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_vectzf3zd2envz21zzjas_classfilez00,
		BgL_bgl_za762vectza7f3za791za7za7j2611za7,
		BGl_z62vectzf3z91zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2attributeszd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2attri2612z00,
		BGl_z62fieldzd2attributeszd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_JasFunzd2targszd2envz00zzjas_classfilez00,
		BgL_bgl_za762jasfunza7d2targ2613z00,
		BGl_z62JasFunzd2targszb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_poolzd2int32zd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2int32za72614za7,
		BGl_z62poolzd2int32zb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_fieldzd2ownerzd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2owner2615z00,
		BGl_z62fieldzd2ownerzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classezd2codezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762classeza7d2code2616z00,
		BGl_z62classezd2codezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_methodzd2pnamezd2envz00zzjas_classfilez00,
		BgL_bgl_za762methodza7d2pnam2617z00,
		BGl_z62methodzd2pnamezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_attributezd2siza7ezd2envza7zzjas_classfilez00,
		BgL_bgl_za762attributeza7d2s2618z00,
		BGl_z62attributezd2siza7ez17zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_JasFunzd2targszd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762jasfunza7d2targ2619z00,
		BGl_z62JasFunzd2targszd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_JasTypezd2nilzd2envz00zzjas_classfilez00,
		BgL_bgl_za762jastypeza7d2nil2620z00,
		BGl_z62JasTypezd2nilzb0zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_basiczf3zd2envz21zzjas_classfilez00,
		BgL_bgl_za762basicza7f3za791za7za72621za7,
		BGl_z62basiczf3z91zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2attributeszd2envz00zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2a2622z00,
		BGl_z62classfilezd2attributeszb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classezd2nilzd2envz00zzjas_classfilez00,
		BgL_bgl_za762classeza7d2nilza72623za7,
		BGl_z62classezd2nilzb0zzjas_classfilez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_JasFunzd2tretzd2envz00zzjas_classfilez00,
		BgL_bgl_za762jasfunza7d2tret2624z00,
		BGl_z62JasFunzd2tretzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_declaredzd2classzd2envz00zzjas_classfilez00,
		BgL_bgl_za762declaredza7d2cl2625z00,
		BGl_z62declaredzd2classzb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2methodszd2envz00zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2m2626z00,
		BGl_z62classfilezd2methodszb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2flagszd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22627za7,
		BGl_z62fieldzd2orzd2methodzd2flagszb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2globalszd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2g2628z00,
		BGl_z62classfilezd2globalszd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_declaredzd2methodzd2envz00zzjas_classfilez00,
		BgL_bgl_za762declaredza7d2me2629z00,
		BGl_z62declaredzd2methodzb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2flagszd2envz00zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2f2630z00,
		BGl_z62classfilezd2flagszb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_poolzd2uint64zd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2uint642631z00,
		BGl_z62poolzd2uint64zb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2attributezd2envz00zzjas_classfilez00,
		BgL_bgl_za762makeza7d2attrib2632z00,
		BGl_z62makezd2attributezb0zzjas_classfilez00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classezd2namezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762classeza7d2name2633z00,
		BGl_z62classezd2namezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2ownerzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2owner2634z00,
		BGl_z62fieldzd2ownerzd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classfilezd2poolzd2envz00zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2p2635z00,
		BGl_z62classfilezd2poolzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classfilezd2nilzd2envz00zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2n2636z00,
		BGl_z62classfilezd2nilzb0zzjas_classfilez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2siza7ezd2envza7zzjas_classfilez00,
		BgL_bgl_za762typeza7d2siza7a7e2637za7,
		BGl_z62typezd2siza7ez17zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectzd2codezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762vectza7d2codeza7d2638za7,
		BGl_z62vectzd2codezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_attributezd2siza7ezd2setz12zd2envz67zzjas_classfilez00,
		BgL_bgl_za762attributeza7d2s2639z00,
		BGl_z62attributezd2siza7ezd2setz12zd7zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_fieldzd2usertypezd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2usert2640z00,
		BGl_z62fieldzd2usertypezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_poolzd2classzd2byzd2namezd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2classza72641za7,
		BGl_z62poolzd2classzd2byzd2namezb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2pnamezd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22642za7,
		BGl_z62fieldzd2orzd2methodzd2pnamezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2pooledzd2nameszd2setz12zd2envz12zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2p2643z00,
		BGl_z62classfilezd2pooledzd2nameszd2setz12za2zzjas_classfilez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classezd2poolzd2envz00zzjas_classfilez00,
		BgL_bgl_za762classeza7d2pool2644z00,
		BGl_z62classezd2poolzb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2globalszd2envz00zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2g2645z00,
		BGl_z62classfilezd2globalszb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_poolzd2namezd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2nameza7b2646za7,
		BGl_z62poolzd2namezb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2namezd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2nameza72647za7,
		BGl_z62fieldzd2namezd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_methodzd2nilzd2envz00zzjas_classfilez00,
		BgL_bgl_za762methodza7d2nilza72648za7,
		BGl_z62methodzd2nilzb0zzjas_classfilez00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_poolzd2intzd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2intza7b02649za7,
		BGl_z62poolzd2intzb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_methodzd2ownerzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762methodza7d2owne2650z00,
		BGl_z62methodzd2ownerzd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzf3zd2envz21zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22651za7,
		BGl_z62fieldzd2orzd2methodzf3z91zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2JasTypezd2envz00zzjas_classfilez00,
		BgL_bgl_za762makeza7d2jastyp2652z00,
		BGl_z62makezd2JasTypezb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classezf3zd2envz21zzjas_classfilez00,
		BgL_bgl_za762classeza7f3za791za72653z00,
		BGl_z62classezf3z91zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2poolzd2siza7ezd2setz12zd2envzb5zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2p2654z00,
		BGl_z62classfilezd2poolzd2siza7ezd2setz12z05zzjas_classfilez00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_methodzd2namezd2envz00zzjas_classfilez00,
		BgL_bgl_za762methodza7d2name2655z00,
		BGl_z62methodzd2namezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2orzd2methodzd2ownerzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2orza7d22656za7,
		BGl_z62fieldzd2orzd2methodzd2ownerzd2setz12z70zzjas_classfilez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classezd2poolzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762classeza7d2pool2657z00,
		BGl_z62classezd2poolzd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_fieldzd2flagszd2envz00zzjas_classfilez00,
		BgL_bgl_za762fieldza7d2flags2658z00,
		BGl_z62fieldzd2flagszb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2classfilezd2envz00zzjas_classfilez00,
		BgL_bgl_za762makeza7d2classf2659z00,
		BGl_z62makezd2classfilezb0zzjas_classfilez00, 0L, BUNSPEC, 12);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_poolzd2llongzd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2llongza72660za7,
		BGl_z62poolzd2llongzb0zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classfilezd2superzd2setz12zd2envzc0zzjas_classfilez00,
		BgL_bgl_za762classfileza7d2s2661z00,
		BGl_z62classfilezd2superzd2setz12z70zzjas_classfilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classezd2flagszd2envz00zzjas_classfilez00,
		BgL_bgl_za762classeza7d2flag2662z00,
		BGl_z62classezd2flagszb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_vectzd2codezd2envz00zzjas_classfilez00,
		BgL_bgl_za762vectza7d2codeza7b2663za7,
		BGl_z62vectzd2codezb0zzjas_classfilez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_poolzd2stringzd2envz00zzjas_classfilez00,
		BgL_bgl_za762poolza7d2string2664z00,
		BGl_z62poolzd2stringzb0zzjas_classfilez00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzjas_classfilez00));
		     ADD_ROOT((void *) (&BGl_attributez00zzjas_classfilez00));
		     ADD_ROOT((void *) (&BGl_JasFunz00zzjas_classfilez00));
		     ADD_ROOT((void *) (&BGl_methodz00zzjas_classfilez00));
		     ADD_ROOT((void *) (&BGl_basicz00zzjas_classfilez00));
		     ADD_ROOT((void *) (&BGl_JasTypez00zzjas_classfilez00));
		     ADD_ROOT((void *) (&BGl_classez00zzjas_classfilez00));
		     ADD_ROOT((void *) (&BGl_vectz00zzjas_classfilez00));
		     ADD_ROOT((void *) (&BGl_fieldz00zzjas_classfilez00));
		     ADD_ROOT((void *) (&BGl_fieldzd2orzd2methodz00zzjas_classfilez00));
		     ADD_ROOT((void *) (&BGl_pourquoi_tant_de_hainez00zzjas_classfilez00));
		     ADD_ROOT((void *) (&BGl_basiczd2encodedzd2typez00zzjas_classfilez00));
		     ADD_ROOT((void *) (&BGl_classfilez00zzjas_classfilez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzjas_classfilez00(long
		BgL_checksumz00_3620, char *BgL_fromz00_3621)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzjas_classfilez00))
				{
					BGl_requirezd2initializa7ationz75zzjas_classfilez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzjas_classfilez00();
					BGl_libraryzd2moduleszd2initz00zzjas_classfilez00();
					BGl_cnstzd2initzd2zzjas_classfilez00();
					BGl_importedzd2moduleszd2initz00zzjas_classfilez00();
					BGl_objectzd2initzd2zzjas_classfilez00();
					return BGl_toplevelzd2initzd2zzjas_classfilez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.scm 1 */
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"jas_classfile");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "jas_classfile");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"jas_classfile");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"jas_classfile");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"jas_classfile");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "jas_classfile");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"jas_classfile");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "jas_classfile");
			BGl_modulezd2initializa7ationz75zz__hashz00(0L, "jas_classfile");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"jas_classfile");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"jas_classfile");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"jas_classfile");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.scm 1 */
			{	/* Jas/classfile.scm 1 */
				obj_t BgL_cportz00_3285;

				{	/* Jas/classfile.scm 1 */
					obj_t BgL_stringz00_3292;

					BgL_stringz00_3292 = BGl_string2348z00zzjas_classfilez00;
					{	/* Jas/classfile.scm 1 */
						obj_t BgL_startz00_3293;

						BgL_startz00_3293 = BINT(0L);
						{	/* Jas/classfile.scm 1 */
							obj_t BgL_endz00_3294;

							BgL_endz00_3294 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3292)));
							{	/* Jas/classfile.scm 1 */

								BgL_cportz00_3285 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3292, BgL_startz00_3293, BgL_endz00_3294);
				}}}}
				{
					long BgL_iz00_3286;

					BgL_iz00_3286 = 50L;
				BgL_loopz00_3287:
					if ((BgL_iz00_3286 == -1L))
						{	/* Jas/classfile.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* Jas/classfile.scm 1 */
							{	/* Jas/classfile.scm 1 */
								obj_t BgL_arg2349z00_3288;

								{	/* Jas/classfile.scm 1 */

									{	/* Jas/classfile.scm 1 */
										obj_t BgL_locationz00_3290;

										BgL_locationz00_3290 = BBOOL(((bool_t) 0));
										{	/* Jas/classfile.scm 1 */

											BgL_arg2349z00_3288 =
												BGl_readz00zz__readerz00(BgL_cportz00_3285,
												BgL_locationz00_3290);
										}
									}
								}
								{	/* Jas/classfile.scm 1 */
									int BgL_tmpz00_3652;

									BgL_tmpz00_3652 = (int) (BgL_iz00_3286);
									CNST_TABLE_SET(BgL_tmpz00_3652, BgL_arg2349z00_3288);
							}}
							{	/* Jas/classfile.scm 1 */
								int BgL_auxz00_3291;

								BgL_auxz00_3291 = (int) ((BgL_iz00_3286 - 1L));
								{
									long BgL_iz00_3657;

									BgL_iz00_3657 = (long) (BgL_auxz00_3291);
									BgL_iz00_3286 = BgL_iz00_3657;
									goto BgL_loopz00_3287;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.scm 1 */
			{	/* Jas/classfile.scm 101 */
				obj_t BgL_arg1375z00_331;
				obj_t BgL_arg1376z00_332;

				{	/* Jas/classfile.scm 101 */
					BgL_basicz00_bglt BgL_arg1377z00_333;

					{	/* Jas/classfile.scm 101 */
						BgL_basicz00_bglt BgL_new1280z00_334;

						{	/* Jas/classfile.scm 101 */
							BgL_basicz00_bglt BgL_new1279z00_335;

							BgL_new1279z00_335 =
								((BgL_basicz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_basicz00_bgl))));
							{	/* Jas/classfile.scm 101 */
								long BgL_arg1378z00_336;

								BgL_arg1378z00_336 =
									BGL_CLASS_NUM(BGl_basicz00zzjas_classfilez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1279z00_335),
									BgL_arg1378z00_336);
							}
							BgL_new1280z00_334 = BgL_new1279z00_335;
						}
						((((BgL_jastypez00_bglt) COBJECT(
										((BgL_jastypez00_bglt) BgL_new1280z00_334)))->BgL_codez00) =
							((obj_t) BGl_string2210z00zzjas_classfilez00), BUNSPEC);
						((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
											BgL_new1280z00_334)))->BgL_vectz00) =
							((obj_t) BFALSE), BUNSPEC);
						BgL_arg1377z00_333 = BgL_new1280z00_334;
					}
					BgL_arg1375z00_331 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), ((obj_t) BgL_arg1377z00_333));
				}
				{	/* Jas/classfile.scm 102 */
					obj_t BgL_arg1379z00_337;
					obj_t BgL_arg1380z00_338;

					{	/* Jas/classfile.scm 102 */
						BgL_basicz00_bglt BgL_arg1408z00_339;

						{	/* Jas/classfile.scm 102 */
							BgL_basicz00_bglt BgL_new1282z00_340;

							{	/* Jas/classfile.scm 102 */
								BgL_basicz00_bglt BgL_new1281z00_341;

								BgL_new1281z00_341 =
									((BgL_basicz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_basicz00_bgl))));
								{	/* Jas/classfile.scm 102 */
									long BgL_arg1410z00_342;

									BgL_arg1410z00_342 =
										BGL_CLASS_NUM(BGl_basicz00zzjas_classfilez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1281z00_341),
										BgL_arg1410z00_342);
								}
								BgL_new1282z00_340 = BgL_new1281z00_341;
							}
							((((BgL_jastypez00_bglt) COBJECT(
											((BgL_jastypez00_bglt) BgL_new1282z00_340)))->
									BgL_codez00) =
								((obj_t) BGl_string2211z00zzjas_classfilez00), BUNSPEC);
							((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
												BgL_new1282z00_340)))->BgL_vectz00) =
								((obj_t) BFALSE), BUNSPEC);
							BgL_arg1408z00_339 = BgL_new1282z00_340;
						}
						BgL_arg1379z00_337 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(1), ((obj_t) BgL_arg1408z00_339));
					}
					{	/* Jas/classfile.scm 103 */
						obj_t BgL_arg1421z00_343;
						obj_t BgL_arg1422z00_344;

						{	/* Jas/classfile.scm 103 */
							BgL_basicz00_bglt BgL_arg1434z00_345;

							{	/* Jas/classfile.scm 103 */
								BgL_basicz00_bglt BgL_new1284z00_346;

								{	/* Jas/classfile.scm 103 */
									BgL_basicz00_bglt BgL_new1283z00_347;

									BgL_new1283z00_347 =
										((BgL_basicz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_basicz00_bgl))));
									{	/* Jas/classfile.scm 103 */
										long BgL_arg1437z00_348;

										BgL_arg1437z00_348 =
											BGL_CLASS_NUM(BGl_basicz00zzjas_classfilez00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1283z00_347),
											BgL_arg1437z00_348);
									}
									BgL_new1284z00_346 = BgL_new1283z00_347;
								}
								((((BgL_jastypez00_bglt) COBJECT(
												((BgL_jastypez00_bglt) BgL_new1284z00_346)))->
										BgL_codez00) =
									((obj_t) BGl_string2212z00zzjas_classfilez00), BUNSPEC);
								((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
													BgL_new1284z00_346)))->BgL_vectz00) =
									((obj_t) BFALSE), BUNSPEC);
								BgL_arg1434z00_345 = BgL_new1284z00_346;
							}
							BgL_arg1421z00_343 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
								((obj_t) BgL_arg1434z00_345));
						}
						{	/* Jas/classfile.scm 104 */
							obj_t BgL_arg1448z00_349;
							obj_t BgL_arg1453z00_350;

							{	/* Jas/classfile.scm 104 */
								BgL_basicz00_bglt BgL_arg1454z00_351;

								{	/* Jas/classfile.scm 104 */
									BgL_basicz00_bglt BgL_new1286z00_352;

									{	/* Jas/classfile.scm 104 */
										BgL_basicz00_bglt BgL_new1285z00_353;

										BgL_new1285z00_353 =
											((BgL_basicz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_basicz00_bgl))));
										{	/* Jas/classfile.scm 104 */
											long BgL_arg1472z00_354;

											BgL_arg1472z00_354 =
												BGL_CLASS_NUM(BGl_basicz00zzjas_classfilez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1285z00_353),
												BgL_arg1472z00_354);
										}
										BgL_new1286z00_352 = BgL_new1285z00_353;
									}
									((((BgL_jastypez00_bglt) COBJECT(
													((BgL_jastypez00_bglt) BgL_new1286z00_352)))->
											BgL_codez00) =
										((obj_t) BGl_string2213z00zzjas_classfilez00), BUNSPEC);
									((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
														BgL_new1286z00_352)))->BgL_vectz00) =
										((obj_t) BFALSE), BUNSPEC);
									BgL_arg1454z00_351 = BgL_new1286z00_352;
								}
								BgL_arg1448z00_349 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
									((obj_t) BgL_arg1454z00_351));
							}
							{	/* Jas/classfile.scm 105 */
								obj_t BgL_arg1473z00_355;
								obj_t BgL_arg1485z00_356;

								{	/* Jas/classfile.scm 105 */
									BgL_basicz00_bglt BgL_arg1489z00_357;

									{	/* Jas/classfile.scm 105 */
										BgL_basicz00_bglt BgL_new1288z00_358;

										{	/* Jas/classfile.scm 105 */
											BgL_basicz00_bglt BgL_new1287z00_359;

											BgL_new1287z00_359 =
												((BgL_basicz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_basicz00_bgl))));
											{	/* Jas/classfile.scm 105 */
												long BgL_arg1502z00_360;

												BgL_arg1502z00_360 =
													BGL_CLASS_NUM(BGl_basicz00zzjas_classfilez00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1287z00_359),
													BgL_arg1502z00_360);
											}
											BgL_new1288z00_358 = BgL_new1287z00_359;
										}
										((((BgL_jastypez00_bglt) COBJECT(
														((BgL_jastypez00_bglt) BgL_new1288z00_358)))->
												BgL_codez00) =
											((obj_t) BGl_string2214z00zzjas_classfilez00), BUNSPEC);
										((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
															BgL_new1288z00_358)))->BgL_vectz00) =
											((obj_t) BFALSE), BUNSPEC);
										BgL_arg1489z00_357 = BgL_new1288z00_358;
									}
									BgL_arg1473z00_355 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
										((obj_t) BgL_arg1489z00_357));
								}
								{	/* Jas/classfile.scm 106 */
									obj_t BgL_arg1509z00_361;
									obj_t BgL_arg1513z00_362;

									{	/* Jas/classfile.scm 106 */
										BgL_basicz00_bglt BgL_arg1514z00_363;

										{	/* Jas/classfile.scm 106 */
											BgL_basicz00_bglt BgL_new1290z00_364;

											{	/* Jas/classfile.scm 106 */
												BgL_basicz00_bglt BgL_new1289z00_365;

												BgL_new1289z00_365 =
													((BgL_basicz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_basicz00_bgl))));
												{	/* Jas/classfile.scm 106 */
													long BgL_arg1516z00_366;

													BgL_arg1516z00_366 =
														BGL_CLASS_NUM(BGl_basicz00zzjas_classfilez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1289z00_365),
														BgL_arg1516z00_366);
												}
												BgL_new1290z00_364 = BgL_new1289z00_365;
											}
											((((BgL_jastypez00_bglt) COBJECT(
															((BgL_jastypez00_bglt) BgL_new1290z00_364)))->
													BgL_codez00) =
												((obj_t) BGl_string2215z00zzjas_classfilez00), BUNSPEC);
											((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
																BgL_new1290z00_364)))->BgL_vectz00) =
												((obj_t) BFALSE), BUNSPEC);
											BgL_arg1514z00_363 = BgL_new1290z00_364;
										}
										BgL_arg1509z00_361 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
											((obj_t) BgL_arg1514z00_363));
									}
									{	/* Jas/classfile.scm 107 */
										obj_t BgL_arg1535z00_367;
										obj_t BgL_arg1540z00_368;

										{	/* Jas/classfile.scm 107 */
											BgL_basicz00_bglt BgL_arg1544z00_369;

											{	/* Jas/classfile.scm 107 */
												BgL_basicz00_bglt BgL_new1292z00_370;

												{	/* Jas/classfile.scm 107 */
													BgL_basicz00_bglt BgL_new1291z00_371;

													BgL_new1291z00_371 =
														((BgL_basicz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_basicz00_bgl))));
													{	/* Jas/classfile.scm 107 */
														long BgL_arg1546z00_372;

														BgL_arg1546z00_372 =
															BGL_CLASS_NUM(BGl_basicz00zzjas_classfilez00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1291z00_371),
															BgL_arg1546z00_372);
													}
													BgL_new1292z00_370 = BgL_new1291z00_371;
												}
												((((BgL_jastypez00_bglt) COBJECT(
																((BgL_jastypez00_bglt) BgL_new1292z00_370)))->
														BgL_codez00) =
													((obj_t) BGl_string2216z00zzjas_classfilez00),
													BUNSPEC);
												((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
																	BgL_new1292z00_370)))->BgL_vectz00) =
													((obj_t) BFALSE), BUNSPEC);
												BgL_arg1544z00_369 = BgL_new1292z00_370;
											}
											BgL_arg1535z00_367 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
												((obj_t) BgL_arg1544z00_369));
										}
										{	/* Jas/classfile.scm 108 */
											obj_t BgL_arg1552z00_373;
											obj_t BgL_arg1553z00_374;

											{	/* Jas/classfile.scm 108 */
												BgL_basicz00_bglt BgL_arg1559z00_375;

												{	/* Jas/classfile.scm 108 */
													BgL_basicz00_bglt BgL_new1294z00_376;

													{	/* Jas/classfile.scm 108 */
														BgL_basicz00_bglt BgL_new1293z00_377;

														BgL_new1293z00_377 =
															((BgL_basicz00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_basicz00_bgl))));
														{	/* Jas/classfile.scm 108 */
															long BgL_arg1561z00_378;

															BgL_arg1561z00_378 =
																BGL_CLASS_NUM(BGl_basicz00zzjas_classfilez00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1293z00_377),
																BgL_arg1561z00_378);
														}
														BgL_new1294z00_376 = BgL_new1293z00_377;
													}
													((((BgL_jastypez00_bglt) COBJECT(
																	((BgL_jastypez00_bglt) BgL_new1294z00_376)))->
															BgL_codez00) =
														((obj_t) BGl_string2217z00zzjas_classfilez00),
														BUNSPEC);
													((((BgL_jastypez00_bglt)
																COBJECT(((BgL_jastypez00_bglt)
																		BgL_new1294z00_376)))->BgL_vectz00) =
														((obj_t) BFALSE), BUNSPEC);
													BgL_arg1559z00_375 = BgL_new1294z00_376;
												}
												BgL_arg1552z00_373 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
													((obj_t) BgL_arg1559z00_375));
											}
											{	/* Jas/classfile.scm 109 */
												obj_t BgL_arg1564z00_379;

												{	/* Jas/classfile.scm 109 */
													BgL_basicz00_bglt BgL_arg1565z00_380;

													{	/* Jas/classfile.scm 109 */
														BgL_basicz00_bglt BgL_new1296z00_381;

														{	/* Jas/classfile.scm 109 */
															BgL_basicz00_bglt BgL_new1295z00_382;

															BgL_new1295z00_382 =
																((BgL_basicz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_basicz00_bgl))));
															{	/* Jas/classfile.scm 109 */
																long BgL_arg1571z00_383;

																BgL_arg1571z00_383 =
																	BGL_CLASS_NUM(BGl_basicz00zzjas_classfilez00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1295z00_382),
																	BgL_arg1571z00_383);
															}
															BgL_new1296z00_381 = BgL_new1295z00_382;
														}
														((((BgL_jastypez00_bglt) COBJECT(
																		((BgL_jastypez00_bglt)
																			BgL_new1296z00_381)))->BgL_codez00) =
															((obj_t) BGl_string2218z00zzjas_classfilez00),
															BUNSPEC);
														((((BgL_jastypez00_bglt)
																	COBJECT(((BgL_jastypez00_bglt)
																			BgL_new1296z00_381)))->BgL_vectz00) =
															((obj_t) BFALSE), BUNSPEC);
														BgL_arg1565z00_380 = BgL_new1296z00_381;
													}
													BgL_arg1564z00_379 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
														((obj_t) BgL_arg1565z00_380));
												}
												BgL_arg1553z00_374 =
													MAKE_YOUNG_PAIR(BgL_arg1564z00_379, BNIL);
											}
											BgL_arg1540z00_368 =
												MAKE_YOUNG_PAIR(BgL_arg1552z00_373, BgL_arg1553z00_374);
										}
										BgL_arg1513z00_362 =
											MAKE_YOUNG_PAIR(BgL_arg1535z00_367, BgL_arg1540z00_368);
									}
									BgL_arg1485z00_356 =
										MAKE_YOUNG_PAIR(BgL_arg1509z00_361, BgL_arg1513z00_362);
								}
								BgL_arg1453z00_350 =
									MAKE_YOUNG_PAIR(BgL_arg1473z00_355, BgL_arg1485z00_356);
							}
							BgL_arg1422z00_344 =
								MAKE_YOUNG_PAIR(BgL_arg1448z00_349, BgL_arg1453z00_350);
						}
						BgL_arg1380z00_338 =
							MAKE_YOUNG_PAIR(BgL_arg1421z00_343, BgL_arg1422z00_344);
					}
					BgL_arg1376z00_332 =
						MAKE_YOUNG_PAIR(BgL_arg1379z00_337, BgL_arg1380z00_338);
				}
				BGl_basiczd2encodedzd2typez00zzjas_classfilez00 =
					MAKE_YOUNG_PAIR(BgL_arg1375z00_331, BgL_arg1376z00_332);
			}
			{	/* Jas/classfile.scm 111 */
				obj_t BgL_arg1573z00_384;

				BgL_arg1573z00_384 =
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF(3),
					BGl_basiczd2encodedzd2typez00zzjas_classfilez00);
				BGl_pourquoi_tant_de_hainez00zzjas_classfilez00 =
					CDR(((obj_t) BgL_arg1573z00_384));
			}
			{	/* Jas/classfile.scm 114 */
				BgL_vectz00_bglt BgL_arg1575z00_385;

				{	/* Jas/classfile.scm 114 */
					BgL_vectz00_bglt BgL_new1298z00_386;

					{	/* Jas/classfile.scm 115 */
						BgL_vectz00_bglt BgL_new1297z00_387;

						BgL_new1297z00_387 =
							((BgL_vectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_vectz00_bgl))));
						{	/* Jas/classfile.scm 115 */
							long BgL_arg1576z00_388;

							BgL_arg1576z00_388 = BGL_CLASS_NUM(BGl_vectz00zzjas_classfilez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1297z00_387), BgL_arg1576z00_388);
						}
						BgL_new1298z00_386 = BgL_new1297z00_387;
					}
					((((BgL_jastypez00_bglt) COBJECT(
									((BgL_jastypez00_bglt) BgL_new1298z00_386)))->BgL_codez00) =
						((obj_t) BGl_string2219z00zzjas_classfilez00), BUNSPEC);
					((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
										BgL_new1298z00_386)))->BgL_vectz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_vectz00_bglt) COBJECT(BgL_new1298z00_386))->BgL_typez00) =
						((BgL_jastypez00_bglt) ((BgL_jastypez00_bglt)
								BGl_pourquoi_tant_de_hainez00zzjas_classfilez00)), BUNSPEC);
					BgL_arg1575z00_385 = BgL_new1298z00_386;
				}
				{	/* Jas/classfile.scm 113 */
					BgL_jastypez00_bglt BgL_oz00_1398;

					BgL_oz00_1398 =
						((BgL_jastypez00_bglt)
						BGl_pourquoi_tant_de_hainez00zzjas_classfilez00);
					return ((((BgL_jastypez00_bglt) COBJECT(BgL_oz00_1398))->
							BgL_vectz00) = ((obj_t) ((obj_t) BgL_arg1575z00_385)), BUNSPEC);
				}
			}
		}

	}



/* make-JasType */
	BGL_EXPORTED_DEF BgL_jastypez00_bglt
		BGl_makezd2JasTypezd2zzjas_classfilez00(obj_t BgL_code1202z00_3,
		obj_t BgL_vect1203z00_4)
	{
		{	/* Jas/classfile.sch 202 */
			{	/* Jas/classfile.sch 202 */
				BgL_jastypez00_bglt BgL_new1139z00_3296;

				{	/* Jas/classfile.sch 202 */
					BgL_jastypez00_bglt BgL_new1138z00_3297;

					BgL_new1138z00_3297 =
						((BgL_jastypez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_jastypez00_bgl))));
					{	/* Jas/classfile.sch 202 */
						long BgL_arg1589z00_3298;

						BgL_arg1589z00_3298 =
							BGL_CLASS_NUM(BGl_JasTypez00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1138z00_3297), BgL_arg1589z00_3298);
					}
					BgL_new1139z00_3296 = BgL_new1138z00_3297;
				}
				((((BgL_jastypez00_bglt) COBJECT(BgL_new1139z00_3296))->BgL_codez00) =
					((obj_t) BgL_code1202z00_3), BUNSPEC);
				((((BgL_jastypez00_bglt) COBJECT(BgL_new1139z00_3296))->BgL_vectz00) =
					((obj_t) BgL_vect1203z00_4), BUNSPEC);
				return BgL_new1139z00_3296;
			}
		}

	}



/* &make-JasType */
	BgL_jastypez00_bglt BGl_z62makezd2JasTypezb0zzjas_classfilez00(obj_t
		BgL_envz00_2406, obj_t BgL_code1202z00_2407, obj_t BgL_vect1203z00_2408)
	{
		{	/* Jas/classfile.sch 202 */
			return
				BGl_makezd2JasTypezd2zzjas_classfilez00(BgL_code1202z00_2407,
				BgL_vect1203z00_2408);
		}

	}



/* JasType? */
	BGL_EXPORTED_DEF bool_t BGl_JasTypezf3zf3zzjas_classfilez00(obj_t
		BgL_objz00_5)
	{
		{	/* Jas/classfile.sch 203 */
			{	/* Jas/classfile.sch 203 */
				obj_t BgL_classz00_3299;

				BgL_classz00_3299 = BGl_JasTypez00zzjas_classfilez00;
				if (BGL_OBJECTP(BgL_objz00_5))
					{	/* Jas/classfile.sch 203 */
						BgL_objectz00_bglt BgL_arg1807z00_3300;

						BgL_arg1807z00_3300 = (BgL_objectz00_bglt) (BgL_objz00_5);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Jas/classfile.sch 203 */
								long BgL_idxz00_3301;

								BgL_idxz00_3301 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3300);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3301 + 1L)) == BgL_classz00_3299);
							}
						else
							{	/* Jas/classfile.sch 203 */
								bool_t BgL_res2179z00_3304;

								{	/* Jas/classfile.sch 203 */
									obj_t BgL_oclassz00_3305;

									{	/* Jas/classfile.sch 203 */
										obj_t BgL_arg1815z00_3306;
										long BgL_arg1816z00_3307;

										BgL_arg1815z00_3306 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Jas/classfile.sch 203 */
											long BgL_arg1817z00_3308;

											BgL_arg1817z00_3308 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3300);
											BgL_arg1816z00_3307 = (BgL_arg1817z00_3308 - OBJECT_TYPE);
										}
										BgL_oclassz00_3305 =
											VECTOR_REF(BgL_arg1815z00_3306, BgL_arg1816z00_3307);
									}
									{	/* Jas/classfile.sch 203 */
										bool_t BgL__ortest_1115z00_3309;

										BgL__ortest_1115z00_3309 =
											(BgL_classz00_3299 == BgL_oclassz00_3305);
										if (BgL__ortest_1115z00_3309)
											{	/* Jas/classfile.sch 203 */
												BgL_res2179z00_3304 = BgL__ortest_1115z00_3309;
											}
										else
											{	/* Jas/classfile.sch 203 */
												long BgL_odepthz00_3310;

												{	/* Jas/classfile.sch 203 */
													obj_t BgL_arg1804z00_3311;

													BgL_arg1804z00_3311 = (BgL_oclassz00_3305);
													BgL_odepthz00_3310 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3311);
												}
												if ((1L < BgL_odepthz00_3310))
													{	/* Jas/classfile.sch 203 */
														obj_t BgL_arg1802z00_3312;

														{	/* Jas/classfile.sch 203 */
															obj_t BgL_arg1803z00_3313;

															BgL_arg1803z00_3313 = (BgL_oclassz00_3305);
															BgL_arg1802z00_3312 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3313,
																1L);
														}
														BgL_res2179z00_3304 =
															(BgL_arg1802z00_3312 == BgL_classz00_3299);
													}
												else
													{	/* Jas/classfile.sch 203 */
														BgL_res2179z00_3304 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2179z00_3304;
							}
					}
				else
					{	/* Jas/classfile.sch 203 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &JasType? */
	obj_t BGl_z62JasTypezf3z91zzjas_classfilez00(obj_t BgL_envz00_2409,
		obj_t BgL_objz00_2410)
	{
		{	/* Jas/classfile.sch 203 */
			return BBOOL(BGl_JasTypezf3zf3zzjas_classfilez00(BgL_objz00_2410));
		}

	}



/* JasType-nil */
	BGL_EXPORTED_DEF BgL_jastypez00_bglt
		BGl_JasTypezd2nilzd2zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.sch 204 */
			{	/* Jas/classfile.sch 204 */
				obj_t BgL_classz00_1440;

				BgL_classz00_1440 = BGl_JasTypez00zzjas_classfilez00;
				{	/* Jas/classfile.sch 204 */
					obj_t BgL__ortest_1117z00_1441;

					BgL__ortest_1117z00_1441 = BGL_CLASS_NIL(BgL_classz00_1440);
					if (CBOOL(BgL__ortest_1117z00_1441))
						{	/* Jas/classfile.sch 204 */
							return ((BgL_jastypez00_bglt) BgL__ortest_1117z00_1441);
						}
					else
						{	/* Jas/classfile.sch 204 */
							return
								((BgL_jastypez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1440));
						}
				}
			}
		}

	}



/* &JasType-nil */
	BgL_jastypez00_bglt BGl_z62JasTypezd2nilzb0zzjas_classfilez00(obj_t
		BgL_envz00_2411)
	{
		{	/* Jas/classfile.sch 204 */
			return BGl_JasTypezd2nilzd2zzjas_classfilez00();
		}

	}



/* JasType-vect */
	BGL_EXPORTED_DEF obj_t
		BGl_JasTypezd2vectzd2zzjas_classfilez00(BgL_jastypez00_bglt BgL_oz00_6)
	{
		{	/* Jas/classfile.sch 205 */
			return (((BgL_jastypez00_bglt) COBJECT(BgL_oz00_6))->BgL_vectz00);
		}

	}



/* &JasType-vect */
	obj_t BGl_z62JasTypezd2vectzb0zzjas_classfilez00(obj_t BgL_envz00_2412,
		obj_t BgL_oz00_2413)
	{
		{	/* Jas/classfile.sch 205 */
			return
				BGl_JasTypezd2vectzd2zzjas_classfilez00(
				((BgL_jastypez00_bglt) BgL_oz00_2413));
		}

	}



/* JasType-vect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_JasTypezd2vectzd2setz12z12zzjas_classfilez00(BgL_jastypez00_bglt
		BgL_oz00_7, obj_t BgL_vz00_8)
	{
		{	/* Jas/classfile.sch 206 */
			return
				((((BgL_jastypez00_bglt) COBJECT(BgL_oz00_7))->BgL_vectz00) =
				((obj_t) BgL_vz00_8), BUNSPEC);
		}

	}



/* &JasType-vect-set! */
	obj_t BGl_z62JasTypezd2vectzd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2414, obj_t BgL_oz00_2415, obj_t BgL_vz00_2416)
	{
		{	/* Jas/classfile.sch 206 */
			return
				BGl_JasTypezd2vectzd2setz12z12zzjas_classfilez00(
				((BgL_jastypez00_bglt) BgL_oz00_2415), BgL_vz00_2416);
		}

	}



/* JasType-code */
	BGL_EXPORTED_DEF obj_t
		BGl_JasTypezd2codezd2zzjas_classfilez00(BgL_jastypez00_bglt BgL_oz00_9)
	{
		{	/* Jas/classfile.sch 207 */
			return (((BgL_jastypez00_bglt) COBJECT(BgL_oz00_9))->BgL_codez00);
		}

	}



/* &JasType-code */
	obj_t BGl_z62JasTypezd2codezb0zzjas_classfilez00(obj_t BgL_envz00_2417,
		obj_t BgL_oz00_2418)
	{
		{	/* Jas/classfile.sch 207 */
			return
				BGl_JasTypezd2codezd2zzjas_classfilez00(
				((BgL_jastypez00_bglt) BgL_oz00_2418));
		}

	}



/* JasType-code-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_JasTypezd2codezd2setz12z12zzjas_classfilez00(BgL_jastypez00_bglt
		BgL_oz00_10, obj_t BgL_vz00_11)
	{
		{	/* Jas/classfile.sch 208 */
			return
				((((BgL_jastypez00_bglt) COBJECT(BgL_oz00_10))->BgL_codez00) =
				((obj_t) BgL_vz00_11), BUNSPEC);
		}

	}



/* &JasType-code-set! */
	obj_t BGl_z62JasTypezd2codezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2419, obj_t BgL_oz00_2420, obj_t BgL_vz00_2421)
	{
		{	/* Jas/classfile.sch 208 */
			return
				BGl_JasTypezd2codezd2setz12z12zzjas_classfilez00(
				((BgL_jastypez00_bglt) BgL_oz00_2420), BgL_vz00_2421);
		}

	}



/* make-basic */
	BGL_EXPORTED_DEF BgL_basicz00_bglt BGl_makezd2basiczd2zzjas_classfilez00(obj_t
		BgL_code1199z00_12, obj_t BgL_vect1200z00_13)
	{
		{	/* Jas/classfile.sch 211 */
			{	/* Jas/classfile.sch 211 */
				BgL_basicz00_bglt BgL_new1145z00_3314;

				{	/* Jas/classfile.sch 211 */
					BgL_basicz00_bglt BgL_new1144z00_3315;

					BgL_new1144z00_3315 =
						((BgL_basicz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_basicz00_bgl))));
					{	/* Jas/classfile.sch 211 */
						long BgL_arg1591z00_3316;

						BgL_arg1591z00_3316 = BGL_CLASS_NUM(BGl_basicz00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1144z00_3315), BgL_arg1591z00_3316);
					}
					BgL_new1145z00_3314 = BgL_new1144z00_3315;
				}
				((((BgL_jastypez00_bglt) COBJECT(
								((BgL_jastypez00_bglt) BgL_new1145z00_3314)))->BgL_codez00) =
					((obj_t) BgL_code1199z00_12), BUNSPEC);
				((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
									BgL_new1145z00_3314)))->BgL_vectz00) =
					((obj_t) BgL_vect1200z00_13), BUNSPEC);
				return BgL_new1145z00_3314;
			}
		}

	}



/* &make-basic */
	BgL_basicz00_bglt BGl_z62makezd2basiczb0zzjas_classfilez00(obj_t
		BgL_envz00_2422, obj_t BgL_code1199z00_2423, obj_t BgL_vect1200z00_2424)
	{
		{	/* Jas/classfile.sch 211 */
			return
				BGl_makezd2basiczd2zzjas_classfilez00(BgL_code1199z00_2423,
				BgL_vect1200z00_2424);
		}

	}



/* basic? */
	BGL_EXPORTED_DEF bool_t BGl_basiczf3zf3zzjas_classfilez00(obj_t BgL_objz00_14)
	{
		{	/* Jas/classfile.sch 212 */
			{	/* Jas/classfile.sch 212 */
				obj_t BgL_classz00_3317;

				BgL_classz00_3317 = BGl_basicz00zzjas_classfilez00;
				if (BGL_OBJECTP(BgL_objz00_14))
					{	/* Jas/classfile.sch 212 */
						BgL_objectz00_bglt BgL_arg1807z00_3318;

						BgL_arg1807z00_3318 = (BgL_objectz00_bglt) (BgL_objz00_14);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Jas/classfile.sch 212 */
								long BgL_idxz00_3319;

								BgL_idxz00_3319 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3318);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3319 + 2L)) == BgL_classz00_3317);
							}
						else
							{	/* Jas/classfile.sch 212 */
								bool_t BgL_res2180z00_3322;

								{	/* Jas/classfile.sch 212 */
									obj_t BgL_oclassz00_3323;

									{	/* Jas/classfile.sch 212 */
										obj_t BgL_arg1815z00_3324;
										long BgL_arg1816z00_3325;

										BgL_arg1815z00_3324 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Jas/classfile.sch 212 */
											long BgL_arg1817z00_3326;

											BgL_arg1817z00_3326 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3318);
											BgL_arg1816z00_3325 = (BgL_arg1817z00_3326 - OBJECT_TYPE);
										}
										BgL_oclassz00_3323 =
											VECTOR_REF(BgL_arg1815z00_3324, BgL_arg1816z00_3325);
									}
									{	/* Jas/classfile.sch 212 */
										bool_t BgL__ortest_1115z00_3327;

										BgL__ortest_1115z00_3327 =
											(BgL_classz00_3317 == BgL_oclassz00_3323);
										if (BgL__ortest_1115z00_3327)
											{	/* Jas/classfile.sch 212 */
												BgL_res2180z00_3322 = BgL__ortest_1115z00_3327;
											}
										else
											{	/* Jas/classfile.sch 212 */
												long BgL_odepthz00_3328;

												{	/* Jas/classfile.sch 212 */
													obj_t BgL_arg1804z00_3329;

													BgL_arg1804z00_3329 = (BgL_oclassz00_3323);
													BgL_odepthz00_3328 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3329);
												}
												if ((2L < BgL_odepthz00_3328))
													{	/* Jas/classfile.sch 212 */
														obj_t BgL_arg1802z00_3330;

														{	/* Jas/classfile.sch 212 */
															obj_t BgL_arg1803z00_3331;

															BgL_arg1803z00_3331 = (BgL_oclassz00_3323);
															BgL_arg1802z00_3330 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3331,
																2L);
														}
														BgL_res2180z00_3322 =
															(BgL_arg1802z00_3330 == BgL_classz00_3317);
													}
												else
													{	/* Jas/classfile.sch 212 */
														BgL_res2180z00_3322 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2180z00_3322;
							}
					}
				else
					{	/* Jas/classfile.sch 212 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &basic? */
	obj_t BGl_z62basiczf3z91zzjas_classfilez00(obj_t BgL_envz00_2425,
		obj_t BgL_objz00_2426)
	{
		{	/* Jas/classfile.sch 212 */
			return BBOOL(BGl_basiczf3zf3zzjas_classfilez00(BgL_objz00_2426));
		}

	}



/* basic-nil */
	BGL_EXPORTED_DEF BgL_basicz00_bglt BGl_basiczd2nilzd2zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.sch 213 */
			{	/* Jas/classfile.sch 213 */
				obj_t BgL_classz00_1486;

				BgL_classz00_1486 = BGl_basicz00zzjas_classfilez00;
				{	/* Jas/classfile.sch 213 */
					obj_t BgL__ortest_1117z00_1487;

					BgL__ortest_1117z00_1487 = BGL_CLASS_NIL(BgL_classz00_1486);
					if (CBOOL(BgL__ortest_1117z00_1487))
						{	/* Jas/classfile.sch 213 */
							return ((BgL_basicz00_bglt) BgL__ortest_1117z00_1487);
						}
					else
						{	/* Jas/classfile.sch 213 */
							return
								((BgL_basicz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1486));
						}
				}
			}
		}

	}



/* &basic-nil */
	BgL_basicz00_bglt BGl_z62basiczd2nilzb0zzjas_classfilez00(obj_t
		BgL_envz00_2427)
	{
		{	/* Jas/classfile.sch 213 */
			return BGl_basiczd2nilzd2zzjas_classfilez00();
		}

	}



/* basic-vect */
	BGL_EXPORTED_DEF obj_t BGl_basiczd2vectzd2zzjas_classfilez00(BgL_basicz00_bglt
		BgL_oz00_15)
	{
		{	/* Jas/classfile.sch 214 */
			return
				(((BgL_jastypez00_bglt) COBJECT(
						((BgL_jastypez00_bglt) BgL_oz00_15)))->BgL_vectz00);
		}

	}



/* &basic-vect */
	obj_t BGl_z62basiczd2vectzb0zzjas_classfilez00(obj_t BgL_envz00_2428,
		obj_t BgL_oz00_2429)
	{
		{	/* Jas/classfile.sch 214 */
			return
				BGl_basiczd2vectzd2zzjas_classfilez00(
				((BgL_basicz00_bglt) BgL_oz00_2429));
		}

	}



/* basic-vect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_basiczd2vectzd2setz12z12zzjas_classfilez00(BgL_basicz00_bglt
		BgL_oz00_16, obj_t BgL_vz00_17)
	{
		{	/* Jas/classfile.sch 215 */
			return
				((((BgL_jastypez00_bglt) COBJECT(
							((BgL_jastypez00_bglt) BgL_oz00_16)))->BgL_vectz00) =
				((obj_t) BgL_vz00_17), BUNSPEC);
		}

	}



/* &basic-vect-set! */
	obj_t BGl_z62basiczd2vectzd2setz12z70zzjas_classfilez00(obj_t BgL_envz00_2430,
		obj_t BgL_oz00_2431, obj_t BgL_vz00_2432)
	{
		{	/* Jas/classfile.sch 215 */
			return
				BGl_basiczd2vectzd2setz12z12zzjas_classfilez00(
				((BgL_basicz00_bglt) BgL_oz00_2431), BgL_vz00_2432);
		}

	}



/* basic-code */
	BGL_EXPORTED_DEF obj_t BGl_basiczd2codezd2zzjas_classfilez00(BgL_basicz00_bglt
		BgL_oz00_18)
	{
		{	/* Jas/classfile.sch 216 */
			return
				(((BgL_jastypez00_bglt) COBJECT(
						((BgL_jastypez00_bglt) BgL_oz00_18)))->BgL_codez00);
		}

	}



/* &basic-code */
	obj_t BGl_z62basiczd2codezb0zzjas_classfilez00(obj_t BgL_envz00_2433,
		obj_t BgL_oz00_2434)
	{
		{	/* Jas/classfile.sch 216 */
			return
				BGl_basiczd2codezd2zzjas_classfilez00(
				((BgL_basicz00_bglt) BgL_oz00_2434));
		}

	}



/* basic-code-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_basiczd2codezd2setz12z12zzjas_classfilez00(BgL_basicz00_bglt
		BgL_oz00_19, obj_t BgL_vz00_20)
	{
		{	/* Jas/classfile.sch 217 */
			return
				((((BgL_jastypez00_bglt) COBJECT(
							((BgL_jastypez00_bglt) BgL_oz00_19)))->BgL_codez00) =
				((obj_t) BgL_vz00_20), BUNSPEC);
		}

	}



/* &basic-code-set! */
	obj_t BGl_z62basiczd2codezd2setz12z70zzjas_classfilez00(obj_t BgL_envz00_2435,
		obj_t BgL_oz00_2436, obj_t BgL_vz00_2437)
	{
		{	/* Jas/classfile.sch 217 */
			return
				BGl_basiczd2codezd2setz12z12zzjas_classfilez00(
				((BgL_basicz00_bglt) BgL_oz00_2436), BgL_vz00_2437);
		}

	}



/* make-vect */
	BGL_EXPORTED_DEF BgL_vectz00_bglt BGl_makezd2vectzd2zzjas_classfilez00(obj_t
		BgL_code1195z00_21, obj_t BgL_vect1196z00_22,
		BgL_jastypez00_bglt BgL_type1197z00_23)
	{
		{	/* Jas/classfile.sch 220 */
			{	/* Jas/classfile.sch 220 */
				BgL_vectz00_bglt BgL_new1151z00_3332;

				{	/* Jas/classfile.sch 220 */
					BgL_vectz00_bglt BgL_new1150z00_3333;

					BgL_new1150z00_3333 =
						((BgL_vectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_vectz00_bgl))));
					{	/* Jas/classfile.sch 220 */
						long BgL_arg1593z00_3334;

						BgL_arg1593z00_3334 = BGL_CLASS_NUM(BGl_vectz00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1150z00_3333), BgL_arg1593z00_3334);
					}
					BgL_new1151z00_3332 = BgL_new1150z00_3333;
				}
				((((BgL_jastypez00_bglt) COBJECT(
								((BgL_jastypez00_bglt) BgL_new1151z00_3332)))->BgL_codez00) =
					((obj_t) BgL_code1195z00_21), BUNSPEC);
				((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
									BgL_new1151z00_3332)))->BgL_vectz00) =
					((obj_t) BgL_vect1196z00_22), BUNSPEC);
				((((BgL_vectz00_bglt) COBJECT(BgL_new1151z00_3332))->BgL_typez00) =
					((BgL_jastypez00_bglt) BgL_type1197z00_23), BUNSPEC);
				return BgL_new1151z00_3332;
			}
		}

	}



/* &make-vect */
	BgL_vectz00_bglt BGl_z62makezd2vectzb0zzjas_classfilez00(obj_t
		BgL_envz00_2438, obj_t BgL_code1195z00_2439, obj_t BgL_vect1196z00_2440,
		obj_t BgL_type1197z00_2441)
	{
		{	/* Jas/classfile.sch 220 */
			return
				BGl_makezd2vectzd2zzjas_classfilez00(BgL_code1195z00_2439,
				BgL_vect1196z00_2440, ((BgL_jastypez00_bglt) BgL_type1197z00_2441));
		}

	}



/* vect? */
	BGL_EXPORTED_DEF bool_t BGl_vectzf3zf3zzjas_classfilez00(obj_t BgL_objz00_24)
	{
		{	/* Jas/classfile.sch 221 */
			{	/* Jas/classfile.sch 221 */
				obj_t BgL_classz00_3335;

				BgL_classz00_3335 = BGl_vectz00zzjas_classfilez00;
				if (BGL_OBJECTP(BgL_objz00_24))
					{	/* Jas/classfile.sch 221 */
						BgL_objectz00_bglt BgL_arg1807z00_3336;

						BgL_arg1807z00_3336 = (BgL_objectz00_bglt) (BgL_objz00_24);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Jas/classfile.sch 221 */
								long BgL_idxz00_3337;

								BgL_idxz00_3337 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3336);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3337 + 2L)) == BgL_classz00_3335);
							}
						else
							{	/* Jas/classfile.sch 221 */
								bool_t BgL_res2181z00_3340;

								{	/* Jas/classfile.sch 221 */
									obj_t BgL_oclassz00_3341;

									{	/* Jas/classfile.sch 221 */
										obj_t BgL_arg1815z00_3342;
										long BgL_arg1816z00_3343;

										BgL_arg1815z00_3342 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Jas/classfile.sch 221 */
											long BgL_arg1817z00_3344;

											BgL_arg1817z00_3344 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3336);
											BgL_arg1816z00_3343 = (BgL_arg1817z00_3344 - OBJECT_TYPE);
										}
										BgL_oclassz00_3341 =
											VECTOR_REF(BgL_arg1815z00_3342, BgL_arg1816z00_3343);
									}
									{	/* Jas/classfile.sch 221 */
										bool_t BgL__ortest_1115z00_3345;

										BgL__ortest_1115z00_3345 =
											(BgL_classz00_3335 == BgL_oclassz00_3341);
										if (BgL__ortest_1115z00_3345)
											{	/* Jas/classfile.sch 221 */
												BgL_res2181z00_3340 = BgL__ortest_1115z00_3345;
											}
										else
											{	/* Jas/classfile.sch 221 */
												long BgL_odepthz00_3346;

												{	/* Jas/classfile.sch 221 */
													obj_t BgL_arg1804z00_3347;

													BgL_arg1804z00_3347 = (BgL_oclassz00_3341);
													BgL_odepthz00_3346 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3347);
												}
												if ((2L < BgL_odepthz00_3346))
													{	/* Jas/classfile.sch 221 */
														obj_t BgL_arg1802z00_3348;

														{	/* Jas/classfile.sch 221 */
															obj_t BgL_arg1803z00_3349;

															BgL_arg1803z00_3349 = (BgL_oclassz00_3341);
															BgL_arg1802z00_3348 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3349,
																2L);
														}
														BgL_res2181z00_3340 =
															(BgL_arg1802z00_3348 == BgL_classz00_3335);
													}
												else
													{	/* Jas/classfile.sch 221 */
														BgL_res2181z00_3340 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2181z00_3340;
							}
					}
				else
					{	/* Jas/classfile.sch 221 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &vect? */
	obj_t BGl_z62vectzf3z91zzjas_classfilez00(obj_t BgL_envz00_2442,
		obj_t BgL_objz00_2443)
	{
		{	/* Jas/classfile.sch 221 */
			return BBOOL(BGl_vectzf3zf3zzjas_classfilez00(BgL_objz00_2443));
		}

	}



/* vect-nil */
	BGL_EXPORTED_DEF BgL_vectz00_bglt BGl_vectzd2nilzd2zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.sch 222 */
			{	/* Jas/classfile.sch 222 */
				obj_t BgL_classz00_1532;

				BgL_classz00_1532 = BGl_vectz00zzjas_classfilez00;
				{	/* Jas/classfile.sch 222 */
					obj_t BgL__ortest_1117z00_1533;

					BgL__ortest_1117z00_1533 = BGL_CLASS_NIL(BgL_classz00_1532);
					if (CBOOL(BgL__ortest_1117z00_1533))
						{	/* Jas/classfile.sch 222 */
							return ((BgL_vectz00_bglt) BgL__ortest_1117z00_1533);
						}
					else
						{	/* Jas/classfile.sch 222 */
							return
								((BgL_vectz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1532));
						}
				}
			}
		}

	}



/* &vect-nil */
	BgL_vectz00_bglt BGl_z62vectzd2nilzb0zzjas_classfilez00(obj_t BgL_envz00_2444)
	{
		{	/* Jas/classfile.sch 222 */
			return BGl_vectzd2nilzd2zzjas_classfilez00();
		}

	}



/* vect-type */
	BGL_EXPORTED_DEF BgL_jastypez00_bglt
		BGl_vectzd2typezd2zzjas_classfilez00(BgL_vectz00_bglt BgL_oz00_25)
	{
		{	/* Jas/classfile.sch 223 */
			return (((BgL_vectz00_bglt) COBJECT(BgL_oz00_25))->BgL_typez00);
		}

	}



/* &vect-type */
	BgL_jastypez00_bglt BGl_z62vectzd2typezb0zzjas_classfilez00(obj_t
		BgL_envz00_2445, obj_t BgL_oz00_2446)
	{
		{	/* Jas/classfile.sch 223 */
			return
				BGl_vectzd2typezd2zzjas_classfilez00(
				((BgL_vectz00_bglt) BgL_oz00_2446));
		}

	}



/* vect-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vectzd2typezd2setz12z12zzjas_classfilez00(BgL_vectz00_bglt BgL_oz00_26,
		BgL_jastypez00_bglt BgL_vz00_27)
	{
		{	/* Jas/classfile.sch 224 */
			return
				((((BgL_vectz00_bglt) COBJECT(BgL_oz00_26))->BgL_typez00) =
				((BgL_jastypez00_bglt) BgL_vz00_27), BUNSPEC);
		}

	}



/* &vect-type-set! */
	obj_t BGl_z62vectzd2typezd2setz12z70zzjas_classfilez00(obj_t BgL_envz00_2447,
		obj_t BgL_oz00_2448, obj_t BgL_vz00_2449)
	{
		{	/* Jas/classfile.sch 224 */
			return
				BGl_vectzd2typezd2setz12z12zzjas_classfilez00(
				((BgL_vectz00_bglt) BgL_oz00_2448),
				((BgL_jastypez00_bglt) BgL_vz00_2449));
		}

	}



/* vect-vect */
	BGL_EXPORTED_DEF obj_t BGl_vectzd2vectzd2zzjas_classfilez00(BgL_vectz00_bglt
		BgL_oz00_28)
	{
		{	/* Jas/classfile.sch 225 */
			return
				(((BgL_jastypez00_bglt) COBJECT(
						((BgL_jastypez00_bglt) BgL_oz00_28)))->BgL_vectz00);
		}

	}



/* &vect-vect */
	obj_t BGl_z62vectzd2vectzb0zzjas_classfilez00(obj_t BgL_envz00_2450,
		obj_t BgL_oz00_2451)
	{
		{	/* Jas/classfile.sch 225 */
			return
				BGl_vectzd2vectzd2zzjas_classfilez00(
				((BgL_vectz00_bglt) BgL_oz00_2451));
		}

	}



/* vect-vect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vectzd2vectzd2setz12z12zzjas_classfilez00(BgL_vectz00_bglt BgL_oz00_29,
		obj_t BgL_vz00_30)
	{
		{	/* Jas/classfile.sch 226 */
			return
				((((BgL_jastypez00_bglt) COBJECT(
							((BgL_jastypez00_bglt) BgL_oz00_29)))->BgL_vectz00) =
				((obj_t) BgL_vz00_30), BUNSPEC);
		}

	}



/* &vect-vect-set! */
	obj_t BGl_z62vectzd2vectzd2setz12z70zzjas_classfilez00(obj_t BgL_envz00_2452,
		obj_t BgL_oz00_2453, obj_t BgL_vz00_2454)
	{
		{	/* Jas/classfile.sch 226 */
			return
				BGl_vectzd2vectzd2setz12z12zzjas_classfilez00(
				((BgL_vectz00_bglt) BgL_oz00_2453), BgL_vz00_2454);
		}

	}



/* vect-code */
	BGL_EXPORTED_DEF obj_t BGl_vectzd2codezd2zzjas_classfilez00(BgL_vectz00_bglt
		BgL_oz00_31)
	{
		{	/* Jas/classfile.sch 227 */
			return
				(((BgL_jastypez00_bglt) COBJECT(
						((BgL_jastypez00_bglt) BgL_oz00_31)))->BgL_codez00);
		}

	}



/* &vect-code */
	obj_t BGl_z62vectzd2codezb0zzjas_classfilez00(obj_t BgL_envz00_2455,
		obj_t BgL_oz00_2456)
	{
		{	/* Jas/classfile.sch 227 */
			return
				BGl_vectzd2codezd2zzjas_classfilez00(
				((BgL_vectz00_bglt) BgL_oz00_2456));
		}

	}



/* vect-code-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vectzd2codezd2setz12z12zzjas_classfilez00(BgL_vectz00_bglt BgL_oz00_32,
		obj_t BgL_vz00_33)
	{
		{	/* Jas/classfile.sch 228 */
			return
				((((BgL_jastypez00_bglt) COBJECT(
							((BgL_jastypez00_bglt) BgL_oz00_32)))->BgL_codez00) =
				((obj_t) BgL_vz00_33), BUNSPEC);
		}

	}



/* &vect-code-set! */
	obj_t BGl_z62vectzd2codezd2setz12z70zzjas_classfilez00(obj_t BgL_envz00_2457,
		obj_t BgL_oz00_2458, obj_t BgL_vz00_2459)
	{
		{	/* Jas/classfile.sch 228 */
			return
				BGl_vectzd2codezd2setz12z12zzjas_classfilez00(
				((BgL_vectz00_bglt) BgL_oz00_2458), BgL_vz00_2459);
		}

	}



/* make-JasFun */
	BGL_EXPORTED_DEF BgL_jasfunz00_bglt
		BGl_makezd2JasFunzd2zzjas_classfilez00(obj_t BgL_code1190z00_34,
		obj_t BgL_vect1191z00_35, BgL_jastypez00_bglt BgL_tret1192z00_36,
		obj_t BgL_targs1193z00_37)
	{
		{	/* Jas/classfile.sch 231 */
			{	/* Jas/classfile.sch 231 */
				BgL_jasfunz00_bglt BgL_new1159z00_3350;

				{	/* Jas/classfile.sch 231 */
					BgL_jasfunz00_bglt BgL_new1158z00_3351;

					BgL_new1158z00_3351 =
						((BgL_jasfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_jasfunz00_bgl))));
					{	/* Jas/classfile.sch 231 */
						long BgL_arg1594z00_3352;

						BgL_arg1594z00_3352 =
							BGL_CLASS_NUM(BGl_JasFunz00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1158z00_3351), BgL_arg1594z00_3352);
					}
					BgL_new1159z00_3350 = BgL_new1158z00_3351;
				}
				((((BgL_jastypez00_bglt) COBJECT(
								((BgL_jastypez00_bglt) BgL_new1159z00_3350)))->BgL_codez00) =
					((obj_t) BgL_code1190z00_34), BUNSPEC);
				((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
									BgL_new1159z00_3350)))->BgL_vectz00) =
					((obj_t) BgL_vect1191z00_35), BUNSPEC);
				((((BgL_jasfunz00_bglt) COBJECT(BgL_new1159z00_3350))->BgL_tretz00) =
					((BgL_jastypez00_bglt) BgL_tret1192z00_36), BUNSPEC);
				((((BgL_jasfunz00_bglt) COBJECT(BgL_new1159z00_3350))->BgL_targsz00) =
					((obj_t) BgL_targs1193z00_37), BUNSPEC);
				return BgL_new1159z00_3350;
			}
		}

	}



/* &make-JasFun */
	BgL_jasfunz00_bglt BGl_z62makezd2JasFunzb0zzjas_classfilez00(obj_t
		BgL_envz00_2460, obj_t BgL_code1190z00_2461, obj_t BgL_vect1191z00_2462,
		obj_t BgL_tret1192z00_2463, obj_t BgL_targs1193z00_2464)
	{
		{	/* Jas/classfile.sch 231 */
			return
				BGl_makezd2JasFunzd2zzjas_classfilez00(BgL_code1190z00_2461,
				BgL_vect1191z00_2462, ((BgL_jastypez00_bglt) BgL_tret1192z00_2463),
				BgL_targs1193z00_2464);
		}

	}



/* JasFun? */
	BGL_EXPORTED_DEF bool_t BGl_JasFunzf3zf3zzjas_classfilez00(obj_t
		BgL_objz00_38)
	{
		{	/* Jas/classfile.sch 232 */
			{	/* Jas/classfile.sch 232 */
				obj_t BgL_classz00_3353;

				BgL_classz00_3353 = BGl_JasFunz00zzjas_classfilez00;
				if (BGL_OBJECTP(BgL_objz00_38))
					{	/* Jas/classfile.sch 232 */
						BgL_objectz00_bglt BgL_arg1807z00_3354;

						BgL_arg1807z00_3354 = (BgL_objectz00_bglt) (BgL_objz00_38);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Jas/classfile.sch 232 */
								long BgL_idxz00_3355;

								BgL_idxz00_3355 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3354);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3355 + 2L)) == BgL_classz00_3353);
							}
						else
							{	/* Jas/classfile.sch 232 */
								bool_t BgL_res2182z00_3358;

								{	/* Jas/classfile.sch 232 */
									obj_t BgL_oclassz00_3359;

									{	/* Jas/classfile.sch 232 */
										obj_t BgL_arg1815z00_3360;
										long BgL_arg1816z00_3361;

										BgL_arg1815z00_3360 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Jas/classfile.sch 232 */
											long BgL_arg1817z00_3362;

											BgL_arg1817z00_3362 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3354);
											BgL_arg1816z00_3361 = (BgL_arg1817z00_3362 - OBJECT_TYPE);
										}
										BgL_oclassz00_3359 =
											VECTOR_REF(BgL_arg1815z00_3360, BgL_arg1816z00_3361);
									}
									{	/* Jas/classfile.sch 232 */
										bool_t BgL__ortest_1115z00_3363;

										BgL__ortest_1115z00_3363 =
											(BgL_classz00_3353 == BgL_oclassz00_3359);
										if (BgL__ortest_1115z00_3363)
											{	/* Jas/classfile.sch 232 */
												BgL_res2182z00_3358 = BgL__ortest_1115z00_3363;
											}
										else
											{	/* Jas/classfile.sch 232 */
												long BgL_odepthz00_3364;

												{	/* Jas/classfile.sch 232 */
													obj_t BgL_arg1804z00_3365;

													BgL_arg1804z00_3365 = (BgL_oclassz00_3359);
													BgL_odepthz00_3364 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3365);
												}
												if ((2L < BgL_odepthz00_3364))
													{	/* Jas/classfile.sch 232 */
														obj_t BgL_arg1802z00_3366;

														{	/* Jas/classfile.sch 232 */
															obj_t BgL_arg1803z00_3367;

															BgL_arg1803z00_3367 = (BgL_oclassz00_3359);
															BgL_arg1802z00_3366 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3367,
																2L);
														}
														BgL_res2182z00_3358 =
															(BgL_arg1802z00_3366 == BgL_classz00_3353);
													}
												else
													{	/* Jas/classfile.sch 232 */
														BgL_res2182z00_3358 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2182z00_3358;
							}
					}
				else
					{	/* Jas/classfile.sch 232 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &JasFun? */
	obj_t BGl_z62JasFunzf3z91zzjas_classfilez00(obj_t BgL_envz00_2465,
		obj_t BgL_objz00_2466)
	{
		{	/* Jas/classfile.sch 232 */
			return BBOOL(BGl_JasFunzf3zf3zzjas_classfilez00(BgL_objz00_2466));
		}

	}



/* JasFun-nil */
	BGL_EXPORTED_DEF BgL_jasfunz00_bglt
		BGl_JasFunzd2nilzd2zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.sch 233 */
			{	/* Jas/classfile.sch 233 */
				obj_t BgL_classz00_1580;

				BgL_classz00_1580 = BGl_JasFunz00zzjas_classfilez00;
				{	/* Jas/classfile.sch 233 */
					obj_t BgL__ortest_1117z00_1581;

					BgL__ortest_1117z00_1581 = BGL_CLASS_NIL(BgL_classz00_1580);
					if (CBOOL(BgL__ortest_1117z00_1581))
						{	/* Jas/classfile.sch 233 */
							return ((BgL_jasfunz00_bglt) BgL__ortest_1117z00_1581);
						}
					else
						{	/* Jas/classfile.sch 233 */
							return
								((BgL_jasfunz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1580));
						}
				}
			}
		}

	}



/* &JasFun-nil */
	BgL_jasfunz00_bglt BGl_z62JasFunzd2nilzb0zzjas_classfilez00(obj_t
		BgL_envz00_2467)
	{
		{	/* Jas/classfile.sch 233 */
			return BGl_JasFunzd2nilzd2zzjas_classfilez00();
		}

	}



/* JasFun-targs */
	BGL_EXPORTED_DEF obj_t
		BGl_JasFunzd2targszd2zzjas_classfilez00(BgL_jasfunz00_bglt BgL_oz00_39)
	{
		{	/* Jas/classfile.sch 234 */
			return (((BgL_jasfunz00_bglt) COBJECT(BgL_oz00_39))->BgL_targsz00);
		}

	}



/* &JasFun-targs */
	obj_t BGl_z62JasFunzd2targszb0zzjas_classfilez00(obj_t BgL_envz00_2468,
		obj_t BgL_oz00_2469)
	{
		{	/* Jas/classfile.sch 234 */
			return
				BGl_JasFunzd2targszd2zzjas_classfilez00(
				((BgL_jasfunz00_bglt) BgL_oz00_2469));
		}

	}



/* JasFun-targs-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_JasFunzd2targszd2setz12z12zzjas_classfilez00(BgL_jasfunz00_bglt
		BgL_oz00_40, obj_t BgL_vz00_41)
	{
		{	/* Jas/classfile.sch 235 */
			return
				((((BgL_jasfunz00_bglt) COBJECT(BgL_oz00_40))->BgL_targsz00) =
				((obj_t) BgL_vz00_41), BUNSPEC);
		}

	}



/* &JasFun-targs-set! */
	obj_t BGl_z62JasFunzd2targszd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2470, obj_t BgL_oz00_2471, obj_t BgL_vz00_2472)
	{
		{	/* Jas/classfile.sch 235 */
			return
				BGl_JasFunzd2targszd2setz12z12zzjas_classfilez00(
				((BgL_jasfunz00_bglt) BgL_oz00_2471), BgL_vz00_2472);
		}

	}



/* JasFun-tret */
	BGL_EXPORTED_DEF BgL_jastypez00_bglt
		BGl_JasFunzd2tretzd2zzjas_classfilez00(BgL_jasfunz00_bglt BgL_oz00_42)
	{
		{	/* Jas/classfile.sch 236 */
			return (((BgL_jasfunz00_bglt) COBJECT(BgL_oz00_42))->BgL_tretz00);
		}

	}



/* &JasFun-tret */
	BgL_jastypez00_bglt BGl_z62JasFunzd2tretzb0zzjas_classfilez00(obj_t
		BgL_envz00_2473, obj_t BgL_oz00_2474)
	{
		{	/* Jas/classfile.sch 236 */
			return
				BGl_JasFunzd2tretzd2zzjas_classfilez00(
				((BgL_jasfunz00_bglt) BgL_oz00_2474));
		}

	}



/* JasFun-tret-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_JasFunzd2tretzd2setz12z12zzjas_classfilez00(BgL_jasfunz00_bglt
		BgL_oz00_43, BgL_jastypez00_bglt BgL_vz00_44)
	{
		{	/* Jas/classfile.sch 237 */
			return
				((((BgL_jasfunz00_bglt) COBJECT(BgL_oz00_43))->BgL_tretz00) =
				((BgL_jastypez00_bglt) BgL_vz00_44), BUNSPEC);
		}

	}



/* &JasFun-tret-set! */
	obj_t BGl_z62JasFunzd2tretzd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2475, obj_t BgL_oz00_2476, obj_t BgL_vz00_2477)
	{
		{	/* Jas/classfile.sch 237 */
			return
				BGl_JasFunzd2tretzd2setz12z12zzjas_classfilez00(
				((BgL_jasfunz00_bglt) BgL_oz00_2476),
				((BgL_jastypez00_bglt) BgL_vz00_2477));
		}

	}



/* JasFun-vect */
	BGL_EXPORTED_DEF obj_t
		BGl_JasFunzd2vectzd2zzjas_classfilez00(BgL_jasfunz00_bglt BgL_oz00_45)
	{
		{	/* Jas/classfile.sch 238 */
			return
				(((BgL_jastypez00_bglt) COBJECT(
						((BgL_jastypez00_bglt) BgL_oz00_45)))->BgL_vectz00);
		}

	}



/* &JasFun-vect */
	obj_t BGl_z62JasFunzd2vectzb0zzjas_classfilez00(obj_t BgL_envz00_2478,
		obj_t BgL_oz00_2479)
	{
		{	/* Jas/classfile.sch 238 */
			return
				BGl_JasFunzd2vectzd2zzjas_classfilez00(
				((BgL_jasfunz00_bglt) BgL_oz00_2479));
		}

	}



/* JasFun-vect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_JasFunzd2vectzd2setz12z12zzjas_classfilez00(BgL_jasfunz00_bglt
		BgL_oz00_46, obj_t BgL_vz00_47)
	{
		{	/* Jas/classfile.sch 239 */
			return
				((((BgL_jastypez00_bglt) COBJECT(
							((BgL_jastypez00_bglt) BgL_oz00_46)))->BgL_vectz00) =
				((obj_t) BgL_vz00_47), BUNSPEC);
		}

	}



/* &JasFun-vect-set! */
	obj_t BGl_z62JasFunzd2vectzd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2480, obj_t BgL_oz00_2481, obj_t BgL_vz00_2482)
	{
		{	/* Jas/classfile.sch 239 */
			return
				BGl_JasFunzd2vectzd2setz12z12zzjas_classfilez00(
				((BgL_jasfunz00_bglt) BgL_oz00_2481), BgL_vz00_2482);
		}

	}



/* JasFun-code */
	BGL_EXPORTED_DEF obj_t
		BGl_JasFunzd2codezd2zzjas_classfilez00(BgL_jasfunz00_bglt BgL_oz00_48)
	{
		{	/* Jas/classfile.sch 240 */
			return
				(((BgL_jastypez00_bglt) COBJECT(
						((BgL_jastypez00_bglt) BgL_oz00_48)))->BgL_codez00);
		}

	}



/* &JasFun-code */
	obj_t BGl_z62JasFunzd2codezb0zzjas_classfilez00(obj_t BgL_envz00_2483,
		obj_t BgL_oz00_2484)
	{
		{	/* Jas/classfile.sch 240 */
			return
				BGl_JasFunzd2codezd2zzjas_classfilez00(
				((BgL_jasfunz00_bglt) BgL_oz00_2484));
		}

	}



/* JasFun-code-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_JasFunzd2codezd2setz12z12zzjas_classfilez00(BgL_jasfunz00_bglt
		BgL_oz00_49, obj_t BgL_vz00_50)
	{
		{	/* Jas/classfile.sch 241 */
			return
				((((BgL_jastypez00_bglt) COBJECT(
							((BgL_jastypez00_bglt) BgL_oz00_49)))->BgL_codez00) =
				((obj_t) BgL_vz00_50), BUNSPEC);
		}

	}



/* &JasFun-code-set! */
	obj_t BGl_z62JasFunzd2codezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2485, obj_t BgL_oz00_2486, obj_t BgL_vz00_2487)
	{
		{	/* Jas/classfile.sch 241 */
			return
				BGl_JasFunzd2codezd2setz12z12zzjas_classfilez00(
				((BgL_jasfunz00_bglt) BgL_oz00_2486), BgL_vz00_2487);
		}

	}



/* make-classe */
	BGL_EXPORTED_DEF BgL_classez00_bglt
		BGl_makezd2classezd2zzjas_classfilez00(obj_t BgL_code1184z00_51,
		obj_t BgL_vect1185z00_52, obj_t BgL_flags1186z00_53,
		obj_t BgL_name1187z00_54, obj_t BgL_pool1188z00_55)
	{
		{	/* Jas/classfile.sch 244 */
			{	/* Jas/classfile.sch 244 */
				BgL_classez00_bglt BgL_new1169z00_3368;

				{	/* Jas/classfile.sch 244 */
					BgL_classez00_bglt BgL_new1168z00_3369;

					BgL_new1168z00_3369 =
						((BgL_classez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_classez00_bgl))));
					{	/* Jas/classfile.sch 244 */
						long BgL_arg1595z00_3370;

						BgL_arg1595z00_3370 =
							BGL_CLASS_NUM(BGl_classez00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1168z00_3369), BgL_arg1595z00_3370);
					}
					BgL_new1169z00_3368 = BgL_new1168z00_3369;
				}
				((((BgL_jastypez00_bglt) COBJECT(
								((BgL_jastypez00_bglt) BgL_new1169z00_3368)))->BgL_codez00) =
					((obj_t) BgL_code1184z00_51), BUNSPEC);
				((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
									BgL_new1169z00_3368)))->BgL_vectz00) =
					((obj_t) BgL_vect1185z00_52), BUNSPEC);
				((((BgL_classez00_bglt) COBJECT(BgL_new1169z00_3368))->BgL_flagsz00) =
					((obj_t) BgL_flags1186z00_53), BUNSPEC);
				((((BgL_classez00_bglt) COBJECT(BgL_new1169z00_3368))->BgL_namez00) =
					((obj_t) BgL_name1187z00_54), BUNSPEC);
				((((BgL_classez00_bglt) COBJECT(BgL_new1169z00_3368))->BgL_poolz00) =
					((obj_t) BgL_pool1188z00_55), BUNSPEC);
				return BgL_new1169z00_3368;
			}
		}

	}



/* &make-classe */
	BgL_classez00_bglt BGl_z62makezd2classezb0zzjas_classfilez00(obj_t
		BgL_envz00_2488, obj_t BgL_code1184z00_2489, obj_t BgL_vect1185z00_2490,
		obj_t BgL_flags1186z00_2491, obj_t BgL_name1187z00_2492,
		obj_t BgL_pool1188z00_2493)
	{
		{	/* Jas/classfile.sch 244 */
			return
				BGl_makezd2classezd2zzjas_classfilez00(BgL_code1184z00_2489,
				BgL_vect1185z00_2490, BgL_flags1186z00_2491, BgL_name1187z00_2492,
				BgL_pool1188z00_2493);
		}

	}



/* classe? */
	BGL_EXPORTED_DEF bool_t BGl_classezf3zf3zzjas_classfilez00(obj_t
		BgL_objz00_56)
	{
		{	/* Jas/classfile.sch 245 */
			{	/* Jas/classfile.sch 245 */
				obj_t BgL_classz00_3371;

				BgL_classz00_3371 = BGl_classez00zzjas_classfilez00;
				if (BGL_OBJECTP(BgL_objz00_56))
					{	/* Jas/classfile.sch 245 */
						BgL_objectz00_bglt BgL_arg1807z00_3372;

						BgL_arg1807z00_3372 = (BgL_objectz00_bglt) (BgL_objz00_56);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Jas/classfile.sch 245 */
								long BgL_idxz00_3373;

								BgL_idxz00_3373 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3372);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3373 + 2L)) == BgL_classz00_3371);
							}
						else
							{	/* Jas/classfile.sch 245 */
								bool_t BgL_res2183z00_3376;

								{	/* Jas/classfile.sch 245 */
									obj_t BgL_oclassz00_3377;

									{	/* Jas/classfile.sch 245 */
										obj_t BgL_arg1815z00_3378;
										long BgL_arg1816z00_3379;

										BgL_arg1815z00_3378 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Jas/classfile.sch 245 */
											long BgL_arg1817z00_3380;

											BgL_arg1817z00_3380 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3372);
											BgL_arg1816z00_3379 = (BgL_arg1817z00_3380 - OBJECT_TYPE);
										}
										BgL_oclassz00_3377 =
											VECTOR_REF(BgL_arg1815z00_3378, BgL_arg1816z00_3379);
									}
									{	/* Jas/classfile.sch 245 */
										bool_t BgL__ortest_1115z00_3381;

										BgL__ortest_1115z00_3381 =
											(BgL_classz00_3371 == BgL_oclassz00_3377);
										if (BgL__ortest_1115z00_3381)
											{	/* Jas/classfile.sch 245 */
												BgL_res2183z00_3376 = BgL__ortest_1115z00_3381;
											}
										else
											{	/* Jas/classfile.sch 245 */
												long BgL_odepthz00_3382;

												{	/* Jas/classfile.sch 245 */
													obj_t BgL_arg1804z00_3383;

													BgL_arg1804z00_3383 = (BgL_oclassz00_3377);
													BgL_odepthz00_3382 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3383);
												}
												if ((2L < BgL_odepthz00_3382))
													{	/* Jas/classfile.sch 245 */
														obj_t BgL_arg1802z00_3384;

														{	/* Jas/classfile.sch 245 */
															obj_t BgL_arg1803z00_3385;

															BgL_arg1803z00_3385 = (BgL_oclassz00_3377);
															BgL_arg1802z00_3384 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3385,
																2L);
														}
														BgL_res2183z00_3376 =
															(BgL_arg1802z00_3384 == BgL_classz00_3371);
													}
												else
													{	/* Jas/classfile.sch 245 */
														BgL_res2183z00_3376 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2183z00_3376;
							}
					}
				else
					{	/* Jas/classfile.sch 245 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &classe? */
	obj_t BGl_z62classezf3z91zzjas_classfilez00(obj_t BgL_envz00_2494,
		obj_t BgL_objz00_2495)
	{
		{	/* Jas/classfile.sch 245 */
			return BBOOL(BGl_classezf3zf3zzjas_classfilez00(BgL_objz00_2495));
		}

	}



/* classe-nil */
	BGL_EXPORTED_DEF BgL_classez00_bglt
		BGl_classezd2nilzd2zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.sch 246 */
			{	/* Jas/classfile.sch 246 */
				obj_t BgL_classz00_1630;

				BgL_classz00_1630 = BGl_classez00zzjas_classfilez00;
				{	/* Jas/classfile.sch 246 */
					obj_t BgL__ortest_1117z00_1631;

					BgL__ortest_1117z00_1631 = BGL_CLASS_NIL(BgL_classz00_1630);
					if (CBOOL(BgL__ortest_1117z00_1631))
						{	/* Jas/classfile.sch 246 */
							return ((BgL_classez00_bglt) BgL__ortest_1117z00_1631);
						}
					else
						{	/* Jas/classfile.sch 246 */
							return
								((BgL_classez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1630));
						}
				}
			}
		}

	}



/* &classe-nil */
	BgL_classez00_bglt BGl_z62classezd2nilzb0zzjas_classfilez00(obj_t
		BgL_envz00_2496)
	{
		{	/* Jas/classfile.sch 246 */
			return BGl_classezd2nilzd2zzjas_classfilez00();
		}

	}



/* classe-pool */
	BGL_EXPORTED_DEF obj_t
		BGl_classezd2poolzd2zzjas_classfilez00(BgL_classez00_bglt BgL_oz00_57)
	{
		{	/* Jas/classfile.sch 247 */
			return (((BgL_classez00_bglt) COBJECT(BgL_oz00_57))->BgL_poolz00);
		}

	}



/* &classe-pool */
	obj_t BGl_z62classezd2poolzb0zzjas_classfilez00(obj_t BgL_envz00_2497,
		obj_t BgL_oz00_2498)
	{
		{	/* Jas/classfile.sch 247 */
			return
				BGl_classezd2poolzd2zzjas_classfilez00(
				((BgL_classez00_bglt) BgL_oz00_2498));
		}

	}



/* classe-pool-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classezd2poolzd2setz12z12zzjas_classfilez00(BgL_classez00_bglt
		BgL_oz00_58, obj_t BgL_vz00_59)
	{
		{	/* Jas/classfile.sch 248 */
			return
				((((BgL_classez00_bglt) COBJECT(BgL_oz00_58))->BgL_poolz00) =
				((obj_t) BgL_vz00_59), BUNSPEC);
		}

	}



/* &classe-pool-set! */
	obj_t BGl_z62classezd2poolzd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2499, obj_t BgL_oz00_2500, obj_t BgL_vz00_2501)
	{
		{	/* Jas/classfile.sch 248 */
			return
				BGl_classezd2poolzd2setz12z12zzjas_classfilez00(
				((BgL_classez00_bglt) BgL_oz00_2500), BgL_vz00_2501);
		}

	}



/* classe-name */
	BGL_EXPORTED_DEF obj_t
		BGl_classezd2namezd2zzjas_classfilez00(BgL_classez00_bglt BgL_oz00_60)
	{
		{	/* Jas/classfile.sch 249 */
			return (((BgL_classez00_bglt) COBJECT(BgL_oz00_60))->BgL_namez00);
		}

	}



/* &classe-name */
	obj_t BGl_z62classezd2namezb0zzjas_classfilez00(obj_t BgL_envz00_2502,
		obj_t BgL_oz00_2503)
	{
		{	/* Jas/classfile.sch 249 */
			return
				BGl_classezd2namezd2zzjas_classfilez00(
				((BgL_classez00_bglt) BgL_oz00_2503));
		}

	}



/* classe-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classezd2namezd2setz12z12zzjas_classfilez00(BgL_classez00_bglt
		BgL_oz00_61, obj_t BgL_vz00_62)
	{
		{	/* Jas/classfile.sch 250 */
			return
				((((BgL_classez00_bglt) COBJECT(BgL_oz00_61))->BgL_namez00) =
				((obj_t) BgL_vz00_62), BUNSPEC);
		}

	}



/* &classe-name-set! */
	obj_t BGl_z62classezd2namezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2504, obj_t BgL_oz00_2505, obj_t BgL_vz00_2506)
	{
		{	/* Jas/classfile.sch 250 */
			return
				BGl_classezd2namezd2setz12z12zzjas_classfilez00(
				((BgL_classez00_bglt) BgL_oz00_2505), BgL_vz00_2506);
		}

	}



/* classe-flags */
	BGL_EXPORTED_DEF obj_t
		BGl_classezd2flagszd2zzjas_classfilez00(BgL_classez00_bglt BgL_oz00_63)
	{
		{	/* Jas/classfile.sch 251 */
			return (((BgL_classez00_bglt) COBJECT(BgL_oz00_63))->BgL_flagsz00);
		}

	}



/* &classe-flags */
	obj_t BGl_z62classezd2flagszb0zzjas_classfilez00(obj_t BgL_envz00_2507,
		obj_t BgL_oz00_2508)
	{
		{	/* Jas/classfile.sch 251 */
			return
				BGl_classezd2flagszd2zzjas_classfilez00(
				((BgL_classez00_bglt) BgL_oz00_2508));
		}

	}



/* classe-flags-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classezd2flagszd2setz12z12zzjas_classfilez00(BgL_classez00_bglt
		BgL_oz00_64, obj_t BgL_vz00_65)
	{
		{	/* Jas/classfile.sch 252 */
			return
				((((BgL_classez00_bglt) COBJECT(BgL_oz00_64))->BgL_flagsz00) =
				((obj_t) BgL_vz00_65), BUNSPEC);
		}

	}



/* &classe-flags-set! */
	obj_t BGl_z62classezd2flagszd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2509, obj_t BgL_oz00_2510, obj_t BgL_vz00_2511)
	{
		{	/* Jas/classfile.sch 252 */
			return
				BGl_classezd2flagszd2setz12z12zzjas_classfilez00(
				((BgL_classez00_bglt) BgL_oz00_2510), BgL_vz00_2511);
		}

	}



/* classe-vect */
	BGL_EXPORTED_DEF obj_t
		BGl_classezd2vectzd2zzjas_classfilez00(BgL_classez00_bglt BgL_oz00_66)
	{
		{	/* Jas/classfile.sch 253 */
			return
				(((BgL_jastypez00_bglt) COBJECT(
						((BgL_jastypez00_bglt) BgL_oz00_66)))->BgL_vectz00);
		}

	}



/* &classe-vect */
	obj_t BGl_z62classezd2vectzb0zzjas_classfilez00(obj_t BgL_envz00_2512,
		obj_t BgL_oz00_2513)
	{
		{	/* Jas/classfile.sch 253 */
			return
				BGl_classezd2vectzd2zzjas_classfilez00(
				((BgL_classez00_bglt) BgL_oz00_2513));
		}

	}



/* classe-vect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classezd2vectzd2setz12z12zzjas_classfilez00(BgL_classez00_bglt
		BgL_oz00_67, obj_t BgL_vz00_68)
	{
		{	/* Jas/classfile.sch 254 */
			return
				((((BgL_jastypez00_bglt) COBJECT(
							((BgL_jastypez00_bglt) BgL_oz00_67)))->BgL_vectz00) =
				((obj_t) BgL_vz00_68), BUNSPEC);
		}

	}



/* &classe-vect-set! */
	obj_t BGl_z62classezd2vectzd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2514, obj_t BgL_oz00_2515, obj_t BgL_vz00_2516)
	{
		{	/* Jas/classfile.sch 254 */
			return
				BGl_classezd2vectzd2setz12z12zzjas_classfilez00(
				((BgL_classez00_bglt) BgL_oz00_2515), BgL_vz00_2516);
		}

	}



/* classe-code */
	BGL_EXPORTED_DEF obj_t
		BGl_classezd2codezd2zzjas_classfilez00(BgL_classez00_bglt BgL_oz00_69)
	{
		{	/* Jas/classfile.sch 255 */
			return
				(((BgL_jastypez00_bglt) COBJECT(
						((BgL_jastypez00_bglt) BgL_oz00_69)))->BgL_codez00);
		}

	}



/* &classe-code */
	obj_t BGl_z62classezd2codezb0zzjas_classfilez00(obj_t BgL_envz00_2517,
		obj_t BgL_oz00_2518)
	{
		{	/* Jas/classfile.sch 255 */
			return
				BGl_classezd2codezd2zzjas_classfilez00(
				((BgL_classez00_bglt) BgL_oz00_2518));
		}

	}



/* classe-code-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classezd2codezd2setz12z12zzjas_classfilez00(BgL_classez00_bglt
		BgL_oz00_70, obj_t BgL_vz00_71)
	{
		{	/* Jas/classfile.sch 256 */
			return
				((((BgL_jastypez00_bglt) COBJECT(
							((BgL_jastypez00_bglt) BgL_oz00_70)))->BgL_codez00) =
				((obj_t) BgL_vz00_71), BUNSPEC);
		}

	}



/* &classe-code-set! */
	obj_t BGl_z62classezd2codezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2519, obj_t BgL_oz00_2520, obj_t BgL_vz00_2521)
	{
		{	/* Jas/classfile.sch 256 */
			return
				BGl_classezd2codezd2setz12z12zzjas_classfilez00(
				((BgL_classez00_bglt) BgL_oz00_2520), BgL_vz00_2521);
		}

	}



/* make-field-or-method */
	BGL_EXPORTED_DEF BgL_fieldzd2orzd2methodz00_bglt
		BGl_makezd2fieldzd2orzd2methodzd2zzjas_classfilez00(obj_t
		BgL_flags1174z00_72, obj_t BgL_name1175z00_73, obj_t BgL_owner1176z00_74,
		obj_t BgL_usertype1177z00_75, obj_t BgL_type1178z00_76,
		obj_t BgL_pname1179z00_77, obj_t BgL_descriptor1180z00_78,
		obj_t BgL_pool1181z00_79, obj_t BgL_attributes1182z00_80)
	{
		{	/* Jas/classfile.sch 259 */
			{	/* Jas/classfile.sch 259 */
				BgL_fieldzd2orzd2methodz00_bglt BgL_new1182z00_3386;

				{	/* Jas/classfile.sch 259 */
					BgL_fieldzd2orzd2methodz00_bglt BgL_new1180z00_3387;

					BgL_new1180z00_3387 =
						((BgL_fieldzd2orzd2methodz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_fieldzd2orzd2methodz00_bgl))));
					{	/* Jas/classfile.sch 259 */
						long BgL_arg1602z00_3388;

						BgL_arg1602z00_3388 =
							BGL_CLASS_NUM(BGl_fieldzd2orzd2methodz00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1180z00_3387), BgL_arg1602z00_3388);
					}
					BgL_new1182z00_3386 = BgL_new1180z00_3387;
				}
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1182z00_3386))->
						BgL_flagsz00) = ((obj_t) BgL_flags1174z00_72), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1182z00_3386))->
						BgL_namez00) = ((obj_t) BgL_name1175z00_73), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1182z00_3386))->
						BgL_ownerz00) = ((obj_t) BgL_owner1176z00_74), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1182z00_3386))->
						BgL_usertypez00) = ((obj_t) BgL_usertype1177z00_75), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1182z00_3386))->
						BgL_typez00) = ((obj_t) BgL_type1178z00_76), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1182z00_3386))->
						BgL_pnamez00) = ((obj_t) BgL_pname1179z00_77), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1182z00_3386))->
						BgL_descriptorz00) = ((obj_t) BgL_descriptor1180z00_78), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1182z00_3386))->
						BgL_poolz00) = ((obj_t) BgL_pool1181z00_79), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1182z00_3386))->
						BgL_attributesz00) = ((obj_t) BgL_attributes1182z00_80), BUNSPEC);
				return BgL_new1182z00_3386;
			}
		}

	}



/* &make-field-or-method */
	BgL_fieldzd2orzd2methodz00_bglt
		BGl_z62makezd2fieldzd2orzd2methodzb0zzjas_classfilez00(obj_t
		BgL_envz00_2522, obj_t BgL_flags1174z00_2523, obj_t BgL_name1175z00_2524,
		obj_t BgL_owner1176z00_2525, obj_t BgL_usertype1177z00_2526,
		obj_t BgL_type1178z00_2527, obj_t BgL_pname1179z00_2528,
		obj_t BgL_descriptor1180z00_2529, obj_t BgL_pool1181z00_2530,
		obj_t BgL_attributes1182z00_2531)
	{
		{	/* Jas/classfile.sch 259 */
			return
				BGl_makezd2fieldzd2orzd2methodzd2zzjas_classfilez00
				(BgL_flags1174z00_2523, BgL_name1175z00_2524, BgL_owner1176z00_2525,
				BgL_usertype1177z00_2526, BgL_type1178z00_2527, BgL_pname1179z00_2528,
				BgL_descriptor1180z00_2529, BgL_pool1181z00_2530,
				BgL_attributes1182z00_2531);
		}

	}



/* field-or-method? */
	BGL_EXPORTED_DEF bool_t BGl_fieldzd2orzd2methodzf3zf3zzjas_classfilez00(obj_t
		BgL_objz00_81)
	{
		{	/* Jas/classfile.sch 260 */
			{	/* Jas/classfile.sch 260 */
				obj_t BgL_classz00_3389;

				BgL_classz00_3389 = BGl_fieldzd2orzd2methodz00zzjas_classfilez00;
				if (BGL_OBJECTP(BgL_objz00_81))
					{	/* Jas/classfile.sch 260 */
						BgL_objectz00_bglt BgL_arg1807z00_3390;

						BgL_arg1807z00_3390 = (BgL_objectz00_bglt) (BgL_objz00_81);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Jas/classfile.sch 260 */
								long BgL_idxz00_3391;

								BgL_idxz00_3391 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3390);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3391 + 1L)) == BgL_classz00_3389);
							}
						else
							{	/* Jas/classfile.sch 260 */
								bool_t BgL_res2184z00_3394;

								{	/* Jas/classfile.sch 260 */
									obj_t BgL_oclassz00_3395;

									{	/* Jas/classfile.sch 260 */
										obj_t BgL_arg1815z00_3396;
										long BgL_arg1816z00_3397;

										BgL_arg1815z00_3396 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Jas/classfile.sch 260 */
											long BgL_arg1817z00_3398;

											BgL_arg1817z00_3398 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3390);
											BgL_arg1816z00_3397 = (BgL_arg1817z00_3398 - OBJECT_TYPE);
										}
										BgL_oclassz00_3395 =
											VECTOR_REF(BgL_arg1815z00_3396, BgL_arg1816z00_3397);
									}
									{	/* Jas/classfile.sch 260 */
										bool_t BgL__ortest_1115z00_3399;

										BgL__ortest_1115z00_3399 =
											(BgL_classz00_3389 == BgL_oclassz00_3395);
										if (BgL__ortest_1115z00_3399)
											{	/* Jas/classfile.sch 260 */
												BgL_res2184z00_3394 = BgL__ortest_1115z00_3399;
											}
										else
											{	/* Jas/classfile.sch 260 */
												long BgL_odepthz00_3400;

												{	/* Jas/classfile.sch 260 */
													obj_t BgL_arg1804z00_3401;

													BgL_arg1804z00_3401 = (BgL_oclassz00_3395);
													BgL_odepthz00_3400 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3401);
												}
												if ((1L < BgL_odepthz00_3400))
													{	/* Jas/classfile.sch 260 */
														obj_t BgL_arg1802z00_3402;

														{	/* Jas/classfile.sch 260 */
															obj_t BgL_arg1803z00_3403;

															BgL_arg1803z00_3403 = (BgL_oclassz00_3395);
															BgL_arg1802z00_3402 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3403,
																1L);
														}
														BgL_res2184z00_3394 =
															(BgL_arg1802z00_3402 == BgL_classz00_3389);
													}
												else
													{	/* Jas/classfile.sch 260 */
														BgL_res2184z00_3394 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2184z00_3394;
							}
					}
				else
					{	/* Jas/classfile.sch 260 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &field-or-method? */
	obj_t BGl_z62fieldzd2orzd2methodzf3z91zzjas_classfilez00(obj_t
		BgL_envz00_2532, obj_t BgL_objz00_2533)
	{
		{	/* Jas/classfile.sch 260 */
			return
				BBOOL(BGl_fieldzd2orzd2methodzf3zf3zzjas_classfilez00(BgL_objz00_2533));
		}

	}



/* field-or-method-nil */
	BGL_EXPORTED_DEF BgL_fieldzd2orzd2methodz00_bglt
		BGl_fieldzd2orzd2methodzd2nilzd2zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.sch 261 */
			{	/* Jas/classfile.sch 261 */
				obj_t BgL_classz00_1682;

				BgL_classz00_1682 = BGl_fieldzd2orzd2methodz00zzjas_classfilez00;
				{	/* Jas/classfile.sch 261 */
					obj_t BgL__ortest_1117z00_1683;

					BgL__ortest_1117z00_1683 = BGL_CLASS_NIL(BgL_classz00_1682);
					if (CBOOL(BgL__ortest_1117z00_1683))
						{	/* Jas/classfile.sch 261 */
							return
								((BgL_fieldzd2orzd2methodz00_bglt) BgL__ortest_1117z00_1683);
						}
					else
						{	/* Jas/classfile.sch 261 */
							return
								((BgL_fieldzd2orzd2methodz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1682));
						}
				}
			}
		}

	}



/* &field-or-method-nil */
	BgL_fieldzd2orzd2methodz00_bglt
		BGl_z62fieldzd2orzd2methodzd2nilzb0zzjas_classfilez00(obj_t BgL_envz00_2534)
	{
		{	/* Jas/classfile.sch 261 */
			return BGl_fieldzd2orzd2methodzd2nilzd2zzjas_classfilez00();
		}

	}



/* field-or-method-attributes */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2attributeszd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_82)
	{
		{	/* Jas/classfile.sch 262 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_82))->
				BgL_attributesz00);
		}

	}



/* &field-or-method-attributes */
	obj_t BGl_z62fieldzd2orzd2methodzd2attributeszb0zzjas_classfilez00(obj_t
		BgL_envz00_2535, obj_t BgL_oz00_2536)
	{
		{	/* Jas/classfile.sch 262 */
			return
				BGl_fieldzd2orzd2methodzd2attributeszd2zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2536));
		}

	}



/* field-or-method-attributes-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2attributeszd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_83, obj_t BgL_vz00_84)
	{
		{	/* Jas/classfile.sch 263 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_83))->
					BgL_attributesz00) = ((obj_t) BgL_vz00_84), BUNSPEC);
		}

	}



/* &field-or-method-attributes-set! */
	obj_t
		BGl_z62fieldzd2orzd2methodzd2attributeszd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2537, obj_t BgL_oz00_2538, obj_t BgL_vz00_2539)
	{
		{	/* Jas/classfile.sch 263 */
			return
				BGl_fieldzd2orzd2methodzd2attributeszd2setz12z12zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2538), BgL_vz00_2539);
		}

	}



/* field-or-method-pool */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2poolzd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_85)
	{
		{	/* Jas/classfile.sch 264 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_85))->BgL_poolz00);
		}

	}



/* &field-or-method-pool */
	obj_t BGl_z62fieldzd2orzd2methodzd2poolzb0zzjas_classfilez00(obj_t
		BgL_envz00_2540, obj_t BgL_oz00_2541)
	{
		{	/* Jas/classfile.sch 264 */
			return
				BGl_fieldzd2orzd2methodzd2poolzd2zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2541));
		}

	}



/* field-or-method-pool-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2poolzd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_86, obj_t BgL_vz00_87)
	{
		{	/* Jas/classfile.sch 265 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_86))->
					BgL_poolz00) = ((obj_t) BgL_vz00_87), BUNSPEC);
		}

	}



/* &field-or-method-pool-set! */
	obj_t BGl_z62fieldzd2orzd2methodzd2poolzd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2542, obj_t BgL_oz00_2543, obj_t BgL_vz00_2544)
	{
		{	/* Jas/classfile.sch 265 */
			return
				BGl_fieldzd2orzd2methodzd2poolzd2setz12z12zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2543), BgL_vz00_2544);
		}

	}



/* field-or-method-descriptor */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2descriptorzd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_88)
	{
		{	/* Jas/classfile.sch 266 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_88))->
				BgL_descriptorz00);
		}

	}



/* &field-or-method-descriptor */
	obj_t BGl_z62fieldzd2orzd2methodzd2descriptorzb0zzjas_classfilez00(obj_t
		BgL_envz00_2545, obj_t BgL_oz00_2546)
	{
		{	/* Jas/classfile.sch 266 */
			return
				BGl_fieldzd2orzd2methodzd2descriptorzd2zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2546));
		}

	}



/* field-or-method-descriptor-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2descriptorzd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_89, obj_t BgL_vz00_90)
	{
		{	/* Jas/classfile.sch 267 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_89))->
					BgL_descriptorz00) = ((obj_t) BgL_vz00_90), BUNSPEC);
		}

	}



/* &field-or-method-descriptor-set! */
	obj_t
		BGl_z62fieldzd2orzd2methodzd2descriptorzd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2547, obj_t BgL_oz00_2548, obj_t BgL_vz00_2549)
	{
		{	/* Jas/classfile.sch 267 */
			return
				BGl_fieldzd2orzd2methodzd2descriptorzd2setz12z12zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2548), BgL_vz00_2549);
		}

	}



/* field-or-method-pname */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2pnamezd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_91)
	{
		{	/* Jas/classfile.sch 268 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_91))->
				BgL_pnamez00);
		}

	}



/* &field-or-method-pname */
	obj_t BGl_z62fieldzd2orzd2methodzd2pnamezb0zzjas_classfilez00(obj_t
		BgL_envz00_2550, obj_t BgL_oz00_2551)
	{
		{	/* Jas/classfile.sch 268 */
			return
				BGl_fieldzd2orzd2methodzd2pnamezd2zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2551));
		}

	}



/* field-or-method-pname-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2pnamezd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_92, obj_t BgL_vz00_93)
	{
		{	/* Jas/classfile.sch 269 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_92))->
					BgL_pnamez00) = ((obj_t) BgL_vz00_93), BUNSPEC);
		}

	}



/* &field-or-method-pname-set! */
	obj_t BGl_z62fieldzd2orzd2methodzd2pnamezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2552, obj_t BgL_oz00_2553, obj_t BgL_vz00_2554)
	{
		{	/* Jas/classfile.sch 269 */
			return
				BGl_fieldzd2orzd2methodzd2pnamezd2setz12z12zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2553), BgL_vz00_2554);
		}

	}



/* field-or-method-type */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2typezd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_94)
	{
		{	/* Jas/classfile.sch 270 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_94))->BgL_typez00);
		}

	}



/* &field-or-method-type */
	obj_t BGl_z62fieldzd2orzd2methodzd2typezb0zzjas_classfilez00(obj_t
		BgL_envz00_2555, obj_t BgL_oz00_2556)
	{
		{	/* Jas/classfile.sch 270 */
			return
				BGl_fieldzd2orzd2methodzd2typezd2zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2556));
		}

	}



/* field-or-method-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2typezd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_95, obj_t BgL_vz00_96)
	{
		{	/* Jas/classfile.sch 271 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_95))->
					BgL_typez00) = ((obj_t) BgL_vz00_96), BUNSPEC);
		}

	}



/* &field-or-method-type-set! */
	obj_t BGl_z62fieldzd2orzd2methodzd2typezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2557, obj_t BgL_oz00_2558, obj_t BgL_vz00_2559)
	{
		{	/* Jas/classfile.sch 271 */
			return
				BGl_fieldzd2orzd2methodzd2typezd2setz12z12zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2558), BgL_vz00_2559);
		}

	}



/* field-or-method-usertype */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2usertypezd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_97)
	{
		{	/* Jas/classfile.sch 272 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_97))->
				BgL_usertypez00);
		}

	}



/* &field-or-method-usertype */
	obj_t BGl_z62fieldzd2orzd2methodzd2usertypezb0zzjas_classfilez00(obj_t
		BgL_envz00_2560, obj_t BgL_oz00_2561)
	{
		{	/* Jas/classfile.sch 272 */
			return
				BGl_fieldzd2orzd2methodzd2usertypezd2zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2561));
		}

	}



/* field-or-method-usertype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2usertypezd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_98, obj_t BgL_vz00_99)
	{
		{	/* Jas/classfile.sch 273 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_98))->
					BgL_usertypez00) = ((obj_t) BgL_vz00_99), BUNSPEC);
		}

	}



/* &field-or-method-usertype-set! */
	obj_t
		BGl_z62fieldzd2orzd2methodzd2usertypezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2562, obj_t BgL_oz00_2563, obj_t BgL_vz00_2564)
	{
		{	/* Jas/classfile.sch 273 */
			return
				BGl_fieldzd2orzd2methodzd2usertypezd2setz12z12zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2563), BgL_vz00_2564);
		}

	}



/* field-or-method-owner */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2ownerzd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_100)
	{
		{	/* Jas/classfile.sch 274 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_100))->
				BgL_ownerz00);
		}

	}



/* &field-or-method-owner */
	obj_t BGl_z62fieldzd2orzd2methodzd2ownerzb0zzjas_classfilez00(obj_t
		BgL_envz00_2565, obj_t BgL_oz00_2566)
	{
		{	/* Jas/classfile.sch 274 */
			return
				BGl_fieldzd2orzd2methodzd2ownerzd2zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2566));
		}

	}



/* field-or-method-owner-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2ownerzd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_101, obj_t BgL_vz00_102)
	{
		{	/* Jas/classfile.sch 275 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_101))->
					BgL_ownerz00) = ((obj_t) BgL_vz00_102), BUNSPEC);
		}

	}



/* &field-or-method-owner-set! */
	obj_t BGl_z62fieldzd2orzd2methodzd2ownerzd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2567, obj_t BgL_oz00_2568, obj_t BgL_vz00_2569)
	{
		{	/* Jas/classfile.sch 275 */
			return
				BGl_fieldzd2orzd2methodzd2ownerzd2setz12z12zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2568), BgL_vz00_2569);
		}

	}



/* field-or-method-name */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2namezd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_103)
	{
		{	/* Jas/classfile.sch 276 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_103))->
				BgL_namez00);
		}

	}



/* &field-or-method-name */
	obj_t BGl_z62fieldzd2orzd2methodzd2namezb0zzjas_classfilez00(obj_t
		BgL_envz00_2570, obj_t BgL_oz00_2571)
	{
		{	/* Jas/classfile.sch 276 */
			return
				BGl_fieldzd2orzd2methodzd2namezd2zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2571));
		}

	}



/* field-or-method-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2namezd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_104, obj_t BgL_vz00_105)
	{
		{	/* Jas/classfile.sch 277 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_104))->
					BgL_namez00) = ((obj_t) BgL_vz00_105), BUNSPEC);
		}

	}



/* &field-or-method-name-set! */
	obj_t BGl_z62fieldzd2orzd2methodzd2namezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2572, obj_t BgL_oz00_2573, obj_t BgL_vz00_2574)
	{
		{	/* Jas/classfile.sch 277 */
			return
				BGl_fieldzd2orzd2methodzd2namezd2setz12z12zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2573), BgL_vz00_2574);
		}

	}



/* field-or-method-flags */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2flagszd2zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_106)
	{
		{	/* Jas/classfile.sch 278 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_106))->
				BgL_flagsz00);
		}

	}



/* &field-or-method-flags */
	obj_t BGl_z62fieldzd2orzd2methodzd2flagszb0zzjas_classfilez00(obj_t
		BgL_envz00_2575, obj_t BgL_oz00_2576)
	{
		{	/* Jas/classfile.sch 278 */
			return
				BGl_fieldzd2orzd2methodzd2flagszd2zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2576));
		}

	}



/* field-or-method-flags-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2orzd2methodzd2flagszd2setz12z12zzjas_classfilez00
		(BgL_fieldzd2orzd2methodz00_bglt BgL_oz00_107, obj_t BgL_vz00_108)
	{
		{	/* Jas/classfile.sch 279 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_oz00_107))->
					BgL_flagsz00) = ((obj_t) BgL_vz00_108), BUNSPEC);
		}

	}



/* &field-or-method-flags-set! */
	obj_t BGl_z62fieldzd2orzd2methodzd2flagszd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2577, obj_t BgL_oz00_2578, obj_t BgL_vz00_2579)
	{
		{	/* Jas/classfile.sch 279 */
			return
				BGl_fieldzd2orzd2methodzd2flagszd2setz12z12zzjas_classfilez00(
				((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_2578), BgL_vz00_2579);
		}

	}



/* make-field */
	BGL_EXPORTED_DEF BgL_fieldz00_bglt BGl_makezd2fieldzd2zzjas_classfilez00(obj_t
		BgL_flags1164z00_109, obj_t BgL_name1165z00_110, obj_t BgL_owner1166z00_111,
		obj_t BgL_usertype1167z00_112, obj_t BgL_type1168z00_113,
		obj_t BgL_pname1169z00_114, obj_t BgL_descriptor1170z00_115,
		obj_t BgL_pool1171z00_116, obj_t BgL_attributes1172z00_117)
	{
		{	/* Jas/classfile.sch 282 */
			{	/* Jas/classfile.sch 282 */
				BgL_fieldz00_bglt BgL_new1202z00_3404;

				{	/* Jas/classfile.sch 282 */
					BgL_fieldz00_bglt BgL_new1201z00_3405;

					BgL_new1201z00_3405 =
						((BgL_fieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_fieldz00_bgl))));
					{	/* Jas/classfile.sch 282 */
						long BgL_arg1605z00_3406;

						BgL_arg1605z00_3406 = BGL_CLASS_NUM(BGl_fieldz00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1201z00_3405), BgL_arg1605z00_3406);
					}
					BgL_new1202z00_3404 = BgL_new1201z00_3405;
				}
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
								((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1202z00_3404)))->
						BgL_flagsz00) = ((obj_t) BgL_flags1164z00_109), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1202z00_3404)))->BgL_namez00) =
					((obj_t) BgL_name1165z00_110), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1202z00_3404)))->BgL_ownerz00) =
					((obj_t) BgL_owner1166z00_111), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1202z00_3404)))->BgL_usertypez00) =
					((obj_t) BgL_usertype1167z00_112), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1202z00_3404)))->BgL_typez00) =
					((obj_t) BgL_type1168z00_113), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1202z00_3404)))->BgL_pnamez00) =
					((obj_t) BgL_pname1169z00_114), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1202z00_3404)))->BgL_descriptorz00) =
					((obj_t) BgL_descriptor1170z00_115), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1202z00_3404)))->BgL_poolz00) =
					((obj_t) BgL_pool1171z00_116), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1202z00_3404)))->BgL_attributesz00) =
					((obj_t) BgL_attributes1172z00_117), BUNSPEC);
				return BgL_new1202z00_3404;
			}
		}

	}



/* &make-field */
	BgL_fieldz00_bglt BGl_z62makezd2fieldzb0zzjas_classfilez00(obj_t
		BgL_envz00_2580, obj_t BgL_flags1164z00_2581, obj_t BgL_name1165z00_2582,
		obj_t BgL_owner1166z00_2583, obj_t BgL_usertype1167z00_2584,
		obj_t BgL_type1168z00_2585, obj_t BgL_pname1169z00_2586,
		obj_t BgL_descriptor1170z00_2587, obj_t BgL_pool1171z00_2588,
		obj_t BgL_attributes1172z00_2589)
	{
		{	/* Jas/classfile.sch 282 */
			return
				BGl_makezd2fieldzd2zzjas_classfilez00(BgL_flags1164z00_2581,
				BgL_name1165z00_2582, BgL_owner1166z00_2583, BgL_usertype1167z00_2584,
				BgL_type1168z00_2585, BgL_pname1169z00_2586, BgL_descriptor1170z00_2587,
				BgL_pool1171z00_2588, BgL_attributes1172z00_2589);
		}

	}



/* field? */
	BGL_EXPORTED_DEF bool_t BGl_fieldzf3zf3zzjas_classfilez00(obj_t
		BgL_objz00_118)
	{
		{	/* Jas/classfile.sch 283 */
			{	/* Jas/classfile.sch 283 */
				obj_t BgL_classz00_3407;

				BgL_classz00_3407 = BGl_fieldz00zzjas_classfilez00;
				if (BGL_OBJECTP(BgL_objz00_118))
					{	/* Jas/classfile.sch 283 */
						BgL_objectz00_bglt BgL_arg1807z00_3408;

						BgL_arg1807z00_3408 = (BgL_objectz00_bglt) (BgL_objz00_118);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Jas/classfile.sch 283 */
								long BgL_idxz00_3409;

								BgL_idxz00_3409 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3408);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3409 + 2L)) == BgL_classz00_3407);
							}
						else
							{	/* Jas/classfile.sch 283 */
								bool_t BgL_res2185z00_3412;

								{	/* Jas/classfile.sch 283 */
									obj_t BgL_oclassz00_3413;

									{	/* Jas/classfile.sch 283 */
										obj_t BgL_arg1815z00_3414;
										long BgL_arg1816z00_3415;

										BgL_arg1815z00_3414 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Jas/classfile.sch 283 */
											long BgL_arg1817z00_3416;

											BgL_arg1817z00_3416 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3408);
											BgL_arg1816z00_3415 = (BgL_arg1817z00_3416 - OBJECT_TYPE);
										}
										BgL_oclassz00_3413 =
											VECTOR_REF(BgL_arg1815z00_3414, BgL_arg1816z00_3415);
									}
									{	/* Jas/classfile.sch 283 */
										bool_t BgL__ortest_1115z00_3417;

										BgL__ortest_1115z00_3417 =
											(BgL_classz00_3407 == BgL_oclassz00_3413);
										if (BgL__ortest_1115z00_3417)
											{	/* Jas/classfile.sch 283 */
												BgL_res2185z00_3412 = BgL__ortest_1115z00_3417;
											}
										else
											{	/* Jas/classfile.sch 283 */
												long BgL_odepthz00_3418;

												{	/* Jas/classfile.sch 283 */
													obj_t BgL_arg1804z00_3419;

													BgL_arg1804z00_3419 = (BgL_oclassz00_3413);
													BgL_odepthz00_3418 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3419);
												}
												if ((2L < BgL_odepthz00_3418))
													{	/* Jas/classfile.sch 283 */
														obj_t BgL_arg1802z00_3420;

														{	/* Jas/classfile.sch 283 */
															obj_t BgL_arg1803z00_3421;

															BgL_arg1803z00_3421 = (BgL_oclassz00_3413);
															BgL_arg1802z00_3420 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3421,
																2L);
														}
														BgL_res2185z00_3412 =
															(BgL_arg1802z00_3420 == BgL_classz00_3407);
													}
												else
													{	/* Jas/classfile.sch 283 */
														BgL_res2185z00_3412 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2185z00_3412;
							}
					}
				else
					{	/* Jas/classfile.sch 283 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &field? */
	obj_t BGl_z62fieldzf3z91zzjas_classfilez00(obj_t BgL_envz00_2590,
		obj_t BgL_objz00_2591)
	{
		{	/* Jas/classfile.sch 283 */
			return BBOOL(BGl_fieldzf3zf3zzjas_classfilez00(BgL_objz00_2591));
		}

	}



/* field-nil */
	BGL_EXPORTED_DEF BgL_fieldz00_bglt BGl_fieldzd2nilzd2zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.sch 284 */
			{	/* Jas/classfile.sch 284 */
				obj_t BgL_classz00_1742;

				BgL_classz00_1742 = BGl_fieldz00zzjas_classfilez00;
				{	/* Jas/classfile.sch 284 */
					obj_t BgL__ortest_1117z00_1743;

					BgL__ortest_1117z00_1743 = BGL_CLASS_NIL(BgL_classz00_1742);
					if (CBOOL(BgL__ortest_1117z00_1743))
						{	/* Jas/classfile.sch 284 */
							return ((BgL_fieldz00_bglt) BgL__ortest_1117z00_1743);
						}
					else
						{	/* Jas/classfile.sch 284 */
							return
								((BgL_fieldz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1742));
						}
				}
			}
		}

	}



/* &field-nil */
	BgL_fieldz00_bglt BGl_z62fieldzd2nilzb0zzjas_classfilez00(obj_t
		BgL_envz00_2592)
	{
		{	/* Jas/classfile.sch 284 */
			return BGl_fieldzd2nilzd2zzjas_classfilez00();
		}

	}



/* field-attributes */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2attributeszd2zzjas_classfilez00(BgL_fieldz00_bglt BgL_oz00_119)
	{
		{	/* Jas/classfile.sch 285 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_119)))->
				BgL_attributesz00);
		}

	}



/* &field-attributes */
	obj_t BGl_z62fieldzd2attributeszb0zzjas_classfilez00(obj_t BgL_envz00_2593,
		obj_t BgL_oz00_2594)
	{
		{	/* Jas/classfile.sch 285 */
			return
				BGl_fieldzd2attributeszd2zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2594));
		}

	}



/* field-attributes-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2attributeszd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt
		BgL_oz00_120, obj_t BgL_vz00_121)
	{
		{	/* Jas/classfile.sch 286 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_120)))->
					BgL_attributesz00) = ((obj_t) BgL_vz00_121), BUNSPEC);
		}

	}



/* &field-attributes-set! */
	obj_t BGl_z62fieldzd2attributeszd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2595, obj_t BgL_oz00_2596, obj_t BgL_vz00_2597)
	{
		{	/* Jas/classfile.sch 286 */
			return
				BGl_fieldzd2attributeszd2setz12z12zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2596), BgL_vz00_2597);
		}

	}



/* field-pool */
	BGL_EXPORTED_DEF obj_t BGl_fieldzd2poolzd2zzjas_classfilez00(BgL_fieldz00_bglt
		BgL_oz00_122)
	{
		{	/* Jas/classfile.sch 287 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_122)))->BgL_poolz00);
		}

	}



/* &field-pool */
	obj_t BGl_z62fieldzd2poolzb0zzjas_classfilez00(obj_t BgL_envz00_2598,
		obj_t BgL_oz00_2599)
	{
		{	/* Jas/classfile.sch 287 */
			return
				BGl_fieldzd2poolzd2zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2599));
		}

	}



/* field-pool-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2poolzd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt
		BgL_oz00_123, obj_t BgL_vz00_124)
	{
		{	/* Jas/classfile.sch 288 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_123)))->BgL_poolz00) =
				((obj_t) BgL_vz00_124), BUNSPEC);
		}

	}



/* &field-pool-set! */
	obj_t BGl_z62fieldzd2poolzd2setz12z70zzjas_classfilez00(obj_t BgL_envz00_2600,
		obj_t BgL_oz00_2601, obj_t BgL_vz00_2602)
	{
		{	/* Jas/classfile.sch 288 */
			return
				BGl_fieldzd2poolzd2setz12z12zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2601), BgL_vz00_2602);
		}

	}



/* field-descriptor */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2descriptorzd2zzjas_classfilez00(BgL_fieldz00_bglt BgL_oz00_125)
	{
		{	/* Jas/classfile.sch 289 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_125)))->
				BgL_descriptorz00);
		}

	}



/* &field-descriptor */
	obj_t BGl_z62fieldzd2descriptorzb0zzjas_classfilez00(obj_t BgL_envz00_2603,
		obj_t BgL_oz00_2604)
	{
		{	/* Jas/classfile.sch 289 */
			return
				BGl_fieldzd2descriptorzd2zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2604));
		}

	}



/* field-descriptor-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2descriptorzd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt
		BgL_oz00_126, obj_t BgL_vz00_127)
	{
		{	/* Jas/classfile.sch 290 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_126)))->
					BgL_descriptorz00) = ((obj_t) BgL_vz00_127), BUNSPEC);
		}

	}



/* &field-descriptor-set! */
	obj_t BGl_z62fieldzd2descriptorzd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2605, obj_t BgL_oz00_2606, obj_t BgL_vz00_2607)
	{
		{	/* Jas/classfile.sch 290 */
			return
				BGl_fieldzd2descriptorzd2setz12z12zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2606), BgL_vz00_2607);
		}

	}



/* field-pname */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2pnamezd2zzjas_classfilez00(BgL_fieldz00_bglt BgL_oz00_128)
	{
		{	/* Jas/classfile.sch 291 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_128)))->BgL_pnamez00);
		}

	}



/* &field-pname */
	obj_t BGl_z62fieldzd2pnamezb0zzjas_classfilez00(obj_t BgL_envz00_2608,
		obj_t BgL_oz00_2609)
	{
		{	/* Jas/classfile.sch 291 */
			return
				BGl_fieldzd2pnamezd2zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2609));
		}

	}



/* field-pname-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2pnamezd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt
		BgL_oz00_129, obj_t BgL_vz00_130)
	{
		{	/* Jas/classfile.sch 292 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_129)))->
					BgL_pnamez00) = ((obj_t) BgL_vz00_130), BUNSPEC);
		}

	}



/* &field-pname-set! */
	obj_t BGl_z62fieldzd2pnamezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2610, obj_t BgL_oz00_2611, obj_t BgL_vz00_2612)
	{
		{	/* Jas/classfile.sch 292 */
			return
				BGl_fieldzd2pnamezd2setz12z12zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2611), BgL_vz00_2612);
		}

	}



/* field-type */
	BGL_EXPORTED_DEF obj_t BGl_fieldzd2typezd2zzjas_classfilez00(BgL_fieldz00_bglt
		BgL_oz00_131)
	{
		{	/* Jas/classfile.sch 293 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_131)))->BgL_typez00);
		}

	}



/* &field-type */
	obj_t BGl_z62fieldzd2typezb0zzjas_classfilez00(obj_t BgL_envz00_2613,
		obj_t BgL_oz00_2614)
	{
		{	/* Jas/classfile.sch 293 */
			return
				BGl_fieldzd2typezd2zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2614));
		}

	}



/* field-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2typezd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt
		BgL_oz00_132, obj_t BgL_vz00_133)
	{
		{	/* Jas/classfile.sch 294 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_132)))->BgL_typez00) =
				((obj_t) BgL_vz00_133), BUNSPEC);
		}

	}



/* &field-type-set! */
	obj_t BGl_z62fieldzd2typezd2setz12z70zzjas_classfilez00(obj_t BgL_envz00_2615,
		obj_t BgL_oz00_2616, obj_t BgL_vz00_2617)
	{
		{	/* Jas/classfile.sch 294 */
			return
				BGl_fieldzd2typezd2setz12z12zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2616), BgL_vz00_2617);
		}

	}



/* field-usertype */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2usertypezd2zzjas_classfilez00(BgL_fieldz00_bglt BgL_oz00_134)
	{
		{	/* Jas/classfile.sch 295 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_134)))->
				BgL_usertypez00);
		}

	}



/* &field-usertype */
	obj_t BGl_z62fieldzd2usertypezb0zzjas_classfilez00(obj_t BgL_envz00_2618,
		obj_t BgL_oz00_2619)
	{
		{	/* Jas/classfile.sch 295 */
			return
				BGl_fieldzd2usertypezd2zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2619));
		}

	}



/* field-usertype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2usertypezd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt
		BgL_oz00_135, obj_t BgL_vz00_136)
	{
		{	/* Jas/classfile.sch 296 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_135)))->
					BgL_usertypez00) = ((obj_t) BgL_vz00_136), BUNSPEC);
		}

	}



/* &field-usertype-set! */
	obj_t BGl_z62fieldzd2usertypezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2620, obj_t BgL_oz00_2621, obj_t BgL_vz00_2622)
	{
		{	/* Jas/classfile.sch 296 */
			return
				BGl_fieldzd2usertypezd2setz12z12zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2621), BgL_vz00_2622);
		}

	}



/* field-owner */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2ownerzd2zzjas_classfilez00(BgL_fieldz00_bglt BgL_oz00_137)
	{
		{	/* Jas/classfile.sch 297 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_137)))->BgL_ownerz00);
		}

	}



/* &field-owner */
	obj_t BGl_z62fieldzd2ownerzb0zzjas_classfilez00(obj_t BgL_envz00_2623,
		obj_t BgL_oz00_2624)
	{
		{	/* Jas/classfile.sch 297 */
			return
				BGl_fieldzd2ownerzd2zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2624));
		}

	}



/* field-owner-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2ownerzd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt
		BgL_oz00_138, obj_t BgL_vz00_139)
	{
		{	/* Jas/classfile.sch 298 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_138)))->
					BgL_ownerz00) = ((obj_t) BgL_vz00_139), BUNSPEC);
		}

	}



/* &field-owner-set! */
	obj_t BGl_z62fieldzd2ownerzd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2625, obj_t BgL_oz00_2626, obj_t BgL_vz00_2627)
	{
		{	/* Jas/classfile.sch 298 */
			return
				BGl_fieldzd2ownerzd2setz12z12zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2626), BgL_vz00_2627);
		}

	}



/* field-name */
	BGL_EXPORTED_DEF obj_t BGl_fieldzd2namezd2zzjas_classfilez00(BgL_fieldz00_bglt
		BgL_oz00_140)
	{
		{	/* Jas/classfile.sch 299 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_140)))->BgL_namez00);
		}

	}



/* &field-name */
	obj_t BGl_z62fieldzd2namezb0zzjas_classfilez00(obj_t BgL_envz00_2628,
		obj_t BgL_oz00_2629)
	{
		{	/* Jas/classfile.sch 299 */
			return
				BGl_fieldzd2namezd2zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2629));
		}

	}



/* field-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2namezd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt
		BgL_oz00_141, obj_t BgL_vz00_142)
	{
		{	/* Jas/classfile.sch 300 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_141)))->BgL_namez00) =
				((obj_t) BgL_vz00_142), BUNSPEC);
		}

	}



/* &field-name-set! */
	obj_t BGl_z62fieldzd2namezd2setz12z70zzjas_classfilez00(obj_t BgL_envz00_2630,
		obj_t BgL_oz00_2631, obj_t BgL_vz00_2632)
	{
		{	/* Jas/classfile.sch 300 */
			return
				BGl_fieldzd2namezd2setz12z12zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2631), BgL_vz00_2632);
		}

	}



/* field-flags */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2flagszd2zzjas_classfilez00(BgL_fieldz00_bglt BgL_oz00_143)
	{
		{	/* Jas/classfile.sch 301 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_143)))->BgL_flagsz00);
		}

	}



/* &field-flags */
	obj_t BGl_z62fieldzd2flagszb0zzjas_classfilez00(obj_t BgL_envz00_2633,
		obj_t BgL_oz00_2634)
	{
		{	/* Jas/classfile.sch 301 */
			return
				BGl_fieldzd2flagszd2zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2634));
		}

	}



/* field-flags-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_fieldzd2flagszd2setz12z12zzjas_classfilez00(BgL_fieldz00_bglt
		BgL_oz00_144, obj_t BgL_vz00_145)
	{
		{	/* Jas/classfile.sch 302 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_144)))->
					BgL_flagsz00) = ((obj_t) BgL_vz00_145), BUNSPEC);
		}

	}



/* &field-flags-set! */
	obj_t BGl_z62fieldzd2flagszd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2635, obj_t BgL_oz00_2636, obj_t BgL_vz00_2637)
	{
		{	/* Jas/classfile.sch 302 */
			return
				BGl_fieldzd2flagszd2setz12z12zzjas_classfilez00(
				((BgL_fieldz00_bglt) BgL_oz00_2636), BgL_vz00_2637);
		}

	}



/* make-method */
	BGL_EXPORTED_DEF BgL_methodz00_bglt
		BGl_makezd2methodzd2zzjas_classfilez00(obj_t BgL_flags1154z00_146,
		obj_t BgL_name1155z00_147, obj_t BgL_owner1156z00_148,
		obj_t BgL_usertype1157z00_149, obj_t BgL_type1158z00_150,
		obj_t BgL_pname1159z00_151, obj_t BgL_descriptor1160z00_152,
		obj_t BgL_pool1161z00_153, obj_t BgL_attributes1162z00_154)
	{
		{	/* Jas/classfile.sch 305 */
			{	/* Jas/classfile.sch 305 */
				BgL_methodz00_bglt BgL_new1222z00_3422;

				{	/* Jas/classfile.sch 305 */
					BgL_methodz00_bglt BgL_new1221z00_3423;

					BgL_new1221z00_3423 =
						((BgL_methodz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_methodz00_bgl))));
					{	/* Jas/classfile.sch 305 */
						long BgL_arg1606z00_3424;

						BgL_arg1606z00_3424 =
							BGL_CLASS_NUM(BGl_methodz00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1221z00_3423), BgL_arg1606z00_3424);
					}
					BgL_new1222z00_3422 = BgL_new1221z00_3423;
				}
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
								((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1222z00_3422)))->
						BgL_flagsz00) = ((obj_t) BgL_flags1154z00_146), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1222z00_3422)))->BgL_namez00) =
					((obj_t) BgL_name1155z00_147), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1222z00_3422)))->BgL_ownerz00) =
					((obj_t) BgL_owner1156z00_148), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1222z00_3422)))->BgL_usertypez00) =
					((obj_t) BgL_usertype1157z00_149), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1222z00_3422)))->BgL_typez00) =
					((obj_t) BgL_type1158z00_150), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1222z00_3422)))->BgL_pnamez00) =
					((obj_t) BgL_pname1159z00_151), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1222z00_3422)))->BgL_descriptorz00) =
					((obj_t) BgL_descriptor1160z00_152), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1222z00_3422)))->BgL_poolz00) =
					((obj_t) BgL_pool1161z00_153), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1222z00_3422)))->BgL_attributesz00) =
					((obj_t) BgL_attributes1162z00_154), BUNSPEC);
				return BgL_new1222z00_3422;
			}
		}

	}



/* &make-method */
	BgL_methodz00_bglt BGl_z62makezd2methodzb0zzjas_classfilez00(obj_t
		BgL_envz00_2638, obj_t BgL_flags1154z00_2639, obj_t BgL_name1155z00_2640,
		obj_t BgL_owner1156z00_2641, obj_t BgL_usertype1157z00_2642,
		obj_t BgL_type1158z00_2643, obj_t BgL_pname1159z00_2644,
		obj_t BgL_descriptor1160z00_2645, obj_t BgL_pool1161z00_2646,
		obj_t BgL_attributes1162z00_2647)
	{
		{	/* Jas/classfile.sch 305 */
			return
				BGl_makezd2methodzd2zzjas_classfilez00(BgL_flags1154z00_2639,
				BgL_name1155z00_2640, BgL_owner1156z00_2641, BgL_usertype1157z00_2642,
				BgL_type1158z00_2643, BgL_pname1159z00_2644, BgL_descriptor1160z00_2645,
				BgL_pool1161z00_2646, BgL_attributes1162z00_2647);
		}

	}



/* method? */
	BGL_EXPORTED_DEF bool_t BGl_methodzf3zf3zzjas_classfilez00(obj_t
		BgL_objz00_155)
	{
		{	/* Jas/classfile.sch 306 */
			{	/* Jas/classfile.sch 306 */
				obj_t BgL_classz00_3425;

				BgL_classz00_3425 = BGl_methodz00zzjas_classfilez00;
				if (BGL_OBJECTP(BgL_objz00_155))
					{	/* Jas/classfile.sch 306 */
						BgL_objectz00_bglt BgL_arg1807z00_3426;

						BgL_arg1807z00_3426 = (BgL_objectz00_bglt) (BgL_objz00_155);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Jas/classfile.sch 306 */
								long BgL_idxz00_3427;

								BgL_idxz00_3427 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3426);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3427 + 2L)) == BgL_classz00_3425);
							}
						else
							{	/* Jas/classfile.sch 306 */
								bool_t BgL_res2186z00_3430;

								{	/* Jas/classfile.sch 306 */
									obj_t BgL_oclassz00_3431;

									{	/* Jas/classfile.sch 306 */
										obj_t BgL_arg1815z00_3432;
										long BgL_arg1816z00_3433;

										BgL_arg1815z00_3432 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Jas/classfile.sch 306 */
											long BgL_arg1817z00_3434;

											BgL_arg1817z00_3434 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3426);
											BgL_arg1816z00_3433 = (BgL_arg1817z00_3434 - OBJECT_TYPE);
										}
										BgL_oclassz00_3431 =
											VECTOR_REF(BgL_arg1815z00_3432, BgL_arg1816z00_3433);
									}
									{	/* Jas/classfile.sch 306 */
										bool_t BgL__ortest_1115z00_3435;

										BgL__ortest_1115z00_3435 =
											(BgL_classz00_3425 == BgL_oclassz00_3431);
										if (BgL__ortest_1115z00_3435)
											{	/* Jas/classfile.sch 306 */
												BgL_res2186z00_3430 = BgL__ortest_1115z00_3435;
											}
										else
											{	/* Jas/classfile.sch 306 */
												long BgL_odepthz00_3436;

												{	/* Jas/classfile.sch 306 */
													obj_t BgL_arg1804z00_3437;

													BgL_arg1804z00_3437 = (BgL_oclassz00_3431);
													BgL_odepthz00_3436 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3437);
												}
												if ((2L < BgL_odepthz00_3436))
													{	/* Jas/classfile.sch 306 */
														obj_t BgL_arg1802z00_3438;

														{	/* Jas/classfile.sch 306 */
															obj_t BgL_arg1803z00_3439;

															BgL_arg1803z00_3439 = (BgL_oclassz00_3431);
															BgL_arg1802z00_3438 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3439,
																2L);
														}
														BgL_res2186z00_3430 =
															(BgL_arg1802z00_3438 == BgL_classz00_3425);
													}
												else
													{	/* Jas/classfile.sch 306 */
														BgL_res2186z00_3430 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2186z00_3430;
							}
					}
				else
					{	/* Jas/classfile.sch 306 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &method? */
	obj_t BGl_z62methodzf3z91zzjas_classfilez00(obj_t BgL_envz00_2648,
		obj_t BgL_objz00_2649)
	{
		{	/* Jas/classfile.sch 306 */
			return BBOOL(BGl_methodzf3zf3zzjas_classfilez00(BgL_objz00_2649));
		}

	}



/* method-nil */
	BGL_EXPORTED_DEF BgL_methodz00_bglt
		BGl_methodzd2nilzd2zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.sch 307 */
			{	/* Jas/classfile.sch 307 */
				obj_t BgL_classz00_1802;

				BgL_classz00_1802 = BGl_methodz00zzjas_classfilez00;
				{	/* Jas/classfile.sch 307 */
					obj_t BgL__ortest_1117z00_1803;

					BgL__ortest_1117z00_1803 = BGL_CLASS_NIL(BgL_classz00_1802);
					if (CBOOL(BgL__ortest_1117z00_1803))
						{	/* Jas/classfile.sch 307 */
							return ((BgL_methodz00_bglt) BgL__ortest_1117z00_1803);
						}
					else
						{	/* Jas/classfile.sch 307 */
							return
								((BgL_methodz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1802));
						}
				}
			}
		}

	}



/* &method-nil */
	BgL_methodz00_bglt BGl_z62methodzd2nilzb0zzjas_classfilez00(obj_t
		BgL_envz00_2650)
	{
		{	/* Jas/classfile.sch 307 */
			return BGl_methodzd2nilzd2zzjas_classfilez00();
		}

	}



/* method-attributes */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2attributeszd2zzjas_classfilez00(BgL_methodz00_bglt
		BgL_oz00_156)
	{
		{	/* Jas/classfile.sch 308 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_156)))->
				BgL_attributesz00);
		}

	}



/* &method-attributes */
	obj_t BGl_z62methodzd2attributeszb0zzjas_classfilez00(obj_t BgL_envz00_2651,
		obj_t BgL_oz00_2652)
	{
		{	/* Jas/classfile.sch 308 */
			return
				BGl_methodzd2attributeszd2zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2652));
		}

	}



/* method-attributes-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2attributeszd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt
		BgL_oz00_157, obj_t BgL_vz00_158)
	{
		{	/* Jas/classfile.sch 309 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_157)))->
					BgL_attributesz00) = ((obj_t) BgL_vz00_158), BUNSPEC);
		}

	}



/* &method-attributes-set! */
	obj_t BGl_z62methodzd2attributeszd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2653, obj_t BgL_oz00_2654, obj_t BgL_vz00_2655)
	{
		{	/* Jas/classfile.sch 309 */
			return
				BGl_methodzd2attributeszd2setz12z12zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2654), BgL_vz00_2655);
		}

	}



/* method-pool */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2poolzd2zzjas_classfilez00(BgL_methodz00_bglt BgL_oz00_159)
	{
		{	/* Jas/classfile.sch 310 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_159)))->BgL_poolz00);
		}

	}



/* &method-pool */
	obj_t BGl_z62methodzd2poolzb0zzjas_classfilez00(obj_t BgL_envz00_2656,
		obj_t BgL_oz00_2657)
	{
		{	/* Jas/classfile.sch 310 */
			return
				BGl_methodzd2poolzd2zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2657));
		}

	}



/* method-pool-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2poolzd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt
		BgL_oz00_160, obj_t BgL_vz00_161)
	{
		{	/* Jas/classfile.sch 311 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_160)))->BgL_poolz00) =
				((obj_t) BgL_vz00_161), BUNSPEC);
		}

	}



/* &method-pool-set! */
	obj_t BGl_z62methodzd2poolzd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2658, obj_t BgL_oz00_2659, obj_t BgL_vz00_2660)
	{
		{	/* Jas/classfile.sch 311 */
			return
				BGl_methodzd2poolzd2setz12z12zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2659), BgL_vz00_2660);
		}

	}



/* method-descriptor */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2descriptorzd2zzjas_classfilez00(BgL_methodz00_bglt
		BgL_oz00_162)
	{
		{	/* Jas/classfile.sch 312 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_162)))->
				BgL_descriptorz00);
		}

	}



/* &method-descriptor */
	obj_t BGl_z62methodzd2descriptorzb0zzjas_classfilez00(obj_t BgL_envz00_2661,
		obj_t BgL_oz00_2662)
	{
		{	/* Jas/classfile.sch 312 */
			return
				BGl_methodzd2descriptorzd2zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2662));
		}

	}



/* method-descriptor-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2descriptorzd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt
		BgL_oz00_163, obj_t BgL_vz00_164)
	{
		{	/* Jas/classfile.sch 313 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_163)))->
					BgL_descriptorz00) = ((obj_t) BgL_vz00_164), BUNSPEC);
		}

	}



/* &method-descriptor-set! */
	obj_t BGl_z62methodzd2descriptorzd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2663, obj_t BgL_oz00_2664, obj_t BgL_vz00_2665)
	{
		{	/* Jas/classfile.sch 313 */
			return
				BGl_methodzd2descriptorzd2setz12z12zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2664), BgL_vz00_2665);
		}

	}



/* method-pname */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2pnamezd2zzjas_classfilez00(BgL_methodz00_bglt BgL_oz00_165)
	{
		{	/* Jas/classfile.sch 314 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_165)))->BgL_pnamez00);
		}

	}



/* &method-pname */
	obj_t BGl_z62methodzd2pnamezb0zzjas_classfilez00(obj_t BgL_envz00_2666,
		obj_t BgL_oz00_2667)
	{
		{	/* Jas/classfile.sch 314 */
			return
				BGl_methodzd2pnamezd2zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2667));
		}

	}



/* method-pname-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2pnamezd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt
		BgL_oz00_166, obj_t BgL_vz00_167)
	{
		{	/* Jas/classfile.sch 315 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_166)))->
					BgL_pnamez00) = ((obj_t) BgL_vz00_167), BUNSPEC);
		}

	}



/* &method-pname-set! */
	obj_t BGl_z62methodzd2pnamezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2668, obj_t BgL_oz00_2669, obj_t BgL_vz00_2670)
	{
		{	/* Jas/classfile.sch 315 */
			return
				BGl_methodzd2pnamezd2setz12z12zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2669), BgL_vz00_2670);
		}

	}



/* method-type */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2typezd2zzjas_classfilez00(BgL_methodz00_bglt BgL_oz00_168)
	{
		{	/* Jas/classfile.sch 316 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_168)))->BgL_typez00);
		}

	}



/* &method-type */
	obj_t BGl_z62methodzd2typezb0zzjas_classfilez00(obj_t BgL_envz00_2671,
		obj_t BgL_oz00_2672)
	{
		{	/* Jas/classfile.sch 316 */
			return
				BGl_methodzd2typezd2zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2672));
		}

	}



/* method-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2typezd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt
		BgL_oz00_169, obj_t BgL_vz00_170)
	{
		{	/* Jas/classfile.sch 317 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_169)))->BgL_typez00) =
				((obj_t) BgL_vz00_170), BUNSPEC);
		}

	}



/* &method-type-set! */
	obj_t BGl_z62methodzd2typezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2673, obj_t BgL_oz00_2674, obj_t BgL_vz00_2675)
	{
		{	/* Jas/classfile.sch 317 */
			return
				BGl_methodzd2typezd2setz12z12zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2674), BgL_vz00_2675);
		}

	}



/* method-usertype */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2usertypezd2zzjas_classfilez00(BgL_methodz00_bglt BgL_oz00_171)
	{
		{	/* Jas/classfile.sch 318 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_171)))->
				BgL_usertypez00);
		}

	}



/* &method-usertype */
	obj_t BGl_z62methodzd2usertypezb0zzjas_classfilez00(obj_t BgL_envz00_2676,
		obj_t BgL_oz00_2677)
	{
		{	/* Jas/classfile.sch 318 */
			return
				BGl_methodzd2usertypezd2zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2677));
		}

	}



/* method-usertype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2usertypezd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt
		BgL_oz00_172, obj_t BgL_vz00_173)
	{
		{	/* Jas/classfile.sch 319 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_172)))->
					BgL_usertypez00) = ((obj_t) BgL_vz00_173), BUNSPEC);
		}

	}



/* &method-usertype-set! */
	obj_t BGl_z62methodzd2usertypezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2678, obj_t BgL_oz00_2679, obj_t BgL_vz00_2680)
	{
		{	/* Jas/classfile.sch 319 */
			return
				BGl_methodzd2usertypezd2setz12z12zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2679), BgL_vz00_2680);
		}

	}



/* method-owner */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2ownerzd2zzjas_classfilez00(BgL_methodz00_bglt BgL_oz00_174)
	{
		{	/* Jas/classfile.sch 320 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_174)))->BgL_ownerz00);
		}

	}



/* &method-owner */
	obj_t BGl_z62methodzd2ownerzb0zzjas_classfilez00(obj_t BgL_envz00_2681,
		obj_t BgL_oz00_2682)
	{
		{	/* Jas/classfile.sch 320 */
			return
				BGl_methodzd2ownerzd2zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2682));
		}

	}



/* method-owner-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2ownerzd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt
		BgL_oz00_175, obj_t BgL_vz00_176)
	{
		{	/* Jas/classfile.sch 321 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_175)))->
					BgL_ownerz00) = ((obj_t) BgL_vz00_176), BUNSPEC);
		}

	}



/* &method-owner-set! */
	obj_t BGl_z62methodzd2ownerzd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2683, obj_t BgL_oz00_2684, obj_t BgL_vz00_2685)
	{
		{	/* Jas/classfile.sch 321 */
			return
				BGl_methodzd2ownerzd2setz12z12zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2684), BgL_vz00_2685);
		}

	}



/* method-name */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2namezd2zzjas_classfilez00(BgL_methodz00_bglt BgL_oz00_177)
	{
		{	/* Jas/classfile.sch 322 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_177)))->BgL_namez00);
		}

	}



/* &method-name */
	obj_t BGl_z62methodzd2namezb0zzjas_classfilez00(obj_t BgL_envz00_2686,
		obj_t BgL_oz00_2687)
	{
		{	/* Jas/classfile.sch 322 */
			return
				BGl_methodzd2namezd2zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2687));
		}

	}



/* method-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2namezd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt
		BgL_oz00_178, obj_t BgL_vz00_179)
	{
		{	/* Jas/classfile.sch 323 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_178)))->BgL_namez00) =
				((obj_t) BgL_vz00_179), BUNSPEC);
		}

	}



/* &method-name-set! */
	obj_t BGl_z62methodzd2namezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2688, obj_t BgL_oz00_2689, obj_t BgL_vz00_2690)
	{
		{	/* Jas/classfile.sch 323 */
			return
				BGl_methodzd2namezd2setz12z12zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2689), BgL_vz00_2690);
		}

	}



/* method-flags */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2flagszd2zzjas_classfilez00(BgL_methodz00_bglt BgL_oz00_180)
	{
		{	/* Jas/classfile.sch 324 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_180)))->BgL_flagsz00);
		}

	}



/* &method-flags */
	obj_t BGl_z62methodzd2flagszb0zzjas_classfilez00(obj_t BgL_envz00_2691,
		obj_t BgL_oz00_2692)
	{
		{	/* Jas/classfile.sch 324 */
			return
				BGl_methodzd2flagszd2zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2692));
		}

	}



/* method-flags-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_methodzd2flagszd2setz12z12zzjas_classfilez00(BgL_methodz00_bglt
		BgL_oz00_181, obj_t BgL_vz00_182)
	{
		{	/* Jas/classfile.sch 325 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_181)))->
					BgL_flagsz00) = ((obj_t) BgL_vz00_182), BUNSPEC);
		}

	}



/* &method-flags-set! */
	obj_t BGl_z62methodzd2flagszd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2693, obj_t BgL_oz00_2694, obj_t BgL_vz00_2695)
	{
		{	/* Jas/classfile.sch 325 */
			return
				BGl_methodzd2flagszd2setz12z12zzjas_classfilez00(
				((BgL_methodz00_bglt) BgL_oz00_2694), BgL_vz00_2695);
		}

	}



/* make-attribute */
	BGL_EXPORTED_DEF BgL_attributez00_bglt
		BGl_makezd2attributezd2zzjas_classfilez00(obj_t BgL_type1149z00_183,
		obj_t BgL_name1150z00_184, obj_t BgL_siza7e1151za7_185,
		obj_t BgL_info1152z00_186)
	{
		{	/* Jas/classfile.sch 328 */
			{	/* Jas/classfile.sch 328 */
				BgL_attributez00_bglt BgL_new1243z00_3440;

				{	/* Jas/classfile.sch 328 */
					BgL_attributez00_bglt BgL_new1242z00_3441;

					BgL_new1242z00_3441 =
						((BgL_attributez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_attributez00_bgl))));
					{	/* Jas/classfile.sch 328 */
						long BgL_arg1609z00_3442;

						BgL_arg1609z00_3442 =
							BGL_CLASS_NUM(BGl_attributez00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1242z00_3441), BgL_arg1609z00_3442);
					}
					BgL_new1243z00_3440 = BgL_new1242z00_3441;
				}
				((((BgL_attributez00_bglt) COBJECT(BgL_new1243z00_3440))->BgL_typez00) =
					((obj_t) BgL_type1149z00_183), BUNSPEC);
				((((BgL_attributez00_bglt) COBJECT(BgL_new1243z00_3440))->BgL_namez00) =
					((obj_t) BgL_name1150z00_184), BUNSPEC);
				((((BgL_attributez00_bglt) COBJECT(BgL_new1243z00_3440))->
						BgL_siza7eza7) = ((obj_t) BgL_siza7e1151za7_185), BUNSPEC);
				((((BgL_attributez00_bglt) COBJECT(BgL_new1243z00_3440))->BgL_infoz00) =
					((obj_t) BgL_info1152z00_186), BUNSPEC);
				return BgL_new1243z00_3440;
			}
		}

	}



/* &make-attribute */
	BgL_attributez00_bglt BGl_z62makezd2attributezb0zzjas_classfilez00(obj_t
		BgL_envz00_2696, obj_t BgL_type1149z00_2697, obj_t BgL_name1150z00_2698,
		obj_t BgL_siza7e1151za7_2699, obj_t BgL_info1152z00_2700)
	{
		{	/* Jas/classfile.sch 328 */
			return
				BGl_makezd2attributezd2zzjas_classfilez00(BgL_type1149z00_2697,
				BgL_name1150z00_2698, BgL_siza7e1151za7_2699, BgL_info1152z00_2700);
		}

	}



/* attribute? */
	BGL_EXPORTED_DEF bool_t BGl_attributezf3zf3zzjas_classfilez00(obj_t
		BgL_objz00_187)
	{
		{	/* Jas/classfile.sch 329 */
			{	/* Jas/classfile.sch 329 */
				obj_t BgL_classz00_3443;

				BgL_classz00_3443 = BGl_attributez00zzjas_classfilez00;
				if (BGL_OBJECTP(BgL_objz00_187))
					{	/* Jas/classfile.sch 329 */
						BgL_objectz00_bglt BgL_arg1807z00_3444;

						BgL_arg1807z00_3444 = (BgL_objectz00_bglt) (BgL_objz00_187);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Jas/classfile.sch 329 */
								long BgL_idxz00_3445;

								BgL_idxz00_3445 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3444);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3445 + 1L)) == BgL_classz00_3443);
							}
						else
							{	/* Jas/classfile.sch 329 */
								bool_t BgL_res2187z00_3448;

								{	/* Jas/classfile.sch 329 */
									obj_t BgL_oclassz00_3449;

									{	/* Jas/classfile.sch 329 */
										obj_t BgL_arg1815z00_3450;
										long BgL_arg1816z00_3451;

										BgL_arg1815z00_3450 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Jas/classfile.sch 329 */
											long BgL_arg1817z00_3452;

											BgL_arg1817z00_3452 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3444);
											BgL_arg1816z00_3451 = (BgL_arg1817z00_3452 - OBJECT_TYPE);
										}
										BgL_oclassz00_3449 =
											VECTOR_REF(BgL_arg1815z00_3450, BgL_arg1816z00_3451);
									}
									{	/* Jas/classfile.sch 329 */
										bool_t BgL__ortest_1115z00_3453;

										BgL__ortest_1115z00_3453 =
											(BgL_classz00_3443 == BgL_oclassz00_3449);
										if (BgL__ortest_1115z00_3453)
											{	/* Jas/classfile.sch 329 */
												BgL_res2187z00_3448 = BgL__ortest_1115z00_3453;
											}
										else
											{	/* Jas/classfile.sch 329 */
												long BgL_odepthz00_3454;

												{	/* Jas/classfile.sch 329 */
													obj_t BgL_arg1804z00_3455;

													BgL_arg1804z00_3455 = (BgL_oclassz00_3449);
													BgL_odepthz00_3454 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3455);
												}
												if ((1L < BgL_odepthz00_3454))
													{	/* Jas/classfile.sch 329 */
														obj_t BgL_arg1802z00_3456;

														{	/* Jas/classfile.sch 329 */
															obj_t BgL_arg1803z00_3457;

															BgL_arg1803z00_3457 = (BgL_oclassz00_3449);
															BgL_arg1802z00_3456 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3457,
																1L);
														}
														BgL_res2187z00_3448 =
															(BgL_arg1802z00_3456 == BgL_classz00_3443);
													}
												else
													{	/* Jas/classfile.sch 329 */
														BgL_res2187z00_3448 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2187z00_3448;
							}
					}
				else
					{	/* Jas/classfile.sch 329 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &attribute? */
	obj_t BGl_z62attributezf3z91zzjas_classfilez00(obj_t BgL_envz00_2701,
		obj_t BgL_objz00_2702)
	{
		{	/* Jas/classfile.sch 329 */
			return BBOOL(BGl_attributezf3zf3zzjas_classfilez00(BgL_objz00_2702));
		}

	}



/* attribute-nil */
	BGL_EXPORTED_DEF BgL_attributez00_bglt
		BGl_attributezd2nilzd2zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.sch 330 */
			{	/* Jas/classfile.sch 330 */
				obj_t BgL_classz00_1862;

				BgL_classz00_1862 = BGl_attributez00zzjas_classfilez00;
				{	/* Jas/classfile.sch 330 */
					obj_t BgL__ortest_1117z00_1863;

					BgL__ortest_1117z00_1863 = BGL_CLASS_NIL(BgL_classz00_1862);
					if (CBOOL(BgL__ortest_1117z00_1863))
						{	/* Jas/classfile.sch 330 */
							return ((BgL_attributez00_bglt) BgL__ortest_1117z00_1863);
						}
					else
						{	/* Jas/classfile.sch 330 */
							return
								((BgL_attributez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1862));
						}
				}
			}
		}

	}



/* &attribute-nil */
	BgL_attributez00_bglt BGl_z62attributezd2nilzb0zzjas_classfilez00(obj_t
		BgL_envz00_2703)
	{
		{	/* Jas/classfile.sch 330 */
			return BGl_attributezd2nilzd2zzjas_classfilez00();
		}

	}



/* attribute-info */
	BGL_EXPORTED_DEF obj_t
		BGl_attributezd2infozd2zzjas_classfilez00(BgL_attributez00_bglt
		BgL_oz00_188)
	{
		{	/* Jas/classfile.sch 331 */
			return (((BgL_attributez00_bglt) COBJECT(BgL_oz00_188))->BgL_infoz00);
		}

	}



/* &attribute-info */
	obj_t BGl_z62attributezd2infozb0zzjas_classfilez00(obj_t BgL_envz00_2704,
		obj_t BgL_oz00_2705)
	{
		{	/* Jas/classfile.sch 331 */
			return
				BGl_attributezd2infozd2zzjas_classfilez00(
				((BgL_attributez00_bglt) BgL_oz00_2705));
		}

	}



/* attribute-info-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_attributezd2infozd2setz12z12zzjas_classfilez00(BgL_attributez00_bglt
		BgL_oz00_189, obj_t BgL_vz00_190)
	{
		{	/* Jas/classfile.sch 332 */
			return
				((((BgL_attributez00_bglt) COBJECT(BgL_oz00_189))->BgL_infoz00) =
				((obj_t) BgL_vz00_190), BUNSPEC);
		}

	}



/* &attribute-info-set! */
	obj_t BGl_z62attributezd2infozd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2706, obj_t BgL_oz00_2707, obj_t BgL_vz00_2708)
	{
		{	/* Jas/classfile.sch 332 */
			return
				BGl_attributezd2infozd2setz12z12zzjas_classfilez00(
				((BgL_attributez00_bglt) BgL_oz00_2707), BgL_vz00_2708);
		}

	}



/* attribute-size */
	BGL_EXPORTED_DEF obj_t
		BGl_attributezd2siza7ez75zzjas_classfilez00(BgL_attributez00_bglt
		BgL_oz00_191)
	{
		{	/* Jas/classfile.sch 333 */
			return (((BgL_attributez00_bglt) COBJECT(BgL_oz00_191))->BgL_siza7eza7);
		}

	}



/* &attribute-size */
	obj_t BGl_z62attributezd2siza7ez17zzjas_classfilez00(obj_t BgL_envz00_2709,
		obj_t BgL_oz00_2710)
	{
		{	/* Jas/classfile.sch 333 */
			return
				BGl_attributezd2siza7ez75zzjas_classfilez00(
				((BgL_attributez00_bglt) BgL_oz00_2710));
		}

	}



/* attribute-size-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_attributezd2siza7ezd2setz12zb5zzjas_classfilez00(BgL_attributez00_bglt
		BgL_oz00_192, obj_t BgL_vz00_193)
	{
		{	/* Jas/classfile.sch 334 */
			return
				((((BgL_attributez00_bglt) COBJECT(BgL_oz00_192))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_193), BUNSPEC);
		}

	}



/* &attribute-size-set! */
	obj_t BGl_z62attributezd2siza7ezd2setz12zd7zzjas_classfilez00(obj_t
		BgL_envz00_2711, obj_t BgL_oz00_2712, obj_t BgL_vz00_2713)
	{
		{	/* Jas/classfile.sch 334 */
			return
				BGl_attributezd2siza7ezd2setz12zb5zzjas_classfilez00(
				((BgL_attributez00_bglt) BgL_oz00_2712), BgL_vz00_2713);
		}

	}



/* attribute-name */
	BGL_EXPORTED_DEF obj_t
		BGl_attributezd2namezd2zzjas_classfilez00(BgL_attributez00_bglt
		BgL_oz00_194)
	{
		{	/* Jas/classfile.sch 335 */
			return (((BgL_attributez00_bglt) COBJECT(BgL_oz00_194))->BgL_namez00);
		}

	}



/* &attribute-name */
	obj_t BGl_z62attributezd2namezb0zzjas_classfilez00(obj_t BgL_envz00_2714,
		obj_t BgL_oz00_2715)
	{
		{	/* Jas/classfile.sch 335 */
			return
				BGl_attributezd2namezd2zzjas_classfilez00(
				((BgL_attributez00_bglt) BgL_oz00_2715));
		}

	}



/* attribute-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_attributezd2namezd2setz12z12zzjas_classfilez00(BgL_attributez00_bglt
		BgL_oz00_195, obj_t BgL_vz00_196)
	{
		{	/* Jas/classfile.sch 336 */
			return
				((((BgL_attributez00_bglt) COBJECT(BgL_oz00_195))->BgL_namez00) =
				((obj_t) BgL_vz00_196), BUNSPEC);
		}

	}



/* &attribute-name-set! */
	obj_t BGl_z62attributezd2namezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2716, obj_t BgL_oz00_2717, obj_t BgL_vz00_2718)
	{
		{	/* Jas/classfile.sch 336 */
			return
				BGl_attributezd2namezd2setz12z12zzjas_classfilez00(
				((BgL_attributez00_bglt) BgL_oz00_2717), BgL_vz00_2718);
		}

	}



/* attribute-type */
	BGL_EXPORTED_DEF obj_t
		BGl_attributezd2typezd2zzjas_classfilez00(BgL_attributez00_bglt
		BgL_oz00_197)
	{
		{	/* Jas/classfile.sch 337 */
			return (((BgL_attributez00_bglt) COBJECT(BgL_oz00_197))->BgL_typez00);
		}

	}



/* &attribute-type */
	obj_t BGl_z62attributezd2typezb0zzjas_classfilez00(obj_t BgL_envz00_2719,
		obj_t BgL_oz00_2720)
	{
		{	/* Jas/classfile.sch 337 */
			return
				BGl_attributezd2typezd2zzjas_classfilez00(
				((BgL_attributez00_bglt) BgL_oz00_2720));
		}

	}



/* attribute-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_attributezd2typezd2setz12z12zzjas_classfilez00(BgL_attributez00_bglt
		BgL_oz00_198, obj_t BgL_vz00_199)
	{
		{	/* Jas/classfile.sch 338 */
			return
				((((BgL_attributez00_bglt) COBJECT(BgL_oz00_198))->BgL_typez00) =
				((obj_t) BgL_vz00_199), BUNSPEC);
		}

	}



/* &attribute-type-set! */
	obj_t BGl_z62attributezd2typezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2721, obj_t BgL_oz00_2722, obj_t BgL_vz00_2723)
	{
		{	/* Jas/classfile.sch 338 */
			return
				BGl_attributezd2typezd2setz12z12zzjas_classfilez00(
				((BgL_attributez00_bglt) BgL_oz00_2722), BgL_vz00_2723);
		}

	}



/* make-classfile */
	BGL_EXPORTED_DEF BgL_classfilez00_bglt
		BGl_makezd2classfilezd2zzjas_classfilez00(obj_t
		BgL_currentzd2method1136zd2_200, obj_t BgL_globals1137z00_201,
		obj_t BgL_pool1138z00_202, obj_t BgL_poolzd2siza7e1139z75_203,
		obj_t BgL_pooledzd2names1140zd2_204, obj_t BgL_flags1141z00_205,
		obj_t BgL_me1142z00_206, obj_t BgL_super1143z00_207,
		obj_t BgL_interfaces1144z00_208, obj_t BgL_fields1145z00_209,
		obj_t BgL_methods1146z00_210, obj_t BgL_attributes1147z00_211)
	{
		{	/* Jas/classfile.sch 341 */
			{	/* Jas/classfile.sch 341 */
				BgL_classfilez00_bglt BgL_new1253z00_3458;

				{	/* Jas/classfile.sch 341 */
					BgL_classfilez00_bglt BgL_new1252z00_3459;

					BgL_new1252z00_3459 =
						((BgL_classfilez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_classfilez00_bgl))));
					{	/* Jas/classfile.sch 341 */
						long BgL_arg1611z00_3460;

						BgL_arg1611z00_3460 =
							BGL_CLASS_NUM(BGl_classfilez00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1252z00_3459), BgL_arg1611z00_3460);
					}
					BgL_new1253z00_3458 = BgL_new1252z00_3459;
				}
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1253z00_3458))->
						BgL_currentzd2methodzd2) =
					((obj_t) BgL_currentzd2method1136zd2_200), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1253z00_3458))->
						BgL_globalsz00) = ((obj_t) BgL_globals1137z00_201), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1253z00_3458))->BgL_poolz00) =
					((obj_t) BgL_pool1138z00_202), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1253z00_3458))->
						BgL_poolzd2siza7ez75) =
					((obj_t) BgL_poolzd2siza7e1139z75_203), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1253z00_3458))->
						BgL_pooledzd2nameszd2) =
					((obj_t) BgL_pooledzd2names1140zd2_204), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1253z00_3458))->
						BgL_flagsz00) = ((obj_t) BgL_flags1141z00_205), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1253z00_3458))->BgL_mez00) =
					((obj_t) BgL_me1142z00_206), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1253z00_3458))->
						BgL_superz00) = ((obj_t) BgL_super1143z00_207), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1253z00_3458))->
						BgL_interfacesz00) = ((obj_t) BgL_interfaces1144z00_208), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1253z00_3458))->
						BgL_fieldsz00) = ((obj_t) BgL_fields1145z00_209), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1253z00_3458))->
						BgL_methodsz00) = ((obj_t) BgL_methods1146z00_210), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1253z00_3458))->
						BgL_attributesz00) = ((obj_t) BgL_attributes1147z00_211), BUNSPEC);
				return BgL_new1253z00_3458;
			}
		}

	}



/* &make-classfile */
	BgL_classfilez00_bglt BGl_z62makezd2classfilezb0zzjas_classfilez00(obj_t
		BgL_envz00_2724, obj_t BgL_currentzd2method1136zd2_2725,
		obj_t BgL_globals1137z00_2726, obj_t BgL_pool1138z00_2727,
		obj_t BgL_poolzd2siza7e1139z75_2728, obj_t BgL_pooledzd2names1140zd2_2729,
		obj_t BgL_flags1141z00_2730, obj_t BgL_me1142z00_2731,
		obj_t BgL_super1143z00_2732, obj_t BgL_interfaces1144z00_2733,
		obj_t BgL_fields1145z00_2734, obj_t BgL_methods1146z00_2735,
		obj_t BgL_attributes1147z00_2736)
	{
		{	/* Jas/classfile.sch 341 */
			return
				BGl_makezd2classfilezd2zzjas_classfilez00
				(BgL_currentzd2method1136zd2_2725, BgL_globals1137z00_2726,
				BgL_pool1138z00_2727, BgL_poolzd2siza7e1139z75_2728,
				BgL_pooledzd2names1140zd2_2729, BgL_flags1141z00_2730,
				BgL_me1142z00_2731, BgL_super1143z00_2732, BgL_interfaces1144z00_2733,
				BgL_fields1145z00_2734, BgL_methods1146z00_2735,
				BgL_attributes1147z00_2736);
		}

	}



/* classfile? */
	BGL_EXPORTED_DEF bool_t BGl_classfilezf3zf3zzjas_classfilez00(obj_t
		BgL_objz00_212)
	{
		{	/* Jas/classfile.sch 342 */
			{	/* Jas/classfile.sch 342 */
				obj_t BgL_classz00_3461;

				BgL_classz00_3461 = BGl_classfilez00zzjas_classfilez00;
				if (BGL_OBJECTP(BgL_objz00_212))
					{	/* Jas/classfile.sch 342 */
						BgL_objectz00_bglt BgL_arg1807z00_3462;

						BgL_arg1807z00_3462 = (BgL_objectz00_bglt) (BgL_objz00_212);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Jas/classfile.sch 342 */
								long BgL_idxz00_3463;

								BgL_idxz00_3463 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3462);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3463 + 1L)) == BgL_classz00_3461);
							}
						else
							{	/* Jas/classfile.sch 342 */
								bool_t BgL_res2188z00_3466;

								{	/* Jas/classfile.sch 342 */
									obj_t BgL_oclassz00_3467;

									{	/* Jas/classfile.sch 342 */
										obj_t BgL_arg1815z00_3468;
										long BgL_arg1816z00_3469;

										BgL_arg1815z00_3468 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Jas/classfile.sch 342 */
											long BgL_arg1817z00_3470;

											BgL_arg1817z00_3470 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3462);
											BgL_arg1816z00_3469 = (BgL_arg1817z00_3470 - OBJECT_TYPE);
										}
										BgL_oclassz00_3467 =
											VECTOR_REF(BgL_arg1815z00_3468, BgL_arg1816z00_3469);
									}
									{	/* Jas/classfile.sch 342 */
										bool_t BgL__ortest_1115z00_3471;

										BgL__ortest_1115z00_3471 =
											(BgL_classz00_3461 == BgL_oclassz00_3467);
										if (BgL__ortest_1115z00_3471)
											{	/* Jas/classfile.sch 342 */
												BgL_res2188z00_3466 = BgL__ortest_1115z00_3471;
											}
										else
											{	/* Jas/classfile.sch 342 */
												long BgL_odepthz00_3472;

												{	/* Jas/classfile.sch 342 */
													obj_t BgL_arg1804z00_3473;

													BgL_arg1804z00_3473 = (BgL_oclassz00_3467);
													BgL_odepthz00_3472 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3473);
												}
												if ((1L < BgL_odepthz00_3472))
													{	/* Jas/classfile.sch 342 */
														obj_t BgL_arg1802z00_3474;

														{	/* Jas/classfile.sch 342 */
															obj_t BgL_arg1803z00_3475;

															BgL_arg1803z00_3475 = (BgL_oclassz00_3467);
															BgL_arg1802z00_3474 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3475,
																1L);
														}
														BgL_res2188z00_3466 =
															(BgL_arg1802z00_3474 == BgL_classz00_3461);
													}
												else
													{	/* Jas/classfile.sch 342 */
														BgL_res2188z00_3466 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2188z00_3466;
							}
					}
				else
					{	/* Jas/classfile.sch 342 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &classfile? */
	obj_t BGl_z62classfilezf3z91zzjas_classfilez00(obj_t BgL_envz00_2737,
		obj_t BgL_objz00_2738)
	{
		{	/* Jas/classfile.sch 342 */
			return BBOOL(BGl_classfilezf3zf3zzjas_classfilez00(BgL_objz00_2738));
		}

	}



/* classfile-nil */
	BGL_EXPORTED_DEF BgL_classfilez00_bglt
		BGl_classfilezd2nilzd2zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.sch 343 */
			{	/* Jas/classfile.sch 343 */
				obj_t BgL_classz00_1912;

				BgL_classz00_1912 = BGl_classfilez00zzjas_classfilez00;
				{	/* Jas/classfile.sch 343 */
					obj_t BgL__ortest_1117z00_1913;

					BgL__ortest_1117z00_1913 = BGL_CLASS_NIL(BgL_classz00_1912);
					if (CBOOL(BgL__ortest_1117z00_1913))
						{	/* Jas/classfile.sch 343 */
							return ((BgL_classfilez00_bglt) BgL__ortest_1117z00_1913);
						}
					else
						{	/* Jas/classfile.sch 343 */
							return
								((BgL_classfilez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1912));
						}
				}
			}
		}

	}



/* &classfile-nil */
	BgL_classfilez00_bglt BGl_z62classfilezd2nilzb0zzjas_classfilez00(obj_t
		BgL_envz00_2739)
	{
		{	/* Jas/classfile.sch 343 */
			return BGl_classfilezd2nilzd2zzjas_classfilez00();
		}

	}



/* classfile-attributes */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2attributeszd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_213)
	{
		{	/* Jas/classfile.sch 344 */
			return
				(((BgL_classfilez00_bglt) COBJECT(BgL_oz00_213))->BgL_attributesz00);
		}

	}



/* &classfile-attributes */
	obj_t BGl_z62classfilezd2attributeszb0zzjas_classfilez00(obj_t
		BgL_envz00_2740, obj_t BgL_oz00_2741)
	{
		{	/* Jas/classfile.sch 344 */
			return
				BGl_classfilezd2attributeszd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2741));
		}

	}



/* classfile-attributes-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2attributeszd2setz12z12zzjas_classfilez00
		(BgL_classfilez00_bglt BgL_oz00_214, obj_t BgL_vz00_215)
	{
		{	/* Jas/classfile.sch 345 */
			return
				((((BgL_classfilez00_bglt) COBJECT(BgL_oz00_214))->BgL_attributesz00) =
				((obj_t) BgL_vz00_215), BUNSPEC);
		}

	}



/* &classfile-attributes-set! */
	obj_t BGl_z62classfilezd2attributeszd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2742, obj_t BgL_oz00_2743, obj_t BgL_vz00_2744)
	{
		{	/* Jas/classfile.sch 345 */
			return
				BGl_classfilezd2attributeszd2setz12z12zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2743), BgL_vz00_2744);
		}

	}



/* classfile-methods */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2methodszd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_216)
	{
		{	/* Jas/classfile.sch 346 */
			return (((BgL_classfilez00_bglt) COBJECT(BgL_oz00_216))->BgL_methodsz00);
		}

	}



/* &classfile-methods */
	obj_t BGl_z62classfilezd2methodszb0zzjas_classfilez00(obj_t BgL_envz00_2745,
		obj_t BgL_oz00_2746)
	{
		{	/* Jas/classfile.sch 346 */
			return
				BGl_classfilezd2methodszd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2746));
		}

	}



/* classfile-methods-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2methodszd2setz12z12zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_217, obj_t BgL_vz00_218)
	{
		{	/* Jas/classfile.sch 347 */
			return
				((((BgL_classfilez00_bglt) COBJECT(BgL_oz00_217))->BgL_methodsz00) =
				((obj_t) BgL_vz00_218), BUNSPEC);
		}

	}



/* &classfile-methods-set! */
	obj_t BGl_z62classfilezd2methodszd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2747, obj_t BgL_oz00_2748, obj_t BgL_vz00_2749)
	{
		{	/* Jas/classfile.sch 347 */
			return
				BGl_classfilezd2methodszd2setz12z12zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2748), BgL_vz00_2749);
		}

	}



/* classfile-fields */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2fieldszd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_219)
	{
		{	/* Jas/classfile.sch 348 */
			return (((BgL_classfilez00_bglt) COBJECT(BgL_oz00_219))->BgL_fieldsz00);
		}

	}



/* &classfile-fields */
	obj_t BGl_z62classfilezd2fieldszb0zzjas_classfilez00(obj_t BgL_envz00_2750,
		obj_t BgL_oz00_2751)
	{
		{	/* Jas/classfile.sch 348 */
			return
				BGl_classfilezd2fieldszd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2751));
		}

	}



/* classfile-fields-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2fieldszd2setz12z12zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_220, obj_t BgL_vz00_221)
	{
		{	/* Jas/classfile.sch 349 */
			return
				((((BgL_classfilez00_bglt) COBJECT(BgL_oz00_220))->BgL_fieldsz00) =
				((obj_t) BgL_vz00_221), BUNSPEC);
		}

	}



/* &classfile-fields-set! */
	obj_t BGl_z62classfilezd2fieldszd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2752, obj_t BgL_oz00_2753, obj_t BgL_vz00_2754)
	{
		{	/* Jas/classfile.sch 349 */
			return
				BGl_classfilezd2fieldszd2setz12z12zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2753), BgL_vz00_2754);
		}

	}



/* classfile-interfaces */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2interfaceszd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_222)
	{
		{	/* Jas/classfile.sch 350 */
			return
				(((BgL_classfilez00_bglt) COBJECT(BgL_oz00_222))->BgL_interfacesz00);
		}

	}



/* &classfile-interfaces */
	obj_t BGl_z62classfilezd2interfaceszb0zzjas_classfilez00(obj_t
		BgL_envz00_2755, obj_t BgL_oz00_2756)
	{
		{	/* Jas/classfile.sch 350 */
			return
				BGl_classfilezd2interfaceszd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2756));
		}

	}



/* classfile-interfaces-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2interfaceszd2setz12z12zzjas_classfilez00
		(BgL_classfilez00_bglt BgL_oz00_223, obj_t BgL_vz00_224)
	{
		{	/* Jas/classfile.sch 351 */
			return
				((((BgL_classfilez00_bglt) COBJECT(BgL_oz00_223))->BgL_interfacesz00) =
				((obj_t) BgL_vz00_224), BUNSPEC);
		}

	}



/* &classfile-interfaces-set! */
	obj_t BGl_z62classfilezd2interfaceszd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2757, obj_t BgL_oz00_2758, obj_t BgL_vz00_2759)
	{
		{	/* Jas/classfile.sch 351 */
			return
				BGl_classfilezd2interfaceszd2setz12z12zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2758), BgL_vz00_2759);
		}

	}



/* classfile-super */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2superzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_225)
	{
		{	/* Jas/classfile.sch 352 */
			return (((BgL_classfilez00_bglt) COBJECT(BgL_oz00_225))->BgL_superz00);
		}

	}



/* &classfile-super */
	obj_t BGl_z62classfilezd2superzb0zzjas_classfilez00(obj_t BgL_envz00_2760,
		obj_t BgL_oz00_2761)
	{
		{	/* Jas/classfile.sch 352 */
			return
				BGl_classfilezd2superzd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2761));
		}

	}



/* classfile-super-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2superzd2setz12z12zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_226, obj_t BgL_vz00_227)
	{
		{	/* Jas/classfile.sch 353 */
			return
				((((BgL_classfilez00_bglt) COBJECT(BgL_oz00_226))->BgL_superz00) =
				((obj_t) BgL_vz00_227), BUNSPEC);
		}

	}



/* &classfile-super-set! */
	obj_t BGl_z62classfilezd2superzd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2762, obj_t BgL_oz00_2763, obj_t BgL_vz00_2764)
	{
		{	/* Jas/classfile.sch 353 */
			return
				BGl_classfilezd2superzd2setz12z12zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2763), BgL_vz00_2764);
		}

	}



/* classfile-me */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2mezd2zzjas_classfilez00(BgL_classfilez00_bglt BgL_oz00_228)
	{
		{	/* Jas/classfile.sch 354 */
			return (((BgL_classfilez00_bglt) COBJECT(BgL_oz00_228))->BgL_mez00);
		}

	}



/* &classfile-me */
	obj_t BGl_z62classfilezd2mezb0zzjas_classfilez00(obj_t BgL_envz00_2765,
		obj_t BgL_oz00_2766)
	{
		{	/* Jas/classfile.sch 354 */
			return
				BGl_classfilezd2mezd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2766));
		}

	}



/* classfile-me-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2mezd2setz12z12zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_229, obj_t BgL_vz00_230)
	{
		{	/* Jas/classfile.sch 355 */
			return
				((((BgL_classfilez00_bglt) COBJECT(BgL_oz00_229))->BgL_mez00) =
				((obj_t) BgL_vz00_230), BUNSPEC);
		}

	}



/* &classfile-me-set! */
	obj_t BGl_z62classfilezd2mezd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2767, obj_t BgL_oz00_2768, obj_t BgL_vz00_2769)
	{
		{	/* Jas/classfile.sch 355 */
			return
				BGl_classfilezd2mezd2setz12z12zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2768), BgL_vz00_2769);
		}

	}



/* classfile-flags */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2flagszd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_231)
	{
		{	/* Jas/classfile.sch 356 */
			return (((BgL_classfilez00_bglt) COBJECT(BgL_oz00_231))->BgL_flagsz00);
		}

	}



/* &classfile-flags */
	obj_t BGl_z62classfilezd2flagszb0zzjas_classfilez00(obj_t BgL_envz00_2770,
		obj_t BgL_oz00_2771)
	{
		{	/* Jas/classfile.sch 356 */
			return
				BGl_classfilezd2flagszd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2771));
		}

	}



/* classfile-flags-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2flagszd2setz12z12zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_232, obj_t BgL_vz00_233)
	{
		{	/* Jas/classfile.sch 357 */
			return
				((((BgL_classfilez00_bglt) COBJECT(BgL_oz00_232))->BgL_flagsz00) =
				((obj_t) BgL_vz00_233), BUNSPEC);
		}

	}



/* &classfile-flags-set! */
	obj_t BGl_z62classfilezd2flagszd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2772, obj_t BgL_oz00_2773, obj_t BgL_vz00_2774)
	{
		{	/* Jas/classfile.sch 357 */
			return
				BGl_classfilezd2flagszd2setz12z12zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2773), BgL_vz00_2774);
		}

	}



/* classfile-pooled-names */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2pooledzd2namesz00zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_234)
	{
		{	/* Jas/classfile.sch 358 */
			return
				(((BgL_classfilez00_bglt) COBJECT(BgL_oz00_234))->
				BgL_pooledzd2nameszd2);
		}

	}



/* &classfile-pooled-names */
	obj_t BGl_z62classfilezd2pooledzd2namesz62zzjas_classfilez00(obj_t
		BgL_envz00_2775, obj_t BgL_oz00_2776)
	{
		{	/* Jas/classfile.sch 358 */
			return
				BGl_classfilezd2pooledzd2namesz00zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2776));
		}

	}



/* classfile-pooled-names-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2pooledzd2nameszd2setz12zc0zzjas_classfilez00
		(BgL_classfilez00_bglt BgL_oz00_235, obj_t BgL_vz00_236)
	{
		{	/* Jas/classfile.sch 359 */
			return
				((((BgL_classfilez00_bglt) COBJECT(BgL_oz00_235))->
					BgL_pooledzd2nameszd2) = ((obj_t) BgL_vz00_236), BUNSPEC);
		}

	}



/* &classfile-pooled-names-set! */
	obj_t BGl_z62classfilezd2pooledzd2nameszd2setz12za2zzjas_classfilez00(obj_t
		BgL_envz00_2777, obj_t BgL_oz00_2778, obj_t BgL_vz00_2779)
	{
		{	/* Jas/classfile.sch 359 */
			return
				BGl_classfilezd2pooledzd2nameszd2setz12zc0zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2778), BgL_vz00_2779);
		}

	}



/* classfile-pool-size */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2poolzd2siza7eza7zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_237)
	{
		{	/* Jas/classfile.sch 360 */
			return
				(((BgL_classfilez00_bglt) COBJECT(BgL_oz00_237))->BgL_poolzd2siza7ez75);
		}

	}



/* &classfile-pool-size */
	obj_t BGl_z62classfilezd2poolzd2siza7ezc5zzjas_classfilez00(obj_t
		BgL_envz00_2780, obj_t BgL_oz00_2781)
	{
		{	/* Jas/classfile.sch 360 */
			return
				BGl_classfilezd2poolzd2siza7eza7zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2781));
		}

	}



/* classfile-pool-size-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2poolzd2siza7ezd2setz12z67zzjas_classfilez00
		(BgL_classfilez00_bglt BgL_oz00_238, obj_t BgL_vz00_239)
	{
		{	/* Jas/classfile.sch 361 */
			return
				((((BgL_classfilez00_bglt) COBJECT(BgL_oz00_238))->
					BgL_poolzd2siza7ez75) = ((obj_t) BgL_vz00_239), BUNSPEC);
		}

	}



/* &classfile-pool-size-set! */
	obj_t BGl_z62classfilezd2poolzd2siza7ezd2setz12z05zzjas_classfilez00(obj_t
		BgL_envz00_2782, obj_t BgL_oz00_2783, obj_t BgL_vz00_2784)
	{
		{	/* Jas/classfile.sch 361 */
			return
				BGl_classfilezd2poolzd2siza7ezd2setz12z67zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2783), BgL_vz00_2784);
		}

	}



/* classfile-pool */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2poolzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_240)
	{
		{	/* Jas/classfile.sch 362 */
			return (((BgL_classfilez00_bglt) COBJECT(BgL_oz00_240))->BgL_poolz00);
		}

	}



/* &classfile-pool */
	obj_t BGl_z62classfilezd2poolzb0zzjas_classfilez00(obj_t BgL_envz00_2785,
		obj_t BgL_oz00_2786)
	{
		{	/* Jas/classfile.sch 362 */
			return
				BGl_classfilezd2poolzd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2786));
		}

	}



/* classfile-pool-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2poolzd2setz12z12zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_241, obj_t BgL_vz00_242)
	{
		{	/* Jas/classfile.sch 363 */
			return
				((((BgL_classfilez00_bglt) COBJECT(BgL_oz00_241))->BgL_poolz00) =
				((obj_t) BgL_vz00_242), BUNSPEC);
		}

	}



/* &classfile-pool-set! */
	obj_t BGl_z62classfilezd2poolzd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2787, obj_t BgL_oz00_2788, obj_t BgL_vz00_2789)
	{
		{	/* Jas/classfile.sch 363 */
			return
				BGl_classfilezd2poolzd2setz12z12zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2788), BgL_vz00_2789);
		}

	}



/* classfile-globals */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2globalszd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_243)
	{
		{	/* Jas/classfile.sch 364 */
			return (((BgL_classfilez00_bglt) COBJECT(BgL_oz00_243))->BgL_globalsz00);
		}

	}



/* &classfile-globals */
	obj_t BGl_z62classfilezd2globalszb0zzjas_classfilez00(obj_t BgL_envz00_2790,
		obj_t BgL_oz00_2791)
	{
		{	/* Jas/classfile.sch 364 */
			return
				BGl_classfilezd2globalszd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2791));
		}

	}



/* classfile-globals-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2globalszd2setz12z12zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_244, obj_t BgL_vz00_245)
	{
		{	/* Jas/classfile.sch 365 */
			return
				((((BgL_classfilez00_bglt) COBJECT(BgL_oz00_244))->BgL_globalsz00) =
				((obj_t) BgL_vz00_245), BUNSPEC);
		}

	}



/* &classfile-globals-set! */
	obj_t BGl_z62classfilezd2globalszd2setz12z70zzjas_classfilez00(obj_t
		BgL_envz00_2792, obj_t BgL_oz00_2793, obj_t BgL_vz00_2794)
	{
		{	/* Jas/classfile.sch 365 */
			return
				BGl_classfilezd2globalszd2setz12z12zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2793), BgL_vz00_2794);
		}

	}



/* classfile-current-method */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2currentzd2methodz00zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_oz00_246)
	{
		{	/* Jas/classfile.sch 366 */
			return
				(((BgL_classfilez00_bglt) COBJECT(BgL_oz00_246))->
				BgL_currentzd2methodzd2);
		}

	}



/* &classfile-current-method */
	obj_t BGl_z62classfilezd2currentzd2methodz62zzjas_classfilez00(obj_t
		BgL_envz00_2795, obj_t BgL_oz00_2796)
	{
		{	/* Jas/classfile.sch 366 */
			return
				BGl_classfilezd2currentzd2methodz00zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2796));
		}

	}



/* classfile-current-method-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_classfilezd2currentzd2methodzd2setz12zc0zzjas_classfilez00
		(BgL_classfilez00_bglt BgL_oz00_247, obj_t BgL_vz00_248)
	{
		{	/* Jas/classfile.sch 367 */
			return
				((((BgL_classfilez00_bglt) COBJECT(BgL_oz00_247))->
					BgL_currentzd2methodzd2) = ((obj_t) BgL_vz00_248), BUNSPEC);
		}

	}



/* &classfile-current-method-set! */
	obj_t BGl_z62classfilezd2currentzd2methodzd2setz12za2zzjas_classfilez00(obj_t
		BgL_envz00_2797, obj_t BgL_oz00_2798, obj_t BgL_vz00_2799)
	{
		{	/* Jas/classfile.sch 367 */
			return
				BGl_classfilezd2currentzd2methodzd2setz12zc0zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_oz00_2798), BgL_vz00_2799);
		}

	}



/* jas-error */
	BGL_EXPORTED_DEF obj_t
		BGl_jaszd2errorzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_249, obj_t BgL_msgz00_250, obj_t BgL_argz00_251)
	{
		{	/* Jas/classfile.scm 89 */
			return
				BGl_errorz00zz__errorz00(
				(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_249))->
					BgL_currentzd2methodzd2), BgL_msgz00_250, BgL_argz00_251);
		}

	}



/* &jas-error */
	obj_t BGl_z62jaszd2errorzb0zzjas_classfilez00(obj_t BgL_envz00_2800,
		obj_t BgL_classfilez00_2801, obj_t BgL_msgz00_2802, obj_t BgL_argz00_2803)
	{
		{	/* Jas/classfile.scm 89 */
			return
				BGl_jaszd2errorzd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_classfilez00_2801), BgL_msgz00_2802,
				BgL_argz00_2803);
		}

	}



/* jas-warning */
	BGL_EXPORTED_DEF obj_t
		BGl_jaszd2warningzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_252, obj_t BgL_msgz00_253, obj_t BgL_argz00_254)
	{
		{	/* Jas/classfile.scm 92 */
			{	/* Jas/classfile.scm 93 */
				obj_t BgL_port1340z00_546;

				{	/* Jas/classfile.scm 93 */
					obj_t BgL_tmpz00_4644;

					BgL_tmpz00_4644 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1340z00_546 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4644);
				}
				bgl_display_string(BGl_string2220z00zzjas_classfilez00,
					BgL_port1340z00_546);
				bgl_display_obj((((BgL_classfilez00_bglt)
							COBJECT(BgL_classfilez00_252))->BgL_currentzd2methodzd2),
					BgL_port1340z00_546);
				bgl_display_string(BGl_string2221z00zzjas_classfilez00,
					BgL_port1340z00_546);
				bgl_display_string(BGl_string2221z00zzjas_classfilez00,
					BgL_port1340z00_546);
				bgl_display_obj(BgL_msgz00_253, BgL_port1340z00_546);
				bgl_display_string(BGl_string2221z00zzjas_classfilez00,
					BgL_port1340z00_546);
				bgl_display_obj(BgL_argz00_254, BgL_port1340z00_546);
				bgl_display_char(((unsigned char) 10), BgL_port1340z00_546);
			}
			return CNST_TABLE_REF(9);
		}

	}



/* &jas-warning */
	obj_t BGl_z62jaszd2warningzb0zzjas_classfilez00(obj_t BgL_envz00_2804,
		obj_t BgL_classfilez00_2805, obj_t BgL_msgz00_2806, obj_t BgL_argz00_2807)
	{
		{	/* Jas/classfile.scm 92 */
			return
				BGl_jaszd2warningzd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_classfilez00_2805), BgL_msgz00_2806,
				BgL_argz00_2807);
		}

	}



/* as-type */
	BGL_EXPORTED_DEF BgL_jastypez00_bglt
		BGl_aszd2typezd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_255, obj_t BgL_typedeclz00_256)
	{
		{	/* Jas/classfile.scm 118 */
			{
				obj_t BgL_namez00_579;

				{

					if (PAIRP(BgL_typedeclz00_256))
						{	/* Jas/classfile.scm 124 */
							obj_t BgL_cdrzd2110zd2_557;

							BgL_cdrzd2110zd2_557 = CDR(((obj_t) BgL_typedeclz00_256));
							if ((CAR(((obj_t) BgL_typedeclz00_256)) == CNST_TABLE_REF(11)))
								{	/* Jas/classfile.scm 124 */
									if (PAIRP(BgL_cdrzd2110zd2_557))
										{	/* Jas/classfile.scm 124 */
											if (NULLP(CDR(BgL_cdrzd2110zd2_557)))
												{	/* Jas/classfile.scm 124 */
													obj_t BgL_arg1642z00_563;

													BgL_arg1642z00_563 = CAR(BgL_cdrzd2110zd2_557);
													{	/* Jas/classfile.scm 126 */
														BgL_jastypez00_bglt BgL_eltzd2typezd2_1998;

														BgL_eltzd2typezd2_1998 =
															BGl_aszd2typezd2zzjas_classfilez00
															(BgL_classfilez00_255, BgL_arg1642z00_563);
														return ((BgL_jastypez00_bglt)
															BGl_getzd2vectzd2typez00zzjas_classfilez00
															(BgL_eltzd2typezd2_1998));
													}
												}
											else
												{
													obj_t BgL_auxz00_4677;

												BgL_tagzd2103zd2_554:
													{	/* Jas/classfile.scm 132 */
														obj_t BgL_g1299z00_574;

														BgL_g1299z00_574 =
															BGl_assqz00zz__r4_pairs_and_lists_6_3z00
															(BgL_typedeclz00_256,
															BGl_basiczd2encodedzd2typez00zzjas_classfilez00);
														if (CBOOL(BgL_g1299z00_574))
															{	/* Jas/classfile.scm 132 */
																BgL_auxz00_4677 =
																	CDR(((obj_t) BgL_g1299z00_574));
															}
														else
															{	/* Jas/classfile.scm 133 */
																obj_t BgL_g1301z00_576;

																BgL_namez00_579 = BgL_typedeclz00_256;
																{	/* Jas/classfile.scm 120 */
																	obj_t BgL_valuez00_581;

																	BgL_valuez00_581 =
																		BGl_getpropz00zz__r4_symbols_6_4z00
																		(BgL_namez00_579, CNST_TABLE_REF(10));
																	{	/* Jas/classfile.scm 121 */
																		bool_t BgL_test2722z00_4685;

																		{	/* Jas/classfile.scm 121 */
																			obj_t BgL_classz00_1953;

																			BgL_classz00_1953 =
																				BGl_classez00zzjas_classfilez00;
																			if (BGL_OBJECTP(BgL_valuez00_581))
																				{	/* Jas/classfile.scm 121 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_1955;
																					BgL_arg1807z00_1955 =
																						(BgL_objectz00_bglt)
																						(BgL_valuez00_581);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Jas/classfile.scm 121 */
																							long BgL_idxz00_1961;

																							BgL_idxz00_1961 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_1955);
																							BgL_test2722z00_4685 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_1961 + 2L)) ==
																								BgL_classz00_1953);
																						}
																					else
																						{	/* Jas/classfile.scm 121 */
																							bool_t BgL_res2189z00_1986;

																							{	/* Jas/classfile.scm 121 */
																								obj_t BgL_oclassz00_1969;

																								{	/* Jas/classfile.scm 121 */
																									obj_t BgL_arg1815z00_1977;
																									long BgL_arg1816z00_1978;

																									BgL_arg1815z00_1977 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Jas/classfile.scm 121 */
																										long BgL_arg1817z00_1979;

																										BgL_arg1817z00_1979 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_1955);
																										BgL_arg1816z00_1978 =
																											(BgL_arg1817z00_1979 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_1969 =
																										VECTOR_REF
																										(BgL_arg1815z00_1977,
																										BgL_arg1816z00_1978);
																								}
																								{	/* Jas/classfile.scm 121 */
																									bool_t
																										BgL__ortest_1115z00_1970;
																									BgL__ortest_1115z00_1970 =
																										(BgL_classz00_1953 ==
																										BgL_oclassz00_1969);
																									if (BgL__ortest_1115z00_1970)
																										{	/* Jas/classfile.scm 121 */
																											BgL_res2189z00_1986 =
																												BgL__ortest_1115z00_1970;
																										}
																									else
																										{	/* Jas/classfile.scm 121 */
																											long BgL_odepthz00_1971;

																											{	/* Jas/classfile.scm 121 */
																												obj_t
																													BgL_arg1804z00_1972;
																												BgL_arg1804z00_1972 =
																													(BgL_oclassz00_1969);
																												BgL_odepthz00_1971 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_1972);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_1971))
																												{	/* Jas/classfile.scm 121 */
																													obj_t
																														BgL_arg1802z00_1974;
																													{	/* Jas/classfile.scm 121 */
																														obj_t
																															BgL_arg1803z00_1975;
																														BgL_arg1803z00_1975
																															=
																															(BgL_oclassz00_1969);
																														BgL_arg1802z00_1974
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_1975,
																															2L);
																													}
																													BgL_res2189z00_1986 =
																														(BgL_arg1802z00_1974
																														==
																														BgL_classz00_1953);
																												}
																											else
																												{	/* Jas/classfile.scm 121 */
																													BgL_res2189z00_1986 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test2722z00_4685 =
																								BgL_res2189z00_1986;
																						}
																				}
																			else
																				{	/* Jas/classfile.scm 121 */
																					BgL_test2722z00_4685 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test2722z00_4685)
																			{	/* Jas/classfile.scm 121 */
																				BgL_g1301z00_576 = BgL_valuez00_581;
																			}
																		else
																			{	/* Jas/classfile.scm 121 */
																				BgL_g1301z00_576 = BFALSE;
																			}
																	}
																}
																if (CBOOL(BgL_g1301z00_576))
																	{	/* Jas/classfile.scm 133 */
																		BgL_auxz00_4677 = BgL_g1301z00_576;
																	}
																else
																	{	/* Jas/classfile.scm 133 */
																		BgL_auxz00_4677 =
																			BGl_errorz00zz__errorz00(
																			(((BgL_classfilez00_bglt)
																					COBJECT(BgL_classfilez00_255))->
																				BgL_currentzd2methodzd2),
																			BGl_string2222z00zzjas_classfilez00,
																			BgL_typedeclz00_256);
																	}
															}
													}
													return ((BgL_jastypez00_bglt) BgL_auxz00_4677);
												}
										}
									else
										{
											obj_t BgL_auxz00_4713;

											goto BgL_tagzd2103zd2_554;
											return ((BgL_jastypez00_bglt) BgL_auxz00_4713);
										}
								}
							else
								{	/* Jas/classfile.scm 124 */
									if (
										(CAR(((obj_t) BgL_typedeclz00_256)) == CNST_TABLE_REF(12)))
										{	/* Jas/classfile.scm 124 */
											if (PAIRP(BgL_cdrzd2110zd2_557))
												{	/* Jas/classfile.scm 124 */
													obj_t BgL_arg1654z00_569;
													obj_t BgL_arg1661z00_570;

													BgL_arg1654z00_569 = CAR(BgL_cdrzd2110zd2_557);
													BgL_arg1661z00_570 = CDR(BgL_cdrzd2110zd2_557);
													return
														((BgL_jastypez00_bglt)
														BGl_aszd2funtypezd2zzjas_classfilez00
														(BgL_classfilez00_255, BgL_arg1654z00_569,
															BgL_arg1661z00_570));
												}
											else
												{
													obj_t BgL_auxz00_4726;

													goto BgL_tagzd2103zd2_554;
													return ((BgL_jastypez00_bglt) BgL_auxz00_4726);
												}
										}
									else
										{
											obj_t BgL_auxz00_4728;

											goto BgL_tagzd2103zd2_554;
											return ((BgL_jastypez00_bglt) BgL_auxz00_4728);
										}
								}
						}
					else
						{
							obj_t BgL_auxz00_4730;

							goto BgL_tagzd2103zd2_554;
							return ((BgL_jastypez00_bglt) BgL_auxz00_4730);
						}
				}
			}
		}

	}



/* &as-type */
	BgL_jastypez00_bglt BGl_z62aszd2typezb0zzjas_classfilez00(obj_t
		BgL_envz00_2808, obj_t BgL_classfilez00_2809, obj_t BgL_typedeclz00_2810)
	{
		{	/* Jas/classfile.scm 118 */
			return
				BGl_aszd2typezd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_classfilez00_2809), BgL_typedeclz00_2810);
		}

	}



/* get-vect-type */
	obj_t BGl_getzd2vectzd2typez00zzjas_classfilez00(BgL_jastypez00_bglt
		BgL_typez00_257)
	{
		{	/* Jas/classfile.scm 136 */
			if (CBOOL(
					(((BgL_jastypez00_bglt) COBJECT(BgL_typez00_257))->BgL_vectz00)))
				{	/* Jas/classfile.scm 138 */
					return
						(((BgL_jastypez00_bglt) COBJECT(BgL_typez00_257))->BgL_vectz00);
				}
			else
				{	/* Jas/classfile.scm 140 */
					BgL_vectz00_bglt BgL_rz00_586;

					{	/* Jas/classfile.scm 140 */
						BgL_vectz00_bglt BgL_new1305z00_587;

						{	/* Jas/classfile.scm 141 */
							BgL_vectz00_bglt BgL_new1304z00_589;

							BgL_new1304z00_589 =
								((BgL_vectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_vectz00_bgl))));
							{	/* Jas/classfile.scm 141 */
								long BgL_arg1688z00_590;

								BgL_arg1688z00_590 =
									BGL_CLASS_NUM(BGl_vectz00zzjas_classfilez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1304z00_589),
									BgL_arg1688z00_590);
							}
							BgL_new1305z00_587 = BgL_new1304z00_589;
						}
						{
							obj_t BgL_auxz00_4742;

							{	/* Jas/classfile.scm 141 */
								obj_t BgL_arg1681z00_588;

								BgL_arg1681z00_588 =
									(((BgL_jastypez00_bglt) COBJECT(BgL_typez00_257))->
									BgL_codez00);
								BgL_auxz00_4742 =
									string_append(BGl_string2223z00zzjas_classfilez00,
									BgL_arg1681z00_588);
							}
							((((BgL_jastypez00_bglt) COBJECT(
											((BgL_jastypez00_bglt) BgL_new1305z00_587)))->
									BgL_codez00) = ((obj_t) BgL_auxz00_4742), BUNSPEC);
						}
						((((BgL_jastypez00_bglt) COBJECT(
										((BgL_jastypez00_bglt) BgL_new1305z00_587)))->BgL_vectz00) =
							((obj_t) BFALSE), BUNSPEC);
						((((BgL_vectz00_bglt) COBJECT(BgL_new1305z00_587))->BgL_typez00) =
							((BgL_jastypez00_bglt) BgL_typez00_257), BUNSPEC);
						BgL_rz00_586 = BgL_new1305z00_587;
					}
					((((BgL_jastypez00_bglt) COBJECT(BgL_typez00_257))->BgL_vectz00) =
						((obj_t) ((obj_t) BgL_rz00_586)), BUNSPEC);
					return ((obj_t) BgL_rz00_586);
				}
		}

	}



/* as-funtype */
	BGL_EXPORTED_DEF BgL_jasfunz00_bglt
		BGl_aszd2funtypezd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_258, obj_t BgL_tretz00_259, obj_t BgL_targsz00_260)
	{
		{	/* Jas/classfile.scm 146 */
			{	/* Jas/classfile.scm 147 */
				BgL_jastypez00_bglt BgL_tretz00_591;
				obj_t BgL_targsz00_592;

				BgL_tretz00_591 =
					BGl_aszd2typezd2zzjas_classfilez00(BgL_classfilez00_258,
					BgL_tretz00_259);
				if (NULLP(BgL_targsz00_260))
					{	/* Jas/classfile.scm 148 */
						BgL_targsz00_592 = BNIL;
					}
				else
					{	/* Jas/classfile.scm 148 */
						obj_t BgL_head1343z00_621;

						BgL_head1343z00_621 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						{
							obj_t BgL_l1341z00_623;
							obj_t BgL_tail1344z00_624;

							BgL_l1341z00_623 = BgL_targsz00_260;
							BgL_tail1344z00_624 = BgL_head1343z00_621;
						BgL_zc3z04anonymousza31716ze3z87_625:
							if (NULLP(BgL_l1341z00_623))
								{	/* Jas/classfile.scm 148 */
									BgL_targsz00_592 = CDR(BgL_head1343z00_621);
								}
							else
								{	/* Jas/classfile.scm 148 */
									obj_t BgL_newtail1345z00_627;

									{	/* Jas/classfile.scm 148 */
										BgL_jastypez00_bglt BgL_arg1722z00_629;

										{	/* Jas/classfile.scm 148 */
											obj_t BgL_tz00_630;

											BgL_tz00_630 = CAR(((obj_t) BgL_l1341z00_623));
											BgL_arg1722z00_629 =
												BGl_aszd2typezd2zzjas_classfilez00(BgL_classfilez00_258,
												BgL_tz00_630);
										}
										BgL_newtail1345z00_627 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_arg1722z00_629), BNIL);
									}
									SET_CDR(BgL_tail1344z00_624, BgL_newtail1345z00_627);
									{	/* Jas/classfile.scm 148 */
										obj_t BgL_arg1720z00_628;

										BgL_arg1720z00_628 = CDR(((obj_t) BgL_l1341z00_623));
										{
											obj_t BgL_tail1344z00_4769;
											obj_t BgL_l1341z00_4768;

											BgL_l1341z00_4768 = BgL_arg1720z00_628;
											BgL_tail1344z00_4769 = BgL_newtail1345z00_627;
											BgL_tail1344z00_624 = BgL_tail1344z00_4769;
											BgL_l1341z00_623 = BgL_l1341z00_4768;
											goto BgL_zc3z04anonymousza31716ze3z87_625;
										}
									}
								}
						}
					}
				{	/* Jas/classfile.scm 149 */
					BgL_jasfunz00_bglt BgL_new1308z00_593;

					{	/* Jas/classfile.scm 150 */
						BgL_jasfunz00_bglt BgL_new1307z00_617;

						BgL_new1307z00_617 =
							((BgL_jasfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_jasfunz00_bgl))));
						{	/* Jas/classfile.scm 150 */
							long BgL_arg1714z00_618;

							BgL_arg1714z00_618 =
								BGL_CLASS_NUM(BGl_JasFunz00zzjas_classfilez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1307z00_617), BgL_arg1714z00_618);
						}
						BgL_new1308z00_593 = BgL_new1307z00_617;
					}
					{
						obj_t BgL_auxz00_4774;

						{	/* Jas/classfile.scm 151 */
							obj_t BgL_arg1689z00_594;
							obj_t BgL_arg1691z00_595;

							{	/* Jas/classfile.scm 151 */
								obj_t BgL_runner1712z00_616;

								if (NULLP(BgL_targsz00_592))
									{	/* Jas/classfile.scm 151 */
										BgL_runner1712z00_616 = BNIL;
									}
								else
									{	/* Jas/classfile.scm 151 */
										obj_t BgL_head1348z00_602;

										{	/* Jas/classfile.scm 151 */
											obj_t BgL_arg1710z00_614;

											BgL_arg1710z00_614 =
												(((BgL_jastypez00_bglt) COBJECT(
														((BgL_jastypez00_bglt)
															CAR(((obj_t) BgL_targsz00_592)))))->BgL_codez00);
											BgL_head1348z00_602 =
												MAKE_YOUNG_PAIR(BgL_arg1710z00_614, BNIL);
										}
										{	/* Jas/classfile.scm 151 */
											obj_t BgL_g1351z00_603;

											BgL_g1351z00_603 = CDR(((obj_t) BgL_targsz00_592));
											{
												obj_t BgL_l1346z00_605;
												obj_t BgL_tail1349z00_606;

												BgL_l1346z00_605 = BgL_g1351z00_603;
												BgL_tail1349z00_606 = BgL_head1348z00_602;
											BgL_zc3z04anonymousza31703ze3z87_607:
												if (NULLP(BgL_l1346z00_605))
													{	/* Jas/classfile.scm 151 */
														BgL_runner1712z00_616 = BgL_head1348z00_602;
													}
												else
													{	/* Jas/classfile.scm 151 */
														obj_t BgL_newtail1350z00_609;

														{	/* Jas/classfile.scm 151 */
															obj_t BgL_arg1708z00_611;

															BgL_arg1708z00_611 =
																(((BgL_jastypez00_bglt) COBJECT(
																		((BgL_jastypez00_bglt)
																			CAR(
																				((obj_t) BgL_l1346z00_605)))))->
																BgL_codez00);
															BgL_newtail1350z00_609 =
																MAKE_YOUNG_PAIR(BgL_arg1708z00_611, BNIL);
														}
														SET_CDR(BgL_tail1349z00_606,
															BgL_newtail1350z00_609);
														{	/* Jas/classfile.scm 151 */
															obj_t BgL_arg1705z00_610;

															BgL_arg1705z00_610 =
																CDR(((obj_t) BgL_l1346z00_605));
															{
																obj_t BgL_tail1349z00_4796;
																obj_t BgL_l1346z00_4795;

																BgL_l1346z00_4795 = BgL_arg1705z00_610;
																BgL_tail1349z00_4796 = BgL_newtail1350z00_609;
																BgL_tail1349z00_606 = BgL_tail1349z00_4796;
																BgL_l1346z00_605 = BgL_l1346z00_4795;
																goto BgL_zc3z04anonymousza31703ze3z87_607;
															}
														}
													}
											}
										}
									}
								BgL_arg1689z00_594 =
									BGl_stringzd2appendzd2zz__r4_strings_6_7z00
									(BgL_runner1712z00_616);
							}
							BgL_arg1691z00_595 =
								(((BgL_jastypez00_bglt) COBJECT(BgL_tretz00_591))->BgL_codez00);
							{	/* Jas/classfile.scm 150 */
								obj_t BgL_list1692z00_596;

								{	/* Jas/classfile.scm 150 */
									obj_t BgL_arg1699z00_597;

									{	/* Jas/classfile.scm 150 */
										obj_t BgL_arg1700z00_598;

										{	/* Jas/classfile.scm 150 */
											obj_t BgL_arg1701z00_599;

											BgL_arg1701z00_599 =
												MAKE_YOUNG_PAIR(BgL_arg1691z00_595, BNIL);
											BgL_arg1700z00_598 =
												MAKE_YOUNG_PAIR(BGl_string2224z00zzjas_classfilez00,
												BgL_arg1701z00_599);
										}
										BgL_arg1699z00_597 =
											MAKE_YOUNG_PAIR(BgL_arg1689z00_594, BgL_arg1700z00_598);
									}
									BgL_list1692z00_596 =
										MAKE_YOUNG_PAIR(BGl_string2225z00zzjas_classfilez00,
										BgL_arg1699z00_597);
								}
								BgL_auxz00_4774 =
									BGl_stringzd2appendzd2zz__r4_strings_6_7z00
									(BgL_list1692z00_596);
							}
						}
						((((BgL_jastypez00_bglt) COBJECT(
										((BgL_jastypez00_bglt) BgL_new1308z00_593)))->BgL_codez00) =
							((obj_t) BgL_auxz00_4774), BUNSPEC);
					}
					((((BgL_jastypez00_bglt) COBJECT(
									((BgL_jastypez00_bglt) BgL_new1308z00_593)))->BgL_vectz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_jasfunz00_bglt) COBJECT(BgL_new1308z00_593))->BgL_tretz00) =
						((BgL_jastypez00_bglt) BgL_tretz00_591), BUNSPEC);
					((((BgL_jasfunz00_bglt) COBJECT(BgL_new1308z00_593))->BgL_targsz00) =
						((obj_t) BgL_targsz00_592), BUNSPEC);
					return BgL_new1308z00_593;
				}
			}
		}

	}



/* &as-funtype */
	BgL_jasfunz00_bglt BGl_z62aszd2funtypezb0zzjas_classfilez00(obj_t
		BgL_envz00_2811, obj_t BgL_classfilez00_2812, obj_t BgL_tretz00_2813,
		obj_t BgL_targsz00_2814)
	{
		{	/* Jas/classfile.scm 146 */
			return
				BGl_aszd2funtypezd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_classfilez00_2812), BgL_tretz00_2813,
				BgL_targsz00_2814);
		}

	}



/* type-size */
	BGL_EXPORTED_DEF int
		BGl_typezd2siza7ez75zzjas_classfilez00(BgL_jastypez00_bglt BgL_typez00_261)
	{
		{	/* Jas/classfile.scm 157 */
			{	/* Jas/classfile.scm 158 */
				obj_t BgL_codez00_632;

				BgL_codez00_632 =
					(((BgL_jastypez00_bglt) COBJECT(BgL_typez00_261))->BgL_codez00);
				if ((STRING_LENGTH(BgL_codez00_632) == 1L))
					{	/* Jas/classfile.scm 160 */
						switch (STRING_REF(BgL_codez00_632, 0L))
							{
							case ((unsigned char) 'V'):

								return (int) (0L);
								break;
							case ((unsigned char) 'J'):
							case ((unsigned char) 'D'):

								return (int) (2L);
								break;
							default:
								return (int) (1L);
					}}
				else
					{	/* Jas/classfile.scm 160 */
						return (int) (1L);
		}}}

	}



/* &type-size */
	obj_t BGl_z62typezd2siza7ez17zzjas_classfilez00(obj_t BgL_envz00_2815,
		obj_t BgL_typez00_2816)
	{
		{	/* Jas/classfile.scm 157 */
			return
				BINT(BGl_typezd2siza7ez75zzjas_classfilez00(
					((BgL_jastypez00_bglt) BgL_typez00_2816)));
		}

	}



/* as-assign */
	BGL_EXPORTED_DEF obj_t
		BGl_aszd2assignzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_262, obj_t BgL_namez00_263, obj_t BgL_valuez00_264)
	{
		{	/* Jas/classfile.scm 170 */
			if (CBOOL(BGl_getpropz00zz__r4_symbols_6_4z00(BgL_namez00_263,
						CNST_TABLE_REF(10))))
				{	/* Jas/classfile.scm 171 */
					return
						BGl_errorz00zz__errorz00(
						(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_262))->
							BgL_currentzd2methodzd2), BGl_string2226z00zzjas_classfilez00,
						BgL_namez00_263);
				}
			else
				{	/* Jas/classfile.scm 171 */
					BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_namez00_263,
						CNST_TABLE_REF(10), BgL_valuez00_264);
					{
						obj_t BgL_auxz00_4832;

						{	/* Jas/classfile.scm 175 */
							obj_t BgL_arg1733z00_640;

							BgL_arg1733z00_640 =
								(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_262))->
								BgL_globalsz00);
							BgL_auxz00_4832 =
								MAKE_YOUNG_PAIR(BgL_namez00_263, BgL_arg1733z00_640);
						}
						return
							((((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_262))->
								BgL_globalsz00) = ((obj_t) BgL_auxz00_4832), BUNSPEC);
					}
				}
		}

	}



/* &as-assign */
	obj_t BGl_z62aszd2assignzb0zzjas_classfilez00(obj_t BgL_envz00_2817,
		obj_t BgL_classfilez00_2818, obj_t BgL_namez00_2819,
		obj_t BgL_valuez00_2820)
	{
		{	/* Jas/classfile.scm 170 */
			return
				BGl_aszd2assignzd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_classfilez00_2818), BgL_namez00_2819,
				BgL_valuez00_2820);
		}

	}



/* declared-class */
	BGL_EXPORTED_DEF BgL_classez00_bglt
		BGl_declaredzd2classzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_269, obj_t BgL_namez00_270)
	{
		{	/* Jas/classfile.scm 187 */
			{	/* Jas/classfile.scm 188 */
				obj_t BgL_valuez00_642;

				{	/* Jas/classfile.scm 184 */
					obj_t BgL__ortest_1312z00_2040;

					BgL__ortest_1312z00_2040 =
						BGl_getpropz00zz__r4_symbols_6_4z00(BgL_namez00_270,
						CNST_TABLE_REF(10));
					if (CBOOL(BgL__ortest_1312z00_2040))
						{	/* Jas/classfile.scm 184 */
							BgL_valuez00_642 = BgL__ortest_1312z00_2040;
						}
					else
						{	/* Jas/classfile.scm 184 */
							BgL_valuez00_642 =
								BGl_errorz00zz__errorz00(
								(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_269))->
									BgL_currentzd2methodzd2), BGl_string2227z00zzjas_classfilez00,
								BgL_namez00_270);
						}
				}
				{	/* Jas/classfile.scm 189 */
					bool_t BgL_test2738z00_4844;

					{	/* Jas/classfile.scm 189 */
						obj_t BgL_classz00_2046;

						BgL_classz00_2046 = BGl_classez00zzjas_classfilez00;
						if (BGL_OBJECTP(BgL_valuez00_642))
							{	/* Jas/classfile.scm 189 */
								BgL_objectz00_bglt BgL_arg1807z00_2048;

								BgL_arg1807z00_2048 = (BgL_objectz00_bglt) (BgL_valuez00_642);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Jas/classfile.scm 189 */
										long BgL_idxz00_2054;

										BgL_idxz00_2054 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2048);
										BgL_test2738z00_4844 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2054 + 2L)) == BgL_classz00_2046);
									}
								else
									{	/* Jas/classfile.scm 189 */
										bool_t BgL_res2190z00_2079;

										{	/* Jas/classfile.scm 189 */
											obj_t BgL_oclassz00_2062;

											{	/* Jas/classfile.scm 189 */
												obj_t BgL_arg1815z00_2070;
												long BgL_arg1816z00_2071;

												BgL_arg1815z00_2070 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Jas/classfile.scm 189 */
													long BgL_arg1817z00_2072;

													BgL_arg1817z00_2072 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2048);
													BgL_arg1816z00_2071 =
														(BgL_arg1817z00_2072 - OBJECT_TYPE);
												}
												BgL_oclassz00_2062 =
													VECTOR_REF(BgL_arg1815z00_2070, BgL_arg1816z00_2071);
											}
											{	/* Jas/classfile.scm 189 */
												bool_t BgL__ortest_1115z00_2063;

												BgL__ortest_1115z00_2063 =
													(BgL_classz00_2046 == BgL_oclassz00_2062);
												if (BgL__ortest_1115z00_2063)
													{	/* Jas/classfile.scm 189 */
														BgL_res2190z00_2079 = BgL__ortest_1115z00_2063;
													}
												else
													{	/* Jas/classfile.scm 189 */
														long BgL_odepthz00_2064;

														{	/* Jas/classfile.scm 189 */
															obj_t BgL_arg1804z00_2065;

															BgL_arg1804z00_2065 = (BgL_oclassz00_2062);
															BgL_odepthz00_2064 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2065);
														}
														if ((2L < BgL_odepthz00_2064))
															{	/* Jas/classfile.scm 189 */
																obj_t BgL_arg1802z00_2067;

																{	/* Jas/classfile.scm 189 */
																	obj_t BgL_arg1803z00_2068;

																	BgL_arg1803z00_2068 = (BgL_oclassz00_2062);
																	BgL_arg1802z00_2067 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2068,
																		2L);
																}
																BgL_res2190z00_2079 =
																	(BgL_arg1802z00_2067 == BgL_classz00_2046);
															}
														else
															{	/* Jas/classfile.scm 189 */
																BgL_res2190z00_2079 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2738z00_4844 = BgL_res2190z00_2079;
									}
							}
						else
							{	/* Jas/classfile.scm 189 */
								BgL_test2738z00_4844 = ((bool_t) 0);
							}
					}
					if (BgL_test2738z00_4844)
						{	/* Jas/classfile.scm 189 */
							return ((BgL_classez00_bglt) BgL_valuez00_642);
						}
					else
						{	/* Jas/classfile.scm 90 */
							obj_t BgL_arg1613z00_2082;

							BgL_arg1613z00_2082 =
								(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_269))->
								BgL_currentzd2methodzd2);
							return ((BgL_classez00_bglt)
								BGl_errorz00zz__errorz00(BgL_arg1613z00_2082,
									BGl_string2228z00zzjas_classfilez00, BgL_namez00_270));
						}
				}
			}
		}

	}



/* &declared-class */
	BgL_classez00_bglt BGl_z62declaredzd2classzb0zzjas_classfilez00(obj_t
		BgL_envz00_2821, obj_t BgL_classfilez00_2822, obj_t BgL_namez00_2823)
	{
		{	/* Jas/classfile.scm 187 */
			return
				BGl_declaredzd2classzd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_classfilez00_2822), BgL_namez00_2823);
		}

	}



/* declared-field */
	BGL_EXPORTED_DEF BgL_fieldz00_bglt
		BGl_declaredzd2fieldzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_271, obj_t BgL_namez00_272)
	{
		{	/* Jas/classfile.scm 193 */
			{	/* Jas/classfile.scm 194 */
				obj_t BgL_valuez00_644;

				{	/* Jas/classfile.scm 184 */
					obj_t BgL__ortest_1312z00_2085;

					BgL__ortest_1312z00_2085 =
						BGl_getpropz00zz__r4_symbols_6_4z00(BgL_namez00_272,
						CNST_TABLE_REF(10));
					if (CBOOL(BgL__ortest_1312z00_2085))
						{	/* Jas/classfile.scm 184 */
							BgL_valuez00_644 = BgL__ortest_1312z00_2085;
						}
					else
						{	/* Jas/classfile.scm 184 */
							BgL_valuez00_644 =
								BGl_errorz00zz__errorz00(
								(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_271))->
									BgL_currentzd2methodzd2), BGl_string2227z00zzjas_classfilez00,
								BgL_namez00_272);
						}
				}
				{	/* Jas/classfile.scm 195 */
					bool_t BgL_test2744z00_4879;

					{	/* Jas/classfile.scm 195 */
						obj_t BgL_classz00_2091;

						BgL_classz00_2091 = BGl_fieldz00zzjas_classfilez00;
						if (BGL_OBJECTP(BgL_valuez00_644))
							{	/* Jas/classfile.scm 195 */
								BgL_objectz00_bglt BgL_arg1807z00_2093;

								BgL_arg1807z00_2093 = (BgL_objectz00_bglt) (BgL_valuez00_644);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Jas/classfile.scm 195 */
										long BgL_idxz00_2099;

										BgL_idxz00_2099 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2093);
										BgL_test2744z00_4879 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2099 + 2L)) == BgL_classz00_2091);
									}
								else
									{	/* Jas/classfile.scm 195 */
										bool_t BgL_res2191z00_2124;

										{	/* Jas/classfile.scm 195 */
											obj_t BgL_oclassz00_2107;

											{	/* Jas/classfile.scm 195 */
												obj_t BgL_arg1815z00_2115;
												long BgL_arg1816z00_2116;

												BgL_arg1815z00_2115 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Jas/classfile.scm 195 */
													long BgL_arg1817z00_2117;

													BgL_arg1817z00_2117 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2093);
													BgL_arg1816z00_2116 =
														(BgL_arg1817z00_2117 - OBJECT_TYPE);
												}
												BgL_oclassz00_2107 =
													VECTOR_REF(BgL_arg1815z00_2115, BgL_arg1816z00_2116);
											}
											{	/* Jas/classfile.scm 195 */
												bool_t BgL__ortest_1115z00_2108;

												BgL__ortest_1115z00_2108 =
													(BgL_classz00_2091 == BgL_oclassz00_2107);
												if (BgL__ortest_1115z00_2108)
													{	/* Jas/classfile.scm 195 */
														BgL_res2191z00_2124 = BgL__ortest_1115z00_2108;
													}
												else
													{	/* Jas/classfile.scm 195 */
														long BgL_odepthz00_2109;

														{	/* Jas/classfile.scm 195 */
															obj_t BgL_arg1804z00_2110;

															BgL_arg1804z00_2110 = (BgL_oclassz00_2107);
															BgL_odepthz00_2109 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2110);
														}
														if ((2L < BgL_odepthz00_2109))
															{	/* Jas/classfile.scm 195 */
																obj_t BgL_arg1802z00_2112;

																{	/* Jas/classfile.scm 195 */
																	obj_t BgL_arg1803z00_2113;

																	BgL_arg1803z00_2113 = (BgL_oclassz00_2107);
																	BgL_arg1802z00_2112 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2113,
																		2L);
																}
																BgL_res2191z00_2124 =
																	(BgL_arg1802z00_2112 == BgL_classz00_2091);
															}
														else
															{	/* Jas/classfile.scm 195 */
																BgL_res2191z00_2124 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2744z00_4879 = BgL_res2191z00_2124;
									}
							}
						else
							{	/* Jas/classfile.scm 195 */
								BgL_test2744z00_4879 = ((bool_t) 0);
							}
					}
					if (BgL_test2744z00_4879)
						{	/* Jas/classfile.scm 195 */
							return ((BgL_fieldz00_bglt) BgL_valuez00_644);
						}
					else
						{	/* Jas/classfile.scm 90 */
							obj_t BgL_arg1613z00_2127;

							BgL_arg1613z00_2127 =
								(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_271))->
								BgL_currentzd2methodzd2);
							return ((BgL_fieldz00_bglt)
								BGl_errorz00zz__errorz00(BgL_arg1613z00_2127,
									BGl_string2229z00zzjas_classfilez00, BgL_namez00_272));
						}
				}
			}
		}

	}



/* &declared-field */
	BgL_fieldz00_bglt BGl_z62declaredzd2fieldzb0zzjas_classfilez00(obj_t
		BgL_envz00_2824, obj_t BgL_classfilez00_2825, obj_t BgL_namez00_2826)
	{
		{	/* Jas/classfile.scm 193 */
			return
				BGl_declaredzd2fieldzd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_classfilez00_2825), BgL_namez00_2826);
		}

	}



/* declared-method */
	BGL_EXPORTED_DEF BgL_methodz00_bglt
		BGl_declaredzd2methodzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_273, obj_t BgL_namez00_274)
	{
		{	/* Jas/classfile.scm 199 */
			{	/* Jas/classfile.scm 200 */
				obj_t BgL_valuez00_646;

				{	/* Jas/classfile.scm 184 */
					obj_t BgL__ortest_1312z00_2130;

					BgL__ortest_1312z00_2130 =
						BGl_getpropz00zz__r4_symbols_6_4z00(BgL_namez00_274,
						CNST_TABLE_REF(10));
					if (CBOOL(BgL__ortest_1312z00_2130))
						{	/* Jas/classfile.scm 184 */
							BgL_valuez00_646 = BgL__ortest_1312z00_2130;
						}
					else
						{	/* Jas/classfile.scm 184 */
							BgL_valuez00_646 =
								BGl_errorz00zz__errorz00(
								(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_273))->
									BgL_currentzd2methodzd2), BGl_string2227z00zzjas_classfilez00,
								BgL_namez00_274);
						}
				}
				{	/* Jas/classfile.scm 201 */
					bool_t BgL_test2750z00_4914;

					{	/* Jas/classfile.scm 201 */
						obj_t BgL_classz00_2136;

						BgL_classz00_2136 = BGl_methodz00zzjas_classfilez00;
						if (BGL_OBJECTP(BgL_valuez00_646))
							{	/* Jas/classfile.scm 201 */
								BgL_objectz00_bglt BgL_arg1807z00_2138;

								BgL_arg1807z00_2138 = (BgL_objectz00_bglt) (BgL_valuez00_646);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Jas/classfile.scm 201 */
										long BgL_idxz00_2144;

										BgL_idxz00_2144 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2138);
										BgL_test2750z00_4914 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2144 + 2L)) == BgL_classz00_2136);
									}
								else
									{	/* Jas/classfile.scm 201 */
										bool_t BgL_res2192z00_2169;

										{	/* Jas/classfile.scm 201 */
											obj_t BgL_oclassz00_2152;

											{	/* Jas/classfile.scm 201 */
												obj_t BgL_arg1815z00_2160;
												long BgL_arg1816z00_2161;

												BgL_arg1815z00_2160 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Jas/classfile.scm 201 */
													long BgL_arg1817z00_2162;

													BgL_arg1817z00_2162 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2138);
													BgL_arg1816z00_2161 =
														(BgL_arg1817z00_2162 - OBJECT_TYPE);
												}
												BgL_oclassz00_2152 =
													VECTOR_REF(BgL_arg1815z00_2160, BgL_arg1816z00_2161);
											}
											{	/* Jas/classfile.scm 201 */
												bool_t BgL__ortest_1115z00_2153;

												BgL__ortest_1115z00_2153 =
													(BgL_classz00_2136 == BgL_oclassz00_2152);
												if (BgL__ortest_1115z00_2153)
													{	/* Jas/classfile.scm 201 */
														BgL_res2192z00_2169 = BgL__ortest_1115z00_2153;
													}
												else
													{	/* Jas/classfile.scm 201 */
														long BgL_odepthz00_2154;

														{	/* Jas/classfile.scm 201 */
															obj_t BgL_arg1804z00_2155;

															BgL_arg1804z00_2155 = (BgL_oclassz00_2152);
															BgL_odepthz00_2154 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2155);
														}
														if ((2L < BgL_odepthz00_2154))
															{	/* Jas/classfile.scm 201 */
																obj_t BgL_arg1802z00_2157;

																{	/* Jas/classfile.scm 201 */
																	obj_t BgL_arg1803z00_2158;

																	BgL_arg1803z00_2158 = (BgL_oclassz00_2152);
																	BgL_arg1802z00_2157 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2158,
																		2L);
																}
																BgL_res2192z00_2169 =
																	(BgL_arg1802z00_2157 == BgL_classz00_2136);
															}
														else
															{	/* Jas/classfile.scm 201 */
																BgL_res2192z00_2169 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2750z00_4914 = BgL_res2192z00_2169;
									}
							}
						else
							{	/* Jas/classfile.scm 201 */
								BgL_test2750z00_4914 = ((bool_t) 0);
							}
					}
					if (BgL_test2750z00_4914)
						{	/* Jas/classfile.scm 201 */
							return ((BgL_methodz00_bglt) BgL_valuez00_646);
						}
					else
						{	/* Jas/classfile.scm 90 */
							obj_t BgL_arg1613z00_2172;

							BgL_arg1613z00_2172 =
								(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_273))->
								BgL_currentzd2methodzd2);
							return ((BgL_methodz00_bglt)
								BGl_errorz00zz__errorz00(BgL_arg1613z00_2172,
									BGl_string2230z00zzjas_classfilez00, BgL_namez00_274));
						}
				}
			}
		}

	}



/* &declared-method */
	BgL_methodz00_bglt BGl_z62declaredzd2methodzb0zzjas_classfilez00(obj_t
		BgL_envz00_2827, obj_t BgL_classfilez00_2828, obj_t BgL_namez00_2829)
	{
		{	/* Jas/classfile.scm 199 */
			return
				BGl_declaredzd2methodzd2zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_classfilez00_2828), BgL_namez00_2829);
		}

	}



/* pool-add */
	obj_t BGl_poolzd2addzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_276, obj_t BgL_itemz00_277)
	{
		{	/* Jas/classfile.scm 213 */
			{	/* Jas/classfile.scm 215 */
				obj_t BgL_rz00_650;

				BgL_rz00_650 =
					(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_276))->
					BgL_poolzd2siza7ez75);
				{
					obj_t BgL_auxz00_4944;

					{	/* Jas/classfile.scm 216 */
						obj_t BgL_arg1739z00_651;

						BgL_arg1739z00_651 =
							(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_276))->
							BgL_poolz00);
						BgL_auxz00_4944 =
							MAKE_YOUNG_PAIR(BgL_itemz00_277, BgL_arg1739z00_651);
					}
					((((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_276))->
							BgL_poolz00) = ((obj_t) BgL_auxz00_4944), BUNSPEC);
				}
				{
					obj_t BgL_auxz00_4948;

					{
						obj_t BgL_tmpz00_4949;

						{
							long BgL_tmpz00_4951;

							if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR
										(BgL_itemz00_277), CNST_TABLE_REF(13))))
								{	/* Jas/classfile.scm 209 */
									BgL_tmpz00_4951 = 2L;
								}
							else
								{	/* Jas/classfile.scm 209 */
									BgL_tmpz00_4951 = 1L;
								}
							BgL_tmpz00_4949 = BINT(BgL_tmpz00_4951);
						}
						BgL_auxz00_4948 =
							ADDFX(
							(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_276))->
								BgL_poolzd2siza7ez75), BgL_tmpz00_4949);
					}
					((((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_276))->
							BgL_poolzd2siza7ez75) = ((obj_t) BgL_auxz00_4948), BUNSPEC);
				}
				return BgL_rz00_650;
			}
		}

	}



/* pool-get */
	long BGl_poolzd2getzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_278, long BgL_tagz00_279, obj_t BgL_valz00_280)
	{
		{	/* Jas/classfile.scm 220 */
			{
				obj_t BgL_lz00_659;
				obj_t BgL_nz00_660;

				BgL_lz00_659 =
					(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_278))->
					BgL_poolz00);
				BgL_nz00_660 =
					(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_278))->
					BgL_poolzd2siza7ez75);
			BgL_zc3z04anonymousza31750ze3z87_661:
				if (NULLP(BgL_lz00_659))
					{	/* Jas/classfile.scm 223 */
						return 0L;
					}
				else
					{	/* Jas/classfile.scm 225 */
						obj_t BgL_xtagz00_663;
						obj_t BgL_xvalz00_664;

						{	/* Jas/classfile.scm 225 */
							obj_t BgL_pairz00_2183;

							BgL_pairz00_2183 = CAR(((obj_t) BgL_lz00_659));
							BgL_xtagz00_663 = CAR(BgL_pairz00_2183);
						}
						{	/* Jas/classfile.scm 225 */
							obj_t BgL_pairz00_2187;

							BgL_pairz00_2187 = CAR(((obj_t) BgL_lz00_659));
							BgL_xvalz00_664 = CDR(BgL_pairz00_2187);
						}
						{	/* Jas/classfile.scm 226 */
							long BgL_herez00_665;

							{	/* Jas/classfile.scm 226 */
								long BgL_tmpz00_4968;

								if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
										(BgL_xtagz00_663, CNST_TABLE_REF(13))))
									{	/* Jas/classfile.scm 209 */
										BgL_tmpz00_4968 = 2L;
									}
								else
									{	/* Jas/classfile.scm 209 */
										BgL_tmpz00_4968 = 1L;
									}
								BgL_herez00_665 = ((long) CINT(BgL_nz00_660) - BgL_tmpz00_4968);
							}
							{	/* Jas/classfile.scm 227 */
								bool_t BgL_test2758z00_4975;

								if ((BgL_xtagz00_663 == BINT(BgL_tagz00_279)))
									{	/* Jas/classfile.scm 227 */
										BgL_test2758z00_4975 =
											BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_xvalz00_664,
											BgL_valz00_280);
									}
								else
									{	/* Jas/classfile.scm 227 */
										BgL_test2758z00_4975 = ((bool_t) 0);
									}
								if (BgL_test2758z00_4975)
									{	/* Jas/classfile.scm 227 */
										return BgL_herez00_665;
									}
								else
									{	/* Jas/classfile.scm 229 */
										obj_t BgL_arg1753z00_667;

										BgL_arg1753z00_667 = CDR(((obj_t) BgL_lz00_659));
										{
											obj_t BgL_nz00_4983;
											obj_t BgL_lz00_4982;

											BgL_lz00_4982 = BgL_arg1753z00_667;
											BgL_nz00_4983 = BINT(BgL_herez00_665);
											BgL_nz00_660 = BgL_nz00_4983;
											BgL_lz00_659 = BgL_lz00_4982;
											goto BgL_zc3z04anonymousza31750ze3z87_661;
										}
									}
							}
						}
					}
			}
		}

	}



/* pool-get-special! */
	obj_t BGl_poolzd2getzd2specialz12z12zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_281, long BgL_tagz00_282, obj_t BgL_valz00_283)
	{
		{	/* Jas/classfile.scm 232 */
			{	/* Jas/classfile.scm 234 */
				obj_t BgL_nz00_671;

				BgL_nz00_671 =
					BGl_hashtablezd2getzd2zz__hashz00(
					(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_281))->
						BgL_pooledzd2nameszd2), BgL_valz00_283);
				if (CBOOL(BgL_nz00_671))
					{	/* Jas/classfile.scm 235 */
						return BgL_nz00_671;
					}
				else
					{	/* Jas/classfile.scm 238 */
						obj_t BgL_rz00_673;

						BgL_rz00_673 =
							(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_281))->
							BgL_poolzd2siza7ez75);
						{
							obj_t BgL_auxz00_4992;

							{	/* Jas/classfile.scm 239 */
								obj_t BgL_arg1755z00_674;
								obj_t BgL_arg1761z00_675;

								BgL_arg1755z00_674 = MAKE_YOUNG_PAIR(BINT(1L), BgL_valz00_283);
								BgL_arg1761z00_675 =
									(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_281))->
									BgL_poolz00);
								BgL_auxz00_4992 =
									MAKE_YOUNG_PAIR(BgL_arg1755z00_674, BgL_arg1761z00_675);
							}
							((((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_281))->
									BgL_poolz00) = ((obj_t) BgL_auxz00_4992), BUNSPEC);
						}
						((((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_281))->
								BgL_poolzd2siza7ez75) =
							((obj_t) ADDFX((((BgL_classfilez00_bglt)
											COBJECT(BgL_classfilez00_281))->BgL_poolzd2siza7ez75),
									BINT(1L))), BUNSPEC);
						BGl_hashtablezd2putz12zc0zz__hashz00((((BgL_classfilez00_bglt)
									COBJECT(BgL_classfilez00_281))->BgL_pooledzd2nameszd2),
							BgL_valz00_283, BgL_rz00_673);
						return BgL_rz00_673;
					}
			}
		}

	}



/* pool-get! */
	obj_t BGl_poolzd2getz12zc0zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_284, long BgL_tagz00_285, obj_t BgL_valz00_286)
	{
		{	/* Jas/classfile.scm 244 */
			{	/* Jas/classfile.scm 245 */
				long BgL_rz00_679;

				BgL_rz00_679 =
					BGl_poolzd2getzd2zzjas_classfilez00(BgL_classfilez00_284,
					BgL_tagz00_285, BgL_valz00_286);
				if ((BgL_rz00_679 == 0L))
					{	/* Jas/classfile.scm 247 */
						obj_t BgL_arg1770z00_681;

						BgL_arg1770z00_681 =
							MAKE_YOUNG_PAIR(BINT(BgL_tagz00_285), BgL_valz00_286);
						return
							BGl_poolzd2addzd2zzjas_classfilez00(BgL_classfilez00_284,
							BgL_arg1770z00_681);
					}
				else
					{	/* Jas/classfile.scm 246 */
						return BINT(BgL_rz00_679);
					}
			}
		}

	}



/* pool-name */
	BGL_EXPORTED_DEF int
		BGl_poolzd2namezd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_287, obj_t BgL_namez00_288)
	{
		{	/* Jas/classfile.scm 251 */
			return
				CINT(BGl_poolzd2getzd2specialz12z12zzjas_classfilez00
				(BgL_classfilez00_287, 1L, BgL_namez00_288));
		}

	}



/* &pool-name */
	obj_t BGl_z62poolzd2namezb0zzjas_classfilez00(obj_t BgL_envz00_2830,
		obj_t BgL_classfilez00_2831, obj_t BgL_namez00_2832)
	{
		{	/* Jas/classfile.scm 251 */
			return
				BINT(BGl_poolzd2namezd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2831), BgL_namez00_2832));
		}

	}



/* pool-int */
	BGL_EXPORTED_DEF int BGl_poolzd2intzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_289, int BgL_nz00_290)
	{
		{	/* Jas/classfile.scm 254 */
			{	/* Jas/classfile.scm 255 */
				obj_t BgL_arg1771z00_2194;

				BgL_arg1771z00_2194 = BGl_w2z00zzjas_libz00(BgL_nz00_290);
				return
					CINT(BGl_poolzd2getz12zc0zzjas_classfilez00(BgL_classfilez00_289, 3L,
						BgL_arg1771z00_2194));
			}
		}

	}



/* &pool-int */
	obj_t BGl_z62poolzd2intzb0zzjas_classfilez00(obj_t BgL_envz00_2833,
		obj_t BgL_classfilez00_2834, obj_t BgL_nz00_2835)
	{
		{	/* Jas/classfile.scm 254 */
			return
				BINT(BGl_poolzd2intzd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2834),
					CINT(BgL_nz00_2835)));
		}

	}



/* pool-elong */
	BGL_EXPORTED_DEF int
		BGl_poolzd2elongzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_291, long BgL_nz00_292)
	{
		{	/* Jas/classfile.scm 257 */
			{	/* Jas/classfile.scm 258 */
				obj_t BgL_arg1773z00_2195;

				BgL_arg1773z00_2195 = BGl_w4elongz00zzjas_libz00(BgL_nz00_292);
				return
					CINT(BGl_poolzd2getz12zc0zzjas_classfilez00(BgL_classfilez00_291, 5L,
						BgL_arg1773z00_2195));
			}
		}

	}



/* &pool-elong */
	obj_t BGl_z62poolzd2elongzb0zzjas_classfilez00(obj_t BgL_envz00_2836,
		obj_t BgL_classfilez00_2837, obj_t BgL_nz00_2838)
	{
		{	/* Jas/classfile.scm 257 */
			return
				BINT(BGl_poolzd2elongzd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2837),
					BELONG_TO_LONG(BgL_nz00_2838)));
		}

	}



/* pool-int32 */
	BGL_EXPORTED_DEF int
		BGl_poolzd2int32zd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_293, int32_t BgL_nz00_294)
	{
		{	/* Jas/classfile.scm 260 */
			{	/* Jas/classfile.scm 261 */
				long BgL_arg1775z00_2196;

				{	/* Jas/classfile.scm 261 */
					long BgL_arg1446z00_2198;

					BgL_arg1446z00_2198 = (long) (BgL_nz00_294);
					BgL_arg1775z00_2196 = (long) (BgL_arg1446z00_2198);
				}
				{	/* Jas/classfile.scm 261 */
					int BgL_res2193z00_2203;

					{	/* Jas/classfile.scm 261 */
						int BgL_nz00_2201;

						BgL_nz00_2201 = (int) (BgL_arg1775z00_2196);
						{	/* Jas/classfile.scm 255 */
							obj_t BgL_arg1771z00_2202;

							BgL_arg1771z00_2202 = BGl_w2z00zzjas_libz00(BgL_nz00_2201);
							BgL_res2193z00_2203 =
								CINT(BGl_poolzd2getz12zc0zzjas_classfilez00
								(BgL_classfilez00_293, 3L, BgL_arg1771z00_2202));
					}}
					return BgL_res2193z00_2203;
				}
			}
		}

	}



/* &pool-int32 */
	obj_t BGl_z62poolzd2int32zb0zzjas_classfilez00(obj_t BgL_envz00_2839,
		obj_t BgL_classfilez00_2840, obj_t BgL_nz00_2841)
	{
		{	/* Jas/classfile.scm 260 */
			return
				BINT(BGl_poolzd2int32zd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2840),
					BGL_BINT32_TO_INT32(BgL_nz00_2841)));
		}

	}



/* pool-uint32 */
	BGL_EXPORTED_DEF int
		BGl_poolzd2uint32zd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_295, uint32_t BgL_nz00_296)
	{
		{	/* Jas/classfile.scm 263 */
			{	/* Jas/classfile.scm 264 */
				long BgL_arg1798z00_2204;

				BgL_arg1798z00_2204 = (long) (BgL_nz00_296);
				{	/* Jas/classfile.scm 264 */
					int BgL_res2194z00_2209;

					{	/* Jas/classfile.scm 264 */
						int BgL_nz00_2207;

						BgL_nz00_2207 = (int) (BgL_arg1798z00_2204);
						{	/* Jas/classfile.scm 255 */
							obj_t BgL_arg1771z00_2208;

							BgL_arg1771z00_2208 = BGl_w2z00zzjas_libz00(BgL_nz00_2207);
							BgL_res2194z00_2209 =
								CINT(BGl_poolzd2getz12zc0zzjas_classfilez00
								(BgL_classfilez00_295, 3L, BgL_arg1771z00_2208));
					}}
					return BgL_res2194z00_2209;
				}
			}
		}

	}



/* &pool-uint32 */
	obj_t BGl_z62poolzd2uint32zb0zzjas_classfilez00(obj_t BgL_envz00_2842,
		obj_t BgL_classfilez00_2843, obj_t BgL_nz00_2844)
	{
		{	/* Jas/classfile.scm 263 */
			return
				BINT(BGl_poolzd2uint32zd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2843),
					BGL_BUINT32_TO_UINT32(BgL_nz00_2844)));
		}

	}



/* pool-int64 */
	BGL_EXPORTED_DEF int
		BGl_poolzd2int64zd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_297, int64_t BgL_nz00_298)
	{
		{	/* Jas/classfile.scm 266 */
			{	/* Jas/classfile.scm 267 */
				BGL_LONGLONG_T BgL_arg1799z00_2210;

				{	/* Jas/classfile.scm 267 */
					uint64_t BgL_nz00_2211;

					BgL_nz00_2211 = (uint64_t) (BgL_nz00_298);
					BgL_arg1799z00_2210 = (BGL_LONGLONG_T) (BgL_nz00_2211);
				}
				{	/* Jas/classfile.scm 267 */
					int BgL_res2195z00_2215;

					{	/* Jas/classfile.scm 279 */
						obj_t BgL_arg1812z00_2214;

						BgL_arg1812z00_2214 =
							BGl_w4llongz00zzjas_libz00(BgL_arg1799z00_2210);
						BgL_res2195z00_2215 =
							CINT(BGl_poolzd2getz12zc0zzjas_classfilez00(BgL_classfilez00_297,
								5L, BgL_arg1812z00_2214));
					}
					return BgL_res2195z00_2215;
				}
			}
		}

	}



/* &pool-int64 */
	obj_t BGl_z62poolzd2int64zb0zzjas_classfilez00(obj_t BgL_envz00_2845,
		obj_t BgL_classfilez00_2846, obj_t BgL_nz00_2847)
	{
		{	/* Jas/classfile.scm 266 */
			return
				BINT(BGl_poolzd2int64zd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2846),
					BGL_BINT64_TO_INT64(BgL_nz00_2847)));
		}

	}



/* pool-uint64 */
	BGL_EXPORTED_DEF int
		BGl_poolzd2uint64zd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_299, uint64_t BgL_nz00_300)
	{
		{	/* Jas/classfile.scm 269 */
			{	/* Jas/classfile.scm 270 */
				BGL_LONGLONG_T BgL_arg1805z00_2216;

				BgL_arg1805z00_2216 = (BGL_LONGLONG_T) (BgL_nz00_300);
				{	/* Jas/classfile.scm 270 */
					int BgL_res2196z00_2221;

					{	/* Jas/classfile.scm 279 */
						obj_t BgL_arg1812z00_2220;

						BgL_arg1812z00_2220 =
							BGl_w4llongz00zzjas_libz00(BgL_arg1805z00_2216);
						BgL_res2196z00_2221 =
							CINT(BGl_poolzd2getz12zc0zzjas_classfilez00(BgL_classfilez00_299,
								5L, BgL_arg1812z00_2220));
					}
					return BgL_res2196z00_2221;
				}
			}
		}

	}



/* &pool-uint64 */
	obj_t BGl_z62poolzd2uint64zb0zzjas_classfilez00(obj_t BgL_envz00_2848,
		obj_t BgL_classfilez00_2849, obj_t BgL_nz00_2850)
	{
		{	/* Jas/classfile.scm 269 */
			return
				BINT(BGl_poolzd2uint64zd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2849),
					BGL_BINT64_TO_INT64(BgL_nz00_2850)));
		}

	}



/* pool-float */
	BGL_EXPORTED_DEF int
		BGl_poolzd2floatzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_301, float BgL_nz00_302)
	{
		{	/* Jas/classfile.scm 272 */
			{	/* Jas/classfile.scm 273 */
				obj_t BgL_arg1806z00_2222;

				BgL_arg1806z00_2222 = BGl_f2z00zzjas_libz00(BgL_nz00_302);
				return
					CINT(BGl_poolzd2getz12zc0zzjas_classfilez00(BgL_classfilez00_301, 4L,
						BgL_arg1806z00_2222));
			}
		}

	}



/* &pool-float */
	obj_t BGl_z62poolzd2floatzb0zzjas_classfilez00(obj_t BgL_envz00_2851,
		obj_t BgL_classfilez00_2852, obj_t BgL_nz00_2853)
	{
		{	/* Jas/classfile.scm 272 */
			return
				BINT(BGl_poolzd2floatzd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2852),
					REAL_TO_FLOAT(BgL_nz00_2853)));
		}

	}



/* pool-long */
	BGL_EXPORTED_DEF int
		BGl_poolzd2longzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_303, long BgL_nz00_304)
	{
		{	/* Jas/classfile.scm 275 */
			{	/* Jas/classfile.scm 276 */
				obj_t BgL_arg1808z00_2223;

				BgL_arg1808z00_2223 = BGl_w4z00zzjas_libz00(BgL_nz00_304);
				return
					CINT(BGl_poolzd2getz12zc0zzjas_classfilez00(BgL_classfilez00_303, 5L,
						BgL_arg1808z00_2223));
			}
		}

	}



/* &pool-long */
	obj_t BGl_z62poolzd2longzb0zzjas_classfilez00(obj_t BgL_envz00_2854,
		obj_t BgL_classfilez00_2855, obj_t BgL_nz00_2856)
	{
		{	/* Jas/classfile.scm 275 */
			return
				BINT(BGl_poolzd2longzd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2855),
					(long) CINT(BgL_nz00_2856)));
		}

	}



/* pool-llong */
	BGL_EXPORTED_DEF int
		BGl_poolzd2llongzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_305, BGL_LONGLONG_T BgL_nz00_306)
	{
		{	/* Jas/classfile.scm 278 */
			{	/* Jas/classfile.scm 279 */
				obj_t BgL_arg1812z00_2224;

				BgL_arg1812z00_2224 = BGl_w4llongz00zzjas_libz00(BgL_nz00_306);
				return
					CINT(BGl_poolzd2getz12zc0zzjas_classfilez00(BgL_classfilez00_305, 5L,
						BgL_arg1812z00_2224));
			}
		}

	}



/* &pool-llong */
	obj_t BGl_z62poolzd2llongzb0zzjas_classfilez00(obj_t BgL_envz00_2857,
		obj_t BgL_classfilez00_2858, obj_t BgL_nz00_2859)
	{
		{	/* Jas/classfile.scm 278 */
			return
				BINT(BGl_poolzd2llongzd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2858),
					BLLONG_TO_LLONG(BgL_nz00_2859)));
		}

	}



/* pool-double */
	BGL_EXPORTED_DEF int
		BGl_poolzd2doublezd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_307, double BgL_nz00_308)
	{
		{	/* Jas/classfile.scm 281 */
			{	/* Jas/classfile.scm 282 */
				obj_t BgL_arg1820z00_2225;

				BgL_arg1820z00_2225 = BGl_f4z00zzjas_libz00(BgL_nz00_308);
				return
					CINT(BGl_poolzd2getz12zc0zzjas_classfilez00(BgL_classfilez00_307, 6L,
						BgL_arg1820z00_2225));
			}
		}

	}



/* &pool-double */
	obj_t BGl_z62poolzd2doublezb0zzjas_classfilez00(obj_t BgL_envz00_2860,
		obj_t BgL_classfilez00_2861, obj_t BgL_nz00_2862)
	{
		{	/* Jas/classfile.scm 281 */
			return
				BINT(BGl_poolzd2doublezd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2861),
					REAL_TO_DOUBLE(BgL_nz00_2862)));
		}

	}



/* pool-class */
	BGL_EXPORTED_DEF int
		BGl_poolzd2classzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_309, BgL_classez00_bglt BgL_classez00_310)
	{
		{	/* Jas/classfile.scm 284 */
			if (CBOOL(
					(((BgL_classez00_bglt) COBJECT(BgL_classez00_310))->BgL_poolz00)))
				{	/* Jas/classfile.scm 286 */
					return
						CINT(
						(((BgL_classez00_bglt) COBJECT(BgL_classez00_310))->BgL_poolz00));
				}
			else
				{	/* Jas/classfile.scm 288 */
					int BgL_pnamez00_694;

					{	/* Jas/classfile.scm 288 */
						obj_t BgL_arg1831z00_698;

						BgL_arg1831z00_698 =
							(((BgL_classez00_bglt) COBJECT(BgL_classez00_310))->BgL_namez00);
						{	/* Jas/classfile.scm 288 */
							int BgL_res2197z00_2228;

							BgL_res2197z00_2228 =
								CINT(BGl_poolzd2getzd2specialz12z12zzjas_classfilez00
								(BgL_classfilez00_309, 1L, ((obj_t) BgL_arg1831z00_698)));
							BgL_pnamez00_694 = BgL_res2197z00_2228;
					}}
					{	/* Jas/classfile.scm 289 */
						obj_t BgL_rz00_695;

						{	/* Jas/classfile.scm 289 */
							obj_t BgL_arg1822z00_696;

							{	/* Jas/classfile.scm 289 */
								obj_t BgL_list1823z00_697;

								BgL_list1823z00_697 =
									MAKE_YOUNG_PAIR(BINT(BgL_pnamez00_694), BNIL);
								BgL_arg1822z00_696 = BgL_list1823z00_697;
							}
							BgL_rz00_695 =
								BGl_poolzd2getz12zc0zzjas_classfilez00(BgL_classfilez00_309, 7L,
								BgL_arg1822z00_696);
						}
						((((BgL_classez00_bglt) COBJECT(BgL_classez00_310))->BgL_poolz00) =
							((obj_t) BgL_rz00_695), BUNSPEC);
						return CINT(BgL_rz00_695);
					}
				}
		}

	}



/* &pool-class */
	obj_t BGl_z62poolzd2classzb0zzjas_classfilez00(obj_t BgL_envz00_2863,
		obj_t BgL_classfilez00_2864, obj_t BgL_classez00_2865)
	{
		{	/* Jas/classfile.scm 284 */
			return
				BINT(BGl_poolzd2classzd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2864),
					((BgL_classez00_bglt) BgL_classez00_2865)));
		}

	}



/* pool-class-by-name */
	BGL_EXPORTED_DEF int
		BGl_poolzd2classzd2byzd2namezd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_311, obj_t BgL_namez00_312)
	{
		{	/* Jas/classfile.scm 293 */
			return
				BGl_poolzd2classzd2zzjas_classfilez00(BgL_classfilez00_311,
				BGl_declaredzd2classzd2zzjas_classfilez00(BgL_classfilez00_311,
					BgL_namez00_312));
		}

	}



/* &pool-class-by-name */
	obj_t BGl_z62poolzd2classzd2byzd2namezb0zzjas_classfilez00(obj_t
		BgL_envz00_2866, obj_t BgL_classfilez00_2867, obj_t BgL_namez00_2868)
	{
		{	/* Jas/classfile.scm 293 */
			return
				BINT(BGl_poolzd2classzd2byzd2namezd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2867), BgL_namez00_2868));
		}

	}



/* pool-class-by-reftype */
	BGL_EXPORTED_DEF int
		BGl_poolzd2classzd2byzd2reftypezd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_313, BgL_jastypez00_bglt BgL_reftypez00_314)
	{
		{	/* Jas/classfile.scm 296 */
			{	/* Jas/classfile.scm 297 */
				bool_t BgL_test2763z00_5117;

				{	/* Jas/classfile.scm 297 */
					obj_t BgL_classz00_2231;

					BgL_classz00_2231 = BGl_vectz00zzjas_classfilez00;
					{	/* Jas/classfile.scm 297 */
						BgL_objectz00_bglt BgL_arg1807z00_2233;

						{	/* Jas/classfile.scm 297 */
							obj_t BgL_tmpz00_5118;

							BgL_tmpz00_5118 = ((obj_t) BgL_reftypez00_314);
							BgL_arg1807z00_2233 = (BgL_objectz00_bglt) (BgL_tmpz00_5118);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Jas/classfile.scm 297 */
								long BgL_idxz00_2239;

								BgL_idxz00_2239 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2233);
								BgL_test2763z00_5117 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2239 + 2L)) == BgL_classz00_2231);
							}
						else
							{	/* Jas/classfile.scm 297 */
								bool_t BgL_res2199z00_2264;

								{	/* Jas/classfile.scm 297 */
									obj_t BgL_oclassz00_2247;

									{	/* Jas/classfile.scm 297 */
										obj_t BgL_arg1815z00_2255;
										long BgL_arg1816z00_2256;

										BgL_arg1815z00_2255 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Jas/classfile.scm 297 */
											long BgL_arg1817z00_2257;

											BgL_arg1817z00_2257 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2233);
											BgL_arg1816z00_2256 = (BgL_arg1817z00_2257 - OBJECT_TYPE);
										}
										BgL_oclassz00_2247 =
											VECTOR_REF(BgL_arg1815z00_2255, BgL_arg1816z00_2256);
									}
									{	/* Jas/classfile.scm 297 */
										bool_t BgL__ortest_1115z00_2248;

										BgL__ortest_1115z00_2248 =
											(BgL_classz00_2231 == BgL_oclassz00_2247);
										if (BgL__ortest_1115z00_2248)
											{	/* Jas/classfile.scm 297 */
												BgL_res2199z00_2264 = BgL__ortest_1115z00_2248;
											}
										else
											{	/* Jas/classfile.scm 297 */
												long BgL_odepthz00_2249;

												{	/* Jas/classfile.scm 297 */
													obj_t BgL_arg1804z00_2250;

													BgL_arg1804z00_2250 = (BgL_oclassz00_2247);
													BgL_odepthz00_2249 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2250);
												}
												if ((2L < BgL_odepthz00_2249))
													{	/* Jas/classfile.scm 297 */
														obj_t BgL_arg1802z00_2252;

														{	/* Jas/classfile.scm 297 */
															obj_t BgL_arg1803z00_2253;

															BgL_arg1803z00_2253 = (BgL_oclassz00_2247);
															BgL_arg1802z00_2252 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2253,
																2L);
														}
														BgL_res2199z00_2264 =
															(BgL_arg1802z00_2252 == BgL_classz00_2231);
													}
												else
													{	/* Jas/classfile.scm 297 */
														BgL_res2199z00_2264 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test2763z00_5117 = BgL_res2199z00_2264;
							}
					}
				}
				if (BgL_test2763z00_5117)
					{	/* Jas/classfile.scm 298 */
						int BgL_pnamez00_701;

						{	/* Jas/classfile.scm 298 */
							obj_t BgL_arg1836z00_704;

							BgL_arg1836z00_704 =
								(((BgL_jastypez00_bglt) COBJECT(BgL_reftypez00_314))->
								BgL_codez00);
							BgL_pnamez00_701 =
								CINT(BGl_poolzd2getzd2specialz12z12zzjas_classfilez00
								(BgL_classfilez00_313, 1L, BgL_arg1836z00_704));
						}
						{	/* Jas/classfile.scm 299 */
							obj_t BgL_arg1834z00_702;

							{	/* Jas/classfile.scm 299 */
								obj_t BgL_list1835z00_703;

								BgL_list1835z00_703 =
									MAKE_YOUNG_PAIR(BINT(BgL_pnamez00_701), BNIL);
								BgL_arg1834z00_702 = BgL_list1835z00_703;
							}
							return
								CINT(BGl_poolzd2getz12zc0zzjas_classfilez00
								(BgL_classfilez00_313, 7L, BgL_arg1834z00_702));
						}
					}
				else
					{	/* Jas/classfile.scm 297 */
						return
							BGl_poolzd2classzd2zzjas_classfilez00(BgL_classfilez00_313,
							((BgL_classez00_bglt) BgL_reftypez00_314));
					}
			}
		}

	}



/* &pool-class-by-reftype */
	obj_t BGl_z62poolzd2classzd2byzd2reftypezb0zzjas_classfilez00(obj_t
		BgL_envz00_2869, obj_t BgL_classfilez00_2870, obj_t BgL_reftypez00_2871)
	{
		{	/* Jas/classfile.scm 296 */
			return
				BINT(BGl_poolzd2classzd2byzd2reftypezd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2870),
					((BgL_jastypez00_bglt) BgL_reftypez00_2871)));
		}

	}



/* pool-string */
	BGL_EXPORTED_DEF int
		BGl_poolzd2stringzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_315, obj_t BgL_strz00_316)
	{
		{	/* Jas/classfile.scm 302 */
			{	/* Jas/classfile.scm 303 */
				obj_t BgL_arg1837z00_705;

				{	/* Jas/classfile.scm 303 */
					int BgL_arg1838z00_706;

					BgL_arg1838z00_706 =
						CINT(BGl_poolzd2getzd2specialz12z12zzjas_classfilez00
						(BgL_classfilez00_315, 1L, BgL_strz00_316));
					{	/* Jas/classfile.scm 303 */
						obj_t BgL_list1839z00_707;

						BgL_list1839z00_707 =
							MAKE_YOUNG_PAIR(BINT(BgL_arg1838z00_706), BNIL);
						BgL_arg1837z00_705 = BgL_list1839z00_707;
				}}
				return
					CINT(BGl_poolzd2getz12zc0zzjas_classfilez00(BgL_classfilez00_315, 8L,
						BgL_arg1837z00_705));
			}
		}

	}



/* &pool-string */
	obj_t BGl_z62poolzd2stringzb0zzjas_classfilez00(obj_t BgL_envz00_2872,
		obj_t BgL_classfilez00_2873, obj_t BgL_strz00_2874)
	{
		{	/* Jas/classfile.scm 302 */
			return
				BINT(BGl_poolzd2stringzd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2873), BgL_strz00_2874));
		}

	}



/* pool-field */
	BGL_EXPORTED_DEF int
		BGl_poolzd2fieldzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_317, BgL_fieldz00_bglt BgL_fieldz00_318)
	{
		{	/* Jas/classfile.scm 305 */
			return
				CINT(BGl_poolzd2fieldzd2methodz00zzjas_classfilez00
				(BgL_classfilez00_317,
					((BgL_fieldzd2orzd2methodz00_bglt) BgL_fieldz00_318), 9L));
		}

	}



/* &pool-field */
	obj_t BGl_z62poolzd2fieldzb0zzjas_classfilez00(obj_t BgL_envz00_2875,
		obj_t BgL_classfilez00_2876, obj_t BgL_fieldz00_2877)
	{
		{	/* Jas/classfile.scm 305 */
			return
				BINT(BGl_poolzd2fieldzd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2876),
					((BgL_fieldz00_bglt) BgL_fieldz00_2877)));
		}

	}



/* pool-method */
	BGL_EXPORTED_DEF int
		BGl_poolzd2methodzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_319, BgL_methodz00_bglt BgL_methodz00_320)
	{
		{	/* Jas/classfile.scm 308 */
			return
				CINT(BGl_poolzd2fieldzd2methodz00zzjas_classfilez00
				(BgL_classfilez00_319,
					((BgL_fieldzd2orzd2methodz00_bglt) BgL_methodz00_320), 10L));
		}

	}



/* &pool-method */
	obj_t BGl_z62poolzd2methodzb0zzjas_classfilez00(obj_t BgL_envz00_2878,
		obj_t BgL_classfilez00_2879, obj_t BgL_methodz00_2880)
	{
		{	/* Jas/classfile.scm 308 */
			return
				BINT(BGl_poolzd2methodzd2zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2879),
					((BgL_methodz00_bglt) BgL_methodz00_2880)));
		}

	}



/* pool-interface-method */
	BGL_EXPORTED_DEF int
		BGl_poolzd2interfacezd2methodz00zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_321, BgL_methodz00_bglt BgL_fieldz00_322)
	{
		{	/* Jas/classfile.scm 311 */
			return
				CINT(BGl_poolzd2fieldzd2methodz00zzjas_classfilez00
				(BgL_classfilez00_321,
					((BgL_fieldzd2orzd2methodz00_bglt) BgL_fieldz00_322), 11L));
		}

	}



/* &pool-interface-method */
	obj_t BGl_z62poolzd2interfacezd2methodz62zzjas_classfilez00(obj_t
		BgL_envz00_2881, obj_t BgL_classfilez00_2882, obj_t BgL_fieldz00_2883)
	{
		{	/* Jas/classfile.scm 311 */
			return
				BINT(BGl_poolzd2interfacezd2methodz00zzjas_classfilez00(
					((BgL_classfilez00_bglt) BgL_classfilez00_2882),
					((BgL_methodz00_bglt) BgL_fieldz00_2883)));
		}

	}



/* pool-field-method */
	obj_t BGl_poolzd2fieldzd2methodz00zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_323, BgL_fieldzd2orzd2methodz00_bglt BgL_fmz00_324,
		long BgL_tagz00_325)
	{
		{	/* Jas/classfile.scm 314 */
			if (CBOOL(
					(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_fmz00_324))->
						BgL_poolz00)))
				{	/* Jas/classfile.scm 316 */
					return
						(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_fmz00_324))->
						BgL_poolz00);
				}
			else
				{	/* Jas/classfile.scm 318 */
					int BgL_pnz00_710;

					{	/* Jas/classfile.scm 318 */
						obj_t BgL_arg1851z00_724;

						BgL_arg1851z00_724 =
							(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_fmz00_324))->
							BgL_namez00);
						BgL_pnz00_710 =
							CINT(BGl_poolzd2getzd2specialz12z12zzjas_classfilez00
							(BgL_classfilez00_323, 1L, BgL_arg1851z00_724));
					}
					{	/* Jas/classfile.scm 318 */
						int BgL_ptz00_711;

						{	/* Jas/classfile.scm 319 */
							obj_t BgL_arg1849z00_722;

							BgL_arg1849z00_722 =
								(((BgL_jastypez00_bglt) COBJECT(
										((BgL_jastypez00_bglt)
											(((BgL_fieldzd2orzd2methodz00_bglt)
													COBJECT(BgL_fmz00_324))->BgL_typez00))))->
								BgL_codez00);
							BgL_ptz00_711 =
								CINT(BGl_poolzd2getzd2specialz12z12zzjas_classfilez00
								(BgL_classfilez00_323, 1L, BgL_arg1849z00_722));
						}
						{	/* Jas/classfile.scm 319 */
							int BgL_cz00_712;

							{	/* Jas/classfile.scm 320 */
								obj_t BgL_arg1848z00_721;

								BgL_arg1848z00_721 =
									(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_fmz00_324))->
									BgL_ownerz00);
								{	/* Jas/classfile.scm 294 */
									BgL_classez00_bglt BgL_arg1832z00_2285;

									BgL_arg1832z00_2285 =
										BGl_declaredzd2classzd2zzjas_classfilez00
										(BgL_classfilez00_323, ((obj_t) BgL_arg1848z00_721));
									BgL_cz00_712 =
										BGl_poolzd2classzd2zzjas_classfilez00(BgL_classfilez00_323,
										BgL_arg1832z00_2285);
							}}
							{	/* Jas/classfile.scm 320 */
								obj_t BgL_dz00_713;

								{	/* Jas/classfile.scm 321 */
									obj_t BgL_arg1845z00_718;

									{	/* Jas/classfile.scm 321 */
										obj_t BgL_list1846z00_719;

										{	/* Jas/classfile.scm 321 */
											obj_t BgL_arg1847z00_720;

											BgL_arg1847z00_720 =
												MAKE_YOUNG_PAIR(BINT(BgL_ptz00_711), BNIL);
											BgL_list1846z00_719 =
												MAKE_YOUNG_PAIR(BINT(BgL_pnz00_710),
												BgL_arg1847z00_720);
										}
										BgL_arg1845z00_718 = BgL_list1846z00_719;
									}
									BgL_dz00_713 =
										BGl_poolzd2getz12zc0zzjas_classfilez00(BgL_classfilez00_323,
										12L, BgL_arg1845z00_718);
								}
								{	/* Jas/classfile.scm 321 */
									obj_t BgL_rz00_714;

									{	/* Jas/classfile.scm 322 */
										obj_t BgL_arg1842z00_715;

										{	/* Jas/classfile.scm 322 */
											obj_t BgL_list1843z00_716;

											{	/* Jas/classfile.scm 322 */
												obj_t BgL_arg1844z00_717;

												BgL_arg1844z00_717 =
													MAKE_YOUNG_PAIR(BgL_dz00_713, BNIL);
												BgL_list1843z00_716 =
													MAKE_YOUNG_PAIR(BINT(BgL_cz00_712),
													BgL_arg1844z00_717);
											}
											BgL_arg1842z00_715 = BgL_list1843z00_716;
										}
										BgL_rz00_714 =
											BGl_poolzd2getz12zc0zzjas_classfilez00
											(BgL_classfilez00_323, BgL_tagz00_325,
											BgL_arg1842z00_715);
									}
									{	/* Jas/classfile.scm 322 */

										((((BgL_fieldzd2orzd2methodz00_bglt)
													COBJECT(BgL_fmz00_324))->BgL_pnamez00) =
											((obj_t) BINT(BgL_pnz00_710)), BUNSPEC);
										((((BgL_fieldzd2orzd2methodz00_bglt)
													COBJECT(BgL_fmz00_324))->BgL_descriptorz00) =
											((obj_t) BINT(BgL_ptz00_711)), BUNSPEC);
										((((BgL_fieldzd2orzd2methodz00_bglt)
													COBJECT(BgL_fmz00_324))->BgL_poolz00) =
											((obj_t) BgL_rz00_714), BUNSPEC);
										return BgL_rz00_714;
									}
								}
							}
						}
					}
				}
		}

	}



/* pool-local-method */
	BGL_EXPORTED_DEF obj_t
		BGl_poolzd2localzd2methodz00zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_326, BgL_methodz00_bglt BgL_methodz00_327)
	{
		{	/* Jas/classfile.scm 328 */
			return
				BGl_poolzd2fieldzd2methodzd2localzd2zzjas_classfilez00
				(BgL_classfilez00_326, BgL_methodz00_327, 10L);
		}

	}



/* &pool-local-method */
	obj_t BGl_z62poolzd2localzd2methodz62zzjas_classfilez00(obj_t BgL_envz00_2884,
		obj_t BgL_classfilez00_2885, obj_t BgL_methodz00_2886)
	{
		{	/* Jas/classfile.scm 328 */
			return
				BGl_poolzd2localzd2methodz00zzjas_classfilez00(
				((BgL_classfilez00_bglt) BgL_classfilez00_2885),
				((BgL_methodz00_bglt) BgL_methodz00_2886));
		}

	}



/* pool-field-method-local */
	obj_t
		BGl_poolzd2fieldzd2methodzd2localzd2zzjas_classfilez00(BgL_classfilez00_bglt
		BgL_classfilez00_328, BgL_methodz00_bglt BgL_fmz00_329, long BgL_tagz00_330)
	{
		{	/* Jas/classfile.scm 332 */
			if (CBOOL(
					(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
								((BgL_fieldzd2orzd2methodz00_bglt) BgL_fmz00_329)))->
						BgL_poolz00)))
				{	/* Jas/classfile.scm 334 */
					return BFALSE;
				}
			else
				{	/* Jas/classfile.scm 334 */
					{
						obj_t BgL_auxz00_5221;

						{	/* Jas/classfile.scm 335 */
							obj_t BgL_arg1853z00_727;

							BgL_arg1853z00_727 =
								(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
										((BgL_fieldzd2orzd2methodz00_bglt) BgL_fmz00_329)))->
								BgL_namez00);
							{	/* Jas/classfile.scm 335 */
								int BgL_res2208z00_2290;

								BgL_res2208z00_2290 =
									CINT(BGl_poolzd2getzd2specialz12z12zzjas_classfilez00
									(BgL_classfilez00_328, 1L, BgL_arg1853z00_727));
								BgL_auxz00_5221 = BINT(BgL_res2208z00_2290);
						}}
						((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
										((BgL_fieldzd2orzd2methodz00_bglt) BgL_fmz00_329)))->
								BgL_pnamez00) = ((obj_t) BgL_auxz00_5221), BUNSPEC);
					}
					{
						obj_t BgL_auxz00_5229;

						{	/* Jas/classfile.scm 336 */
							obj_t BgL_arg1854z00_728;

							BgL_arg1854z00_728 =
								(((BgL_jastypez00_bglt) COBJECT(
										((BgL_jastypez00_bglt)
											(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
														((BgL_fieldzd2orzd2methodz00_bglt)
															BgL_fmz00_329)))->BgL_typez00))))->BgL_codez00);
							{	/* Jas/classfile.scm 336 */
								int BgL_res2209z00_2295;

								BgL_res2209z00_2295 =
									CINT(BGl_poolzd2getzd2specialz12z12zzjas_classfilez00
									(BgL_classfilez00_328, 1L, BgL_arg1854z00_728));
								BgL_auxz00_5229 = BINT(BgL_res2209z00_2295);
						}}
						return
							((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
										((BgL_fieldzd2orzd2methodz00_bglt) BgL_fmz00_329)))->
								BgL_descriptorz00) = ((obj_t) BgL_auxz00_5229), BUNSPEC);
					}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.scm 1 */
			{	/* Jas/classfile.scm 5 */
				obj_t BgL_arg1860z00_733;
				obj_t BgL_arg1862z00_734;

				{	/* Jas/classfile.scm 5 */
					obj_t BgL_v1352z00_746;

					BgL_v1352z00_746 = create_vector(2L);
					{	/* Jas/classfile.scm 5 */
						obj_t BgL_arg1868z00_747;

						BgL_arg1868z00_747 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(14),
							BGl_proc2232z00zzjas_classfilez00,
							BGl_proc2231z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(15));
						VECTOR_SET(BgL_v1352z00_746, 0L, BgL_arg1868z00_747);
					}
					{	/* Jas/classfile.scm 5 */
						obj_t BgL_arg1873z00_757;

						BgL_arg1873z00_757 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(16),
							BGl_proc2235z00zzjas_classfilez00,
							BGl_proc2234z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2233z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1352z00_746, 1L, BgL_arg1873z00_757);
					}
					BgL_arg1860z00_733 = BgL_v1352z00_746;
				}
				{	/* Jas/classfile.scm 5 */
					obj_t BgL_v1353z00_770;

					BgL_v1353z00_770 = create_vector(0L);
					BgL_arg1862z00_734 = BgL_v1353z00_770;
				}
				BGl_JasTypez00zzjas_classfilez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(18),
					CNST_TABLE_REF(19), BGl_objectz00zz__objectz00, 38675L,
					BGl_proc2238z00zzjas_classfilez00, BGl_proc2237z00zzjas_classfilez00,
					BFALSE, BGl_proc2236z00zzjas_classfilez00, BFALSE, BgL_arg1860z00_733,
					BgL_arg1862z00_734);
			}
			{	/* Jas/classfile.scm 9 */
				obj_t BgL_arg1884z00_777;
				obj_t BgL_arg1885z00_778;

				{	/* Jas/classfile.scm 9 */
					obj_t BgL_v1354z00_790;

					BgL_v1354z00_790 = create_vector(0L);
					BgL_arg1884z00_777 = BgL_v1354z00_790;
				}
				{	/* Jas/classfile.scm 9 */
					obj_t BgL_v1355z00_791;

					BgL_v1355z00_791 = create_vector(0L);
					BgL_arg1885z00_778 = BgL_v1355z00_791;
				}
				BGl_basicz00zzjas_classfilez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(20),
					CNST_TABLE_REF(19), BGl_JasTypez00zzjas_classfilez00, 41705L,
					BGl_proc2241z00zzjas_classfilez00, BGl_proc2240z00zzjas_classfilez00,
					BFALSE, BGl_proc2239z00zzjas_classfilez00, BFALSE, BgL_arg1884z00_777,
					BgL_arg1885z00_778);
			}
			{	/* Jas/classfile.scm 11 */
				obj_t BgL_arg1894z00_798;
				obj_t BgL_arg1896z00_799;

				{	/* Jas/classfile.scm 11 */
					obj_t BgL_v1356z00_812;

					BgL_v1356z00_812 = create_vector(1L);
					{	/* Jas/classfile.scm 11 */
						obj_t BgL_arg1903z00_813;

						BgL_arg1903z00_813 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(21),
							BGl_proc2243z00zzjas_classfilez00,
							BGl_proc2242z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_JasTypez00zzjas_classfilez00);
						VECTOR_SET(BgL_v1356z00_812, 0L, BgL_arg1903z00_813);
					}
					BgL_arg1894z00_798 = BgL_v1356z00_812;
				}
				{	/* Jas/classfile.scm 11 */
					obj_t BgL_v1357z00_823;

					BgL_v1357z00_823 = create_vector(0L);
					BgL_arg1896z00_799 = BgL_v1357z00_823;
				}
				BGl_vectz00zzjas_classfilez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(16),
					CNST_TABLE_REF(19), BGl_JasTypez00zzjas_classfilez00, 19371L,
					BGl_proc2246z00zzjas_classfilez00, BGl_proc2245z00zzjas_classfilez00,
					BFALSE, BGl_proc2244z00zzjas_classfilez00, BFALSE, BgL_arg1894z00_798,
					BgL_arg1896z00_799);
			}
			{	/* Jas/classfile.scm 13 */
				obj_t BgL_arg1913z00_830;
				obj_t BgL_arg1914z00_831;

				{	/* Jas/classfile.scm 13 */
					obj_t BgL_v1358z00_845;

					BgL_v1358z00_845 = create_vector(2L);
					{	/* Jas/classfile.scm 13 */
						obj_t BgL_arg1920z00_846;

						BgL_arg1920z00_846 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(22),
							BGl_proc2248z00zzjas_classfilez00,
							BGl_proc2247z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_JasTypez00zzjas_classfilez00);
						VECTOR_SET(BgL_v1358z00_845, 0L, BgL_arg1920z00_846);
					}
					{	/* Jas/classfile.scm 13 */
						obj_t BgL_arg1927z00_856;

						BgL_arg1927z00_856 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(23),
							BGl_proc2250z00zzjas_classfilez00,
							BGl_proc2249z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1358z00_845, 1L, BgL_arg1927z00_856);
					}
					BgL_arg1913z00_830 = BgL_v1358z00_845;
				}
				{	/* Jas/classfile.scm 13 */
					obj_t BgL_v1359z00_866;

					BgL_v1359z00_866 = create_vector(0L);
					BgL_arg1914z00_831 = BgL_v1359z00_866;
				}
				BGl_JasFunz00zzjas_classfilez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(24),
					CNST_TABLE_REF(19), BGl_JasTypez00zzjas_classfilez00, 41514L,
					BGl_proc2253z00zzjas_classfilez00, BGl_proc2252z00zzjas_classfilez00,
					BFALSE, BGl_proc2251z00zzjas_classfilez00, BFALSE, BgL_arg1913z00_830,
					BgL_arg1914z00_831);
			}
			{	/* Jas/classfile.scm 15 */
				obj_t BgL_arg1935z00_873;
				obj_t BgL_arg1936z00_874;

				{	/* Jas/classfile.scm 15 */
					obj_t BgL_v1360z00_889;

					BgL_v1360z00_889 = create_vector(3L);
					{	/* Jas/classfile.scm 15 */
						obj_t BgL_arg1942z00_890;

						BgL_arg1942z00_890 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(25),
							BGl_proc2255z00zzjas_classfilez00,
							BGl_proc2254z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1360z00_889, 0L, BgL_arg1942z00_890);
					}
					{	/* Jas/classfile.scm 15 */
						obj_t BgL_arg1947z00_900;

						BgL_arg1947z00_900 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(26),
							BGl_proc2257z00zzjas_classfilez00,
							BGl_proc2256z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1360z00_889, 1L, BgL_arg1947z00_900);
					}
					{	/* Jas/classfile.scm 15 */
						obj_t BgL_arg1952z00_910;

						BgL_arg1952z00_910 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(27),
							BGl_proc2260z00zzjas_classfilez00,
							BGl_proc2259z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2258z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1360z00_889, 2L, BgL_arg1952z00_910);
					}
					BgL_arg1935z00_873 = BgL_v1360z00_889;
				}
				{	/* Jas/classfile.scm 15 */
					obj_t BgL_v1361z00_923;

					BgL_v1361z00_923 = create_vector(0L);
					BgL_arg1936z00_874 = BgL_v1361z00_923;
				}
				BGl_classez00zzjas_classfilez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(28),
					CNST_TABLE_REF(19), BGl_JasTypez00zzjas_classfilez00, 56172L,
					BGl_proc2263z00zzjas_classfilez00, BGl_proc2262z00zzjas_classfilez00,
					BFALSE, BGl_proc2261z00zzjas_classfilez00, BFALSE, BgL_arg1935z00_873,
					BgL_arg1936z00_874);
			}
			{	/* Jas/classfile.scm 20 */
				obj_t BgL_arg1962z00_930;
				obj_t BgL_arg1963z00_931;

				{	/* Jas/classfile.scm 20 */
					obj_t BgL_v1362z00_950;

					BgL_v1362z00_950 = create_vector(9L);
					{	/* Jas/classfile.scm 20 */
						obj_t BgL_arg1969z00_951;

						BgL_arg1969z00_951 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(25),
							BGl_proc2265z00zzjas_classfilez00,
							BGl_proc2264z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1362z00_950, 0L, BgL_arg1969z00_951);
					}
					{	/* Jas/classfile.scm 20 */
						obj_t BgL_arg1974z00_961;

						BgL_arg1974z00_961 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(26),
							BGl_proc2267z00zzjas_classfilez00,
							BGl_proc2266z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(15));
						VECTOR_SET(BgL_v1362z00_950, 1L, BgL_arg1974z00_961);
					}
					{	/* Jas/classfile.scm 20 */
						obj_t BgL_arg1979z00_971;

						BgL_arg1979z00_971 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(29),
							BGl_proc2269z00zzjas_classfilez00,
							BGl_proc2268z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1362z00_950, 2L, BgL_arg1979z00_971);
					}
					{	/* Jas/classfile.scm 20 */
						obj_t BgL_arg1984z00_981;

						BgL_arg1984z00_981 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(30),
							BGl_proc2271z00zzjas_classfilez00,
							BGl_proc2270z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1362z00_950, 3L, BgL_arg1984z00_981);
					}
					{	/* Jas/classfile.scm 20 */
						obj_t BgL_arg1989z00_991;

						BgL_arg1989z00_991 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(21),
							BGl_proc2274z00zzjas_classfilez00,
							BGl_proc2273z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2272z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1362z00_950, 4L, BgL_arg1989z00_991);
					}
					{	/* Jas/classfile.scm 20 */
						obj_t BgL_arg1996z00_1004;

						BgL_arg1996z00_1004 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(31),
							BGl_proc2277z00zzjas_classfilez00,
							BGl_proc2276z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2275z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1362z00_950, 5L, BgL_arg1996z00_1004);
					}
					{	/* Jas/classfile.scm 20 */
						obj_t BgL_arg2003z00_1017;

						BgL_arg2003z00_1017 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(32),
							BGl_proc2280z00zzjas_classfilez00,
							BGl_proc2279z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2278z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1362z00_950, 6L, BgL_arg2003z00_1017);
					}
					{	/* Jas/classfile.scm 20 */
						obj_t BgL_arg2011z00_1030;

						BgL_arg2011z00_1030 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(27),
							BGl_proc2283z00zzjas_classfilez00,
							BGl_proc2282z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2281z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1362z00_950, 7L, BgL_arg2011z00_1030);
					}
					{	/* Jas/classfile.scm 20 */
						obj_t BgL_arg2018z00_1043;

						BgL_arg2018z00_1043 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(33),
							BGl_proc2286z00zzjas_classfilez00,
							BGl_proc2285z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2284z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1362z00_950, 8L, BgL_arg2018z00_1043);
					}
					BgL_arg1962z00_930 = BgL_v1362z00_950;
				}
				{	/* Jas/classfile.scm 20 */
					obj_t BgL_v1363z00_1056;

					BgL_v1363z00_1056 = create_vector(0L);
					BgL_arg1963z00_931 = BgL_v1363z00_1056;
				}
				BGl_fieldzd2orzd2methodz00zzjas_classfilez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(34),
					CNST_TABLE_REF(19), BGl_objectz00zz__objectz00, 44068L,
					BGl_proc2289z00zzjas_classfilez00, BGl_proc2288z00zzjas_classfilez00,
					BFALSE, BGl_proc2287z00zzjas_classfilez00, BFALSE, BgL_arg1962z00_930,
					BgL_arg1963z00_931);
			}
			{	/* Jas/classfile.scm 31 */
				obj_t BgL_arg2029z00_1063;
				obj_t BgL_arg2030z00_1064;

				{	/* Jas/classfile.scm 31 */
					obj_t BgL_v1364z00_1083;

					BgL_v1364z00_1083 = create_vector(0L);
					BgL_arg2029z00_1063 = BgL_v1364z00_1083;
				}
				{	/* Jas/classfile.scm 31 */
					obj_t BgL_v1365z00_1084;

					BgL_v1365z00_1084 = create_vector(0L);
					BgL_arg2030z00_1064 = BgL_v1365z00_1084;
				}
				BGl_fieldz00zzjas_classfilez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(35),
					CNST_TABLE_REF(19), BGl_fieldzd2orzd2methodz00zzjas_classfilez00,
					63723L, BGl_proc2292z00zzjas_classfilez00,
					BGl_proc2291z00zzjas_classfilez00, BFALSE,
					BGl_proc2290z00zzjas_classfilez00, BFALSE, BgL_arg2029z00_1063,
					BgL_arg2030z00_1064);
			}
			{	/* Jas/classfile.scm 33 */
				obj_t BgL_arg2041z00_1091;
				obj_t BgL_arg2042z00_1092;

				{	/* Jas/classfile.scm 33 */
					obj_t BgL_v1366z00_1111;

					BgL_v1366z00_1111 = create_vector(0L);
					BgL_arg2041z00_1091 = BgL_v1366z00_1111;
				}
				{	/* Jas/classfile.scm 33 */
					obj_t BgL_v1367z00_1112;

					BgL_v1367z00_1112 = create_vector(0L);
					BgL_arg2042z00_1092 = BgL_v1367z00_1112;
				}
				BGl_methodz00zzjas_classfilez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(36),
					CNST_TABLE_REF(19), BGl_fieldzd2orzd2methodz00zzjas_classfilez00,
					4174L, BGl_proc2295z00zzjas_classfilez00,
					BGl_proc2294z00zzjas_classfilez00, BFALSE,
					BGl_proc2293z00zzjas_classfilez00, BFALSE, BgL_arg2041z00_1091,
					BgL_arg2042z00_1092);
			}
			{	/* Jas/classfile.scm 35 */
				obj_t BgL_arg2051z00_1119;
				obj_t BgL_arg2052z00_1120;

				{	/* Jas/classfile.scm 35 */
					obj_t BgL_v1368z00_1134;

					BgL_v1368z00_1134 = create_vector(4L);
					{	/* Jas/classfile.scm 35 */
						obj_t BgL_arg2059z00_1135;

						BgL_arg2059z00_1135 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(21),
							BGl_proc2297z00zzjas_classfilez00,
							BGl_proc2296z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1368z00_1134, 0L, BgL_arg2059z00_1135);
					}
					{	/* Jas/classfile.scm 35 */
						obj_t BgL_arg2064z00_1145;

						BgL_arg2064z00_1145 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(26),
							BGl_proc2299z00zzjas_classfilez00,
							BGl_proc2298z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1368z00_1134, 1L, BgL_arg2064z00_1145);
					}
					{	/* Jas/classfile.scm 35 */
						obj_t BgL_arg2070z00_1155;

						BgL_arg2070z00_1155 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(37),
							BGl_proc2301z00zzjas_classfilez00,
							BGl_proc2300z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1368z00_1134, 2L, BgL_arg2070z00_1155);
					}
					{	/* Jas/classfile.scm 35 */
						obj_t BgL_arg2077z00_1165;

						BgL_arg2077z00_1165 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(38),
							BGl_proc2303z00zzjas_classfilez00,
							BGl_proc2302z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1368z00_1134, 3L, BgL_arg2077z00_1165);
					}
					BgL_arg2051z00_1119 = BgL_v1368z00_1134;
				}
				{	/* Jas/classfile.scm 35 */
					obj_t BgL_v1369z00_1175;

					BgL_v1369z00_1175 = create_vector(0L);
					BgL_arg2052z00_1120 = BgL_v1369z00_1175;
				}
				BGl_attributez00zzjas_classfilez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(39),
					CNST_TABLE_REF(19), BGl_objectz00zz__objectz00, 48179L,
					BGl_proc2306z00zzjas_classfilez00, BGl_proc2305z00zzjas_classfilez00,
					BFALSE, BGl_proc2304z00zzjas_classfilez00, BFALSE,
					BgL_arg2051z00_1119, BgL_arg2052z00_1120);
			}
			{	/* Jas/classfile.scm 41 */
				obj_t BgL_arg2086z00_1182;
				obj_t BgL_arg2087z00_1183;

				{	/* Jas/classfile.scm 41 */
					obj_t BgL_v1370z00_1205;

					BgL_v1370z00_1205 = create_vector(12L);
					{	/* Jas/classfile.scm 41 */
						obj_t BgL_arg2093z00_1206;

						BgL_arg2093z00_1206 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(40),
							BGl_proc2309z00zzjas_classfilez00,
							BGl_proc2308z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2307z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1370z00_1205, 0L, BgL_arg2093z00_1206);
					}
					{	/* Jas/classfile.scm 41 */
						obj_t BgL_arg2100z00_1219;

						BgL_arg2100z00_1219 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(41),
							BGl_proc2312z00zzjas_classfilez00,
							BGl_proc2311z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2310z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1370z00_1205, 1L, BgL_arg2100z00_1219);
					}
					{	/* Jas/classfile.scm 41 */
						obj_t BgL_arg2107z00_1232;

						BgL_arg2107z00_1232 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(27),
							BGl_proc2315z00zzjas_classfilez00,
							BGl_proc2314z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2313z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1370z00_1205, 2L, BgL_arg2107z00_1232);
					}
					{	/* Jas/classfile.scm 41 */
						obj_t BgL_arg2114z00_1245;

						BgL_arg2114z00_1245 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(42),
							BGl_proc2318z00zzjas_classfilez00,
							BGl_proc2317z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2316z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1370z00_1205, 3L, BgL_arg2114z00_1245);
					}
					{	/* Jas/classfile.scm 41 */
						obj_t BgL_arg2121z00_1258;

						BgL_arg2121z00_1258 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(43),
							BGl_proc2321z00zzjas_classfilez00,
							BGl_proc2320z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2319z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1370z00_1205, 4L, BgL_arg2121z00_1258);
					}
					{	/* Jas/classfile.scm 41 */
						obj_t BgL_arg2129z00_1272;

						BgL_arg2129z00_1272 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(25),
							BGl_proc2324z00zzjas_classfilez00,
							BGl_proc2323z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2322z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1370z00_1205, 5L, BgL_arg2129z00_1272);
					}
					{	/* Jas/classfile.scm 41 */
						obj_t BgL_arg2136z00_1285;

						BgL_arg2136z00_1285 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(44),
							BGl_proc2327z00zzjas_classfilez00,
							BGl_proc2326z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2325z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1370z00_1205, 6L, BgL_arg2136z00_1285);
					}
					{	/* Jas/classfile.scm 41 */
						obj_t BgL_arg2143z00_1298;

						BgL_arg2143z00_1298 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(45),
							BGl_proc2330z00zzjas_classfilez00,
							BGl_proc2329z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2328z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1370z00_1205, 7L, BgL_arg2143z00_1298);
					}
					{	/* Jas/classfile.scm 41 */
						obj_t BgL_arg2150z00_1311;

						BgL_arg2150z00_1311 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(46),
							BGl_proc2333z00zzjas_classfilez00,
							BGl_proc2332z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2331z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1370z00_1205, 8L, BgL_arg2150z00_1311);
					}
					{	/* Jas/classfile.scm 41 */
						obj_t BgL_arg2158z00_1324;

						BgL_arg2158z00_1324 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(47),
							BGl_proc2336z00zzjas_classfilez00,
							BGl_proc2335z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2334z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1370z00_1205, 9L, BgL_arg2158z00_1324);
					}
					{	/* Jas/classfile.scm 41 */
						obj_t BgL_arg2165z00_1337;

						BgL_arg2165z00_1337 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(48),
							BGl_proc2339z00zzjas_classfilez00,
							BGl_proc2338z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2337z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1370z00_1205, 10L, BgL_arg2165z00_1337);
					}
					{	/* Jas/classfile.scm 41 */
						obj_t BgL_arg2172z00_1350;

						BgL_arg2172z00_1350 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(33),
							BGl_proc2342z00zzjas_classfilez00,
							BGl_proc2341z00zzjas_classfilez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2340z00zzjas_classfilez00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1370z00_1205, 11L, BgL_arg2172z00_1350);
					}
					BgL_arg2086z00_1182 = BgL_v1370z00_1205;
				}
				{	/* Jas/classfile.scm 41 */
					obj_t BgL_v1371z00_1363;

					BgL_v1371z00_1363 = create_vector(0L);
					BgL_arg2087z00_1183 = BgL_v1371z00_1363;
				}
				return (BGl_classfilez00zzjas_classfilez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(49),
						CNST_TABLE_REF(19), BGl_objectz00zz__objectz00, 18281L,
						BGl_proc2345z00zzjas_classfilez00,
						BGl_proc2344z00zzjas_classfilez00, BFALSE,
						BGl_proc2343z00zzjas_classfilez00, BFALSE, BgL_arg2086z00_1182,
						BgL_arg2087z00_1183), BUNSPEC);
			}
		}

	}



/* &<@anonymous:2092> */
	obj_t BGl_z62zc3z04anonymousza32092ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3002, obj_t BgL_new1137z00_3003)
	{
		{	/* Jas/classfile.scm 41 */
			{
				BgL_classfilez00_bglt BgL_auxz00_5419;

				((((BgL_classfilez00_bglt) COBJECT(
								((BgL_classfilez00_bglt) BgL_new1137z00_3003)))->
						BgL_currentzd2methodzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(((BgL_classfilez00_bglt)
									BgL_new1137z00_3003)))->BgL_globalsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(((BgL_classfilez00_bglt)
									BgL_new1137z00_3003)))->BgL_poolz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(((BgL_classfilez00_bglt)
									BgL_new1137z00_3003)))->BgL_poolzd2siza7ez75) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(((BgL_classfilez00_bglt)
									BgL_new1137z00_3003)))->BgL_pooledzd2nameszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(((BgL_classfilez00_bglt)
									BgL_new1137z00_3003)))->BgL_flagsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(((BgL_classfilez00_bglt)
									BgL_new1137z00_3003)))->BgL_mez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(((BgL_classfilez00_bglt)
									BgL_new1137z00_3003)))->BgL_superz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(((BgL_classfilez00_bglt)
									BgL_new1137z00_3003)))->BgL_interfacesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(((BgL_classfilez00_bglt)
									BgL_new1137z00_3003)))->BgL_fieldsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(((BgL_classfilez00_bglt)
									BgL_new1137z00_3003)))->BgL_methodsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(((BgL_classfilez00_bglt)
									BgL_new1137z00_3003)))->BgL_attributesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5419 = ((BgL_classfilez00_bglt) BgL_new1137z00_3003);
				return ((obj_t) BgL_auxz00_5419);
			}
		}

	}



/* &lambda2090 */
	BgL_classfilez00_bglt BGl_z62lambda2090z62zzjas_classfilez00(obj_t
		BgL_envz00_3004)
	{
		{	/* Jas/classfile.scm 41 */
			{	/* Jas/classfile.scm 41 */
				BgL_classfilez00_bglt BgL_new1136z00_3477;

				BgL_new1136z00_3477 =
					((BgL_classfilez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_classfilez00_bgl))));
				{	/* Jas/classfile.scm 41 */
					long BgL_arg2091z00_3478;

					BgL_arg2091z00_3478 =
						BGL_CLASS_NUM(BGl_classfilez00zzjas_classfilez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1136z00_3477), BgL_arg2091z00_3478);
				}
				return BgL_new1136z00_3477;
			}
		}

	}



/* &lambda2088 */
	BgL_classfilez00_bglt BGl_z62lambda2088z62zzjas_classfilez00(obj_t
		BgL_envz00_3005, obj_t BgL_currentzd2method1124zd2_3006,
		obj_t BgL_globals1125z00_3007, obj_t BgL_pool1126z00_3008,
		obj_t BgL_poolzd2siza7e1127z75_3009, obj_t BgL_pooledzd2names1128zd2_3010,
		obj_t BgL_flags1129z00_3011, obj_t BgL_me1130z00_3012,
		obj_t BgL_super1131z00_3013, obj_t BgL_interfaces1132z00_3014,
		obj_t BgL_fields1133z00_3015, obj_t BgL_methods1134z00_3016,
		obj_t BgL_attributes1135z00_3017)
	{
		{	/* Jas/classfile.scm 41 */
			{	/* Jas/classfile.scm 41 */
				BgL_classfilez00_bglt BgL_new1339z00_3479;

				{	/* Jas/classfile.scm 41 */
					BgL_classfilez00_bglt BgL_new1338z00_3480;

					BgL_new1338z00_3480 =
						((BgL_classfilez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_classfilez00_bgl))));
					{	/* Jas/classfile.scm 41 */
						long BgL_arg2089z00_3481;

						BgL_arg2089z00_3481 =
							BGL_CLASS_NUM(BGl_classfilez00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1338z00_3480), BgL_arg2089z00_3481);
					}
					BgL_new1339z00_3479 = BgL_new1338z00_3480;
				}
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1339z00_3479))->
						BgL_currentzd2methodzd2) =
					((obj_t) BgL_currentzd2method1124zd2_3006), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1339z00_3479))->
						BgL_globalsz00) = ((obj_t) BgL_globals1125z00_3007), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1339z00_3479))->BgL_poolz00) =
					((obj_t) BgL_pool1126z00_3008), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1339z00_3479))->
						BgL_poolzd2siza7ez75) =
					((obj_t) BgL_poolzd2siza7e1127z75_3009), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1339z00_3479))->
						BgL_pooledzd2nameszd2) =
					((obj_t) BgL_pooledzd2names1128zd2_3010), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1339z00_3479))->
						BgL_flagsz00) = ((obj_t) BgL_flags1129z00_3011), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1339z00_3479))->BgL_mez00) =
					((obj_t) BgL_me1130z00_3012), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1339z00_3479))->
						BgL_superz00) = ((obj_t) BgL_super1131z00_3013), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1339z00_3479))->
						BgL_interfacesz00) = ((obj_t) BgL_interfaces1132z00_3014), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1339z00_3479))->
						BgL_fieldsz00) = ((obj_t) BgL_fields1133z00_3015), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1339z00_3479))->
						BgL_methodsz00) = ((obj_t) BgL_methods1134z00_3016), BUNSPEC);
				((((BgL_classfilez00_bglt) COBJECT(BgL_new1339z00_3479))->
						BgL_attributesz00) = ((obj_t) BgL_attributes1135z00_3017), BUNSPEC);
				return BgL_new1339z00_3479;
			}
		}

	}



/* &<@anonymous:2178> */
	obj_t BGl_z62zc3z04anonymousza32178ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3018)
	{
		{	/* Jas/classfile.scm 41 */
			return BNIL;
		}

	}



/* &lambda2177 */
	obj_t BGl_z62lambda2177z62zzjas_classfilez00(obj_t BgL_envz00_3019,
		obj_t BgL_oz00_3020, obj_t BgL_vz00_3021)
	{
		{	/* Jas/classfile.scm 41 */
			return
				((((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_oz00_3020)))->BgL_attributesz00) =
				((obj_t) BgL_vz00_3021), BUNSPEC);
		}

	}



/* &lambda2176 */
	obj_t BGl_z62lambda2176z62zzjas_classfilez00(obj_t BgL_envz00_3022,
		obj_t BgL_oz00_3023)
	{
		{	/* Jas/classfile.scm 41 */
			return
				(((BgL_classfilez00_bglt) COBJECT(
						((BgL_classfilez00_bglt) BgL_oz00_3023)))->BgL_attributesz00);
		}

	}



/* &<@anonymous:2171> */
	obj_t BGl_z62zc3z04anonymousza32171ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3024)
	{
		{	/* Jas/classfile.scm 41 */
			return BNIL;
		}

	}



/* &lambda2170 */
	obj_t BGl_z62lambda2170z62zzjas_classfilez00(obj_t BgL_envz00_3025,
		obj_t BgL_oz00_3026, obj_t BgL_vz00_3027)
	{
		{	/* Jas/classfile.scm 41 */
			return
				((((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_oz00_3026)))->BgL_methodsz00) =
				((obj_t) BgL_vz00_3027), BUNSPEC);
		}

	}



/* &lambda2169 */
	obj_t BGl_z62lambda2169z62zzjas_classfilez00(obj_t BgL_envz00_3028,
		obj_t BgL_oz00_3029)
	{
		{	/* Jas/classfile.scm 41 */
			return
				(((BgL_classfilez00_bglt) COBJECT(
						((BgL_classfilez00_bglt) BgL_oz00_3029)))->BgL_methodsz00);
		}

	}



/* &<@anonymous:2164> */
	obj_t BGl_z62zc3z04anonymousza32164ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3030)
	{
		{	/* Jas/classfile.scm 41 */
			return BNIL;
		}

	}



/* &lambda2163 */
	obj_t BGl_z62lambda2163z62zzjas_classfilez00(obj_t BgL_envz00_3031,
		obj_t BgL_oz00_3032, obj_t BgL_vz00_3033)
	{
		{	/* Jas/classfile.scm 41 */
			return
				((((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_oz00_3032)))->BgL_fieldsz00) =
				((obj_t) BgL_vz00_3033), BUNSPEC);
		}

	}



/* &lambda2162 */
	obj_t BGl_z62lambda2162z62zzjas_classfilez00(obj_t BgL_envz00_3034,
		obj_t BgL_oz00_3035)
	{
		{	/* Jas/classfile.scm 41 */
			return
				(((BgL_classfilez00_bglt) COBJECT(
						((BgL_classfilez00_bglt) BgL_oz00_3035)))->BgL_fieldsz00);
		}

	}



/* &<@anonymous:2157> */
	obj_t BGl_z62zc3z04anonymousza32157ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3036)
	{
		{	/* Jas/classfile.scm 41 */
			return BNIL;
		}

	}



/* &lambda2156 */
	obj_t BGl_z62lambda2156z62zzjas_classfilez00(obj_t BgL_envz00_3037,
		obj_t BgL_oz00_3038, obj_t BgL_vz00_3039)
	{
		{	/* Jas/classfile.scm 41 */
			return
				((((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_oz00_3038)))->BgL_interfacesz00) =
				((obj_t) BgL_vz00_3039), BUNSPEC);
		}

	}



/* &lambda2155 */
	obj_t BGl_z62lambda2155z62zzjas_classfilez00(obj_t BgL_envz00_3040,
		obj_t BgL_oz00_3041)
	{
		{	/* Jas/classfile.scm 41 */
			return
				(((BgL_classfilez00_bglt) COBJECT(
						((BgL_classfilez00_bglt) BgL_oz00_3041)))->BgL_interfacesz00);
		}

	}



/* &<@anonymous:2149> */
	obj_t BGl_z62zc3z04anonymousza32149ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3042)
	{
		{	/* Jas/classfile.scm 41 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2148 */
	obj_t BGl_z62lambda2148z62zzjas_classfilez00(obj_t BgL_envz00_3043,
		obj_t BgL_oz00_3044, obj_t BgL_vz00_3045)
	{
		{	/* Jas/classfile.scm 41 */
			return
				((((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_oz00_3044)))->BgL_superz00) =
				((obj_t) BgL_vz00_3045), BUNSPEC);
		}

	}



/* &lambda2147 */
	obj_t BGl_z62lambda2147z62zzjas_classfilez00(obj_t BgL_envz00_3046,
		obj_t BgL_oz00_3047)
	{
		{	/* Jas/classfile.scm 41 */
			return
				(((BgL_classfilez00_bglt) COBJECT(
						((BgL_classfilez00_bglt) BgL_oz00_3047)))->BgL_superz00);
		}

	}



/* &<@anonymous:2142> */
	obj_t BGl_z62zc3z04anonymousza32142ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3048)
	{
		{	/* Jas/classfile.scm 41 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2141 */
	obj_t BGl_z62lambda2141z62zzjas_classfilez00(obj_t BgL_envz00_3049,
		obj_t BgL_oz00_3050, obj_t BgL_vz00_3051)
	{
		{	/* Jas/classfile.scm 41 */
			return
				((((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_oz00_3050)))->BgL_mez00) =
				((obj_t) BgL_vz00_3051), BUNSPEC);
		}

	}



/* &lambda2140 */
	obj_t BGl_z62lambda2140z62zzjas_classfilez00(obj_t BgL_envz00_3052,
		obj_t BgL_oz00_3053)
	{
		{	/* Jas/classfile.scm 41 */
			return
				(((BgL_classfilez00_bglt) COBJECT(
						((BgL_classfilez00_bglt) BgL_oz00_3053)))->BgL_mez00);
		}

	}



/* &<@anonymous:2135> */
	obj_t BGl_z62zc3z04anonymousza32135ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3054)
	{
		{	/* Jas/classfile.scm 41 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2134 */
	obj_t BGl_z62lambda2134z62zzjas_classfilez00(obj_t BgL_envz00_3055,
		obj_t BgL_oz00_3056, obj_t BgL_vz00_3057)
	{
		{	/* Jas/classfile.scm 41 */
			return
				((((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_oz00_3056)))->BgL_flagsz00) =
				((obj_t) BgL_vz00_3057), BUNSPEC);
		}

	}



/* &lambda2133 */
	obj_t BGl_z62lambda2133z62zzjas_classfilez00(obj_t BgL_envz00_3058,
		obj_t BgL_oz00_3059)
	{
		{	/* Jas/classfile.scm 41 */
			return
				(((BgL_classfilez00_bglt) COBJECT(
						((BgL_classfilez00_bglt) BgL_oz00_3059)))->BgL_flagsz00);
		}

	}



/* &<@anonymous:2127> */
	obj_t BGl_z62zc3z04anonymousza32127ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3060)
	{
		{	/* Jas/classfile.scm 41 */
			return BGl_makezd2hashtablezd2zz__hashz00(BNIL);
		}

	}



/* &lambda2126 */
	obj_t BGl_z62lambda2126z62zzjas_classfilez00(obj_t BgL_envz00_3061,
		obj_t BgL_oz00_3062, obj_t BgL_vz00_3063)
	{
		{	/* Jas/classfile.scm 41 */
			return
				((((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_oz00_3062)))->
					BgL_pooledzd2nameszd2) = ((obj_t) BgL_vz00_3063), BUNSPEC);
		}

	}



/* &lambda2125 */
	obj_t BGl_z62lambda2125z62zzjas_classfilez00(obj_t BgL_envz00_3064,
		obj_t BgL_oz00_3065)
	{
		{	/* Jas/classfile.scm 41 */
			return
				(((BgL_classfilez00_bglt) COBJECT(
						((BgL_classfilez00_bglt) BgL_oz00_3065)))->BgL_pooledzd2nameszd2);
		}

	}



/* &<@anonymous:2120> */
	obj_t BGl_z62zc3z04anonymousza32120ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3066)
	{
		{	/* Jas/classfile.scm 41 */
			return BINT(1L);
		}

	}



/* &lambda2119 */
	obj_t BGl_z62lambda2119z62zzjas_classfilez00(obj_t BgL_envz00_3067,
		obj_t BgL_oz00_3068, obj_t BgL_vz00_3069)
	{
		{	/* Jas/classfile.scm 41 */
			return
				((((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_oz00_3068)))->BgL_poolzd2siza7ez75) =
				((obj_t) BgL_vz00_3069), BUNSPEC);
		}

	}



/* &lambda2118 */
	obj_t BGl_z62lambda2118z62zzjas_classfilez00(obj_t BgL_envz00_3070,
		obj_t BgL_oz00_3071)
	{
		{	/* Jas/classfile.scm 41 */
			return
				(((BgL_classfilez00_bglt) COBJECT(
						((BgL_classfilez00_bglt) BgL_oz00_3071)))->BgL_poolzd2siza7ez75);
		}

	}



/* &<@anonymous:2113> */
	obj_t BGl_z62zc3z04anonymousza32113ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3072)
	{
		{	/* Jas/classfile.scm 41 */
			return BNIL;
		}

	}



/* &lambda2112 */
	obj_t BGl_z62lambda2112z62zzjas_classfilez00(obj_t BgL_envz00_3073,
		obj_t BgL_oz00_3074, obj_t BgL_vz00_3075)
	{
		{	/* Jas/classfile.scm 41 */
			return
				((((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_oz00_3074)))->BgL_poolz00) =
				((obj_t) BgL_vz00_3075), BUNSPEC);
		}

	}



/* &lambda2111 */
	obj_t BGl_z62lambda2111z62zzjas_classfilez00(obj_t BgL_envz00_3076,
		obj_t BgL_oz00_3077)
	{
		{	/* Jas/classfile.scm 41 */
			return
				(((BgL_classfilez00_bglt) COBJECT(
						((BgL_classfilez00_bglt) BgL_oz00_3077)))->BgL_poolz00);
		}

	}



/* &<@anonymous:2106> */
	obj_t BGl_z62zc3z04anonymousza32106ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3078)
	{
		{	/* Jas/classfile.scm 41 */
			return BNIL;
		}

	}



/* &lambda2105 */
	obj_t BGl_z62lambda2105z62zzjas_classfilez00(obj_t BgL_envz00_3079,
		obj_t BgL_oz00_3080, obj_t BgL_vz00_3081)
	{
		{	/* Jas/classfile.scm 41 */
			return
				((((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_oz00_3080)))->BgL_globalsz00) =
				((obj_t) BgL_vz00_3081), BUNSPEC);
		}

	}



/* &lambda2104 */
	obj_t BGl_z62lambda2104z62zzjas_classfilez00(obj_t BgL_envz00_3082,
		obj_t BgL_oz00_3083)
	{
		{	/* Jas/classfile.scm 41 */
			return
				(((BgL_classfilez00_bglt) COBJECT(
						((BgL_classfilez00_bglt) BgL_oz00_3083)))->BgL_globalsz00);
		}

	}



/* &<@anonymous:2099> */
	obj_t BGl_z62zc3z04anonymousza32099ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3084)
	{
		{	/* Jas/classfile.scm 41 */
			return CNST_TABLE_REF(50);
		}

	}



/* &lambda2098 */
	obj_t BGl_z62lambda2098z62zzjas_classfilez00(obj_t BgL_envz00_3085,
		obj_t BgL_oz00_3086, obj_t BgL_vz00_3087)
	{
		{	/* Jas/classfile.scm 41 */
			return
				((((BgL_classfilez00_bglt) COBJECT(
							((BgL_classfilez00_bglt) BgL_oz00_3086)))->
					BgL_currentzd2methodzd2) = ((obj_t) BgL_vz00_3087), BUNSPEC);
		}

	}



/* &lambda2097 */
	obj_t BGl_z62lambda2097z62zzjas_classfilez00(obj_t BgL_envz00_3088,
		obj_t BgL_oz00_3089)
	{
		{	/* Jas/classfile.scm 41 */
			return
				(((BgL_classfilez00_bglt) COBJECT(
						((BgL_classfilez00_bglt) BgL_oz00_3089)))->BgL_currentzd2methodzd2);
		}

	}



/* &<@anonymous:2058> */
	obj_t BGl_z62zc3z04anonymousza32058ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3090, obj_t BgL_new1122z00_3091)
	{
		{	/* Jas/classfile.scm 35 */
			{
				BgL_attributez00_bglt BgL_auxz00_5520;

				((((BgL_attributez00_bglt) COBJECT(
								((BgL_attributez00_bglt) BgL_new1122z00_3091)))->BgL_typez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_attributez00_bglt) COBJECT(((BgL_attributez00_bglt)
									BgL_new1122z00_3091)))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_attributez00_bglt) COBJECT(((BgL_attributez00_bglt)
									BgL_new1122z00_3091)))->BgL_siza7eza7) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_attributez00_bglt) COBJECT(((BgL_attributez00_bglt)
									BgL_new1122z00_3091)))->BgL_infoz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5520 = ((BgL_attributez00_bglt) BgL_new1122z00_3091);
				return ((obj_t) BgL_auxz00_5520);
			}
		}

	}



/* &lambda2056 */
	BgL_attributez00_bglt BGl_z62lambda2056z62zzjas_classfilez00(obj_t
		BgL_envz00_3092)
	{
		{	/* Jas/classfile.scm 35 */
			{	/* Jas/classfile.scm 35 */
				BgL_attributez00_bglt BgL_new1120z00_3507;

				BgL_new1120z00_3507 =
					((BgL_attributez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_attributez00_bgl))));
				{	/* Jas/classfile.scm 35 */
					long BgL_arg2057z00_3508;

					BgL_arg2057z00_3508 =
						BGL_CLASS_NUM(BGl_attributez00zzjas_classfilez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1120z00_3507), BgL_arg2057z00_3508);
				}
				return BgL_new1120z00_3507;
			}
		}

	}



/* &lambda2053 */
	BgL_attributez00_bglt BGl_z62lambda2053z62zzjas_classfilez00(obj_t
		BgL_envz00_3093, obj_t BgL_type1116z00_3094, obj_t BgL_name1117z00_3095,
		obj_t BgL_siza7e1118za7_3096, obj_t BgL_info1119z00_3097)
	{
		{	/* Jas/classfile.scm 35 */
			{	/* Jas/classfile.scm 35 */
				BgL_attributez00_bglt BgL_new1337z00_3509;

				{	/* Jas/classfile.scm 35 */
					BgL_attributez00_bglt BgL_new1336z00_3510;

					BgL_new1336z00_3510 =
						((BgL_attributez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_attributez00_bgl))));
					{	/* Jas/classfile.scm 35 */
						long BgL_arg2055z00_3511;

						BgL_arg2055z00_3511 =
							BGL_CLASS_NUM(BGl_attributez00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1336z00_3510), BgL_arg2055z00_3511);
					}
					BgL_new1337z00_3509 = BgL_new1336z00_3510;
				}
				((((BgL_attributez00_bglt) COBJECT(BgL_new1337z00_3509))->BgL_typez00) =
					((obj_t) BgL_type1116z00_3094), BUNSPEC);
				((((BgL_attributez00_bglt) COBJECT(BgL_new1337z00_3509))->BgL_namez00) =
					((obj_t) BgL_name1117z00_3095), BUNSPEC);
				((((BgL_attributez00_bglt) COBJECT(BgL_new1337z00_3509))->
						BgL_siza7eza7) = ((obj_t) BgL_siza7e1118za7_3096), BUNSPEC);
				((((BgL_attributez00_bglt) COBJECT(BgL_new1337z00_3509))->BgL_infoz00) =
					((obj_t) BgL_info1119z00_3097), BUNSPEC);
				return BgL_new1337z00_3509;
			}
		}

	}



/* &lambda2081 */
	obj_t BGl_z62lambda2081z62zzjas_classfilez00(obj_t BgL_envz00_3098,
		obj_t BgL_oz00_3099, obj_t BgL_vz00_3100)
	{
		{	/* Jas/classfile.scm 35 */
			return
				((((BgL_attributez00_bglt) COBJECT(
							((BgL_attributez00_bglt) BgL_oz00_3099)))->BgL_infoz00) =
				((obj_t) BgL_vz00_3100), BUNSPEC);
		}

	}



/* &lambda2080 */
	obj_t BGl_z62lambda2080z62zzjas_classfilez00(obj_t BgL_envz00_3101,
		obj_t BgL_oz00_3102)
	{
		{	/* Jas/classfile.scm 35 */
			return
				(((BgL_attributez00_bglt) COBJECT(
						((BgL_attributez00_bglt) BgL_oz00_3102)))->BgL_infoz00);
		}

	}



/* &lambda2076 */
	obj_t BGl_z62lambda2076z62zzjas_classfilez00(obj_t BgL_envz00_3103,
		obj_t BgL_oz00_3104, obj_t BgL_vz00_3105)
	{
		{	/* Jas/classfile.scm 35 */
			return
				((((BgL_attributez00_bglt) COBJECT(
							((BgL_attributez00_bglt) BgL_oz00_3104)))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_3105), BUNSPEC);
		}

	}



/* &lambda2075 */
	obj_t BGl_z62lambda2075z62zzjas_classfilez00(obj_t BgL_envz00_3106,
		obj_t BgL_oz00_3107)
	{
		{	/* Jas/classfile.scm 35 */
			return
				(((BgL_attributez00_bglt) COBJECT(
						((BgL_attributez00_bglt) BgL_oz00_3107)))->BgL_siza7eza7);
		}

	}



/* &lambda2069 */
	obj_t BGl_z62lambda2069z62zzjas_classfilez00(obj_t BgL_envz00_3108,
		obj_t BgL_oz00_3109, obj_t BgL_vz00_3110)
	{
		{	/* Jas/classfile.scm 35 */
			return
				((((BgL_attributez00_bglt) COBJECT(
							((BgL_attributez00_bglt) BgL_oz00_3109)))->BgL_namez00) =
				((obj_t) BgL_vz00_3110), BUNSPEC);
		}

	}



/* &lambda2068 */
	obj_t BGl_z62lambda2068z62zzjas_classfilez00(obj_t BgL_envz00_3111,
		obj_t BgL_oz00_3112)
	{
		{	/* Jas/classfile.scm 35 */
			return
				(((BgL_attributez00_bglt) COBJECT(
						((BgL_attributez00_bglt) BgL_oz00_3112)))->BgL_namez00);
		}

	}



/* &lambda2063 */
	obj_t BGl_z62lambda2063z62zzjas_classfilez00(obj_t BgL_envz00_3113,
		obj_t BgL_oz00_3114, obj_t BgL_vz00_3115)
	{
		{	/* Jas/classfile.scm 35 */
			return
				((((BgL_attributez00_bglt) COBJECT(
							((BgL_attributez00_bglt) BgL_oz00_3114)))->BgL_typez00) =
				((obj_t) BgL_vz00_3115), BUNSPEC);
		}

	}



/* &lambda2062 */
	obj_t BGl_z62lambda2062z62zzjas_classfilez00(obj_t BgL_envz00_3116,
		obj_t BgL_oz00_3117)
	{
		{	/* Jas/classfile.scm 35 */
			return
				(((BgL_attributez00_bglt) COBJECT(
						((BgL_attributez00_bglt) BgL_oz00_3117)))->BgL_typez00);
		}

	}



/* &<@anonymous:2047> */
	obj_t BGl_z62zc3z04anonymousza32047ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3118, obj_t BgL_new1114z00_3119)
	{
		{	/* Jas/classfile.scm 33 */
			{
				BgL_methodz00_bglt BgL_auxz00_5559;

				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
								((BgL_fieldzd2orzd2methodz00_bglt)
									((BgL_methodz00_bglt) BgL_new1114z00_3119))))->BgL_flagsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) ((BgL_methodz00_bglt)
										BgL_new1114z00_3119))))->BgL_namez00) =
					((obj_t) BGl_string2346z00zzjas_classfilez00), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) ((BgL_methodz00_bglt)
										BgL_new1114z00_3119))))->BgL_ownerz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) ((BgL_methodz00_bglt)
										BgL_new1114z00_3119))))->BgL_usertypez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) ((BgL_methodz00_bglt)
										BgL_new1114z00_3119))))->BgL_typez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) ((BgL_methodz00_bglt)
										BgL_new1114z00_3119))))->BgL_pnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) ((BgL_methodz00_bglt)
										BgL_new1114z00_3119))))->BgL_descriptorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) ((BgL_methodz00_bglt)
										BgL_new1114z00_3119))))->BgL_poolz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) ((BgL_methodz00_bglt)
										BgL_new1114z00_3119))))->BgL_attributesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5559 = ((BgL_methodz00_bglt) BgL_new1114z00_3119);
				return ((obj_t) BgL_auxz00_5559);
			}
		}

	}



/* &lambda2045 */
	BgL_methodz00_bglt BGl_z62lambda2045z62zzjas_classfilez00(obj_t
		BgL_envz00_3120)
	{
		{	/* Jas/classfile.scm 33 */
			{	/* Jas/classfile.scm 33 */
				BgL_methodz00_bglt BgL_new1113z00_3521;

				BgL_new1113z00_3521 =
					((BgL_methodz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_methodz00_bgl))));
				{	/* Jas/classfile.scm 33 */
					long BgL_arg2046z00_3522;

					BgL_arg2046z00_3522 = BGL_CLASS_NUM(BGl_methodz00zzjas_classfilez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1113z00_3521), BgL_arg2046z00_3522);
				}
				return BgL_new1113z00_3521;
			}
		}

	}



/* &lambda2043 */
	BgL_methodz00_bglt BGl_z62lambda2043z62zzjas_classfilez00(obj_t
		BgL_envz00_3121, obj_t BgL_flags1103z00_3122, obj_t BgL_name1104z00_3123,
		obj_t BgL_owner1105z00_3124, obj_t BgL_usertype1106z00_3125,
		obj_t BgL_type1107z00_3126, obj_t BgL_pname1108z00_3127,
		obj_t BgL_descriptor1109z00_3128, obj_t BgL_pool1110z00_3129,
		obj_t BgL_attributes1111z00_3130)
	{
		{	/* Jas/classfile.scm 33 */
			{	/* Jas/classfile.scm 33 */
				BgL_methodz00_bglt BgL_new1335z00_3524;

				{	/* Jas/classfile.scm 33 */
					BgL_methodz00_bglt BgL_new1334z00_3525;

					BgL_new1334z00_3525 =
						((BgL_methodz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_methodz00_bgl))));
					{	/* Jas/classfile.scm 33 */
						long BgL_arg2044z00_3526;

						BgL_arg2044z00_3526 =
							BGL_CLASS_NUM(BGl_methodz00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1334z00_3525), BgL_arg2044z00_3526);
					}
					BgL_new1335z00_3524 = BgL_new1334z00_3525;
				}
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
								((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1335z00_3524)))->
						BgL_flagsz00) = ((obj_t) BgL_flags1103z00_3122), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1335z00_3524)))->BgL_namez00) =
					((obj_t) ((obj_t) BgL_name1104z00_3123)), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1335z00_3524)))->BgL_ownerz00) =
					((obj_t) BgL_owner1105z00_3124), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1335z00_3524)))->BgL_usertypez00) =
					((obj_t) BgL_usertype1106z00_3125), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1335z00_3524)))->BgL_typez00) =
					((obj_t) BgL_type1107z00_3126), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1335z00_3524)))->BgL_pnamez00) =
					((obj_t) BgL_pname1108z00_3127), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1335z00_3524)))->BgL_descriptorz00) =
					((obj_t) BgL_descriptor1109z00_3128), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1335z00_3524)))->BgL_poolz00) =
					((obj_t) BgL_pool1110z00_3129), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1335z00_3524)))->BgL_attributesz00) =
					((obj_t) BgL_attributes1111z00_3130), BUNSPEC);
				return BgL_new1335z00_3524;
			}
		}

	}



/* &<@anonymous:2037> */
	obj_t BGl_z62zc3z04anonymousza32037ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3131, obj_t BgL_new1101z00_3132)
	{
		{	/* Jas/classfile.scm 31 */
			{
				BgL_fieldz00_bglt BgL_auxz00_5616;

				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
								((BgL_fieldzd2orzd2methodz00_bglt)
									((BgL_fieldz00_bglt) BgL_new1101z00_3132))))->BgL_flagsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) ((BgL_fieldz00_bglt)
										BgL_new1101z00_3132))))->BgL_namez00) =
					((obj_t) BGl_string2346z00zzjas_classfilez00), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) ((BgL_fieldz00_bglt)
										BgL_new1101z00_3132))))->BgL_ownerz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) ((BgL_fieldz00_bglt)
										BgL_new1101z00_3132))))->BgL_usertypez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) ((BgL_fieldz00_bglt)
										BgL_new1101z00_3132))))->BgL_typez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) ((BgL_fieldz00_bglt)
										BgL_new1101z00_3132))))->BgL_pnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) ((BgL_fieldz00_bglt)
										BgL_new1101z00_3132))))->BgL_descriptorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) ((BgL_fieldz00_bglt)
										BgL_new1101z00_3132))))->BgL_poolz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) ((BgL_fieldz00_bglt)
										BgL_new1101z00_3132))))->BgL_attributesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5616 = ((BgL_fieldz00_bglt) BgL_new1101z00_3132);
				return ((obj_t) BgL_auxz00_5616);
			}
		}

	}



/* &lambda2034 */
	BgL_fieldz00_bglt BGl_z62lambda2034z62zzjas_classfilez00(obj_t
		BgL_envz00_3133)
	{
		{	/* Jas/classfile.scm 31 */
			{	/* Jas/classfile.scm 31 */
				BgL_fieldz00_bglt BgL_new1100z00_3528;

				BgL_new1100z00_3528 =
					((BgL_fieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_fieldz00_bgl))));
				{	/* Jas/classfile.scm 31 */
					long BgL_arg2036z00_3529;

					BgL_arg2036z00_3529 = BGL_CLASS_NUM(BGl_fieldz00zzjas_classfilez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1100z00_3528), BgL_arg2036z00_3529);
				}
				return BgL_new1100z00_3528;
			}
		}

	}



/* &lambda2031 */
	BgL_fieldz00_bglt BGl_z62lambda2031z62zzjas_classfilez00(obj_t
		BgL_envz00_3134, obj_t BgL_flags1091z00_3135, obj_t BgL_name1092z00_3136,
		obj_t BgL_owner1093z00_3137, obj_t BgL_usertype1094z00_3138,
		obj_t BgL_type1095z00_3139, obj_t BgL_pname1096z00_3140,
		obj_t BgL_descriptor1097z00_3141, obj_t BgL_pool1098z00_3142,
		obj_t BgL_attributes1099z00_3143)
	{
		{	/* Jas/classfile.scm 31 */
			{	/* Jas/classfile.scm 31 */
				BgL_fieldz00_bglt BgL_new1333z00_3531;

				{	/* Jas/classfile.scm 31 */
					BgL_fieldz00_bglt BgL_new1332z00_3532;

					BgL_new1332z00_3532 =
						((BgL_fieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_fieldz00_bgl))));
					{	/* Jas/classfile.scm 31 */
						long BgL_arg2033z00_3533;

						BgL_arg2033z00_3533 = BGL_CLASS_NUM(BGl_fieldz00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1332z00_3532), BgL_arg2033z00_3533);
					}
					BgL_new1333z00_3531 = BgL_new1332z00_3532;
				}
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
								((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1333z00_3531)))->
						BgL_flagsz00) = ((obj_t) BgL_flags1091z00_3135), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1333z00_3531)))->BgL_namez00) =
					((obj_t) ((obj_t) BgL_name1092z00_3136)), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1333z00_3531)))->BgL_ownerz00) =
					((obj_t) BgL_owner1093z00_3137), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1333z00_3531)))->BgL_usertypez00) =
					((obj_t) BgL_usertype1094z00_3138), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1333z00_3531)))->BgL_typez00) =
					((obj_t) BgL_type1095z00_3139), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1333z00_3531)))->BgL_pnamez00) =
					((obj_t) BgL_pname1096z00_3140), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1333z00_3531)))->BgL_descriptorz00) =
					((obj_t) BgL_descriptor1097z00_3141), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1333z00_3531)))->BgL_poolz00) =
					((obj_t) BgL_pool1098z00_3142), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1333z00_3531)))->BgL_attributesz00) =
					((obj_t) BgL_attributes1099z00_3143), BUNSPEC);
				return BgL_new1333z00_3531;
			}
		}

	}



/* &<@anonymous:1968> */
	obj_t BGl_z62zc3z04anonymousza31968ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3144, obj_t BgL_new1089z00_3145)
	{
		{	/* Jas/classfile.scm 20 */
			{
				BgL_fieldzd2orzd2methodz00_bglt BgL_auxz00_5673;

				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
								((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1089z00_3145)))->
						BgL_flagsz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1089z00_3145)))->BgL_namez00) =
					((obj_t) BGl_string2346z00zzjas_classfilez00), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1089z00_3145)))->BgL_ownerz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1089z00_3145)))->BgL_usertypez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1089z00_3145)))->BgL_typez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1089z00_3145)))->BgL_pnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1089z00_3145)))->BgL_descriptorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1089z00_3145)))->BgL_poolz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt)
							COBJECT(((BgL_fieldzd2orzd2methodz00_bglt)
									BgL_new1089z00_3145)))->BgL_attributesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5673 =
					((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1089z00_3145);
				return ((obj_t) BgL_auxz00_5673);
			}
		}

	}



/* &lambda1966 */
	BgL_fieldzd2orzd2methodz00_bglt BGl_z62lambda1966z62zzjas_classfilez00(obj_t
		BgL_envz00_3146)
	{
		{	/* Jas/classfile.scm 20 */
			{	/* Jas/classfile.scm 20 */
				BgL_fieldzd2orzd2methodz00_bglt BgL_new1088z00_3535;

				BgL_new1088z00_3535 =
					((BgL_fieldzd2orzd2methodz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_fieldzd2orzd2methodz00_bgl))));
				{	/* Jas/classfile.scm 20 */
					long BgL_arg1967z00_3536;

					BgL_arg1967z00_3536 =
						BGL_CLASS_NUM(BGl_fieldzd2orzd2methodz00zzjas_classfilez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1088z00_3535), BgL_arg1967z00_3536);
				}
				return BgL_new1088z00_3535;
			}
		}

	}



/* &lambda1964 */
	BgL_fieldzd2orzd2methodz00_bglt BGl_z62lambda1964z62zzjas_classfilez00(obj_t
		BgL_envz00_3147, obj_t BgL_flags1079z00_3148, obj_t BgL_name1080z00_3149,
		obj_t BgL_owner1081z00_3150, obj_t BgL_usertype1082z00_3151,
		obj_t BgL_type1083z00_3152, obj_t BgL_pname1084z00_3153,
		obj_t BgL_descriptor1085z00_3154, obj_t BgL_pool1086z00_3155,
		obj_t BgL_attributes1087z00_3156)
	{
		{	/* Jas/classfile.scm 20 */
			{	/* Jas/classfile.scm 20 */
				BgL_fieldzd2orzd2methodz00_bglt BgL_new1331z00_3538;

				{	/* Jas/classfile.scm 20 */
					BgL_fieldzd2orzd2methodz00_bglt BgL_new1330z00_3539;

					BgL_new1330z00_3539 =
						((BgL_fieldzd2orzd2methodz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_fieldzd2orzd2methodz00_bgl))));
					{	/* Jas/classfile.scm 20 */
						long BgL_arg1965z00_3540;

						BgL_arg1965z00_3540 =
							BGL_CLASS_NUM(BGl_fieldzd2orzd2methodz00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1330z00_3539), BgL_arg1965z00_3540);
					}
					BgL_new1331z00_3538 = BgL_new1330z00_3539;
				}
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1331z00_3538))->
						BgL_flagsz00) = ((obj_t) BgL_flags1079z00_3148), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1331z00_3538))->
						BgL_namez00) = ((obj_t) ((obj_t) BgL_name1080z00_3149)), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1331z00_3538))->
						BgL_ownerz00) = ((obj_t) BgL_owner1081z00_3150), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1331z00_3538))->
						BgL_usertypez00) = ((obj_t) BgL_usertype1082z00_3151), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1331z00_3538))->
						BgL_typez00) = ((obj_t) BgL_type1083z00_3152), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1331z00_3538))->
						BgL_pnamez00) = ((obj_t) BgL_pname1084z00_3153), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1331z00_3538))->
						BgL_descriptorz00) = ((obj_t) BgL_descriptor1085z00_3154), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1331z00_3538))->
						BgL_poolz00) = ((obj_t) BgL_pool1086z00_3155), BUNSPEC);
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(BgL_new1331z00_3538))->
						BgL_attributesz00) = ((obj_t) BgL_attributes1087z00_3156), BUNSPEC);
				return BgL_new1331z00_3538;
			}
		}

	}



/* &<@anonymous:2024> */
	obj_t BGl_z62zc3z04anonymousza32024ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3157)
	{
		{	/* Jas/classfile.scm 20 */
			return BNIL;
		}

	}



/* &lambda2023 */
	obj_t BGl_z62lambda2023z62zzjas_classfilez00(obj_t BgL_envz00_3158,
		obj_t BgL_oz00_3159, obj_t BgL_vz00_3160)
	{
		{	/* Jas/classfile.scm 20 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3159)))->
					BgL_attributesz00) = ((obj_t) BgL_vz00_3160), BUNSPEC);
		}

	}



/* &lambda2022 */
	obj_t BGl_z62lambda2022z62zzjas_classfilez00(obj_t BgL_envz00_3161,
		obj_t BgL_oz00_3162)
	{
		{	/* Jas/classfile.scm 20 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3162)))->
				BgL_attributesz00);
		}

	}



/* &<@anonymous:2017> */
	obj_t BGl_z62zc3z04anonymousza32017ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3163)
	{
		{	/* Jas/classfile.scm 20 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2016 */
	obj_t BGl_z62lambda2016z62zzjas_classfilez00(obj_t BgL_envz00_3164,
		obj_t BgL_oz00_3165, obj_t BgL_vz00_3166)
	{
		{	/* Jas/classfile.scm 20 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3165)))->
					BgL_poolz00) = ((obj_t) BgL_vz00_3166), BUNSPEC);
		}

	}



/* &lambda2015 */
	obj_t BGl_z62lambda2015z62zzjas_classfilez00(obj_t BgL_envz00_3167,
		obj_t BgL_oz00_3168)
	{
		{	/* Jas/classfile.scm 20 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3168)))->BgL_poolz00);
		}

	}



/* &<@anonymous:2010> */
	obj_t BGl_z62zc3z04anonymousza32010ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3169)
	{
		{	/* Jas/classfile.scm 20 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2009 */
	obj_t BGl_z62lambda2009z62zzjas_classfilez00(obj_t BgL_envz00_3170,
		obj_t BgL_oz00_3171, obj_t BgL_vz00_3172)
	{
		{	/* Jas/classfile.scm 20 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3171)))->
					BgL_descriptorz00) = ((obj_t) BgL_vz00_3172), BUNSPEC);
		}

	}



/* &lambda2008 */
	obj_t BGl_z62lambda2008z62zzjas_classfilez00(obj_t BgL_envz00_3173,
		obj_t BgL_oz00_3174)
	{
		{	/* Jas/classfile.scm 20 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3174)))->
				BgL_descriptorz00);
		}

	}



/* &<@anonymous:2002> */
	obj_t BGl_z62zc3z04anonymousza32002ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3175)
	{
		{	/* Jas/classfile.scm 20 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2001 */
	obj_t BGl_z62lambda2001z62zzjas_classfilez00(obj_t BgL_envz00_3176,
		obj_t BgL_oz00_3177, obj_t BgL_vz00_3178)
	{
		{	/* Jas/classfile.scm 20 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3177)))->
					BgL_pnamez00) = ((obj_t) BgL_vz00_3178), BUNSPEC);
		}

	}



/* &lambda2000 */
	obj_t BGl_z62lambda2000z62zzjas_classfilez00(obj_t BgL_envz00_3179,
		obj_t BgL_oz00_3180)
	{
		{	/* Jas/classfile.scm 20 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3180)))->BgL_pnamez00);
		}

	}



/* &<@anonymous:1995> */
	obj_t BGl_z62zc3z04anonymousza31995ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3181)
	{
		{	/* Jas/classfile.scm 20 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1994 */
	obj_t BGl_z62lambda1994z62zzjas_classfilez00(obj_t BgL_envz00_3182,
		obj_t BgL_oz00_3183, obj_t BgL_vz00_3184)
	{
		{	/* Jas/classfile.scm 20 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3183)))->
					BgL_typez00) = ((obj_t) BgL_vz00_3184), BUNSPEC);
		}

	}



/* &lambda1993 */
	obj_t BGl_z62lambda1993z62zzjas_classfilez00(obj_t BgL_envz00_3185,
		obj_t BgL_oz00_3186)
	{
		{	/* Jas/classfile.scm 20 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3186)))->BgL_typez00);
		}

	}



/* &lambda1988 */
	obj_t BGl_z62lambda1988z62zzjas_classfilez00(obj_t BgL_envz00_3187,
		obj_t BgL_oz00_3188, obj_t BgL_vz00_3189)
	{
		{	/* Jas/classfile.scm 20 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3188)))->
					BgL_usertypez00) = ((obj_t) BgL_vz00_3189), BUNSPEC);
		}

	}



/* &lambda1987 */
	obj_t BGl_z62lambda1987z62zzjas_classfilez00(obj_t BgL_envz00_3190,
		obj_t BgL_oz00_3191)
	{
		{	/* Jas/classfile.scm 20 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3191)))->
				BgL_usertypez00);
		}

	}



/* &lambda1983 */
	obj_t BGl_z62lambda1983z62zzjas_classfilez00(obj_t BgL_envz00_3192,
		obj_t BgL_oz00_3193, obj_t BgL_vz00_3194)
	{
		{	/* Jas/classfile.scm 20 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3193)))->
					BgL_ownerz00) = ((obj_t) BgL_vz00_3194), BUNSPEC);
		}

	}



/* &lambda1982 */
	obj_t BGl_z62lambda1982z62zzjas_classfilez00(obj_t BgL_envz00_3195,
		obj_t BgL_oz00_3196)
	{
		{	/* Jas/classfile.scm 20 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3196)))->BgL_ownerz00);
		}

	}



/* &lambda1978 */
	obj_t BGl_z62lambda1978z62zzjas_classfilez00(obj_t BgL_envz00_3197,
		obj_t BgL_oz00_3198, obj_t BgL_vz00_3199)
	{
		{	/* Jas/classfile.scm 20 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3198)))->
					BgL_namez00) = ((obj_t) ((obj_t) BgL_vz00_3199)), BUNSPEC);
		}

	}



/* &lambda1977 */
	obj_t BGl_z62lambda1977z62zzjas_classfilez00(obj_t BgL_envz00_3200,
		obj_t BgL_oz00_3201)
	{
		{	/* Jas/classfile.scm 20 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3201)))->BgL_namez00);
		}

	}



/* &lambda1973 */
	obj_t BGl_z62lambda1973z62zzjas_classfilez00(obj_t BgL_envz00_3202,
		obj_t BgL_oz00_3203, obj_t BgL_vz00_3204)
	{
		{	/* Jas/classfile.scm 20 */
			return
				((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
							((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3203)))->
					BgL_flagsz00) = ((obj_t) BgL_vz00_3204), BUNSPEC);
		}

	}



/* &lambda1972 */
	obj_t BGl_z62lambda1972z62zzjas_classfilez00(obj_t BgL_envz00_3205,
		obj_t BgL_oz00_3206)
	{
		{	/* Jas/classfile.scm 20 */
			return
				(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
						((BgL_fieldzd2orzd2methodz00_bglt) BgL_oz00_3206)))->BgL_flagsz00);
		}

	}



/* &<@anonymous:1941> */
	obj_t BGl_z62zc3z04anonymousza31941ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3207, obj_t BgL_new1077z00_3208)
	{
		{	/* Jas/classfile.scm 15 */
			{
				BgL_classez00_bglt BgL_auxz00_5753;

				((((BgL_jastypez00_bglt) COBJECT(
								((BgL_jastypez00_bglt)
									((BgL_classez00_bglt) BgL_new1077z00_3208))))->BgL_codez00) =
					((obj_t) BGl_string2346z00zzjas_classfilez00), BUNSPEC);
				((((BgL_jastypez00_bglt)
							COBJECT(((BgL_jastypez00_bglt) ((BgL_classez00_bglt)
										BgL_new1077z00_3208))))->BgL_vectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_classez00_bglt) COBJECT(((BgL_classez00_bglt)
									BgL_new1077z00_3208)))->BgL_flagsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_classez00_bglt) COBJECT(((BgL_classez00_bglt)
									BgL_new1077z00_3208)))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_classez00_bglt) COBJECT(((BgL_classez00_bglt)
									BgL_new1077z00_3208)))->BgL_poolz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5753 = ((BgL_classez00_bglt) BgL_new1077z00_3208);
				return ((obj_t) BgL_auxz00_5753);
			}
		}

	}



/* &lambda1939 */
	BgL_classez00_bglt BGl_z62lambda1939z62zzjas_classfilez00(obj_t
		BgL_envz00_3209)
	{
		{	/* Jas/classfile.scm 15 */
			{	/* Jas/classfile.scm 15 */
				BgL_classez00_bglt BgL_new1076z00_3561;

				BgL_new1076z00_3561 =
					((BgL_classez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_classez00_bgl))));
				{	/* Jas/classfile.scm 15 */
					long BgL_arg1940z00_3562;

					BgL_arg1940z00_3562 = BGL_CLASS_NUM(BGl_classez00zzjas_classfilez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1076z00_3561), BgL_arg1940z00_3562);
				}
				return BgL_new1076z00_3561;
			}
		}

	}



/* &lambda1937 */
	BgL_classez00_bglt BGl_z62lambda1937z62zzjas_classfilez00(obj_t
		BgL_envz00_3210, obj_t BgL_code1071z00_3211, obj_t BgL_vect1072z00_3212,
		obj_t BgL_flags1073z00_3213, obj_t BgL_name1074z00_3214,
		obj_t BgL_pool1075z00_3215)
	{
		{	/* Jas/classfile.scm 15 */
			{	/* Jas/classfile.scm 15 */
				BgL_classez00_bglt BgL_new1329z00_3564;

				{	/* Jas/classfile.scm 15 */
					BgL_classez00_bglt BgL_new1328z00_3565;

					BgL_new1328z00_3565 =
						((BgL_classez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_classez00_bgl))));
					{	/* Jas/classfile.scm 15 */
						long BgL_arg1938z00_3566;

						BgL_arg1938z00_3566 =
							BGL_CLASS_NUM(BGl_classez00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1328z00_3565), BgL_arg1938z00_3566);
					}
					BgL_new1329z00_3564 = BgL_new1328z00_3565;
				}
				((((BgL_jastypez00_bglt) COBJECT(
								((BgL_jastypez00_bglt) BgL_new1329z00_3564)))->BgL_codez00) =
					((obj_t) ((obj_t) BgL_code1071z00_3211)), BUNSPEC);
				((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
									BgL_new1329z00_3564)))->BgL_vectz00) =
					((obj_t) BgL_vect1072z00_3212), BUNSPEC);
				((((BgL_classez00_bglt) COBJECT(BgL_new1329z00_3564))->BgL_flagsz00) =
					((obj_t) BgL_flags1073z00_3213), BUNSPEC);
				((((BgL_classez00_bglt) COBJECT(BgL_new1329z00_3564))->BgL_namez00) =
					((obj_t) BgL_name1074z00_3214), BUNSPEC);
				((((BgL_classez00_bglt) COBJECT(BgL_new1329z00_3564))->BgL_poolz00) =
					((obj_t) BgL_pool1075z00_3215), BUNSPEC);
				return BgL_new1329z00_3564;
			}
		}

	}



/* &<@anonymous:1958> */
	obj_t BGl_z62zc3z04anonymousza31958ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3216)
	{
		{	/* Jas/classfile.scm 15 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1957 */
	obj_t BGl_z62lambda1957z62zzjas_classfilez00(obj_t BgL_envz00_3217,
		obj_t BgL_oz00_3218, obj_t BgL_vz00_3219)
	{
		{	/* Jas/classfile.scm 15 */
			return
				((((BgL_classez00_bglt) COBJECT(
							((BgL_classez00_bglt) BgL_oz00_3218)))->BgL_poolz00) =
				((obj_t) BgL_vz00_3219), BUNSPEC);
		}

	}



/* &lambda1956 */
	obj_t BGl_z62lambda1956z62zzjas_classfilez00(obj_t BgL_envz00_3220,
		obj_t BgL_oz00_3221)
	{
		{	/* Jas/classfile.scm 15 */
			return
				(((BgL_classez00_bglt) COBJECT(
						((BgL_classez00_bglt) BgL_oz00_3221)))->BgL_poolz00);
		}

	}



/* &lambda1951 */
	obj_t BGl_z62lambda1951z62zzjas_classfilez00(obj_t BgL_envz00_3222,
		obj_t BgL_oz00_3223, obj_t BgL_vz00_3224)
	{
		{	/* Jas/classfile.scm 15 */
			return
				((((BgL_classez00_bglt) COBJECT(
							((BgL_classez00_bglt) BgL_oz00_3223)))->BgL_namez00) =
				((obj_t) BgL_vz00_3224), BUNSPEC);
		}

	}



/* &lambda1950 */
	obj_t BGl_z62lambda1950z62zzjas_classfilez00(obj_t BgL_envz00_3225,
		obj_t BgL_oz00_3226)
	{
		{	/* Jas/classfile.scm 15 */
			return
				(((BgL_classez00_bglt) COBJECT(
						((BgL_classez00_bglt) BgL_oz00_3226)))->BgL_namez00);
		}

	}



/* &lambda1946 */
	obj_t BGl_z62lambda1946z62zzjas_classfilez00(obj_t BgL_envz00_3227,
		obj_t BgL_oz00_3228, obj_t BgL_vz00_3229)
	{
		{	/* Jas/classfile.scm 15 */
			return
				((((BgL_classez00_bglt) COBJECT(
							((BgL_classez00_bglt) BgL_oz00_3228)))->BgL_flagsz00) =
				((obj_t) BgL_vz00_3229), BUNSPEC);
		}

	}



/* &lambda1945 */
	obj_t BGl_z62lambda1945z62zzjas_classfilez00(obj_t BgL_envz00_3230,
		obj_t BgL_oz00_3231)
	{
		{	/* Jas/classfile.scm 15 */
			return
				(((BgL_classez00_bglt) COBJECT(
						((BgL_classez00_bglt) BgL_oz00_3231)))->BgL_flagsz00);
		}

	}



/* &<@anonymous:1919> */
	obj_t BGl_z62zc3z04anonymousza31919ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3232, obj_t BgL_new1069z00_3233)
	{
		{	/* Jas/classfile.scm 13 */
			{
				BgL_jasfunz00_bglt BgL_auxz00_5797;

				((((BgL_jastypez00_bglt) COBJECT(
								((BgL_jastypez00_bglt)
									((BgL_jasfunz00_bglt) BgL_new1069z00_3233))))->BgL_codez00) =
					((obj_t) BGl_string2346z00zzjas_classfilez00), BUNSPEC);
				((((BgL_jastypez00_bglt)
							COBJECT(((BgL_jastypez00_bglt) ((BgL_jasfunz00_bglt)
										BgL_new1069z00_3233))))->BgL_vectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_jastypez00_bglt BgL_auxz00_5804;

					{	/* Jas/classfile.scm 13 */
						obj_t BgL_classz00_3574;

						BgL_classz00_3574 = BGl_JasTypez00zzjas_classfilez00;
						{	/* Jas/classfile.scm 13 */
							obj_t BgL__ortest_1117z00_3575;

							BgL__ortest_1117z00_3575 = BGL_CLASS_NIL(BgL_classz00_3574);
							if (CBOOL(BgL__ortest_1117z00_3575))
								{	/* Jas/classfile.scm 13 */
									BgL_auxz00_5804 =
										((BgL_jastypez00_bglt) BgL__ortest_1117z00_3575);
								}
							else
								{	/* Jas/classfile.scm 13 */
									BgL_auxz00_5804 =
										((BgL_jastypez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3574));
								}
						}
					}
					((((BgL_jasfunz00_bglt) COBJECT(
									((BgL_jasfunz00_bglt) BgL_new1069z00_3233)))->BgL_tretz00) =
						((BgL_jastypez00_bglt) BgL_auxz00_5804), BUNSPEC);
				}
				((((BgL_jasfunz00_bglt) COBJECT(
								((BgL_jasfunz00_bglt) BgL_new1069z00_3233)))->BgL_targsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5797 = ((BgL_jasfunz00_bglt) BgL_new1069z00_3233);
				return ((obj_t) BgL_auxz00_5797);
			}
		}

	}



/* &lambda1917 */
	BgL_jasfunz00_bglt BGl_z62lambda1917z62zzjas_classfilez00(obj_t
		BgL_envz00_3234)
	{
		{	/* Jas/classfile.scm 13 */
			{	/* Jas/classfile.scm 13 */
				BgL_jasfunz00_bglt BgL_new1068z00_3576;

				BgL_new1068z00_3576 =
					((BgL_jasfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_jasfunz00_bgl))));
				{	/* Jas/classfile.scm 13 */
					long BgL_arg1918z00_3577;

					BgL_arg1918z00_3577 = BGL_CLASS_NUM(BGl_JasFunz00zzjas_classfilez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1068z00_3576), BgL_arg1918z00_3577);
				}
				return BgL_new1068z00_3576;
			}
		}

	}



/* &lambda1915 */
	BgL_jasfunz00_bglt BGl_z62lambda1915z62zzjas_classfilez00(obj_t
		BgL_envz00_3235, obj_t BgL_code1064z00_3236, obj_t BgL_vect1065z00_3237,
		obj_t BgL_tret1066z00_3238, obj_t BgL_targs1067z00_3239)
	{
		{	/* Jas/classfile.scm 13 */
			{	/* Jas/classfile.scm 13 */
				BgL_jasfunz00_bglt BgL_new1327z00_3580;

				{	/* Jas/classfile.scm 13 */
					BgL_jasfunz00_bglt BgL_new1326z00_3581;

					BgL_new1326z00_3581 =
						((BgL_jasfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_jasfunz00_bgl))));
					{	/* Jas/classfile.scm 13 */
						long BgL_arg1916z00_3582;

						BgL_arg1916z00_3582 =
							BGL_CLASS_NUM(BGl_JasFunz00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1326z00_3581), BgL_arg1916z00_3582);
					}
					BgL_new1327z00_3580 = BgL_new1326z00_3581;
				}
				((((BgL_jastypez00_bglt) COBJECT(
								((BgL_jastypez00_bglt) BgL_new1327z00_3580)))->BgL_codez00) =
					((obj_t) ((obj_t) BgL_code1064z00_3236)), BUNSPEC);
				((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
									BgL_new1327z00_3580)))->BgL_vectz00) =
					((obj_t) BgL_vect1065z00_3237), BUNSPEC);
				((((BgL_jasfunz00_bglt) COBJECT(BgL_new1327z00_3580))->BgL_tretz00) =
					((BgL_jastypez00_bglt) ((BgL_jastypez00_bglt) BgL_tret1066z00_3238)),
					BUNSPEC);
				((((BgL_jasfunz00_bglt) COBJECT(BgL_new1327z00_3580))->BgL_targsz00) =
					((obj_t) BgL_targs1067z00_3239), BUNSPEC);
				return BgL_new1327z00_3580;
			}
		}

	}



/* &lambda1931 */
	obj_t BGl_z62lambda1931z62zzjas_classfilez00(obj_t BgL_envz00_3240,
		obj_t BgL_oz00_3241, obj_t BgL_vz00_3242)
	{
		{	/* Jas/classfile.scm 13 */
			return
				((((BgL_jasfunz00_bglt) COBJECT(
							((BgL_jasfunz00_bglt) BgL_oz00_3241)))->BgL_targsz00) =
				((obj_t) BgL_vz00_3242), BUNSPEC);
		}

	}



/* &lambda1930 */
	obj_t BGl_z62lambda1930z62zzjas_classfilez00(obj_t BgL_envz00_3243,
		obj_t BgL_oz00_3244)
	{
		{	/* Jas/classfile.scm 13 */
			return
				(((BgL_jasfunz00_bglt) COBJECT(
						((BgL_jasfunz00_bglt) BgL_oz00_3244)))->BgL_targsz00);
		}

	}



/* &lambda1926 */
	obj_t BGl_z62lambda1926z62zzjas_classfilez00(obj_t BgL_envz00_3245,
		obj_t BgL_oz00_3246, obj_t BgL_vz00_3247)
	{
		{	/* Jas/classfile.scm 13 */
			return
				((((BgL_jasfunz00_bglt) COBJECT(
							((BgL_jasfunz00_bglt) BgL_oz00_3246)))->BgL_tretz00) =
				((BgL_jastypez00_bglt) ((BgL_jastypez00_bglt) BgL_vz00_3247)), BUNSPEC);
		}

	}



/* &lambda1925 */
	BgL_jastypez00_bglt BGl_z62lambda1925z62zzjas_classfilez00(obj_t
		BgL_envz00_3248, obj_t BgL_oz00_3249)
	{
		{	/* Jas/classfile.scm 13 */
			return
				(((BgL_jasfunz00_bglt) COBJECT(
						((BgL_jasfunz00_bglt) BgL_oz00_3249)))->BgL_tretz00);
		}

	}



/* &<@anonymous:1902> */
	obj_t BGl_z62zc3z04anonymousza31902ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3250, obj_t BgL_new1062z00_3251)
	{
		{	/* Jas/classfile.scm 11 */
			{
				BgL_vectz00_bglt BgL_auxz00_5842;

				((((BgL_jastypez00_bglt) COBJECT(
								((BgL_jastypez00_bglt)
									((BgL_vectz00_bglt) BgL_new1062z00_3251))))->BgL_codez00) =
					((obj_t) BGl_string2346z00zzjas_classfilez00), BUNSPEC);
				((((BgL_jastypez00_bglt)
							COBJECT(((BgL_jastypez00_bglt) ((BgL_vectz00_bglt)
										BgL_new1062z00_3251))))->BgL_vectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_jastypez00_bglt BgL_auxz00_5849;

					{	/* Jas/classfile.scm 11 */
						obj_t BgL_classz00_3589;

						BgL_classz00_3589 = BGl_JasTypez00zzjas_classfilez00;
						{	/* Jas/classfile.scm 11 */
							obj_t BgL__ortest_1117z00_3590;

							BgL__ortest_1117z00_3590 = BGL_CLASS_NIL(BgL_classz00_3589);
							if (CBOOL(BgL__ortest_1117z00_3590))
								{	/* Jas/classfile.scm 11 */
									BgL_auxz00_5849 =
										((BgL_jastypez00_bglt) BgL__ortest_1117z00_3590);
								}
							else
								{	/* Jas/classfile.scm 11 */
									BgL_auxz00_5849 =
										((BgL_jastypez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3589));
								}
						}
					}
					((((BgL_vectz00_bglt) COBJECT(
									((BgL_vectz00_bglt) BgL_new1062z00_3251)))->BgL_typez00) =
						((BgL_jastypez00_bglt) BgL_auxz00_5849), BUNSPEC);
				}
				BgL_auxz00_5842 = ((BgL_vectz00_bglt) BgL_new1062z00_3251);
				return ((obj_t) BgL_auxz00_5842);
			}
		}

	}



/* &lambda1899 */
	BgL_vectz00_bglt BGl_z62lambda1899z62zzjas_classfilez00(obj_t BgL_envz00_3252)
	{
		{	/* Jas/classfile.scm 11 */
			{	/* Jas/classfile.scm 11 */
				BgL_vectz00_bglt BgL_new1061z00_3591;

				BgL_new1061z00_3591 =
					((BgL_vectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_vectz00_bgl))));
				{	/* Jas/classfile.scm 11 */
					long BgL_arg1901z00_3592;

					BgL_arg1901z00_3592 = BGL_CLASS_NUM(BGl_vectz00zzjas_classfilez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1061z00_3591), BgL_arg1901z00_3592);
				}
				return BgL_new1061z00_3591;
			}
		}

	}



/* &lambda1897 */
	BgL_vectz00_bglt BGl_z62lambda1897z62zzjas_classfilez00(obj_t BgL_envz00_3253,
		obj_t BgL_code1058z00_3254, obj_t BgL_vect1059z00_3255,
		obj_t BgL_type1060z00_3256)
	{
		{	/* Jas/classfile.scm 11 */
			{	/* Jas/classfile.scm 11 */
				BgL_vectz00_bglt BgL_new1325z00_3595;

				{	/* Jas/classfile.scm 11 */
					BgL_vectz00_bglt BgL_new1324z00_3596;

					BgL_new1324z00_3596 =
						((BgL_vectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_vectz00_bgl))));
					{	/* Jas/classfile.scm 11 */
						long BgL_arg1898z00_3597;

						BgL_arg1898z00_3597 = BGL_CLASS_NUM(BGl_vectz00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1324z00_3596), BgL_arg1898z00_3597);
					}
					BgL_new1325z00_3595 = BgL_new1324z00_3596;
				}
				((((BgL_jastypez00_bglt) COBJECT(
								((BgL_jastypez00_bglt) BgL_new1325z00_3595)))->BgL_codez00) =
					((obj_t) ((obj_t) BgL_code1058z00_3254)), BUNSPEC);
				((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
									BgL_new1325z00_3595)))->BgL_vectz00) =
					((obj_t) BgL_vect1059z00_3255), BUNSPEC);
				((((BgL_vectz00_bglt) COBJECT(BgL_new1325z00_3595))->BgL_typez00) =
					((BgL_jastypez00_bglt) ((BgL_jastypez00_bglt) BgL_type1060z00_3256)),
					BUNSPEC);
				return BgL_new1325z00_3595;
			}
		}

	}



/* &lambda1908 */
	obj_t BGl_z62lambda1908z62zzjas_classfilez00(obj_t BgL_envz00_3257,
		obj_t BgL_oz00_3258, obj_t BgL_vz00_3259)
	{
		{	/* Jas/classfile.scm 11 */
			return
				((((BgL_vectz00_bglt) COBJECT(
							((BgL_vectz00_bglt) BgL_oz00_3258)))->BgL_typez00) =
				((BgL_jastypez00_bglt) ((BgL_jastypez00_bglt) BgL_vz00_3259)), BUNSPEC);
		}

	}



/* &lambda1907 */
	BgL_jastypez00_bglt BGl_z62lambda1907z62zzjas_classfilez00(obj_t
		BgL_envz00_3260, obj_t BgL_oz00_3261)
	{
		{	/* Jas/classfile.scm 11 */
			return
				(((BgL_vectz00_bglt) COBJECT(
						((BgL_vectz00_bglt) BgL_oz00_3261)))->BgL_typez00);
		}

	}



/* &<@anonymous:1890> */
	obj_t BGl_z62zc3z04anonymousza31890ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3262, obj_t BgL_new1056z00_3263)
	{
		{	/* Jas/classfile.scm 9 */
			{
				BgL_basicz00_bglt BgL_auxz00_5880;

				((((BgL_jastypez00_bglt) COBJECT(
								((BgL_jastypez00_bglt)
									((BgL_basicz00_bglt) BgL_new1056z00_3263))))->BgL_codez00) =
					((obj_t) BGl_string2346z00zzjas_classfilez00), BUNSPEC);
				((((BgL_jastypez00_bglt)
							COBJECT(((BgL_jastypez00_bglt) ((BgL_basicz00_bglt)
										BgL_new1056z00_3263))))->BgL_vectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5880 = ((BgL_basicz00_bglt) BgL_new1056z00_3263);
				return ((obj_t) BgL_auxz00_5880);
			}
		}

	}



/* &lambda1888 */
	BgL_basicz00_bglt BGl_z62lambda1888z62zzjas_classfilez00(obj_t
		BgL_envz00_3264)
	{
		{	/* Jas/classfile.scm 9 */
			{	/* Jas/classfile.scm 9 */
				BgL_basicz00_bglt BgL_new1055z00_3602;

				BgL_new1055z00_3602 =
					((BgL_basicz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_basicz00_bgl))));
				{	/* Jas/classfile.scm 9 */
					long BgL_arg1889z00_3603;

					BgL_arg1889z00_3603 = BGL_CLASS_NUM(BGl_basicz00zzjas_classfilez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1055z00_3602), BgL_arg1889z00_3603);
				}
				return BgL_new1055z00_3602;
			}
		}

	}



/* &lambda1886 */
	BgL_basicz00_bglt BGl_z62lambda1886z62zzjas_classfilez00(obj_t
		BgL_envz00_3265, obj_t BgL_code1053z00_3266, obj_t BgL_vect1054z00_3267)
	{
		{	/* Jas/classfile.scm 9 */
			{	/* Jas/classfile.scm 9 */
				BgL_basicz00_bglt BgL_new1323z00_3605;

				{	/* Jas/classfile.scm 9 */
					BgL_basicz00_bglt BgL_new1322z00_3606;

					BgL_new1322z00_3606 =
						((BgL_basicz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_basicz00_bgl))));
					{	/* Jas/classfile.scm 9 */
						long BgL_arg1887z00_3607;

						BgL_arg1887z00_3607 = BGL_CLASS_NUM(BGl_basicz00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1322z00_3606), BgL_arg1887z00_3607);
					}
					BgL_new1323z00_3605 = BgL_new1322z00_3606;
				}
				((((BgL_jastypez00_bglt) COBJECT(
								((BgL_jastypez00_bglt) BgL_new1323z00_3605)))->BgL_codez00) =
					((obj_t) ((obj_t) BgL_code1053z00_3266)), BUNSPEC);
				((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
									BgL_new1323z00_3605)))->BgL_vectz00) =
					((obj_t) BgL_vect1054z00_3267), BUNSPEC);
				return BgL_new1323z00_3605;
			}
		}

	}



/* &<@anonymous:1867> */
	obj_t BGl_z62zc3z04anonymousza31867ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3268, obj_t BgL_new1051z00_3269)
	{
		{	/* Jas/classfile.scm 5 */
			{
				BgL_jastypez00_bglt BgL_auxz00_5902;

				((((BgL_jastypez00_bglt) COBJECT(
								((BgL_jastypez00_bglt) BgL_new1051z00_3269)))->BgL_codez00) =
					((obj_t) BGl_string2346z00zzjas_classfilez00), BUNSPEC);
				((((BgL_jastypez00_bglt) COBJECT(((BgL_jastypez00_bglt)
									BgL_new1051z00_3269)))->BgL_vectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5902 = ((BgL_jastypez00_bglt) BgL_new1051z00_3269);
				return ((obj_t) BgL_auxz00_5902);
			}
		}

	}



/* &lambda1865 */
	BgL_jastypez00_bglt BGl_z62lambda1865z62zzjas_classfilez00(obj_t
		BgL_envz00_3270)
	{
		{	/* Jas/classfile.scm 5 */
			{	/* Jas/classfile.scm 5 */
				BgL_jastypez00_bglt BgL_new1050z00_3609;

				BgL_new1050z00_3609 =
					((BgL_jastypez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_jastypez00_bgl))));
				{	/* Jas/classfile.scm 5 */
					long BgL_arg1866z00_3610;

					BgL_arg1866z00_3610 = BGL_CLASS_NUM(BGl_JasTypez00zzjas_classfilez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1050z00_3609), BgL_arg1866z00_3610);
				}
				return BgL_new1050z00_3609;
			}
		}

	}



/* &lambda1863 */
	BgL_jastypez00_bglt BGl_z62lambda1863z62zzjas_classfilez00(obj_t
		BgL_envz00_3271, obj_t BgL_code1048z00_3272, obj_t BgL_vect1049z00_3273)
	{
		{	/* Jas/classfile.scm 5 */
			{	/* Jas/classfile.scm 5 */
				BgL_jastypez00_bglt BgL_new1321z00_3612;

				{	/* Jas/classfile.scm 5 */
					BgL_jastypez00_bglt BgL_new1320z00_3613;

					BgL_new1320z00_3613 =
						((BgL_jastypez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_jastypez00_bgl))));
					{	/* Jas/classfile.scm 5 */
						long BgL_arg1864z00_3614;

						BgL_arg1864z00_3614 =
							BGL_CLASS_NUM(BGl_JasTypez00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1320z00_3613), BgL_arg1864z00_3614);
					}
					BgL_new1321z00_3612 = BgL_new1320z00_3613;
				}
				((((BgL_jastypez00_bglt) COBJECT(BgL_new1321z00_3612))->BgL_codez00) =
					((obj_t) ((obj_t) BgL_code1048z00_3272)), BUNSPEC);
				((((BgL_jastypez00_bglt) COBJECT(BgL_new1321z00_3612))->BgL_vectz00) =
					((obj_t) BgL_vect1049z00_3273), BUNSPEC);
				return BgL_new1321z00_3612;
			}
		}

	}



/* &<@anonymous:1879> */
	obj_t BGl_z62zc3z04anonymousza31879ze3ze5zzjas_classfilez00(obj_t
		BgL_envz00_3274)
	{
		{	/* Jas/classfile.scm 5 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1878 */
	obj_t BGl_z62lambda1878z62zzjas_classfilez00(obj_t BgL_envz00_3275,
		obj_t BgL_oz00_3276, obj_t BgL_vz00_3277)
	{
		{	/* Jas/classfile.scm 5 */
			return
				((((BgL_jastypez00_bglt) COBJECT(
							((BgL_jastypez00_bglt) BgL_oz00_3276)))->BgL_vectz00) =
				((obj_t) BgL_vz00_3277), BUNSPEC);
		}

	}



/* &lambda1877 */
	obj_t BGl_z62lambda1877z62zzjas_classfilez00(obj_t BgL_envz00_3278,
		obj_t BgL_oz00_3279)
	{
		{	/* Jas/classfile.scm 5 */
			return
				(((BgL_jastypez00_bglt) COBJECT(
						((BgL_jastypez00_bglt) BgL_oz00_3279)))->BgL_vectz00);
		}

	}



/* &lambda1872 */
	obj_t BGl_z62lambda1872z62zzjas_classfilez00(obj_t BgL_envz00_3280,
		obj_t BgL_oz00_3281, obj_t BgL_vz00_3282)
	{
		{	/* Jas/classfile.scm 5 */
			return
				((((BgL_jastypez00_bglt) COBJECT(
							((BgL_jastypez00_bglt) BgL_oz00_3281)))->BgL_codez00) = ((obj_t)
					((obj_t) BgL_vz00_3282)), BUNSPEC);
		}

	}



/* &lambda1871 */
	obj_t BGl_z62lambda1871z62zzjas_classfilez00(obj_t BgL_envz00_3283,
		obj_t BgL_oz00_3284)
	{
		{	/* Jas/classfile.scm 5 */
			return
				(((BgL_jastypez00_bglt) COBJECT(
						((BgL_jastypez00_bglt) BgL_oz00_3284)))->BgL_codez00);
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzjas_classfilez00(void)
	{
		{	/* Jas/classfile.scm 1 */
			return
				BGl_modulezd2initializa7ationz75zzjas_libz00(267157509L,
				BSTRING_TO_STRING(BGl_string2347z00zzjas_classfilez00));
		}

	}

#ifdef __cplusplus
}
#endif
