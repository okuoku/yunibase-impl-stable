/*===========================================================================*/
/*   (Jas/labels.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Jas/labels.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_JAS_LABELS_TYPE_DEFINITIONS
#define BGL_JAS_LABELS_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_classfilez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_currentzd2methodzd2;
		obj_t BgL_globalsz00;
		obj_t BgL_poolz00;
		obj_t BgL_poolzd2siza7ez75;
		obj_t BgL_pooledzd2nameszd2;
		obj_t BgL_flagsz00;
		obj_t BgL_mez00;
		obj_t BgL_superz00;
		obj_t BgL_interfacesz00;
		obj_t BgL_fieldsz00;
		obj_t BgL_methodsz00;
		obj_t BgL_attributesz00;
	}                   *BgL_classfilez00_bglt;


#endif													// BGL_JAS_LABELS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_getzd2targetze70z35zzjas_labelsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_getzd2targetze71z35zzjas_labelsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzjas_labelsz00 = BUNSPEC;
	static obj_t BGl_siza7ezd2insz75zzjas_labelsz00(obj_t, obj_t);
	static obj_t BGl_paddingz00zzjas_labelsz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzjas_labelsz00(void);
	BGL_IMPORT obj_t BGl_absz00zz__r4_numbers_6_5z00(obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzjas_labelsz00(void);
	static obj_t BGl_objectzd2initzd2zzjas_labelsz00(void);
	BGL_IMPORT obj_t BGl_appendz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_IMPORT obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_bignum_mul(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzjas_labelsz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzjas_labelsz00(void);
	extern obj_t BGl_u2z00zzjas_libz00(int);
	extern obj_t BGl_u4z00zzjas_libz00(int);
	static obj_t BGl_widezd2conditionalszd2zzjas_labelsz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_bignum_sub(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	static obj_t BGl_resolvezd2pczd2zzjas_labelsz00(obj_t, obj_t);
	BGL_IMPORT long BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(long, long);
	BGL_EXPORTED_DEF obj_t BGl_resolvezd2labelszd2zzjas_labelsz00 = BUNSPEC;
	static obj_t BGl_resolvezd2definitivezd2labelsz00zzjas_labelsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzjas_labelsz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_classfilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_libz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	static obj_t BGl_z62zc3z04resolvezd2labelsza31244ze3z37zzjas_labelsz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzjas_labelsz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzjas_labelsz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzjas_labelsz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzjas_labelsz00(void);
	extern obj_t BGl_jaszd2errorzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t, obj_t);
	static obj_t BGl_za2opcodezd2siza7eza2z75zzjas_labelsz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_resolvezd2target2ze70z35zzjas_labelsz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t __cnst[5];


	   
		 
		DEFINE_STRING(BGl_string1851z00zzjas_labelsz00,
		BgL_bgl_string1851za700za7za7j1859za7, "F", 1);
	      DEFINE_STRING(BGl_string1852z00zzjas_labelsz00,
		BgL_bgl_string1852za700za7za7j1860za7, "far label in handler", 20);
	      DEFINE_STRING(BGl_string1853z00zzjas_labelsz00,
		BgL_bgl_string1853za700za7za7j1861za7, "far label in localvar", 21);
	      DEFINE_STRING(BGl_string1854z00zzjas_labelsz00,
		BgL_bgl_string1854za700za7za7j1862za7, "unknown label", 13);
	      DEFINE_STRING(BGl_string1855z00zzjas_labelsz00,
		BgL_bgl_string1855za700za7za7j1863za7, "too far", 7);
	      DEFINE_STRING(BGl_string1856z00zzjas_labelsz00,
		BgL_bgl_string1856za700za7za7j1864za7, "jas_labels", 10);
	      DEFINE_STRING(BGl_string1857z00zzjas_labelsz00,
		BgL_bgl_string1857za700za7za7j1865za7,
		"(0) (0 0) (0 0 0) ok #(1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 3 2 3 3 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 3 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 2 0 0 1 1 1 1 1 1 3 3 3 3 3 3 3 5 0 3 2 3 1 1 3 3 1 1 0 4 3 3 5 5 0 0 0 0) ",
		436);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1850z00zzjas_labelsz00,
		BgL_bgl_za762za7c3za704resolve1866za7,
		BGl_z62zc3z04resolvezd2labelsza31244ze3z37zzjas_labelsz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzjas_labelsz00));
		     ADD_ROOT((void *) (&BGl_resolvezd2labelszd2zzjas_labelsz00));
		     ADD_ROOT((void *) (&BGl_za2opcodezd2siza7eza2z75zzjas_labelsz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzjas_labelsz00(long
		BgL_checksumz00_1140, char *BgL_fromz00_1141)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzjas_labelsz00))
				{
					BGl_requirezd2initializa7ationz75zzjas_labelsz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzjas_labelsz00();
					BGl_libraryzd2moduleszd2initz00zzjas_labelsz00();
					BGl_cnstzd2initzd2zzjas_labelsz00();
					BGl_importedzd2moduleszd2initz00zzjas_labelsz00();
					return BGl_toplevelzd2initzd2zzjas_labelsz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzjas_labelsz00(void)
	{
		{	/* Jas/labels.scm 1 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"jas_labels");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "jas_labels");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "jas_labels");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "jas_labels");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"jas_labels");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "jas_labels");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "jas_labels");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "jas_labels");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzjas_labelsz00(void)
	{
		{	/* Jas/labels.scm 1 */
			{	/* Jas/labels.scm 1 */
				obj_t BgL_cportz00_1128;

				{	/* Jas/labels.scm 1 */
					obj_t BgL_stringz00_1135;

					BgL_stringz00_1135 = BGl_string1857z00zzjas_labelsz00;
					{	/* Jas/labels.scm 1 */
						obj_t BgL_startz00_1136;

						BgL_startz00_1136 = BINT(0L);
						{	/* Jas/labels.scm 1 */
							obj_t BgL_endz00_1137;

							BgL_endz00_1137 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1135)));
							{	/* Jas/labels.scm 1 */

								BgL_cportz00_1128 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1135, BgL_startz00_1136, BgL_endz00_1137);
				}}}}
				{
					long BgL_iz00_1129;

					BgL_iz00_1129 = 4L;
				BgL_loopz00_1130:
					if ((BgL_iz00_1129 == -1L))
						{	/* Jas/labels.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* Jas/labels.scm 1 */
							{	/* Jas/labels.scm 1 */
								obj_t BgL_arg1858z00_1131;

								{	/* Jas/labels.scm 1 */

									{	/* Jas/labels.scm 1 */
										obj_t BgL_locationz00_1133;

										BgL_locationz00_1133 = BBOOL(((bool_t) 0));
										{	/* Jas/labels.scm 1 */

											BgL_arg1858z00_1131 =
												BGl_readz00zz__readerz00(BgL_cportz00_1128,
												BgL_locationz00_1133);
										}
									}
								}
								{	/* Jas/labels.scm 1 */
									int BgL_tmpz00_1167;

									BgL_tmpz00_1167 = (int) (BgL_iz00_1129);
									CNST_TABLE_SET(BgL_tmpz00_1167, BgL_arg1858z00_1131);
							}}
							{	/* Jas/labels.scm 1 */
								int BgL_auxz00_1134;

								BgL_auxz00_1134 = (int) ((BgL_iz00_1129 - 1L));
								{
									long BgL_iz00_1172;

									BgL_iz00_1172 = (long) (BgL_auxz00_1134);
									BgL_iz00_1129 = BgL_iz00_1172;
									goto BgL_loopz00_1130;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzjas_labelsz00(void)
	{
		{	/* Jas/labels.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzjas_labelsz00(void)
	{
		{	/* Jas/labels.scm 1 */
			BGl_resolvezd2labelszd2zzjas_labelsz00 = BGl_proc1850z00zzjas_labelsz00;
			return (BGl_za2opcodezd2siza7eza2z75zzjas_labelsz00 =
				CNST_TABLE_REF(0), BUNSPEC);
		}

	}



/* &<@resolve-labels:1244> */
	obj_t BGl_z62zc3z04resolvezd2labelsza31244ze3z37zzjas_labelsz00(obj_t
		BgL_envz00_1118, obj_t BgL_classfilez00_1119, obj_t BgL_lz00_1120)
	{
		{	/* Jas/labels.scm 8 */
			{

			BgL_fixpointz00_1139:
				if (CBOOL(BGl_widezd2conditionalszd2zzjas_labelsz00
						(BgL_classfilez00_1119, BgL_lz00_1120)))
					{	/* Jas/labels.scm 10 */
						CNST_TABLE_REF(1);
					}
				else
					{	/* Jas/labels.scm 10 */
						goto BgL_fixpointz00_1139;
					}
				return
					BGl_resolvezd2definitivezd2labelsz00zzjas_labelsz00
					(BgL_classfilez00_1119, BgL_lz00_1120);
			}
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzjas_labelsz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_265;

				BgL_headz00_265 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_266;
					obj_t BgL_tailz00_267;

					BgL_prevz00_266 = BgL_headz00_265;
					BgL_tailz00_267 = BgL_l1z00_1;
				BgL_loopz00_268:
					if (PAIRP(BgL_tailz00_267))
						{
							obj_t BgL_newzd2prevzd2_270;

							BgL_newzd2prevzd2_270 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_267), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_266, BgL_newzd2prevzd2_270);
							{
								obj_t BgL_tailz00_1188;
								obj_t BgL_prevz00_1187;

								BgL_prevz00_1187 = BgL_newzd2prevzd2_270;
								BgL_tailz00_1188 = CDR(BgL_tailz00_267);
								BgL_tailz00_267 = BgL_tailz00_1188;
								BgL_prevz00_266 = BgL_prevz00_1187;
								goto BgL_loopz00_268;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_265);
				}
			}
		}

	}



/* wide-conditionals */
	obj_t BGl_widezd2conditionalszd2zzjas_labelsz00(obj_t BgL_classfilez00_3,
		obj_t BgL_lz00_4)
	{
		{	/* Jas/labels.scm 18 */
			{	/* Jas/labels.scm 19 */
				obj_t BgL_envz00_273;

				BgL_envz00_273 =
					BGl_resolvezd2pczd2zzjas_labelsz00(BgL_classfilez00_3, BgL_lz00_4);
				{
					obj_t BgL_copz00_289;
					obj_t BgL_lz00_296;
					obj_t BgL_insz00_297;
					long BgL_pcz00_298;
					obj_t BgL_rz00_299;
					obj_t BgL_lz00_383;
					long BgL_pcz00_384;
					obj_t BgL_rz00_385;

					BgL_lz00_383 = BgL_lz00_4;
					BgL_pcz00_384 = 0L;
					BgL_rz00_385 = BTRUE;
				BgL_zc3z04anonymousza31490ze3z87_386:
					if (NULLP(BgL_lz00_383))
						{	/* Jas/labels.scm 62 */
							return BgL_rz00_385;
						}
					else
						{	/* Jas/labels.scm 63 */
							bool_t BgL_test1872z00_1194;

							{	/* Jas/labels.scm 63 */
								obj_t BgL_tmpz00_1195;

								BgL_tmpz00_1195 = CAR(((obj_t) BgL_lz00_383));
								BgL_test1872z00_1194 = SYMBOLP(BgL_tmpz00_1195);
							}
							if (BgL_test1872z00_1194)
								{	/* Jas/labels.scm 63 */
									obj_t BgL_arg1509z00_390;

									BgL_arg1509z00_390 = CDR(((obj_t) BgL_lz00_383));
									{
										obj_t BgL_lz00_1201;

										BgL_lz00_1201 = BgL_arg1509z00_390;
										BgL_lz00_383 = BgL_lz00_1201;
										goto BgL_zc3z04anonymousza31490ze3z87_386;
									}
								}
							else
								{	/* Jas/labels.scm 65 */
									obj_t BgL_arg1513z00_391;
									long BgL_arg1514z00_392;
									obj_t BgL_arg1516z00_393;

									BgL_arg1513z00_391 = CDR(((obj_t) BgL_lz00_383));
									{	/* Jas/labels.scm 66 */
										obj_t BgL_arg1535z00_394;

										{	/* Jas/labels.scm 66 */
											obj_t BgL_arg1540z00_395;

											BgL_arg1540z00_395 = CAR(((obj_t) BgL_lz00_383));
											BgL_arg1535z00_394 =
												BGl_siza7ezd2insz75zzjas_labelsz00(BgL_arg1540z00_395,
												BINT(BgL_pcz00_384));
										}
										BgL_arg1514z00_392 =
											(BgL_pcz00_384 + (long) CINT(BgL_arg1535z00_394));
									}
									{	/* Jas/labels.scm 67 */
										obj_t BgL_arg1544z00_396;

										BgL_arg1544z00_396 = CAR(((obj_t) BgL_lz00_383));
										BgL_lz00_296 = BgL_lz00_383;
										BgL_insz00_297 = BgL_arg1544z00_396;
										BgL_pcz00_298 = BgL_pcz00_384;
										BgL_rz00_299 = BgL_rz00_385;
										{	/* Jas/labels.scm 30 */
											obj_t BgL_aux1060z00_302;

											BgL_aux1060z00_302 = CAR(((obj_t) BgL_insz00_297));
											if (INTEGERP(BgL_aux1060z00_302))
												{	/* Jas/labels.scm 30 */
													switch ((long) CINT(BgL_aux1060z00_302))
														{
														case 153L:
														case 154L:
														case 155L:
														case 156L:
														case 157L:
														case 158L:
														case 159L:
														case 160L:
														case 161L:
														case 162L:
														case 163L:
														case 164L:
														case 165L:
														case 166L:
														case 198L:
														case 199L:

															{	/* Jas/labels.scm 32 */
																bool_t BgL_test1874z00_1216;

																{	/* Jas/labels.scm 32 */
																	obj_t BgL_arg1323z00_316;

																	{	/* Jas/labels.scm 32 */
																		obj_t BgL_pairz00_810;

																		BgL_pairz00_810 =
																			CDR(((obj_t) BgL_insz00_297));
																		BgL_arg1323z00_316 = CAR(BgL_pairz00_810);
																	}
																	{	/* Jas/labels.scm 24 */
																		obj_t BgL_arg1268z00_811;

																		{	/* Jas/labels.scm 24 */
																			long BgL_arg1272z00_812;

																			{	/* Jas/labels.scm 24 */
																				obj_t BgL_arg1284z00_813;

																				BgL_arg1284z00_813 =
																					BGl_getzd2targetze71z35zzjas_labelsz00
																					(BgL_classfilez00_3, BgL_envz00_273,
																					BgL_arg1323z00_316);
																				BgL_arg1272z00_812 =
																					((long) CINT(BgL_arg1284z00_813) -
																					BgL_pcz00_298);
																			}
																			BgL_arg1268z00_811 =
																				BGl_absz00zz__r4_numbers_6_5z00(BINT
																				(BgL_arg1272z00_812));
																		}
																		BgL_test1874z00_1216 =
																			(
																			(long) CINT(BgL_arg1268z00_811) > 32752L);
																}}
																if (BgL_test1874z00_1216)
																	{	/* Jas/labels.scm 33 */
																		obj_t BgL_labz00_305;

																		BgL_labz00_305 =
																			BGl_gensymz00zz__r4_symbols_6_4z00
																			(BGl_string1851z00zzjas_labelsz00);
																		{	/* Jas/labels.scm 34 */
																			obj_t BgL_arg1312z00_306;

																			{	/* Jas/labels.scm 34 */
																				long BgL_arg1314z00_307;
																				obj_t BgL_arg1315z00_308;

																				BgL_copz00_289 =
																					CAR(((obj_t) BgL_insz00_297));
																				if (
																					((long) CINT(BgL_copz00_289) <= 166L))
																					{	/* Jas/labels.scm 27 */
																						long BgL_arg1304z00_292;

																						{	/* Jas/labels.scm 27 */
																							long BgL_arg1305z00_293;

																							{	/* Jas/labels.scm 27 */
																								long BgL_arg1306z00_294;

																								{	/* Jas/labels.scm 27 */
																									long BgL_n1z00_788;
																									long BgL_n2z00_789;

																									BgL_n1z00_788 =
																										(long) CINT(BgL_copz00_289);
																									BgL_n2z00_789 = 2L;
																									{	/* Jas/labels.scm 27 */
																										bool_t BgL_test1876z00_1232;

																										{	/* Jas/labels.scm 27 */
																											long BgL_arg1338z00_791;

																											BgL_arg1338z00_791 =
																												(((BgL_n1z00_788) |
																													(BgL_n2z00_789)) &
																												-2147483648);
																											BgL_test1876z00_1232 =
																												(BgL_arg1338z00_791 ==
																												0L);
																										}
																										if (BgL_test1876z00_1232)
																											{	/* Jas/labels.scm 27 */
																												int32_t
																													BgL_arg1334z00_792;
																												{	/* Jas/labels.scm 27 */
																													int32_t
																														BgL_arg1336z00_793;
																													int32_t
																														BgL_arg1337z00_794;
																													BgL_arg1336z00_793 =
																														(int32_t)
																														(BgL_n1z00_788);
																													BgL_arg1337z00_794 =
																														(int32_t)
																														(BgL_n2z00_789);
																													BgL_arg1334z00_792 =
																														(BgL_arg1336z00_793
																														%
																														BgL_arg1337z00_794);
																												}
																												{	/* Jas/labels.scm 27 */
																													long
																														BgL_arg1446z00_799;
																													BgL_arg1446z00_799 =
																														(long)
																														(BgL_arg1334z00_792);
																													BgL_arg1306z00_294 =
																														(long)
																														(BgL_arg1446z00_799);
																											}}
																										else
																											{	/* Jas/labels.scm 27 */
																												BgL_arg1306z00_294 =
																													(BgL_n1z00_788 %
																													BgL_n2z00_789);
																											}
																									}
																								}
																								BgL_arg1305z00_293 =
																									(2L * BgL_arg1306z00_294);
																							}
																							BgL_arg1304z00_292 =
																								(BgL_arg1305z00_293 - 1L);
																						}
																						BgL_arg1314z00_307 =
																							(
																							(long) CINT(BgL_copz00_289) +
																							BgL_arg1304z00_292);
																					}
																				else
																					{	/* Jas/labels.scm 26 */
																						if (
																							((long) CINT(BgL_copz00_289) ==
																								198L))
																							{	/* Jas/labels.scm 28 */
																								BgL_arg1314z00_307 = 199L;
																							}
																						else
																							{	/* Jas/labels.scm 28 */
																								BgL_arg1314z00_307 = 198L;
																							}
																					}
																				BgL_arg1315z00_308 =
																					MAKE_YOUNG_PAIR(BgL_labz00_305, BNIL);
																				BgL_arg1312z00_306 =
																					MAKE_YOUNG_PAIR(BINT
																					(BgL_arg1314z00_307),
																					BgL_arg1315z00_308);
																			}
																			{	/* Jas/labels.scm 34 */
																				obj_t BgL_tmpz00_1253;

																				BgL_tmpz00_1253 =
																					((obj_t) BgL_lz00_296);
																				SET_CAR(BgL_tmpz00_1253,
																					BgL_arg1312z00_306);
																			}
																		}
																		{	/* Jas/labels.scm 35 */
																			obj_t BgL_arg1317z00_310;

																			{	/* Jas/labels.scm 35 */
																				obj_t BgL_arg1318z00_311;
																				obj_t BgL_arg1319z00_312;

																				{	/* Jas/labels.scm 35 */
																					obj_t BgL_arg1320z00_313;

																					{	/* Jas/labels.scm 35 */
																						obj_t BgL_arg1321z00_314;

																						{	/* Jas/labels.scm 35 */
																							obj_t BgL_pairz00_822;

																							BgL_pairz00_822 =
																								CDR(((obj_t) BgL_insz00_297));
																							BgL_arg1321z00_314 =
																								CAR(BgL_pairz00_822);
																						}
																						BgL_arg1320z00_313 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1321z00_314, BNIL);
																					}
																					BgL_arg1318z00_311 =
																						MAKE_YOUNG_PAIR(BINT(200L),
																						BgL_arg1320z00_313);
																				}
																				{	/* Jas/labels.scm 37 */
																					obj_t BgL_arg1322z00_315;

																					BgL_arg1322z00_315 =
																						CDR(((obj_t) BgL_lz00_296));
																					BgL_arg1319z00_312 =
																						MAKE_YOUNG_PAIR(BgL_labz00_305,
																						BgL_arg1322z00_315);
																				}
																				BgL_arg1317z00_310 =
																					MAKE_YOUNG_PAIR(BgL_arg1318z00_311,
																					BgL_arg1319z00_312);
																			}
																			{	/* Jas/labels.scm 35 */
																				obj_t BgL_tmpz00_1266;

																				BgL_tmpz00_1266 =
																					((obj_t) BgL_lz00_296);
																				SET_CDR(BgL_tmpz00_1266,
																					BgL_arg1317z00_310);
																			}
																		}
																		BgL_arg1516z00_393 = BFALSE;
																	}
																else
																	{	/* Jas/labels.scm 32 */
																		BgL_arg1516z00_393 = BgL_rz00_299;
																	}
															}
															break;
														case 167L:
														case 168L:

															{	/* Jas/labels.scm 41 */
																bool_t BgL_test1878z00_1269;

																{	/* Jas/labels.scm 41 */
																	obj_t BgL_arg1331z00_322;

																	{	/* Jas/labels.scm 41 */
																		obj_t BgL_pairz00_828;

																		BgL_pairz00_828 =
																			CDR(((obj_t) BgL_insz00_297));
																		BgL_arg1331z00_322 = CAR(BgL_pairz00_828);
																	}
																	{	/* Jas/labels.scm 24 */
																		obj_t BgL_arg1268z00_829;

																		{	/* Jas/labels.scm 24 */
																			long BgL_arg1272z00_830;

																			{	/* Jas/labels.scm 24 */
																				obj_t BgL_arg1284z00_831;

																				BgL_arg1284z00_831 =
																					BGl_getzd2targetze71z35zzjas_labelsz00
																					(BgL_classfilez00_3, BgL_envz00_273,
																					BgL_arg1331z00_322);
																				BgL_arg1272z00_830 =
																					((long) CINT(BgL_arg1284z00_831) -
																					BgL_pcz00_298);
																			}
																			BgL_arg1268z00_829 =
																				BGl_absz00zz__r4_numbers_6_5z00(BINT
																				(BgL_arg1272z00_830));
																		}
																		BgL_test1878z00_1269 =
																			(
																			(long) CINT(BgL_arg1268z00_829) > 32752L);
																}}
																if (BgL_test1878z00_1269)
																	{	/* Jas/labels.scm 41 */
																		{	/* Jas/labels.scm 43 */
																			long BgL_arg1327z00_319;

																			BgL_arg1327z00_319 =
																				(200L +
																				((long) CINT(CAR(
																							((obj_t) BgL_insz00_297))) -
																					167L));
																			{	/* Jas/labels.scm 43 */
																				obj_t BgL_auxz00_1287;
																				obj_t BgL_tmpz00_1285;

																				BgL_auxz00_1287 =
																					BINT(BgL_arg1327z00_319);
																				BgL_tmpz00_1285 =
																					((obj_t) BgL_insz00_297);
																				SET_CAR(BgL_tmpz00_1285,
																					BgL_auxz00_1287);
																		}}
																		BgL_arg1516z00_393 = BFALSE;
																	}
																else
																	{	/* Jas/labels.scm 41 */
																		BgL_arg1516z00_393 = BgL_rz00_299;
																	}
															}
															break;
														case 202L:

															{
																obj_t BgL_begz00_323;
																obj_t BgL_endz00_324;
																obj_t BgL_labz00_325;
																obj_t BgL_typez00_326;

																{	/* Jas/labels.scm 47 */
																	obj_t BgL_ezd2102zd2_328;

																	BgL_ezd2102zd2_328 =
																		CDR(((obj_t) BgL_insz00_297));
																	if (PAIRP(BgL_ezd2102zd2_328))
																		{	/* Jas/labels.scm 46 */
																			obj_t BgL_cdrzd2112zd2_330;

																			BgL_cdrzd2112zd2_330 =
																				CDR(BgL_ezd2102zd2_328);
																			if (PAIRP(BgL_cdrzd2112zd2_330))
																				{	/* Jas/labels.scm 46 */
																					obj_t BgL_cdrzd2118zd2_332;

																					BgL_cdrzd2118zd2_332 =
																						CDR(BgL_cdrzd2112zd2_330);
																					if (PAIRP(BgL_cdrzd2118zd2_332))
																						{	/* Jas/labels.scm 46 */
																							obj_t BgL_cdrzd2123zd2_334;

																							BgL_cdrzd2123zd2_334 =
																								CDR(BgL_cdrzd2118zd2_332);
																							if (PAIRP(BgL_cdrzd2123zd2_334))
																								{	/* Jas/labels.scm 46 */
																									if (NULLP(CDR
																											(BgL_cdrzd2123zd2_334)))
																										{	/* Jas/labels.scm 46 */
																											BgL_begz00_323 =
																												CAR(BgL_ezd2102zd2_328);
																											BgL_endz00_324 =
																												CAR
																												(BgL_cdrzd2112zd2_330);
																											BgL_labz00_325 =
																												CAR
																												(BgL_cdrzd2118zd2_332);
																											BgL_typez00_326 =
																												CAR
																												(BgL_cdrzd2123zd2_334);
																											{	/* Jas/labels.scm 49 */
																												bool_t
																													BgL_test1884z00_1306;
																												{	/* Jas/labels.scm 49 */
																													bool_t
																														BgL_test1885z00_1307;
																													{	/* Jas/labels.scm 49 */
																														obj_t
																															BgL_arg1370z00_350;
																														BgL_arg1370z00_350 =
																															BGl_getzd2targetze71z35zzjas_labelsz00
																															(BgL_classfilez00_3,
																															BgL_envz00_273,
																															BgL_endz00_324);
																														BgL_test1885z00_1307
																															=
																															((long)
																															CINT
																															(BgL_arg1370z00_350)
																															>= 65536L);
																													}
																													if (BgL_test1885z00_1307)
																														{	/* Jas/labels.scm 49 */
																															BgL_test1884z00_1306
																																= ((bool_t) 1);
																														}
																													else
																														{	/* Jas/labels.scm 50 */
																															obj_t
																																BgL_arg1367z00_349;
																															BgL_arg1367z00_349
																																=
																																BGl_getzd2targetze71z35zzjas_labelsz00
																																(BgL_classfilez00_3,
																																BgL_envz00_273,
																																BgL_labz00_325);
																															BgL_test1884z00_1306
																																=
																																((long)
																																CINT
																																(BgL_arg1367z00_349)
																																>= 65536L);
																												}}
																												if (BgL_test1884z00_1306)
																													{	/* Jas/labels.scm 49 */
																														BgL_arg1516z00_393 =
																															BGl_jaszd2errorzd2zzjas_classfilez00
																															(((BgL_classfilez00_bglt) BgL_classfilez00_3), BGl_string1852z00zzjas_labelsz00, BgL_insz00_297);
																													}
																												else
																													{	/* Jas/labels.scm 49 */
																														BgL_arg1516z00_393 =
																															BgL_rz00_299;
																													}
																											}
																										}
																									else
																										{	/* Jas/labels.scm 46 */
																											BgL_arg1516z00_393 =
																												BFALSE;
																										}
																								}
																							else
																								{	/* Jas/labels.scm 46 */
																									BgL_arg1516z00_393 = BFALSE;
																								}
																						}
																					else
																						{	/* Jas/labels.scm 46 */
																							BgL_arg1516z00_393 = BFALSE;
																						}
																				}
																			else
																				{	/* Jas/labels.scm 46 */
																					BgL_arg1516z00_393 = BFALSE;
																				}
																		}
																	else
																		{	/* Jas/labels.scm 46 */
																			BgL_arg1516z00_393 = BFALSE;
																		}
																}
															}
															break;
														case 205L:

															{	/* Jas/labels.scm 54 */
																obj_t BgL_ezd2130zd2_357;

																BgL_ezd2130zd2_357 =
																	CDR(((obj_t) BgL_insz00_297));
																if (PAIRP(BgL_ezd2130zd2_357))
																	{	/* Jas/labels.scm 53 */
																		obj_t BgL_cdrzd2142zd2_359;

																		BgL_cdrzd2142zd2_359 =
																			CDR(BgL_ezd2130zd2_357);
																		if (PAIRP(BgL_cdrzd2142zd2_359))
																			{	/* Jas/labels.scm 53 */
																				obj_t BgL_cdrzd2149zd2_361;

																				BgL_cdrzd2149zd2_361 =
																					CDR(BgL_cdrzd2142zd2_359);
																				if (PAIRP(BgL_cdrzd2149zd2_361))
																					{	/* Jas/labels.scm 53 */
																						obj_t BgL_cdrzd2155zd2_363;

																						BgL_cdrzd2155zd2_363 =
																							CDR(BgL_cdrzd2149zd2_361);
																						if (PAIRP(BgL_cdrzd2155zd2_363))
																							{	/* Jas/labels.scm 53 */
																								obj_t BgL_cdrzd2160zd2_365;

																								BgL_cdrzd2160zd2_365 =
																									CDR(BgL_cdrzd2155zd2_363);
																								if (PAIRP(BgL_cdrzd2160zd2_365))
																									{	/* Jas/labels.scm 53 */
																										if (NULLP(CDR
																												(BgL_cdrzd2160zd2_365)))
																											{	/* Jas/labels.scm 53 */
																												obj_t
																													BgL_arg1421z00_369;
																												obj_t
																													BgL_arg1422z00_370;
																												BgL_arg1421z00_369 =
																													CAR
																													(BgL_ezd2130zd2_357);
																												BgL_arg1422z00_370 =
																													CAR
																													(BgL_cdrzd2142zd2_359);
																												{	/* Jas/labels.scm 56 */
																													bool_t
																														BgL_test1892z00_1341;
																													{	/* Jas/labels.scm 56 */
																														bool_t
																															BgL_test1893z00_1342;
																														{	/* Jas/labels.scm 56 */
																															obj_t
																																BgL_arg1489z00_869;
																															BgL_arg1489z00_869
																																=
																																BGl_getzd2targetze71z35zzjas_labelsz00
																																(BgL_classfilez00_3,
																																BgL_envz00_273,
																																BgL_arg1421z00_369);
																															BgL_test1893z00_1342
																																=
																																((long)
																																CINT
																																(BgL_arg1489z00_869)
																																>= 65536L);
																														}
																														if (BgL_test1893z00_1342)
																															{	/* Jas/labels.scm 56 */
																																BgL_test1892z00_1341
																																	=
																																	((bool_t) 1);
																															}
																														else
																															{	/* Jas/labels.scm 57 */
																																obj_t
																																	BgL_arg1485z00_870;
																																BgL_arg1485z00_870
																																	=
																																	BGl_getzd2targetze71z35zzjas_labelsz00
																																	(BgL_classfilez00_3,
																																	BgL_envz00_273,
																																	BgL_arg1422z00_370);
																																BgL_test1892z00_1341
																																	=
																																	((long)
																																	CINT
																																	(BgL_arg1485z00_870)
																																	>= 65536L);
																													}}
																													if (BgL_test1892z00_1341)
																														{	/* Jas/labels.scm 56 */
																															BgL_arg1516z00_393
																																=
																																BGl_jaszd2errorzd2zzjas_classfilez00
																																(((BgL_classfilez00_bglt) BgL_classfilez00_3), BGl_string1853z00zzjas_labelsz00, BgL_insz00_297);
																														}
																													else
																														{	/* Jas/labels.scm 56 */
																															BgL_arg1516z00_393
																																= BgL_rz00_299;
																														}
																												}
																											}
																										else
																											{	/* Jas/labels.scm 53 */
																												BgL_arg1516z00_393 =
																													BFALSE;
																											}
																									}
																								else
																									{	/* Jas/labels.scm 53 */
																										BgL_arg1516z00_393 = BFALSE;
																									}
																							}
																						else
																							{	/* Jas/labels.scm 53 */
																								BgL_arg1516z00_393 = BFALSE;
																							}
																					}
																				else
																					{	/* Jas/labels.scm 53 */
																						BgL_arg1516z00_393 = BFALSE;
																					}
																			}
																		else
																			{	/* Jas/labels.scm 53 */
																				BgL_arg1516z00_393 = BFALSE;
																			}
																	}
																else
																	{	/* Jas/labels.scm 53 */
																		BgL_arg1516z00_393 = BFALSE;
																	}
															}
															break;
														default:
															BgL_arg1516z00_393 = BgL_rz00_299;
														}
												}
											else
												{	/* Jas/labels.scm 30 */
													BgL_arg1516z00_393 = BgL_rz00_299;
												}
										}
									}
									{
										obj_t BgL_rz00_1355;
										long BgL_pcz00_1354;
										obj_t BgL_lz00_1353;

										BgL_lz00_1353 = BgL_arg1513z00_391;
										BgL_pcz00_1354 = BgL_arg1514z00_392;
										BgL_rz00_1355 = BgL_arg1516z00_393;
										BgL_rz00_385 = BgL_rz00_1355;
										BgL_pcz00_384 = BgL_pcz00_1354;
										BgL_lz00_383 = BgL_lz00_1353;
										goto BgL_zc3z04anonymousza31490ze3z87_386;
									}
								}
						}
				}
			}
		}

	}



/* get-target~1 */
	obj_t BGl_getzd2targetze71z35zzjas_labelsz00(obj_t BgL_classfilez00_1127,
		obj_t BgL_envz00_1126, obj_t BgL_labelz00_279)
	{
		{	/* Jas/labels.scm 22 */
			{	/* Jas/labels.scm 21 */
				obj_t BgL_arg1252z00_281;

				{	/* Jas/labels.scm 21 */
					obj_t BgL__ortest_1058z00_282;

					BgL__ortest_1058z00_282 =
						BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_labelz00_279,
						BgL_envz00_1126);
					if (CBOOL(BgL__ortest_1058z00_282))
						{	/* Jas/labels.scm 21 */
							BgL_arg1252z00_281 = BgL__ortest_1058z00_282;
						}
					else
						{	/* Jas/labels.scm 21 */
							BgL_arg1252z00_281 =
								BGl_jaszd2errorzd2zzjas_classfilez00(
								((BgL_classfilez00_bglt) BgL_classfilez00_1127),
								BGl_string1854z00zzjas_labelsz00, BgL_labelz00_279);
						}
				}
				return CDR(((obj_t) BgL_arg1252z00_281));
			}
		}

	}



/* resolve-definitive-labels */
	obj_t BGl_resolvezd2definitivezd2labelsz00zzjas_labelsz00(obj_t
		BgL_classfilez00_5, obj_t BgL_lz00_6)
	{
		{	/* Jas/labels.scm 73 */
			{	/* Jas/labels.scm 74 */
				obj_t BgL_envz00_403;

				BgL_envz00_403 =
					BGl_resolvezd2pczd2zzjas_labelsz00(BgL_classfilez00_5, BgL_lz00_6);
				{
					obj_t BgL_insz00_406;
					obj_t BgL_pcz00_407;
					obj_t BgL_lz00_552;
					obj_t BgL_pcz00_553;
					obj_t BgL_donez00_554;

					BgL_lz00_552 = BgL_lz00_6;
					BgL_pcz00_553 = BINT(0L);
					BgL_donez00_554 = BNIL;
				BgL_zc3z04anonymousza31748ze3z87_555:
					if (NULLP(BgL_lz00_552))
						{	/* Jas/labels.scm 117 */
							return bgl_reverse_bang(BgL_donez00_554);
						}
					else
						{	/* Jas/labels.scm 118 */
							bool_t BgL_test1896z00_1367;

							{	/* Jas/labels.scm 118 */
								obj_t BgL_tmpz00_1368;

								BgL_tmpz00_1368 = CAR(((obj_t) BgL_lz00_552));
								BgL_test1896z00_1367 = SYMBOLP(BgL_tmpz00_1368);
							}
							if (BgL_test1896z00_1367)
								{	/* Jas/labels.scm 118 */
									obj_t BgL_arg1753z00_559;

									BgL_arg1753z00_559 = CDR(((obj_t) BgL_lz00_552));
									{
										obj_t BgL_lz00_1374;

										BgL_lz00_1374 = BgL_arg1753z00_559;
										BgL_lz00_552 = BgL_lz00_1374;
										goto BgL_zc3z04anonymousza31748ze3z87_555;
									}
								}
							else
								{	/* Jas/labels.scm 120 */
									obj_t BgL_arg1754z00_560;
									obj_t BgL_arg1755z00_561;
									obj_t BgL_arg1761z00_562;

									BgL_arg1754z00_560 = CDR(((obj_t) BgL_lz00_552));
									{	/* Jas/labels.scm 121 */
										obj_t BgL_b1226z00_563;

										{	/* Jas/labels.scm 121 */
											obj_t BgL_arg1765z00_565;

											BgL_arg1765z00_565 = CAR(((obj_t) BgL_lz00_552));
											BgL_b1226z00_563 =
												BGl_siza7ezd2insz75zzjas_labelsz00(BgL_arg1765z00_565,
												BgL_pcz00_553);
										}
										{	/* Jas/labels.scm 121 */
											bool_t BgL_test1897z00_1380;

											if (INTEGERP(BgL_pcz00_553))
												{	/* Jas/labels.scm 121 */
													BgL_test1897z00_1380 = INTEGERP(BgL_b1226z00_563);
												}
											else
												{	/* Jas/labels.scm 121 */
													BgL_test1897z00_1380 = ((bool_t) 0);
												}
											if (BgL_test1897z00_1380)
												{	/* Jas/labels.scm 121 */
													obj_t BgL_tmpz00_1008;

													BgL_tmpz00_1008 = BINT(0L);
													if (BGL_ADDFX_OV(BgL_pcz00_553, BgL_b1226z00_563,
															BgL_tmpz00_1008))
														{	/* Jas/labels.scm 121 */
															BgL_arg1755z00_561 =
																bgl_bignum_add(bgl_long_to_bignum(
																	(long) CINT(BgL_pcz00_553)),
																bgl_long_to_bignum(
																	(long) CINT(BgL_b1226z00_563)));
														}
													else
														{	/* Jas/labels.scm 121 */
															BgL_arg1755z00_561 = BgL_tmpz00_1008;
														}
												}
											else
												{	/* Jas/labels.scm 121 */
													BgL_arg1755z00_561 =
														BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_pcz00_553,
														BgL_b1226z00_563);
												}
										}
									}
									{	/* Jas/labels.scm 122 */
										obj_t BgL_arg1767z00_566;

										{	/* Jas/labels.scm 122 */
											obj_t BgL_arg1770z00_567;

											BgL_arg1770z00_567 = CAR(((obj_t) BgL_lz00_552));
											BgL_insz00_406 = BgL_arg1770z00_567;
											BgL_pcz00_407 = BgL_pcz00_553;
											{
												obj_t BgL_labelz00_532;

												{	/* Jas/labels.scm 86 */
													obj_t BgL_aux1063z00_413;

													BgL_aux1063z00_413 = CAR(((obj_t) BgL_insz00_406));
													if (INTEGERP(BgL_aux1063z00_413))
														{	/* Jas/labels.scm 86 */
															switch ((long) CINT(BgL_aux1063z00_413))
																{
																case 153L:
																case 154L:
																case 155L:
																case 156L:
																case 157L:
																case 158L:
																case 159L:
																case 160L:
																case 161L:
																case 162L:
																case 163L:
																case 164L:
																case 165L:
																case 166L:
																case 167L:
																case 168L:
																case 198L:
																case 199L:

																	{	/* Jas/labels.scm 89 */
																		obj_t BgL_arg1553z00_415;

																		{	/* Jas/labels.scm 89 */
																			obj_t BgL_arg1559z00_416;

																			{	/* Jas/labels.scm 89 */
																				obj_t BgL_pairz00_908;

																				BgL_pairz00_908 =
																					CDR(((obj_t) BgL_insz00_406));
																				BgL_arg1559z00_416 =
																					CAR(BgL_pairz00_908);
																			}
																			BgL_labelz00_532 = BgL_arg1559z00_416;
																			{	/* Jas/labels.scm 80 */
																				obj_t BgL_dz00_534;

																				{	/* Jas/labels.scm 80 */
																					obj_t BgL_a1207z00_542;

																					BgL_a1207z00_542 =
																						BGl_getzd2targetze70z35zzjas_labelsz00
																						(BgL_classfilez00_5, BgL_envz00_403,
																						BgL_labelz00_532);
																					{	/* Jas/labels.scm 80 */
																						bool_t BgL_test1901z00_1403;

																						if (INTEGERP(BgL_a1207z00_542))
																							{	/* Jas/labels.scm 80 */
																								BgL_test1901z00_1403 =
																									INTEGERP(BgL_pcz00_407);
																							}
																						else
																							{	/* Jas/labels.scm 80 */
																								BgL_test1901z00_1403 =
																									((bool_t) 0);
																							}
																						if (BgL_test1901z00_1403)
																							{	/* Jas/labels.scm 80 */
																								obj_t BgL_tmpz00_883;

																								BgL_tmpz00_883 = BINT(0L);
																								if (BGL_SUBFX_OV
																									(BgL_a1207z00_542,
																										BgL_pcz00_407,
																										BgL_tmpz00_883))
																									{	/* Jas/labels.scm 80 */
																										BgL_dz00_534 =
																											bgl_bignum_sub
																											(bgl_long_to_bignum((long)
																												CINT(BgL_a1207z00_542)),
																											bgl_long_to_bignum((long)
																												CINT(BgL_pcz00_407)));
																									}
																								else
																									{	/* Jas/labels.scm 80 */
																										BgL_dz00_534 =
																											BgL_tmpz00_883;
																									}
																							}
																						else
																							{	/* Jas/labels.scm 80 */
																								BgL_dz00_534 =
																									BGl_2zd2zd2zz__r4_numbers_6_5z00
																									(BgL_a1207z00_542,
																									BgL_pcz00_407);
																							}
																					}
																				}
																				{	/* Jas/labels.scm 81 */
																					bool_t BgL_test1904z00_1416;

																					{	/* Jas/labels.scm 81 */
																						obj_t BgL_a1208z00_539;

																						BgL_a1208z00_539 =
																							BGl_absz00zz__r4_numbers_6_5z00
																							(BgL_dz00_534);
																						{	/* Jas/labels.scm 81 */

																							if (INTEGERP(BgL_a1208z00_539))
																								{	/* Jas/labels.scm 81 */
																									BgL_test1904z00_1416 =
																										(
																										(long)
																										CINT(BgL_a1208z00_539) >
																										32752L);
																								}
																							else
																								{	/* Jas/labels.scm 81 */
																									BgL_test1904z00_1416 =
																										BGl_2ze3ze3zz__r4_numbers_6_5z00
																										(BgL_a1208z00_539,
																										BINT(32752L));
																								}
																						}
																					}
																					if (BgL_test1904z00_1416)
																						{	/* Jas/labels.scm 81 */
																							BgL_arg1553z00_415 =
																								BGl_jaszd2errorzd2zzjas_classfilez00
																								(((BgL_classfilez00_bglt)
																									BgL_classfilez00_5),
																								BGl_string1855z00zzjas_labelsz00,
																								BgL_labelz00_532);
																						}
																					else
																						{	/* Jas/labels.scm 81 */
																							BgL_arg1553z00_415 =
																								BGl_u2z00zzjas_libz00(CINT
																								(BgL_dz00_534));
																						}
																				}
																			}
																		}
																		BgL_arg1767z00_566 =
																			MAKE_YOUNG_PAIR(BgL_aux1063z00_413,
																			BgL_arg1553z00_415);
																	}
																	break;
																case 200L:
																case 201L:

																	{	/* Jas/labels.scm 91 */
																		obj_t BgL_arg1564z00_418;

																		{	/* Jas/labels.scm 91 */
																			obj_t BgL_arg1565z00_419;

																			{	/* Jas/labels.scm 91 */
																				obj_t BgL_pairz00_913;

																				BgL_pairz00_913 =
																					CDR(((obj_t) BgL_insz00_406));
																				BgL_arg1565z00_419 =
																					CAR(BgL_pairz00_913);
																			}
																			BgL_arg1564z00_418 =
																				BGl_resolvezd2target2ze70z35zzjas_labelsz00
																				(BgL_pcz00_407, BgL_classfilez00_5,
																				BgL_envz00_403, BgL_arg1565z00_419);
																		}
																		BgL_arg1767z00_566 =
																			MAKE_YOUNG_PAIR(BgL_aux1063z00_413,
																			BgL_arg1564z00_418);
																	}
																	break;
																case 170L:

																	{	/* Jas/labels.scm 93 */
																		obj_t BgL_arg1571z00_420;

																		{	/* Jas/labels.scm 93 */
																			obj_t BgL_arg1573z00_421;
																			obj_t BgL_arg1575z00_422;

																			BgL_arg1573z00_421 =
																				BGl_paddingz00zzjas_labelsz00
																				(BgL_pcz00_407);
																			{	/* Jas/labels.scm 93 */
																				obj_t BgL_arg1576z00_423;
																				obj_t BgL_arg1584z00_424;

																				{	/* Jas/labels.scm 93 */
																					obj_t BgL_arg1585z00_425;

																					{	/* Jas/labels.scm 93 */
																						obj_t BgL_pairz00_917;

																						BgL_pairz00_917 =
																							CDR(((obj_t) BgL_insz00_406));
																						BgL_arg1585z00_425 =
																							CAR(BgL_pairz00_917);
																					}
																					BgL_arg1576z00_423 =
																						BGl_resolvezd2target2ze70z35zzjas_labelsz00
																						(BgL_pcz00_407, BgL_classfilez00_5,
																						BgL_envz00_403, BgL_arg1585z00_425);
																				}
																				{	/* Jas/labels.scm 94 */
																					obj_t BgL_arg1589z00_426;
																					obj_t BgL_arg1591z00_427;

																					{	/* Jas/labels.scm 94 */
																						obj_t BgL_arg1593z00_428;

																						{	/* Jas/labels.scm 94 */
																							obj_t BgL_pairz00_923;

																							{	/* Jas/labels.scm 94 */
																								obj_t BgL_pairz00_922;

																								BgL_pairz00_922 =
																									CDR(((obj_t) BgL_insz00_406));
																								BgL_pairz00_923 =
																									CDR(BgL_pairz00_922);
																							}
																							BgL_arg1593z00_428 =
																								CAR(BgL_pairz00_923);
																						}
																						BgL_arg1589z00_426 =
																							BGl_u4z00zzjas_libz00(CINT
																							(BgL_arg1593z00_428));
																					}
																					{	/* Jas/labels.scm 95 */
																						obj_t BgL_arg1594z00_429;
																						obj_t BgL_arg1595z00_430;

																						{	/* Jas/labels.scm 95 */
																							obj_t BgL_arg1602z00_431;

																							{	/* Jas/labels.scm 95 */
																								obj_t BgL_a1213z00_432;

																								{	/* Jas/labels.scm 95 */
																									obj_t BgL_a1211z00_435;

																									{	/* Jas/labels.scm 95 */
																										obj_t BgL_pairz00_929;

																										{	/* Jas/labels.scm 95 */
																											obj_t BgL_pairz00_928;

																											BgL_pairz00_928 =
																												CDR(
																												((obj_t)
																													BgL_insz00_406));
																											BgL_pairz00_929 =
																												CDR(BgL_pairz00_928);
																										}
																										BgL_a1211z00_435 =
																											CAR(BgL_pairz00_929);
																									}
																									{	/* Jas/labels.scm 95 */
																										long BgL_b1212z00_436;

																										{	/* Jas/labels.scm 95 */
																											obj_t BgL_auxz00_1449;

																											{	/* Jas/labels.scm 95 */
																												obj_t BgL_pairz00_935;

																												{	/* Jas/labels.scm 95 */
																													obj_t BgL_pairz00_934;

																													BgL_pairz00_934 =
																														CDR(
																														((obj_t)
																															BgL_insz00_406));
																													BgL_pairz00_935 =
																														CDR
																														(BgL_pairz00_934);
																												}
																												BgL_auxz00_1449 =
																													CDR(BgL_pairz00_935);
																											}
																											BgL_b1212z00_436 =
																												bgl_list_length
																												(BgL_auxz00_1449);
																										}
																										{	/* Jas/labels.scm 95 */

																											if (INTEGERP
																												(BgL_a1211z00_435))
																												{	/* Jas/labels.scm 95 */
																													obj_t BgL_za72za7_937;

																													BgL_za72za7_937 =
																														BINT
																														(BgL_b1212z00_436);
																													{	/* Jas/labels.scm 95 */
																														obj_t
																															BgL_tmpz00_938;
																														BgL_tmpz00_938 =
																															BINT(0L);
																														if (BGL_ADDFX_OV
																															(BgL_a1211z00_435,
																																BgL_za72za7_937,
																																BgL_tmpz00_938))
																															{	/* Jas/labels.scm 95 */
																																BgL_a1213z00_432
																																	=
																																	bgl_bignum_add
																																	(bgl_long_to_bignum
																																	((long)
																																		CINT
																																		(BgL_a1211z00_435)),
																																	bgl_long_to_bignum
																																	((long)
																																		CINT
																																		(BgL_za72za7_937)));
																															}
																														else
																															{	/* Jas/labels.scm 95 */
																																BgL_a1213z00_432
																																	=
																																	BgL_tmpz00_938;
																															}
																													}
																												}
																											else
																												{	/* Jas/labels.scm 95 */
																													BgL_a1213z00_432 =
																														BGl_2zb2zb2zz__r4_numbers_6_5z00
																														(BgL_a1211z00_435,
																														BINT
																														(BgL_b1212z00_436));
																												}
																										}
																									}
																								}
																								{	/* Jas/labels.scm 95 */

																									if (INTEGERP
																										(BgL_a1213z00_432))
																										{	/* Jas/labels.scm 95 */
																											obj_t BgL_za72za7_947;

																											BgL_za72za7_947 =
																												BINT(1L);
																											{	/* Jas/labels.scm 95 */
																												obj_t BgL_tmpz00_948;

																												BgL_tmpz00_948 =
																													BINT(0L);
																												if (BGL_SUBFX_OV
																													(BgL_a1213z00_432,
																														BgL_za72za7_947,
																														BgL_tmpz00_948))
																													{	/* Jas/labels.scm 95 */
																														BgL_arg1602z00_431 =
																															bgl_bignum_sub
																															(bgl_long_to_bignum
																															((long)
																																CINT
																																(BgL_a1213z00_432)),
																															bgl_long_to_bignum
																															((long)
																																CINT
																																(BgL_za72za7_947)));
																													}
																												else
																													{	/* Jas/labels.scm 95 */
																														BgL_arg1602z00_431 =
																															BgL_tmpz00_948;
																													}
																											}
																										}
																									else
																										{	/* Jas/labels.scm 95 */
																											BgL_arg1602z00_431 =
																												BGl_2zd2zd2zz__r4_numbers_6_5z00
																												(BgL_a1213z00_432,
																												BINT(1L));
																										}
																								}
																							}
																							BgL_arg1594z00_429 =
																								BGl_u4z00zzjas_libz00(CINT
																								(BgL_arg1602z00_431));
																						}
																						{	/* Jas/labels.scm 96 */
																							obj_t BgL_runner1617z00_455;

																							{	/* Jas/labels.scm 96 */
																								obj_t BgL_l1215z00_439;

																								{	/* Jas/labels.scm 96 */
																									obj_t BgL_pairz00_961;

																									{	/* Jas/labels.scm 96 */
																										obj_t BgL_pairz00_960;

																										BgL_pairz00_960 =
																											CDR(
																											((obj_t) BgL_insz00_406));
																										BgL_pairz00_961 =
																											CDR(BgL_pairz00_960);
																									}
																									BgL_l1215z00_439 =
																										CDR(BgL_pairz00_961);
																								}
																								if (NULLP(BgL_l1215z00_439))
																									{	/* Jas/labels.scm 96 */
																										BgL_runner1617z00_455 =
																											BNIL;
																									}
																								else
																									{	/* Jas/labels.scm 96 */
																										obj_t BgL_head1217z00_441;

																										{	/* Jas/labels.scm 96 */
																											obj_t BgL_arg1615z00_453;

																											{	/* Jas/labels.scm 96 */
																												obj_t
																													BgL_arg1616z00_454;
																												BgL_arg1616z00_454 =
																													CAR(((obj_t)
																														BgL_l1215z00_439));
																												BgL_arg1615z00_453 =
																													BGl_resolvezd2target2ze70z35zzjas_labelsz00
																													(BgL_pcz00_407,
																													BgL_classfilez00_5,
																													BgL_envz00_403,
																													BgL_arg1616z00_454);
																											}
																											BgL_head1217z00_441 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1615z00_453,
																												BNIL);
																										}
																										{	/* Jas/labels.scm 96 */
																											obj_t BgL_g1220z00_442;

																											BgL_g1220z00_442 =
																												CDR(
																												((obj_t)
																													BgL_l1215z00_439));
																											{
																												obj_t BgL_l1215z00_444;
																												obj_t
																													BgL_tail1218z00_445;
																												BgL_l1215z00_444 =
																													BgL_g1220z00_442;
																												BgL_tail1218z00_445 =
																													BgL_head1217z00_441;
																											BgL_zc3z04anonymousza31607ze3z87_446:
																												if (NULLP
																													(BgL_l1215z00_444))
																													{	/* Jas/labels.scm 96 */
																														BgL_runner1617z00_455
																															=
																															BgL_head1217z00_441;
																													}
																												else
																													{	/* Jas/labels.scm 96 */
																														obj_t
																															BgL_newtail1219z00_448;
																														{	/* Jas/labels.scm 96 */
																															obj_t
																																BgL_arg1611z00_450;
																															{	/* Jas/labels.scm 96 */
																																obj_t
																																	BgL_arg1613z00_451;
																																BgL_arg1613z00_451
																																	=
																																	CAR(((obj_t)
																																		BgL_l1215z00_444));
																																BgL_arg1611z00_450
																																	=
																																	BGl_resolvezd2target2ze70z35zzjas_labelsz00
																																	(BgL_pcz00_407,
																																	BgL_classfilez00_5,
																																	BgL_envz00_403,
																																	BgL_arg1613z00_451);
																															}
																															BgL_newtail1219z00_448
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1611z00_450,
																																BNIL);
																														}
																														SET_CDR
																															(BgL_tail1218z00_445,
																															BgL_newtail1219z00_448);
																														{	/* Jas/labels.scm 96 */
																															obj_t
																																BgL_arg1609z00_449;
																															BgL_arg1609z00_449
																																=
																																CDR(((obj_t)
																																	BgL_l1215z00_444));
																															{
																																obj_t
																																	BgL_tail1218z00_1505;
																																obj_t
																																	BgL_l1215z00_1504;
																																BgL_l1215z00_1504
																																	=
																																	BgL_arg1609z00_449;
																																BgL_tail1218z00_1505
																																	=
																																	BgL_newtail1219z00_448;
																																BgL_tail1218z00_445
																																	=
																																	BgL_tail1218z00_1505;
																																BgL_l1215z00_444
																																	=
																																	BgL_l1215z00_1504;
																																goto
																																	BgL_zc3z04anonymousza31607ze3z87_446;
																															}
																														}
																													}
																											}
																										}
																									}
																							}
																							BgL_arg1595z00_430 =
																								BGl_appendz00zz__r4_pairs_and_lists_6_3z00
																								(BgL_runner1617z00_455);
																						}
																						BgL_arg1591z00_427 =
																							BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																							(BgL_arg1594z00_429,
																							BgL_arg1595z00_430);
																					}
																					BgL_arg1584z00_424 =
																						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																						(BgL_arg1589z00_426,
																						BgL_arg1591z00_427);
																				}
																				BgL_arg1575z00_422 =
																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																					(BgL_arg1576z00_423,
																					BgL_arg1584z00_424);
																			}
																			BgL_arg1571z00_420 =
																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																				(BgL_arg1573z00_421,
																				BgL_arg1575z00_422);
																		}
																		BgL_arg1767z00_566 =
																			MAKE_YOUNG_PAIR(BINT(170L),
																			BgL_arg1571z00_420);
																	}
																	break;
																case 171L:

																	{	/* Jas/labels.scm 98 */
																		obj_t BgL_arg1625z00_456;

																		{	/* Jas/labels.scm 98 */
																			obj_t BgL_arg1626z00_457;
																			obj_t BgL_arg1627z00_458;

																			BgL_arg1626z00_457 =
																				BGl_paddingz00zzjas_labelsz00
																				(BgL_pcz00_407);
																			{	/* Jas/labels.scm 98 */
																				obj_t BgL_arg1629z00_459;
																				obj_t BgL_arg1630z00_460;

																				{	/* Jas/labels.scm 98 */
																					obj_t BgL_arg1642z00_461;

																					{	/* Jas/labels.scm 98 */
																						obj_t BgL_pairz00_970;

																						BgL_pairz00_970 =
																							CDR(((obj_t) BgL_insz00_406));
																						BgL_arg1642z00_461 =
																							CAR(BgL_pairz00_970);
																					}
																					BgL_arg1629z00_459 =
																						BGl_resolvezd2target2ze70z35zzjas_labelsz00
																						(BgL_pcz00_407, BgL_classfilez00_5,
																						BgL_envz00_403, BgL_arg1642z00_461);
																				}
																				{	/* Jas/labels.scm 99 */
																					obj_t BgL_arg1646z00_462;
																					obj_t BgL_arg1650z00_463;

																					{	/* Jas/labels.scm 99 */
																						long BgL_arg1651z00_464;

																						{	/* Jas/labels.scm 99 */
																							obj_t BgL_auxz00_1518;

																							{	/* Jas/labels.scm 99 */
																								obj_t BgL_pairz00_974;

																								BgL_pairz00_974 =
																									CDR(((obj_t) BgL_insz00_406));
																								BgL_auxz00_1518 =
																									CDR(BgL_pairz00_974);
																							}
																							BgL_arg1651z00_464 =
																								bgl_list_length
																								(BgL_auxz00_1518);
																						}
																						BgL_arg1646z00_462 =
																							BGl_u4z00zzjas_libz00(
																							(int) (BgL_arg1651z00_464));
																					}
																					{	/* Jas/labels.scm 100 */
																						obj_t BgL_runner1689z00_483;

																						{	/* Jas/labels.scm 101 */
																							obj_t BgL_l1221z00_466;

																							{	/* Jas/labels.scm 104 */
																								obj_t BgL_pairz00_978;

																								BgL_pairz00_978 =
																									CDR(((obj_t) BgL_insz00_406));
																								BgL_l1221z00_466 =
																									CDR(BgL_pairz00_978);
																							}
																							if (NULLP(BgL_l1221z00_466))
																								{	/* Jas/labels.scm 101 */
																									BgL_runner1689z00_483 = BNIL;
																								}
																							else
																								{	/* Jas/labels.scm 101 */
																									obj_t BgL_head1223z00_468;

																									BgL_head1223z00_468 =
																										MAKE_YOUNG_PAIR(BNIL, BNIL);
																									{
																										obj_t BgL_l1221z00_470;
																										obj_t BgL_tail1224z00_471;

																										BgL_l1221z00_470 =
																											BgL_l1221z00_466;
																										BgL_tail1224z00_471 =
																											BgL_head1223z00_468;
																									BgL_zc3z04anonymousza31656ze3z87_472:
																										if (NULLP
																											(BgL_l1221z00_470))
																											{	/* Jas/labels.scm 101 */
																												BgL_runner1689z00_483 =
																													CDR
																													(BgL_head1223z00_468);
																											}
																										else
																											{	/* Jas/labels.scm 101 */
																												obj_t
																													BgL_newtail1225z00_474;
																												{	/* Jas/labels.scm 101 */
																													obj_t
																														BgL_arg1663z00_476;
																													{	/* Jas/labels.scm 101 */
																														obj_t BgL_xz00_477;

																														BgL_xz00_477 =
																															CAR(
																															((obj_t)
																																BgL_l1221z00_470));
																														{	/* Jas/labels.scm 102 */
																															obj_t
																																BgL_arg1675z00_478;
																															obj_t
																																BgL_arg1678z00_479;
																															{	/* Jas/labels.scm 102 */
																																obj_t
																																	BgL_arg1681z00_480;
																																BgL_arg1681z00_480
																																	=
																																	CAR(((obj_t)
																																		BgL_xz00_477));
																																BgL_arg1675z00_478
																																	=
																																	BGl_u4z00zzjas_libz00
																																	(CINT
																																	(BgL_arg1681z00_480));
																															}
																															{	/* Jas/labels.scm 103 */
																																obj_t
																																	BgL_arg1688z00_481;
																																BgL_arg1688z00_481
																																	=
																																	CDR(((obj_t)
																																		BgL_xz00_477));
																																BgL_arg1678z00_479
																																	=
																																	BGl_resolvezd2target2ze70z35zzjas_labelsz00
																																	(BgL_pcz00_407,
																																	BgL_classfilez00_5,
																																	BgL_envz00_403,
																																	BgL_arg1688z00_481);
																															}
																															BgL_arg1663z00_476
																																=
																																BGl_appendzd221011zd2zzjas_labelsz00
																																(BgL_arg1675z00_478,
																																BgL_arg1678z00_479);
																														}
																													}
																													BgL_newtail1225z00_474
																														=
																														MAKE_YOUNG_PAIR
																														(BgL_arg1663z00_476,
																														BNIL);
																												}
																												SET_CDR
																													(BgL_tail1224z00_471,
																													BgL_newtail1225z00_474);
																												{	/* Jas/labels.scm 101 */
																													obj_t
																														BgL_arg1661z00_475;
																													BgL_arg1661z00_475 =
																														CDR(((obj_t)
																															BgL_l1221z00_470));
																													{
																														obj_t
																															BgL_tail1224z00_1549;
																														obj_t
																															BgL_l1221z00_1548;
																														BgL_l1221z00_1548 =
																															BgL_arg1661z00_475;
																														BgL_tail1224z00_1549
																															=
																															BgL_newtail1225z00_474;
																														BgL_tail1224z00_471
																															=
																															BgL_tail1224z00_1549;
																														BgL_l1221z00_470 =
																															BgL_l1221z00_1548;
																														goto
																															BgL_zc3z04anonymousza31656ze3z87_472;
																													}
																												}
																											}
																									}
																								}
																						}
																						BgL_arg1650z00_463 =
																							BGl_appendz00zz__r4_pairs_and_lists_6_3z00
																							(BgL_runner1689z00_483);
																					}
																					BgL_arg1630z00_460 =
																						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																						(BgL_arg1646z00_462,
																						BgL_arg1650z00_463);
																				}
																				BgL_arg1627z00_458 =
																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																					(BgL_arg1629z00_459,
																					BgL_arg1630z00_460);
																			}
																			BgL_arg1625z00_456 =
																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																				(BgL_arg1626z00_457,
																				BgL_arg1627z00_458);
																		}
																		BgL_arg1767z00_566 =
																			MAKE_YOUNG_PAIR(BINT(171L),
																			BgL_arg1625z00_456);
																	}
																	break;
																case 202L:

																	{
																		obj_t BgL_begz00_484;
																		obj_t BgL_endz00_485;
																		obj_t BgL_labz00_486;
																		obj_t BgL_typez00_487;

																		{	/* Jas/labels.scm 106 */
																			obj_t BgL_ezd2167zd2_489;

																			BgL_ezd2167zd2_489 =
																				CDR(((obj_t) BgL_insz00_406));
																			if (PAIRP(BgL_ezd2167zd2_489))
																				{	/* Jas/labels.scm 105 */
																					obj_t BgL_cdrzd2177zd2_491;

																					BgL_cdrzd2177zd2_491 =
																						CDR(BgL_ezd2167zd2_489);
																					if (PAIRP(BgL_cdrzd2177zd2_491))
																						{	/* Jas/labels.scm 105 */
																							obj_t BgL_cdrzd2183zd2_493;

																							BgL_cdrzd2183zd2_493 =
																								CDR(BgL_cdrzd2177zd2_491);
																							if (PAIRP(BgL_cdrzd2183zd2_493))
																								{	/* Jas/labels.scm 105 */
																									obj_t BgL_cdrzd2188zd2_495;

																									BgL_cdrzd2188zd2_495 =
																										CDR(BgL_cdrzd2183zd2_493);
																									if (PAIRP
																										(BgL_cdrzd2188zd2_495))
																										{	/* Jas/labels.scm 105 */
																											if (NULLP(CDR
																													(BgL_cdrzd2188zd2_495)))
																												{	/* Jas/labels.scm 105 */
																													BgL_begz00_484 =
																														CAR
																														(BgL_ezd2167zd2_489);
																													BgL_endz00_485 =
																														CAR
																														(BgL_cdrzd2177zd2_491);
																													BgL_labz00_486 =
																														CAR
																														(BgL_cdrzd2183zd2_493);
																													BgL_typez00_487 =
																														CAR
																														(BgL_cdrzd2188zd2_495);
																													{	/* Jas/labels.scm 108 */
																														obj_t
																															BgL_arg1709z00_504;
																														obj_t
																															BgL_arg1710z00_505;
																														obj_t
																															BgL_arg1711z00_506;
																														BgL_arg1709z00_504 =
																															BGl_getzd2targetze70z35zzjas_labelsz00
																															(BgL_classfilez00_5,
																															BgL_envz00_403,
																															BgL_begz00_484);
																														BgL_arg1710z00_505 =
																															BGl_getzd2targetze70z35zzjas_labelsz00
																															(BgL_classfilez00_5,
																															BgL_envz00_403,
																															BgL_endz00_485);
																														BgL_arg1711z00_506 =
																															BGl_getzd2targetze70z35zzjas_labelsz00
																															(BgL_classfilez00_5,
																															BgL_envz00_403,
																															BgL_labz00_486);
																														{	/* Jas/labels.scm 108 */
																															obj_t
																																BgL_list1712z00_507;
																															{	/* Jas/labels.scm 108 */
																																obj_t
																																	BgL_arg1714z00_508;
																																{	/* Jas/labels.scm 108 */
																																	obj_t
																																		BgL_arg1717z00_509;
																																	{	/* Jas/labels.scm 108 */
																																		obj_t
																																			BgL_arg1718z00_510;
																																		{	/* Jas/labels.scm 108 */
																																			obj_t
																																				BgL_arg1720z00_511;
																																			BgL_arg1720z00_511
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_typez00_487,
																																				BNIL);
																																			BgL_arg1718z00_510
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1711z00_506,
																																				BgL_arg1720z00_511);
																																		}
																																		BgL_arg1717z00_509
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1710z00_505,
																																			BgL_arg1718z00_510);
																																	}
																																	BgL_arg1714z00_508
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1709z00_504,
																																		BgL_arg1717z00_509);
																																}
																																BgL_list1712z00_507
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BINT(202L),
																																	BgL_arg1714z00_508);
																															}
																															BgL_arg1767z00_566
																																=
																																BgL_list1712z00_507;
																														}
																													}
																												}
																											else
																												{	/* Jas/labels.scm 105 */
																													BgL_arg1767z00_566 =
																														BFALSE;
																												}
																										}
																									else
																										{	/* Jas/labels.scm 105 */
																											BgL_arg1767z00_566 =
																												BFALSE;
																										}
																								}
																							else
																								{	/* Jas/labels.scm 105 */
																									BgL_arg1767z00_566 = BFALSE;
																								}
																						}
																					else
																						{	/* Jas/labels.scm 105 */
																							BgL_arg1767z00_566 = BFALSE;
																						}
																				}
																			else
																				{	/* Jas/labels.scm 105 */
																					BgL_arg1767z00_566 = BFALSE;
																				}
																		}
																	}
																	break;
																case 205L:

																	{
																		obj_t BgL_begz00_512;
																		obj_t BgL_endz00_513;
																		obj_t BgL_restz00_514;

																		{	/* Jas/labels.scm 111 */
																			obj_t BgL_ezd2195zd2_516;

																			BgL_ezd2195zd2_516 =
																				CDR(((obj_t) BgL_insz00_406));
																			if (PAIRP(BgL_ezd2195zd2_516))
																				{	/* Jas/labels.scm 110 */
																					obj_t BgL_cdrzd2203zd2_518;

																					BgL_cdrzd2203zd2_518 =
																						CDR(BgL_ezd2195zd2_516);
																					if (PAIRP(BgL_cdrzd2203zd2_518))
																						{	/* Jas/labels.scm 110 */
																							BgL_begz00_512 =
																								CAR(BgL_ezd2195zd2_516);
																							BgL_endz00_513 =
																								CAR(BgL_cdrzd2203zd2_518);
																							BgL_restz00_514 =
																								CDR(BgL_cdrzd2203zd2_518);
																							{	/* Jas/labels.scm 113 */
																								obj_t BgL_bz00_523;
																								obj_t BgL_ez00_524;

																								BgL_bz00_523 =
																									BGl_getzd2targetze70z35zzjas_labelsz00
																									(BgL_classfilez00_5,
																									BgL_envz00_403,
																									BgL_begz00_512);
																								BgL_ez00_524 =
																									BGl_getzd2targetze70z35zzjas_labelsz00
																									(BgL_classfilez00_5,
																									BgL_envz00_403,
																									BgL_endz00_513);
																								{	/* Jas/labels.scm 114 */
																									obj_t BgL_arg1735z00_525;

																									{	/* Jas/labels.scm 114 */
																										obj_t BgL_arg1736z00_526;

																										{	/* Jas/labels.scm 114 */
																											long BgL_arg1737z00_527;

																											BgL_arg1737z00_527 =
																												(
																												(long)
																												CINT(BgL_ez00_524) -
																												(long)
																												CINT(BgL_bz00_523));
																											BgL_arg1736z00_526 =
																												MAKE_YOUNG_PAIR(BINT
																												(BgL_arg1737z00_527),
																												BgL_restz00_514);
																										}
																										BgL_arg1735z00_525 =
																											MAKE_YOUNG_PAIR
																											(BgL_bz00_523,
																											BgL_arg1736z00_526);
																									}
																									BgL_arg1767z00_566 =
																										MAKE_YOUNG_PAIR(BINT(205L),
																										BgL_arg1735z00_525);
																						}}}
																					else
																						{	/* Jas/labels.scm 110 */
																							BgL_arg1767z00_566 = BFALSE;
																						}
																				}
																			else
																				{	/* Jas/labels.scm 110 */
																					BgL_arg1767z00_566 = BFALSE;
																				}
																		}
																	}
																	break;
																default:
																	BgL_arg1767z00_566 = BgL_insz00_406;
																}
														}
													else
														{	/* Jas/labels.scm 86 */
															BgL_arg1767z00_566 = BgL_insz00_406;
														}
												}
											}
										}
										BgL_arg1761z00_562 =
											MAKE_YOUNG_PAIR(BgL_arg1767z00_566, BgL_donez00_554);
									}
									{
										obj_t BgL_donez00_1610;
										obj_t BgL_pcz00_1609;
										obj_t BgL_lz00_1608;

										BgL_lz00_1608 = BgL_arg1754z00_560;
										BgL_pcz00_1609 = BgL_arg1755z00_561;
										BgL_donez00_1610 = BgL_arg1761z00_562;
										BgL_donez00_554 = BgL_donez00_1610;
										BgL_pcz00_553 = BgL_pcz00_1609;
										BgL_lz00_552 = BgL_lz00_1608;
										goto BgL_zc3z04anonymousza31748ze3z87_555;
									}
								}
						}
				}
			}
		}

	}



/* resolve-target2~0 */
	obj_t BGl_resolvezd2target2ze70z35zzjas_labelsz00(obj_t BgL_pcz00_1123,
		obj_t BgL_classfilez00_1122, obj_t BgL_envz00_1121, obj_t BgL_labelz00_544)
	{
		{	/* Jas/labels.scm 85 */
			{	/* Jas/labels.scm 85 */
				obj_t BgL_arg1746z00_546;

				{	/* Jas/labels.scm 85 */
					obj_t BgL_a1210z00_547;

					BgL_a1210z00_547 =
						BGl_getzd2targetze70z35zzjas_labelsz00(BgL_classfilez00_1122,
						BgL_envz00_1121, BgL_labelz00_544);
					{	/* Jas/labels.scm 85 */
						bool_t BgL_test1921z00_1613;

						if (INTEGERP(BgL_a1210z00_547))
							{	/* Jas/labels.scm 85 */
								BgL_test1921z00_1613 = INTEGERP(BgL_pcz00_1123);
							}
						else
							{	/* Jas/labels.scm 85 */
								BgL_test1921z00_1613 = ((bool_t) 0);
							}
						if (BgL_test1921z00_1613)
							{	/* Jas/labels.scm 85 */
								obj_t BgL_tmpz00_895;

								BgL_tmpz00_895 = BINT(0L);
								if (BGL_SUBFX_OV(BgL_a1210z00_547, BgL_pcz00_1123,
										BgL_tmpz00_895))
									{	/* Jas/labels.scm 85 */
										BgL_arg1746z00_546 =
											bgl_bignum_sub(bgl_long_to_bignum(
												(long) CINT(BgL_a1210z00_547)),
											bgl_long_to_bignum((long) CINT(BgL_pcz00_1123)));
									}
								else
									{	/* Jas/labels.scm 85 */
										BgL_arg1746z00_546 = BgL_tmpz00_895;
									}
							}
						else
							{	/* Jas/labels.scm 85 */
								BgL_arg1746z00_546 =
									BGl_2zd2zd2zz__r4_numbers_6_5z00(BgL_a1210z00_547,
									BgL_pcz00_1123);
							}
					}
				}
				return BGl_u4z00zzjas_libz00(CINT(BgL_arg1746z00_546));
			}
		}

	}



/* get-target~0 */
	obj_t BGl_getzd2targetze70z35zzjas_labelsz00(obj_t BgL_classfilez00_1125,
		obj_t BgL_envz00_1124, obj_t BgL_labelz00_528)
	{
		{	/* Jas/labels.scm 78 */
			{	/* Jas/labels.scm 77 */
				obj_t BgL_arg1739z00_530;

				{	/* Jas/labels.scm 77 */
					obj_t BgL__ortest_1061z00_531;

					BgL__ortest_1061z00_531 =
						BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_labelz00_528,
						BgL_envz00_1124);
					if (CBOOL(BgL__ortest_1061z00_531))
						{	/* Jas/labels.scm 77 */
							BgL_arg1739z00_530 = BgL__ortest_1061z00_531;
						}
					else
						{	/* Jas/labels.scm 77 */
							BgL_arg1739z00_530 =
								BGl_jaszd2errorzd2zzjas_classfilez00(
								((BgL_classfilez00_bglt) BgL_classfilez00_1125),
								BGl_string1854z00zzjas_labelsz00, BgL_labelz00_528);
						}
				}
				return CDR(((obj_t) BgL_arg1739z00_530));
			}
		}

	}



/* padding */
	obj_t BGl_paddingz00zzjas_labelsz00(obj_t BgL_pcz00_7)
	{
		{	/* Jas/labels.scm 125 */
			{	/* Jas/labels.scm 126 */
				long BgL_aux1065z00_572;

				BgL_aux1065z00_572 =
					BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
					(long) CINT(BgL_pcz00_7), 4L);
				switch (BgL_aux1065z00_572)
					{
					case 0L:

						return CNST_TABLE_REF(2);
						break;
					case 1L:

						return CNST_TABLE_REF(3);
						break;
					case 2L:

						return CNST_TABLE_REF(4);
						break;
					default:
						return BNIL;
					}
			}
		}

	}



/* size-ins */
	obj_t BGl_siza7ezd2insz75zzjas_labelsz00(obj_t BgL_insz00_8,
		obj_t BgL_pcz00_9)
	{
		{	/* Jas/labels.scm 150 */
			{	/* Jas/labels.scm 151 */
				obj_t BgL_siza7eza7_573;

				{	/* Jas/labels.scm 151 */
					obj_t BgL_arg1812z00_608;

					BgL_arg1812z00_608 = CAR(((obj_t) BgL_insz00_8));
					BgL_siza7eza7_573 =
						VECTOR_REF(BGl_za2opcodezd2siza7eza2z75zzjas_labelsz00,
						(long) CINT(BgL_arg1812z00_608));
				}
				{	/* Jas/labels.scm 152 */
					bool_t BgL_test1925z00_1645;

					if (INTEGERP(BgL_siza7eza7_573))
						{	/* Jas/labels.scm 152 */
							BgL_test1925z00_1645 = ((long) CINT(BgL_siza7eza7_573) == 0L);
						}
					else
						{	/* Jas/labels.scm 152 */
							BgL_test1925z00_1645 =
								BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_siza7eza7_573, BINT(0L));
						}
					if (BgL_test1925z00_1645)
						{	/* Jas/labels.scm 153 */
							obj_t BgL_aux1067z00_576;

							BgL_aux1067z00_576 = CAR(((obj_t) BgL_insz00_8));
							if (INTEGERP(BgL_aux1067z00_576))
								{	/* Jas/labels.scm 153 */
									switch ((long) CINT(BgL_aux1067z00_576))
										{
										case 170L:

											{	/* Jas/labels.scm 155 */
												obj_t BgL_b1232z00_578;

												{	/* Jas/labels.scm 155 */
													long BgL_a1229z00_580;

													BgL_a1229z00_580 =
														bgl_list_length(BGl_paddingz00zzjas_labelsz00
														(BgL_pcz00_9));
													{	/* Jas/labels.scm 156 */
														obj_t BgL_b1230z00_581;

														{	/* Jas/labels.scm 156 */
															long BgL_b1228z00_584;

															BgL_b1228z00_584 = bgl_list_length(BgL_insz00_8);
															{	/* Jas/labels.scm 156 */

																{	/* Jas/labels.scm 156 */
																	obj_t BgL_za71za7_1022;
																	obj_t BgL_za72za7_1023;

																	BgL_za71za7_1022 = BINT(4L);
																	BgL_za72za7_1023 = BINT(BgL_b1228z00_584);
																	{	/* Jas/labels.scm 156 */
																		obj_t BgL_tmpz00_1024;

																		BgL_tmpz00_1024 = BINT(0L);
																		{	/* Jas/labels.scm 156 */
																			bool_t BgL_test1928z00_1662;

																			{	/* Jas/labels.scm 156 */
																				long BgL_tmpz00_1663;

																				BgL_tmpz00_1663 =
																					(long) CINT(BgL_za72za7_1023);
																				BgL_test1928z00_1662 =
																					BGL_MULFX_OV(BgL_za71za7_1022,
																					BgL_tmpz00_1663, BgL_tmpz00_1024);
																			}
																			if (BgL_test1928z00_1662)
																				{	/* Jas/labels.scm 156 */
																					BgL_b1230z00_581 =
																						bgl_bignum_mul(bgl_long_to_bignum(
																							(long) CINT(BgL_za71za7_1022)),
																						bgl_long_to_bignum(
																							(long) CINT(BgL_za72za7_1023)));
																				}
																			else
																				{	/* Jas/labels.scm 156 */
																					BgL_b1230z00_581 = BgL_tmpz00_1024;
																				}
																		}
																	}
																}
															}
														}
														{	/* Jas/labels.scm 155 */

															if (INTEGERP(BgL_b1230z00_581))
																{	/* Jas/labels.scm 155 */
																	obj_t BgL_za71za7_1032;

																	BgL_za71za7_1032 = BINT(BgL_a1229z00_580);
																	{	/* Jas/labels.scm 155 */
																		obj_t BgL_tmpz00_1034;

																		BgL_tmpz00_1034 = BINT(0L);
																		if (BGL_ADDFX_OV(BgL_za71za7_1032,
																				BgL_b1230z00_581, BgL_tmpz00_1034))
																			{	/* Jas/labels.scm 155 */
																				BgL_b1232z00_578 =
																					bgl_bignum_add(bgl_long_to_bignum(
																						(long) CINT(BgL_za71za7_1032)),
																					bgl_long_to_bignum(
																						(long) CINT(BgL_b1230z00_581)));
																			}
																		else
																			{	/* Jas/labels.scm 155 */
																				BgL_b1232z00_578 = BgL_tmpz00_1034;
																			}
																	}
																}
															else
																{	/* Jas/labels.scm 155 */
																	BgL_b1232z00_578 =
																		BGl_2zb2zb2zz__r4_numbers_6_5z00(BINT
																		(BgL_a1229z00_580), BgL_b1230z00_581);
																}
														}
													}
												}
												{	/* Jas/labels.scm 155 */

													if (INTEGERP(BgL_b1232z00_578))
														{	/* Jas/labels.scm 155 */
															obj_t BgL_za71za7_1042;

															BgL_za71za7_1042 = BINT(1L);
															{	/* Jas/labels.scm 155 */
																obj_t BgL_tmpz00_1044;

																BgL_tmpz00_1044 = BINT(0L);
																if (BGL_ADDFX_OV(BgL_za71za7_1042,
																		BgL_b1232z00_578, BgL_tmpz00_1044))
																	{	/* Jas/labels.scm 155 */
																		return
																			bgl_bignum_add(bgl_long_to_bignum(
																				(long) CINT(BgL_za71za7_1042)),
																			bgl_long_to_bignum(
																				(long) CINT(BgL_b1232z00_578)));
																	}
																else
																	{	/* Jas/labels.scm 155 */
																		return BgL_tmpz00_1044;
																	}
															}
														}
													else
														{	/* Jas/labels.scm 155 */
															BGL_TAIL return
																BGl_2zb2zb2zz__r4_numbers_6_5z00(BINT(1L),
																BgL_b1232z00_578);
														}
												}
											}
											break;
										case 171L:

											{	/* Jas/labels.scm 158 */
												obj_t BgL_b1240z00_588;

												{	/* Jas/labels.scm 158 */
													long BgL_a1237z00_590;

													BgL_a1237z00_590 =
														bgl_list_length(BGl_paddingz00zzjas_labelsz00
														(BgL_pcz00_9));
													{	/* Jas/labels.scm 158 */
														obj_t BgL_b1238z00_591;

														{	/* Jas/labels.scm 159 */
															obj_t BgL_b1236z00_594;

															{	/* Jas/labels.scm 159 */
																long BgL_b1234z00_597;

																{	/* Jas/labels.scm 159 */
																	obj_t BgL_auxz00_1699;

																	{	/* Jas/labels.scm 159 */
																		obj_t BgL_pairz00_1055;

																		BgL_pairz00_1055 =
																			CDR(((obj_t) BgL_insz00_8));
																		BgL_auxz00_1699 = CDR(BgL_pairz00_1055);
																	}
																	BgL_b1234z00_597 =
																		bgl_list_length(BgL_auxz00_1699);
																}
																{	/* Jas/labels.scm 159 */

																	{	/* Jas/labels.scm 159 */
																		obj_t BgL_za71za7_1056;
																		obj_t BgL_za72za7_1057;

																		BgL_za71za7_1056 = BINT(8L);
																		BgL_za72za7_1057 = BINT(BgL_b1234z00_597);
																		{	/* Jas/labels.scm 159 */
																			obj_t BgL_tmpz00_1058;

																			BgL_tmpz00_1058 = BINT(0L);
																			{	/* Jas/labels.scm 159 */
																				bool_t BgL_test1933z00_1707;

																				{	/* Jas/labels.scm 159 */
																					long BgL_tmpz00_1708;

																					BgL_tmpz00_1708 =
																						(long) CINT(BgL_za72za7_1057);
																					BgL_test1933z00_1707 =
																						BGL_MULFX_OV(BgL_za71za7_1056,
																						BgL_tmpz00_1708, BgL_tmpz00_1058);
																				}
																				if (BgL_test1933z00_1707)
																					{	/* Jas/labels.scm 159 */
																						BgL_b1236z00_594 =
																							bgl_bignum_mul(bgl_long_to_bignum(
																								(long) CINT(BgL_za71za7_1056)),
																							bgl_long_to_bignum(
																								(long) CINT(BgL_za72za7_1057)));
																					}
																				else
																					{	/* Jas/labels.scm 159 */
																						BgL_b1236z00_594 = BgL_tmpz00_1058;
																					}
																			}
																		}
																	}
																}
															}
															{	/* Jas/labels.scm 158 */

																if (INTEGERP(BgL_b1236z00_594))
																	{	/* Jas/labels.scm 158 */
																		obj_t BgL_za71za7_1066;

																		BgL_za71za7_1066 = BINT(8L);
																		{	/* Jas/labels.scm 158 */
																			obj_t BgL_tmpz00_1068;

																			BgL_tmpz00_1068 = BINT(0L);
																			if (BGL_ADDFX_OV(BgL_za71za7_1066,
																					BgL_b1236z00_594, BgL_tmpz00_1068))
																				{	/* Jas/labels.scm 158 */
																					BgL_b1238z00_591 =
																						bgl_bignum_add(bgl_long_to_bignum(
																							(long) CINT(BgL_za71za7_1066)),
																						bgl_long_to_bignum(
																							(long) CINT(BgL_b1236z00_594)));
																				}
																			else
																				{	/* Jas/labels.scm 158 */
																					BgL_b1238z00_591 = BgL_tmpz00_1068;
																				}
																		}
																	}
																else
																	{	/* Jas/labels.scm 158 */
																		BgL_b1238z00_591 =
																			BGl_2zb2zb2zz__r4_numbers_6_5z00(BINT(8L),
																			BgL_b1236z00_594);
																	}
															}
														}
														{	/* Jas/labels.scm 158 */

															if (INTEGERP(BgL_b1238z00_591))
																{	/* Jas/labels.scm 158 */
																	obj_t BgL_za71za7_1076;

																	BgL_za71za7_1076 = BINT(BgL_a1237z00_590);
																	{	/* Jas/labels.scm 158 */
																		obj_t BgL_tmpz00_1078;

																		BgL_tmpz00_1078 = BINT(0L);
																		if (BGL_ADDFX_OV(BgL_za71za7_1076,
																				BgL_b1238z00_591, BgL_tmpz00_1078))
																			{	/* Jas/labels.scm 158 */
																				BgL_b1240z00_588 =
																					bgl_bignum_add(bgl_long_to_bignum(
																						(long) CINT(BgL_za71za7_1076)),
																					bgl_long_to_bignum(
																						(long) CINT(BgL_b1238z00_591)));
																			}
																		else
																			{	/* Jas/labels.scm 158 */
																				BgL_b1240z00_588 = BgL_tmpz00_1078;
																			}
																	}
																}
															else
																{	/* Jas/labels.scm 158 */
																	BgL_b1240z00_588 =
																		BGl_2zb2zb2zz__r4_numbers_6_5z00(BINT
																		(BgL_a1237z00_590), BgL_b1238z00_591);
																}
														}
													}
												}
												{	/* Jas/labels.scm 158 */

													if (INTEGERP(BgL_b1240z00_588))
														{	/* Jas/labels.scm 158 */
															obj_t BgL_za71za7_1086;

															BgL_za71za7_1086 = BINT(1L);
															{	/* Jas/labels.scm 158 */
																obj_t BgL_tmpz00_1088;

																BgL_tmpz00_1088 = BINT(0L);
																if (BGL_ADDFX_OV(BgL_za71za7_1086,
																		BgL_b1240z00_588, BgL_tmpz00_1088))
																	{	/* Jas/labels.scm 158 */
																		return
																			bgl_bignum_add(bgl_long_to_bignum(
																				(long) CINT(BgL_za71za7_1086)),
																			bgl_long_to_bignum(
																				(long) CINT(BgL_b1240z00_588)));
																	}
																else
																	{	/* Jas/labels.scm 158 */
																		return BgL_tmpz00_1088;
																	}
															}
														}
													else
														{	/* Jas/labels.scm 158 */
															BGL_TAIL return
																BGl_2zb2zb2zz__r4_numbers_6_5z00(BINT(1L),
																BgL_b1240z00_588);
														}
												}
											}
											break;
										case 196L:

											{	/* Jas/labels.scm 161 */
												bool_t BgL_test1940z00_1755;

												{	/* Jas/labels.scm 161 */
													obj_t BgL_a1241z00_605;

													{	/* Jas/labels.scm 161 */
														obj_t BgL_pairz00_1099;

														BgL_pairz00_1099 = CDR(((obj_t) BgL_insz00_8));
														BgL_a1241z00_605 = CAR(BgL_pairz00_1099);
													}
													{	/* Jas/labels.scm 161 */

														if (INTEGERP(BgL_a1241z00_605))
															{	/* Jas/labels.scm 161 */
																BgL_test1940z00_1755 =
																	((long) CINT(BgL_a1241z00_605) == 132L);
															}
														else
															{	/* Jas/labels.scm 161 */
																BgL_test1940z00_1755 =
																	BGl_2zd3zd3zz__r4_numbers_6_5z00
																	(BgL_a1241z00_605, BINT(132L));
															}
													}
												}
												if (BgL_test1940z00_1755)
													{	/* Jas/labels.scm 161 */
														return BINT(6L);
													}
												else
													{	/* Jas/labels.scm 161 */
														return BINT(4L);
													}
											}
											break;
										case 202L:

											return BINT(0L);
											break;
										case 203L:

											return BINT(0L);
											break;
										case 204L:

											return BINT(0L);
											break;
										case 205L:

											return BINT(0L);
											break;
										default:
											return BUNSPEC;
										}
								}
							else
								{	/* Jas/labels.scm 153 */
									return BUNSPEC;
								}
						}
					else
						{	/* Jas/labels.scm 152 */
							return BgL_siza7eza7_573;
						}
				}
			}
		}

	}



/* resolve-pc */
	obj_t BGl_resolvezd2pczd2zzjas_labelsz00(obj_t BgL_classfilez00_10,
		obj_t BgL_codez00_11)
	{
		{	/* Jas/labels.scm 172 */
			{
				obj_t BgL_lz00_610;
				obj_t BgL_envz00_611;
				obj_t BgL_pcz00_612;

				BgL_lz00_610 = BgL_codez00_11;
				BgL_envz00_611 = BNIL;
				BgL_pcz00_612 = BINT(0L);
			BgL_zc3z04anonymousza31813ze3z87_613:
				if (NULLP(BgL_lz00_610))
					{	/* Jas/labels.scm 174 */
						return BgL_envz00_611;
					}
				else
					{	/* Jas/labels.scm 176 */
						bool_t BgL_test1943z00_1775;

						{	/* Jas/labels.scm 176 */
							obj_t BgL_tmpz00_1776;

							BgL_tmpz00_1776 = CAR(((obj_t) BgL_lz00_610));
							BgL_test1943z00_1775 = SYMBOLP(BgL_tmpz00_1776);
						}
						if (BgL_test1943z00_1775)
							{	/* Jas/labels.scm 177 */
								obj_t BgL_arg1822z00_617;
								obj_t BgL_arg1823z00_618;

								BgL_arg1822z00_617 = CDR(((obj_t) BgL_lz00_610));
								{	/* Jas/labels.scm 177 */
									obj_t BgL_arg1831z00_619;

									{	/* Jas/labels.scm 177 */
										obj_t BgL_arg1832z00_620;

										BgL_arg1832z00_620 = CAR(((obj_t) BgL_lz00_610));
										BgL_arg1831z00_619 =
											MAKE_YOUNG_PAIR(BgL_arg1832z00_620, BgL_pcz00_612);
									}
									BgL_arg1823z00_618 =
										MAKE_YOUNG_PAIR(BgL_arg1831z00_619, BgL_envz00_611);
								}
								{
									obj_t BgL_envz00_1787;
									obj_t BgL_lz00_1786;

									BgL_lz00_1786 = BgL_arg1822z00_617;
									BgL_envz00_1787 = BgL_arg1823z00_618;
									BgL_envz00_611 = BgL_envz00_1787;
									BgL_lz00_610 = BgL_lz00_1786;
									goto BgL_zc3z04anonymousza31813ze3z87_613;
								}
							}
						else
							{	/* Jas/labels.scm 179 */
								obj_t BgL_arg1833z00_621;
								obj_t BgL_arg1834z00_622;

								BgL_arg1833z00_621 = CDR(((obj_t) BgL_lz00_610));
								{	/* Jas/labels.scm 179 */
									obj_t BgL_b1243z00_623;

									{	/* Jas/labels.scm 179 */
										obj_t BgL_arg1836z00_625;

										BgL_arg1836z00_625 = CAR(((obj_t) BgL_lz00_610));
										BgL_b1243z00_623 =
											BGl_siza7ezd2insz75zzjas_labelsz00(BgL_arg1836z00_625,
											BgL_pcz00_612);
									}
									{	/* Jas/labels.scm 179 */
										bool_t BgL_test1944z00_1793;

										if (INTEGERP(BgL_pcz00_612))
											{	/* Jas/labels.scm 179 */
												BgL_test1944z00_1793 = INTEGERP(BgL_b1243z00_623);
											}
										else
											{	/* Jas/labels.scm 179 */
												BgL_test1944z00_1793 = ((bool_t) 0);
											}
										if (BgL_test1944z00_1793)
											{	/* Jas/labels.scm 179 */
												obj_t BgL_tmpz00_1109;

												BgL_tmpz00_1109 = BINT(0L);
												if (BGL_ADDFX_OV(BgL_pcz00_612, BgL_b1243z00_623,
														BgL_tmpz00_1109))
													{	/* Jas/labels.scm 179 */
														BgL_arg1834z00_622 =
															bgl_bignum_add(bgl_long_to_bignum(
																(long) CINT(BgL_pcz00_612)),
															bgl_long_to_bignum(
																(long) CINT(BgL_b1243z00_623)));
													}
												else
													{	/* Jas/labels.scm 179 */
														BgL_arg1834z00_622 = BgL_tmpz00_1109;
													}
											}
										else
											{	/* Jas/labels.scm 179 */
												BgL_arg1834z00_622 =
													BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_pcz00_612,
													BgL_b1243z00_623);
											}
									}
								}
								{
									obj_t BgL_pcz00_1807;
									obj_t BgL_lz00_1806;

									BgL_lz00_1806 = BgL_arg1833z00_621;
									BgL_pcz00_1807 = BgL_arg1834z00_622;
									BgL_pcz00_612 = BgL_pcz00_1807;
									BgL_lz00_610 = BgL_lz00_1806;
									goto BgL_zc3z04anonymousza31813ze3z87_613;
								}
							}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzjas_labelsz00(void)
	{
		{	/* Jas/labels.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzjas_labelsz00(void)
	{
		{	/* Jas/labels.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzjas_labelsz00(void)
	{
		{	/* Jas/labels.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzjas_labelsz00(void)
	{
		{	/* Jas/labels.scm 1 */
			BGl_modulezd2initializa7ationz75zzjas_libz00(267157509L,
				BSTRING_TO_STRING(BGl_string1856z00zzjas_labelsz00));
			return
				BGl_modulezd2initializa7ationz75zzjas_classfilez00(135094074L,
				BSTRING_TO_STRING(BGl_string1856z00zzjas_labelsz00));
		}

	}

#ifdef __cplusplus
}
#endif
