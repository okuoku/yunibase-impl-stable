/*===========================================================================*/
/*   (Jas/profile.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Jas/profile.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_JAS_PROFILE_TYPE_DEFINITIONS
#define BGL_JAS_PROFILE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL__envz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_thisz00;
		obj_t BgL_jstringz00;
		obj_t BgL_has_clinitz00;
		obj_t BgL_clinitz00;
		obj_t BgL_resz00;
		obj_t BgL_namesz00;
		obj_t BgL_lnamesz00;
		obj_t BgL_linesz00;
	}              *BgL__envz00_bglt;


#endif													// BGL_JAS_PROFILE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_profilezd2makezd2namesz00zzjas_profilez00(long, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31951ze3ze5zzjas_profilez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzjas_profilez00 = BUNSPEC;
	static BgL__envz00_bglt BGl_z62lambda1947z62zzjas_profilez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL__envz00_bglt BGl_z62lambda1949z62zzjas_profilez00(obj_t);
	static obj_t BGl_z62lambda1955z62zzjas_profilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1956z62zzjas_profilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1960z62zzjas_profilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1961z62zzjas_profilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_profilezd2codezd2zzjas_profilez00(BgL__envz00_bglt, long,
		obj_t);
	static obj_t BGl_z62lambda1965z62zzjas_profilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1966z62zzjas_profilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzjas_profilez00(void);
	static obj_t BGl_z62lambda1970z62zzjas_profilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1971z62zzjas_profilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1975z62zzjas_profilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1976z62zzjas_profilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1980z62zzjas_profilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1981z62zzjas_profilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1985z62zzjas_profilez00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzjas_profilez00(void);
	static obj_t BGl_z62lambda1986z62zzjas_profilez00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzjas_profilez00(void);
	static obj_t BGl_z62lambda1990z62zzjas_profilez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1991z62zzjas_profilez00(obj_t, obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2zc3zc3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_jaszd2profilezd2zzjas_profilez00(obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_profilezd2infoszd2zzjas_profilez00(BgL__envz00_bglt, obj_t);
	static obj_t BGl_profilezd2extrazd2clinitz00zzjas_profilez00(BgL__envz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzjas_profilez00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzjas_profilez00(void);
	static obj_t BGl_profilezd2makezd2resz00zzjas_profilez00(long, obj_t, obj_t);
	static obj_t BGl_getzd2idze70z35zzjas_profilez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62jaszd2profilezb0zzjas_profilez00(obj_t, obj_t);
	static obj_t BGl_profilingz00zzjas_profilez00(obj_t);
	static obj_t BGl_profilezd2makezd2linesz00zzjas_profilez00(long, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzjas_profilez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_libz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static BgL__envz00_bglt BGl_makezd2envzd2zzjas_profilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_profilezd2declarationszd2zzjas_profilez00(BgL__envz00_bglt,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzjas_profilez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzjas_profilez00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzjas_profilez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzjas_profilez00(void);
	BGL_IMPORT obj_t BGl_objectz00zz__objectz00;
	static obj_t BGl_mfze70ze7zzjas_profilez00(BgL__envz00_bglt, obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2jaszd2profilezd2modeza2z00zzjas_profilez00 =
		BUNSPEC;
	static obj_t BGl_profilezd2methodszd2zzjas_profilez00(BgL__envz00_bglt,
		obj_t);
	static obj_t BGl_pushzd2intzd2zzjas_profilez00(obj_t);
	static obj_t BGl__envz00zzjas_profilez00 = BUNSPEC;
	static obj_t __cnst[54];


	   
		 
		DEFINE_STRING(BGl_string1992z00zzjas_profilez00,
		BgL_bgl_string1992za700za7za7j2030za7, "jas", 3);
	      DEFINE_STRING(BGl_string1993z00zzjas_profilez00,
		BgL_bgl_string1993za700za7za7j2031za7, "bad module definition", 21);
	      DEFINE_STRING(BGl_string1994z00zzjas_profilez00,
		BgL_bgl_string1994za700za7za7j2032za7, "undef", 5);
	      DEFINE_STRING(BGl_string1995z00zzjas_profilez00,
		BgL_bgl_string1995za700za7za7j2033za7, "__prof", 6);
	      DEFINE_STRING(BGl_string1996z00zzjas_profilez00,
		BgL_bgl_string1996za700za7za7j2034za7, "<clinit>", 8);
	      DEFINE_STRING(BGl_string1997z00zzjas_profilez00,
		BgL_bgl_string1997za700za7za7j2035za7, "java.lang.String", 16);
	      DEFINE_STRING(BGl_string1998z00zzjas_profilez00,
		BgL_bgl_string1998za700za7za7j2036za7, "__profiler", 10);
	      DEFINE_STRING(BGl_string1999z00zzjas_profilez00,
		BgL_bgl_string1999za700za7za7j2037za7, "__profiler__lines", 17);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_jaszd2profilezd2envz00zzjas_profilez00,
		BgL_bgl_za762jasza7d2profile2038z00,
		BGl_z62jaszd2profilezb0zzjas_profilez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2000z00zzjas_profilez00,
		BgL_bgl_string2000za700za7za7j2039za7, "__profiler__names", 17);
	      DEFINE_STRING(BGl_string2001z00zzjas_profilez00,
		BgL_bgl_string2001za700za7za7j2040za7, "__profiler__res", 15);
	      DEFINE_STRING(BGl_string2002z00zzjas_profilez00,
		BgL_bgl_string2002za700za7za7j2041za7, "cant reach clinit", 17);
	      DEFINE_STRING(BGl_string2003z00zzjas_profilez00,
		BgL_bgl_string2003za700za7za7j2042za7, "as", 2);
	      DEFINE_STRING(BGl_string2004z00zzjas_profilez00,
		BgL_bgl_string2004za700za7za7j2043za7, "bad method definition", 21);
	      DEFINE_STRING(BGl_string2005z00zzjas_profilez00,
		BgL_bgl_string2005za700za7za7j2044za7, "profile", 7);
	      DEFINE_STRING(BGl_string2006z00zzjas_profilez00,
		BgL_bgl_string2006za700za7za7j2045za7, "empty basic block", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2007z00zzjas_profilez00,
		BgL_bgl_za762lambda1956za7622046z00, BGl_z62lambda1956z62zzjas_profilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2008z00zzjas_profilez00,
		BgL_bgl_za762lambda1955za7622047z00, BGl_z62lambda1955z62zzjas_profilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2009z00zzjas_profilez00,
		BgL_bgl_za762lambda1961za7622048z00, BGl_z62lambda1961z62zzjas_profilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2010z00zzjas_profilez00,
		BgL_bgl_za762lambda1960za7622049z00, BGl_z62lambda1960z62zzjas_profilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2011z00zzjas_profilez00,
		BgL_bgl_za762lambda1966za7622050z00, BGl_z62lambda1966z62zzjas_profilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2012z00zzjas_profilez00,
		BgL_bgl_za762lambda1965za7622051z00, BGl_z62lambda1965z62zzjas_profilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2013z00zzjas_profilez00,
		BgL_bgl_za762lambda1971za7622052z00, BGl_z62lambda1971z62zzjas_profilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2014z00zzjas_profilez00,
		BgL_bgl_za762lambda1970za7622053z00, BGl_z62lambda1970z62zzjas_profilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2015z00zzjas_profilez00,
		BgL_bgl_za762lambda1976za7622054z00, BGl_z62lambda1976z62zzjas_profilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2016z00zzjas_profilez00,
		BgL_bgl_za762lambda1975za7622055z00, BGl_z62lambda1975z62zzjas_profilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2017z00zzjas_profilez00,
		BgL_bgl_za762lambda1981za7622056z00, BGl_z62lambda1981z62zzjas_profilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2018z00zzjas_profilez00,
		BgL_bgl_za762lambda1980za7622057z00, BGl_z62lambda1980z62zzjas_profilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2019z00zzjas_profilez00,
		BgL_bgl_za762lambda1986za7622058z00, BGl_z62lambda1986z62zzjas_profilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2026z00zzjas_profilez00,
		BgL_bgl_string2026za700za7za7j2059za7, "jas_profile", 11);
	      DEFINE_STRING(BGl_string2027z00zzjas_profilez00,
		BgL_bgl_string2027za700za7za7j2060za7,
		"jas_profile _env lines lnames names res clinit has_clinit jstring obj this int (newarray int) (iastore) anewarray putstatic (aastore) (dup) (iconst_5) (iconst_4) (iconst_3) (iconst_2) (iconst_1) (iconst_0) (iconst_m1) ldc sipush bipush ***start*** (handler line comment localvar) getstatic aaload dup2 iaload iconst_1 iadd iastore ((return)) fields sde sourcefile field method void public static vector (vector (vector int)) __profile_lines __profile_names __profile_res class (sourcefile sde fields) declare ",
		509);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2020z00zzjas_profilez00,
		BgL_bgl_za762lambda1985za7622061z00, BGl_z62lambda1985z62zzjas_profilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2021z00zzjas_profilez00,
		BgL_bgl_za762lambda1991za7622062z00, BGl_z62lambda1991z62zzjas_profilez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2022z00zzjas_profilez00,
		BgL_bgl_za762lambda1990za7622063z00, BGl_z62lambda1990z62zzjas_profilez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2023z00zzjas_profilez00,
		BgL_bgl_za762za7c3za704anonymo2064za7,
		BGl_z62zc3z04anonymousza31951ze3ze5zzjas_profilez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2024z00zzjas_profilez00,
		BgL_bgl_za762lambda1949za7622065z00, BGl_z62lambda1949z62zzjas_profilez00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2025z00zzjas_profilez00,
		BgL_bgl_za762lambda1947za7622066z00, BGl_z62lambda1947z62zzjas_profilez00,
		0L, BUNSPEC, 8);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzjas_profilez00));
		   
			 ADD_ROOT((void *) (&BGl_za2jaszd2profilezd2modeza2z00zzjas_profilez00));
		     ADD_ROOT((void *) (&BGl__envz00zzjas_profilez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzjas_profilez00(long
		BgL_checksumz00_1080, char *BgL_fromz00_1081)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzjas_profilez00))
				{
					BGl_requirezd2initializa7ationz75zzjas_profilez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzjas_profilez00();
					BGl_libraryzd2moduleszd2initz00zzjas_profilez00();
					BGl_cnstzd2initzd2zzjas_profilez00();
					BGl_importedzd2moduleszd2initz00zzjas_profilez00();
					BGl_objectzd2initzd2zzjas_profilez00();
					return BGl_toplevelzd2initzd2zzjas_profilez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzjas_profilez00(void)
	{
		{	/* Jas/profile.scm 1 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"jas_profile");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "jas_profile");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "jas_profile");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "jas_profile");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "jas_profile");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "jas_profile");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "jas_profile");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "jas_profile");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "jas_profile");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"jas_profile");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzjas_profilez00(void)
	{
		{	/* Jas/profile.scm 1 */
			{	/* Jas/profile.scm 1 */
				obj_t BgL_cportz00_1047;

				{	/* Jas/profile.scm 1 */
					obj_t BgL_stringz00_1054;

					BgL_stringz00_1054 = BGl_string2027z00zzjas_profilez00;
					{	/* Jas/profile.scm 1 */
						obj_t BgL_startz00_1055;

						BgL_startz00_1055 = BINT(0L);
						{	/* Jas/profile.scm 1 */
							obj_t BgL_endz00_1056;

							BgL_endz00_1056 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1054)));
							{	/* Jas/profile.scm 1 */

								BgL_cportz00_1047 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1054, BgL_startz00_1055, BgL_endz00_1056);
				}}}}
				{
					long BgL_iz00_1048;

					BgL_iz00_1048 = 53L;
				BgL_loopz00_1049:
					if ((BgL_iz00_1048 == -1L))
						{	/* Jas/profile.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* Jas/profile.scm 1 */
							{	/* Jas/profile.scm 1 */
								obj_t BgL_arg2029z00_1050;

								{	/* Jas/profile.scm 1 */

									{	/* Jas/profile.scm 1 */
										obj_t BgL_locationz00_1052;

										BgL_locationz00_1052 = BBOOL(((bool_t) 0));
										{	/* Jas/profile.scm 1 */

											BgL_arg2029z00_1050 =
												BGl_readz00zz__readerz00(BgL_cportz00_1047,
												BgL_locationz00_1052);
										}
									}
								}
								{	/* Jas/profile.scm 1 */
									int BgL_tmpz00_1110;

									BgL_tmpz00_1110 = (int) (BgL_iz00_1048);
									CNST_TABLE_SET(BgL_tmpz00_1110, BgL_arg2029z00_1050);
							}}
							{	/* Jas/profile.scm 1 */
								int BgL_auxz00_1053;

								BgL_auxz00_1053 = (int) ((BgL_iz00_1048 - 1L));
								{
									long BgL_iz00_1115;

									BgL_iz00_1115 = (long) (BgL_auxz00_1053);
									BgL_iz00_1048 = BgL_iz00_1115;
									goto BgL_loopz00_1049;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzjas_profilez00(void)
	{
		{	/* Jas/profile.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzjas_profilez00(void)
	{
		{	/* Jas/profile.scm 1 */
			return (BGl_za2jaszd2profilezd2modeza2z00zzjas_profilez00 =
				BINT(0L), BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzjas_profilez00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_30;

				BgL_headz00_30 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_31;
					obj_t BgL_tailz00_32;

					BgL_prevz00_31 = BgL_headz00_30;
					BgL_tailz00_32 = BgL_l1z00_1;
				BgL_loopz00_33:
					if (PAIRP(BgL_tailz00_32))
						{
							obj_t BgL_newzd2prevzd2_35;

							BgL_newzd2prevzd2_35 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_32), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_31, BgL_newzd2prevzd2_35);
							{
								obj_t BgL_tailz00_1126;
								obj_t BgL_prevz00_1125;

								BgL_prevz00_1125 = BgL_newzd2prevzd2_35;
								BgL_tailz00_1126 = CDR(BgL_tailz00_32);
								BgL_tailz00_32 = BgL_tailz00_1126;
								BgL_prevz00_31 = BgL_prevz00_1125;
								goto BgL_loopz00_33;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_30);
				}
			}
		}

	}



/* jas-profile */
	BGL_EXPORTED_DEF obj_t BGl_jaszd2profilezd2zzjas_profilez00(obj_t BgL_lz00_3)
	{
		{	/* Jas/profile.scm 15 */
			if (((long) CINT(BGl_za2jaszd2profilezd2modeza2z00zzjas_profilez00) > 0L))
				{	/* Jas/profile.scm 16 */
					BGL_TAIL return BGl_profilingz00zzjas_profilez00(BgL_lz00_3);
				}
			else
				{	/* Jas/profile.scm 16 */
					return BgL_lz00_3;
				}
		}

	}



/* &jas-profile */
	obj_t BGl_z62jaszd2profilezb0zzjas_profilez00(obj_t BgL_envz00_973,
		obj_t BgL_lz00_974)
	{
		{	/* Jas/profile.scm 15 */
			return BGl_jaszd2profilezd2zzjas_profilez00(BgL_lz00_974);
		}

	}



/* profiling */
	obj_t BGl_profilingz00zzjas_profilez00(obj_t BgL_lz00_4)
	{
		{	/* Jas/profile.scm 27 */
			{
				obj_t BgL_keyz00_40;
				obj_t BgL_thisz00_41;
				obj_t BgL_extendz00_42;
				obj_t BgL_implementsz00_43;
				obj_t BgL_declsz00_44;
				obj_t BgL_infosz00_45;

				if (NULLP(BgL_lz00_4))
					{	/* Jas/profile.scm 28 */
					BgL_tagzd2102zd2_47:
						return
							BGl_errorz00zz__errorz00(BGl_string1992z00zzjas_profilez00,
							BGl_string1993z00zzjas_profilez00, BgL_lz00_4);
					}
				else
					{	/* Jas/profile.scm 28 */
						obj_t BgL_cdrzd2119zd2_50;

						BgL_cdrzd2119zd2_50 = CDR(((obj_t) BgL_lz00_4));
						{	/* Jas/profile.scm 28 */
							obj_t BgL_keyz00_51;

							BgL_keyz00_51 = CAR(((obj_t) BgL_lz00_4));
							if (PAIRP(BgL_cdrzd2119zd2_50))
								{	/* Jas/profile.scm 28 */
									obj_t BgL_carzd2126zd2_53;
									obj_t BgL_cdrzd2127zd2_54;

									BgL_carzd2126zd2_53 = CAR(BgL_cdrzd2119zd2_50);
									BgL_cdrzd2127zd2_54 = CDR(BgL_cdrzd2119zd2_50);
									if (SYMBOLP(BgL_carzd2126zd2_53))
										{	/* Jas/profile.scm 28 */
											if (PAIRP(BgL_cdrzd2127zd2_54))
												{	/* Jas/profile.scm 28 */
													obj_t BgL_carzd2134zd2_58;
													obj_t BgL_cdrzd2135zd2_59;

													BgL_carzd2134zd2_58 = CAR(BgL_cdrzd2127zd2_54);
													BgL_cdrzd2135zd2_59 = CDR(BgL_cdrzd2127zd2_54);
													if (SYMBOLP(BgL_carzd2134zd2_58))
														{	/* Jas/profile.scm 28 */
															if (PAIRP(BgL_cdrzd2135zd2_59))
																{	/* Jas/profile.scm 28 */
																	obj_t BgL_carzd2141zd2_63;
																	obj_t BgL_cdrzd2142zd2_64;

																	BgL_carzd2141zd2_63 =
																		CAR(BgL_cdrzd2135zd2_59);
																	BgL_cdrzd2142zd2_64 =
																		CDR(BgL_cdrzd2135zd2_59);
																	{
																		obj_t BgL_gzd2158zd2_84;
																		obj_t BgL_gzd2145zd2_68;

																		BgL_gzd2145zd2_68 = BgL_carzd2141zd2_63;
																		if (NULLP(BgL_gzd2145zd2_68))
																			{	/* Jas/profile.scm 28 */
																				if (PAIRP(BgL_cdrzd2142zd2_64))
																					{	/* Jas/profile.scm 28 */
																						obj_t BgL_carzd2149zd2_72;

																						BgL_carzd2149zd2_72 =
																							CAR(BgL_cdrzd2142zd2_64);
																						if (PAIRP(BgL_carzd2149zd2_72))
																							{	/* Jas/profile.scm 28 */
																								if (
																									(CAR(BgL_carzd2149zd2_72) ==
																										CNST_TABLE_REF(0)))
																									{	/* Jas/profile.scm 28 */
																										BgL_keyz00_40 =
																											BgL_keyz00_51;
																										BgL_thisz00_41 =
																											BgL_carzd2126zd2_53;
																										BgL_extendz00_42 =
																											BgL_carzd2134zd2_58;
																										BgL_implementsz00_43 =
																											BgL_carzd2141zd2_63;
																										BgL_declsz00_44 =
																											CDR(BgL_carzd2149zd2_72);
																										BgL_infosz00_45 =
																											CDR(BgL_cdrzd2142zd2_64);
																									BgL_tagzd2101zd2_46:
																										{	/* Jas/profile.scm 33 */
																											BgL__envz00_bglt
																												BgL_envz00_102;
																											BgL_envz00_102 =
																												BGl_makezd2envzd2zzjas_profilez00
																												(BgL_thisz00_41,
																												BgL_declsz00_44,
																												BgL_infosz00_45);
																											{	/* Jas/profile.scm 35 */
																												obj_t
																													BgL_arg1145z00_103;
																												{	/* Jas/profile.scm 35 */
																													obj_t
																														BgL_arg1148z00_104;
																													{	/* Jas/profile.scm 35 */
																														obj_t
																															BgL_arg1149z00_105;
																														{	/* Jas/profile.scm 35 */
																															obj_t
																																BgL_arg1152z00_106;
																															{	/* Jas/profile.scm 35 */
																																obj_t
																																	BgL_arg1153z00_107;
																																obj_t
																																	BgL_arg1154z00_108;
																																{	/* Jas/profile.scm 35 */
																																	obj_t
																																		BgL_arg1157z00_109;
																																	BgL_arg1157z00_109
																																		=
																																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																		(BGl_profilezd2declarationszd2zzjas_profilez00
																																		(BgL_envz00_102,
																																			BgL_declsz00_44),
																																		BNIL);
																																	BgL_arg1153z00_107
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(0),
																																		BgL_arg1157z00_109);
																																}
																																BgL_arg1154z00_108
																																	=
																																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																	(BGl_profilezd2infoszd2zzjas_profilez00
																																	(BgL_envz00_102,
																																		BgL_infosz00_45),
																																	BNIL);
																																BgL_arg1152z00_106
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1153z00_107,
																																	BgL_arg1154z00_108);
																															}
																															BgL_arg1149z00_105
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_implementsz00_43,
																																BgL_arg1152z00_106);
																														}
																														BgL_arg1148z00_104 =
																															MAKE_YOUNG_PAIR
																															(BgL_extendz00_42,
																															BgL_arg1149z00_105);
																													}
																													BgL_arg1145z00_103 =
																														MAKE_YOUNG_PAIR
																														(BgL_thisz00_41,
																														BgL_arg1148z00_104);
																												}
																												return
																													MAKE_YOUNG_PAIR
																													(BgL_keyz00_40,
																													BgL_arg1145z00_103);
																											}
																										}
																									}
																								else
																									{	/* Jas/profile.scm 28 */
																										goto BgL_tagzd2102zd2_47;
																									}
																							}
																						else
																							{	/* Jas/profile.scm 28 */
																								goto BgL_tagzd2102zd2_47;
																							}
																					}
																				else
																					{	/* Jas/profile.scm 28 */
																						goto BgL_tagzd2102zd2_47;
																					}
																			}
																		else
																			{	/* Jas/profile.scm 28 */
																				if (PAIRP(BgL_gzd2145zd2_68))
																					{	/* Jas/profile.scm 28 */
																						bool_t BgL_test2082z00_1184;

																						{	/* Jas/profile.scm 28 */
																							obj_t BgL_tmpz00_1185;

																							BgL_tmpz00_1185 =
																								CAR(BgL_gzd2145zd2_68);
																							BgL_test2082z00_1184 =
																								SYMBOLP(BgL_tmpz00_1185);
																						}
																						if (BgL_test2082z00_1184)
																							{	/* Jas/profile.scm 28 */
																								BgL_gzd2158zd2_84 =
																									CDR(BgL_gzd2145zd2_68);
																							BgL_zc3z04anonymousza31124ze3z87_85:
																								if (NULLP
																									(BgL_gzd2158zd2_84))
																									{	/* Jas/profile.scm 28 */
																										if (PAIRP
																											(BgL_cdrzd2142zd2_64))
																											{	/* Jas/profile.scm 28 */
																												obj_t
																													BgL_carzd2162zd2_88;
																												BgL_carzd2162zd2_88 =
																													CAR
																													(BgL_cdrzd2142zd2_64);
																												if (PAIRP
																													(BgL_carzd2162zd2_88))
																													{	/* Jas/profile.scm 28 */
																														if (
																															(CAR
																																(BgL_carzd2162zd2_88)
																																==
																																CNST_TABLE_REF
																																(0)))
																															{
																																obj_t
																																	BgL_infosz00_1205;
																																obj_t
																																	BgL_declsz00_1203;
																																obj_t
																																	BgL_implementsz00_1202;
																																obj_t
																																	BgL_extendz00_1201;
																																obj_t
																																	BgL_thisz00_1200;
																																obj_t
																																	BgL_keyz00_1199;
																																BgL_keyz00_1199
																																	=
																																	BgL_keyz00_51;
																																BgL_thisz00_1200
																																	=
																																	BgL_carzd2126zd2_53;
																																BgL_extendz00_1201
																																	=
																																	BgL_carzd2134zd2_58;
																																BgL_implementsz00_1202
																																	=
																																	BgL_carzd2141zd2_63;
																																BgL_declsz00_1203
																																	=
																																	CDR
																																	(BgL_carzd2162zd2_88);
																																BgL_infosz00_1205
																																	=
																																	CDR
																																	(BgL_cdrzd2142zd2_64);
																																BgL_infosz00_45
																																	=
																																	BgL_infosz00_1205;
																																BgL_declsz00_44
																																	=
																																	BgL_declsz00_1203;
																																BgL_implementsz00_43
																																	=
																																	BgL_implementsz00_1202;
																																BgL_extendz00_42
																																	=
																																	BgL_extendz00_1201;
																																BgL_thisz00_41 =
																																	BgL_thisz00_1200;
																																BgL_keyz00_40 =
																																	BgL_keyz00_1199;
																																goto
																																	BgL_tagzd2101zd2_46;
																															}
																														else
																															{	/* Jas/profile.scm 28 */
																																goto
																																	BgL_tagzd2102zd2_47;
																															}
																													}
																												else
																													{	/* Jas/profile.scm 28 */
																														goto
																															BgL_tagzd2102zd2_47;
																													}
																											}
																										else
																											{	/* Jas/profile.scm 28 */
																												goto
																													BgL_tagzd2102zd2_47;
																											}
																									}
																								else
																									{	/* Jas/profile.scm 28 */
																										if (PAIRP
																											(BgL_gzd2158zd2_84))
																											{	/* Jas/profile.scm 28 */
																												bool_t
																													BgL_test2088z00_1209;
																												{	/* Jas/profile.scm 28 */
																													obj_t BgL_tmpz00_1210;

																													BgL_tmpz00_1210 =
																														CAR
																														(BgL_gzd2158zd2_84);
																													BgL_test2088z00_1209 =
																														SYMBOLP
																														(BgL_tmpz00_1210);
																												}
																												if (BgL_test2088z00_1209)
																													{
																														obj_t
																															BgL_gzd2158zd2_1213;
																														BgL_gzd2158zd2_1213
																															=
																															CDR
																															(BgL_gzd2158zd2_84);
																														BgL_gzd2158zd2_84 =
																															BgL_gzd2158zd2_1213;
																														goto
																															BgL_zc3z04anonymousza31124ze3z87_85;
																													}
																												else
																													{	/* Jas/profile.scm 28 */
																														goto
																															BgL_tagzd2102zd2_47;
																													}
																											}
																										else
																											{	/* Jas/profile.scm 28 */
																												goto
																													BgL_tagzd2102zd2_47;
																											}
																									}
																							}
																						else
																							{	/* Jas/profile.scm 28 */
																								goto BgL_tagzd2102zd2_47;
																							}
																					}
																				else
																					{	/* Jas/profile.scm 28 */
																						goto BgL_tagzd2102zd2_47;
																					}
																			}
																	}
																}
															else
																{	/* Jas/profile.scm 28 */
																	goto BgL_tagzd2102zd2_47;
																}
														}
													else
														{	/* Jas/profile.scm 28 */
															goto BgL_tagzd2102zd2_47;
														}
												}
											else
												{	/* Jas/profile.scm 28 */
													goto BgL_tagzd2102zd2_47;
												}
										}
									else
										{	/* Jas/profile.scm 28 */
											goto BgL_tagzd2102zd2_47;
										}
								}
							else
								{	/* Jas/profile.scm 28 */
									goto BgL_tagzd2102zd2_47;
								}
						}
					}
			}
		}

	}



/* make-env */
	BgL__envz00_bglt BGl_makezd2envzd2zzjas_profilez00(obj_t BgL_thisz00_5,
		obj_t BgL_declsz00_6, obj_t BgL_infosz00_7)
	{
		{	/* Jas/profile.scm 42 */
			{
				obj_t BgL_valz00_143;
				obj_t BgL_varz00_150;
				obj_t BgL_lz00_151;
				obj_t BgL_infosz00_160;
				obj_t BgL_declsz00_161;

				{	/* Jas/profile.scm 71 */
					obj_t BgL_vclinitz00_116;
					obj_t BgL_lnamesz00_117;

					BgL_vclinitz00_116 =
						BGl_getzd2idze70z35zzjas_profilez00
						(BGl_string1996z00zzjas_profilez00, BgL_declsz00_6);
					BgL_infosz00_160 = BgL_infosz00_7;
					BgL_declsz00_161 = BgL_declsz00_6;
				BgL_zc3z04anonymousza31211ze3z87_162:
					if (NULLP(BgL_infosz00_160))
						{	/* Jas/profile.scm 66 */
							BgL_lnamesz00_117 = BNIL;
						}
					else
						{	/* Jas/profile.scm 67 */
							bool_t BgL_test2090z00_1219;

							{	/* Jas/profile.scm 67 */
								obj_t BgL_tmpz00_1220;

								{	/* Jas/profile.scm 67 */
									obj_t BgL_auxz00_1221;

									{	/* Jas/profile.scm 67 */
										obj_t BgL_pairz00_819;

										BgL_pairz00_819 = CAR(((obj_t) BgL_infosz00_160));
										BgL_auxz00_1221 = CAR(BgL_pairz00_819);
									}
									BgL_tmpz00_1220 =
										BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_1221,
										CNST_TABLE_REF(1));
								}
								BgL_test2090z00_1219 = CBOOL(BgL_tmpz00_1220);
							}
							if (BgL_test2090z00_1219)
								{	/* Jas/profile.scm 68 */
									obj_t BgL_arg1218z00_166;

									BgL_arg1218z00_166 = CDR(((obj_t) BgL_infosz00_160));
									{
										obj_t BgL_infosz00_1230;

										BgL_infosz00_1230 = BgL_arg1218z00_166;
										BgL_infosz00_160 = BgL_infosz00_1230;
										goto BgL_zc3z04anonymousza31211ze3z87_162;
									}
								}
							else
								{	/* Jas/profile.scm 70 */
									obj_t BgL_head1074z00_169;

									BgL_head1074z00_169 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									{
										obj_t BgL_l1072z00_171;
										obj_t BgL_tail1075z00_172;

										BgL_l1072z00_171 = BgL_infosz00_160;
										BgL_tail1075z00_172 = BgL_head1074z00_169;
									BgL_zc3z04anonymousza31220ze3z87_173:
										if (NULLP(BgL_l1072z00_171))
											{	/* Jas/profile.scm 70 */
												BgL_lnamesz00_117 = CDR(BgL_head1074z00_169);
											}
										else
											{	/* Jas/profile.scm 70 */
												obj_t BgL_newtail1076z00_175;

												{	/* Jas/profile.scm 70 */
													obj_t BgL_arg1225z00_177;

													{	/* Jas/profile.scm 70 */
														obj_t BgL_mz00_178;

														BgL_mz00_178 = CAR(((obj_t) BgL_l1072z00_171));
														{	/* Jas/profile.scm 70 */
															obj_t BgL_arg1226z00_179;

															{	/* Jas/profile.scm 70 */
																obj_t BgL_pairz00_826;

																BgL_pairz00_826 = CDR(((obj_t) BgL_mz00_178));
																BgL_arg1226z00_179 = CAR(BgL_pairz00_826);
															}
															BgL_varz00_150 = BgL_arg1226z00_179;
															BgL_lz00_151 = BgL_declsz00_161;
														BgL_zc3z04anonymousza31200ze3z87_152:
															if (NULLP(BgL_lz00_151))
																{	/* Jas/profile.scm 61 */
																	BgL_arg1225z00_177 =
																		BGl_errorz00zz__errorz00
																		(BGl_string1992z00zzjas_profilez00,
																		BGl_string1994z00zzjas_profilez00,
																		BgL_varz00_150);
																}
															else
																{	/* Jas/profile.scm 62 */
																	bool_t BgL_test2093z00_1243;

																	{	/* Jas/profile.scm 62 */
																		obj_t BgL_tmpz00_1244;

																		{	/* Jas/profile.scm 62 */
																			obj_t BgL_pairz00_799;

																			BgL_pairz00_799 =
																				CAR(((obj_t) BgL_lz00_151));
																			BgL_tmpz00_1244 = CAR(BgL_pairz00_799);
																		}
																		BgL_test2093z00_1243 =
																			(BgL_tmpz00_1244 == BgL_varz00_150);
																	}
																	if (BgL_test2093z00_1243)
																		{	/* Jas/profile.scm 63 */
																			obj_t BgL_tmpz00_1249;

																			{
																				obj_t BgL_auxz00_1250;

																				{	/* Jas/profile.scm 63 */
																					obj_t BgL_pairz00_806;

																					{	/* Jas/profile.scm 63 */
																						obj_t BgL_pairz00_805;

																						{	/* Jas/profile.scm 63 */
																							obj_t BgL_pairz00_804;

																							BgL_pairz00_804 =
																								CAR(((obj_t) BgL_lz00_151));
																							BgL_pairz00_805 =
																								CDR(BgL_pairz00_804);
																						}
																						BgL_pairz00_806 =
																							CAR(BgL_pairz00_805);
																					}
																					BgL_auxz00_1250 =
																						CDR(CDR(CDR(CDR(BgL_pairz00_806))));
																				}
																				BgL_tmpz00_1249 =
																					((obj_t) BgL_auxz00_1250);
																			}
																			BgL_arg1225z00_177 = CAR(BgL_tmpz00_1249);
																		}
																	else
																		{	/* Jas/profile.scm 64 */
																			obj_t BgL_arg1209z00_158;

																			BgL_arg1209z00_158 =
																				CDR(((obj_t) BgL_lz00_151));
																			{
																				obj_t BgL_lz00_1263;

																				BgL_lz00_1263 = BgL_arg1209z00_158;
																				BgL_lz00_151 = BgL_lz00_1263;
																				goto
																					BgL_zc3z04anonymousza31200ze3z87_152;
																			}
																		}
																}
														}
													}
													BgL_newtail1076z00_175 =
														MAKE_YOUNG_PAIR(BgL_arg1225z00_177, BNIL);
												}
												SET_CDR(BgL_tail1075z00_172, BgL_newtail1076z00_175);
												{	/* Jas/profile.scm 70 */
													obj_t BgL_arg1223z00_176;

													BgL_arg1223z00_176 = CDR(((obj_t) BgL_l1072z00_171));
													{
														obj_t BgL_tail1075z00_1269;
														obj_t BgL_l1072z00_1268;

														BgL_l1072z00_1268 = BgL_arg1223z00_176;
														BgL_tail1075z00_1269 = BgL_newtail1076z00_175;
														BgL_tail1075z00_172 = BgL_tail1075z00_1269;
														BgL_l1072z00_171 = BgL_l1072z00_1268;
														goto BgL_zc3z04anonymousza31220ze3z87_173;
													}
												}
											}
									}
								}
						}
					{	/* Jas/profile.scm 73 */
						BgL__envz00_bglt BgL_new1059z00_118;

						{	/* Jas/profile.scm 74 */
							BgL__envz00_bglt BgL_new1058z00_124;

							BgL_new1058z00_124 =
								((BgL__envz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL__envz00_bgl))));
							{	/* Jas/profile.scm 74 */
								long BgL_arg1172z00_125;

								BgL_arg1172z00_125 = BGL_CLASS_NUM(BGl__envz00zzjas_profilez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1058z00_124),
									BgL_arg1172z00_125);
							}
							BgL_new1059z00_118 = BgL_new1058z00_124;
						}
						((((BgL__envz00_bglt) COBJECT(BgL_new1059z00_118))->BgL_thisz00) =
							((obj_t) BgL_thisz00_5), BUNSPEC);
						{
							obj_t BgL_auxz00_1275;

							{	/* Jas/profile.scm 75 */
								obj_t BgL__ortest_1060z00_119;

								BgL__ortest_1060z00_119 =
									BGl_getzd2idze70z35zzjas_profilez00
									(BGl_string1997z00zzjas_profilez00, BgL_declsz00_6);
								if (CBOOL(BgL__ortest_1060z00_119))
									{	/* Jas/profile.scm 75 */
										BgL_auxz00_1275 = BgL__ortest_1060z00_119;
									}
								else
									{	/* Jas/profile.scm 76 */
										obj_t BgL_arg1164z00_120;

										{	/* Jas/profile.scm 76 */
											obj_t BgL_arg1166z00_121;

											{	/* Jas/profile.scm 76 */
												obj_t BgL_arg1171z00_122;

												BgL_arg1171z00_122 =
													MAKE_YOUNG_PAIR(BGl_string1997z00zzjas_profilez00,
													BNIL);
												BgL_arg1166z00_121 =
													MAKE_YOUNG_PAIR(BNIL, BgL_arg1171z00_122);
											}
											BgL_arg1164z00_120 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BgL_arg1166z00_121);
										}
										BgL_valz00_143 = BgL_arg1164z00_120;
										{	/* Jas/profile.scm 57 */
											obj_t BgL_rz00_145;

											BgL_rz00_145 =
												BGl_gensymz00zz__r4_symbols_6_4z00
												(BGl_string1995z00zzjas_profilez00);
											{	/* Jas/profile.scm 58 */
												obj_t BgL_arg1196z00_146;

												{	/* Jas/profile.scm 58 */
													obj_t BgL_arg1197z00_147;
													obj_t BgL_arg1198z00_148;

													{	/* Jas/profile.scm 58 */
														obj_t BgL_arg1199z00_149;

														BgL_arg1199z00_149 =
															MAKE_YOUNG_PAIR(BgL_valz00_143, BNIL);
														BgL_arg1197z00_147 =
															MAKE_YOUNG_PAIR(BgL_rz00_145, BgL_arg1199z00_149);
													}
													BgL_arg1198z00_148 = CDR(BgL_valz00_143);
													BgL_arg1196z00_146 =
														MAKE_YOUNG_PAIR(BgL_arg1197z00_147,
														BgL_arg1198z00_148);
												}
												SET_CDR(BgL_valz00_143, BgL_arg1196z00_146);
											}
											BgL_auxz00_1275 = BgL_rz00_145;
										}
									}
							}
							((((BgL__envz00_bglt) COBJECT(BgL_new1059z00_118))->
									BgL_jstringz00) = ((obj_t) BgL_auxz00_1275), BUNSPEC);
						}
						((((BgL__envz00_bglt) COBJECT(BgL_new1059z00_118))->
								BgL_has_clinitz00) = ((obj_t) BgL_vclinitz00_116), BUNSPEC);
						{
							obj_t BgL_auxz00_1291;

							if (CBOOL(BgL_vclinitz00_116))
								{	/* Jas/profile.scm 78 */
									BgL_auxz00_1291 = BgL_vclinitz00_116;
								}
							else
								{	/* Jas/profile.scm 78 */
									BgL_auxz00_1291 =
										BGl_gensymz00zz__r4_symbols_6_4z00
										(BGl_string1998z00zzjas_profilez00);
								}
							((((BgL__envz00_bglt) COBJECT(BgL_new1059z00_118))->
									BgL_clinitz00) = ((obj_t) BgL_auxz00_1291), BUNSPEC);
						}
						((((BgL__envz00_bglt) COBJECT(BgL_new1059z00_118))->BgL_resz00) =
							((obj_t) CNST_TABLE_REF(3)), BUNSPEC);
						((((BgL__envz00_bglt) COBJECT(BgL_new1059z00_118))->BgL_namesz00) =
							((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
						((((BgL__envz00_bglt) COBJECT(BgL_new1059z00_118))->BgL_lnamesz00) =
							((obj_t) BgL_lnamesz00_117), BUNSPEC);
						((((BgL__envz00_bglt) COBJECT(BgL_new1059z00_118))->BgL_linesz00) =
							((obj_t) CNST_TABLE_REF(5)), BUNSPEC);
						return BgL_new1059z00_118;
					}
				}
			}
		}

	}



/* get-id~0 */
	obj_t BGl_getzd2idze70z35zzjas_profilez00(obj_t BgL_strz00_126,
		obj_t BgL_lz00_127)
	{
		{	/* Jas/profile.scm 55 */
		BGl_getzd2idze70z35zzjas_profilez00:
			if (NULLP(BgL_lz00_127))
				{	/* Jas/profile.scm 44 */
					return BFALSE;
				}
			else
				{	/* Jas/profile.scm 46 */
					obj_t BgL_declz00_130;

					BgL_declz00_130 = CAR(((obj_t) BgL_lz00_127));
					{	/* Jas/profile.scm 47 */
						obj_t BgL_namez00_131;
						obj_t BgL_valz00_132;

						BgL_namez00_131 = CAR(((obj_t) BgL_declz00_130));
						{	/* Jas/profile.scm 47 */
							obj_t BgL_pairz00_755;

							BgL_pairz00_755 = CDR(((obj_t) BgL_declz00_130));
							BgL_valz00_132 = CAR(BgL_pairz00_755);
						}
						{	/* Jas/profile.scm 48 */
							obj_t BgL_tyz00_133;
							obj_t BgL_defz00_134;

							BgL_tyz00_133 = CAR(((obj_t) BgL_valz00_132));
							BgL_defz00_134 = CDR(((obj_t) BgL_valz00_132));
							if ((BgL_tyz00_133 == CNST_TABLE_REF(2)))
								{	/* Jas/profile.scm 50 */
									bool_t BgL_test2098z00_1319;

									{	/* Jas/profile.scm 50 */
										obj_t BgL_arg1188z00_138;

										{	/* Jas/profile.scm 50 */
											obj_t BgL_pairz00_761;

											BgL_pairz00_761 = CDR(((obj_t) BgL_defz00_134));
											BgL_arg1188z00_138 = CAR(BgL_pairz00_761);
										}
										{	/* Jas/profile.scm 50 */
											long BgL_l1z00_764;

											BgL_l1z00_764 =
												STRING_LENGTH(((obj_t) BgL_arg1188z00_138));
											if ((BgL_l1z00_764 == STRING_LENGTH(BgL_strz00_126)))
												{	/* Jas/profile.scm 50 */
													int BgL_arg1282z00_767;

													{	/* Jas/profile.scm 50 */
														char *BgL_auxz00_1331;
														char *BgL_tmpz00_1328;

														BgL_auxz00_1331 = BSTRING_TO_STRING(BgL_strz00_126);
														BgL_tmpz00_1328 =
															BSTRING_TO_STRING(((obj_t) BgL_arg1188z00_138));
														BgL_arg1282z00_767 =
															memcmp(BgL_tmpz00_1328, BgL_auxz00_1331,
															BgL_l1z00_764);
													}
													BgL_test2098z00_1319 =
														((long) (BgL_arg1282z00_767) == 0L);
												}
											else
												{	/* Jas/profile.scm 50 */
													BgL_test2098z00_1319 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test2098z00_1319)
										{	/* Jas/profile.scm 50 */
											return BgL_namez00_131;
										}
									else
										{	/* Jas/profile.scm 52 */
											obj_t BgL_arg1187z00_137;

											BgL_arg1187z00_137 = CDR(((obj_t) BgL_lz00_127));
											{
												obj_t BgL_lz00_1338;

												BgL_lz00_1338 = BgL_arg1187z00_137;
												BgL_lz00_127 = BgL_lz00_1338;
												goto BGl_getzd2idze70z35zzjas_profilez00;
											}
										}
								}
							else
								{	/* Jas/profile.scm 53 */
									bool_t BgL_test2100z00_1339;

									{	/* Jas/profile.scm 53 */
										obj_t BgL_arg1193z00_142;

										{	/* Jas/profile.scm 53 */
											obj_t BgL_pairz00_781;

											{	/* Jas/profile.scm 53 */
												obj_t BgL_pairz00_780;

												{	/* Jas/profile.scm 53 */
													obj_t BgL_pairz00_779;

													BgL_pairz00_779 = CDR(((obj_t) BgL_defz00_134));
													BgL_pairz00_780 = CDR(BgL_pairz00_779);
												}
												BgL_pairz00_781 = CDR(BgL_pairz00_780);
											}
											BgL_arg1193z00_142 = CAR(BgL_pairz00_781);
										}
										{	/* Jas/profile.scm 53 */
											long BgL_l1z00_784;

											BgL_l1z00_784 =
												STRING_LENGTH(((obj_t) BgL_arg1193z00_142));
											if ((BgL_l1z00_784 == STRING_LENGTH(BgL_strz00_126)))
												{	/* Jas/profile.scm 53 */
													int BgL_arg1282z00_787;

													{	/* Jas/profile.scm 53 */
														char *BgL_auxz00_1353;
														char *BgL_tmpz00_1350;

														BgL_auxz00_1353 = BSTRING_TO_STRING(BgL_strz00_126);
														BgL_tmpz00_1350 =
															BSTRING_TO_STRING(((obj_t) BgL_arg1193z00_142));
														BgL_arg1282z00_787 =
															memcmp(BgL_tmpz00_1350, BgL_auxz00_1353,
															BgL_l1z00_784);
													}
													BgL_test2100z00_1339 =
														((long) (BgL_arg1282z00_787) == 0L);
												}
											else
												{	/* Jas/profile.scm 53 */
													BgL_test2100z00_1339 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test2100z00_1339)
										{	/* Jas/profile.scm 53 */
											return BgL_namez00_131;
										}
									else
										{	/* Jas/profile.scm 55 */
											obj_t BgL_arg1191z00_141;

											BgL_arg1191z00_141 = CDR(((obj_t) BgL_lz00_127));
											{
												obj_t BgL_lz00_1360;

												BgL_lz00_1360 = BgL_arg1191z00_141;
												BgL_lz00_127 = BgL_lz00_1360;
												goto BGl_getzd2idze70z35zzjas_profilez00;
											}
										}
								}
						}
					}
				}
		}

	}



/* profile-declarations */
	obj_t BGl_profilezd2declarationszd2zzjas_profilez00(BgL__envz00_bglt
		BgL_envz00_8, obj_t BgL_declsz00_9)
	{
		{	/* Jas/profile.scm 88 */
			{	/* Jas/profile.scm 92 */
				obj_t BgL_arg1228z00_188;
				obj_t BgL_arg1229z00_189;

				{	/* Jas/profile.scm 92 */
					obj_t BgL_arg1230z00_190;

					BgL_arg1230z00_190 =
						(((BgL__envz00_bglt) COBJECT(BgL_envz00_8))->BgL_linesz00);
					BgL_arg1228z00_188 =
						BGl_mfze70ze7zzjas_profilez00(BgL_envz00_8, BgL_arg1230z00_190,
						CNST_TABLE_REF(6), BGl_string1999z00zzjas_profilez00);
				}
				{	/* Jas/profile.scm 93 */
					obj_t BgL_arg1231z00_191;
					obj_t BgL_arg1232z00_192;

					{	/* Jas/profile.scm 93 */
						obj_t BgL_arg1233z00_193;
						obj_t BgL_arg1234z00_194;

						BgL_arg1233z00_193 =
							(((BgL__envz00_bglt) COBJECT(BgL_envz00_8))->BgL_namesz00);
						{	/* Jas/profile.scm 93 */
							obj_t BgL_arg1236z00_195;

							BgL_arg1236z00_195 =
								MAKE_YOUNG_PAIR(
								(((BgL__envz00_bglt) COBJECT(BgL_envz00_8))->BgL_jstringz00),
								BNIL);
							BgL_arg1234z00_194 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1236z00_195);
						}
						BgL_arg1231z00_191 =
							BGl_mfze70ze7zzjas_profilez00(BgL_envz00_8, BgL_arg1233z00_193,
							BgL_arg1234z00_194, BGl_string2000z00zzjas_profilez00);
					}
					{	/* Jas/profile.scm 94 */
						obj_t BgL_arg1239z00_197;
						obj_t BgL_arg1242z00_198;

						{	/* Jas/profile.scm 94 */
							obj_t BgL_arg1244z00_199;

							BgL_arg1244z00_199 =
								(((BgL__envz00_bglt) COBJECT(BgL_envz00_8))->BgL_resz00);
							BgL_arg1239z00_197 =
								BGl_mfze70ze7zzjas_profilez00(BgL_envz00_8, BgL_arg1244z00_199,
								CNST_TABLE_REF(6), BGl_string2001z00zzjas_profilez00);
						}
						{	/* Jas/profile.scm 95 */
							obj_t BgL_arg1248z00_200;
							obj_t BgL_arg1249z00_201;

							if (CBOOL(
									(((BgL__envz00_bglt) COBJECT(BgL_envz00_8))->
										BgL_has_clinitz00)))
								{	/* Jas/profile.scm 95 */
									BgL_arg1248z00_200 = BNIL;
								}
							else
								{	/* Jas/profile.scm 97 */
									obj_t BgL_arg1252z00_203;

									{	/* Jas/profile.scm 97 */
										obj_t BgL_arg1268z00_204;
										obj_t BgL_arg1272z00_205;

										BgL_arg1268z00_204 =
											(((BgL__envz00_bglt) COBJECT(BgL_envz00_8))->
											BgL_clinitz00);
										{	/* Jas/profile.scm 97 */
											obj_t BgL_arg1284z00_206;

											{	/* Jas/profile.scm 97 */
												obj_t BgL_arg1304z00_207;

												{	/* Jas/profile.scm 97 */
													obj_t BgL_arg1305z00_208;
													obj_t BgL_arg1306z00_209;

													BgL_arg1305z00_208 =
														(((BgL__envz00_bglt) COBJECT(BgL_envz00_8))->
														BgL_thisz00);
													{	/* Jas/profile.scm 97 */
														obj_t BgL_arg1307z00_210;
														obj_t BgL_arg1308z00_211;

														{	/* Jas/profile.scm 97 */
															obj_t BgL_arg1310z00_212;

															BgL_arg1310z00_212 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(8), BNIL);
															BgL_arg1307z00_210 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
																BgL_arg1310z00_212);
														}
														{	/* Jas/profile.scm 97 */
															obj_t BgL_arg1311z00_213;

															BgL_arg1311z00_213 =
																MAKE_YOUNG_PAIR
																(BGl_string1996z00zzjas_profilez00, BNIL);
															BgL_arg1308z00_211 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																BgL_arg1311z00_213);
														}
														BgL_arg1306z00_209 =
															MAKE_YOUNG_PAIR(BgL_arg1307z00_210,
															BgL_arg1308z00_211);
													}
													BgL_arg1304z00_207 =
														MAKE_YOUNG_PAIR(BgL_arg1305z00_208,
														BgL_arg1306z00_209);
												}
												BgL_arg1284z00_206 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
													BgL_arg1304z00_207);
											}
											BgL_arg1272z00_205 =
												MAKE_YOUNG_PAIR(BgL_arg1284z00_206, BNIL);
										}
										BgL_arg1252z00_203 =
											MAKE_YOUNG_PAIR(BgL_arg1268z00_204, BgL_arg1272z00_205);
									}
									BgL_arg1248z00_200 =
										MAKE_YOUNG_PAIR(BgL_arg1252z00_203, BNIL);
								}
							BgL_arg1249z00_201 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_declsz00_9,
								BNIL);
							BgL_arg1242z00_198 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BgL_arg1248z00_200, BgL_arg1249z00_201);
						}
						BgL_arg1232z00_192 =
							MAKE_YOUNG_PAIR(BgL_arg1239z00_197, BgL_arg1242z00_198);
					}
					BgL_arg1229z00_189 =
						MAKE_YOUNG_PAIR(BgL_arg1231z00_191, BgL_arg1232z00_192);
				}
				return MAKE_YOUNG_PAIR(BgL_arg1228z00_188, BgL_arg1229z00_189);
			}
		}

	}



/* mf~0 */
	obj_t BGl_mfze70ze7zzjas_profilez00(BgL__envz00_bglt BgL_i1062z00_1046,
		obj_t BgL_namez00_214, obj_t BgL_tyz00_215, obj_t BgL_strz00_216)
	{
		{	/* Jas/profile.scm 91 */
			{	/* Jas/profile.scm 91 */
				obj_t BgL_arg1314z00_218;

				{	/* Jas/profile.scm 91 */
					obj_t BgL_arg1315z00_219;

					{	/* Jas/profile.scm 91 */
						obj_t BgL_arg1316z00_220;

						{	/* Jas/profile.scm 91 */
							obj_t BgL_arg1317z00_221;
							obj_t BgL_arg1318z00_222;

							BgL_arg1317z00_221 =
								(((BgL__envz00_bglt) COBJECT(BgL_i1062z00_1046))->BgL_thisz00);
							{	/* Jas/profile.scm 91 */
								obj_t BgL_arg1319z00_223;
								obj_t BgL_arg1320z00_224;

								{	/* Jas/profile.scm 91 */
									obj_t BgL_arg1321z00_225;

									BgL_arg1321z00_225 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(8), BNIL);
									BgL_arg1319z00_223 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(9), BgL_arg1321z00_225);
								}
								{	/* Jas/profile.scm 91 */
									obj_t BgL_arg1322z00_226;

									BgL_arg1322z00_226 = MAKE_YOUNG_PAIR(BgL_strz00_216, BNIL);
									BgL_arg1320z00_224 =
										MAKE_YOUNG_PAIR(BgL_tyz00_215, BgL_arg1322z00_226);
								}
								BgL_arg1318z00_222 =
									MAKE_YOUNG_PAIR(BgL_arg1319z00_223, BgL_arg1320z00_224);
							}
							BgL_arg1316z00_220 =
								MAKE_YOUNG_PAIR(BgL_arg1317z00_221, BgL_arg1318z00_222);
						}
						BgL_arg1315z00_219 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(12), BgL_arg1316z00_220);
					}
					BgL_arg1314z00_218 = MAKE_YOUNG_PAIR(BgL_arg1315z00_219, BNIL);
				}
				return MAKE_YOUNG_PAIR(BgL_namez00_214, BgL_arg1314z00_218);
			}
		}

	}



/* profile-infos */
	obj_t BGl_profilezd2infoszd2zzjas_profilez00(BgL__envz00_bglt BgL_envz00_10,
		obj_t BgL_infosz00_11)
	{
		{	/* Jas/profile.scm 103 */
			{
				obj_t BgL_idz00_271;
				obj_t BgL_lz00_272;

				if (NULLP(BgL_infosz00_11))
					{	/* Jas/profile.scm 110 */
						return BNIL;
					}
				else
					{	/* Jas/profile.scm 111 */
						bool_t BgL_test2104z00_1412;

						{	/* Jas/profile.scm 111 */
							bool_t BgL_test2105z00_1413;

							{	/* Jas/profile.scm 111 */
								obj_t BgL_tmpz00_1414;

								{	/* Jas/profile.scm 111 */
									obj_t BgL_pairz00_850;

									BgL_pairz00_850 = CAR(((obj_t) BgL_infosz00_11));
									BgL_tmpz00_1414 = CAR(BgL_pairz00_850);
								}
								BgL_test2105z00_1413 = (BgL_tmpz00_1414 == CNST_TABLE_REF(13));
							}
							if (BgL_test2105z00_1413)
								{	/* Jas/profile.scm 111 */
									BgL_test2104z00_1412 = ((bool_t) 1);
								}
							else
								{	/* Jas/profile.scm 111 */
									obj_t BgL_tmpz00_1420;

									{	/* Jas/profile.scm 111 */
										obj_t BgL_pairz00_854;

										BgL_pairz00_854 = CAR(((obj_t) BgL_infosz00_11));
										BgL_tmpz00_1420 = CAR(BgL_pairz00_854);
									}
									BgL_test2104z00_1412 =
										(BgL_tmpz00_1420 == CNST_TABLE_REF(14));
								}
						}
						if (BgL_test2104z00_1412)
							{	/* Jas/profile.scm 112 */
								obj_t BgL_arg1331z00_235;
								obj_t BgL_arg1332z00_236;

								BgL_arg1331z00_235 = CAR(((obj_t) BgL_infosz00_11));
								{	/* Jas/profile.scm 112 */
									obj_t BgL_arg1333z00_237;

									BgL_arg1333z00_237 = CDR(((obj_t) BgL_infosz00_11));
									BgL_arg1332z00_236 =
										BGl_profilezd2infoszd2zzjas_profilez00(BgL_envz00_10,
										BgL_arg1333z00_237);
								}
								return MAKE_YOUNG_PAIR(BgL_arg1331z00_235, BgL_arg1332z00_236);
							}
						else
							{	/* Jas/profile.scm 113 */
								bool_t BgL_test2106z00_1432;

								{	/* Jas/profile.scm 113 */
									obj_t BgL_tmpz00_1433;

									{	/* Jas/profile.scm 113 */
										obj_t BgL_pairz00_860;

										BgL_pairz00_860 = CAR(((obj_t) BgL_infosz00_11));
										BgL_tmpz00_1433 = CAR(BgL_pairz00_860);
									}
									BgL_test2106z00_1432 =
										(BgL_tmpz00_1433 == CNST_TABLE_REF(15));
								}
								if (BgL_test2106z00_1432)
									{	/* Jas/profile.scm 115 */
										obj_t BgL_arg1339z00_241;
										obj_t BgL_arg1340z00_242;

										{	/* Jas/profile.scm 115 */
											obj_t BgL_arg1342z00_243;

											{	/* Jas/profile.scm 115 */
												obj_t BgL_arg1343z00_244;
												obj_t BgL_arg1346z00_245;

												BgL_arg1343z00_244 =
													(((BgL__envz00_bglt) COBJECT(BgL_envz00_10))->
													BgL_resz00);
												{	/* Jas/profile.scm 115 */
													obj_t BgL_arg1348z00_246;
													obj_t BgL_arg1349z00_247;

													BgL_arg1348z00_246 =
														(((BgL__envz00_bglt) COBJECT(BgL_envz00_10))->
														BgL_namesz00);
													{	/* Jas/profile.scm 115 */
														obj_t BgL_arg1351z00_248;
														obj_t BgL_arg1352z00_249;

														BgL_arg1351z00_248 =
															(((BgL__envz00_bglt) COBJECT(BgL_envz00_10))->
															BgL_linesz00);
														{	/* Jas/profile.scm 115 */
															obj_t BgL_auxz00_1442;

															{	/* Jas/profile.scm 115 */
																obj_t BgL_pairz00_864;

																BgL_pairz00_864 =
																	CAR(((obj_t) BgL_infosz00_11));
																BgL_auxz00_1442 = CDR(BgL_pairz00_864);
															}
															BgL_arg1352z00_249 =
																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																(BgL_auxz00_1442, BNIL);
														}
														BgL_arg1349z00_247 =
															MAKE_YOUNG_PAIR(BgL_arg1351z00_248,
															BgL_arg1352z00_249);
													}
													BgL_arg1346z00_245 =
														MAKE_YOUNG_PAIR(BgL_arg1348z00_246,
														BgL_arg1349z00_247);
												}
												BgL_arg1342z00_243 =
													MAKE_YOUNG_PAIR(BgL_arg1343z00_244,
													BgL_arg1346z00_245);
											}
											BgL_arg1339z00_241 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(15), BgL_arg1342z00_243);
										}
										{	/* Jas/profile.scm 116 */
											obj_t BgL_arg1364z00_251;

											BgL_arg1364z00_251 = CDR(((obj_t) BgL_infosz00_11));
											BgL_arg1340z00_242 =
												BGl_profilezd2infoszd2zzjas_profilez00(BgL_envz00_10,
												BgL_arg1364z00_251);
										}
										return
											MAKE_YOUNG_PAIR(BgL_arg1339z00_241, BgL_arg1340z00_242);
									}
								else
									{	/* Jas/profile.scm 118 */
										obj_t BgL_extraz00_252;

										BgL_extraz00_252 =
											BGl_profilezd2extrazd2clinitz00zzjas_profilez00
											(BgL_envz00_10, BgL_infosz00_11);
										{	/* Jas/profile.scm 119 */
											obj_t BgL_rz00_253;

											BgL_rz00_253 =
												BGl_profilezd2methodszd2zzjas_profilez00(BgL_envz00_10,
												BgL_infosz00_11);
											if (CBOOL((((BgL__envz00_bglt) COBJECT(BgL_envz00_10))->
														BgL_has_clinitz00)))
												{	/* Jas/profile.scm 122 */
													obj_t BgL_slotz00_256;

													BgL_idz00_271 =
														(((BgL__envz00_bglt) COBJECT(BgL_envz00_10))->
														BgL_clinitz00);
													BgL_lz00_272 = BgL_rz00_253;
												BgL_zc3z04anonymousza31423ze3z87_273:
													if (NULLP(BgL_lz00_272))
														{	/* Jas/profile.scm 105 */
															BgL_slotz00_256 =
																BGl_errorz00zz__errorz00
																(BGl_string1992z00zzjas_profilez00,
																BGl_string2002z00zzjas_profilez00,
																BgL_idz00_271);
														}
													else
														{	/* Jas/profile.scm 106 */
															bool_t BgL_test2109z00_1464;

															{	/* Jas/profile.scm 106 */
																obj_t BgL_tmpz00_1465;

																{	/* Jas/profile.scm 106 */
																	obj_t BgL_pairz00_837;

																	{	/* Jas/profile.scm 106 */
																		obj_t BgL_pairz00_836;

																		BgL_pairz00_836 =
																			CAR(((obj_t) BgL_lz00_272));
																		BgL_pairz00_837 = CDR(BgL_pairz00_836);
																	}
																	BgL_tmpz00_1465 = CAR(BgL_pairz00_837);
																}
																BgL_test2109z00_1464 =
																	(BgL_tmpz00_1465 == BgL_idz00_271);
															}
															if (BgL_test2109z00_1464)
																{	/* Jas/profile.scm 107 */
																	obj_t BgL_pairz00_845;

																	{	/* Jas/profile.scm 107 */
																		obj_t BgL_pairz00_844;

																		{	/* Jas/profile.scm 107 */
																			obj_t BgL_pairz00_843;

																			BgL_pairz00_843 =
																				CAR(((obj_t) BgL_lz00_272));
																			BgL_pairz00_844 = CDR(BgL_pairz00_843);
																		}
																		BgL_pairz00_845 = CDR(BgL_pairz00_844);
																	}
																	BgL_slotz00_256 = CDR(BgL_pairz00_845);
																}
															else
																{	/* Jas/profile.scm 108 */
																	obj_t BgL_arg1437z00_277;

																	BgL_arg1437z00_277 =
																		CDR(((obj_t) BgL_lz00_272));
																	{
																		obj_t BgL_lz00_1478;

																		BgL_lz00_1478 = BgL_arg1437z00_277;
																		BgL_lz00_272 = BgL_lz00_1478;
																		goto BgL_zc3z04anonymousza31423ze3z87_273;
																	}
																}
														}
													{	/* Jas/profile.scm 124 */
														obj_t BgL_arg1367z00_257;

														{	/* Jas/profile.scm 124 */
															obj_t BgL_arg1370z00_258;

															BgL_arg1370z00_258 =
																CDR(((obj_t) BgL_slotz00_256));
															BgL_arg1367z00_257 =
																BGl_appendzd221011zd2zzjas_profilez00
																(BgL_extraz00_252, BgL_arg1370z00_258);
														}
														{	/* Jas/profile.scm 123 */
															obj_t BgL_tmpz00_1483;

															BgL_tmpz00_1483 = ((obj_t) BgL_slotz00_256);
															SET_CDR(BgL_tmpz00_1483, BgL_arg1367z00_257);
														}
													}
													return BgL_rz00_253;
												}
											else
												{	/* Jas/profile.scm 126 */
													obj_t BgL_arg1375z00_260;

													{	/* Jas/profile.scm 126 */
														obj_t BgL_arg1376z00_261;

														{	/* Jas/profile.scm 126 */
															obj_t BgL_arg1377z00_262;
															obj_t BgL_arg1378z00_263;

															BgL_arg1377z00_262 =
																(((BgL__envz00_bglt) COBJECT(BgL_envz00_10))->
																BgL_clinitz00);
															{	/* Jas/profile.scm 126 */
																obj_t BgL_arg1379z00_264;

																BgL_arg1379z00_264 =
																	MAKE_YOUNG_PAIR(BNIL,
																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																	(BGl_appendzd221011zd2zzjas_profilez00
																		(BgL_extraz00_252, CNST_TABLE_REF(16)),
																		BNIL));
																BgL_arg1378z00_263 =
																	MAKE_YOUNG_PAIR(BNIL, BgL_arg1379z00_264);
															}
															BgL_arg1376z00_261 =
																MAKE_YOUNG_PAIR(BgL_arg1377z00_262,
																BgL_arg1378z00_263);
														}
														BgL_arg1375z00_260 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
															BgL_arg1376z00_261);
													}
													return
														MAKE_YOUNG_PAIR(BgL_arg1375z00_260, BgL_rz00_253);
												}
										}
									}
							}
					}
			}
		}

	}



/* profile-methods */
	obj_t BGl_profilezd2methodszd2zzjas_profilez00(BgL__envz00_bglt BgL_envz00_12,
		obj_t BgL_methodsz00_13)
	{
		{	/* Jas/profile.scm 128 */
			{
				long BgL_iz00_294;
				obj_t BgL_mz00_295;

				{
					obj_t BgL_lz00_283;
					long BgL_iz00_284;
					obj_t BgL_rz00_285;

					BgL_lz00_283 = BgL_methodsz00_13;
					BgL_iz00_284 = 0L;
					BgL_rz00_285 = BNIL;
				BgL_zc3z04anonymousza31449ze3z87_286:
					if (NULLP(BgL_lz00_283))
						{	/* Jas/profile.scm 135 */
							return bgl_reverse_bang(BgL_rz00_285);
						}
					else
						{	/* Jas/profile.scm 137 */
							obj_t BgL_arg1453z00_288;
							long BgL_arg1454z00_289;
							obj_t BgL_arg1472z00_290;

							BgL_arg1453z00_288 = CDR(((obj_t) BgL_lz00_283));
							BgL_arg1454z00_289 = (BgL_iz00_284 + 1L);
							{	/* Jas/profile.scm 137 */
								obj_t BgL_arg1473z00_291;

								{	/* Jas/profile.scm 137 */
									obj_t BgL_arg1485z00_292;

									BgL_arg1485z00_292 = CAR(((obj_t) BgL_lz00_283));
									BgL_iz00_294 = BgL_iz00_284;
									BgL_mz00_295 = BgL_arg1485z00_292;
									{

										if (PAIRP(BgL_mz00_295))
											{	/* Jas/profile.scm 133 */
												obj_t BgL_cdrzd2183zd2_305;

												BgL_cdrzd2183zd2_305 = CDR(((obj_t) BgL_mz00_295));
												if ((CAR(((obj_t) BgL_mz00_295)) == CNST_TABLE_REF(11)))
													{	/* Jas/profile.scm 133 */
														if (PAIRP(BgL_cdrzd2183zd2_305))
															{	/* Jas/profile.scm 133 */
																obj_t BgL_cdrzd2189zd2_309;

																BgL_cdrzd2189zd2_309 =
																	CDR(BgL_cdrzd2183zd2_305);
																if (PAIRP(BgL_cdrzd2189zd2_309))
																	{	/* Jas/profile.scm 133 */
																		obj_t BgL_cdrzd2195zd2_311;

																		BgL_cdrzd2195zd2_311 =
																			CDR(BgL_cdrzd2189zd2_309);
																		if (PAIRP(BgL_cdrzd2195zd2_311))
																			{	/* Jas/profile.scm 133 */
																				obj_t BgL_arg1502z00_313;
																				obj_t BgL_arg1509z00_314;
																				obj_t BgL_arg1513z00_315;
																				obj_t BgL_arg1514z00_316;

																				BgL_arg1502z00_313 =
																					CAR(BgL_cdrzd2183zd2_305);
																				BgL_arg1509z00_314 =
																					CAR(BgL_cdrzd2189zd2_309);
																				BgL_arg1513z00_315 =
																					CAR(BgL_cdrzd2195zd2_311);
																				BgL_arg1514z00_316 =
																					CDR(BgL_cdrzd2195zd2_311);
																				{	/* Jas/profile.scm 132 */
																					obj_t BgL_arg1535z00_881;

																					{	/* Jas/profile.scm 132 */
																						obj_t BgL_arg1540z00_882;

																						{	/* Jas/profile.scm 132 */
																							obj_t BgL_arg1544z00_883;

																							BgL_arg1544z00_883 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1513z00_315,
																								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																								(BGl_profilezd2codezd2zzjas_profilez00
																									(BgL_envz00_12, BgL_iz00_294,
																										BgL_arg1514z00_316), BNIL));
																							BgL_arg1540z00_882 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1509z00_314,
																								BgL_arg1544z00_883);
																						}
																						BgL_arg1535z00_881 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1502z00_313,
																							BgL_arg1540z00_882);
																					}
																					BgL_arg1473z00_291 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
																						BgL_arg1535z00_881);
																				}
																			}
																		else
																			{	/* Jas/profile.scm 133 */
																			BgL_tagzd2172zd2_302:
																				BgL_arg1473z00_291 =
																					BGl_errorz00zz__errorz00
																					(BGl_string2003z00zzjas_profilez00,
																					BGl_string2004z00zzjas_profilez00,
																					BgL_mz00_295);
																			}
																	}
																else
																	{	/* Jas/profile.scm 133 */
																		goto BgL_tagzd2172zd2_302;
																	}
															}
														else
															{	/* Jas/profile.scm 133 */
																goto BgL_tagzd2172zd2_302;
															}
													}
												else
													{	/* Jas/profile.scm 133 */
														goto BgL_tagzd2172zd2_302;
													}
											}
										else
											{	/* Jas/profile.scm 133 */
												goto BgL_tagzd2172zd2_302;
											}
									}
								}
								BgL_arg1472z00_290 =
									MAKE_YOUNG_PAIR(BgL_arg1473z00_291, BgL_rz00_285);
							}
							{
								obj_t BgL_rz00_1536;
								long BgL_iz00_1535;
								obj_t BgL_lz00_1534;

								BgL_lz00_1534 = BgL_arg1453z00_288;
								BgL_iz00_1535 = BgL_arg1454z00_289;
								BgL_rz00_1536 = BgL_arg1472z00_290;
								BgL_rz00_285 = BgL_rz00_1536;
								BgL_iz00_284 = BgL_iz00_1535;
								BgL_lz00_283 = BgL_lz00_1534;
								goto BgL_zc3z04anonymousza31449ze3z87_286;
							}
						}
				}
			}
		}

	}



/* profile-code */
	obj_t BGl_profilezd2codezd2zzjas_profilez00(BgL__envz00_bglt BgL_envz00_14,
		long BgL_curzd2fntzd2_15, obj_t BgL_lz00_16)
	{
		{	/* Jas/profile.scm 139 */
			{
				obj_t BgL_lz00_368;
				long BgL_iz00_369;
				obj_t BgL_rz00_370;
				obj_t BgL_lz00_350;
				long BgL_iz00_351;
				obj_t BgL_rz00_352;
				obj_t BgL_extraz00_353;
				long BgL_iz00_328;

				{	/* Jas/profile.scm 168 */
					obj_t BgL_arg1553z00_327;

					BgL_arg1553z00_327 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(25), BgL_lz00_16);
					BgL_lz00_368 = BgL_arg1553z00_327;
					BgL_iz00_369 = 0L;
					BgL_rz00_370 = BNIL;
				BgL_zc3z04anonymousza31643ze3z87_371:
					if (NULLP(BgL_lz00_368))
						{	/* Jas/profile.scm 164 */
							return bgl_reverse_bang(BgL_rz00_370);
						}
					else
						{	/* Jas/profile.scm 165 */
							bool_t BgL_test2117z00_1542;

							{	/* Jas/profile.scm 165 */
								obj_t BgL_tmpz00_1543;

								BgL_tmpz00_1543 = CAR(((obj_t) BgL_lz00_368));
								BgL_test2117z00_1542 = PAIRP(BgL_tmpz00_1543);
							}
							if (BgL_test2117z00_1542)
								{	/* Jas/profile.scm 167 */
									obj_t BgL_arg1650z00_375;
									obj_t BgL_arg1651z00_376;

									BgL_arg1650z00_375 = CDR(((obj_t) BgL_lz00_368));
									{	/* Jas/profile.scm 167 */
										obj_t BgL_arg1654z00_377;

										BgL_arg1654z00_377 = CAR(((obj_t) BgL_lz00_368));
										BgL_arg1651z00_376 =
											MAKE_YOUNG_PAIR(BgL_arg1654z00_377, BgL_rz00_370);
									}
									{
										obj_t BgL_rz00_1553;
										obj_t BgL_lz00_1552;

										BgL_lz00_1552 = BgL_arg1650z00_375;
										BgL_rz00_1553 = BgL_arg1651z00_376;
										BgL_rz00_370 = BgL_rz00_1553;
										BgL_lz00_368 = BgL_lz00_1552;
										goto BgL_zc3z04anonymousza31643ze3z87_371;
									}
								}
							else
								{	/* Jas/profile.scm 165 */
									BgL_lz00_350 = BgL_lz00_368;
									BgL_iz00_351 = (BgL_iz00_369 + 1L);
									BgL_rz00_352 = BgL_rz00_370;
									BgL_iz00_328 = BgL_iz00_369;
									{	/* Jas/profile.scm 143 */
										obj_t BgL_arg1559z00_331;
										obj_t BgL_arg1561z00_332;

										BgL_arg1559z00_331 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(17), BNIL);
										{	/* Jas/profile.scm 144 */
											obj_t BgL_arg1564z00_333;
											obj_t BgL_arg1565z00_334;

											BgL_arg1564z00_333 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(18), BNIL);
											{	/* Jas/profile.scm 145 */
												obj_t BgL_arg1571z00_335;
												obj_t BgL_arg1573z00_336;

												BgL_arg1571z00_335 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(19), BNIL);
												{	/* Jas/profile.scm 146 */
													obj_t BgL_arg1575z00_337;
													obj_t BgL_arg1576z00_338;

													BgL_arg1575z00_337 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(20), BNIL);
													{	/* Jas/profile.scm 147 */
														obj_t BgL_arg1584z00_339;
														obj_t BgL_arg1585z00_340;

														BgL_arg1584z00_339 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(21), BNIL);
														{	/* Jas/profile.scm 148 */
															obj_t BgL_arg1589z00_341;
															obj_t BgL_arg1591z00_342;

															BgL_arg1589z00_341 =
																BGl_pushzd2intzd2zzjas_profilez00(BINT
																(BgL_iz00_328));
															{	/* Jas/profile.scm 149 */
																obj_t BgL_arg1593z00_343;
																obj_t BgL_arg1594z00_344;

																BgL_arg1593z00_343 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(22), BNIL);
																{	/* Jas/profile.scm 150 */
																	obj_t BgL_arg1595z00_345;
																	obj_t BgL_arg1602z00_346;

																	BgL_arg1595z00_345 =
																		BGl_pushzd2intzd2zzjas_profilez00(BINT
																		(BgL_curzd2fntzd2_15));
																	{	/* Jas/profile.scm 151 */
																		obj_t BgL_arg1605z00_347;

																		{	/* Jas/profile.scm 151 */
																			obj_t BgL_arg1606z00_348;

																			BgL_arg1606z00_348 =
																				MAKE_YOUNG_PAIR(
																				(((BgL__envz00_bglt)
																						COBJECT(BgL_envz00_14))->
																					BgL_resz00), BNIL);
																			BgL_arg1605z00_347 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(23),
																				BgL_arg1606z00_348);
																		}
																		BgL_arg1602z00_346 =
																			MAKE_YOUNG_PAIR(BgL_arg1605z00_347, BNIL);
																	}
																	BgL_arg1594z00_344 =
																		MAKE_YOUNG_PAIR(BgL_arg1595z00_345,
																		BgL_arg1602z00_346);
																}
																BgL_arg1591z00_342 =
																	MAKE_YOUNG_PAIR(BgL_arg1593z00_343,
																	BgL_arg1594z00_344);
															}
															BgL_arg1585z00_340 =
																MAKE_YOUNG_PAIR(BgL_arg1589z00_341,
																BgL_arg1591z00_342);
														}
														BgL_arg1576z00_338 =
															MAKE_YOUNG_PAIR(BgL_arg1584z00_339,
															BgL_arg1585z00_340);
													}
													BgL_arg1573z00_336 =
														MAKE_YOUNG_PAIR(BgL_arg1575z00_337,
														BgL_arg1576z00_338);
												}
												BgL_arg1565z00_334 =
													MAKE_YOUNG_PAIR(BgL_arg1571z00_335,
													BgL_arg1573z00_336);
											}
											BgL_arg1561z00_332 =
												MAKE_YOUNG_PAIR(BgL_arg1564z00_333, BgL_arg1565z00_334);
										}
										BgL_extraz00_353 =
											MAKE_YOUNG_PAIR(BgL_arg1559z00_331, BgL_arg1561z00_332);
									}
								BgL_zc3z04anonymousza31610ze3z87_354:
									if (NULLP(BgL_lz00_350))
										{	/* Jas/profile.scm 157 */
											return
												BGl_errorz00zz__errorz00
												(BGl_string2005z00zzjas_profilez00,
												BGl_string2006z00zzjas_profilez00, BgL_lz00_350);
										}
									else
										{	/* Jas/profile.scm 158 */
											bool_t BgL_test2119z00_1557;

											{	/* Jas/profile.scm 158 */
												bool_t BgL_test2120z00_1558;

												{	/* Jas/profile.scm 158 */
													obj_t BgL_tmpz00_1559;

													BgL_tmpz00_1559 = CAR(((obj_t) BgL_lz00_350));
													BgL_test2120z00_1558 = PAIRP(BgL_tmpz00_1559);
												}
												if (BgL_test2120z00_1558)
													{	/* Jas/profile.scm 159 */
														obj_t BgL_tmpz00_1563;

														{	/* Jas/profile.scm 159 */
															obj_t BgL_auxz00_1564;

															{	/* Jas/profile.scm 159 */
																obj_t BgL_pairz00_893;

																BgL_pairz00_893 = CAR(((obj_t) BgL_lz00_350));
																BgL_auxz00_1564 = CAR(BgL_pairz00_893);
															}
															BgL_tmpz00_1563 =
																BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																(BgL_auxz00_1564, CNST_TABLE_REF(24));
														}
														BgL_test2119z00_1557 = CBOOL(BgL_tmpz00_1563);
													}
												else
													{	/* Jas/profile.scm 158 */
														BgL_test2119z00_1557 = ((bool_t) 1);
													}
											}
											if (BgL_test2119z00_1557)
												{	/* Jas/profile.scm 160 */
													obj_t BgL_arg1625z00_361;
													obj_t BgL_arg1626z00_362;

													BgL_arg1625z00_361 = CDR(((obj_t) BgL_lz00_350));
													{	/* Jas/profile.scm 160 */
														obj_t BgL_arg1627z00_363;

														BgL_arg1627z00_363 = CAR(((obj_t) BgL_lz00_350));
														BgL_arg1626z00_362 =
															MAKE_YOUNG_PAIR(BgL_arg1627z00_363, BgL_rz00_352);
													}
													{
														obj_t BgL_rz00_1577;
														obj_t BgL_lz00_1576;

														BgL_lz00_1576 = BgL_arg1625z00_361;
														BgL_rz00_1577 = BgL_arg1626z00_362;
														BgL_rz00_352 = BgL_rz00_1577;
														BgL_lz00_350 = BgL_lz00_1576;
														goto BgL_zc3z04anonymousza31610ze3z87_354;
													}
												}
											else
												{
													obj_t BgL_rz00_1580;
													long BgL_iz00_1579;
													obj_t BgL_lz00_1578;

													BgL_lz00_1578 = BgL_lz00_350;
													BgL_iz00_1579 = BgL_iz00_351;
													BgL_rz00_1580 =
														BGl_appendzd221011zd2zzjas_profilez00
														(BgL_extraz00_353, BgL_rz00_352);
													BgL_rz00_370 = BgL_rz00_1580;
													BgL_iz00_369 = BgL_iz00_1579;
													BgL_lz00_368 = BgL_lz00_1578;
													goto BgL_zc3z04anonymousza31643ze3z87_371;
												}
										}
								}
						}
				}
			}
		}

	}



/* push-int */
	obj_t BGl_pushzd2intzd2zzjas_profilez00(obj_t BgL_nz00_17)
	{
		{	/* Jas/profile.scm 170 */
			{

				if (INTEGERP(BgL_nz00_17))
					{	/* Jas/profile.scm 171 */
						switch ((long) CINT(BgL_nz00_17))
							{
							case -1L:

								return CNST_TABLE_REF(29);
								break;
							case 0L:

								return CNST_TABLE_REF(30);
								break;
							case 1L:

								return CNST_TABLE_REF(31);
								break;
							case 2L:

								return CNST_TABLE_REF(32);
								break;
							case 3L:

								return CNST_TABLE_REF(33);
								break;
							case 4L:

								return CNST_TABLE_REF(34);
								break;
							case 5L:

								return CNST_TABLE_REF(35);
								break;
							default:
							BgL_case_else1067z00_384:
								{	/* Jas/profile.scm 180 */
									bool_t BgL_test2122z00_1621;

									{	/* Jas/profile.scm 180 */
										bool_t BgL_test2123z00_1622;

										if (INTEGERP(BgL_nz00_17))
											{	/* Jas/profile.scm 180 */
												BgL_test2123z00_1622 =
													((long) CINT(BgL_nz00_17) > -129L);
											}
										else
											{	/* Jas/profile.scm 180 */
												BgL_test2123z00_1622 =
													BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_nz00_17,
													BINT(-129L));
											}
										if (BgL_test2123z00_1622)
											{	/* Jas/profile.scm 180 */
												if (INTEGERP(BgL_nz00_17))
													{	/* Jas/profile.scm 180 */
														BgL_test2122z00_1621 =
															((long) CINT(BgL_nz00_17) < 128L);
													}
												else
													{	/* Jas/profile.scm 180 */
														BgL_test2122z00_1621 =
															BGl_2zc3zc3zz__r4_numbers_6_5z00(BgL_nz00_17,
															BINT(128L));
													}
											}
										else
											{	/* Jas/profile.scm 180 */
												BgL_test2122z00_1621 = ((bool_t) 0);
											}
									}
									if (BgL_test2122z00_1621)
										{	/* Jas/profile.scm 180 */
											obj_t BgL_arg1678z00_388;

											BgL_arg1678z00_388 = MAKE_YOUNG_PAIR(BgL_nz00_17, BNIL);
											return
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(26), BgL_arg1678z00_388);
										}
									else
										{	/* Jas/profile.scm 181 */
											bool_t BgL_test2126z00_1638;

											{	/* Jas/profile.scm 181 */
												bool_t BgL_test2127z00_1639;

												if (INTEGERP(BgL_nz00_17))
													{	/* Jas/profile.scm 181 */
														BgL_test2127z00_1639 =
															((long) CINT(BgL_nz00_17) > -32769L);
													}
												else
													{	/* Jas/profile.scm 181 */
														BgL_test2127z00_1639 =
															BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_nz00_17,
															BINT(-32769L));
													}
												if (BgL_test2127z00_1639)
													{	/* Jas/profile.scm 181 */
														if (INTEGERP(BgL_nz00_17))
															{	/* Jas/profile.scm 181 */
																BgL_test2126z00_1638 =
																	((long) CINT(BgL_nz00_17) < 32768L);
															}
														else
															{	/* Jas/profile.scm 181 */
																BgL_test2126z00_1638 =
																	BGl_2zc3zc3zz__r4_numbers_6_5z00(BgL_nz00_17,
																	BINT(32768L));
															}
													}
												else
													{	/* Jas/profile.scm 181 */
														BgL_test2126z00_1638 = ((bool_t) 0);
													}
											}
											if (BgL_test2126z00_1638)
												{	/* Jas/profile.scm 181 */
													obj_t BgL_arg1688z00_391;

													BgL_arg1688z00_391 =
														MAKE_YOUNG_PAIR(BgL_nz00_17, BNIL);
													return
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(27),
														BgL_arg1688z00_391);
												}
											else
												{	/* Jas/profile.scm 182 */
													obj_t BgL_arg1689z00_392;

													BgL_arg1689z00_392 =
														MAKE_YOUNG_PAIR(BgL_nz00_17, BNIL);
													return
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(28),
														BgL_arg1689z00_392);
												}
										}
								}
							}
					}
				else
					{	/* Jas/profile.scm 171 */
						goto BgL_case_else1067z00_384;
					}
			}
		}

	}



/* profile-extra-clinit */
	obj_t BGl_profilezd2extrazd2clinitz00zzjas_profilez00(BgL__envz00_bglt
		BgL_envz00_18, obj_t BgL_methodsz00_19)
	{
		{	/* Jas/profile.scm 187 */
			{	/* Jas/profile.scm 189 */
				long BgL_siza7eza7_396;

				BgL_siza7eza7_396 =
					bgl_list_length(
					(((BgL__envz00_bglt) COBJECT(BgL_envz00_18))->BgL_lnamesz00));
				return
					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
					(BGl_profilezd2makezd2namesz00zzjas_profilez00(BgL_siza7eza7_396,
						(((BgL__envz00_bglt) COBJECT(BgL_envz00_18))->BgL_namesz00),
						(((BgL__envz00_bglt) COBJECT(BgL_envz00_18))->BgL_lnamesz00),
						(((BgL__envz00_bglt) COBJECT(BgL_envz00_18))->BgL_jstringz00)),
					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
					(BGl_profilezd2makezd2linesz00zzjas_profilez00(BgL_siza7eza7_396,
							(((BgL__envz00_bglt) COBJECT(BgL_envz00_18))->BgL_linesz00),
							BgL_methodsz00_19),
						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
						(BGl_profilezd2makezd2resz00zzjas_profilez00(BgL_siza7eza7_396,
								(((BgL__envz00_bglt) COBJECT(BgL_envz00_18))->BgL_resz00),
								BgL_methodsz00_19), BNIL)));
			}
		}

	}



/* profile-make-names */
	obj_t BGl_profilezd2makezd2namesz00zzjas_profilez00(long BgL_siza7eza7_20,
		obj_t BgL_namesz00_21, obj_t BgL_lz00_22, obj_t BgL_jstringz00_23)
	{
		{	/* Jas/profile.scm 194 */
			{
				obj_t BgL_codez00_418;
				long BgL_iz00_419;
				obj_t BgL_namesz00_420;

				{	/* Jas/profile.scm 202 */
					obj_t BgL_arg1711z00_409;

					{	/* Jas/profile.scm 202 */
						obj_t BgL_arg1714z00_410;
						obj_t BgL_arg1717z00_411;

						{	/* Jas/profile.scm 202 */
							obj_t BgL_arg1718z00_412;

							BgL_arg1718z00_412 = MAKE_YOUNG_PAIR(BgL_namesz00_21, BNIL);
							BgL_arg1714z00_410 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(38), BgL_arg1718z00_412);
						}
						{	/* Jas/profile.scm 203 */
							obj_t BgL_arg1720z00_413;

							{	/* Jas/profile.scm 203 */
								obj_t BgL_arg1722z00_414;
								obj_t BgL_arg1724z00_415;

								{	/* Jas/profile.scm 203 */
									obj_t BgL_arg1733z00_416;

									BgL_arg1733z00_416 = MAKE_YOUNG_PAIR(BgL_jstringz00_23, BNIL);
									BgL_arg1722z00_414 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(39), BgL_arg1733z00_416);
								}
								BgL_arg1724z00_415 =
									MAKE_YOUNG_PAIR(BGl_pushzd2intzd2zzjas_profilez00(BINT
										(BgL_siza7eza7_20)), BNIL);
								BgL_arg1720z00_413 =
									MAKE_YOUNG_PAIR(BgL_arg1722z00_414, BgL_arg1724z00_415);
							}
							BgL_codez00_418 = BgL_arg1720z00_413;
							BgL_iz00_419 = 0L;
							BgL_namesz00_420 = BgL_lz00_22;
						BgL_zc3z04anonymousza31735ze3z87_421:
							if (NULLP(BgL_namesz00_420))
								{	/* Jas/profile.scm 196 */
									BgL_arg1717z00_411 = BgL_codez00_418;
								}
							else
								{	/* Jas/profile.scm 198 */
									obj_t BgL_arg1737z00_423;
									long BgL_arg1738z00_424;
									obj_t BgL_arg1739z00_425;

									{	/* Jas/profile.scm 198 */
										obj_t BgL_arg1740z00_426;

										{	/* Jas/profile.scm 198 */
											obj_t BgL_arg1746z00_427;
											obj_t BgL_arg1747z00_428;

											{	/* Jas/profile.scm 198 */
												obj_t BgL_arg1748z00_429;

												{	/* Jas/profile.scm 198 */
													obj_t BgL_arg1749z00_430;

													BgL_arg1749z00_430 = CAR(((obj_t) BgL_namesz00_420));
													BgL_arg1748z00_429 =
														MAKE_YOUNG_PAIR(BgL_arg1749z00_430, BNIL);
												}
												BgL_arg1746z00_427 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(28),
													BgL_arg1748z00_429);
											}
											{	/* Jas/profile.scm 198 */
												obj_t BgL_arg1750z00_431;
												obj_t BgL_arg1751z00_432;

												BgL_arg1750z00_431 =
													BGl_pushzd2intzd2zzjas_profilez00(BINT(BgL_iz00_419));
												BgL_arg1751z00_432 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(36), BgL_codez00_418);
												BgL_arg1747z00_428 =
													MAKE_YOUNG_PAIR(BgL_arg1750z00_431,
													BgL_arg1751z00_432);
											}
											BgL_arg1740z00_426 =
												MAKE_YOUNG_PAIR(BgL_arg1746z00_427, BgL_arg1747z00_428);
										}
										BgL_arg1737z00_423 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(37), BgL_arg1740z00_426);
									}
									BgL_arg1738z00_424 = (BgL_iz00_419 + 1L);
									BgL_arg1739z00_425 = CDR(((obj_t) BgL_namesz00_420));
									{
										obj_t BgL_namesz00_1703;
										long BgL_iz00_1702;
										obj_t BgL_codez00_1701;

										BgL_codez00_1701 = BgL_arg1737z00_423;
										BgL_iz00_1702 = BgL_arg1738z00_424;
										BgL_namesz00_1703 = BgL_arg1739z00_425;
										BgL_namesz00_420 = BgL_namesz00_1703;
										BgL_iz00_419 = BgL_iz00_1702;
										BgL_codez00_418 = BgL_codez00_1701;
										goto BgL_zc3z04anonymousza31735ze3z87_421;
									}
								}
						}
						BgL_arg1711z00_409 =
							MAKE_YOUNG_PAIR(BgL_arg1714z00_410, BgL_arg1717z00_411);
					}
					return bgl_reverse_bang(BgL_arg1711z00_409);
				}
			}
		}

	}



/* profile-make-lines */
	obj_t BGl_profilezd2makezd2linesz00zzjas_profilez00(long BgL_siza7eza7_24,
		obj_t BgL_linesz00_25, obj_t BgL_methodsz00_26)
	{
		{	/* Jas/profile.scm 205 */
			{
				obj_t BgL_codez00_513;
				long BgL_iz00_514;
				obj_t BgL_methodsz00_515;
				obj_t BgL_codez00_499;
				long BgL_iz00_500;
				obj_t BgL_labsz00_501;
				obj_t BgL_mz00_475;
				obj_t BgL_lz00_462;
				long BgL_iz00_463;
				obj_t BgL_rz00_464;
				obj_t BgL_lz00_450;

				{	/* Jas/profile.scm 247 */
					obj_t BgL_arg1752z00_439;

					{	/* Jas/profile.scm 247 */
						obj_t BgL_arg1753z00_440;
						obj_t BgL_arg1754z00_441;

						{	/* Jas/profile.scm 247 */
							obj_t BgL_arg1755z00_442;

							BgL_arg1755z00_442 = MAKE_YOUNG_PAIR(BgL_linesz00_25, BNIL);
							BgL_arg1753z00_440 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(38), BgL_arg1755z00_442);
						}
						{	/* Jas/profile.scm 248 */
							obj_t BgL_arg1761z00_443;

							{	/* Jas/profile.scm 248 */
								obj_t BgL_arg1762z00_444;
								obj_t BgL_arg1765z00_445;

								{	/* Jas/profile.scm 248 */
									obj_t BgL_arg1767z00_446;

									{	/* Jas/profile.scm 248 */
										obj_t BgL_arg1770z00_447;

										{	/* Jas/profile.scm 248 */
											obj_t BgL_arg1771z00_448;

											BgL_arg1771z00_448 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(42), BNIL);
											BgL_arg1770z00_447 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1771z00_448);
										}
										BgL_arg1767z00_446 =
											MAKE_YOUNG_PAIR(BgL_arg1770z00_447, BNIL);
									}
									BgL_arg1762z00_444 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(39), BgL_arg1767z00_446);
								}
								BgL_arg1765z00_445 =
									MAKE_YOUNG_PAIR(BGl_pushzd2intzd2zzjas_profilez00(BINT
										(BgL_siza7eza7_24)), BNIL);
								BgL_arg1761z00_443 =
									MAKE_YOUNG_PAIR(BgL_arg1762z00_444, BgL_arg1765z00_445);
							}
							BgL_codez00_513 = BgL_arg1761z00_443;
							BgL_iz00_514 = 0L;
							BgL_methodsz00_515 = BgL_methodsz00_26;
						BgL_zc3z04anonymousza31863ze3z87_516:
							if (NULLP(BgL_methodsz00_515))
								{	/* Jas/profile.scm 233 */
									BgL_arg1754z00_441 = BgL_codez00_513;
								}
							else
								{	/* Jas/profile.scm 235 */
									obj_t BgL_arg1866z00_518;
									long BgL_arg1868z00_519;
									obj_t BgL_arg1869z00_520;

									{	/* Jas/profile.scm 235 */
										obj_t BgL_labsz00_521;

										{	/* Jas/profile.scm 235 */
											obj_t BgL_arg1879z00_530;

											BgL_arg1879z00_530 = CAR(((obj_t) BgL_methodsz00_515));
											BgL_mz00_475 = BgL_arg1879z00_530;
											{

												if (PAIRP(BgL_mz00_475))
													{	/* Jas/profile.scm 221 */
														obj_t BgL_cdrzd2215zd2_485;

														BgL_cdrzd2215zd2_485 = CDR(((obj_t) BgL_mz00_475));
														if (
															(CAR(
																	((obj_t) BgL_mz00_475)) ==
																CNST_TABLE_REF(11)))
															{	/* Jas/profile.scm 221 */
																if (PAIRP(BgL_cdrzd2215zd2_485))
																	{	/* Jas/profile.scm 221 */
																		obj_t BgL_cdrzd2221zd2_489;

																		BgL_cdrzd2221zd2_489 =
																			CDR(BgL_cdrzd2215zd2_485);
																		if (PAIRP(BgL_cdrzd2221zd2_489))
																			{	/* Jas/profile.scm 221 */
																				obj_t BgL_cdrzd2227zd2_491;

																				BgL_cdrzd2227zd2_491 =
																					CDR(BgL_cdrzd2221zd2_489);
																				if (PAIRP(BgL_cdrzd2227zd2_491))
																					{	/* Jas/profile.scm 221 */
																						obj_t BgL_arg1847z00_496;

																						BgL_arg1847z00_496 =
																							CDR(BgL_cdrzd2227zd2_491);
																						{	/* Jas/profile.scm 220 */
																							obj_t BgL_arg1849z00_926;

																							BgL_arg1849z00_926 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(25), BgL_arg1847z00_496);
																							BgL_lz00_462 = BgL_arg1849z00_926;
																							BgL_iz00_463 = 0L;
																							BgL_rz00_464 = BNIL;
																						BgL_zc3z04anonymousza31821ze3z87_465:
																							if (NULLP
																								(BgL_lz00_462))
																								{	/* Jas/profile.scm 213 */
																									BgL_labsz00_521 =
																										bgl_reverse_bang
																										(BgL_rz00_464);
																								}
																							else
																								{	/* Jas/profile.scm 214 */
																									bool_t BgL_test2138z00_1747;

																									{	/* Jas/profile.scm 214 */
																										obj_t BgL_tmpz00_1748;

																										BgL_tmpz00_1748 =
																											CAR(
																											((obj_t) BgL_lz00_462));
																										BgL_test2138z00_1747 =
																											PAIRP(BgL_tmpz00_1748);
																									}
																									if (BgL_test2138z00_1747)
																										{	/* Jas/profile.scm 216 */
																											obj_t BgL_arg1831z00_469;
																											long BgL_arg1832z00_470;

																											BgL_arg1831z00_469 =
																												CDR(
																												((obj_t) BgL_lz00_462));
																											BgL_arg1832z00_470 =
																												(BgL_iz00_463 + 1L);
																											{
																												long BgL_iz00_1756;
																												obj_t BgL_lz00_1755;

																												BgL_lz00_1755 =
																													BgL_arg1831z00_469;
																												BgL_iz00_1756 =
																													BgL_arg1832z00_470;
																												BgL_iz00_463 =
																													BgL_iz00_1756;
																												BgL_lz00_462 =
																													BgL_lz00_1755;
																												goto
																													BgL_zc3z04anonymousza31821ze3z87_465;
																											}
																										}
																									else
																										{	/* Jas/profile.scm 215 */
																											obj_t BgL_arg1833z00_471;
																											obj_t BgL_arg1834z00_472;

																											{	/* Jas/profile.scm 215 */
																												obj_t
																													BgL_arg1835z00_473;
																												BgL_arg1835z00_473 =
																													CDR(((obj_t)
																														BgL_lz00_462));
																												BgL_lz00_450 =
																													BgL_arg1835z00_473;
																											BgL_zc3z04anonymousza31774ze3z87_451:
																												if (NULLP
																													(BgL_lz00_450))
																													{	/* Jas/profile.scm 207 */
																														BgL_arg1833z00_471 =
																															BNIL;
																													}
																												else
																													{	/* Jas/profile.scm 208 */
																														bool_t
																															BgL_test2140z00_1761;
																														{	/* Jas/profile.scm 208 */
																															bool_t
																																BgL_test2141z00_1762;
																															{	/* Jas/profile.scm 208 */
																																obj_t
																																	BgL_tmpz00_1763;
																																BgL_tmpz00_1763
																																	=
																																	CAR(((obj_t)
																																		BgL_lz00_450));
																																BgL_test2141z00_1762
																																	=
																																	PAIRP
																																	(BgL_tmpz00_1763);
																															}
																															if (BgL_test2141z00_1762)
																																{	/* Jas/profile.scm 209 */
																																	obj_t
																																		BgL_tmpz00_1767;
																																	{	/* Jas/profile.scm 209 */
																																		obj_t
																																			BgL_auxz00_1768;
																																		{	/* Jas/profile.scm 209 */
																																			obj_t
																																				BgL_pairz00_911;
																																			BgL_pairz00_911
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_lz00_450));
																																			BgL_auxz00_1768
																																				=
																																				CAR
																																				(BgL_pairz00_911);
																																		}
																																		BgL_tmpz00_1767
																																			=
																																			BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																																			(BgL_auxz00_1768,
																																			CNST_TABLE_REF
																																			(24));
																																	}
																																	BgL_test2140z00_1761
																																		=
																																		CBOOL
																																		(BgL_tmpz00_1767);
																																}
																															else
																																{	/* Jas/profile.scm 208 */
																																	BgL_test2140z00_1761
																																		=
																																		((bool_t)
																																		1);
																																}
																														}
																														if (BgL_test2140z00_1761)
																															{	/* Jas/profile.scm 210 */
																																obj_t
																																	BgL_arg1808z00_458;
																																BgL_arg1808z00_458
																																	=
																																	CDR(((obj_t)
																																		BgL_lz00_450));
																																{
																																	obj_t
																																		BgL_lz00_1777;
																																	BgL_lz00_1777
																																		=
																																		BgL_arg1808z00_458;
																																	BgL_lz00_450 =
																																		BgL_lz00_1777;
																																	goto
																																		BgL_zc3z04anonymousza31774ze3z87_451;
																																}
																															}
																														else
																															{	/* Jas/profile.scm 208 */
																																BgL_arg1833z00_471
																																	=
																																	BgL_lz00_450;
																															}
																													}
																											}
																											BgL_arg1834z00_472 =
																												MAKE_YOUNG_PAIR(BINT
																												(BgL_iz00_463),
																												BgL_rz00_464);
																											{
																												obj_t BgL_rz00_1781;
																												obj_t BgL_lz00_1780;

																												BgL_lz00_1780 =
																													BgL_arg1833z00_471;
																												BgL_rz00_1781 =
																													BgL_arg1834z00_472;
																												BgL_rz00_464 =
																													BgL_rz00_1781;
																												BgL_lz00_462 =
																													BgL_lz00_1780;
																												goto
																													BgL_zc3z04anonymousza31821ze3z87_465;
																											}
																										}
																								}
																						}
																					}
																				else
																					{	/* Jas/profile.scm 221 */
																					BgL_tagzd2204zd2_482:
																						BgL_labsz00_521 =
																							BGl_errorz00zz__errorz00
																							(BGl_string2003z00zzjas_profilez00,
																							BGl_string2004z00zzjas_profilez00,
																							BgL_mz00_475);
																					}
																			}
																		else
																			{	/* Jas/profile.scm 221 */
																				goto BgL_tagzd2204zd2_482;
																			}
																	}
																else
																	{	/* Jas/profile.scm 221 */
																		goto BgL_tagzd2204zd2_482;
																	}
															}
														else
															{	/* Jas/profile.scm 221 */
																goto BgL_tagzd2204zd2_482;
															}
													}
												else
													{	/* Jas/profile.scm 221 */
														goto BgL_tagzd2204zd2_482;
													}
											}
										}
										{	/* Jas/profile.scm 238 */
											obj_t BgL_arg1870z00_522;

											{	/* Jas/profile.scm 238 */
												obj_t BgL_arg1872z00_523;

												{	/* Jas/profile.scm 238 */
													obj_t BgL_arg1873z00_524;

													{	/* Jas/profile.scm 238 */
														obj_t BgL_arg1874z00_525;
														obj_t BgL_arg1875z00_526;

														{	/* Jas/profile.scm 238 */
															long BgL_arg1876z00_527;

															BgL_arg1876z00_527 =
																bgl_list_length(BgL_labsz00_521);
															BgL_arg1874z00_525 =
																BGl_pushzd2intzd2zzjas_profilez00(BINT
																(BgL_arg1876z00_527));
														}
														{	/* Jas/profile.scm 239 */
															obj_t BgL_arg1877z00_528;
															obj_t BgL_arg1878z00_529;

															BgL_arg1877z00_528 =
																BGl_pushzd2intzd2zzjas_profilez00(BINT
																(BgL_iz00_514));
															BgL_arg1878z00_529 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(36),
																BgL_codez00_513);
															BgL_arg1875z00_526 =
																MAKE_YOUNG_PAIR(BgL_arg1877z00_528,
																BgL_arg1878z00_529);
														}
														BgL_arg1873z00_524 =
															MAKE_YOUNG_PAIR(BgL_arg1874z00_525,
															BgL_arg1875z00_526);
													}
													BgL_arg1872z00_523 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(41),
														BgL_arg1873z00_524);
												}
												BgL_codez00_499 = BgL_arg1872z00_523;
												BgL_iz00_500 = 0L;
												BgL_labsz00_501 = BgL_labsz00_521;
											BgL_zc3z04anonymousza31850ze3z87_502:
												if (NULLP(BgL_labsz00_501))
													{	/* Jas/profile.scm 223 */
														BgL_arg1870z00_522 = BgL_codez00_499;
													}
												else
													{	/* Jas/profile.scm 226 */
														obj_t BgL_arg1852z00_504;
														long BgL_arg1853z00_505;
														obj_t BgL_arg1854z00_506;

														{	/* Jas/profile.scm 226 */
															obj_t BgL_arg1856z00_507;

															{	/* Jas/profile.scm 226 */
																obj_t BgL_arg1857z00_508;
																obj_t BgL_arg1858z00_509;

																{	/* Jas/profile.scm 226 */
																	obj_t BgL_arg1859z00_510;

																	BgL_arg1859z00_510 =
																		CAR(((obj_t) BgL_labsz00_501));
																	BgL_arg1857z00_508 =
																		BGl_pushzd2intzd2zzjas_profilez00
																		(BgL_arg1859z00_510);
																}
																{	/* Jas/profile.scm 227 */
																	obj_t BgL_arg1860z00_511;
																	obj_t BgL_arg1862z00_512;

																	BgL_arg1860z00_511 =
																		BGl_pushzd2intzd2zzjas_profilez00(BINT
																		(BgL_iz00_500));
																	BgL_arg1862z00_512 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(36),
																		BgL_codez00_499);
																	BgL_arg1858z00_509 =
																		MAKE_YOUNG_PAIR(BgL_arg1860z00_511,
																		BgL_arg1862z00_512);
																}
																BgL_arg1856z00_507 =
																	MAKE_YOUNG_PAIR(BgL_arg1857z00_508,
																	BgL_arg1858z00_509);
															}
															BgL_arg1852z00_504 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(40),
																BgL_arg1856z00_507);
														}
														BgL_arg1853z00_505 = (BgL_iz00_500 + 1L);
														BgL_arg1854z00_506 = CDR(((obj_t) BgL_labsz00_501));
														{
															obj_t BgL_labsz00_1812;
															long BgL_iz00_1811;
															obj_t BgL_codez00_1810;

															BgL_codez00_1810 = BgL_arg1852z00_504;
															BgL_iz00_1811 = BgL_arg1853z00_505;
															BgL_labsz00_1812 = BgL_arg1854z00_506;
															BgL_labsz00_501 = BgL_labsz00_1812;
															BgL_iz00_500 = BgL_iz00_1811;
															BgL_codez00_499 = BgL_codez00_1810;
															goto BgL_zc3z04anonymousza31850ze3z87_502;
														}
													}
											}
											BgL_arg1866z00_518 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(37), BgL_arg1870z00_522);
										}
									}
									BgL_arg1868z00_519 = (BgL_iz00_514 + 1L);
									BgL_arg1869z00_520 = CDR(((obj_t) BgL_methodsz00_515));
									{
										obj_t BgL_methodsz00_1820;
										long BgL_iz00_1819;
										obj_t BgL_codez00_1818;

										BgL_codez00_1818 = BgL_arg1866z00_518;
										BgL_iz00_1819 = BgL_arg1868z00_519;
										BgL_methodsz00_1820 = BgL_arg1869z00_520;
										BgL_methodsz00_515 = BgL_methodsz00_1820;
										BgL_iz00_514 = BgL_iz00_1819;
										BgL_codez00_513 = BgL_codez00_1818;
										goto BgL_zc3z04anonymousza31863ze3z87_516;
									}
								}
						}
						BgL_arg1752z00_439 =
							MAKE_YOUNG_PAIR(BgL_arg1753z00_440, BgL_arg1754z00_441);
					}
					return bgl_reverse_bang(BgL_arg1752z00_439);
				}
			}
		}

	}



/* profile-make-res */
	obj_t BGl_profilezd2makezd2resz00zzjas_profilez00(long BgL_siza7eza7_27,
		obj_t BgL_resz00_28, obj_t BgL_methodsz00_29)
	{
		{	/* Jas/profile.scm 251 */
			{
				obj_t BgL_codez00_598;
				long BgL_iz00_599;
				obj_t BgL_methodsz00_600;
				obj_t BgL_mz00_574;
				obj_t BgL_lz00_563;
				long BgL_rz00_564;
				obj_t BgL_lz00_551;

				{	/* Jas/profile.scm 280 */
					obj_t BgL_arg1880z00_540;

					{	/* Jas/profile.scm 280 */
						obj_t BgL_arg1882z00_541;
						obj_t BgL_arg1883z00_542;

						{	/* Jas/profile.scm 280 */
							obj_t BgL_arg1884z00_543;

							BgL_arg1884z00_543 = MAKE_YOUNG_PAIR(BgL_resz00_28, BNIL);
							BgL_arg1882z00_541 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(38), BgL_arg1884z00_543);
						}
						{	/* Jas/profile.scm 281 */
							obj_t BgL_arg1885z00_544;

							{	/* Jas/profile.scm 281 */
								obj_t BgL_arg1887z00_545;
								obj_t BgL_arg1888z00_546;

								{	/* Jas/profile.scm 281 */
									obj_t BgL_arg1889z00_547;

									{	/* Jas/profile.scm 281 */
										obj_t BgL_arg1890z00_548;

										{	/* Jas/profile.scm 281 */
											obj_t BgL_arg1891z00_549;

											BgL_arg1891z00_549 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(42), BNIL);
											BgL_arg1890z00_548 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1891z00_549);
										}
										BgL_arg1889z00_547 =
											MAKE_YOUNG_PAIR(BgL_arg1890z00_548, BNIL);
									}
									BgL_arg1887z00_545 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(39), BgL_arg1889z00_547);
								}
								BgL_arg1888z00_546 =
									MAKE_YOUNG_PAIR(BGl_pushzd2intzd2zzjas_profilez00(BINT
										(BgL_siza7eza7_27)), BNIL);
								BgL_arg1885z00_544 =
									MAKE_YOUNG_PAIR(BgL_arg1887z00_545, BgL_arg1888z00_546);
							}
							BgL_codez00_598 = BgL_arg1885z00_544;
							BgL_iz00_599 = 0L;
							BgL_methodsz00_600 = BgL_methodsz00_29;
						BgL_zc3z04anonymousza31929ze3z87_601:
							if (NULLP(BgL_methodsz00_600))
								{	/* Jas/profile.scm 269 */
									BgL_arg1883z00_542 = BgL_codez00_598;
								}
							else
								{	/* Jas/profile.scm 273 */
									obj_t BgL_arg1931z00_603;
									long BgL_arg1932z00_604;
									obj_t BgL_arg1933z00_605;

									{	/* Jas/profile.scm 273 */
										obj_t BgL_arg1934z00_606;

										{	/* Jas/profile.scm 273 */
											obj_t BgL_arg1935z00_607;

											{	/* Jas/profile.scm 273 */
												obj_t BgL_arg1936z00_608;
												obj_t BgL_arg1937z00_609;

												{	/* Jas/profile.scm 273 */
													obj_t BgL_arg1938z00_610;

													{	/* Jas/profile.scm 273 */
														obj_t BgL_arg1939z00_611;

														BgL_arg1939z00_611 =
															CAR(((obj_t) BgL_methodsz00_600));
														BgL_mz00_574 = BgL_arg1939z00_611;
														{

															if (PAIRP(BgL_mz00_574))
																{	/* Jas/profile.scm 267 */
																	obj_t BgL_cdrzd2247zd2_584;

																	BgL_cdrzd2247zd2_584 =
																		CDR(((obj_t) BgL_mz00_574));
																	if (
																		(CAR(
																				((obj_t) BgL_mz00_574)) ==
																			CNST_TABLE_REF(11)))
																		{	/* Jas/profile.scm 267 */
																			if (PAIRP(BgL_cdrzd2247zd2_584))
																				{	/* Jas/profile.scm 267 */
																					obj_t BgL_cdrzd2253zd2_588;

																					BgL_cdrzd2253zd2_588 =
																						CDR(BgL_cdrzd2247zd2_584);
																					if (PAIRP(BgL_cdrzd2253zd2_588))
																						{	/* Jas/profile.scm 267 */
																							obj_t BgL_cdrzd2259zd2_590;

																							BgL_cdrzd2259zd2_590 =
																								CDR(BgL_cdrzd2253zd2_588);
																							if (PAIRP(BgL_cdrzd2259zd2_590))
																								{	/* Jas/profile.scm 267 */
																									obj_t BgL_arg1926z00_595;

																									BgL_arg1926z00_595 =
																										CDR(BgL_cdrzd2259zd2_590);
																									{	/* Jas/profile.scm 266 */
																										obj_t BgL_arg1928z00_952;

																										BgL_arg1928z00_952 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(25),
																											BgL_arg1926z00_595);
																										{	/* Jas/profile.scm 266 */
																											long BgL_tmpz00_1861;

																											BgL_lz00_563 =
																												BgL_arg1928z00_952;
																											BgL_rz00_564 = 0L;
																										BgL_zc3z04anonymousza31904ze3z87_565:
																											if (NULLP
																												(BgL_lz00_563))
																												{	/* Jas/profile.scm 259 */
																													BgL_tmpz00_1861 =
																														BgL_rz00_564;
																												}
																											else
																												{	/* Jas/profile.scm 260 */
																													bool_t
																														BgL_test2150z00_1864;
																													{	/* Jas/profile.scm 260 */
																														obj_t
																															BgL_tmpz00_1865;
																														BgL_tmpz00_1865 =
																															CAR(((obj_t)
																																BgL_lz00_563));
																														BgL_test2150z00_1864
																															=
																															PAIRP
																															(BgL_tmpz00_1865);
																													}
																													if (BgL_test2150z00_1864)
																														{	/* Jas/profile.scm 262 */
																															obj_t
																																BgL_arg1910z00_569;
																															BgL_arg1910z00_569
																																=
																																CDR(((obj_t)
																																	BgL_lz00_563));
																															{
																																obj_t
																																	BgL_lz00_1871;
																																BgL_lz00_1871 =
																																	BgL_arg1910z00_569;
																																BgL_lz00_563 =
																																	BgL_lz00_1871;
																																goto
																																	BgL_zc3z04anonymousza31904ze3z87_565;
																															}
																														}
																													else
																														{	/* Jas/profile.scm 261 */
																															obj_t
																																BgL_arg1911z00_570;
																															long
																																BgL_arg1912z00_571;
																															{	/* Jas/profile.scm 261 */
																																obj_t
																																	BgL_arg1913z00_572;
																																BgL_arg1913z00_572
																																	=
																																	CDR(((obj_t)
																																		BgL_lz00_563));
																																BgL_lz00_551 =
																																	BgL_arg1913z00_572;
																															BgL_zc3z04anonymousza31893ze3z87_552:
																																if (NULLP
																																	(BgL_lz00_551))
																																	{	/* Jas/profile.scm 253 */
																																		BgL_arg1911z00_570
																																			= BNIL;
																																	}
																																else
																																	{	/* Jas/profile.scm 254 */
																																		bool_t
																																			BgL_test2152z00_1876;
																																		{	/* Jas/profile.scm 254 */
																																			bool_t
																																				BgL_test2153z00_1877;
																																			{	/* Jas/profile.scm 254 */
																																				obj_t
																																					BgL_tmpz00_1878;
																																				BgL_tmpz00_1878
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_lz00_551));
																																				BgL_test2153z00_1877
																																					=
																																					PAIRP
																																					(BgL_tmpz00_1878);
																																			}
																																			if (BgL_test2153z00_1877)
																																				{	/* Jas/profile.scm 255 */
																																					obj_t
																																						BgL_tmpz00_1882;
																																					{	/* Jas/profile.scm 255 */
																																						obj_t
																																							BgL_auxz00_1883;
																																						{	/* Jas/profile.scm 255 */
																																							obj_t
																																								BgL_pairz00_937;
																																							BgL_pairz00_937
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_lz00_551));
																																							BgL_auxz00_1883
																																								=
																																								CAR
																																								(BgL_pairz00_937);
																																						}
																																						BgL_tmpz00_1882
																																							=
																																							BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																																							(BgL_auxz00_1883,
																																							CNST_TABLE_REF
																																							(24));
																																					}
																																					BgL_test2152z00_1876
																																						=
																																						CBOOL
																																						(BgL_tmpz00_1882);
																																				}
																																			else
																																				{	/* Jas/profile.scm 254 */
																																					BgL_test2152z00_1876
																																						=
																																						(
																																						(bool_t)
																																						1);
																																				}
																																		}
																																		if (BgL_test2152z00_1876)
																																			{	/* Jas/profile.scm 256 */
																																				obj_t
																																					BgL_arg1901z00_559;
																																				BgL_arg1901z00_559
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_lz00_551));
																																				{
																																					obj_t
																																						BgL_lz00_1892;
																																					BgL_lz00_1892
																																						=
																																						BgL_arg1901z00_559;
																																					BgL_lz00_551
																																						=
																																						BgL_lz00_1892;
																																					goto
																																						BgL_zc3z04anonymousza31893ze3z87_552;
																																				}
																																			}
																																		else
																																			{	/* Jas/profile.scm 254 */
																																				BgL_arg1911z00_570
																																					=
																																					BgL_lz00_551;
																																			}
																																	}
																															}
																															BgL_arg1912z00_571
																																=
																																(BgL_rz00_564 +
																																1L);
																															{
																																long
																																	BgL_rz00_1895;
																																obj_t
																																	BgL_lz00_1894;
																																BgL_lz00_1894 =
																																	BgL_arg1911z00_570;
																																BgL_rz00_1895 =
																																	BgL_arg1912z00_571;
																																BgL_rz00_564 =
																																	BgL_rz00_1895;
																																BgL_lz00_563 =
																																	BgL_lz00_1894;
																																goto
																																	BgL_zc3z04anonymousza31904ze3z87_565;
																															}
																														}
																												}
																											BgL_arg1938z00_610 =
																												BINT(BgL_tmpz00_1861);
																										}
																									}
																								}
																							else
																								{	/* Jas/profile.scm 267 */
																								BgL_tagzd2236zd2_581:
																									BgL_arg1938z00_610 =
																										BGl_errorz00zz__errorz00
																										(BGl_string2003z00zzjas_profilez00,
																										BGl_string2004z00zzjas_profilez00,
																										BgL_mz00_574);
																								}
																						}
																					else
																						{	/* Jas/profile.scm 267 */
																							goto BgL_tagzd2236zd2_581;
																						}
																				}
																			else
																				{	/* Jas/profile.scm 267 */
																					goto BgL_tagzd2236zd2_581;
																				}
																		}
																	else
																		{	/* Jas/profile.scm 267 */
																			goto BgL_tagzd2236zd2_581;
																		}
																}
															else
																{	/* Jas/profile.scm 267 */
																	goto BgL_tagzd2236zd2_581;
																}
														}
													}
													BgL_arg1936z00_608 =
														BGl_pushzd2intzd2zzjas_profilez00
														(BgL_arg1938z00_610);
												}
												{	/* Jas/profile.scm 274 */
													obj_t BgL_arg1940z00_612;
													obj_t BgL_arg1941z00_613;

													BgL_arg1940z00_612 =
														BGl_pushzd2intzd2zzjas_profilez00(BINT
														(BgL_iz00_599));
													BgL_arg1941z00_613 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(36),
														BgL_codez00_598);
													BgL_arg1937z00_609 =
														MAKE_YOUNG_PAIR(BgL_arg1940z00_612,
														BgL_arg1941z00_613);
												}
												BgL_arg1935z00_607 =
													MAKE_YOUNG_PAIR(BgL_arg1936z00_608,
													BgL_arg1937z00_609);
											}
											BgL_arg1934z00_606 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(41), BgL_arg1935z00_607);
										}
										BgL_arg1931z00_603 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(37), BgL_arg1934z00_606);
									}
									BgL_arg1932z00_604 = (BgL_iz00_599 + 1L);
									BgL_arg1933z00_605 = CDR(((obj_t) BgL_methodsz00_600));
									{
										obj_t BgL_methodsz00_1914;
										long BgL_iz00_1913;
										obj_t BgL_codez00_1912;

										BgL_codez00_1912 = BgL_arg1931z00_603;
										BgL_iz00_1913 = BgL_arg1932z00_604;
										BgL_methodsz00_1914 = BgL_arg1933z00_605;
										BgL_methodsz00_600 = BgL_methodsz00_1914;
										BgL_iz00_599 = BgL_iz00_1913;
										BgL_codez00_598 = BgL_codez00_1912;
										goto BgL_zc3z04anonymousza31929ze3z87_601;
									}
								}
						}
						BgL_arg1880z00_540 =
							MAKE_YOUNG_PAIR(BgL_arg1882z00_541, BgL_arg1883z00_542);
					}
					return bgl_reverse_bang(BgL_arg1880z00_540);
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzjas_profilez00(void)
	{
		{	/* Jas/profile.scm 1 */
			{	/* Jas/profile.scm 5 */
				obj_t BgL_arg1945z00_621;
				obj_t BgL_arg1946z00_622;

				{	/* Jas/profile.scm 5 */
					obj_t BgL_v1077z00_640;

					BgL_v1077z00_640 = create_vector(8L);
					{	/* Jas/profile.scm 5 */
						obj_t BgL_arg1952z00_641;

						BgL_arg1952z00_641 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(43),
							BGl_proc2008z00zzjas_profilez00, BGl_proc2007z00zzjas_profilez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(44));
						VECTOR_SET(BgL_v1077z00_640, 0L, BgL_arg1952z00_641);
					}
					{	/* Jas/profile.scm 5 */
						obj_t BgL_arg1957z00_651;

						BgL_arg1957z00_651 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(45),
							BGl_proc2010z00zzjas_profilez00, BGl_proc2009z00zzjas_profilez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(44));
						VECTOR_SET(BgL_v1077z00_640, 1L, BgL_arg1957z00_651);
					}
					{	/* Jas/profile.scm 5 */
						obj_t BgL_arg1962z00_661;

						BgL_arg1962z00_661 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(46),
							BGl_proc2012z00zzjas_profilez00, BGl_proc2011z00zzjas_profilez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(44));
						VECTOR_SET(BgL_v1077z00_640, 2L, BgL_arg1962z00_661);
					}
					{	/* Jas/profile.scm 5 */
						obj_t BgL_arg1967z00_671;

						BgL_arg1967z00_671 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(47),
							BGl_proc2014z00zzjas_profilez00, BGl_proc2013z00zzjas_profilez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(44));
						VECTOR_SET(BgL_v1077z00_640, 3L, BgL_arg1967z00_671);
					}
					{	/* Jas/profile.scm 5 */
						obj_t BgL_arg1972z00_681;

						BgL_arg1972z00_681 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(48),
							BGl_proc2016z00zzjas_profilez00, BGl_proc2015z00zzjas_profilez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(44));
						VECTOR_SET(BgL_v1077z00_640, 4L, BgL_arg1972z00_681);
					}
					{	/* Jas/profile.scm 5 */
						obj_t BgL_arg1977z00_691;

						BgL_arg1977z00_691 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(49),
							BGl_proc2018z00zzjas_profilez00, BGl_proc2017z00zzjas_profilez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(44));
						VECTOR_SET(BgL_v1077z00_640, 5L, BgL_arg1977z00_691);
					}
					{	/* Jas/profile.scm 5 */
						obj_t BgL_arg1982z00_701;

						BgL_arg1982z00_701 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(50),
							BGl_proc2020z00zzjas_profilez00, BGl_proc2019z00zzjas_profilez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(44));
						VECTOR_SET(BgL_v1077z00_640, 6L, BgL_arg1982z00_701);
					}
					{	/* Jas/profile.scm 5 */
						obj_t BgL_arg1987z00_711;

						BgL_arg1987z00_711 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(51),
							BGl_proc2022z00zzjas_profilez00, BGl_proc2021z00zzjas_profilez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(44));
						VECTOR_SET(BgL_v1077z00_640, 7L, BgL_arg1987z00_711);
					}
					BgL_arg1945z00_621 = BgL_v1077z00_640;
				}
				{	/* Jas/profile.scm 5 */
					obj_t BgL_v1078z00_721;

					BgL_v1078z00_721 = create_vector(0L);
					BgL_arg1946z00_622 = BgL_v1078z00_721;
				}
				return (BGl__envz00zzjas_profilez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(52),
						CNST_TABLE_REF(53), BGl_objectz00zz__objectz00, 12034L,
						BGl_proc2025z00zzjas_profilez00, BGl_proc2024z00zzjas_profilez00,
						BFALSE, BGl_proc2023z00zzjas_profilez00, BFALSE, BgL_arg1945z00_621,
						BgL_arg1946z00_622), BUNSPEC);
			}
		}

	}



/* &<@anonymous:1951> */
	obj_t BGl_z62zc3z04anonymousza31951ze3ze5zzjas_profilez00(obj_t
		BgL_envz00_994, obj_t BgL_new1057z00_995)
	{
		{	/* Jas/profile.scm 5 */
			{
				BgL__envz00_bglt BgL_auxz00_1954;

				((((BgL__envz00_bglt) COBJECT(
								((BgL__envz00_bglt) BgL_new1057z00_995)))->BgL_thisz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL__envz00_bglt) COBJECT(((BgL__envz00_bglt)
									BgL_new1057z00_995)))->BgL_jstringz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL__envz00_bglt) COBJECT(((BgL__envz00_bglt)
									BgL_new1057z00_995)))->BgL_has_clinitz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL__envz00_bglt) COBJECT(((BgL__envz00_bglt)
									BgL_new1057z00_995)))->BgL_clinitz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL__envz00_bglt) COBJECT(((BgL__envz00_bglt)
									BgL_new1057z00_995)))->BgL_resz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL__envz00_bglt) COBJECT(((BgL__envz00_bglt)
									BgL_new1057z00_995)))->BgL_namesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL__envz00_bglt) COBJECT(((BgL__envz00_bglt)
									BgL_new1057z00_995)))->BgL_lnamesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL__envz00_bglt) COBJECT(((BgL__envz00_bglt)
									BgL_new1057z00_995)))->BgL_linesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_1954 = ((BgL__envz00_bglt) BgL_new1057z00_995);
				return ((obj_t) BgL_auxz00_1954);
			}
		}

	}



/* &lambda1949 */
	BgL__envz00_bglt BGl_z62lambda1949z62zzjas_profilez00(obj_t BgL_envz00_996)
	{
		{	/* Jas/profile.scm 5 */
			{	/* Jas/profile.scm 5 */
				BgL__envz00_bglt BgL_new1056z00_1059;

				BgL_new1056z00_1059 =
					((BgL__envz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL__envz00_bgl))));
				{	/* Jas/profile.scm 5 */
					long BgL_arg1950z00_1060;

					BgL_arg1950z00_1060 = BGL_CLASS_NUM(BGl__envz00zzjas_profilez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1056z00_1059), BgL_arg1950z00_1060);
				}
				return BgL_new1056z00_1059;
			}
		}

	}



/* &lambda1947 */
	BgL__envz00_bglt BGl_z62lambda1947z62zzjas_profilez00(obj_t BgL_envz00_997,
		obj_t BgL_this1048z00_998, obj_t BgL_jstring1049z00_999,
		obj_t BgL_has_clinit1050z00_1000, obj_t BgL_clinit1051z00_1001,
		obj_t BgL_res1052z00_1002, obj_t BgL_names1053z00_1003,
		obj_t BgL_lnames1054z00_1004, obj_t BgL_lines1055z00_1005)
	{
		{	/* Jas/profile.scm 5 */
			{	/* Jas/profile.scm 5 */
				BgL__envz00_bglt BgL_new1071z00_1061;

				{	/* Jas/profile.scm 5 */
					BgL__envz00_bglt BgL_new1070z00_1062;

					BgL_new1070z00_1062 =
						((BgL__envz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL__envz00_bgl))));
					{	/* Jas/profile.scm 5 */
						long BgL_arg1948z00_1063;

						BgL_arg1948z00_1063 = BGL_CLASS_NUM(BGl__envz00zzjas_profilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1070z00_1062), BgL_arg1948z00_1063);
					}
					BgL_new1071z00_1061 = BgL_new1070z00_1062;
				}
				((((BgL__envz00_bglt) COBJECT(BgL_new1071z00_1061))->BgL_thisz00) =
					((obj_t) BgL_this1048z00_998), BUNSPEC);
				((((BgL__envz00_bglt) COBJECT(BgL_new1071z00_1061))->BgL_jstringz00) =
					((obj_t) BgL_jstring1049z00_999), BUNSPEC);
				((((BgL__envz00_bglt) COBJECT(BgL_new1071z00_1061))->
						BgL_has_clinitz00) = ((obj_t) BgL_has_clinit1050z00_1000), BUNSPEC);
				((((BgL__envz00_bglt) COBJECT(BgL_new1071z00_1061))->BgL_clinitz00) =
					((obj_t) BgL_clinit1051z00_1001), BUNSPEC);
				((((BgL__envz00_bglt) COBJECT(BgL_new1071z00_1061))->BgL_resz00) =
					((obj_t) BgL_res1052z00_1002), BUNSPEC);
				((((BgL__envz00_bglt) COBJECT(BgL_new1071z00_1061))->BgL_namesz00) =
					((obj_t) BgL_names1053z00_1003), BUNSPEC);
				((((BgL__envz00_bglt) COBJECT(BgL_new1071z00_1061))->BgL_lnamesz00) =
					((obj_t) BgL_lnames1054z00_1004), BUNSPEC);
				((((BgL__envz00_bglt) COBJECT(BgL_new1071z00_1061))->BgL_linesz00) =
					((obj_t) BgL_lines1055z00_1005), BUNSPEC);
				return BgL_new1071z00_1061;
			}
		}

	}



/* &lambda1991 */
	obj_t BGl_z62lambda1991z62zzjas_profilez00(obj_t BgL_envz00_1006,
		obj_t BgL_oz00_1007, obj_t BgL_vz00_1008)
	{
		{	/* Jas/profile.scm 5 */
			return
				((((BgL__envz00_bglt) COBJECT(
							((BgL__envz00_bglt) BgL_oz00_1007)))->BgL_linesz00) =
				((obj_t) BgL_vz00_1008), BUNSPEC);
		}

	}



/* &lambda1990 */
	obj_t BGl_z62lambda1990z62zzjas_profilez00(obj_t BgL_envz00_1009,
		obj_t BgL_oz00_1010)
	{
		{	/* Jas/profile.scm 5 */
			return
				(((BgL__envz00_bglt) COBJECT(
						((BgL__envz00_bglt) BgL_oz00_1010)))->BgL_linesz00);
		}

	}



/* &lambda1986 */
	obj_t BGl_z62lambda1986z62zzjas_profilez00(obj_t BgL_envz00_1011,
		obj_t BgL_oz00_1012, obj_t BgL_vz00_1013)
	{
		{	/* Jas/profile.scm 5 */
			return
				((((BgL__envz00_bglt) COBJECT(
							((BgL__envz00_bglt) BgL_oz00_1012)))->BgL_lnamesz00) =
				((obj_t) BgL_vz00_1013), BUNSPEC);
		}

	}



/* &lambda1985 */
	obj_t BGl_z62lambda1985z62zzjas_profilez00(obj_t BgL_envz00_1014,
		obj_t BgL_oz00_1015)
	{
		{	/* Jas/profile.scm 5 */
			return
				(((BgL__envz00_bglt) COBJECT(
						((BgL__envz00_bglt) BgL_oz00_1015)))->BgL_lnamesz00);
		}

	}



/* &lambda1981 */
	obj_t BGl_z62lambda1981z62zzjas_profilez00(obj_t BgL_envz00_1016,
		obj_t BgL_oz00_1017, obj_t BgL_vz00_1018)
	{
		{	/* Jas/profile.scm 5 */
			return
				((((BgL__envz00_bglt) COBJECT(
							((BgL__envz00_bglt) BgL_oz00_1017)))->BgL_namesz00) =
				((obj_t) BgL_vz00_1018), BUNSPEC);
		}

	}



/* &lambda1980 */
	obj_t BGl_z62lambda1980z62zzjas_profilez00(obj_t BgL_envz00_1019,
		obj_t BgL_oz00_1020)
	{
		{	/* Jas/profile.scm 5 */
			return
				(((BgL__envz00_bglt) COBJECT(
						((BgL__envz00_bglt) BgL_oz00_1020)))->BgL_namesz00);
		}

	}



/* &lambda1976 */
	obj_t BGl_z62lambda1976z62zzjas_profilez00(obj_t BgL_envz00_1021,
		obj_t BgL_oz00_1022, obj_t BgL_vz00_1023)
	{
		{	/* Jas/profile.scm 5 */
			return
				((((BgL__envz00_bglt) COBJECT(
							((BgL__envz00_bglt) BgL_oz00_1022)))->BgL_resz00) =
				((obj_t) BgL_vz00_1023), BUNSPEC);
		}

	}



/* &lambda1975 */
	obj_t BGl_z62lambda1975z62zzjas_profilez00(obj_t BgL_envz00_1024,
		obj_t BgL_oz00_1025)
	{
		{	/* Jas/profile.scm 5 */
			return
				(((BgL__envz00_bglt) COBJECT(
						((BgL__envz00_bglt) BgL_oz00_1025)))->BgL_resz00);
		}

	}



/* &lambda1971 */
	obj_t BGl_z62lambda1971z62zzjas_profilez00(obj_t BgL_envz00_1026,
		obj_t BgL_oz00_1027, obj_t BgL_vz00_1028)
	{
		{	/* Jas/profile.scm 5 */
			return
				((((BgL__envz00_bglt) COBJECT(
							((BgL__envz00_bglt) BgL_oz00_1027)))->BgL_clinitz00) =
				((obj_t) BgL_vz00_1028), BUNSPEC);
		}

	}



/* &lambda1970 */
	obj_t BGl_z62lambda1970z62zzjas_profilez00(obj_t BgL_envz00_1029,
		obj_t BgL_oz00_1030)
	{
		{	/* Jas/profile.scm 5 */
			return
				(((BgL__envz00_bglt) COBJECT(
						((BgL__envz00_bglt) BgL_oz00_1030)))->BgL_clinitz00);
		}

	}



/* &lambda1966 */
	obj_t BGl_z62lambda1966z62zzjas_profilez00(obj_t BgL_envz00_1031,
		obj_t BgL_oz00_1032, obj_t BgL_vz00_1033)
	{
		{	/* Jas/profile.scm 5 */
			return
				((((BgL__envz00_bglt) COBJECT(
							((BgL__envz00_bglt) BgL_oz00_1032)))->BgL_has_clinitz00) =
				((obj_t) BgL_vz00_1033), BUNSPEC);
		}

	}



/* &lambda1965 */
	obj_t BGl_z62lambda1965z62zzjas_profilez00(obj_t BgL_envz00_1034,
		obj_t BgL_oz00_1035)
	{
		{	/* Jas/profile.scm 5 */
			return
				(((BgL__envz00_bglt) COBJECT(
						((BgL__envz00_bglt) BgL_oz00_1035)))->BgL_has_clinitz00);
		}

	}



/* &lambda1961 */
	obj_t BGl_z62lambda1961z62zzjas_profilez00(obj_t BgL_envz00_1036,
		obj_t BgL_oz00_1037, obj_t BgL_vz00_1038)
	{
		{	/* Jas/profile.scm 5 */
			return
				((((BgL__envz00_bglt) COBJECT(
							((BgL__envz00_bglt) BgL_oz00_1037)))->BgL_jstringz00) =
				((obj_t) BgL_vz00_1038), BUNSPEC);
		}

	}



/* &lambda1960 */
	obj_t BGl_z62lambda1960z62zzjas_profilez00(obj_t BgL_envz00_1039,
		obj_t BgL_oz00_1040)
	{
		{	/* Jas/profile.scm 5 */
			return
				(((BgL__envz00_bglt) COBJECT(
						((BgL__envz00_bglt) BgL_oz00_1040)))->BgL_jstringz00);
		}

	}



/* &lambda1956 */
	obj_t BGl_z62lambda1956z62zzjas_profilez00(obj_t BgL_envz00_1041,
		obj_t BgL_oz00_1042, obj_t BgL_vz00_1043)
	{
		{	/* Jas/profile.scm 5 */
			return
				((((BgL__envz00_bglt) COBJECT(
							((BgL__envz00_bglt) BgL_oz00_1042)))->BgL_thisz00) =
				((obj_t) BgL_vz00_1043), BUNSPEC);
		}

	}



/* &lambda1955 */
	obj_t BGl_z62lambda1955z62zzjas_profilez00(obj_t BgL_envz00_1044,
		obj_t BgL_oz00_1045)
	{
		{	/* Jas/profile.scm 5 */
			return
				(((BgL__envz00_bglt) COBJECT(
						((BgL__envz00_bglt) BgL_oz00_1045)))->BgL_thisz00);
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzjas_profilez00(void)
	{
		{	/* Jas/profile.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzjas_profilez00(void)
	{
		{	/* Jas/profile.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzjas_profilez00(void)
	{
		{	/* Jas/profile.scm 1 */
			return
				BGl_modulezd2initializa7ationz75zzjas_libz00(267157509L,
				BSTRING_TO_STRING(BGl_string2026z00zzjas_profilez00));
		}

	}

#ifdef __cplusplus
}
#endif
