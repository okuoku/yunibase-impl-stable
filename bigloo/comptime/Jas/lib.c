/*===========================================================================*/
/*   (Jas/lib.scm)                                                           */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Jas/lib.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_JAS_LIB_TYPE_DEFINITIONS
#define BGL_JAS_LIB_TYPE_DEFINITIONS
#endif													// BGL_JAS_LIB_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62w2z62zzjas_libz00(obj_t, obj_t);
	static obj_t BGl_z62w4z62zzjas_libz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzjas_libz00 = BUNSPEC;
	static obj_t BGl_z62w4elongz62zzjas_libz00(obj_t, obj_t);
	static obj_t BGl_stringzd2ze3shortlistz31zzjas_libz00(obj_t);
	static long BGl_utf8zd2length1zd2zzjas_libz00(long);
	static obj_t BGl_genericzd2initzd2zzjas_libz00(void);
	static obj_t BGl_objectzd2initzd2zzjas_libz00(void);
	static long BGl_utf8zd2lengthzd2zzjas_libz00(obj_t);
	static obj_t BGl_methodzd2initzd2zzjas_libz00(void);
	static obj_t BGl_z62stringzd2ze3utf8z53zzjas_libz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_u2z00zzjas_libz00(int);
	BGL_EXPORTED_DECL obj_t BGl_u4z00zzjas_libz00(int);
	BGL_IMPORT obj_t make_string(long, unsigned char);
	BGL_EXPORTED_DECL obj_t BGl_w2z00zzjas_libz00(int);
	BGL_EXPORTED_DECL obj_t BGl_w4z00zzjas_libz00(long);
	static obj_t BGl_z62f2z62zzjas_libz00(obj_t, obj_t);
	static obj_t BGl_z62f4z62zzjas_libz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_w4llongz00zzjas_libz00(BGL_LONGLONG_T);
	BGL_IMPORT obj_t bgl_double_to_ieee_string(double);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzjas_libz00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_w4elongz00zzjas_libz00(long);
	static obj_t BGl_producezd2utf8zd2zzjas_libz00(obj_t, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzjas_libz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzjas_libz00(void);
	static long BGl_producezd2utf8zd21z00zzjas_libz00(obj_t, long, long);
	static obj_t BGl_collectze70ze7zzjas_libz00(long, obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_f2z00zzjas_libz00(float);
	BGL_EXPORTED_DECL obj_t BGl_f4z00zzjas_libz00(double);
	BGL_IMPORT obj_t bgl_float_to_ieee_string(float);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2ze3utf8z31zzjas_libz00(obj_t);
	static obj_t BGl_z62u2z62zzjas_libz00(obj_t, obj_t);
	static obj_t BGl_z62u4z62zzjas_libz00(obj_t, obj_t);
	static obj_t BGl_z62w4llongz62zzjas_libz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_stringzd2ze3utf8zd2envze3zzjas_libz00,
		BgL_bgl_za762stringza7d2za7e3u1206za7,
		BGl_z62stringzd2ze3utf8z53zzjas_libz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_f4zd2envzd2zzjas_libz00,
		BgL_bgl_za762f4za762za7za7jas_li1207z00, BGl_z62f4z62zzjas_libz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u2zd2envzd2zzjas_libz00,
		BgL_bgl_za762u2za762za7za7jas_li1208z00, BGl_z62u2z62zzjas_libz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_w4llongzd2envzd2zzjas_libz00,
		BgL_bgl_za762w4llongza762za7za7j1209z00, BGl_z62w4llongz62zzjas_libz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u4zd2envzd2zzjas_libz00,
		BgL_bgl_za762u4za762za7za7jas_li1210z00, BGl_z62u4z62zzjas_libz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_w2zd2envzd2zzjas_libz00,
		BgL_bgl_za762w2za762za7za7jas_li1211z00, BGl_z62w2z62zzjas_libz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_w4elongzd2envzd2zzjas_libz00,
		BgL_bgl_za762w4elongza762za7za7j1212z00, BGl_z62w4elongz62zzjas_libz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_w4zd2envzd2zzjas_libz00,
		BgL_bgl_za762w4za762za7za7jas_li1213z00, BGl_z62w4z62zzjas_libz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_f2zd2envzd2zzjas_libz00,
		BgL_bgl_za762f2za762za7za7jas_li1214z00, BGl_z62f2z62zzjas_libz00, 0L,
		BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzjas_libz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzjas_libz00(long
		BgL_checksumz00_434, char *BgL_fromz00_435)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzjas_libz00))
				{
					BGl_requirezd2initializa7ationz75zzjas_libz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzjas_libz00();
					BGl_libraryzd2moduleszd2initz00zzjas_libz00();
					return BGl_methodzd2initzd2zzjas_libz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzjas_libz00(void)
	{
		{	/* Jas/lib.scm 1 */
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L, "jas_lib");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "jas_lib");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"jas_lib");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"jas_lib");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"jas_lib");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "jas_lib");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "jas_lib");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzjas_libz00(void)
	{
		{	/* Jas/lib.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* u2 */
	BGL_EXPORTED_DEF obj_t BGl_u2z00zzjas_libz00(int BgL_nz00_3)
	{
		{	/* Jas/lib.scm 13 */
			{	/* Jas/lib.scm 14 */
				long BgL_arg1015z00_35;
				long BgL_arg1016z00_36;

				BgL_arg1015z00_35 = (((long) (BgL_nz00_3) >> (int) (8L)) & 255L);
				BgL_arg1016z00_36 = ((long) (BgL_nz00_3) & 255L);
				{	/* Jas/lib.scm 14 */
					obj_t BgL_list1017z00_37;

					{	/* Jas/lib.scm 14 */
						obj_t BgL_arg1018z00_38;

						BgL_arg1018z00_38 = MAKE_YOUNG_PAIR(BINT(BgL_arg1016z00_36), BNIL);
						BgL_list1017z00_37 =
							MAKE_YOUNG_PAIR(BINT(BgL_arg1015z00_35), BgL_arg1018z00_38);
					}
					return BgL_list1017z00_37;
				}
			}
		}

	}



/* &u2 */
	obj_t BGl_z62u2z62zzjas_libz00(obj_t BgL_envz00_414, obj_t BgL_nz00_415)
	{
		{	/* Jas/lib.scm 13 */
			return BGl_u2z00zzjas_libz00(CINT(BgL_nz00_415));
		}

	}



/* u4 */
	BGL_EXPORTED_DEF obj_t BGl_u4z00zzjas_libz00(int BgL_nz00_4)
	{
		{	/* Jas/lib.scm 17 */
			{	/* Jas/lib.scm 18 */
				long BgL_arg1020z00_40;
				long BgL_arg1021z00_41;
				long BgL_arg1022z00_42;
				long BgL_arg1023z00_43;

				BgL_arg1020z00_40 = (((long) (BgL_nz00_4) >> (int) (24L)) & 255L);
				BgL_arg1021z00_41 = (((long) (BgL_nz00_4) >> (int) (16L)) & 255L);
				BgL_arg1022z00_42 = (((long) (BgL_nz00_4) >> (int) (8L)) & 255L);
				BgL_arg1023z00_43 = ((long) (BgL_nz00_4) & 255L);
				{	/* Jas/lib.scm 18 */
					obj_t BgL_list1024z00_44;

					{	/* Jas/lib.scm 18 */
						obj_t BgL_arg1025z00_45;

						{	/* Jas/lib.scm 18 */
							obj_t BgL_arg1026z00_46;

							{	/* Jas/lib.scm 18 */
								obj_t BgL_arg1027z00_47;

								BgL_arg1027z00_47 =
									MAKE_YOUNG_PAIR(BINT(BgL_arg1023z00_43), BNIL);
								BgL_arg1026z00_46 =
									MAKE_YOUNG_PAIR(BINT(BgL_arg1022z00_42), BgL_arg1027z00_47);
							}
							BgL_arg1025z00_45 =
								MAKE_YOUNG_PAIR(BINT(BgL_arg1021z00_41), BgL_arg1026z00_46);
						}
						BgL_list1024z00_44 =
							MAKE_YOUNG_PAIR(BINT(BgL_arg1020z00_40), BgL_arg1025z00_45);
					}
					return BgL_list1024z00_44;
				}
			}
		}

	}



/* &u4 */
	obj_t BGl_z62u4z62zzjas_libz00(obj_t BgL_envz00_416, obj_t BgL_nz00_417)
	{
		{	/* Jas/lib.scm 17 */
			return BGl_u4z00zzjas_libz00(CINT(BgL_nz00_417));
		}

	}



/* w2 */
	BGL_EXPORTED_DEF obj_t BGl_w2z00zzjas_libz00(int BgL_nz00_5)
	{
		{	/* Jas/lib.scm 23 */
			{	/* Jas/lib.scm 24 */
				long BgL_arg1033z00_51;
				long BgL_arg1035z00_52;

				BgL_arg1033z00_51 = (((long) (BgL_nz00_5) >> (int) (16L)) & 65535L);
				BgL_arg1035z00_52 = ((long) (BgL_nz00_5) & 65535L);
				{	/* Jas/lib.scm 24 */
					obj_t BgL_list1036z00_53;

					{	/* Jas/lib.scm 24 */
						obj_t BgL_arg1037z00_54;

						BgL_arg1037z00_54 = MAKE_YOUNG_PAIR(BINT(BgL_arg1035z00_52), BNIL);
						BgL_list1036z00_53 =
							MAKE_YOUNG_PAIR(BINT(BgL_arg1033z00_51), BgL_arg1037z00_54);
					}
					return BgL_list1036z00_53;
				}
			}
		}

	}



/* &w2 */
	obj_t BGl_z62w2z62zzjas_libz00(obj_t BgL_envz00_418, obj_t BgL_nz00_419)
	{
		{	/* Jas/lib.scm 23 */
			return BGl_w2z00zzjas_libz00(CINT(BgL_nz00_419));
		}

	}



/* w4elong */
	BGL_EXPORTED_DEF obj_t BGl_w4elongz00zzjas_libz00(long BgL_nz00_7)
	{
		{	/* Jas/lib.scm 30 */
			{	/* Jas/lib.scm 31 */
				long BgL_arg1041z00_58;
				long BgL_arg1042z00_59;
				long BgL_arg1044z00_60;
				long BgL_arg1045z00_61;

				{	/* Jas/lib.scm 31 */
					long BgL_arg1050z00_66;

					BgL_arg1050z00_66 =
						(((BgL_nz00_7 >> (int) (16L)) >> (int) (16L)) >> (int) (16L));
					{	/* Jas/lib.scm 28 */
						long BgL_arg1039z00_196;

						{	/* Jas/lib.scm 28 */
							long BgL_arg1040z00_197;

							BgL_arg1040z00_197 = (long) (65535L);
							BgL_arg1039z00_196 = (BgL_arg1050z00_66 & BgL_arg1040z00_197);
						}
						BgL_arg1041z00_58 = (long) (BgL_arg1039z00_196);
				}}
				{	/* Jas/lib.scm 32 */
					long BgL_arg1053z00_69;

					BgL_arg1053z00_69 = ((BgL_nz00_7 >> (int) (16L)) >> (int) (16L));
					{	/* Jas/lib.scm 28 */
						long BgL_arg1039z00_204;

						{	/* Jas/lib.scm 28 */
							long BgL_arg1040z00_205;

							BgL_arg1040z00_205 = (long) (65535L);
							BgL_arg1039z00_204 = (BgL_arg1053z00_69 & BgL_arg1040z00_205);
						}
						BgL_arg1042z00_59 = (long) (BgL_arg1039z00_204);
				}}
				{	/* Jas/lib.scm 33 */
					long BgL_arg1055z00_71;

					BgL_arg1055z00_71 = (BgL_nz00_7 >> (int) (16L));
					{	/* Jas/lib.scm 28 */
						long BgL_arg1039z00_211;

						{	/* Jas/lib.scm 28 */
							long BgL_arg1040z00_212;

							BgL_arg1040z00_212 = (long) (65535L);
							BgL_arg1039z00_211 = (BgL_arg1055z00_71 & BgL_arg1040z00_212);
						}
						BgL_arg1044z00_60 = (long) (BgL_arg1039z00_211);
				}}
				{	/* Jas/lib.scm 28 */
					long BgL_arg1039z00_217;

					{	/* Jas/lib.scm 28 */
						long BgL_arg1040z00_218;

						BgL_arg1040z00_218 = (long) (65535L);
						BgL_arg1039z00_217 = (BgL_nz00_7 & BgL_arg1040z00_218);
					}
					BgL_arg1045z00_61 = (long) (BgL_arg1039z00_217);
				}
				{	/* Jas/lib.scm 31 */
					obj_t BgL_list1046z00_62;

					{	/* Jas/lib.scm 31 */
						obj_t BgL_arg1047z00_63;

						{	/* Jas/lib.scm 31 */
							obj_t BgL_arg1048z00_64;

							{	/* Jas/lib.scm 31 */
								obj_t BgL_arg1049z00_65;

								BgL_arg1049z00_65 =
									MAKE_YOUNG_PAIR(BINT(BgL_arg1045z00_61), BNIL);
								BgL_arg1048z00_64 =
									MAKE_YOUNG_PAIR(BINT(BgL_arg1044z00_60), BgL_arg1049z00_65);
							}
							BgL_arg1047z00_63 =
								MAKE_YOUNG_PAIR(BINT(BgL_arg1042z00_59), BgL_arg1048z00_64);
						}
						BgL_list1046z00_62 =
							MAKE_YOUNG_PAIR(BINT(BgL_arg1041z00_58), BgL_arg1047z00_63);
					}
					return BgL_list1046z00_62;
				}
			}
		}

	}



/* &w4elong */
	obj_t BGl_z62w4elongz62zzjas_libz00(obj_t BgL_envz00_420, obj_t BgL_nz00_421)
	{
		{	/* Jas/lib.scm 30 */
			return BGl_w4elongz00zzjas_libz00(BELONG_TO_LONG(BgL_nz00_421));
		}

	}



/* w4 */
	BGL_EXPORTED_DEF obj_t BGl_w4z00zzjas_libz00(long BgL_nz00_8)
	{
		{	/* Jas/lib.scm 36 */
			{	/* Jas/lib.scm 38 */
				long BgL_arg1056z00_72;
				long BgL_arg1057z00_73;
				long BgL_arg1058z00_74;
				long BgL_arg1059z00_75;

				BgL_arg1056z00_72 =
					(
					(((BgL_nz00_8 >>
								(int) (16L)) >> (int) (16L)) >> (int) (16L)) & 65535L);
				BgL_arg1057z00_73 =
					(((BgL_nz00_8 >> (int) (16L)) >> (int) (16L)) & 65535L);
				BgL_arg1058z00_74 = ((BgL_nz00_8 >> (int) (16L)) & 65535L);
				BgL_arg1059z00_75 = (BgL_nz00_8 & 65535L);
				{	/* Jas/lib.scm 38 */
					obj_t BgL_list1060z00_76;

					{	/* Jas/lib.scm 38 */
						obj_t BgL_arg1062z00_77;

						{	/* Jas/lib.scm 38 */
							obj_t BgL_arg1063z00_78;

							{	/* Jas/lib.scm 38 */
								obj_t BgL_arg1065z00_79;

								BgL_arg1065z00_79 =
									MAKE_YOUNG_PAIR(BINT(BgL_arg1059z00_75), BNIL);
								BgL_arg1063z00_78 =
									MAKE_YOUNG_PAIR(BINT(BgL_arg1058z00_74), BgL_arg1065z00_79);
							}
							BgL_arg1062z00_77 =
								MAKE_YOUNG_PAIR(BINT(BgL_arg1057z00_73), BgL_arg1063z00_78);
						}
						BgL_list1060z00_76 =
							MAKE_YOUNG_PAIR(BINT(BgL_arg1056z00_72), BgL_arg1062z00_77);
					}
					return BgL_list1060z00_76;
				}
			}
		}

	}



/* &w4 */
	obj_t BGl_z62w4z62zzjas_libz00(obj_t BgL_envz00_422, obj_t BgL_nz00_423)
	{
		{	/* Jas/lib.scm 36 */
			return BGl_w4z00zzjas_libz00((long) CINT(BgL_nz00_423));
		}

	}



/* w4llong */
	BGL_EXPORTED_DEF obj_t BGl_w4llongz00zzjas_libz00(BGL_LONGLONG_T BgL_nz00_10)
	{
		{	/* Jas/lib.scm 46 */
			{	/* Jas/lib.scm 48 */
				long BgL_arg1082z00_88;
				long BgL_arg1083z00_89;
				long BgL_arg1084z00_90;
				long BgL_arg1085z00_91;

				{	/* Jas/lib.scm 48 */
					BGL_LONGLONG_T BgL_arg1092z00_96;

					BgL_arg1092z00_96 =
						(((BgL_nz00_10 >> (int) (16L)) >> (int) (16L)) >> (int) (16L));
					{	/* Jas/lib.scm 44 */
						BGL_LONGLONG_T BgL_arg1079z00_243;

						{	/* Jas/lib.scm 44 */
							BGL_LONGLONG_T BgL_arg1080z00_244;

							BgL_arg1080z00_244 = LONG_TO_LLONG(65535L);
							BgL_arg1079z00_243 = (BgL_arg1092z00_96 & BgL_arg1080z00_244);
						}
						BgL_arg1082z00_88 = LLONG_TO_LONG(BgL_arg1079z00_243);
				}}
				{	/* Jas/lib.scm 49 */
					BGL_LONGLONG_T BgL_arg1103z00_99;

					BgL_arg1103z00_99 = ((BgL_nz00_10 >> (int) (16L)) >> (int) (16L));
					{	/* Jas/lib.scm 44 */
						BGL_LONGLONG_T BgL_arg1079z00_251;

						{	/* Jas/lib.scm 44 */
							BGL_LONGLONG_T BgL_arg1080z00_252;

							BgL_arg1080z00_252 = LONG_TO_LLONG(65535L);
							BgL_arg1079z00_251 = (BgL_arg1103z00_99 & BgL_arg1080z00_252);
						}
						BgL_arg1083z00_89 = LLONG_TO_LONG(BgL_arg1079z00_251);
				}}
				{	/* Jas/lib.scm 50 */
					BGL_LONGLONG_T BgL_arg1114z00_101;

					BgL_arg1114z00_101 = (BgL_nz00_10 >> (int) (16L));
					{	/* Jas/lib.scm 44 */
						BGL_LONGLONG_T BgL_arg1079z00_258;

						{	/* Jas/lib.scm 44 */
							BGL_LONGLONG_T BgL_arg1080z00_259;

							BgL_arg1080z00_259 = LONG_TO_LLONG(65535L);
							BgL_arg1079z00_258 = (BgL_arg1114z00_101 & BgL_arg1080z00_259);
						}
						BgL_arg1084z00_90 = LLONG_TO_LONG(BgL_arg1079z00_258);
				}}
				{	/* Jas/lib.scm 44 */
					BGL_LONGLONG_T BgL_arg1079z00_264;

					{	/* Jas/lib.scm 44 */
						BGL_LONGLONG_T BgL_arg1080z00_265;

						BgL_arg1080z00_265 = LONG_TO_LLONG(65535L);
						BgL_arg1079z00_264 = (BgL_nz00_10 & BgL_arg1080z00_265);
					}
					BgL_arg1085z00_91 = LLONG_TO_LONG(BgL_arg1079z00_264);
				}
				{	/* Jas/lib.scm 48 */
					obj_t BgL_list1086z00_92;

					{	/* Jas/lib.scm 48 */
						obj_t BgL_arg1087z00_93;

						{	/* Jas/lib.scm 48 */
							obj_t BgL_arg1088z00_94;

							{	/* Jas/lib.scm 48 */
								obj_t BgL_arg1090z00_95;

								BgL_arg1090z00_95 =
									MAKE_YOUNG_PAIR(BINT(BgL_arg1085z00_91), BNIL);
								BgL_arg1088z00_94 =
									MAKE_YOUNG_PAIR(BINT(BgL_arg1084z00_90), BgL_arg1090z00_95);
							}
							BgL_arg1087z00_93 =
								MAKE_YOUNG_PAIR(BINT(BgL_arg1083z00_89), BgL_arg1088z00_94);
						}
						BgL_list1086z00_92 =
							MAKE_YOUNG_PAIR(BINT(BgL_arg1082z00_88), BgL_arg1087z00_93);
					}
					return BgL_list1086z00_92;
				}
			}
		}

	}



/* &w4llong */
	obj_t BGl_z62w4llongz62zzjas_libz00(obj_t BgL_envz00_424, obj_t BgL_nz00_425)
	{
		{	/* Jas/lib.scm 46 */
			return BGl_w4llongz00zzjas_libz00(BLLONG_TO_LLONG(BgL_nz00_425));
		}

	}



/* f2 */
	BGL_EXPORTED_DEF obj_t BGl_f2z00zzjas_libz00(float BgL_nz00_11)
	{
		{	/* Jas/lib.scm 53 */
			return
				BGl_stringzd2ze3shortlistz31zzjas_libz00(bgl_float_to_ieee_string
				(BgL_nz00_11));
		}

	}



/* &f2 */
	obj_t BGl_z62f2z62zzjas_libz00(obj_t BgL_envz00_426, obj_t BgL_nz00_427)
	{
		{	/* Jas/lib.scm 53 */
			return BGl_f2z00zzjas_libz00(REAL_TO_FLOAT(BgL_nz00_427));
		}

	}



/* f4 */
	BGL_EXPORTED_DEF obj_t BGl_f4z00zzjas_libz00(double BgL_nz00_12)
	{
		{	/* Jas/lib.scm 56 */
			return
				BGl_stringzd2ze3shortlistz31zzjas_libz00(bgl_double_to_ieee_string
				(BgL_nz00_12));
		}

	}



/* &f4 */
	obj_t BGl_z62f4z62zzjas_libz00(obj_t BgL_envz00_428, obj_t BgL_nz00_429)
	{
		{	/* Jas/lib.scm 56 */
			return BGl_f4z00zzjas_libz00(REAL_TO_DOUBLE(BgL_nz00_429));
		}

	}



/* string->shortlist */
	obj_t BGl_stringzd2ze3shortlistz31zzjas_libz00(obj_t BgL_sz00_13)
	{
		{	/* Jas/lib.scm 59 */
			return
				BGl_collectze70ze7zzjas_libz00(STRING_LENGTH(BgL_sz00_13), BgL_sz00_13,
				0L);
		}

	}



/* collect~0 */
	obj_t BGl_collectze70ze7zzjas_libz00(long BgL_nz00_433, obj_t BgL_sz00_432,
		long BgL_iz00_106)
	{
		{	/* Jas/lib.scm 66 */
			if ((BgL_iz00_106 == BgL_nz00_433))
				{	/* Jas/lib.scm 62 */
					return BNIL;
				}
			else
				{	/* Jas/lib.scm 64 */
					long BgL_arg1125z00_109;
					obj_t BgL_arg1126z00_110;

					BgL_arg1125z00_109 =
						(
						((STRING_REF(BgL_sz00_432, BgL_iz00_106)) <<
							(int) (8L)) | (STRING_REF(BgL_sz00_432, (BgL_iz00_106 + 1L))));
					BgL_arg1126z00_110 =
						BGl_collectze70ze7zzjas_libz00(BgL_nz00_433, BgL_sz00_432,
						(BgL_iz00_106 + 2L));
					return MAKE_YOUNG_PAIR(BINT(BgL_arg1125z00_109), BgL_arg1126z00_110);
				}
		}

	}



/* string->utf8 */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2ze3utf8z31zzjas_libz00(obj_t BgL_sz00_14)
	{
		{	/* Jas/lib.scm 69 */
			{	/* Jas/lib.scm 70 */
				obj_t BgL_arg1141z00_119;

				{	/* Jas/lib.scm 70 */
					long BgL_arg1142z00_120;

					BgL_arg1142z00_120 = BGl_utf8zd2lengthzd2zzjas_libz00(BgL_sz00_14);
					{	/* Jas/lib.scm 70 */

						BgL_arg1141z00_119 =
							make_string(BgL_arg1142z00_120, ((unsigned char) ' '));
				}}
				return
					BGl_producezd2utf8zd2zzjas_libz00(BgL_sz00_14, BgL_arg1141z00_119);
			}
		}

	}



/* &string->utf8 */
	obj_t BGl_z62stringzd2ze3utf8z53zzjas_libz00(obj_t BgL_envz00_430,
		obj_t BgL_sz00_431)
	{
		{	/* Jas/lib.scm 69 */
			return BGl_stringzd2ze3utf8z31zzjas_libz00(BgL_sz00_431);
		}

	}



/* utf8-length */
	long BGl_utf8zd2lengthzd2zzjas_libz00(obj_t BgL_sz00_20)
	{
		{	/* Jas/lib.scm 78 */
			{	/* Jas/lib.scm 79 */
				long BgL_nz00_125;

				BgL_nz00_125 = STRING_LENGTH(((obj_t) BgL_sz00_20));
				{
					long BgL_iz00_309;
					long BgL_rz00_310;

					BgL_iz00_309 = 0L;
					BgL_rz00_310 = 0L;
				BgL_walkz00_308:
					if ((BgL_iz00_309 == BgL_nz00_125))
						{	/* Jas/lib.scm 81 */
							return BgL_rz00_310;
						}
					else
						{
							long BgL_rz00_626;
							long BgL_iz00_624;

							BgL_iz00_624 = (BgL_iz00_309 + 1L);
							BgL_rz00_626 =
								(BgL_rz00_310 +
								BGl_utf8zd2length1zd2zzjas_libz00(
									(STRING_REF(((obj_t) BgL_sz00_20), BgL_iz00_309))));
							BgL_rz00_310 = BgL_rz00_626;
							BgL_iz00_309 = BgL_iz00_624;
							goto BgL_walkz00_308;
						}
				}
			}
		}

	}



/* utf8-length1 */
	long BGl_utf8zd2length1zd2zzjas_libz00(long BgL_cnz00_21)
	{
		{	/* Jas/lib.scm 86 */
			if ((BgL_cnz00_21 == 0L))
				{	/* Jas/lib.scm 88 */
					return 2L;
				}
			else
				{	/* Jas/lib.scm 88 */
					if ((BgL_cnz00_21 < 128L))
						{	/* Jas/lib.scm 89 */
							return 1L;
						}
					else
						{	/* Jas/lib.scm 89 */
							if ((BgL_cnz00_21 < 2048L))
								{	/* Jas/lib.scm 90 */
									return 2L;
								}
							else
								{	/* Jas/lib.scm 90 */
									return 3L;
								}
						}
				}
		}

	}



/* produce-utf8 */
	obj_t BGl_producezd2utf8zd2zzjas_libz00(obj_t BgL_sz00_22, obj_t BgL_s8z00_23)
	{
		{	/* Jas/lib.scm 93 */
			{	/* Jas/lib.scm 94 */
				long BgL_nz00_140;

				BgL_nz00_140 = STRING_LENGTH(((obj_t) BgL_sz00_22));
				{
					long BgL_iz00_142;
					long BgL_jz00_143;

					BgL_iz00_142 = 0L;
					BgL_jz00_143 = 0L;
				BgL_zc3z04anonymousza31157ze3z87_144:
					if ((BgL_iz00_142 == BgL_nz00_140))
						{	/* Jas/lib.scm 96 */
							return BgL_s8z00_23;
						}
					else
						{	/* Jas/lib.scm 98 */
							long BgL_arg1162z00_146;
							long BgL_arg1164z00_147;

							BgL_arg1162z00_146 = (BgL_iz00_142 + 1L);
							{	/* Jas/lib.scm 98 */
								long BgL_arg1166z00_148;

								{	/* Jas/lib.scm 98 */
									long BgL_arg1171z00_149;

									BgL_arg1171z00_149 =
										(STRING_REF(((obj_t) BgL_sz00_22), BgL_iz00_142));
									BgL_arg1166z00_148 =
										BGl_producezd2utf8zd21z00zzjas_libz00(BgL_s8z00_23,
										BgL_jz00_143, BgL_arg1171z00_149);
								}
								BgL_arg1164z00_147 = (BgL_jz00_143 + BgL_arg1166z00_148);
							}
							{
								long BgL_jz00_649;
								long BgL_iz00_648;

								BgL_iz00_648 = BgL_arg1162z00_146;
								BgL_jz00_649 = BgL_arg1164z00_147;
								BgL_jz00_143 = BgL_jz00_649;
								BgL_iz00_142 = BgL_iz00_648;
								goto BgL_zc3z04anonymousza31157ze3z87_144;
							}
						}
				}
			}
		}

	}



/* produce-utf8-1 */
	long BGl_producezd2utf8zd21z00zzjas_libz00(obj_t BgL_s8z00_24,
		long BgL_jz00_25, long BgL_cnz00_26)
	{
		{	/* Jas/lib.scm 101 */
			if ((BgL_cnz00_26 == 0L))
				{	/* Jas/lib.scm 105 */
					{	/* Jas/lib.scm 76 */
						unsigned char BgL_auxz00_654;
						long BgL_tmpz00_652;

						BgL_auxz00_654 = (192L);
						BgL_tmpz00_652 = (BgL_jz00_25 + 0L);
						STRING_SET(BgL_s8z00_24, BgL_tmpz00_652, BgL_auxz00_654);
					}
					{	/* Jas/lib.scm 76 */
						unsigned char BgL_auxz00_659;
						long BgL_tmpz00_657;

						BgL_auxz00_659 = (128L);
						BgL_tmpz00_657 = (BgL_jz00_25 + 1L);
						STRING_SET(BgL_s8z00_24, BgL_tmpz00_657, BgL_auxz00_659);
					}
					return 2L;
				}
			else
				{	/* Jas/lib.scm 105 */
					if ((BgL_cnz00_26 < 128L))
						{	/* Jas/lib.scm 109 */
							{	/* Jas/lib.scm 76 */
								unsigned char BgL_auxz00_666;
								long BgL_tmpz00_664;

								BgL_auxz00_666 = (BgL_cnz00_26);
								BgL_tmpz00_664 = (BgL_jz00_25 + 0L);
								STRING_SET(BgL_s8z00_24, BgL_tmpz00_664, BgL_auxz00_666);
							}
							return 1L;
						}
					else
						{	/* Jas/lib.scm 109 */
							if ((BgL_cnz00_26 < 2048L))
								{	/* Jas/lib.scm 112 */
									{	/* Jas/lib.scm 113 */
										long BgL_arg1182z00_155;

										BgL_arg1182z00_155 = (192L | (BgL_cnz00_26 >> (int) (6L)));
										{	/* Jas/lib.scm 76 */
											unsigned char BgL_auxz00_676;
											long BgL_tmpz00_674;

											BgL_auxz00_676 = (BgL_arg1182z00_155);
											BgL_tmpz00_674 = (BgL_jz00_25 + 0L);
											STRING_SET(BgL_s8z00_24, BgL_tmpz00_674, BgL_auxz00_676);
									}}
									{	/* Jas/lib.scm 114 */
										long BgL_arg1187z00_157;

										BgL_arg1187z00_157 = (128L | (BgL_cnz00_26 & 63L));
										{	/* Jas/lib.scm 76 */
											unsigned char BgL_auxz00_683;
											long BgL_tmpz00_681;

											BgL_auxz00_683 = (BgL_arg1187z00_157);
											BgL_tmpz00_681 = (BgL_jz00_25 + 1L);
											STRING_SET(BgL_s8z00_24, BgL_tmpz00_681, BgL_auxz00_683);
									}}
									return 2L;
								}
							else
								{	/* Jas/lib.scm 112 */
									{	/* Jas/lib.scm 117 */
										long BgL_arg1189z00_159;

										BgL_arg1189z00_159 = (224L | (BgL_cnz00_26 >> (int) (12L)));
										{	/* Jas/lib.scm 76 */
											unsigned char BgL_auxz00_691;
											long BgL_tmpz00_689;

											BgL_auxz00_691 = (BgL_arg1189z00_159);
											BgL_tmpz00_689 = (BgL_jz00_25 + 0L);
											STRING_SET(BgL_s8z00_24, BgL_tmpz00_689, BgL_auxz00_691);
									}}
									{	/* Jas/lib.scm 118 */
										long BgL_arg1191z00_161;

										BgL_arg1191z00_161 =
											(128L | ((BgL_cnz00_26 >> (int) (6L)) & 63L));
										{	/* Jas/lib.scm 76 */
											unsigned char BgL_auxz00_700;
											long BgL_tmpz00_698;

											BgL_auxz00_700 = (BgL_arg1191z00_161);
											BgL_tmpz00_698 = (BgL_jz00_25 + 1L);
											STRING_SET(BgL_s8z00_24, BgL_tmpz00_698, BgL_auxz00_700);
									}}
									{	/* Jas/lib.scm 119 */
										long BgL_arg1196z00_164;

										BgL_arg1196z00_164 = (128L | (BgL_cnz00_26 & 63L));
										{	/* Jas/lib.scm 76 */
											unsigned char BgL_auxz00_707;
											long BgL_tmpz00_705;

											BgL_auxz00_707 = (BgL_arg1196z00_164);
											BgL_tmpz00_705 = (BgL_jz00_25 + 2L);
											STRING_SET(BgL_s8z00_24, BgL_tmpz00_705, BgL_auxz00_707);
									}}
									return 3L;
								}
						}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzjas_libz00(void)
	{
		{	/* Jas/lib.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzjas_libz00(void)
	{
		{	/* Jas/lib.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzjas_libz00(void)
	{
		{	/* Jas/lib.scm 1 */
			return BUNSPEC;
		}

	}

#ifdef __cplusplus
}
#endif
