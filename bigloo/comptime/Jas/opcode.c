/*===========================================================================*/
/*   (Jas/opcode.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Jas/opcode.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_JAS_OPCODE_TYPE_DEFINITIONS
#define BGL_JAS_OPCODE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_jastypez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_codez00;
		obj_t BgL_vectz00;
	}                 *BgL_jastypez00_bglt;

	typedef struct BgL_classez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_codez00;
		obj_t BgL_vectz00;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_poolz00;
	}                *BgL_classez00_bglt;

	typedef struct BgL_fieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}               *BgL_fieldz00_bglt;

	typedef struct BgL_methodz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}                *BgL_methodz00_bglt;

	typedef struct BgL_classfilez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_currentzd2methodzd2;
		obj_t BgL_globalsz00;
		obj_t BgL_poolz00;
		obj_t BgL_poolzd2siza7ez75;
		obj_t BgL_pooledzd2nameszd2;
		obj_t BgL_flagsz00;
		obj_t BgL_mez00;
		obj_t BgL_superz00;
		obj_t BgL_interfacesz00;
		obj_t BgL_fieldsz00;
		obj_t BgL_methodsz00;
		obj_t BgL_attributesz00;
	}                   *BgL_classfilez00_bglt;


#endif													// BGL_JAS_OPCODE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	extern BgL_fieldz00_bglt
		BGl_declaredzd2fieldzd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t);
	static obj_t BGl_initzd2copzd2valuez00zzjas_opcodez00(void);
	static obj_t BGl_vzd2indexzd2zzjas_opcodez00(obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzjas_opcodez00 = BUNSPEC;
	static obj_t BGl_getzd2typezd2numz00zzjas_opcodez00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzjas_opcodez00(void);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzjas_opcodez00(void);
	static obj_t BGl_resolvezd2opcodeze70z35zzjas_opcodez00(obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzjas_opcodez00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzjas_opcodez00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzjas_opcodez00(void);
	extern int
		BGl_poolzd2classzd2byzd2namezd2zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t);
	extern BgL_methodz00_bglt
		BGl_declaredzd2methodzd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t);
	BGL_IMPORT obj_t bgl_list_ref(obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzjas_opcodez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_classfilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_libz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	static bool_t BGl_slotzf3zf3zzjas_opcodez00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzjas_opcodez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzjas_opcodez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzjas_opcodez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzjas_opcodez00(void);
	static obj_t BGl_za2copzd2listza2zd2zzjas_opcodez00 = BUNSPEC;
	extern BgL_jastypez00_bglt
		BGl_aszd2typezd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t);
	extern obj_t BGl_jaszd2errorzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t, obj_t);
	static obj_t BGl_resolvezd2argszd2zzjas_opcodez00(obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04resolvezd2opcodesza31209ze3z37zzjas_opcodez00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_resolvezd2opcodeszd2zzjas_opcodez00 = BUNSPEC;
	extern BgL_classez00_bglt
		BGl_declaredzd2classzd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_STRING(BGl_string1926z00zzjas_opcodez00,
		BgL_bgl_string1926za700za7za7j1934za7, "Bad mnemonic", 12);
	      DEFINE_STRING(BGl_string1927z00zzjas_opcodez00,
		BgL_bgl_string1927za700za7za7j1935za7, "unknown code operation", 22);
	      DEFINE_STRING(BGl_string1928z00zzjas_opcodez00,
		BgL_bgl_string1928za700za7za7j1936za7, "Syntax error", 12);
	      DEFINE_STRING(BGl_string1929z00zzjas_opcodez00,
		BgL_bgl_string1929za700za7za7j1937za7, "Bad wide mnemonic", 17);
	      DEFINE_STRING(BGl_string1930z00zzjas_opcodez00,
		BgL_bgl_string1930za700za7za7j1938za7, "unknown variable", 16);
	      DEFINE_STRING(BGl_string1931z00zzjas_opcodez00,
		BgL_bgl_string1931za700za7za7j1939za7, "jas_opcode", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1925z00zzjas_opcodez00,
		BgL_bgl_za762za7c3za704resolve1940za7,
		BGl_z62zc3z04resolvezd2opcodesza31209ze3z37zzjas_opcodez00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string1932z00zzjas_opcodez00,
		BgL_bgl_string1932za700za7za7j1941za7,
		"((boolean . 4) (char . 5) (float . 6) (double . 7) (byte . 8) (short . 9) (int . 10) (long . 11)) as-cop-value (nop aconst_null iconst_m1 iconst_0 iconst_1 iconst_2 iconst_3 iconst_4 iconst_5 lconst_0 lconst_1 fconst_0 fconst_1 fconst_2 dconst_0 dconst_1 bipush sipush ldc ldc_w ldc2_w iload lload fload dload aload iload_0 iload_1 iload_2 iload_3 lload_0 lload_1 lload_2 lload_3 fload_0 fload_1 fload_2 fload_3 dload_0 dload_1 dload_2 dload_3 aload_0 aload_1 aload_2 aload_3 iaload laload faload daload aaload baload caload saload istore lstore fstore dstore astore istore_0 istore_1 istore_2 istore_3 lstore_0 lstore_1 lstore_2 lstore_3 fstore_0 fstore_1 fstore_2 fstore_3 dstore_0 dstore_1 dstore_2 dstore_3 astore_0 astore_1 astore_2 astore_3 iastore lastore fastore dastore aastore bastore castore sastore pop pop2 dup dup_x1 dup_x2 dup2 dup2_x1 dup2_x2 swap iadd ladd fadd dadd isub lsub fsub dsub imul lmul fmul dmul idiv ldiv fdiv ddiv irem lrem frem drem ineg lneg fneg dneg ishl lshl ishr lshr iushr lushr iand lan"
		"d ior lor ixor lxor iinc i2l i2f i2d l2i l2f l2d f2i f2l f2d d2i d2l d2f i2b i2c i2s lcmp fcmpl fcmpg dcmpl dcmpg ifeq ifne iflt ifge ifgt ifle if_icmpeq if_icmpne if_icmplt if_icmpge if_icmpgt if_icmple if_acmpeq if_acmpne goto jsr ret tableswitch lookupswitch ireturn lreturn freturn dreturn areturn return getstatic putstatic getfield putfield invokevirtual invokespecial invokestatic invokeinterface xxxunusedxxx new newarray anewarray arraylength athrow checkcast instanceof monitorenter monitorexit wide multianewarray ifnull ifnonnull goto_w jsr_w handler line comment localvar) ",
		1610);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzjas_opcodez00));
		     ADD_ROOT((void *) (&BGl_za2copzd2listza2zd2zzjas_opcodez00));
		     ADD_ROOT((void *) (&BGl_resolvezd2opcodeszd2zzjas_opcodez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzjas_opcodez00(long
		BgL_checksumz00_1200, char *BgL_fromz00_1201)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzjas_opcodez00))
				{
					BGl_requirezd2initializa7ationz75zzjas_opcodez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzjas_opcodez00();
					BGl_libraryzd2moduleszd2initz00zzjas_opcodez00();
					BGl_cnstzd2initzd2zzjas_opcodez00();
					BGl_importedzd2moduleszd2initz00zzjas_opcodez00();
					return BGl_toplevelzd2initzd2zzjas_opcodez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzjas_opcodez00(void)
	{
		{	/* Jas/opcode.scm 1 */
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "jas_opcode");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"jas_opcode");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "jas_opcode");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "jas_opcode");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "jas_opcode");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"jas_opcode");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"jas_opcode");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzjas_opcodez00(void)
	{
		{	/* Jas/opcode.scm 1 */
			{	/* Jas/opcode.scm 1 */
				obj_t BgL_cportz00_1177;

				{	/* Jas/opcode.scm 1 */
					obj_t BgL_stringz00_1184;

					BgL_stringz00_1184 = BGl_string1932z00zzjas_opcodez00;
					{	/* Jas/opcode.scm 1 */
						obj_t BgL_startz00_1185;

						BgL_startz00_1185 = BINT(0L);
						{	/* Jas/opcode.scm 1 */
							obj_t BgL_endz00_1186;

							BgL_endz00_1186 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1184)));
							{	/* Jas/opcode.scm 1 */

								BgL_cportz00_1177 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1184, BgL_startz00_1185, BgL_endz00_1186);
				}}}}
				{
					long BgL_iz00_1178;

					BgL_iz00_1178 = 2L;
				BgL_loopz00_1179:
					if ((BgL_iz00_1178 == -1L))
						{	/* Jas/opcode.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* Jas/opcode.scm 1 */
							{	/* Jas/opcode.scm 1 */
								obj_t BgL_arg1933z00_1180;

								{	/* Jas/opcode.scm 1 */

									{	/* Jas/opcode.scm 1 */
										obj_t BgL_locationz00_1182;

										BgL_locationz00_1182 = BBOOL(((bool_t) 0));
										{	/* Jas/opcode.scm 1 */

											BgL_arg1933z00_1180 =
												BGl_readz00zz__readerz00(BgL_cportz00_1177,
												BgL_locationz00_1182);
										}
									}
								}
								{	/* Jas/opcode.scm 1 */
									int BgL_tmpz00_1226;

									BgL_tmpz00_1226 = (int) (BgL_iz00_1178);
									CNST_TABLE_SET(BgL_tmpz00_1226, BgL_arg1933z00_1180);
							}}
							{	/* Jas/opcode.scm 1 */
								int BgL_auxz00_1183;

								BgL_auxz00_1183 = (int) ((BgL_iz00_1178 - 1L));
								{
									long BgL_iz00_1231;

									BgL_iz00_1231 = (long) (BgL_auxz00_1183);
									BgL_iz00_1178 = BgL_iz00_1231;
									goto BgL_loopz00_1179;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzjas_opcodez00(void)
	{
		{	/* Jas/opcode.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzjas_opcodez00(void)
	{
		{	/* Jas/opcode.scm 1 */
			BGl_za2copzd2listza2zd2zzjas_opcodez00 = CNST_TABLE_REF(0);
			BGl_initzd2copzd2valuez00zzjas_opcodez00();
			return (BGl_resolvezd2opcodeszd2zzjas_opcodez00 =
				BGl_proc1925z00zzjas_opcodez00, BUNSPEC);
		}

	}



/* &<@resolve-opcodes:1209> */
	obj_t BGl_z62zc3z04resolvezd2opcodesza31209ze3z37zzjas_opcodez00(obj_t
		BgL_envz00_1170, obj_t BgL_classfilez00_1171, obj_t BgL_paramz00_1172,
		obj_t BgL_localsz00_1173, obj_t BgL_codez00_1174)
	{
		{	/* Jas/opcode.scm 76 */
			{	/* Jas/opcode.scm 77 */
				obj_t BgL_varsz00_1188;

				BgL_varsz00_1188 =
					BGl_appendzd221011zd2zzjas_opcodez00(BgL_paramz00_1172,
					BgL_localsz00_1173);
				if (NULLP(BgL_codez00_1174))
					{	/* Jas/opcode.scm 85 */
						return BNIL;
					}
				else
					{	/* Jas/opcode.scm 85 */
						obj_t BgL_head1205z00_1189;

						{	/* Jas/opcode.scm 85 */
							obj_t BgL_arg1219z00_1190;

							{	/* Jas/opcode.scm 85 */
								obj_t BgL_arg1220z00_1191;

								BgL_arg1220z00_1191 = CAR(((obj_t) BgL_codez00_1174));
								BgL_arg1219z00_1190 =
									BGl_resolvezd2opcodeze70z35zzjas_opcodez00(BgL_varsz00_1188,
									BgL_classfilez00_1171, BgL_arg1220z00_1191);
							}
							BgL_head1205z00_1189 = MAKE_YOUNG_PAIR(BgL_arg1219z00_1190, BNIL);
						}
						{	/* Jas/opcode.scm 85 */
							obj_t BgL_g1208z00_1192;

							BgL_g1208z00_1192 = CDR(((obj_t) BgL_codez00_1174));
							{
								obj_t BgL_l1203z00_1194;
								obj_t BgL_tail1206z00_1195;

								BgL_l1203z00_1194 = BgL_g1208z00_1192;
								BgL_tail1206z00_1195 = BgL_head1205z00_1189;
							BgL_zc3z04anonymousza31211ze3z87_1193:
								if (NULLP(BgL_l1203z00_1194))
									{	/* Jas/opcode.scm 85 */
										return BgL_head1205z00_1189;
									}
								else
									{	/* Jas/opcode.scm 85 */
										obj_t BgL_newtail1207z00_1196;

										{	/* Jas/opcode.scm 85 */
											obj_t BgL_arg1216z00_1197;

											{	/* Jas/opcode.scm 85 */
												obj_t BgL_arg1218z00_1198;

												BgL_arg1218z00_1198 = CAR(((obj_t) BgL_l1203z00_1194));
												BgL_arg1216z00_1197 =
													BGl_resolvezd2opcodeze70z35zzjas_opcodez00
													(BgL_varsz00_1188, BgL_classfilez00_1171,
													BgL_arg1218z00_1198);
											}
											BgL_newtail1207z00_1196 =
												MAKE_YOUNG_PAIR(BgL_arg1216z00_1197, BNIL);
										}
										SET_CDR(BgL_tail1206z00_1195, BgL_newtail1207z00_1196);
										{	/* Jas/opcode.scm 85 */
											obj_t BgL_arg1215z00_1199;

											BgL_arg1215z00_1199 = CDR(((obj_t) BgL_l1203z00_1194));
											{
												obj_t BgL_tail1206z00_1255;
												obj_t BgL_l1203z00_1254;

												BgL_l1203z00_1254 = BgL_arg1215z00_1199;
												BgL_tail1206z00_1255 = BgL_newtail1207z00_1196;
												BgL_tail1206z00_1195 = BgL_tail1206z00_1255;
												BgL_l1203z00_1194 = BgL_l1203z00_1254;
												goto BgL_zc3z04anonymousza31211ze3z87_1193;
											}
										}
									}
							}
						}
					}
			}
		}

	}



/* resolve-opcode~0 */
	obj_t BGl_resolvezd2opcodeze70z35zzjas_opcodez00(obj_t BgL_varsz00_1176,
		obj_t BgL_classfilez00_1175, obj_t BgL_insz00_288)
	{
		{	/* Jas/opcode.scm 84 */
			if (PAIRP(BgL_insz00_288))
				{	/* Jas/opcode.scm 81 */
					obj_t BgL_copz00_291;

					{	/* Jas/opcode.scm 81 */
						obj_t BgL_arg1226z00_294;

						BgL_arg1226z00_294 = CAR(BgL_insz00_288);
						if (SYMBOLP(BgL_arg1226z00_294))
							{	/* Jas/opcode.scm 68 */
								BgL_copz00_291 =
									BGl_getpropz00zz__r4_symbols_6_4z00(BgL_arg1226z00_294,
									CNST_TABLE_REF(1));
							}
						else
							{	/* Jas/opcode.scm 68 */
								BgL_copz00_291 =
									BGl_jaszd2errorzd2zzjas_classfilez00(
									((BgL_classfilez00_bglt) BgL_classfilez00_1175),
									BGl_string1926z00zzjas_opcodez00, BgL_arg1226z00_294);
							}
					}
					if (CBOOL(BgL_copz00_291))
						{	/* Jas/opcode.scm 82 */
							return
								BGl_resolvezd2argszd2zzjas_opcodez00(BgL_classfilez00_1175,
								BgL_varsz00_1176, BgL_copz00_291, CDR(BgL_insz00_288));
						}
					else
						{	/* Jas/opcode.scm 84 */
							obj_t BgL_arg1225z00_293;

							BgL_arg1225z00_293 = CAR(BgL_insz00_288);
							return
								BGl_jaszd2errorzd2zzjas_classfilez00(
								((BgL_classfilez00_bglt) BgL_classfilez00_1175),
								BGl_string1926z00zzjas_opcodez00, BgL_arg1225z00_293);
						}
				}
			else
				{	/* Jas/opcode.scm 79 */
					return BgL_insz00_288;
				}
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzjas_opcodez00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_296;

				BgL_headz00_296 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_297;
					obj_t BgL_tailz00_298;

					BgL_prevz00_297 = BgL_headz00_296;
					BgL_tailz00_298 = BgL_l1z00_1;
				BgL_loopz00_299:
					if (PAIRP(BgL_tailz00_298))
						{
							obj_t BgL_newzd2prevzd2_301;

							BgL_newzd2prevzd2_301 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_298), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_297, BgL_newzd2prevzd2_301);
							{
								obj_t BgL_tailz00_1279;
								obj_t BgL_prevz00_1278;

								BgL_prevz00_1278 = BgL_newzd2prevzd2_301;
								BgL_tailz00_1279 = CDR(BgL_tailz00_298);
								BgL_tailz00_298 = BgL_tailz00_1279;
								BgL_prevz00_297 = BgL_prevz00_1278;
								goto BgL_loopz00_299;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_296);
				}
			}
		}

	}



/* init-cop-value */
	obj_t BGl_initzd2copzd2valuez00zzjas_opcodez00(void)
	{
		{	/* Jas/opcode.scm 55 */
			{
				obj_t BgL_lz00_945;
				long BgL_nz00_946;

				BgL_lz00_945 = CNST_TABLE_REF(0);
				BgL_nz00_946 = 0L;
			BgL_walkz00_944:
				if (NULLP(BgL_lz00_945))
					{	/* Jas/opcode.scm 57 */
						return BgL_lz00_945;
					}
				else
					{	/* Jas/opcode.scm 57 */
						{	/* Jas/opcode.scm 58 */
							obj_t BgL_arg1232z00_948;

							BgL_arg1232z00_948 = CAR(((obj_t) BgL_lz00_945));
							BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_arg1232z00_948,
								CNST_TABLE_REF(1), BINT(BgL_nz00_946));
						}
						{	/* Jas/opcode.scm 59 */
							obj_t BgL_arg1233z00_950;
							long BgL_arg1234z00_951;

							BgL_arg1233z00_950 = CDR(((obj_t) BgL_lz00_945));
							BgL_arg1234z00_951 = (BgL_nz00_946 + 1L);
							{
								long BgL_nz00_1293;
								obj_t BgL_lz00_1292;

								BgL_lz00_1292 = BgL_arg1233z00_950;
								BgL_nz00_1293 = BgL_arg1234z00_951;
								BgL_nz00_946 = BgL_nz00_1293;
								BgL_lz00_945 = BgL_lz00_1292;
								goto BgL_walkz00_944;
							}
						}
					}
			}
		}

	}



/* resolve-args */
	obj_t BGl_resolvezd2argszd2zzjas_opcodez00(obj_t BgL_classfilez00_6,
		obj_t BgL_localsz00_7, obj_t BgL_copz00_8, obj_t BgL_argsz00_9)
	{
		{	/* Jas/opcode.scm 87 */
		BGl_resolvezd2argszd2zzjas_opcodez00:
			{

				if (INTEGERP(BgL_copz00_8))
					{	/* Jas/opcode.scm 88 */
						switch ((long) CINT(BgL_copz00_8))
							{
							case 0L:
							case 1L:
							case 2L:
							case 3L:
							case 4L:
							case 5L:
							case 6L:
							case 7L:
							case 8L:
							case 9L:
							case 10L:
							case 11L:
							case 12L:
							case 13L:
							case 14L:
							case 15L:
							case 46L:
							case 47L:
							case 48L:
							case 49L:
							case 50L:
							case 51L:
							case 52L:
							case 53L:
							case 79L:
							case 80L:
							case 81L:
							case 82L:
							case 83L:
							case 84L:
							case 85L:
							case 86L:
							case 87L:
							case 88L:
							case 89L:
							case 90L:
							case 91L:
							case 92L:
							case 93L:
							case 94L:
							case 95L:
							case 96L:
							case 97L:
							case 98L:
							case 99L:
							case 100L:
							case 101L:
							case 102L:
							case 103L:
							case 104L:
							case 105L:
							case 106L:
							case 107L:
							case 108L:
							case 109L:
							case 110L:
							case 111L:
							case 112L:
							case 113L:
							case 114L:
							case 115L:
							case 116L:
							case 117L:
							case 118L:
							case 119L:
							case 120L:
							case 121L:
							case 122L:
							case 123L:
							case 124L:
							case 125L:
							case 126L:
							case 127L:
							case 128L:
							case 129L:
							case 130L:
							case 131L:
							case 133L:
							case 134L:
							case 135L:
							case 136L:
							case 137L:
							case 138L:
							case 139L:
							case 140L:
							case 141L:
							case 142L:
							case 143L:
							case 144L:
							case 145L:
							case 146L:
							case 147L:
							case 148L:
							case 149L:
							case 150L:
							case 151L:
							case 152L:
							case 172L:
							case 173L:
							case 174L:
							case 175L:
							case 176L:
							case 177L:
							case 190L:
							case 191L:
							case 194L:
							case 195L:

								{

									if (NULLP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 89 */
											return MAKE_YOUNG_PAIR(BgL_copz00_8, BNIL);
										}
									else
										{	/* Jas/opcode.scm 89 */
											{	/* Jas/opcode.scm 218 */
												obj_t BgL_arg1877z00_955;

												BgL_arg1877z00_955 =
													MAKE_YOUNG_PAIR(bgl_list_ref(CNST_TABLE_REF(0),
														(long) CINT(BgL_copz00_8)), BgL_argsz00_9);
												return
													BGl_jaszd2errorzd2zzjas_classfilez00(
													((BgL_classfilez00_bglt) BgL_classfilez00_6),
													BGl_string1928z00zzjas_opcodez00, BgL_arg1877z00_955);
											}
										}
								}
								break;
							case 16L:
							case 17L:

								{

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 102 */
											if (INTEGERP(CAR(((obj_t) BgL_argsz00_9))))
												{	/* Jas/opcode.scm 102 */
													if (NULLP(CDR(((obj_t) BgL_argsz00_9))))
														{	/* Jas/opcode.scm 102 */
															return
																MAKE_YOUNG_PAIR(BgL_copz00_8, BgL_argsz00_9);
														}
													else
														{	/* Jas/opcode.scm 102 */
														BgL_tagzd2112zd2_335:
															{	/* Jas/opcode.scm 218 */
																obj_t BgL_arg1877z00_988;

																BgL_arg1877z00_988 =
																	MAKE_YOUNG_PAIR(bgl_list_ref(CNST_TABLE_REF
																		(0), (long) CINT(BgL_copz00_8)),
																	BgL_argsz00_9);
																return
																	BGl_jaszd2errorzd2zzjas_classfilez00((
																		(BgL_classfilez00_bglt) BgL_classfilez00_6),
																	BGl_string1928z00zzjas_opcodez00,
																	BgL_arg1877z00_988);
															}
														}
												}
											else
												{	/* Jas/opcode.scm 102 */
													bool_t BgL_test1956z00_1323;

													{	/* Jas/opcode.scm 102 */
														obj_t BgL_tmpz00_1324;

														BgL_tmpz00_1324 = CAR(((obj_t) BgL_argsz00_9));
														BgL_test1956z00_1323 = ELONGP(BgL_tmpz00_1324);
													}
													if (BgL_test1956z00_1323)
														{	/* Jas/opcode.scm 102 */
															if (NULLP(CDR(((obj_t) BgL_argsz00_9))))
																{	/* Jas/opcode.scm 102 */
																	{	/* Jas/opcode.scm 105 */
																		long BgL_arg1351z00_386;

																		{	/* Jas/opcode.scm 105 */
																			obj_t BgL_arg1364z00_389;

																			BgL_arg1364z00_389 =
																				CAR(((obj_t) BgL_argsz00_9));
																			{	/* Jas/opcode.scm 105 */
																				long BgL_xz00_958;

																				BgL_xz00_958 =
																					BELONG_TO_LONG(BgL_arg1364z00_389);
																				BgL_arg1351z00_386 =
																					(long) (BgL_xz00_958);
																		}}
																		{	/* Jas/opcode.scm 105 */
																			obj_t BgL_list1352z00_387;

																			{	/* Jas/opcode.scm 105 */
																				obj_t BgL_arg1361z00_388;

																				BgL_arg1361z00_388 =
																					MAKE_YOUNG_PAIR(BINT
																					(BgL_arg1351z00_386), BNIL);
																				BgL_list1352z00_387 =
																					MAKE_YOUNG_PAIR(BgL_copz00_8,
																					BgL_arg1361z00_388);
																			}
																			return BgL_list1352z00_387;
																		}
																	}
																}
															else
																{	/* Jas/opcode.scm 102 */
																	goto BgL_tagzd2112zd2_335;
																}
														}
													else
														{	/* Jas/opcode.scm 102 */
															bool_t BgL_test1958z00_1339;

															{	/* Jas/opcode.scm 102 */
																obj_t BgL_tmpz00_1340;

																BgL_tmpz00_1340 = CAR(((obj_t) BgL_argsz00_9));
																BgL_test1958z00_1339 =
																	BGL_UINT32P(BgL_tmpz00_1340);
															}
															if (BgL_test1958z00_1339)
																{	/* Jas/opcode.scm 102 */
																	if (NULLP(CDR(((obj_t) BgL_argsz00_9))))
																		{	/* Jas/opcode.scm 102 */
																			{	/* Jas/opcode.scm 106 */
																				long BgL_arg1367z00_390;

																				{	/* Jas/opcode.scm 106 */
																					obj_t BgL_arg1371z00_393;

																					BgL_arg1371z00_393 =
																						CAR(((obj_t) BgL_argsz00_9));
																					{	/* Jas/opcode.scm 106 */
																						uint32_t BgL_xz00_961;

																						BgL_xz00_961 =
																							BGL_BUINT32_TO_UINT32
																							(BgL_arg1371z00_393);
																						BgL_arg1367z00_390 =
																							(long) (BgL_xz00_961);
																				}}
																				{	/* Jas/opcode.scm 106 */
																					obj_t BgL_list1368z00_391;

																					{	/* Jas/opcode.scm 106 */
																						obj_t BgL_arg1370z00_392;

																						BgL_arg1370z00_392 =
																							MAKE_YOUNG_PAIR(BINT
																							(BgL_arg1367z00_390), BNIL);
																						BgL_list1368z00_391 =
																							MAKE_YOUNG_PAIR(BgL_copz00_8,
																							BgL_arg1370z00_392);
																					}
																					return BgL_list1368z00_391;
																				}
																			}
																		}
																	else
																		{	/* Jas/opcode.scm 102 */
																			goto BgL_tagzd2112zd2_335;
																		}
																}
															else
																{	/* Jas/opcode.scm 102 */
																	bool_t BgL_test1960z00_1355;

																	{	/* Jas/opcode.scm 102 */
																		obj_t BgL_tmpz00_1356;

																		BgL_tmpz00_1356 =
																			CAR(((obj_t) BgL_argsz00_9));
																		BgL_test1960z00_1355 =
																			BGL_INT32P(BgL_tmpz00_1356);
																	}
																	if (BgL_test1960z00_1355)
																		{	/* Jas/opcode.scm 102 */
																			if (NULLP(CDR(((obj_t) BgL_argsz00_9))))
																				{	/* Jas/opcode.scm 102 */
																					{	/* Jas/opcode.scm 107 */
																						long BgL_arg1375z00_394;

																						{	/* Jas/opcode.scm 107 */
																							obj_t BgL_arg1378z00_397;

																							BgL_arg1378z00_397 =
																								CAR(((obj_t) BgL_argsz00_9));
																							{	/* Jas/opcode.scm 107 */
																								int32_t BgL_xz00_964;

																								BgL_xz00_964 =
																									BGL_BINT32_TO_INT32
																									(BgL_arg1378z00_397);
																								{	/* Jas/opcode.scm 107 */
																									long BgL_arg1446z00_965;

																									BgL_arg1446z00_965 =
																										(long) (BgL_xz00_964);
																									BgL_arg1375z00_394 =
																										(long) (BgL_arg1446z00_965);
																						}}}
																						{	/* Jas/opcode.scm 107 */
																							obj_t BgL_list1376z00_395;

																							{	/* Jas/opcode.scm 107 */
																								obj_t BgL_arg1377z00_396;

																								BgL_arg1377z00_396 =
																									MAKE_YOUNG_PAIR(BINT
																									(BgL_arg1375z00_394), BNIL);
																								BgL_list1376z00_395 =
																									MAKE_YOUNG_PAIR(BgL_copz00_8,
																									BgL_arg1377z00_396);
																							}
																							return BgL_list1376z00_395;
																						}
																					}
																				}
																			else
																				{	/* Jas/opcode.scm 102 */
																					goto BgL_tagzd2112zd2_335;
																				}
																		}
																	else
																		{	/* Jas/opcode.scm 102 */
																			bool_t BgL_test1962z00_1372;

																			{	/* Jas/opcode.scm 102 */
																				obj_t BgL_tmpz00_1373;

																				BgL_tmpz00_1373 =
																					CAR(((obj_t) BgL_argsz00_9));
																				BgL_test1962z00_1372 =
																					BGL_INT16P(BgL_tmpz00_1373);
																			}
																			if (BgL_test1962z00_1372)
																				{	/* Jas/opcode.scm 102 */
																					if (NULLP(CDR(
																								((obj_t) BgL_argsz00_9))))
																						{	/* Jas/opcode.scm 102 */
																							{	/* Jas/opcode.scm 108 */
																								long BgL_arg1379z00_398;

																								{	/* Jas/opcode.scm 108 */
																									obj_t BgL_arg1410z00_401;

																									BgL_arg1410z00_401 =
																										CAR(
																										((obj_t) BgL_argsz00_9));
																									{	/* Jas/opcode.scm 108 */
																										int16_t BgL_xz00_969;

																										BgL_xz00_969 =
																											BGL_BINT16_TO_INT16
																											(BgL_arg1410z00_401);
																										{	/* Jas/opcode.scm 108 */
																											long BgL_arg1449z00_970;

																											BgL_arg1449z00_970 =
																												(long) (BgL_xz00_969);
																											BgL_arg1379z00_398 =
																												(long)
																												(BgL_arg1449z00_970);
																								}}}
																								{	/* Jas/opcode.scm 108 */
																									obj_t BgL_list1380z00_399;

																									{	/* Jas/opcode.scm 108 */
																										obj_t BgL_arg1408z00_400;

																										BgL_arg1408z00_400 =
																											MAKE_YOUNG_PAIR(BINT
																											(BgL_arg1379z00_398),
																											BNIL);
																										BgL_list1380z00_399 =
																											MAKE_YOUNG_PAIR
																											(BgL_copz00_8,
																											BgL_arg1408z00_400);
																									}
																									return BgL_list1380z00_399;
																								}
																							}
																						}
																					else
																						{	/* Jas/opcode.scm 102 */
																							goto BgL_tagzd2112zd2_335;
																						}
																				}
																			else
																				{	/* Jas/opcode.scm 102 */
																					bool_t BgL_test1964z00_1389;

																					{	/* Jas/opcode.scm 102 */
																						obj_t BgL_tmpz00_1390;

																						BgL_tmpz00_1390 =
																							CAR(((obj_t) BgL_argsz00_9));
																						BgL_test1964z00_1389 =
																							BGL_UINT16P(BgL_tmpz00_1390);
																					}
																					if (BgL_test1964z00_1389)
																						{	/* Jas/opcode.scm 102 */
																							if (NULLP(CDR(
																										((obj_t) BgL_argsz00_9))))
																								{	/* Jas/opcode.scm 102 */
																									{	/* Jas/opcode.scm 109 */
																										long BgL_arg1421z00_402;

																										{	/* Jas/opcode.scm 109 */
																											obj_t BgL_arg1437z00_405;

																											BgL_arg1437z00_405 =
																												CAR(
																												((obj_t)
																													BgL_argsz00_9));
																											{	/* Jas/opcode.scm 109 */
																												uint16_t BgL_xz00_974;

																												BgL_xz00_974 =
																													BGL_BUINT16_TO_UINT16
																													(BgL_arg1437z00_405);
																												{	/* Jas/opcode.scm 109 */
																													long
																														BgL_arg1447z00_975;
																													BgL_arg1447z00_975 =
																														(long)
																														(BgL_xz00_974);
																													BgL_arg1421z00_402 =
																														(long)
																														(BgL_arg1447z00_975);
																										}}}
																										{	/* Jas/opcode.scm 109 */
																											obj_t BgL_list1422z00_403;

																											{	/* Jas/opcode.scm 109 */
																												obj_t
																													BgL_arg1434z00_404;
																												BgL_arg1434z00_404 =
																													MAKE_YOUNG_PAIR(BINT
																													(BgL_arg1421z00_402),
																													BNIL);
																												BgL_list1422z00_403 =
																													MAKE_YOUNG_PAIR
																													(BgL_copz00_8,
																													BgL_arg1434z00_404);
																											}
																											return
																												BgL_list1422z00_403;
																										}
																									}
																								}
																							else
																								{	/* Jas/opcode.scm 102 */
																									goto BgL_tagzd2112zd2_335;
																								}
																						}
																					else
																						{	/* Jas/opcode.scm 102 */
																							bool_t BgL_test1966z00_1406;

																							{	/* Jas/opcode.scm 102 */
																								obj_t BgL_tmpz00_1407;

																								BgL_tmpz00_1407 =
																									CAR(((obj_t) BgL_argsz00_9));
																								BgL_test1966z00_1406 =
																									BGL_INT8P(BgL_tmpz00_1407);
																							}
																							if (BgL_test1966z00_1406)
																								{	/* Jas/opcode.scm 102 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_argsz00_9))))
																										{	/* Jas/opcode.scm 102 */
																											{	/* Jas/opcode.scm 110 */
																												long BgL_arg1448z00_406;

																												{	/* Jas/opcode.scm 110 */
																													obj_t
																														BgL_arg1454z00_409;
																													BgL_arg1454z00_409 =
																														CAR(((obj_t)
																															BgL_argsz00_9));
																													{	/* Jas/opcode.scm 110 */
																														int8_t BgL_xz00_979;

																														BgL_xz00_979 =
																															BGL_BINT8_TO_INT8
																															(BgL_arg1454z00_409);
																														{	/* Jas/opcode.scm 110 */
																															long
																																BgL_arg1451z00_980;
																															BgL_arg1451z00_980
																																=
																																(long)
																																(BgL_xz00_979);
																															BgL_arg1448z00_406
																																=
																																(long)
																																(BgL_arg1451z00_980);
																												}}}
																												{	/* Jas/opcode.scm 110 */
																													obj_t
																														BgL_list1449z00_407;
																													{	/* Jas/opcode.scm 110 */
																														obj_t
																															BgL_arg1453z00_408;
																														BgL_arg1453z00_408 =
																															MAKE_YOUNG_PAIR
																															(BINT
																															(BgL_arg1448z00_406),
																															BNIL);
																														BgL_list1449z00_407
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_copz00_8,
																															BgL_arg1453z00_408);
																													}
																													return
																														BgL_list1449z00_407;
																												}
																											}
																										}
																									else
																										{	/* Jas/opcode.scm 102 */
																											goto BgL_tagzd2112zd2_335;
																										}
																								}
																							else
																								{	/* Jas/opcode.scm 102 */
																									bool_t BgL_test1968z00_1423;

																									{	/* Jas/opcode.scm 102 */
																										obj_t BgL_tmpz00_1424;

																										BgL_tmpz00_1424 =
																											CAR(
																											((obj_t) BgL_argsz00_9));
																										BgL_test1968z00_1423 =
																											BGL_UINT8P
																											(BgL_tmpz00_1424);
																									}
																									if (BgL_test1968z00_1423)
																										{	/* Jas/opcode.scm 102 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_argsz00_9))))
																												{	/* Jas/opcode.scm 102 */
																													{	/* Jas/opcode.scm 111 */
																														long
																															BgL_arg1472z00_410;
																														{	/* Jas/opcode.scm 111 */
																															obj_t
																																BgL_arg1489z00_413;
																															BgL_arg1489z00_413
																																=
																																CAR(((obj_t)
																																	BgL_argsz00_9));
																															{	/* Jas/opcode.scm 111 */
																																uint8_t
																																	BgL_xz00_984;
																																BgL_xz00_984 =
																																	BGL_BUINT8_TO_UINT8
																																	(BgL_arg1489z00_413);
																																{	/* Jas/opcode.scm 111 */
																																	long
																																		BgL_arg1450z00_985;
																																	BgL_arg1450z00_985
																																		=
																																		(long)
																																		(BgL_xz00_984);
																																	BgL_arg1472z00_410
																																		=
																																		(long)
																																		(BgL_arg1450z00_985);
																														}}}
																														{	/* Jas/opcode.scm 111 */
																															obj_t
																																BgL_list1473z00_411;
																															{	/* Jas/opcode.scm 111 */
																																obj_t
																																	BgL_arg1485z00_412;
																																BgL_arg1485z00_412
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BINT
																																	(BgL_arg1472z00_410),
																																	BNIL);
																																BgL_list1473z00_411
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_copz00_8,
																																	BgL_arg1485z00_412);
																															}
																															return
																																BgL_list1473z00_411;
																														}
																													}
																												}
																											else
																												{	/* Jas/opcode.scm 102 */
																													goto
																														BgL_tagzd2112zd2_335;
																												}
																										}
																									else
																										{	/* Jas/opcode.scm 102 */
																											goto BgL_tagzd2112zd2_335;
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
									else
										{	/* Jas/opcode.scm 102 */
											goto BgL_tagzd2112zd2_335;
										}
								}
								break;
							case 18L:
							case 19L:

								{

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 113 */
											obj_t BgL_carzd2134zd2_419;

											BgL_carzd2134zd2_419 = CAR(((obj_t) BgL_argsz00_9));
											{

												if (INTEGERP(BgL_carzd2134zd2_419))
													{	/* Jas/opcode.scm 113 */
													BgL_kapzd2136zd2_420:
														if (NULLP(CDR(((obj_t) BgL_argsz00_9))))
															{	/* Jas/opcode.scm 113 */
																return
																	MAKE_YOUNG_PAIR(BINT(18L), BgL_argsz00_9);
															}
														else
															{	/* Jas/opcode.scm 113 */
															BgL_tagzd2132zd2_416:
																{	/* Jas/opcode.scm 218 */
																	obj_t BgL_arg1877z00_1006;

																	BgL_arg1877z00_1006 =
																		MAKE_YOUNG_PAIR(bgl_list_ref(CNST_TABLE_REF
																			(0), (long) CINT(BgL_copz00_8)),
																		BgL_argsz00_9);
																	return
																		BGl_jaszd2errorzd2zzjas_classfilez00((
																			(BgL_classfilez00_bglt)
																			BgL_classfilez00_6),
																		BGl_string1928z00zzjas_opcodez00,
																		BgL_arg1877z00_1006);
																}
															}
													}
												else
													{	/* Jas/opcode.scm 113 */
														if (REALP(BgL_carzd2134zd2_419))
															{	/* Jas/opcode.scm 113 */
																goto BgL_kapzd2136zd2_420;
															}
														else
															{	/* Jas/opcode.scm 113 */
																if (STRINGP(BgL_carzd2134zd2_419))
																	{	/* Jas/opcode.scm 113 */
																		goto BgL_kapzd2136zd2_420;
																	}
																else
																	{

																		if (BGL_INT32P(BgL_carzd2134zd2_419))
																			{	/* Jas/opcode.scm 113 */
																			BgL_kapzd2140zd2_425:
																				if (NULLP(CDR(((obj_t) BgL_argsz00_9))))
																					{	/* Jas/opcode.scm 113 */
																						return
																							MAKE_YOUNG_PAIR(BINT(18L),
																							BgL_argsz00_9);
																					}
																				else
																					{	/* Jas/opcode.scm 113 */
																						goto BgL_tagzd2132zd2_416;
																					}
																			}
																		else
																			{	/* Jas/opcode.scm 113 */
																				if (BGL_UINT32P(BgL_carzd2134zd2_419))
																					{	/* Jas/opcode.scm 113 */
																						goto BgL_kapzd2140zd2_425;
																					}
																				else
																					{	/* Jas/opcode.scm 113 */
																						goto BgL_tagzd2132zd2_416;
																					}
																			}
																	}
															}
													}
											}
										}
									else
										{	/* Jas/opcode.scm 113 */
											goto BgL_tagzd2132zd2_416;
										}
								}
								break;
							case 20L:

								{

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 118 */
											obj_t BgL_carzd2145zd2_439;

											BgL_carzd2145zd2_439 = CAR(((obj_t) BgL_argsz00_9));
											{

												if (INTEGERP(BgL_carzd2145zd2_439))
													{	/* Jas/opcode.scm 118 */
													BgL_kapzd2147zd2_440:
														if (NULLP(CDR(((obj_t) BgL_argsz00_9))))
															{	/* Jas/opcode.scm 118 */
																return
																	MAKE_YOUNG_PAIR(BINT(20L), BgL_argsz00_9);
															}
														else
															{	/* Jas/opcode.scm 118 */
															BgL_tagzd2143zd2_436:
																{	/* Jas/opcode.scm 218 */
																	obj_t BgL_arg1877z00_1012;

																	BgL_arg1877z00_1012 =
																		MAKE_YOUNG_PAIR(bgl_list_ref(CNST_TABLE_REF
																			(0), (long) CINT(BgL_copz00_8)),
																		BgL_argsz00_9);
																	return
																		BGl_jaszd2errorzd2zzjas_classfilez00((
																			(BgL_classfilez00_bglt)
																			BgL_classfilez00_6),
																		BGl_string1928z00zzjas_opcodez00,
																		BgL_arg1877z00_1012);
																}
															}
													}
												else
													{	/* Jas/opcode.scm 118 */
														if (REALP(BgL_carzd2145zd2_439))
															{	/* Jas/opcode.scm 118 */
																goto BgL_kapzd2147zd2_440;
															}
														else
															{	/* Jas/opcode.scm 118 */
																if (ELONGP(BgL_carzd2145zd2_439))
																	{	/* Jas/opcode.scm 118 */
																		goto BgL_kapzd2147zd2_440;
																	}
																else
																	{	/* Jas/opcode.scm 118 */
																		if (LLONGP(BgL_carzd2145zd2_439))
																			{	/* Jas/opcode.scm 118 */
																				goto BgL_kapzd2147zd2_440;
																			}
																		else
																			{

																				if (BGL_UINT64P(BgL_carzd2145zd2_439))
																					{	/* Jas/opcode.scm 118 */
																					BgL_kapzd2152zd2_446:
																						if (NULLP(CDR(
																									((obj_t) BgL_argsz00_9))))
																							{	/* Jas/opcode.scm 118 */
																								return
																									MAKE_YOUNG_PAIR(BINT(20L),
																									BgL_argsz00_9);
																							}
																						else
																							{	/* Jas/opcode.scm 118 */
																								goto BgL_tagzd2143zd2_436;
																							}
																					}
																				else
																					{	/* Jas/opcode.scm 118 */
																						if (BGL_INT64P
																							(BgL_carzd2145zd2_439))
																							{	/* Jas/opcode.scm 118 */
																								goto BgL_kapzd2152zd2_446;
																							}
																						else
																							{	/* Jas/opcode.scm 118 */
																								goto BgL_tagzd2143zd2_436;
																							}
																					}
																			}
																	}
															}
													}
											}
										}
									else
										{	/* Jas/opcode.scm 118 */
											goto BgL_tagzd2143zd2_436;
										}
								}
								break;
							case 21L:
							case 22L:
							case 23L:
							case 24L:
							case 25L:
							case 54L:
							case 55L:
							case 56L:
							case 57L:
							case 58L:
							case 169L:

								{
									obj_t BgL_varz00_455;

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 125 */
											obj_t BgL_carzd2158zd2_460;

											BgL_carzd2158zd2_460 = CAR(((obj_t) BgL_argsz00_9));
											if (SYMBOLP(BgL_carzd2158zd2_460))
												{	/* Jas/opcode.scm 125 */
													if (NULLP(CDR(((obj_t) BgL_argsz00_9))))
														{	/* Jas/opcode.scm 125 */
															BgL_varz00_455 = BgL_carzd2158zd2_460;
															{	/* Jas/opcode.scm 127 */
																obj_t BgL_arg1565z00_465;

																BgL_arg1565z00_465 =
																	BGl_vzd2indexzd2zzjas_opcodez00
																	(BgL_classfilez00_6, BgL_varz00_455,
																	BgL_localsz00_7);
																{	/* Jas/opcode.scm 127 */
																	obj_t BgL_list1566z00_466;

																	{	/* Jas/opcode.scm 127 */
																		obj_t BgL_arg1571z00_467;

																		BgL_arg1571z00_467 =
																			MAKE_YOUNG_PAIR(BgL_arg1565z00_465, BNIL);
																		BgL_list1566z00_466 =
																			MAKE_YOUNG_PAIR(BgL_copz00_8,
																			BgL_arg1571z00_467);
																	}
																	return BgL_list1566z00_466;
																}
															}
														}
													else
														{	/* Jas/opcode.scm 125 */
														BgL_tagzd2154zd2_457:
															{	/* Jas/opcode.scm 218 */
																obj_t BgL_arg1877z00_1019;

																BgL_arg1877z00_1019 =
																	MAKE_YOUNG_PAIR(bgl_list_ref(CNST_TABLE_REF
																		(0), (long) CINT(BgL_copz00_8)),
																	BgL_argsz00_9);
																return
																	BGl_jaszd2errorzd2zzjas_classfilez00((
																		(BgL_classfilez00_bglt) BgL_classfilez00_6),
																	BGl_string1928z00zzjas_opcodez00,
																	BgL_arg1877z00_1019);
															}
														}
												}
											else
												{	/* Jas/opcode.scm 125 */
													goto BgL_tagzd2154zd2_457;
												}
										}
									else
										{	/* Jas/opcode.scm 125 */
											goto BgL_tagzd2154zd2_457;
										}
								}
								break;
							case 26L:
							case 27L:
							case 28L:
							case 29L:
							case 30L:
							case 31L:
							case 32L:
							case 33L:
							case 34L:
							case 35L:
							case 36L:
							case 37L:
							case 38L:
							case 39L:
							case 40L:
							case 41L:
							case 42L:
							case 43L:
							case 44L:
							case 45L:

								{

									if (NULLP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 129 */
											{	/* Jas/opcode.scm 131 */
												long BgL_dz00_472;

												BgL_dz00_472 = ((long) CINT(BgL_copz00_8) - 26L);
												{	/* Jas/opcode.scm 132 */
													long BgL_arg1575z00_473;
													long BgL_arg1576z00_474;

													BgL_arg1575z00_473 = (21L + (BgL_dz00_472 / 4L));
													{	/* Jas/opcode.scm 132 */
														long BgL_n1z00_1026;
														long BgL_n2z00_1027;

														BgL_n1z00_1026 = BgL_dz00_472;
														BgL_n2z00_1027 = 4L;
														{	/* Jas/opcode.scm 132 */
															bool_t BgL_test1991z00_1531;

															{	/* Jas/opcode.scm 132 */
																long BgL_arg1338z00_1029;

																BgL_arg1338z00_1029 =
																	(((BgL_n1z00_1026) | (BgL_n2z00_1027)) &
																	-2147483648);
																BgL_test1991z00_1531 =
																	(BgL_arg1338z00_1029 == 0L);
															}
															if (BgL_test1991z00_1531)
																{	/* Jas/opcode.scm 132 */
																	int32_t BgL_arg1334z00_1030;

																	{	/* Jas/opcode.scm 132 */
																		int32_t BgL_arg1336z00_1031;
																		int32_t BgL_arg1337z00_1032;

																		BgL_arg1336z00_1031 =
																			(int32_t) (BgL_n1z00_1026);
																		BgL_arg1337z00_1032 =
																			(int32_t) (BgL_n2z00_1027);
																		BgL_arg1334z00_1030 =
																			(BgL_arg1336z00_1031 %
																			BgL_arg1337z00_1032);
																	}
																	{	/* Jas/opcode.scm 132 */
																		long BgL_arg1446z00_1037;

																		BgL_arg1446z00_1037 =
																			(long) (BgL_arg1334z00_1030);
																		BgL_arg1576z00_474 =
																			(long) (BgL_arg1446z00_1037);
																}}
															else
																{	/* Jas/opcode.scm 132 */
																	BgL_arg1576z00_474 =
																		(BgL_n1z00_1026 % BgL_n2z00_1027);
																}
														}
													}
													{	/* Jas/opcode.scm 132 */
														obj_t BgL_list1577z00_475;

														{	/* Jas/opcode.scm 132 */
															obj_t BgL_arg1584z00_476;

															BgL_arg1584z00_476 =
																MAKE_YOUNG_PAIR(BINT(BgL_arg1576z00_474), BNIL);
															BgL_list1577z00_475 =
																MAKE_YOUNG_PAIR(BINT(BgL_arg1575z00_473),
																BgL_arg1584z00_476);
														}
														return BgL_list1577z00_475;
													}
												}
											}
										}
									else
										{	/* Jas/opcode.scm 129 */
											{	/* Jas/opcode.scm 218 */
												obj_t BgL_arg1877z00_1040;

												BgL_arg1877z00_1040 =
													MAKE_YOUNG_PAIR(bgl_list_ref(CNST_TABLE_REF(0),
														(long) CINT(BgL_copz00_8)), BgL_argsz00_9);
												return
													BGl_jaszd2errorzd2zzjas_classfilez00(
													((BgL_classfilez00_bglt) BgL_classfilez00_6),
													BGl_string1928z00zzjas_opcodez00,
													BgL_arg1877z00_1040);
											}
										}
								}
								break;
							case 59L:
							case 60L:
							case 61L:
							case 62L:
							case 63L:
							case 64L:
							case 65L:
							case 66L:
							case 67L:
							case 68L:
							case 69L:
							case 70L:
							case 71L:
							case 72L:
							case 73L:
							case 74L:
							case 75L:
							case 76L:
							case 77L:
							case 78L:

								{

									if (NULLP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 134 */
											{	/* Jas/opcode.scm 136 */
												long BgL_dz00_482;

												BgL_dz00_482 = ((long) CINT(BgL_copz00_8) - 59L);
												{	/* Jas/opcode.scm 137 */
													long BgL_arg1589z00_483;
													long BgL_arg1591z00_484;

													BgL_arg1589z00_483 = (54L + (BgL_dz00_482 / 4L));
													{	/* Jas/opcode.scm 137 */
														long BgL_n1z00_1045;
														long BgL_n2z00_1046;

														BgL_n1z00_1045 = BgL_dz00_482;
														BgL_n2z00_1046 = 4L;
														{	/* Jas/opcode.scm 137 */
															bool_t BgL_test1993z00_1556;

															{	/* Jas/opcode.scm 137 */
																long BgL_arg1338z00_1048;

																BgL_arg1338z00_1048 =
																	(((BgL_n1z00_1045) | (BgL_n2z00_1046)) &
																	-2147483648);
																BgL_test1993z00_1556 =
																	(BgL_arg1338z00_1048 == 0L);
															}
															if (BgL_test1993z00_1556)
																{	/* Jas/opcode.scm 137 */
																	int32_t BgL_arg1334z00_1049;

																	{	/* Jas/opcode.scm 137 */
																		int32_t BgL_arg1336z00_1050;
																		int32_t BgL_arg1337z00_1051;

																		BgL_arg1336z00_1050 =
																			(int32_t) (BgL_n1z00_1045);
																		BgL_arg1337z00_1051 =
																			(int32_t) (BgL_n2z00_1046);
																		BgL_arg1334z00_1049 =
																			(BgL_arg1336z00_1050 %
																			BgL_arg1337z00_1051);
																	}
																	{	/* Jas/opcode.scm 137 */
																		long BgL_arg1446z00_1056;

																		BgL_arg1446z00_1056 =
																			(long) (BgL_arg1334z00_1049);
																		BgL_arg1591z00_484 =
																			(long) (BgL_arg1446z00_1056);
																}}
															else
																{	/* Jas/opcode.scm 137 */
																	BgL_arg1591z00_484 =
																		(BgL_n1z00_1045 % BgL_n2z00_1046);
																}
														}
													}
													{	/* Jas/opcode.scm 137 */
														obj_t BgL_list1592z00_485;

														{	/* Jas/opcode.scm 137 */
															obj_t BgL_arg1593z00_486;

															BgL_arg1593z00_486 =
																MAKE_YOUNG_PAIR(BINT(BgL_arg1591z00_484), BNIL);
															BgL_list1592z00_485 =
																MAKE_YOUNG_PAIR(BINT(BgL_arg1589z00_483),
																BgL_arg1593z00_486);
														}
														return BgL_list1592z00_485;
													}
												}
											}
										}
									else
										{	/* Jas/opcode.scm 134 */
											{	/* Jas/opcode.scm 218 */
												obj_t BgL_arg1877z00_1059;

												BgL_arg1877z00_1059 =
													MAKE_YOUNG_PAIR(bgl_list_ref(CNST_TABLE_REF(0),
														(long) CINT(BgL_copz00_8)), BgL_argsz00_9);
												return
													BGl_jaszd2errorzd2zzjas_classfilez00(
													((BgL_classfilez00_bglt) BgL_classfilez00_6),
													BGl_string1928z00zzjas_opcodez00,
													BgL_arg1877z00_1059);
											}
										}
								}
								break;
							case 132L:

								{
									obj_t BgL_varz00_488;
									obj_t BgL_nz00_489;

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 139 */
											obj_t BgL_carzd2175zd2_494;
											obj_t BgL_cdrzd2176zd2_495;

											BgL_carzd2175zd2_494 = CAR(((obj_t) BgL_argsz00_9));
											BgL_cdrzd2176zd2_495 = CDR(((obj_t) BgL_argsz00_9));
											if (SYMBOLP(BgL_carzd2175zd2_494))
												{	/* Jas/opcode.scm 139 */
													if (PAIRP(BgL_cdrzd2176zd2_495))
														{	/* Jas/opcode.scm 139 */
															obj_t BgL_carzd2180zd2_498;

															BgL_carzd2180zd2_498 = CAR(BgL_cdrzd2176zd2_495);
															if (INTEGERP(BgL_carzd2180zd2_498))
																{	/* Jas/opcode.scm 139 */
																	if (NULLP(CDR(BgL_cdrzd2176zd2_495)))
																		{	/* Jas/opcode.scm 139 */
																			BgL_varz00_488 = BgL_carzd2175zd2_494;
																			BgL_nz00_489 = BgL_carzd2180zd2_498;
																			{	/* Jas/opcode.scm 142 */
																				obj_t BgL_arg1606z00_503;

																				BgL_arg1606z00_503 =
																					BGl_vzd2indexzd2zzjas_opcodez00
																					(BgL_classfilez00_6, BgL_varz00_488,
																					BgL_localsz00_7);
																				{	/* Jas/opcode.scm 142 */
																					obj_t BgL_list1607z00_504;

																					{	/* Jas/opcode.scm 142 */
																						obj_t BgL_arg1609z00_505;

																						{	/* Jas/opcode.scm 142 */
																							obj_t BgL_arg1611z00_506;

																							BgL_arg1611z00_506 =
																								MAKE_YOUNG_PAIR(BgL_nz00_489,
																								BNIL);
																							BgL_arg1609z00_505 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1606z00_503,
																								BgL_arg1611z00_506);
																						}
																						BgL_list1607z00_504 =
																							MAKE_YOUNG_PAIR(BgL_copz00_8,
																							BgL_arg1609z00_505);
																					}
																					return BgL_list1607z00_504;
																				}
																			}
																		}
																	else
																		{	/* Jas/opcode.scm 139 */
																		BgL_tagzd2169zd2_491:
																			{	/* Jas/opcode.scm 218 */
																				obj_t BgL_arg1877z00_1062;

																				BgL_arg1877z00_1062 =
																					MAKE_YOUNG_PAIR(bgl_list_ref
																					(CNST_TABLE_REF(0),
																						(long) CINT(BgL_copz00_8)),
																					BgL_argsz00_9);
																				return
																					BGl_jaszd2errorzd2zzjas_classfilez00((
																						(BgL_classfilez00_bglt)
																						BgL_classfilez00_6),
																					BGl_string1928z00zzjas_opcodez00,
																					BgL_arg1877z00_1062);
																			}
																		}
																}
															else
																{	/* Jas/opcode.scm 139 */
																	goto BgL_tagzd2169zd2_491;
																}
														}
													else
														{	/* Jas/opcode.scm 139 */
															goto BgL_tagzd2169zd2_491;
														}
												}
											else
												{	/* Jas/opcode.scm 139 */
													goto BgL_tagzd2169zd2_491;
												}
										}
									else
										{	/* Jas/opcode.scm 139 */
											goto BgL_tagzd2169zd2_491;
										}
								}
								break;
							case 153L:
							case 154L:
							case 155L:
							case 156L:
							case 157L:
							case 158L:
							case 159L:
							case 160L:
							case 161L:
							case 162L:
							case 163L:
							case 164L:
							case 165L:
							case 166L:
							case 167L:
							case 168L:
							case 198L:
							case 199L:
							case 200L:
							case 201L:

								{
									obj_t BgL_labz00_507;

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 144 */
											obj_t BgL_carzd2189zd2_512;

											BgL_carzd2189zd2_512 = CAR(((obj_t) BgL_argsz00_9));
											if (SYMBOLP(BgL_carzd2189zd2_512))
												{	/* Jas/opcode.scm 144 */
													if (NULLP(CDR(((obj_t) BgL_argsz00_9))))
														{	/* Jas/opcode.scm 144 */
															BgL_labz00_507 = BgL_carzd2189zd2_512;
															{	/* Jas/opcode.scm 147 */
																obj_t BgL_list1626z00_517;

																{	/* Jas/opcode.scm 147 */
																	obj_t BgL_arg1627z00_518;

																	BgL_arg1627z00_518 =
																		MAKE_YOUNG_PAIR(BgL_labz00_507, BNIL);
																	BgL_list1626z00_517 =
																		MAKE_YOUNG_PAIR(BgL_copz00_8,
																		BgL_arg1627z00_518);
																}
																return BgL_list1626z00_517;
															}
														}
													else
														{	/* Jas/opcode.scm 144 */
														BgL_tagzd2185zd2_509:
															{	/* Jas/opcode.scm 218 */
																obj_t BgL_arg1877z00_1069;

																BgL_arg1877z00_1069 =
																	MAKE_YOUNG_PAIR(bgl_list_ref(CNST_TABLE_REF
																		(0), (long) CINT(BgL_copz00_8)),
																	BgL_argsz00_9);
																return
																	BGl_jaszd2errorzd2zzjas_classfilez00((
																		(BgL_classfilez00_bglt) BgL_classfilez00_6),
																	BGl_string1928z00zzjas_opcodez00,
																	BgL_arg1877z00_1069);
															}
														}
												}
											else
												{	/* Jas/opcode.scm 144 */
													goto BgL_tagzd2185zd2_509;
												}
										}
									else
										{	/* Jas/opcode.scm 144 */
											goto BgL_tagzd2185zd2_509;
										}
								}
								break;
							case 170L:

								{

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 149 */
											obj_t BgL_carzd2204zd2_526;
											obj_t BgL_cdrzd2205zd2_527;

											BgL_carzd2204zd2_526 = CAR(((obj_t) BgL_argsz00_9));
											BgL_cdrzd2205zd2_527 = CDR(((obj_t) BgL_argsz00_9));
											if (SYMBOLP(BgL_carzd2204zd2_526))
												{	/* Jas/opcode.scm 149 */
													if (PAIRP(BgL_cdrzd2205zd2_527))
														{	/* Jas/opcode.scm 149 */
															obj_t BgL_carzd2210zd2_531;
															obj_t BgL_cdrzd2211zd2_532;

															BgL_carzd2210zd2_531 = CAR(BgL_cdrzd2205zd2_527);
															BgL_cdrzd2211zd2_532 = CDR(BgL_cdrzd2205zd2_527);
															if (INTEGERP(BgL_carzd2210zd2_531))
																{
																	obj_t BgL_gzd2220zd2_546;
																	obj_t BgL_gzd2216zd2_538;

																	BgL_gzd2216zd2_538 = BgL_cdrzd2211zd2_532;
																	if (NULLP(BgL_gzd2216zd2_538))
																		{	/* Jas/opcode.scm 149 */
																			return
																				MAKE_YOUNG_PAIR(BgL_copz00_8,
																				BgL_argsz00_9);
																		}
																	else
																		{	/* Jas/opcode.scm 149 */
																			if (PAIRP(BgL_gzd2216zd2_538))
																				{	/* Jas/opcode.scm 149 */
																					bool_t BgL_test2008z00_1638;

																					{	/* Jas/opcode.scm 149 */
																						obj_t BgL_tmpz00_1639;

																						BgL_tmpz00_1639 =
																							CAR(BgL_gzd2216zd2_538);
																						BgL_test2008z00_1638 =
																							SYMBOLP(BgL_tmpz00_1639);
																					}
																					if (BgL_test2008z00_1638)
																						{	/* Jas/opcode.scm 149 */
																							BgL_gzd2220zd2_546 =
																								CDR(BgL_gzd2216zd2_538);
																						BgL_zc3z04anonymousza31651ze3z87_547:
																							if (NULLP
																								(BgL_gzd2220zd2_546))
																								{	/* Jas/opcode.scm 149 */
																									return
																										MAKE_YOUNG_PAIR
																										(BgL_copz00_8,
																										BgL_argsz00_9);
																								}
																							else
																								{	/* Jas/opcode.scm 149 */
																									if (PAIRP(BgL_gzd2220zd2_546))
																										{	/* Jas/opcode.scm 149 */
																											bool_t
																												BgL_test2011z00_1647;
																											{	/* Jas/opcode.scm 149 */
																												obj_t BgL_tmpz00_1648;

																												BgL_tmpz00_1648 =
																													CAR
																													(BgL_gzd2220zd2_546);
																												BgL_test2011z00_1647 =
																													SYMBOLP
																													(BgL_tmpz00_1648);
																											}
																											if (BgL_test2011z00_1647)
																												{
																													obj_t
																														BgL_gzd2220zd2_1651;
																													BgL_gzd2220zd2_1651 =
																														CDR
																														(BgL_gzd2220zd2_546);
																													BgL_gzd2220zd2_546 =
																														BgL_gzd2220zd2_1651;
																													goto
																														BgL_zc3z04anonymousza31651ze3z87_547;
																												}
																											else
																												{	/* Jas/opcode.scm 149 */
																												BgL_tagzd2194zd2_523:
																													{	/* Jas/opcode.scm 218 */
																														obj_t
																															BgL_arg1877z00_1073;
																														BgL_arg1877z00_1073
																															=
																															MAKE_YOUNG_PAIR
																															(bgl_list_ref
																															(CNST_TABLE_REF
																																(0),
																																(long)
																																CINT
																																(BgL_copz00_8)),
																															BgL_argsz00_9);
																														return
																															BGl_jaszd2errorzd2zzjas_classfilez00
																															(((BgL_classfilez00_bglt) BgL_classfilez00_6), BGl_string1928z00zzjas_opcodez00, BgL_arg1877z00_1073);
																													}
																												}
																										}
																									else
																										{	/* Jas/opcode.scm 149 */
																											goto BgL_tagzd2194zd2_523;
																										}
																								}
																						}
																					else
																						{	/* Jas/opcode.scm 149 */
																							goto BgL_tagzd2194zd2_523;
																						}
																				}
																			else
																				{	/* Jas/opcode.scm 149 */
																					goto BgL_tagzd2194zd2_523;
																				}
																		}
																}
															else
																{	/* Jas/opcode.scm 149 */
																	goto BgL_tagzd2194zd2_523;
																}
														}
													else
														{	/* Jas/opcode.scm 149 */
															goto BgL_tagzd2194zd2_523;
														}
												}
											else
												{	/* Jas/opcode.scm 149 */
													goto BgL_tagzd2194zd2_523;
												}
										}
									else
										{	/* Jas/opcode.scm 149 */
											goto BgL_tagzd2194zd2_523;
										}
								}
								break;
							case 171L:

								{

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 155 */
											obj_t BgL_carzd2233zd2_562;
											obj_t BgL_cdrzd2234zd2_563;

											BgL_carzd2233zd2_562 = CAR(((obj_t) BgL_argsz00_9));
											BgL_cdrzd2234zd2_563 = CDR(((obj_t) BgL_argsz00_9));
											if (SYMBOLP(BgL_carzd2233zd2_562))
												{
													obj_t BgL_gzd2243zd2_577;
													obj_t BgL_gzd2239zd2_569;

													BgL_gzd2239zd2_569 = BgL_cdrzd2234zd2_563;
													if (NULLP(BgL_gzd2239zd2_569))
														{	/* Jas/opcode.scm 155 */
															return
																MAKE_YOUNG_PAIR(BgL_copz00_8, BgL_argsz00_9);
														}
													else
														{	/* Jas/opcode.scm 155 */
															if (PAIRP(BgL_gzd2239zd2_569))
																{	/* Jas/opcode.scm 155 */
																	if (BGl_slotzf3zf3zzjas_opcodez00(CAR
																			(BgL_gzd2239zd2_569)))
																		{	/* Jas/opcode.scm 155 */
																			BgL_gzd2243zd2_577 =
																				CDR(BgL_gzd2239zd2_569);
																		BgL_zc3z04anonymousza31682ze3z87_578:
																			if (NULLP(BgL_gzd2243zd2_577))
																				{	/* Jas/opcode.scm 155 */
																					return
																						MAKE_YOUNG_PAIR(BgL_copz00_8,
																						BgL_argsz00_9);
																				}
																			else
																				{	/* Jas/opcode.scm 155 */
																					if (PAIRP(BgL_gzd2243zd2_577))
																						{	/* Jas/opcode.scm 155 */
																							if (BGl_slotzf3zf3zzjas_opcodez00
																								(CAR(BgL_gzd2243zd2_577)))
																								{
																									obj_t BgL_gzd2243zd2_1684;

																									BgL_gzd2243zd2_1684 =
																										CDR(BgL_gzd2243zd2_577);
																									BgL_gzd2243zd2_577 =
																										BgL_gzd2243zd2_1684;
																									goto
																										BgL_zc3z04anonymousza31682ze3z87_578;
																								}
																							else
																								{	/* Jas/opcode.scm 155 */
																								BgL_tagzd2225zd2_559:
																									{	/* Jas/opcode.scm 218 */
																										obj_t BgL_arg1877z00_1083;

																										BgL_arg1877z00_1083 =
																											MAKE_YOUNG_PAIR
																											(bgl_list_ref
																											(CNST_TABLE_REF(0),
																												(long)
																												CINT(BgL_copz00_8)),
																											BgL_argsz00_9);
																										return
																											BGl_jaszd2errorzd2zzjas_classfilez00
																											(((BgL_classfilez00_bglt)
																												BgL_classfilez00_6),
																											BGl_string1928z00zzjas_opcodez00,
																											BgL_arg1877z00_1083);
																									}
																								}
																						}
																					else
																						{	/* Jas/opcode.scm 155 */
																							goto BgL_tagzd2225zd2_559;
																						}
																				}
																		}
																	else
																		{	/* Jas/opcode.scm 155 */
																			goto BgL_tagzd2225zd2_559;
																		}
																}
															else
																{	/* Jas/opcode.scm 155 */
																	goto BgL_tagzd2225zd2_559;
																}
														}
												}
											else
												{	/* Jas/opcode.scm 155 */
													goto BgL_tagzd2225zd2_559;
												}
										}
									else
										{	/* Jas/opcode.scm 155 */
											goto BgL_tagzd2225zd2_559;
										}
								}
								break;
							case 178L:
							case 179L:
							case 180L:
							case 181L:

								{
									obj_t BgL_varz00_587;

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 160 */
											obj_t BgL_carzd2252zd2_592;

											BgL_carzd2252zd2_592 = CAR(((obj_t) BgL_argsz00_9));
											if (SYMBOLP(BgL_carzd2252zd2_592))
												{	/* Jas/opcode.scm 160 */
													if (NULLP(CDR(((obj_t) BgL_argsz00_9))))
														{	/* Jas/opcode.scm 160 */
															BgL_varz00_587 = BgL_carzd2252zd2_592;
															{	/* Jas/opcode.scm 162 */
																BgL_fieldz00_bglt BgL_arg1702z00_597;

																BgL_arg1702z00_597 =
																	BGl_declaredzd2fieldzd2zzjas_classfilez00(
																	((BgL_classfilez00_bglt) BgL_classfilez00_6),
																	BgL_varz00_587);
																{	/* Jas/opcode.scm 162 */
																	obj_t BgL_list1703z00_598;

																	{	/* Jas/opcode.scm 162 */
																		obj_t BgL_arg1705z00_599;

																		BgL_arg1705z00_599 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_arg1702z00_597), BNIL);
																		BgL_list1703z00_598 =
																			MAKE_YOUNG_PAIR(BgL_copz00_8,
																			BgL_arg1705z00_599);
																	}
																	return BgL_list1703z00_598;
																}
															}
														}
													else
														{	/* Jas/opcode.scm 160 */
														BgL_tagzd2248zd2_589:
															{	/* Jas/opcode.scm 218 */
																obj_t BgL_arg1877z00_1092;

																BgL_arg1877z00_1092 =
																	MAKE_YOUNG_PAIR(bgl_list_ref(CNST_TABLE_REF
																		(0), (long) CINT(BgL_copz00_8)),
																	BgL_argsz00_9);
																return
																	BGl_jaszd2errorzd2zzjas_classfilez00((
																		(BgL_classfilez00_bglt) BgL_classfilez00_6),
																	BGl_string1928z00zzjas_opcodez00,
																	BgL_arg1877z00_1092);
															}
														}
												}
											else
												{	/* Jas/opcode.scm 160 */
													goto BgL_tagzd2248zd2_589;
												}
										}
									else
										{	/* Jas/opcode.scm 160 */
											goto BgL_tagzd2248zd2_589;
										}
								}
								break;
							case 182L:
							case 183L:
							case 184L:
							case 185L:

								{
									obj_t BgL_varz00_600;

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 164 */
											obj_t BgL_carzd2261zd2_605;

											BgL_carzd2261zd2_605 = CAR(((obj_t) BgL_argsz00_9));
											if (SYMBOLP(BgL_carzd2261zd2_605))
												{	/* Jas/opcode.scm 164 */
													if (NULLP(CDR(((obj_t) BgL_argsz00_9))))
														{	/* Jas/opcode.scm 164 */
															BgL_varz00_600 = BgL_carzd2261zd2_605;
															{	/* Jas/opcode.scm 166 */
																BgL_methodz00_bglt BgL_arg1717z00_610;

																BgL_arg1717z00_610 =
																	BGl_declaredzd2methodzd2zzjas_classfilez00(
																	((BgL_classfilez00_bglt) BgL_classfilez00_6),
																	BgL_varz00_600);
																{	/* Jas/opcode.scm 166 */
																	obj_t BgL_list1718z00_611;

																	{	/* Jas/opcode.scm 166 */
																		obj_t BgL_arg1720z00_612;

																		BgL_arg1720z00_612 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_arg1717z00_610), BNIL);
																		BgL_list1718z00_611 =
																			MAKE_YOUNG_PAIR(BgL_copz00_8,
																			BgL_arg1720z00_612);
																	}
																	return BgL_list1718z00_611;
																}
															}
														}
													else
														{	/* Jas/opcode.scm 164 */
														BgL_tagzd2257zd2_602:
															{	/* Jas/opcode.scm 218 */
																obj_t BgL_arg1877z00_1097;

																BgL_arg1877z00_1097 =
																	MAKE_YOUNG_PAIR(bgl_list_ref(CNST_TABLE_REF
																		(0), (long) CINT(BgL_copz00_8)),
																	BgL_argsz00_9);
																return
																	BGl_jaszd2errorzd2zzjas_classfilez00((
																		(BgL_classfilez00_bglt) BgL_classfilez00_6),
																	BGl_string1928z00zzjas_opcodez00,
																	BgL_arg1877z00_1097);
															}
														}
												}
											else
												{	/* Jas/opcode.scm 164 */
													goto BgL_tagzd2257zd2_602;
												}
										}
									else
										{	/* Jas/opcode.scm 164 */
											goto BgL_tagzd2257zd2_602;
										}
								}
								break;
							case 187L:

								{
									obj_t BgL_varz00_613;

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 168 */
											obj_t BgL_carzd2270zd2_618;

											BgL_carzd2270zd2_618 = CAR(((obj_t) BgL_argsz00_9));
											if (SYMBOLP(BgL_carzd2270zd2_618))
												{	/* Jas/opcode.scm 168 */
													if (NULLP(CDR(((obj_t) BgL_argsz00_9))))
														{	/* Jas/opcode.scm 168 */
															BgL_varz00_613 = BgL_carzd2270zd2_618;
															{	/* Jas/opcode.scm 170 */
																BgL_classez00_bglt BgL_arg1734z00_623;

																BgL_arg1734z00_623 =
																	BGl_declaredzd2classzd2zzjas_classfilez00(
																	((BgL_classfilez00_bglt) BgL_classfilez00_6),
																	BgL_varz00_613);
																{	/* Jas/opcode.scm 170 */
																	obj_t BgL_list1735z00_624;

																	{	/* Jas/opcode.scm 170 */
																		obj_t BgL_arg1736z00_625;

																		BgL_arg1736z00_625 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_arg1734z00_623), BNIL);
																		BgL_list1735z00_624 =
																			MAKE_YOUNG_PAIR(BgL_copz00_8,
																			BgL_arg1736z00_625);
																	}
																	return BgL_list1735z00_624;
																}
															}
														}
													else
														{	/* Jas/opcode.scm 168 */
														BgL_tagzd2266zd2_615:
															{	/* Jas/opcode.scm 218 */
																obj_t BgL_arg1877z00_1102;

																BgL_arg1877z00_1102 =
																	MAKE_YOUNG_PAIR(bgl_list_ref(CNST_TABLE_REF
																		(0), (long) CINT(BgL_copz00_8)),
																	BgL_argsz00_9);
																return
																	BGl_jaszd2errorzd2zzjas_classfilez00((
																		(BgL_classfilez00_bglt) BgL_classfilez00_6),
																	BGl_string1928z00zzjas_opcodez00,
																	BgL_arg1877z00_1102);
															}
														}
												}
											else
												{	/* Jas/opcode.scm 168 */
													goto BgL_tagzd2266zd2_615;
												}
										}
									else
										{	/* Jas/opcode.scm 168 */
											goto BgL_tagzd2266zd2_615;
										}
								}
								break;
							case 188L:

								{
									obj_t BgL_typez00_626;

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 172 */
											if (NULLP(CDR(((obj_t) BgL_argsz00_9))))
												{	/* Jas/opcode.scm 172 */
													obj_t BgL_arg1740z00_633;

													BgL_arg1740z00_633 = CAR(((obj_t) BgL_argsz00_9));
													BgL_typez00_626 = BgL_arg1740z00_633;
													{	/* Jas/opcode.scm 174 */
														obj_t BgL_arg1747z00_635;

														BgL_arg1747z00_635 =
															BGl_getzd2typezd2numz00zzjas_opcodez00
															(BgL_classfilez00_6, BgL_typez00_626);
														{	/* Jas/opcode.scm 174 */
															obj_t BgL_list1748z00_636;

															{	/* Jas/opcode.scm 174 */
																obj_t BgL_arg1749z00_637;

																BgL_arg1749z00_637 =
																	MAKE_YOUNG_PAIR(BgL_arg1747z00_635, BNIL);
																BgL_list1748z00_636 =
																	MAKE_YOUNG_PAIR(BgL_copz00_8,
																	BgL_arg1749z00_637);
															}
															return BgL_list1748z00_636;
														}
													}
												}
											else
												{	/* Jas/opcode.scm 172 */
												BgL_tagzd2275zd2_628:
													{	/* Jas/opcode.scm 218 */
														obj_t BgL_arg1877z00_1107;

														BgL_arg1877z00_1107 =
															MAKE_YOUNG_PAIR(bgl_list_ref(CNST_TABLE_REF(0),
																(long) CINT(BgL_copz00_8)), BgL_argsz00_9);
														return
															BGl_jaszd2errorzd2zzjas_classfilez00(
															((BgL_classfilez00_bglt) BgL_classfilez00_6),
															BGl_string1928z00zzjas_opcodez00,
															BgL_arg1877z00_1107);
													}
												}
										}
									else
										{	/* Jas/opcode.scm 172 */
											goto BgL_tagzd2275zd2_628;
										}
								}
								break;
							case 189L:
							case 192L:
							case 193L:

								{
									obj_t BgL_typez00_638;

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 176 */
											if (NULLP(CDR(((obj_t) BgL_argsz00_9))))
												{	/* Jas/opcode.scm 176 */
													obj_t BgL_arg1753z00_645;

													BgL_arg1753z00_645 = CAR(((obj_t) BgL_argsz00_9));
													BgL_typez00_638 = BgL_arg1753z00_645;
													{	/* Jas/opcode.scm 178 */
														BgL_jastypez00_bglt BgL_arg1755z00_647;

														BgL_arg1755z00_647 =
															BGl_aszd2typezd2zzjas_classfilez00(
															((BgL_classfilez00_bglt) BgL_classfilez00_6),
															BgL_typez00_638);
														{	/* Jas/opcode.scm 178 */
															obj_t BgL_list1756z00_648;

															{	/* Jas/opcode.scm 178 */
																obj_t BgL_arg1761z00_649;

																BgL_arg1761z00_649 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_arg1755z00_647), BNIL);
																BgL_list1756z00_648 =
																	MAKE_YOUNG_PAIR(BgL_copz00_8,
																	BgL_arg1761z00_649);
															}
															return BgL_list1756z00_648;
														}
													}
												}
											else
												{	/* Jas/opcode.scm 176 */
												BgL_tagzd2283zd2_640:
													{	/* Jas/opcode.scm 218 */
														obj_t BgL_arg1877z00_1112;

														BgL_arg1877z00_1112 =
															MAKE_YOUNG_PAIR(bgl_list_ref(CNST_TABLE_REF(0),
																(long) CINT(BgL_copz00_8)), BgL_argsz00_9);
														return
															BGl_jaszd2errorzd2zzjas_classfilez00(
															((BgL_classfilez00_bglt) BgL_classfilez00_6),
															BGl_string1928z00zzjas_opcodez00,
															BgL_arg1877z00_1112);
													}
												}
										}
									else
										{	/* Jas/opcode.scm 176 */
											goto BgL_tagzd2283zd2_640;
										}
								}
								break;
							case 196L:

								{
									obj_t BgL_wcopz00_650;
									obj_t BgL_wargsz00_651;

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 180 */
											obj_t BgL_carzd2297zd2_656;

											BgL_carzd2297zd2_656 = CAR(((obj_t) BgL_argsz00_9));
											if (PAIRP(BgL_carzd2297zd2_656))
												{	/* Jas/opcode.scm 180 */
													if (NULLP(CDR(((obj_t) BgL_argsz00_9))))
														{	/* Jas/opcode.scm 180 */
															BgL_wcopz00_650 = CAR(BgL_carzd2297zd2_656);
															BgL_wargsz00_651 = CDR(BgL_carzd2297zd2_656);
															{	/* Jas/opcode.scm 183 */
																obj_t BgL_ncopz00_663;

																if (SYMBOLP(BgL_wcopz00_650))
																	{	/* Jas/opcode.scm 68 */
																		BgL_ncopz00_663 =
																			BGl_getpropz00zz__r4_symbols_6_4z00
																			(BgL_wcopz00_650, CNST_TABLE_REF(1));
																	}
																else
																	{	/* Jas/opcode.scm 68 */
																		BgL_ncopz00_663 =
																			BGl_jaszd2errorzd2zzjas_classfilez00(
																			((BgL_classfilez00_bglt)
																				BgL_classfilez00_6),
																			BGl_string1926z00zzjas_opcodez00,
																			BgL_wcopz00_650);
																	}
																if (CBOOL(BgL_ncopz00_663))
																	{
																		obj_t BgL_argsz00_1811;
																		obj_t BgL_copz00_1810;

																		BgL_copz00_1810 = BgL_ncopz00_663;
																		BgL_argsz00_1811 = BgL_wargsz00_651;
																		BgL_argsz00_9 = BgL_argsz00_1811;
																		BgL_copz00_8 = BgL_copz00_1810;
																		goto BGl_resolvezd2argszd2zzjas_opcodez00;
																	}
																else
																	{	/* Jas/opcode.scm 184 */
																		return
																			BGl_jaszd2errorzd2zzjas_classfilez00(
																			((BgL_classfilez00_bglt)
																				BgL_classfilez00_6),
																			BGl_string1929z00zzjas_opcodez00,
																			BgL_wcopz00_650);
																	}
															}
														}
													else
														{	/* Jas/opcode.scm 180 */
														BgL_tagzd2291zd2_653:
															{	/* Jas/opcode.scm 218 */
																obj_t BgL_arg1877z00_1117;

																BgL_arg1877z00_1117 =
																	MAKE_YOUNG_PAIR(bgl_list_ref(CNST_TABLE_REF
																		(0), (long) CINT(BgL_copz00_8)),
																	BgL_argsz00_9);
																return
																	BGl_jaszd2errorzd2zzjas_classfilez00((
																		(BgL_classfilez00_bglt) BgL_classfilez00_6),
																	BGl_string1928z00zzjas_opcodez00,
																	BgL_arg1877z00_1117);
															}
														}
												}
											else
												{	/* Jas/opcode.scm 180 */
													goto BgL_tagzd2291zd2_653;
												}
										}
									else
										{	/* Jas/opcode.scm 180 */
											goto BgL_tagzd2291zd2_653;
										}
								}
								break;
							case 197L:

								{
									obj_t BgL_typez00_664;
									obj_t BgL_nz00_665;

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 188 */
											obj_t BgL_cdrzd2313zd2_670;

											BgL_cdrzd2313zd2_670 = CDR(((obj_t) BgL_argsz00_9));
											if (PAIRP(BgL_cdrzd2313zd2_670))
												{	/* Jas/opcode.scm 188 */
													obj_t BgL_carzd2316zd2_672;

													BgL_carzd2316zd2_672 = CAR(BgL_cdrzd2313zd2_670);
													if (INTEGERP(BgL_carzd2316zd2_672))
														{	/* Jas/opcode.scm 188 */
															if (NULLP(CDR(BgL_cdrzd2313zd2_670)))
																{	/* Jas/opcode.scm 188 */
																	obj_t BgL_arg1798z00_676;

																	BgL_arg1798z00_676 =
																		CAR(((obj_t) BgL_argsz00_9));
																	BgL_typez00_664 = BgL_arg1798z00_676;
																	BgL_nz00_665 = BgL_carzd2316zd2_672;
																	{	/* Jas/opcode.scm 190 */
																		BgL_jastypez00_bglt BgL_arg1805z00_678;

																		BgL_arg1805z00_678 =
																			BGl_aszd2typezd2zzjas_classfilez00(
																			((BgL_classfilez00_bglt)
																				BgL_classfilez00_6), BgL_typez00_664);
																		{	/* Jas/opcode.scm 190 */
																			obj_t BgL_list1806z00_679;

																			{	/* Jas/opcode.scm 190 */
																				obj_t BgL_arg1808z00_680;

																				{	/* Jas/opcode.scm 190 */
																					obj_t BgL_arg1812z00_681;

																					BgL_arg1812z00_681 =
																						MAKE_YOUNG_PAIR(BgL_nz00_665, BNIL);
																					BgL_arg1808z00_680 =
																						MAKE_YOUNG_PAIR(
																						((obj_t) BgL_arg1805z00_678),
																						BgL_arg1812z00_681);
																				}
																				BgL_list1806z00_679 =
																					MAKE_YOUNG_PAIR(BgL_copz00_8,
																					BgL_arg1808z00_680);
																			}
																			return BgL_list1806z00_679;
																		}
																	}
																}
															else
																{	/* Jas/opcode.scm 188 */
																BgL_tagzd2306zd2_667:
																	{	/* Jas/opcode.scm 218 */
																		obj_t BgL_arg1877z00_1124;

																		BgL_arg1877z00_1124 =
																			MAKE_YOUNG_PAIR(bgl_list_ref
																			(CNST_TABLE_REF(0),
																				(long) CINT(BgL_copz00_8)),
																			BgL_argsz00_9);
																		return
																			BGl_jaszd2errorzd2zzjas_classfilez00((
																				(BgL_classfilez00_bglt)
																				BgL_classfilez00_6),
																			BGl_string1928z00zzjas_opcodez00,
																			BgL_arg1877z00_1124);
																	}
																}
														}
													else
														{	/* Jas/opcode.scm 188 */
															goto BgL_tagzd2306zd2_667;
														}
												}
											else
												{	/* Jas/opcode.scm 188 */
													goto BgL_tagzd2306zd2_667;
												}
										}
									else
										{	/* Jas/opcode.scm 188 */
											goto BgL_tagzd2306zd2_667;
										}
								}
								break;
							case 202L:

								{
									obj_t BgL_begz00_682;
									obj_t BgL_endz00_683;
									obj_t BgL_labz00_684;
									obj_t BgL_typez00_685;

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 192 */
											obj_t BgL_cdrzd2331zd2_689;

											BgL_cdrzd2331zd2_689 = CDR(((obj_t) BgL_argsz00_9));
											if (PAIRP(BgL_cdrzd2331zd2_689))
												{	/* Jas/opcode.scm 192 */
													obj_t BgL_cdrzd2337zd2_691;

													BgL_cdrzd2337zd2_691 = CDR(BgL_cdrzd2331zd2_689);
													if (PAIRP(BgL_cdrzd2337zd2_691))
														{	/* Jas/opcode.scm 192 */
															obj_t BgL_cdrzd2342zd2_693;

															BgL_cdrzd2342zd2_693 = CDR(BgL_cdrzd2337zd2_691);
															if (PAIRP(BgL_cdrzd2342zd2_693))
																{	/* Jas/opcode.scm 192 */
																	if (NULLP(CDR(BgL_cdrzd2342zd2_693)))
																		{	/* Jas/opcode.scm 192 */
																			obj_t BgL_arg1822z00_697;
																			obj_t BgL_arg1823z00_698;
																			obj_t BgL_arg1831z00_699;
																			obj_t BgL_arg1832z00_700;

																			BgL_arg1822z00_697 =
																				CAR(((obj_t) BgL_argsz00_9));
																			BgL_arg1823z00_698 =
																				CAR(BgL_cdrzd2331zd2_689);
																			BgL_arg1831z00_699 =
																				CAR(BgL_cdrzd2337zd2_691);
																			BgL_arg1832z00_700 =
																				CAR(BgL_cdrzd2342zd2_693);
																			BgL_begz00_682 = BgL_arg1822z00_697;
																			BgL_endz00_683 = BgL_arg1823z00_698;
																			BgL_labz00_684 = BgL_arg1831z00_699;
																			BgL_typez00_685 = BgL_arg1832z00_700;
																			{	/* Jas/opcode.scm 196 */
																				long BgL_arg1834z00_702;

																				if ((BgL_typez00_685 == BINT(0L)))
																					{	/* Jas/opcode.scm 196 */
																						BgL_arg1834z00_702 = 0L;
																					}
																				else
																					{	/* Jas/opcode.scm 196 */
																						BgL_arg1834z00_702 =
																							(long)
																							(BGl_poolzd2classzd2byzd2namezd2zzjas_classfilez00
																							(((BgL_classfilez00_bglt)
																									BgL_classfilez00_6),
																								BgL_typez00_685));
																					}
																				{	/* Jas/opcode.scm 195 */
																					obj_t BgL_list1835z00_703;

																					{	/* Jas/opcode.scm 195 */
																						obj_t BgL_arg1836z00_704;

																						{	/* Jas/opcode.scm 195 */
																							obj_t BgL_arg1837z00_705;

																							{	/* Jas/opcode.scm 195 */
																								obj_t BgL_arg1838z00_706;

																								{	/* Jas/opcode.scm 195 */
																									obj_t BgL_arg1839z00_707;

																									BgL_arg1839z00_707 =
																										MAKE_YOUNG_PAIR(BINT
																										(BgL_arg1834z00_702), BNIL);
																									BgL_arg1838z00_706 =
																										MAKE_YOUNG_PAIR
																										(BgL_labz00_684,
																										BgL_arg1839z00_707);
																								}
																								BgL_arg1837z00_705 =
																									MAKE_YOUNG_PAIR
																									(BgL_endz00_683,
																									BgL_arg1838z00_706);
																							}
																							BgL_arg1836z00_704 =
																								MAKE_YOUNG_PAIR(BgL_begz00_682,
																								BgL_arg1837z00_705);
																						}
																						BgL_list1835z00_703 =
																							MAKE_YOUNG_PAIR(BgL_copz00_8,
																							BgL_arg1836z00_704);
																					}
																					return BgL_list1835z00_703;
																				}
																			}
																		}
																	else
																		{	/* Jas/opcode.scm 192 */
																			return BFALSE;
																		}
																}
															else
																{	/* Jas/opcode.scm 192 */
																	return BFALSE;
																}
														}
													else
														{	/* Jas/opcode.scm 192 */
															return BFALSE;
														}
												}
											else
												{	/* Jas/opcode.scm 192 */
													return BFALSE;
												}
										}
									else
										{	/* Jas/opcode.scm 192 */
											return BFALSE;
										}
								}
								break;
							case 203L:

								{

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 197 */
											obj_t BgL_cdrzd2352zd2_712;

											BgL_cdrzd2352zd2_712 = CDR(((obj_t) BgL_argsz00_9));
											if (INTEGERP(CAR(((obj_t) BgL_argsz00_9))))
												{	/* Jas/opcode.scm 197 */
													if (PAIRP(BgL_cdrzd2352zd2_712))
														{	/* Jas/opcode.scm 197 */
															if (INTEGERP(CAR(BgL_cdrzd2352zd2_712)))
																{	/* Jas/opcode.scm 197 */
																	if (NULLP(CDR(BgL_cdrzd2352zd2_712)))
																		{	/* Jas/opcode.scm 197 */
																			return
																				MAKE_YOUNG_PAIR(BgL_copz00_8,
																				BgL_argsz00_9);
																		}
																	else
																		{	/* Jas/opcode.scm 197 */
																		BgL_tagzd2349zd2_709:
																			{	/* Jas/opcode.scm 218 */
																				obj_t BgL_arg1877z00_1139;

																				BgL_arg1877z00_1139 =
																					MAKE_YOUNG_PAIR(bgl_list_ref
																					(CNST_TABLE_REF(0),
																						(long) CINT(BgL_copz00_8)),
																					BgL_argsz00_9);
																				return
																					BGl_jaszd2errorzd2zzjas_classfilez00((
																						(BgL_classfilez00_bglt)
																						BgL_classfilez00_6),
																					BGl_string1928z00zzjas_opcodez00,
																					BgL_arg1877z00_1139);
																			}
																		}
																}
															else
																{	/* Jas/opcode.scm 197 */
																	goto BgL_tagzd2349zd2_709;
																}
														}
													else
														{	/* Jas/opcode.scm 197 */
															goto BgL_tagzd2349zd2_709;
														}
												}
											else
												{	/* Jas/opcode.scm 197 */
													goto BgL_tagzd2349zd2_709;
												}
										}
									else
										{	/* Jas/opcode.scm 197 */
											goto BgL_tagzd2349zd2_709;
										}
								}
								break;
							case 204L:

								return MAKE_YOUNG_PAIR(BgL_copz00_8, BgL_argsz00_9);
								break;
							case 205L:

								{
									obj_t BgL_begz00_723;
									obj_t BgL_endz00_724;
									obj_t BgL_namez00_725;
									obj_t BgL_typez00_726;
									obj_t BgL_varz00_727;

									if (PAIRP(BgL_argsz00_9))
										{	/* Jas/opcode.scm 203 */
											obj_t BgL_carzd2368zd2_732;
											obj_t BgL_cdrzd2369zd2_733;

											BgL_carzd2368zd2_732 = CAR(((obj_t) BgL_argsz00_9));
											BgL_cdrzd2369zd2_733 = CDR(((obj_t) BgL_argsz00_9));
											if (SYMBOLP(BgL_carzd2368zd2_732))
												{	/* Jas/opcode.scm 203 */
													if (PAIRP(BgL_cdrzd2369zd2_733))
														{	/* Jas/opcode.scm 203 */
															obj_t BgL_carzd2376zd2_736;
															obj_t BgL_cdrzd2377zd2_737;

															BgL_carzd2376zd2_736 = CAR(BgL_cdrzd2369zd2_733);
															BgL_cdrzd2377zd2_737 = CDR(BgL_cdrzd2369zd2_733);
															if (SYMBOLP(BgL_carzd2376zd2_736))
																{	/* Jas/opcode.scm 203 */
																	if (PAIRP(BgL_cdrzd2377zd2_737))
																		{	/* Jas/opcode.scm 203 */
																			obj_t BgL_carzd2383zd2_740;
																			obj_t BgL_cdrzd2384zd2_741;

																			BgL_carzd2383zd2_740 =
																				CAR(BgL_cdrzd2377zd2_737);
																			BgL_cdrzd2384zd2_741 =
																				CDR(BgL_cdrzd2377zd2_737);
																			if (STRINGP(BgL_carzd2383zd2_740))
																				{	/* Jas/opcode.scm 203 */
																					if (PAIRP(BgL_cdrzd2384zd2_741))
																						{	/* Jas/opcode.scm 203 */
																							obj_t BgL_cdrzd2390zd2_744;

																							BgL_cdrzd2390zd2_744 =
																								CDR(BgL_cdrzd2384zd2_741);
																							if (PAIRP(BgL_cdrzd2390zd2_744))
																								{	/* Jas/opcode.scm 203 */
																									obj_t BgL_carzd2393zd2_746;

																									BgL_carzd2393zd2_746 =
																										CAR(BgL_cdrzd2390zd2_744);
																									if (SYMBOLP
																										(BgL_carzd2393zd2_746))
																										{	/* Jas/opcode.scm 203 */
																											if (NULLP(CDR
																													(BgL_cdrzd2390zd2_744)))
																												{	/* Jas/opcode.scm 203 */
																													BgL_begz00_723 =
																														BgL_carzd2368zd2_732;
																													BgL_endz00_724 =
																														BgL_carzd2376zd2_736;
																													BgL_namez00_725 =
																														BgL_carzd2383zd2_740;
																													BgL_typez00_726 =
																														CAR
																														(BgL_cdrzd2384zd2_741);
																													BgL_varz00_727 =
																														BgL_carzd2393zd2_746;
																													{	/* Jas/opcode.scm 211 */
																														BgL_jastypez00_bglt
																															BgL_arg1868z00_752;
																														obj_t
																															BgL_arg1869z00_753;
																														BgL_arg1868z00_752 =
																															BGl_aszd2typezd2zzjas_classfilez00
																															(((BgL_classfilez00_bglt) BgL_classfilez00_6), BgL_typez00_726);
																														BgL_arg1869z00_753 =
																															BGl_vzd2indexzd2zzjas_opcodez00
																															(BgL_classfilez00_6,
																															BgL_varz00_727,
																															BgL_localsz00_7);
																														{	/* Jas/opcode.scm 210 */
																															obj_t
																																BgL_list1870z00_754;
																															{	/* Jas/opcode.scm 210 */
																																obj_t
																																	BgL_arg1872z00_755;
																																{	/* Jas/opcode.scm 210 */
																																	obj_t
																																		BgL_arg1873z00_756;
																																	{	/* Jas/opcode.scm 210 */
																																		obj_t
																																			BgL_arg1874z00_757;
																																		{	/* Jas/opcode.scm 210 */
																																			obj_t
																																				BgL_arg1875z00_758;
																																			{	/* Jas/opcode.scm 210 */
																																				obj_t
																																					BgL_arg1876z00_759;
																																				BgL_arg1876z00_759
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1869z00_753,
																																					BNIL);
																																				BgL_arg1875z00_758
																																					=
																																					MAKE_YOUNG_PAIR
																																					(((obj_t) BgL_arg1868z00_752), BgL_arg1876z00_759);
																																			}
																																			BgL_arg1874z00_757
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_namez00_725,
																																				BgL_arg1875z00_758);
																																		}
																																		BgL_arg1873z00_756
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_endz00_724,
																																			BgL_arg1874z00_757);
																																	}
																																	BgL_arg1872z00_755
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_begz00_723,
																																		BgL_arg1873z00_756);
																																}
																																BgL_list1870z00_754
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_copz00_8,
																																	BgL_arg1872z00_755);
																															}
																															return
																																BgL_list1870z00_754;
																														}
																													}
																												}
																											else
																												{	/* Jas/opcode.scm 203 */
																												BgL_tagzd2356zd2_729:
																													{	/* Jas/opcode.scm 218 */
																														obj_t
																															BgL_arg1877z00_1146;
																														BgL_arg1877z00_1146
																															=
																															MAKE_YOUNG_PAIR
																															(bgl_list_ref
																															(CNST_TABLE_REF
																																(0),
																																(long)
																																CINT
																																(BgL_copz00_8)),
																															BgL_argsz00_9);
																														return
																															BGl_jaszd2errorzd2zzjas_classfilez00
																															(((BgL_classfilez00_bglt) BgL_classfilez00_6), BGl_string1928z00zzjas_opcodez00, BgL_arg1877z00_1146);
																													}
																												}
																										}
																									else
																										{	/* Jas/opcode.scm 203 */
																											goto BgL_tagzd2356zd2_729;
																										}
																								}
																							else
																								{	/* Jas/opcode.scm 203 */
																									goto BgL_tagzd2356zd2_729;
																								}
																						}
																					else
																						{	/* Jas/opcode.scm 203 */
																							goto BgL_tagzd2356zd2_729;
																						}
																				}
																			else
																				{	/* Jas/opcode.scm 203 */
																					goto BgL_tagzd2356zd2_729;
																				}
																		}
																	else
																		{	/* Jas/opcode.scm 203 */
																			goto BgL_tagzd2356zd2_729;
																		}
																}
															else
																{	/* Jas/opcode.scm 203 */
																	goto BgL_tagzd2356zd2_729;
																}
														}
													else
														{	/* Jas/opcode.scm 203 */
															goto BgL_tagzd2356zd2_729;
														}
												}
											else
												{	/* Jas/opcode.scm 203 */
													goto BgL_tagzd2356zd2_729;
												}
										}
									else
										{	/* Jas/opcode.scm 203 */
											goto BgL_tagzd2356zd2_729;
										}
								}
								break;
							default:
							BgL_case_else1057z00_321:
								return
									BGl_jaszd2errorzd2zzjas_classfilez00(
									((BgL_classfilez00_bglt) BgL_classfilez00_6),
									BGl_string1927z00zzjas_opcodez00, BgL_copz00_8);
							}
					}
				else
					{	/* Jas/opcode.scm 88 */
						goto BgL_case_else1057z00_321;
					}
			}
		}

	}



/* slot? */
	bool_t BGl_slotzf3zf3zzjas_opcodez00(obj_t BgL_slotz00_13)
	{
		{	/* Jas/opcode.scm 221 */
			if (PAIRP(BgL_slotz00_13))
				{	/* Jas/opcode.scm 222 */
					if (INTEGERP(CAR(BgL_slotz00_13)))
						{	/* Jas/opcode.scm 222 */
							obj_t BgL_tmpz00_1961;

							BgL_tmpz00_1961 = CDR(BgL_slotz00_13);
							return SYMBOLP(BgL_tmpz00_1961);
						}
					else
						{	/* Jas/opcode.scm 222 */
							return ((bool_t) 0);
						}
				}
			else
				{	/* Jas/opcode.scm 222 */
					return ((bool_t) 0);
				}
		}

	}



/* get-type-num */
	obj_t BGl_getzd2typezd2numz00zzjas_opcodez00(obj_t BgL_classfilez00_14,
		obj_t BgL_typez00_15)
	{
		{	/* Jas/opcode.scm 225 */
			{	/* Jas/opcode.scm 226 */
				obj_t BgL_slotz00_766;

				BgL_slotz00_766 =
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_typez00_15,
					CNST_TABLE_REF(2));
				if (CBOOL(BgL_slotz00_766))
					{	/* Jas/opcode.scm 229 */
						return CDR(((obj_t) BgL_slotz00_766));
					}
				else
					{	/* Jas/opcode.scm 231 */
						obj_t BgL_arg1882z00_767;

						{	/* Jas/opcode.scm 231 */
							obj_t BgL_list1883z00_768;

							BgL_list1883z00_768 = MAKE_YOUNG_PAIR(BgL_typez00_15, BNIL);
							BgL_arg1882z00_767 = BgL_list1883z00_768;
						}
						{	/* Jas/opcode.scm 218 */
							obj_t BgL_arg1877z00_1164;

							BgL_arg1877z00_1164 =
								MAKE_YOUNG_PAIR(bgl_list_ref(CNST_TABLE_REF(0), 188L),
								BgL_arg1882z00_767);
							return
								BGl_jaszd2errorzd2zzjas_classfilez00(((BgL_classfilez00_bglt)
									BgL_classfilez00_14), BGl_string1928z00zzjas_opcodez00,
								BgL_arg1877z00_1164);
						}
					}
			}
		}

	}



/* v-index */
	obj_t BGl_vzd2indexzd2zzjas_opcodez00(obj_t BgL_classfilez00_16,
		obj_t BgL_varz00_17, obj_t BgL_localsz00_18)
	{
		{	/* Jas/opcode.scm 234 */
			{
				obj_t BgL_lz00_770;
				long BgL_nz00_771;

				BgL_lz00_770 = BgL_localsz00_18;
				BgL_nz00_771 = 0L;
			BgL_zc3z04anonymousza31884ze3z87_772:
				if (NULLP(BgL_lz00_770))
					{	/* Jas/opcode.scm 237 */
						return
							BGl_jaszd2errorzd2zzjas_classfilez00(
							((BgL_classfilez00_bglt) BgL_classfilez00_16),
							BGl_string1930z00zzjas_opcodez00, BgL_varz00_17);
					}
				else
					{	/* Jas/opcode.scm 237 */
						if ((CAR(((obj_t) BgL_lz00_770)) == BgL_varz00_17))
							{	/* Jas/opcode.scm 238 */
								return BINT(BgL_nz00_771);
							}
						else
							{	/* Jas/opcode.scm 239 */
								obj_t BgL_arg1889z00_776;
								long BgL_arg1890z00_777;

								BgL_arg1889z00_776 = CDR(((obj_t) BgL_lz00_770));
								BgL_arg1890z00_777 = (BgL_nz00_771 + 1L);
								{
									long BgL_nz00_1989;
									obj_t BgL_lz00_1988;

									BgL_lz00_1988 = BgL_arg1889z00_776;
									BgL_nz00_1989 = BgL_arg1890z00_777;
									BgL_nz00_771 = BgL_nz00_1989;
									BgL_lz00_770 = BgL_lz00_1988;
									goto BgL_zc3z04anonymousza31884ze3z87_772;
								}
							}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzjas_opcodez00(void)
	{
		{	/* Jas/opcode.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzjas_opcodez00(void)
	{
		{	/* Jas/opcode.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzjas_opcodez00(void)
	{
		{	/* Jas/opcode.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzjas_opcodez00(void)
	{
		{	/* Jas/opcode.scm 1 */
			BGl_modulezd2initializa7ationz75zzjas_libz00(267157509L,
				BSTRING_TO_STRING(BGl_string1931z00zzjas_opcodez00));
			return
				BGl_modulezd2initializa7ationz75zzjas_classfilez00(135094074L,
				BSTRING_TO_STRING(BGl_string1931z00zzjas_opcodez00));
		}

	}

#ifdef __cplusplus
}
#endif
