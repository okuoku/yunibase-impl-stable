/*===========================================================================*/
/*   (Jas/as.scm)                                                            */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Jas/as.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_JAS_AS_TYPE_DEFINITIONS
#define BGL_JAS_AS_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_jastypez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_codez00;
		obj_t BgL_vectz00;
	}                 *BgL_jastypez00_bglt;

	typedef struct BgL_classez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_codez00;
		obj_t BgL_vectz00;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_poolz00;
	}                *BgL_classez00_bglt;

	typedef struct BgL_fieldzd2orzd2methodz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}                             *BgL_fieldzd2orzd2methodz00_bglt;

	typedef struct BgL_fieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}               *BgL_fieldz00_bglt;

	typedef struct BgL_methodz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_flagsz00;
		obj_t BgL_namez00;
		obj_t BgL_ownerz00;
		obj_t BgL_usertypez00;
		obj_t BgL_typez00;
		obj_t BgL_pnamez00;
		obj_t BgL_descriptorz00;
		obj_t BgL_poolz00;
		obj_t BgL_attributesz00;
	}                *BgL_methodz00_bglt;

	typedef struct BgL_attributez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_typez00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_infoz00;
	}                   *BgL_attributez00_bglt;

	typedef struct BgL_classfilez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_currentzd2methodzd2;
		obj_t BgL_globalsz00;
		obj_t BgL_poolz00;
		obj_t BgL_poolzd2siza7ez75;
		obj_t BgL_pooledzd2nameszd2;
		obj_t BgL_flagsz00;
		obj_t BgL_mez00;
		obj_t BgL_superz00;
		obj_t BgL_interfacesz00;
		obj_t BgL_fieldsz00;
		obj_t BgL_methodsz00;
		obj_t BgL_attributesz00;
	}                   *BgL_classfilez00_bglt;


#endif													// BGL_JAS_AS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern BgL_fieldz00_bglt
		BGl_declaredzd2fieldzd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t);
	static obj_t BGl_getzd2localvarszd2zzjas_asz00(obj_t);
	BGL_IMPORT obj_t BGl_2minz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_za2za2sdefullfileza2za2z00zzjas_asz00 = BUNSPEC;
	static bool_t
		BGl_setzd2fieldzd2methodzd2typezd2zzjas_asz00(BgL_classfilez00_bglt);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_scanzd2infoszd2zzjas_asz00(BgL_classfilez00_bglt, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzjas_asz00 = BUNSPEC;
	extern obj_t BGl_attributez00zzjas_classfilez00;
	BGL_IMPORT obj_t BGl_raisez00zz__errorz00(obj_t);
	BGL_IMPORT bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	static BgL_attributez00_bglt
		BGl_aszd2codezd2zzjas_asz00(BgL_classfilez00_bglt, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32030ze3ze5zzjas_asz00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzjas_asz00(void);
	BGL_EXPORTED_DECL obj_t BGl_jvmzd2aszd2zzjas_asz00(obj_t, obj_t);
	static obj_t BGl_za2za2sdefileza2za2z00zzjas_asz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static BgL_attributez00_bglt
		BGl_makezd2linezd2attributez00zzjas_asz00(BgL_classfilez00_bglt, obj_t);
	extern obj_t BGl_resolvezd2widezd2zzjas_widez00;
	static obj_t BGl_genericzd2initzd2zzjas_asz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzjas_asz00(void);
	BGL_IMPORT obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_bignum_mul(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32106ze3ze5zzjas_asz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_jaszd2profilezd2zzjas_profilez00(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_peepz00zzjas_peepz00;
	static obj_t BGl_z62zc3z04anonymousza32034ze3ze5zzjas_asz00(obj_t, obj_t);
	static long BGl_aszd2fieldzd2modifiersz00zzjas_asz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2jaszd2warningza2zd2zzjas_asz00 = BUNSPEC;
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00(obj_t);
	static obj_t BGl_aszd2declarezd2zzjas_asz00(BgL_classfilez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_linezd2compresszd2zzjas_asz00(obj_t, obj_t);
	extern obj_t BGl_methodz00zzjas_classfilez00;
	BGL_IMPORT obj_t BGl_rempropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t);
	static BgL_attributez00_bglt BGl_srcfilez00zzjas_asz00(BgL_classfilez00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzjas_asz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzjas_asz00(void);
	static obj_t BGl_z62zc3z04anonymousza31332ze3ze5zzjas_asz00(obj_t);
	static obj_t BGl_infoze70ze7zzjas_asz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_getzd2bytecodezd2zzjas_asz00(obj_t);
	extern obj_t
		BGl_poolzd2localzd2methodz00zzjas_classfilez00(BgL_classfilez00_bglt,
		BgL_methodz00_bglt);
	BGL_IMPORT obj_t make_string(long, unsigned char);
	static obj_t BGl_za2za2allzd2linesza2za2zd2zzjas_asz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_jvmzd2asfilezd2zzjas_asz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	BGL_IMPORT obj_t close_binary_port(obj_t);
	extern obj_t BGl_za2za2lastzd2numberzd2ofzd2localsza2za2zd2zzjas_peepz00;
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	static obj_t BGl_z62jvmzd2aszb0zzjas_asz00(obj_t, obj_t, obj_t);
	BGL_IMPORT bool_t bigloo_strncmp(obj_t, obj_t, long);
	extern obj_t BGl_aszd2assignzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62jvmzd2asfilezb0zzjas_asz00(obj_t, obj_t, obj_t);
	extern int
		BGl_poolzd2classzd2byzd2namezd2zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t);
	extern obj_t BGl_resolvezd2labelszd2zzjas_labelsz00;
	extern BgL_methodz00_bglt
		BGl_declaredzd2methodzd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t);
	static obj_t BGl_getzd2lineszd2zzjas_asz00(obj_t, long);
	extern obj_t BGl_producez00zzjas_producez00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_remq_bang(obj_t, obj_t);
	static obj_t BGl_smapfilez00zzjas_asz00(void);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzjas_asz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_stackz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_labelsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_widez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_peepz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_opcodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_profilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_producez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_classfilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_libz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__binaryz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	static long BGl_za2za2maxposza2za2z00zzjas_asz00 = 0L;
	BGL_IMPORT obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	extern obj_t BGl_classez00zzjas_classfilez00;
	static BgL_attributez00_bglt
		BGl_makezd2localvarszd2zzjas_asz00(BgL_classfilez00_bglt, obj_t);
	static obj_t BGl_cnstzd2initzd2zzjas_asz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzjas_asz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static bool_t BGl_za2profileza2z00zzjas_asz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzjas_asz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzjas_asz00(void);
	static obj_t BGl_za2za2maxlineza2za2z00zzjas_asz00 = BUNSPEC;
	static obj_t BGl_reorderzd2classfilezd2zzjas_asz00(BgL_classfilez00_bglt,
		obj_t);
	extern BgL_jastypez00_bglt
		BGl_aszd2typezd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t);
	static obj_t BGl_za2za2maxcharza2za2z00zzjas_asz00 = BUNSPEC;
	extern obj_t BGl_jaszd2errorzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_2maxz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern int BGl_poolzd2classzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		BgL_classez00_bglt);
	static obj_t BGl_getzd2handlerszd2zzjas_asz00(obj_t);
	BGL_IMPORT obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	extern obj_t BGl_stkzd2analysiszd2zzjas_stackz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
	static long BGl_aszd2classzd2modifiersz00zzjas_asz00(obj_t);
	static obj_t BGl_asz00zzjas_asz00(obj_t);
	extern obj_t BGl_fieldz00zzjas_classfilez00;
	BGL_IMPORT obj_t c_substring(obj_t, long, long);
	extern obj_t BGl_resolvezd2opcodeszd2zzjas_opcodez00;
	extern obj_t BGl_fieldzd2orzd2methodz00zzjas_classfilez00;
	static obj_t BGl_zc3z04exitza31329ze3ze70z60zzjas_asz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_getzd2procedurezd2codeze70ze7zzjas_asz00(BgL_classfilez00_bglt, obj_t,
		obj_t);
	static obj_t BGl_za2za2poszd2ze3lineza2za2z31zzjas_asz00 = BUNSPEC;
	BGL_IMPORT obj_t
		BGl_callzd2withzd2inputzd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static long BGl_aszd2methodzd2modifiersz00zzjas_asz00(obj_t);
	static obj_t BGl_aszd2methodzd2zzjas_asz00(BgL_classfilez00_bglt, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2jvmzd2charzd2infoza2z00zzjas_asz00 = BUNSPEC;
	extern int BGl_poolzd2fieldzd2zzjas_classfilez00(BgL_classfilez00_bglt,
		BgL_fieldz00_bglt);
	extern BgL_classez00_bglt
		BGl_declaredzd2classzd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t);
	static obj_t BGl_poszd2infozd2ze3linezd2infoz12z23zzjas_asz00(void);
	extern int BGl_poolzd2namezd2zzjas_classfilez00(BgL_classfilez00_bglt, obj_t);
	extern obj_t BGl_stringzd2ze3utf8z31zzjas_libz00(obj_t);
	static obj_t BGl_pathnamez00zzjas_asz00(obj_t);
	static BgL_attributez00_bglt BGl_smapz00zzjas_asz00(BgL_classfilez00_bglt);
	extern obj_t BGl_classfilez00zzjas_classfilez00;
	static obj_t __cnst[29];


	   
		 
		DEFINE_STRING(BGl_string2200z00zzjas_asz00,
		BgL_bgl_string2200za700za7za7j2226za7, "1#1,", 4);
	      DEFINE_STRING(BGl_string2201z00zzjas_asz00,
		BgL_bgl_string2201za700za7za7j2227za7, "*S ", 3);
	      DEFINE_STRING(BGl_string2202z00zzjas_asz00,
		BgL_bgl_string2202za700za7za7j2228za7, "SMAP\n", 5);
	      DEFINE_STRING(BGl_string2203z00zzjas_asz00,
		BgL_bgl_string2203za700za7za7j2229za7, "L", 1);
	      DEFINE_STRING(BGl_string2204z00zzjas_asz00,
		BgL_bgl_string2204za700za7za7j2230za7, ";", 1);
	      DEFINE_STRING(BGl_string2205z00zzjas_asz00,
		BgL_bgl_string2205za700za7za7j2231za7, "bad declaration", 15);
	      DEFINE_STRING(BGl_string2206z00zzjas_asz00,
		BgL_bgl_string2206za700za7za7j2232za7, "as", 2);
	      DEFINE_STRING(BGl_string2207z00zzjas_asz00,
		BgL_bgl_string2207za700za7za7j2233za7, "bad method modifier", 19);
	      DEFINE_STRING(BGl_string2208z00zzjas_asz00,
		BgL_bgl_string2208za700za7za7j2234za7, "bad field modifier", 18);
	      DEFINE_STRING(BGl_string2209z00zzjas_asz00,
		BgL_bgl_string2209za700za7za7j2235za7, "bad method definition", 21);
	      DEFINE_STRING(BGl_string2210z00zzjas_asz00,
		BgL_bgl_string2210za700za7za7j2236za7,
		"You should consider splitting this function in small pieces.", 60);
	      DEFINE_STRING(BGl_string2211z00zzjas_asz00,
		BgL_bgl_string2211za700za7za7j2237za7, ", limit size: 8000).\n", 21);
	      DEFINE_STRING(BGl_string2212z00zzjas_asz00,
		BgL_bgl_string2212za700za7za7j2238za7,
		"Method too large. This may cause some troubles to Jvm jits (current size: ",
		74);
	      DEFINE_STRING(BGl_string2213z00zzjas_asz00,
		BgL_bgl_string2213za700za7za7j2239za7, "Code", 4);
	      DEFINE_STRING(BGl_string2214z00zzjas_asz00,
		BgL_bgl_string2214za700za7za7j2240za7, "LineNumberTable", 15);
	      DEFINE_STRING(BGl_string2217z00zzjas_asz00,
		BgL_bgl_string2217za700za7za7j2241za7, "LocalVariableTable", 18);
	      DEFINE_STRING(BGl_string2218z00zzjas_asz00,
		BgL_bgl_string2218za700za7za7j2242za7,
		"You should consider splitting this source file in small pieces.", 63);
	      DEFINE_STRING(BGl_string2219z00zzjas_asz00,
		BgL_bgl_string2219za700za7za7j2243za7, ", limit size: 65535).\n", 22);
	      DEFINE_STRING(BGl_string2220z00zzjas_asz00,
		BgL_bgl_string2220za700za7za7j2244za7,
		"Source file too large for generating character information (current size: ",
		74);
	      DEFINE_STRING(BGl_string2221z00zzjas_asz00,
		BgL_bgl_string2221za700za7za7j2245za7, "*jvm-char-info*", 15);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2215z00zzjas_asz00,
		BgL_bgl_za762za7c3za704anonymo2246za7,
		BGl_z62zc3z04anonymousza32030ze3ze5zzjas_asz00);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2216z00zzjas_asz00,
		BgL_bgl_za762za7c3za704anonymo2247za7,
		BGl_z62zc3z04anonymousza32034ze3ze5zzjas_asz00);
	      DEFINE_STRING(BGl_string2223z00zzjas_asz00,
		BgL_bgl_string2223za700za7za7j2248za7, "jas_as", 6);
	      DEFINE_STRING(BGl_string2224z00zzjas_asz00,
		BgL_bgl_string2224za700za7za7j2249za7,
		"localvariable linenumber code native synchronized transient volatile static protected private abstract interface super final public field class function bytevector srcfile sde sourcefile fields declare as method list_to_vector invokestatic jas-global-value ",
		257);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2222z00zzjas_asz00,
		BgL_bgl_za762za7c3za704anonymo2250za7,
		BGl_z62zc3z04anonymousza32106ze3ze5zzjas_asz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_jvmzd2asfilezd2envz00zzjas_asz00,
		BgL_bgl_za762jvmza7d2asfileza72251za7, BGl_z62jvmzd2asfilezb0zzjas_asz00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2180z00zzjas_asz00,
		BgL_bgl_string2180za700za7za7j2252za7, "Jas", 3);
	      DEFINE_STRING(BGl_string2181z00zzjas_asz00,
		BgL_bgl_string2181za700za7za7j2253za7, "Can't open file for output", 26);
	      DEFINE_STRING(BGl_string2182z00zzjas_asz00,
		BgL_bgl_string2182za700za7za7j2254za7, "_", 1);
	      DEFINE_STRING(BGl_string2183z00zzjas_asz00,
		BgL_bgl_string2183za700za7za7j2255za7, "BgL__", 5);
	      DEFINE_STRING(BGl_string2184z00zzjas_asz00,
		BgL_bgl_string2184za700za7za7j2256za7, "apply", 5);
	      DEFINE_STRING(BGl_string2185z00zzjas_asz00,
		BgL_bgl_string2185za700za7za7j2257za7, "jas", 3);
	      DEFINE_STRING(BGl_string2186z00zzjas_asz00,
		BgL_bgl_string2186za700za7za7j2258za7, "bad module definition", 21);
	      DEFINE_STRING(BGl_string2187z00zzjas_asz00,
		BgL_bgl_string2187za700za7za7j2259za7, "SourceFile", 10);
	      DEFINE_STRING(BGl_string2188z00zzjas_asz00,
		BgL_bgl_string2188za700za7za7j2260za7, "SourceDebugExtension", 20);
	      DEFINE_STRING(BGl_string2189z00zzjas_asz00,
		BgL_bgl_string2189za700za7za7j2261za7, "BiglooChar", 10);
	      DEFINE_STRING(BGl_string2190z00zzjas_asz00,
		BgL_bgl_string2190za700za7za7j2262za7, "Bigloo", 6);
	      DEFINE_STRING(BGl_string2191z00zzjas_asz00,
		BgL_bgl_string2191za700za7za7j2263za7, "~a#1,1:~a,~a\n", 13);
	      DEFINE_STRING(BGl_string2192z00zzjas_asz00,
		BgL_bgl_string2192za700za7za7j2264za7, "*L\n", 3);
	      DEFINE_STRING(BGl_string2193z00zzjas_asz00,
		BgL_bgl_string2193za700za7za7j2265za7, "\n", 1);
	      DEFINE_STRING(BGl_string2194z00zzjas_asz00,
		BgL_bgl_string2194za700za7za7j2266za7, "+ 1 ", 4);
	      DEFINE_STRING(BGl_string2195z00zzjas_asz00,
		BgL_bgl_string2195za700za7za7j2267za7, "*F\n", 3);
	      DEFINE_STRING(BGl_string2196z00zzjas_asz00,
		BgL_bgl_string2196za700za7za7j2268za7, "*S Bigloo\n", 10);
	      DEFINE_STRING(BGl_string2197z00zzjas_asz00,
		BgL_bgl_string2197za700za7za7j2269za7, "", 0);
	      DEFINE_STRING(BGl_string2198z00zzjas_asz00,
		BgL_bgl_string2198za700za7za7j2270za7, "*E\n", 3);
	      DEFINE_STRING(BGl_string2199z00zzjas_asz00,
		BgL_bgl_string2199za700za7za7j2271za7, ":1\n", 3);
	BGL_IMPORT obj_t BGl_readzd2envzd2zz__readerz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jvmzd2aszd2envz00zzjas_asz00,
		BgL_bgl_za762jvmza7d2asza7b0za7za72272za7, BGl_z62jvmzd2aszb0zzjas_asz00,
		0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2za2sdefullfileza2za2z00zzjas_asz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzjas_asz00));
		     ADD_ROOT((void *) (&BGl_za2za2sdefileza2za2z00zzjas_asz00));
		     ADD_ROOT((void *) (&BGl_za2jaszd2warningza2zd2zzjas_asz00));
		     ADD_ROOT((void *) (&BGl_za2za2allzd2linesza2za2zd2zzjas_asz00));
		     ADD_ROOT((void *) (&BGl_za2za2maxlineza2za2z00zzjas_asz00));
		     ADD_ROOT((void *) (&BGl_za2za2maxcharza2za2z00zzjas_asz00));
		     ADD_ROOT((void *) (&BGl_za2za2poszd2ze3lineza2za2z31zzjas_asz00));
		     ADD_ROOT((void *) (&BGl_za2jvmzd2charzd2infoza2z00zzjas_asz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzjas_asz00(long
		BgL_checksumz00_2078, char *BgL_fromz00_2079)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzjas_asz00))
				{
					BGl_requirezd2initializa7ationz75zzjas_asz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzjas_asz00();
					BGl_libraryzd2moduleszd2initz00zzjas_asz00();
					BGl_cnstzd2initzd2zzjas_asz00();
					BGl_importedzd2moduleszd2initz00zzjas_asz00();
					return BGl_toplevelzd2initzd2zzjas_asz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzjas_asz00(void)
	{
		{	/* Jas/as.scm 1 */
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "jas_as");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"jas_as");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "jas_as");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "jas_as");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "jas_as");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "jas_as");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "jas_as");
			BGl_modulezd2initializa7ationz75zz__binaryz00(0L, "jas_as");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "jas_as");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "jas_as");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "jas_as");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "jas_as");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"jas_as");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "jas_as");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L, "jas_as");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "jas_as");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "jas_as");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "jas_as");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzjas_asz00(void)
	{
		{	/* Jas/as.scm 1 */
			{	/* Jas/as.scm 1 */
				obj_t BgL_cportz00_2058;

				{	/* Jas/as.scm 1 */
					obj_t BgL_stringz00_2065;

					BgL_stringz00_2065 = BGl_string2224z00zzjas_asz00;
					{	/* Jas/as.scm 1 */
						obj_t BgL_startz00_2066;

						BgL_startz00_2066 = BINT(0L);
						{	/* Jas/as.scm 1 */
							obj_t BgL_endz00_2067;

							BgL_endz00_2067 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2065)));
							{	/* Jas/as.scm 1 */

								BgL_cportz00_2058 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2065, BgL_startz00_2066, BgL_endz00_2067);
				}}}}
				{
					long BgL_iz00_2059;

					BgL_iz00_2059 = 28L;
				BgL_loopz00_2060:
					if ((BgL_iz00_2059 == -1L))
						{	/* Jas/as.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* Jas/as.scm 1 */
							{	/* Jas/as.scm 1 */
								obj_t BgL_arg2225z00_2061;

								{	/* Jas/as.scm 1 */

									{	/* Jas/as.scm 1 */
										obj_t BgL_locationz00_2063;

										BgL_locationz00_2063 = BBOOL(((bool_t) 0));
										{	/* Jas/as.scm 1 */

											BgL_arg2225z00_2061 =
												BGl_readz00zz__readerz00(BgL_cportz00_2058,
												BgL_locationz00_2063);
										}
									}
								}
								{	/* Jas/as.scm 1 */
									int BgL_tmpz00_2115;

									BgL_tmpz00_2115 = (int) (BgL_iz00_2059);
									CNST_TABLE_SET(BgL_tmpz00_2115, BgL_arg2225z00_2061);
							}}
							{	/* Jas/as.scm 1 */
								int BgL_auxz00_2064;

								BgL_auxz00_2064 = (int) ((BgL_iz00_2059 - 1L));
								{
									long BgL_iz00_2120;

									BgL_iz00_2120 = (long) (BgL_auxz00_2064);
									BgL_iz00_2059 = BgL_iz00_2120;
									goto BgL_loopz00_2060;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzjas_asz00(void)
	{
		{	/* Jas/as.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzjas_asz00(void)
	{
		{	/* Jas/as.scm 1 */
			BGl_za2jaszd2warningza2zd2zzjas_asz00 = BFALSE;
			BGl_za2jvmzd2charzd2infoza2z00zzjas_asz00 = BFALSE;
			BGl_za2za2sdefileza2za2z00zzjas_asz00 = BFALSE;
			BGl_za2za2sdefullfileza2za2z00zzjas_asz00 = BFALSE;
			BGl_za2za2maxposza2za2z00zzjas_asz00 = 0L;
			BGl_za2za2maxcharza2za2z00zzjas_asz00 = BINT(0L);
			BGl_za2za2maxlineza2za2z00zzjas_asz00 = BINT(0L);
			BGl_za2profileza2z00zzjas_asz00 = ((bool_t) 0);
			BGl_za2za2allzd2linesza2za2zd2zzjas_asz00 = BNIL;
			return (BGl_za2za2poszd2ze3lineza2za2z31zzjas_asz00 = BNIL, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzjas_asz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_287;

				BgL_headz00_287 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_288;
					obj_t BgL_tailz00_289;

					BgL_prevz00_288 = BgL_headz00_287;
					BgL_tailz00_289 = BgL_l1z00_1;
				BgL_loopz00_290:
					if (PAIRP(BgL_tailz00_289))
						{
							obj_t BgL_newzd2prevzd2_292;

							BgL_newzd2prevzd2_292 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_289), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_288, BgL_newzd2prevzd2_292);
							{
								obj_t BgL_tailz00_2132;
								obj_t BgL_prevz00_2131;

								BgL_prevz00_2131 = BgL_newzd2prevzd2_292;
								BgL_tailz00_2132 = CDR(BgL_tailz00_289);
								BgL_tailz00_289 = BgL_tailz00_2132;
								BgL_prevz00_288 = BgL_prevz00_2131;
								goto BgL_loopz00_290;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_287);
				}
			}
		}

	}



/* jvm-asfile */
	BGL_EXPORTED_DEF obj_t BGl_jvmzd2asfilezd2zzjas_asz00(obj_t BgL_fileinz00_3,
		obj_t BgL_fileoutz00_4)
	{
		{	/* Jas/as.scm 35 */
			{	/* Jas/as.scm 36 */
				obj_t BgL_portz00_295;

				BgL_portz00_295 =
					BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00(BgL_fileoutz00_4);
				if (BINARY_PORTP(BgL_portz00_295))
					{	/* Jas/as.scm 37 */
						BFALSE;
					}
				else
					{	/* Jas/as.scm 37 */
						BGl_errorz00zz__errorz00(BGl_string2180z00zzjas_asz00,
							BGl_string2181z00zzjas_asz00, BgL_fileoutz00_4);
					}
				{	/* Jas/as.scm 39 */
					obj_t BgL_env1065z00_298;

					BgL_env1065z00_298 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Jas/as.scm 39 */
						obj_t BgL_cell1060z00_299;

						BgL_cell1060z00_299 = MAKE_STACK_CELL(BUNSPEC);
						{	/* Jas/as.scm 39 */
							obj_t BgL_val1064z00_300;

							BgL_val1064z00_300 =
								BGl_zc3z04exitza31329ze3ze70z60zzjas_asz00(BgL_fileinz00_3,
								BgL_portz00_295, BgL_cell1060z00_299, BgL_env1065z00_298);
							if ((BgL_val1064z00_300 == BgL_cell1060z00_299))
								{	/* Jas/as.scm 39 */
									{	/* Jas/as.scm 39 */
										int BgL_tmpz00_2144;

										BgL_tmpz00_2144 = (int) (0L);
										BGL_SIGSETMASK(BgL_tmpz00_2144);
									}
									{	/* Jas/as.scm 39 */
										obj_t BgL_arg1328z00_301;

										BgL_arg1328z00_301 = CELL_REF(((obj_t) BgL_val1064z00_300));
										{	/* Jas/as.scm 41 */
											char *BgL_stringz00_1280;

											BgL_stringz00_1280 = BSTRING_TO_STRING(BgL_fileoutz00_4);
											if (unlink(BgL_stringz00_1280))
												{	/* Jas/as.scm 41 */
													((bool_t) 0);
												}
											else
												{	/* Jas/as.scm 41 */
													((bool_t) 1);
												}
										}
										return BGl_raisez00zz__errorz00(BgL_arg1328z00_301);
									}
								}
							else
								{	/* Jas/as.scm 39 */
									return BgL_val1064z00_300;
								}
						}
					}
				}
			}
		}

	}



/* <@exit:1329>~0 */
	obj_t BGl_zc3z04exitza31329ze3ze70z60zzjas_asz00(obj_t BgL_fileinz00_2057,
		obj_t BgL_portz00_2056, obj_t BgL_cell1060z00_2055,
		obj_t BgL_env1065z00_2054)
	{
		{	/* Jas/as.scm 39 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1073z00_303;

			if (SET_EXIT(BgL_an_exit1073z00_303))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1073z00_303 = (void *) jmpbuf;
					PUSH_ENV_EXIT(BgL_env1065z00_2054, BgL_an_exit1073z00_303, 1L);
					{	/* Jas/as.scm 39 */
						obj_t BgL_escape1061z00_304;

						BgL_escape1061z00_304 =
							BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1065z00_2054);
						{	/* Jas/as.scm 39 */
							obj_t BgL_res1076z00_305;

							{	/* Jas/as.scm 39 */
								obj_t BgL_ohs1057z00_306;

								BgL_ohs1057z00_306 =
									BGL_ENV_ERROR_HANDLER_GET(BgL_env1065z00_2054);
								{	/* Jas/as.scm 39 */
									obj_t BgL_hds1058z00_307;

									BgL_hds1058z00_307 =
										MAKE_STACK_PAIR(BgL_escape1061z00_304,
										BgL_cell1060z00_2055);
									BGL_ENV_ERROR_HANDLER_SET(BgL_env1065z00_2054,
										BgL_hds1058z00_307);
									BUNSPEC;
									{	/* Jas/as.scm 39 */
										obj_t BgL_exitd1066z00_308;

										BgL_exitd1066z00_308 =
											BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1065z00_2054);
										{	/* Jas/as.scm 39 */
											obj_t BgL_tmp1068z00_309;

											{	/* Jas/as.scm 39 */
												obj_t BgL_arg1333z00_317;

												BgL_arg1333z00_317 =
													BGL_EXITD_PROTECT(BgL_exitd1066z00_308);
												BgL_tmp1068z00_309 =
													MAKE_YOUNG_PAIR(BgL_ohs1057z00_306,
													BgL_arg1333z00_317);
											}
											{	/* Jas/as.scm 39 */

												BGL_EXITD_PROTECT_SET(BgL_exitd1066z00_308,
													BgL_tmp1068z00_309);
												BUNSPEC;
												{	/* Jas/as.scm 43 */
													obj_t BgL_tmp1067z00_310;

													{	/* Jas/as.scm 43 */
														obj_t BgL_exitd1069z00_311;

														BgL_exitd1069z00_311 = BGL_EXITD_TOP_AS_OBJ();
														{	/* Jas/as.scm 45 */
															obj_t BgL_zc3z04anonymousza31332ze3z87_2031;

															BgL_zc3z04anonymousza31332ze3z87_2031 =
																MAKE_FX_PROCEDURE
																(BGl_z62zc3z04anonymousza31332ze3ze5zzjas_asz00,
																(int) (0L), (int) (1L));
															PROCEDURE_SET
																(BgL_zc3z04anonymousza31332ze3z87_2031,
																(int) (0L), BgL_portz00_2056);
															{	/* Jas/as.scm 43 */
																obj_t BgL_arg1828z00_1266;

																{	/* Jas/as.scm 43 */
																	obj_t BgL_arg1829z00_1267;

																	BgL_arg1829z00_1267 =
																		BGL_EXITD_PROTECT(BgL_exitd1069z00_311);
																	BgL_arg1828z00_1266 =
																		MAKE_YOUNG_PAIR
																		(BgL_zc3z04anonymousza31332ze3z87_2031,
																		BgL_arg1829z00_1267);
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1069z00_311,
																	BgL_arg1828z00_1266);
																BUNSPEC;
															}
															{	/* Jas/as.scm 44 */
																obj_t BgL_tmp1071z00_313;

																BgL_tmp1071z00_313 =
																	BGl_jvmzd2aszd2zzjas_asz00
																	(BGl_callzd2withzd2inputzd2filezd2zz__r4_ports_6_10_1z00
																	(BgL_fileinz00_2057,
																		BGl_readzd2envzd2zz__readerz00),
																	BgL_portz00_2056);
																{	/* Jas/as.scm 43 */
																	bool_t BgL_test2279z00_2175;

																	{	/* Jas/as.scm 43 */
																		obj_t BgL_arg1827z00_1269;

																		BgL_arg1827z00_1269 =
																			BGL_EXITD_PROTECT(BgL_exitd1069z00_311);
																		BgL_test2279z00_2175 =
																			PAIRP(BgL_arg1827z00_1269);
																	}
																	if (BgL_test2279z00_2175)
																		{	/* Jas/as.scm 43 */
																			obj_t BgL_arg1825z00_1270;

																			{	/* Jas/as.scm 43 */
																				obj_t BgL_arg1826z00_1271;

																				BgL_arg1826z00_1271 =
																					BGL_EXITD_PROTECT
																					(BgL_exitd1069z00_311);
																				BgL_arg1825z00_1270 =
																					CDR(((obj_t) BgL_arg1826z00_1271));
																			}
																			BGL_EXITD_PROTECT_SET
																				(BgL_exitd1069z00_311,
																				BgL_arg1825z00_1270);
																			BUNSPEC;
																		}
																	else
																		{	/* Jas/as.scm 43 */
																			BFALSE;
																		}
																}
																close_binary_port(((obj_t) BgL_portz00_2056));
																BgL_tmp1067z00_310 = BgL_tmp1071z00_313;
															}
														}
													}
													{	/* Jas/as.scm 39 */
														bool_t BgL_test2280z00_2184;

														{	/* Jas/as.scm 39 */
															obj_t BgL_arg1827z00_1275;

															BgL_arg1827z00_1275 =
																BGL_EXITD_PROTECT(BgL_exitd1066z00_308);
															BgL_test2280z00_2184 = PAIRP(BgL_arg1827z00_1275);
														}
														if (BgL_test2280z00_2184)
															{	/* Jas/as.scm 39 */
																obj_t BgL_arg1825z00_1276;

																{	/* Jas/as.scm 39 */
																	obj_t BgL_arg1826z00_1277;

																	BgL_arg1826z00_1277 =
																		BGL_EXITD_PROTECT(BgL_exitd1066z00_308);
																	BgL_arg1825z00_1276 =
																		CDR(((obj_t) BgL_arg1826z00_1277));
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1066z00_308,
																	BgL_arg1825z00_1276);
																BUNSPEC;
															}
														else
															{	/* Jas/as.scm 39 */
																BFALSE;
															}
													}
													BGL_ENV_ERROR_HANDLER_SET(BgL_env1065z00_2054,
														BgL_ohs1057z00_306);
													BUNSPEC;
													BgL_res1076z00_305 = BgL_tmp1067z00_310;
												}
											}
										}
									}
								}
							}
							POP_ENV_EXIT(BgL_env1065z00_2054);
							return BgL_res1076z00_305;
						}
					}
				}
		}

	}



/* &jvm-asfile */
	obj_t BGl_z62jvmzd2asfilezb0zzjas_asz00(obj_t BgL_envz00_2032,
		obj_t BgL_fileinz00_2033, obj_t BgL_fileoutz00_2034)
	{
		{	/* Jas/as.scm 35 */
			return
				BGl_jvmzd2asfilezd2zzjas_asz00(BgL_fileinz00_2033, BgL_fileoutz00_2034);
		}

	}



/* &<@anonymous:1332> */
	obj_t BGl_z62zc3z04anonymousza31332ze3ze5zzjas_asz00(obj_t BgL_envz00_2035)
	{
		{	/* Jas/as.scm 43 */
			{	/* Jas/as.scm 45 */
				obj_t BgL_portz00_2036;

				BgL_portz00_2036 = PROCEDURE_REF(BgL_envz00_2035, (int) (0L));
				return close_binary_port(((obj_t) BgL_portz00_2036));
			}
		}

	}



/* jvm-as */
	BGL_EXPORTED_DEF obj_t BGl_jvmzd2aszd2zzjas_asz00(obj_t BgL_lz00_5,
		obj_t BgL_outchanz00_6)
	{
		{	/* Jas/as.scm 47 */
			{	/* Jas/as.scm 48 */
				obj_t BgL_classfilez00_321;

				BgL_classfilez00_321 =
					BGl_asz00zzjas_asz00(BGl_jaszd2profilezd2zzjas_profilez00
					(BgL_lz00_5));
				BGl_reorderzd2classfilezd2zzjas_asz00(((BgL_classfilez00_bglt)
						BgL_classfilez00_321), BgL_lz00_5);
				BGl_producez00zzjas_producez00(BgL_outchanz00_6, BgL_classfilez00_321);
				{	/* Jas/as.scm 53 */
					obj_t BgL_g1247z00_322;

					BgL_g1247z00_322 =
						(((BgL_classfilez00_bglt) COBJECT(
								((BgL_classfilez00_bglt) BgL_classfilez00_321)))->
						BgL_globalsz00);
					{
						obj_t BgL_l1245z00_324;

						{	/* Jas/as.scm 54 */
							bool_t BgL_tmpz00_2205;

							BgL_l1245z00_324 = BgL_g1247z00_322;
						BgL_zc3z04anonymousza31335ze3z87_325:
							if (PAIRP(BgL_l1245z00_324))
								{	/* Jas/as.scm 54 */
									{	/* Jas/as.scm 53 */
										obj_t BgL_sz00_327;

										BgL_sz00_327 = CAR(BgL_l1245z00_324);
										BGl_rempropz12z12zz__r4_symbols_6_4z00(BgL_sz00_327,
											CNST_TABLE_REF(0));
									}
									{
										obj_t BgL_l1245z00_2211;

										BgL_l1245z00_2211 = CDR(BgL_l1245z00_324);
										BgL_l1245z00_324 = BgL_l1245z00_2211;
										goto BgL_zc3z04anonymousza31335ze3z87_325;
									}
								}
							else
								{	/* Jas/as.scm 54 */
									BgL_tmpz00_2205 = ((bool_t) 1);
								}
							return BBOOL(BgL_tmpz00_2205);
						}
					}
				}
			}
		}

	}



/* &jvm-as */
	obj_t BGl_z62jvmzd2aszb0zzjas_asz00(obj_t BgL_envz00_2040,
		obj_t BgL_lz00_2041, obj_t BgL_outchanz00_2042)
	{
		{	/* Jas/as.scm 47 */
			return BGl_jvmzd2aszd2zzjas_asz00(BgL_lz00_2041, BgL_outchanz00_2042);
		}

	}



/* reorder-classfile */
	obj_t BGl_reorderzd2classfilezd2zzjas_asz00(BgL_classfilez00_bglt
		BgL_classez00_7, obj_t BgL_lz00_8)
	{
		{	/* Jas/as.scm 60 */
			{	/* Jas/as.scm 61 */
				obj_t BgL_defsz00_331;

				{	/* Jas/as.scm 61 */
					obj_t BgL_pairz00_1292;

					{	/* Jas/as.scm 61 */
						obj_t BgL_pairz00_1291;

						{	/* Jas/as.scm 61 */
							obj_t BgL_pairz00_1290;

							BgL_pairz00_1290 = CDR(((obj_t) BgL_lz00_8));
							BgL_pairz00_1291 = CDR(BgL_pairz00_1290);
						}
						BgL_pairz00_1292 = CDR(BgL_pairz00_1291);
					}
					BgL_defsz00_331 = CDR(CDR(BgL_pairz00_1292));
				}
				{
					obj_t BgL_codez00_382;
					obj_t BgL_accz00_383;
					obj_t BgL_namez00_407;

					{	/* Jas/as.scm 111 */
						obj_t BgL_codez00_335;

						BgL_codez00_335 =
							BGl_getzd2procedurezd2codeze70ze7zzjas_asz00(BgL_classez00_7,
							BgL_defsz00_331, BGl_string2184z00zzjas_asz00);
						if (CBOOL(BgL_codez00_335))
							{	/* Jas/as.scm 113 */
								obj_t BgL_closuresz00_336;
								obj_t BgL_methodsz00_337;

								BgL_codez00_382 = BgL_codez00_335;
								BgL_accz00_383 = BNIL;
							BgL_zc3z04anonymousza31422ze3z87_384:
								if (NULLP(BgL_codez00_382))
									{	/* Jas/as.scm 75 */
										BgL_closuresz00_336 = BgL_accz00_383;
									}
								else
									{	/* Jas/as.scm 76 */
										bool_t BgL_test2284z00_2226;

										{	/* Jas/as.scm 76 */
											bool_t BgL_test2285z00_2227;

											{	/* Jas/as.scm 76 */
												obj_t BgL_tmpz00_2228;

												BgL_tmpz00_2228 = CAR(((obj_t) BgL_codez00_382));
												BgL_test2285z00_2227 = PAIRP(BgL_tmpz00_2228);
											}
											if (BgL_test2285z00_2227)
												{	/* Jas/as.scm 76 */
													bool_t BgL_test2286z00_2232;

													{	/* Jas/as.scm 76 */
														obj_t BgL_tmpz00_2233;

														{	/* Jas/as.scm 76 */
															obj_t BgL_pairz00_1325;

															BgL_pairz00_1325 = CAR(((obj_t) BgL_codez00_382));
															BgL_tmpz00_2233 = CAR(BgL_pairz00_1325);
														}
														BgL_test2286z00_2232 =
															(BgL_tmpz00_2233 == CNST_TABLE_REF(1));
													}
													if (BgL_test2286z00_2232)
														{	/* Jas/as.scm 77 */
															bool_t BgL_test2287z00_2239;

															{	/* Jas/as.scm 77 */
																obj_t BgL_tmpz00_2240;

																{	/* Jas/as.scm 77 */
																	obj_t BgL_pairz00_1331;

																	{	/* Jas/as.scm 77 */
																		obj_t BgL_pairz00_1330;

																		BgL_pairz00_1330 =
																			CAR(((obj_t) BgL_codez00_382));
																		BgL_pairz00_1331 = CDR(BgL_pairz00_1330);
																	}
																	BgL_tmpz00_2240 = CAR(BgL_pairz00_1331);
																}
																BgL_test2287z00_2239 =
																	(BgL_tmpz00_2240 == CNST_TABLE_REF(2));
															}
															if (BgL_test2287z00_2239)
																{	/* Jas/as.scm 77 */
																	BgL_test2284z00_2226 = ((bool_t) 0);
																}
															else
																{	/* Jas/as.scm 77 */
																	BgL_test2284z00_2226 = ((bool_t) 1);
																}
														}
													else
														{	/* Jas/as.scm 76 */
															BgL_test2284z00_2226 = ((bool_t) 0);
														}
												}
											else
												{	/* Jas/as.scm 76 */
													BgL_test2284z00_2226 = ((bool_t) 0);
												}
										}
										if (BgL_test2284z00_2226)
											{	/* Jas/as.scm 78 */
												obj_t BgL_arg1485z00_396;
												obj_t BgL_arg1489z00_397;

												BgL_arg1485z00_396 = CDR(((obj_t) BgL_codez00_382));
												{	/* Jas/as.scm 79 */
													BgL_methodz00_bglt BgL_arg1502z00_398;

													{	/* Jas/as.scm 79 */
														obj_t BgL_arg1509z00_399;

														{	/* Jas/as.scm 79 */
															obj_t BgL_pairz00_1338;

															{	/* Jas/as.scm 79 */
																obj_t BgL_pairz00_1337;

																BgL_pairz00_1337 =
																	CAR(((obj_t) BgL_codez00_382));
																BgL_pairz00_1338 = CDR(BgL_pairz00_1337);
															}
															BgL_arg1509z00_399 = CAR(BgL_pairz00_1338);
														}
														BgL_namez00_407 = BgL_arg1509z00_399;
														{	/* Jas/as.scm 85 */
															BgL_methodz00_bglt BgL_mz00_409;

															BgL_mz00_409 =
																BGl_declaredzd2methodzd2zzjas_classfilez00
																(BgL_classez00_7, BgL_namez00_407);
															{	/* Jas/as.scm 85 */
																obj_t BgL_stubz00_410;

																BgL_stubz00_410 =
																	(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
																			((BgL_fieldzd2orzd2methodz00_bglt)
																				BgL_mz00_409)))->BgL_namez00);
																{	/* Jas/as.scm 86 */

																	{	/* Jas/as.scm 87 */
																		bool_t BgL_test2288z00_2256;

																		if (bigloo_strncmp(BgL_stubz00_410,
																				BGl_string2182z00zzjas_asz00, 1L))
																			{	/* Jas/as.scm 87 */
																				BgL_test2288z00_2256 = ((bool_t) 1);
																			}
																		else
																			{	/* Jas/as.scm 87 */
																				BgL_test2288z00_2256 =
																					bigloo_strncmp(BgL_stubz00_410,
																					BGl_string2183z00zzjas_asz00, 5L);
																			}
																		if (BgL_test2288z00_2256)
																			{	/* Jas/as.scm 90 */
																				obj_t BgL_ownerz00_413;

																				{	/* Jas/as.scm 90 */
																					obj_t BgL_arg1602z00_452;

																					BgL_arg1602z00_452 =
																						(((BgL_fieldzd2orzd2methodz00_bglt)
																							COBJECT((
																									(BgL_fieldzd2orzd2methodz00_bglt)
																									BgL_mz00_409)))->
																						BgL_ownerz00);
																					{	/* Jas/as.scm 90 */
																						obj_t BgL_arg1455z00_1349;

																						BgL_arg1455z00_1349 =
																							SYMBOL_TO_STRING(
																							((obj_t) BgL_arg1602z00_452));
																						BgL_ownerz00_413 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_1349);
																					}
																				}
																				{	/* Jas/as.scm 90 */
																					obj_t BgL_name2z00_414;

																					{	/* Jas/as.scm 91 */
																						obj_t BgL_arg1455z00_1351;

																						BgL_arg1455z00_1351 =
																							SYMBOL_TO_STRING(
																							((obj_t) BgL_namez00_407));
																						BgL_name2z00_414 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_1351);
																					}
																					{	/* Jas/as.scm 91 */
																						obj_t BgL_nameprocz00_415;

																						BgL_nameprocz00_415 =
																							string_append(c_substring
																							(BgL_name2z00_414, 0L,
																								STRING_LENGTH
																								(BgL_ownerz00_413)),
																							c_substring(BgL_name2z00_414,
																								(1L +
																									STRING_LENGTH
																									(BgL_ownerz00_413)),
																								STRING_LENGTH
																								(BgL_name2z00_414)));
																						{	/* Jas/as.scm 92 */
																							obj_t BgL_proczd2inzd2codez00_416;

																							{	/* Jas/as.scm 97 */
																								obj_t BgL_hook1252z00_420;

																								BgL_hook1252z00_420 =
																									MAKE_YOUNG_PAIR(BFALSE, BNIL);
																								{	/* Jas/as.scm 103 */
																									obj_t BgL_g1253z00_421;

																									{	/* Jas/as.scm 103 */
																										obj_t BgL_arg1584z00_445;

																										BgL_arg1584z00_445 =
																											(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_mz00_409)))->BgL_namez00);
																										BgL_g1253z00_421 =
																											BGl_getzd2procedurezd2codeze70ze7zzjas_asz00
																											(BgL_classez00_7,
																											BgL_lz00_8,
																											BgL_arg1584z00_445);
																									}
																									{
																										obj_t BgL_l1249z00_423;
																										obj_t BgL_h1250z00_424;

																										BgL_l1249z00_423 =
																											BgL_g1253z00_421;
																										BgL_h1250z00_424 =
																											BgL_hook1252z00_420;
																									BgL_zc3z04anonymousza31545ze3z87_425:
																										if (NULLP
																											(BgL_l1249z00_423))
																											{	/* Jas/as.scm 103 */
																												BgL_proczd2inzd2codez00_416
																													=
																													CDR
																													(BgL_hook1252z00_420);
																											}
																										else
																											{	/* Jas/as.scm 103 */
																												bool_t
																													BgL_test2291z00_2282;
																												{	/* Jas/as.scm 99 */
																													obj_t BgL_xz00_438;

																													BgL_xz00_438 =
																														CAR(
																														((obj_t)
																															BgL_l1249z00_423));
																													if (PAIRP
																														(BgL_xz00_438))
																														{	/* Jas/as.scm 99 */
																															if (
																																(CAR
																																	(BgL_xz00_438)
																																	==
																																	CNST_TABLE_REF
																																	(1)))
																																{	/* Jas/as.scm 100 */
																																	BgL_test2291z00_2282
																																		=
																																		(CAR(CDR
																																			(BgL_xz00_438))
																																		==
																																		bstring_to_symbol
																																		(BgL_nameprocz00_415));
																																}
																															else
																																{	/* Jas/as.scm 100 */
																																	BgL_test2291z00_2282
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																													else
																														{	/* Jas/as.scm 99 */
																															BgL_test2291z00_2282
																																= ((bool_t) 0);
																														}
																												}
																												if (BgL_test2291z00_2282)
																													{	/* Jas/as.scm 103 */
																														obj_t
																															BgL_nh1251z00_434;
																														{	/* Jas/as.scm 103 */
																															obj_t
																																BgL_arg1565z00_436;
																															BgL_arg1565z00_436
																																=
																																CAR(((obj_t)
																																	BgL_l1249z00_423));
																															BgL_nh1251z00_434
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1565z00_436,
																																BNIL);
																														}
																														SET_CDR
																															(BgL_h1250z00_424,
																															BgL_nh1251z00_434);
																														{	/* Jas/as.scm 103 */
																															obj_t
																																BgL_arg1564z00_435;
																															BgL_arg1564z00_435
																																=
																																CDR(((obj_t)
																																	BgL_l1249z00_423));
																															{
																																obj_t
																																	BgL_h1250z00_2302;
																																obj_t
																																	BgL_l1249z00_2301;
																																BgL_l1249z00_2301
																																	=
																																	BgL_arg1564z00_435;
																																BgL_h1250z00_2302
																																	=
																																	BgL_nh1251z00_434;
																																BgL_h1250z00_424
																																	=
																																	BgL_h1250z00_2302;
																																BgL_l1249z00_423
																																	=
																																	BgL_l1249z00_2301;
																																goto
																																	BgL_zc3z04anonymousza31545ze3z87_425;
																															}
																														}
																													}
																												else
																													{	/* Jas/as.scm 103 */
																														obj_t
																															BgL_arg1571z00_437;
																														BgL_arg1571z00_437 =
																															CDR(((obj_t)
																																BgL_l1249z00_423));
																														{
																															obj_t
																																BgL_l1249z00_2305;
																															BgL_l1249z00_2305
																																=
																																BgL_arg1571z00_437;
																															BgL_l1249z00_423 =
																																BgL_l1249z00_2305;
																															goto
																																BgL_zc3z04anonymousza31545ze3z87_425;
																														}
																													}
																											}
																									}
																								}
																							}
																							{	/* Jas/as.scm 97 */

																								if (NULLP
																									(BgL_proczd2inzd2codez00_416))
																									{	/* Jas/as.scm 106 */
																										BgL_arg1502z00_398 =
																											BGl_declaredzd2methodzd2zzjas_classfilez00
																											(BgL_classez00_7,
																											BgL_namez00_407);
																									}
																								else
																									{	/* Jas/as.scm 107 */
																										obj_t BgL_arg1540z00_418;

																										{	/* Jas/as.scm 107 */
																											obj_t BgL_pairz00_1376;

																											BgL_pairz00_1376 =
																												CAR(
																												((obj_t)
																													BgL_proczd2inzd2codez00_416));
																											BgL_arg1540z00_418 =
																												CAR(CDR
																												(BgL_pairz00_1376));
																										}
																										BgL_arg1502z00_398 =
																											BGl_declaredzd2methodzd2zzjas_classfilez00
																											(BgL_classez00_7,
																											BgL_arg1540z00_418);
																									}
																							}
																						}
																					}
																				}
																			}
																		else
																			{	/* Jas/as.scm 87 */
																				BgL_arg1502z00_398 =
																					BGl_declaredzd2methodzd2zzjas_classfilez00
																					(BgL_classez00_7, BgL_namez00_407);
																			}
																	}
																}
															}
														}
													}
													BgL_arg1489z00_397 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_arg1502z00_398), BgL_accz00_383);
												}
												{
													obj_t BgL_accz00_2318;
													obj_t BgL_codez00_2317;

													BgL_codez00_2317 = BgL_arg1485z00_396;
													BgL_accz00_2318 = BgL_arg1489z00_397;
													BgL_accz00_383 = BgL_accz00_2318;
													BgL_codez00_382 = BgL_codez00_2317;
													goto BgL_zc3z04anonymousza31422ze3z87_384;
												}
											}
										else
											{	/* Jas/as.scm 80 */
												obj_t BgL_arg1513z00_400;

												BgL_arg1513z00_400 = CDR(((obj_t) BgL_codez00_382));
												{
													obj_t BgL_codez00_2321;

													BgL_codez00_2321 = BgL_arg1513z00_400;
													BgL_codez00_382 = BgL_codez00_2321;
													goto BgL_zc3z04anonymousza31422ze3z87_384;
												}
											}
									}
								BgL_methodsz00_337 =
									(((BgL_classfilez00_bglt) COBJECT(BgL_classez00_7))->
									BgL_methodsz00);
								{
									obj_t BgL_l1254z00_339;

									BgL_l1254z00_339 = BgL_closuresz00_336;
								BgL_zc3z04anonymousza31341ze3z87_340:
									if (PAIRP(BgL_l1254z00_339))
										{	/* Jas/as.scm 116 */
											{	/* Jas/as.scm 116 */
												obj_t BgL_xz00_342;

												BgL_xz00_342 = CAR(BgL_l1254z00_339);
												BgL_methodsz00_337 =
													bgl_remq_bang(BgL_xz00_342, BgL_methodsz00_337);
											}
											{
												obj_t BgL_l1254z00_2327;

												BgL_l1254z00_2327 = CDR(BgL_l1254z00_339);
												BgL_l1254z00_339 = BgL_l1254z00_2327;
												goto BgL_zc3z04anonymousza31341ze3z87_340;
											}
										}
									else
										{	/* Jas/as.scm 116 */
											((bool_t) 1);
										}
								}
								return
									((((BgL_classfilez00_bglt) COBJECT(BgL_classez00_7))->
										BgL_methodsz00) =
									((obj_t) BGl_appendzd221011zd2zzjas_asz00(BgL_methodsz00_337,
											BgL_closuresz00_336)), BUNSPEC);
							}
						else
							{	/* Jas/as.scm 112 */
								return BFALSE;
							}
					}
				}
			}
		}

	}



/* get-procedure-code~0 */
	obj_t BGl_getzd2procedurezd2codeze70ze7zzjas_asz00(BgL_classfilez00_bglt
		BgL_classez00_2053, obj_t BgL_lz00_346, obj_t BgL_procnamez00_347)
	{
		{	/* Jas/as.scm 71 */
		BGl_getzd2procedurezd2codeze70ze7zzjas_asz00:
			{

				if (NULLP(BgL_lz00_346))
					{	/* Jas/as.scm 71 */
						return BFALSE;
					}
				else
					{	/* Jas/as.scm 71 */
						if (PAIRP(BgL_lz00_346))
							{	/* Jas/as.scm 71 */
								obj_t BgL_carzd2120zd2_360;

								BgL_carzd2120zd2_360 = CAR(((obj_t) BgL_lz00_346));
								if (PAIRP(BgL_carzd2120zd2_360))
									{	/* Jas/as.scm 71 */
										obj_t BgL_cdrzd2127zd2_362;

										BgL_cdrzd2127zd2_362 = CDR(BgL_carzd2120zd2_360);
										if ((CAR(BgL_carzd2120zd2_360) == CNST_TABLE_REF(3)))
											{	/* Jas/as.scm 71 */
												if (PAIRP(BgL_cdrzd2127zd2_362))
													{	/* Jas/as.scm 71 */
														obj_t BgL_cdrzd2133zd2_366;

														BgL_cdrzd2133zd2_366 = CDR(BgL_cdrzd2127zd2_362);
														if (PAIRP(BgL_cdrzd2133zd2_366))
															{	/* Jas/as.scm 71 */
																obj_t BgL_cdrzd2139zd2_368;

																BgL_cdrzd2139zd2_368 =
																	CDR(BgL_cdrzd2133zd2_366);
																if (PAIRP(BgL_cdrzd2139zd2_368))
																	{	/* Jas/as.scm 71 */
																		obj_t BgL_arg1367z00_370;
																		obj_t BgL_arg1375z00_373;

																		BgL_arg1367z00_370 =
																			CAR(BgL_cdrzd2127zd2_362);
																		BgL_arg1375z00_373 =
																			CDR(BgL_cdrzd2139zd2_368);
																		{	/* Jas/as.scm 67 */
																			BgL_methodz00_bglt BgL_mz00_1314;

																			BgL_mz00_1314 =
																				BGl_declaredzd2methodzd2zzjas_classfilez00
																				(BgL_classez00_2053,
																				BgL_arg1367z00_370);
																			if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_mz00_1314)))->BgL_namez00), BgL_procnamez00_347))
																				{	/* Jas/as.scm 68 */
																					return BgL_arg1375z00_373;
																				}
																			else
																				{
																					obj_t BgL_lz00_2359;

																					BgL_lz00_2359 = CDR(BgL_lz00_346);
																					BgL_lz00_346 = BgL_lz00_2359;
																					goto
																						BGl_getzd2procedurezd2codeze70ze7zzjas_asz00;
																				}
																		}
																	}
																else
																	{	/* Jas/as.scm 71 */
																	BgL_tagzd2103zd2_356:
																		{	/* Jas/as.scm 71 */
																			obj_t BgL_arg1421z00_381;

																			BgL_arg1421z00_381 =
																				CDR(((obj_t) BgL_lz00_346));
																			{
																				obj_t BgL_lz00_2363;

																				BgL_lz00_2363 = BgL_arg1421z00_381;
																				BgL_lz00_346 = BgL_lz00_2363;
																				goto
																					BGl_getzd2procedurezd2codeze70ze7zzjas_asz00;
																			}
																		}
																	}
															}
														else
															{	/* Jas/as.scm 71 */
																goto BgL_tagzd2103zd2_356;
															}
													}
												else
													{	/* Jas/as.scm 71 */
														goto BgL_tagzd2103zd2_356;
													}
											}
										else
											{	/* Jas/as.scm 71 */
												goto BgL_tagzd2103zd2_356;
											}
									}
								else
									{	/* Jas/as.scm 71 */
										goto BgL_tagzd2103zd2_356;
									}
							}
						else
							{	/* Jas/as.scm 71 */
								goto BgL_tagzd2103zd2_356;
							}
					}
			}
		}

	}



/* as */
	obj_t BGl_asz00zzjas_asz00(obj_t BgL_lz00_9)
	{
		{	/* Jas/as.scm 123 */
			{
				obj_t BgL_keyz00_458;
				obj_t BgL_thisz00_459;
				obj_t BgL_extendz00_460;
				obj_t BgL_implementsz00_461;
				obj_t BgL_declsz00_462;
				obj_t BgL_infosz00_463;

				if (NULLP(BgL_lz00_9))
					{	/* Jas/as.scm 124 */
					BgL_tagzd2149zd2_465:
						return
							BGl_errorz00zz__errorz00(BGl_string2185z00zzjas_asz00,
							BGl_string2186z00zzjas_asz00, BgL_lz00_9);
					}
				else
					{	/* Jas/as.scm 124 */
						obj_t BgL_cdrzd2166zd2_468;

						BgL_cdrzd2166zd2_468 = CDR(((obj_t) BgL_lz00_9));
						{	/* Jas/as.scm 124 */
							obj_t BgL_keyz00_469;

							BgL_keyz00_469 = CAR(((obj_t) BgL_lz00_9));
							if (PAIRP(BgL_cdrzd2166zd2_468))
								{	/* Jas/as.scm 124 */
									obj_t BgL_carzd2173zd2_471;
									obj_t BgL_cdrzd2174zd2_472;

									BgL_carzd2173zd2_471 = CAR(BgL_cdrzd2166zd2_468);
									BgL_cdrzd2174zd2_472 = CDR(BgL_cdrzd2166zd2_468);
									if (SYMBOLP(BgL_carzd2173zd2_471))
										{	/* Jas/as.scm 124 */
											if (PAIRP(BgL_cdrzd2174zd2_472))
												{	/* Jas/as.scm 124 */
													obj_t BgL_carzd2181zd2_476;
													obj_t BgL_cdrzd2182zd2_477;

													BgL_carzd2181zd2_476 = CAR(BgL_cdrzd2174zd2_472);
													BgL_cdrzd2182zd2_477 = CDR(BgL_cdrzd2174zd2_472);
													if (SYMBOLP(BgL_carzd2181zd2_476))
														{	/* Jas/as.scm 124 */
															if (PAIRP(BgL_cdrzd2182zd2_477))
																{	/* Jas/as.scm 124 */
																	obj_t BgL_carzd2188zd2_481;
																	obj_t BgL_cdrzd2189zd2_482;

																	BgL_carzd2188zd2_481 =
																		CAR(BgL_cdrzd2182zd2_477);
																	BgL_cdrzd2189zd2_482 =
																		CDR(BgL_cdrzd2182zd2_477);
																	{
																		obj_t BgL_gzd2205zd2_502;
																		obj_t BgL_gzd2192zd2_486;

																		BgL_gzd2192zd2_486 = BgL_carzd2188zd2_481;
																		if (NULLP(BgL_gzd2192zd2_486))
																			{	/* Jas/as.scm 124 */
																				if (PAIRP(BgL_cdrzd2189zd2_482))
																					{	/* Jas/as.scm 124 */
																						obj_t BgL_carzd2196zd2_490;

																						BgL_carzd2196zd2_490 =
																							CAR(BgL_cdrzd2189zd2_482);
																						if (PAIRP(BgL_carzd2196zd2_490))
																							{	/* Jas/as.scm 124 */
																								if (
																									(CAR(BgL_carzd2196zd2_490) ==
																										CNST_TABLE_REF(5)))
																									{	/* Jas/as.scm 124 */
																										obj_t BgL_arg1627z00_494;
																										obj_t BgL_arg1629z00_495;

																										BgL_arg1627z00_494 =
																											CDR(BgL_carzd2196zd2_490);
																										BgL_arg1629z00_495 =
																											CDR(BgL_cdrzd2189zd2_482);
																										{
																											BgL_classfilez00_bglt
																												BgL_auxz00_2400;
																											BgL_keyz00_458 =
																												BgL_keyz00_469;
																											BgL_thisz00_459 =
																												BgL_carzd2173zd2_471;
																											BgL_extendz00_460 =
																												BgL_carzd2181zd2_476;
																											BgL_implementsz00_461 =
																												BgL_carzd2188zd2_481;
																											BgL_declsz00_462 =
																												BgL_arg1627z00_494;
																											BgL_infosz00_463 =
																												BgL_arg1629z00_495;
																										BgL_tagzd2148zd2_464:
																											{	/* Jas/as.scm 129 */
																												BgL_classfilez00_bglt
																													BgL_classfilez00_520;
																												{	/* Jas/as.scm 129 */
																													BgL_classfilez00_bglt
																														BgL_new1081z00_561;
																													{	/* Jas/classfile.scm 42 */
																														BgL_classfilez00_bglt
																															BgL_new1080z00_564;
																														BgL_new1080z00_564 =
																															(
																															(BgL_classfilez00_bglt)
																															BOBJECT(GC_MALLOC
																																(sizeof(struct
																																		BgL_classfilez00_bgl))));
																														{	/* Jas/classfile.scm 42 */
																															long
																																BgL_arg1724z00_565;
																															BgL_arg1724z00_565
																																=
																																BGL_CLASS_NUM
																																(BGl_classfilez00zzjas_classfilez00);
																															BGL_OBJECT_CLASS_NUM_SET
																																(((BgL_objectz00_bglt) BgL_new1080z00_564), BgL_arg1724z00_565);
																														}
																														BgL_new1081z00_561 =
																															BgL_new1080z00_564;
																													}
																													((((BgL_classfilez00_bglt) COBJECT(BgL_new1081z00_561))->BgL_currentzd2methodzd2) = ((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
																													((((BgL_classfilez00_bglt) COBJECT(BgL_new1081z00_561))->BgL_globalsz00) = ((obj_t) BNIL), BUNSPEC);
																													((((BgL_classfilez00_bglt) COBJECT(BgL_new1081z00_561))->BgL_poolz00) = ((obj_t) BNIL), BUNSPEC);
																													((((BgL_classfilez00_bglt) COBJECT(BgL_new1081z00_561))->BgL_poolzd2siza7ez75) = ((obj_t) BINT(1L)), BUNSPEC);
																													{
																														obj_t
																															BgL_auxz00_2411;
																														{	/* Jas/classfile.scm 44 */
																															obj_t
																																BgL_arg1720z00_562;
																															{	/* Jas/classfile.scm 44 */
																																obj_t
																																	BgL_arg1722z00_563;
																																{	/* Jas/classfile.scm 44 */
																																	obj_t
																																		BgL_classz00_1389;
																																	BgL_classz00_1389
																																		=
																																		BGl_classfilez00zzjas_classfilez00;
																																	BgL_arg1722z00_563
																																		=
																																		BGL_CLASS_ALL_FIELDS
																																		(BgL_classz00_1389);
																																}
																																BgL_arg1720z00_562
																																	=
																																	VECTOR_REF
																																	(BgL_arg1722z00_563,
																																	4L);
																															}
																															BgL_auxz00_2411 =
																																BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																																(BgL_arg1720z00_562);
																														}
																														((((BgL_classfilez00_bglt) COBJECT(BgL_new1081z00_561))->BgL_pooledzd2nameszd2) = ((obj_t) BgL_auxz00_2411), BUNSPEC);
																													}
																													((((BgL_classfilez00_bglt) COBJECT(BgL_new1081z00_561))->BgL_flagsz00) = ((obj_t) BFALSE), BUNSPEC);
																													((((BgL_classfilez00_bglt) COBJECT(BgL_new1081z00_561))->BgL_mez00) = ((obj_t) BFALSE), BUNSPEC);
																													((((BgL_classfilez00_bglt) COBJECT(BgL_new1081z00_561))->BgL_superz00) = ((obj_t) BFALSE), BUNSPEC);
																													((((BgL_classfilez00_bglt) COBJECT(BgL_new1081z00_561))->BgL_interfacesz00) = ((obj_t) BNIL), BUNSPEC);
																													((((BgL_classfilez00_bglt) COBJECT(BgL_new1081z00_561))->BgL_fieldsz00) = ((obj_t) BNIL), BUNSPEC);
																													((((BgL_classfilez00_bglt) COBJECT(BgL_new1081z00_561))->BgL_methodsz00) = ((obj_t) BNIL), BUNSPEC);
																													((((BgL_classfilez00_bglt) COBJECT(BgL_new1081z00_561))->BgL_attributesz00) = ((obj_t) BNIL), BUNSPEC);
																													BgL_classfilez00_520 =
																														BgL_new1081z00_561;
																												}
																												if (NULLP
																													(BgL_declsz00_462))
																													{	/* Jas/as.scm 130 */
																														BNIL;
																													}
																												else
																													{	/* Jas/as.scm 130 */
																														obj_t
																															BgL_head1258z00_523;
																														BgL_head1258z00_523
																															=
																															MAKE_YOUNG_PAIR
																															(BNIL, BNIL);
																														{
																															obj_t
																																BgL_l1256z00_525;
																															obj_t
																																BgL_tail1259z00_526;
																															BgL_l1256z00_525 =
																																BgL_declsz00_462;
																															BgL_tail1259z00_526
																																=
																																BgL_head1258z00_523;
																														BgL_zc3z04anonymousza31694ze3z87_527:
																															if (NULLP
																																(BgL_l1256z00_525))
																																{	/* Jas/as.scm 130 */
																																	CDR
																																		(BgL_head1258z00_523);
																																}
																															else
																																{	/* Jas/as.scm 130 */
																																	obj_t
																																		BgL_newtail1260z00_529;
																																	{	/* Jas/as.scm 130 */
																																		obj_t
																																			BgL_arg1700z00_531;
																																		{	/* Jas/as.scm 130 */
																																			obj_t
																																				BgL_declz00_532;
																																			BgL_declz00_532
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_l1256z00_525));
																																			BgL_arg1700z00_531
																																				=
																																				BGl_aszd2declarezd2zzjas_asz00
																																				(BgL_classfilez00_520,
																																				BgL_declz00_532);
																																		}
																																		BgL_newtail1260z00_529
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1700z00_531,
																																			BNIL);
																																	}
																																	SET_CDR
																																		(BgL_tail1259z00_526,
																																		BgL_newtail1260z00_529);
																																	{	/* Jas/as.scm 130 */
																																		obj_t
																																			BgL_arg1699z00_530;
																																		BgL_arg1699z00_530
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_l1256z00_525));
																																		{
																																			obj_t
																																				BgL_tail1259z00_2437;
																																			obj_t
																																				BgL_l1256z00_2436;
																																			BgL_l1256z00_2436
																																				=
																																				BgL_arg1699z00_530;
																																			BgL_tail1259z00_2437
																																				=
																																				BgL_newtail1260z00_529;
																																			BgL_tail1259z00_526
																																				=
																																				BgL_tail1259z00_2437;
																																			BgL_l1256z00_525
																																				=
																																				BgL_l1256z00_2436;
																																			goto
																																				BgL_zc3z04anonymousza31694ze3z87_527;
																																		}
																																	}
																																}
																														}
																													}
																												BGl_setzd2fieldzd2methodzd2typezd2zzjas_asz00
																													(BgL_classfilez00_520);
																												{	/* Jas/as.scm 134 */
																													BgL_classez00_bglt
																														BgL_cthisz00_536;
																													BgL_classez00_bglt
																														BgL_cextendz00_537;
																													BgL_cthisz00_536 =
																														BGl_declaredzd2classzd2zzjas_classfilez00
																														(BgL_classfilez00_520,
																														BgL_thisz00_459);
																													BgL_cextendz00_537 =
																														BGl_declaredzd2classzd2zzjas_classfilez00
																														(BgL_classfilez00_520,
																														BgL_extendz00_460);
																													((((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_520))->BgL_flagsz00) = ((obj_t) (((BgL_classez00_bglt) COBJECT(BgL_cthisz00_536))->BgL_flagsz00)), BUNSPEC);
																													((((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_520))->BgL_mez00) = ((obj_t) BINT(BGl_poolzd2classzd2zzjas_classfilez00(BgL_classfilez00_520, BgL_cthisz00_536))), BUNSPEC);
																												}
																												((((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_520))->BgL_superz00) = ((obj_t) BINT(BGl_poolzd2classzd2byzd2namezd2zzjas_classfilez00(BgL_classfilez00_520, BgL_extendz00_460))), BUNSPEC);
																												{
																													obj_t BgL_auxz00_2449;

																													if (NULLP
																														(BgL_implementsz00_461))
																														{	/* Jas/as.scm 139 */
																															BgL_auxz00_2449 =
																																BNIL;
																														}
																													else
																														{	/* Jas/as.scm 139 */
																															obj_t
																																BgL_head1263z00_540;
																															{	/* Jas/as.scm 139 */
																																int
																																	BgL_arg1710z00_552;
																																{	/* Jas/as.scm 139 */
																																	obj_t
																																		BgL_arg1711z00_553;
																																	BgL_arg1711z00_553
																																		=
																																		CAR(((obj_t)
																																			BgL_implementsz00_461));
																																	BgL_arg1710z00_552
																																		=
																																		BGl_poolzd2classzd2byzd2namezd2zzjas_classfilez00
																																		(BgL_classfilez00_520,
																																		BgL_arg1711z00_553);
																																}
																																BgL_head1263z00_540
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BINT
																																	(BgL_arg1710z00_552),
																																	BNIL);
																															}
																															{	/* Jas/as.scm 139 */
																																obj_t
																																	BgL_g1266z00_541;
																																BgL_g1266z00_541
																																	=
																																	CDR(((obj_t)
																																		BgL_implementsz00_461));
																																{
																																	obj_t
																																		BgL_l1261z00_543;
																																	obj_t
																																		BgL_tail1264z00_544;
																																	BgL_l1261z00_543
																																		=
																																		BgL_g1266z00_541;
																																	BgL_tail1264z00_544
																																		=
																																		BgL_head1263z00_540;
																																BgL_zc3z04anonymousza31702ze3z87_545:
																																	if (NULLP
																																		(BgL_l1261z00_543))
																																		{	/* Jas/as.scm 139 */
																																			BgL_auxz00_2449
																																				=
																																				BgL_head1263z00_540;
																																		}
																																	else
																																		{	/* Jas/as.scm 139 */
																																			obj_t
																																				BgL_newtail1265z00_547;
																																			{	/* Jas/as.scm 139 */
																																				int
																																					BgL_arg1708z00_549;
																																				{	/* Jas/as.scm 139 */
																																					obj_t
																																						BgL_arg1709z00_550;
																																					BgL_arg1709z00_550
																																						=
																																						CAR(
																																						((obj_t) BgL_l1261z00_543));
																																					BgL_arg1708z00_549
																																						=
																																						BGl_poolzd2classzd2byzd2namezd2zzjas_classfilez00
																																						(BgL_classfilez00_520,
																																						BgL_arg1709z00_550);
																																				}
																																				BgL_newtail1265z00_547
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BINT
																																					(BgL_arg1708z00_549),
																																					BNIL);
																																			}
																																			SET_CDR
																																				(BgL_tail1264z00_544,
																																				BgL_newtail1265z00_547);
																																			{	/* Jas/as.scm 139 */
																																				obj_t
																																					BgL_arg1705z00_548;
																																				BgL_arg1705z00_548
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_l1261z00_543));
																																				{
																																					obj_t
																																						BgL_tail1264z00_2470;
																																					obj_t
																																						BgL_l1261z00_2469;
																																					BgL_l1261z00_2469
																																						=
																																						BgL_arg1705z00_548;
																																					BgL_tail1264z00_2470
																																						=
																																						BgL_newtail1265z00_547;
																																					BgL_tail1264z00_544
																																						=
																																						BgL_tail1264z00_2470;
																																					BgL_l1261z00_543
																																						=
																																						BgL_l1261z00_2469;
																																					goto
																																						BgL_zc3z04anonymousza31702ze3z87_545;
																																				}
																																			}
																																		}
																																}
																															}
																														}
																													((((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_520))->BgL_interfacesz00) = ((obj_t) BgL_auxz00_2449), BUNSPEC);
																												}
																												BGl_scanzd2infoszd2zzjas_asz00
																													(BgL_classfilez00_520,
																													BgL_infosz00_463);
																												if (STRINGP
																													(BGl_za2za2sdefullfileza2za2z00zzjas_asz00))
																													{
																														obj_t
																															BgL_auxz00_2475;
																														{	/* Jas/as.scm 143 */
																															BgL_attributez00_bglt
																																BgL_arg1714z00_556;
																															obj_t
																																BgL_arg1717z00_557;
																															BgL_arg1714z00_556
																																=
																																BGl_smapz00zzjas_asz00
																																(BgL_classfilez00_520);
																															BgL_arg1717z00_557
																																=
																																(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_520))->BgL_attributesz00);
																															BgL_auxz00_2475 =
																																MAKE_YOUNG_PAIR(
																																((obj_t)
																																	BgL_arg1714z00_556),
																																BgL_arg1717z00_557);
																														}
																														((((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_520))->BgL_attributesz00) = ((obj_t) BgL_auxz00_2475), BUNSPEC);
																													}
																												else
																													{	/* Jas/as.scm 141 */
																														BFALSE;
																													}
																												BgL_auxz00_2400 =
																													BgL_classfilez00_520;
																											}
																											return
																												((obj_t)
																												BgL_auxz00_2400);
																										}
																									}
																								else
																									{	/* Jas/as.scm 124 */
																										goto BgL_tagzd2149zd2_465;
																									}
																							}
																						else
																							{	/* Jas/as.scm 124 */
																								goto BgL_tagzd2149zd2_465;
																							}
																					}
																				else
																					{	/* Jas/as.scm 124 */
																						goto BgL_tagzd2149zd2_465;
																					}
																			}
																		else
																			{	/* Jas/as.scm 124 */
																				if (PAIRP(BgL_gzd2192zd2_486))
																					{	/* Jas/as.scm 124 */
																						bool_t BgL_test2320z00_2484;

																						{	/* Jas/as.scm 124 */
																							obj_t BgL_tmpz00_2485;

																							BgL_tmpz00_2485 =
																								CAR(BgL_gzd2192zd2_486);
																							BgL_test2320z00_2484 =
																								SYMBOLP(BgL_tmpz00_2485);
																						}
																						if (BgL_test2320z00_2484)
																							{	/* Jas/as.scm 124 */
																								BgL_gzd2205zd2_502 =
																									CDR(BgL_gzd2192zd2_486);
																							BgL_zc3z04anonymousza31651ze3z87_503:
																								if (NULLP
																									(BgL_gzd2205zd2_502))
																									{	/* Jas/as.scm 124 */
																										if (PAIRP
																											(BgL_cdrzd2189zd2_482))
																											{	/* Jas/as.scm 124 */
																												obj_t
																													BgL_carzd2209zd2_506;
																												BgL_carzd2209zd2_506 =
																													CAR
																													(BgL_cdrzd2189zd2_482);
																												if (PAIRP
																													(BgL_carzd2209zd2_506))
																													{	/* Jas/as.scm 124 */
																														if (
																															(CAR
																																(BgL_carzd2209zd2_506)
																																==
																																CNST_TABLE_REF
																																(5)))
																															{	/* Jas/as.scm 124 */
																																obj_t
																																	BgL_arg1675z00_510;
																																obj_t
																																	BgL_arg1678z00_511;
																																BgL_arg1675z00_510
																																	=
																																	CDR
																																	(BgL_carzd2209zd2_506);
																																BgL_arg1678z00_511
																																	=
																																	CDR
																																	(BgL_cdrzd2189zd2_482);
																																{
																																	BgL_classfilez00_bglt
																																		BgL_auxz00_2501;
																																	{
																																		obj_t
																																			BgL_infosz00_2507;
																																		obj_t
																																			BgL_declsz00_2506;
																																		obj_t
																																			BgL_implementsz00_2505;
																																		obj_t
																																			BgL_extendz00_2504;
																																		obj_t
																																			BgL_thisz00_2503;
																																		obj_t
																																			BgL_keyz00_2502;
																																		BgL_keyz00_2502
																																			=
																																			BgL_keyz00_469;
																																		BgL_thisz00_2503
																																			=
																																			BgL_carzd2173zd2_471;
																																		BgL_extendz00_2504
																																			=
																																			BgL_carzd2181zd2_476;
																																		BgL_implementsz00_2505
																																			=
																																			BgL_carzd2188zd2_481;
																																		BgL_declsz00_2506
																																			=
																																			BgL_arg1675z00_510;
																																		BgL_infosz00_2507
																																			=
																																			BgL_arg1678z00_511;
																																		BgL_infosz00_463
																																			=
																																			BgL_infosz00_2507;
																																		BgL_declsz00_462
																																			=
																																			BgL_declsz00_2506;
																																		BgL_implementsz00_461
																																			=
																																			BgL_implementsz00_2505;
																																		BgL_extendz00_460
																																			=
																																			BgL_extendz00_2504;
																																		BgL_thisz00_459
																																			=
																																			BgL_thisz00_2503;
																																		BgL_keyz00_458
																																			=
																																			BgL_keyz00_2502;
																																		goto
																																			BgL_tagzd2148zd2_464;
																																	}
																																	return
																																		((obj_t)
																																		BgL_auxz00_2501);
																																}
																															}
																														else
																															{	/* Jas/as.scm 124 */
																																goto
																																	BgL_tagzd2149zd2_465;
																															}
																													}
																												else
																													{	/* Jas/as.scm 124 */
																														goto
																															BgL_tagzd2149zd2_465;
																													}
																											}
																										else
																											{	/* Jas/as.scm 124 */
																												goto
																													BgL_tagzd2149zd2_465;
																											}
																									}
																								else
																									{	/* Jas/as.scm 124 */
																										if (PAIRP
																											(BgL_gzd2205zd2_502))
																											{	/* Jas/as.scm 124 */
																												bool_t
																													BgL_test2326z00_2511;
																												{	/* Jas/as.scm 124 */
																													obj_t BgL_tmpz00_2512;

																													BgL_tmpz00_2512 =
																														CAR
																														(BgL_gzd2205zd2_502);
																													BgL_test2326z00_2511 =
																														SYMBOLP
																														(BgL_tmpz00_2512);
																												}
																												if (BgL_test2326z00_2511)
																													{
																														obj_t
																															BgL_gzd2205zd2_2515;
																														BgL_gzd2205zd2_2515
																															=
																															CDR
																															(BgL_gzd2205zd2_502);
																														BgL_gzd2205zd2_502 =
																															BgL_gzd2205zd2_2515;
																														goto
																															BgL_zc3z04anonymousza31651ze3z87_503;
																													}
																												else
																													{	/* Jas/as.scm 124 */
																														goto
																															BgL_tagzd2149zd2_465;
																													}
																											}
																										else
																											{	/* Jas/as.scm 124 */
																												goto
																													BgL_tagzd2149zd2_465;
																											}
																									}
																							}
																						else
																							{	/* Jas/as.scm 124 */
																								goto BgL_tagzd2149zd2_465;
																							}
																					}
																				else
																					{	/* Jas/as.scm 124 */
																						goto BgL_tagzd2149zd2_465;
																					}
																			}
																	}
																}
															else
																{	/* Jas/as.scm 124 */
																	goto BgL_tagzd2149zd2_465;
																}
														}
													else
														{	/* Jas/as.scm 124 */
															goto BgL_tagzd2149zd2_465;
														}
												}
											else
												{	/* Jas/as.scm 124 */
													goto BgL_tagzd2149zd2_465;
												}
										}
									else
										{	/* Jas/as.scm 124 */
											goto BgL_tagzd2149zd2_465;
										}
								}
							else
								{	/* Jas/as.scm 124 */
									goto BgL_tagzd2149zd2_465;
								}
						}
					}
			}
		}

	}



/* set-field-method-type */
	bool_t BGl_setzd2fieldzd2methodzd2typezd2zzjas_asz00(BgL_classfilez00_bglt
		BgL_classfilez00_10)
	{
		{	/* Jas/as.scm 147 */
			{	/* Jas/as.scm 148 */
				obj_t BgL_g1269z00_566;

				BgL_g1269z00_566 =
					(((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_10))->
					BgL_globalsz00);
				{
					obj_t BgL_l1267z00_568;

					BgL_l1267z00_568 = BgL_g1269z00_566;
				BgL_zc3z04anonymousza31725ze3z87_569:
					if (PAIRP(BgL_l1267z00_568))
						{	/* Jas/as.scm 155 */
							{	/* Jas/as.scm 151 */
								obj_t BgL_slotz00_571;

								BgL_slotz00_571 = CAR(BgL_l1267z00_568);
								{	/* Jas/as.scm 151 */
									obj_t BgL_valuez00_572;

									BgL_valuez00_572 =
										BGl_getpropz00zz__r4_symbols_6_4z00(BgL_slotz00_571,
										CNST_TABLE_REF(0));
									{	/* Jas/as.scm 152 */
										bool_t BgL_test2328z00_2524;

										{	/* Jas/as.scm 152 */
											obj_t BgL_classz00_1426;

											BgL_classz00_1426 =
												BGl_fieldzd2orzd2methodz00zzjas_classfilez00;
											if (BGL_OBJECTP(BgL_valuez00_572))
												{	/* Jas/as.scm 152 */
													BgL_objectz00_bglt BgL_arg1807z00_1428;

													BgL_arg1807z00_1428 =
														(BgL_objectz00_bglt) (BgL_valuez00_572);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Jas/as.scm 152 */
															long BgL_idxz00_1434;

															BgL_idxz00_1434 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1428);
															BgL_test2328z00_2524 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_1434 + 1L)) == BgL_classz00_1426);
														}
													else
														{	/* Jas/as.scm 152 */
															bool_t BgL_res2172z00_1459;

															{	/* Jas/as.scm 152 */
																obj_t BgL_oclassz00_1442;

																{	/* Jas/as.scm 152 */
																	obj_t BgL_arg1815z00_1450;
																	long BgL_arg1816z00_1451;

																	BgL_arg1815z00_1450 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Jas/as.scm 152 */
																		long BgL_arg1817z00_1452;

																		BgL_arg1817z00_1452 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1428);
																		BgL_arg1816z00_1451 =
																			(BgL_arg1817z00_1452 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_1442 =
																		VECTOR_REF(BgL_arg1815z00_1450,
																		BgL_arg1816z00_1451);
																}
																{	/* Jas/as.scm 152 */
																	bool_t BgL__ortest_1115z00_1443;

																	BgL__ortest_1115z00_1443 =
																		(BgL_classz00_1426 == BgL_oclassz00_1442);
																	if (BgL__ortest_1115z00_1443)
																		{	/* Jas/as.scm 152 */
																			BgL_res2172z00_1459 =
																				BgL__ortest_1115z00_1443;
																		}
																	else
																		{	/* Jas/as.scm 152 */
																			long BgL_odepthz00_1444;

																			{	/* Jas/as.scm 152 */
																				obj_t BgL_arg1804z00_1445;

																				BgL_arg1804z00_1445 =
																					(BgL_oclassz00_1442);
																				BgL_odepthz00_1444 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_1445);
																			}
																			if ((1L < BgL_odepthz00_1444))
																				{	/* Jas/as.scm 152 */
																					obj_t BgL_arg1802z00_1447;

																					{	/* Jas/as.scm 152 */
																						obj_t BgL_arg1803z00_1448;

																						BgL_arg1803z00_1448 =
																							(BgL_oclassz00_1442);
																						BgL_arg1802z00_1447 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_1448, 1L);
																					}
																					BgL_res2172z00_1459 =
																						(BgL_arg1802z00_1447 ==
																						BgL_classz00_1426);
																				}
																			else
																				{	/* Jas/as.scm 152 */
																					BgL_res2172z00_1459 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2328z00_2524 = BgL_res2172z00_1459;
														}
												}
											else
												{	/* Jas/as.scm 152 */
													BgL_test2328z00_2524 = ((bool_t) 0);
												}
										}
										if (BgL_test2328z00_2524)
											{
												obj_t BgL_auxz00_2547;

												{	/* Jas/as.scm 154 */
													obj_t BgL_arg1733z00_575;

													BgL_arg1733z00_575 =
														(((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
																((BgL_fieldzd2orzd2methodz00_bglt)
																	BgL_valuez00_572)))->BgL_usertypez00);
													BgL_auxz00_2547 =
														((obj_t)
														BGl_aszd2typezd2zzjas_classfilez00
														(BgL_classfilez00_10, BgL_arg1733z00_575));
												}
												((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
																((BgL_fieldzd2orzd2methodz00_bglt)
																	BgL_valuez00_572)))->BgL_typez00) =
													((obj_t) BgL_auxz00_2547), BUNSPEC);
											}
										else
											{	/* Jas/as.scm 152 */
												BFALSE;
											}
									}
								}
							}
							{
								obj_t BgL_l1267z00_2554;

								BgL_l1267z00_2554 = CDR(BgL_l1267z00_568);
								BgL_l1267z00_568 = BgL_l1267z00_2554;
								goto BgL_zc3z04anonymousza31725ze3z87_569;
							}
						}
					else
						{	/* Jas/as.scm 155 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* scan-infos */
	obj_t BGl_scanzd2infoszd2zzjas_asz00(BgL_classfilez00_bglt
		BgL_classfilez00_11, obj_t BgL_infosz00_12)
	{
		{	/* Jas/as.scm 157 */
		BGl_scanzd2infoszd2zzjas_asz00:
			if (NULLP(BgL_infosz00_12))
				{	/* Jas/as.scm 159 */
					return ((obj_t) BgL_classfilez00_11);
				}
			else
				{	/* Jas/as.scm 160 */
					bool_t BgL_test2334z00_2559;

					{	/* Jas/as.scm 160 */
						obj_t BgL_tmpz00_2560;

						{	/* Jas/as.scm 160 */
							obj_t BgL_pairz00_1464;

							BgL_pairz00_1464 = CAR(((obj_t) BgL_infosz00_12));
							BgL_tmpz00_2560 = CAR(BgL_pairz00_1464);
						}
						BgL_test2334z00_2559 = (BgL_tmpz00_2560 == CNST_TABLE_REF(6));
					}
					if (BgL_test2334z00_2559)
						{	/* Jas/as.scm 160 */
							{
								obj_t BgL_auxz00_2566;

								{	/* Jas/as.scm 162 */
									obj_t BgL_l1270z00_582;

									{	/* Jas/as.scm 162 */
										obj_t BgL_pairz00_1468;

										BgL_pairz00_1468 = CAR(((obj_t) BgL_infosz00_12));
										BgL_l1270z00_582 = CDR(BgL_pairz00_1468);
									}
									if (NULLP(BgL_l1270z00_582))
										{	/* Jas/as.scm 162 */
											BgL_auxz00_2566 = BNIL;
										}
									else
										{	/* Jas/as.scm 162 */
											obj_t BgL_head1272z00_584;

											BgL_head1272z00_584 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											{
												obj_t BgL_l1270z00_586;
												obj_t BgL_tail1273z00_587;

												BgL_l1270z00_586 = BgL_l1270z00_582;
												BgL_tail1273z00_587 = BgL_head1272z00_584;
											BgL_zc3z04anonymousza31740ze3z87_588:
												if (NULLP(BgL_l1270z00_586))
													{	/* Jas/as.scm 162 */
														BgL_auxz00_2566 = CDR(BgL_head1272z00_584);
													}
												else
													{	/* Jas/as.scm 162 */
														obj_t BgL_newtail1274z00_590;

														{	/* Jas/as.scm 162 */
															BgL_fieldz00_bglt BgL_arg1747z00_592;

															{	/* Jas/as.scm 162 */
																obj_t BgL_fz00_593;

																BgL_fz00_593 = CAR(((obj_t) BgL_l1270z00_586));
																{	/* Jas/as.scm 329 */
																	BgL_fieldz00_bglt BgL_fieldz00_1471;

																	BgL_fieldz00_1471 =
																		BGl_declaredzd2fieldzd2zzjas_classfilez00
																		(BgL_classfilez00_11, BgL_fz00_593);
																	BGl_poolzd2fieldzd2zzjas_classfilez00
																		(BgL_classfilez00_11, BgL_fieldz00_1471);
																	BgL_arg1747z00_592 = BgL_fieldz00_1471;
																}
															}
															BgL_newtail1274z00_590 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg1747z00_592), BNIL);
														}
														SET_CDR(BgL_tail1273z00_587,
															BgL_newtail1274z00_590);
														{	/* Jas/as.scm 162 */
															obj_t BgL_arg1746z00_591;

															BgL_arg1746z00_591 =
																CDR(((obj_t) BgL_l1270z00_586));
															{
																obj_t BgL_tail1273z00_2586;
																obj_t BgL_l1270z00_2585;

																BgL_l1270z00_2585 = BgL_arg1746z00_591;
																BgL_tail1273z00_2586 = BgL_newtail1274z00_590;
																BgL_tail1273z00_587 = BgL_tail1273z00_2586;
																BgL_l1270z00_586 = BgL_l1270z00_2585;
																goto BgL_zc3z04anonymousza31740ze3z87_588;
															}
														}
													}
											}
										}
								}
								((((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_11))->
										BgL_fieldsz00) = ((obj_t) BgL_auxz00_2566), BUNSPEC);
							}
							{	/* Jas/as.scm 163 */
								obj_t BgL_arg1748z00_595;

								BgL_arg1748z00_595 = CDR(((obj_t) BgL_infosz00_12));
								{
									obj_t BgL_infosz00_2590;

									BgL_infosz00_2590 = BgL_arg1748z00_595;
									BgL_infosz00_12 = BgL_infosz00_2590;
									goto BGl_scanzd2infoszd2zzjas_asz00;
								}
							}
						}
					else
						{	/* Jas/as.scm 164 */
							bool_t BgL_test2337z00_2591;

							{	/* Jas/as.scm 164 */
								obj_t BgL_tmpz00_2592;

								{	/* Jas/as.scm 164 */
									obj_t BgL_pairz00_1478;

									BgL_pairz00_1478 = CAR(((obj_t) BgL_infosz00_12));
									BgL_tmpz00_2592 = CAR(BgL_pairz00_1478);
								}
								BgL_test2337z00_2591 = (BgL_tmpz00_2592 == CNST_TABLE_REF(7));
							}
							if (BgL_test2337z00_2591)
								{	/* Jas/as.scm 164 */
									{
										obj_t BgL_auxz00_2598;

										{	/* Jas/as.scm 166 */
											BgL_attributez00_bglt BgL_arg1751z00_599;
											obj_t BgL_arg1752z00_600;

											{	/* Jas/as.scm 166 */
												obj_t BgL_arg1753z00_601;

												{	/* Jas/as.scm 166 */
													obj_t BgL_pairz00_1484;

													{	/* Jas/as.scm 166 */
														obj_t BgL_pairz00_1483;

														BgL_pairz00_1483 = CAR(((obj_t) BgL_infosz00_12));
														BgL_pairz00_1484 = CDR(BgL_pairz00_1483);
													}
													BgL_arg1753z00_601 = CAR(BgL_pairz00_1484);
												}
												BgL_arg1751z00_599 =
													BGl_srcfilez00zzjas_asz00(BgL_classfilez00_11,
													BgL_arg1753z00_601);
											}
											BgL_arg1752z00_600 =
												(((BgL_classfilez00_bglt)
													COBJECT(BgL_classfilez00_11))->BgL_attributesz00);
											BgL_auxz00_2598 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1751z00_599),
												BgL_arg1752z00_600);
										}
										((((BgL_classfilez00_bglt) COBJECT(BgL_classfilez00_11))->
												BgL_attributesz00) =
											((obj_t) BgL_auxz00_2598), BUNSPEC);
									}
									{	/* Jas/as.scm 167 */
										obj_t BgL_arg1754z00_602;

										BgL_arg1754z00_602 = CDR(((obj_t) BgL_infosz00_12));
										{
											obj_t BgL_infosz00_2610;

											BgL_infosz00_2610 = BgL_arg1754z00_602;
											BgL_infosz00_12 = BgL_infosz00_2610;
											goto BGl_scanzd2infoszd2zzjas_asz00;
										}
									}
								}
							else
								{	/* Jas/as.scm 168 */
									bool_t BgL_test2338z00_2611;

									{	/* Jas/as.scm 168 */
										obj_t BgL_tmpz00_2612;

										{	/* Jas/as.scm 168 */
											obj_t BgL_pairz00_1489;

											BgL_pairz00_1489 = CAR(((obj_t) BgL_infosz00_12));
											BgL_tmpz00_2612 = CAR(BgL_pairz00_1489);
										}
										BgL_test2338z00_2611 =
											(BgL_tmpz00_2612 == CNST_TABLE_REF(8));
									}
									if (BgL_test2338z00_2611)
										{	/* Jas/as.scm 168 */
											{	/* Jas/as.scm 169 */
												obj_t BgL_pairz00_1495;

												{	/* Jas/as.scm 169 */
													obj_t BgL_pairz00_1494;

													BgL_pairz00_1494 = CAR(((obj_t) BgL_infosz00_12));
													BgL_pairz00_1495 = CDR(BgL_pairz00_1494);
												}
												BGl_za2za2sdefileza2za2z00zzjas_asz00 =
													CAR(BgL_pairz00_1495);
											}
											{	/* Jas/as.scm 170 */
												obj_t BgL_pairz00_1503;

												{	/* Jas/as.scm 170 */
													obj_t BgL_pairz00_1502;

													{	/* Jas/as.scm 170 */
														obj_t BgL_pairz00_1501;

														BgL_pairz00_1501 = CAR(((obj_t) BgL_infosz00_12));
														BgL_pairz00_1502 = CDR(BgL_pairz00_1501);
													}
													BgL_pairz00_1503 = CDR(BgL_pairz00_1502);
												}
												BGl_za2za2sdefullfileza2za2z00zzjas_asz00 =
													CAR(BgL_pairz00_1503);
											}
											{	/* Jas/as.scm 171 */
												obj_t BgL_arg1761z00_605;

												BgL_arg1761z00_605 = CDR(((obj_t) BgL_infosz00_12));
												{
													obj_t BgL_infosz00_2629;

													BgL_infosz00_2629 = BgL_arg1761z00_605;
													BgL_infosz00_12 = BgL_infosz00_2629;
													goto BGl_scanzd2infoszd2zzjas_asz00;
												}
											}
										}
									else
										{	/* Jas/as.scm 168 */
											{
												obj_t BgL_auxz00_2630;

												{	/* Jas/as.scm 174 */
													obj_t BgL_head1277z00_609;

													BgL_head1277z00_609 = MAKE_YOUNG_PAIR(BNIL, BNIL);
													{
														obj_t BgL_l1275z00_611;
														obj_t BgL_tail1278z00_612;

														BgL_l1275z00_611 = BgL_infosz00_12;
														BgL_tail1278z00_612 = BgL_head1277z00_609;
													BgL_zc3z04anonymousza31764ze3z87_613:
														if (NULLP(BgL_l1275z00_611))
															{	/* Jas/as.scm 174 */
																BgL_auxz00_2630 = CDR(BgL_head1277z00_609);
															}
														else
															{	/* Jas/as.scm 174 */
																obj_t BgL_newtail1279z00_615;

																{	/* Jas/as.scm 174 */
																	obj_t BgL_arg1770z00_617;

																	{	/* Jas/as.scm 174 */
																		obj_t BgL_mz00_618;

																		BgL_mz00_618 =
																			CAR(((obj_t) BgL_l1275z00_611));
																		BgL_arg1770z00_617 =
																			BGl_aszd2methodzd2zzjas_asz00
																			(BgL_classfilez00_11, BgL_mz00_618);
																	}
																	BgL_newtail1279z00_615 =
																		MAKE_YOUNG_PAIR(BgL_arg1770z00_617, BNIL);
																}
																SET_CDR(BgL_tail1278z00_612,
																	BgL_newtail1279z00_615);
																{	/* Jas/as.scm 174 */
																	obj_t BgL_arg1767z00_616;

																	BgL_arg1767z00_616 =
																		CDR(((obj_t) BgL_l1275z00_611));
																	{
																		obj_t BgL_tail1278z00_2643;
																		obj_t BgL_l1275z00_2642;

																		BgL_l1275z00_2642 = BgL_arg1767z00_616;
																		BgL_tail1278z00_2643 =
																			BgL_newtail1279z00_615;
																		BgL_tail1278z00_612 = BgL_tail1278z00_2643;
																		BgL_l1275z00_611 = BgL_l1275z00_2642;
																		goto BgL_zc3z04anonymousza31764ze3z87_613;
																	}
																}
															}
													}
												}
												((((BgL_classfilez00_bglt)
															COBJECT(BgL_classfilez00_11))->BgL_methodsz00) =
													((obj_t) BgL_auxz00_2630), BUNSPEC);
											}
											if (NULLP(BGl_za2za2allzd2linesza2za2zd2zzjas_asz00))
												{	/* Jas/as.scm 176 */
													BGl_za2jvmzd2charzd2infoza2z00zzjas_asz00 = BFALSE;
												}
											else
												{	/* Jas/as.scm 176 */
													BFALSE;
												}
											if (CBOOL(BGl_za2jvmzd2charzd2infoza2z00zzjas_asz00))
												{	/* Jas/as.scm 177 */
													return
														BGl_poszd2infozd2ze3linezd2infoz12z23zzjas_asz00();
												}
											else
												{	/* Jas/as.scm 177 */
													return BFALSE;
												}
										}
								}
						}
				}
		}

	}



/* srcfile */
	BgL_attributez00_bglt BGl_srcfilez00zzjas_asz00(BgL_classfilez00_bglt
		BgL_classfilez00_13, obj_t BgL_namez00_14)
	{
		{	/* Jas/as.scm 182 */
			{	/* Jas/as.scm 183 */
				BgL_attributez00_bglt BgL_new1089z00_624;

				{	/* Jas/as.scm 184 */
					BgL_attributez00_bglt BgL_new1088z00_625;

					BgL_new1088z00_625 =
						((BgL_attributez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_attributez00_bgl))));
					{	/* Jas/as.scm 184 */
						long BgL_arg1799z00_626;

						BgL_arg1799z00_626 =
							BGL_CLASS_NUM(BGl_attributez00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1088z00_625), BgL_arg1799z00_626);
					}
					BgL_new1089z00_624 = BgL_new1088z00_625;
				}
				((((BgL_attributez00_bglt) COBJECT(BgL_new1089z00_624))->BgL_typez00) =
					((obj_t) CNST_TABLE_REF(9)), BUNSPEC);
				((((BgL_attributez00_bglt) COBJECT(BgL_new1089z00_624))->BgL_namez00) =
					((obj_t)
						BINT(BGl_poolzd2namezd2zzjas_classfilez00(BgL_classfilez00_13,
								BGl_string2187z00zzjas_asz00))), BUNSPEC);
				((((BgL_attributez00_bglt) COBJECT(BgL_new1089z00_624))->
						BgL_siza7eza7) = ((obj_t) BINT(2L)), BUNSPEC);
				((((BgL_attributez00_bglt) COBJECT(BgL_new1089z00_624))->BgL_infoz00) =
					((obj_t)
						BINT(BGl_poolzd2namezd2zzjas_classfilez00(BgL_classfilez00_13,
								BgL_namez00_14))), BUNSPEC);
				return BgL_new1089z00_624;
			}
		}

	}



/* smap */
	BgL_attributez00_bglt BGl_smapz00zzjas_asz00(BgL_classfilez00_bglt
		BgL_classfilez00_15)
	{
		{	/* Jas/as.scm 192 */
			{	/* Jas/as.scm 193 */
				obj_t BgL_smapfilez00_627;

				BgL_smapfilez00_627 =
					BGl_stringzd2ze3utf8z31zzjas_libz00(BGl_smapfilez00zzjas_asz00());
				{	/* Jas/as.scm 194 */
					BgL_attributez00_bglt BgL_new1091z00_628;

					{	/* Jas/as.scm 195 */
						BgL_attributez00_bglt BgL_new1090z00_629;

						BgL_new1090z00_629 =
							((BgL_attributez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_attributez00_bgl))));
						{	/* Jas/as.scm 195 */
							long BgL_arg1805z00_630;

							BgL_arg1805z00_630 =
								BGL_CLASS_NUM(BGl_attributez00zzjas_classfilez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1090z00_629), BgL_arg1805z00_630);
						}
						BgL_new1091z00_628 = BgL_new1090z00_629;
					}
					((((BgL_attributez00_bglt) COBJECT(BgL_new1091z00_628))->
							BgL_typez00) = ((obj_t) CNST_TABLE_REF(10)), BUNSPEC);
					((((BgL_attributez00_bglt) COBJECT(BgL_new1091z00_628))->
							BgL_namez00) =
						((obj_t)
							BINT(BGl_poolzd2namezd2zzjas_classfilez00(BgL_classfilez00_15,
									BGl_string2188z00zzjas_asz00))), BUNSPEC);
					((((BgL_attributez00_bglt) COBJECT(BgL_new1091z00_628))->
							BgL_siza7eza7) =
						((obj_t) BINT(STRING_LENGTH(((obj_t) BgL_smapfilez00_627)))),
						BUNSPEC);
					((((BgL_attributez00_bglt) COBJECT(BgL_new1091z00_628))->
							BgL_infoz00) = ((obj_t) BgL_smapfilez00_627), BUNSPEC);
					return BgL_new1091z00_628;
				}
			}
		}

	}



/* smapfile */
	obj_t BGl_smapfilez00zzjas_asz00(void)
	{
		{	/* Jas/as.scm 200 */
			{	/* Jas/as.scm 207 */
				obj_t BgL_arg1808z00_632;
				obj_t BgL_arg1812z00_633;
				obj_t BgL_arg1820z00_634;

				if (CBOOL(BGl_za2jvmzd2charzd2infoza2z00zzjas_asz00))
					{	/* Jas/as.scm 207 */
						BgL_arg1808z00_632 = BGl_string2189z00zzjas_asz00;
					}
				else
					{	/* Jas/as.scm 207 */
						BgL_arg1808z00_632 = BGl_string2190z00zzjas_asz00;
					}
				{	/* Jas/as.scm 214 */
					obj_t BgL_arg1849z00_655;

					if (CBOOL(BGl_za2jvmzd2charzd2infoza2z00zzjas_asz00))
						{	/* Jas/as.scm 214 */
							BgL_arg1849z00_655 = BGl_za2za2maxcharza2za2z00zzjas_asz00;
						}
					else
						{	/* Jas/as.scm 214 */
							BgL_arg1849z00_655 = BGl_za2za2maxlineza2za2z00zzjas_asz00;
						}
					{	/* Jas/as.scm 214 */

						BgL_arg1812z00_633 =
							BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
							(long) CINT(BgL_arg1849z00_655), 10L);
				}}
				if (CBOOL(BGl_za2jvmzd2charzd2infoza2z00zzjas_asz00))
					{	/* Jas/as.scm 225 */
						obj_t BgL_arg1850z00_658;

						{	/* Jas/as.scm 225 */
							obj_t BgL_runner1877z00_690;

							{	/* Jas/as.scm 226 */
								obj_t BgL_l1280z00_668;

								BgL_l1280z00_668 = BGl_za2za2poszd2ze3lineza2za2z31zzjas_asz00;
								if (NULLP(BgL_l1280z00_668))
									{	/* Jas/as.scm 226 */
										BgL_runner1877z00_690 = BNIL;
									}
								else
									{	/* Jas/as.scm 226 */
										obj_t BgL_head1282z00_670;

										BgL_head1282z00_670 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1280z00_672;
											obj_t BgL_tail1283z00_673;

											BgL_l1280z00_672 = BgL_l1280z00_668;
											BgL_tail1283z00_673 = BgL_head1282z00_670;
										BgL_zc3z04anonymousza31862ze3z87_674:
											if (NULLP(BgL_l1280z00_672))
												{	/* Jas/as.scm 226 */
													BgL_runner1877z00_690 = CDR(BgL_head1282z00_670);
												}
											else
												{	/* Jas/as.scm 226 */
													obj_t BgL_newtail1284z00_676;

													{	/* Jas/as.scm 226 */
														obj_t BgL_arg1866z00_678;

														{	/* Jas/as.scm 226 */
															obj_t BgL_xz00_679;

															BgL_xz00_679 = CAR(((obj_t) BgL_l1280z00_672));
															{	/* Jas/as.scm 228 */
																obj_t BgL_arg1868z00_680;
																obj_t BgL_arg1869z00_681;
																long BgL_arg1870z00_682;

																BgL_arg1868z00_680 =
																	CAR(((obj_t) BgL_xz00_679));
																{	/* Jas/as.scm 228 */
																	obj_t BgL_pairz00_1523;

																	BgL_pairz00_1523 =
																		CDR(((obj_t) BgL_xz00_679));
																	BgL_arg1869z00_681 = CAR(BgL_pairz00_1523);
																}
																{	/* Jas/as.scm 229 */
																	long BgL_tmpz00_2701;

																	{	/* Jas/as.scm 229 */
																		long BgL_auxz00_2708;
																		long BgL_tmpz00_2702;

																		{	/* Jas/as.scm 229 */
																			obj_t BgL_pairz00_1533;

																			BgL_pairz00_1533 =
																				CDR(((obj_t) BgL_xz00_679));
																			BgL_auxz00_2708 =
																				(long) CINT(CAR(BgL_pairz00_1533));
																		}
																		{	/* Jas/as.scm 229 */
																			obj_t BgL_pairz00_1529;

																			{	/* Jas/as.scm 229 */
																				obj_t BgL_pairz00_1528;

																				BgL_pairz00_1528 =
																					CDR(((obj_t) BgL_xz00_679));
																				BgL_pairz00_1529 =
																					CDR(BgL_pairz00_1528);
																			}
																			BgL_tmpz00_2702 =
																				(long) CINT(CAR(BgL_pairz00_1529));
																		}
																		BgL_tmpz00_2701 =
																			(BgL_tmpz00_2702 - BgL_auxz00_2708);
																	}
																	BgL_arg1870z00_682 = (1L + BgL_tmpz00_2701);
																}
																{	/* Jas/as.scm 227 */
																	obj_t BgL_list1871z00_683;

																	{	/* Jas/as.scm 227 */
																		obj_t BgL_arg1872z00_684;

																		{	/* Jas/as.scm 227 */
																			obj_t BgL_arg1873z00_685;

																			BgL_arg1873z00_685 =
																				MAKE_YOUNG_PAIR(BINT
																				(BgL_arg1870z00_682), BNIL);
																			BgL_arg1872z00_684 =
																				MAKE_YOUNG_PAIR(BgL_arg1869z00_681,
																				BgL_arg1873z00_685);
																		}
																		BgL_list1871z00_683 =
																			MAKE_YOUNG_PAIR(BgL_arg1868z00_680,
																			BgL_arg1872z00_684);
																	}
																	BgL_arg1866z00_678 =
																		BGl_formatz00zz__r4_output_6_10_3z00
																		(BGl_string2191z00zzjas_asz00,
																		BgL_list1871z00_683);
														}}}
														BgL_newtail1284z00_676 =
															MAKE_YOUNG_PAIR(BgL_arg1866z00_678, BNIL);
													}
													SET_CDR(BgL_tail1283z00_673, BgL_newtail1284z00_676);
													{	/* Jas/as.scm 226 */
														obj_t BgL_arg1864z00_677;

														BgL_arg1864z00_677 =
															CDR(((obj_t) BgL_l1280z00_672));
														{
															obj_t BgL_tail1283z00_2725;
															obj_t BgL_l1280z00_2724;

															BgL_l1280z00_2724 = BgL_arg1864z00_677;
															BgL_tail1283z00_2725 = BgL_newtail1284z00_676;
															BgL_tail1283z00_673 = BgL_tail1283z00_2725;
															BgL_l1280z00_672 = BgL_l1280z00_2724;
															goto BgL_zc3z04anonymousza31862ze3z87_674;
														}
													}
												}
										}
									}
							}
							BgL_arg1850z00_658 =
								BGl_stringzd2appendzd2zz__r4_strings_6_7z00
								(BgL_runner1877z00_690);
						}
						{	/* Jas/as.scm 219 */
							obj_t BgL_list1851z00_659;

							{	/* Jas/as.scm 219 */
								obj_t BgL_arg1852z00_660;

								{	/* Jas/as.scm 219 */
									obj_t BgL_arg1853z00_661;

									{	/* Jas/as.scm 219 */
										obj_t BgL_arg1854z00_662;

										{	/* Jas/as.scm 219 */
											obj_t BgL_arg1856z00_663;

											{	/* Jas/as.scm 219 */
												obj_t BgL_arg1857z00_664;

												{	/* Jas/as.scm 219 */
													obj_t BgL_arg1858z00_665;

													{	/* Jas/as.scm 219 */
														obj_t BgL_arg1859z00_666;

														{	/* Jas/as.scm 219 */
															obj_t BgL_arg1860z00_667;

															BgL_arg1860z00_667 =
																MAKE_YOUNG_PAIR(BgL_arg1850z00_658, BNIL);
															BgL_arg1859z00_666 =
																MAKE_YOUNG_PAIR(BGl_string2192z00zzjas_asz00,
																BgL_arg1860z00_667);
														}
														BgL_arg1858z00_665 =
															MAKE_YOUNG_PAIR(BGl_string2193z00zzjas_asz00,
															BgL_arg1859z00_666);
													}
													BgL_arg1857z00_664 =
														MAKE_YOUNG_PAIR
														(BGl_za2za2sdefullfileza2za2z00zzjas_asz00,
														BgL_arg1858z00_665);
												}
												BgL_arg1856z00_663 =
													MAKE_YOUNG_PAIR(BGl_string2193z00zzjas_asz00,
													BgL_arg1857z00_664);
											}
											BgL_arg1854z00_662 =
												MAKE_YOUNG_PAIR(BGl_za2za2sdefileza2za2z00zzjas_asz00,
												BgL_arg1856z00_663);
										}
										BgL_arg1853z00_661 =
											MAKE_YOUNG_PAIR(BGl_string2194z00zzjas_asz00,
											BgL_arg1854z00_662);
									}
									BgL_arg1852z00_660 =
										MAKE_YOUNG_PAIR(BGl_string2195z00zzjas_asz00,
										BgL_arg1853z00_661);
								}
								BgL_list1851z00_659 =
									MAKE_YOUNG_PAIR(BGl_string2196z00zzjas_asz00,
									BgL_arg1852z00_660);
							}
							BgL_arg1820z00_634 =
								BGl_stringzd2appendzd2zz__r4_strings_6_7z00
								(BgL_list1851z00_659);
						}
					}
				else
					{	/* Jas/as.scm 218 */
						BgL_arg1820z00_634 = BGl_string2197z00zzjas_asz00;
					}
				{	/* Jas/as.scm 201 */
					obj_t BgL_list1821z00_635;

					{	/* Jas/as.scm 201 */
						obj_t BgL_arg1822z00_636;

						{	/* Jas/as.scm 201 */
							obj_t BgL_arg1823z00_637;

							{	/* Jas/as.scm 201 */
								obj_t BgL_arg1831z00_638;

								{	/* Jas/as.scm 201 */
									obj_t BgL_arg1832z00_639;

									{	/* Jas/as.scm 201 */
										obj_t BgL_arg1833z00_640;

										{	/* Jas/as.scm 201 */
											obj_t BgL_arg1834z00_641;

											{	/* Jas/as.scm 201 */
												obj_t BgL_arg1835z00_642;

												{	/* Jas/as.scm 201 */
													obj_t BgL_arg1836z00_643;

													{	/* Jas/as.scm 201 */
														obj_t BgL_arg1837z00_644;

														{	/* Jas/as.scm 201 */
															obj_t BgL_arg1838z00_645;

															{	/* Jas/as.scm 201 */
																obj_t BgL_arg1839z00_646;

																{	/* Jas/as.scm 201 */
																	obj_t BgL_arg1840z00_647;

																	{	/* Jas/as.scm 201 */
																		obj_t BgL_arg1842z00_648;

																		{	/* Jas/as.scm 201 */
																			obj_t BgL_arg1843z00_649;

																			{	/* Jas/as.scm 201 */
																				obj_t BgL_arg1844z00_650;

																				{	/* Jas/as.scm 201 */
																					obj_t BgL_arg1845z00_651;

																					{	/* Jas/as.scm 201 */
																						obj_t BgL_arg1846z00_652;

																						{	/* Jas/as.scm 201 */
																							obj_t BgL_arg1847z00_653;

																							{	/* Jas/as.scm 201 */
																								obj_t BgL_arg1848z00_654;

																								BgL_arg1848z00_654 =
																									MAKE_YOUNG_PAIR
																									(BGl_string2198z00zzjas_asz00,
																									BNIL);
																								BgL_arg1847z00_653 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1820z00_634,
																									BgL_arg1848z00_654);
																							}
																							BgL_arg1846z00_652 =
																								MAKE_YOUNG_PAIR
																								(BGl_string2199z00zzjas_asz00,
																								BgL_arg1847z00_653);
																						}
																						BgL_arg1845z00_651 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1812z00_633,
																							BgL_arg1846z00_652);
																					}
																					BgL_arg1844z00_650 =
																						MAKE_YOUNG_PAIR
																						(BGl_string2200z00zzjas_asz00,
																						BgL_arg1845z00_651);
																				}
																				BgL_arg1843z00_649 =
																					MAKE_YOUNG_PAIR
																					(BGl_string2192z00zzjas_asz00,
																					BgL_arg1844z00_650);
																			}
																			BgL_arg1842z00_648 =
																				MAKE_YOUNG_PAIR
																				(BGl_string2193z00zzjas_asz00,
																				BgL_arg1843z00_649);
																		}
																		BgL_arg1840z00_647 =
																			MAKE_YOUNG_PAIR
																			(BGl_za2za2sdefullfileza2za2z00zzjas_asz00,
																			BgL_arg1842z00_648);
																	}
																	BgL_arg1839z00_646 =
																		MAKE_YOUNG_PAIR
																		(BGl_string2193z00zzjas_asz00,
																		BgL_arg1840z00_647);
																}
																BgL_arg1838z00_645 =
																	MAKE_YOUNG_PAIR
																	(BGl_za2za2sdefileza2za2z00zzjas_asz00,
																	BgL_arg1839z00_646);
															}
															BgL_arg1837z00_644 =
																MAKE_YOUNG_PAIR(BGl_string2194z00zzjas_asz00,
																BgL_arg1838z00_645);
														}
														BgL_arg1836z00_643 =
															MAKE_YOUNG_PAIR(BGl_string2195z00zzjas_asz00,
															BgL_arg1837z00_644);
													}
													BgL_arg1835z00_642 =
														MAKE_YOUNG_PAIR(BGl_string2193z00zzjas_asz00,
														BgL_arg1836z00_643);
												}
												BgL_arg1834z00_641 =
													MAKE_YOUNG_PAIR(BgL_arg1808z00_632,
													BgL_arg1835z00_642);
											}
											BgL_arg1833z00_640 =
												MAKE_YOUNG_PAIR(BGl_string2201z00zzjas_asz00,
												BgL_arg1834z00_641);
										}
										BgL_arg1832z00_639 =
											MAKE_YOUNG_PAIR(BGl_string2193z00zzjas_asz00,
											BgL_arg1833z00_640);
									}
									BgL_arg1831z00_638 =
										MAKE_YOUNG_PAIR(BGl_string2190z00zzjas_asz00,
										BgL_arg1832z00_639);
								}
								BgL_arg1823z00_637 =
									MAKE_YOUNG_PAIR(BGl_string2193z00zzjas_asz00,
									BgL_arg1831z00_638);
							}
							BgL_arg1822z00_636 =
								MAKE_YOUNG_PAIR(BGl_za2za2sdefileza2za2z00zzjas_asz00,
								BgL_arg1823z00_637);
						}
						BgL_list1821z00_635 =
							MAKE_YOUNG_PAIR(BGl_string2202z00zzjas_asz00, BgL_arg1822z00_636);
					}
					return
						BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1821z00_635);
				}
			}
		}

	}



/* as-declare */
	obj_t BGl_aszd2declarezd2zzjas_asz00(BgL_classfilez00_bglt
		BgL_classfilez00_16, obj_t BgL_declz00_17)
	{
		{	/* Jas/as.scm 237 */
			{
				obj_t BgL_gnamez00_691;
				obj_t BgL_valuez00_692;

				if (PAIRP(BgL_declz00_17))
					{	/* Jas/as.scm 238 */
						obj_t BgL_cdrzd2226zd2_697;

						BgL_cdrzd2226zd2_697 = CDR(((obj_t) BgL_declz00_17));
						if (PAIRP(BgL_cdrzd2226zd2_697))
							{	/* Jas/as.scm 238 */
								if (NULLP(CDR(BgL_cdrzd2226zd2_697)))
									{	/* Jas/as.scm 238 */
										obj_t BgL_arg1882z00_701;
										obj_t BgL_arg1883z00_702;

										BgL_arg1882z00_701 = CAR(((obj_t) BgL_declz00_17));
										BgL_arg1883z00_702 = CAR(BgL_cdrzd2226zd2_697);
										BgL_gnamez00_691 = BgL_arg1882z00_701;
										BgL_valuez00_692 = BgL_arg1883z00_702;
										{	/* Jas/as.scm 240 */
											obj_t BgL_arg1885z00_704;

											{
												obj_t BgL_classz00_713;
												obj_t BgL_modifiersz00_714;
												obj_t BgL_tretz00_715;
												obj_t BgL_namez00_716;
												obj_t BgL_targsz00_717;
												obj_t BgL_classz00_708;
												obj_t BgL_modifiersz00_709;
												obj_t BgL_typez00_710;
												obj_t BgL_namez00_711;
												obj_t BgL_modifiersz00_705;
												obj_t BgL_namez00_706;

												if (PAIRP(BgL_valuez00_692))
													{	/* Jas/as.scm 240 */
														obj_t BgL_cdrzd2246zd2_722;

														BgL_cdrzd2246zd2_722 =
															CDR(((obj_t) BgL_valuez00_692));
														if (
															(CAR(
																	((obj_t) BgL_valuez00_692)) ==
																CNST_TABLE_REF(12)))
															{	/* Jas/as.scm 240 */
																if (PAIRP(BgL_cdrzd2246zd2_722))
																	{	/* Jas/as.scm 240 */
																		obj_t BgL_cdrzd2250zd2_726;

																		BgL_cdrzd2250zd2_726 =
																			CDR(BgL_cdrzd2246zd2_722);
																		if (PAIRP(BgL_cdrzd2250zd2_726))
																			{	/* Jas/as.scm 240 */
																				obj_t BgL_carzd2253zd2_728;

																				BgL_carzd2253zd2_728 =
																					CAR(BgL_cdrzd2250zd2_726);
																				if (STRINGP(BgL_carzd2253zd2_728))
																					{	/* Jas/as.scm 240 */
																						if (NULLP(CDR
																								(BgL_cdrzd2250zd2_726)))
																							{	/* Jas/as.scm 240 */
																								obj_t BgL_arg1894z00_732;

																								BgL_arg1894z00_732 =
																									CAR(BgL_cdrzd2246zd2_722);
																								{
																									BgL_classez00_bglt
																										BgL_auxz00_2791;
																									BgL_modifiersz00_705 =
																										BgL_arg1894z00_732;
																									BgL_namez00_706 =
																										BgL_carzd2253zd2_728;
																									{	/* Jas/as.scm 243 */
																										obj_t BgL_namezf2zf2_771;

																										BgL_namezf2zf2_771 =
																											BGl_pathnamez00zzjas_asz00
																											(BgL_namez00_706);
																										{	/* Jas/as.scm 244 */
																											BgL_classez00_bglt
																												BgL_new1093z00_772;
																											{	/* Jas/as.scm 245 */
																												BgL_classez00_bglt
																													BgL_new1092z00_773;
																												BgL_new1092z00_773 =
																													((BgL_classez00_bglt)
																													BOBJECT(GC_MALLOC
																														(sizeof(struct
																																BgL_classez00_bgl))));
																												{	/* Jas/as.scm 245 */
																													long
																														BgL_arg1929z00_774;
																													BgL_arg1929z00_774 =
																														BGL_CLASS_NUM
																														(BGl_classez00zzjas_classfilez00);
																													BGL_OBJECT_CLASS_NUM_SET
																														(((BgL_objectz00_bglt) BgL_new1092z00_773), BgL_arg1929z00_774);
																												}
																												BgL_new1093z00_772 =
																													BgL_new1092z00_773;
																											}
																											((((BgL_jastypez00_bglt)
																														COBJECT((
																																(BgL_jastypez00_bglt)
																																BgL_new1093z00_772)))->
																													BgL_codez00) =
																												((obj_t)
																													string_append_3
																													(BGl_string2203z00zzjas_asz00,
																														BgL_namezf2zf2_771,
																														BGl_string2204z00zzjas_asz00)),
																												BUNSPEC);
																											((((BgL_jastypez00_bglt)
																														COBJECT((
																																(BgL_jastypez00_bglt)
																																BgL_new1093z00_772)))->
																													BgL_vectz00) =
																												((obj_t) BFALSE),
																												BUNSPEC);
																											((((BgL_classez00_bglt)
																														COBJECT
																														(BgL_new1093z00_772))->
																													BgL_flagsz00) =
																												((obj_t)
																													BINT
																													(BGl_aszd2classzd2modifiersz00zzjas_asz00
																														(BgL_modifiersz00_705))),
																												BUNSPEC);
																											((((BgL_classez00_bglt)
																														COBJECT
																														(BgL_new1093z00_772))->
																													BgL_namez00) =
																												((obj_t)
																													BgL_namezf2zf2_771),
																												BUNSPEC);
																											((((BgL_classez00_bglt)
																														COBJECT
																														(BgL_new1093z00_772))->
																													BgL_poolz00) =
																												((obj_t) BFALSE),
																												BUNSPEC);
																											BgL_auxz00_2791 =
																												BgL_new1093z00_772;
																									}}
																									BgL_arg1885z00_704 =
																										((obj_t) BgL_auxz00_2791);
																							}}
																						else
																							{	/* Jas/as.scm 240 */
																							BgL_tagzd2235zd2_719:
																								BgL_arg1885z00_704 =
																									BGl_jaszd2errorzd2zzjas_classfilez00
																									(BgL_classfilez00_16,
																									BGl_string2205z00zzjas_asz00,
																									BgL_declz00_17);
																							}
																					}
																				else
																					{	/* Jas/as.scm 240 */
																						goto BgL_tagzd2235zd2_719;
																					}
																			}
																		else
																			{	/* Jas/as.scm 240 */
																				goto BgL_tagzd2235zd2_719;
																			}
																	}
																else
																	{	/* Jas/as.scm 240 */
																		goto BgL_tagzd2235zd2_719;
																	}
															}
														else
															{	/* Jas/as.scm 240 */
																if (
																	(CAR(
																			((obj_t) BgL_valuez00_692)) ==
																		CNST_TABLE_REF(13)))
																	{	/* Jas/as.scm 240 */
																		if (PAIRP(BgL_cdrzd2246zd2_722))
																			{	/* Jas/as.scm 240 */
																				obj_t BgL_cdrzd2355zd2_738;

																				BgL_cdrzd2355zd2_738 =
																					CDR(BgL_cdrzd2246zd2_722);
																				if (PAIRP(BgL_cdrzd2355zd2_738))
																					{	/* Jas/as.scm 240 */
																						obj_t BgL_cdrzd2361zd2_740;

																						BgL_cdrzd2361zd2_740 =
																							CDR(BgL_cdrzd2355zd2_738);
																						if (PAIRP(BgL_cdrzd2361zd2_740))
																							{	/* Jas/as.scm 240 */
																								obj_t BgL_cdrzd2366zd2_742;

																								BgL_cdrzd2366zd2_742 =
																									CDR(BgL_cdrzd2361zd2_740);
																								if (PAIRP(BgL_cdrzd2366zd2_742))
																									{	/* Jas/as.scm 240 */
																										obj_t BgL_carzd2369zd2_744;

																										BgL_carzd2369zd2_744 =
																											CAR(BgL_cdrzd2366zd2_742);
																										if (STRINGP
																											(BgL_carzd2369zd2_744))
																											{	/* Jas/as.scm 240 */
																												if (NULLP(CDR
																														(BgL_cdrzd2366zd2_742)))
																													{	/* Jas/as.scm 240 */
																														obj_t
																															BgL_arg1906z00_748;
																														obj_t
																															BgL_arg1910z00_749;
																														obj_t
																															BgL_arg1911z00_750;
																														BgL_arg1906z00_748 =
																															CAR
																															(BgL_cdrzd2246zd2_722);
																														BgL_arg1910z00_749 =
																															CAR
																															(BgL_cdrzd2355zd2_738);
																														BgL_arg1911z00_750 =
																															CAR
																															(BgL_cdrzd2361zd2_740);
																														{
																															BgL_fieldz00_bglt
																																BgL_auxz00_2834;
																															BgL_classz00_708 =
																																BgL_arg1906z00_748;
																															BgL_modifiersz00_709
																																=
																																BgL_arg1910z00_749;
																															BgL_typez00_710 =
																																BgL_arg1911z00_750;
																															BgL_namez00_711 =
																																BgL_carzd2369zd2_744;
																															{	/* Jas/as.scm 249 */
																																BgL_fieldz00_bglt
																																	BgL_new1095z00_775;
																																{	/* Jas/as.scm 250 */
																																	BgL_fieldz00_bglt
																																		BgL_new1094z00_776;
																																	BgL_new1094z00_776
																																		=
																																		(
																																		(BgL_fieldz00_bglt)
																																		BOBJECT
																																		(GC_MALLOC
																																			(sizeof
																																				(struct
																																					BgL_fieldz00_bgl))));
																																	{	/* Jas/as.scm 250 */
																																		long
																																			BgL_arg1930z00_777;
																																		BgL_arg1930z00_777
																																			=
																																			BGL_CLASS_NUM
																																			(BGl_fieldz00zzjas_classfilez00);
																																		BGL_OBJECT_CLASS_NUM_SET
																																			(((BgL_objectz00_bglt) BgL_new1094z00_776), BgL_arg1930z00_777);
																																	}
																																	BgL_new1095z00_775
																																		=
																																		BgL_new1094z00_776;
																																}
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1095z00_775)))->BgL_flagsz00) = ((obj_t) BINT(BGl_aszd2fieldzd2modifiersz00zzjas_asz00(BgL_modifiersz00_709))), BUNSPEC);
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1095z00_775)))->BgL_namez00) = ((obj_t) BgL_namez00_711), BUNSPEC);
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1095z00_775)))->BgL_ownerz00) = ((obj_t) BgL_classz00_708), BUNSPEC);
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1095z00_775)))->BgL_usertypez00) = ((obj_t) BgL_typez00_710), BUNSPEC);
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1095z00_775)))->BgL_typez00) = ((obj_t) BFALSE), BUNSPEC);
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1095z00_775)))->BgL_pnamez00) = ((obj_t) BFALSE), BUNSPEC);
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1095z00_775)))->BgL_descriptorz00) = ((obj_t) BFALSE), BUNSPEC);
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1095z00_775)))->BgL_poolz00) = ((obj_t) BFALSE), BUNSPEC);
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1095z00_775)))->BgL_attributesz00) = ((obj_t) BNIL), BUNSPEC);
																																BgL_auxz00_2834
																																	=
																																	BgL_new1095z00_775;
																															}
																															BgL_arg1885z00_704
																																=
																																((obj_t)
																																BgL_auxz00_2834);
																													}}
																												else
																													{	/* Jas/as.scm 240 */
																														goto
																															BgL_tagzd2235zd2_719;
																													}
																											}
																										else
																											{	/* Jas/as.scm 240 */
																												goto
																													BgL_tagzd2235zd2_719;
																											}
																									}
																								else
																									{	/* Jas/as.scm 240 */
																										goto BgL_tagzd2235zd2_719;
																									}
																							}
																						else
																							{	/* Jas/as.scm 240 */
																								goto BgL_tagzd2235zd2_719;
																							}
																					}
																				else
																					{	/* Jas/as.scm 240 */
																						goto BgL_tagzd2235zd2_719;
																					}
																			}
																		else
																			{	/* Jas/as.scm 240 */
																				goto BgL_tagzd2235zd2_719;
																			}
																	}
																else
																	{	/* Jas/as.scm 240 */
																		obj_t BgL_cdrzd2445zd2_752;

																		BgL_cdrzd2445zd2_752 =
																			CDR(((obj_t) BgL_valuez00_692));
																		if (
																			(CAR(
																					((obj_t) BgL_valuez00_692)) ==
																				CNST_TABLE_REF(3)))
																			{	/* Jas/as.scm 240 */
																				if (PAIRP(BgL_cdrzd2445zd2_752))
																					{	/* Jas/as.scm 240 */
																						obj_t BgL_cdrzd2452zd2_756;

																						BgL_cdrzd2452zd2_756 =
																							CDR(BgL_cdrzd2445zd2_752);
																						if (PAIRP(BgL_cdrzd2452zd2_756))
																							{	/* Jas/as.scm 240 */
																								obj_t BgL_cdrzd2459zd2_758;

																								BgL_cdrzd2459zd2_758 =
																									CDR(BgL_cdrzd2452zd2_756);
																								if (PAIRP(BgL_cdrzd2459zd2_758))
																									{	/* Jas/as.scm 240 */
																										obj_t BgL_cdrzd2465zd2_760;

																										BgL_cdrzd2465zd2_760 =
																											CDR(BgL_cdrzd2459zd2_758);
																										if (PAIRP
																											(BgL_cdrzd2465zd2_760))
																											{	/* Jas/as.scm 240 */
																												obj_t
																													BgL_carzd2469zd2_762;
																												BgL_carzd2469zd2_762 =
																													CAR
																													(BgL_cdrzd2465zd2_760);
																												if (STRINGP
																													(BgL_carzd2469zd2_762))
																													{	/* Jas/as.scm 240 */
																														obj_t
																															BgL_arg1920z00_764;
																														obj_t
																															BgL_arg1923z00_765;
																														obj_t
																															BgL_arg1924z00_766;
																														obj_t
																															BgL_arg1925z00_767;
																														BgL_arg1920z00_764 =
																															CAR
																															(BgL_cdrzd2445zd2_752);
																														BgL_arg1923z00_765 =
																															CAR
																															(BgL_cdrzd2452zd2_756);
																														BgL_arg1924z00_766 =
																															CAR
																															(BgL_cdrzd2459zd2_758);
																														BgL_arg1925z00_767 =
																															CDR
																															(BgL_cdrzd2465zd2_760);
																														{
																															BgL_methodz00_bglt
																																BgL_auxz00_2885;
																															BgL_classz00_713 =
																																BgL_arg1920z00_764;
																															BgL_modifiersz00_714
																																=
																																BgL_arg1923z00_765;
																															BgL_tretz00_715 =
																																BgL_arg1924z00_766;
																															BgL_namez00_716 =
																																BgL_carzd2469zd2_762;
																															BgL_targsz00_717 =
																																BgL_arg1925z00_767;
																															{	/* Jas/as.scm 255 */
																																BgL_methodz00_bglt
																																	BgL_new1097z00_778;
																																{	/* Jas/as.scm 256 */
																																	BgL_methodz00_bglt
																																		BgL_new1096z00_781;
																																	BgL_new1096z00_781
																																		=
																																		(
																																		(BgL_methodz00_bglt)
																																		BOBJECT
																																		(GC_MALLOC
																																			(sizeof
																																				(struct
																																					BgL_methodz00_bgl))));
																																	{	/* Jas/as.scm 256 */
																																		long
																																			BgL_arg1933z00_782;
																																		BgL_arg1933z00_782
																																			=
																																			BGL_CLASS_NUM
																																			(BGl_methodz00zzjas_classfilez00);
																																		BGL_OBJECT_CLASS_NUM_SET
																																			(((BgL_objectz00_bglt) BgL_new1096z00_781), BgL_arg1933z00_782);
																																	}
																																	BgL_new1097z00_778
																																		=
																																		BgL_new1096z00_781;
																																}
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1097z00_778)))->BgL_flagsz00) = ((obj_t) BINT(BGl_aszd2methodzd2modifiersz00zzjas_asz00(BgL_modifiersz00_714))), BUNSPEC);
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1097z00_778)))->BgL_namez00) = ((obj_t) BgL_namez00_716), BUNSPEC);
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1097z00_778)))->BgL_ownerz00) = ((obj_t) BgL_classz00_713), BUNSPEC);
																																{
																																	obj_t
																																		BgL_auxz00_2898;
																																	{	/* Jas/as.scm 259 */
																																		obj_t
																																			BgL_arg1931z00_779;
																																		{	/* Jas/as.scm 259 */
																																			obj_t
																																				BgL_arg1932z00_780;
																																			BgL_arg1932z00_780
																																				=
																																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																				(BgL_targsz00_717,
																																				BNIL);
																																			BgL_arg1931z00_779
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_tretz00_715,
																																				BgL_arg1932z00_780);
																																		}
																																		BgL_auxz00_2898
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(11),
																																			BgL_arg1931z00_779);
																																	}
																																	((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1097z00_778)))->BgL_usertypez00) = ((obj_t) BgL_auxz00_2898), BUNSPEC);
																																}
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1097z00_778)))->BgL_typez00) = ((obj_t) BFALSE), BUNSPEC);
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1097z00_778)))->BgL_pnamez00) = ((obj_t) BFALSE), BUNSPEC);
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1097z00_778)))->BgL_descriptorz00) = ((obj_t) BFALSE), BUNSPEC);
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1097z00_778)))->BgL_poolz00) = ((obj_t) BFALSE), BUNSPEC);
																																((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(((BgL_fieldzd2orzd2methodz00_bglt) BgL_new1097z00_778)))->BgL_attributesz00) = ((obj_t) BNIL), BUNSPEC);
																																BgL_auxz00_2885
																																	=
																																	BgL_new1097z00_778;
																															}
																															BgL_arg1885z00_704
																																=
																																((obj_t)
																																BgL_auxz00_2885);
																													}}
																												else
																													{	/* Jas/as.scm 240 */
																														goto
																															BgL_tagzd2235zd2_719;
																													}
																											}
																										else
																											{	/* Jas/as.scm 240 */
																												goto
																													BgL_tagzd2235zd2_719;
																											}
																									}
																								else
																									{	/* Jas/as.scm 240 */
																										goto BgL_tagzd2235zd2_719;
																									}
																							}
																						else
																							{	/* Jas/as.scm 240 */
																								goto BgL_tagzd2235zd2_719;
																							}
																					}
																				else
																					{	/* Jas/as.scm 240 */
																						goto BgL_tagzd2235zd2_719;
																					}
																			}
																		else
																			{	/* Jas/as.scm 240 */
																				goto BgL_tagzd2235zd2_719;
																			}
																	}
															}
													}
												else
													{	/* Jas/as.scm 240 */
														goto BgL_tagzd2235zd2_719;
													}
											}
											return
												BGl_aszd2assignzd2zzjas_classfilez00
												(BgL_classfilez00_16, BgL_gnamez00_691,
												BgL_arg1885z00_704);
										}
									}
								else
									{	/* Jas/as.scm 238 */
									BgL_tagzd2219zd2_694:
										return
											BGl_jaszd2errorzd2zzjas_classfilez00(BgL_classfilez00_16,
											BGl_string2205z00zzjas_asz00, BgL_declz00_17);
									}
							}
						else
							{	/* Jas/as.scm 238 */
								goto BgL_tagzd2219zd2_694;
							}
					}
				else
					{	/* Jas/as.scm 238 */
						goto BgL_tagzd2219zd2_694;
					}
			}
		}

	}



/* pathname */
	obj_t BGl_pathnamez00zzjas_asz00(obj_t BgL_strz00_18)
	{
		{	/* Jas/as.scm 263 */
			{	/* Jas/as.scm 264 */
				long BgL_lenz00_783;

				BgL_lenz00_783 = STRING_LENGTH(BgL_strz00_18);
				{	/* Jas/as.scm 264 */
					obj_t BgL_resz00_784;

					{	/* Jas/as.scm 265 */

						BgL_resz00_784 = make_string(BgL_lenz00_783, ((unsigned char) ' '));
					}
					{	/* Jas/as.scm 265 */

						{	/* Jas/as.scm 266 */
							long BgL_g1098z00_785;

							BgL_g1098z00_785 = (BgL_lenz00_783 - 1L);
							{
								long BgL_iz00_787;

								BgL_iz00_787 = BgL_g1098z00_785;
							BgL_zc3z04anonymousza31934ze3z87_788:
								if ((BgL_iz00_787 == -1L))
									{	/* Jas/as.scm 268 */
										return BgL_resz00_784;
									}
								else
									{	/* Jas/as.scm 268 */
										if (
											(STRING_REF(BgL_strz00_18,
													BgL_iz00_787) == ((unsigned char) '.')))
											{	/* Jas/as.scm 270 */
												STRING_SET(BgL_resz00_784, BgL_iz00_787,
													((unsigned char) '/'));
												{
													long BgL_iz00_2927;

													BgL_iz00_2927 = (BgL_iz00_787 - 1L);
													BgL_iz00_787 = BgL_iz00_2927;
													goto BgL_zc3z04anonymousza31934ze3z87_788;
												}
											}
										else
											{	/* Jas/as.scm 270 */
												{	/* Jas/as.scm 274 */
													unsigned char BgL_tmpz00_2929;

													BgL_tmpz00_2929 =
														STRING_REF(BgL_strz00_18, BgL_iz00_787);
													STRING_SET(BgL_resz00_784, BgL_iz00_787,
														BgL_tmpz00_2929);
												}
												{
													long BgL_iz00_2932;

													BgL_iz00_2932 = (BgL_iz00_787 - 1L);
													BgL_iz00_787 = BgL_iz00_2932;
													goto BgL_zc3z04anonymousza31934ze3z87_788;
												}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* as-class-modifiers */
	long BGl_aszd2classzd2modifiersz00zzjas_asz00(obj_t BgL_modifiersz00_19)
	{
		{	/* Jas/as.scm 278 */
			{	/* Jas/as.scm 279 */
				long BgL_rz00_799;

				BgL_rz00_799 = 0L;
				{
					obj_t BgL_l1285z00_801;

					BgL_l1285z00_801 = BgL_modifiersz00_19;
				BgL_zc3z04anonymousza31942ze3z87_802:
					if (PAIRP(BgL_l1285z00_801))
						{	/* Jas/as.scm 280 */
							{	/* Jas/as.scm 282 */
								obj_t BgL_namez00_804;

								BgL_namez00_804 = CAR(BgL_l1285z00_801);
								if ((BgL_namez00_804 == CNST_TABLE_REF(14)))
									{	/* Jas/as.scm 282 */
										BgL_rz00_799 = (BgL_rz00_799 | 1L);
									}
								else
									{	/* Jas/as.scm 282 */
										if ((BgL_namez00_804 == CNST_TABLE_REF(15)))
											{	/* Jas/as.scm 282 */
												BgL_rz00_799 = (BgL_rz00_799 | 16L);
											}
										else
											{	/* Jas/as.scm 282 */
												if ((BgL_namez00_804 == CNST_TABLE_REF(16)))
													{	/* Jas/as.scm 282 */
														BgL_rz00_799 = (BgL_rz00_799 | 32L);
													}
												else
													{	/* Jas/as.scm 282 */
														if ((BgL_namez00_804 == CNST_TABLE_REF(17)))
															{	/* Jas/as.scm 282 */
																BgL_rz00_799 = (BgL_rz00_799 | 512L);
															}
														else
															{	/* Jas/as.scm 282 */
																if ((BgL_namez00_804 == CNST_TABLE_REF(18)))
																	{	/* Jas/as.scm 282 */
																		BgL_rz00_799 = (BgL_rz00_799 | 1024L);
																	}
																else
																	{	/* Jas/as.scm 282 */
																		BGl_errorz00zz__errorz00
																			(BGl_string2206z00zzjas_asz00,
																			BGl_string2207z00zzjas_asz00,
																			BgL_namez00_804);
																	}
															}
													}
											}
									}
							}
							{
								obj_t BgL_l1285z00_2958;

								BgL_l1285z00_2958 = CDR(BgL_l1285z00_801);
								BgL_l1285z00_801 = BgL_l1285z00_2958;
								goto BgL_zc3z04anonymousza31942ze3z87_802;
							}
						}
					else
						{	/* Jas/as.scm 280 */
							((bool_t) 1);
						}
				}
				return BgL_rz00_799;
			}
		}

	}



/* as-field-modifiers */
	long BGl_aszd2fieldzd2modifiersz00zzjas_asz00(obj_t BgL_modifiersz00_20)
	{
		{	/* Jas/as.scm 292 */
			{	/* Jas/as.scm 293 */
				long BgL_rz00_814;

				BgL_rz00_814 = 0L;
				{
					obj_t BgL_l1287z00_816;

					BgL_l1287z00_816 = BgL_modifiersz00_20;
				BgL_zc3z04anonymousza31950ze3z87_817:
					if (PAIRP(BgL_l1287z00_816))
						{	/* Jas/as.scm 294 */
							{	/* Jas/as.scm 296 */
								obj_t BgL_namez00_819;

								BgL_namez00_819 = CAR(BgL_l1287z00_816);
								if ((BgL_namez00_819 == CNST_TABLE_REF(14)))
									{	/* Jas/as.scm 296 */
										BgL_rz00_814 = (BgL_rz00_814 | 1L);
									}
								else
									{	/* Jas/as.scm 296 */
										if ((BgL_namez00_819 == CNST_TABLE_REF(19)))
											{	/* Jas/as.scm 296 */
												BgL_rz00_814 = (BgL_rz00_814 | 2L);
											}
										else
											{	/* Jas/as.scm 296 */
												if ((BgL_namez00_819 == CNST_TABLE_REF(20)))
													{	/* Jas/as.scm 296 */
														BgL_rz00_814 = (BgL_rz00_814 | 4L);
													}
												else
													{	/* Jas/as.scm 296 */
														if ((BgL_namez00_819 == CNST_TABLE_REF(21)))
															{	/* Jas/as.scm 296 */
																BgL_rz00_814 = (BgL_rz00_814 | 8L);
															}
														else
															{	/* Jas/as.scm 296 */
																if ((BgL_namez00_819 == CNST_TABLE_REF(15)))
																	{	/* Jas/as.scm 296 */
																		BgL_rz00_814 = (BgL_rz00_814 | 16L);
																	}
																else
																	{	/* Jas/as.scm 296 */
																		if ((BgL_namez00_819 == CNST_TABLE_REF(22)))
																			{	/* Jas/as.scm 296 */
																				BgL_rz00_814 = (BgL_rz00_814 | 64L);
																			}
																		else
																			{	/* Jas/as.scm 296 */
																				if (
																					(BgL_namez00_819 ==
																						CNST_TABLE_REF(23)))
																					{	/* Jas/as.scm 296 */
																						BgL_rz00_814 =
																							(BgL_rz00_814 | 128L);
																					}
																				else
																					{	/* Jas/as.scm 296 */
																						BGl_errorz00zz__errorz00
																							(BGl_string2206z00zzjas_asz00,
																							BGl_string2208z00zzjas_asz00,
																							BgL_namez00_819);
																					}
																			}
																	}
															}
													}
											}
									}
							}
							{
								obj_t BgL_l1287z00_2992;

								BgL_l1287z00_2992 = CDR(BgL_l1287z00_816);
								BgL_l1287z00_816 = BgL_l1287z00_2992;
								goto BgL_zc3z04anonymousza31950ze3z87_817;
							}
						}
					else
						{	/* Jas/as.scm 294 */
							((bool_t) 1);
						}
				}
				return BgL_rz00_814;
			}
		}

	}



/* as-method-modifiers */
	long BGl_aszd2methodzd2modifiersz00zzjas_asz00(obj_t BgL_modifiersz00_21)
	{
		{	/* Jas/as.scm 308 */
			{	/* Jas/as.scm 309 */
				long BgL_rz00_831;

				BgL_rz00_831 = 0L;
				{
					obj_t BgL_l1289z00_833;

					BgL_l1289z00_833 = BgL_modifiersz00_21;
				BgL_zc3z04anonymousza31960ze3z87_834:
					if (PAIRP(BgL_l1289z00_833))
						{	/* Jas/as.scm 310 */
							{	/* Jas/as.scm 312 */
								obj_t BgL_namez00_836;

								BgL_namez00_836 = CAR(BgL_l1289z00_833);
								if ((BgL_namez00_836 == CNST_TABLE_REF(14)))
									{	/* Jas/as.scm 312 */
										BgL_rz00_831 = (BgL_rz00_831 | 1L);
									}
								else
									{	/* Jas/as.scm 312 */
										if ((BgL_namez00_836 == CNST_TABLE_REF(19)))
											{	/* Jas/as.scm 312 */
												BgL_rz00_831 = (BgL_rz00_831 | 2L);
											}
										else
											{	/* Jas/as.scm 312 */
												if ((BgL_namez00_836 == CNST_TABLE_REF(20)))
													{	/* Jas/as.scm 312 */
														BgL_rz00_831 = (BgL_rz00_831 | 4L);
													}
												else
													{	/* Jas/as.scm 312 */
														if ((BgL_namez00_836 == CNST_TABLE_REF(21)))
															{	/* Jas/as.scm 312 */
																BgL_rz00_831 = (BgL_rz00_831 | 8L);
															}
														else
															{	/* Jas/as.scm 312 */
																if ((BgL_namez00_836 == CNST_TABLE_REF(15)))
																	{	/* Jas/as.scm 312 */
																		BgL_rz00_831 = (BgL_rz00_831 | 16L);
																	}
																else
																	{	/* Jas/as.scm 312 */
																		if ((BgL_namez00_836 == CNST_TABLE_REF(24)))
																			{	/* Jas/as.scm 312 */
																				BgL_rz00_831 = (BgL_rz00_831 | 32L);
																			}
																		else
																			{	/* Jas/as.scm 312 */
																				if (
																					(BgL_namez00_836 ==
																						CNST_TABLE_REF(25)))
																					{	/* Jas/as.scm 312 */
																						BgL_rz00_831 =
																							(BgL_rz00_831 | 256L);
																					}
																				else
																					{	/* Jas/as.scm 312 */
																						if (
																							(BgL_namez00_836 ==
																								CNST_TABLE_REF(18)))
																							{	/* Jas/as.scm 312 */
																								BgL_rz00_831 =
																									(BgL_rz00_831 | 1024L);
																							}
																						else
																							{	/* Jas/as.scm 312 */
																								BGl_errorz00zz__errorz00
																									(BGl_string2206z00zzjas_asz00,
																									BGl_string2207z00zzjas_asz00,
																									BgL_namez00_836);
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
							{
								obj_t BgL_l1289z00_3030;

								BgL_l1289z00_3030 = CDR(BgL_l1289z00_833);
								BgL_l1289z00_833 = BgL_l1289z00_3030;
								goto BgL_zc3z04anonymousza31960ze3z87_834;
							}
						}
					else
						{	/* Jas/as.scm 310 */
							((bool_t) 1);
						}
				}
				return BgL_rz00_831;
			}
		}

	}



/* as-method */
	obj_t BGl_aszd2methodzd2zzjas_asz00(BgL_classfilez00_bglt BgL_classfilez00_24,
		obj_t BgL_declz00_25)
	{
		{	/* Jas/as.scm 338 */
			{
				obj_t BgL_gnamez00_850;
				obj_t BgL_paramsz00_851;
				obj_t BgL_localsz00_852;
				obj_t BgL_codez00_853;

				if (PAIRP(BgL_declz00_25))
					{	/* Jas/as.scm 339 */
						obj_t BgL_cdrzd2507zd2_858;

						BgL_cdrzd2507zd2_858 = CDR(((obj_t) BgL_declz00_25));
						if ((CAR(((obj_t) BgL_declz00_25)) == CNST_TABLE_REF(3)))
							{	/* Jas/as.scm 339 */
								if (PAIRP(BgL_cdrzd2507zd2_858))
									{	/* Jas/as.scm 339 */
										obj_t BgL_cdrzd2513zd2_862;

										BgL_cdrzd2513zd2_862 = CDR(BgL_cdrzd2507zd2_858);
										if (PAIRP(BgL_cdrzd2513zd2_862))
											{	/* Jas/as.scm 339 */
												obj_t BgL_cdrzd2519zd2_864;

												BgL_cdrzd2519zd2_864 = CDR(BgL_cdrzd2513zd2_862);
												if (PAIRP(BgL_cdrzd2519zd2_864))
													{	/* Jas/as.scm 339 */
														obj_t BgL_arg1977z00_866;
														obj_t BgL_arg1978z00_867;
														obj_t BgL_arg1979z00_868;
														obj_t BgL_arg1980z00_869;

														BgL_arg1977z00_866 = CAR(BgL_cdrzd2507zd2_858);
														BgL_arg1978z00_867 = CAR(BgL_cdrzd2513zd2_862);
														BgL_arg1979z00_868 = CAR(BgL_cdrzd2519zd2_864);
														BgL_arg1980z00_869 = CDR(BgL_cdrzd2519zd2_864);
														{
															BgL_methodz00_bglt BgL_auxz00_3053;

															BgL_gnamez00_850 = BgL_arg1977z00_866;
															BgL_paramsz00_851 = BgL_arg1978z00_867;
															BgL_localsz00_852 = BgL_arg1979z00_868;
															BgL_codez00_853 = BgL_arg1980z00_869;
															((((BgL_classfilez00_bglt)
																		COBJECT(BgL_classfilez00_24))->
																	BgL_currentzd2methodzd2) =
																((obj_t) BgL_gnamez00_850), BUNSPEC);
															{	/* Jas/as.scm 342 */
																BgL_methodz00_bglt BgL_mz00_871;

																BgL_mz00_871 =
																	BGl_declaredzd2methodzd2zzjas_classfilez00
																	(BgL_classfilez00_24, BgL_gnamez00_850);
																BGl_poolzd2localzd2methodz00zzjas_classfilez00
																	(BgL_classfilez00_24, BgL_mz00_871);
																{
																	obj_t BgL_auxz00_3057;

																	{	/* Jas/as.scm 345 */
																		BgL_attributez00_bglt BgL_arg1982z00_873;
																		obj_t BgL_arg1983z00_874;

																		BgL_arg1982z00_873 =
																			BGl_aszd2codezd2zzjas_asz00
																			(BgL_classfilez00_24, BgL_paramsz00_851,
																			BgL_localsz00_852, BgL_codez00_853);
																		BgL_arg1983z00_874 =
																			(((BgL_fieldzd2orzd2methodz00_bglt)
																				COBJECT((
																						(BgL_fieldzd2orzd2methodz00_bglt)
																						BgL_mz00_871)))->BgL_attributesz00);
																		BgL_auxz00_3057 =
																			MAKE_YOUNG_PAIR(((obj_t)
																				BgL_arg1982z00_873),
																			BgL_arg1983z00_874);
																	}
																	((((BgL_fieldzd2orzd2methodz00_bglt) COBJECT(
																					((BgL_fieldzd2orzd2methodz00_bglt)
																						BgL_mz00_871)))->
																			BgL_attributesz00) =
																		((obj_t) BgL_auxz00_3057), BUNSPEC);
																}
																BgL_auxz00_3053 = BgL_mz00_871;
															}
															return ((obj_t) BgL_auxz00_3053);
														}
													}
												else
													{	/* Jas/as.scm 339 */
													BgL_tagzd2496zd2_855:
														return
															BGl_errorz00zz__errorz00
															(BGl_string2206z00zzjas_asz00,
															BGl_string2209z00zzjas_asz00, BgL_declz00_25);
													}
											}
										else
											{	/* Jas/as.scm 339 */
												goto BgL_tagzd2496zd2_855;
											}
									}
								else
									{	/* Jas/as.scm 339 */
										goto BgL_tagzd2496zd2_855;
									}
							}
						else
							{	/* Jas/as.scm 339 */
								goto BgL_tagzd2496zd2_855;
							}
					}
				else
					{	/* Jas/as.scm 339 */
						goto BgL_tagzd2496zd2_855;
					}
			}
		}

	}



/* as-code */
	BgL_attributez00_bglt BGl_aszd2codezd2zzjas_asz00(BgL_classfilez00_bglt
		BgL_classfilez00_26, obj_t BgL_paramz00_27, obj_t BgL_localsz00_28,
		obj_t BgL_codez00_29)
	{
		{	/* Jas/as.scm 350 */
			{	/* Jas/as.scm 351 */
				obj_t BgL_l1z00_875;

				BgL_l1z00_875 =
					BGL_PROCEDURE_CALL4(BGl_resolvezd2opcodeszd2zzjas_opcodez00,
					((obj_t) BgL_classfilez00_26), BgL_paramz00_27, BgL_localsz00_28,
					BgL_codez00_29);
				{	/* Jas/as.scm 351 */
					obj_t BgL_lpz00_876;

					BgL_lpz00_876 =
						BGL_PROCEDURE_CALL4(BGl_peepz00zzjas_peepz00,
						((obj_t) BgL_classfilez00_26), BgL_paramz00_27, BgL_localsz00_28,
						BgL_l1z00_875);
					{	/* Jas/as.scm 352 */
						obj_t BgL_lwz00_877;

						BgL_lwz00_877 =
							BGL_PROCEDURE_CALL2(BGl_resolvezd2widezd2zzjas_widez00,
							((obj_t) BgL_classfilez00_26), BgL_lpz00_876);
						{	/* Jas/as.scm 353 */
							obj_t BgL_l3z00_878;

							BgL_l3z00_878 =
								BGL_PROCEDURE_CALL2(BGl_resolvezd2labelszd2zzjas_labelsz00,
								((obj_t) BgL_classfilez00_26), BgL_lwz00_877);
							{	/* Jas/as.scm 354 */
								obj_t BgL_handlersz00_879;

								BgL_handlersz00_879 =
									BGl_getzd2handlerszd2zzjas_asz00(BgL_l3z00_878);
								{	/* Jas/as.scm 355 */
									obj_t BgL_lines0z00_880;

									BgL_lines0z00_880 =
										BGl_getzd2lineszd2zzjas_asz00(BgL_l3z00_878, 0L);
									{	/* Jas/as.scm 356 */
										obj_t BgL_linesz00_881;

										if (CBOOL(BGl_za2jvmzd2charzd2infoza2z00zzjas_asz00))
											{	/* Jas/as.scm 357 */
												BgL_linesz00_881 = BgL_lines0z00_880;
											}
										else
											{	/* Jas/as.scm 357 */
												BgL_linesz00_881 =
													BGl_linezd2compresszd2zzjas_asz00(BgL_lines0z00_880,
													BINT(-1L));
											}
										{	/* Jas/as.scm 357 */
											obj_t BgL_localvarsz00_882;

											BgL_localvarsz00_882 =
												BGl_getzd2localvarszd2zzjas_asz00(BgL_l3z00_878);
											{	/* Jas/as.scm 360 */
												obj_t BgL_bytecodez00_883;

												BgL_bytecodez00_883 =
													BGl_getzd2bytecodezd2zzjas_asz00(BgL_l3z00_878);
												{	/* Jas/as.scm 361 */

													BGl_za2za2allzd2linesza2za2zd2zzjas_asz00 =
														BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00
														(BGl_za2za2allzd2linesza2za2zd2zzjas_asz00,
														BgL_lines0z00_880);
													{	/* Jas/as.scm 363 */
														long BgL_nz00_884;

														BgL_nz00_884 = bgl_list_length(BgL_bytecodez00_883);
														{	/* Jas/as.scm 364 */
															bool_t BgL_test2400z00_3105;

															if ((BgL_nz00_884 >= 8000L))
																{	/* Jas/as.scm 364 */
																	if (CBOOL
																		(BGl_za2jaszd2warningza2zd2zzjas_asz00))
																		{	/* Jas/as.scm 364 */
																			BgL_test2400z00_3105 = ((bool_t) 0);
																		}
																	else
																		{	/* Jas/as.scm 364 */
																			BgL_test2400z00_3105 = ((bool_t) 1);
																		}
																}
															else
																{	/* Jas/as.scm 364 */
																	BgL_test2400z00_3105 = ((bool_t) 0);
																}
															if (BgL_test2400z00_3105)
																{	/* Jas/as.scm 365 */
																	obj_t BgL_arg1986z00_887;

																	BgL_arg1986z00_887 =
																		(((BgL_classfilez00_bglt)
																			COBJECT(BgL_classfilez00_26))->
																		BgL_currentzd2methodzd2);
																	{	/* Jas/as.scm 365 */
																		obj_t BgL_list1987z00_888;

																		{	/* Jas/as.scm 365 */
																			obj_t BgL_arg1988z00_889;

																			{	/* Jas/as.scm 365 */
																				obj_t BgL_arg1989z00_890;

																				{	/* Jas/as.scm 365 */
																					obj_t BgL_arg1990z00_891;

																					{	/* Jas/as.scm 365 */
																						obj_t BgL_arg1991z00_892;

																						BgL_arg1991z00_892 =
																							MAKE_YOUNG_PAIR
																							(BGl_string2210z00zzjas_asz00,
																							BNIL);
																						BgL_arg1990z00_891 =
																							MAKE_YOUNG_PAIR
																							(BGl_string2211z00zzjas_asz00,
																							BgL_arg1991z00_892);
																					}
																					BgL_arg1989z00_890 =
																						MAKE_YOUNG_PAIR(BINT(BgL_nz00_884),
																						BgL_arg1990z00_891);
																				}
																				BgL_arg1988z00_889 =
																					MAKE_YOUNG_PAIR
																					(BGl_string2212z00zzjas_asz00,
																					BgL_arg1989z00_890);
																			}
																			BgL_list1987z00_888 =
																				MAKE_YOUNG_PAIR(BgL_arg1986z00_887,
																				BgL_arg1988z00_889);
																		}
																		BGl_warningz00zz__errorz00
																			(BgL_list1987z00_888);
																	}
																}
															else
																{	/* Jas/as.scm 364 */
																	BFALSE;
																}
														}
													}
													if (NULLP(BgL_linesz00_881))
														{	/* Jas/as.scm 369 */
															obj_t BgL_arg1993z00_895;

															{	/* Jas/as.scm 369 */
																obj_t BgL_list1994z00_896;

																{	/* Jas/as.scm 369 */
																	obj_t BgL_arg1995z00_897;

																	{	/* Jas/as.scm 369 */
																		obj_t BgL_arg1996z00_898;

																		BgL_arg1996z00_898 =
																			MAKE_YOUNG_PAIR(BINT(0L), BNIL);
																		BgL_arg1995z00_897 =
																			MAKE_YOUNG_PAIR(BINT(0L),
																			BgL_arg1996z00_898);
																	}
																	BgL_list1994z00_896 =
																		MAKE_YOUNG_PAIR(BINT(0L),
																		BgL_arg1995z00_897);
																}
																BgL_arg1993z00_895 = BgL_list1994z00_896;
															}
															BgL_linesz00_881 =
																MAKE_YOUNG_PAIR(BgL_arg1993z00_895, BNIL);
														}
													else
														{	/* Jas/as.scm 369 */
															BFALSE;
														}
													{	/* Jas/as.scm 370 */
														BgL_attributez00_bglt BgL_new1101z00_899;

														{	/* Jas/as.scm 371 */
															BgL_attributez00_bglt BgL_new1100z00_941;

															BgL_new1100z00_941 =
																((BgL_attributez00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_attributez00_bgl))));
															{	/* Jas/as.scm 371 */
																long BgL_arg2020z00_942;

																BgL_arg2020z00_942 =
																	BGL_CLASS_NUM
																	(BGl_attributez00zzjas_classfilez00);
																BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																		BgL_new1100z00_941), BgL_arg2020z00_942);
															}
															BgL_new1101z00_899 = BgL_new1100z00_941;
														}
														((((BgL_attributez00_bglt)
																	COBJECT(BgL_new1101z00_899))->BgL_typez00) =
															((obj_t) CNST_TABLE_REF(26)), BUNSPEC);
														((((BgL_attributez00_bglt)
																	COBJECT(BgL_new1101z00_899))->BgL_namez00) =
															((obj_t)
																BINT(BGl_poolzd2namezd2zzjas_classfilez00
																	(BgL_classfilez00_26,
																		BGl_string2213z00zzjas_asz00))), BUNSPEC);
														{
															obj_t BgL_auxz00_3136;

															{	/* Jas/as.scm 373 */
																obj_t BgL_b1308z00_901;

																{	/* Jas/as.scm 373 */
																	long BgL_a1305z00_903;

																	BgL_a1305z00_903 =
																		bgl_list_length(BgL_bytecodez00_883);
																	{	/* Jas/as.scm 373 */
																		obj_t BgL_b1306z00_904;

																		{	/* Jas/as.scm 373 */
																			obj_t BgL_a1303z00_906;

																			{	/* Jas/as.scm 373 */
																				long BgL_b1292z00_927;

																				BgL_b1292z00_927 =
																					bgl_list_length(BgL_handlersz00_879);
																				{	/* Jas/as.scm 373 */

																					{	/* Jas/as.scm 373 */
																						obj_t BgL_za71za7_1661;
																						obj_t BgL_za72za7_1662;

																						BgL_za71za7_1661 = BINT(8L);
																						BgL_za72za7_1662 =
																							BINT(BgL_b1292z00_927);
																						{	/* Jas/as.scm 373 */
																							obj_t BgL_tmpz00_1663;

																							BgL_tmpz00_1663 = BINT(0L);
																							{	/* Jas/as.scm 373 */
																								bool_t BgL_test2404z00_3142;

																								{	/* Jas/as.scm 373 */
																									long BgL_tmpz00_3143;

																									BgL_tmpz00_3143 =
																										(long)
																										CINT(BgL_za72za7_1662);
																									BgL_test2404z00_3142 =
																										BGL_MULFX_OV
																										(BgL_za71za7_1661,
																										BgL_tmpz00_3143,
																										BgL_tmpz00_1663);
																								}
																								if (BgL_test2404z00_3142)
																									{	/* Jas/as.scm 373 */
																										BgL_a1303z00_906 =
																											bgl_bignum_mul
																											(bgl_long_to_bignum((long)
																												CINT(BgL_za71za7_1661)),
																											bgl_long_to_bignum((long)
																												CINT
																												(BgL_za72za7_1662)));
																									}
																								else
																									{	/* Jas/as.scm 373 */
																										BgL_a1303z00_906 =
																											BgL_tmpz00_1663;
																									}
																							}
																						}
																					}
																				}
																			}
																			{	/* Jas/as.scm 374 */
																				obj_t BgL_b1304z00_907;

																				{	/* Jas/as.scm 374 */
																					obj_t BgL_a1301z00_909;

																					{	/* Jas/as.scm 374 */
																						bool_t BgL_test2405z00_3151;

																						{	/* Jas/as.scm 374 */
																							obj_t BgL_objz00_1671;

																							BgL_objz00_1671 =
																								BgL_linesz00_881;
																							BgL_test2405z00_3151 =
																								NULLP(BgL_objz00_1671);
																						}
																						if (BgL_test2405z00_3151)
																							{	/* Jas/as.scm 374 */
																								BgL_a1301z00_909 = BINT(0L);
																							}
																						else
																							{	/* Jas/as.scm 374 */
																								obj_t BgL_b1296z00_921;

																								{	/* Jas/as.scm 374 */
																									long BgL_b1294z00_924;

																									BgL_b1294z00_924 =
																										bgl_list_length
																										(BgL_linesz00_881);
																									{	/* Jas/as.scm 374 */

																										{	/* Jas/as.scm 374 */
																											obj_t BgL_za71za7_1672;
																											obj_t BgL_za72za7_1673;

																											BgL_za71za7_1672 =
																												BINT(4L);
																											BgL_za72za7_1673 =
																												BINT(BgL_b1294z00_924);
																											{	/* Jas/as.scm 374 */
																												obj_t BgL_tmpz00_1674;

																												BgL_tmpz00_1674 =
																													BINT(0L);
																												{	/* Jas/as.scm 374 */
																													bool_t
																														BgL_test2406z00_3158;
																													{	/* Jas/as.scm 374 */
																														long
																															BgL_tmpz00_3159;
																														BgL_tmpz00_3159 =
																															(long)
																															CINT
																															(BgL_za72za7_1673);
																														BgL_test2406z00_3158
																															=
																															BGL_MULFX_OV
																															(BgL_za71za7_1672,
																															BgL_tmpz00_3159,
																															BgL_tmpz00_1674);
																													}
																													if (BgL_test2406z00_3158)
																														{	/* Jas/as.scm 374 */
																															BgL_b1296z00_921 =
																																bgl_bignum_mul
																																(bgl_long_to_bignum
																																((long)
																																	CINT
																																	(BgL_za71za7_1672)),
																																bgl_long_to_bignum
																																((long)
																																	CINT
																																	(BgL_za72za7_1673)));
																														}
																													else
																														{	/* Jas/as.scm 374 */
																															BgL_b1296z00_921 =
																																BgL_tmpz00_1674;
																														}
																												}
																											}
																										}
																									}
																								}
																								{	/* Jas/as.scm 374 */

																									if (INTEGERP
																										(BgL_b1296z00_921))
																										{	/* Jas/as.scm 374 */
																											obj_t BgL_za71za7_1682;

																											BgL_za71za7_1682 =
																												BINT(8L);
																											{	/* Jas/as.scm 374 */
																												obj_t BgL_tmpz00_1684;

																												BgL_tmpz00_1684 =
																													BINT(0L);
																												if (BGL_ADDFX_OV
																													(BgL_za71za7_1682,
																														BgL_b1296z00_921,
																														BgL_tmpz00_1684))
																													{	/* Jas/as.scm 374 */
																														BgL_a1301z00_909 =
																															bgl_bignum_add
																															(bgl_long_to_bignum
																															((long)
																																CINT
																																(BgL_za71za7_1682)),
																															bgl_long_to_bignum
																															((long)
																																CINT
																																(BgL_b1296z00_921)));
																													}
																												else
																													{	/* Jas/as.scm 374 */
																														BgL_a1301z00_909 =
																															BgL_tmpz00_1684;
																													}
																											}
																										}
																									else
																										{	/* Jas/as.scm 374 */
																											BgL_a1301z00_909 =
																												BGl_2zb2zb2zz__r4_numbers_6_5z00
																												(BINT(8L),
																												BgL_b1296z00_921);
																										}
																								}
																							}
																					}
																					{	/* Jas/as.scm 375 */
																						obj_t BgL_b1302z00_910;

																						if (NULLP(BgL_localvarsz00_882))
																							{	/* Jas/as.scm 375 */
																								BgL_b1302z00_910 = BINT(0L);
																							}
																						else
																							{	/* Jas/as.scm 375 */
																								obj_t BgL_b1300z00_914;

																								{	/* Jas/as.scm 375 */
																									long BgL_b1298z00_917;

																									BgL_b1298z00_917 =
																										bgl_list_length
																										(BgL_localvarsz00_882);
																									{	/* Jas/as.scm 375 */

																										{	/* Jas/as.scm 375 */
																											obj_t BgL_za71za7_1692;
																											obj_t BgL_za72za7_1693;

																											BgL_za71za7_1692 =
																												BINT(10L);
																											BgL_za72za7_1693 =
																												BINT(BgL_b1298z00_917);
																											{	/* Jas/as.scm 375 */
																												obj_t BgL_tmpz00_1694;

																												BgL_tmpz00_1694 =
																													BINT(0L);
																												{	/* Jas/as.scm 375 */
																													bool_t
																														BgL_test2410z00_3187;
																													{	/* Jas/as.scm 375 */
																														long
																															BgL_tmpz00_3188;
																														BgL_tmpz00_3188 =
																															(long)
																															CINT
																															(BgL_za72za7_1693);
																														BgL_test2410z00_3187
																															=
																															BGL_MULFX_OV
																															(BgL_za71za7_1692,
																															BgL_tmpz00_3188,
																															BgL_tmpz00_1694);
																													}
																													if (BgL_test2410z00_3187)
																														{	/* Jas/as.scm 375 */
																															BgL_b1300z00_914 =
																																bgl_bignum_mul
																																(bgl_long_to_bignum
																																((long)
																																	CINT
																																	(BgL_za71za7_1692)),
																																bgl_long_to_bignum
																																((long)
																																	CINT
																																	(BgL_za72za7_1693)));
																														}
																													else
																														{	/* Jas/as.scm 375 */
																															BgL_b1300z00_914 =
																																BgL_tmpz00_1694;
																														}
																												}
																											}
																										}
																									}
																								}
																								{	/* Jas/as.scm 375 */

																									if (INTEGERP
																										(BgL_b1300z00_914))
																										{	/* Jas/as.scm 375 */
																											obj_t BgL_za71za7_1702;

																											BgL_za71za7_1702 =
																												BINT(8L);
																											{	/* Jas/as.scm 375 */
																												obj_t BgL_tmpz00_1704;

																												BgL_tmpz00_1704 =
																													BINT(0L);
																												if (BGL_ADDFX_OV
																													(BgL_za71za7_1702,
																														BgL_b1300z00_914,
																														BgL_tmpz00_1704))
																													{	/* Jas/as.scm 375 */
																														BgL_b1302z00_910 =
																															bgl_bignum_add
																															(bgl_long_to_bignum
																															((long)
																																CINT
																																(BgL_za71za7_1702)),
																															bgl_long_to_bignum
																															((long)
																																CINT
																																(BgL_b1300z00_914)));
																													}
																												else
																													{	/* Jas/as.scm 375 */
																														BgL_b1302z00_910 =
																															BgL_tmpz00_1704;
																													}
																											}
																										}
																									else
																										{	/* Jas/as.scm 375 */
																											BgL_b1302z00_910 =
																												BGl_2zb2zb2zz__r4_numbers_6_5z00
																												(BINT(8L),
																												BgL_b1300z00_914);
																										}
																								}
																							}
																						{	/* Jas/as.scm 373 */

																							{	/* Jas/as.scm 373 */
																								bool_t BgL_test2413z00_3209;

																								if (INTEGERP(BgL_a1301z00_909))
																									{	/* Jas/as.scm 373 */
																										BgL_test2413z00_3209 =
																											INTEGERP
																											(BgL_b1302z00_910);
																									}
																								else
																									{	/* Jas/as.scm 373 */
																										BgL_test2413z00_3209 =
																											((bool_t) 0);
																									}
																								if (BgL_test2413z00_3209)
																									{	/* Jas/as.scm 373 */
																										obj_t BgL_tmpz00_1714;

																										BgL_tmpz00_1714 = BINT(0L);
																										if (BGL_ADDFX_OV
																											(BgL_a1301z00_909,
																												BgL_b1302z00_910,
																												BgL_tmpz00_1714))
																											{	/* Jas/as.scm 373 */
																												BgL_b1304z00_907 =
																													bgl_bignum_add
																													(bgl_long_to_bignum(
																														(long)
																														CINT
																														(BgL_a1301z00_909)),
																													bgl_long_to_bignum(
																														(long)
																														CINT
																														(BgL_b1302z00_910)));
																											}
																										else
																											{	/* Jas/as.scm 373 */
																												BgL_b1304z00_907 =
																													BgL_tmpz00_1714;
																											}
																									}
																								else
																									{	/* Jas/as.scm 373 */
																										BgL_b1304z00_907 =
																											BGl_2zb2zb2zz__r4_numbers_6_5z00
																											(BgL_a1301z00_909,
																											BgL_b1302z00_910);
																									}
																							}
																						}
																					}
																				}
																				{	/* Jas/as.scm 373 */

																					{	/* Jas/as.scm 373 */
																						bool_t BgL_test2416z00_3222;

																						if (INTEGERP(BgL_a1303z00_906))
																							{	/* Jas/as.scm 373 */
																								BgL_test2416z00_3222 =
																									INTEGERP(BgL_b1304z00_907);
																							}
																						else
																							{	/* Jas/as.scm 373 */
																								BgL_test2416z00_3222 =
																									((bool_t) 0);
																							}
																						if (BgL_test2416z00_3222)
																							{	/* Jas/as.scm 373 */
																								obj_t BgL_tmpz00_1724;

																								BgL_tmpz00_1724 = BINT(0L);
																								if (BGL_ADDFX_OV
																									(BgL_a1303z00_906,
																										BgL_b1304z00_907,
																										BgL_tmpz00_1724))
																									{	/* Jas/as.scm 373 */
																										BgL_b1306z00_904 =
																											bgl_bignum_add
																											(bgl_long_to_bignum((long)
																												CINT(BgL_a1303z00_906)),
																											bgl_long_to_bignum((long)
																												CINT
																												(BgL_b1304z00_907)));
																									}
																								else
																									{	/* Jas/as.scm 373 */
																										BgL_b1306z00_904 =
																											BgL_tmpz00_1724;
																									}
																							}
																						else
																							{	/* Jas/as.scm 373 */
																								BgL_b1306z00_904 =
																									BGl_2zb2zb2zz__r4_numbers_6_5z00
																									(BgL_a1303z00_906,
																									BgL_b1304z00_907);
																							}
																					}
																				}
																			}
																		}
																		{	/* Jas/as.scm 373 */

																			if (INTEGERP(BgL_b1306z00_904))
																				{	/* Jas/as.scm 373 */
																					obj_t BgL_za71za7_1732;

																					BgL_za71za7_1732 =
																						BINT(BgL_a1305z00_903);
																					{	/* Jas/as.scm 373 */
																						obj_t BgL_tmpz00_1734;

																						BgL_tmpz00_1734 = BINT(0L);
																						if (BGL_ADDFX_OV(BgL_za71za7_1732,
																								BgL_b1306z00_904,
																								BgL_tmpz00_1734))
																							{	/* Jas/as.scm 373 */
																								BgL_b1308z00_901 =
																									bgl_bignum_add
																									(bgl_long_to_bignum((long)
																										CINT(BgL_za71za7_1732)),
																									bgl_long_to_bignum((long)
																										CINT(BgL_b1306z00_904)));
																							}
																						else
																							{	/* Jas/as.scm 373 */
																								BgL_b1308z00_901 =
																									BgL_tmpz00_1734;
																							}
																					}
																				}
																			else
																				{	/* Jas/as.scm 373 */
																					BgL_b1308z00_901 =
																						BGl_2zb2zb2zz__r4_numbers_6_5z00
																						(BINT(BgL_a1305z00_903),
																						BgL_b1306z00_904);
																				}
																		}
																	}
																}
																{	/* Jas/as.scm 373 */

																	if (INTEGERP(BgL_b1308z00_901))
																		{	/* Jas/as.scm 373 */
																			obj_t BgL_za71za7_1742;

																			BgL_za71za7_1742 = BINT(12L);
																			{	/* Jas/as.scm 373 */
																				obj_t BgL_tmpz00_1744;

																				BgL_tmpz00_1744 = BINT(0L);
																				if (BGL_ADDFX_OV(BgL_za71za7_1742,
																						BgL_b1308z00_901, BgL_tmpz00_1744))
																					{	/* Jas/as.scm 373 */
																						BgL_auxz00_3136 =
																							bgl_bignum_add(bgl_long_to_bignum(
																								(long) CINT(BgL_za71za7_1742)),
																							bgl_long_to_bignum(
																								(long) CINT(BgL_b1308z00_901)));
																					}
																				else
																					{	/* Jas/as.scm 373 */
																						BgL_auxz00_3136 = BgL_tmpz00_1744;
																					}
																			}
																		}
																	else
																		{	/* Jas/as.scm 373 */
																			BgL_auxz00_3136 =
																				BGl_2zb2zb2zz__r4_numbers_6_5z00(BINT
																				(12L), BgL_b1308z00_901);
																		}
																}
															}
															((((BgL_attributez00_bglt)
																		COBJECT(BgL_new1101z00_899))->
																	BgL_siza7eza7) =
																((obj_t) BgL_auxz00_3136), BUNSPEC);
														}
														{
															obj_t BgL_auxz00_3262;

															{	/* Jas/as.scm 376 */
																obj_t BgL_arg2008z00_929;
																obj_t BgL_arg2009z00_930;

																BgL_arg2008z00_929 =
																	BGl_stkzd2analysiszd2zzjas_stackz00(
																	((obj_t) BgL_classfilez00_26), BgL_lpz00_876);
																{	/* Jas/as.scm 380 */
																	obj_t BgL_arg2010z00_931;

																	{	/* Jas/as.scm 380 */
																		obj_t BgL_arg2011z00_932;

																		{	/* Jas/as.scm 380 */
																			obj_t BgL_arg2012z00_933;

																			{	/* Jas/as.scm 380 */
																				obj_t BgL_arg2013z00_934;
																				obj_t BgL_arg2014z00_935;

																				{	/* Jas/as.scm 380 */
																					bool_t BgL_test2423z00_3265;

																					{	/* Jas/as.scm 380 */
																						obj_t BgL_objz00_1752;

																						BgL_objz00_1752 = BgL_linesz00_881;
																						BgL_test2423z00_3265 =
																							NULLP(BgL_objz00_1752);
																					}
																					if (BgL_test2423z00_3265)
																						{	/* Jas/as.scm 380 */
																							BgL_arg2013z00_934 = BNIL;
																						}
																					else
																						{	/* Jas/as.scm 381 */
																							BgL_attributez00_bglt
																								BgL_arg2016z00_937;
																							BgL_arg2016z00_937 =
																								BGl_makezd2linezd2attributez00zzjas_asz00
																								(BgL_classfilez00_26,
																								BgL_linesz00_881);
																							BgL_arg2013z00_934 =
																								MAKE_YOUNG_PAIR(((obj_t)
																									BgL_arg2016z00_937), BNIL);
																						}
																				}
																				{	/* Jas/as.scm 382 */
																					obj_t BgL_arg2017z00_938;

																					if (NULLP(BgL_localvarsz00_882))
																						{	/* Jas/as.scm 382 */
																							BgL_arg2017z00_938 = BNIL;
																						}
																					else
																						{	/* Jas/as.scm 383 */
																							BgL_attributez00_bglt
																								BgL_arg2019z00_940;
																							BgL_arg2019z00_940 =
																								BGl_makezd2localvarszd2zzjas_asz00
																								(BgL_classfilez00_26,
																								BgL_localvarsz00_882);
																							BgL_arg2017z00_938 =
																								MAKE_YOUNG_PAIR(((obj_t)
																									BgL_arg2019z00_940), BNIL);
																						}
																					BgL_arg2014z00_935 =
																						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																						(BgL_arg2017z00_938, BNIL);
																				}
																				BgL_arg2012z00_933 =
																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																					(BgL_arg2013z00_934,
																					BgL_arg2014z00_935);
																			}
																			BgL_arg2011z00_932 =
																				MAKE_YOUNG_PAIR(BgL_handlersz00_879,
																				BgL_arg2012z00_933);
																		}
																		BgL_arg2010z00_931 =
																			MAKE_YOUNG_PAIR(BgL_bytecodez00_883,
																			BgL_arg2011z00_932);
																	}
																	BgL_arg2009z00_930 =
																		MAKE_YOUNG_PAIR
																		(BGl_za2za2lastzd2numberzd2ofzd2localsza2za2zd2zzjas_peepz00,
																		BgL_arg2010z00_931);
																}
																BgL_auxz00_3262 =
																	MAKE_YOUNG_PAIR(BgL_arg2008z00_929,
																	BgL_arg2009z00_930);
															}
															((((BgL_attributez00_bglt)
																		COBJECT(BgL_new1101z00_899))->BgL_infoz00) =
																((obj_t) BgL_auxz00_3262), BUNSPEC);
														}
														return BgL_new1101z00_899;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* make-line-attribute */
	BgL_attributez00_bglt
		BGl_makezd2linezd2attributez00zzjas_asz00(BgL_classfilez00_bglt
		BgL_classfilez00_30, obj_t BgL_linesz00_31)
	{
		{	/* Jas/as.scm 385 */
			{	/* Jas/as.scm 386 */
				BgL_attributez00_bglt BgL_new1103z00_943;

				{	/* Jas/as.scm 387 */
					BgL_attributez00_bglt BgL_new1102z00_972;

					BgL_new1102z00_972 =
						((BgL_attributez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_attributez00_bgl))));
					{	/* Jas/as.scm 387 */
						long BgL_arg2038z00_973;

						BgL_arg2038z00_973 =
							BGL_CLASS_NUM(BGl_attributez00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1102z00_972), BgL_arg2038z00_973);
					}
					BgL_new1103z00_943 = BgL_new1102z00_972;
				}
				((((BgL_attributez00_bglt) COBJECT(BgL_new1103z00_943))->BgL_typez00) =
					((obj_t) CNST_TABLE_REF(27)), BUNSPEC);
				((((BgL_attributez00_bglt) COBJECT(BgL_new1103z00_943))->BgL_namez00) =
					((obj_t)
						BINT(BGl_poolzd2namezd2zzjas_classfilez00(BgL_classfilez00_30,
								BGl_string2214z00zzjas_asz00))), BUNSPEC);
				{
					obj_t BgL_auxz00_3291;

					{	/* Jas/as.scm 389 */
						obj_t BgL_b1312z00_945;

						{	/* Jas/as.scm 389 */
							long BgL_b1310z00_948;

							BgL_b1310z00_948 = bgl_list_length(BgL_linesz00_31);
							{	/* Jas/as.scm 389 */

								{	/* Jas/as.scm 389 */
									obj_t BgL_za71za7_1756;
									obj_t BgL_za72za7_1757;

									BgL_za71za7_1756 = BINT(4L);
									BgL_za72za7_1757 = BINT(BgL_b1310z00_948);
									{	/* Jas/as.scm 389 */
										obj_t BgL_tmpz00_1758;

										BgL_tmpz00_1758 = BINT(0L);
										{	/* Jas/as.scm 389 */
											bool_t BgL_test2425z00_3296;

											{	/* Jas/as.scm 389 */
												long BgL_tmpz00_3297;

												BgL_tmpz00_3297 = (long) CINT(BgL_za72za7_1757);
												BgL_test2425z00_3296 =
													BGL_MULFX_OV(BgL_za71za7_1756, BgL_tmpz00_3297,
													BgL_tmpz00_1758);
											}
											if (BgL_test2425z00_3296)
												{	/* Jas/as.scm 389 */
													BgL_b1312z00_945 =
														bgl_bignum_mul(bgl_long_to_bignum(
															(long) CINT(BgL_za71za7_1756)),
														bgl_long_to_bignum((long) CINT(BgL_za72za7_1757)));
												}
											else
												{	/* Jas/as.scm 389 */
													BgL_b1312z00_945 = BgL_tmpz00_1758;
												}
										}
									}
								}
							}
						}
						{	/* Jas/as.scm 389 */

							if (INTEGERP(BgL_b1312z00_945))
								{	/* Jas/as.scm 389 */
									obj_t BgL_za71za7_1766;

									BgL_za71za7_1766 = BINT(2L);
									{	/* Jas/as.scm 389 */
										obj_t BgL_tmpz00_1768;

										BgL_tmpz00_1768 = BINT(0L);
										if (BGL_ADDFX_OV(BgL_za71za7_1766, BgL_b1312z00_945,
												BgL_tmpz00_1768))
											{	/* Jas/as.scm 389 */
												BgL_auxz00_3291 =
													bgl_bignum_add(bgl_long_to_bignum(
														(long) CINT(BgL_za71za7_1766)),
													bgl_long_to_bignum((long) CINT(BgL_b1312z00_945)));
											}
										else
											{	/* Jas/as.scm 389 */
												BgL_auxz00_3291 = BgL_tmpz00_1768;
											}
									}
								}
							else
								{	/* Jas/as.scm 389 */
									BgL_auxz00_3291 =
										BGl_2zb2zb2zz__r4_numbers_6_5z00(BINT(2L),
										BgL_b1312z00_945);
								}
						}
					}
					((((BgL_attributez00_bglt) COBJECT(BgL_new1103z00_943))->
							BgL_siza7eza7) = ((obj_t) BgL_auxz00_3291), BUNSPEC);
				}
				{
					obj_t BgL_auxz00_3319;

					{	/* Jas/as.scm 390 */
						obj_t BgL_fun1318z00_950;

						if (CBOOL(BGl_za2jvmzd2charzd2infoza2z00zzjas_asz00))
							{	/* Jas/as.scm 390 */
								BgL_fun1318z00_950 = BGl_proc2215z00zzjas_asz00;
							}
						else
							{	/* Jas/as.scm 390 */
								BgL_fun1318z00_950 = BGl_proc2216z00zzjas_asz00;
							}
						if (NULLP(BgL_linesz00_31))
							{	/* Jas/as.scm 390 */
								BgL_auxz00_3319 = BNIL;
							}
						else
							{	/* Jas/as.scm 390 */
								obj_t BgL_head1315z00_953;

								BgL_head1315z00_953 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1313z00_955;
									obj_t BgL_tail1316z00_956;

									BgL_l1313z00_955 = BgL_linesz00_31;
									BgL_tail1316z00_956 = BgL_head1315z00_953;
								BgL_zc3z04anonymousza32024ze3z87_957:
									if (NULLP(BgL_l1313z00_955))
										{	/* Jas/as.scm 390 */
											BgL_auxz00_3319 = CDR(BgL_head1315z00_953);
										}
									else
										{	/* Jas/as.scm 390 */
											obj_t BgL_newtail1317z00_959;

											{	/* Jas/as.scm 390 */
												obj_t BgL_arg2027z00_961;

												{	/* Jas/as.scm 390 */
													obj_t BgL_arg2029z00_962;

													BgL_arg2029z00_962 = CAR(((obj_t) BgL_l1313z00_955));
													BgL_arg2027z00_961 =
														((obj_t(*)(obj_t,
																obj_t))
														PROCEDURE_L_ENTRY(BgL_fun1318z00_950))
														(BgL_fun1318z00_950, BgL_arg2029z00_962);
												}
												BgL_newtail1317z00_959 =
													MAKE_YOUNG_PAIR(BgL_arg2027z00_961, BNIL);
											}
											SET_CDR(BgL_tail1316z00_956, BgL_newtail1317z00_959);
											{	/* Jas/as.scm 390 */
												obj_t BgL_arg2026z00_960;

												BgL_arg2026z00_960 = CDR(((obj_t) BgL_l1313z00_955));
												{
													obj_t BgL_tail1316z00_3339;
													obj_t BgL_l1313z00_3338;

													BgL_l1313z00_3338 = BgL_arg2026z00_960;
													BgL_tail1316z00_3339 = BgL_newtail1317z00_959;
													BgL_tail1316z00_956 = BgL_tail1316z00_3339;
													BgL_l1313z00_955 = BgL_l1313z00_3338;
													goto BgL_zc3z04anonymousza32024ze3z87_957;
												}
											}
										}
								}
							}
					}
					((((BgL_attributez00_bglt) COBJECT(BgL_new1103z00_943))->
							BgL_infoz00) = ((obj_t) BgL_auxz00_3319), BUNSPEC);
				}
				return BgL_new1103z00_943;
			}
		}

	}



/* &<@anonymous:2030> */
	obj_t BGl_z62zc3z04anonymousza32030ze3ze5zzjas_asz00(obj_t BgL_envz00_2045,
		obj_t BgL_xz00_2046)
	{
		{	/* Jas/as.scm 391 */
			{	/* Jas/as.scm 391 */
				obj_t BgL_arg2031z00_2069;
				obj_t BgL_arg2033z00_2070;

				BgL_arg2031z00_2069 = CAR(((obj_t) BgL_xz00_2046));
				{	/* Jas/as.scm 391 */
					obj_t BgL_pairz00_2071;

					{	/* Jas/as.scm 391 */
						obj_t BgL_pairz00_2072;

						BgL_pairz00_2072 = CDR(((obj_t) BgL_xz00_2046));
						BgL_pairz00_2071 = CDR(BgL_pairz00_2072);
					}
					BgL_arg2033z00_2070 = CAR(BgL_pairz00_2071);
				}
				return MAKE_YOUNG_PAIR(BgL_arg2031z00_2069, BgL_arg2033z00_2070);
			}
		}

	}



/* &<@anonymous:2034> */
	obj_t BGl_z62zc3z04anonymousza32034ze3ze5zzjas_asz00(obj_t BgL_envz00_2047,
		obj_t BgL_xz00_2048)
	{
		{	/* Jas/as.scm 392 */
			{	/* Jas/as.scm 392 */
				obj_t BgL_arg2036z00_2073;
				obj_t BgL_arg2037z00_2074;

				BgL_arg2036z00_2073 = CAR(((obj_t) BgL_xz00_2048));
				{	/* Jas/as.scm 392 */
					obj_t BgL_pairz00_2075;

					BgL_pairz00_2075 = CDR(((obj_t) BgL_xz00_2048));
					BgL_arg2037z00_2074 = CAR(BgL_pairz00_2075);
				}
				return MAKE_YOUNG_PAIR(BgL_arg2036z00_2073, BgL_arg2037z00_2074);
			}
		}

	}



/* make-localvars */
	BgL_attributez00_bglt BGl_makezd2localvarszd2zzjas_asz00(BgL_classfilez00_bglt
		BgL_classfilez00_32, obj_t BgL_localvarsz00_33)
	{
		{	/* Jas/as.scm 394 */
			{	/* Jas/as.scm 395 */
				BgL_attributez00_bglt BgL_new1105z00_974;

				{	/* Jas/as.scm 396 */
					BgL_attributez00_bglt BgL_new1104z00_981;

					BgL_new1104z00_981 =
						((BgL_attributez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_attributez00_bgl))));
					{	/* Jas/as.scm 396 */
						long BgL_arg2041z00_982;

						BgL_arg2041z00_982 =
							BGL_CLASS_NUM(BGl_attributez00zzjas_classfilez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1104z00_981), BgL_arg2041z00_982);
					}
					BgL_new1105z00_974 = BgL_new1104z00_981;
				}
				((((BgL_attributez00_bglt) COBJECT(BgL_new1105z00_974))->BgL_typez00) =
					((obj_t) CNST_TABLE_REF(28)), BUNSPEC);
				((((BgL_attributez00_bglt) COBJECT(BgL_new1105z00_974))->BgL_namez00) =
					((obj_t)
						BINT(BGl_poolzd2namezd2zzjas_classfilez00(BgL_classfilez00_32,
								BGl_string2217z00zzjas_asz00))), BUNSPEC);
				{
					obj_t BgL_auxz00_3363;

					{	/* Jas/as.scm 398 */
						obj_t BgL_b1322z00_976;

						{	/* Jas/as.scm 398 */
							long BgL_b1320z00_979;

							BgL_b1320z00_979 = bgl_list_length(BgL_localvarsz00_33);
							{	/* Jas/as.scm 398 */

								{	/* Jas/as.scm 398 */
									obj_t BgL_za71za7_1799;
									obj_t BgL_za72za7_1800;

									BgL_za71za7_1799 = BINT(10L);
									BgL_za72za7_1800 = BINT(BgL_b1320z00_979);
									{	/* Jas/as.scm 398 */
										obj_t BgL_tmpz00_1801;

										BgL_tmpz00_1801 = BINT(0L);
										{	/* Jas/as.scm 398 */
											bool_t BgL_test2431z00_3368;

											{	/* Jas/as.scm 398 */
												long BgL_tmpz00_3369;

												BgL_tmpz00_3369 = (long) CINT(BgL_za72za7_1800);
												BgL_test2431z00_3368 =
													BGL_MULFX_OV(BgL_za71za7_1799, BgL_tmpz00_3369,
													BgL_tmpz00_1801);
											}
											if (BgL_test2431z00_3368)
												{	/* Jas/as.scm 398 */
													BgL_b1322z00_976 =
														bgl_bignum_mul(bgl_long_to_bignum(
															(long) CINT(BgL_za71za7_1799)),
														bgl_long_to_bignum((long) CINT(BgL_za72za7_1800)));
												}
											else
												{	/* Jas/as.scm 398 */
													BgL_b1322z00_976 = BgL_tmpz00_1801;
												}
										}
									}
								}
							}
						}
						{	/* Jas/as.scm 398 */

							if (INTEGERP(BgL_b1322z00_976))
								{	/* Jas/as.scm 398 */
									obj_t BgL_za71za7_1809;

									BgL_za71za7_1809 = BINT(2L);
									{	/* Jas/as.scm 398 */
										obj_t BgL_tmpz00_1811;

										BgL_tmpz00_1811 = BINT(0L);
										if (BGL_ADDFX_OV(BgL_za71za7_1809, BgL_b1322z00_976,
												BgL_tmpz00_1811))
											{	/* Jas/as.scm 398 */
												BgL_auxz00_3363 =
													bgl_bignum_add(bgl_long_to_bignum(
														(long) CINT(BgL_za71za7_1809)),
													bgl_long_to_bignum((long) CINT(BgL_b1322z00_976)));
											}
										else
											{	/* Jas/as.scm 398 */
												BgL_auxz00_3363 = BgL_tmpz00_1811;
											}
									}
								}
							else
								{	/* Jas/as.scm 398 */
									BgL_auxz00_3363 =
										BGl_2zb2zb2zz__r4_numbers_6_5z00(BINT(2L),
										BgL_b1322z00_976);
								}
						}
					}
					((((BgL_attributez00_bglt) COBJECT(BgL_new1105z00_974))->
							BgL_siza7eza7) = ((obj_t) BgL_auxz00_3363), BUNSPEC);
				}
				((((BgL_attributez00_bglt) COBJECT(BgL_new1105z00_974))->BgL_infoz00) =
					((obj_t) BgL_localvarsz00_33), BUNSPEC);
				return BgL_new1105z00_974;
			}
		}

	}



/* get-handlers */
	obj_t BGl_getzd2handlerszd2zzjas_asz00(obj_t BgL_lz00_34)
	{
		{	/* Jas/as.scm 402 */
		BGl_getzd2handlerszd2zzjas_asz00:
			if (NULLP(BgL_lz00_34))
				{	/* Jas/as.scm 403 */
					return BgL_lz00_34;
				}
			else
				{	/* Jas/as.scm 404 */
					bool_t BgL_test2435z00_3394;

					{	/* Jas/as.scm 404 */
						obj_t BgL_tmpz00_3395;

						{	/* Jas/as.scm 404 */
							obj_t BgL_pairz00_1822;

							BgL_pairz00_1822 = CAR(((obj_t) BgL_lz00_34));
							BgL_tmpz00_3395 = CAR(BgL_pairz00_1822);
						}
						BgL_test2435z00_3394 = (BgL_tmpz00_3395 == BINT(202L));
					}
					if (BgL_test2435z00_3394)
						{	/* Jas/as.scm 404 */
							obj_t BgL_arg2046z00_986;
							obj_t BgL_arg2047z00_987;

							{	/* Jas/as.scm 404 */
								obj_t BgL_pairz00_1826;

								BgL_pairz00_1826 = CAR(((obj_t) BgL_lz00_34));
								BgL_arg2046z00_986 = CDR(BgL_pairz00_1826);
							}
							{	/* Jas/as.scm 404 */
								obj_t BgL_arg2048z00_988;

								BgL_arg2048z00_988 = CDR(((obj_t) BgL_lz00_34));
								BgL_arg2047z00_987 =
									BGl_getzd2handlerszd2zzjas_asz00(BgL_arg2048z00_988);
							}
							return MAKE_YOUNG_PAIR(BgL_arg2046z00_986, BgL_arg2047z00_987);
						}
					else
						{	/* Jas/as.scm 405 */
							obj_t BgL_arg2049z00_989;

							BgL_arg2049z00_989 = CDR(((obj_t) BgL_lz00_34));
							{
								obj_t BgL_lz00_3410;

								BgL_lz00_3410 = BgL_arg2049z00_989;
								BgL_lz00_34 = BgL_lz00_3410;
								goto BGl_getzd2handlerszd2zzjas_asz00;
							}
						}
				}
		}

	}



/* get-lines */
	obj_t BGl_getzd2lineszd2zzjas_asz00(obj_t BgL_lz00_35, long BgL_pcz00_36)
	{
		{	/* Jas/as.scm 409 */
		BGl_getzd2lineszd2zzjas_asz00:
			if (NULLP(BgL_lz00_35))
				{	/* Jas/as.scm 410 */
					{	/* Jas/as.scm 411 */
						bool_t BgL_test2437z00_3413;

						if (CBOOL(BGl_za2jvmzd2charzd2infoza2z00zzjas_asz00))
							{	/* Jas/as.scm 411 */
								BgL_test2437z00_3413 =
									((long) CINT(BGl_za2za2maxcharza2za2z00zzjas_asz00) > 65535L);
							}
						else
							{	/* Jas/as.scm 411 */
								BgL_test2437z00_3413 = ((bool_t) 0);
							}
						if (BgL_test2437z00_3413)
							{	/* Jas/as.scm 411 */
								{	/* Jas/as.scm 412 */
									obj_t BgL_list2053z00_993;

									{	/* Jas/as.scm 412 */
										obj_t BgL_arg2055z00_994;

										{	/* Jas/as.scm 412 */
											obj_t BgL_arg2056z00_995;

											{	/* Jas/as.scm 412 */
												obj_t BgL_arg2057z00_996;

												{	/* Jas/as.scm 412 */
													obj_t BgL_arg2058z00_997;

													BgL_arg2058z00_997 =
														MAKE_YOUNG_PAIR(BGl_string2218z00zzjas_asz00, BNIL);
													BgL_arg2057z00_996 =
														MAKE_YOUNG_PAIR(BGl_string2219z00zzjas_asz00,
														BgL_arg2058z00_997);
												}
												BgL_arg2056z00_995 =
													MAKE_YOUNG_PAIR(BGl_za2za2maxcharza2za2z00zzjas_asz00,
													BgL_arg2057z00_996);
											}
											BgL_arg2055z00_994 =
												MAKE_YOUNG_PAIR(BGl_string2220z00zzjas_asz00,
												BgL_arg2056z00_995);
										}
										BgL_list2053z00_993 =
											MAKE_YOUNG_PAIR(BGl_string2221z00zzjas_asz00,
											BgL_arg2055z00_994);
									}
									BGl_warningz00zz__errorz00(BgL_list2053z00_993);
								}
								BGl_za2jvmzd2charzd2infoza2z00zzjas_asz00 = BFALSE;
							}
						else
							{	/* Jas/as.scm 411 */
								BFALSE;
							}
					}
					return BgL_lz00_35;
				}
			else
				{	/* Jas/as.scm 417 */
					bool_t BgL_test2439z00_3424;

					{	/* Jas/as.scm 417 */
						obj_t BgL_tmpz00_3425;

						{	/* Jas/as.scm 417 */
							obj_t BgL_pairz00_1833;

							BgL_pairz00_1833 = CAR(((obj_t) BgL_lz00_35));
							BgL_tmpz00_3425 = CAR(BgL_pairz00_1833);
						}
						BgL_test2439z00_3424 = (BgL_tmpz00_3425 == BINT(202L));
					}
					if (BgL_test2439z00_3424)
						{	/* Jas/as.scm 417 */
							obj_t BgL_arg2061z00_1000;

							BgL_arg2061z00_1000 = CDR(((obj_t) BgL_lz00_35));
							{
								obj_t BgL_lz00_3433;

								BgL_lz00_3433 = BgL_arg2061z00_1000;
								BgL_lz00_35 = BgL_lz00_3433;
								goto BGl_getzd2lineszd2zzjas_asz00;
							}
						}
					else
						{	/* Jas/as.scm 418 */
							bool_t BgL_test2440z00_3434;

							{	/* Jas/as.scm 418 */
								obj_t BgL_tmpz00_3435;

								{	/* Jas/as.scm 418 */
									obj_t BgL_pairz00_1838;

									BgL_pairz00_1838 = CAR(((obj_t) BgL_lz00_35));
									BgL_tmpz00_3435 = CAR(BgL_pairz00_1838);
								}
								BgL_test2440z00_3434 = (BgL_tmpz00_3435 == BINT(203L));
							}
							if (BgL_test2440z00_3434)
								{	/* Jas/as.scm 418 */
									{	/* Jas/as.scm 419 */
										bool_t BgL_test2441z00_3441;

										{	/* Jas/as.scm 419 */
											long BgL_n1z00_1845;
											long BgL_n2z00_1846;

											{	/* Jas/as.scm 419 */
												obj_t BgL_pairz00_1844;

												{	/* Jas/as.scm 419 */
													obj_t BgL_pairz00_1843;

													BgL_pairz00_1843 = CAR(((obj_t) BgL_lz00_35));
													BgL_pairz00_1844 = CDR(BgL_pairz00_1843);
												}
												BgL_n1z00_1845 = (long) CINT(CAR(BgL_pairz00_1844));
											}
											BgL_n2z00_1846 =
												(long) CINT(BGl_za2za2maxlineza2za2z00zzjas_asz00);
											BgL_test2441z00_3441 = (BgL_n1z00_1845 > BgL_n2z00_1846);
										}
										if (BgL_test2441z00_3441)
											{	/* Jas/as.scm 419 */
												obj_t BgL_pairz00_1852;

												{	/* Jas/as.scm 419 */
													obj_t BgL_pairz00_1851;

													BgL_pairz00_1851 = CAR(((obj_t) BgL_lz00_35));
													BgL_pairz00_1852 = CDR(BgL_pairz00_1851);
												}
												BGl_za2za2maxlineza2za2z00zzjas_asz00 =
													CAR(BgL_pairz00_1852);
											}
										else
											{	/* Jas/as.scm 419 */
												BFALSE;
											}
									}
									{	/* Jas/as.scm 420 */
										bool_t BgL_test2442z00_3453;

										{	/* Jas/as.scm 420 */
											long BgL_n1z00_1861;
											long BgL_n2z00_1862;

											{	/* Jas/as.scm 420 */
												obj_t BgL_pairz00_1860;

												{	/* Jas/as.scm 420 */
													obj_t BgL_pairz00_1859;

													{	/* Jas/as.scm 420 */
														obj_t BgL_pairz00_1858;

														BgL_pairz00_1858 = CAR(((obj_t) BgL_lz00_35));
														BgL_pairz00_1859 = CDR(BgL_pairz00_1858);
													}
													BgL_pairz00_1860 = CDR(BgL_pairz00_1859);
												}
												BgL_n1z00_1861 = (long) CINT(CAR(BgL_pairz00_1860));
											}
											BgL_n2z00_1862 =
												(long) CINT(BGl_za2za2maxcharza2za2z00zzjas_asz00);
											BgL_test2442z00_3453 = (BgL_n1z00_1861 > BgL_n2z00_1862);
										}
										if (BgL_test2442z00_3453)
											{	/* Jas/as.scm 420 */
												obj_t BgL_pairz00_1870;

												{	/* Jas/as.scm 420 */
													obj_t BgL_pairz00_1869;

													{	/* Jas/as.scm 420 */
														obj_t BgL_pairz00_1868;

														BgL_pairz00_1868 = CAR(((obj_t) BgL_lz00_35));
														BgL_pairz00_1869 = CDR(BgL_pairz00_1868);
													}
													BgL_pairz00_1870 = CDR(BgL_pairz00_1869);
												}
												BGl_za2za2maxcharza2za2z00zzjas_asz00 =
													CAR(BgL_pairz00_1870);
											}
										else
											{	/* Jas/as.scm 420 */
												BFALSE;
											}
									}
									{	/* Jas/as.scm 421 */
										obj_t BgL_arg2072z00_1009;
										obj_t BgL_arg2074z00_1010;

										{	/* Jas/as.scm 421 */
											obj_t BgL_arg2075z00_1011;
											obj_t BgL_arg2076z00_1012;

											{	/* Jas/as.scm 421 */
												obj_t BgL_pairz00_1876;

												{	/* Jas/as.scm 421 */
													obj_t BgL_pairz00_1875;

													BgL_pairz00_1875 = CAR(((obj_t) BgL_lz00_35));
													BgL_pairz00_1876 = CDR(BgL_pairz00_1875);
												}
												BgL_arg2075z00_1011 = CAR(BgL_pairz00_1876);
											}
											{	/* Jas/as.scm 421 */
												obj_t BgL_pairz00_1884;

												{	/* Jas/as.scm 421 */
													obj_t BgL_pairz00_1883;

													{	/* Jas/as.scm 421 */
														obj_t BgL_pairz00_1882;

														BgL_pairz00_1882 = CAR(((obj_t) BgL_lz00_35));
														BgL_pairz00_1883 = CDR(BgL_pairz00_1882);
													}
													BgL_pairz00_1884 = CDR(BgL_pairz00_1883);
												}
												BgL_arg2076z00_1012 = CAR(BgL_pairz00_1884);
											}
											{	/* Jas/as.scm 421 */
												obj_t BgL_list2077z00_1013;

												{	/* Jas/as.scm 421 */
													obj_t BgL_arg2078z00_1014;

													{	/* Jas/as.scm 421 */
														obj_t BgL_arg2079z00_1015;

														BgL_arg2079z00_1015 =
															MAKE_YOUNG_PAIR(BgL_arg2076z00_1012, BNIL);
														BgL_arg2078z00_1014 =
															MAKE_YOUNG_PAIR(BgL_arg2075z00_1011,
															BgL_arg2079z00_1015);
													}
													BgL_list2077z00_1013 =
														MAKE_YOUNG_PAIR(BINT(BgL_pcz00_36),
														BgL_arg2078z00_1014);
												}
												BgL_arg2072z00_1009 = BgL_list2077z00_1013;
											}
										}
										{	/* Jas/as.scm 421 */
											obj_t BgL_arg2080z00_1016;

											BgL_arg2080z00_1016 = CDR(((obj_t) BgL_lz00_35));
											BgL_arg2074z00_1010 =
												BGl_getzd2lineszd2zzjas_asz00(BgL_arg2080z00_1016,
												BgL_pcz00_36);
										}
										return
											MAKE_YOUNG_PAIR(BgL_arg2072z00_1009, BgL_arg2074z00_1010);
									}
								}
							else
								{	/* Jas/as.scm 422 */
									bool_t BgL_test2443z00_3484;

									{	/* Jas/as.scm 422 */
										obj_t BgL_tmpz00_3485;

										{	/* Jas/as.scm 422 */
											obj_t BgL_pairz00_1890;

											BgL_pairz00_1890 = CAR(((obj_t) BgL_lz00_35));
											BgL_tmpz00_3485 = CAR(BgL_pairz00_1890);
										}
										BgL_test2443z00_3484 = (BgL_tmpz00_3485 == BINT(204L));
									}
									if (BgL_test2443z00_3484)
										{	/* Jas/as.scm 422 */
											obj_t BgL_arg2083z00_1019;

											BgL_arg2083z00_1019 = CDR(((obj_t) BgL_lz00_35));
											{
												obj_t BgL_lz00_3493;

												BgL_lz00_3493 = BgL_arg2083z00_1019;
												BgL_lz00_35 = BgL_lz00_3493;
												goto BGl_getzd2lineszd2zzjas_asz00;
											}
										}
									else
										{	/* Jas/as.scm 423 */
											bool_t BgL_test2444z00_3494;

											{	/* Jas/as.scm 423 */
												obj_t BgL_tmpz00_3495;

												{	/* Jas/as.scm 423 */
													obj_t BgL_pairz00_1895;

													BgL_pairz00_1895 = CAR(((obj_t) BgL_lz00_35));
													BgL_tmpz00_3495 = CAR(BgL_pairz00_1895);
												}
												BgL_test2444z00_3494 = (BgL_tmpz00_3495 == BINT(205L));
											}
											if (BgL_test2444z00_3494)
												{	/* Jas/as.scm 423 */
													obj_t BgL_arg2086z00_1022;

													BgL_arg2086z00_1022 = CDR(((obj_t) BgL_lz00_35));
													{
														obj_t BgL_lz00_3503;

														BgL_lz00_3503 = BgL_arg2086z00_1022;
														BgL_lz00_35 = BgL_lz00_3503;
														goto BGl_getzd2lineszd2zzjas_asz00;
													}
												}
											else
												{	/* Jas/as.scm 424 */
													obj_t BgL_arg2087z00_1023;
													long BgL_arg2088z00_1024;

													BgL_arg2087z00_1023 = CDR(((obj_t) BgL_lz00_35));
													BgL_arg2088z00_1024 =
														(BgL_pcz00_36 +
														bgl_list_length(CAR(((obj_t) BgL_lz00_35))));
													{
														long BgL_pcz00_3511;
														obj_t BgL_lz00_3510;

														BgL_lz00_3510 = BgL_arg2087z00_1023;
														BgL_pcz00_3511 = BgL_arg2088z00_1024;
														BgL_pcz00_36 = BgL_pcz00_3511;
														BgL_lz00_35 = BgL_lz00_3510;
														goto BGl_getzd2lineszd2zzjas_asz00;
													}
												}
										}
								}
						}
				}
		}

	}



/* line-compress */
	obj_t BGl_linezd2compresszd2zzjas_asz00(obj_t BgL_lz00_37,
		obj_t BgL_linez00_38)
	{
		{	/* Jas/as.scm 426 */
		BGl_linezd2compresszd2zzjas_asz00:
			if (NULLP(BgL_lz00_37))
				{	/* Jas/as.scm 428 */
					return BgL_lz00_37;
				}
			else
				{	/* Jas/as.scm 429 */
					bool_t BgL_test2446z00_3514;

					{	/* Jas/as.scm 429 */
						obj_t BgL_tmpz00_3515;

						{	/* Jas/as.scm 429 */
							obj_t BgL_pairz00_1906;

							{	/* Jas/as.scm 429 */
								obj_t BgL_pairz00_1905;

								BgL_pairz00_1905 = CAR(((obj_t) BgL_lz00_37));
								BgL_pairz00_1906 = CDR(BgL_pairz00_1905);
							}
							BgL_tmpz00_3515 = CAR(BgL_pairz00_1906);
						}
						BgL_test2446z00_3514 = (BgL_tmpz00_3515 == BgL_linez00_38);
					}
					if (BgL_test2446z00_3514)
						{	/* Jas/as.scm 429 */
							obj_t BgL_arg2099z00_1034;

							BgL_arg2099z00_1034 = CDR(((obj_t) BgL_lz00_37));
							{
								obj_t BgL_lz00_3523;

								BgL_lz00_3523 = BgL_arg2099z00_1034;
								BgL_lz00_37 = BgL_lz00_3523;
								goto BGl_linezd2compresszd2zzjas_asz00;
							}
						}
					else
						{	/* Jas/as.scm 431 */
							obj_t BgL_arg2100z00_1035;
							obj_t BgL_arg2101z00_1036;

							BgL_arg2100z00_1035 = CAR(((obj_t) BgL_lz00_37));
							{	/* Jas/as.scm 431 */
								obj_t BgL_arg2102z00_1037;
								obj_t BgL_arg2103z00_1038;

								BgL_arg2102z00_1037 = CDR(((obj_t) BgL_lz00_37));
								{	/* Jas/as.scm 431 */
									obj_t BgL_pairz00_1915;

									{	/* Jas/as.scm 431 */
										obj_t BgL_pairz00_1914;

										BgL_pairz00_1914 = CAR(((obj_t) BgL_lz00_37));
										BgL_pairz00_1915 = CDR(BgL_pairz00_1914);
									}
									BgL_arg2103z00_1038 = CAR(BgL_pairz00_1915);
								}
								BgL_arg2101z00_1036 =
									BGl_linezd2compresszd2zzjas_asz00(BgL_arg2102z00_1037,
									BgL_arg2103z00_1038);
							}
							return MAKE_YOUNG_PAIR(BgL_arg2100z00_1035, BgL_arg2101z00_1036);
						}
				}
		}

	}



/* pos-info->line-info! */
	obj_t BGl_poszd2infozd2ze3linezd2infoz12z23zzjas_asz00(void)
	{
		{	/* Jas/as.scm 433 */
			{	/* Jas/as.scm 449 */
				obj_t BgL_xz00_1041;

				BgL_xz00_1041 =
					BGl_sortz00zz__r4_vectors_6_8z00
					(BGl_za2za2allzd2linesza2za2zd2zzjas_asz00,
					BGl_proc2222z00zzjas_asz00);
				return (BGl_za2za2poszd2ze3lineza2za2z31zzjas_asz00 =
					BGl_infoze70ze7zzjas_asz00(BgL_xz00_1041, BFALSE, BINT(0L), BINT(0L)),
					BUNSPEC);
			}
		}

	}



/* info~0 */
	obj_t BGl_infoze70ze7zzjas_asz00(obj_t BgL_lz00_1049, obj_t BgL_linez00_1050,
		obj_t BgL_miniz00_1051, obj_t BgL_maxiz00_1052)
	{
		{	/* Jas/as.scm 448 */
		BGl_infoze70ze7zzjas_asz00:
			if (NULLP(BgL_lz00_1049))
				{	/* Jas/as.scm 436 */
					if (CBOOL(BgL_linez00_1050))
						{	/* Jas/as.scm 438 */
							obj_t BgL_arg2111z00_1055;

							{	/* Jas/as.scm 438 */
								obj_t BgL_list2112z00_1056;

								{	/* Jas/as.scm 438 */
									obj_t BgL_arg2113z00_1057;

									{	/* Jas/as.scm 438 */
										obj_t BgL_arg2114z00_1058;

										BgL_arg2114z00_1058 =
											MAKE_YOUNG_PAIR(BgL_maxiz00_1052, BNIL);
										BgL_arg2113z00_1057 =
											MAKE_YOUNG_PAIR(BgL_miniz00_1051, BgL_arg2114z00_1058);
									}
									BgL_list2112z00_1056 =
										MAKE_YOUNG_PAIR(BgL_linez00_1050, BgL_arg2113z00_1057);
								}
								BgL_arg2111z00_1055 = BgL_list2112z00_1056;
							}
							return MAKE_YOUNG_PAIR(BgL_arg2111z00_1055, BNIL);
						}
					else
						{	/* Jas/as.scm 437 */
							return BNIL;
						}
				}
			else
				{	/* Jas/as.scm 436 */
					if (CBOOL(BgL_linez00_1050))
						{	/* Jas/as.scm 443 */
							bool_t BgL_test2450z00_3548;

							{	/* Jas/as.scm 443 */
								long BgL_tmpz00_3549;

								{	/* Jas/as.scm 443 */
									obj_t BgL_tmpz00_3551;

									{	/* Jas/as.scm 443 */
										obj_t BgL_pairz00_1918;

										BgL_pairz00_1918 = CAR(((obj_t) BgL_lz00_1049));
										BgL_tmpz00_3551 = CAR(CDR(BgL_pairz00_1918));
									}
									BgL_tmpz00_3549 = (long) CINT(BgL_tmpz00_3551);
								}
								BgL_test2450z00_3548 =
									((long) CINT(BgL_linez00_1050) == BgL_tmpz00_3549);
							}
							if (BgL_test2450z00_3548)
								{	/* Jas/as.scm 444 */
									obj_t BgL_posz00_1062;

									{	/* Jas/as.scm 444 */
										obj_t BgL_pairz00_1925;

										BgL_pairz00_1925 = CAR(((obj_t) BgL_lz00_1049));
										BgL_posz00_1062 = CAR(CDR(CDR(BgL_pairz00_1925)));
									}
									{	/* Jas/as.scm 445 */
										obj_t BgL_arg2118z00_1063;
										obj_t BgL_arg2119z00_1064;
										obj_t BgL_arg2120z00_1065;

										BgL_arg2118z00_1063 = CDR(((obj_t) BgL_lz00_1049));
										BgL_arg2119z00_1064 =
											BGl_2minz00zz__r4_numbers_6_5z00(BgL_miniz00_1051,
											BgL_posz00_1062);
										BgL_arg2120z00_1065 =
											BGl_2maxz00zz__r4_numbers_6_5z00(BgL_maxiz00_1052,
											BgL_posz00_1062);
										{
											obj_t BgL_maxiz00_3569;
											obj_t BgL_miniz00_3568;
											obj_t BgL_lz00_3567;

											BgL_lz00_3567 = BgL_arg2118z00_1063;
											BgL_miniz00_3568 = BgL_arg2119z00_1064;
											BgL_maxiz00_3569 = BgL_arg2120z00_1065;
											BgL_maxiz00_1052 = BgL_maxiz00_3569;
											BgL_miniz00_1051 = BgL_miniz00_3568;
											BgL_lz00_1049 = BgL_lz00_3567;
											goto BGl_infoze70ze7zzjas_asz00;
										}
									}
								}
							else
								{	/* Jas/as.scm 447 */
									obj_t BgL_arg2122z00_1071;
									obj_t BgL_arg2123z00_1072;

									{	/* Jas/as.scm 447 */
										obj_t BgL_list2124z00_1073;

										{	/* Jas/as.scm 447 */
											obj_t BgL_arg2125z00_1074;

											{	/* Jas/as.scm 447 */
												obj_t BgL_arg2126z00_1075;

												BgL_arg2126z00_1075 =
													MAKE_YOUNG_PAIR(BgL_maxiz00_1052, BNIL);
												BgL_arg2125z00_1074 =
													MAKE_YOUNG_PAIR(BgL_miniz00_1051,
													BgL_arg2126z00_1075);
											}
											BgL_list2124z00_1073 =
												MAKE_YOUNG_PAIR(BgL_linez00_1050, BgL_arg2125z00_1074);
										}
										BgL_arg2122z00_1071 = BgL_list2124z00_1073;
									}
									BgL_arg2123z00_1072 =
										BGl_infoze70ze7zzjas_asz00(BgL_lz00_1049, BFALSE,
										BINT(0L), BINT(0L));
									return
										MAKE_YOUNG_PAIR(BgL_arg2122z00_1071, BgL_arg2123z00_1072);
								}
						}
					else
						{	/* Jas/as.scm 441 */
							obj_t BgL_posz00_1078;

							{	/* Jas/as.scm 441 */
								obj_t BgL_pairz00_1934;

								BgL_pairz00_1934 = CAR(((obj_t) BgL_lz00_1049));
								BgL_posz00_1078 = CAR(CDR(CDR(BgL_pairz00_1934)));
							}
							{	/* Jas/as.scm 442 */
								obj_t BgL_arg2130z00_1079;

								{	/* Jas/as.scm 442 */
									obj_t BgL_pairz00_1941;

									BgL_pairz00_1941 = CAR(((obj_t) BgL_lz00_1049));
									BgL_arg2130z00_1079 = CAR(CDR(BgL_pairz00_1941));
								}
								{
									obj_t BgL_maxiz00_3588;
									obj_t BgL_miniz00_3587;
									obj_t BgL_linez00_3586;

									BgL_linez00_3586 = BgL_arg2130z00_1079;
									BgL_miniz00_3587 = BgL_posz00_1078;
									BgL_maxiz00_3588 = BgL_posz00_1078;
									BgL_maxiz00_1052 = BgL_maxiz00_3588;
									BgL_miniz00_1051 = BgL_miniz00_3587;
									BgL_linez00_1050 = BgL_linez00_3586;
									goto BGl_infoze70ze7zzjas_asz00;
								}
							}
						}
				}
		}

	}



/* &<@anonymous:2106> */
	obj_t BGl_z62zc3z04anonymousza32106ze3ze5zzjas_asz00(obj_t BgL_envz00_2050,
		obj_t BgL_xz00_2051, obj_t BgL_yz00_2052)
	{
		{	/* Jas/as.scm 449 */
			{	/* Jas/as.scm 449 */
				bool_t BgL_tmpz00_3589;

				{	/* Jas/as.scm 449 */
					long BgL_auxz00_3595;
					long BgL_tmpz00_3590;

					{	/* Jas/as.scm 449 */
						obj_t BgL_pairz00_2077;

						BgL_pairz00_2077 = CDR(((obj_t) BgL_yz00_2052));
						BgL_auxz00_3595 = (long) CINT(CAR(BgL_pairz00_2077));
					}
					{	/* Jas/as.scm 449 */
						obj_t BgL_pairz00_2076;

						BgL_pairz00_2076 = CDR(((obj_t) BgL_xz00_2051));
						BgL_tmpz00_3590 = (long) CINT(CAR(BgL_pairz00_2076));
					}
					BgL_tmpz00_3589 = (BgL_tmpz00_3590 <= BgL_auxz00_3595);
				}
				return BBOOL(BgL_tmpz00_3589);
			}
		}

	}



/* get-localvars */
	obj_t BGl_getzd2localvarszd2zzjas_asz00(obj_t BgL_lz00_39)
	{
		{	/* Jas/as.scm 453 */
		BGl_getzd2localvarszd2zzjas_asz00:
			if (NULLP(BgL_lz00_39))
				{	/* Jas/as.scm 455 */
					return BgL_lz00_39;
				}
			else
				{	/* Jas/as.scm 456 */
					bool_t BgL_test2452z00_3604;

					{	/* Jas/as.scm 456 */
						obj_t BgL_tmpz00_3605;

						{	/* Jas/as.scm 456 */
							obj_t BgL_pairz00_1960;

							BgL_pairz00_1960 = CAR(((obj_t) BgL_lz00_39));
							BgL_tmpz00_3605 = CAR(BgL_pairz00_1960);
						}
						BgL_test2452z00_3604 = (BgL_tmpz00_3605 == BINT(205L));
					}
					if (BgL_test2452z00_3604)
						{	/* Jas/as.scm 457 */
							obj_t BgL_arg2136z00_1086;
							obj_t BgL_arg2137z00_1087;

							{	/* Jas/as.scm 457 */
								obj_t BgL_pairz00_1964;

								BgL_pairz00_1964 = CAR(((obj_t) BgL_lz00_39));
								BgL_arg2136z00_1086 = CDR(BgL_pairz00_1964);
							}
							{	/* Jas/as.scm 457 */
								obj_t BgL_arg2138z00_1088;

								BgL_arg2138z00_1088 = CDR(((obj_t) BgL_lz00_39));
								BgL_arg2137z00_1087 =
									BGl_getzd2localvarszd2zzjas_asz00(BgL_arg2138z00_1088);
							}
							return MAKE_YOUNG_PAIR(BgL_arg2136z00_1086, BgL_arg2137z00_1087);
						}
					else
						{	/* Jas/as.scm 458 */
							obj_t BgL_arg2139z00_1089;

							BgL_arg2139z00_1089 = CDR(((obj_t) BgL_lz00_39));
							{
								obj_t BgL_lz00_3620;

								BgL_lz00_3620 = BgL_arg2139z00_1089;
								BgL_lz00_39 = BgL_lz00_3620;
								goto BGl_getzd2localvarszd2zzjas_asz00;
							}
						}
				}
		}

	}



/* get-bytecode */
	obj_t BGl_getzd2bytecodezd2zzjas_asz00(obj_t BgL_lz00_40)
	{
		{	/* Jas/as.scm 461 */
		BGl_getzd2bytecodezd2zzjas_asz00:
			if (NULLP(BgL_lz00_40))
				{	/* Jas/as.scm 462 */
					return BgL_lz00_40;
				}
			else
				{	/* Jas/as.scm 463 */
					bool_t BgL_test2454z00_3623;

					{	/* Jas/as.scm 463 */
						obj_t BgL_tmpz00_3624;

						{	/* Jas/as.scm 463 */
							obj_t BgL_pairz00_1970;

							BgL_pairz00_1970 = CAR(((obj_t) BgL_lz00_40));
							BgL_tmpz00_3624 = CAR(BgL_pairz00_1970);
						}
						BgL_test2454z00_3623 = (BgL_tmpz00_3624 == BINT(202L));
					}
					if (BgL_test2454z00_3623)
						{	/* Jas/as.scm 463 */
							obj_t BgL_arg2145z00_1094;

							BgL_arg2145z00_1094 = CDR(((obj_t) BgL_lz00_40));
							{
								obj_t BgL_lz00_3632;

								BgL_lz00_3632 = BgL_arg2145z00_1094;
								BgL_lz00_40 = BgL_lz00_3632;
								goto BGl_getzd2bytecodezd2zzjas_asz00;
							}
						}
					else
						{	/* Jas/as.scm 464 */
							bool_t BgL_test2455z00_3633;

							{	/* Jas/as.scm 464 */
								obj_t BgL_tmpz00_3634;

								{	/* Jas/as.scm 464 */
									obj_t BgL_pairz00_1975;

									BgL_pairz00_1975 = CAR(((obj_t) BgL_lz00_40));
									BgL_tmpz00_3634 = CAR(BgL_pairz00_1975);
								}
								BgL_test2455z00_3633 = (BgL_tmpz00_3634 == BINT(203L));
							}
							if (BgL_test2455z00_3633)
								{	/* Jas/as.scm 464 */
									obj_t BgL_arg2148z00_1097;

									BgL_arg2148z00_1097 = CDR(((obj_t) BgL_lz00_40));
									{
										obj_t BgL_lz00_3642;

										BgL_lz00_3642 = BgL_arg2148z00_1097;
										BgL_lz00_40 = BgL_lz00_3642;
										goto BGl_getzd2bytecodezd2zzjas_asz00;
									}
								}
							else
								{	/* Jas/as.scm 465 */
									bool_t BgL_test2456z00_3643;

									{	/* Jas/as.scm 465 */
										obj_t BgL_tmpz00_3644;

										{	/* Jas/as.scm 465 */
											obj_t BgL_pairz00_1980;

											BgL_pairz00_1980 = CAR(((obj_t) BgL_lz00_40));
											BgL_tmpz00_3644 = CAR(BgL_pairz00_1980);
										}
										BgL_test2456z00_3643 = (BgL_tmpz00_3644 == BINT(204L));
									}
									if (BgL_test2456z00_3643)
										{	/* Jas/as.scm 465 */
											obj_t BgL_arg2151z00_1100;

											BgL_arg2151z00_1100 = CDR(((obj_t) BgL_lz00_40));
											{
												obj_t BgL_lz00_3652;

												BgL_lz00_3652 = BgL_arg2151z00_1100;
												BgL_lz00_40 = BgL_lz00_3652;
												goto BGl_getzd2bytecodezd2zzjas_asz00;
											}
										}
									else
										{	/* Jas/as.scm 466 */
											bool_t BgL_test2457z00_3653;

											{	/* Jas/as.scm 466 */
												obj_t BgL_tmpz00_3654;

												{	/* Jas/as.scm 466 */
													obj_t BgL_pairz00_1985;

													BgL_pairz00_1985 = CAR(((obj_t) BgL_lz00_40));
													BgL_tmpz00_3654 = CAR(BgL_pairz00_1985);
												}
												BgL_test2457z00_3653 = (BgL_tmpz00_3654 == BINT(205L));
											}
											if (BgL_test2457z00_3653)
												{	/* Jas/as.scm 466 */
													obj_t BgL_arg2154z00_1103;

													BgL_arg2154z00_1103 = CDR(((obj_t) BgL_lz00_40));
													{
														obj_t BgL_lz00_3662;

														BgL_lz00_3662 = BgL_arg2154z00_1103;
														BgL_lz00_40 = BgL_lz00_3662;
														goto BGl_getzd2bytecodezd2zzjas_asz00;
													}
												}
											else
												{	/* Jas/as.scm 467 */
													obj_t BgL_arg2155z00_1104;
													obj_t BgL_arg2156z00_1105;

													BgL_arg2155z00_1104 = CAR(((obj_t) BgL_lz00_40));
													{	/* Jas/as.scm 467 */
														obj_t BgL_arg2157z00_1106;

														BgL_arg2157z00_1106 = CDR(((obj_t) BgL_lz00_40));
														BgL_arg2156z00_1105 =
															BGl_getzd2bytecodezd2zzjas_asz00
															(BgL_arg2157z00_1106);
													}
													return
														BGl_appendzd221011zd2zzjas_asz00
														(BgL_arg2155z00_1104, BgL_arg2156z00_1105);
												}
										}
								}
						}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzjas_asz00(void)
	{
		{	/* Jas/as.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzjas_asz00(void)
	{
		{	/* Jas/as.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzjas_asz00(void)
	{
		{	/* Jas/as.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzjas_asz00(void)
	{
		{	/* Jas/as.scm 1 */
			BGl_modulezd2initializa7ationz75zzjas_libz00(267157509L,
				BSTRING_TO_STRING(BGl_string2223z00zzjas_asz00));
			BGl_modulezd2initializa7ationz75zzjas_classfilez00(135094074L,
				BSTRING_TO_STRING(BGl_string2223z00zzjas_asz00));
			BGl_modulezd2initializa7ationz75zzjas_producez00(468081789L,
				BSTRING_TO_STRING(BGl_string2223z00zzjas_asz00));
			BGl_modulezd2initializa7ationz75zzjas_profilez00(474882254L,
				BSTRING_TO_STRING(BGl_string2223z00zzjas_asz00));
			BGl_modulezd2initializa7ationz75zzjas_opcodez00(412836297L,
				BSTRING_TO_STRING(BGl_string2223z00zzjas_asz00));
			BGl_modulezd2initializa7ationz75zzjas_peepz00(121595967L,
				BSTRING_TO_STRING(BGl_string2223z00zzjas_asz00));
			BGl_modulezd2initializa7ationz75zzjas_widez00(108239066L,
				BSTRING_TO_STRING(BGl_string2223z00zzjas_asz00));
			BGl_modulezd2initializa7ationz75zzjas_labelsz00(208213070L,
				BSTRING_TO_STRING(BGl_string2223z00zzjas_asz00));
			return
				BGl_modulezd2initializa7ationz75zzjas_stackz00(163304304L,
				BSTRING_TO_STRING(BGl_string2223z00zzjas_asz00));
		}

	}

#ifdef __cplusplus
}
#endif
