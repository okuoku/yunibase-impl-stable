/*===========================================================================*/
/*   (Stackable/walk.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Stackable/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_STACKABLE_WALK_TYPE_DEFINITIONS
#define BGL_STACKABLE_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_appzf2depthzf2_bgl
	{
		long BgL_depthz00;
	}                     *BgL_appzf2depthzf2_bglt;

	typedef struct BgL_localzf2depthzf2_bgl
	{
		long BgL_depthz00;
		obj_t BgL_ownerz00;
	}                       *BgL_localzf2depthzf2_bglt;


#endif													// BGL_STACKABLE_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static BgL_localz00_bglt BGl_z62lambda1768z62zzstackable_walkz00(obj_t,
		obj_t);
	static obj_t BGl_z62escapez12zd2makezd2box1496z70zzstackable_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62escapez12z70zzstackable_walkz00(obj_t, obj_t, obj_t);
	static BgL_localz00_bglt BGl_z62lambda1772z62zzstackable_walkz00(obj_t,
		obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzstackable_walkz00 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	static obj_t
		BGl_z62depthzd2letzd2setzd2exzd2it1520z62zzstackable_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_funz00zzast_varz00;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t
		BGl_globalzd2depthzd2letz12z12zzstackable_walkz00(BgL_variablez00_bglt);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	extern obj_t BGl_za2optimzd2stackablezf3za2z21zzengine_paramz00;
	static obj_t BGl_z62stackablezd2conditiona1474zb0zzstackable_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_z62nodezd2escape1499zb0zzstackable_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62stackablezd2app1480zb0zzstackable_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzstackable_walkz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_nodezd2escapezd2zzstackable_walkz00(BgL_nodez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	static obj_t BGl_localzf2depthzf2zzstackable_walkz00 = BUNSPEC;
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzstackable_walkz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_varzd2stackablezd2zzstackable_walkz00(BgL_variablez00_bglt,
		obj_t);
	static obj_t BGl_z62stackablezd2letzd2fun1478z62zzstackable_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62stackablez62zzstackable_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzstackable_walkz00(void);
	static obj_t BGl_appzf2depthzf2zzstackable_walkz00 = BUNSPEC;
	static obj_t BGl_z62stackablezd2sequence1484zb0zzstackable_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_za2keyzd2shapezf3za2z21zzengine_paramz00;
	static obj_t
		BGl_globalzd2liftzd2letz12z12zzstackable_walkz00(BgL_variablez00_bglt);
	extern obj_t BGl_typez00zztype_typez00;
	static BgL_nodez00_bglt BGl_z62liftzd2letz12za2zzstackable_walkz00(obj_t,
		obj_t);
	static obj_t BGl_zc3z04anonymousza32075ze3ze70z60zzstackable_walkz00(obj_t);
	static obj_t BGl_z62initzd2stackablezb0zzstackable_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62shapezd2localzf2depth1462z42zzstackable_walkz00(obj_t,
		obj_t);
	static obj_t BGl_stackablez00zzstackable_walkz00(BgL_nodez00_bglt, bool_t,
		long, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzstackable_walkz00(void);
	static obj_t BGl_z62initzd2stackablezd2app1524z62zzstackable_walkz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_liftzd2letz12zc0zzstackable_walkz00(BgL_nodez00_bglt);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static obj_t BGl_z62nodezd2escapezb0zzstackable_walkz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t BGl_z62escapez121485z70zzstackable_walkz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t
		BGl_z62nodezd2ze3sexpzd2appzf2depth1464z73zzstackable_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62liftzd2letz12zd2letzd2var1508za2zzstackable_walkz00(obj_t, obj_t);
	static obj_t BGl_initzd2stackablezd2zzstackable_walkz00(BgL_nodez00_bglt);
	static obj_t BGl_z62liftzd2letz121505za2zzstackable_walkz00(obj_t, obj_t);
	static obj_t
		BGl_z62nodezd2escapezd2makezd2box1504zb0zzstackable_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62depthzd2letzd2letzd2fun1516zb0zzstackable_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static bool_t BGl_liftablezf3ze70z14zzstackable_walkz00(obj_t);
	static obj_t BGl_escapez12z12zzstackable_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_append2(obj_t, obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	extern obj_t BGl_walk0z00zzast_walkz00(BgL_nodez00_bglt, obj_t);
	static obj_t BGl_z62stackable1465z62zzstackable_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_walk1z00zzast_walkz00(BgL_nodez00_bglt, obj_t, obj_t);
	extern obj_t BGl_walk2z00zzast_walkz00(BgL_nodez00_bglt, obj_t, obj_t, obj_t);
	extern obj_t BGl_walk3z00zzast_walkz00(BgL_nodez00_bglt, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62stackablezd2letzd2var1476z62zzstackable_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62escapez12zd2closure1498za2zzstackable_walkz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzstackable_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_occurz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_z62stackablezd2funcall1482zb0zzstackable_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62depthzd2let1509zb0zzstackable_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62escapez12zd2local1490za2zzstackable_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62depthzd2letzd2var1512z62zzstackable_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2escapezd2app1502z62zzstackable_walkz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_cnstzd2initzd2zzstackable_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzstackable_walkz00(void);
	static obj_t BGl_z62stackablezd2closure1470zb0zzstackable_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzstackable_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzstackable_walkz00(void);
	static obj_t BGl_z62initzd2stackable1521zb0zzstackable_walkz00(obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_walk0z12z12zzast_walkz00(BgL_nodez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stackablezd2walkz12zc0zzstackable_walkz00(obj_t);
	static obj_t BGl_z62stackablezd2walkz12za2zzstackable_walkz00(obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_walk1z12z12zzast_walkz00(BgL_nodez00_bglt, obj_t,
		obj_t);
	static obj_t BGl_z62depthzd2letzb0zzstackable_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62depthzd2letzd2app1518z62zzstackable_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62escapez12zd2global1488za2zzstackable_walkz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_valuez00zzast_varz00;
	static obj_t BGl_z62escapez12zd2node1492za2zzstackable_walkz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62initzd2stackablezd2closu1528z62zzstackable_walkz00(obj_t,
		obj_t);
	static BgL_appz00_bglt BGl_z62lambda1725z62zzstackable_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1807z62zzstackable_walkz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1808z62zzstackable_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62depthzd2letzd2letzd2var1514zb0zzstackable_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_callzd2nextzd2methodze70ze7zzstackable_walkz00(BgL_localz00_bglt);
	static obj_t BGl_depthzd2letzd2zzstackable_walkz00(BgL_nodez00_bglt, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza31737ze3ze5zzstackable_walkz00(obj_t,
		obj_t);
	static BgL_appz00_bglt BGl_z62lambda1735z62zzstackable_walkz00(obj_t, obj_t);
	static BgL_appz00_bglt BGl_z62lambda1738z62zzstackable_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1823z62zzstackable_walkz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1824z62zzstackable_walkz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62initzd2stackablezd2makezd21526zb0zzstackable_walkz00(obj_t, obj_t);
	static obj_t BGl_z62stackablezd2var1468zb0zzstackable_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static bool_t BGl_importzf3zf3zzstackable_walkz00(BgL_variablez00_bglt);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t BGl_z62stackablezd2setq1472zb0zzstackable_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_z62zc3z04anonymousza31771ze3ze5zzstackable_walkz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1750z62zzstackable_walkz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1751z62zzstackable_walkz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t BGl_z62escapez12zd2app1494za2zzstackable_walkz00(obj_t, obj_t,
		obj_t);
	static BgL_localz00_bglt BGl_z62lambda1763z62zzstackable_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t __cnst[12];


	   
		 
		DEFINE_STRING(BGl_string2300z00zzstackable_walkz00,
		BgL_bgl_string2300za700za7za7s2384za7, "Stackable", 9);
	      DEFINE_STRING(BGl_string2301z00zzstackable_walkz00,
		BgL_bgl_string2301za700za7za7s2385za7, "   . ", 5);
	      DEFINE_STRING(BGl_string2302z00zzstackable_walkz00,
		BgL_bgl_string2302za700za7za7s2386za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string2303z00zzstackable_walkz00,
		BgL_bgl_string2303za700za7za7s2387za7, " error", 6);
	      DEFINE_STRING(BGl_string2304z00zzstackable_walkz00,
		BgL_bgl_string2304za700za7za7s2388za7, "s", 1);
	      DEFINE_STRING(BGl_string2305z00zzstackable_walkz00,
		BgL_bgl_string2305za700za7za7s2389za7, "", 0);
	      DEFINE_STRING(BGl_string2306z00zzstackable_walkz00,
		BgL_bgl_string2306za700za7za7s2390za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string2307z00zzstackable_walkz00,
		BgL_bgl_string2307za700za7za7s2391za7, "failure during postlude hook", 28);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2308z00zzstackable_walkz00,
		BgL_bgl_za762lambda1751za7622392z00,
		BGl_z62lambda1751z62zzstackable_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2309z00zzstackable_walkz00,
		BgL_bgl_za762lambda1750za7622393z00,
		BGl_z62lambda1750z62zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2310z00zzstackable_walkz00,
		BgL_bgl_za762lambda1738za7622394z00,
		BGl_z62lambda1738z62zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2311z00zzstackable_walkz00,
		BgL_bgl_za762za7c3za704anonymo2395za7,
		BGl_z62zc3z04anonymousza31737ze3ze5zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2312z00zzstackable_walkz00,
		BgL_bgl_za762lambda1735za7622396z00,
		BGl_z62lambda1735z62zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2313z00zzstackable_walkz00,
		BgL_bgl_za762lambda1725za7622397z00,
		BGl_z62lambda1725z62zzstackable_walkz00, 0L, BUNSPEC, 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2314z00zzstackable_walkz00,
		BgL_bgl_za762lambda1808za7622398z00,
		BGl_z62lambda1808z62zzstackable_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2315z00zzstackable_walkz00,
		BgL_bgl_za762lambda1807za7622399z00,
		BGl_z62lambda1807z62zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2316z00zzstackable_walkz00,
		BgL_bgl_za762lambda1824za7622400z00,
		BGl_z62lambda1824z62zzstackable_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2323z00zzstackable_walkz00,
		BgL_bgl_string2323za700za7za7s2401za7, "stackable1465", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2317z00zzstackable_walkz00,
		BgL_bgl_za762lambda1823za7622402z00,
		BGl_z62lambda1823z62zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2318z00zzstackable_walkz00,
		BgL_bgl_za762lambda1772za7622403z00,
		BGl_z62lambda1772z62zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2325z00zzstackable_walkz00,
		BgL_bgl_string2325za700za7za7s2404za7, "escape!1485", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2319z00zzstackable_walkz00,
		BgL_bgl_za762za7c3za704anonymo2405za7,
		BGl_z62zc3z04anonymousza31771ze3ze5zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2327z00zzstackable_walkz00,
		BgL_bgl_string2327za700za7za7s2406za7, "node-escape1499", 15);
	      DEFINE_STRING(BGl_string2329z00zzstackable_walkz00,
		BgL_bgl_string2329za700za7za7s2407za7, "lift-let!1505", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2320z00zzstackable_walkz00,
		BgL_bgl_za762lambda1768za7622408z00,
		BGl_z62lambda1768z62zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2321z00zzstackable_walkz00,
		BgL_bgl_za762lambda1763za7622409z00,
		BGl_z62lambda1763z62zzstackable_walkz00, 0L, BUNSPEC, 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2322z00zzstackable_walkz00,
		BgL_bgl_za762stackable14652410za7,
		BGl_z62stackable1465z62zzstackable_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2324z00zzstackable_walkz00,
		BgL_bgl_za762escapeza71214852411z00,
		BGl_z62escapez121485z70zzstackable_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2331z00zzstackable_walkz00,
		BgL_bgl_string2331za700za7za7s2412za7, "depth-let1509", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2326z00zzstackable_walkz00,
		BgL_bgl_za762nodeza7d2escape2413z00,
		BGl_z62nodezd2escape1499zb0zzstackable_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2333z00zzstackable_walkz00,
		BgL_bgl_string2333za700za7za7s2414za7, "init-stackable1521", 18);
	      DEFINE_STRING(BGl_string2334z00zzstackable_walkz00,
		BgL_bgl_string2334za700za7za7s2415za7, "ESCAPE! should not be here ", 27);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2328z00zzstackable_walkz00,
		BgL_bgl_za762liftza7d2letza7122416za7,
		BGl_z62liftzd2letz121505za2zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2335z00zzstackable_walkz00,
		BgL_bgl_string2335za700za7za7s2417za7, ":", 1);
	      DEFINE_STRING(BGl_string2336z00zzstackable_walkz00,
		BgL_bgl_string2336za700za7za7s2418za7, ",", 1);
	      DEFINE_STRING(BGl_string2337z00zzstackable_walkz00,
		BgL_bgl_string2337za700za7za7s2419za7, "Stackable/walk.scm", 18);
	      DEFINE_STRING(BGl_string2339z00zzstackable_walkz00,
		BgL_bgl_string2339za700za7za7s2420za7, "shape", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2330z00zzstackable_walkz00,
		BgL_bgl_za762depthza7d2let152421z00,
		BGl_z62depthzd2let1509zb0zzstackable_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2332z00zzstackable_walkz00,
		BgL_bgl_za762initza7d2stacka2422z00,
		BGl_z62initzd2stackable1521zb0zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2341z00zzstackable_walkz00,
		BgL_bgl_string2341za700za7za7s2423za7, "node->sexp", 10);
	      DEFINE_STRING(BGl_string2343z00zzstackable_walkz00,
		BgL_bgl_string2343za700za7za7s2424za7, "stackable", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2338z00zzstackable_walkz00,
		BgL_bgl_za762shapeza7d2local2425z00,
		BGl_z62shapezd2localzf2depth1462z42zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2340z00zzstackable_walkz00,
		BgL_bgl_za762nodeza7d2za7e3sex2426za7,
		BGl_z62nodezd2ze3sexpzd2appzf2depth1464z73zzstackable_walkz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2342z00zzstackable_walkz00,
		BgL_bgl_za762stackableza7d2v2427z00,
		BGl_z62stackablezd2var1468zb0zzstackable_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2344z00zzstackable_walkz00,
		BgL_bgl_za762stackableza7d2c2428z00,
		BGl_z62stackablezd2closure1470zb0zzstackable_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2345z00zzstackable_walkz00,
		BgL_bgl_za762stackableza7d2s2429z00,
		BGl_z62stackablezd2setq1472zb0zzstackable_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2346z00zzstackable_walkz00,
		BgL_bgl_za762stackableza7d2c2430z00,
		BGl_z62stackablezd2conditiona1474zb0zzstackable_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2353z00zzstackable_walkz00,
		BgL_bgl_string2353za700za7za7s2431za7, "escape!", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2347z00zzstackable_walkz00,
		BgL_bgl_za762stackableza7d2l2432z00,
		BGl_z62stackablezd2letzd2var1476z62zzstackable_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2348z00zzstackable_walkz00,
		BgL_bgl_za762stackableza7d2l2433z00,
		BGl_z62stackablezd2letzd2fun1478z62zzstackable_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2349z00zzstackable_walkz00,
		BgL_bgl_za762stackableza7d2a2434z00,
		BGl_z62stackablezd2app1480zb0zzstackable_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2359z00zzstackable_walkz00,
		BgL_bgl_string2359za700za7za7s2435za7, "escape!::node", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2350z00zzstackable_walkz00,
		BgL_bgl_za762stackableza7d2f2436z00,
		BGl_z62stackablezd2funcall1482zb0zzstackable_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2351z00zzstackable_walkz00,
		BgL_bgl_za762stackableza7d2s2437z00,
		BGl_z62stackablezd2sequence1484zb0zzstackable_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2352z00zzstackable_walkz00,
		BgL_bgl_za762escapeza712za7d2g2438za7,
		BGl_z62escapez12zd2global1488za2zzstackable_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2354z00zzstackable_walkz00,
		BgL_bgl_za762escapeza712za7d2l2439za7,
		BGl_z62escapez12zd2local1490za2zzstackable_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2361z00zzstackable_walkz00,
		BgL_bgl_string2361za700za7za7s2440za7, "node-escape", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2355z00zzstackable_walkz00,
		BgL_bgl_za762escapeza712za7d2n2441za7,
		BGl_z62escapez12zd2node1492za2zzstackable_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2356z00zzstackable_walkz00,
		BgL_bgl_za762escapeza712za7d2a2442za7,
		BGl_z62escapez12zd2app1494za2zzstackable_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2357z00zzstackable_walkz00,
		BgL_bgl_za762escapeza712za7d2m2443za7,
		BGl_z62escapez12zd2makezd2box1496z70zzstackable_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2364z00zzstackable_walkz00,
		BgL_bgl_string2364za700za7za7s2444za7, "lift-let!::node", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2358z00zzstackable_walkz00,
		BgL_bgl_za762escapeza712za7d2c2445za7,
		BGl_z62escapez12zd2closure1498za2zzstackable_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2366z00zzstackable_walkz00,
		BgL_bgl_string2366za700za7za7s2446za7, "depth-let", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2360z00zzstackable_walkz00,
		BgL_bgl_za762nodeza7d2escape2447z00,
		BGl_z62nodezd2escapezd2app1502z62zzstackable_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2362z00zzstackable_walkz00,
		BgL_bgl_za762nodeza7d2escape2448z00,
		BGl_z62nodezd2escapezd2makezd2box1504zb0zzstackable_walkz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2363z00zzstackable_walkz00,
		BgL_bgl_za762liftza7d2letza7122449za7,
		BGl_z62liftzd2letz12zd2letzd2var1508za2zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2365z00zzstackable_walkz00,
		BgL_bgl_za762depthza7d2letza7d2450za7,
		BGl_z62depthzd2letzd2var1512z62zzstackable_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2372z00zzstackable_walkz00,
		BgL_bgl_string2372za700za7za7s2451za7, "init-stackable", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2367z00zzstackable_walkz00,
		BgL_bgl_za762depthza7d2letza7d2452za7,
		BGl_z62depthzd2letzd2letzd2var1514zb0zzstackable_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2368z00zzstackable_walkz00,
		BgL_bgl_za762depthza7d2letza7d2453za7,
		BGl_z62depthzd2letzd2letzd2fun1516zb0zzstackable_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2375z00zzstackable_walkz00,
		BgL_bgl_string2375za700za7za7s2454za7, "\n", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2369z00zzstackable_walkz00,
		BgL_bgl_za762depthza7d2letza7d2455za7,
		BGl_z62depthzd2letzd2app1518z62zzstackable_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2376z00zzstackable_walkz00,
		BgL_bgl_string2376za700za7za7s2456za7, " ", 1);
	      DEFINE_STRING(BGl_string2377z00zzstackable_walkz00,
		BgL_bgl_string2377za700za7za7s2457za7, "<-", 2);
	      DEFINE_STRING(BGl_string2378z00zzstackable_walkz00,
		BgL_bgl_string2378za700za7za7s2458za7, "      lifting binding ", 22);
	      DEFINE_STRING(BGl_string2379z00zzstackable_walkz00,
		BgL_bgl_string2379za700za7za7s2459za7, "[~a]", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2370z00zzstackable_walkz00,
		BgL_bgl_za762depthza7d2letza7d2460za7,
		BGl_z62depthzd2letzd2setzd2exzd2it1520z62zzstackable_walkz00, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2371z00zzstackable_walkz00,
		BgL_bgl_za762initza7d2stacka2461z00,
		BGl_z62initzd2stackablezd2app1524z62zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2373z00zzstackable_walkz00,
		BgL_bgl_za762initza7d2stacka2462z00,
		BGl_z62initzd2stackablezd2makezd21526zb0zzstackable_walkz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2380z00zzstackable_walkz00,
		BgL_bgl_string2380za700za7za7s2463za7, "[~a]~a", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2374z00zzstackable_walkz00,
		BgL_bgl_za762initza7d2stacka2464z00,
		BGl_z62initzd2stackablezd2closu1528z62zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2381z00zzstackable_walkz00,
		BgL_bgl_string2381za700za7za7s2465za7, "stackable_walk", 14);
	      DEFINE_STRING(BGl_string2382z00zzstackable_walkz00,
		BgL_bgl_string2382za700za7za7s2466za7,
		"* ($make-stack-cell $acons) foreign _ local/depth obj owner stackable_walk app/depth long depth pass-started ",
		109);
	extern obj_t BGl_shapezd2envzd2zztools_shapez00;
	   
		 
		DEFINE_STATIC_BGL_GENERIC(BGl_stackablezd2envzd2zzstackable_walkz00,
		BgL_bgl_za762stackableza762za72467za7,
		BGl_z62stackablez62zzstackable_walkz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stackablezd2walkz12zd2envz12zzstackable_walkz00,
		BgL_bgl_za762stackableza7d2w2468z00,
		BGl_z62stackablezd2walkz12za2zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_liftzd2letz12zd2envz12zzstackable_walkz00,
		BgL_bgl_za762liftza7d2letza7122469za7,
		BGl_z62liftzd2letz12za2zzstackable_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_nodezd2escapezd2envz00zzstackable_walkz00,
		BgL_bgl_za762nodeza7d2escape2470z00,
		BGl_z62nodezd2escapezb0zzstackable_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_GENERIC(BGl_depthzd2letzd2envz00zzstackable_walkz00,
		BgL_bgl_za762depthza7d2letza7b2471za7,
		BGl_z62depthzd2letzb0zzstackable_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_GENERIC(BGl_escapez12zd2envzc0zzstackable_walkz00,
		BgL_bgl_za762escapeza712za770za72472z00,
		BGl_z62escapez12z70zzstackable_walkz00, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_initzd2stackablezd2envz00zzstackable_walkz00,
		BgL_bgl_za762initza7d2stacka2473z00,
		BGl_z62initzd2stackablezb0zzstackable_walkz00, 0L, BUNSPEC, 1);
	extern obj_t BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzstackable_walkz00));
		     ADD_ROOT((void *) (&BGl_localzf2depthzf2zzstackable_walkz00));
		     ADD_ROOT((void *) (&BGl_appzf2depthzf2zzstackable_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzstackable_walkz00(long
		BgL_checksumz00_5237, char *BgL_fromz00_5238)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzstackable_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzstackable_walkz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzstackable_walkz00();
					BGl_libraryzd2moduleszd2initz00zzstackable_walkz00();
					BGl_cnstzd2initzd2zzstackable_walkz00();
					BGl_importedzd2moduleszd2initz00zzstackable_walkz00();
					BGl_objectzd2initzd2zzstackable_walkz00();
					BGl_genericzd2initzd2zzstackable_walkz00();
					BGl_methodzd2initzd2zzstackable_walkz00();
					return BGl_toplevelzd2initzd2zzstackable_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzstackable_walkz00(void)
	{
		{	/* Stackable/walk.scm 16 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "stackable_walk");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"stackable_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"stackable_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"stackable_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"stackable_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"stackable_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "stackable_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"stackable_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "stackable_walk");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"stackable_walk");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "stackable_walk");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"stackable_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"stackable_walk");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "stackable_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzstackable_walkz00(void)
	{
		{	/* Stackable/walk.scm 16 */
			{	/* Stackable/walk.scm 16 */
				obj_t BgL_cportz00_4740;

				{	/* Stackable/walk.scm 16 */
					obj_t BgL_stringz00_4747;

					BgL_stringz00_4747 = BGl_string2382z00zzstackable_walkz00;
					{	/* Stackable/walk.scm 16 */
						obj_t BgL_startz00_4748;

						BgL_startz00_4748 = BINT(0L);
						{	/* Stackable/walk.scm 16 */
							obj_t BgL_endz00_4749;

							BgL_endz00_4749 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4747)));
							{	/* Stackable/walk.scm 16 */

								BgL_cportz00_4740 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4747, BgL_startz00_4748, BgL_endz00_4749);
				}}}}
				{
					long BgL_iz00_4741;

					BgL_iz00_4741 = 11L;
				BgL_loopz00_4742:
					if ((BgL_iz00_4741 == -1L))
						{	/* Stackable/walk.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Stackable/walk.scm 16 */
							{	/* Stackable/walk.scm 16 */
								obj_t BgL_arg2383z00_4743;

								{	/* Stackable/walk.scm 16 */

									{	/* Stackable/walk.scm 16 */
										obj_t BgL_locationz00_4745;

										BgL_locationz00_4745 = BBOOL(((bool_t) 0));
										{	/* Stackable/walk.scm 16 */

											BgL_arg2383z00_4743 =
												BGl_readz00zz__readerz00(BgL_cportz00_4740,
												BgL_locationz00_4745);
										}
									}
								}
								{	/* Stackable/walk.scm 16 */
									int BgL_tmpz00_5273;

									BgL_tmpz00_5273 = (int) (BgL_iz00_4741);
									CNST_TABLE_SET(BgL_tmpz00_5273, BgL_arg2383z00_4743);
							}}
							{	/* Stackable/walk.scm 16 */
								int BgL_auxz00_4746;

								BgL_auxz00_4746 = (int) ((BgL_iz00_4741 - 1L));
								{
									long BgL_iz00_5278;

									BgL_iz00_5278 = (long) (BgL_auxz00_4746);
									BgL_iz00_4741 = BgL_iz00_5278;
									goto BgL_loopz00_4742;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzstackable_walkz00(void)
	{
		{	/* Stackable/walk.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzstackable_walkz00(void)
	{
		{	/* Stackable/walk.scm 16 */
			return BUNSPEC;
		}

	}



/* stackable-walk! */
	BGL_EXPORTED_DEF obj_t BGl_stackablezd2walkz12zc0zzstackable_walkz00(obj_t
		BgL_globalsz00_19)
	{
		{	/* Stackable/walk.scm 68 */
			if (CBOOL(BGl_za2optimzd2stackablezf3za2z21zzengine_paramz00))
				{	/* Stackable/walk.scm 69 */
					{	/* Stackable/walk.scm 71 */
						obj_t BgL_list1565z00_1705;

						{	/* Stackable/walk.scm 71 */
							obj_t BgL_arg1571z00_1706;

							{	/* Stackable/walk.scm 71 */
								obj_t BgL_arg1573z00_1707;

								BgL_arg1573z00_1707 =
									MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
								BgL_arg1571z00_1706 =
									MAKE_YOUNG_PAIR(BGl_string2300z00zzstackable_walkz00,
									BgL_arg1573z00_1707);
							}
							BgL_list1565z00_1705 =
								MAKE_YOUNG_PAIR(BGl_string2301z00zzstackable_walkz00,
								BgL_arg1571z00_1706);
						}
						BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1565z00_1705);
					}
					BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
					BGl_za2currentzd2passza2zd2zzengine_passz00 =
						BGl_string2300z00zzstackable_walkz00;
					{	/* Stackable/walk.scm 71 */
						obj_t BgL_g1166z00_1708;

						BgL_g1166z00_1708 = BNIL;
						{
							obj_t BgL_hooksz00_1711;
							obj_t BgL_hnamesz00_1712;

							BgL_hooksz00_1711 = BgL_g1166z00_1708;
							BgL_hnamesz00_1712 = BNIL;
						BgL_zc3z04anonymousza31574ze3z87_1713:
							if (NULLP(BgL_hooksz00_1711))
								{	/* Stackable/walk.scm 71 */
									CNST_TABLE_REF(0);
								}
							else
								{	/* Stackable/walk.scm 71 */
									bool_t BgL_test2478z00_5293;

									{	/* Stackable/walk.scm 71 */
										obj_t BgL_fun1590z00_1720;

										BgL_fun1590z00_1720 = CAR(((obj_t) BgL_hooksz00_1711));
										BgL_test2478z00_5293 =
											CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1590z00_1720));
									}
									if (BgL_test2478z00_5293)
										{	/* Stackable/walk.scm 71 */
											obj_t BgL_arg1584z00_1717;
											obj_t BgL_arg1585z00_1718;

											BgL_arg1584z00_1717 = CDR(((obj_t) BgL_hooksz00_1711));
											BgL_arg1585z00_1718 = CDR(((obj_t) BgL_hnamesz00_1712));
											{
												obj_t BgL_hnamesz00_5305;
												obj_t BgL_hooksz00_5304;

												BgL_hooksz00_5304 = BgL_arg1584z00_1717;
												BgL_hnamesz00_5305 = BgL_arg1585z00_1718;
												BgL_hnamesz00_1712 = BgL_hnamesz00_5305;
												BgL_hooksz00_1711 = BgL_hooksz00_5304;
												goto BgL_zc3z04anonymousza31574ze3z87_1713;
											}
										}
									else
										{	/* Stackable/walk.scm 71 */
											obj_t BgL_arg1589z00_1719;

											BgL_arg1589z00_1719 = CAR(((obj_t) BgL_hnamesz00_1712));
											BGl_internalzd2errorzd2zztools_errorz00
												(BGl_string2300z00zzstackable_walkz00,
												BGl_string2302z00zzstackable_walkz00,
												BgL_arg1589z00_1719);
										}
								}
						}
					}
					{
						obj_t BgL_l1399z00_1724;

						BgL_l1399z00_1724 = BgL_globalsz00_19;
					BgL_zc3z04anonymousza31592ze3z87_1725:
						if (PAIRP(BgL_l1399z00_1724))
							{	/* Stackable/walk.scm 72 */
								{	/* Stackable/walk.scm 72 */
									obj_t BgL_arg1595z00_1727;

									BgL_arg1595z00_1727 = CAR(BgL_l1399z00_1724);
									BGl_globalzd2liftzd2letz12z12zzstackable_walkz00(
										((BgL_variablez00_bglt) BgL_arg1595z00_1727));
								}
								{
									obj_t BgL_l1399z00_5314;

									BgL_l1399z00_5314 = CDR(BgL_l1399z00_1724);
									BgL_l1399z00_1724 = BgL_l1399z00_5314;
									goto BgL_zc3z04anonymousza31592ze3z87_1725;
								}
							}
						else
							{	/* Stackable/walk.scm 72 */
								((bool_t) 1);
							}
					}
					{
						obj_t BgL_l1401z00_1731;

						BgL_l1401z00_1731 = BgL_globalsz00_19;
					BgL_zc3z04anonymousza31603ze3z87_1732:
						if (PAIRP(BgL_l1401z00_1731))
							{	/* Stackable/walk.scm 73 */
								{	/* Stackable/walk.scm 73 */
									obj_t BgL_arg1605z00_1734;

									BgL_arg1605z00_1734 = CAR(BgL_l1401z00_1731);
									BGl_globalzd2depthzd2letz12z12zzstackable_walkz00(
										((BgL_variablez00_bglt) BgL_arg1605z00_1734));
								}
								{
									obj_t BgL_l1401z00_5321;

									BgL_l1401z00_5321 = CDR(BgL_l1401z00_1731);
									BgL_l1401z00_1731 = BgL_l1401z00_5321;
									goto BgL_zc3z04anonymousza31603ze3z87_1732;
								}
							}
						else
							{	/* Stackable/walk.scm 73 */
								((bool_t) 1);
							}
					}
					{
						obj_t BgL_l1403z00_1738;

						BgL_l1403z00_1738 = BgL_globalsz00_19;
					BgL_zc3z04anonymousza31607ze3z87_1739:
						if (PAIRP(BgL_l1403z00_1738))
							{	/* Stackable/walk.scm 74 */
								{	/* Stackable/walk.scm 74 */
									obj_t BgL_arg1609z00_1741;

									BgL_arg1609z00_1741 = CAR(BgL_l1403z00_1738);
									{	/* Stackable/walk.scm 529 */
										BgL_sfunz00_bglt BgL_i1260z00_3157;

										BgL_i1260z00_3157 =
											((BgL_sfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_arg1609z00_1741)))->
												BgL_valuez00));
										{	/* Stackable/walk.scm 530 */
											obj_t BgL_arg1711z00_3158;

											BgL_arg1711z00_3158 =
												(((BgL_sfunz00_bglt) COBJECT(BgL_i1260z00_3157))->
												BgL_bodyz00);
											BGl_initzd2stackablezd2zzstackable_walkz00((
													(BgL_nodez00_bglt) BgL_arg1711z00_3158));
										}
									}
								}
								{
									obj_t BgL_l1403z00_5332;

									BgL_l1403z00_5332 = CDR(BgL_l1403z00_1738);
									BgL_l1403z00_1738 = BgL_l1403z00_5332;
									goto BgL_zc3z04anonymousza31607ze3z87_1739;
								}
							}
						else
							{	/* Stackable/walk.scm 74 */
								((bool_t) 1);
							}
					}
					{	/* Stackable/walk.scm 75 */
						obj_t BgL_g1168z00_1744;

						BgL_g1168z00_1744 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
						{
							obj_t BgL_ctxz00_1746;

							BgL_ctxz00_1746 = BgL_g1168z00_1744;
						BgL_zc3z04anonymousza31612ze3z87_1747:
							if (CBOOL(CAR(BgL_ctxz00_1746)))
								{	/* Stackable/walk.scm 76 */
									((bool_t) 0);
								}
							else
								{	/* Stackable/walk.scm 76 */
									SET_CAR(BgL_ctxz00_1746, BTRUE);
									SET_CDR(BgL_ctxz00_1746, BNIL);
									{
										obj_t BgL_l1405z00_1750;

										BgL_l1405z00_1750 = BgL_globalsz00_19;
									BgL_zc3z04anonymousza31615ze3z87_1751:
										if (PAIRP(BgL_l1405z00_1750))
											{	/* Stackable/walk.scm 79 */
												{	/* Stackable/walk.scm 79 */
													obj_t BgL_gz00_1753;

													BgL_gz00_1753 = CAR(BgL_l1405z00_1750);
													BGl_varzd2stackablezd2zzstackable_walkz00(
														((BgL_variablez00_bglt) BgL_gz00_1753),
														BgL_ctxz00_1746);
												}
												{
													obj_t BgL_l1405z00_5345;

													BgL_l1405z00_5345 = CDR(BgL_l1405z00_1750);
													BgL_l1405z00_1750 = BgL_l1405z00_5345;
													goto BgL_zc3z04anonymousza31615ze3z87_1751;
												}
											}
										else
											{	/* Stackable/walk.scm 79 */
												((bool_t) 1);
											}
									}
									{

										goto BgL_zc3z04anonymousza31612ze3z87_1747;
									}
								}
						}
					}
					if (
						((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) >
							0L))
						{	/* Stackable/walk.scm 81 */
							{	/* Stackable/walk.scm 81 */
								obj_t BgL_port1407z00_1759;

								{	/* Stackable/walk.scm 81 */
									obj_t BgL_tmpz00_5350;

									BgL_tmpz00_5350 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_port1407z00_1759 =
										BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_5350);
								}
								bgl_display_obj
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
									BgL_port1407z00_1759);
								bgl_display_string(BGl_string2303z00zzstackable_walkz00,
									BgL_port1407z00_1759);
								{	/* Stackable/walk.scm 81 */
									obj_t BgL_arg1627z00_1760;

									{	/* Stackable/walk.scm 81 */
										bool_t BgL_test2485z00_5355;

										if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Stackable/walk.scm 81 */
												if (INTEGERP
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
													{	/* Stackable/walk.scm 81 */
														BgL_test2485z00_5355 =
															(
															(long)
															CINT
															(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
															> 1L);
													}
												else
													{	/* Stackable/walk.scm 81 */
														BgL_test2485z00_5355 =
															BGl_2ze3ze3zz__r4_numbers_6_5z00
															(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
															BINT(1L));
													}
											}
										else
											{	/* Stackable/walk.scm 81 */
												BgL_test2485z00_5355 = ((bool_t) 0);
											}
										if (BgL_test2485z00_5355)
											{	/* Stackable/walk.scm 81 */
												BgL_arg1627z00_1760 =
													BGl_string2304z00zzstackable_walkz00;
											}
										else
											{	/* Stackable/walk.scm 81 */
												BgL_arg1627z00_1760 =
													BGl_string2305z00zzstackable_walkz00;
											}
									}
									bgl_display_obj(BgL_arg1627z00_1760, BgL_port1407z00_1759);
								}
								bgl_display_string(BGl_string2306z00zzstackable_walkz00,
									BgL_port1407z00_1759);
								bgl_display_char(((unsigned char) 10), BgL_port1407z00_1759);
							}
							{	/* Stackable/walk.scm 81 */
								obj_t BgL_list1630z00_1764;

								BgL_list1630z00_1764 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
								BGL_TAIL return BGl_exitz00zz__errorz00(BgL_list1630z00_1764);
							}
						}
					else
						{	/* Stackable/walk.scm 81 */
							obj_t BgL_g1169z00_1765;

							BgL_g1169z00_1765 = BNIL;
							{
								obj_t BgL_hooksz00_1768;
								obj_t BgL_hnamesz00_1769;

								BgL_hooksz00_1768 = BgL_g1169z00_1765;
								BgL_hnamesz00_1769 = BNIL;
							BgL_zc3z04anonymousza31631ze3z87_1770:
								if (NULLP(BgL_hooksz00_1768))
									{	/* Stackable/walk.scm 81 */
										return BgL_globalsz00_19;
									}
								else
									{	/* Stackable/walk.scm 81 */
										bool_t BgL_test2489z00_5372;

										{	/* Stackable/walk.scm 81 */
											obj_t BgL_fun1651z00_1777;

											BgL_fun1651z00_1777 = CAR(((obj_t) BgL_hooksz00_1768));
											BgL_test2489z00_5372 =
												CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1651z00_1777));
										}
										if (BgL_test2489z00_5372)
											{	/* Stackable/walk.scm 81 */
												obj_t BgL_arg1642z00_1774;
												obj_t BgL_arg1646z00_1775;

												BgL_arg1642z00_1774 = CDR(((obj_t) BgL_hooksz00_1768));
												BgL_arg1646z00_1775 = CDR(((obj_t) BgL_hnamesz00_1769));
												{
													obj_t BgL_hnamesz00_5384;
													obj_t BgL_hooksz00_5383;

													BgL_hooksz00_5383 = BgL_arg1642z00_1774;
													BgL_hnamesz00_5384 = BgL_arg1646z00_1775;
													BgL_hnamesz00_1769 = BgL_hnamesz00_5384;
													BgL_hooksz00_1768 = BgL_hooksz00_5383;
													goto BgL_zc3z04anonymousza31631ze3z87_1770;
												}
											}
										else
											{	/* Stackable/walk.scm 81 */
												obj_t BgL_arg1650z00_1776;

												BgL_arg1650z00_1776 = CAR(((obj_t) BgL_hnamesz00_1769));
												return
													BGl_internalzd2errorzd2zztools_errorz00
													(BGl_za2currentzd2passza2zd2zzengine_passz00,
													BGl_string2307z00zzstackable_walkz00,
													BgL_arg1650z00_1776);
											}
									}
							}
						}
				}
			else
				{	/* Stackable/walk.scm 69 */
					return BgL_globalsz00_19;
				}
		}

	}



/* &stackable-walk! */
	obj_t BGl_z62stackablezd2walkz12za2zzstackable_walkz00(obj_t BgL_envz00_4493,
		obj_t BgL_globalsz00_4494)
	{
		{	/* Stackable/walk.scm 68 */
			return BGl_stackablezd2walkz12zc0zzstackable_walkz00(BgL_globalsz00_4494);
		}

	}



/* import? */
	bool_t BGl_importzf3zf3zzstackable_walkz00(BgL_variablez00_bglt BgL_varz00_20)
	{
		{	/* Stackable/walk.scm 87 */
			{	/* Stackable/walk.scm 88 */
				bool_t BgL_test2490z00_5389;

				{	/* Stackable/walk.scm 88 */
					obj_t BgL_classz00_3178;

					BgL_classz00_3178 = BGl_globalz00zzast_varz00;
					{	/* Stackable/walk.scm 88 */
						BgL_objectz00_bglt BgL_arg1807z00_3180;

						{	/* Stackable/walk.scm 88 */
							obj_t BgL_tmpz00_5390;

							BgL_tmpz00_5390 = ((obj_t) BgL_varz00_20);
							BgL_arg1807z00_3180 = (BgL_objectz00_bglt) (BgL_tmpz00_5390);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Stackable/walk.scm 88 */
								long BgL_idxz00_3186;

								BgL_idxz00_3186 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3180);
								BgL_test2490z00_5389 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3186 + 2L)) == BgL_classz00_3178);
							}
						else
							{	/* Stackable/walk.scm 88 */
								bool_t BgL_res2241z00_3211;

								{	/* Stackable/walk.scm 88 */
									obj_t BgL_oclassz00_3194;

									{	/* Stackable/walk.scm 88 */
										obj_t BgL_arg1815z00_3202;
										long BgL_arg1816z00_3203;

										BgL_arg1815z00_3202 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Stackable/walk.scm 88 */
											long BgL_arg1817z00_3204;

											BgL_arg1817z00_3204 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3180);
											BgL_arg1816z00_3203 = (BgL_arg1817z00_3204 - OBJECT_TYPE);
										}
										BgL_oclassz00_3194 =
											VECTOR_REF(BgL_arg1815z00_3202, BgL_arg1816z00_3203);
									}
									{	/* Stackable/walk.scm 88 */
										bool_t BgL__ortest_1115z00_3195;

										BgL__ortest_1115z00_3195 =
											(BgL_classz00_3178 == BgL_oclassz00_3194);
										if (BgL__ortest_1115z00_3195)
											{	/* Stackable/walk.scm 88 */
												BgL_res2241z00_3211 = BgL__ortest_1115z00_3195;
											}
										else
											{	/* Stackable/walk.scm 88 */
												long BgL_odepthz00_3196;

												{	/* Stackable/walk.scm 88 */
													obj_t BgL_arg1804z00_3197;

													BgL_arg1804z00_3197 = (BgL_oclassz00_3194);
													BgL_odepthz00_3196 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3197);
												}
												if ((2L < BgL_odepthz00_3196))
													{	/* Stackable/walk.scm 88 */
														obj_t BgL_arg1802z00_3199;

														{	/* Stackable/walk.scm 88 */
															obj_t BgL_arg1803z00_3200;

															BgL_arg1803z00_3200 = (BgL_oclassz00_3194);
															BgL_arg1802z00_3199 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3200,
																2L);
														}
														BgL_res2241z00_3211 =
															(BgL_arg1802z00_3199 == BgL_classz00_3178);
													}
												else
													{	/* Stackable/walk.scm 88 */
														BgL_res2241z00_3211 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test2490z00_5389 = BgL_res2241z00_3211;
							}
					}
				}
				if (BgL_test2490z00_5389)
					{	/* Stackable/walk.scm 88 */
						bool_t BgL_test2494z00_5412;

						{	/* Stackable/walk.scm 88 */
							obj_t BgL_arg1661z00_1783;

							BgL_arg1661z00_1783 =
								(((BgL_globalz00_bglt) COBJECT(
										((BgL_globalz00_bglt) BgL_varz00_20)))->BgL_modulez00);
							BgL_test2494z00_5412 =
								(BgL_arg1661z00_1783 == BGl_za2moduleza2z00zzmodule_modulez00);
						}
						if (BgL_test2494z00_5412)
							{	/* Stackable/walk.scm 88 */
								return ((bool_t) 0);
							}
						else
							{	/* Stackable/walk.scm 88 */
								return ((bool_t) 1);
							}
					}
				else
					{	/* Stackable/walk.scm 88 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* var-stackable */
	obj_t BGl_varzd2stackablezd2zzstackable_walkz00(BgL_variablez00_bglt
		BgL_varz00_21, obj_t BgL_ctxz00_22)
	{
		{	/* Stackable/walk.scm 93 */
			{	/* Stackable/walk.scm 94 */
				bool_t BgL_test2495z00_5416;

				if (BGl_importzf3zf3zzstackable_walkz00(BgL_varz00_21))
					{	/* Stackable/walk.scm 94 */
						BgL_test2495z00_5416 = ((bool_t) 0);
					}
				else
					{	/* Stackable/walk.scm 94 */
						if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
									((obj_t) BgL_varz00_21), CDR(BgL_ctxz00_22))))
							{	/* Stackable/walk.scm 94 */
								BgL_test2495z00_5416 = ((bool_t) 0);
							}
						else
							{	/* Stackable/walk.scm 94 */
								BgL_test2495z00_5416 = ((bool_t) 1);
							}
					}
				if (BgL_test2495z00_5416)
					{	/* Stackable/walk.scm 94 */
						{	/* Stackable/walk.scm 96 */
							obj_t BgL_arg1681z00_1789;

							{	/* Stackable/walk.scm 96 */
								obj_t BgL_arg1688z00_1790;

								BgL_arg1688z00_1790 = CDR(BgL_ctxz00_22);
								BgL_arg1681z00_1789 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_varz00_21), BgL_arg1688z00_1790);
							}
							SET_CDR(BgL_ctxz00_22, BgL_arg1681z00_1789);
						}
						{	/* Stackable/walk.scm 98 */
							BgL_sfunz00_bglt BgL_i1174z00_1792;

							BgL_i1174z00_1792 =
								((BgL_sfunz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(BgL_varz00_21))->
									BgL_valuez00));
							{	/* Stackable/walk.scm 99 */
								bool_t BgL_test2498z00_5430;

								{	/* Stackable/walk.scm 99 */
									obj_t BgL_arg1701z00_1800;

									BgL_arg1701z00_1800 =
										(((BgL_sfunz00_bglt) COBJECT(BgL_i1174z00_1792))->
										BgL_bodyz00);
									{	/* Stackable/walk.scm 99 */
										obj_t BgL_classz00_3216;

										BgL_classz00_3216 = BGl_nodez00zzast_nodez00;
										if (BGL_OBJECTP(BgL_arg1701z00_1800))
											{	/* Stackable/walk.scm 99 */
												BgL_objectz00_bglt BgL_arg1807z00_3218;

												BgL_arg1807z00_3218 =
													(BgL_objectz00_bglt) (BgL_arg1701z00_1800);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Stackable/walk.scm 99 */
														long BgL_idxz00_3224;

														BgL_idxz00_3224 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3218);
														BgL_test2498z00_5430 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3224 + 1L)) == BgL_classz00_3216);
													}
												else
													{	/* Stackable/walk.scm 99 */
														bool_t BgL_res2242z00_3249;

														{	/* Stackable/walk.scm 99 */
															obj_t BgL_oclassz00_3232;

															{	/* Stackable/walk.scm 99 */
																obj_t BgL_arg1815z00_3240;
																long BgL_arg1816z00_3241;

																BgL_arg1815z00_3240 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Stackable/walk.scm 99 */
																	long BgL_arg1817z00_3242;

																	BgL_arg1817z00_3242 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3218);
																	BgL_arg1816z00_3241 =
																		(BgL_arg1817z00_3242 - OBJECT_TYPE);
																}
																BgL_oclassz00_3232 =
																	VECTOR_REF(BgL_arg1815z00_3240,
																	BgL_arg1816z00_3241);
															}
															{	/* Stackable/walk.scm 99 */
																bool_t BgL__ortest_1115z00_3233;

																BgL__ortest_1115z00_3233 =
																	(BgL_classz00_3216 == BgL_oclassz00_3232);
																if (BgL__ortest_1115z00_3233)
																	{	/* Stackable/walk.scm 99 */
																		BgL_res2242z00_3249 =
																			BgL__ortest_1115z00_3233;
																	}
																else
																	{	/* Stackable/walk.scm 99 */
																		long BgL_odepthz00_3234;

																		{	/* Stackable/walk.scm 99 */
																			obj_t BgL_arg1804z00_3235;

																			BgL_arg1804z00_3235 =
																				(BgL_oclassz00_3232);
																			BgL_odepthz00_3234 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3235);
																		}
																		if ((1L < BgL_odepthz00_3234))
																			{	/* Stackable/walk.scm 99 */
																				obj_t BgL_arg1802z00_3237;

																				{	/* Stackable/walk.scm 99 */
																					obj_t BgL_arg1803z00_3238;

																					BgL_arg1803z00_3238 =
																						(BgL_oclassz00_3232);
																					BgL_arg1802z00_3237 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3238, 1L);
																				}
																				BgL_res2242z00_3249 =
																					(BgL_arg1802z00_3237 ==
																					BgL_classz00_3216);
																			}
																		else
																			{	/* Stackable/walk.scm 99 */
																				BgL_res2242z00_3249 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2498z00_5430 = BgL_res2242z00_3249;
													}
											}
										else
											{	/* Stackable/walk.scm 99 */
												BgL_test2498z00_5430 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test2498z00_5430)
									{	/* Stackable/walk.scm 100 */
										bool_t BgL_test2503z00_5454;

										{	/* Stackable/walk.scm 100 */
											obj_t BgL_classz00_3250;

											BgL_classz00_3250 = BGl_globalz00zzast_varz00;
											{	/* Stackable/walk.scm 100 */
												BgL_objectz00_bglt BgL_arg1807z00_3252;

												{	/* Stackable/walk.scm 100 */
													obj_t BgL_tmpz00_5455;

													BgL_tmpz00_5455 = ((obj_t) BgL_varz00_21);
													BgL_arg1807z00_3252 =
														(BgL_objectz00_bglt) (BgL_tmpz00_5455);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Stackable/walk.scm 100 */
														long BgL_idxz00_3258;

														BgL_idxz00_3258 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3252);
														BgL_test2503z00_5454 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3258 + 2L)) == BgL_classz00_3250);
													}
												else
													{	/* Stackable/walk.scm 100 */
														bool_t BgL_res2243z00_3283;

														{	/* Stackable/walk.scm 100 */
															obj_t BgL_oclassz00_3266;

															{	/* Stackable/walk.scm 100 */
																obj_t BgL_arg1815z00_3274;
																long BgL_arg1816z00_3275;

																BgL_arg1815z00_3274 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Stackable/walk.scm 100 */
																	long BgL_arg1817z00_3276;

																	BgL_arg1817z00_3276 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3252);
																	BgL_arg1816z00_3275 =
																		(BgL_arg1817z00_3276 - OBJECT_TYPE);
																}
																BgL_oclassz00_3266 =
																	VECTOR_REF(BgL_arg1815z00_3274,
																	BgL_arg1816z00_3275);
															}
															{	/* Stackable/walk.scm 100 */
																bool_t BgL__ortest_1115z00_3267;

																BgL__ortest_1115z00_3267 =
																	(BgL_classz00_3250 == BgL_oclassz00_3266);
																if (BgL__ortest_1115z00_3267)
																	{	/* Stackable/walk.scm 100 */
																		BgL_res2243z00_3283 =
																			BgL__ortest_1115z00_3267;
																	}
																else
																	{	/* Stackable/walk.scm 100 */
																		long BgL_odepthz00_3268;

																		{	/* Stackable/walk.scm 100 */
																			obj_t BgL_arg1804z00_3269;

																			BgL_arg1804z00_3269 =
																				(BgL_oclassz00_3266);
																			BgL_odepthz00_3268 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3269);
																		}
																		if ((2L < BgL_odepthz00_3268))
																			{	/* Stackable/walk.scm 100 */
																				obj_t BgL_arg1802z00_3271;

																				{	/* Stackable/walk.scm 100 */
																					obj_t BgL_arg1803z00_3272;

																					BgL_arg1803z00_3272 =
																						(BgL_oclassz00_3266);
																					BgL_arg1802z00_3271 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3272, 2L);
																				}
																				BgL_res2243z00_3283 =
																					(BgL_arg1802z00_3271 ==
																					BgL_classz00_3250);
																			}
																		else
																			{	/* Stackable/walk.scm 100 */
																				BgL_res2243z00_3283 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2503z00_5454 = BgL_res2243z00_3283;
													}
											}
										}
										if (BgL_test2503z00_5454)
											{	/* Stackable/walk.scm 101 */
												obj_t BgL_arg1692z00_1796;

												BgL_arg1692z00_1796 =
													(((BgL_sfunz00_bglt) COBJECT(BgL_i1174z00_1792))->
													BgL_bodyz00);
												return
													BGl_stackablez00zzstackable_walkz00((
														(BgL_nodez00_bglt) BgL_arg1692z00_1796),
													((bool_t) 1), 0L, BgL_ctxz00_22);
											}
										else
											{	/* Stackable/walk.scm 103 */
												obj_t BgL_arg1699z00_1798;
												long BgL_arg1700z00_1799;

												BgL_arg1699z00_1798 =
													(((BgL_sfunz00_bglt) COBJECT(BgL_i1174z00_1792))->
													BgL_bodyz00);
												{
													BgL_localzf2depthzf2_bglt BgL_auxz00_5481;

													{
														obj_t BgL_auxz00_5482;

														{	/* Stackable/walk.scm 103 */
															BgL_objectz00_bglt BgL_tmpz00_5483;

															BgL_tmpz00_5483 =
																((BgL_objectz00_bglt)
																((BgL_localz00_bglt) BgL_varz00_21));
															BgL_auxz00_5482 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_5483);
														}
														BgL_auxz00_5481 =
															((BgL_localzf2depthzf2_bglt) BgL_auxz00_5482);
													}
													BgL_arg1700z00_1799 =
														(((BgL_localzf2depthzf2_bglt)
															COBJECT(BgL_auxz00_5481))->BgL_depthz00);
												}
												return
													BGl_stackablez00zzstackable_walkz00(
													((BgL_nodez00_bglt) BgL_arg1699z00_1798),
													((bool_t) 1), BgL_arg1700z00_1799, BgL_ctxz00_22);
											}
									}
								else
									{	/* Stackable/walk.scm 99 */
										return BFALSE;
									}
							}
						}
					}
				else
					{	/* Stackable/walk.scm 94 */
						return BFALSE;
					}
			}
		}

	}



/* global-lift-let! */
	obj_t BGl_globalzd2liftzd2letz12z12zzstackable_walkz00(BgL_variablez00_bglt
		BgL_varz00_83)
	{
		{	/* Stackable/walk.scm 379 */
			{	/* Stackable/walk.scm 381 */
				BgL_sfunz00_bglt BgL_i1207z00_1805;

				BgL_i1207z00_1805 =
					((BgL_sfunz00_bglt)
					(((BgL_variablez00_bglt) COBJECT(BgL_varz00_83))->BgL_valuez00));
				{
					obj_t BgL_auxz00_5493;

					{	/* Stackable/walk.scm 382 */
						obj_t BgL_arg1703z00_1806;

						BgL_arg1703z00_1806 =
							(((BgL_sfunz00_bglt) COBJECT(BgL_i1207z00_1805))->BgL_bodyz00);
						BgL_auxz00_5493 =
							((obj_t)
							BGl_liftzd2letz12zc0zzstackable_walkz00(
								((BgL_nodez00_bglt) BgL_arg1703z00_1806)));
					}
					return
						((((BgL_sfunz00_bglt) COBJECT(BgL_i1207z00_1805))->BgL_bodyz00) =
						((obj_t) BgL_auxz00_5493), BUNSPEC);
				}
			}
		}

	}



/* global-depth-let! */
	obj_t BGl_globalzd2depthzd2letz12z12zzstackable_walkz00(BgL_variablez00_bglt
		BgL_varz00_86)
	{
		{	/* Stackable/walk.scm 442 */
			{	/* Stackable/walk.scm 444 */
				BgL_sfunz00_bglt BgL_i1223z00_1808;

				BgL_i1223z00_1808 =
					((BgL_sfunz00_bglt)
					(((BgL_variablez00_bglt) COBJECT(BgL_varz00_86))->BgL_valuez00));
				{	/* Stackable/walk.scm 445 */
					obj_t BgL_g1447z00_1809;

					BgL_g1447z00_1809 =
						(((BgL_sfunz00_bglt) COBJECT(BgL_i1223z00_1808))->BgL_argsz00);
					{
						obj_t BgL_l1445z00_1811;

						BgL_l1445z00_1811 = BgL_g1447z00_1809;
					BgL_zc3z04anonymousza31704ze3z87_1812:
						if (PAIRP(BgL_l1445z00_1811))
							{	/* Stackable/walk.scm 445 */
								{	/* Stackable/walk.scm 446 */
									obj_t BgL_az00_1814;

									BgL_az00_1814 = CAR(BgL_l1445z00_1811);
									{	/* Stackable/walk.scm 446 */
										BgL_localzf2depthzf2_bglt BgL_wide1226z00_1817;

										BgL_wide1226z00_1817 =
											((BgL_localzf2depthzf2_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_localzf2depthzf2_bgl))));
										{	/* Stackable/walk.scm 446 */
											obj_t BgL_auxz00_5510;
											BgL_objectz00_bglt BgL_tmpz00_5506;

											BgL_auxz00_5510 = ((obj_t) BgL_wide1226z00_1817);
											BgL_tmpz00_5506 =
												((BgL_objectz00_bglt)
												((BgL_localz00_bglt)
													((BgL_localz00_bglt) BgL_az00_1814)));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5506, BgL_auxz00_5510);
										}
										((BgL_objectz00_bglt)
											((BgL_localz00_bglt)
												((BgL_localz00_bglt) BgL_az00_1814)));
										{	/* Stackable/walk.scm 446 */
											long BgL_arg1708z00_1818;

											BgL_arg1708z00_1818 =
												BGL_CLASS_NUM(BGl_localzf2depthzf2zzstackable_walkz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt)
													((BgL_localz00_bglt)
														((BgL_localz00_bglt) BgL_az00_1814))),
												BgL_arg1708z00_1818);
										}
										((BgL_localz00_bglt)
											((BgL_localz00_bglt)
												((BgL_localz00_bglt) BgL_az00_1814)));
									}
									{
										BgL_localzf2depthzf2_bglt BgL_auxz00_5524;

										{
											obj_t BgL_auxz00_5525;

											{	/* Stackable/walk.scm 448 */
												BgL_objectz00_bglt BgL_tmpz00_5526;

												BgL_tmpz00_5526 =
													((BgL_objectz00_bglt)
													((BgL_localz00_bglt)
														((BgL_localz00_bglt) BgL_az00_1814)));
												BgL_auxz00_5525 = BGL_OBJECT_WIDENING(BgL_tmpz00_5526);
											}
											BgL_auxz00_5524 =
												((BgL_localzf2depthzf2_bglt) BgL_auxz00_5525);
										}
										((((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_5524))->
												BgL_depthz00) = ((long) 0L), BUNSPEC);
									}
									{
										BgL_localzf2depthzf2_bglt BgL_auxz00_5533;

										{
											obj_t BgL_auxz00_5534;

											{	/* Stackable/walk.scm 447 */
												BgL_objectz00_bglt BgL_tmpz00_5535;

												BgL_tmpz00_5535 =
													((BgL_objectz00_bglt)
													((BgL_localz00_bglt)
														((BgL_localz00_bglt) BgL_az00_1814)));
												BgL_auxz00_5534 = BGL_OBJECT_WIDENING(BgL_tmpz00_5535);
											}
											BgL_auxz00_5533 =
												((BgL_localzf2depthzf2_bglt) BgL_auxz00_5534);
										}
										((((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_5533))->
												BgL_ownerz00) =
											((obj_t) ((obj_t) BgL_varz00_86)), BUNSPEC);
									}
									((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_az00_1814));
								}
								{
									obj_t BgL_l1445z00_5545;

									BgL_l1445z00_5545 = CDR(BgL_l1445z00_1811);
									BgL_l1445z00_1811 = BgL_l1445z00_5545;
									goto BgL_zc3z04anonymousza31704ze3z87_1812;
								}
							}
						else
							{	/* Stackable/walk.scm 445 */
								((bool_t) 1);
							}
					}
				}
				{	/* Stackable/walk.scm 450 */
					obj_t BgL_arg1710z00_1822;

					BgL_arg1710z00_1822 =
						(((BgL_sfunz00_bglt) COBJECT(BgL_i1223z00_1808))->BgL_bodyz00);
					return
						BGl_depthzd2letzd2zzstackable_walkz00(
						((BgL_nodez00_bglt) BgL_arg1710z00_1822),
						BINT(0L), ((obj_t) BgL_varz00_86));
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzstackable_walkz00(void)
	{
		{	/* Stackable/walk.scm 16 */
			{	/* Stackable/walk.scm 38 */
				obj_t BgL_arg1722z00_1830;
				obj_t BgL_arg1724z00_1831;

				{	/* Stackable/walk.scm 38 */
					obj_t BgL_v1457z00_1860;

					BgL_v1457z00_1860 = create_vector(1L);
					{	/* Stackable/walk.scm 38 */
						obj_t BgL_arg1747z00_1861;

						BgL_arg1747z00_1861 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc2309z00zzstackable_walkz00,
							BGl_proc2308z00zzstackable_walkz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(2));
						VECTOR_SET(BgL_v1457z00_1860, 0L, BgL_arg1747z00_1861);
					}
					BgL_arg1722z00_1830 = BgL_v1457z00_1860;
				}
				{	/* Stackable/walk.scm 38 */
					obj_t BgL_v1458z00_1871;

					BgL_v1458z00_1871 = create_vector(0L);
					BgL_arg1724z00_1831 = BgL_v1458z00_1871;
				}
				BGl_appzf2depthzf2zzstackable_walkz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(3),
					CNST_TABLE_REF(4), BGl_appz00zzast_nodez00, 17275L,
					BGl_proc2313z00zzstackable_walkz00,
					BGl_proc2312z00zzstackable_walkz00, BFALSE,
					BGl_proc2311z00zzstackable_walkz00,
					BGl_proc2310z00zzstackable_walkz00, BgL_arg1722z00_1830,
					BgL_arg1724z00_1831);
			}
			{	/* Stackable/walk.scm 40 */
				obj_t BgL_arg1761z00_1880;
				obj_t BgL_arg1762z00_1881;

				{	/* Stackable/walk.scm 40 */
					obj_t BgL_v1459z00_1917;

					BgL_v1459z00_1917 = create_vector(2L);
					{	/* Stackable/walk.scm 40 */
						obj_t BgL_arg1799z00_1918;

						BgL_arg1799z00_1918 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc2315z00zzstackable_walkz00,
							BGl_proc2314z00zzstackable_walkz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(2));
						VECTOR_SET(BgL_v1459z00_1917, 0L, BgL_arg1799z00_1918);
					}
					{	/* Stackable/walk.scm 40 */
						obj_t BgL_arg1812z00_1928;

						BgL_arg1812z00_1928 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(5),
							BGl_proc2317z00zzstackable_walkz00,
							BGl_proc2316z00zzstackable_walkz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(6));
						VECTOR_SET(BgL_v1459z00_1917, 1L, BgL_arg1812z00_1928);
					}
					BgL_arg1761z00_1880 = BgL_v1459z00_1917;
				}
				{	/* Stackable/walk.scm 40 */
					obj_t BgL_v1460z00_1938;

					BgL_v1460z00_1938 = create_vector(0L);
					BgL_arg1762z00_1881 = BgL_v1460z00_1938;
				}
				return (BGl_localzf2depthzf2zzstackable_walkz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(7),
						CNST_TABLE_REF(4), BGl_localz00zzast_varz00, 2096L,
						BGl_proc2321z00zzstackable_walkz00,
						BGl_proc2320z00zzstackable_walkz00, BFALSE,
						BGl_proc2319z00zzstackable_walkz00,
						BGl_proc2318z00zzstackable_walkz00, BgL_arg1761z00_1880,
						BgL_arg1762z00_1881), BUNSPEC);
			}
		}

	}



/* &lambda1772 */
	BgL_localz00_bglt BGl_z62lambda1772z62zzstackable_walkz00(obj_t
		BgL_envz00_4509, obj_t BgL_o1140z00_4510)
	{
		{	/* Stackable/walk.scm 40 */
			{	/* Stackable/walk.scm 40 */
				long BgL_arg1773z00_4752;

				{	/* Stackable/walk.scm 40 */
					obj_t BgL_arg1775z00_4753;

					{	/* Stackable/walk.scm 40 */
						obj_t BgL_arg1798z00_4754;

						{	/* Stackable/walk.scm 40 */
							obj_t BgL_arg1815z00_4755;
							long BgL_arg1816z00_4756;

							BgL_arg1815z00_4755 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Stackable/walk.scm 40 */
								long BgL_arg1817z00_4757;

								BgL_arg1817z00_4757 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_o1140z00_4510)));
								BgL_arg1816z00_4756 = (BgL_arg1817z00_4757 - OBJECT_TYPE);
							}
							BgL_arg1798z00_4754 =
								VECTOR_REF(BgL_arg1815z00_4755, BgL_arg1816z00_4756);
						}
						BgL_arg1775z00_4753 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1798z00_4754);
					}
					{	/* Stackable/walk.scm 40 */
						obj_t BgL_tmpz00_5581;

						BgL_tmpz00_5581 = ((obj_t) BgL_arg1775z00_4753);
						BgL_arg1773z00_4752 = BGL_CLASS_NUM(BgL_tmpz00_5581);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_localz00_bglt) BgL_o1140z00_4510)), BgL_arg1773z00_4752);
			}
			{	/* Stackable/walk.scm 40 */
				BgL_objectz00_bglt BgL_tmpz00_5587;

				BgL_tmpz00_5587 =
					((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1140z00_4510));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5587, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1140z00_4510));
			return ((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1140z00_4510));
		}

	}



/* &<@anonymous:1771> */
	obj_t BGl_z62zc3z04anonymousza31771ze3ze5zzstackable_walkz00(obj_t
		BgL_envz00_4511, obj_t BgL_new1139z00_4512)
	{
		{	/* Stackable/walk.scm 40 */
			{
				BgL_localz00_bglt BgL_auxz00_5595;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1139z00_4512))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(8)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1139z00_4512))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_5603;

					{	/* Stackable/walk.scm 40 */
						obj_t BgL_classz00_4759;

						BgL_classz00_4759 = BGl_typez00zztype_typez00;
						{	/* Stackable/walk.scm 40 */
							obj_t BgL__ortest_1117z00_4760;

							BgL__ortest_1117z00_4760 = BGL_CLASS_NIL(BgL_classz00_4759);
							if (CBOOL(BgL__ortest_1117z00_4760))
								{	/* Stackable/walk.scm 40 */
									BgL_auxz00_5603 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_4760);
								}
							else
								{	/* Stackable/walk.scm 40 */
									BgL_auxz00_5603 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4759));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1139z00_4512))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_5603), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_5613;

					{	/* Stackable/walk.scm 40 */
						obj_t BgL_classz00_4761;

						BgL_classz00_4761 = BGl_valuez00zzast_varz00;
						{	/* Stackable/walk.scm 40 */
							obj_t BgL__ortest_1117z00_4762;

							BgL__ortest_1117z00_4762 = BGL_CLASS_NIL(BgL_classz00_4761);
							if (CBOOL(BgL__ortest_1117z00_4762))
								{	/* Stackable/walk.scm 40 */
									BgL_auxz00_5613 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_4762);
								}
							else
								{	/* Stackable/walk.scm 40 */
									BgL_auxz00_5613 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4761));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1139z00_4512))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_5613), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1139z00_4512))))->BgL_accessz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1139z00_4512))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1139z00_4512))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1139z00_4512))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1139z00_4512))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1139z00_4512))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1139z00_4512))))->BgL_keyz00) =
					((long) 0L), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1139z00_4512))))->BgL_valzd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1139z00_4512))))->BgL_volatilez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_localzf2depthzf2_bglt BgL_auxz00_5650;

					{
						obj_t BgL_auxz00_5651;

						{	/* Stackable/walk.scm 40 */
							BgL_objectz00_bglt BgL_tmpz00_5652;

							BgL_tmpz00_5652 =
								((BgL_objectz00_bglt)
								((BgL_localz00_bglt) BgL_new1139z00_4512));
							BgL_auxz00_5651 = BGL_OBJECT_WIDENING(BgL_tmpz00_5652);
						}
						BgL_auxz00_5650 = ((BgL_localzf2depthzf2_bglt) BgL_auxz00_5651);
					}
					((((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_5650))->
							BgL_depthz00) = ((long) 0L), BUNSPEC);
				}
				{
					BgL_localzf2depthzf2_bglt BgL_auxz00_5658;

					{
						obj_t BgL_auxz00_5659;

						{	/* Stackable/walk.scm 40 */
							BgL_objectz00_bglt BgL_tmpz00_5660;

							BgL_tmpz00_5660 =
								((BgL_objectz00_bglt)
								((BgL_localz00_bglt) BgL_new1139z00_4512));
							BgL_auxz00_5659 = BGL_OBJECT_WIDENING(BgL_tmpz00_5660);
						}
						BgL_auxz00_5658 = ((BgL_localzf2depthzf2_bglt) BgL_auxz00_5659);
					}
					((((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_5658))->
							BgL_ownerz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_5595 = ((BgL_localz00_bglt) BgL_new1139z00_4512);
				return ((obj_t) BgL_auxz00_5595);
			}
		}

	}



/* &lambda1768 */
	BgL_localz00_bglt BGl_z62lambda1768z62zzstackable_walkz00(obj_t
		BgL_envz00_4513, obj_t BgL_o1136z00_4514)
	{
		{	/* Stackable/walk.scm 40 */
			{	/* Stackable/walk.scm 40 */
				BgL_localzf2depthzf2_bglt BgL_wide1138z00_4764;

				BgL_wide1138z00_4764 =
					((BgL_localzf2depthzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_localzf2depthzf2_bgl))));
				{	/* Stackable/walk.scm 40 */
					obj_t BgL_auxz00_5673;
					BgL_objectz00_bglt BgL_tmpz00_5669;

					BgL_auxz00_5673 = ((obj_t) BgL_wide1138z00_4764);
					BgL_tmpz00_5669 =
						((BgL_objectz00_bglt)
						((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1136z00_4514)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5669, BgL_auxz00_5673);
				}
				((BgL_objectz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1136z00_4514)));
				{	/* Stackable/walk.scm 40 */
					long BgL_arg1770z00_4765;

					BgL_arg1770z00_4765 =
						BGL_CLASS_NUM(BGl_localzf2depthzf2zzstackable_walkz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_localz00_bglt)
								((BgL_localz00_bglt) BgL_o1136z00_4514))), BgL_arg1770z00_4765);
				}
				return
					((BgL_localz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1136z00_4514)));
			}
		}

	}



/* &lambda1763 */
	BgL_localz00_bglt BGl_z62lambda1763z62zzstackable_walkz00(obj_t
		BgL_envz00_4515, obj_t BgL_id1121z00_4516, obj_t BgL_name1122z00_4517,
		obj_t BgL_type1123z00_4518, obj_t BgL_value1124z00_4519,
		obj_t BgL_access1125z00_4520, obj_t BgL_fastzd2alpha1126zd2_4521,
		obj_t BgL_removable1127z00_4522, obj_t BgL_occurrence1128z00_4523,
		obj_t BgL_occurrencew1129z00_4524, obj_t BgL_userzf31130zf3_4525,
		obj_t BgL_key1131z00_4526, obj_t BgL_valzd2noescape1132zd2_4527,
		obj_t BgL_volatile1133z00_4528, obj_t BgL_depth1134z00_4529,
		obj_t BgL_owner1135z00_4530)
	{
		{	/* Stackable/walk.scm 40 */
			{	/* Stackable/walk.scm 40 */
				long BgL_occurrence1128z00_4769;
				long BgL_occurrencew1129z00_4770;
				bool_t BgL_userzf31130zf3_4771;
				long BgL_key1131z00_4772;
				bool_t BgL_volatile1133z00_4773;
				long BgL_depth1134z00_4774;

				BgL_occurrence1128z00_4769 = (long) CINT(BgL_occurrence1128z00_4523);
				BgL_occurrencew1129z00_4770 = (long) CINT(BgL_occurrencew1129z00_4524);
				BgL_userzf31130zf3_4771 = CBOOL(BgL_userzf31130zf3_4525);
				BgL_key1131z00_4772 = (long) CINT(BgL_key1131z00_4526);
				BgL_volatile1133z00_4773 = CBOOL(BgL_volatile1133z00_4528);
				BgL_depth1134z00_4774 = (long) CINT(BgL_depth1134z00_4529);
				{	/* Stackable/walk.scm 40 */
					BgL_localz00_bglt BgL_new1272z00_4775;

					{	/* Stackable/walk.scm 40 */
						BgL_localz00_bglt BgL_tmp1270z00_4776;
						BgL_localzf2depthzf2_bglt BgL_wide1271z00_4777;

						{
							BgL_localz00_bglt BgL_auxz00_5693;

							{	/* Stackable/walk.scm 40 */
								BgL_localz00_bglt BgL_new1269z00_4778;

								BgL_new1269z00_4778 =
									((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_localz00_bgl))));
								{	/* Stackable/walk.scm 40 */
									long BgL_arg1767z00_4779;

									BgL_arg1767z00_4779 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1269z00_4778),
										BgL_arg1767z00_4779);
								}
								{	/* Stackable/walk.scm 40 */
									BgL_objectz00_bglt BgL_tmpz00_5698;

									BgL_tmpz00_5698 = ((BgL_objectz00_bglt) BgL_new1269z00_4778);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5698, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1269z00_4778);
								BgL_auxz00_5693 = BgL_new1269z00_4778;
							}
							BgL_tmp1270z00_4776 = ((BgL_localz00_bglt) BgL_auxz00_5693);
						}
						BgL_wide1271z00_4777 =
							((BgL_localzf2depthzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_localzf2depthzf2_bgl))));
						{	/* Stackable/walk.scm 40 */
							obj_t BgL_auxz00_5706;
							BgL_objectz00_bglt BgL_tmpz00_5704;

							BgL_auxz00_5706 = ((obj_t) BgL_wide1271z00_4777);
							BgL_tmpz00_5704 = ((BgL_objectz00_bglt) BgL_tmp1270z00_4776);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5704, BgL_auxz00_5706);
						}
						((BgL_objectz00_bglt) BgL_tmp1270z00_4776);
						{	/* Stackable/walk.scm 40 */
							long BgL_arg1765z00_4780;

							BgL_arg1765z00_4780 =
								BGL_CLASS_NUM(BGl_localzf2depthzf2zzstackable_walkz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1270z00_4776),
								BgL_arg1765z00_4780);
						}
						BgL_new1272z00_4775 = ((BgL_localz00_bglt) BgL_tmp1270z00_4776);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1272z00_4775)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1121z00_4516)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1272z00_4775)))->BgL_namez00) =
						((obj_t) BgL_name1122z00_4517), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1272z00_4775)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1123z00_4518)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1272z00_4775)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1124z00_4519)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1272z00_4775)))->BgL_accessz00) =
						((obj_t) BgL_access1125z00_4520), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1272z00_4775)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1126zd2_4521), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1272z00_4775)))->BgL_removablez00) =
						((obj_t) BgL_removable1127z00_4522), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1272z00_4775)))->BgL_occurrencez00) =
						((long) BgL_occurrence1128z00_4769), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1272z00_4775)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1129z00_4770), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1272z00_4775)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31130zf3_4771), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1272z00_4775)))->BgL_keyz00) =
						((long) BgL_key1131z00_4772), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1272z00_4775)))->BgL_valzd2noescapezd2) =
						((obj_t) BgL_valzd2noescape1132zd2_4527), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1272z00_4775)))->BgL_volatilez00) =
						((bool_t) BgL_volatile1133z00_4773), BUNSPEC);
					{
						BgL_localzf2depthzf2_bglt BgL_auxz00_5743;

						{
							obj_t BgL_auxz00_5744;

							{	/* Stackable/walk.scm 40 */
								BgL_objectz00_bglt BgL_tmpz00_5745;

								BgL_tmpz00_5745 = ((BgL_objectz00_bglt) BgL_new1272z00_4775);
								BgL_auxz00_5744 = BGL_OBJECT_WIDENING(BgL_tmpz00_5745);
							}
							BgL_auxz00_5743 = ((BgL_localzf2depthzf2_bglt) BgL_auxz00_5744);
						}
						((((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_5743))->
								BgL_depthz00) = ((long) BgL_depth1134z00_4774), BUNSPEC);
					}
					{
						BgL_localzf2depthzf2_bglt BgL_auxz00_5750;

						{
							obj_t BgL_auxz00_5751;

							{	/* Stackable/walk.scm 40 */
								BgL_objectz00_bglt BgL_tmpz00_5752;

								BgL_tmpz00_5752 = ((BgL_objectz00_bglt) BgL_new1272z00_4775);
								BgL_auxz00_5751 = BGL_OBJECT_WIDENING(BgL_tmpz00_5752);
							}
							BgL_auxz00_5750 = ((BgL_localzf2depthzf2_bglt) BgL_auxz00_5751);
						}
						((((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_5750))->
								BgL_ownerz00) = ((obj_t) BgL_owner1135z00_4530), BUNSPEC);
					}
					return BgL_new1272z00_4775;
				}
			}
		}

	}



/* &lambda1824 */
	obj_t BGl_z62lambda1824z62zzstackable_walkz00(obj_t BgL_envz00_4531,
		obj_t BgL_oz00_4532, obj_t BgL_vz00_4533)
	{
		{	/* Stackable/walk.scm 40 */
			{
				BgL_localzf2depthzf2_bglt BgL_auxz00_5757;

				{
					obj_t BgL_auxz00_5758;

					{	/* Stackable/walk.scm 40 */
						BgL_objectz00_bglt BgL_tmpz00_5759;

						BgL_tmpz00_5759 =
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_4532));
						BgL_auxz00_5758 = BGL_OBJECT_WIDENING(BgL_tmpz00_5759);
					}
					BgL_auxz00_5757 = ((BgL_localzf2depthzf2_bglt) BgL_auxz00_5758);
				}
				return
					((((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_5757))->
						BgL_ownerz00) = ((obj_t) BgL_vz00_4533), BUNSPEC);
			}
		}

	}



/* &lambda1823 */
	obj_t BGl_z62lambda1823z62zzstackable_walkz00(obj_t BgL_envz00_4534,
		obj_t BgL_oz00_4535)
	{
		{	/* Stackable/walk.scm 40 */
			{
				BgL_localzf2depthzf2_bglt BgL_auxz00_5765;

				{
					obj_t BgL_auxz00_5766;

					{	/* Stackable/walk.scm 40 */
						BgL_objectz00_bglt BgL_tmpz00_5767;

						BgL_tmpz00_5767 =
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_4535));
						BgL_auxz00_5766 = BGL_OBJECT_WIDENING(BgL_tmpz00_5767);
					}
					BgL_auxz00_5765 = ((BgL_localzf2depthzf2_bglt) BgL_auxz00_5766);
				}
				return
					(((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_5765))->
					BgL_ownerz00);
			}
		}

	}



/* &lambda1808 */
	obj_t BGl_z62lambda1808z62zzstackable_walkz00(obj_t BgL_envz00_4536,
		obj_t BgL_oz00_4537, obj_t BgL_vz00_4538)
	{
		{	/* Stackable/walk.scm 40 */
			{	/* Stackable/walk.scm 40 */
				long BgL_vz00_4784;

				BgL_vz00_4784 = (long) CINT(BgL_vz00_4538);
				{
					BgL_localzf2depthzf2_bglt BgL_auxz00_5774;

					{
						obj_t BgL_auxz00_5775;

						{	/* Stackable/walk.scm 40 */
							BgL_objectz00_bglt BgL_tmpz00_5776;

							BgL_tmpz00_5776 =
								((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_4537));
							BgL_auxz00_5775 = BGL_OBJECT_WIDENING(BgL_tmpz00_5776);
						}
						BgL_auxz00_5774 = ((BgL_localzf2depthzf2_bglt) BgL_auxz00_5775);
					}
					return
						((((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_5774))->
							BgL_depthz00) = ((long) BgL_vz00_4784), BUNSPEC);
		}}}

	}



/* &lambda1807 */
	obj_t BGl_z62lambda1807z62zzstackable_walkz00(obj_t BgL_envz00_4539,
		obj_t BgL_oz00_4540)
	{
		{	/* Stackable/walk.scm 40 */
			{	/* Stackable/walk.scm 40 */
				long BgL_tmpz00_5782;

				{
					BgL_localzf2depthzf2_bglt BgL_auxz00_5783;

					{
						obj_t BgL_auxz00_5784;

						{	/* Stackable/walk.scm 40 */
							BgL_objectz00_bglt BgL_tmpz00_5785;

							BgL_tmpz00_5785 =
								((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_4540));
							BgL_auxz00_5784 = BGL_OBJECT_WIDENING(BgL_tmpz00_5785);
						}
						BgL_auxz00_5783 = ((BgL_localzf2depthzf2_bglt) BgL_auxz00_5784);
					}
					BgL_tmpz00_5782 =
						(((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_5783))->
						BgL_depthz00);
				}
				return BINT(BgL_tmpz00_5782);
			}
		}

	}



/* &lambda1738 */
	BgL_appz00_bglt BGl_z62lambda1738z62zzstackable_walkz00(obj_t BgL_envz00_4541,
		obj_t BgL_o1119z00_4542)
	{
		{	/* Stackable/walk.scm 38 */
			{	/* Stackable/walk.scm 38 */
				long BgL_arg1739z00_4787;

				{	/* Stackable/walk.scm 38 */
					obj_t BgL_arg1740z00_4788;

					{	/* Stackable/walk.scm 38 */
						obj_t BgL_arg1746z00_4789;

						{	/* Stackable/walk.scm 38 */
							obj_t BgL_arg1815z00_4790;
							long BgL_arg1816z00_4791;

							BgL_arg1815z00_4790 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Stackable/walk.scm 38 */
								long BgL_arg1817z00_4792;

								BgL_arg1817z00_4792 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_o1119z00_4542)));
								BgL_arg1816z00_4791 = (BgL_arg1817z00_4792 - OBJECT_TYPE);
							}
							BgL_arg1746z00_4789 =
								VECTOR_REF(BgL_arg1815z00_4790, BgL_arg1816z00_4791);
						}
						BgL_arg1740z00_4788 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1746z00_4789);
					}
					{	/* Stackable/walk.scm 38 */
						obj_t BgL_tmpz00_5799;

						BgL_tmpz00_5799 = ((obj_t) BgL_arg1740z00_4788);
						BgL_arg1739z00_4787 = BGL_CLASS_NUM(BgL_tmpz00_5799);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_appz00_bglt) BgL_o1119z00_4542)), BgL_arg1739z00_4787);
			}
			{	/* Stackable/walk.scm 38 */
				BgL_objectz00_bglt BgL_tmpz00_5805;

				BgL_tmpz00_5805 =
					((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_o1119z00_4542));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5805, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_o1119z00_4542));
			return ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_o1119z00_4542));
		}

	}



/* &<@anonymous:1737> */
	obj_t BGl_z62zc3z04anonymousza31737ze3ze5zzstackable_walkz00(obj_t
		BgL_envz00_4543, obj_t BgL_new1118z00_4544)
	{
		{	/* Stackable/walk.scm 38 */
			{
				BgL_appz00_bglt BgL_auxz00_5813;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_appz00_bglt) BgL_new1118z00_4544))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_5817;

					{	/* Stackable/walk.scm 38 */
						obj_t BgL_classz00_4794;

						BgL_classz00_4794 = BGl_typez00zztype_typez00;
						{	/* Stackable/walk.scm 38 */
							obj_t BgL__ortest_1117z00_4795;

							BgL__ortest_1117z00_4795 = BGL_CLASS_NIL(BgL_classz00_4794);
							if (CBOOL(BgL__ortest_1117z00_4795))
								{	/* Stackable/walk.scm 38 */
									BgL_auxz00_5817 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_4795);
								}
							else
								{	/* Stackable/walk.scm 38 */
									BgL_auxz00_5817 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4794));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_appz00_bglt) BgL_new1118z00_4544))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_5817), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_appz00_bglt) BgL_new1118z00_4544))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_appz00_bglt)
										BgL_new1118z00_4544))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_varz00_bglt BgL_auxz00_5833;

					{	/* Stackable/walk.scm 38 */
						obj_t BgL_classz00_4796;

						BgL_classz00_4796 = BGl_varz00zzast_nodez00;
						{	/* Stackable/walk.scm 38 */
							obj_t BgL__ortest_1117z00_4797;

							BgL__ortest_1117z00_4797 = BGL_CLASS_NIL(BgL_classz00_4796);
							if (CBOOL(BgL__ortest_1117z00_4797))
								{	/* Stackable/walk.scm 38 */
									BgL_auxz00_5833 =
										((BgL_varz00_bglt) BgL__ortest_1117z00_4797);
								}
							else
								{	/* Stackable/walk.scm 38 */
									BgL_auxz00_5833 =
										((BgL_varz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4796));
								}
						}
					}
					((((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_new1118z00_4544))))->BgL_funz00) =
						((BgL_varz00_bglt) BgL_auxz00_5833), BUNSPEC);
				}
				((((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt)
									((BgL_appz00_bglt) BgL_new1118z00_4544))))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) ((BgL_appz00_bglt)
										BgL_new1118z00_4544))))->BgL_stackablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_appzf2depthzf2_bglt BgL_auxz00_5849;

					{
						obj_t BgL_auxz00_5850;

						{	/* Stackable/walk.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_5851;

							BgL_tmpz00_5851 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_new1118z00_4544));
							BgL_auxz00_5850 = BGL_OBJECT_WIDENING(BgL_tmpz00_5851);
						}
						BgL_auxz00_5849 = ((BgL_appzf2depthzf2_bglt) BgL_auxz00_5850);
					}
					((((BgL_appzf2depthzf2_bglt) COBJECT(BgL_auxz00_5849))->
							BgL_depthz00) = ((long) 0L), BUNSPEC);
				}
				BgL_auxz00_5813 = ((BgL_appz00_bglt) BgL_new1118z00_4544);
				return ((obj_t) BgL_auxz00_5813);
			}
		}

	}



/* &lambda1735 */
	BgL_appz00_bglt BGl_z62lambda1735z62zzstackable_walkz00(obj_t BgL_envz00_4545,
		obj_t BgL_o1115z00_4546)
	{
		{	/* Stackable/walk.scm 38 */
			{	/* Stackable/walk.scm 38 */
				BgL_appzf2depthzf2_bglt BgL_wide1117z00_4799;

				BgL_wide1117z00_4799 =
					((BgL_appzf2depthzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_appzf2depthzf2_bgl))));
				{	/* Stackable/walk.scm 38 */
					obj_t BgL_auxz00_5864;
					BgL_objectz00_bglt BgL_tmpz00_5860;

					BgL_auxz00_5864 = ((obj_t) BgL_wide1117z00_4799);
					BgL_tmpz00_5860 =
						((BgL_objectz00_bglt)
						((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_o1115z00_4546)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5860, BgL_auxz00_5864);
				}
				((BgL_objectz00_bglt)
					((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_o1115z00_4546)));
				{	/* Stackable/walk.scm 38 */
					long BgL_arg1736z00_4800;

					BgL_arg1736z00_4800 =
						BGL_CLASS_NUM(BGl_appzf2depthzf2zzstackable_walkz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) BgL_o1115z00_4546))), BgL_arg1736z00_4800);
				}
				return
					((BgL_appz00_bglt)
					((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_o1115z00_4546)));
			}
		}

	}



/* &lambda1725 */
	BgL_appz00_bglt BGl_z62lambda1725z62zzstackable_walkz00(obj_t BgL_envz00_4547,
		obj_t BgL_loc1107z00_4548, obj_t BgL_type1108z00_4549,
		obj_t BgL_sidezd2effect1109zd2_4550, obj_t BgL_key1110z00_4551,
		obj_t BgL_fun1111z00_4552, obj_t BgL_args1112z00_4553,
		obj_t BgL_stackable1113z00_4554, obj_t BgL_depth1114z00_4555)
	{
		{	/* Stackable/walk.scm 38 */
			{	/* Stackable/walk.scm 38 */
				long BgL_depth1114z00_4803;

				BgL_depth1114z00_4803 = (long) CINT(BgL_depth1114z00_4555);
				{	/* Stackable/walk.scm 38 */
					BgL_appz00_bglt BgL_new1267z00_4804;

					{	/* Stackable/walk.scm 38 */
						BgL_appz00_bglt BgL_tmp1265z00_4805;
						BgL_appzf2depthzf2_bglt BgL_wide1266z00_4806;

						{
							BgL_appz00_bglt BgL_auxz00_5879;

							{	/* Stackable/walk.scm 38 */
								BgL_appz00_bglt BgL_new1264z00_4807;

								BgL_new1264z00_4807 =
									((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_appz00_bgl))));
								{	/* Stackable/walk.scm 38 */
									long BgL_arg1734z00_4808;

									BgL_arg1734z00_4808 = BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1264z00_4807),
										BgL_arg1734z00_4808);
								}
								{	/* Stackable/walk.scm 38 */
									BgL_objectz00_bglt BgL_tmpz00_5884;

									BgL_tmpz00_5884 = ((BgL_objectz00_bglt) BgL_new1264z00_4807);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5884, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1264z00_4807);
								BgL_auxz00_5879 = BgL_new1264z00_4807;
							}
							BgL_tmp1265z00_4805 = ((BgL_appz00_bglt) BgL_auxz00_5879);
						}
						BgL_wide1266z00_4806 =
							((BgL_appzf2depthzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_appzf2depthzf2_bgl))));
						{	/* Stackable/walk.scm 38 */
							obj_t BgL_auxz00_5892;
							BgL_objectz00_bglt BgL_tmpz00_5890;

							BgL_auxz00_5892 = ((obj_t) BgL_wide1266z00_4806);
							BgL_tmpz00_5890 = ((BgL_objectz00_bglt) BgL_tmp1265z00_4805);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5890, BgL_auxz00_5892);
						}
						((BgL_objectz00_bglt) BgL_tmp1265z00_4805);
						{	/* Stackable/walk.scm 38 */
							long BgL_arg1733z00_4809;

							BgL_arg1733z00_4809 =
								BGL_CLASS_NUM(BGl_appzf2depthzf2zzstackable_walkz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1265z00_4805),
								BgL_arg1733z00_4809);
						}
						BgL_new1267z00_4804 = ((BgL_appz00_bglt) BgL_tmp1265z00_4805);
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1267z00_4804)))->BgL_locz00) =
						((obj_t) BgL_loc1107z00_4548), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1267z00_4804)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1108z00_4549)),
						BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1267z00_4804)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1109zd2_4550), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1267z00_4804)))->BgL_keyz00) =
						((obj_t) BgL_key1110z00_4551), BUNSPEC);
					((((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt)
										BgL_new1267z00_4804)))->BgL_funz00) =
						((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_fun1111z00_4552)),
						BUNSPEC);
					((((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt)
										BgL_new1267z00_4804)))->BgL_argsz00) =
						((obj_t) BgL_args1112z00_4553), BUNSPEC);
					((((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt)
										BgL_new1267z00_4804)))->BgL_stackablez00) =
						((obj_t) BgL_stackable1113z00_4554), BUNSPEC);
					{
						BgL_appzf2depthzf2_bglt BgL_auxz00_5916;

						{
							obj_t BgL_auxz00_5917;

							{	/* Stackable/walk.scm 38 */
								BgL_objectz00_bglt BgL_tmpz00_5918;

								BgL_tmpz00_5918 = ((BgL_objectz00_bglt) BgL_new1267z00_4804);
								BgL_auxz00_5917 = BGL_OBJECT_WIDENING(BgL_tmpz00_5918);
							}
							BgL_auxz00_5916 = ((BgL_appzf2depthzf2_bglt) BgL_auxz00_5917);
						}
						((((BgL_appzf2depthzf2_bglt) COBJECT(BgL_auxz00_5916))->
								BgL_depthz00) = ((long) BgL_depth1114z00_4803), BUNSPEC);
					}
					return BgL_new1267z00_4804;
				}
			}
		}

	}



/* &lambda1751 */
	obj_t BGl_z62lambda1751z62zzstackable_walkz00(obj_t BgL_envz00_4556,
		obj_t BgL_oz00_4557, obj_t BgL_vz00_4558)
	{
		{	/* Stackable/walk.scm 38 */
			{	/* Stackable/walk.scm 38 */
				long BgL_vz00_4811;

				BgL_vz00_4811 = (long) CINT(BgL_vz00_4558);
				{
					BgL_appzf2depthzf2_bglt BgL_auxz00_5924;

					{
						obj_t BgL_auxz00_5925;

						{	/* Stackable/walk.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_5926;

							BgL_tmpz00_5926 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_oz00_4557));
							BgL_auxz00_5925 = BGL_OBJECT_WIDENING(BgL_tmpz00_5926);
						}
						BgL_auxz00_5924 = ((BgL_appzf2depthzf2_bglt) BgL_auxz00_5925);
					}
					return
						((((BgL_appzf2depthzf2_bglt) COBJECT(BgL_auxz00_5924))->
							BgL_depthz00) = ((long) BgL_vz00_4811), BUNSPEC);
		}}}

	}



/* &lambda1750 */
	obj_t BGl_z62lambda1750z62zzstackable_walkz00(obj_t BgL_envz00_4559,
		obj_t BgL_oz00_4560)
	{
		{	/* Stackable/walk.scm 38 */
			{	/* Stackable/walk.scm 38 */
				long BgL_tmpz00_5932;

				{
					BgL_appzf2depthzf2_bglt BgL_auxz00_5933;

					{
						obj_t BgL_auxz00_5934;

						{	/* Stackable/walk.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_5935;

							BgL_tmpz00_5935 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_oz00_4560));
							BgL_auxz00_5934 = BGL_OBJECT_WIDENING(BgL_tmpz00_5935);
						}
						BgL_auxz00_5933 = ((BgL_appzf2depthzf2_bglt) BgL_auxz00_5934);
					}
					BgL_tmpz00_5932 =
						(((BgL_appzf2depthzf2_bglt) COBJECT(BgL_auxz00_5933))->
						BgL_depthz00);
				}
				return BINT(BgL_tmpz00_5932);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzstackable_walkz00(void)
	{
		{	/* Stackable/walk.scm 16 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_stackablezd2envzd2zzstackable_walkz00,
				BGl_proc2322z00zzstackable_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2323z00zzstackable_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_escapez12zd2envzc0zzstackable_walkz00,
				BGl_proc2324z00zzstackable_walkz00, BFALSE,
				BGl_string2325z00zzstackable_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2escapezd2envz00zzstackable_walkz00,
				BGl_proc2326z00zzstackable_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2327z00zzstackable_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_liftzd2letz12zd2envz12zzstackable_walkz00,
				BGl_proc2328z00zzstackable_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2329z00zzstackable_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_depthzd2letzd2envz00zzstackable_walkz00,
				BGl_proc2330z00zzstackable_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2331z00zzstackable_walkz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_initzd2stackablezd2envz00zzstackable_walkz00,
				BGl_proc2332z00zzstackable_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2333z00zzstackable_walkz00);
		}

	}



/* &init-stackable1521 */
	obj_t BGl_z62initzd2stackable1521zb0zzstackable_walkz00(obj_t BgL_envz00_4583,
		obj_t BgL_nodez00_4584)
	{
		{	/* Stackable/walk.scm 535 */
			return
				BGl_walk0z00zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_4584),
				BGl_initzd2stackablezd2envz00zzstackable_walkz00);
		}

	}



/* &depth-let1509 */
	obj_t BGl_z62depthzd2let1509zb0zzstackable_walkz00(obj_t BgL_envz00_4585,
		obj_t BgL_nodez00_4586, obj_t BgL_depthz00_4587, obj_t BgL_funz00_4588)
	{
		{	/* Stackable/walk.scm 455 */
			{

				return
					BGl_walk2z00zzast_walkz00(
					((BgL_nodez00_bglt) BgL_nodez00_4586),
					BGl_depthzd2letzd2envz00zzstackable_walkz00, BgL_depthz00_4587,
					BgL_funz00_4588);
			}
		}

	}



/* &lift-let!1505 */
	obj_t BGl_z62liftzd2letz121505za2zzstackable_walkz00(obj_t BgL_envz00_4589,
		obj_t BgL_nodez00_4590)
	{
		{	/* Stackable/walk.scm 387 */
			return
				((obj_t)
				BGl_walk0z12z12zzast_walkz00(
					((BgL_nodez00_bglt) BgL_nodez00_4590),
					BGl_liftzd2letz12zd2envz12zzstackable_walkz00));
		}

	}



/* &node-escape1499 */
	obj_t BGl_z62nodezd2escape1499zb0zzstackable_walkz00(obj_t BgL_envz00_4591,
		obj_t BgL_nodez00_4592, obj_t BgL_ctxz00_4593)
	{
		{	/* Stackable/walk.scm 355 */
			{

				return
					BGl_walk1z00zzast_walkz00(
					((BgL_nodez00_bglt) BgL_nodez00_4592),
					BGl_nodezd2escapezd2envz00zzstackable_walkz00, BgL_ctxz00_4593);
			}
		}

	}



/* &escape!1485 */
	obj_t BGl_z62escapez121485z70zzstackable_walkz00(obj_t BgL_envz00_4594,
		obj_t BgL_nodez00_4595, obj_t BgL_ctxz00_4596)
	{
		{	/* Stackable/walk.scm 296 */
			{	/* Stackable/walk.scm 297 */
				obj_t BgL_arg1836z00_4820;
				obj_t BgL_arg1837z00_4821;

				{	/* Stackable/walk.scm 297 */
					obj_t BgL_tmpz00_5957;

					BgL_tmpz00_5957 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1836z00_4820 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_5957);
				}
				BgL_arg1837z00_4821 = BGl_shapez00zztools_shapez00(BgL_nodez00_4595);
				{	/* Stackable/walk.scm 297 */
					obj_t BgL_list1838z00_4822;

					{	/* Stackable/walk.scm 297 */
						obj_t BgL_arg1839z00_4823;

						{	/* Stackable/walk.scm 297 */
							obj_t BgL_arg1840z00_4824;

							{	/* Stackable/walk.scm 297 */
								obj_t BgL_arg1842z00_4825;

								{	/* Stackable/walk.scm 297 */
									obj_t BgL_arg1843z00_4826;

									{	/* Stackable/walk.scm 297 */
										obj_t BgL_arg1844z00_4827;

										BgL_arg1844z00_4827 =
											MAKE_YOUNG_PAIR(BgL_arg1837z00_4821, BNIL);
										BgL_arg1843z00_4826 =
											MAKE_YOUNG_PAIR(BGl_string2334z00zzstackable_walkz00,
											BgL_arg1844z00_4827);
									}
									BgL_arg1842z00_4825 =
										MAKE_YOUNG_PAIR(BGl_string2335z00zzstackable_walkz00,
										BgL_arg1843z00_4826);
								}
								BgL_arg1840z00_4824 =
									MAKE_YOUNG_PAIR(BINT(297L), BgL_arg1842z00_4825);
							}
							BgL_arg1839z00_4823 =
								MAKE_YOUNG_PAIR(BGl_string2336z00zzstackable_walkz00,
								BgL_arg1840z00_4824);
						}
						BgL_list1838z00_4822 =
							MAKE_YOUNG_PAIR(BGl_string2337z00zzstackable_walkz00,
							BgL_arg1839z00_4823);
					}
					BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg1836z00_4820,
						BgL_list1838z00_4822);
				}
			}
			return BUNSPEC;
		}

	}



/* &stackable1465 */
	obj_t BGl_z62stackable1465z62zzstackable_walkz00(obj_t BgL_envz00_4597,
		obj_t BgL_nodez00_4598, obj_t BgL_escpz00_4599, obj_t BgL_depthz00_4600,
		obj_t BgL_ctxz00_4601)
	{
		{	/* Stackable/walk.scm 113 */
			{	/* Stackable/walk.scm 113 */
				bool_t BgL_escpz00_4829;
				long BgL_depthz00_4830;

				BgL_escpz00_4829 = CBOOL(BgL_escpz00_4599);
				BgL_depthz00_4830 = (long) CINT(BgL_depthz00_4600);
				{

					BgL_escpz00_4829 = ((bool_t) 1);
					return
						BGl_walk3z00zzast_walkz00(
						((BgL_nodez00_bglt) BgL_nodez00_4598),
						BGl_stackablezd2envzd2zzstackable_walkz00, BBOOL(BgL_escpz00_4829),
						BINT(BgL_depthz00_4830), ((obj_t) BgL_ctxz00_4601));
				}
			}
		}

	}



/* stackable */
	obj_t BGl_stackablez00zzstackable_walkz00(BgL_nodez00_bglt BgL_nodez00_23,
		bool_t BgL_escpz00_24, long BgL_depthz00_25, obj_t BgL_ctxz00_26)
	{
		{	/* Stackable/walk.scm 113 */
			{	/* Stackable/walk.scm 113 */
				obj_t BgL_method1466z00_2049;

				{	/* Stackable/walk.scm 113 */
					obj_t BgL_res2248z00_3405;

					{	/* Stackable/walk.scm 113 */
						long BgL_objzd2classzd2numz00_3376;

						BgL_objzd2classzd2numz00_3376 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_23));
						{	/* Stackable/walk.scm 113 */
							obj_t BgL_arg1811z00_3377;

							BgL_arg1811z00_3377 =
								PROCEDURE_REF(BGl_stackablezd2envzd2zzstackable_walkz00,
								(int) (1L));
							{	/* Stackable/walk.scm 113 */
								int BgL_offsetz00_3380;

								BgL_offsetz00_3380 = (int) (BgL_objzd2classzd2numz00_3376);
								{	/* Stackable/walk.scm 113 */
									long BgL_offsetz00_3381;

									BgL_offsetz00_3381 =
										((long) (BgL_offsetz00_3380) - OBJECT_TYPE);
									{	/* Stackable/walk.scm 113 */
										long BgL_modz00_3382;

										BgL_modz00_3382 =
											(BgL_offsetz00_3381 >> (int) ((long) ((int) (4L))));
										{	/* Stackable/walk.scm 113 */
											long BgL_restz00_3384;

											BgL_restz00_3384 =
												(BgL_offsetz00_3381 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Stackable/walk.scm 113 */

												{	/* Stackable/walk.scm 113 */
													obj_t BgL_bucketz00_3386;

													BgL_bucketz00_3386 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3377), BgL_modz00_3382);
													BgL_res2248z00_3405 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3386), BgL_restz00_3384);
					}}}}}}}}
					BgL_method1466z00_2049 = BgL_res2248z00_3405;
				}
				return
					BGL_PROCEDURE_CALL4(BgL_method1466z00_2049,
					((obj_t) BgL_nodez00_23),
					BBOOL(BgL_escpz00_24), BINT(BgL_depthz00_25), BgL_ctxz00_26);
			}
		}

	}



/* &stackable */
	obj_t BGl_z62stackablez62zzstackable_walkz00(obj_t BgL_envz00_4578,
		obj_t BgL_nodez00_4579, obj_t BgL_escpz00_4580, obj_t BgL_depthz00_4581,
		obj_t BgL_ctxz00_4582)
	{
		{	/* Stackable/walk.scm 113 */
			return
				BGl_stackablez00zzstackable_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_4579),
				CBOOL(BgL_escpz00_4580),
				(long) CINT(BgL_depthz00_4581), BgL_ctxz00_4582);
		}

	}



/* escape! */
	obj_t BGl_escapez12z12zzstackable_walkz00(obj_t BgL_nodez00_63,
		obj_t BgL_ctxz00_64)
	{
		{	/* Stackable/walk.scm 296 */
			if (BGL_OBJECTP(BgL_nodez00_63))
				{	/* Stackable/walk.scm 296 */
					obj_t BgL_method1486z00_2051;

					{	/* Stackable/walk.scm 296 */
						obj_t BgL_res2253z00_3436;

						{	/* Stackable/walk.scm 296 */
							long BgL_objzd2classzd2numz00_3407;

							BgL_objzd2classzd2numz00_3407 =
								BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_63));
							{	/* Stackable/walk.scm 296 */
								obj_t BgL_arg1811z00_3408;

								BgL_arg1811z00_3408 =
									PROCEDURE_REF(BGl_escapez12zd2envzc0zzstackable_walkz00,
									(int) (1L));
								{	/* Stackable/walk.scm 296 */
									int BgL_offsetz00_3411;

									BgL_offsetz00_3411 = (int) (BgL_objzd2classzd2numz00_3407);
									{	/* Stackable/walk.scm 296 */
										long BgL_offsetz00_3412;

										BgL_offsetz00_3412 =
											((long) (BgL_offsetz00_3411) - OBJECT_TYPE);
										{	/* Stackable/walk.scm 296 */
											long BgL_modz00_3413;

											BgL_modz00_3413 =
												(BgL_offsetz00_3412 >> (int) ((long) ((int) (4L))));
											{	/* Stackable/walk.scm 296 */
												long BgL_restz00_3415;

												BgL_restz00_3415 =
													(BgL_offsetz00_3412 &
													(long) (
														(int) (
															((long) (
																	(int) (
																		(1L <<
																			(int) ((long) ((int) (4L)))))) - 1L))));
												{	/* Stackable/walk.scm 296 */

													{	/* Stackable/walk.scm 296 */
														obj_t BgL_bucketz00_3417;

														BgL_bucketz00_3417 =
															VECTOR_REF(
															((obj_t) BgL_arg1811z00_3408), BgL_modz00_3413);
														BgL_res2253z00_3436 =
															VECTOR_REF(
															((obj_t) BgL_bucketz00_3417), BgL_restz00_3415);
						}}}}}}}}
						BgL_method1486z00_2051 = BgL_res2253z00_3436;
					}
					return
						BGL_PROCEDURE_CALL2(BgL_method1486z00_2051, BgL_nodez00_63,
						BgL_ctxz00_64);
				}
			else
				{	/* Stackable/walk.scm 296 */
					obj_t BgL_fun1862z00_2052;

					BgL_fun1862z00_2052 =
						PROCEDURE_REF(BGl_escapez12zd2envzc0zzstackable_walkz00,
						(int) (0L));
					return
						BGL_PROCEDURE_CALL2(BgL_fun1862z00_2052, BgL_nodez00_63,
						BgL_ctxz00_64);
				}
		}

	}



/* &escape! */
	obj_t BGl_z62escapez12z70zzstackable_walkz00(obj_t BgL_envz00_4602,
		obj_t BgL_nodez00_4603, obj_t BgL_ctxz00_4604)
	{
		{	/* Stackable/walk.scm 296 */
			return
				BGl_escapez12z12zzstackable_walkz00(BgL_nodez00_4603, BgL_ctxz00_4604);
		}

	}



/* node-escape */
	obj_t BGl_nodezd2escapezd2zzstackable_walkz00(BgL_nodez00_bglt BgL_nodez00_77,
		obj_t BgL_ctxz00_78)
	{
		{	/* Stackable/walk.scm 355 */
			{	/* Stackable/walk.scm 355 */
				obj_t BgL_method1500z00_2053;

				{	/* Stackable/walk.scm 355 */
					obj_t BgL_res2259z00_3468;

					{	/* Stackable/walk.scm 355 */
						long BgL_objzd2classzd2numz00_3439;

						BgL_objzd2classzd2numz00_3439 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_77));
						{	/* Stackable/walk.scm 355 */
							obj_t BgL_arg1811z00_3440;

							BgL_arg1811z00_3440 =
								PROCEDURE_REF(BGl_nodezd2escapezd2envz00zzstackable_walkz00,
								(int) (1L));
							{	/* Stackable/walk.scm 355 */
								int BgL_offsetz00_3443;

								BgL_offsetz00_3443 = (int) (BgL_objzd2classzd2numz00_3439);
								{	/* Stackable/walk.scm 355 */
									long BgL_offsetz00_3444;

									BgL_offsetz00_3444 =
										((long) (BgL_offsetz00_3443) - OBJECT_TYPE);
									{	/* Stackable/walk.scm 355 */
										long BgL_modz00_3445;

										BgL_modz00_3445 =
											(BgL_offsetz00_3444 >> (int) ((long) ((int) (4L))));
										{	/* Stackable/walk.scm 355 */
											long BgL_restz00_3447;

											BgL_restz00_3447 =
												(BgL_offsetz00_3444 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Stackable/walk.scm 355 */

												{	/* Stackable/walk.scm 355 */
													obj_t BgL_bucketz00_3449;

													BgL_bucketz00_3449 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3440), BgL_modz00_3445);
													BgL_res2259z00_3468 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3449), BgL_restz00_3447);
					}}}}}}}}
					BgL_method1500z00_2053 = BgL_res2259z00_3468;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1500z00_2053,
					((obj_t) BgL_nodez00_77), BgL_ctxz00_78);
			}
		}

	}



/* &node-escape */
	obj_t BGl_z62nodezd2escapezb0zzstackable_walkz00(obj_t BgL_envz00_4573,
		obj_t BgL_nodez00_4574, obj_t BgL_ctxz00_4575)
	{
		{	/* Stackable/walk.scm 355 */
			return
				BGl_nodezd2escapezd2zzstackable_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_4574), BgL_ctxz00_4575);
		}

	}



/* lift-let! */
	BgL_nodez00_bglt BGl_liftzd2letz12zc0zzstackable_walkz00(BgL_nodez00_bglt
		BgL_nodez00_84)
	{
		{	/* Stackable/walk.scm 387 */
			{	/* Stackable/walk.scm 387 */
				obj_t BgL_method1506z00_2054;

				{	/* Stackable/walk.scm 387 */
					obj_t BgL_res2264z00_3499;

					{	/* Stackable/walk.scm 387 */
						long BgL_objzd2classzd2numz00_3470;

						BgL_objzd2classzd2numz00_3470 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_84));
						{	/* Stackable/walk.scm 387 */
							obj_t BgL_arg1811z00_3471;

							BgL_arg1811z00_3471 =
								PROCEDURE_REF(BGl_liftzd2letz12zd2envz12zzstackable_walkz00,
								(int) (1L));
							{	/* Stackable/walk.scm 387 */
								int BgL_offsetz00_3474;

								BgL_offsetz00_3474 = (int) (BgL_objzd2classzd2numz00_3470);
								{	/* Stackable/walk.scm 387 */
									long BgL_offsetz00_3475;

									BgL_offsetz00_3475 =
										((long) (BgL_offsetz00_3474) - OBJECT_TYPE);
									{	/* Stackable/walk.scm 387 */
										long BgL_modz00_3476;

										BgL_modz00_3476 =
											(BgL_offsetz00_3475 >> (int) ((long) ((int) (4L))));
										{	/* Stackable/walk.scm 387 */
											long BgL_restz00_3478;

											BgL_restz00_3478 =
												(BgL_offsetz00_3475 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Stackable/walk.scm 387 */

												{	/* Stackable/walk.scm 387 */
													obj_t BgL_bucketz00_3480;

													BgL_bucketz00_3480 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3471), BgL_modz00_3476);
													BgL_res2264z00_3499 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3480), BgL_restz00_3478);
					}}}}}}}}
					BgL_method1506z00_2054 = BgL_res2264z00_3499;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1506z00_2054,
						((obj_t) BgL_nodez00_84)));
			}
		}

	}



/* &lift-let! */
	BgL_nodez00_bglt BGl_z62liftzd2letz12za2zzstackable_walkz00(obj_t
		BgL_envz00_4570, obj_t BgL_nodez00_4571)
	{
		{	/* Stackable/walk.scm 387 */
			return
				BGl_liftzd2letz12zc0zzstackable_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_4571));
		}

	}



/* depth-let */
	obj_t BGl_depthzd2letzd2zzstackable_walkz00(BgL_nodez00_bglt BgL_nodez00_87,
		obj_t BgL_depthz00_88, obj_t BgL_funz00_89)
	{
		{	/* Stackable/walk.scm 455 */
			{	/* Stackable/walk.scm 455 */
				obj_t BgL_method1510z00_2055;

				{	/* Stackable/walk.scm 455 */
					obj_t BgL_res2269z00_3530;

					{	/* Stackable/walk.scm 455 */
						long BgL_objzd2classzd2numz00_3501;

						BgL_objzd2classzd2numz00_3501 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_87));
						{	/* Stackable/walk.scm 455 */
							obj_t BgL_arg1811z00_3502;

							BgL_arg1811z00_3502 =
								PROCEDURE_REF(BGl_depthzd2letzd2envz00zzstackable_walkz00,
								(int) (1L));
							{	/* Stackable/walk.scm 455 */
								int BgL_offsetz00_3505;

								BgL_offsetz00_3505 = (int) (BgL_objzd2classzd2numz00_3501);
								{	/* Stackable/walk.scm 455 */
									long BgL_offsetz00_3506;

									BgL_offsetz00_3506 =
										((long) (BgL_offsetz00_3505) - OBJECT_TYPE);
									{	/* Stackable/walk.scm 455 */
										long BgL_modz00_3507;

										BgL_modz00_3507 =
											(BgL_offsetz00_3506 >> (int) ((long) ((int) (4L))));
										{	/* Stackable/walk.scm 455 */
											long BgL_restz00_3509;

											BgL_restz00_3509 =
												(BgL_offsetz00_3506 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Stackable/walk.scm 455 */

												{	/* Stackable/walk.scm 455 */
													obj_t BgL_bucketz00_3511;

													BgL_bucketz00_3511 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3502), BgL_modz00_3507);
													BgL_res2269z00_3530 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3511), BgL_restz00_3509);
					}}}}}}}}
					BgL_method1510z00_2055 = BgL_res2269z00_3530;
				}
				return
					BGL_PROCEDURE_CALL3(BgL_method1510z00_2055,
					((obj_t) BgL_nodez00_87), BgL_depthz00_88, BgL_funz00_89);
			}
		}

	}



/* &depth-let */
	obj_t BGl_z62depthzd2letzb0zzstackable_walkz00(obj_t BgL_envz00_4565,
		obj_t BgL_nodez00_4566, obj_t BgL_depthz00_4567, obj_t BgL_funz00_4568)
	{
		{	/* Stackable/walk.scm 455 */
			return
				BGl_depthzd2letzd2zzstackable_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_4566), BgL_depthz00_4567,
				BgL_funz00_4568);
		}

	}



/* init-stackable */
	obj_t BGl_initzd2stackablezd2zzstackable_walkz00(BgL_nodez00_bglt
		BgL_nodez00_106)
	{
		{	/* Stackable/walk.scm 535 */
			{	/* Stackable/walk.scm 535 */
				obj_t BgL_method1522z00_2056;

				{	/* Stackable/walk.scm 535 */
					obj_t BgL_res2274z00_3561;

					{	/* Stackable/walk.scm 535 */
						long BgL_objzd2classzd2numz00_3532;

						BgL_objzd2classzd2numz00_3532 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_106));
						{	/* Stackable/walk.scm 535 */
							obj_t BgL_arg1811z00_3533;

							BgL_arg1811z00_3533 =
								PROCEDURE_REF(BGl_initzd2stackablezd2envz00zzstackable_walkz00,
								(int) (1L));
							{	/* Stackable/walk.scm 535 */
								int BgL_offsetz00_3536;

								BgL_offsetz00_3536 = (int) (BgL_objzd2classzd2numz00_3532);
								{	/* Stackable/walk.scm 535 */
									long BgL_offsetz00_3537;

									BgL_offsetz00_3537 =
										((long) (BgL_offsetz00_3536) - OBJECT_TYPE);
									{	/* Stackable/walk.scm 535 */
										long BgL_modz00_3538;

										BgL_modz00_3538 =
											(BgL_offsetz00_3537 >> (int) ((long) ((int) (4L))));
										{	/* Stackable/walk.scm 535 */
											long BgL_restz00_3540;

											BgL_restz00_3540 =
												(BgL_offsetz00_3537 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Stackable/walk.scm 535 */

												{	/* Stackable/walk.scm 535 */
													obj_t BgL_bucketz00_3542;

													BgL_bucketz00_3542 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3533), BgL_modz00_3538);
													BgL_res2274z00_3561 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3542), BgL_restz00_3540);
					}}}}}}}}
					BgL_method1522z00_2056 = BgL_res2274z00_3561;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1522z00_2056,
					((obj_t) BgL_nodez00_106));
			}
		}

	}



/* &init-stackable */
	obj_t BGl_z62initzd2stackablezb0zzstackable_walkz00(obj_t BgL_envz00_4562,
		obj_t BgL_nodez00_4563)
	{
		{	/* Stackable/walk.scm 535 */
			return
				BGl_initzd2stackablezd2zzstackable_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_4563));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzstackable_walkz00(void)
	{
		{	/* Stackable/walk.scm 16 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00,
				BGl_localzf2depthzf2zzstackable_walkz00,
				BGl_proc2338z00zzstackable_walkz00,
				BGl_string2339z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00,
				BGl_appzf2depthzf2zzstackable_walkz00,
				BGl_proc2340z00zzstackable_walkz00,
				BGl_string2341z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_stackablezd2envzd2zzstackable_walkz00, BGl_varz00zzast_nodez00,
				BGl_proc2342z00zzstackable_walkz00,
				BGl_string2343z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_stackablezd2envzd2zzstackable_walkz00, BGl_closurez00zzast_nodez00,
				BGl_proc2344z00zzstackable_walkz00,
				BGl_string2343z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_stackablezd2envzd2zzstackable_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc2345z00zzstackable_walkz00,
				BGl_string2343z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_stackablezd2envzd2zzstackable_walkz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2346z00zzstackable_walkz00,
				BGl_string2343z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_stackablezd2envzd2zzstackable_walkz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2347z00zzstackable_walkz00,
				BGl_string2343z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_stackablezd2envzd2zzstackable_walkz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2348z00zzstackable_walkz00,
				BGl_string2343z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_stackablezd2envzd2zzstackable_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc2349z00zzstackable_walkz00,
				BGl_string2343z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_stackablezd2envzd2zzstackable_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc2350z00zzstackable_walkz00,
				BGl_string2343z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_stackablezd2envzd2zzstackable_walkz00,
				BGl_sequencez00zzast_nodez00, BGl_proc2351z00zzstackable_walkz00,
				BGl_string2343z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzstackable_walkz00, BGl_globalz00zzast_varz00,
				BGl_proc2352z00zzstackable_walkz00,
				BGl_string2353z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzstackable_walkz00, BGl_localz00zzast_varz00,
				BGl_proc2354z00zzstackable_walkz00,
				BGl_string2353z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzstackable_walkz00, BGl_nodez00zzast_nodez00,
				BGl_proc2355z00zzstackable_walkz00,
				BGl_string2353z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzstackable_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc2356z00zzstackable_walkz00,
				BGl_string2353z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzstackable_walkz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2357z00zzstackable_walkz00,
				BGl_string2353z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzstackable_walkz00, BGl_closurez00zzast_nodez00,
				BGl_proc2358z00zzstackable_walkz00,
				BGl_string2359z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2escapezd2envz00zzstackable_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc2360z00zzstackable_walkz00,
				BGl_string2361z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2escapezd2envz00zzstackable_walkz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2362z00zzstackable_walkz00,
				BGl_string2361z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_liftzd2letz12zd2envz12zzstackable_walkz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2363z00zzstackable_walkz00,
				BGl_string2364z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_depthzd2letzd2envz00zzstackable_walkz00, BGl_varz00zzast_nodez00,
				BGl_proc2365z00zzstackable_walkz00,
				BGl_string2366z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_depthzd2letzd2envz00zzstackable_walkz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2367z00zzstackable_walkz00,
				BGl_string2366z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_depthzd2letzd2envz00zzstackable_walkz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2368z00zzstackable_walkz00,
				BGl_string2366z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_depthzd2letzd2envz00zzstackable_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc2369z00zzstackable_walkz00,
				BGl_string2366z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_depthzd2letzd2envz00zzstackable_walkz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2370z00zzstackable_walkz00,
				BGl_string2366z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initzd2stackablezd2envz00zzstackable_walkz00,
				BGl_appz00zzast_nodez00, BGl_proc2371z00zzstackable_walkz00,
				BGl_string2372z00zzstackable_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initzd2stackablezd2envz00zzstackable_walkz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2373z00zzstackable_walkz00,
				BGl_string2372z00zzstackable_walkz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initzd2stackablezd2envz00zzstackable_walkz00,
				BGl_closurez00zzast_nodez00, BGl_proc2374z00zzstackable_walkz00,
				BGl_string2372z00zzstackable_walkz00);
		}

	}



/* &init-stackable-closu1528 */
	obj_t BGl_z62initzd2stackablezd2closu1528z62zzstackable_walkz00(obj_t
		BgL_envz00_4637, obj_t BgL_nodez00_4638)
	{
		{	/* Stackable/walk.scm 557 */
			BGl_walk0z00zzast_walkz00(
				((BgL_nodez00_bglt)
					((BgL_closurez00_bglt) BgL_nodez00_4638)),
				BGl_initzd2stackablezd2envz00zzstackable_walkz00);
			{	/* Stackable/walk.scm 559 */
				BgL_variablez00_bglt BgL_vz00_4834;

				BgL_vz00_4834 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt)
								((BgL_closurez00_bglt) BgL_nodez00_4638))))->BgL_variablez00);
				{	/* Stackable/walk.scm 559 */
					BgL_valuez00_bglt BgL_fz00_4835;

					BgL_fz00_4835 =
						(((BgL_variablez00_bglt) COBJECT(BgL_vz00_4834))->BgL_valuez00);
					{	/* Stackable/walk.scm 560 */

						return
							((((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt) BgL_fz00_4835)))->BgL_stackablez00) =
							((obj_t) BTRUE), BUNSPEC);
					}
				}
			}
		}

	}



/* &init-stackable-make-1526 */
	obj_t BGl_z62initzd2stackablezd2makezd21526zb0zzstackable_walkz00(obj_t
		BgL_envz00_4639, obj_t BgL_nodez00_4640)
	{
		{	/* Stackable/walk.scm 549 */
			BGl_walk0z00zzast_walkz00(
				((BgL_nodez00_bglt)
					((BgL_makezd2boxzd2_bglt) BgL_nodez00_4640)),
				BGl_initzd2stackablezd2envz00zzstackable_walkz00);
			return ((((BgL_makezd2boxzd2_bglt) COBJECT(((BgL_makezd2boxzd2_bglt)
								BgL_nodez00_4640)))->BgL_stackablez00) =
				((obj_t) BTRUE), BUNSPEC);
		}

	}



/* &init-stackable-app1524 */
	obj_t BGl_z62initzd2stackablezd2app1524z62zzstackable_walkz00(obj_t
		BgL_envz00_4641, obj_t BgL_nodez00_4642)
	{
		{	/* Stackable/walk.scm 541 */
			BGl_walk0z00zzast_walkz00(
				((BgL_nodez00_bglt)
					((BgL_appz00_bglt) BgL_nodez00_4642)),
				BGl_initzd2stackablezd2envz00zzstackable_walkz00);
			return ((((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt)
								BgL_nodez00_4642)))->BgL_stackablez00) =
				((obj_t) BTRUE), BUNSPEC);
		}

	}



/* &depth-let-set-ex-it1520 */
	obj_t BGl_z62depthzd2letzd2setzd2exzd2it1520z62zzstackable_walkz00(obj_t
		BgL_envz00_4643, obj_t BgL_nodez00_4644, obj_t BgL_depthz00_4645,
		obj_t BgL_funz00_4646)
	{
		{	/* Stackable/walk.scm 513 */
			{

				BGl_walk2z00zzast_walkz00(
					((BgL_nodez00_bglt)
						((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4644)),
					BGl_depthzd2letzd2envz00zzstackable_walkz00, BgL_depthz00_4645,
					BgL_funz00_4646);
				{	/* Stackable/walk.scm 516 */
					BgL_varz00_bglt BgL_i1253z00_4840;

					BgL_i1253z00_4840 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4644)))->BgL_varz00);
					{	/* Stackable/walk.scm 517 */
						BgL_localz00_bglt BgL_i1254z00_4841;

						BgL_i1254z00_4841 =
							((BgL_localz00_bglt)
							(((BgL_varz00_bglt) COBJECT(BgL_i1253z00_4840))->
								BgL_variablez00));
						((((BgL_localz00_bglt) COBJECT(BgL_i1254z00_4841))->
								BgL_valzd2noescapezd2) = ((obj_t) BFALSE), BUNSPEC);
					}
					{	/* Stackable/walk.scm 519 */
						BgL_localz00_bglt BgL_tmp1255z00_4842;

						BgL_tmp1255z00_4842 =
							((BgL_localz00_bglt)
							(((BgL_varz00_bglt) COBJECT(BgL_i1253z00_4840))->
								BgL_variablez00));
						{	/* Stackable/walk.scm 519 */
							BgL_localzf2depthzf2_bglt BgL_wide1257z00_4843;

							BgL_wide1257z00_4843 =
								((BgL_localzf2depthzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_localzf2depthzf2_bgl))));
							{	/* Stackable/walk.scm 519 */
								obj_t BgL_auxz00_6248;
								BgL_objectz00_bglt BgL_tmpz00_6245;

								BgL_auxz00_6248 = ((obj_t) BgL_wide1257z00_4843);
								BgL_tmpz00_6245 =
									((BgL_objectz00_bglt)
									((BgL_localz00_bglt) BgL_tmp1255z00_4842));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6245, BgL_auxz00_6248);
							}
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_tmp1255z00_4842));
							{	/* Stackable/walk.scm 519 */
								long BgL_arg2162z00_4844;

								BgL_arg2162z00_4844 =
									BGL_CLASS_NUM(BGl_localzf2depthzf2zzstackable_walkz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_tmp1255z00_4842)),
									BgL_arg2162z00_4844);
							}
							((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_tmp1255z00_4842));
						}
						{
							BgL_localzf2depthzf2_bglt BgL_auxz00_6259;

							{
								obj_t BgL_auxz00_6260;

								{	/* Stackable/walk.scm 521 */
									BgL_objectz00_bglt BgL_tmpz00_6261;

									BgL_tmpz00_6261 =
										((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_tmp1255z00_4842));
									BgL_auxz00_6260 = BGL_OBJECT_WIDENING(BgL_tmpz00_6261);
								}
								BgL_auxz00_6259 = ((BgL_localzf2depthzf2_bglt) BgL_auxz00_6260);
							}
							((((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_6259))->
									BgL_depthz00) =
								((long) ((long) CINT(BgL_depthz00_4645) + 1L)), BUNSPEC);
						}
						{
							BgL_localzf2depthzf2_bglt BgL_auxz00_6269;

							{
								obj_t BgL_auxz00_6270;

								{	/* Stackable/walk.scm 520 */
									BgL_objectz00_bglt BgL_tmpz00_6271;

									BgL_tmpz00_6271 =
										((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_tmp1255z00_4842));
									BgL_auxz00_6270 = BGL_OBJECT_WIDENING(BgL_tmpz00_6271);
								}
								BgL_auxz00_6269 = ((BgL_localzf2depthzf2_bglt) BgL_auxz00_6270);
							}
							((((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_6269))->
									BgL_ownerz00) = ((obj_t) BgL_funz00_4646), BUNSPEC);
						}
						((BgL_localz00_bglt) BgL_tmp1255z00_4842);
				}}
				{	/* Stackable/walk.scm 522 */
					BgL_nodez00_bglt BgL_arg2163z00_4845;
					long BgL_arg2164z00_4846;

					BgL_arg2163z00_4845 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4644)))->BgL_bodyz00);
					BgL_arg2164z00_4846 = ((long) CINT(BgL_depthz00_4645) + 1L);
					return
						BGl_depthzd2letzd2zzstackable_walkz00(BgL_arg2163z00_4845,
						BINT(BgL_arg2164z00_4846), BgL_funz00_4646);
				}
			}
		}

	}



/* &depth-let-app1518 */
	obj_t BGl_z62depthzd2letzd2app1518z62zzstackable_walkz00(obj_t
		BgL_envz00_4647, obj_t BgL_nodez00_4648, obj_t BgL_depthz00_4649,
		obj_t BgL_funz00_4650)
	{
		{	/* Stackable/walk.scm 506 */
			{
				BgL_appz00_bglt BgL_auxz00_6284;

				{

					BGl_walk2z00zzast_walkz00(
						((BgL_nodez00_bglt)
							((BgL_appz00_bglt) BgL_nodez00_4648)),
						BGl_depthzd2letzd2envz00zzstackable_walkz00, BgL_depthz00_4649,
						BgL_funz00_4650);
					{	/* Stackable/walk.scm 508 */
						BgL_appzf2depthzf2_bglt BgL_wide1250z00_4849;

						BgL_wide1250z00_4849 =
							((BgL_appzf2depthzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_appzf2depthzf2_bgl))));
						{	/* Stackable/walk.scm 508 */
							obj_t BgL_auxz00_6293;
							BgL_objectz00_bglt BgL_tmpz00_6289;

							BgL_auxz00_6293 = ((obj_t) BgL_wide1250z00_4849);
							BgL_tmpz00_6289 =
								((BgL_objectz00_bglt)
								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4648)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6289, BgL_auxz00_6293);
						}
						((BgL_objectz00_bglt)
							((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4648)));
						{	/* Stackable/walk.scm 508 */
							long BgL_arg2158z00_4850;

							BgL_arg2158z00_4850 =
								BGL_CLASS_NUM(BGl_appzf2depthzf2zzstackable_walkz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_4648))),
								BgL_arg2158z00_4850);
						}
						((BgL_appz00_bglt)
							((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4648)));
					}
					{
						BgL_appzf2depthzf2_bglt BgL_auxz00_6307;

						{
							obj_t BgL_auxz00_6308;

							{	/* Stackable/walk.scm 508 */
								BgL_objectz00_bglt BgL_tmpz00_6309;

								BgL_tmpz00_6309 =
									((BgL_objectz00_bglt)
									((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4648)));
								BgL_auxz00_6308 = BGL_OBJECT_WIDENING(BgL_tmpz00_6309);
							}
							BgL_auxz00_6307 = ((BgL_appzf2depthzf2_bglt) BgL_auxz00_6308);
						}
						((((BgL_appzf2depthzf2_bglt) COBJECT(BgL_auxz00_6307))->
								BgL_depthz00) =
							((long) (long) CINT(BgL_depthz00_4649)), BUNSPEC);
					}
					BgL_auxz00_6284 =
						((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4648));
				}
				return ((obj_t) BgL_auxz00_6284);
			}
		}

	}



/* &depth-let-let-fun1516 */
	obj_t BGl_z62depthzd2letzd2letzd2fun1516zb0zzstackable_walkz00(obj_t
		BgL_envz00_4651, obj_t BgL_nodez00_4652, obj_t BgL_depthz00_4653,
		obj_t BgL_funz00_4654)
	{
		{	/* Stackable/walk.scm 485 */
			{	/* Stackable/walk.scm 486 */
				long BgL_depthz00_4852;

				BgL_depthz00_4852 = ((long) CINT(BgL_depthz00_4653) + 1L);
				{	/* Stackable/walk.scm 488 */
					obj_t BgL_g1456z00_4853;

					BgL_g1456z00_4853 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_4652)))->BgL_localsz00);
					{
						obj_t BgL_l1454z00_4855;

						BgL_l1454z00_4855 = BgL_g1456z00_4853;
					BgL_zc3z04anonymousza32143ze3z87_4854:
						if (PAIRP(BgL_l1454z00_4855))
							{	/* Stackable/walk.scm 488 */
								{	/* Stackable/walk.scm 489 */
									obj_t BgL_localz00_4856;

									BgL_localz00_4856 = CAR(BgL_l1454z00_4855);
									{	/* Stackable/walk.scm 490 */
										BgL_localzf2depthzf2_bglt BgL_wide1240z00_4857;

										BgL_wide1240z00_4857 =
											((BgL_localzf2depthzf2_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_localzf2depthzf2_bgl))));
										{	/* Stackable/walk.scm 490 */
											obj_t BgL_auxz00_6332;
											BgL_objectz00_bglt BgL_tmpz00_6328;

											BgL_auxz00_6332 = ((obj_t) BgL_wide1240z00_4857);
											BgL_tmpz00_6328 =
												((BgL_objectz00_bglt)
												((BgL_localz00_bglt)
													((BgL_localz00_bglt) BgL_localz00_4856)));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6328, BgL_auxz00_6332);
										}
										((BgL_objectz00_bglt)
											((BgL_localz00_bglt)
												((BgL_localz00_bglt) BgL_localz00_4856)));
										{	/* Stackable/walk.scm 490 */
											long BgL_arg2145z00_4858;

											BgL_arg2145z00_4858 =
												BGL_CLASS_NUM(BGl_localzf2depthzf2zzstackable_walkz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt)
													((BgL_localz00_bglt)
														((BgL_localz00_bglt) BgL_localz00_4856))),
												BgL_arg2145z00_4858);
										}
										((BgL_localz00_bglt)
											((BgL_localz00_bglt)
												((BgL_localz00_bglt) BgL_localz00_4856)));
									}
									{
										BgL_localzf2depthzf2_bglt BgL_auxz00_6346;

										{
											obj_t BgL_auxz00_6347;

											{	/* Stackable/walk.scm 492 */
												BgL_objectz00_bglt BgL_tmpz00_6348;

												BgL_tmpz00_6348 =
													((BgL_objectz00_bglt)
													((BgL_localz00_bglt)
														((BgL_localz00_bglt) BgL_localz00_4856)));
												BgL_auxz00_6347 = BGL_OBJECT_WIDENING(BgL_tmpz00_6348);
											}
											BgL_auxz00_6346 =
												((BgL_localzf2depthzf2_bglt) BgL_auxz00_6347);
										}
										((((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_6346))->
												BgL_depthz00) = ((long) BgL_depthz00_4852), BUNSPEC);
									}
									{
										BgL_localzf2depthzf2_bglt BgL_auxz00_6355;

										{
											obj_t BgL_auxz00_6356;

											{	/* Stackable/walk.scm 491 */
												BgL_objectz00_bglt BgL_tmpz00_6357;

												BgL_tmpz00_6357 =
													((BgL_objectz00_bglt)
													((BgL_localz00_bglt)
														((BgL_localz00_bglt) BgL_localz00_4856)));
												BgL_auxz00_6356 = BGL_OBJECT_WIDENING(BgL_tmpz00_6357);
											}
											BgL_auxz00_6355 =
												((BgL_localzf2depthzf2_bglt) BgL_auxz00_6356);
										}
										((((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_6355))->
												BgL_ownerz00) = ((obj_t) BgL_funz00_4654), BUNSPEC);
									}
									((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_localz00_4856));
									{	/* Stackable/walk.scm 493 */
										BgL_sfunz00_bglt BgL_i1243z00_4859;

										BgL_i1243z00_4859 =
											((BgL_sfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt) BgL_localz00_4856))))->
												BgL_valuez00));
										{	/* Stackable/walk.scm 494 */
											obj_t BgL_g1453z00_4860;

											BgL_g1453z00_4860 =
												(((BgL_sfunz00_bglt) COBJECT(BgL_i1243z00_4859))->
												BgL_argsz00);
											{
												obj_t BgL_l1451z00_4862;

												BgL_l1451z00_4862 = BgL_g1453z00_4860;
											BgL_zc3z04anonymousza32146ze3z87_4861:
												if (PAIRP(BgL_l1451z00_4862))
													{	/* Stackable/walk.scm 494 */
														{	/* Stackable/walk.scm 495 */
															obj_t BgL_az00_4863;

															BgL_az00_4863 = CAR(BgL_l1451z00_4862);
															{	/* Stackable/walk.scm 495 */
																BgL_localzf2depthzf2_bglt BgL_wide1246z00_4864;

																BgL_wide1246z00_4864 =
																	((BgL_localzf2depthzf2_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_localzf2depthzf2_bgl))));
																{	/* Stackable/walk.scm 495 */
																	obj_t BgL_auxz00_6379;
																	BgL_objectz00_bglt BgL_tmpz00_6375;

																	BgL_auxz00_6379 =
																		((obj_t) BgL_wide1246z00_4864);
																	BgL_tmpz00_6375 =
																		((BgL_objectz00_bglt)
																		((BgL_localz00_bglt)
																			((BgL_localz00_bglt) BgL_az00_4863)));
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6375,
																		BgL_auxz00_6379);
																}
																((BgL_objectz00_bglt)
																	((BgL_localz00_bglt)
																		((BgL_localz00_bglt) BgL_az00_4863)));
																{	/* Stackable/walk.scm 495 */
																	long BgL_arg2148z00_4865;

																	BgL_arg2148z00_4865 =
																		BGL_CLASS_NUM
																		(BGl_localzf2depthzf2zzstackable_walkz00);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			((BgL_localz00_bglt) ((BgL_localz00_bglt)
																					BgL_az00_4863))),
																		BgL_arg2148z00_4865);
																}
																((BgL_localz00_bglt)
																	((BgL_localz00_bglt)
																		((BgL_localz00_bglt) BgL_az00_4863)));
															}
															{
																BgL_localzf2depthzf2_bglt BgL_auxz00_6393;

																{
																	obj_t BgL_auxz00_6394;

																	{	/* Stackable/walk.scm 497 */
																		BgL_objectz00_bglt BgL_tmpz00_6395;

																		BgL_tmpz00_6395 =
																			((BgL_objectz00_bglt)
																			((BgL_localz00_bglt)
																				((BgL_localz00_bglt) BgL_az00_4863)));
																		BgL_auxz00_6394 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_6395);
																	}
																	BgL_auxz00_6393 =
																		((BgL_localzf2depthzf2_bglt)
																		BgL_auxz00_6394);
																}
																((((BgL_localzf2depthzf2_bglt)
																			COBJECT(BgL_auxz00_6393))->BgL_depthz00) =
																	((long) (BgL_depthz00_4852 + 1L)), BUNSPEC);
															}
															{
																BgL_localzf2depthzf2_bglt BgL_auxz00_6403;

																{
																	obj_t BgL_auxz00_6404;

																	{	/* Stackable/walk.scm 496 */
																		BgL_objectz00_bglt BgL_tmpz00_6405;

																		BgL_tmpz00_6405 =
																			((BgL_objectz00_bglt)
																			((BgL_localz00_bglt)
																				((BgL_localz00_bglt) BgL_az00_4863)));
																		BgL_auxz00_6404 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_6405);
																	}
																	BgL_auxz00_6403 =
																		((BgL_localzf2depthzf2_bglt)
																		BgL_auxz00_6404);
																}
																((((BgL_localzf2depthzf2_bglt)
																			COBJECT(BgL_auxz00_6403))->BgL_ownerz00) =
																	((obj_t) BgL_localz00_4856), BUNSPEC);
															}
															((BgL_localz00_bglt)
																((BgL_localz00_bglt) BgL_az00_4863));
														}
														{
															obj_t BgL_l1451z00_6414;

															BgL_l1451z00_6414 = CDR(BgL_l1451z00_4862);
															BgL_l1451z00_4862 = BgL_l1451z00_6414;
															goto BgL_zc3z04anonymousza32146ze3z87_4861;
														}
													}
												else
													{	/* Stackable/walk.scm 494 */
														((bool_t) 1);
													}
											}
										}
										{	/* Stackable/walk.scm 499 */
											obj_t BgL_arg2150z00_4866;
											long BgL_arg2151z00_4867;

											BgL_arg2150z00_4866 =
												(((BgL_sfunz00_bglt) COBJECT(BgL_i1243z00_4859))->
												BgL_bodyz00);
											BgL_arg2151z00_4867 = (BgL_depthz00_4852 + 1L);
											BGl_depthzd2letzd2zzstackable_walkz00(
												((BgL_nodez00_bglt) BgL_arg2150z00_4866),
												BINT(BgL_arg2151z00_4867), BgL_localz00_4856);
								}}}
								{
									obj_t BgL_l1454z00_6421;

									BgL_l1454z00_6421 = CDR(BgL_l1454z00_4855);
									BgL_l1454z00_4855 = BgL_l1454z00_6421;
									goto BgL_zc3z04anonymousza32143ze3z87_4854;
								}
							}
						else
							{	/* Stackable/walk.scm 488 */
								((bool_t) 1);
							}
					}
				}
				return
					BGl_depthzd2letzd2zzstackable_walkz00(
					(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_4652)))->BgL_bodyz00),
					BINT(BgL_depthz00_4852), BgL_funz00_4654);
			}
		}

	}



/* &depth-let-let-var1514 */
	obj_t BGl_z62depthzd2letzd2letzd2var1514zb0zzstackable_walkz00(obj_t
		BgL_envz00_4655, obj_t BgL_nodez00_4656, obj_t BgL_depthz00_4657,
		obj_t BgL_funz00_4658)
	{
		{	/* Stackable/walk.scm 471 */
			{	/* Stackable/walk.scm 472 */
				long BgL_depthz00_4869;

				BgL_depthz00_4869 = ((long) CINT(BgL_depthz00_4657) + 1L);
				{	/* Stackable/walk.scm 474 */
					obj_t BgL_g1450z00_4870;

					BgL_g1450z00_4870 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_4656)))->BgL_bindingsz00);
					{
						obj_t BgL_l1448z00_4872;

						BgL_l1448z00_4872 = BgL_g1450z00_4870;
					BgL_zc3z04anonymousza32134ze3z87_4871:
						if (PAIRP(BgL_l1448z00_4872))
							{	/* Stackable/walk.scm 474 */
								{	/* Stackable/walk.scm 477 */
									obj_t BgL_bindingz00_4873;

									BgL_bindingz00_4873 = CAR(BgL_l1448z00_4872);
									{	/* Stackable/walk.scm 475 */
										BgL_localz00_bglt BgL_tmp1232z00_4874;

										BgL_tmp1232z00_4874 =
											((BgL_localz00_bglt) CAR(((obj_t) BgL_bindingz00_4873)));
										{	/* Stackable/walk.scm 475 */
											BgL_localzf2depthzf2_bglt BgL_wide1234z00_4875;

											BgL_wide1234z00_4875 =
												((BgL_localzf2depthzf2_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_localzf2depthzf2_bgl))));
											{	/* Stackable/walk.scm 475 */
												obj_t BgL_auxz00_6441;
												BgL_objectz00_bglt BgL_tmpz00_6438;

												BgL_auxz00_6441 = ((obj_t) BgL_wide1234z00_4875);
												BgL_tmpz00_6438 =
													((BgL_objectz00_bglt)
													((BgL_localz00_bglt) BgL_tmp1232z00_4874));
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6438,
													BgL_auxz00_6441);
											}
											((BgL_objectz00_bglt)
												((BgL_localz00_bglt) BgL_tmp1232z00_4874));
											{	/* Stackable/walk.scm 475 */
												long BgL_arg2136z00_4876;

												BgL_arg2136z00_4876 =
													BGL_CLASS_NUM
													(BGl_localzf2depthzf2zzstackable_walkz00);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
															(BgL_localz00_bglt) BgL_tmp1232z00_4874)),
													BgL_arg2136z00_4876);
											}
											((BgL_localz00_bglt)
												((BgL_localz00_bglt) BgL_tmp1232z00_4874));
										}
										{
											BgL_localzf2depthzf2_bglt BgL_auxz00_6452;

											{
												obj_t BgL_auxz00_6453;

												{	/* Stackable/walk.scm 477 */
													BgL_objectz00_bglt BgL_tmpz00_6454;

													BgL_tmpz00_6454 =
														((BgL_objectz00_bglt)
														((BgL_localz00_bglt) BgL_tmp1232z00_4874));
													BgL_auxz00_6453 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_6454);
												}
												BgL_auxz00_6452 =
													((BgL_localzf2depthzf2_bglt) BgL_auxz00_6453);
											}
											((((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_6452))->
													BgL_depthz00) = ((long) BgL_depthz00_4869), BUNSPEC);
										}
										{
											BgL_localzf2depthzf2_bglt BgL_auxz00_6460;

											{
												obj_t BgL_auxz00_6461;

												{	/* Stackable/walk.scm 476 */
													BgL_objectz00_bglt BgL_tmpz00_6462;

													BgL_tmpz00_6462 =
														((BgL_objectz00_bglt)
														((BgL_localz00_bglt) BgL_tmp1232z00_4874));
													BgL_auxz00_6461 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_6462);
												}
												BgL_auxz00_6460 =
													((BgL_localzf2depthzf2_bglt) BgL_auxz00_6461);
											}
											((((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_6460))->
													BgL_ownerz00) = ((obj_t) BgL_funz00_4658), BUNSPEC);
										}
										((BgL_localz00_bglt) BgL_tmp1232z00_4874);
									}
									{	/* Stackable/walk.scm 478 */
										obj_t BgL_arg2137z00_4877;

										BgL_arg2137z00_4877 = CDR(((obj_t) BgL_bindingz00_4873));
										BGl_depthzd2letzd2zzstackable_walkz00(
											((BgL_nodez00_bglt) BgL_arg2137z00_4877),
											BINT(BgL_depthz00_4869), BgL_funz00_4658);
								}}
								{
									obj_t BgL_l1448z00_6474;

									BgL_l1448z00_6474 = CDR(BgL_l1448z00_4872);
									BgL_l1448z00_4872 = BgL_l1448z00_6474;
									goto BgL_zc3z04anonymousza32134ze3z87_4871;
								}
							}
						else
							{	/* Stackable/walk.scm 474 */
								((bool_t) 1);
							}
					}
				}
				return
					BGl_depthzd2letzd2zzstackable_walkz00(
					(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_4656)))->BgL_bodyz00),
					BINT(BgL_depthz00_4869), BgL_funz00_4658);
			}
		}

	}



/* &depth-let-var1512 */
	obj_t BGl_z62depthzd2letzd2var1512z62zzstackable_walkz00(obj_t
		BgL_envz00_4659, obj_t BgL_nodez00_4660, obj_t BgL_depthz00_4661,
		obj_t BgL_funz00_4662)
	{
		{	/* Stackable/walk.scm 461 */
			{	/* Stackable/walk.scm 463 */
				bool_t BgL_test2516z00_6480;

				{	/* Stackable/walk.scm 463 */
					BgL_variablez00_bglt BgL_arg2130z00_4879;

					BgL_arg2130z00_4879 =
						(((BgL_varz00_bglt) COBJECT(
								((BgL_varz00_bglt) BgL_nodez00_4660)))->BgL_variablez00);
					{	/* Stackable/walk.scm 463 */
						obj_t BgL_classz00_4880;

						BgL_classz00_4880 = BGl_localzf2depthzf2zzstackable_walkz00;
						{	/* Stackable/walk.scm 463 */
							obj_t BgL_oclassz00_4881;

							{	/* Stackable/walk.scm 463 */
								obj_t BgL_arg1815z00_4882;
								long BgL_arg1816z00_4883;

								BgL_arg1815z00_4882 = (BGl_za2classesza2z00zz__objectz00);
								{	/* Stackable/walk.scm 463 */
									long BgL_arg1817z00_4884;

									BgL_arg1817z00_4884 =
										BGL_OBJECT_CLASS_NUM(
										((BgL_objectz00_bglt) BgL_arg2130z00_4879));
									BgL_arg1816z00_4883 = (BgL_arg1817z00_4884 - OBJECT_TYPE);
								}
								BgL_oclassz00_4881 =
									VECTOR_REF(BgL_arg1815z00_4882, BgL_arg1816z00_4883);
							}
							BgL_test2516z00_6480 = (BgL_oclassz00_4881 == BgL_classz00_4880);
				}}}
				if (BgL_test2516z00_6480)
					{	/* Stackable/walk.scm 464 */
						BgL_localz00_bglt BgL_i1230z00_4885;

						BgL_i1230z00_4885 =
							((BgL_localz00_bglt)
							(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_nodez00_4660)))->BgL_variablez00));
						{	/* Stackable/walk.scm 465 */
							bool_t BgL_test2517z00_6492;

							{	/* Stackable/walk.scm 465 */
								obj_t BgL_arg2129z00_4886;

								{
									BgL_localzf2depthzf2_bglt BgL_auxz00_6493;

									{
										obj_t BgL_auxz00_6494;

										{	/* Stackable/walk.scm 465 */
											BgL_objectz00_bglt BgL_tmpz00_6495;

											BgL_tmpz00_6495 =
												((BgL_objectz00_bglt) BgL_i1230z00_4885);
											BgL_auxz00_6494 = BGL_OBJECT_WIDENING(BgL_tmpz00_6495);
										}
										BgL_auxz00_6493 =
											((BgL_localzf2depthzf2_bglt) BgL_auxz00_6494);
									}
									BgL_arg2129z00_4886 =
										(((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_6493))->
										BgL_ownerz00);
								}
								BgL_test2517z00_6492 = (BgL_arg2129z00_4886 == BgL_funz00_4662);
							}
							if (BgL_test2517z00_6492)
								{	/* Stackable/walk.scm 465 */
									return BFALSE;
								}
							else
								{	/* Stackable/walk.scm 465 */
									return
										((((BgL_localz00_bglt) COBJECT(
													((BgL_localz00_bglt) BgL_i1230z00_4885)))->
											BgL_valzd2noescapezd2) = ((obj_t) BFALSE), BUNSPEC);
								}
						}
					}
				else
					{	/* Stackable/walk.scm 463 */
						return BFALSE;
					}
			}
		}

	}



/* &lift-let!-let-var1508 */
	BgL_nodez00_bglt
		BGl_z62liftzd2letz12zd2letzd2var1508za2zzstackable_walkz00(obj_t
		BgL_envz00_4663, obj_t BgL_nodez00_4664)
	{
		{	/* Stackable/walk.scm 393 */
			{	/* Stackable/walk.scm 429 */
				obj_t BgL_lbindingsz00_4888;

				BgL_lbindingsz00_4888 =
					BGl_zc3z04anonymousza32075ze3ze70z60zzstackable_walkz00(
					(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_4664)))->BgL_bindingsz00));
				if (PAIRP(BgL_lbindingsz00_4888))
					{	/* Stackable/walk.scm 432 */
						BgL_letzd2varzd2_bglt BgL_arg2072z00_4889;

						{	/* Stackable/walk.scm 432 */
							BgL_letzd2varzd2_bglt BgL_new1215z00_4890;

							{	/* Stackable/walk.scm 432 */
								BgL_letzd2varzd2_bglt BgL_new1221z00_4891;

								BgL_new1221z00_4891 =
									((BgL_letzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_letzd2varzd2_bgl))));
								{	/* Stackable/walk.scm 432 */
									long BgL_arg2074z00_4892;

									BgL_arg2074z00_4892 =
										BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1221z00_4891),
										BgL_arg2074z00_4892);
								}
								{	/* Stackable/walk.scm 432 */
									BgL_objectz00_bglt BgL_tmpz00_6512;

									BgL_tmpz00_6512 = ((BgL_objectz00_bglt) BgL_new1221z00_4891);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6512, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1221z00_4891);
								BgL_new1215z00_4890 = BgL_new1221z00_4891;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1215z00_4890)))->BgL_locz00) =
								((obj_t) (((BgL_nodez00_bglt)
											COBJECT(((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt)
														BgL_nodez00_4664))))->BgL_locz00)), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1215z00_4890)))->BgL_typez00) =
								((BgL_typez00_bglt) (((BgL_nodez00_bglt)
											COBJECT(((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt)
														BgL_nodez00_4664))))->BgL_typez00)), BUNSPEC);
							((((BgL_nodezf2effectzf2_bglt)
										COBJECT(((BgL_nodezf2effectzf2_bglt)
												BgL_new1215z00_4890)))->BgL_sidezd2effectzd2) =
								((obj_t) (((BgL_nodezf2effectzf2_bglt)
											COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
															(BgL_letzd2varzd2_bglt) BgL_nodez00_4664)))))->
										BgL_sidezd2effectzd2)), BUNSPEC);
							((((BgL_nodezf2effectzf2_bglt)
										COBJECT(((BgL_nodezf2effectzf2_bglt)
												BgL_new1215z00_4890)))->BgL_keyz00) =
								((obj_t) (((BgL_nodezf2effectzf2_bglt)
											COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
															(BgL_letzd2varzd2_bglt) BgL_nodez00_4664)))))->
										BgL_keyz00)), BUNSPEC);
							((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1215z00_4890))->
									BgL_bindingsz00) = ((obj_t) BgL_lbindingsz00_4888), BUNSPEC);
							((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1215z00_4890))->
									BgL_bodyz00) =
								((BgL_nodez00_bglt) ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt)
											BgL_nodez00_4664))), BUNSPEC);
							((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1215z00_4890))->
									BgL_removablezf3zf3) =
								((bool_t) (((BgL_letzd2varzd2_bglt)
											COBJECT(((BgL_letzd2varzd2_bglt) ((BgL_nodez00_bglt) (
															(BgL_letzd2varzd2_bglt) BgL_nodez00_4664)))))->
										BgL_removablezf3zf3)), BUNSPEC);
							BgL_arg2072z00_4889 = BgL_new1215z00_4890;
						}
						return
							BGl_liftzd2letz12zc0zzstackable_walkz00(
							((BgL_nodez00_bglt) BgL_arg2072z00_4889));
					}
				else
					{	/* Stackable/walk.scm 430 */
						return
							BGl_walk0z12z12zzast_walkz00(
							((BgL_nodez00_bglt)
								((BgL_letzd2varzd2_bglt) BgL_nodez00_4664)),
							BGl_liftzd2letz12zd2envz12zzstackable_walkz00);
					}
			}
		}

	}



/* liftable?~0 */
	bool_t BGl_liftablezf3ze70z14zzstackable_walkz00(obj_t BgL_nodez00_2671)
	{
		{	/* Stackable/walk.scm 415 */
		BGl_liftablezf3ze70z14zzstackable_walkz00:
			{	/* Stackable/walk.scm 401 */
				bool_t BgL_test2519z00_6552;

				{	/* Stackable/walk.scm 401 */
					obj_t BgL_classz00_4102;

					BgL_classz00_4102 = BGl_letzd2varzd2zzast_nodez00;
					if (BGL_OBJECTP(BgL_nodez00_2671))
						{	/* Stackable/walk.scm 401 */
							BgL_objectz00_bglt BgL_arg1807z00_4104;

							BgL_arg1807z00_4104 = (BgL_objectz00_bglt) (BgL_nodez00_2671);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Stackable/walk.scm 401 */
									long BgL_idxz00_4110;

									BgL_idxz00_4110 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4104);
									BgL_test2519z00_6552 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4110 + 3L)) == BgL_classz00_4102);
								}
							else
								{	/* Stackable/walk.scm 401 */
									bool_t BgL_res2289z00_4135;

									{	/* Stackable/walk.scm 401 */
										obj_t BgL_oclassz00_4118;

										{	/* Stackable/walk.scm 401 */
											obj_t BgL_arg1815z00_4126;
											long BgL_arg1816z00_4127;

											BgL_arg1815z00_4126 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Stackable/walk.scm 401 */
												long BgL_arg1817z00_4128;

												BgL_arg1817z00_4128 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4104);
												BgL_arg1816z00_4127 =
													(BgL_arg1817z00_4128 - OBJECT_TYPE);
											}
											BgL_oclassz00_4118 =
												VECTOR_REF(BgL_arg1815z00_4126, BgL_arg1816z00_4127);
										}
										{	/* Stackable/walk.scm 401 */
											bool_t BgL__ortest_1115z00_4119;

											BgL__ortest_1115z00_4119 =
												(BgL_classz00_4102 == BgL_oclassz00_4118);
											if (BgL__ortest_1115z00_4119)
												{	/* Stackable/walk.scm 401 */
													BgL_res2289z00_4135 = BgL__ortest_1115z00_4119;
												}
											else
												{	/* Stackable/walk.scm 401 */
													long BgL_odepthz00_4120;

													{	/* Stackable/walk.scm 401 */
														obj_t BgL_arg1804z00_4121;

														BgL_arg1804z00_4121 = (BgL_oclassz00_4118);
														BgL_odepthz00_4120 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4121);
													}
													if ((3L < BgL_odepthz00_4120))
														{	/* Stackable/walk.scm 401 */
															obj_t BgL_arg1802z00_4123;

															{	/* Stackable/walk.scm 401 */
																obj_t BgL_arg1803z00_4124;

																BgL_arg1803z00_4124 = (BgL_oclassz00_4118);
																BgL_arg1802z00_4123 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4124,
																	3L);
															}
															BgL_res2289z00_4135 =
																(BgL_arg1802z00_4123 == BgL_classz00_4102);
														}
													else
														{	/* Stackable/walk.scm 401 */
															BgL_res2289z00_4135 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2519z00_6552 = BgL_res2289z00_4135;
								}
						}
					else
						{	/* Stackable/walk.scm 401 */
							BgL_test2519z00_6552 = ((bool_t) 0);
						}
				}
				if (BgL_test2519z00_6552)
					{	/* Stackable/walk.scm 403 */
						bool_t BgL_test2524z00_6575;

						{	/* Stackable/walk.scm 403 */
							obj_t BgL_tmpz00_6576;

							BgL_tmpz00_6576 =
								(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_nodez00_2671)))->
								BgL_bindingsz00);
							BgL_test2524z00_6575 = PAIRP(BgL_tmpz00_6576);
						}
						if (BgL_test2524z00_6575)
							{	/* Stackable/walk.scm 404 */
								bool_t BgL_test2525z00_6580;

								{	/* Stackable/walk.scm 404 */
									obj_t BgL_tmpz00_6581;

									{	/* Stackable/walk.scm 404 */
										obj_t BgL_pairz00_4136;

										BgL_pairz00_4136 =
											(((BgL_letzd2varzd2_bglt) COBJECT(
													((BgL_letzd2varzd2_bglt) BgL_nodez00_2671)))->
											BgL_bindingsz00);
										BgL_tmpz00_6581 = CDR(BgL_pairz00_4136);
									}
									BgL_test2525z00_6580 = NULLP(BgL_tmpz00_6581);
								}
								if (BgL_test2525z00_6580)
									{	/* Stackable/walk.scm 405 */
										bool_t BgL_test2526z00_6586;

										{	/* Stackable/walk.scm 405 */
											obj_t BgL_arg2087z00_2679;

											{	/* Stackable/walk.scm 405 */
												obj_t BgL_pairz00_4137;

												BgL_pairz00_4137 =
													(((BgL_letzd2varzd2_bglt) COBJECT(
															((BgL_letzd2varzd2_bglt) BgL_nodez00_2671)))->
													BgL_bindingsz00);
												BgL_arg2087z00_2679 = CDR(CAR(BgL_pairz00_4137));
											}
											BgL_test2526z00_6586 =
												BGl_liftablezf3ze70z14zzstackable_walkz00
												(BgL_arg2087z00_2679);
										}
										if (BgL_test2526z00_6586)
											{	/* Stackable/walk.scm 406 */
												BgL_nodez00_bglt BgL_arg2086z00_2678;

												BgL_arg2086z00_2678 =
													(((BgL_letzd2varzd2_bglt) COBJECT(
															((BgL_letzd2varzd2_bglt) BgL_nodez00_2671)))->
													BgL_bodyz00);
												{
													obj_t BgL_nodez00_6594;

													BgL_nodez00_6594 = ((obj_t) BgL_arg2086z00_2678);
													BgL_nodez00_2671 = BgL_nodez00_6594;
													goto BGl_liftablezf3ze70z14zzstackable_walkz00;
												}
											}
										else
											{	/* Stackable/walk.scm 405 */
												return ((bool_t) 0);
											}
									}
								else
									{	/* Stackable/walk.scm 404 */
										return ((bool_t) 0);
									}
							}
						else
							{	/* Stackable/walk.scm 403 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* Stackable/walk.scm 407 */
						bool_t BgL_test2527z00_6596;

						{	/* Stackable/walk.scm 407 */
							obj_t BgL_classz00_4141;

							BgL_classz00_4141 = BGl_varz00zzast_nodez00;
							if (BGL_OBJECTP(BgL_nodez00_2671))
								{	/* Stackable/walk.scm 407 */
									BgL_objectz00_bglt BgL_arg1807z00_4143;

									BgL_arg1807z00_4143 = (BgL_objectz00_bglt) (BgL_nodez00_2671);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Stackable/walk.scm 407 */
											long BgL_idxz00_4149;

											BgL_idxz00_4149 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4143);
											BgL_test2527z00_6596 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4149 + 2L)) == BgL_classz00_4141);
										}
									else
										{	/* Stackable/walk.scm 407 */
											bool_t BgL_res2290z00_4174;

											{	/* Stackable/walk.scm 407 */
												obj_t BgL_oclassz00_4157;

												{	/* Stackable/walk.scm 407 */
													obj_t BgL_arg1815z00_4165;
													long BgL_arg1816z00_4166;

													BgL_arg1815z00_4165 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Stackable/walk.scm 407 */
														long BgL_arg1817z00_4167;

														BgL_arg1817z00_4167 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4143);
														BgL_arg1816z00_4166 =
															(BgL_arg1817z00_4167 - OBJECT_TYPE);
													}
													BgL_oclassz00_4157 =
														VECTOR_REF(BgL_arg1815z00_4165,
														BgL_arg1816z00_4166);
												}
												{	/* Stackable/walk.scm 407 */
													bool_t BgL__ortest_1115z00_4158;

													BgL__ortest_1115z00_4158 =
														(BgL_classz00_4141 == BgL_oclassz00_4157);
													if (BgL__ortest_1115z00_4158)
														{	/* Stackable/walk.scm 407 */
															BgL_res2290z00_4174 = BgL__ortest_1115z00_4158;
														}
													else
														{	/* Stackable/walk.scm 407 */
															long BgL_odepthz00_4159;

															{	/* Stackable/walk.scm 407 */
																obj_t BgL_arg1804z00_4160;

																BgL_arg1804z00_4160 = (BgL_oclassz00_4157);
																BgL_odepthz00_4159 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4160);
															}
															if ((2L < BgL_odepthz00_4159))
																{	/* Stackable/walk.scm 407 */
																	obj_t BgL_arg1802z00_4162;

																	{	/* Stackable/walk.scm 407 */
																		obj_t BgL_arg1803z00_4163;

																		BgL_arg1803z00_4163 = (BgL_oclassz00_4157);
																		BgL_arg1802z00_4162 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4163, 2L);
																	}
																	BgL_res2290z00_4174 =
																		(BgL_arg1802z00_4162 == BgL_classz00_4141);
																}
															else
																{	/* Stackable/walk.scm 407 */
																	BgL_res2290z00_4174 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2527z00_6596 = BgL_res2290z00_4174;
										}
								}
							else
								{	/* Stackable/walk.scm 407 */
									BgL_test2527z00_6596 = ((bool_t) 0);
								}
						}
						if (BgL_test2527z00_6596)
							{	/* Stackable/walk.scm 408 */
								BgL_variablez00_bglt BgL_arg2093z00_2685;

								BgL_arg2093z00_2685 =
									(((BgL_varz00_bglt) COBJECT(
											((BgL_varz00_bglt) BgL_nodez00_2671)))->BgL_variablez00);
								{	/* Stackable/walk.scm 408 */
									obj_t BgL_classz00_4176;

									BgL_classz00_4176 = BGl_localz00zzast_varz00;
									{	/* Stackable/walk.scm 408 */
										BgL_objectz00_bglt BgL_arg1807z00_4178;

										{	/* Stackable/walk.scm 408 */
											obj_t BgL_tmpz00_6621;

											BgL_tmpz00_6621 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg2093z00_2685));
											BgL_arg1807z00_4178 =
												(BgL_objectz00_bglt) (BgL_tmpz00_6621);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Stackable/walk.scm 408 */
												long BgL_idxz00_4184;

												BgL_idxz00_4184 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4178);
												return
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_4184 + 2L)) == BgL_classz00_4176);
											}
										else
											{	/* Stackable/walk.scm 408 */
												bool_t BgL_res2291z00_4209;

												{	/* Stackable/walk.scm 408 */
													obj_t BgL_oclassz00_4192;

													{	/* Stackable/walk.scm 408 */
														obj_t BgL_arg1815z00_4200;
														long BgL_arg1816z00_4201;

														BgL_arg1815z00_4200 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Stackable/walk.scm 408 */
															long BgL_arg1817z00_4202;

															BgL_arg1817z00_4202 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4178);
															BgL_arg1816z00_4201 =
																(BgL_arg1817z00_4202 - OBJECT_TYPE);
														}
														BgL_oclassz00_4192 =
															VECTOR_REF(BgL_arg1815z00_4200,
															BgL_arg1816z00_4201);
													}
													{	/* Stackable/walk.scm 408 */
														bool_t BgL__ortest_1115z00_4193;

														BgL__ortest_1115z00_4193 =
															(BgL_classz00_4176 == BgL_oclassz00_4192);
														if (BgL__ortest_1115z00_4193)
															{	/* Stackable/walk.scm 408 */
																BgL_res2291z00_4209 = BgL__ortest_1115z00_4193;
															}
														else
															{	/* Stackable/walk.scm 408 */
																long BgL_odepthz00_4194;

																{	/* Stackable/walk.scm 408 */
																	obj_t BgL_arg1804z00_4195;

																	BgL_arg1804z00_4195 = (BgL_oclassz00_4192);
																	BgL_odepthz00_4194 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_4195);
																}
																if ((2L < BgL_odepthz00_4194))
																	{	/* Stackable/walk.scm 408 */
																		obj_t BgL_arg1802z00_4197;

																		{	/* Stackable/walk.scm 408 */
																			obj_t BgL_arg1803z00_4198;

																			BgL_arg1803z00_4198 =
																				(BgL_oclassz00_4192);
																			BgL_arg1802z00_4197 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_4198, 2L);
																		}
																		BgL_res2291z00_4209 =
																			(BgL_arg1802z00_4197 ==
																			BgL_classz00_4176);
																	}
																else
																	{	/* Stackable/walk.scm 408 */
																		BgL_res2291z00_4209 = ((bool_t) 0);
																	}
															}
													}
												}
												return BgL_res2291z00_4209;
											}
									}
								}
							}
						else
							{	/* Stackable/walk.scm 409 */
								bool_t BgL_test2535z00_6644;

								{	/* Stackable/walk.scm 409 */
									obj_t BgL_classz00_4210;

									BgL_classz00_4210 = BGl_appz00zzast_nodez00;
									if (BGL_OBJECTP(BgL_nodez00_2671))
										{	/* Stackable/walk.scm 409 */
											BgL_objectz00_bglt BgL_arg1807z00_4212;

											BgL_arg1807z00_4212 =
												(BgL_objectz00_bglt) (BgL_nodez00_2671);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Stackable/walk.scm 409 */
													long BgL_idxz00_4218;

													BgL_idxz00_4218 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4212);
													BgL_test2535z00_6644 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_4218 + 3L)) == BgL_classz00_4210);
												}
											else
												{	/* Stackable/walk.scm 409 */
													bool_t BgL_res2292z00_4243;

													{	/* Stackable/walk.scm 409 */
														obj_t BgL_oclassz00_4226;

														{	/* Stackable/walk.scm 409 */
															obj_t BgL_arg1815z00_4234;
															long BgL_arg1816z00_4235;

															BgL_arg1815z00_4234 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Stackable/walk.scm 409 */
																long BgL_arg1817z00_4236;

																BgL_arg1817z00_4236 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4212);
																BgL_arg1816z00_4235 =
																	(BgL_arg1817z00_4236 - OBJECT_TYPE);
															}
															BgL_oclassz00_4226 =
																VECTOR_REF(BgL_arg1815z00_4234,
																BgL_arg1816z00_4235);
														}
														{	/* Stackable/walk.scm 409 */
															bool_t BgL__ortest_1115z00_4227;

															BgL__ortest_1115z00_4227 =
																(BgL_classz00_4210 == BgL_oclassz00_4226);
															if (BgL__ortest_1115z00_4227)
																{	/* Stackable/walk.scm 409 */
																	BgL_res2292z00_4243 =
																		BgL__ortest_1115z00_4227;
																}
															else
																{	/* Stackable/walk.scm 409 */
																	long BgL_odepthz00_4228;

																	{	/* Stackable/walk.scm 409 */
																		obj_t BgL_arg1804z00_4229;

																		BgL_arg1804z00_4229 = (BgL_oclassz00_4226);
																		BgL_odepthz00_4228 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_4229);
																	}
																	if ((3L < BgL_odepthz00_4228))
																		{	/* Stackable/walk.scm 409 */
																			obj_t BgL_arg1802z00_4231;

																			{	/* Stackable/walk.scm 409 */
																				obj_t BgL_arg1803z00_4232;

																				BgL_arg1803z00_4232 =
																					(BgL_oclassz00_4226);
																				BgL_arg1802z00_4231 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_4232, 3L);
																			}
																			BgL_res2292z00_4243 =
																				(BgL_arg1802z00_4231 ==
																				BgL_classz00_4210);
																		}
																	else
																		{	/* Stackable/walk.scm 409 */
																			BgL_res2292z00_4243 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2535z00_6644 = BgL_res2292z00_4243;
												}
										}
									else
										{	/* Stackable/walk.scm 409 */
											BgL_test2535z00_6644 = ((bool_t) 0);
										}
								}
								if (BgL_test2535z00_6644)
									{	/* Stackable/walk.scm 411 */
										BgL_variablez00_bglt BgL_vz00_2688;

										BgL_vz00_2688 =
											(((BgL_varz00_bglt) COBJECT(
													(((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt) BgL_nodez00_2671)))->
														BgL_funz00)))->BgL_variablez00);
										{	/* Stackable/walk.scm 412 */
											bool_t BgL_test2540z00_6670;

											{	/* Stackable/walk.scm 412 */
												obj_t BgL_classz00_4245;

												BgL_classz00_4245 = BGl_globalz00zzast_varz00;
												{	/* Stackable/walk.scm 412 */
													BgL_objectz00_bglt BgL_arg1807z00_4247;

													{	/* Stackable/walk.scm 412 */
														obj_t BgL_tmpz00_6671;

														BgL_tmpz00_6671 =
															((obj_t) ((BgL_objectz00_bglt) BgL_vz00_2688));
														BgL_arg1807z00_4247 =
															(BgL_objectz00_bglt) (BgL_tmpz00_6671);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Stackable/walk.scm 412 */
															long BgL_idxz00_4253;

															BgL_idxz00_4253 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4247);
															BgL_test2540z00_6670 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_4253 + 2L)) == BgL_classz00_4245);
														}
													else
														{	/* Stackable/walk.scm 412 */
															bool_t BgL_res2293z00_4278;

															{	/* Stackable/walk.scm 412 */
																obj_t BgL_oclassz00_4261;

																{	/* Stackable/walk.scm 412 */
																	obj_t BgL_arg1815z00_4269;
																	long BgL_arg1816z00_4270;

																	BgL_arg1815z00_4269 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Stackable/walk.scm 412 */
																		long BgL_arg1817z00_4271;

																		BgL_arg1817z00_4271 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4247);
																		BgL_arg1816z00_4270 =
																			(BgL_arg1817z00_4271 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_4261 =
																		VECTOR_REF(BgL_arg1815z00_4269,
																		BgL_arg1816z00_4270);
																}
																{	/* Stackable/walk.scm 412 */
																	bool_t BgL__ortest_1115z00_4262;

																	BgL__ortest_1115z00_4262 =
																		(BgL_classz00_4245 == BgL_oclassz00_4261);
																	if (BgL__ortest_1115z00_4262)
																		{	/* Stackable/walk.scm 412 */
																			BgL_res2293z00_4278 =
																				BgL__ortest_1115z00_4262;
																		}
																	else
																		{	/* Stackable/walk.scm 412 */
																			long BgL_odepthz00_4263;

																			{	/* Stackable/walk.scm 412 */
																				obj_t BgL_arg1804z00_4264;

																				BgL_arg1804z00_4264 =
																					(BgL_oclassz00_4261);
																				BgL_odepthz00_4263 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_4264);
																			}
																			if ((2L < BgL_odepthz00_4263))
																				{	/* Stackable/walk.scm 412 */
																					obj_t BgL_arg1802z00_4266;

																					{	/* Stackable/walk.scm 412 */
																						obj_t BgL_arg1803z00_4267;

																						BgL_arg1803z00_4267 =
																							(BgL_oclassz00_4261);
																						BgL_arg1802z00_4266 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_4267, 2L);
																					}
																					BgL_res2293z00_4278 =
																						(BgL_arg1802z00_4266 ==
																						BgL_classz00_4245);
																				}
																			else
																				{	/* Stackable/walk.scm 412 */
																					BgL_res2293z00_4278 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2540z00_6670 = BgL_res2293z00_4278;
														}
												}
											}
											if (BgL_test2540z00_6670)
												{	/* Stackable/walk.scm 413 */
													obj_t BgL_tmpz00_6694;

													BgL_tmpz00_6694 =
														(((BgL_funz00_bglt) COBJECT(
																((BgL_funz00_bglt)
																	(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_globalz00_bglt)
																						BgL_vz00_2688))))->
																		BgL_valuez00))))->BgL_stackzd2allocatorzd2);
													return PAIRP(BgL_tmpz00_6694);
												}
											else
												{	/* Stackable/walk.scm 412 */
													return ((bool_t) 0);
												}
										}
									}
								else
									{	/* Stackable/walk.scm 415 */
										obj_t BgL_classz00_4281;

										BgL_classz00_4281 = BGl_makezd2boxzd2zzast_nodez00;
										if (BGL_OBJECTP(BgL_nodez00_2671))
											{	/* Stackable/walk.scm 415 */
												BgL_objectz00_bglt BgL_arg1807z00_4283;

												BgL_arg1807z00_4283 =
													(BgL_objectz00_bglt) (BgL_nodez00_2671);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Stackable/walk.scm 415 */
														long BgL_idxz00_4289;

														BgL_idxz00_4289 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4283);
														return
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_4289 + 3L)) == BgL_classz00_4281);
													}
												else
													{	/* Stackable/walk.scm 415 */
														bool_t BgL_res2294z00_4314;

														{	/* Stackable/walk.scm 415 */
															obj_t BgL_oclassz00_4297;

															{	/* Stackable/walk.scm 415 */
																obj_t BgL_arg1815z00_4305;
																long BgL_arg1816z00_4306;

																BgL_arg1815z00_4305 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Stackable/walk.scm 415 */
																	long BgL_arg1817z00_4307;

																	BgL_arg1817z00_4307 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4283);
																	BgL_arg1816z00_4306 =
																		(BgL_arg1817z00_4307 - OBJECT_TYPE);
																}
																BgL_oclassz00_4297 =
																	VECTOR_REF(BgL_arg1815z00_4305,
																	BgL_arg1816z00_4306);
															}
															{	/* Stackable/walk.scm 415 */
																bool_t BgL__ortest_1115z00_4298;

																BgL__ortest_1115z00_4298 =
																	(BgL_classz00_4281 == BgL_oclassz00_4297);
																if (BgL__ortest_1115z00_4298)
																	{	/* Stackable/walk.scm 415 */
																		BgL_res2294z00_4314 =
																			BgL__ortest_1115z00_4298;
																	}
																else
																	{	/* Stackable/walk.scm 415 */
																		long BgL_odepthz00_4299;

																		{	/* Stackable/walk.scm 415 */
																			obj_t BgL_arg1804z00_4300;

																			BgL_arg1804z00_4300 =
																				(BgL_oclassz00_4297);
																			BgL_odepthz00_4299 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_4300);
																		}
																		if ((3L < BgL_odepthz00_4299))
																			{	/* Stackable/walk.scm 415 */
																				obj_t BgL_arg1802z00_4302;

																				{	/* Stackable/walk.scm 415 */
																					obj_t BgL_arg1803z00_4303;

																					BgL_arg1803z00_4303 =
																						(BgL_oclassz00_4297);
																					BgL_arg1802z00_4302 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_4303, 3L);
																				}
																				BgL_res2294z00_4314 =
																					(BgL_arg1802z00_4302 ==
																					BgL_classz00_4281);
																			}
																		else
																			{	/* Stackable/walk.scm 415 */
																				BgL_res2294z00_4314 = ((bool_t) 0);
																			}
																	}
															}
														}
														return BgL_res2294z00_4314;
													}
											}
										else
											{	/* Stackable/walk.scm 415 */
												return ((bool_t) 0);
											}
									}
							}
					}
			}
		}

	}



/* <@anonymous:2075>~0 */
	obj_t BGl_zc3z04anonymousza32075ze3ze70z60zzstackable_walkz00(obj_t
		BgL_l1443z00_2658)
	{
		{	/* Stackable/walk.scm 429 */
			{
				obj_t BgL_bindingz00_2693;

				if (NULLP(BgL_l1443z00_2658))
					{	/* Stackable/walk.scm 429 */
						return BNIL;
					}
				else
					{	/* Stackable/walk.scm 429 */
						obj_t BgL_arg2077z00_2661;
						obj_t BgL_arg2078z00_2662;

						{	/* Stackable/walk.scm 429 */
							obj_t BgL_arg2079z00_2663;

							BgL_arg2079z00_2663 = CAR(((obj_t) BgL_l1443z00_2658));
							BgL_bindingz00_2693 = BgL_arg2079z00_2663;
							{	/* Stackable/walk.scm 418 */
								bool_t BgL_test2549z00_6727;

								{	/* Stackable/walk.scm 418 */
									bool_t BgL_test2550z00_6728;

									{	/* Stackable/walk.scm 418 */
										obj_t BgL_arg2124z00_2729;

										BgL_arg2124z00_2729 = CDR(((obj_t) BgL_bindingz00_2693));
										{	/* Stackable/walk.scm 418 */
											obj_t BgL_classz00_4316;

											BgL_classz00_4316 = BGl_letzd2varzd2zzast_nodez00;
											if (BGL_OBJECTP(BgL_arg2124z00_2729))
												{	/* Stackable/walk.scm 418 */
													BgL_objectz00_bglt BgL_arg1807z00_4318;

													BgL_arg1807z00_4318 =
														(BgL_objectz00_bglt) (BgL_arg2124z00_2729);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Stackable/walk.scm 418 */
															long BgL_idxz00_4324;

															BgL_idxz00_4324 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4318);
															BgL_test2550z00_6728 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_4324 + 3L)) == BgL_classz00_4316);
														}
													else
														{	/* Stackable/walk.scm 418 */
															bool_t BgL_res2295z00_4349;

															{	/* Stackable/walk.scm 418 */
																obj_t BgL_oclassz00_4332;

																{	/* Stackable/walk.scm 418 */
																	obj_t BgL_arg1815z00_4340;
																	long BgL_arg1816z00_4341;

																	BgL_arg1815z00_4340 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Stackable/walk.scm 418 */
																		long BgL_arg1817z00_4342;

																		BgL_arg1817z00_4342 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4318);
																		BgL_arg1816z00_4341 =
																			(BgL_arg1817z00_4342 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_4332 =
																		VECTOR_REF(BgL_arg1815z00_4340,
																		BgL_arg1816z00_4341);
																}
																{	/* Stackable/walk.scm 418 */
																	bool_t BgL__ortest_1115z00_4333;

																	BgL__ortest_1115z00_4333 =
																		(BgL_classz00_4316 == BgL_oclassz00_4332);
																	if (BgL__ortest_1115z00_4333)
																		{	/* Stackable/walk.scm 418 */
																			BgL_res2295z00_4349 =
																				BgL__ortest_1115z00_4333;
																		}
																	else
																		{	/* Stackable/walk.scm 418 */
																			long BgL_odepthz00_4334;

																			{	/* Stackable/walk.scm 418 */
																				obj_t BgL_arg1804z00_4335;

																				BgL_arg1804z00_4335 =
																					(BgL_oclassz00_4332);
																				BgL_odepthz00_4334 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_4335);
																			}
																			if ((3L < BgL_odepthz00_4334))
																				{	/* Stackable/walk.scm 418 */
																					obj_t BgL_arg1802z00_4337;

																					{	/* Stackable/walk.scm 418 */
																						obj_t BgL_arg1803z00_4338;

																						BgL_arg1803z00_4338 =
																							(BgL_oclassz00_4332);
																						BgL_arg1802z00_4337 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_4338, 3L);
																					}
																					BgL_res2295z00_4349 =
																						(BgL_arg1802z00_4337 ==
																						BgL_classz00_4316);
																				}
																			else
																				{	/* Stackable/walk.scm 418 */
																					BgL_res2295z00_4349 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2550z00_6728 = BgL_res2295z00_4349;
														}
												}
											else
												{	/* Stackable/walk.scm 418 */
													BgL_test2550z00_6728 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test2550z00_6728)
										{	/* Stackable/walk.scm 418 */
											obj_t BgL_arg2123z00_2728;

											BgL_arg2123z00_2728 = CDR(((obj_t) BgL_bindingz00_2693));
											BgL_test2549z00_6727 =
												BGl_liftablezf3ze70z14zzstackable_walkz00
												(BgL_arg2123z00_2728);
										}
									else
										{	/* Stackable/walk.scm 418 */
											BgL_test2549z00_6727 = ((bool_t) 0);
										}
								}
								if (BgL_test2549z00_6727)
									{	/* Stackable/walk.scm 419 */
										BgL_letzd2varzd2_bglt BgL_i1213z00_2700;

										BgL_i1213z00_2700 =
											((BgL_letzd2varzd2_bglt)
											CDR(((obj_t) BgL_bindingz00_2693)));
										{	/* Stackable/walk.scm 421 */
											obj_t BgL_arg2105z00_2701;
											obj_t BgL_arg2106z00_2702;
											obj_t BgL_arg2107z00_2703;

											BgL_arg2105z00_2701 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															CAR(((obj_t) BgL_bindingz00_2693)))))->BgL_idz00);
											{	/* Stackable/walk.scm 422 */
												obj_t BgL_l1437z00_2712;

												BgL_l1437z00_2712 =
													(((BgL_letzd2varzd2_bglt)
														COBJECT(BgL_i1213z00_2700))->BgL_bindingsz00);
												if (NULLP(BgL_l1437z00_2712))
													{	/* Stackable/walk.scm 422 */
														BgL_arg2106z00_2702 = BNIL;
													}
												else
													{	/* Stackable/walk.scm 422 */
														obj_t BgL_head1439z00_2714;

														BgL_head1439z00_2714 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1437z00_2716;
															obj_t BgL_tail1440z00_2717;

															BgL_l1437z00_2716 = BgL_l1437z00_2712;
															BgL_tail1440z00_2717 = BgL_head1439z00_2714;
														BgL_zc3z04anonymousza32117ze3z87_2718:
															if (NULLP(BgL_l1437z00_2716))
																{	/* Stackable/walk.scm 422 */
																	BgL_arg2106z00_2702 =
																		CDR(BgL_head1439z00_2714);
																}
															else
																{	/* Stackable/walk.scm 422 */
																	obj_t BgL_newtail1441z00_2720;

																	{	/* Stackable/walk.scm 422 */
																		obj_t BgL_arg2120z00_2722;

																		{
																			BgL_variablez00_bglt BgL_auxz00_6770;

																			{	/* Stackable/walk.scm 422 */
																				obj_t BgL_pairz00_4356;

																				BgL_pairz00_4356 =
																					CAR(((obj_t) BgL_l1437z00_2716));
																				BgL_auxz00_6770 =
																					((BgL_variablez00_bglt)
																					CAR(BgL_pairz00_4356));
																			}
																			BgL_arg2120z00_2722 =
																				(((BgL_variablez00_bglt)
																					COBJECT(BgL_auxz00_6770))->BgL_idz00);
																		}
																		BgL_newtail1441z00_2720 =
																			MAKE_YOUNG_PAIR(BgL_arg2120z00_2722,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1440z00_2717,
																		BgL_newtail1441z00_2720);
																	{	/* Stackable/walk.scm 422 */
																		obj_t BgL_arg2119z00_2721;

																		BgL_arg2119z00_2721 =
																			CDR(((obj_t) BgL_l1437z00_2716));
																		{
																			obj_t BgL_tail1440z00_6781;
																			obj_t BgL_l1437z00_6780;

																			BgL_l1437z00_6780 = BgL_arg2119z00_2721;
																			BgL_tail1440z00_6781 =
																				BgL_newtail1441z00_2720;
																			BgL_tail1440z00_2717 =
																				BgL_tail1440z00_6781;
																			BgL_l1437z00_2716 = BgL_l1437z00_6780;
																			goto
																				BgL_zc3z04anonymousza32117ze3z87_2718;
																		}
																	}
																}
														}
													}
											}
											BgL_arg2107z00_2703 =
												(((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_i1213z00_2700)))->
												BgL_locz00);
											{	/* Stackable/walk.scm 420 */
												obj_t BgL_list2108z00_2704;

												{	/* Stackable/walk.scm 420 */
													obj_t BgL_arg2109z00_2705;

													{	/* Stackable/walk.scm 420 */
														obj_t BgL_arg2110z00_2706;

														{	/* Stackable/walk.scm 420 */
															obj_t BgL_arg2111z00_2707;

															{	/* Stackable/walk.scm 420 */
																obj_t BgL_arg2112z00_2708;

																{	/* Stackable/walk.scm 420 */
																	obj_t BgL_arg2113z00_2709;

																	{	/* Stackable/walk.scm 420 */
																		obj_t BgL_arg2114z00_2710;

																		BgL_arg2114z00_2710 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2375z00zzstackable_walkz00,
																			BNIL);
																		BgL_arg2113z00_2709 =
																			MAKE_YOUNG_PAIR(BgL_arg2107z00_2703,
																			BgL_arg2114z00_2710);
																	}
																	BgL_arg2112z00_2708 =
																		MAKE_YOUNG_PAIR
																		(BGl_string2376z00zzstackable_walkz00,
																		BgL_arg2113z00_2709);
																}
																BgL_arg2111z00_2707 =
																	MAKE_YOUNG_PAIR(BgL_arg2106z00_2702,
																	BgL_arg2112z00_2708);
															}
															BgL_arg2110z00_2706 =
																MAKE_YOUNG_PAIR
																(BGl_string2377z00zzstackable_walkz00,
																BgL_arg2111z00_2707);
														}
														BgL_arg2109z00_2705 =
															MAKE_YOUNG_PAIR(BgL_arg2105z00_2701,
															BgL_arg2110z00_2706);
													}
													BgL_list2108z00_2704 =
														MAKE_YOUNG_PAIR
														(BGl_string2378z00zzstackable_walkz00,
														BgL_arg2109z00_2705);
												}
												BGl_verbosez00zztools_speekz00(BINT(3L),
													BgL_list2108z00_2704);
											}
										}
										{	/* Stackable/walk.scm 424 */
											BgL_nodez00_bglt BgL_arg2122z00_2726;

											BgL_arg2122z00_2726 =
												(((BgL_letzd2varzd2_bglt) COBJECT(BgL_i1213z00_2700))->
												BgL_bodyz00);
											{	/* Stackable/walk.scm 424 */
												obj_t BgL_auxz00_6796;
												obj_t BgL_tmpz00_6794;

												BgL_auxz00_6796 = ((obj_t) BgL_arg2122z00_2726);
												BgL_tmpz00_6794 = ((obj_t) BgL_bindingz00_2693);
												SET_CDR(BgL_tmpz00_6794, BgL_auxz00_6796);
											}
										}
										BgL_arg2077z00_2661 =
											(((BgL_letzd2varzd2_bglt) COBJECT(BgL_i1213z00_2700))->
											BgL_bindingsz00);
									}
								else
									{	/* Stackable/walk.scm 418 */
										BgL_arg2077z00_2661 = BNIL;
									}
							}
						}
						{	/* Stackable/walk.scm 429 */
							obj_t BgL_arg2080z00_2664;

							BgL_arg2080z00_2664 = CDR(((obj_t) BgL_l1443z00_2658));
							BgL_arg2078z00_2662 =
								BGl_zc3z04anonymousza32075ze3ze70z60zzstackable_walkz00
								(BgL_arg2080z00_2664);
						}
						return bgl_append2(BgL_arg2077z00_2661, BgL_arg2078z00_2662);
					}
			}
		}

	}



/* &node-escape-make-box1504 */
	obj_t BGl_z62nodezd2escapezd2makezd2box1504zb0zzstackable_walkz00(obj_t
		BgL_envz00_4665, obj_t BgL_nodez00_4666, obj_t BgL_ctxz00_4667)
	{
		{	/* Stackable/walk.scm 367 */
			return
				BGl_escapez12z12zzstackable_walkz00(
				((obj_t) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_4666)), BgL_ctxz00_4667);
		}

	}



/* &node-escape-app1502 */
	obj_t BGl_z62nodezd2escapezd2app1502z62zzstackable_walkz00(obj_t
		BgL_envz00_4668, obj_t BgL_nodez00_4669, obj_t BgL_ctxz00_4670)
	{
		{	/* Stackable/walk.scm 361 */
			return
				BGl_escapez12z12zzstackable_walkz00(
				((obj_t) ((BgL_appz00_bglt) BgL_nodez00_4669)), BgL_ctxz00_4670);
		}

	}



/* &escape!-closure1498 */
	obj_t BGl_z62escapez12zd2closure1498za2zzstackable_walkz00(obj_t
		BgL_envz00_4671, obj_t BgL_nodez00_4672, obj_t BgL_ctxz00_4673)
	{
		{	/* Stackable/walk.scm 343 */
			{

				BGl_walk1z12z12zzast_walkz00(
					((BgL_nodez00_bglt)
						((BgL_closurez00_bglt) BgL_nodez00_4672)),
					BGl_escapez12zd2envzc0zzstackable_walkz00, BgL_ctxz00_4673);
				{	/* Stackable/walk.scm 345 */
					BgL_variablez00_bglt BgL_vz00_4897;

					BgL_vz00_4897 =
						(((BgL_varz00_bglt) COBJECT(
								((BgL_varz00_bglt)
									((BgL_closurez00_bglt) BgL_nodez00_4672))))->BgL_variablez00);
					{	/* Stackable/walk.scm 345 */
						BgL_valuez00_bglt BgL_fz00_4898;

						BgL_fz00_4898 =
							(((BgL_variablez00_bglt) COBJECT(BgL_vz00_4897))->BgL_valuez00);
						{	/* Stackable/walk.scm 346 */

							if (CBOOL(
									(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_fz00_4898)))->
										BgL_stackablez00)))
								{	/* Stackable/walk.scm 348 */
									{	/* Stackable/walk.scm 349 */
										obj_t BgL_tmpz00_6821;

										BgL_tmpz00_6821 = ((obj_t) BgL_ctxz00_4673);
										SET_CAR(BgL_tmpz00_6821, BFALSE);
									}
									return
										((((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_fz00_4898)))->
											BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
								}
							else
								{	/* Stackable/walk.scm 348 */
									return BFALSE;
								}
						}
					}
				}
			}
		}

	}



/* &escape!-make-box1496 */
	obj_t BGl_z62escapez12zd2makezd2box1496z70zzstackable_walkz00(obj_t
		BgL_envz00_4674, obj_t BgL_nodez00_4675, obj_t BgL_ctxz00_4676)
	{
		{	/* Stackable/walk.scm 333 */
			if (CBOOL(
					(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_4675)))->
						BgL_stackablez00)))
				{	/* Stackable/walk.scm 335 */
					{	/* Stackable/walk.scm 336 */
						obj_t BgL_tmpz00_6830;

						BgL_tmpz00_6830 = ((obj_t) BgL_ctxz00_4676);
						SET_CAR(BgL_tmpz00_6830, BFALSE);
					}
					((((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_4675)))->
							BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
					{	/* Stackable/walk.scm 338 */
						BgL_nodez00_bglt BgL_arg2060z00_4900;

						BgL_arg2060z00_4900 =
							(((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_4675)))->BgL_valuez00);
						return
							BGl_escapez12z12zzstackable_walkz00(
							((obj_t) BgL_arg2060z00_4900), BgL_ctxz00_4676);
					}
				}
			else
				{	/* Stackable/walk.scm 335 */
					return BFALSE;
				}
		}

	}



/* &escape!-app1494 */
	obj_t BGl_z62escapez12zd2app1494za2zzstackable_walkz00(obj_t BgL_envz00_4677,
		obj_t BgL_nodez00_4678, obj_t BgL_ctxz00_4679)
	{
		{	/* Stackable/walk.scm 324 */
			if (CBOOL(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_4678)))->BgL_stackablez00)))
				{	/* Stackable/walk.scm 326 */
					{	/* Stackable/walk.scm 327 */
						obj_t BgL_tmpz00_6843;

						BgL_tmpz00_6843 = ((obj_t) BgL_ctxz00_4679);
						SET_CAR(BgL_tmpz00_6843, BFALSE);
					}
					return
						((((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nodez00_4678)))->BgL_stackablez00) =
						((obj_t) BFALSE), BUNSPEC);
				}
			else
				{	/* Stackable/walk.scm 326 */
					return BFALSE;
				}
		}

	}



/* &escape!-node1492 */
	obj_t BGl_z62escapez12zd2node1492za2zzstackable_walkz00(obj_t BgL_envz00_4680,
		obj_t BgL_nodez00_4681, obj_t BgL_ctxz00_4682)
	{
		{	/* Stackable/walk.scm 318 */
			return
				BGl_nodezd2escapezd2zzstackable_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_4681), BgL_ctxz00_4682);
		}

	}



/* &escape!-local1490 */
	obj_t BGl_z62escapez12zd2local1490za2zzstackable_walkz00(obj_t
		BgL_envz00_4683, obj_t BgL_varz00_4684, obj_t BgL_ctxz00_4685)
	{
		{	/* Stackable/walk.scm 309 */
			if (
				((((BgL_localz00_bglt) COBJECT(
								((BgL_localz00_bglt) BgL_varz00_4684)))->
						BgL_valzd2noescapezd2) == BTRUE))
				{	/* Stackable/walk.scm 311 */
					{	/* Stackable/walk.scm 312 */
						obj_t BgL_tmpz00_6854;

						BgL_tmpz00_6854 = ((obj_t) BgL_ctxz00_4685);
						SET_CAR(BgL_tmpz00_6854, BFALSE);
					}
					return
						((((BgL_localz00_bglt) COBJECT(
									((BgL_localz00_bglt) BgL_varz00_4684)))->
							BgL_valzd2noescapezd2) = ((obj_t) BFALSE), BUNSPEC);
				}
			else
				{	/* Stackable/walk.scm 311 */
					return BFALSE;
				}
		}

	}



/* &escape!-global1488 */
	obj_t BGl_z62escapez12zd2global1488za2zzstackable_walkz00(obj_t
		BgL_envz00_4686, obj_t BgL_varz00_4687, obj_t BgL_ctxz00_4688)
	{
		{	/* Stackable/walk.scm 303 */
			return BUNSPEC;
		}

	}



/* &stackable-sequence1484 */
	obj_t BGl_z62stackablezd2sequence1484zb0zzstackable_walkz00(obj_t
		BgL_envz00_4689, obj_t BgL_nodez00_4690, obj_t BgL_escpz00_4691,
		obj_t BgL_depthz00_4692, obj_t BgL_ctxz00_4693)
	{
		{	/* Stackable/walk.scm 280 */
			if (NULLP(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_4690)))->BgL_nodesz00)))
				{	/* Stackable/walk.scm 282 */
					return BFALSE;
				}
			else
				{
					obj_t BgL_nodesz00_4908;

					BgL_nodesz00_4908 =
						(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_4690)))->BgL_nodesz00);
				BgL_loopz00_4907:
					if (NULLP(CDR(((obj_t) BgL_nodesz00_4908))))
						{	/* Stackable/walk.scm 285 */
							obj_t BgL_arg2046z00_4909;

							BgL_arg2046z00_4909 = CAR(((obj_t) BgL_nodesz00_4908));
							return
								BGl_stackablez00zzstackable_walkz00(
								((BgL_nodez00_bglt) BgL_arg2046z00_4909),
								CBOOL(BgL_escpz00_4691),
								(long) CINT(BgL_depthz00_4692), ((obj_t) BgL_ctxz00_4693));
						}
					else
						{	/* Stackable/walk.scm 284 */
							{	/* Stackable/walk.scm 287 */
								obj_t BgL_arg2047z00_4910;

								BgL_arg2047z00_4910 = CAR(((obj_t) BgL_nodesz00_4908));
								BGl_stackablez00zzstackable_walkz00(
									((BgL_nodez00_bglt) BgL_arg2047z00_4910), ((bool_t) 0),
									100000L, ((obj_t) BgL_ctxz00_4693));
							}
							{	/* Stackable/walk.scm 288 */
								obj_t BgL_arg2049z00_4911;

								BgL_arg2049z00_4911 = CDR(((obj_t) BgL_nodesz00_4908));
								{
									obj_t BgL_nodesz00_6881;

									BgL_nodesz00_6881 = BgL_arg2049z00_4911;
									BgL_nodesz00_4908 = BgL_nodesz00_6881;
									goto BgL_loopz00_4907;
								}
							}
						}
				}
		}

	}



/* &stackable-funcall1482 */
	obj_t BGl_z62stackablezd2funcall1482zb0zzstackable_walkz00(obj_t
		BgL_envz00_4694, obj_t BgL_nodez00_4695, obj_t BgL_escpz00_4696,
		obj_t BgL_depthz00_4697, obj_t BgL_ctxz00_4698)
	{
		{	/* Stackable/walk.scm 273 */
			{	/* Stackable/walk.scm 273 */
				obj_t BgL_escpz00_4913;

				BgL_escpz00_4913 = BgL_escpz00_4696;
				{

					BgL_escpz00_4913 = BTRUE;
					return
						BGl_walk3z00zzast_walkz00(
						((BgL_nodez00_bglt)
							((BgL_funcallz00_bglt) BgL_nodez00_4695)),
						BGl_stackablezd2envzd2zzstackable_walkz00, BgL_escpz00_4913,
						BgL_depthz00_4697, ((obj_t) BgL_ctxz00_4698));
				}
			}
		}

	}



/* &stackable-app1480 */
	obj_t BGl_z62stackablezd2app1480zb0zzstackable_walkz00(obj_t BgL_envz00_4699,
		obj_t BgL_nodez00_4700, obj_t BgL_escpz00_4701, obj_t BgL_depthz00_4702,
		obj_t BgL_ctxz00_4703)
	{
		{	/* Stackable/walk.scm 189 */
			{	/* Stackable/walk.scm 189 */
				bool_t BgL_tmpz00_6888;

				{
					obj_t BgL_argsz00_4919;

					{	/* Stackable/walk.scm 225 */
						BgL_variablez00_bglt BgL_vz00_5015;

						BgL_vz00_5015 =
							(((BgL_varz00_bglt) COBJECT(
									(((BgL_appz00_bglt) COBJECT(
												((BgL_appz00_bglt) BgL_nodez00_4700)))->BgL_funz00)))->
							BgL_variablez00);
						{	/* Stackable/walk.scm 225 */
							BgL_valuez00_bglt BgL_fz00_5016;

							BgL_fz00_5016 =
								(((BgL_variablez00_bglt) COBJECT(BgL_vz00_5015))->BgL_valuez00);
							{	/* Stackable/walk.scm 226 */

								{	/* Stackable/walk.scm 228 */
									bool_t BgL_test2563z00_6893;

									if (CBOOL(BgL_escpz00_4701))
										{	/* Stackable/walk.scm 228 */
											BgL_test2563z00_6893 = ((bool_t) 1);
										}
									else
										{	/* Stackable/walk.scm 228 */
											long BgL_arg1949z00_5017;

											{
												BgL_appzf2depthzf2_bglt BgL_auxz00_6896;

												{
													obj_t BgL_auxz00_6897;

													{	/* Stackable/walk.scm 228 */
														BgL_objectz00_bglt BgL_tmpz00_6898;

														BgL_tmpz00_6898 =
															((BgL_objectz00_bglt)
															((BgL_appz00_bglt)
																((BgL_appz00_bglt) BgL_nodez00_4700)));
														BgL_auxz00_6897 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_6898);
													}
													BgL_auxz00_6896 =
														((BgL_appzf2depthzf2_bglt) BgL_auxz00_6897);
												}
												BgL_arg1949z00_5017 =
													(((BgL_appzf2depthzf2_bglt)
														COBJECT(BgL_auxz00_6896))->BgL_depthz00);
											}
											BgL_test2563z00_6893 =
												((long) CINT(BgL_depthz00_4702) < BgL_arg1949z00_5017);
										}
									if (BgL_test2563z00_6893)
										{	/* Stackable/walk.scm 229 */
											bool_t BgL_test2565z00_6907;

											{	/* Stackable/walk.scm 229 */
												bool_t BgL_test2566z00_6908;

												{	/* Stackable/walk.scm 229 */
													obj_t BgL_classz00_5018;

													BgL_classz00_5018 = BGl_globalz00zzast_varz00;
													{	/* Stackable/walk.scm 229 */
														BgL_objectz00_bglt BgL_arg1807z00_5019;

														{	/* Stackable/walk.scm 229 */
															obj_t BgL_tmpz00_6909;

															BgL_tmpz00_6909 =
																((obj_t) ((BgL_objectz00_bglt) BgL_vz00_5015));
															BgL_arg1807z00_5019 =
																(BgL_objectz00_bglt) (BgL_tmpz00_6909);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Stackable/walk.scm 229 */
																long BgL_idxz00_5020;

																BgL_idxz00_5020 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_5019);
																BgL_test2566z00_6908 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_5020 + 2L)) ==
																	BgL_classz00_5018);
															}
														else
															{	/* Stackable/walk.scm 229 */
																bool_t BgL_res2284z00_5023;

																{	/* Stackable/walk.scm 229 */
																	obj_t BgL_oclassz00_5024;

																	{	/* Stackable/walk.scm 229 */
																		obj_t BgL_arg1815z00_5025;
																		long BgL_arg1816z00_5026;

																		BgL_arg1815z00_5025 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Stackable/walk.scm 229 */
																			long BgL_arg1817z00_5027;

																			BgL_arg1817z00_5027 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_5019);
																			BgL_arg1816z00_5026 =
																				(BgL_arg1817z00_5027 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_5024 =
																			VECTOR_REF(BgL_arg1815z00_5025,
																			BgL_arg1816z00_5026);
																	}
																	{	/* Stackable/walk.scm 229 */
																		bool_t BgL__ortest_1115z00_5028;

																		BgL__ortest_1115z00_5028 =
																			(BgL_classz00_5018 == BgL_oclassz00_5024);
																		if (BgL__ortest_1115z00_5028)
																			{	/* Stackable/walk.scm 229 */
																				BgL_res2284z00_5023 =
																					BgL__ortest_1115z00_5028;
																			}
																		else
																			{	/* Stackable/walk.scm 229 */
																				long BgL_odepthz00_5029;

																				{	/* Stackable/walk.scm 229 */
																					obj_t BgL_arg1804z00_5030;

																					BgL_arg1804z00_5030 =
																						(BgL_oclassz00_5024);
																					BgL_odepthz00_5029 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_5030);
																				}
																				if ((2L < BgL_odepthz00_5029))
																					{	/* Stackable/walk.scm 229 */
																						obj_t BgL_arg1802z00_5031;

																						{	/* Stackable/walk.scm 229 */
																							obj_t BgL_arg1803z00_5032;

																							BgL_arg1803z00_5032 =
																								(BgL_oclassz00_5024);
																							BgL_arg1802z00_5031 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_5032, 2L);
																						}
																						BgL_res2284z00_5023 =
																							(BgL_arg1802z00_5031 ==
																							BgL_classz00_5018);
																					}
																				else
																					{	/* Stackable/walk.scm 229 */
																						BgL_res2284z00_5023 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2566z00_6908 = BgL_res2284z00_5023;
															}
													}
												}
												if (BgL_test2566z00_6908)
													{	/* Stackable/walk.scm 229 */
														if (
															((((BgL_globalz00_bglt) COBJECT(
																			((BgL_globalz00_bglt) BgL_vz00_5015)))->
																	BgL_modulez00) == CNST_TABLE_REF(9)))
															{	/* Stackable/walk.scm 231 */
																BgL_test2565z00_6907 =
																	CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt) (
																							(BgL_globalz00_bglt)
																							BgL_vz00_5015))))->BgL_idz00),
																		CNST_TABLE_REF(10)));
															}
														else
															{	/* Stackable/walk.scm 231 */
																BgL_test2565z00_6907 = ((bool_t) 0);
															}
													}
												else
													{	/* Stackable/walk.scm 229 */
														BgL_test2565z00_6907 = ((bool_t) 0);
													}
											}
											if (BgL_test2565z00_6907)
												{	/* Stackable/walk.scm 229 */
													BFALSE;
												}
											else
												{	/* Stackable/walk.scm 229 */
													BGl_escapez12z12zzstackable_walkz00(
														((obj_t)
															((BgL_appz00_bglt) BgL_nodez00_4700)),
														((obj_t) BgL_ctxz00_4703));
												}
										}
									else
										{	/* Stackable/walk.scm 228 */
											BFALSE;
										}
								}
								{	/* Stackable/walk.scm 235 */
									bool_t BgL_test2571z00_6947;

									{	/* Stackable/walk.scm 235 */
										obj_t BgL_classz00_5033;

										BgL_classz00_5033 = BGl_localz00zzast_varz00;
										{	/* Stackable/walk.scm 235 */
											BgL_objectz00_bglt BgL_arg1807z00_5034;

											{	/* Stackable/walk.scm 235 */
												obj_t BgL_tmpz00_6948;

												BgL_tmpz00_6948 =
													((obj_t) ((BgL_objectz00_bglt) BgL_vz00_5015));
												BgL_arg1807z00_5034 =
													(BgL_objectz00_bglt) (BgL_tmpz00_6948);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Stackable/walk.scm 235 */
													long BgL_idxz00_5035;

													BgL_idxz00_5035 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5034);
													BgL_test2571z00_6947 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_5035 + 2L)) == BgL_classz00_5033);
												}
											else
												{	/* Stackable/walk.scm 235 */
													bool_t BgL_res2285z00_5038;

													{	/* Stackable/walk.scm 235 */
														obj_t BgL_oclassz00_5039;

														{	/* Stackable/walk.scm 235 */
															obj_t BgL_arg1815z00_5040;
															long BgL_arg1816z00_5041;

															BgL_arg1815z00_5040 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Stackable/walk.scm 235 */
																long BgL_arg1817z00_5042;

																BgL_arg1817z00_5042 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5034);
																BgL_arg1816z00_5041 =
																	(BgL_arg1817z00_5042 - OBJECT_TYPE);
															}
															BgL_oclassz00_5039 =
																VECTOR_REF(BgL_arg1815z00_5040,
																BgL_arg1816z00_5041);
														}
														{	/* Stackable/walk.scm 235 */
															bool_t BgL__ortest_1115z00_5043;

															BgL__ortest_1115z00_5043 =
																(BgL_classz00_5033 == BgL_oclassz00_5039);
															if (BgL__ortest_1115z00_5043)
																{	/* Stackable/walk.scm 235 */
																	BgL_res2285z00_5038 =
																		BgL__ortest_1115z00_5043;
																}
															else
																{	/* Stackable/walk.scm 235 */
																	long BgL_odepthz00_5044;

																	{	/* Stackable/walk.scm 235 */
																		obj_t BgL_arg1804z00_5045;

																		BgL_arg1804z00_5045 = (BgL_oclassz00_5039);
																		BgL_odepthz00_5044 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_5045);
																	}
																	if ((2L < BgL_odepthz00_5044))
																		{	/* Stackable/walk.scm 235 */
																			obj_t BgL_arg1802z00_5046;

																			{	/* Stackable/walk.scm 235 */
																				obj_t BgL_arg1803z00_5047;

																				BgL_arg1803z00_5047 =
																					(BgL_oclassz00_5039);
																				BgL_arg1802z00_5046 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_5047, 2L);
																			}
																			BgL_res2285z00_5038 =
																				(BgL_arg1802z00_5046 ==
																				BgL_classz00_5033);
																		}
																	else
																		{	/* Stackable/walk.scm 235 */
																			BgL_res2285z00_5038 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2571z00_6947 = BgL_res2285z00_5038;
												}
										}
									}
									if (BgL_test2571z00_6947)
										{	/* Stackable/walk.scm 237 */
											obj_t BgL_g1419z00_5048;

											BgL_g1419z00_5048 =
												(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_nodez00_4700)))->
												BgL_argsz00);
											{
												obj_t BgL_l1417z00_5050;

												BgL_l1417z00_5050 = BgL_g1419z00_5048;
											BgL_zc3z04anonymousza31951ze3z87_5049:
												if (PAIRP(BgL_l1417z00_5050))
													{	/* Stackable/walk.scm 237 */
														{	/* Stackable/walk.scm 237 */
															obj_t BgL_az00_5051;

															BgL_az00_5051 = CAR(BgL_l1417z00_5050);
															BGl_stackablez00zzstackable_walkz00(
																((BgL_nodez00_bglt) BgL_az00_5051),
																((bool_t) 1), 100000L,
																((obj_t) BgL_ctxz00_4703));
														}
														{
															obj_t BgL_l1417z00_6979;

															BgL_l1417z00_6979 = CDR(BgL_l1417z00_5050);
															BgL_l1417z00_5050 = BgL_l1417z00_6979;
															goto BgL_zc3z04anonymousza31951ze3z87_5049;
														}
													}
												else
													{	/* Stackable/walk.scm 237 */
														BgL_tmpz00_6888 = ((bool_t) 1);
													}
											}
										}
									else
										{	/* Stackable/walk.scm 238 */
											bool_t BgL_test2576z00_6981;

											{	/* Stackable/walk.scm 238 */
												bool_t BgL_test2577z00_6982;

												{	/* Stackable/walk.scm 238 */
													obj_t BgL_classz00_5052;

													BgL_classz00_5052 = BGl_sfunz00zzast_varz00;
													{	/* Stackable/walk.scm 238 */
														BgL_objectz00_bglt BgL_arg1807z00_5053;

														{	/* Stackable/walk.scm 238 */
															obj_t BgL_tmpz00_6983;

															BgL_tmpz00_6983 =
																((obj_t) ((BgL_objectz00_bglt) BgL_fz00_5016));
															BgL_arg1807z00_5053 =
																(BgL_objectz00_bglt) (BgL_tmpz00_6983);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Stackable/walk.scm 238 */
																long BgL_idxz00_5054;

																BgL_idxz00_5054 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_5053);
																BgL_test2577z00_6982 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_5054 + 3L)) ==
																	BgL_classz00_5052);
															}
														else
															{	/* Stackable/walk.scm 238 */
																bool_t BgL_res2286z00_5057;

																{	/* Stackable/walk.scm 238 */
																	obj_t BgL_oclassz00_5058;

																	{	/* Stackable/walk.scm 238 */
																		obj_t BgL_arg1815z00_5059;
																		long BgL_arg1816z00_5060;

																		BgL_arg1815z00_5059 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Stackable/walk.scm 238 */
																			long BgL_arg1817z00_5061;

																			BgL_arg1817z00_5061 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_5053);
																			BgL_arg1816z00_5060 =
																				(BgL_arg1817z00_5061 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_5058 =
																			VECTOR_REF(BgL_arg1815z00_5059,
																			BgL_arg1816z00_5060);
																	}
																	{	/* Stackable/walk.scm 238 */
																		bool_t BgL__ortest_1115z00_5062;

																		BgL__ortest_1115z00_5062 =
																			(BgL_classz00_5052 == BgL_oclassz00_5058);
																		if (BgL__ortest_1115z00_5062)
																			{	/* Stackable/walk.scm 238 */
																				BgL_res2286z00_5057 =
																					BgL__ortest_1115z00_5062;
																			}
																		else
																			{	/* Stackable/walk.scm 238 */
																				long BgL_odepthz00_5063;

																				{	/* Stackable/walk.scm 238 */
																					obj_t BgL_arg1804z00_5064;

																					BgL_arg1804z00_5064 =
																						(BgL_oclassz00_5058);
																					BgL_odepthz00_5063 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_5064);
																				}
																				if ((3L < BgL_odepthz00_5063))
																					{	/* Stackable/walk.scm 238 */
																						obj_t BgL_arg1802z00_5065;

																						{	/* Stackable/walk.scm 238 */
																							obj_t BgL_arg1803z00_5066;

																							BgL_arg1803z00_5066 =
																								(BgL_oclassz00_5058);
																							BgL_arg1802z00_5065 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_5066, 3L);
																						}
																						BgL_res2286z00_5057 =
																							(BgL_arg1802z00_5065 ==
																							BgL_classz00_5052);
																					}
																				else
																					{	/* Stackable/walk.scm 238 */
																						BgL_res2286z00_5057 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2577z00_6982 = BgL_res2286z00_5057;
															}
													}
												}
												if (BgL_test2577z00_6982)
													{	/* Stackable/walk.scm 238 */
														if (BGl_importzf3zf3zzstackable_walkz00
															(BgL_vz00_5015))
															{	/* Stackable/walk.scm 238 */
																BgL_test2576z00_6981 = ((bool_t) 0);
															}
														else
															{	/* Stackable/walk.scm 238 */
																BgL_test2576z00_6981 = ((bool_t) 1);
															}
													}
												else
													{	/* Stackable/walk.scm 238 */
														BgL_test2576z00_6981 = ((bool_t) 0);
													}
											}
											if (BgL_test2576z00_6981)
												{	/* Stackable/walk.scm 238 */
													BGl_varzd2stackablezd2zzstackable_walkz00
														(BgL_vz00_5015, ((obj_t) BgL_ctxz00_4703));
													{	/* Stackable/walk.scm 241 */
														obj_t BgL_g1423z00_5067;
														obj_t BgL_g1424z00_5068;

														BgL_g1423z00_5067 =
															(((BgL_sfunz00_bglt) COBJECT(
																	((BgL_sfunz00_bglt) BgL_fz00_5016)))->
															BgL_argsz00);
														BgL_g1424z00_5068 =
															(((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt)
																		BgL_nodez00_4700)))->BgL_argsz00);
														{
															obj_t BgL_ll1420z00_5070;
															obj_t BgL_ll1421z00_5071;

															BgL_ll1420z00_5070 = BgL_g1423z00_5067;
															BgL_ll1421z00_5071 = BgL_g1424z00_5068;
														BgL_zc3z04anonymousza31958ze3z87_5069:
															if (NULLP(BgL_ll1420z00_5070))
																{	/* Stackable/walk.scm 241 */
																	BgL_tmpz00_6888 = ((bool_t) 1);
																}
															else
																{	/* Stackable/walk.scm 241 */
																	{	/* Stackable/walk.scm 242 */
																		obj_t BgL_pz00_5072;
																		obj_t BgL_az00_5073;

																		BgL_pz00_5072 =
																			CAR(((obj_t) BgL_ll1420z00_5070));
																		BgL_az00_5073 =
																			CAR(((obj_t) BgL_ll1421z00_5071));
																		{	/* Stackable/walk.scm 242 */
																			bool_t BgL_test2583z00_7020;

																			{	/* Stackable/walk.scm 242 */
																				obj_t BgL_classz00_5074;

																				BgL_classz00_5074 =
																					BGl_localz00zzast_varz00;
																				if (BGL_OBJECTP(BgL_pz00_5072))
																					{	/* Stackable/walk.scm 242 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_5075;
																						BgL_arg1807z00_5075 =
																							(BgL_objectz00_bglt)
																							(BgL_pz00_5072);
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Stackable/walk.scm 242 */
																								long BgL_idxz00_5076;

																								BgL_idxz00_5076 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_5075);
																								BgL_test2583z00_7020 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_5076 + 2L)) ==
																									BgL_classz00_5074);
																							}
																						else
																							{	/* Stackable/walk.scm 242 */
																								bool_t BgL_res2287z00_5079;

																								{	/* Stackable/walk.scm 242 */
																									obj_t BgL_oclassz00_5080;

																									{	/* Stackable/walk.scm 242 */
																										obj_t BgL_arg1815z00_5081;
																										long BgL_arg1816z00_5082;

																										BgL_arg1815z00_5081 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Stackable/walk.scm 242 */
																											long BgL_arg1817z00_5083;

																											BgL_arg1817z00_5083 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_5075);
																											BgL_arg1816z00_5082 =
																												(BgL_arg1817z00_5083 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_5080 =
																											VECTOR_REF
																											(BgL_arg1815z00_5081,
																											BgL_arg1816z00_5082);
																									}
																									{	/* Stackable/walk.scm 242 */
																										bool_t
																											BgL__ortest_1115z00_5084;
																										BgL__ortest_1115z00_5084 =
																											(BgL_classz00_5074 ==
																											BgL_oclassz00_5080);
																										if (BgL__ortest_1115z00_5084)
																											{	/* Stackable/walk.scm 242 */
																												BgL_res2287z00_5079 =
																													BgL__ortest_1115z00_5084;
																											}
																										else
																											{	/* Stackable/walk.scm 242 */
																												long BgL_odepthz00_5085;

																												{	/* Stackable/walk.scm 242 */
																													obj_t
																														BgL_arg1804z00_5086;
																													BgL_arg1804z00_5086 =
																														(BgL_oclassz00_5080);
																													BgL_odepthz00_5085 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_5086);
																												}
																												if (
																													(2L <
																														BgL_odepthz00_5085))
																													{	/* Stackable/walk.scm 242 */
																														obj_t
																															BgL_arg1802z00_5087;
																														{	/* Stackable/walk.scm 242 */
																															obj_t
																																BgL_arg1803z00_5088;
																															BgL_arg1803z00_5088
																																=
																																(BgL_oclassz00_5080);
																															BgL_arg1802z00_5087
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_5088,
																																2L);
																														}
																														BgL_res2287z00_5079
																															=
																															(BgL_arg1802z00_5087
																															==
																															BgL_classz00_5074);
																													}
																												else
																													{	/* Stackable/walk.scm 242 */
																														BgL_res2287z00_5079
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test2583z00_7020 =
																									BgL_res2287z00_5079;
																							}
																					}
																				else
																					{	/* Stackable/walk.scm 242 */
																						BgL_test2583z00_7020 = ((bool_t) 0);
																					}
																			}
																			if (BgL_test2583z00_7020)
																				{	/* Stackable/walk.scm 244 */
																					bool_t BgL_arg1961z00_5089;

																					if (CBOOL(
																							(((BgL_localz00_bglt) COBJECT(
																										((BgL_localz00_bglt)
																											BgL_pz00_5072)))->
																								BgL_valzd2noescapezd2)))
																						{	/* Stackable/walk.scm 244 */
																							BgL_arg1961z00_5089 =
																								((bool_t) 0);
																						}
																					else
																						{	/* Stackable/walk.scm 244 */
																							BgL_arg1961z00_5089 =
																								((bool_t) 1);
																						}
																					BGl_stackablez00zzstackable_walkz00(
																						((BgL_nodez00_bglt) BgL_az00_5073),
																						BgL_arg1961z00_5089, 100000L,
																						((obj_t) BgL_ctxz00_4703));
																				}
																			else
																				{	/* Stackable/walk.scm 242 */
																					BFALSE;
																				}
																		}
																	}
																	{	/* Stackable/walk.scm 241 */
																		obj_t BgL_arg1964z00_5090;
																		obj_t BgL_arg1965z00_5091;

																		BgL_arg1964z00_5090 =
																			CDR(((obj_t) BgL_ll1420z00_5070));
																		BgL_arg1965z00_5091 =
																			CDR(((obj_t) BgL_ll1421z00_5071));
																		{
																			obj_t BgL_ll1421z00_7055;
																			obj_t BgL_ll1420z00_7054;

																			BgL_ll1420z00_7054 = BgL_arg1964z00_5090;
																			BgL_ll1421z00_7055 = BgL_arg1965z00_5091;
																			BgL_ll1421z00_5071 = BgL_ll1421z00_7055;
																			BgL_ll1420z00_5070 = BgL_ll1420z00_7054;
																			goto
																				BgL_zc3z04anonymousza31958ze3z87_5069;
																		}
																	}
																}
														}
													}
												}
											else
												{	/* Stackable/walk.scm 246 */
													bool_t BgL_test2589z00_7056;

													{	/* Stackable/walk.scm 246 */
														obj_t BgL_classz00_5092;

														BgL_classz00_5092 = BGl_funz00zzast_varz00;
														{	/* Stackable/walk.scm 246 */
															BgL_objectz00_bglt BgL_arg1807z00_5093;

															{	/* Stackable/walk.scm 246 */
																obj_t BgL_tmpz00_7057;

																BgL_tmpz00_7057 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_fz00_5016));
																BgL_arg1807z00_5093 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_7057);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Stackable/walk.scm 246 */
																	long BgL_idxz00_5094;

																	BgL_idxz00_5094 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_5093);
																	BgL_test2589z00_7056 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_5094 + 2L)) ==
																		BgL_classz00_5092);
																}
															else
																{	/* Stackable/walk.scm 246 */
																	bool_t BgL_res2288z00_5097;

																	{	/* Stackable/walk.scm 246 */
																		obj_t BgL_oclassz00_5098;

																		{	/* Stackable/walk.scm 246 */
																			obj_t BgL_arg1815z00_5099;
																			long BgL_arg1816z00_5100;

																			BgL_arg1815z00_5099 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Stackable/walk.scm 246 */
																				long BgL_arg1817z00_5101;

																				BgL_arg1817z00_5101 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_5093);
																				BgL_arg1816z00_5100 =
																					(BgL_arg1817z00_5101 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_5098 =
																				VECTOR_REF(BgL_arg1815z00_5099,
																				BgL_arg1816z00_5100);
																		}
																		{	/* Stackable/walk.scm 246 */
																			bool_t BgL__ortest_1115z00_5102;

																			BgL__ortest_1115z00_5102 =
																				(BgL_classz00_5092 ==
																				BgL_oclassz00_5098);
																			if (BgL__ortest_1115z00_5102)
																				{	/* Stackable/walk.scm 246 */
																					BgL_res2288z00_5097 =
																						BgL__ortest_1115z00_5102;
																				}
																			else
																				{	/* Stackable/walk.scm 246 */
																					long BgL_odepthz00_5103;

																					{	/* Stackable/walk.scm 246 */
																						obj_t BgL_arg1804z00_5104;

																						BgL_arg1804z00_5104 =
																							(BgL_oclassz00_5098);
																						BgL_odepthz00_5103 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_5104);
																					}
																					if ((2L < BgL_odepthz00_5103))
																						{	/* Stackable/walk.scm 246 */
																							obj_t BgL_arg1802z00_5105;

																							{	/* Stackable/walk.scm 246 */
																								obj_t BgL_arg1803z00_5106;

																								BgL_arg1803z00_5106 =
																									(BgL_oclassz00_5098);
																								BgL_arg1802z00_5105 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_5106, 2L);
																							}
																							BgL_res2288z00_5097 =
																								(BgL_arg1802z00_5105 ==
																								BgL_classz00_5092);
																						}
																					else
																						{	/* Stackable/walk.scm 246 */
																							BgL_res2288z00_5097 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2589z00_7056 = BgL_res2288z00_5097;
																}
														}
													}
													if (BgL_test2589z00_7056)
														{	/* Stackable/walk.scm 246 */
															if (
																((((BgL_funz00_bglt) COBJECT(
																				((BgL_funz00_bglt) BgL_fz00_5016)))->
																		BgL_argszd2noescapezd2) ==
																	CNST_TABLE_REF(11)))
																{	/* Stackable/walk.scm 250 */
																	obj_t BgL_g1427z00_5107;

																	BgL_g1427z00_5107 =
																		(((BgL_appz00_bglt) COBJECT(
																				((BgL_appz00_bglt) BgL_nodez00_4700)))->
																		BgL_argsz00);
																	{
																		obj_t BgL_l1425z00_5109;

																		BgL_l1425z00_5109 = BgL_g1427z00_5107;
																	BgL_zc3z04anonymousza31969ze3z87_5108:
																		if (PAIRP(BgL_l1425z00_5109))
																			{	/* Stackable/walk.scm 250 */
																				{	/* Stackable/walk.scm 250 */
																					obj_t BgL_az00_5110;

																					BgL_az00_5110 =
																						CAR(BgL_l1425z00_5109);
																					BGl_stackablez00zzstackable_walkz00(
																						((BgL_nodez00_bglt) BgL_az00_5110),
																						((bool_t) 0), 100000L,
																						((obj_t) BgL_ctxz00_4703));
																				}
																				{
																					obj_t BgL_l1425z00_7093;

																					BgL_l1425z00_7093 =
																						CDR(BgL_l1425z00_5109);
																					BgL_l1425z00_5109 = BgL_l1425z00_7093;
																					goto
																						BgL_zc3z04anonymousza31969ze3z87_5108;
																				}
																			}
																		else
																			{	/* Stackable/walk.scm 250 */
																				BgL_tmpz00_6888 = ((bool_t) 1);
																			}
																	}
																}
															else
																{	/* Stackable/walk.scm 251 */
																	bool_t BgL_test2595z00_7095;

																	if (
																		((((BgL_funz00_bglt) COBJECT(
																						((BgL_funz00_bglt)
																							BgL_fz00_5016)))->
																				BgL_argszd2retescapezd2) ==
																			CNST_TABLE_REF(11)))
																		{	/* Stackable/walk.scm 251 */
																			if (CBOOL(
																					(((BgL_appz00_bglt) COBJECT(
																								((BgL_appz00_bglt)
																									BgL_nodez00_4700)))->
																						BgL_stackablez00)))
																				{	/* Stackable/walk.scm 251 */
																					BgL_argsz00_4919 =
																						(((BgL_appz00_bglt) COBJECT(
																								((BgL_appz00_bglt)
																									BgL_nodez00_4700)))->
																						BgL_argsz00);
																					{
																						obj_t BgL_l1414z00_4921;

																						BgL_l1414z00_4921 =
																							BgL_argsz00_4919;
																					BgL_zc3z04anonymousza32025ze3z87_4920:
																						if (NULLP
																							(BgL_l1414z00_4921))
																							{	/* Stackable/walk.scm 207 */
																								BgL_test2595z00_7095 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Stackable/walk.scm 209 */
																								obj_t BgL_nvz00_4922;

																								{	/* Stackable/walk.scm 209 */
																									obj_t BgL_az00_4923;

																									BgL_az00_4923 =
																										CAR(
																										((obj_t)
																											BgL_l1414z00_4921));
																									{	/* Stackable/walk.scm 209 */
																										bool_t BgL_test2599z00_7109;

																										{	/* Stackable/walk.scm 209 */
																											obj_t BgL_classz00_4924;

																											BgL_classz00_4924 =
																												BGl_appz00zzast_nodez00;
																											if (BGL_OBJECTP
																												(BgL_az00_4923))
																												{	/* Stackable/walk.scm 209 */
																													BgL_objectz00_bglt
																														BgL_arg1807z00_4925;
																													BgL_arg1807z00_4925 =
																														(BgL_objectz00_bglt)
																														(BgL_az00_4923);
																													if (BGL_CONDEXPAND_ISA_ARCH64())
																														{	/* Stackable/walk.scm 209 */
																															long
																																BgL_idxz00_4926;
																															BgL_idxz00_4926 =
																																BGL_OBJECT_INHERITANCE_NUM
																																(BgL_arg1807z00_4925);
																															BgL_test2599z00_7109
																																=
																																(VECTOR_REF
																																(BGl_za2inheritancesza2z00zz__objectz00,
																																	(BgL_idxz00_4926
																																		+ 3L)) ==
																																BgL_classz00_4924);
																														}
																													else
																														{	/* Stackable/walk.scm 209 */
																															bool_t
																																BgL_res2278z00_4929;
																															{	/* Stackable/walk.scm 209 */
																																obj_t
																																	BgL_oclassz00_4930;
																																{	/* Stackable/walk.scm 209 */
																																	obj_t
																																		BgL_arg1815z00_4931;
																																	long
																																		BgL_arg1816z00_4932;
																																	BgL_arg1815z00_4931
																																		=
																																		(BGl_za2classesza2z00zz__objectz00);
																																	{	/* Stackable/walk.scm 209 */
																																		long
																																			BgL_arg1817z00_4933;
																																		BgL_arg1817z00_4933
																																			=
																																			BGL_OBJECT_CLASS_NUM
																																			(BgL_arg1807z00_4925);
																																		BgL_arg1816z00_4932
																																			=
																																			(BgL_arg1817z00_4933
																																			-
																																			OBJECT_TYPE);
																																	}
																																	BgL_oclassz00_4930
																																		=
																																		VECTOR_REF
																																		(BgL_arg1815z00_4931,
																																		BgL_arg1816z00_4932);
																																}
																																{	/* Stackable/walk.scm 209 */
																																	bool_t
																																		BgL__ortest_1115z00_4934;
																																	BgL__ortest_1115z00_4934
																																		=
																																		(BgL_classz00_4924
																																		==
																																		BgL_oclassz00_4930);
																																	if (BgL__ortest_1115z00_4934)
																																		{	/* Stackable/walk.scm 209 */
																																			BgL_res2278z00_4929
																																				=
																																				BgL__ortest_1115z00_4934;
																																		}
																																	else
																																		{	/* Stackable/walk.scm 209 */
																																			long
																																				BgL_odepthz00_4935;
																																			{	/* Stackable/walk.scm 209 */
																																				obj_t
																																					BgL_arg1804z00_4936;
																																				BgL_arg1804z00_4936
																																					=
																																					(BgL_oclassz00_4930);
																																				BgL_odepthz00_4935
																																					=
																																					BGL_CLASS_DEPTH
																																					(BgL_arg1804z00_4936);
																																			}
																																			if (
																																				(3L <
																																					BgL_odepthz00_4935))
																																				{	/* Stackable/walk.scm 209 */
																																					obj_t
																																						BgL_arg1802z00_4937;
																																					{	/* Stackable/walk.scm 209 */
																																						obj_t
																																							BgL_arg1803z00_4938;
																																						BgL_arg1803z00_4938
																																							=
																																							(BgL_oclassz00_4930);
																																						BgL_arg1802z00_4937
																																							=
																																							BGL_CLASS_ANCESTORS_REF
																																							(BgL_arg1803z00_4938,
																																							3L);
																																					}
																																					BgL_res2278z00_4929
																																						=
																																						(BgL_arg1802z00_4937
																																						==
																																						BgL_classz00_4924);
																																				}
																																			else
																																				{	/* Stackable/walk.scm 209 */
																																					BgL_res2278z00_4929
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																															}
																															BgL_test2599z00_7109
																																=
																																BgL_res2278z00_4929;
																														}
																												}
																											else
																												{	/* Stackable/walk.scm 209 */
																													BgL_test2599z00_7109 =
																														((bool_t) 0);
																												}
																										}
																										if (BgL_test2599z00_7109)
																											{	/* Stackable/walk.scm 209 */
																												BgL_nvz00_4922 =
																													(((BgL_appz00_bglt)
																														COBJECT((
																																(BgL_appz00_bglt)
																																BgL_az00_4923)))->
																													BgL_stackablez00);
																											}
																										else
																											{	/* Stackable/walk.scm 211 */
																												bool_t
																													BgL_test2604z00_7134;
																												{	/* Stackable/walk.scm 211 */
																													obj_t
																														BgL_classz00_4939;
																													BgL_classz00_4939 =
																														BGl_makezd2boxzd2zzast_nodez00;
																													if (BGL_OBJECTP
																														(BgL_az00_4923))
																														{	/* Stackable/walk.scm 211 */
																															BgL_objectz00_bglt
																																BgL_arg1807z00_4940;
																															BgL_arg1807z00_4940
																																=
																																(BgL_objectz00_bglt)
																																(BgL_az00_4923);
																															if (BGL_CONDEXPAND_ISA_ARCH64())
																																{	/* Stackable/walk.scm 211 */
																																	long
																																		BgL_idxz00_4941;
																																	BgL_idxz00_4941
																																		=
																																		BGL_OBJECT_INHERITANCE_NUM
																																		(BgL_arg1807z00_4940);
																																	BgL_test2604z00_7134
																																		=
																																		(VECTOR_REF
																																		(BGl_za2inheritancesza2z00zz__objectz00,
																																			(BgL_idxz00_4941
																																				+
																																				3L)) ==
																																		BgL_classz00_4939);
																																}
																															else
																																{	/* Stackable/walk.scm 211 */
																																	bool_t
																																		BgL_res2279z00_4944;
																																	{	/* Stackable/walk.scm 211 */
																																		obj_t
																																			BgL_oclassz00_4945;
																																		{	/* Stackable/walk.scm 211 */
																																			obj_t
																																				BgL_arg1815z00_4946;
																																			long
																																				BgL_arg1816z00_4947;
																																			BgL_arg1815z00_4946
																																				=
																																				(BGl_za2classesza2z00zz__objectz00);
																																			{	/* Stackable/walk.scm 211 */
																																				long
																																					BgL_arg1817z00_4948;
																																				BgL_arg1817z00_4948
																																					=
																																					BGL_OBJECT_CLASS_NUM
																																					(BgL_arg1807z00_4940);
																																				BgL_arg1816z00_4947
																																					=
																																					(BgL_arg1817z00_4948
																																					-
																																					OBJECT_TYPE);
																																			}
																																			BgL_oclassz00_4945
																																				=
																																				VECTOR_REF
																																				(BgL_arg1815z00_4946,
																																				BgL_arg1816z00_4947);
																																		}
																																		{	/* Stackable/walk.scm 211 */
																																			bool_t
																																				BgL__ortest_1115z00_4949;
																																			BgL__ortest_1115z00_4949
																																				=
																																				(BgL_classz00_4939
																																				==
																																				BgL_oclassz00_4945);
																																			if (BgL__ortest_1115z00_4949)
																																				{	/* Stackable/walk.scm 211 */
																																					BgL_res2279z00_4944
																																						=
																																						BgL__ortest_1115z00_4949;
																																				}
																																			else
																																				{	/* Stackable/walk.scm 211 */
																																					long
																																						BgL_odepthz00_4950;
																																					{	/* Stackable/walk.scm 211 */
																																						obj_t
																																							BgL_arg1804z00_4951;
																																						BgL_arg1804z00_4951
																																							=
																																							(BgL_oclassz00_4945);
																																						BgL_odepthz00_4950
																																							=
																																							BGL_CLASS_DEPTH
																																							(BgL_arg1804z00_4951);
																																					}
																																					if (
																																						(3L
																																							<
																																							BgL_odepthz00_4950))
																																						{	/* Stackable/walk.scm 211 */
																																							obj_t
																																								BgL_arg1802z00_4952;
																																							{	/* Stackable/walk.scm 211 */
																																								obj_t
																																									BgL_arg1803z00_4953;
																																								BgL_arg1803z00_4953
																																									=
																																									(BgL_oclassz00_4945);
																																								BgL_arg1802z00_4952
																																									=
																																									BGL_CLASS_ANCESTORS_REF
																																									(BgL_arg1803z00_4953,
																																									3L);
																																							}
																																							BgL_res2279z00_4944
																																								=
																																								(BgL_arg1802z00_4952
																																								==
																																								BgL_classz00_4939);
																																						}
																																					else
																																						{	/* Stackable/walk.scm 211 */
																																							BgL_res2279z00_4944
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																		}
																																	}
																																	BgL_test2604z00_7134
																																		=
																																		BgL_res2279z00_4944;
																																}
																														}
																													else
																														{	/* Stackable/walk.scm 211 */
																															BgL_test2604z00_7134
																																= ((bool_t) 0);
																														}
																												}
																												if (BgL_test2604z00_7134)
																													{	/* Stackable/walk.scm 211 */
																														BgL_nvz00_4922 =
																															(((BgL_makezd2boxzd2_bglt) COBJECT(((BgL_makezd2boxzd2_bglt) BgL_az00_4923)))->BgL_stackablez00);
																													}
																												else
																													{	/* Stackable/walk.scm 213 */
																														bool_t
																															BgL_test2609z00_7159;
																														{	/* Stackable/walk.scm 213 */
																															obj_t
																																BgL_classz00_4954;
																															BgL_classz00_4954
																																=
																																BGl_varz00zzast_nodez00;
																															if (BGL_OBJECTP
																																(BgL_az00_4923))
																																{	/* Stackable/walk.scm 213 */
																																	BgL_objectz00_bglt
																																		BgL_arg1807z00_4955;
																																	BgL_arg1807z00_4955
																																		=
																																		(BgL_objectz00_bglt)
																																		(BgL_az00_4923);
																																	if (BGL_CONDEXPAND_ISA_ARCH64())
																																		{	/* Stackable/walk.scm 213 */
																																			long
																																				BgL_idxz00_4956;
																																			BgL_idxz00_4956
																																				=
																																				BGL_OBJECT_INHERITANCE_NUM
																																				(BgL_arg1807z00_4955);
																																			BgL_test2609z00_7159
																																				=
																																				(VECTOR_REF
																																				(BGl_za2inheritancesza2z00zz__objectz00,
																																					(BgL_idxz00_4956
																																						+
																																						2L))
																																				==
																																				BgL_classz00_4954);
																																		}
																																	else
																																		{	/* Stackable/walk.scm 213 */
																																			bool_t
																																				BgL_res2280z00_4959;
																																			{	/* Stackable/walk.scm 213 */
																																				obj_t
																																					BgL_oclassz00_4960;
																																				{	/* Stackable/walk.scm 213 */
																																					obj_t
																																						BgL_arg1815z00_4961;
																																					long
																																						BgL_arg1816z00_4962;
																																					BgL_arg1815z00_4961
																																						=
																																						(BGl_za2classesza2z00zz__objectz00);
																																					{	/* Stackable/walk.scm 213 */
																																						long
																																							BgL_arg1817z00_4963;
																																						BgL_arg1817z00_4963
																																							=
																																							BGL_OBJECT_CLASS_NUM
																																							(BgL_arg1807z00_4955);
																																						BgL_arg1816z00_4962
																																							=
																																							(BgL_arg1817z00_4963
																																							-
																																							OBJECT_TYPE);
																																					}
																																					BgL_oclassz00_4960
																																						=
																																						VECTOR_REF
																																						(BgL_arg1815z00_4961,
																																						BgL_arg1816z00_4962);
																																				}
																																				{	/* Stackable/walk.scm 213 */
																																					bool_t
																																						BgL__ortest_1115z00_4964;
																																					BgL__ortest_1115z00_4964
																																						=
																																						(BgL_classz00_4954
																																						==
																																						BgL_oclassz00_4960);
																																					if (BgL__ortest_1115z00_4964)
																																						{	/* Stackable/walk.scm 213 */
																																							BgL_res2280z00_4959
																																								=
																																								BgL__ortest_1115z00_4964;
																																						}
																																					else
																																						{	/* Stackable/walk.scm 213 */
																																							long
																																								BgL_odepthz00_4965;
																																							{	/* Stackable/walk.scm 213 */
																																								obj_t
																																									BgL_arg1804z00_4966;
																																								BgL_arg1804z00_4966
																																									=
																																									(BgL_oclassz00_4960);
																																								BgL_odepthz00_4965
																																									=
																																									BGL_CLASS_DEPTH
																																									(BgL_arg1804z00_4966);
																																							}
																																							if ((2L < BgL_odepthz00_4965))
																																								{	/* Stackable/walk.scm 213 */
																																									obj_t
																																										BgL_arg1802z00_4967;
																																									{	/* Stackable/walk.scm 213 */
																																										obj_t
																																											BgL_arg1803z00_4968;
																																										BgL_arg1803z00_4968
																																											=
																																											(BgL_oclassz00_4960);
																																										BgL_arg1802z00_4967
																																											=
																																											BGL_CLASS_ANCESTORS_REF
																																											(BgL_arg1803z00_4968,
																																											2L);
																																									}
																																									BgL_res2280z00_4959
																																										=
																																										(BgL_arg1802z00_4967
																																										==
																																										BgL_classz00_4954);
																																								}
																																							else
																																								{	/* Stackable/walk.scm 213 */
																																									BgL_res2280z00_4959
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																				}
																																			}
																																			BgL_test2609z00_7159
																																				=
																																				BgL_res2280z00_4959;
																																		}
																																}
																															else
																																{	/* Stackable/walk.scm 213 */
																																	BgL_test2609z00_7159
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																														if (BgL_test2609z00_7159)
																															{	/* Stackable/walk.scm 215 */
																																bool_t
																																	BgL_test2614z00_7182;
																																{	/* Stackable/walk.scm 215 */
																																	BgL_variablez00_bglt
																																		BgL_arg2033z00_4969;
																																	BgL_arg2033z00_4969
																																		=
																																		(((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt) BgL_az00_4923)))->BgL_variablez00);
																																	{	/* Stackable/walk.scm 215 */
																																		obj_t
																																			BgL_classz00_4970;
																																		BgL_classz00_4970
																																			=
																																			BGl_localz00zzast_varz00;
																																		{	/* Stackable/walk.scm 215 */
																																			BgL_objectz00_bglt
																																				BgL_arg1807z00_4971;
																																			{	/* Stackable/walk.scm 215 */
																																				obj_t
																																					BgL_tmpz00_7185;
																																				BgL_tmpz00_7185
																																					=
																																					(
																																					(obj_t)
																																					((BgL_objectz00_bglt) BgL_arg2033z00_4969));
																																				BgL_arg1807z00_4971
																																					=
																																					(BgL_objectz00_bglt)
																																					(BgL_tmpz00_7185);
																																			}
																																			if (BGL_CONDEXPAND_ISA_ARCH64())
																																				{	/* Stackable/walk.scm 215 */
																																					long
																																						BgL_idxz00_4972;
																																					BgL_idxz00_4972
																																						=
																																						BGL_OBJECT_INHERITANCE_NUM
																																						(BgL_arg1807z00_4971);
																																					BgL_test2614z00_7182
																																						=
																																						(VECTOR_REF
																																						(BGl_za2inheritancesza2z00zz__objectz00,
																																							(BgL_idxz00_4972
																																								+
																																								2L))
																																						==
																																						BgL_classz00_4970);
																																				}
																																			else
																																				{	/* Stackable/walk.scm 215 */
																																					bool_t
																																						BgL_res2281z00_4975;
																																					{	/* Stackable/walk.scm 215 */
																																						obj_t
																																							BgL_oclassz00_4976;
																																						{	/* Stackable/walk.scm 215 */
																																							obj_t
																																								BgL_arg1815z00_4977;
																																							long
																																								BgL_arg1816z00_4978;
																																							BgL_arg1815z00_4977
																																								=
																																								(BGl_za2classesza2z00zz__objectz00);
																																							{	/* Stackable/walk.scm 215 */
																																								long
																																									BgL_arg1817z00_4979;
																																								BgL_arg1817z00_4979
																																									=
																																									BGL_OBJECT_CLASS_NUM
																																									(BgL_arg1807z00_4971);
																																								BgL_arg1816z00_4978
																																									=
																																									(BgL_arg1817z00_4979
																																									-
																																									OBJECT_TYPE);
																																							}
																																							BgL_oclassz00_4976
																																								=
																																								VECTOR_REF
																																								(BgL_arg1815z00_4977,
																																								BgL_arg1816z00_4978);
																																						}
																																						{	/* Stackable/walk.scm 215 */
																																							bool_t
																																								BgL__ortest_1115z00_4980;
																																							BgL__ortest_1115z00_4980
																																								=
																																								(BgL_classz00_4970
																																								==
																																								BgL_oclassz00_4976);
																																							if (BgL__ortest_1115z00_4980)
																																								{	/* Stackable/walk.scm 215 */
																																									BgL_res2281z00_4975
																																										=
																																										BgL__ortest_1115z00_4980;
																																								}
																																							else
																																								{	/* Stackable/walk.scm 215 */
																																									long
																																										BgL_odepthz00_4981;
																																									{	/* Stackable/walk.scm 215 */
																																										obj_t
																																											BgL_arg1804z00_4982;
																																										BgL_arg1804z00_4982
																																											=
																																											(BgL_oclassz00_4976);
																																										BgL_odepthz00_4981
																																											=
																																											BGL_CLASS_DEPTH
																																											(BgL_arg1804z00_4982);
																																									}
																																									if ((2L < BgL_odepthz00_4981))
																																										{	/* Stackable/walk.scm 215 */
																																											obj_t
																																												BgL_arg1802z00_4983;
																																											{	/* Stackable/walk.scm 215 */
																																												obj_t
																																													BgL_arg1803z00_4984;
																																												BgL_arg1803z00_4984
																																													=
																																													(BgL_oclassz00_4976);
																																												BgL_arg1802z00_4983
																																													=
																																													BGL_CLASS_ANCESTORS_REF
																																													(BgL_arg1803z00_4984,
																																													2L);
																																											}
																																											BgL_res2281z00_4975
																																												=
																																												(BgL_arg1802z00_4983
																																												==
																																												BgL_classz00_4970);
																																										}
																																									else
																																										{	/* Stackable/walk.scm 215 */
																																											BgL_res2281z00_4975
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																						}
																																					}
																																					BgL_test2614z00_7182
																																						=
																																						BgL_res2281z00_4975;
																																				}
																																		}
																																	}
																																}
																																if (BgL_test2614z00_7182)
																																	{	/* Stackable/walk.scm 215 */
																																		BgL_nvz00_4922
																																			=
																																			(((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) (((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt) BgL_az00_4923)))->BgL_variablez00))))->BgL_valzd2noescapezd2);
																																	}
																																else
																																	{	/* Stackable/walk.scm 215 */
																																		BgL_nvz00_4922
																																			= BFALSE;
																																	}
																															}
																														else
																															{	/* Stackable/walk.scm 218 */
																																bool_t
																																	BgL_test2618z00_7212;
																																{	/* Stackable/walk.scm 218 */
																																	obj_t
																																		BgL_classz00_4985;
																																	BgL_classz00_4985
																																		=
																																		BGl_atomz00zzast_nodez00;
																																	if (BGL_OBJECTP(BgL_az00_4923))
																																		{	/* Stackable/walk.scm 218 */
																																			BgL_objectz00_bglt
																																				BgL_arg1807z00_4986;
																																			BgL_arg1807z00_4986
																																				=
																																				(BgL_objectz00_bglt)
																																				(BgL_az00_4923);
																																			if (BGL_CONDEXPAND_ISA_ARCH64())
																																				{	/* Stackable/walk.scm 218 */
																																					long
																																						BgL_idxz00_4987;
																																					BgL_idxz00_4987
																																						=
																																						BGL_OBJECT_INHERITANCE_NUM
																																						(BgL_arg1807z00_4986);
																																					BgL_test2618z00_7212
																																						=
																																						(VECTOR_REF
																																						(BGl_za2inheritancesza2z00zz__objectz00,
																																							(BgL_idxz00_4987
																																								+
																																								2L))
																																						==
																																						BgL_classz00_4985);
																																				}
																																			else
																																				{	/* Stackable/walk.scm 218 */
																																					bool_t
																																						BgL_res2282z00_4990;
																																					{	/* Stackable/walk.scm 218 */
																																						obj_t
																																							BgL_oclassz00_4991;
																																						{	/* Stackable/walk.scm 218 */
																																							obj_t
																																								BgL_arg1815z00_4992;
																																							long
																																								BgL_arg1816z00_4993;
																																							BgL_arg1815z00_4992
																																								=
																																								(BGl_za2classesza2z00zz__objectz00);
																																							{	/* Stackable/walk.scm 218 */
																																								long
																																									BgL_arg1817z00_4994;
																																								BgL_arg1817z00_4994
																																									=
																																									BGL_OBJECT_CLASS_NUM
																																									(BgL_arg1807z00_4986);
																																								BgL_arg1816z00_4993
																																									=
																																									(BgL_arg1817z00_4994
																																									-
																																									OBJECT_TYPE);
																																							}
																																							BgL_oclassz00_4991
																																								=
																																								VECTOR_REF
																																								(BgL_arg1815z00_4992,
																																								BgL_arg1816z00_4993);
																																						}
																																						{	/* Stackable/walk.scm 218 */
																																							bool_t
																																								BgL__ortest_1115z00_4995;
																																							BgL__ortest_1115z00_4995
																																								=
																																								(BgL_classz00_4985
																																								==
																																								BgL_oclassz00_4991);
																																							if (BgL__ortest_1115z00_4995)
																																								{	/* Stackable/walk.scm 218 */
																																									BgL_res2282z00_4990
																																										=
																																										BgL__ortest_1115z00_4995;
																																								}
																																							else
																																								{	/* Stackable/walk.scm 218 */
																																									long
																																										BgL_odepthz00_4996;
																																									{	/* Stackable/walk.scm 218 */
																																										obj_t
																																											BgL_arg1804z00_4997;
																																										BgL_arg1804z00_4997
																																											=
																																											(BgL_oclassz00_4991);
																																										BgL_odepthz00_4996
																																											=
																																											BGL_CLASS_DEPTH
																																											(BgL_arg1804z00_4997);
																																									}
																																									if ((2L < BgL_odepthz00_4996))
																																										{	/* Stackable/walk.scm 218 */
																																											obj_t
																																												BgL_arg1802z00_4998;
																																											{	/* Stackable/walk.scm 218 */
																																												obj_t
																																													BgL_arg1803z00_4999;
																																												BgL_arg1803z00_4999
																																													=
																																													(BgL_oclassz00_4991);
																																												BgL_arg1802z00_4998
																																													=
																																													BGL_CLASS_ANCESTORS_REF
																																													(BgL_arg1803z00_4999,
																																													2L);
																																											}
																																											BgL_res2282z00_4990
																																												=
																																												(BgL_arg1802z00_4998
																																												==
																																												BgL_classz00_4985);
																																										}
																																									else
																																										{	/* Stackable/walk.scm 218 */
																																											BgL_res2282z00_4990
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																						}
																																					}
																																					BgL_test2618z00_7212
																																						=
																																						BgL_res2282z00_4990;
																																				}
																																		}
																																	else
																																		{	/* Stackable/walk.scm 218 */
																																			BgL_test2618z00_7212
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																																if (BgL_test2618z00_7212)
																																	{	/* Stackable/walk.scm 218 */
																																		BgL_nvz00_4922
																																			= BTRUE;
																																	}
																																else
																																	{	/* Stackable/walk.scm 218 */
																																		obj_t
																																			BgL_classz00_5000;
																																		BgL_classz00_5000
																																			=
																																			BGl_kwotez00zzast_nodez00;
																																		if (BGL_OBJECTP(BgL_az00_4923))
																																			{	/* Stackable/walk.scm 218 */
																																				BgL_objectz00_bglt
																																					BgL_arg1807z00_5001;
																																				BgL_arg1807z00_5001
																																					=
																																					(BgL_objectz00_bglt)
																																					(BgL_az00_4923);
																																				if (BGL_CONDEXPAND_ISA_ARCH64())
																																					{	/* Stackable/walk.scm 218 */
																																						long
																																							BgL_idxz00_5002;
																																						BgL_idxz00_5002
																																							=
																																							BGL_OBJECT_INHERITANCE_NUM
																																							(BgL_arg1807z00_5001);
																																						{	/* Stackable/walk.scm 218 */
																																							obj_t
																																								BgL_arg1800z00_5003;
																																							BgL_arg1800z00_5003
																																								=
																																								VECTOR_REF
																																								(BGl_za2inheritancesza2z00zz__objectz00,
																																								(BgL_idxz00_5002
																																									+
																																									2L));
																																							BgL_nvz00_4922
																																								=
																																								BBOOL
																																								(
																																								(BgL_arg1800z00_5003
																																									==
																																									BgL_classz00_5000));
																																					}}
																																				else
																																					{	/* Stackable/walk.scm 218 */
																																						bool_t
																																							BgL_res2283z00_5005;
																																						{	/* Stackable/walk.scm 218 */
																																							obj_t
																																								BgL_oclassz00_5006;
																																							{	/* Stackable/walk.scm 218 */
																																								obj_t
																																									BgL_arg1815z00_5007;
																																								long
																																									BgL_arg1816z00_5008;
																																								BgL_arg1815z00_5007
																																									=
																																									(BGl_za2classesza2z00zz__objectz00);
																																								{	/* Stackable/walk.scm 218 */
																																									long
																																										BgL_arg1817z00_5009;
																																									BgL_arg1817z00_5009
																																										=
																																										BGL_OBJECT_CLASS_NUM
																																										(BgL_arg1807z00_5001);
																																									BgL_arg1816z00_5008
																																										=
																																										(BgL_arg1817z00_5009
																																										-
																																										OBJECT_TYPE);
																																								}
																																								BgL_oclassz00_5006
																																									=
																																									VECTOR_REF
																																									(BgL_arg1815z00_5007,
																																									BgL_arg1816z00_5008);
																																							}
																																							{	/* Stackable/walk.scm 218 */
																																								bool_t
																																									BgL__ortest_1115z00_5010;
																																								BgL__ortest_1115z00_5010
																																									=
																																									(BgL_classz00_5000
																																									==
																																									BgL_oclassz00_5006);
																																								if (BgL__ortest_1115z00_5010)
																																									{	/* Stackable/walk.scm 218 */
																																										BgL_res2283z00_5005
																																											=
																																											BgL__ortest_1115z00_5010;
																																									}
																																								else
																																									{	/* Stackable/walk.scm 218 */
																																										long
																																											BgL_odepthz00_5011;
																																										{	/* Stackable/walk.scm 218 */
																																											obj_t
																																												BgL_arg1804z00_5012;
																																											BgL_arg1804z00_5012
																																												=
																																												(BgL_oclassz00_5006);
																																											BgL_odepthz00_5011
																																												=
																																												BGL_CLASS_DEPTH
																																												(BgL_arg1804z00_5012);
																																										}
																																										if ((2L < BgL_odepthz00_5011))
																																											{	/* Stackable/walk.scm 218 */
																																												obj_t
																																													BgL_arg1802z00_5013;
																																												{	/* Stackable/walk.scm 218 */
																																													obj_t
																																														BgL_arg1803z00_5014;
																																													BgL_arg1803z00_5014
																																														=
																																														(BgL_oclassz00_5006);
																																													BgL_arg1802z00_5013
																																														=
																																														BGL_CLASS_ANCESTORS_REF
																																														(BgL_arg1803z00_5014,
																																														2L);
																																												}
																																												BgL_res2283z00_5005
																																													=
																																													(BgL_arg1802z00_5013
																																													==
																																													BgL_classz00_5000);
																																											}
																																										else
																																											{	/* Stackable/walk.scm 218 */
																																												BgL_res2283z00_5005
																																													=
																																													(
																																													(bool_t)
																																													0);
																																											}
																																									}
																																							}
																																						}
																																						BgL_nvz00_4922
																																							=
																																							BBOOL
																																							(BgL_res2283z00_5005);
																																					}
																																			}
																																		else
																																			{	/* Stackable/walk.scm 218 */
																																				BgL_nvz00_4922
																																					=
																																					BFALSE;
																																			}
																																	}
																															}
																													}
																											}
																									}
																								}
																								if (CBOOL(BgL_nvz00_4922))
																									{
																										obj_t BgL_l1414z00_7261;

																										BgL_l1414z00_7261 =
																											CDR(
																											((obj_t)
																												BgL_l1414z00_4921));
																										BgL_l1414z00_4921 =
																											BgL_l1414z00_7261;
																										goto
																											BgL_zc3z04anonymousza32025ze3z87_4920;
																									}
																								else
																									{	/* Stackable/walk.scm 207 */
																										BgL_test2595z00_7095 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																			else
																				{	/* Stackable/walk.scm 251 */
																					BgL_test2595z00_7095 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Stackable/walk.scm 251 */
																			BgL_test2595z00_7095 = ((bool_t) 0);
																		}
																	if (BgL_test2595z00_7095)
																		{	/* Stackable/walk.scm 252 */
																			obj_t BgL_g1430z00_5111;

																			BgL_g1430z00_5111 =
																				(((BgL_appz00_bglt) COBJECT(
																						((BgL_appz00_bglt)
																							BgL_nodez00_4700)))->BgL_argsz00);
																			{
																				obj_t BgL_l1428z00_5113;

																				BgL_l1428z00_5113 = BgL_g1430z00_5111;
																			BgL_zc3z04anonymousza31979ze3z87_5112:
																				if (PAIRP(BgL_l1428z00_5113))
																					{	/* Stackable/walk.scm 252 */
																						{	/* Stackable/walk.scm 252 */
																							obj_t BgL_az00_5114;

																							BgL_az00_5114 =
																								CAR(BgL_l1428z00_5113);
																							BGl_stackablez00zzstackable_walkz00
																								(((BgL_nodez00_bglt)
																									BgL_az00_5114), ((bool_t) 0),
																								(long) CINT(BgL_depthz00_4702),
																								((obj_t) BgL_ctxz00_4703));
																						}
																						{
																							obj_t BgL_l1428z00_7275;

																							BgL_l1428z00_7275 =
																								CDR(BgL_l1428z00_5113);
																							BgL_l1428z00_5113 =
																								BgL_l1428z00_7275;
																							goto
																								BgL_zc3z04anonymousza31979ze3z87_5112;
																						}
																					}
																				else
																					{	/* Stackable/walk.scm 252 */
																						BgL_tmpz00_6888 = ((bool_t) 1);
																					}
																			}
																		}
																	else
																		{	/* Stackable/walk.scm 253 */
																			bool_t BgL_test2629z00_7277;

																			{	/* Stackable/walk.scm 253 */
																				bool_t BgL_test2630z00_7278;

																				{	/* Stackable/walk.scm 253 */
																					obj_t BgL_tmpz00_7279;

																					BgL_tmpz00_7279 =
																						(((BgL_funz00_bglt) COBJECT(
																								((BgL_funz00_bglt)
																									BgL_fz00_5016)))->
																						BgL_argszd2noescapezd2);
																					BgL_test2630z00_7278 =
																						PAIRP(BgL_tmpz00_7279);
																				}
																				if (BgL_test2630z00_7278)
																					{	/* Stackable/walk.scm 253 */
																						BgL_test2629z00_7277 = ((bool_t) 1);
																					}
																				else
																					{	/* Stackable/walk.scm 253 */
																						obj_t BgL_tmpz00_7283;

																						BgL_tmpz00_7283 =
																							(((BgL_funz00_bglt) COBJECT(
																									((BgL_funz00_bglt)
																										BgL_fz00_5016)))->
																							BgL_argszd2retescapezd2);
																						BgL_test2629z00_7277 =
																							PAIRP(BgL_tmpz00_7283);
																					}
																			}
																			if (BgL_test2629z00_7277)
																				{
																					long BgL_iz00_5116;
																					obj_t BgL_argsz00_5117;

																					BgL_iz00_5116 = 0L;
																					BgL_argsz00_5117 =
																						(((BgL_appz00_bglt) COBJECT(
																								((BgL_appz00_bglt)
																									BgL_nodez00_4700)))->
																						BgL_argsz00);
																				BgL_loopz00_5115:
																					if (PAIRP(BgL_argsz00_5117))
																						{	/* Stackable/walk.scm 256 */
																							if (CBOOL
																								(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																									(BINT(BgL_iz00_5116),
																										(((BgL_funz00_bglt)
																												COBJECT((
																														(BgL_funz00_bglt)
																														BgL_fz00_5016)))->
																											BgL_argszd2noescapezd2))))
																								{	/* Stackable/walk.scm 259 */
																									obj_t BgL_arg1992z00_5118;

																									BgL_arg1992z00_5118 =
																										CAR(BgL_argsz00_5117);
																									BGl_stackablez00zzstackable_walkz00
																										(((BgL_nodez00_bglt)
																											BgL_arg1992z00_5118),
																										((bool_t) 0), 100000L,
																										((obj_t) BgL_ctxz00_4703));
																								}
																							else
																								{	/* Stackable/walk.scm 260 */
																									bool_t BgL_test2633z00_7299;

																									if (CBOOL
																										(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																											(BINT(BgL_iz00_5116),
																												(((BgL_funz00_bglt)
																														COBJECT((
																																(BgL_funz00_bglt)
																																BgL_fz00_5016)))->
																													BgL_argszd2retescapezd2))))
																										{	/* Stackable/walk.scm 260 */
																											BgL_test2633z00_7299 =
																												CBOOL(
																												(((BgL_appz00_bglt)
																														COBJECT((
																																(BgL_appz00_bglt)
																																BgL_nodez00_4700)))->
																													BgL_stackablez00));
																										}
																									else
																										{	/* Stackable/walk.scm 260 */
																											BgL_test2633z00_7299 =
																												((bool_t) 0);
																										}
																									if (BgL_test2633z00_7299)
																										{	/* Stackable/walk.scm 261 */
																											obj_t BgL_arg1998z00_5119;
																											bool_t
																												BgL_arg1999z00_5120;
																											BgL_arg1998z00_5119 =
																												CAR(BgL_argsz00_5117);
																											if (CBOOL(((
																															(BgL_appz00_bglt)
																															COBJECT((
																																	(BgL_appz00_bglt)
																																	BgL_nodez00_4700)))->
																														BgL_stackablez00)))
																												{	/* Stackable/walk.scm 261 */
																													BgL_arg1999z00_5120 =
																														((bool_t) 0);
																												}
																											else
																												{	/* Stackable/walk.scm 261 */
																													BgL_arg1999z00_5120 =
																														((bool_t) 1);
																												}
																											BGl_stackablez00zzstackable_walkz00
																												(((BgL_nodez00_bglt)
																													BgL_arg1998z00_5119),
																												BgL_arg1999z00_5120,
																												(long)
																												CINT(BgL_depthz00_4702),
																												((obj_t)
																													BgL_ctxz00_4703));
																										}
																									else
																										{	/* Stackable/walk.scm 263 */
																											obj_t BgL_arg2001z00_5121;

																											BgL_arg2001z00_5121 =
																												CAR(BgL_argsz00_5117);
																											BGl_stackablez00zzstackable_walkz00
																												(((BgL_nodez00_bglt)
																													BgL_arg2001z00_5121),
																												((bool_t) 1),
																												(long)
																												CINT(BgL_depthz00_4702),
																												((obj_t)
																													BgL_ctxz00_4703));
																								}}
																							{
																								obj_t BgL_argsz00_7325;
																								long BgL_iz00_7323;

																								BgL_iz00_7323 =
																									(BgL_iz00_5116 + 1L);
																								BgL_argsz00_7325 =
																									CDR(BgL_argsz00_5117);
																								BgL_argsz00_5117 =
																									BgL_argsz00_7325;
																								BgL_iz00_5116 = BgL_iz00_7323;
																								goto BgL_loopz00_5115;
																							}
																						}
																					else
																						{	/* Stackable/walk.scm 256 */
																							BgL_tmpz00_6888 = ((bool_t) 0);
																						}
																				}
																			else
																				{	/* Stackable/walk.scm 266 */
																					obj_t BgL_g1433z00_5122;

																					BgL_g1433z00_5122 =
																						(((BgL_appz00_bglt) COBJECT(
																								((BgL_appz00_bglt)
																									BgL_nodez00_4700)))->
																						BgL_argsz00);
																					{
																						obj_t BgL_l1431z00_5124;

																						BgL_l1431z00_5124 =
																							BgL_g1433z00_5122;
																					BgL_zc3z04anonymousza32007ze3z87_5123:
																						if (PAIRP
																							(BgL_l1431z00_5124))
																							{	/* Stackable/walk.scm 266 */
																								{	/* Stackable/walk.scm 266 */
																									obj_t BgL_az00_5125;

																									BgL_az00_5125 =
																										CAR(BgL_l1431z00_5124);
																									BGl_stackablez00zzstackable_walkz00
																										(((BgL_nodez00_bglt)
																											BgL_az00_5125),
																										((bool_t) 1), 100000L,
																										((obj_t) BgL_ctxz00_4703));
																								}
																								{
																									obj_t BgL_l1431z00_7337;

																									BgL_l1431z00_7337 =
																										CDR(BgL_l1431z00_5124);
																									BgL_l1431z00_5124 =
																										BgL_l1431z00_7337;
																									goto
																										BgL_zc3z04anonymousza32007ze3z87_5123;
																								}
																							}
																						else
																							{	/* Stackable/walk.scm 266 */
																								BgL_tmpz00_6888 = ((bool_t) 1);
																							}
																					}
																				}
																		}
																}
														}
													else
														{	/* Stackable/walk.scm 268 */
															obj_t BgL_g1436z00_5126;

															BgL_g1436z00_5126 =
																(((BgL_appz00_bglt) COBJECT(
																		((BgL_appz00_bglt) BgL_nodez00_4700)))->
																BgL_argsz00);
															{
																obj_t BgL_l1434z00_5128;

																BgL_l1434z00_5128 = BgL_g1436z00_5126;
															BgL_zc3z04anonymousza32016ze3z87_5127:
																if (PAIRP(BgL_l1434z00_5128))
																	{	/* Stackable/walk.scm 268 */
																		{	/* Stackable/walk.scm 268 */
																			obj_t BgL_az00_5129;

																			BgL_az00_5129 = CAR(BgL_l1434z00_5128);
																			BGl_stackablez00zzstackable_walkz00(
																				((BgL_nodez00_bglt) BgL_az00_5129),
																				((bool_t) 1), 100000L,
																				((obj_t) BgL_ctxz00_4703));
																		}
																		{
																			obj_t BgL_l1434z00_7347;

																			BgL_l1434z00_7347 =
																				CDR(BgL_l1434z00_5128);
																			BgL_l1434z00_5128 = BgL_l1434z00_7347;
																			goto
																				BgL_zc3z04anonymousza32016ze3z87_5127;
																		}
																	}
																else
																	{	/* Stackable/walk.scm 268 */
																		BgL_tmpz00_6888 = ((bool_t) 1);
																	}
															}
														}
												}
										}
								}
							}
						}
					}
				}
				return BBOOL(BgL_tmpz00_6888);
			}
		}

	}



/* &stackable-let-fun1478 */
	obj_t BGl_z62stackablezd2letzd2fun1478z62zzstackable_walkz00(obj_t
		BgL_envz00_4704, obj_t BgL_nodez00_4705, obj_t BgL_escpz00_4706,
		obj_t BgL_depthz00_4707, obj_t BgL_ctxz00_4708)
	{
		{	/* Stackable/walk.scm 177 */
			{	/* Stackable/walk.scm 179 */
				obj_t BgL_g1413z00_5132;

				BgL_g1413z00_5132 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_4705)))->BgL_localsz00);
				{
					obj_t BgL_l1411z00_5134;

					BgL_l1411z00_5134 = BgL_g1413z00_5132;
				BgL_zc3z04anonymousza31932ze3z87_5133:
					if (PAIRP(BgL_l1411z00_5134))
						{	/* Stackable/walk.scm 179 */
							{	/* Stackable/walk.scm 180 */
								obj_t BgL_localz00_5135;

								BgL_localz00_5135 = CAR(BgL_l1411z00_5134);
								{	/* Stackable/walk.scm 181 */
									BgL_sfunz00_bglt BgL_i1189z00_5136;

									BgL_i1189z00_5136 =
										((BgL_sfunz00_bglt)
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_localz00_5135))))->
											BgL_valuez00));
									{	/* Stackable/walk.scm 182 */
										obj_t BgL_arg1934z00_5137;
										long BgL_arg1935z00_5138;

										BgL_arg1934z00_5137 =
											(((BgL_sfunz00_bglt) COBJECT(BgL_i1189z00_5136))->
											BgL_bodyz00);
										{
											BgL_localzf2depthzf2_bglt BgL_auxz00_7360;

											{
												obj_t BgL_auxz00_7361;

												{	/* Stackable/walk.scm 182 */
													BgL_objectz00_bglt BgL_tmpz00_7362;

													BgL_tmpz00_7362 =
														((BgL_objectz00_bglt)
														((BgL_localz00_bglt) BgL_localz00_5135));
													BgL_auxz00_7361 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_7362);
												}
												BgL_auxz00_7360 =
													((BgL_localzf2depthzf2_bglt) BgL_auxz00_7361);
											}
											BgL_arg1935z00_5138 =
												(((BgL_localzf2depthzf2_bglt)
													COBJECT(BgL_auxz00_7360))->BgL_depthz00);
										}
										BGl_stackablez00zzstackable_walkz00(
											((BgL_nodez00_bglt) BgL_arg1934z00_5137), ((bool_t) 0),
											BgL_arg1935z00_5138, ((obj_t) BgL_ctxz00_4708));
							}}}
							{
								obj_t BgL_l1411z00_7371;

								BgL_l1411z00_7371 = CDR(BgL_l1411z00_5134);
								BgL_l1411z00_5134 = BgL_l1411z00_7371;
								goto BgL_zc3z04anonymousza31932ze3z87_5133;
							}
						}
					else
						{	/* Stackable/walk.scm 179 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_stackablez00zzstackable_walkz00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_4705)))->BgL_bodyz00),
				CBOOL(BgL_escpz00_4706),
				(long) CINT(BgL_depthz00_4707), ((obj_t) BgL_ctxz00_4708));
		}

	}



/* &stackable-let-var1476 */
	obj_t BGl_z62stackablezd2letzd2var1476z62zzstackable_walkz00(obj_t
		BgL_envz00_4709, obj_t BgL_nodez00_4710, obj_t BgL_escpz00_4711,
		obj_t BgL_depthz00_4712, obj_t BgL_ctxz00_4713)
	{
		{	/* Stackable/walk.scm 164 */
			{	/* Stackable/walk.scm 166 */
				obj_t BgL_g1410z00_5141;

				BgL_g1410z00_5141 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_4710)))->BgL_bindingsz00);
				{
					obj_t BgL_l1408z00_5143;

					BgL_l1408z00_5143 = BgL_g1410z00_5141;
				BgL_zc3z04anonymousza31922ze3z87_5142:
					if (PAIRP(BgL_l1408z00_5143))
						{	/* Stackable/walk.scm 166 */
							{	/* Stackable/walk.scm 167 */
								obj_t BgL_bindz00_5144;

								BgL_bindz00_5144 = CAR(BgL_l1408z00_5143);
								{	/* Stackable/walk.scm 167 */
									obj_t BgL_varz00_5145;
									obj_t BgL_valz00_5146;

									BgL_varz00_5145 = CAR(((obj_t) BgL_bindz00_5144));
									BgL_valz00_5146 = CDR(((obj_t) BgL_bindz00_5144));
									{	/* Stackable/walk.scm 170 */
										bool_t BgL_arg1924z00_5147;
										long BgL_arg1925z00_5148;

										if (CBOOL(
												(((BgL_localz00_bglt) COBJECT(
															((BgL_localz00_bglt)
																((BgL_localz00_bglt) BgL_varz00_5145))))->
													BgL_valzd2noescapezd2)))
											{	/* Stackable/walk.scm 170 */
												BgL_arg1924z00_5147 = ((bool_t) 0);
											}
										else
											{	/* Stackable/walk.scm 170 */
												BgL_arg1924z00_5147 = ((bool_t) 1);
											}
										{
											BgL_localzf2depthzf2_bglt BgL_auxz00_7393;

											{
												obj_t BgL_auxz00_7394;

												{	/* Stackable/walk.scm 170 */
													BgL_objectz00_bglt BgL_tmpz00_7395;

													BgL_tmpz00_7395 =
														((BgL_objectz00_bglt)
														((BgL_localz00_bglt) BgL_varz00_5145));
													BgL_auxz00_7394 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_7395);
												}
												BgL_auxz00_7393 =
													((BgL_localzf2depthzf2_bglt) BgL_auxz00_7394);
											}
											BgL_arg1925z00_5148 =
												(((BgL_localzf2depthzf2_bglt)
													COBJECT(BgL_auxz00_7393))->BgL_depthz00);
										}
										BGl_stackablez00zzstackable_walkz00(
											((BgL_nodez00_bglt) BgL_valz00_5146), BgL_arg1924z00_5147,
											BgL_arg1925z00_5148, ((obj_t) BgL_ctxz00_4713));
									}
								}
							}
							{
								obj_t BgL_l1408z00_7404;

								BgL_l1408z00_7404 = CDR(BgL_l1408z00_5143);
								BgL_l1408z00_5143 = BgL_l1408z00_7404;
								goto BgL_zc3z04anonymousza31922ze3z87_5142;
							}
						}
					else
						{	/* Stackable/walk.scm 166 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_stackablez00zzstackable_walkz00(
				(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_4710)))->BgL_bodyz00),
				CBOOL(BgL_escpz00_4711),
				(long) CINT(BgL_depthz00_4712), ((obj_t) BgL_ctxz00_4713));
		}

	}



/* &stackable-conditiona1474 */
	obj_t BGl_z62stackablezd2conditiona1474zb0zzstackable_walkz00(obj_t
		BgL_envz00_4714, obj_t BgL_nodez00_4715, obj_t BgL_escpz00_4716,
		obj_t BgL_depthz00_4717, obj_t BgL_ctxz00_4718)
	{
		{	/* Stackable/walk.scm 155 */
			BGl_stackablez00zzstackable_walkz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_4715)))->BgL_testz00),
				((bool_t) 0), (long) CINT(BgL_depthz00_4717),
				((obj_t) BgL_ctxz00_4718));
			BGl_stackablez00zzstackable_walkz00((((BgL_conditionalz00_bglt)
						COBJECT(((BgL_conditionalz00_bglt) BgL_nodez00_4715)))->
					BgL_truez00), CBOOL(BgL_escpz00_4716), (long) CINT(BgL_depthz00_4717),
				((obj_t) BgL_ctxz00_4718));
			return BGl_stackablez00zzstackable_walkz00((((BgL_conditionalz00_bglt)
						COBJECT(((BgL_conditionalz00_bglt) BgL_nodez00_4715)))->
					BgL_falsez00), CBOOL(BgL_escpz00_4716),
				(long) CINT(BgL_depthz00_4717), ((obj_t) BgL_ctxz00_4718));
		}

	}



/* &stackable-setq1472 */
	obj_t BGl_z62stackablezd2setq1472zb0zzstackable_walkz00(obj_t BgL_envz00_4719,
		obj_t BgL_nodez00_4720, obj_t BgL_escpz00_4721, obj_t BgL_depthz00_4722,
		obj_t BgL_ctxz00_4723)
	{
		{	/* Stackable/walk.scm 144 */
			{	/* Stackable/walk.scm 146 */
				BgL_varz00_bglt BgL_i1182z00_5153;

				BgL_i1182z00_5153 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_4720)))->BgL_varz00);
				{	/* Stackable/walk.scm 147 */
					bool_t BgL_test2641z00_7431;

					{	/* Stackable/walk.scm 147 */
						BgL_variablez00_bglt BgL_arg1912z00_5154;

						BgL_arg1912z00_5154 =
							(((BgL_varz00_bglt) COBJECT(BgL_i1182z00_5153))->BgL_variablez00);
						{	/* Stackable/walk.scm 147 */
							obj_t BgL_classz00_5155;

							BgL_classz00_5155 = BGl_globalz00zzast_varz00;
							{	/* Stackable/walk.scm 147 */
								BgL_objectz00_bglt BgL_arg1807z00_5156;

								{	/* Stackable/walk.scm 147 */
									obj_t BgL_tmpz00_7433;

									BgL_tmpz00_7433 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg1912z00_5154));
									BgL_arg1807z00_5156 = (BgL_objectz00_bglt) (BgL_tmpz00_7433);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Stackable/walk.scm 147 */
										long BgL_idxz00_5157;

										BgL_idxz00_5157 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5156);
										BgL_test2641z00_7431 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_5157 + 2L)) == BgL_classz00_5155);
									}
								else
									{	/* Stackable/walk.scm 147 */
										bool_t BgL_res2277z00_5160;

										{	/* Stackable/walk.scm 147 */
											obj_t BgL_oclassz00_5161;

											{	/* Stackable/walk.scm 147 */
												obj_t BgL_arg1815z00_5162;
												long BgL_arg1816z00_5163;

												BgL_arg1815z00_5162 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Stackable/walk.scm 147 */
													long BgL_arg1817z00_5164;

													BgL_arg1817z00_5164 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5156);
													BgL_arg1816z00_5163 =
														(BgL_arg1817z00_5164 - OBJECT_TYPE);
												}
												BgL_oclassz00_5161 =
													VECTOR_REF(BgL_arg1815z00_5162, BgL_arg1816z00_5163);
											}
											{	/* Stackable/walk.scm 147 */
												bool_t BgL__ortest_1115z00_5165;

												BgL__ortest_1115z00_5165 =
													(BgL_classz00_5155 == BgL_oclassz00_5161);
												if (BgL__ortest_1115z00_5165)
													{	/* Stackable/walk.scm 147 */
														BgL_res2277z00_5160 = BgL__ortest_1115z00_5165;
													}
												else
													{	/* Stackable/walk.scm 147 */
														long BgL_odepthz00_5166;

														{	/* Stackable/walk.scm 147 */
															obj_t BgL_arg1804z00_5167;

															BgL_arg1804z00_5167 = (BgL_oclassz00_5161);
															BgL_odepthz00_5166 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_5167);
														}
														if ((2L < BgL_odepthz00_5166))
															{	/* Stackable/walk.scm 147 */
																obj_t BgL_arg1802z00_5168;

																{	/* Stackable/walk.scm 147 */
																	obj_t BgL_arg1803z00_5169;

																	BgL_arg1803z00_5169 = (BgL_oclassz00_5161);
																	BgL_arg1802z00_5168 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5169,
																		2L);
																}
																BgL_res2277z00_5160 =
																	(BgL_arg1802z00_5168 == BgL_classz00_5155);
															}
														else
															{	/* Stackable/walk.scm 147 */
																BgL_res2277z00_5160 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2641z00_7431 = BgL_res2277z00_5160;
									}
							}
						}
					}
					if (BgL_test2641z00_7431)
						{	/* Stackable/walk.scm 147 */
							return
								BGl_stackablez00zzstackable_walkz00(
								(((BgL_setqz00_bglt) COBJECT(
											((BgL_setqz00_bglt) BgL_nodez00_4720)))->BgL_valuez00),
								((bool_t) 1), -1L, ((obj_t) BgL_ctxz00_4723));
						}
					else
						{	/* Stackable/walk.scm 149 */
							BgL_localz00_bglt BgL_i1183z00_5170;

							BgL_i1183z00_5170 =
								((BgL_localz00_bglt)
								(((BgL_varz00_bglt) COBJECT(BgL_i1182z00_5153))->
									BgL_variablez00));
							{	/* Stackable/walk.scm 150 */
								BgL_nodez00_bglt BgL_arg1910z00_5171;
								long BgL_arg1911z00_5172;

								BgL_arg1910z00_5171 =
									(((BgL_setqz00_bglt) COBJECT(
											((BgL_setqz00_bglt) BgL_nodez00_4720)))->BgL_valuez00);
								{
									BgL_localzf2depthzf2_bglt BgL_auxz00_7464;

									{
										obj_t BgL_auxz00_7465;

										{	/* Stackable/walk.scm 150 */
											BgL_objectz00_bglt BgL_tmpz00_7466;

											BgL_tmpz00_7466 =
												((BgL_objectz00_bglt) BgL_i1183z00_5170);
											BgL_auxz00_7465 = BGL_OBJECT_WIDENING(BgL_tmpz00_7466);
										}
										BgL_auxz00_7464 =
											((BgL_localzf2depthzf2_bglt) BgL_auxz00_7465);
									}
									BgL_arg1911z00_5172 =
										(((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_7464))->
										BgL_depthz00);
								}
								return
									BGl_stackablez00zzstackable_walkz00(BgL_arg1910z00_5171,
									CBOOL(BgL_escpz00_4721), BgL_arg1911z00_5172,
									((obj_t) BgL_ctxz00_4723));
							}
						}
				}
			}
		}

	}



/* &stackable-closure1470 */
	obj_t BGl_z62stackablezd2closure1470zb0zzstackable_walkz00(obj_t
		BgL_envz00_4724, obj_t BgL_nodez00_4725, obj_t BgL_escpz00_4726,
		obj_t BgL_depthz00_4727, obj_t BgL_ctxz00_4728)
	{
		{	/* Stackable/walk.scm 130 */
			{

				{	/* Stackable/walk.scm 130 */
					obj_t BgL_nextzd2method1469zd2_5176;

					BgL_nextzd2method1469zd2_5176 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_closurez00_bglt) BgL_nodez00_4725)),
						BGl_stackablezd2envzd2zzstackable_walkz00,
						BGl_closurez00zzast_nodez00);
					BGL_PROCEDURE_CALL4(BgL_nextzd2method1469zd2_5176,
						((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_4725)),
						BgL_escpz00_4726, BgL_depthz00_4727, ((obj_t) BgL_ctxz00_4728));
				}
				{	/* Stackable/walk.scm 132 */
					BgL_variablez00_bglt BgL_vz00_5177;

					BgL_vz00_5177 =
						(((BgL_varz00_bglt) COBJECT(
								((BgL_varz00_bglt)
									((BgL_closurez00_bglt) BgL_nodez00_4725))))->BgL_variablez00);
					{	/* Stackable/walk.scm 133 */
						bool_t BgL_test2645z00_7490;

						{	/* Stackable/walk.scm 133 */
							obj_t BgL_classz00_5178;

							BgL_classz00_5178 = BGl_localz00zzast_varz00;
							{	/* Stackable/walk.scm 133 */
								BgL_objectz00_bglt BgL_arg1807z00_5179;

								{	/* Stackable/walk.scm 133 */
									obj_t BgL_tmpz00_7491;

									BgL_tmpz00_7491 =
										((obj_t) ((BgL_objectz00_bglt) BgL_vz00_5177));
									BgL_arg1807z00_5179 = (BgL_objectz00_bglt) (BgL_tmpz00_7491);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Stackable/walk.scm 133 */
										long BgL_idxz00_5180;

										BgL_idxz00_5180 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5179);
										BgL_test2645z00_7490 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_5180 + 2L)) == BgL_classz00_5178);
									}
								else
									{	/* Stackable/walk.scm 133 */
										bool_t BgL_res2276z00_5183;

										{	/* Stackable/walk.scm 133 */
											obj_t BgL_oclassz00_5184;

											{	/* Stackable/walk.scm 133 */
												obj_t BgL_arg1815z00_5185;
												long BgL_arg1816z00_5186;

												BgL_arg1815z00_5185 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Stackable/walk.scm 133 */
													long BgL_arg1817z00_5187;

													BgL_arg1817z00_5187 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5179);
													BgL_arg1816z00_5186 =
														(BgL_arg1817z00_5187 - OBJECT_TYPE);
												}
												BgL_oclassz00_5184 =
													VECTOR_REF(BgL_arg1815z00_5185, BgL_arg1816z00_5186);
											}
											{	/* Stackable/walk.scm 133 */
												bool_t BgL__ortest_1115z00_5188;

												BgL__ortest_1115z00_5188 =
													(BgL_classz00_5178 == BgL_oclassz00_5184);
												if (BgL__ortest_1115z00_5188)
													{	/* Stackable/walk.scm 133 */
														BgL_res2276z00_5183 = BgL__ortest_1115z00_5188;
													}
												else
													{	/* Stackable/walk.scm 133 */
														long BgL_odepthz00_5189;

														{	/* Stackable/walk.scm 133 */
															obj_t BgL_arg1804z00_5190;

															BgL_arg1804z00_5190 = (BgL_oclassz00_5184);
															BgL_odepthz00_5189 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_5190);
														}
														if ((2L < BgL_odepthz00_5189))
															{	/* Stackable/walk.scm 133 */
																obj_t BgL_arg1802z00_5191;

																{	/* Stackable/walk.scm 133 */
																	obj_t BgL_arg1803z00_5192;

																	BgL_arg1803z00_5192 = (BgL_oclassz00_5184);
																	BgL_arg1802z00_5191 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5192,
																		2L);
																}
																BgL_res2276z00_5183 =
																	(BgL_arg1802z00_5191 == BgL_classz00_5178);
															}
														else
															{	/* Stackable/walk.scm 133 */
																BgL_res2276z00_5183 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2645z00_7490 = BgL_res2276z00_5183;
									}
							}
						}
						if (BgL_test2645z00_7490)
							{	/* Stackable/walk.scm 134 */
								BgL_valuez00_bglt BgL_fz00_5193;

								BgL_fz00_5193 =
									(((BgL_variablez00_bglt) COBJECT(BgL_vz00_5177))->
									BgL_valuez00);
								{	/* Stackable/walk.scm 137 */
									bool_t BgL_test2649z00_7515;

									if (CBOOL(
											(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_fz00_5193)))->
												BgL_stackablez00)))
										{	/* Stackable/walk.scm 137 */
											if (CBOOL(BgL_escpz00_4726))
												{	/* Stackable/walk.scm 137 */
													BgL_test2649z00_7515 = CBOOL(BgL_escpz00_4726);
												}
											else
												{	/* Stackable/walk.scm 137 */
													if (CBOOL(
															(((BgL_localz00_bglt) COBJECT(
																		((BgL_localz00_bglt)
																			((BgL_localz00_bglt) BgL_vz00_5177))))->
																BgL_valzd2noescapezd2)))
														{	/* Stackable/walk.scm 137 */
															BgL_test2649z00_7515 = ((bool_t) 0);
														}
													else
														{	/* Stackable/walk.scm 137 */
															BgL_test2649z00_7515 = ((bool_t) 1);
														}
												}
										}
									else
										{	/* Stackable/walk.scm 137 */
											BgL_test2649z00_7515 = ((bool_t) 0);
										}
									if (BgL_test2649z00_7515)
										{	/* Stackable/walk.scm 137 */
											{	/* Stackable/walk.scm 138 */
												obj_t BgL_tmpz00_7528;

												BgL_tmpz00_7528 = ((obj_t) BgL_ctxz00_4728);
												SET_CAR(BgL_tmpz00_7528, BFALSE);
											}
											return
												((((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_fz00_5193)))->
													BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
										}
									else
										{	/* Stackable/walk.scm 137 */
											return BFALSE;
										}
								}
							}
						else
							{	/* Stackable/walk.scm 133 */
								return BFALSE;
							}
					}
				}
			}
		}

	}



/* &stackable-var1468 */
	obj_t BGl_z62stackablezd2var1468zb0zzstackable_walkz00(obj_t BgL_envz00_4729,
		obj_t BgL_nodez00_4730, obj_t BgL_escpz00_4731, obj_t BgL_depthz00_4732,
		obj_t BgL_ctxz00_4733)
	{
		{	/* Stackable/walk.scm 120 */
			{	/* Stackable/walk.scm 122 */
				bool_t BgL_test2653z00_7533;

				{	/* Stackable/walk.scm 122 */
					BgL_variablez00_bglt BgL_arg1893z00_5196;

					BgL_arg1893z00_5196 =
						(((BgL_varz00_bglt) COBJECT(
								((BgL_varz00_bglt) BgL_nodez00_4730)))->BgL_variablez00);
					{	/* Stackable/walk.scm 122 */
						obj_t BgL_classz00_5197;

						BgL_classz00_5197 = BGl_localz00zzast_varz00;
						{	/* Stackable/walk.scm 122 */
							BgL_objectz00_bglt BgL_arg1807z00_5198;

							{	/* Stackable/walk.scm 122 */
								obj_t BgL_tmpz00_7536;

								BgL_tmpz00_7536 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg1893z00_5196));
								BgL_arg1807z00_5198 = (BgL_objectz00_bglt) (BgL_tmpz00_7536);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Stackable/walk.scm 122 */
									long BgL_idxz00_5199;

									BgL_idxz00_5199 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5198);
									BgL_test2653z00_7533 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_5199 + 2L)) == BgL_classz00_5197);
								}
							else
								{	/* Stackable/walk.scm 122 */
									bool_t BgL_res2275z00_5202;

									{	/* Stackable/walk.scm 122 */
										obj_t BgL_oclassz00_5203;

										{	/* Stackable/walk.scm 122 */
											obj_t BgL_arg1815z00_5204;
											long BgL_arg1816z00_5205;

											BgL_arg1815z00_5204 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Stackable/walk.scm 122 */
												long BgL_arg1817z00_5206;

												BgL_arg1817z00_5206 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5198);
												BgL_arg1816z00_5205 =
													(BgL_arg1817z00_5206 - OBJECT_TYPE);
											}
											BgL_oclassz00_5203 =
												VECTOR_REF(BgL_arg1815z00_5204, BgL_arg1816z00_5205);
										}
										{	/* Stackable/walk.scm 122 */
											bool_t BgL__ortest_1115z00_5207;

											BgL__ortest_1115z00_5207 =
												(BgL_classz00_5197 == BgL_oclassz00_5203);
											if (BgL__ortest_1115z00_5207)
												{	/* Stackable/walk.scm 122 */
													BgL_res2275z00_5202 = BgL__ortest_1115z00_5207;
												}
											else
												{	/* Stackable/walk.scm 122 */
													long BgL_odepthz00_5208;

													{	/* Stackable/walk.scm 122 */
														obj_t BgL_arg1804z00_5209;

														BgL_arg1804z00_5209 = (BgL_oclassz00_5203);
														BgL_odepthz00_5208 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_5209);
													}
													if ((2L < BgL_odepthz00_5208))
														{	/* Stackable/walk.scm 122 */
															obj_t BgL_arg1802z00_5210;

															{	/* Stackable/walk.scm 122 */
																obj_t BgL_arg1803z00_5211;

																BgL_arg1803z00_5211 = (BgL_oclassz00_5203);
																BgL_arg1802z00_5210 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5211,
																	2L);
															}
															BgL_res2275z00_5202 =
																(BgL_arg1802z00_5210 == BgL_classz00_5197);
														}
													else
														{	/* Stackable/walk.scm 122 */
															BgL_res2275z00_5202 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2653z00_7533 = BgL_res2275z00_5202;
								}
						}
					}
				}
				if (BgL_test2653z00_7533)
					{	/* Stackable/walk.scm 123 */
						BgL_localz00_bglt BgL_i1177z00_5212;

						BgL_i1177z00_5212 =
							((BgL_localz00_bglt)
							(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_nodez00_4730)))->BgL_variablez00));
						{	/* Stackable/walk.scm 124 */
							bool_t BgL_test2657z00_7562;

							if (CBOOL(BgL_escpz00_4731))
								{	/* Stackable/walk.scm 124 */
									BgL_test2657z00_7562 = ((bool_t) 1);
								}
							else
								{	/* Stackable/walk.scm 124 */
									long BgL_arg1892z00_5213;

									{
										BgL_localzf2depthzf2_bglt BgL_auxz00_7565;

										{
											obj_t BgL_auxz00_7566;

											{	/* Stackable/walk.scm 124 */
												BgL_objectz00_bglt BgL_tmpz00_7567;

												BgL_tmpz00_7567 =
													((BgL_objectz00_bglt) BgL_i1177z00_5212);
												BgL_auxz00_7566 = BGL_OBJECT_WIDENING(BgL_tmpz00_7567);
											}
											BgL_auxz00_7565 =
												((BgL_localzf2depthzf2_bglt) BgL_auxz00_7566);
										}
										BgL_arg1892z00_5213 =
											(((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_7565))->
											BgL_depthz00);
									}
									BgL_test2657z00_7562 =
										((long) CINT(BgL_depthz00_4732) < BgL_arg1892z00_5213);
								}
							if (BgL_test2657z00_7562)
								{	/* Stackable/walk.scm 125 */
									BgL_variablez00_bglt BgL_arg1891z00_5214;

									BgL_arg1891z00_5214 =
										(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt) BgL_nodez00_4730)))->
										BgL_variablez00);
									return BGl_escapez12z12zzstackable_walkz00(((obj_t)
											BgL_arg1891z00_5214), ((obj_t) BgL_ctxz00_4733));
								}
							else
								{	/* Stackable/walk.scm 124 */
									return BFALSE;
								}
						}
					}
				else
					{	/* Stackable/walk.scm 122 */
						return BFALSE;
					}
			}
		}

	}



/* &node->sexp-app/depth1464 */
	obj_t BGl_z62nodezd2ze3sexpzd2appzf2depth1464z73zzstackable_walkz00(obj_t
		BgL_envz00_4734, obj_t BgL_nodez00_4735)
	{
		{	/* Stackable/walk.scm 57 */
			{

				{	/* Stackable/walk.scm 59 */
					obj_t BgL_nz00_5218;

					{	/* Stackable/walk.scm 57 */
						obj_t BgL_nextzd2method1463zd2_5217;

						BgL_nextzd2method1463zd2_5217 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_4735)),
							BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00,
							BGl_appzf2depthzf2zzstackable_walkz00);
						BgL_nz00_5218 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1463zd2_5217,
							((obj_t) ((BgL_appz00_bglt) BgL_nodez00_4735)));
					}
					{	/* Stackable/walk.scm 60 */
						bool_t BgL_test2659z00_7588;

						{	/* Stackable/walk.scm 60 */
							bool_t BgL_test2660z00_7589;

							{	/* Stackable/walk.scm 60 */
								obj_t BgL_tmpz00_7590;

								BgL_tmpz00_7590 = CAR(((obj_t) BgL_nz00_5218));
								BgL_test2660z00_7589 = SYMBOLP(BgL_tmpz00_7590);
							}
							if (BgL_test2660z00_7589)
								{	/* Stackable/walk.scm 60 */
									BgL_test2659z00_7588 =
										CBOOL(BGl_za2keyzd2shapezf3za2z21zzengine_paramz00);
								}
							else
								{	/* Stackable/walk.scm 60 */
									BgL_test2659z00_7588 = ((bool_t) 0);
								}
						}
						if (BgL_test2659z00_7588)
							{	/* Stackable/walk.scm 61 */
								obj_t BgL_arg1874z00_5219;
								obj_t BgL_arg1875z00_5220;

								{	/* Stackable/walk.scm 61 */
									obj_t BgL_arg1876z00_5221;

									{	/* Stackable/walk.scm 61 */
										obj_t BgL_arg1877z00_5222;
										obj_t BgL_arg1878z00_5223;

										{	/* Stackable/walk.scm 61 */
											obj_t BgL_arg1879z00_5224;

											{	/* Stackable/walk.scm 61 */
												obj_t BgL_arg1880z00_5225;

												{	/* Stackable/walk.scm 61 */
													long BgL_arg1882z00_5226;

													{
														BgL_appzf2depthzf2_bglt BgL_auxz00_7595;

														{
															obj_t BgL_auxz00_7596;

															{	/* Stackable/walk.scm 61 */
																BgL_objectz00_bglt BgL_tmpz00_7597;

																BgL_tmpz00_7597 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt) BgL_nodez00_4735));
																BgL_auxz00_7596 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_7597);
															}
															BgL_auxz00_7595 =
																((BgL_appzf2depthzf2_bglt) BgL_auxz00_7596);
														}
														BgL_arg1882z00_5226 =
															(((BgL_appzf2depthzf2_bglt)
																COBJECT(BgL_auxz00_7595))->BgL_depthz00);
													}
													{	/* Stackable/walk.scm 61 */
														obj_t BgL_list1883z00_5227;

														BgL_list1883z00_5227 =
															MAKE_YOUNG_PAIR(BINT(BgL_arg1882z00_5226), BNIL);
														BgL_arg1880z00_5225 =
															BGl_formatz00zz__r4_output_6_10_3z00
															(BGl_string2379z00zzstackable_walkz00,
															BgL_list1883z00_5227);
												}}
												BgL_arg1879z00_5224 =
													bstring_to_symbol(BgL_arg1880z00_5225);
											}
											{	/* Stackable/walk.scm 61 */
												obj_t BgL_arg1455z00_5228;

												BgL_arg1455z00_5228 =
													SYMBOL_TO_STRING(BgL_arg1879z00_5224);
												BgL_arg1877z00_5222 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_5228);
										}}
										{	/* Stackable/walk.scm 61 */
											obj_t BgL_arg1884z00_5229;

											BgL_arg1884z00_5229 = CAR(((obj_t) BgL_nz00_5218));
											{	/* Stackable/walk.scm 61 */
												obj_t BgL_arg1455z00_5230;

												BgL_arg1455z00_5230 =
													SYMBOL_TO_STRING(((obj_t) BgL_arg1884z00_5229));
												BgL_arg1878z00_5223 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_5230);
										}}
										BgL_arg1876z00_5221 =
											string_append(BgL_arg1877z00_5222, BgL_arg1878z00_5223);
									}
									BgL_arg1874z00_5219 = bstring_to_symbol(BgL_arg1876z00_5221);
								}
								BgL_arg1875z00_5220 = CDR(((obj_t) BgL_nz00_5218));
								return
									MAKE_YOUNG_PAIR(BgL_arg1874z00_5219, BgL_arg1875z00_5220);
							}
						else
							{	/* Stackable/walk.scm 60 */
								return BgL_nz00_5218;
							}
					}
				}
			}
		}

	}



/* &shape-local/depth1462 */
	obj_t BGl_z62shapezd2localzf2depth1462z42zzstackable_walkz00(obj_t
		BgL_envz00_4736, obj_t BgL_varz00_4737)
	{
		{	/* Stackable/walk.scm 48 */
			if (CBOOL(BGl_za2keyzd2shapezf3za2z21zzengine_paramz00))
				{	/* Stackable/walk.scm 51 */
					obj_t BgL_arg1863z00_5232;

					{	/* Stackable/walk.scm 51 */
						long BgL_arg1864z00_5233;
						obj_t BgL_arg1866z00_5234;

						{
							BgL_localzf2depthzf2_bglt BgL_auxz00_7621;

							{
								obj_t BgL_auxz00_7622;

								{	/* Stackable/walk.scm 51 */
									BgL_objectz00_bglt BgL_tmpz00_7623;

									BgL_tmpz00_7623 =
										((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_varz00_4737));
									BgL_auxz00_7622 = BGL_OBJECT_WIDENING(BgL_tmpz00_7623);
								}
								BgL_auxz00_7621 = ((BgL_localzf2depthzf2_bglt) BgL_auxz00_7622);
							}
							BgL_arg1864z00_5233 =
								(((BgL_localzf2depthzf2_bglt) COBJECT(BgL_auxz00_7621))->
								BgL_depthz00);
						}
						BgL_arg1866z00_5234 =
							BGl_callzd2nextzd2methodze70ze7zzstackable_walkz00(
							((BgL_localz00_bglt) BgL_varz00_4737));
						{	/* Stackable/walk.scm 51 */
							obj_t BgL_list1867z00_5235;

							{	/* Stackable/walk.scm 51 */
								obj_t BgL_arg1868z00_5236;

								BgL_arg1868z00_5236 =
									MAKE_YOUNG_PAIR(BgL_arg1866z00_5234, BNIL);
								BgL_list1867z00_5235 =
									MAKE_YOUNG_PAIR(BINT(BgL_arg1864z00_5233),
									BgL_arg1868z00_5236);
							}
							BgL_arg1863z00_5232 =
								BGl_formatz00zz__r4_output_6_10_3z00
								(BGl_string2380z00zzstackable_walkz00, BgL_list1867z00_5235);
					}}
					return bstring_to_symbol(BgL_arg1863z00_5232);
				}
			else
				{	/* Stackable/walk.scm 50 */
					return
						BGl_callzd2nextzd2methodze70ze7zzstackable_walkz00(
						((BgL_localz00_bglt) BgL_varz00_4737));
				}
		}

	}



/* call-next-method~0 */
	obj_t BGl_callzd2nextzd2methodze70ze7zzstackable_walkz00(BgL_localz00_bglt
		BgL_varz00_4738)
	{
		{	/* Stackable/walk.scm 48 */
			{	/* Stackable/walk.scm 48 */
				obj_t BgL_nextzd2method1461zd2_2066;

				BgL_nextzd2method1461zd2_2066 =
					BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
					((BgL_objectz00_bglt) BgL_varz00_4738),
					BGl_shapezd2envzd2zztools_shapez00,
					BGl_localzf2depthzf2zzstackable_walkz00);
				return BGL_PROCEDURE_CALL1(BgL_nextzd2method1461zd2_2066,
					((obj_t) BgL_varz00_4738));
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzstackable_walkz00(void)
	{
		{	/* Stackable/walk.scm 16 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zztype_miscz00(49975086L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zzast_walkz00(343174225L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zzast_occurz00(282085879L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2381z00zzstackable_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
