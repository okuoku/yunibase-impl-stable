/*===========================================================================*/
/*   (Cnst/initialize.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cnst/initialize.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CNST_INITIALIZE_TYPE_DEFINITIONS
#define BGL_CNST_INITIALIZE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;


#endif													// BGL_CNST_INITIALIZE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_readzd2fullzd2cnstzd2initializa7ez12z67zzcnst_initializa7eza7(void);
	static obj_t BGl_requirezd2initializa7ationz75zzcnst_initializa7eza7 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	extern obj_t BGl_za2objectzd2initzd2modeza2z00zzengine_paramz00;
	static obj_t BGl_toplevelzd2initzd2zzcnst_initializa7eza7(void);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_internzd2fullzd2cnstzd2initializa7ez12z67zzcnst_initializa7eza7(void);
	extern obj_t BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nodez00_bglt);
	static obj_t BGl_genericzd2initzd2zzcnst_initializa7eza7(void);
	static obj_t BGl_objectzd2initzd2zzcnst_initializa7eza7(void);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_za2initzd2modeza2zd2zzengine_paramz00;
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_writez00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcnst_initializa7eza7(void);
	BGL_IMPORT obj_t obj_to_string(obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_cnstzd2alloczd2stringz00zzcnst_allocz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t BGl_getzd2cnstzd2setz00zzcnst_allocz00(void);
	static obj_t
		BGl_internzd2emptyzd2cnstzd2initializa7ez12z67zzcnst_initializa7eza7(void);
	BGL_IMPORT obj_t bgl_close_output_port(obj_t);
	static obj_t BGl_libzd2initializa7ez12z67zzcnst_initializa7eza7(void);
	static obj_t BGl_internzd2initializa7ez12z67zzcnst_initializa7eza7(void);
	extern BgL_nodez00_bglt BGl_coercez12z12zzcoerce_coercez00(BgL_nodez00_bglt,
		obj_t, BgL_typez00_bglt, bool_t);
	static obj_t
		BGl_cnstzd2setzd2ze3cnstzd2stringz31zzcnst_initializa7eza7(obj_t);
	extern obj_t BGl_getzd2cnstzd2offsetz00zzcnst_allocz00(void);
	extern obj_t BGl_buildzd2astzd2zzast_buildz00(obj_t);
	static obj_t BGl_z62initializa7ezd2astz17zzcnst_initializa7eza7(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzcnst_initializa7eza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_allocz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_buildz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__intextz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t
		BGl_backendzd2cnstzd2tablezd2namezd2zzbackend_backendz00
		(BgL_backendz00_bglt, int);
	static obj_t BGl_readzd2initializa7ez12z67zzcnst_initializa7eza7(void);
	static obj_t
		BGl_readzd2emptyzd2cnstzd2initializa7ez12z67zzcnst_initializa7eza7(void);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static obj_t BGl_cnstzd2initzd2zzcnst_initializa7eza7(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcnst_initializa7eza7(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcnst_initializa7eza7(void);
	static obj_t BGl_gczd2rootszd2initz00zzcnst_initializa7eza7(void);
	BGL_EXPORTED_DECL obj_t BGl_initializa7ezd2astz75zzcnst_initializa7eza7(void);
	extern obj_t BGl_getzd2cnstzd2sexpz00zzcnst_allocz00(void);
	extern obj_t BGl_getzd2cnstzd2tablez00zzcnst_allocz00(void);
	static obj_t __cnst[29];


	   
		 
		DEFINE_STRING(BGl_string1857z00zzcnst_initializa7eza7,
		BgL_bgl_string1857za700za7za7c1863za7, "intialize-stop!", 15);
	      DEFINE_STRING(BGl_string1858z00zzcnst_initializa7eza7,
		BgL_bgl_string1858za700za7za7c1864za7, "Illegal init-mode", 17);
	      DEFINE_STRING(BGl_string1859z00zzcnst_initializa7eza7,
		BgL_bgl_string1859za700za7za7c1865za7, "cnst_initialize", 15);
	      DEFINE_STRING(BGl_string1860z00zzcnst_initializa7eza7,
		BgL_bgl_string1860za700za7za7c1866za7,
		"$vector-ref-ur cnst-tmp i::int cnst-tmp::vector string->obj __intext labels if begin let loop aux aux::int $-fx cnst-table-set! cport @ __reader $=fx i i::long cport::input-port open-input-string! unit cnst staged intern read lib ",
		230);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_initializa7ezd2astzd2envza7zzcnst_initializa7eza7,
		BgL_bgl_za762initializa7a7eza71867za7,
		BGl_z62initializa7ezd2astz17zzcnst_initializa7eza7, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzcnst_initializa7eza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzcnst_initializa7eza7(long
		BgL_checksumz00_1891, char *BgL_fromz00_1892)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcnst_initializa7eza7))
				{
					BGl_requirezd2initializa7ationz75zzcnst_initializa7eza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcnst_initializa7eza7();
					BGl_libraryzd2moduleszd2initz00zzcnst_initializa7eza7();
					BGl_cnstzd2initzd2zzcnst_initializa7eza7();
					BGl_importedzd2moduleszd2initz00zzcnst_initializa7eza7();
					return BGl_toplevelzd2initzd2zzcnst_initializa7eza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcnst_initializa7eza7(void)
	{
		{	/* Cnst/initialize.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"cnst_initialize");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cnst_initialize");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cnst_initialize");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"cnst_initialize");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"cnst_initialize");
			BGl_modulezd2initializa7ationz75zz__intextz00(0L, "cnst_initialize");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"cnst_initialize");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cnst_initialize");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cnst_initialize");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "cnst_initialize");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "cnst_initialize");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcnst_initializa7eza7(void)
	{
		{	/* Cnst/initialize.scm 15 */
			{	/* Cnst/initialize.scm 15 */
				obj_t BgL_cportz00_1880;

				{	/* Cnst/initialize.scm 15 */
					obj_t BgL_stringz00_1887;

					BgL_stringz00_1887 = BGl_string1860z00zzcnst_initializa7eza7;
					{	/* Cnst/initialize.scm 15 */
						obj_t BgL_startz00_1888;

						BgL_startz00_1888 = BINT(0L);
						{	/* Cnst/initialize.scm 15 */
							obj_t BgL_endz00_1889;

							BgL_endz00_1889 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1887)));
							{	/* Cnst/initialize.scm 15 */

								BgL_cportz00_1880 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1887, BgL_startz00_1888, BgL_endz00_1889);
				}}}}
				{
					long BgL_iz00_1881;

					BgL_iz00_1881 = 28L;
				BgL_loopz00_1882:
					if ((BgL_iz00_1881 == -1L))
						{	/* Cnst/initialize.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cnst/initialize.scm 15 */
							{	/* Cnst/initialize.scm 15 */
								obj_t BgL_arg1862z00_1883;

								{	/* Cnst/initialize.scm 15 */

									{	/* Cnst/initialize.scm 15 */
										obj_t BgL_locationz00_1885;

										BgL_locationz00_1885 = BBOOL(((bool_t) 0));
										{	/* Cnst/initialize.scm 15 */

											BgL_arg1862z00_1883 =
												BGl_readz00zz__readerz00(BgL_cportz00_1880,
												BgL_locationz00_1885);
										}
									}
								}
								{	/* Cnst/initialize.scm 15 */
									int BgL_tmpz00_1921;

									BgL_tmpz00_1921 = (int) (BgL_iz00_1881);
									CNST_TABLE_SET(BgL_tmpz00_1921, BgL_arg1862z00_1883);
							}}
							{	/* Cnst/initialize.scm 15 */
								int BgL_auxz00_1886;

								BgL_auxz00_1886 = (int) ((BgL_iz00_1881 - 1L));
								{
									long BgL_iz00_1926;

									BgL_iz00_1926 = (long) (BgL_auxz00_1886);
									BgL_iz00_1881 = BgL_iz00_1926;
									goto BgL_loopz00_1882;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcnst_initializa7eza7(void)
	{
		{	/* Cnst/initialize.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcnst_initializa7eza7(void)
	{
		{	/* Cnst/initialize.scm 15 */
			return BUNSPEC;
		}

	}



/* initialize-ast */
	BGL_EXPORTED_DEF obj_t BGl_initializa7ezd2astz75zzcnst_initializa7eza7(void)
	{
		{	/* Cnst/initialize.scm 41 */
			{	/* Cnst/initialize.scm 42 */
				obj_t BgL_bodyz00_1486;

				{	/* Cnst/initialize.scm 42 */
					obj_t BgL_casezd2valuezd2_1503;

					BgL_casezd2valuezd2_1503 = BGl_za2initzd2modeza2zd2zzengine_paramz00;
					if ((BgL_casezd2valuezd2_1503 == CNST_TABLE_REF(0)))
						{	/* Cnst/initialize.scm 42 */
							BgL_bodyz00_1486 =
								BGl_libzd2initializa7ez12z67zzcnst_initializa7eza7();
						}
					else
						{	/* Cnst/initialize.scm 42 */
							if ((BgL_casezd2valuezd2_1503 == CNST_TABLE_REF(1)))
								{	/* Cnst/initialize.scm 42 */
									BgL_bodyz00_1486 =
										BGl_readzd2initializa7ez12z67zzcnst_initializa7eza7();
								}
							else
								{	/* Cnst/initialize.scm 42 */
									if ((BgL_casezd2valuezd2_1503 == CNST_TABLE_REF(2)))
										{	/* Cnst/initialize.scm 42 */
											BgL_bodyz00_1486 =
												BGl_internzd2initializa7ez12z67zzcnst_initializa7eza7();
										}
									else
										{	/* Cnst/initialize.scm 42 */
											BgL_bodyz00_1486 =
												BGl_internalzd2errorzd2zztools_errorz00
												(BGl_string1857z00zzcnst_initializa7eza7,
												BGl_string1858z00zzcnst_initializa7eza7,
												BGl_za2initzd2modeza2zd2zzengine_paramz00);
										}
								}
						}
				}
				if (PAIRP(BgL_bodyz00_1486))
					{	/* Cnst/initialize.scm 54 */
						obj_t BgL_unitz00_1488;

						{	/* Cnst/initialize.scm 54 */
							bool_t BgL_arg1272z00_1502;

							BgL_arg1272z00_1502 =
								(BGl_za2objectzd2initzd2modeza2z00zzengine_paramz00 ==
								CNST_TABLE_REF(3));
							{	/* Cnst/initialize.scm 54 */
								obj_t BgL_idz00_1841;

								BgL_idz00_1841 = CNST_TABLE_REF(4);
								{	/* Cnst/initialize.scm 54 */
									obj_t BgL_newz00_1842;

									BgL_newz00_1842 =
										create_struct(CNST_TABLE_REF(5), (int) (5L));
									{	/* Cnst/initialize.scm 54 */
										obj_t BgL_auxz00_1952;
										int BgL_tmpz00_1950;

										BgL_auxz00_1952 = BBOOL(BgL_arg1272z00_1502);
										BgL_tmpz00_1950 = (int) (4L);
										STRUCT_SET(BgL_newz00_1842, BgL_tmpz00_1950,
											BgL_auxz00_1952);
									}
									{	/* Cnst/initialize.scm 54 */
										int BgL_tmpz00_1955;

										BgL_tmpz00_1955 = (int) (3L);
										STRUCT_SET(BgL_newz00_1842, BgL_tmpz00_1955, BTRUE);
									}
									{	/* Cnst/initialize.scm 54 */
										int BgL_tmpz00_1958;

										BgL_tmpz00_1958 = (int) (2L);
										STRUCT_SET(BgL_newz00_1842, BgL_tmpz00_1958,
											BgL_bodyz00_1486);
									}
									{	/* Cnst/initialize.scm 54 */
										obj_t BgL_auxz00_1963;
										int BgL_tmpz00_1961;

										BgL_auxz00_1963 = BINT(8L);
										BgL_tmpz00_1961 = (int) (1L);
										STRUCT_SET(BgL_newz00_1842, BgL_tmpz00_1961,
											BgL_auxz00_1963);
									}
									{	/* Cnst/initialize.scm 54 */
										int BgL_tmpz00_1966;

										BgL_tmpz00_1966 = (int) (0L);
										STRUCT_SET(BgL_newz00_1842, BgL_tmpz00_1966,
											BgL_idz00_1841);
									}
									BgL_unitz00_1488 = BgL_newz00_1842;
						}}}
						{	/* Cnst/initialize.scm 55 */
							obj_t BgL_astz00_1489;

							{	/* Cnst/initialize.scm 55 */
								obj_t BgL_arg1268z00_1500;

								{	/* Cnst/initialize.scm 55 */
									obj_t BgL_list1269z00_1501;

									BgL_list1269z00_1501 =
										MAKE_YOUNG_PAIR(BgL_unitz00_1488, BNIL);
									BgL_arg1268z00_1500 = BgL_list1269z00_1501;
								}
								BgL_astz00_1489 =
									BGl_buildzd2astzd2zzast_buildz00(BgL_arg1268z00_1500);
							}
							{
								obj_t BgL_l1224z00_1491;

								BgL_l1224z00_1491 = BgL_astz00_1489;
							BgL_zc3z04anonymousza31246ze3z87_1492:
								if (PAIRP(BgL_l1224z00_1491))
									{	/* Cnst/initialize.scm 56 */
										{	/* Cnst/initialize.scm 57 */
											obj_t BgL_globalz00_1494;

											BgL_globalz00_1494 = CAR(BgL_l1224z00_1491);
											{	/* Cnst/initialize.scm 57 */
												obj_t BgL_bodyz00_1495;

												BgL_bodyz00_1495 =
													(((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_globalz00_bglt)
																					BgL_globalz00_1494))))->
																	BgL_valuez00))))->BgL_bodyz00);
												BGl_lvtypezd2nodez12zc0zzast_lvtypez00((
														(BgL_nodez00_bglt) BgL_bodyz00_1495));
												{	/* Cnst/initialize.scm 59 */
													BgL_typez00_bglt BgL_arg1248z00_1496;

													BgL_arg1248z00_1496 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_globalz00_bglt) BgL_globalz00_1494))))->
														BgL_typez00);
													BGl_coercez12z12zzcoerce_coercez00(((BgL_nodez00_bglt)
															BgL_bodyz00_1495), BgL_globalz00_1494,
														BgL_arg1248z00_1496, ((bool_t) 0));
												}
											}
										}
										{
											obj_t BgL_l1224z00_1986;

											BgL_l1224z00_1986 = CDR(BgL_l1224z00_1491);
											BgL_l1224z00_1491 = BgL_l1224z00_1986;
											goto BgL_zc3z04anonymousza31246ze3z87_1492;
										}
									}
								else
									{	/* Cnst/initialize.scm 56 */
										((bool_t) 1);
									}
							}
							return BgL_astz00_1489;
						}
					}
				else
					{	/* Cnst/initialize.scm 53 */
						return BNIL;
					}
			}
		}

	}



/* &initialize-ast */
	obj_t BGl_z62initializa7ezd2astz17zzcnst_initializa7eza7(obj_t
		BgL_envz00_1878)
	{
		{	/* Cnst/initialize.scm 41 */
			return BGl_initializa7ezd2astz75zzcnst_initializa7eza7();
		}

	}



/* lib-initialize! */
	obj_t BGl_libzd2initializa7ez12z67zzcnst_initializa7eza7(void)
	{
		{	/* Cnst/initialize.scm 67 */
			{	/* Cnst/initialize.scm 68 */
				obj_t BgL_arg1284z00_1507;
				obj_t BgL_arg1304z00_1508;

				BgL_arg1284z00_1507 = BGl_getzd2cnstzd2tablez00zzcnst_allocz00();
				{	/* Cnst/initialize.scm 68 */
					obj_t BgL_arg1305z00_1509;

					BgL_arg1305z00_1509 = BGl_thezd2backendzd2zzbackend_backendz00();
					BgL_arg1304z00_1508 =
						BGl_backendzd2cnstzd2tablezd2namezd2zzbackend_backendz00(
						((BgL_backendz00_bglt) BgL_arg1305z00_1509), (int) (0L));
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_arg1284z00_1507))))->BgL_namez00) =
					((obj_t) BgL_arg1304z00_1508), BUNSPEC);
			}
			{	/* Cnst/initialize.scm 69 */
				obj_t BgL_arg1306z00_1510;

				BgL_arg1306z00_1510 = BGl_getzd2cnstzd2tablez00zzcnst_allocz00();
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_arg1306z00_1510))))->
						BgL_userzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
			}
			BGL_TAIL return BGl_getzd2cnstzd2sexpz00zzcnst_allocz00();
		}

	}



/* read-initialize! */
	obj_t BGl_readzd2initializa7ez12z67zzcnst_initializa7eza7(void)
	{
		{	/* Cnst/initialize.scm 75 */
			{	/* Cnst/initialize.scm 76 */
				bool_t BgL_test1875z00_2002;

				{	/* Cnst/initialize.scm 76 */
					obj_t BgL_arg1310z00_1513;

					BgL_arg1310z00_1513 = BGl_getzd2cnstzd2offsetz00zzcnst_allocz00();
					BgL_test1875z00_2002 = ((long) CINT(BgL_arg1310z00_1513) == 0L);
				}
				if (BgL_test1875z00_2002)
					{	/* Cnst/initialize.scm 76 */
						BGL_TAIL return
							BGl_readzd2emptyzd2cnstzd2initializa7ez12z67zzcnst_initializa7eza7
							();
					}
				else
					{	/* Cnst/initialize.scm 76 */
						BGL_TAIL return
							BGl_readzd2fullzd2cnstzd2initializa7ez12z67zzcnst_initializa7eza7
							();
					}
			}
		}

	}



/* read-empty-cnst-initialize! */
	obj_t BGl_readzd2emptyzd2cnstzd2initializa7ez12z67zzcnst_initializa7eza7(void)
	{
		{	/* Cnst/initialize.scm 83 */
			{	/* Cnst/initialize.scm 84 */
				obj_t BgL_arg1311z00_1514;
				obj_t BgL_arg1312z00_1515;

				BgL_arg1311z00_1514 = BGl_getzd2cnstzd2tablez00zzcnst_allocz00();
				{	/* Cnst/initialize.scm 84 */
					obj_t BgL_arg1314z00_1516;

					BgL_arg1314z00_1516 = BGl_thezd2backendzd2zzbackend_backendz00();
					BgL_arg1312z00_1515 =
						BGl_backendzd2cnstzd2tablezd2namezd2zzbackend_backendz00(
						((BgL_backendz00_bglt) BgL_arg1314z00_1516), (int) (0L));
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_arg1311z00_1514))))->BgL_namez00) =
					((obj_t) BgL_arg1312z00_1515), BUNSPEC);
			}
			{	/* Cnst/initialize.scm 85 */
				obj_t BgL_arg1315z00_1517;

				BgL_arg1315z00_1517 = BGl_getzd2cnstzd2tablez00zzcnst_allocz00();
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_arg1315z00_1517))))->
						BgL_userzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
			}
			BGL_TAIL return BGl_getzd2cnstzd2sexpz00zzcnst_allocz00();
		}

	}



/* read-full-cnst-initialize! */
	obj_t BGl_readzd2fullzd2cnstzd2initializa7ez12z67zzcnst_initializa7eza7(void)
	{
		{	/* Cnst/initialize.scm 91 */
			{
				obj_t BgL_cnstzd2stringzd2_1526;

				{	/* Cnst/initialize.scm 110 */
					obj_t BgL_arg1316z00_1519;
					obj_t BgL_arg1317z00_1520;

					BgL_arg1316z00_1519 = BGl_getzd2cnstzd2tablez00zzcnst_allocz00();
					{	/* Cnst/initialize.scm 111 */
						obj_t BgL_arg1318z00_1521;
						obj_t BgL_arg1319z00_1522;

						BgL_arg1318z00_1521 = BGl_thezd2backendzd2zzbackend_backendz00();
						BgL_arg1319z00_1522 = BGl_getzd2cnstzd2offsetz00zzcnst_allocz00();
						BgL_arg1317z00_1520 =
							BGl_backendzd2cnstzd2tablezd2namezd2zzbackend_backendz00(
							((BgL_backendz00_bglt) BgL_arg1318z00_1521),
							CINT(BgL_arg1319z00_1522));
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_arg1316z00_1519))))->
							BgL_namez00) = ((obj_t) BgL_arg1317z00_1520), BUNSPEC);
				}
				{	/* Cnst/initialize.scm 112 */
					obj_t BgL_arg1320z00_1523;

					BgL_arg1320z00_1523 = BGl_getzd2cnstzd2tablez00zzcnst_allocz00();
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_arg1320z00_1523))))->
							BgL_userzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				BgL_cnstzd2stringzd2_1526 =
					BGl_cnstzd2setzd2ze3cnstzd2stringz31zzcnst_initializa7eza7
					(BGl_getzd2cnstzd2setz00zzcnst_allocz00());
				{	/* Cnst/initialize.scm 94 */
					BgL_nodez00_bglt BgL_varzd2stringzd2_1528;

					BgL_varzd2stringzd2_1528 =
						BGl_cnstzd2alloczd2stringz00zzcnst_allocz00
						(BgL_cnstzd2stringzd2_1526, BFALSE);
					{	/* Cnst/initialize.scm 94 */
						obj_t BgL_sexpz00_1529;

						{	/* Cnst/initialize.scm 97 */
							obj_t BgL_arg1325z00_1531;

							{	/* Cnst/initialize.scm 97 */
								obj_t BgL_arg1326z00_1532;
								obj_t BgL_arg1327z00_1533;

								{	/* Cnst/initialize.scm 97 */
									obj_t BgL_arg1328z00_1534;

									{	/* Cnst/initialize.scm 97 */
										obj_t BgL_arg1329z00_1535;

										{	/* Cnst/initialize.scm 97 */
											obj_t BgL_arg1331z00_1536;

											{	/* Cnst/initialize.scm 97 */
												obj_t BgL_arg1332z00_1537;

												{	/* Cnst/initialize.scm 97 */
													obj_t BgL_arg1333z00_1538;

													BgL_arg1333z00_1538 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_globalz00_bglt)
																		(((BgL_varz00_bglt) COBJECT(
																					((BgL_varz00_bglt)
																						BgL_varzd2stringzd2_1528)))->
																			BgL_variablez00)))))->BgL_idz00);
													BgL_arg1332z00_1537 =
														MAKE_YOUNG_PAIR(BgL_arg1333z00_1538, BNIL);
												}
												BgL_arg1331z00_1536 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
													BgL_arg1332z00_1537);
											}
											BgL_arg1329z00_1535 =
												MAKE_YOUNG_PAIR(BgL_arg1331z00_1536, BNIL);
										}
										BgL_arg1328z00_1534 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1329z00_1535);
									}
									BgL_arg1326z00_1532 =
										MAKE_YOUNG_PAIR(BgL_arg1328z00_1534, BNIL);
								}
								{	/* Cnst/initialize.scm 98 */
									obj_t BgL_arg1339z00_1540;

									{	/* Cnst/initialize.scm 98 */
										obj_t BgL_arg1340z00_1541;

										{	/* Cnst/initialize.scm 98 */
											obj_t BgL_arg1342z00_1542;
											obj_t BgL_arg1343z00_1543;

											{	/* Cnst/initialize.scm 98 */
												obj_t BgL_arg1346z00_1544;

												{	/* Cnst/initialize.scm 98 */
													obj_t BgL_arg1348z00_1545;

													{	/* Cnst/initialize.scm 98 */
														obj_t BgL_arg1349z00_1546;
														obj_t BgL_arg1351z00_1547;

														BgL_arg1349z00_1546 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(8), BNIL);
														{	/* Cnst/initialize.scm 99 */
															obj_t BgL_arg1352z00_1548;

															{	/* Cnst/initialize.scm 99 */
																obj_t BgL_arg1361z00_1549;

																{	/* Cnst/initialize.scm 99 */
																	obj_t BgL_arg1364z00_1550;
																	obj_t BgL_arg1367z00_1551;

																	{	/* Cnst/initialize.scm 99 */
																		obj_t BgL_arg1370z00_1552;

																		{	/* Cnst/initialize.scm 99 */
																			obj_t BgL_arg1371z00_1553;

																			BgL_arg1371z00_1553 =
																				MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
																			BgL_arg1370z00_1552 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
																				BgL_arg1371z00_1553);
																		}
																		BgL_arg1364z00_1550 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																			BgL_arg1370z00_1552);
																	}
																	{	/* Cnst/initialize.scm 104 */
																		obj_t BgL_arg1375z00_1554;

																		{	/* Cnst/initialize.scm 104 */
																			obj_t BgL_arg1376z00_1555;

																			{	/* Cnst/initialize.scm 104 */
																				obj_t BgL_arg1377z00_1556;

																				{	/* Cnst/initialize.scm 104 */
																					obj_t BgL_arg1378z00_1557;
																					obj_t BgL_arg1379z00_1558;

																					{	/* Cnst/initialize.scm 104 */
																						obj_t BgL_arg1380z00_1559;

																						{	/* Cnst/initialize.scm 104 */
																							obj_t BgL_arg1408z00_1560;

																							{	/* Cnst/initialize.scm 104 */
																								obj_t BgL_arg1410z00_1561;

																								{	/* Cnst/initialize.scm 104 */
																									obj_t BgL_arg1421z00_1562;
																									obj_t BgL_arg1422z00_1563;

																									{	/* Cnst/initialize.scm 104 */
																										obj_t BgL_arg1434z00_1564;

																										{	/* Cnst/initialize.scm 104 */
																											obj_t BgL_arg1437z00_1565;

																											BgL_arg1437z00_1565 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(11),
																												BNIL);
																											BgL_arg1434z00_1564 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(1),
																												BgL_arg1437z00_1565);
																										}
																										BgL_arg1421z00_1562 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(12),
																											BgL_arg1434z00_1564);
																									}
																									BgL_arg1422z00_1563 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(13), BNIL);
																									BgL_arg1410z00_1561 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1421z00_1562,
																										BgL_arg1422z00_1563);
																								}
																								BgL_arg1408z00_1560 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1410z00_1561, BNIL);
																							}
																							BgL_arg1380z00_1559 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(9), BgL_arg1408z00_1560);
																						}
																						BgL_arg1378z00_1557 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(14), BgL_arg1380z00_1559);
																					}
																					{	/* Cnst/initialize.scm 105 */
																						obj_t BgL_arg1448z00_1566;

																						{	/* Cnst/initialize.scm 105 */
																							obj_t BgL_arg1453z00_1567;

																							{	/* Cnst/initialize.scm 105 */
																								obj_t BgL_arg1454z00_1568;
																								obj_t BgL_arg1472z00_1569;

																								{	/* Cnst/initialize.scm 105 */
																									obj_t BgL_arg1473z00_1570;

																									{	/* Cnst/initialize.scm 105 */
																										obj_t BgL_arg1485z00_1571;

																										{	/* Cnst/initialize.scm 105 */
																											obj_t BgL_arg1489z00_1572;

																											{	/* Cnst/initialize.scm 105 */
																												obj_t
																													BgL_arg1502z00_1573;
																												{	/* Cnst/initialize.scm 105 */
																													obj_t
																														BgL_arg1509z00_1574;
																													BgL_arg1509z00_1574 =
																														MAKE_YOUNG_PAIR(BINT
																														(1L), BNIL);
																													BgL_arg1502z00_1573 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(9),
																														BgL_arg1509z00_1574);
																												}
																												BgL_arg1489z00_1572 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(15),
																													BgL_arg1502z00_1573);
																											}
																											BgL_arg1485z00_1571 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1489z00_1572,
																												BNIL);
																										}
																										BgL_arg1473z00_1570 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(16),
																											BgL_arg1485z00_1571);
																									}
																									BgL_arg1454z00_1568 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1473z00_1570, BNIL);
																								}
																								{	/* Cnst/initialize.scm 106 */
																									obj_t BgL_arg1513z00_1575;

																									{	/* Cnst/initialize.scm 106 */
																										obj_t BgL_arg1514z00_1576;

																										BgL_arg1514z00_1576 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(17),
																											BNIL);
																										BgL_arg1513z00_1575 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(18),
																											BgL_arg1514z00_1576);
																									}
																									BgL_arg1472z00_1569 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1513z00_1575, BNIL);
																								}
																								BgL_arg1453z00_1567 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1454z00_1568,
																									BgL_arg1472z00_1569);
																							}
																							BgL_arg1448z00_1566 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(19), BgL_arg1453z00_1567);
																						}
																						BgL_arg1379z00_1558 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1448z00_1566, BNIL);
																					}
																					BgL_arg1377z00_1556 =
																						MAKE_YOUNG_PAIR(BgL_arg1378z00_1557,
																						BgL_arg1379z00_1558);
																				}
																				BgL_arg1376z00_1555 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(20),
																					BgL_arg1377z00_1556);
																			}
																			BgL_arg1375z00_1554 =
																				MAKE_YOUNG_PAIR(BgL_arg1376z00_1555,
																				BNIL);
																		}
																		BgL_arg1367z00_1551 =
																			MAKE_YOUNG_PAIR(BUNSPEC,
																			BgL_arg1375z00_1554);
																	}
																	BgL_arg1361z00_1549 =
																		MAKE_YOUNG_PAIR(BgL_arg1364z00_1550,
																		BgL_arg1367z00_1551);
																}
																BgL_arg1352z00_1548 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(21),
																	BgL_arg1361z00_1549);
															}
															BgL_arg1351z00_1547 =
																MAKE_YOUNG_PAIR(BgL_arg1352z00_1548, BNIL);
														}
														BgL_arg1348z00_1545 =
															MAKE_YOUNG_PAIR(BgL_arg1349z00_1546,
															BgL_arg1351z00_1547);
													}
													BgL_arg1346z00_1544 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
														BgL_arg1348z00_1545);
												}
												BgL_arg1342z00_1542 =
													MAKE_YOUNG_PAIR(BgL_arg1346z00_1544, BNIL);
											}
											{	/* Cnst/initialize.scm 107 */
												obj_t BgL_arg1516z00_1577;

												{	/* Cnst/initialize.scm 107 */
													obj_t BgL_arg1535z00_1578;

													{	/* Cnst/initialize.scm 107 */
														long BgL_arg1540z00_1579;

														{	/* Cnst/initialize.scm 107 */
															obj_t BgL_arg1544z00_1580;

															BgL_arg1544z00_1580 =
																BGl_getzd2cnstzd2offsetz00zzcnst_allocz00();
															BgL_arg1540z00_1579 =
																((long) CINT(BgL_arg1544z00_1580) - 1L);
														}
														BgL_arg1535z00_1578 =
															MAKE_YOUNG_PAIR(BINT(BgL_arg1540z00_1579), BNIL);
													}
													BgL_arg1516z00_1577 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
														BgL_arg1535z00_1578);
												}
												BgL_arg1343z00_1543 =
													MAKE_YOUNG_PAIR(BgL_arg1516z00_1577, BNIL);
											}
											BgL_arg1340z00_1541 =
												MAKE_YOUNG_PAIR(BgL_arg1342z00_1542,
												BgL_arg1343z00_1543);
										}
										BgL_arg1339z00_1540 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(22), BgL_arg1340z00_1541);
									}
									BgL_arg1327z00_1533 =
										MAKE_YOUNG_PAIR(BgL_arg1339z00_1540, BNIL);
								}
								BgL_arg1325z00_1531 =
									MAKE_YOUNG_PAIR(BgL_arg1326z00_1532, BgL_arg1327z00_1533);
							}
							BgL_sexpz00_1529 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(19), BgL_arg1325z00_1531);
						}
						{	/* Cnst/initialize.scm 95 */

							return
								MAKE_YOUNG_PAIR(BgL_sexpz00_1529,
								BGl_getzd2cnstzd2sexpz00zzcnst_allocz00());
						}
					}
				}
			}
		}

	}



/* cnst-set->cnst-string */
	obj_t BGl_cnstzd2setzd2ze3cnstzd2stringz31zzcnst_initializa7eza7(obj_t
		BgL_setz00_25)
	{
		{	/* Cnst/initialize.scm 123 */
			{	/* Cnst/initialize.scm 124 */
				obj_t BgL_portz00_1582;

				{	/* Cnst/initialize.scm 124 */

					{	/* Cnst/initialize.scm 124 */

						BgL_portz00_1582 =
							BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(BTRUE);
					}
				}
				{
					obj_t BgL_l1226z00_1585;

					BgL_l1226z00_1585 = BgL_setz00_25;
				BgL_zc3z04anonymousza31547ze3z87_1586:
					if (PAIRP(BgL_l1226z00_1585))
						{	/* Cnst/initialize.scm 130 */
							{	/* Cnst/initialize.scm 131 */
								obj_t BgL_cnstz00_1588;

								BgL_cnstz00_1588 = CAR(BgL_l1226z00_1585);
								{	/* Cnst/initialize.scm 131 */
									obj_t BgL_list1549z00_1589;

									BgL_list1549z00_1589 =
										MAKE_YOUNG_PAIR(BgL_portz00_1582, BNIL);
									BGl_writez00zz__r4_output_6_10_3z00(BgL_cnstz00_1588,
										BgL_list1549z00_1589);
								}
								bgl_display_char(((unsigned char) ' '), BgL_portz00_1582);
							}
							{
								obj_t BgL_l1226z00_2127;

								BgL_l1226z00_2127 = CDR(BgL_l1226z00_1585);
								BgL_l1226z00_1585 = BgL_l1226z00_2127;
								goto BgL_zc3z04anonymousza31547ze3z87_1586;
							}
						}
					else
						{	/* Cnst/initialize.scm 130 */
							((bool_t) 1);
						}
				}
				return bgl_close_output_port(BgL_portz00_1582);
			}
		}

	}



/* intern-initialize! */
	obj_t BGl_internzd2initializa7ez12z67zzcnst_initializa7eza7(void)
	{
		{	/* Cnst/initialize.scm 139 */
			{	/* Cnst/initialize.scm 140 */
				bool_t BgL_test1877z00_2130;

				{	/* Cnst/initialize.scm 140 */
					obj_t BgL_arg1559z00_1595;

					BgL_arg1559z00_1595 = BGl_getzd2cnstzd2offsetz00zzcnst_allocz00();
					BgL_test1877z00_2130 = ((long) CINT(BgL_arg1559z00_1595) == 0L);
				}
				if (BgL_test1877z00_2130)
					{	/* Cnst/initialize.scm 140 */
						BGL_TAIL return
							BGl_internzd2emptyzd2cnstzd2initializa7ez12z67zzcnst_initializa7eza7
							();
					}
				else
					{	/* Cnst/initialize.scm 140 */
						BGL_TAIL return
							BGl_internzd2fullzd2cnstzd2initializa7ez12z67zzcnst_initializa7eza7
							();
					}
			}
		}

	}



/* intern-empty-cnst-initialize! */
	obj_t
		BGl_internzd2emptyzd2cnstzd2initializa7ez12z67zzcnst_initializa7eza7(void)
	{
		{	/* Cnst/initialize.scm 147 */
			{	/* Cnst/initialize.scm 148 */
				obj_t BgL_arg1561z00_1596;
				obj_t BgL_arg1564z00_1597;

				BgL_arg1561z00_1596 = BGl_getzd2cnstzd2tablez00zzcnst_allocz00();
				{	/* Cnst/initialize.scm 149 */
					obj_t BgL_arg1565z00_1598;

					BgL_arg1565z00_1598 = BGl_thezd2backendzd2zzbackend_backendz00();
					BgL_arg1564z00_1597 =
						BGl_backendzd2cnstzd2tablezd2namezd2zzbackend_backendz00(
						((BgL_backendz00_bglt) BgL_arg1565z00_1598), (int) (0L));
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_arg1561z00_1596))))->BgL_namez00) =
					((obj_t) BgL_arg1564z00_1597), BUNSPEC);
			}
			BGL_TAIL return BGl_getzd2cnstzd2sexpz00zzcnst_allocz00();
		}

	}



/* intern-full-cnst-initialize! */
	obj_t
		BGl_internzd2fullzd2cnstzd2initializa7ez12z67zzcnst_initializa7eza7(void)
	{
		{	/* Cnst/initialize.scm 155 */
			{
				obj_t BgL_cnstzd2stringzd2_1610;

				{	/* Cnst/initialize.scm 175 */
					obj_t BgL_arg1571z00_1600;
					obj_t BgL_arg1573z00_1601;

					BgL_arg1571z00_1600 = BGl_getzd2cnstzd2tablez00zzcnst_allocz00();
					{	/* Cnst/initialize.scm 176 */
						obj_t BgL_arg1575z00_1602;
						obj_t BgL_arg1576z00_1603;

						BgL_arg1575z00_1602 = BGl_thezd2backendzd2zzbackend_backendz00();
						BgL_arg1576z00_1603 = BGl_getzd2cnstzd2offsetz00zzcnst_allocz00();
						BgL_arg1573z00_1601 =
							BGl_backendzd2cnstzd2tablezd2namezd2zzbackend_backendz00(
							((BgL_backendz00_bglt) BgL_arg1575z00_1602),
							CINT(BgL_arg1576z00_1603));
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_arg1571z00_1600))))->
							BgL_namez00) = ((obj_t) BgL_arg1573z00_1601), BUNSPEC);
				}
				{	/* Cnst/initialize.scm 178 */
					obj_t BgL_cnstzd2stringzd2_1604;

					{	/* Cnst/initialize.scm 178 */
						obj_t BgL_arg1584z00_1605;

						BgL_arg1584z00_1605 =
							BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(bgl_reverse_bang
							(BGl_getzd2cnstzd2setz00zzcnst_allocz00()));
						{	/* Cnst/initialize.scm 178 */

							BgL_cnstzd2stringzd2_1604 =
								obj_to_string(BgL_arg1584z00_1605, BFALSE);
					}}
					{	/* Cnst/initialize.scm 178 */

						BgL_cnstzd2stringzd2_1610 = BgL_cnstzd2stringzd2_1604;
						{	/* Cnst/initialize.scm 158 */
							BgL_nodez00_bglt BgL_varzd2stringzd2_1612;

							BgL_varzd2stringzd2_1612 =
								BGl_cnstzd2alloczd2stringz00zzcnst_allocz00
								(BgL_cnstzd2stringzd2_1610, BFALSE);
							{	/* Cnst/initialize.scm 158 */
								obj_t BgL_sexpz00_1613;

								{	/* Cnst/initialize.scm 160 */
									obj_t BgL_arg1593z00_1615;

									{	/* Cnst/initialize.scm 160 */
										obj_t BgL_arg1594z00_1616;
										obj_t BgL_arg1595z00_1617;

										{	/* Cnst/initialize.scm 160 */
											obj_t BgL_arg1602z00_1618;

											{	/* Cnst/initialize.scm 160 */
												obj_t BgL_arg1605z00_1619;

												{	/* Cnst/initialize.scm 160 */
													obj_t BgL_arg1606z00_1620;

													{	/* Cnst/initialize.scm 160 */
														obj_t BgL_arg1609z00_1621;
														obj_t BgL_arg1611z00_1622;

														{	/* Cnst/initialize.scm 160 */
															obj_t BgL_arg1613z00_1623;

															{	/* Cnst/initialize.scm 160 */
																obj_t BgL_arg1615z00_1624;

																BgL_arg1615z00_1624 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(23), BNIL);
																BgL_arg1613z00_1623 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(24),
																	BgL_arg1615z00_1624);
															}
															BgL_arg1609z00_1621 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
																BgL_arg1613z00_1623);
														}
														{	/* Cnst/initialize.scm 162 */
															obj_t BgL_arg1616z00_1625;

															BgL_arg1616z00_1625 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_globalz00_bglt)
																				(((BgL_varz00_bglt) COBJECT(
																							((BgL_varz00_bglt)
																								BgL_varzd2stringzd2_1612)))->
																					BgL_variablez00)))))->BgL_idz00);
															BgL_arg1611z00_1622 =
																MAKE_YOUNG_PAIR(BgL_arg1616z00_1625, BNIL);
														}
														BgL_arg1606z00_1620 =
															MAKE_YOUNG_PAIR(BgL_arg1609z00_1621,
															BgL_arg1611z00_1622);
													}
													BgL_arg1605z00_1619 =
														MAKE_YOUNG_PAIR(BgL_arg1606z00_1620, BNIL);
												}
												BgL_arg1602z00_1618 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(25),
													BgL_arg1605z00_1619);
											}
											BgL_arg1594z00_1616 =
												MAKE_YOUNG_PAIR(BgL_arg1602z00_1618, BNIL);
										}
										{	/* Cnst/initialize.scm 163 */
											obj_t BgL_arg1626z00_1627;

											{	/* Cnst/initialize.scm 163 */
												obj_t BgL_arg1627z00_1628;

												{	/* Cnst/initialize.scm 163 */
													obj_t BgL_arg1629z00_1629;
													obj_t BgL_arg1630z00_1630;

													{	/* Cnst/initialize.scm 163 */
														obj_t BgL_arg1642z00_1631;

														{	/* Cnst/initialize.scm 163 */
															obj_t BgL_arg1646z00_1632;

															{	/* Cnst/initialize.scm 163 */
																obj_t BgL_arg1650z00_1633;
																obj_t BgL_arg1651z00_1634;

																BgL_arg1650z00_1633 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(26), BNIL);
																{	/* Cnst/initialize.scm 164 */
																	obj_t BgL_arg1654z00_1635;

																	{	/* Cnst/initialize.scm 164 */
																		obj_t BgL_arg1661z00_1636;

																		{	/* Cnst/initialize.scm 164 */
																			obj_t BgL_arg1663z00_1637;
																			obj_t BgL_arg1675z00_1638;

																			{	/* Cnst/initialize.scm 164 */
																				obj_t BgL_arg1678z00_1639;

																				{	/* Cnst/initialize.scm 164 */
																					obj_t BgL_arg1681z00_1640;

																					BgL_arg1681z00_1640 =
																						MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
																					BgL_arg1678z00_1639 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
																						BgL_arg1681z00_1640);
																				}
																				BgL_arg1663z00_1637 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																					BgL_arg1678z00_1639);
																			}
																			{	/* Cnst/initialize.scm 169 */
																				obj_t BgL_arg1688z00_1641;

																				{	/* Cnst/initialize.scm 169 */
																					obj_t BgL_arg1689z00_1642;

																					{	/* Cnst/initialize.scm 169 */
																						obj_t BgL_arg1691z00_1643;

																						{	/* Cnst/initialize.scm 169 */
																							obj_t BgL_arg1692z00_1644;
																							obj_t BgL_arg1699z00_1645;

																							{	/* Cnst/initialize.scm 169 */
																								obj_t BgL_arg1700z00_1646;

																								{	/* Cnst/initialize.scm 169 */
																									obj_t BgL_arg1701z00_1647;

																									{	/* Cnst/initialize.scm 169 */
																										obj_t BgL_arg1702z00_1648;

																										{	/* Cnst/initialize.scm 169 */
																											obj_t BgL_arg1703z00_1649;

																											{	/* Cnst/initialize.scm 169 */
																												obj_t
																													BgL_arg1705z00_1650;
																												BgL_arg1705z00_1650 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(9),
																													BNIL);
																												BgL_arg1703z00_1649 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(27),
																													BgL_arg1705z00_1650);
																											}
																											BgL_arg1702z00_1648 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(28),
																												BgL_arg1703z00_1649);
																										}
																										BgL_arg1701z00_1647 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1702z00_1648,
																											BNIL);
																									}
																									BgL_arg1700z00_1646 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(9),
																										BgL_arg1701z00_1647);
																								}
																								BgL_arg1692z00_1644 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(14), BgL_arg1700z00_1646);
																							}
																							{	/* Cnst/initialize.scm 170 */
																								obj_t BgL_arg1708z00_1651;

																								{	/* Cnst/initialize.scm 170 */
																									obj_t BgL_arg1709z00_1652;

																									{	/* Cnst/initialize.scm 170 */
																										obj_t BgL_arg1710z00_1653;
																										obj_t BgL_arg1711z00_1654;

																										{	/* Cnst/initialize.scm 170 */
																											obj_t BgL_arg1714z00_1655;

																											{	/* Cnst/initialize.scm 170 */
																												obj_t
																													BgL_arg1717z00_1656;
																												{	/* Cnst/initialize.scm 170 */
																													obj_t
																														BgL_arg1718z00_1657;
																													{	/* Cnst/initialize.scm 170 */
																														obj_t
																															BgL_arg1720z00_1658;
																														{	/* Cnst/initialize.scm 170 */
																															obj_t
																																BgL_arg1722z00_1659;
																															BgL_arg1722z00_1659
																																=
																																MAKE_YOUNG_PAIR
																																(BINT(1L),
																																BNIL);
																															BgL_arg1720z00_1658
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(9),
																																BgL_arg1722z00_1659);
																														}
																														BgL_arg1718z00_1657
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(15),
																															BgL_arg1720z00_1658);
																													}
																													BgL_arg1717z00_1656 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1718z00_1657,
																														BNIL);
																												}
																												BgL_arg1714z00_1655 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(16),
																													BgL_arg1717z00_1656);
																											}
																											BgL_arg1710z00_1653 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1714z00_1655,
																												BNIL);
																										}
																										{	/* Cnst/initialize.scm 171 */
																											obj_t BgL_arg1724z00_1660;

																											{	/* Cnst/initialize.scm 171 */
																												obj_t
																													BgL_arg1733z00_1661;
																												BgL_arg1733z00_1661 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(17),
																													BNIL);
																												BgL_arg1724z00_1660 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(18),
																													BgL_arg1733z00_1661);
																											}
																											BgL_arg1711z00_1654 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1724z00_1660,
																												BNIL);
																										}
																										BgL_arg1709z00_1652 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1710z00_1653,
																											BgL_arg1711z00_1654);
																									}
																									BgL_arg1708z00_1651 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(19),
																										BgL_arg1709z00_1652);
																								}
																								BgL_arg1699z00_1645 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1708z00_1651, BNIL);
																							}
																							BgL_arg1691z00_1643 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1692z00_1644,
																								BgL_arg1699z00_1645);
																						}
																						BgL_arg1689z00_1642 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(20), BgL_arg1691z00_1643);
																					}
																					BgL_arg1688z00_1641 =
																						MAKE_YOUNG_PAIR(BgL_arg1689z00_1642,
																						BNIL);
																				}
																				BgL_arg1675z00_1638 =
																					MAKE_YOUNG_PAIR(BUNSPEC,
																					BgL_arg1688z00_1641);
																			}
																			BgL_arg1661z00_1636 =
																				MAKE_YOUNG_PAIR(BgL_arg1663z00_1637,
																				BgL_arg1675z00_1638);
																		}
																		BgL_arg1654z00_1635 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(21),
																			BgL_arg1661z00_1636);
																	}
																	BgL_arg1651z00_1634 =
																		MAKE_YOUNG_PAIR(BgL_arg1654z00_1635, BNIL);
																}
																BgL_arg1646z00_1632 =
																	MAKE_YOUNG_PAIR(BgL_arg1650z00_1633,
																	BgL_arg1651z00_1634);
															}
															BgL_arg1642z00_1631 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																BgL_arg1646z00_1632);
														}
														BgL_arg1629z00_1629 =
															MAKE_YOUNG_PAIR(BgL_arg1642z00_1631, BNIL);
													}
													{	/* Cnst/initialize.scm 172 */
														obj_t BgL_arg1734z00_1662;

														{	/* Cnst/initialize.scm 172 */
															obj_t BgL_arg1735z00_1663;

															{	/* Cnst/initialize.scm 172 */
																long BgL_arg1736z00_1664;

																{	/* Cnst/initialize.scm 172 */
																	obj_t BgL_arg1737z00_1665;

																	BgL_arg1737z00_1665 =
																		BGl_getzd2cnstzd2offsetz00zzcnst_allocz00();
																	BgL_arg1736z00_1664 =
																		((long) CINT(BgL_arg1737z00_1665) - 1L);
																}
																BgL_arg1735z00_1663 =
																	MAKE_YOUNG_PAIR(BINT(BgL_arg1736z00_1664),
																	BNIL);
															}
															BgL_arg1734z00_1662 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																BgL_arg1735z00_1663);
														}
														BgL_arg1630z00_1630 =
															MAKE_YOUNG_PAIR(BgL_arg1734z00_1662, BNIL);
													}
													BgL_arg1627z00_1628 =
														MAKE_YOUNG_PAIR(BgL_arg1629z00_1629,
														BgL_arg1630z00_1630);
												}
												BgL_arg1626z00_1627 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(22),
													BgL_arg1627z00_1628);
											}
											BgL_arg1595z00_1617 =
												MAKE_YOUNG_PAIR(BgL_arg1626z00_1627, BNIL);
										}
										BgL_arg1593z00_1615 =
											MAKE_YOUNG_PAIR(BgL_arg1594z00_1616, BgL_arg1595z00_1617);
									}
									BgL_sexpz00_1613 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(19), BgL_arg1593z00_1615);
								}
								{	/* Cnst/initialize.scm 159 */

									return
										MAKE_YOUNG_PAIR(BgL_sexpz00_1613,
										BGl_getzd2cnstzd2sexpz00zzcnst_allocz00());
								}
							}
						}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcnst_initializa7eza7(void)
	{
		{	/* Cnst/initialize.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcnst_initializa7eza7(void)
	{
		{	/* Cnst/initialize.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcnst_initializa7eza7(void)
	{
		{	/* Cnst/initialize.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcnst_initializa7eza7(void)
	{
		{	/* Cnst/initialize.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1859z00zzcnst_initializa7eza7));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1859z00zzcnst_initializa7eza7));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1859z00zzcnst_initializa7eza7));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1859z00zzcnst_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1859z00zzcnst_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1859z00zzcnst_initializa7eza7));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1859z00zzcnst_initializa7eza7));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1859z00zzcnst_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1859z00zzcnst_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1859z00zzcnst_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1859z00zzcnst_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzast_buildz00(428035925L,
				BSTRING_TO_STRING(BGl_string1859z00zzcnst_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string1859z00zzcnst_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzcoerce_coercez00(361167184L,
				BSTRING_TO_STRING(BGl_string1859z00zzcnst_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzcnst_allocz00(192699986L,
				BSTRING_TO_STRING(BGl_string1859z00zzcnst_initializa7eza7));
			return
				BGl_modulezd2initializa7ationz75zzcnst_nodez00(89013043L,
				BSTRING_TO_STRING(BGl_string1859z00zzcnst_initializa7eza7));
		}

	}

#ifdef __cplusplus
}
#endif
