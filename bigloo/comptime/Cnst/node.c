/*===========================================================================*/
/*   (Cnst/node.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cnst/node.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CNST_NODE_TYPE_DEFINITIONS
#define BGL_CNST_NODE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_patchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
		struct BgL_varz00_bgl *BgL_refz00;
		long BgL_indexz00;
		obj_t BgL_patchidz00;
	}               *BgL_patchz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_localzf2bvaluezf2_bgl
	{
		struct BgL_nodez00_bgl *BgL_bindingzd2valuezd2;
	}                        *BgL_localzf2bvaluezf2_bglt;


#endif													// BGL_CNST_NODE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static BgL_nodez00_bglt BGl_z62cnstz12zd2appzd2ly1342z70zzcnst_nodez00(obj_t,
		obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static BgL_nodez00_bglt BGl_z62cnstz12zd2extern1316za2zzcnst_nodez00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcnst_nodez00 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	extern BgL_nodez00_bglt BGl_cnstzd2alloczd2keywordz00zzcnst_allocz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62cnstz12zd2switch1326za2zzcnst_nodez00(obj_t,
		obj_t);
	extern BgL_nodez00_bglt
		BGl_cnstzd2alloczd2llongz00zzcnst_allocz00(BGL_LONGLONG_T, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	extern obj_t BGl_za2bboolza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62cnstz12z70zzcnst_nodez00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzcnst_nodez00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62cnstz12zd2jumpzd2exzd2it1334za2zzcnst_nodez00(obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_cnstzd2alloczd2symbolz00zzcnst_allocz00(obj_t,
		obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzcnst_nodez00(void);
	extern BgL_nodez00_bglt BGl_cnstzd2alloczd2elongz00zzcnst_allocz00(long,
		obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62cnstz12zd2setzd2exzd2it1332za2zzcnst_nodez00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzcnst_nodez00(void);
	static obj_t BGl_localzf2bvaluezf2zzcnst_nodez00 = BUNSPEC;
	extern BgL_nodez00_bglt BGl_cnstzd2alloczd2listz00zzcnst_allocz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62cnstz12zd2conditional1322za2zzcnst_nodez00(obj_t, obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	extern obj_t BGl_za2llongzd2ze3bllongza2z31zzcnst_cachez00;
	static obj_t BGl_z62getzd2nodezd2atomzd2valuezb0zzcnst_nodez00(obj_t, obj_t);
	extern obj_t BGl_za2makezd2lzd2procedureza2z00zzcnst_cachez00;
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_cnstza2z12zb0zzcnst_nodez00(obj_t);
	extern BgL_nodez00_bglt BGl_cnstzd2alloczd2tvectorz00zzcnst_allocz00(obj_t,
		obj_t);
	extern BgL_nodez00_bglt BGl_cnstzd2alloczd2int32z00zzcnst_allocz00(int32_t,
		obj_t);
	extern obj_t BGl_za2bfalseza2z00zzcnst_cachez00;
	extern obj_t BGl_za2elongzd2ze3belongza2z31zzcnst_cachez00;
	extern obj_t BGl_za2stringzd2ze3bstringza2z31zzcnst_cachez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	extern BgL_nodez00_bglt BGl_cnstzd2alloczd2vectorz00zzcnst_allocz00(obj_t,
		obj_t);
	static obj_t BGl_z62cnstz121299z70zzcnst_nodez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62cnstz12zd2var1308za2zzcnst_nodez00(obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzcnst_nodez00(void);
	extern BgL_nodez00_bglt BGl_cnstzd2alloczd2int64z00zzcnst_allocz00(int64_t,
		obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	extern BgL_nodez00_bglt BGl_cnstzd2alloczd2stringz00zzcnst_allocz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62cnstz12zd2kwote1306za2zzcnst_nodez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern BgL_typez00_bglt BGl_getzd2typezd2atomz00zztype_typeofz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31514ze3ze5zzcnst_nodez00(obj_t, obj_t);
	static obj_t BGl_z62getzd2nodezd2atomzd2valuezd21350z62zzcnst_nodez00(obj_t,
		obj_t);
	static obj_t BGl_z62getzd2nodezd2atomzd2valuezd21352z62zzcnst_nodez00(obj_t,
		obj_t);
	extern obj_t BGl_za2boolzd2ze3bboolza2z31zzcnst_cachez00;
	static obj_t BGl_z62getzd2nodezd2atomzd2valuezd21354z62zzcnst_nodez00(obj_t,
		obj_t);
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2nodezd2atomzd2valuezd2zzcnst_nodez00(BgL_nodez00_bglt);
	static BgL_nodez00_bglt BGl_z62cnstz12zd2patch1304za2zzcnst_nodez00(obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62cnstz12zd2letzd2fun1328z70zzcnst_nodez00(obj_t,
		obj_t);
	extern BgL_nodez00_bglt
		BGl_cnstzd2alloczd2lzd2procedurezd2zzcnst_allocz00(BgL_nodez00_bglt, obj_t);
	extern obj_t BGl_za2makezd2fxzd2procedureza2z00zzcnst_cachez00;
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcnst_nodez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_allocz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__srfi4z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__unicodez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_za2int64zd2ze3bint64za2z31zzcnst_cachez00;
	static obj_t BGl_z62getzd2nodezd2atomzd2value1347zb0zzcnst_nodez00(obj_t,
		obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	extern obj_t BGl_za2uint64zd2ze3buint64za2z31zzcnst_cachez00;
	static BgL_nodez00_bglt BGl_z62cnstz12zd2app1346za2zzcnst_nodez00(obj_t,
		obj_t);
	extern obj_t BGl_za2makezd2vazd2procedureza2z00zzcnst_cachez00;
	extern obj_t BGl_za2longzd2ze3intza2z31zzcnst_cachez00;
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzcnst_nodez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcnst_nodez00(void);
	extern BgL_nodez00_bglt
		BGl_cnstzd2alloczd2ucs2zd2stringzd2zzcnst_allocz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_za2uint32zd2ze3buint32za2z31zzcnst_cachez00;
	static obj_t BGl_importedzd2moduleszd2initz00zzcnst_nodez00(void);
	static BgL_nodez00_bglt
		BGl_z62cnstz12zd2makezd2box1336z70zzcnst_nodez00(obj_t, obj_t);
	static BgL_localz00_bglt BGl_z62lambda1510z62zzcnst_nodez00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzcnst_nodez00(void);
	static BgL_localz00_bglt BGl_z62lambda1515z62zzcnst_nodez00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62cnstz12zd2boxzd2setz121340z62zzcnst_nodez00(obj_t, obj_t);
	extern obj_t BGl_za2fixnumzd2ze3bignumza2z31zzcnst_cachez00;
	extern BgL_nodez00_bglt
		BGl_cnstzd2alloczd2homogenouszd2vectorzd2zzcnst_allocz00(obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_cnstzd2alloczd2uint32z00zzcnst_allocz00(uint32_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62cnstz12zd2literal1302za2zzcnst_nodez00(obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62cnstz12zd2cast1318za2zzcnst_nodez00(obj_t,
		obj_t);
	extern obj_t BGl_za2btrueza2z00zzcnst_cachez00;
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_valuez00zzast_varz00;
	extern BgL_nodez00_bglt BGl_cnstzd2alloczd2uint64z00zzcnst_allocz00(uint64_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62lambda1553z62zzcnst_nodez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1554z62zzcnst_nodez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2int32zd2ze3bint32za2z31zzcnst_cachez00;
	extern BgL_nodez00_bglt BGl_cnstzd2alloczd2structz00zzcnst_allocz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern BgL_nodez00_bglt
		BGl_cnstzd2alloczd2procedurez00zzcnst_allocz00(BgL_nodez00_bglt, obj_t);
	extern obj_t BGl_literalz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62cnstz12zd2setq1320za2zzcnst_nodez00(obj_t,
		obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_za2doublezd2ze3realza2z31zzcnst_cachez00;
	extern obj_t BGl_cnstzd2alloczd2bignumz00zzcnst_allocz00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62cnstz12zd2sync1314za2zzcnst_nodez00(obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62cnstz12zd2fail1324za2zzcnst_nodez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static BgL_localz00_bglt BGl_z62lambda1490z62zzcnst_nodez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62cnstz12zd2letzd2var1330z70zzcnst_nodez00(obj_t,
		obj_t);
	extern BgL_nodez00_bglt BGl_cnstzd2alloczd2realz00zzcnst_allocz00(obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62cnstz12zd2sequence1312za2zzcnst_nodez00(obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62cnstz12zd2funcall1344za2zzcnst_nodez00(obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62cnstz12zd2closure1310za2zzcnst_nodez00(obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62cnstz12zd2boxzd2ref1338z70zzcnst_nodez00(obj_t,
		obj_t);
	extern obj_t BGl_patchz00zzast_nodez00;
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstz12z12zzcnst_nodez00(BgL_nodez00_bglt);
	static obj_t __cnst[9];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2058z00zzcnst_nodez00,
		BgL_bgl_za762lambda1554za7622103z00, BGl_z62lambda1554z62zzcnst_nodez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2065z00zzcnst_nodez00,
		BgL_bgl_string2065za700za7za7c2104za7, "cnst!1299", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2059z00zzcnst_nodez00,
		BgL_bgl_za762lambda1553za7622105z00, BGl_z62lambda1553z62zzcnst_nodez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2067z00zzcnst_nodez00,
		BgL_bgl_string2067za700za7za7c2106za7, "get-node-atom-value1347", 23);
	      DEFINE_STRING(BGl_string2068z00zzcnst_nodez00,
		BgL_bgl_string2068za700za7za7c2107za7, "No method for this object", 25);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_cnstz12zd2envzc0zzcnst_nodez00,
		BgL_bgl_za762cnstza712za770za7za7c2108za7, BGl_z62cnstz12z70zzcnst_nodez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2060z00zzcnst_nodez00,
		BgL_bgl_za762lambda1515za7622109z00, BGl_z62lambda1515z62zzcnst_nodez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2061z00zzcnst_nodez00,
		BgL_bgl_za762za7c3za704anonymo2110za7,
		BGl_z62zc3z04anonymousza31514ze3ze5zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2062z00zzcnst_nodez00,
		BgL_bgl_za762lambda1510za7622111z00, BGl_z62lambda1510z62zzcnst_nodez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2063z00zzcnst_nodez00,
		BgL_bgl_za762lambda1490za7622112z00, BGl_z62lambda1490z62zzcnst_nodez00, 0L,
		BUNSPEC, 14);
	      DEFINE_STRING(BGl_string2070z00zzcnst_nodez00,
		BgL_bgl_string2070za700za7za7c2113za7, "cnst!", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2064z00zzcnst_nodez00,
		BgL_bgl_za762cnstza7121299za772114za7, BGl_z62cnstz121299z70zzcnst_nodez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2066z00zzcnst_nodez00,
		BgL_bgl_za762getza7d2nodeza7d22115za7,
		BGl_z62getzd2nodezd2atomzd2value1347zb0zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2069z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2lit2116za7,
		BGl_z62cnstz12zd2literal1302za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2071z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2pat2117za7,
		BGl_z62cnstz12zd2patch1304za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2072z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2kwo2118za7,
		BGl_z62cnstz12zd2kwote1306za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2073z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2var2119za7,
		BGl_z62cnstz12zd2var1308za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2074z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2clo2120za7,
		BGl_z62cnstz12zd2closure1310za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2075z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2seq2121za7,
		BGl_z62cnstz12zd2sequence1312za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2076z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2syn2122za7,
		BGl_z62cnstz12zd2sync1314za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2077z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2ext2123za7,
		BGl_z62cnstz12zd2extern1316za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2078z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2cas2124za7,
		BGl_z62cnstz12zd2cast1318za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2079z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2set2125za7,
		BGl_z62cnstz12zd2setq1320za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2080z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2con2126za7,
		BGl_z62cnstz12zd2conditional1322za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2081z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2fai2127za7,
		BGl_z62cnstz12zd2fail1324za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2082z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2swi2128za7,
		BGl_z62cnstz12zd2switch1326za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2083z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2let2129za7,
		BGl_z62cnstz12zd2letzd2fun1328z70zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2084z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2let2130za7,
		BGl_z62cnstz12zd2letzd2var1330z70zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2085z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2set2131za7,
		BGl_z62cnstz12zd2setzd2exzd2it1332za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2086z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2jum2132za7,
		BGl_z62cnstz12zd2jumpzd2exzd2it1334za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2087z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2mak2133za7,
		BGl_z62cnstz12zd2makezd2box1336z70zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2094z00zzcnst_nodez00,
		BgL_bgl_string2094za700za7za7c2134za7, "get-node-atom-value", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2088z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2box2135za7,
		BGl_z62cnstz12zd2boxzd2ref1338z70zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2089z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2box2136za7,
		BGl_z62cnstz12zd2boxzd2setz121340z62zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2097z00zzcnst_nodez00,
		BgL_bgl_string2097za700za7za7c2137za7, "Unexepected `closure' node", 26);
	      DEFINE_STRING(BGl_string2098z00zzcnst_nodez00,
		BgL_bgl_string2098za700za7za7c2138za7, "cnst-quote", 10);
	      DEFINE_STRING(BGl_string2099z00zzcnst_nodez00,
		BgL_bgl_string2099za700za7za7c2139za7, "Illegal expression", 18);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2090z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2app2140za7,
		BGl_z62cnstz12zd2appzd2ly1342z70zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2091z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2fun2141za7,
		BGl_z62cnstz12zd2funcall1344za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2092z00zzcnst_nodez00,
		BgL_bgl_za762cnstza712za7d2app2142za7,
		BGl_z62cnstz12zd2app1346za2zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2093z00zzcnst_nodez00,
		BgL_bgl_za762getza7d2nodeza7d22143za7,
		BGl_z62getzd2nodezd2atomzd2valuezd21350z62zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2095z00zzcnst_nodez00,
		BgL_bgl_za762getza7d2nodeza7d22144za7,
		BGl_z62getzd2nodezd2atomzd2valuezd21352z62zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2096z00zzcnst_nodez00,
		BgL_bgl_za762getza7d2nodeza7d22145za7,
		BGl_z62getzd2nodezd2atomzd2valuezd21354z62zzcnst_nodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_getzd2nodezd2atomzd2valuezd2envz00zzcnst_nodez00,
		BgL_bgl_za762getza7d2nodeza7d22146za7,
		BGl_z62getzd2nodezd2atomzd2valuezb0zzcnst_nodez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2100z00zzcnst_nodez00,
		BgL_bgl_string2100za700za7za7c2147za7, "cnst_node", 9);
	      DEFINE_STRING(BGl_string2101z00zzcnst_nodez00,
		BgL_bgl_string2101za700za7za7c2148za7,
		"a-tvector read cnst!1299 (no-atom-value) _ cnst_node local/bvalue binding-value done ",
		85);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcnst_nodez00));
		     ADD_ROOT((void *) (&BGl_localzf2bvaluezf2zzcnst_nodez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcnst_nodez00(long
		BgL_checksumz00_2643, char *BgL_fromz00_2644)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcnst_nodez00))
				{
					BGl_requirezd2initializa7ationz75zzcnst_nodez00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcnst_nodez00();
					BGl_libraryzd2moduleszd2initz00zzcnst_nodez00();
					BGl_cnstzd2initzd2zzcnst_nodez00();
					BGl_importedzd2moduleszd2initz00zzcnst_nodez00();
					BGl_objectzd2initzd2zzcnst_nodez00();
					BGl_genericzd2initzd2zzcnst_nodez00();
					BGl_methodzd2initzd2zzcnst_nodez00();
					return BGl_toplevelzd2initzd2zzcnst_nodez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcnst_nodez00(void)
	{
		{	/* Cnst/node.scm 16 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cnst_node");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cnst_node");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cnst_node");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cnst_node");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"cnst_node");
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(0L, "cnst_node");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L, "cnst_node");
			BGl_modulezd2initializa7ationz75zz__srfi4z00(0L, "cnst_node");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cnst_node");
			BGl_modulezd2initializa7ationz75zz__unicodez00(0L, "cnst_node");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "cnst_node");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "cnst_node");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cnst_node");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cnst_node");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cnst_node");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cnst_node");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "cnst_node");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "cnst_node");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcnst_nodez00(void)
	{
		{	/* Cnst/node.scm 16 */
			{	/* Cnst/node.scm 16 */
				obj_t BgL_cportz00_2437;

				{	/* Cnst/node.scm 16 */
					obj_t BgL_stringz00_2444;

					BgL_stringz00_2444 = BGl_string2101z00zzcnst_nodez00;
					{	/* Cnst/node.scm 16 */
						obj_t BgL_startz00_2445;

						BgL_startz00_2445 = BINT(0L);
						{	/* Cnst/node.scm 16 */
							obj_t BgL_endz00_2446;

							BgL_endz00_2446 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2444)));
							{	/* Cnst/node.scm 16 */

								BgL_cportz00_2437 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2444, BgL_startz00_2445, BgL_endz00_2446);
				}}}}
				{
					long BgL_iz00_2438;

					BgL_iz00_2438 = 8L;
				BgL_loopz00_2439:
					if ((BgL_iz00_2438 == -1L))
						{	/* Cnst/node.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Cnst/node.scm 16 */
							{	/* Cnst/node.scm 16 */
								obj_t BgL_arg2102z00_2440;

								{	/* Cnst/node.scm 16 */

									{	/* Cnst/node.scm 16 */
										obj_t BgL_locationz00_2442;

										BgL_locationz00_2442 = BBOOL(((bool_t) 0));
										{	/* Cnst/node.scm 16 */

											BgL_arg2102z00_2440 =
												BGl_readz00zz__readerz00(BgL_cportz00_2437,
												BgL_locationz00_2442);
										}
									}
								}
								{	/* Cnst/node.scm 16 */
									int BgL_tmpz00_2683;

									BgL_tmpz00_2683 = (int) (BgL_iz00_2438);
									CNST_TABLE_SET(BgL_tmpz00_2683, BgL_arg2102z00_2440);
							}}
							{	/* Cnst/node.scm 16 */
								int BgL_auxz00_2443;

								BgL_auxz00_2443 = (int) ((BgL_iz00_2438 - 1L));
								{
									long BgL_iz00_2688;

									BgL_iz00_2688 = (long) (BgL_auxz00_2443);
									BgL_iz00_2438 = BgL_iz00_2688;
									goto BgL_loopz00_2439;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcnst_nodez00(void)
	{
		{	/* Cnst/node.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcnst_nodez00(void)
	{
		{	/* Cnst/node.scm 16 */
			return BUNSPEC;
		}

	}



/* cnst*! */
	obj_t BGl_cnstza2z12zb0zzcnst_nodez00(obj_t BgL_nodesz00_85)
	{
		{	/* Cnst/node.scm 289 */
			{
				obj_t BgL_hookz00_1458;

				BgL_hookz00_1458 = BgL_nodesz00_85;
			BgL_zc3z04anonymousza31422ze3z87_1459:
				if (NULLP(BgL_hookz00_1458))
					{	/* Cnst/node.scm 291 */
						return CNST_TABLE_REF(0);
					}
				else
					{	/* Cnst/node.scm 291 */
						{	/* Cnst/node.scm 294 */
							BgL_nodez00_bglt BgL_arg1434z00_1461;

							{	/* Cnst/node.scm 294 */
								obj_t BgL_arg1437z00_1462;

								BgL_arg1437z00_1462 = CAR(((obj_t) BgL_hookz00_1458));
								BgL_arg1434z00_1461 =
									BGl_cnstz12z12zzcnst_nodez00(
									((BgL_nodez00_bglt) BgL_arg1437z00_1462));
							}
							{	/* Cnst/node.scm 294 */
								obj_t BgL_auxz00_2700;
								obj_t BgL_tmpz00_2698;

								BgL_auxz00_2700 = ((obj_t) BgL_arg1434z00_1461);
								BgL_tmpz00_2698 = ((obj_t) BgL_hookz00_1458);
								SET_CAR(BgL_tmpz00_2698, BgL_auxz00_2700);
							}
						}
						{	/* Cnst/node.scm 295 */
							obj_t BgL_arg1448z00_1463;

							BgL_arg1448z00_1463 = CDR(((obj_t) BgL_hookz00_1458));
							{
								obj_t BgL_hookz00_2705;

								BgL_hookz00_2705 = BgL_arg1448z00_1463;
								BgL_hookz00_1458 = BgL_hookz00_2705;
								goto BgL_zc3z04anonymousza31422ze3z87_1459;
							}
						}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcnst_nodez00(void)
	{
		{	/* Cnst/node.scm 16 */
			{	/* Cnst/node.scm 33 */
				obj_t BgL_arg1485z00_1469;
				obj_t BgL_arg1489z00_1470;

				{	/* Cnst/node.scm 33 */
					obj_t BgL_v1297z00_1505;

					BgL_v1297z00_1505 = create_vector(1L);
					{	/* Cnst/node.scm 33 */
						obj_t BgL_arg1544z00_1506;

						BgL_arg1544z00_1506 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc2059z00zzcnst_nodez00, BGl_proc2058z00zzcnst_nodez00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_nodez00zzast_nodez00);
						VECTOR_SET(BgL_v1297z00_1505, 0L, BgL_arg1544z00_1506);
					}
					BgL_arg1485z00_1469 = BgL_v1297z00_1505;
				}
				{	/* Cnst/node.scm 33 */
					obj_t BgL_v1298z00_1516;

					BgL_v1298z00_1516 = create_vector(0L);
					BgL_arg1489z00_1470 = BgL_v1298z00_1516;
				}
				return (BGl_localzf2bvaluezf2zzcnst_nodez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(2),
						CNST_TABLE_REF(3), BGl_localz00zzast_varz00, 17823L,
						BGl_proc2063z00zzcnst_nodez00, BGl_proc2062z00zzcnst_nodez00,
						BFALSE, BGl_proc2061z00zzcnst_nodez00,
						BGl_proc2060z00zzcnst_nodez00, BgL_arg1485z00_1469,
						BgL_arg1489z00_1470), BUNSPEC);
			}
		}

	}



/* &lambda1515 */
	BgL_localz00_bglt BGl_z62lambda1515z62zzcnst_nodez00(obj_t BgL_envz00_2322,
		obj_t BgL_o1121z00_2323)
	{
		{	/* Cnst/node.scm 33 */
			{	/* Cnst/node.scm 33 */
				long BgL_arg1516z00_2449;

				{	/* Cnst/node.scm 33 */
					obj_t BgL_arg1535z00_2450;

					{	/* Cnst/node.scm 33 */
						obj_t BgL_arg1540z00_2451;

						{	/* Cnst/node.scm 33 */
							obj_t BgL_arg1815z00_2452;
							long BgL_arg1816z00_2453;

							BgL_arg1815z00_2452 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Cnst/node.scm 33 */
								long BgL_arg1817z00_2454;

								BgL_arg1817z00_2454 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_o1121z00_2323)));
								BgL_arg1816z00_2453 = (BgL_arg1817z00_2454 - OBJECT_TYPE);
							}
							BgL_arg1540z00_2451 =
								VECTOR_REF(BgL_arg1815z00_2452, BgL_arg1816z00_2453);
						}
						BgL_arg1535z00_2450 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1540z00_2451);
					}
					{	/* Cnst/node.scm 33 */
						obj_t BgL_tmpz00_2721;

						BgL_tmpz00_2721 = ((obj_t) BgL_arg1535z00_2450);
						BgL_arg1516z00_2449 = BGL_CLASS_NUM(BgL_tmpz00_2721);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_localz00_bglt) BgL_o1121z00_2323)), BgL_arg1516z00_2449);
			}
			{	/* Cnst/node.scm 33 */
				BgL_objectz00_bglt BgL_tmpz00_2727;

				BgL_tmpz00_2727 =
					((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1121z00_2323));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2727, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1121z00_2323));
			return ((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1121z00_2323));
		}

	}



/* &<@anonymous:1514> */
	obj_t BGl_z62zc3z04anonymousza31514ze3ze5zzcnst_nodez00(obj_t BgL_envz00_2324,
		obj_t BgL_new1120z00_2325)
	{
		{	/* Cnst/node.scm 33 */
			{
				BgL_localz00_bglt BgL_auxz00_2735;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1120z00_2325))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2325))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_2743;

					{	/* Cnst/node.scm 33 */
						obj_t BgL_classz00_2456;

						BgL_classz00_2456 = BGl_typez00zztype_typez00;
						{	/* Cnst/node.scm 33 */
							obj_t BgL__ortest_1117z00_2457;

							BgL__ortest_1117z00_2457 = BGL_CLASS_NIL(BgL_classz00_2456);
							if (CBOOL(BgL__ortest_1117z00_2457))
								{	/* Cnst/node.scm 33 */
									BgL_auxz00_2743 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_2457);
								}
							else
								{	/* Cnst/node.scm 33 */
									BgL_auxz00_2743 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2456));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1120z00_2325))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_2743), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_2753;

					{	/* Cnst/node.scm 33 */
						obj_t BgL_classz00_2458;

						BgL_classz00_2458 = BGl_valuez00zzast_varz00;
						{	/* Cnst/node.scm 33 */
							obj_t BgL__ortest_1117z00_2459;

							BgL__ortest_1117z00_2459 = BGL_CLASS_NIL(BgL_classz00_2458);
							if (CBOOL(BgL__ortest_1117z00_2459))
								{	/* Cnst/node.scm 33 */
									BgL_auxz00_2753 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_2459);
								}
							else
								{	/* Cnst/node.scm 33 */
									BgL_auxz00_2753 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2458));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1120z00_2325))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_2753), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1120z00_2325))))->BgL_accessz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2325))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2325))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2325))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2325))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2325))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2325))))->BgL_keyz00) =
					((long) 0L), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2325))))->BgL_valzd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2325))))->BgL_volatilez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_2797;
					BgL_localzf2bvaluezf2_bglt BgL_auxz00_2790;

					{	/* Cnst/node.scm 33 */
						obj_t BgL_classz00_2460;

						BgL_classz00_2460 = BGl_nodez00zzast_nodez00;
						{	/* Cnst/node.scm 33 */
							obj_t BgL__ortest_1117z00_2461;

							BgL__ortest_1117z00_2461 = BGL_CLASS_NIL(BgL_classz00_2460);
							if (CBOOL(BgL__ortest_1117z00_2461))
								{	/* Cnst/node.scm 33 */
									BgL_auxz00_2797 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_2461);
								}
							else
								{	/* Cnst/node.scm 33 */
									BgL_auxz00_2797 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2460));
								}
						}
					}
					{
						obj_t BgL_auxz00_2791;

						{	/* Cnst/node.scm 33 */
							BgL_objectz00_bglt BgL_tmpz00_2792;

							BgL_tmpz00_2792 =
								((BgL_objectz00_bglt)
								((BgL_localz00_bglt) BgL_new1120z00_2325));
							BgL_auxz00_2791 = BGL_OBJECT_WIDENING(BgL_tmpz00_2792);
						}
						BgL_auxz00_2790 = ((BgL_localzf2bvaluezf2_bglt) BgL_auxz00_2791);
					}
					((((BgL_localzf2bvaluezf2_bglt) COBJECT(BgL_auxz00_2790))->
							BgL_bindingzd2valuezd2) =
						((BgL_nodez00_bglt) BgL_auxz00_2797), BUNSPEC);
				}
				BgL_auxz00_2735 = ((BgL_localz00_bglt) BgL_new1120z00_2325);
				return ((obj_t) BgL_auxz00_2735);
			}
		}

	}



/* &lambda1510 */
	BgL_localz00_bglt BGl_z62lambda1510z62zzcnst_nodez00(obj_t BgL_envz00_2326,
		obj_t BgL_o1117z00_2327)
	{
		{	/* Cnst/node.scm 33 */
			{	/* Cnst/node.scm 33 */
				BgL_localzf2bvaluezf2_bglt BgL_wide1119z00_2463;

				BgL_wide1119z00_2463 =
					((BgL_localzf2bvaluezf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_localzf2bvaluezf2_bgl))));
				{	/* Cnst/node.scm 33 */
					obj_t BgL_auxz00_2812;
					BgL_objectz00_bglt BgL_tmpz00_2808;

					BgL_auxz00_2812 = ((obj_t) BgL_wide1119z00_2463);
					BgL_tmpz00_2808 =
						((BgL_objectz00_bglt)
						((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1117z00_2327)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2808, BgL_auxz00_2812);
				}
				((BgL_objectz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1117z00_2327)));
				{	/* Cnst/node.scm 33 */
					long BgL_arg1513z00_2464;

					BgL_arg1513z00_2464 =
						BGL_CLASS_NUM(BGl_localzf2bvaluezf2zzcnst_nodez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_localz00_bglt)
								((BgL_localz00_bglt) BgL_o1117z00_2327))), BgL_arg1513z00_2464);
				}
				return
					((BgL_localz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1117z00_2327)));
			}
		}

	}



/* &lambda1490 */
	BgL_localz00_bglt BGl_z62lambda1490z62zzcnst_nodez00(obj_t BgL_envz00_2328,
		obj_t BgL_id1103z00_2329, obj_t BgL_name1104z00_2330,
		obj_t BgL_type1105z00_2331, obj_t BgL_value1106z00_2332,
		obj_t BgL_access1107z00_2333, obj_t BgL_fastzd2alpha1108zd2_2334,
		obj_t BgL_removable1109z00_2335, obj_t BgL_occurrence1110z00_2336,
		obj_t BgL_occurrencew1111z00_2337, obj_t BgL_userzf31112zf3_2338,
		obj_t BgL_key1113z00_2339, obj_t BgL_valzd2noescape1114zd2_2340,
		obj_t BgL_volatile1115z00_2341, obj_t BgL_bindingzd2value1116zd2_2342)
	{
		{	/* Cnst/node.scm 33 */
			{	/* Cnst/node.scm 33 */
				long BgL_occurrence1110z00_2468;
				long BgL_occurrencew1111z00_2469;
				bool_t BgL_userzf31112zf3_2470;
				long BgL_key1113z00_2471;
				bool_t BgL_volatile1115z00_2472;

				BgL_occurrence1110z00_2468 = (long) CINT(BgL_occurrence1110z00_2336);
				BgL_occurrencew1111z00_2469 = (long) CINT(BgL_occurrencew1111z00_2337);
				BgL_userzf31112zf3_2470 = CBOOL(BgL_userzf31112zf3_2338);
				BgL_key1113z00_2471 = (long) CINT(BgL_key1113z00_2339);
				BgL_volatile1115z00_2472 = CBOOL(BgL_volatile1115z00_2341);
				{	/* Cnst/node.scm 33 */
					BgL_localz00_bglt BgL_new1171z00_2474;

					{	/* Cnst/node.scm 33 */
						BgL_localz00_bglt BgL_tmp1169z00_2475;
						BgL_localzf2bvaluezf2_bglt BgL_wide1170z00_2476;

						{
							BgL_localz00_bglt BgL_auxz00_2831;

							{	/* Cnst/node.scm 33 */
								BgL_localz00_bglt BgL_new1168z00_2477;

								BgL_new1168z00_2477 =
									((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_localz00_bgl))));
								{	/* Cnst/node.scm 33 */
									long BgL_arg1509z00_2478;

									BgL_arg1509z00_2478 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1168z00_2477),
										BgL_arg1509z00_2478);
								}
								{	/* Cnst/node.scm 33 */
									BgL_objectz00_bglt BgL_tmpz00_2836;

									BgL_tmpz00_2836 = ((BgL_objectz00_bglt) BgL_new1168z00_2477);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2836, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1168z00_2477);
								BgL_auxz00_2831 = BgL_new1168z00_2477;
							}
							BgL_tmp1169z00_2475 = ((BgL_localz00_bglt) BgL_auxz00_2831);
						}
						BgL_wide1170z00_2476 =
							((BgL_localzf2bvaluezf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_localzf2bvaluezf2_bgl))));
						{	/* Cnst/node.scm 33 */
							obj_t BgL_auxz00_2844;
							BgL_objectz00_bglt BgL_tmpz00_2842;

							BgL_auxz00_2844 = ((obj_t) BgL_wide1170z00_2476);
							BgL_tmpz00_2842 = ((BgL_objectz00_bglt) BgL_tmp1169z00_2475);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2842, BgL_auxz00_2844);
						}
						((BgL_objectz00_bglt) BgL_tmp1169z00_2475);
						{	/* Cnst/node.scm 33 */
							long BgL_arg1502z00_2479;

							BgL_arg1502z00_2479 =
								BGL_CLASS_NUM(BGl_localzf2bvaluezf2zzcnst_nodez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1169z00_2475),
								BgL_arg1502z00_2479);
						}
						BgL_new1171z00_2474 = ((BgL_localz00_bglt) BgL_tmp1169z00_2475);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1171z00_2474)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1103z00_2329)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1171z00_2474)))->BgL_namez00) =
						((obj_t) BgL_name1104z00_2330), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1171z00_2474)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1105z00_2331)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1171z00_2474)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1106z00_2332)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1171z00_2474)))->BgL_accessz00) =
						((obj_t) BgL_access1107z00_2333), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1171z00_2474)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1108zd2_2334), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1171z00_2474)))->BgL_removablez00) =
						((obj_t) BgL_removable1109z00_2335), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1171z00_2474)))->BgL_occurrencez00) =
						((long) BgL_occurrence1110z00_2468), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1171z00_2474)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1111z00_2469), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1171z00_2474)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31112zf3_2470), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1171z00_2474)))->BgL_keyz00) =
						((long) BgL_key1113z00_2471), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1171z00_2474)))->BgL_valzd2noescapezd2) =
						((obj_t) BgL_valzd2noescape1114zd2_2340), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1171z00_2474)))->BgL_volatilez00) =
						((bool_t) BgL_volatile1115z00_2472), BUNSPEC);
					{
						BgL_localzf2bvaluezf2_bglt BgL_auxz00_2881;

						{
							obj_t BgL_auxz00_2882;

							{	/* Cnst/node.scm 33 */
								BgL_objectz00_bglt BgL_tmpz00_2883;

								BgL_tmpz00_2883 = ((BgL_objectz00_bglt) BgL_new1171z00_2474);
								BgL_auxz00_2882 = BGL_OBJECT_WIDENING(BgL_tmpz00_2883);
							}
							BgL_auxz00_2881 = ((BgL_localzf2bvaluezf2_bglt) BgL_auxz00_2882);
						}
						((((BgL_localzf2bvaluezf2_bglt) COBJECT(BgL_auxz00_2881))->
								BgL_bindingzd2valuezd2) =
							((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
									BgL_bindingzd2value1116zd2_2342)), BUNSPEC);
					}
					return BgL_new1171z00_2474;
				}
			}
		}

	}



/* &lambda1554 */
	obj_t BGl_z62lambda1554z62zzcnst_nodez00(obj_t BgL_envz00_2343,
		obj_t BgL_oz00_2344, obj_t BgL_vz00_2345)
	{
		{	/* Cnst/node.scm 33 */
			{
				BgL_localzf2bvaluezf2_bglt BgL_auxz00_2889;

				{
					obj_t BgL_auxz00_2890;

					{	/* Cnst/node.scm 33 */
						BgL_objectz00_bglt BgL_tmpz00_2891;

						BgL_tmpz00_2891 =
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_2344));
						BgL_auxz00_2890 = BGL_OBJECT_WIDENING(BgL_tmpz00_2891);
					}
					BgL_auxz00_2889 = ((BgL_localzf2bvaluezf2_bglt) BgL_auxz00_2890);
				}
				return
					((((BgL_localzf2bvaluezf2_bglt) COBJECT(BgL_auxz00_2889))->
						BgL_bindingzd2valuezd2) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_vz00_2345)), BUNSPEC);
			}
		}

	}



/* &lambda1553 */
	BgL_nodez00_bglt BGl_z62lambda1553z62zzcnst_nodez00(obj_t BgL_envz00_2346,
		obj_t BgL_oz00_2347)
	{
		{	/* Cnst/node.scm 33 */
			{
				BgL_localzf2bvaluezf2_bglt BgL_auxz00_2898;

				{
					obj_t BgL_auxz00_2899;

					{	/* Cnst/node.scm 33 */
						BgL_objectz00_bglt BgL_tmpz00_2900;

						BgL_tmpz00_2900 =
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_2347));
						BgL_auxz00_2899 = BGL_OBJECT_WIDENING(BgL_tmpz00_2900);
					}
					BgL_auxz00_2898 = ((BgL_localzf2bvaluezf2_bglt) BgL_auxz00_2899);
				}
				return
					(((BgL_localzf2bvaluezf2_bglt) COBJECT(BgL_auxz00_2898))->
					BgL_bindingzd2valuezd2);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcnst_nodez00(void)
	{
		{	/* Cnst/node.scm 16 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_proc2064z00zzcnst_nodez00,
				BGl_nodez00zzast_nodez00, BGl_string2065z00zzcnst_nodez00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_getzd2nodezd2atomzd2valuezd2envz00zzcnst_nodez00,
				BGl_proc2066z00zzcnst_nodez00, BGl_nodez00zzast_nodez00,
				BGl_string2067z00zzcnst_nodez00);
		}

	}



/* &get-node-atom-value1347 */
	obj_t BGl_z62getzd2nodezd2atomzd2value1347zb0zzcnst_nodez00(obj_t
		BgL_envz00_2350, obj_t BgL_nodez00_2351)
	{
		{	/* Cnst/node.scm 380 */
			return CNST_TABLE_REF(5);
		}

	}



/* &cnst!1299 */
	obj_t BGl_z62cnstz121299z70zzcnst_nodez00(obj_t BgL_envz00_2352,
		obj_t BgL_nodez00_2353)
	{
		{	/* Cnst/node.scm 39 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(6),
				BGl_string2068z00zzcnst_nodez00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2353)));
		}

	}



/* cnst! */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstz12z12zzcnst_nodez00(BgL_nodez00_bglt BgL_nodez00_62)
	{
		{	/* Cnst/node.scm 39 */
			{	/* Cnst/node.scm 39 */
				obj_t BgL_method1300z00_1529;

				{	/* Cnst/node.scm 39 */
					obj_t BgL_res2051z00_2200;

					{	/* Cnst/node.scm 39 */
						long BgL_objzd2classzd2numz00_2171;

						BgL_objzd2classzd2numz00_2171 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_62));
						{	/* Cnst/node.scm 39 */
							obj_t BgL_arg1811z00_2172;

							BgL_arg1811z00_2172 =
								PROCEDURE_REF(BGl_cnstz12zd2envzc0zzcnst_nodez00, (int) (1L));
							{	/* Cnst/node.scm 39 */
								int BgL_offsetz00_2175;

								BgL_offsetz00_2175 = (int) (BgL_objzd2classzd2numz00_2171);
								{	/* Cnst/node.scm 39 */
									long BgL_offsetz00_2176;

									BgL_offsetz00_2176 =
										((long) (BgL_offsetz00_2175) - OBJECT_TYPE);
									{	/* Cnst/node.scm 39 */
										long BgL_modz00_2177;

										BgL_modz00_2177 =
											(BgL_offsetz00_2176 >> (int) ((long) ((int) (4L))));
										{	/* Cnst/node.scm 39 */
											long BgL_restz00_2179;

											BgL_restz00_2179 =
												(BgL_offsetz00_2176 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cnst/node.scm 39 */

												{	/* Cnst/node.scm 39 */
													obj_t BgL_bucketz00_2181;

													BgL_bucketz00_2181 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2172), BgL_modz00_2177);
													BgL_res2051z00_2200 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2181), BgL_restz00_2179);
					}}}}}}}}
					BgL_method1300z00_1529 = BgL_res2051z00_2200;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1300z00_1529,
						((obj_t) BgL_nodez00_62)));
			}
		}

	}



/* &cnst! */
	BgL_nodez00_bglt BGl_z62cnstz12z70zzcnst_nodez00(obj_t BgL_envz00_2354,
		obj_t BgL_nodez00_2355)
	{
		{	/* Cnst/node.scm 39 */
			return
				BGl_cnstz12z12zzcnst_nodez00(((BgL_nodez00_bglt) BgL_nodez00_2355));
		}

	}



/* get-node-atom-value */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2nodezd2atomzd2valuezd2zzcnst_nodez00(BgL_nodez00_bglt
		BgL_nodez00_87)
	{
		{	/* Cnst/node.scm 380 */
			{	/* Cnst/node.scm 380 */
				obj_t BgL_method1348z00_1530;

				{	/* Cnst/node.scm 380 */
					obj_t BgL_res2056z00_2231;

					{	/* Cnst/node.scm 380 */
						long BgL_objzd2classzd2numz00_2202;

						BgL_objzd2classzd2numz00_2202 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_87));
						{	/* Cnst/node.scm 380 */
							obj_t BgL_arg1811z00_2203;

							BgL_arg1811z00_2203 =
								PROCEDURE_REF
								(BGl_getzd2nodezd2atomzd2valuezd2envz00zzcnst_nodez00,
								(int) (1L));
							{	/* Cnst/node.scm 380 */
								int BgL_offsetz00_2206;

								BgL_offsetz00_2206 = (int) (BgL_objzd2classzd2numz00_2202);
								{	/* Cnst/node.scm 380 */
									long BgL_offsetz00_2207;

									BgL_offsetz00_2207 =
										((long) (BgL_offsetz00_2206) - OBJECT_TYPE);
									{	/* Cnst/node.scm 380 */
										long BgL_modz00_2208;

										BgL_modz00_2208 =
											(BgL_offsetz00_2207 >> (int) ((long) ((int) (4L))));
										{	/* Cnst/node.scm 380 */
											long BgL_restz00_2210;

											BgL_restz00_2210 =
												(BgL_offsetz00_2207 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cnst/node.scm 380 */

												{	/* Cnst/node.scm 380 */
													obj_t BgL_bucketz00_2212;

													BgL_bucketz00_2212 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2203), BgL_modz00_2208);
													BgL_res2056z00_2231 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2212), BgL_restz00_2210);
					}}}}}}}}
					BgL_method1348z00_1530 = BgL_res2056z00_2231;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1348z00_1530, ((obj_t) BgL_nodez00_87));
			}
		}

	}



/* &get-node-atom-value */
	obj_t BGl_z62getzd2nodezd2atomzd2valuezb0zzcnst_nodez00(obj_t BgL_envz00_2356,
		obj_t BgL_nodez00_2357)
	{
		{	/* Cnst/node.scm 380 */
			return
				BGl_getzd2nodezd2atomzd2valuezd2zzcnst_nodez00(
				((BgL_nodez00_bglt) BgL_nodez00_2357));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcnst_nodez00(void)
	{
		{	/* Cnst/node.scm 16 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_literalz00zzast_nodez00,
				BGl_proc2069z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_patchz00zzast_nodez00,
				BGl_proc2071z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_kwotez00zzast_nodez00,
				BGl_proc2072z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_varz00zzast_nodez00,
				BGl_proc2073z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_closurez00zzast_nodez00,
				BGl_proc2074z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_sequencez00zzast_nodez00,
				BGl_proc2075z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_syncz00zzast_nodez00,
				BGl_proc2076z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_externz00zzast_nodez00,
				BGl_proc2077z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_castz00zzast_nodez00,
				BGl_proc2078z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_setqz00zzast_nodez00,
				BGl_proc2079z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_conditionalz00zzast_nodez00,
				BGl_proc2080z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_failz00zzast_nodez00,
				BGl_proc2081z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_switchz00zzast_nodez00,
				BGl_proc2082z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc2083z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc2084z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc2085z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc2086z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc2087z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc2088z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc2089z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc2090z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_funcallz00zzast_nodez00,
				BGl_proc2091z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cnstz12zd2envzc0zzcnst_nodez00, BGl_appz00zzast_nodez00,
				BGl_proc2092z00zzcnst_nodez00, BGl_string2070z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2nodezd2atomzd2valuezd2envz00zzcnst_nodez00,
				BGl_atomz00zzast_nodez00, BGl_proc2093z00zzcnst_nodez00,
				BGl_string2094z00zzcnst_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2nodezd2atomzd2valuezd2envz00zzcnst_nodez00,
				BGl_varz00zzast_nodez00, BGl_proc2095z00zzcnst_nodez00,
				BGl_string2094z00zzcnst_nodez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2nodezd2atomzd2valuezd2envz00zzcnst_nodez00,
				BGl_appz00zzast_nodez00, BGl_proc2096z00zzcnst_nodez00,
				BGl_string2094z00zzcnst_nodez00);
		}

	}



/* &get-node-atom-value-1354 */
	obj_t BGl_z62getzd2nodezd2atomzd2valuezd21354z62zzcnst_nodez00(obj_t
		BgL_envz00_2384, obj_t BgL_nodez00_2385)
	{
		{	/* Cnst/node.scm 401 */
			{	/* Cnst/node.scm 403 */
				BgL_variablez00_bglt BgL_funz00_2486;

				BgL_funz00_2486 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_2385)))->BgL_funz00)))->
					BgL_variablez00);
				if ((((obj_t) BgL_funz00_2486) ==
						BGl_za2longzd2ze3intza2z31zzcnst_cachez00))
					{	/* Cnst/node.scm 406 */
						bool_t BgL_test2156z00_3010;

						{	/* Cnst/node.scm 406 */
							bool_t BgL_test2157z00_3011;

							{	/* Cnst/node.scm 406 */
								obj_t BgL_tmpz00_3012;

								BgL_tmpz00_3012 =
									(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_2385)))->BgL_argsz00);
								BgL_test2157z00_3011 = PAIRP(BgL_tmpz00_3012);
							}
							if (BgL_test2157z00_3011)
								{	/* Cnst/node.scm 406 */
									BgL_test2156z00_3010 =
										NULLP(CDR(
											(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_nodez00_2385)))->
												BgL_argsz00)));
								}
							else
								{	/* Cnst/node.scm 406 */
									BgL_test2156z00_3010 = ((bool_t) 0);
								}
						}
						if (BgL_test2156z00_3010)
							{	/* Cnst/node.scm 407 */
								obj_t BgL_arg1980z00_2487;

								BgL_arg1980z00_2487 =
									CAR(
									(((BgL_appz00_bglt) COBJECT(
												((BgL_appz00_bglt) BgL_nodez00_2385)))->BgL_argsz00));
								return
									BGl_getzd2nodezd2atomzd2valuezd2zzcnst_nodez00(
									((BgL_nodez00_bglt) BgL_arg1980z00_2487));
							}
						else
							{	/* Cnst/node.scm 406 */
								return CNST_TABLE_REF(5);
							}
					}
				else
					{	/* Cnst/node.scm 405 */
						return CNST_TABLE_REF(5);
					}
			}
		}

	}



/* &get-node-atom-value-1352 */
	obj_t BGl_z62getzd2nodezd2atomzd2valuezd21352z62zzcnst_nodez00(obj_t
		BgL_envz00_2386, obj_t BgL_nodez00_2387)
	{
		{	/* Cnst/node.scm 392 */
			{	/* Cnst/node.scm 393 */
				BgL_variablez00_bglt BgL_vz00_2489;

				BgL_vz00_2489 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt) BgL_nodez00_2387)))->BgL_variablez00);
				{	/* Cnst/node.scm 394 */
					bool_t BgL_test2158z00_3029;

					{	/* Cnst/node.scm 394 */
						obj_t BgL_classz00_2490;

						BgL_classz00_2490 = BGl_localzf2bvaluezf2zzcnst_nodez00;
						{	/* Cnst/node.scm 394 */
							obj_t BgL_oclassz00_2491;

							{	/* Cnst/node.scm 394 */
								obj_t BgL_arg1815z00_2492;
								long BgL_arg1816z00_2493;

								BgL_arg1815z00_2492 = (BGl_za2classesza2z00zz__objectz00);
								{	/* Cnst/node.scm 394 */
									long BgL_arg1817z00_2494;

									BgL_arg1817z00_2494 =
										BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_vz00_2489));
									BgL_arg1816z00_2493 = (BgL_arg1817z00_2494 - OBJECT_TYPE);
								}
								BgL_oclassz00_2491 =
									VECTOR_REF(BgL_arg1815z00_2492, BgL_arg1816z00_2493);
							}
							BgL_test2158z00_3029 = (BgL_oclassz00_2491 == BgL_classz00_2490);
					}}
					if (BgL_test2158z00_3029)
						{	/* Cnst/node.scm 395 */
							BgL_nodez00_bglt BgL_arg1973z00_2495;

							{
								BgL_localzf2bvaluezf2_bglt BgL_auxz00_3036;

								{
									obj_t BgL_auxz00_3037;

									{	/* Cnst/node.scm 395 */
										BgL_objectz00_bglt BgL_tmpz00_3038;

										BgL_tmpz00_3038 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt) BgL_vz00_2489));
										BgL_auxz00_3037 = BGL_OBJECT_WIDENING(BgL_tmpz00_3038);
									}
									BgL_auxz00_3036 =
										((BgL_localzf2bvaluezf2_bglt) BgL_auxz00_3037);
								}
								BgL_arg1973z00_2495 =
									(((BgL_localzf2bvaluezf2_bglt) COBJECT(BgL_auxz00_3036))->
									BgL_bindingzd2valuezd2);
							}
							return
								BGl_getzd2nodezd2atomzd2valuezd2zzcnst_nodez00
								(BgL_arg1973z00_2495);
						}
					else
						{	/* Cnst/node.scm 394 */
							return CNST_TABLE_REF(5);
						}
				}
			}
		}

	}



/* &get-node-atom-value-1350 */
	obj_t BGl_z62getzd2nodezd2atomzd2valuezd21350z62zzcnst_nodez00(obj_t
		BgL_envz00_2388, obj_t BgL_nodez00_2389)
	{
		{	/* Cnst/node.scm 386 */
			return
				(((BgL_atomz00_bglt) COBJECT(
						((BgL_atomz00_bglt) BgL_nodez00_2389)))->BgL_valuez00);
		}

	}



/* &cnst!-app1346 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2app1346za2zzcnst_nodez00(obj_t
		BgL_envz00_2390, obj_t BgL_nodez00_2391)
	{
		{	/* Cnst/node.scm 300 */
			BGl_cnstza2z12zb0zzcnst_nodez00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2391)))->BgL_argsz00));
			if (NULLP(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2391)))->BgL_argsz00)))
				{	/* Cnst/node.scm 304 */
					return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2391));
				}
			else
				{	/* Cnst/node.scm 306 */
					BgL_variablez00_bglt BgL_funz00_2498;

					BgL_funz00_2498 =
						(((BgL_varz00_bglt) COBJECT(
								(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_2391)))->BgL_funz00)))->
						BgL_variablez00);
					{	/* Cnst/node.scm 306 */
						obj_t BgL_actualz00_2499;

						BgL_actualz00_2499 =
							CAR(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_2391)))->BgL_argsz00));
						{	/* Cnst/node.scm 307 */
							obj_t BgL_actualzd2valuezd2_2500;

							BgL_actualzd2valuezd2_2500 =
								BGl_getzd2nodezd2atomzd2valuezd2zzcnst_nodez00(
								((BgL_nodez00_bglt) BgL_actualz00_2499));
							{	/* Cnst/node.scm 308 */

								if (
									(((obj_t) BgL_funz00_2498) ==
										BGl_za2stringzd2ze3bstringza2z31zzcnst_cachez00))
									{	/* Cnst/node.scm 310 */
										if (STRINGP(BgL_actualzd2valuezd2_2500))
											{	/* Cnst/node.scm 312 */
												BgL_nodez00_bglt BgL_rz00_2501;

												{	/* Cnst/node.scm 312 */
													obj_t BgL_arg1937z00_2502;

													BgL_arg1937z00_2502 =
														(((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt)
																	((BgL_appz00_bglt) BgL_nodez00_2391))))->
														BgL_locz00);
													BgL_rz00_2501 =
														BGl_cnstzd2alloczd2stringz00zzcnst_allocz00
														(BgL_actualzd2valuezd2_2500, BgL_arg1937z00_2502);
												}
												return BgL_rz00_2501;
											}
										else
											{	/* Cnst/node.scm 311 */
												return
													((BgL_nodez00_bglt)
													((BgL_appz00_bglt) BgL_nodez00_2391));
											}
									}
								else
									{	/* Cnst/node.scm 310 */
										if (
											(((obj_t) BgL_funz00_2498) ==
												BGl_za2boolzd2ze3bboolza2z31zzcnst_cachez00))
											{	/* Cnst/node.scm 316 */
												if (BOOLEANP(BgL_actualzd2valuezd2_2500))
													{	/* Cnst/node.scm 317 */
														if (CBOOL(BgL_actualzd2valuezd2_2500))
															{	/* Cnst/node.scm 319 */
																BgL_refz00_bglt BgL_new1163z00_2503;

																{	/* Cnst/node.scm 320 */
																	BgL_refz00_bglt BgL_new1162z00_2504;

																	BgL_new1162z00_2504 =
																		((BgL_refz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_refz00_bgl))));
																	{	/* Cnst/node.scm 320 */
																		long BgL_arg1939z00_2505;

																		BgL_arg1939z00_2505 =
																			BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1162z00_2504),
																			BgL_arg1939z00_2505);
																	}
																	{	/* Cnst/node.scm 320 */
																		BgL_objectz00_bglt BgL_tmpz00_3087;

																		BgL_tmpz00_3087 =
																			((BgL_objectz00_bglt)
																			BgL_new1162z00_2504);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3087,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1162z00_2504);
																	BgL_new1163z00_2503 = BgL_new1162z00_2504;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1163z00_2503)))->BgL_locz00) =
																	((obj_t) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt) (
																							(BgL_appz00_bglt)
																							BgL_nodez00_2391))))->
																			BgL_locz00)), BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1163z00_2503)))->BgL_typez00) =
																	((BgL_typez00_bglt) ((BgL_typez00_bglt)
																			BGl_za2bboolza2z00zztype_cachez00)),
																	BUNSPEC);
																((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																					BgL_new1163z00_2503)))->
																		BgL_variablez00) =
																	((BgL_variablez00_bglt) (
																			(BgL_variablez00_bglt)
																			BGl_za2btrueza2z00zzcnst_cachez00)),
																	BUNSPEC);
																return ((BgL_nodez00_bglt) BgL_new1163z00_2503);
															}
														else
															{	/* Cnst/node.scm 323 */
																BgL_refz00_bglt BgL_new1165z00_2506;

																{	/* Cnst/node.scm 324 */
																	BgL_refz00_bglt BgL_new1164z00_2507;

																	BgL_new1164z00_2507 =
																		((BgL_refz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_refz00_bgl))));
																	{	/* Cnst/node.scm 324 */
																		long BgL_arg1940z00_2508;

																		BgL_arg1940z00_2508 =
																			BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1164z00_2507),
																			BgL_arg1940z00_2508);
																	}
																	{	/* Cnst/node.scm 324 */
																		BgL_objectz00_bglt BgL_tmpz00_3107;

																		BgL_tmpz00_3107 =
																			((BgL_objectz00_bglt)
																			BgL_new1164z00_2507);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3107,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1164z00_2507);
																	BgL_new1165z00_2506 = BgL_new1164z00_2507;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1165z00_2506)))->BgL_locz00) =
																	((obj_t) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt) (
																							(BgL_appz00_bglt)
																							BgL_nodez00_2391))))->
																			BgL_locz00)), BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1165z00_2506)))->BgL_typez00) =
																	((BgL_typez00_bglt) ((BgL_typez00_bglt)
																			BGl_za2bboolza2z00zztype_cachez00)),
																	BUNSPEC);
																((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																					BgL_new1165z00_2506)))->
																		BgL_variablez00) =
																	((BgL_variablez00_bglt) (
																			(BgL_variablez00_bglt)
																			BGl_za2bfalseza2z00zzcnst_cachez00)),
																	BUNSPEC);
																return ((BgL_nodez00_bglt) BgL_new1165z00_2506);
															}
													}
												else
													{	/* Cnst/node.scm 317 */
														return
															((BgL_nodez00_bglt)
															((BgL_appz00_bglt) BgL_nodez00_2391));
													}
											}
										else
											{	/* Cnst/node.scm 328 */
												bool_t BgL_test2165z00_3125;

												if (
													(((obj_t) BgL_funz00_2498) ==
														BGl_za2makezd2fxzd2procedureza2z00zzcnst_cachez00))
													{	/* Cnst/node.scm 328 */
														BgL_test2165z00_3125 = ((bool_t) 1);
													}
												else
													{	/* Cnst/node.scm 328 */
														BgL_test2165z00_3125 =
															(
															((obj_t) BgL_funz00_2498) ==
															BGl_za2makezd2vazd2procedureza2z00zzcnst_cachez00);
													}
												if (BgL_test2165z00_3125)
													{	/* Cnst/node.scm 330 */
														obj_t BgL_siza7ezd2valuez75_2509;

														{	/* Cnst/node.scm 330 */
															obj_t BgL_arg1945z00_2510;

															{	/* Cnst/node.scm 330 */
																obj_t BgL_pairz00_2511;

																BgL_pairz00_2511 =
																	(((BgL_appz00_bglt) COBJECT(
																			((BgL_appz00_bglt) BgL_nodez00_2391)))->
																	BgL_argsz00);
																BgL_arg1945z00_2510 =
																	CAR(CDR(CDR(BgL_pairz00_2511)));
															}
															BgL_siza7ezd2valuez75_2509 =
																BGl_getzd2nodezd2atomzd2valuezd2zzcnst_nodez00(
																((BgL_nodez00_bglt) BgL_arg1945z00_2510));
														}
														{	/* Cnst/node.scm 331 */
															bool_t BgL_test2167z00_3138;

															if (INTEGERP(BgL_siza7ezd2valuez75_2509))
																{	/* Cnst/node.scm 331 */
																	BgL_test2167z00_3138 =
																		(
																		(long) CINT(BgL_siza7ezd2valuez75_2509) ==
																		0L);
																}
															else
																{	/* Cnst/node.scm 331 */
																	BgL_test2167z00_3138 = ((bool_t) 0);
																}
															if (BgL_test2167z00_3138)
																{	/* Cnst/node.scm 332 */
																	obj_t BgL_arg1944z00_2512;

																	BgL_arg1944z00_2512 =
																		(((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					((BgL_appz00_bglt)
																						BgL_nodez00_2391))))->BgL_locz00);
																	return
																		BGl_cnstzd2alloczd2procedurez00zzcnst_allocz00
																		(((BgL_nodez00_bglt) ((BgL_appz00_bglt)
																				BgL_nodez00_2391)),
																		BgL_arg1944z00_2512);
																}
															else
																{	/* Cnst/node.scm 331 */
																	return
																		((BgL_nodez00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_2391));
																}
														}
													}
												else
													{	/* Cnst/node.scm 328 */
														if (
															(((obj_t) BgL_funz00_2498) ==
																BGl_za2makezd2lzd2procedureza2z00zzcnst_cachez00))
															{	/* Cnst/node.scm 335 */
																obj_t BgL_siza7ezd2valuez75_2513;

																{	/* Cnst/node.scm 335 */
																	obj_t BgL_arg1950z00_2514;

																	{	/* Cnst/node.scm 335 */
																		obj_t BgL_pairz00_2515;

																		BgL_pairz00_2515 =
																			(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						BgL_nodez00_2391)))->BgL_argsz00);
																		BgL_arg1950z00_2514 =
																			CAR(CDR(BgL_pairz00_2515));
																	}
																	BgL_siza7ezd2valuez75_2513 =
																		BGl_getzd2nodezd2atomzd2valuezd2zzcnst_nodez00
																		(((BgL_nodez00_bglt) BgL_arg1950z00_2514));
																}
																{	/* Cnst/node.scm 336 */
																	bool_t BgL_test2170z00_3160;

																	if (INTEGERP(BgL_siza7ezd2valuez75_2513))
																		{	/* Cnst/node.scm 336 */
																			BgL_test2170z00_3160 =
																				(
																				(long) CINT(BgL_siza7ezd2valuez75_2513)
																				== 0L);
																		}
																	else
																		{	/* Cnst/node.scm 336 */
																			BgL_test2170z00_3160 = ((bool_t) 0);
																		}
																	if (BgL_test2170z00_3160)
																		{	/* Cnst/node.scm 337 */
																			obj_t BgL_arg1949z00_2516;

																			BgL_arg1949z00_2516 =
																				(((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							((BgL_appz00_bglt)
																								BgL_nodez00_2391))))->
																				BgL_locz00);
																			return
																				BGl_cnstzd2alloczd2lzd2procedurezd2zzcnst_allocz00
																				(((BgL_nodez00_bglt) ((BgL_appz00_bglt)
																						BgL_nodez00_2391)),
																				BgL_arg1949z00_2516);
																		}
																	else
																		{	/* Cnst/node.scm 336 */
																			return
																				((BgL_nodez00_bglt)
																				((BgL_appz00_bglt) BgL_nodez00_2391));
																		}
																}
															}
														else
															{	/* Cnst/node.scm 334 */
																if (
																	(((obj_t) BgL_funz00_2498) ==
																		BGl_za2doublezd2ze3realza2z31zzcnst_cachez00))
																	{	/* Cnst/node.scm 340 */
																		bool_t BgL_test2173z00_3176;

																		if (INTEGERP(BgL_actualzd2valuezd2_2500))
																			{	/* Cnst/node.scm 340 */
																				BgL_test2173z00_3176 = ((bool_t) 1);
																			}
																		else
																			{	/* Cnst/node.scm 340 */
																				BgL_test2173z00_3176 =
																					REALP(BgL_actualzd2valuezd2_2500);
																			}
																		if (BgL_test2173z00_3176)
																			{	/* Cnst/node.scm 341 */
																				obj_t BgL_arg1953z00_2517;

																				BgL_arg1953z00_2517 =
																					(((BgL_nodez00_bglt) COBJECT(
																							((BgL_nodez00_bglt)
																								((BgL_appz00_bglt)
																									BgL_nodez00_2391))))->
																					BgL_locz00);
																				return
																					BGl_cnstzd2alloczd2realz00zzcnst_allocz00
																					(BgL_actualzd2valuezd2_2500,
																					BgL_arg1953z00_2517);
																			}
																		else
																			{	/* Cnst/node.scm 340 */
																				return
																					((BgL_nodez00_bglt)
																					((BgL_appz00_bglt) BgL_nodez00_2391));
																			}
																	}
																else
																	{	/* Cnst/node.scm 339 */
																		if (
																			(((obj_t) BgL_funz00_2498) ==
																				BGl_za2elongzd2ze3belongza2z31zzcnst_cachez00))
																			{	/* Cnst/node.scm 343 */
																				if (ELONGP(BgL_actualzd2valuezd2_2500))
																					{	/* Cnst/node.scm 345 */
																						obj_t BgL_arg1955z00_2518;

																						BgL_arg1955z00_2518 =
																							(((BgL_nodez00_bglt) COBJECT(
																									((BgL_nodez00_bglt)
																										((BgL_appz00_bglt)
																											BgL_nodez00_2391))))->
																							BgL_locz00);
																						return
																							BGl_cnstzd2alloczd2elongz00zzcnst_allocz00
																							(BELONG_TO_LONG
																							(BgL_actualzd2valuezd2_2500),
																							BgL_arg1955z00_2518);
																					}
																				else
																					{	/* Cnst/node.scm 344 */
																						return
																							((BgL_nodez00_bglt)
																							((BgL_appz00_bglt)
																								BgL_nodez00_2391));
																					}
																			}
																		else
																			{	/* Cnst/node.scm 343 */
																				if (
																					(((obj_t) BgL_funz00_2498) ==
																						BGl_za2llongzd2ze3bllongza2z31zzcnst_cachez00))
																					{	/* Cnst/node.scm 347 */
																						if (LLONGP
																							(BgL_actualzd2valuezd2_2500))
																							{	/* Cnst/node.scm 349 */
																								obj_t BgL_arg1957z00_2519;

																								BgL_arg1957z00_2519 =
																									(((BgL_nodez00_bglt) COBJECT(
																											((BgL_nodez00_bglt)
																												((BgL_appz00_bglt)
																													BgL_nodez00_2391))))->
																									BgL_locz00);
																								return
																									BGl_cnstzd2alloczd2llongz00zzcnst_allocz00
																									(BLLONG_TO_LLONG
																									(BgL_actualzd2valuezd2_2500),
																									BgL_arg1957z00_2519);
																							}
																						else
																							{	/* Cnst/node.scm 348 */
																								return
																									((BgL_nodez00_bglt)
																									((BgL_appz00_bglt)
																										BgL_nodez00_2391));
																							}
																					}
																				else
																					{	/* Cnst/node.scm 347 */
																						if (
																							(((obj_t) BgL_funz00_2498) ==
																								BGl_za2int32zd2ze3bint32za2z31zzcnst_cachez00))
																							{	/* Cnst/node.scm 351 */
																								if (BGL_INT32P
																									(BgL_actualzd2valuezd2_2500))
																									{	/* Cnst/node.scm 353 */
																										obj_t BgL_arg1959z00_2520;

																										BgL_arg1959z00_2520 =
																											(((BgL_nodez00_bglt)
																												COBJECT((
																														(BgL_nodez00_bglt) (
																															(BgL_appz00_bglt)
																															BgL_nodez00_2391))))->
																											BgL_locz00);
																										return
																											BGl_cnstzd2alloczd2int32z00zzcnst_allocz00
																											(BGL_BINT32_TO_INT32
																											(BgL_actualzd2valuezd2_2500),
																											BgL_arg1959z00_2520);
																									}
																								else
																									{	/* Cnst/node.scm 352 */
																										return
																											((BgL_nodez00_bglt)
																											((BgL_appz00_bglt)
																												BgL_nodez00_2391));
																									}
																							}
																						else
																							{	/* Cnst/node.scm 351 */
																								if (
																									(((obj_t) BgL_funz00_2498) ==
																										BGl_za2uint32zd2ze3buint32za2z31zzcnst_cachez00))
																									{	/* Cnst/node.scm 355 */
																										if (BGL_UINT32P
																											(BgL_actualzd2valuezd2_2500))
																											{	/* Cnst/node.scm 357 */
																												obj_t
																													BgL_arg1961z00_2521;
																												BgL_arg1961z00_2521 =
																													(((BgL_nodez00_bglt)
																														COBJECT((
																																(BgL_nodez00_bglt)
																																((BgL_appz00_bglt) BgL_nodez00_2391))))->BgL_locz00);
																												return
																													BGl_cnstzd2alloczd2uint32z00zzcnst_allocz00
																													(BGL_BUINT32_TO_UINT32
																													(BgL_actualzd2valuezd2_2500),
																													BgL_arg1961z00_2521);
																											}
																										else
																											{	/* Cnst/node.scm 356 */
																												return
																													((BgL_nodez00_bglt)
																													((BgL_appz00_bglt)
																														BgL_nodez00_2391));
																											}
																									}
																								else
																									{	/* Cnst/node.scm 355 */
																										if (
																											(((obj_t) BgL_funz00_2498)
																												==
																												BGl_za2int64zd2ze3bint64za2z31zzcnst_cachez00))
																											{	/* Cnst/node.scm 359 */
																												if (BGL_INT64P
																													(BgL_actualzd2valuezd2_2500))
																													{	/* Cnst/node.scm 361 */
																														obj_t
																															BgL_arg1963z00_2522;
																														BgL_arg1963z00_2522
																															=
																															(((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2391))))->BgL_locz00);
																														return
																															BGl_cnstzd2alloczd2int64z00zzcnst_allocz00
																															(BGL_BINT64_TO_INT64
																															(BgL_actualzd2valuezd2_2500),
																															BgL_arg1963z00_2522);
																													}
																												else
																													{	/* Cnst/node.scm 360 */
																														return
																															(
																															(BgL_nodez00_bglt)
																															((BgL_appz00_bglt)
																																BgL_nodez00_2391));
																													}
																											}
																										else
																											{	/* Cnst/node.scm 359 */
																												if (
																													(((obj_t)
																															BgL_funz00_2498)
																														==
																														BGl_za2uint64zd2ze3buint64za2z31zzcnst_cachez00))
																													{	/* Cnst/node.scm 363 */
																														if (BGL_UINT64P
																															(BgL_actualzd2valuezd2_2500))
																															{	/* Cnst/node.scm 365 */
																																obj_t
																																	BgL_arg1965z00_2523;
																																BgL_arg1965z00_2523
																																	=
																																	(((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2391))))->BgL_locz00);
																																return
																																	BGl_cnstzd2alloczd2uint64z00zzcnst_allocz00
																																	(BGL_BINT64_TO_INT64
																																	(BgL_actualzd2valuezd2_2500),
																																	BgL_arg1965z00_2523);
																															}
																														else
																															{	/* Cnst/node.scm 364 */
																																return
																																	(
																																	(BgL_nodez00_bglt)
																																	((BgL_appz00_bglt) BgL_nodez00_2391));
																															}
																													}
																												else
																													{	/* Cnst/node.scm 363 */
																														if (
																															(((obj_t)
																																	BgL_funz00_2498)
																																==
																																BGl_za2fixnumzd2ze3bignumza2z31zzcnst_cachez00))
																															{	/* Cnst/node.scm 367 */
																																if (INTEGERP
																																	(BgL_actualzd2valuezd2_2500))
																																	{	/* Cnst/node.scm 369 */
																																		obj_t
																																			BgL__ortest_1166z00_2524;
																																		{	/* Cnst/node.scm 369 */
																																			obj_t
																																				BgL_arg1967z00_2525;
																																			obj_t
																																				BgL_arg1968z00_2526;
																																			BgL_arg1967z00_2525
																																				=
																																				bgl_long_to_bignum
																																				((long)
																																				CINT
																																				(BgL_actualzd2valuezd2_2500));
																																			BgL_arg1968z00_2526
																																				=
																																				(((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2391))))->BgL_locz00);
																																			BgL__ortest_1166z00_2524
																																				=
																																				BGl_cnstzd2alloczd2bignumz00zzcnst_allocz00
																																				(BgL_arg1967z00_2525,
																																				BgL_arg1968z00_2526);
																																		}
																																		if (CBOOL
																																			(BgL__ortest_1166z00_2524))
																																			{	/* Cnst/node.scm 369 */
																																				return
																																					(
																																					(BgL_nodez00_bglt)
																																					BgL__ortest_1166z00_2524);
																																			}
																																		else
																																			{	/* Cnst/node.scm 369 */
																																				return
																																					(
																																					(BgL_nodez00_bglt)
																																					((BgL_appz00_bglt) BgL_nodez00_2391));
																																			}
																																	}
																																else
																																	{	/* Cnst/node.scm 368 */
																																		return
																																			(
																																			(BgL_nodez00_bglt)
																																			((BgL_appz00_bglt) BgL_nodez00_2391));
																																	}
																															}
																														else
																															{	/* Cnst/node.scm 367 */
																																return
																																	(
																																	(BgL_nodez00_bglt)
																																	((BgL_appz00_bglt) BgL_nodez00_2391));
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
						}
					}
				}
		}

	}



/* &cnst!-funcall1344 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2funcall1344za2zzcnst_nodez00(obj_t
		BgL_envz00_2392, obj_t BgL_nodez00_2393)
	{
		{	/* Cnst/node.scm 280 */
			{
				BgL_nodez00_bglt BgL_auxz00_3278;

				{	/* Cnst/node.scm 282 */
					BgL_nodez00_bglt BgL_arg1931z00_2528;

					BgL_arg1931z00_2528 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2393)))->BgL_funz00);
					BgL_auxz00_3278 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1931z00_2528);
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2393)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3278), BUNSPEC);
			}
			BGl_cnstza2z12zb0zzcnst_nodez00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2393)))->BgL_argsz00));
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_2393));
		}

	}



/* &cnst!-app-ly1342 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2appzd2ly1342z70zzcnst_nodez00(obj_t
		BgL_envz00_2394, obj_t BgL_nodez00_2395)
	{
		{	/* Cnst/node.scm 271 */
			{
				BgL_nodez00_bglt BgL_auxz00_3289;

				{	/* Cnst/node.scm 273 */
					BgL_nodez00_bglt BgL_arg1929z00_2530;

					BgL_arg1929z00_2530 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2395)))->BgL_funz00);
					BgL_auxz00_3289 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1929z00_2530);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2395)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3289), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3295;

				{	/* Cnst/node.scm 274 */
					BgL_nodez00_bglt BgL_arg1930z00_2531;

					BgL_arg1930z00_2531 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2395)))->BgL_argz00);
					BgL_auxz00_3295 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1930z00_2531);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2395)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3295), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2395));
		}

	}



/* &cnst!-box-set!1340 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2boxzd2setz121340z62zzcnst_nodez00(obj_t
		BgL_envz00_2396, obj_t BgL_nodez00_2397)
	{
		{	/* Cnst/node.scm 262 */
			{
				BgL_varz00_bglt BgL_auxz00_3303;

				{	/* Cnst/node.scm 264 */
					BgL_varz00_bglt BgL_arg1927z00_2533;

					BgL_arg1927z00_2533 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2397)))->BgL_varz00);
					BgL_auxz00_3303 =
						((BgL_varz00_bglt)
						BGl_cnstz12z12zzcnst_nodez00(
							((BgL_nodez00_bglt) BgL_arg1927z00_2533)));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2397)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3303), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3311;

				{	/* Cnst/node.scm 265 */
					BgL_nodez00_bglt BgL_arg1928z00_2534;

					BgL_arg1928z00_2534 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2397)))->BgL_valuez00);
					BgL_auxz00_3311 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1928z00_2534);
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2397)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3311), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2397));
		}

	}



/* &cnst!-box-ref1338 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2boxzd2ref1338z70zzcnst_nodez00(obj_t
		BgL_envz00_2398, obj_t BgL_nodez00_2399)
	{
		{	/* Cnst/node.scm 254 */
			{
				BgL_varz00_bglt BgL_auxz00_3319;

				{	/* Cnst/node.scm 256 */
					BgL_varz00_bglt BgL_arg1926z00_2536;

					BgL_arg1926z00_2536 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2399)))->BgL_varz00);
					BgL_auxz00_3319 =
						((BgL_varz00_bglt)
						BGl_cnstz12z12zzcnst_nodez00(
							((BgL_nodez00_bglt) BgL_arg1926z00_2536)));
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2399)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3319), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2399));
		}

	}



/* &cnst!-make-box1336 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2makezd2box1336z70zzcnst_nodez00(obj_t
		BgL_envz00_2400, obj_t BgL_nodez00_2401)
	{
		{	/* Cnst/node.scm 246 */
			{
				BgL_nodez00_bglt BgL_auxz00_3329;

				{	/* Cnst/node.scm 248 */
					BgL_nodez00_bglt BgL_arg1925z00_2538;

					BgL_arg1925z00_2538 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2401)))->BgL_valuez00);
					BgL_auxz00_3329 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1925z00_2538);
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2401)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3329), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2401));
		}

	}



/* &cnst!-jump-ex-it1334 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2jumpzd2exzd2it1334za2zzcnst_nodez00(obj_t
		BgL_envz00_2402, obj_t BgL_nodez00_2403)
	{
		{	/* Cnst/node.scm 237 */
			{
				BgL_nodez00_bglt BgL_auxz00_3337;

				{	/* Cnst/node.scm 239 */
					BgL_nodez00_bglt BgL_arg1923z00_2540;

					BgL_arg1923z00_2540 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2403)))->BgL_exitz00);
					BgL_auxz00_3337 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1923z00_2540);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2403)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_3337), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3343;

				{	/* Cnst/node.scm 240 */
					BgL_nodez00_bglt BgL_arg1924z00_2541;

					BgL_arg1924z00_2541 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2403)))->
						BgL_valuez00);
					BgL_auxz00_3343 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1924z00_2541);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2403)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_3343), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2403));
		}

	}



/* &cnst!-set-ex-it1332 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2setzd2exzd2it1332za2zzcnst_nodez00(obj_t
		BgL_envz00_2404, obj_t BgL_nodez00_2405)
	{
		{	/* Cnst/node.scm 228 */
			{
				BgL_nodez00_bglt BgL_auxz00_3351;

				{	/* Cnst/node.scm 230 */
					BgL_nodez00_bglt BgL_arg1919z00_2543;

					BgL_arg1919z00_2543 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2405)))->BgL_bodyz00);
					BgL_auxz00_3351 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1919z00_2543);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2405)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3351), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3357;

				{	/* Cnst/node.scm 231 */
					BgL_nodez00_bglt BgL_arg1920z00_2544;

					BgL_arg1920z00_2544 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2405)))->
						BgL_onexitz00);
					BgL_auxz00_3357 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1920z00_2544);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2405)))->
						BgL_onexitz00) = ((BgL_nodez00_bglt) BgL_auxz00_3357), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2405));
		}

	}



/* &cnst!-let-var1330 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2letzd2var1330z70zzcnst_nodez00(obj_t
		BgL_envz00_2406, obj_t BgL_nodez00_2407)
	{
		{	/* Cnst/node.scm 213 */
			{	/* Cnst/node.scm 215 */
				obj_t BgL_g1296z00_2546;

				BgL_g1296z00_2546 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2407)))->BgL_bindingsz00);
				{
					obj_t BgL_l1294z00_2548;

					BgL_l1294z00_2548 = BgL_g1296z00_2546;
				BgL_zc3z04anonymousza31905ze3z87_2547:
					if (PAIRP(BgL_l1294z00_2548))
						{	/* Cnst/node.scm 215 */
							{	/* Cnst/node.scm 216 */
								obj_t BgL_bindingz00_2549;

								BgL_bindingz00_2549 = CAR(BgL_l1294z00_2548);
								{	/* Cnst/node.scm 216 */
									obj_t BgL_varz00_2550;

									BgL_varz00_2550 = CAR(((obj_t) BgL_bindingz00_2549));
									{	/* Cnst/node.scm 217 */
										BgL_nodez00_bglt BgL_arg1910z00_2551;

										{	/* Cnst/node.scm 217 */
											obj_t BgL_arg1911z00_2552;

											BgL_arg1911z00_2552 = CDR(((obj_t) BgL_bindingz00_2549));
											BgL_arg1910z00_2551 =
												BGl_cnstz12z12zzcnst_nodez00(
												((BgL_nodez00_bglt) BgL_arg1911z00_2552));
										}
										{	/* Cnst/node.scm 217 */
											obj_t BgL_auxz00_3378;
											obj_t BgL_tmpz00_3376;

											BgL_auxz00_3378 = ((obj_t) BgL_arg1910z00_2551);
											BgL_tmpz00_3376 = ((obj_t) BgL_bindingz00_2549);
											SET_CDR(BgL_tmpz00_3376, BgL_auxz00_3378);
										}
									}
									if (
										((((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt) BgL_varz00_2550))))->
												BgL_accessz00) == CNST_TABLE_REF(7)))
										{	/* Cnst/node.scm 218 */
											{	/* Cnst/node.scm 219 */
												BgL_localzf2bvaluezf2_bglt BgL_wide1152z00_2553;

												BgL_wide1152z00_2553 =
													((BgL_localzf2bvaluezf2_bglt)
													BOBJECT(GC_MALLOC(sizeof(struct
																BgL_localzf2bvaluezf2_bgl))));
												{	/* Cnst/node.scm 219 */
													obj_t BgL_auxz00_3392;
													BgL_objectz00_bglt BgL_tmpz00_3388;

													BgL_auxz00_3392 = ((obj_t) BgL_wide1152z00_2553);
													BgL_tmpz00_3388 =
														((BgL_objectz00_bglt)
														((BgL_localz00_bglt)
															((BgL_localz00_bglt) BgL_varz00_2550)));
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3388,
														BgL_auxz00_3392);
												}
												((BgL_objectz00_bglt)
													((BgL_localz00_bglt)
														((BgL_localz00_bglt) BgL_varz00_2550)));
												{	/* Cnst/node.scm 219 */
													long BgL_arg1914z00_2554;

													BgL_arg1914z00_2554 =
														BGL_CLASS_NUM(BGl_localzf2bvaluezf2zzcnst_nodez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt)
															((BgL_localz00_bglt)
																((BgL_localz00_bglt) BgL_varz00_2550))),
														BgL_arg1914z00_2554);
												}
												((BgL_localz00_bglt)
													((BgL_localz00_bglt)
														((BgL_localz00_bglt) BgL_varz00_2550)));
											}
											{
												BgL_localzf2bvaluezf2_bglt BgL_auxz00_3406;

												{
													obj_t BgL_auxz00_3407;

													{	/* Cnst/node.scm 220 */
														BgL_objectz00_bglt BgL_tmpz00_3408;

														BgL_tmpz00_3408 =
															((BgL_objectz00_bglt)
															((BgL_localz00_bglt)
																((BgL_localz00_bglt) BgL_varz00_2550)));
														BgL_auxz00_3407 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3408);
													}
													BgL_auxz00_3406 =
														((BgL_localzf2bvaluezf2_bglt) BgL_auxz00_3407);
												}
												((((BgL_localzf2bvaluezf2_bglt)
															COBJECT(BgL_auxz00_3406))->
														BgL_bindingzd2valuezd2) =
													((BgL_nodez00_bglt) ((BgL_nodez00_bglt) CDR(((obj_t)
																	BgL_bindingz00_2549)))), BUNSPEC);
											}
											((obj_t)
												((BgL_localz00_bglt)
													((BgL_localz00_bglt) BgL_varz00_2550)));
										}
									else
										{	/* Cnst/node.scm 218 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1294z00_3421;

								BgL_l1294z00_3421 = CDR(BgL_l1294z00_2548);
								BgL_l1294z00_2548 = BgL_l1294z00_3421;
								goto BgL_zc3z04anonymousza31905ze3z87_2547;
							}
						}
					else
						{	/* Cnst/node.scm 215 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3423;

				{	/* Cnst/node.scm 222 */
					BgL_nodez00_bglt BgL_arg1918z00_2555;

					BgL_arg1918z00_2555 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2407)))->BgL_bodyz00);
					BgL_auxz00_3423 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1918z00_2555);
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2407)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3423), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_2407));
		}

	}



/* &cnst!-let-fun1328 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2letzd2fun1328z70zzcnst_nodez00(obj_t
		BgL_envz00_2408, obj_t BgL_nodez00_2409)
	{
		{	/* Cnst/node.scm 201 */
			{	/* Cnst/node.scm 203 */
				obj_t BgL_g1293z00_2557;

				BgL_g1293z00_2557 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2409)))->BgL_localsz00);
				{
					obj_t BgL_l1291z00_2559;

					BgL_l1291z00_2559 = BgL_g1293z00_2557;
				BgL_zc3z04anonymousza31898ze3z87_2558:
					if (PAIRP(BgL_l1291z00_2559))
						{	/* Cnst/node.scm 203 */
							{	/* Cnst/node.scm 204 */
								obj_t BgL_localz00_2560;

								BgL_localz00_2560 = CAR(BgL_l1291z00_2559);
								{	/* Cnst/node.scm 204 */
									BgL_valuez00_bglt BgL_funz00_2561;

									BgL_funz00_2561 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2560))))->
										BgL_valuez00);
									{	/* Cnst/node.scm 205 */
										BgL_nodez00_bglt BgL_arg1901z00_2562;

										{	/* Cnst/node.scm 205 */
											obj_t BgL_arg1902z00_2563;

											BgL_arg1902z00_2563 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2561)))->
												BgL_bodyz00);
											BgL_arg1901z00_2562 =
												BGl_cnstz12z12zzcnst_nodez00(((BgL_nodez00_bglt)
													BgL_arg1902z00_2563));
										}
										((((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2561)))->
												BgL_bodyz00) =
											((obj_t) ((obj_t) BgL_arg1901z00_2562)), BUNSPEC);
									}
								}
							}
							{
								obj_t BgL_l1291z00_3446;

								BgL_l1291z00_3446 = CDR(BgL_l1291z00_2559);
								BgL_l1291z00_2559 = BgL_l1291z00_3446;
								goto BgL_zc3z04anonymousza31898ze3z87_2558;
							}
						}
					else
						{	/* Cnst/node.scm 203 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3448;

				{	/* Cnst/node.scm 207 */
					BgL_nodez00_bglt BgL_arg1904z00_2564;

					BgL_arg1904z00_2564 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2409)))->BgL_bodyz00);
					BgL_auxz00_3448 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1904z00_2564);
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2409)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3448), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2409));
		}

	}



/* &cnst!-switch1326 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2switch1326za2zzcnst_nodez00(obj_t
		BgL_envz00_2410, obj_t BgL_nodez00_2411)
	{
		{	/* Cnst/node.scm 190 */
			{
				BgL_nodez00_bglt BgL_auxz00_3456;

				{	/* Cnst/node.scm 192 */
					BgL_nodez00_bglt BgL_arg1891z00_2566;

					BgL_arg1891z00_2566 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2411)))->BgL_testz00);
					BgL_auxz00_3456 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1891z00_2566);
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2411)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3456), BUNSPEC);
			}
			{	/* Cnst/node.scm 193 */
				obj_t BgL_g1290z00_2567;

				BgL_g1290z00_2567 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2411)))->BgL_clausesz00);
				{
					obj_t BgL_l1288z00_2569;

					BgL_l1288z00_2569 = BgL_g1290z00_2567;
				BgL_zc3z04anonymousza31892ze3z87_2568:
					if (PAIRP(BgL_l1288z00_2569))
						{	/* Cnst/node.scm 193 */
							{	/* Cnst/node.scm 194 */
								obj_t BgL_clausez00_2570;

								BgL_clausez00_2570 = CAR(BgL_l1288z00_2569);
								{	/* Cnst/node.scm 194 */
									BgL_nodez00_bglt BgL_arg1894z00_2571;

									{	/* Cnst/node.scm 194 */
										obj_t BgL_arg1896z00_2572;

										BgL_arg1896z00_2572 = CDR(((obj_t) BgL_clausez00_2570));
										BgL_arg1894z00_2571 =
											BGl_cnstz12z12zzcnst_nodez00(
											((BgL_nodez00_bglt) BgL_arg1896z00_2572));
									}
									{	/* Cnst/node.scm 194 */
										obj_t BgL_auxz00_3473;
										obj_t BgL_tmpz00_3471;

										BgL_auxz00_3473 = ((obj_t) BgL_arg1894z00_2571);
										BgL_tmpz00_3471 = ((obj_t) BgL_clausez00_2570);
										SET_CDR(BgL_tmpz00_3471, BgL_auxz00_3473);
									}
								}
							}
							{
								obj_t BgL_l1288z00_3476;

								BgL_l1288z00_3476 = CDR(BgL_l1288z00_2569);
								BgL_l1288z00_2569 = BgL_l1288z00_3476;
								goto BgL_zc3z04anonymousza31892ze3z87_2568;
							}
						}
					else
						{	/* Cnst/node.scm 193 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_2411));
		}

	}



/* &cnst!-fail1324 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2fail1324za2zzcnst_nodez00(obj_t
		BgL_envz00_2412, obj_t BgL_nodez00_2413)
	{
		{	/* Cnst/node.scm 180 */
			{
				BgL_nodez00_bglt BgL_auxz00_3480;

				{	/* Cnst/node.scm 182 */
					BgL_nodez00_bglt BgL_arg1888z00_2574;

					BgL_arg1888z00_2574 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2413)))->BgL_procz00);
					BgL_auxz00_3480 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1888z00_2574);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2413)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3480), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3486;

				{	/* Cnst/node.scm 183 */
					BgL_nodez00_bglt BgL_arg1889z00_2575;

					BgL_arg1889z00_2575 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2413)))->BgL_msgz00);
					BgL_auxz00_3486 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1889z00_2575);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2413)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3486), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3492;

				{	/* Cnst/node.scm 184 */
					BgL_nodez00_bglt BgL_arg1890z00_2576;

					BgL_arg1890z00_2576 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2413)))->BgL_objz00);
					BgL_auxz00_3492 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1890z00_2576);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2413)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3492), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_2413));
		}

	}



/* &cnst!-conditional1322 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2conditional1322za2zzcnst_nodez00(obj_t
		BgL_envz00_2414, obj_t BgL_nodez00_2415)
	{
		{	/* Cnst/node.scm 170 */
			{
				BgL_nodez00_bglt BgL_auxz00_3500;

				{	/* Cnst/node.scm 172 */
					BgL_nodez00_bglt BgL_arg1884z00_2578;

					BgL_arg1884z00_2578 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2415)))->BgL_testz00);
					BgL_auxz00_3500 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1884z00_2578);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2415)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3500), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3506;

				{	/* Cnst/node.scm 173 */
					BgL_nodez00_bglt BgL_arg1885z00_2579;

					BgL_arg1885z00_2579 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2415)))->BgL_truez00);
					BgL_auxz00_3506 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1885z00_2579);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2415)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3506), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3512;

				{	/* Cnst/node.scm 174 */
					BgL_nodez00_bglt BgL_arg1887z00_2580;

					BgL_arg1887z00_2580 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2415)))->BgL_falsez00);
					BgL_auxz00_3512 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1887z00_2580);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2415)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3512), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_2415));
		}

	}



/* &cnst!-setq1320 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2setq1320za2zzcnst_nodez00(obj_t
		BgL_envz00_2416, obj_t BgL_nodez00_2417)
	{
		{	/* Cnst/node.scm 162 */
			{
				BgL_nodez00_bglt BgL_auxz00_3520;

				{	/* Cnst/node.scm 164 */
					BgL_nodez00_bglt BgL_arg1883z00_2582;

					BgL_arg1883z00_2582 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2417)))->BgL_valuez00);
					BgL_auxz00_3520 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1883z00_2582);
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2417)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3520), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_2417));
		}

	}



/* &cnst!-cast1318 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2cast1318za2zzcnst_nodez00(obj_t
		BgL_envz00_2418, obj_t BgL_nodez00_2419)
	{
		{	/* Cnst/node.scm 154 */
			{
				BgL_nodez00_bglt BgL_auxz00_3528;

				{	/* Cnst/node.scm 156 */
					BgL_nodez00_bglt BgL_arg1882z00_2584;

					BgL_arg1882z00_2584 =
						(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_2419)))->BgL_argz00);
					BgL_auxz00_3528 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1882z00_2584);
				}
				((((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_2419)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3528), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_2419));
		}

	}



/* &cnst!-extern1316 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2extern1316za2zzcnst_nodez00(obj_t
		BgL_envz00_2420, obj_t BgL_nodez00_2421)
	{
		{	/* Cnst/node.scm 146 */
			BGl_cnstza2z12zb0zzcnst_nodez00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2421)))->BgL_exprza2za2));
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2421));
		}

	}



/* &cnst!-sync1314 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2sync1314za2zzcnst_nodez00(obj_t
		BgL_envz00_2422, obj_t BgL_nodez00_2423)
	{
		{	/* Cnst/node.scm 136 */
			{
				BgL_nodez00_bglt BgL_auxz00_3541;

				{	/* Cnst/node.scm 138 */
					BgL_nodez00_bglt BgL_arg1877z00_2587;

					BgL_arg1877z00_2587 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2423)))->BgL_mutexz00);
					BgL_auxz00_3541 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1877z00_2587);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2423)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3541), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3547;

				{	/* Cnst/node.scm 139 */
					BgL_nodez00_bglt BgL_arg1878z00_2588;

					BgL_arg1878z00_2588 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2423)))->BgL_prelockz00);
					BgL_auxz00_3547 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1878z00_2588);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2423)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3547), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3553;

				{	/* Cnst/node.scm 140 */
					BgL_nodez00_bglt BgL_arg1879z00_2589;

					BgL_arg1879z00_2589 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2423)))->BgL_bodyz00);
					BgL_auxz00_3553 = BGl_cnstz12z12zzcnst_nodez00(BgL_arg1879z00_2589);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2423)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3553), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2423));
		}

	}



/* &cnst!-sequence1312 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2sequence1312za2zzcnst_nodez00(obj_t
		BgL_envz00_2424, obj_t BgL_nodez00_2425)
	{
		{	/* Cnst/node.scm 128 */
			BGl_cnstza2z12zb0zzcnst_nodez00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2425)))->BgL_nodesz00));
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_2425));
		}

	}



/* &cnst!-closure1310 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2closure1310za2zzcnst_nodez00(obj_t
		BgL_envz00_2426, obj_t BgL_nodez00_2427)
	{
		{	/* Cnst/node.scm 122 */
			{	/* Cnst/node.scm 123 */
				obj_t BgL_arg1875z00_2592;

				BgL_arg1875z00_2592 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_2427)));
				return
					((BgL_nodez00_bglt)
					BGl_internalzd2errorzd2zztools_errorz00
					(BGl_string2070z00zzcnst_nodez00, BGl_string2097z00zzcnst_nodez00,
						BgL_arg1875z00_2592));
			}
		}

	}



/* &cnst!-var1308 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2var1308za2zzcnst_nodez00(obj_t
		BgL_envz00_2428, obj_t BgL_nodez00_2429)
	{
		{	/* Cnst/node.scm 116 */
			return ((BgL_nodez00_bglt) ((BgL_varz00_bglt) BgL_nodez00_2429));
		}

	}



/* &cnst!-kwote1306 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2kwote1306za2zzcnst_nodez00(obj_t
		BgL_envz00_2430, obj_t BgL_nodez00_2431)
	{
		{	/* Cnst/node.scm 64 */
			{	/* Cnst/node.scm 67 */
				bool_t BgL_test2194z00_3573;

				{	/* Cnst/node.scm 67 */
					obj_t BgL_tmpz00_3574;

					BgL_tmpz00_3574 =
						(((BgL_kwotez00_bglt) COBJECT(
								((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
					BgL_test2194z00_3573 = SYMBOLP(BgL_tmpz00_3574);
				}
				if (BgL_test2194z00_3573)
					{	/* Cnst/node.scm 68 */
						obj_t BgL_arg1625z00_2595;
						obj_t BgL_arg1626z00_2596;

						BgL_arg1625z00_2595 =
							(((BgL_kwotez00_bglt) COBJECT(
									((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
						BgL_arg1626z00_2596 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_kwotez00_bglt) BgL_nodez00_2431))))->BgL_locz00);
						return
							BGl_cnstzd2alloczd2symbolz00zzcnst_allocz00(BgL_arg1625z00_2595,
							BgL_arg1626z00_2596);
					}
				else
					{	/* Cnst/node.scm 69 */
						bool_t BgL_test2195z00_3584;

						{	/* Cnst/node.scm 69 */
							obj_t BgL_tmpz00_3585;

							BgL_tmpz00_3585 =
								(((BgL_kwotez00_bglt) COBJECT(
										((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
							BgL_test2195z00_3584 = KEYWORDP(BgL_tmpz00_3585);
						}
						if (BgL_test2195z00_3584)
							{	/* Cnst/node.scm 70 */
								obj_t BgL_arg1629z00_2597;
								obj_t BgL_arg1630z00_2598;

								BgL_arg1629z00_2597 =
									(((BgL_kwotez00_bglt) COBJECT(
											((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
								BgL_arg1630z00_2598 =
									(((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_kwotez00_bglt) BgL_nodez00_2431))))->BgL_locz00);
								return
									BGl_cnstzd2alloczd2keywordz00zzcnst_allocz00
									(BgL_arg1629z00_2597, BgL_arg1630z00_2598);
							}
						else
							{	/* Cnst/node.scm 71 */
								bool_t BgL_test2196z00_3595;

								{	/* Cnst/node.scm 71 */
									obj_t BgL_tmpz00_3596;

									BgL_tmpz00_3596 =
										(((BgL_kwotez00_bglt) COBJECT(
												((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
									BgL_test2196z00_3595 = PAIRP(BgL_tmpz00_3596);
								}
								if (BgL_test2196z00_3595)
									{	/* Cnst/node.scm 72 */
										obj_t BgL_arg1646z00_2599;
										obj_t BgL_arg1650z00_2600;

										BgL_arg1646z00_2599 =
											(((BgL_kwotez00_bglt) COBJECT(
													((BgL_kwotez00_bglt) BgL_nodez00_2431)))->
											BgL_valuez00);
										BgL_arg1650z00_2600 =
											(((BgL_nodez00_bglt)
												COBJECT(((BgL_nodez00_bglt) ((BgL_kwotez00_bglt)
															BgL_nodez00_2431))))->BgL_locz00);
										return
											BGl_cnstzd2alloczd2listz00zzcnst_allocz00
											(BgL_arg1646z00_2599, BgL_arg1650z00_2600);
									}
								else
									{	/* Cnst/node.scm 73 */
										bool_t BgL_test2197z00_3606;

										{	/* Cnst/node.scm 73 */
											obj_t BgL_tmpz00_3607;

											BgL_tmpz00_3607 =
												(((BgL_kwotez00_bglt) COBJECT(
														((BgL_kwotez00_bglt) BgL_nodez00_2431)))->
												BgL_valuez00);
											BgL_test2197z00_3606 = VECTORP(BgL_tmpz00_3607);
										}
										if (BgL_test2197z00_3606)
											{	/* Cnst/node.scm 74 */
												obj_t BgL_arg1654z00_2601;
												obj_t BgL_arg1661z00_2602;

												BgL_arg1654z00_2601 =
													(((BgL_kwotez00_bglt) COBJECT(
															((BgL_kwotez00_bglt) BgL_nodez00_2431)))->
													BgL_valuez00);
												BgL_arg1661z00_2602 =
													(((BgL_nodez00_bglt)
														COBJECT(((BgL_nodez00_bglt) ((BgL_kwotez00_bglt)
																	BgL_nodez00_2431))))->BgL_locz00);
												return
													BGl_cnstzd2alloczd2vectorz00zzcnst_allocz00
													(BgL_arg1654z00_2601, BgL_arg1661z00_2602);
											}
										else
											{	/* Cnst/node.scm 75 */
												bool_t BgL_test2198z00_3617;

												{	/* Cnst/node.scm 75 */
													obj_t BgL_tmpz00_3618;

													BgL_tmpz00_3618 =
														(((BgL_kwotez00_bglt) COBJECT(
																((BgL_kwotez00_bglt) BgL_nodez00_2431)))->
														BgL_valuez00);
													BgL_test2198z00_3617 = BGL_HVECTORP(BgL_tmpz00_3618);
												}
												if (BgL_test2198z00_3617)
													{	/* Cnst/node.scm 76 */
														obj_t BgL_arg1675z00_2603;
														obj_t BgL_arg1678z00_2604;

														BgL_arg1675z00_2603 =
															(((BgL_kwotez00_bglt) COBJECT(
																	((BgL_kwotez00_bglt) BgL_nodez00_2431)))->
															BgL_valuez00);
														BgL_arg1678z00_2604 =
															(((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) ((BgL_kwotez00_bglt)
																			BgL_nodez00_2431))))->BgL_locz00);
														return
															BGl_cnstzd2alloczd2homogenouszd2vectorzd2zzcnst_allocz00
															(BgL_arg1675z00_2603, BgL_arg1678z00_2604);
													}
												else
													{	/* Cnst/node.scm 77 */
														bool_t BgL_test2199z00_3628;

														{	/* Cnst/node.scm 77 */
															obj_t BgL_tmpz00_3629;

															BgL_tmpz00_3629 =
																(((BgL_kwotez00_bglt) COBJECT(
																		((BgL_kwotez00_bglt) BgL_nodez00_2431)))->
																BgL_valuez00);
															BgL_test2199z00_3628 = STRINGP(BgL_tmpz00_3629);
														}
														if (BgL_test2199z00_3628)
															{	/* Cnst/node.scm 78 */
																obj_t BgL_arg1688z00_2605;
																obj_t BgL_arg1689z00_2606;

																BgL_arg1688z00_2605 =
																	(((BgL_kwotez00_bglt) COBJECT(
																			((BgL_kwotez00_bglt) BgL_nodez00_2431)))->
																	BgL_valuez00);
																BgL_arg1689z00_2606 =
																	(((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt) (
																					(BgL_kwotez00_bglt)
																					BgL_nodez00_2431))))->BgL_locz00);
																return
																	BGl_cnstzd2alloczd2stringz00zzcnst_allocz00
																	(BgL_arg1688z00_2605, BgL_arg1689z00_2606);
															}
														else
															{	/* Cnst/node.scm 79 */
																bool_t BgL_test2200z00_3639;

																{	/* Cnst/node.scm 79 */
																	obj_t BgL_tmpz00_3640;

																	BgL_tmpz00_3640 =
																		(((BgL_kwotez00_bglt) COBJECT(
																				((BgL_kwotez00_bglt)
																					BgL_nodez00_2431)))->BgL_valuez00);
																	BgL_test2200z00_3639 =
																		UCS2_STRINGP(BgL_tmpz00_3640);
																}
																if (BgL_test2200z00_3639)
																	{	/* Cnst/node.scm 80 */
																		obj_t BgL_arg1699z00_2607;
																		obj_t BgL_arg1700z00_2608;

																		BgL_arg1699z00_2607 =
																			(((BgL_kwotez00_bglt) COBJECT(
																					((BgL_kwotez00_bglt)
																						BgL_nodez00_2431)))->BgL_valuez00);
																		BgL_arg1700z00_2608 =
																			(((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt) (
																							(BgL_kwotez00_bglt)
																							BgL_nodez00_2431))))->BgL_locz00);
																		return
																			BGl_cnstzd2alloczd2ucs2zd2stringzd2zzcnst_allocz00
																			(BgL_arg1699z00_2607,
																			BgL_arg1700z00_2608);
																	}
																else
																	{	/* Cnst/node.scm 81 */
																		bool_t BgL_test2201z00_3650;

																		{	/* Cnst/node.scm 81 */
																			obj_t BgL_arg1864z00_2609;

																			BgL_arg1864z00_2609 =
																				(((BgL_kwotez00_bglt) COBJECT(
																						((BgL_kwotez00_bglt)
																							BgL_nodez00_2431)))->
																				BgL_valuez00);
																			if (STRUCTP(BgL_arg1864z00_2609))
																				{	/* Cnst/node.scm 81 */
																					BgL_test2201z00_3650 =
																						(STRUCT_KEY(BgL_arg1864z00_2609) ==
																						CNST_TABLE_REF(8));
																				}
																			else
																				{	/* Cnst/node.scm 81 */
																					BgL_test2201z00_3650 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test2201z00_3650)
																			{	/* Cnst/node.scm 82 */
																				obj_t BgL_arg1703z00_2610;
																				obj_t BgL_arg1705z00_2611;

																				BgL_arg1703z00_2610 =
																					(((BgL_kwotez00_bglt) COBJECT(
																							((BgL_kwotez00_bglt)
																								BgL_nodez00_2431)))->
																					BgL_valuez00);
																				BgL_arg1705z00_2611 =
																					(((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt) (
																									(BgL_kwotez00_bglt)
																									BgL_nodez00_2431))))->
																					BgL_locz00);
																				return
																					BGl_cnstzd2alloczd2tvectorz00zzcnst_allocz00
																					(BgL_arg1703z00_2610,
																					BgL_arg1705z00_2611);
																			}
																		else
																			{	/* Cnst/node.scm 83 */
																				bool_t BgL_test2203z00_3664;

																				{	/* Cnst/node.scm 83 */
																					bool_t BgL_test2204z00_3665;

																					{	/* Cnst/node.scm 83 */
																						obj_t BgL_tmpz00_3666;

																						BgL_tmpz00_3666 =
																							(((BgL_kwotez00_bglt) COBJECT(
																									((BgL_kwotez00_bglt)
																										BgL_nodez00_2431)))->
																							BgL_valuez00);
																						BgL_test2204z00_3665 =
																							CHARP(BgL_tmpz00_3666);
																					}
																					if (BgL_test2204z00_3665)
																						{	/* Cnst/node.scm 83 */
																							BgL_test2203z00_3664 =
																								((bool_t) 1);
																						}
																					else
																						{	/* Cnst/node.scm 83 */
																							if (INTEGERP(
																									(((BgL_kwotez00_bglt) COBJECT(
																												((BgL_kwotez00_bglt)
																													BgL_nodez00_2431)))->
																										BgL_valuez00)))
																								{	/* Cnst/node.scm 84 */
																									BgL_test2203z00_3664 =
																										((bool_t) 1);
																								}
																							else
																								{	/* Cnst/node.scm 85 */
																									bool_t BgL_test2206z00_3674;

																									{	/* Cnst/node.scm 85 */
																										obj_t BgL_tmpz00_3675;

																										BgL_tmpz00_3675 =
																											(((BgL_kwotez00_bglt)
																												COBJECT((
																														(BgL_kwotez00_bglt)
																														BgL_nodez00_2431)))->
																											BgL_valuez00);
																										BgL_test2206z00_3674 =
																											BOOLEANP(BgL_tmpz00_3675);
																									}
																									if (BgL_test2206z00_3674)
																										{	/* Cnst/node.scm 85 */
																											BgL_test2203z00_3664 =
																												((bool_t) 1);
																										}
																									else
																										{	/* Cnst/node.scm 86 */
																											bool_t
																												BgL_test2207z00_3679;
																											{	/* Cnst/node.scm 86 */
																												obj_t
																													BgL_arg1859z00_2612;
																												BgL_arg1859z00_2612 =
																													(((BgL_kwotez00_bglt)
																														COBJECT((
																																(BgL_kwotez00_bglt)
																																BgL_nodez00_2431)))->
																													BgL_valuez00);
																												if (INTEGERP
																													(BgL_arg1859z00_2612))
																													{	/* Cnst/node.scm 86 */
																														BgL_test2207z00_3679
																															= ((bool_t) 1);
																													}
																												else
																													{	/* Cnst/node.scm 86 */
																														BgL_test2207z00_3679
																															=
																															REALP
																															(BgL_arg1859z00_2612);
																													}
																											}
																											if (BgL_test2207z00_3679)
																												{	/* Cnst/node.scm 86 */
																													BgL_test2203z00_3664 =
																														((bool_t) 1);
																												}
																											else
																												{	/* Cnst/node.scm 87 */
																													bool_t
																														BgL_test2209z00_3685;
																													{	/* Cnst/node.scm 87 */
																														obj_t
																															BgL_tmpz00_3686;
																														BgL_tmpz00_3686 =
																															(((BgL_kwotez00_bglt) COBJECT(((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
																														BgL_test2209z00_3685
																															=
																															CNSTP
																															(BgL_tmpz00_3686);
																													}
																													if (BgL_test2209z00_3685)
																														{	/* Cnst/node.scm 87 */
																															BgL_test2203z00_3664
																																= ((bool_t) 1);
																														}
																													else
																														{	/* Cnst/node.scm 88 */
																															bool_t
																																BgL_test2210z00_3690;
																															{	/* Cnst/node.scm 88 */
																																obj_t
																																	BgL_tmpz00_3691;
																																BgL_tmpz00_3691
																																	=
																																	(((BgL_kwotez00_bglt) COBJECT(((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
																																BgL_test2210z00_3690
																																	=
																																	ELONGP
																																	(BgL_tmpz00_3691);
																															}
																															if (BgL_test2210z00_3690)
																																{	/* Cnst/node.scm 88 */
																																	BgL_test2203z00_3664
																																		=
																																		((bool_t)
																																		1);
																																}
																															else
																																{	/* Cnst/node.scm 89 */
																																	bool_t
																																		BgL_test2211z00_3695;
																																	{	/* Cnst/node.scm 89 */
																																		obj_t
																																			BgL_tmpz00_3696;
																																		BgL_tmpz00_3696
																																			=
																																			(((BgL_kwotez00_bglt) COBJECT(((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
																																		BgL_test2211z00_3695
																																			=
																																			LLONGP
																																			(BgL_tmpz00_3696);
																																	}
																																	if (BgL_test2211z00_3695)
																																		{	/* Cnst/node.scm 89 */
																																			BgL_test2203z00_3664
																																				=
																																				(
																																				(bool_t)
																																				1);
																																		}
																																	else
																																		{	/* Cnst/node.scm 90 */
																																			bool_t
																																				BgL_test2212z00_3700;
																																			{	/* Cnst/node.scm 90 */
																																				obj_t
																																					BgL_tmpz00_3701;
																																				BgL_tmpz00_3701
																																					=
																																					(((BgL_kwotez00_bglt) COBJECT(((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
																																				BgL_test2212z00_3700
																																					=
																																					BGL_INT8P
																																					(BgL_tmpz00_3701);
																																			}
																																			if (BgL_test2212z00_3700)
																																				{	/* Cnst/node.scm 90 */
																																					BgL_test2203z00_3664
																																						=
																																						(
																																						(bool_t)
																																						1);
																																				}
																																			else
																																				{	/* Cnst/node.scm 91 */
																																					bool_t
																																						BgL_test2213z00_3705;
																																					{	/* Cnst/node.scm 91 */
																																						obj_t
																																							BgL_tmpz00_3706;
																																						BgL_tmpz00_3706
																																							=
																																							(((BgL_kwotez00_bglt) COBJECT(((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
																																						BgL_test2213z00_3705
																																							=
																																							BGL_UINT8P
																																							(BgL_tmpz00_3706);
																																					}
																																					if (BgL_test2213z00_3705)
																																						{	/* Cnst/node.scm 91 */
																																							BgL_test2203z00_3664
																																								=
																																								(
																																								(bool_t)
																																								1);
																																						}
																																					else
																																						{	/* Cnst/node.scm 92 */
																																							bool_t
																																								BgL_test2214z00_3710;
																																							{	/* Cnst/node.scm 92 */
																																								obj_t
																																									BgL_tmpz00_3711;
																																								BgL_tmpz00_3711
																																									=
																																									(
																																									((BgL_kwotez00_bglt) COBJECT(((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
																																								BgL_test2214z00_3710
																																									=
																																									BGL_INT16P
																																									(BgL_tmpz00_3711);
																																							}
																																							if (BgL_test2214z00_3710)
																																								{	/* Cnst/node.scm 92 */
																																									BgL_test2203z00_3664
																																										=
																																										(
																																										(bool_t)
																																										1);
																																								}
																																							else
																																								{	/* Cnst/node.scm 93 */
																																									bool_t
																																										BgL_test2215z00_3715;
																																									{	/* Cnst/node.scm 93 */
																																										obj_t
																																											BgL_tmpz00_3716;
																																										BgL_tmpz00_3716
																																											=
																																											(
																																											((BgL_kwotez00_bglt) COBJECT(((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
																																										BgL_test2215z00_3715
																																											=
																																											BGL_UINT16P
																																											(BgL_tmpz00_3716);
																																									}
																																									if (BgL_test2215z00_3715)
																																										{	/* Cnst/node.scm 93 */
																																											BgL_test2203z00_3664
																																												=
																																												(
																																												(bool_t)
																																												1);
																																										}
																																									else
																																										{	/* Cnst/node.scm 94 */
																																											bool_t
																																												BgL_test2216z00_3720;
																																											{	/* Cnst/node.scm 94 */
																																												obj_t
																																													BgL_tmpz00_3721;
																																												BgL_tmpz00_3721
																																													=
																																													(
																																													((BgL_kwotez00_bglt) COBJECT(((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
																																												BgL_test2216z00_3720
																																													=
																																													BGL_INT32P
																																													(BgL_tmpz00_3721);
																																											}
																																											if (BgL_test2216z00_3720)
																																												{	/* Cnst/node.scm 94 */
																																													BgL_test2203z00_3664
																																														=
																																														(
																																														(bool_t)
																																														1);
																																												}
																																											else
																																												{	/* Cnst/node.scm 95 */
																																													bool_t
																																														BgL_test2217z00_3725;
																																													{	/* Cnst/node.scm 95 */
																																														obj_t
																																															BgL_tmpz00_3726;
																																														BgL_tmpz00_3726
																																															=
																																															(
																																															((BgL_kwotez00_bglt) COBJECT(((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
																																														BgL_test2217z00_3725
																																															=
																																															BGL_UINT32P
																																															(BgL_tmpz00_3726);
																																													}
																																													if (BgL_test2217z00_3725)
																																														{	/* Cnst/node.scm 95 */
																																															BgL_test2203z00_3664
																																																=
																																																(
																																																(bool_t)
																																																1);
																																														}
																																													else
																																														{	/* Cnst/node.scm 96 */
																																															bool_t
																																																BgL_test2218z00_3730;
																																															{	/* Cnst/node.scm 96 */
																																																obj_t
																																																	BgL_tmpz00_3731;
																																																BgL_tmpz00_3731
																																																	=
																																																	(
																																																	((BgL_kwotez00_bglt) COBJECT(((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
																																																BgL_test2218z00_3730
																																																	=
																																																	BGL_INT64P
																																																	(BgL_tmpz00_3731);
																																															}
																																															if (BgL_test2218z00_3730)
																																																{	/* Cnst/node.scm 96 */
																																																	BgL_test2203z00_3664
																																																		=
																																																		(
																																																		(bool_t)
																																																		1);
																																																}
																																															else
																																																{	/* Cnst/node.scm 97 */
																																																	obj_t
																																																		BgL_tmpz00_3735;
																																																	BgL_tmpz00_3735
																																																		=
																																																		(
																																																		((BgL_kwotez00_bglt) COBJECT(((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
																																																	BgL_test2203z00_3664
																																																		=
																																																		BGL_UINT64P
																																																		(BgL_tmpz00_3735);
																																																}
																																														}
																																												}
																																										}
																																								}
																																						}
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																				if (BgL_test2203z00_3664)
																					{	/* Cnst/node.scm 98 */
																						BgL_literalz00_bglt
																							BgL_new1136z00_2613;
																						{	/* Cnst/node.scm 99 */
																							BgL_literalz00_bglt
																								BgL_new1135z00_2614;
																							BgL_new1135z00_2614 =
																								((BgL_literalz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_literalz00_bgl))));
																							{	/* Cnst/node.scm 99 */
																								long BgL_arg1831z00_2615;

																								BgL_arg1831z00_2615 =
																									BGL_CLASS_NUM
																									(BGl_literalz00zzast_nodez00);
																								BGL_OBJECT_CLASS_NUM_SET((
																										(BgL_objectz00_bglt)
																										BgL_new1135z00_2614),
																									BgL_arg1831z00_2615);
																							}
																							{	/* Cnst/node.scm 99 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_3743;
																								BgL_tmpz00_3743 =
																									((BgL_objectz00_bglt)
																									BgL_new1135z00_2614);
																								BGL_OBJECT_WIDENING_SET
																									(BgL_tmpz00_3743, BFALSE);
																							}
																							((BgL_objectz00_bglt)
																								BgL_new1135z00_2614);
																							BgL_new1136z00_2613 =
																								BgL_new1135z00_2614;
																						}
																						((((BgL_nodez00_bglt) COBJECT(
																										((BgL_nodez00_bglt)
																											BgL_new1136z00_2613)))->
																								BgL_locz00) =
																							((obj_t) (((BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												((BgL_kwotez00_bglt)
																													BgL_nodez00_2431))))->
																									BgL_locz00)), BUNSPEC);
																						{
																							BgL_typez00_bglt BgL_auxz00_3752;

																							{	/* Cnst/node.scm 100 */
																								BgL_typez00_bglt
																									BgL_arg1820z00_2616;
																								BgL_typez00_bglt
																									BgL_arg1822z00_2617;
																								{	/* Cnst/node.scm 100 */
																									obj_t BgL_arg1823z00_2618;

																									BgL_arg1823z00_2618 =
																										(((BgL_kwotez00_bglt)
																											COBJECT((
																													(BgL_kwotez00_bglt)
																													BgL_nodez00_2431)))->
																										BgL_valuez00);
																									BgL_arg1820z00_2616 =
																										BGl_getzd2typezd2atomz00zztype_typeofz00
																										(BgL_arg1823z00_2618);
																								}
																								BgL_arg1822z00_2617 =
																									(((BgL_nodez00_bglt) COBJECT(
																											((BgL_nodez00_bglt)
																												((BgL_kwotez00_bglt)
																													BgL_nodez00_2431))))->
																									BgL_typez00);
																								BgL_auxz00_3752 =
																									BGl_strictzd2nodezd2typez00zzast_nodez00
																									(BgL_arg1820z00_2616,
																									BgL_arg1822z00_2617);
																							}
																							((((BgL_nodez00_bglt) COBJECT(
																											((BgL_nodez00_bglt)
																												BgL_new1136z00_2613)))->
																									BgL_typez00) =
																								((BgL_typez00_bglt)
																									BgL_auxz00_3752), BUNSPEC);
																						}
																						((((BgL_atomz00_bglt) COBJECT(
																										((BgL_atomz00_bglt)
																											BgL_new1136z00_2613)))->
																								BgL_valuez00) =
																							((obj_t) (((BgL_kwotez00_bglt)
																										COBJECT(((BgL_kwotez00_bglt)
																												BgL_nodez00_2431)))->
																									BgL_valuez00)), BUNSPEC);
																						return ((BgL_nodez00_bglt)
																							BgL_new1136z00_2613);
																					}
																				else
																					{	/* Cnst/node.scm 102 */
																						bool_t BgL_test2219z00_3767;

																						{	/* Cnst/node.scm 102 */
																							obj_t BgL_tmpz00_3768;

																							BgL_tmpz00_3768 =
																								(((BgL_kwotez00_bglt) COBJECT(
																										((BgL_kwotez00_bglt)
																											BgL_nodez00_2431)))->
																								BgL_valuez00);
																							BgL_test2219z00_3767 =
																								BIGNUMP(BgL_tmpz00_3768);
																						}
																						if (BgL_test2219z00_3767)
																							{	/* Cnst/node.scm 103 */
																								obj_t BgL__ortest_1137z00_2619;

																								{	/* Cnst/node.scm 103 */
																									obj_t BgL_arg1838z00_2620;
																									obj_t BgL_arg1839z00_2621;

																									BgL_arg1838z00_2620 =
																										(((BgL_kwotez00_bglt)
																											COBJECT((
																													(BgL_kwotez00_bglt)
																													BgL_nodez00_2431)))->
																										BgL_valuez00);
																									BgL_arg1839z00_2621 =
																										(((BgL_nodez00_bglt)
																											COBJECT((
																													(BgL_nodez00_bglt) (
																														(BgL_kwotez00_bglt)
																														BgL_nodez00_2431))))->
																										BgL_locz00);
																									BgL__ortest_1137z00_2619 =
																										BGl_cnstzd2alloczd2bignumz00zzcnst_allocz00
																										(BgL_arg1838z00_2620,
																										BgL_arg1839z00_2621);
																								}
																								if (CBOOL
																									(BgL__ortest_1137z00_2619))
																									{	/* Cnst/node.scm 103 */
																										return
																											((BgL_nodez00_bglt)
																											BgL__ortest_1137z00_2619);
																									}
																								else
																									{	/* Cnst/node.scm 104 */
																										BgL_literalz00_bglt
																											BgL_new1139z00_2622;
																										{	/* Cnst/node.scm 105 */
																											BgL_literalz00_bglt
																												BgL_new1138z00_2623;
																											BgL_new1138z00_2623 =
																												((BgL_literalz00_bglt)
																												BOBJECT(GC_MALLOC(sizeof
																														(struct
																															BgL_literalz00_bgl))));
																											{	/* Cnst/node.scm 105 */
																												long
																													BgL_arg1837z00_2624;
																												BgL_arg1837z00_2624 =
																													BGL_CLASS_NUM
																													(BGl_literalz00zzast_nodez00);
																												BGL_OBJECT_CLASS_NUM_SET
																													(((BgL_objectz00_bglt)
																														BgL_new1138z00_2623),
																													BgL_arg1837z00_2624);
																											}
																											{	/* Cnst/node.scm 105 */
																												BgL_objectz00_bglt
																													BgL_tmpz00_3785;
																												BgL_tmpz00_3785 =
																													((BgL_objectz00_bglt)
																													BgL_new1138z00_2623);
																												BGL_OBJECT_WIDENING_SET
																													(BgL_tmpz00_3785,
																													BFALSE);
																											}
																											((BgL_objectz00_bglt)
																												BgL_new1138z00_2623);
																											BgL_new1139z00_2622 =
																												BgL_new1138z00_2623;
																										}
																										((((BgL_nodez00_bglt)
																													COBJECT((
																															(BgL_nodez00_bglt)
																															BgL_new1139z00_2622)))->
																												BgL_locz00) =
																											((obj_t) ((
																														(BgL_nodez00_bglt)
																														COBJECT((
																																(BgL_nodez00_bglt)
																																((BgL_kwotez00_bglt) BgL_nodez00_2431))))->BgL_locz00)), BUNSPEC);
																										{
																											BgL_typez00_bglt
																												BgL_auxz00_3794;
																											{	/* Cnst/node.scm 106 */
																												BgL_typez00_bglt
																													BgL_arg1834z00_2625;
																												BgL_typez00_bglt
																													BgL_arg1835z00_2626;
																												{	/* Cnst/node.scm 106 */
																													obj_t
																														BgL_arg1836z00_2627;
																													BgL_arg1836z00_2627 =
																														(((BgL_kwotez00_bglt) COBJECT(((BgL_kwotez00_bglt) BgL_nodez00_2431)))->BgL_valuez00);
																													BgL_arg1834z00_2625 =
																														BGl_getzd2typezd2atomz00zztype_typeofz00
																														(BgL_arg1836z00_2627);
																												}
																												BgL_arg1835z00_2626 =
																													(((BgL_nodez00_bglt)
																														COBJECT((
																																(BgL_nodez00_bglt)
																																((BgL_kwotez00_bglt) BgL_nodez00_2431))))->BgL_typez00);
																												BgL_auxz00_3794 =
																													BGl_strictzd2nodezd2typez00zzast_nodez00
																													(BgL_arg1834z00_2625,
																													BgL_arg1835z00_2626);
																											}
																											((((BgL_nodez00_bglt)
																														COBJECT((
																																(BgL_nodez00_bglt)
																																BgL_new1139z00_2622)))->
																													BgL_typez00) =
																												((BgL_typez00_bglt)
																													BgL_auxz00_3794),
																												BUNSPEC);
																										}
																										((((BgL_atomz00_bglt)
																													COBJECT((
																															(BgL_atomz00_bglt)
																															BgL_new1139z00_2622)))->
																												BgL_valuez00) =
																											((obj_t) ((
																														(BgL_kwotez00_bglt)
																														COBJECT((
																																(BgL_kwotez00_bglt)
																																BgL_nodez00_2431)))->
																													BgL_valuez00)),
																											BUNSPEC);
																										return ((BgL_nodez00_bglt)
																											BgL_new1139z00_2622);
																									}
																							}
																						else
																							{	/* Cnst/node.scm 108 */
																								bool_t BgL_test2221z00_3809;

																								{	/* Cnst/node.scm 108 */
																									obj_t BgL_tmpz00_3810;

																									BgL_tmpz00_3810 =
																										(((BgL_kwotez00_bglt)
																											COBJECT((
																													(BgL_kwotez00_bglt)
																													BgL_nodez00_2431)))->
																										BgL_valuez00);
																									BgL_test2221z00_3809 =
																										STRUCTP(BgL_tmpz00_3810);
																								}
																								if (BgL_test2221z00_3809)
																									{	/* Cnst/node.scm 109 */
																										obj_t BgL_arg1842z00_2628;
																										obj_t BgL_arg1843z00_2629;

																										BgL_arg1842z00_2628 =
																											(((BgL_kwotez00_bglt)
																												COBJECT((
																														(BgL_kwotez00_bglt)
																														BgL_nodez00_2431)))->
																											BgL_valuez00);
																										BgL_arg1843z00_2629 =
																											(((BgL_nodez00_bglt)
																												COBJECT((
																														(BgL_nodez00_bglt) (
																															(BgL_kwotez00_bglt)
																															BgL_nodez00_2431))))->
																											BgL_locz00);
																										return
																											BGl_cnstzd2alloczd2structz00zzcnst_allocz00
																											(BgL_arg1842z00_2628,
																											BgL_arg1843z00_2629);
																									}
																								else
																									{	/* Cnst/node.scm 111 */
																										obj_t BgL_arg1844z00_2630;

																										BgL_arg1844z00_2630 =
																											BGl_shapez00zztools_shapez00
																											(((obj_t) (
																													(BgL_kwotez00_bglt)
																													BgL_nodez00_2431)));
																										return ((BgL_nodez00_bglt)
																											BGl_internalzd2errorzd2zztools_errorz00
																											(BGl_string2098z00zzcnst_nodez00,
																												BGl_string2099z00zzcnst_nodez00,
																												BgL_arg1844z00_2630));
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* &cnst!-patch1304 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2patch1304za2zzcnst_nodez00(obj_t
		BgL_envz00_2432, obj_t BgL_nodez00_2433)
	{
		{	/* Cnst/node.scm 56 */
			{	/* Cnst/node.scm 58 */
				obj_t BgL_arg1615z00_2632;

				BgL_arg1615z00_2632 =
					(((BgL_atomz00_bglt) COBJECT(
							((BgL_atomz00_bglt)
								((BgL_patchz00_bglt) BgL_nodez00_2433))))->BgL_valuez00);
				BGl_cnstz12z12zzcnst_nodez00(((BgL_nodez00_bglt) BgL_arg1615z00_2632));
			}
			return ((BgL_nodez00_bglt) ((BgL_patchz00_bglt) BgL_nodez00_2433));
		}

	}



/* &cnst!-literal1302 */
	BgL_nodez00_bglt BGl_z62cnstz12zd2literal1302za2zzcnst_nodez00(obj_t
		BgL_envz00_2434, obj_t BgL_nodez00_2435)
	{
		{	/* Cnst/node.scm 44 */
			{	/* Cnst/node.scm 47 */
				bool_t BgL_test2222z00_3832;

				{	/* Cnst/node.scm 47 */
					obj_t BgL_tmpz00_3833;

					BgL_tmpz00_3833 =
						(((BgL_atomz00_bglt) COBJECT(
								((BgL_atomz00_bglt)
									((BgL_literalz00_bglt) BgL_nodez00_2435))))->BgL_valuez00);
					BgL_test2222z00_3832 = KEYWORDP(BgL_tmpz00_3833);
				}
				if (BgL_test2222z00_3832)
					{	/* Cnst/node.scm 47 */
						obj_t BgL_arg1571z00_2634;
						obj_t BgL_arg1573z00_2635;

						BgL_arg1571z00_2634 =
							(((BgL_atomz00_bglt) COBJECT(
									((BgL_atomz00_bglt)
										((BgL_literalz00_bglt) BgL_nodez00_2435))))->BgL_valuez00);
						BgL_arg1573z00_2635 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_literalz00_bglt) BgL_nodez00_2435))))->BgL_locz00);
						return
							BGl_cnstzd2alloczd2keywordz00zzcnst_allocz00(BgL_arg1571z00_2634,
							BgL_arg1573z00_2635);
					}
				else
					{	/* Cnst/node.scm 48 */
						bool_t BgL_test2223z00_3845;

						{	/* Cnst/node.scm 48 */
							obj_t BgL_tmpz00_3846;

							BgL_tmpz00_3846 =
								(((BgL_atomz00_bglt) COBJECT(
										((BgL_atomz00_bglt)
											((BgL_literalz00_bglt) BgL_nodez00_2435))))->
								BgL_valuez00);
							BgL_test2223z00_3845 = UCS2_STRINGP(BgL_tmpz00_3846);
						}
						if (BgL_test2223z00_3845)
							{	/* Cnst/node.scm 48 */
								obj_t BgL_arg1584z00_2636;
								obj_t BgL_arg1585z00_2637;

								BgL_arg1584z00_2636 =
									(((BgL_atomz00_bglt) COBJECT(
											((BgL_atomz00_bglt)
												((BgL_literalz00_bglt) BgL_nodez00_2435))))->
									BgL_valuez00);
								BgL_arg1585z00_2637 =
									(((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) ((BgL_literalz00_bglt)
													BgL_nodez00_2435))))->BgL_locz00);
								return
									BGl_cnstzd2alloczd2ucs2zd2stringzd2zzcnst_allocz00
									(BgL_arg1584z00_2636, BgL_arg1585z00_2637);
							}
						else
							{	/* Cnst/node.scm 49 */
								bool_t BgL_test2224z00_3858;

								{	/* Cnst/node.scm 49 */
									obj_t BgL_tmpz00_3859;

									BgL_tmpz00_3859 =
										(((BgL_atomz00_bglt) COBJECT(
												((BgL_atomz00_bglt)
													((BgL_literalz00_bglt) BgL_nodez00_2435))))->
										BgL_valuez00);
									BgL_test2224z00_3858 = STRINGP(BgL_tmpz00_3859);
								}
								if (BgL_test2224z00_3858)
									{	/* Cnst/node.scm 49 */
										obj_t BgL_arg1593z00_2638;
										obj_t BgL_arg1594z00_2639;

										BgL_arg1593z00_2638 =
											(((BgL_atomz00_bglt) COBJECT(
													((BgL_atomz00_bglt)
														((BgL_literalz00_bglt) BgL_nodez00_2435))))->
											BgL_valuez00);
										BgL_arg1594z00_2639 =
											(((BgL_nodez00_bglt)
												COBJECT(((BgL_nodez00_bglt) ((BgL_literalz00_bglt)
															BgL_nodez00_2435))))->BgL_locz00);
										return
											BGl_cnstzd2alloczd2stringz00zzcnst_allocz00
											(BgL_arg1593z00_2638, BgL_arg1594z00_2639);
									}
								else
									{	/* Cnst/node.scm 50 */
										bool_t BgL_test2225z00_3871;

										{	/* Cnst/node.scm 50 */
											obj_t BgL_tmpz00_3872;

											BgL_tmpz00_3872 =
												(((BgL_atomz00_bglt) COBJECT(
														((BgL_atomz00_bglt)
															((BgL_literalz00_bglt) BgL_nodez00_2435))))->
												BgL_valuez00);
											BgL_test2225z00_3871 = BIGNUMP(BgL_tmpz00_3872);
										}
										if (BgL_test2225z00_3871)
											{	/* Cnst/node.scm 50 */
												obj_t BgL__ortest_1132z00_2640;

												{	/* Cnst/node.scm 50 */
													obj_t BgL_arg1602z00_2641;
													obj_t BgL_arg1605z00_2642;

													BgL_arg1602z00_2641 =
														(((BgL_atomz00_bglt) COBJECT(
																((BgL_atomz00_bglt)
																	((BgL_literalz00_bglt) BgL_nodez00_2435))))->
														BgL_valuez00);
													BgL_arg1605z00_2642 =
														(((BgL_nodez00_bglt)
															COBJECT(((BgL_nodez00_bglt) ((BgL_literalz00_bglt)
																		BgL_nodez00_2435))))->BgL_locz00);
													BgL__ortest_1132z00_2640 =
														BGl_cnstzd2alloczd2bignumz00zzcnst_allocz00
														(BgL_arg1602z00_2641, BgL_arg1605z00_2642);
												}
												if (CBOOL(BgL__ortest_1132z00_2640))
													{	/* Cnst/node.scm 50 */
														return
															((BgL_nodez00_bglt) BgL__ortest_1132z00_2640);
													}
												else
													{	/* Cnst/node.scm 50 */
														return
															((BgL_nodez00_bglt)
															((BgL_literalz00_bglt) BgL_nodez00_2435));
													}
											}
										else
											{	/* Cnst/node.scm 50 */
												return
													((BgL_nodez00_bglt)
													((BgL_literalz00_bglt) BgL_nodez00_2435));
											}
									}
							}
					}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcnst_nodez00(void)
	{
		{	/* Cnst/node.scm 16 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2100z00zzcnst_nodez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2100z00zzcnst_nodez00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2100z00zzcnst_nodez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2100z00zzcnst_nodez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2100z00zzcnst_nodez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2100z00zzcnst_nodez00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2100z00zzcnst_nodez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2100z00zzcnst_nodez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2100z00zzcnst_nodez00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2100z00zzcnst_nodez00));
			BGl_modulezd2initializa7ationz75zzcnst_cachez00(156818605L,
				BSTRING_TO_STRING(BGl_string2100z00zzcnst_nodez00));
			return
				BGl_modulezd2initializa7ationz75zzcnst_allocz00(192699986L,
				BSTRING_TO_STRING(BGl_string2100z00zzcnst_nodez00));
		}

	}

#ifdef __cplusplus
}
#endif
