/*===========================================================================*/
/*   (Cnst/cache.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cnst/cache.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CNST_CACHE_TYPE_DEFINITIONS
#define BGL_CNST_CACHE_TYPE_DEFINITIONS
#endif													// BGL_CNST_CACHE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DEF obj_t BGl_za2stringzd2ze3bignumza2z31zzcnst_cachez00 =
		BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zzcnst_cachez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_startzd2cnstzd2cachez12z12zzcnst_cachez00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2cnstzd2tablezd2refza2z00zzcnst_cachez00 =
		BUNSPEC;
	extern obj_t BGl_getzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzcnst_cachez00(void);
	static obj_t BGl_genericzd2initzd2zzcnst_cachez00(void);
	static obj_t BGl_objectzd2initzd2zzcnst_cachez00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2llongzd2ze3bllongza2z31zzcnst_cachez00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2makezd2lzd2procedureza2z00zzcnst_cachez00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bfalseza2z00zzcnst_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2elongzd2ze3belongza2z31zzcnst_cachez00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2stringzd2ze3bstringza2z31zzcnst_cachez00 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2cnstzd2tablezd2setz12za2z12zzcnst_cachez00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bstringzd2ze3keywordza2z31zzcnst_cachez00 =
		BUNSPEC;
	static obj_t BGl_methodzd2initzd2zzcnst_cachez00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2listzd2ze3vectorza2z31zzcnst_cachez00 = BUNSPEC;
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stopzd2cnstzd2cachez12z12zzcnst_cachez00(void);
	static obj_t BGl_z62startzd2cnstzd2cachez12z70zzcnst_cachez00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2bstringzd2ze3symbolza2z31zzcnst_cachez00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2listzd2ze3structza2z31zzcnst_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2boolzd2ze3bboolza2z31zzcnst_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2consza2z00zzcnst_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2makezd2fxzd2procedureza2z00zzcnst_cachez00 =
		BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcnst_cachez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_EXPORTED_DEF obj_t BGl_za2int64zd2ze3bint64za2z31zzcnst_cachez00 =
		BUNSPEC;
	static obj_t BGl_z62stopzd2cnstzd2cachez12z70zzcnst_cachez00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2uint64zd2ze3buint64za2z31zzcnst_cachez00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2makezd2vazd2procedureza2z00zzcnst_cachez00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2longzd2ze3intza2z31zzcnst_cachez00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zzcnst_cachez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcnst_cachez00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2uint32zd2ze3buint32za2z31zzcnst_cachez00 =
		BUNSPEC;
	static obj_t BGl_importedzd2moduleszd2initz00zzcnst_cachez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzcnst_cachez00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2fixnumzd2ze3bignumza2z31zzcnst_cachez00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2vectorzd2tagzd2setz12za2z12zzcnst_cachez00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2btrueza2z00zzcnst_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2stringzd2ze3ucs2stringza2z31zzcnst_cachez00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2int32zd2ze3bint32za2z31zzcnst_cachez00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2doublezd2ze3realza2z31zzcnst_cachez00 = BUNSPEC;
	static bool_t BGl_za2cachezd2startedzf3za2z21zzcnst_cachez00;
	static obj_t __cnst[29];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stopzd2cnstzd2cachez12zd2envzc0zzcnst_cachez00,
		BgL_bgl_za762stopza7d2cnstza7d1138za7,
		BGl_z62stopzd2cnstzd2cachez12z70zzcnst_cachez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_startzd2cnstzd2cachez12zd2envzc0zzcnst_cachez00,
		BgL_bgl_za762startza7d2cnstza71139za7,
		BGl_z62startzd2cnstzd2cachez12z70zzcnst_cachez00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1133z00zzcnst_cachez00,
		BgL_bgl_string1133za700za7za7c1140za7, "cnst_cache", 10);
	      DEFINE_STRING(BGl_string1134z00zzcnst_cachez00,
		BgL_bgl_string1134za700za7za7c1141za7,
		"$fixnum->bignum __bignum $string->bignum list->struct vector-tag-set! list->vector $uint64->buint64 $int64->bint64 $uint32->buint32 $int32->bint32 $llong->bllong $elong->belong $double->real make-l-procedure make-va-procedure make-fx-procedure $long->int $bool->bbool string->keyword __r4_symbols_6_4 string->symbol c-utf8-string->ucs2-string $string->bstring bfalse btrue $cons cnst-table-set! foreign cnst-table-ref ",
		418);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2stringzd2ze3bignumza2z31zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2cnstzd2tablezd2refza2z00zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2llongzd2ze3bllongza2z31zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2makezd2lzd2procedureza2z00zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2bfalseza2z00zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2elongzd2ze3belongza2z31zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2stringzd2ze3bstringza2z31zzcnst_cachez00));
		   
			 ADD_ROOT((void *) (&BGl_za2cnstzd2tablezd2setz12za2z12zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2bstringzd2ze3keywordza2z31zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2listzd2ze3vectorza2z31zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2bstringzd2ze3symbolza2z31zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2listzd2ze3structza2z31zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2boolzd2ze3bboolza2z31zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2consza2z00zzcnst_cachez00));
		   
			 ADD_ROOT((void *) (&BGl_za2makezd2fxzd2procedureza2z00zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2int64zd2ze3bint64za2z31zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2uint64zd2ze3buint64za2z31zzcnst_cachez00));
		   
			 ADD_ROOT((void *) (&BGl_za2makezd2vazd2procedureza2z00zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2longzd2ze3intza2z31zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2uint32zd2ze3buint32za2z31zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2fixnumzd2ze3bignumza2z31zzcnst_cachez00));
		   
			 ADD_ROOT((void *) (&BGl_za2vectorzd2tagzd2setz12za2z12zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2btrueza2z00zzcnst_cachez00));
		   
			 ADD_ROOT((void *) (&BGl_za2stringzd2ze3ucs2stringza2z31zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2int32zd2ze3bint32za2z31zzcnst_cachez00));
		     ADD_ROOT((void *) (&BGl_za2doublezd2ze3realza2z31zzcnst_cachez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcnst_cachez00(long
		BgL_checksumz00_501, char *BgL_fromz00_502)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcnst_cachez00))
				{
					BGl_requirezd2initializa7ationz75zzcnst_cachez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcnst_cachez00();
					BGl_libraryzd2moduleszd2initz00zzcnst_cachez00();
					BGl_cnstzd2initzd2zzcnst_cachez00();
					BGl_importedzd2moduleszd2initz00zzcnst_cachez00();
					return BGl_toplevelzd2initzd2zzcnst_cachez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcnst_cachez00(void)
	{
		{	/* Cnst/cache.scm 15 */
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cnst_cache");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cnst_cache");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cnst_cache");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcnst_cachez00(void)
	{
		{	/* Cnst/cache.scm 15 */
			{	/* Cnst/cache.scm 15 */
				obj_t BgL_cportz00_490;

				{	/* Cnst/cache.scm 15 */
					obj_t BgL_stringz00_497;

					BgL_stringz00_497 = BGl_string1134z00zzcnst_cachez00;
					{	/* Cnst/cache.scm 15 */
						obj_t BgL_startz00_498;

						BgL_startz00_498 = BINT(0L);
						{	/* Cnst/cache.scm 15 */
							obj_t BgL_endz00_499;

							BgL_endz00_499 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_497)));
							{	/* Cnst/cache.scm 15 */

								BgL_cportz00_490 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_497, BgL_startz00_498, BgL_endz00_499);
				}}}}
				{
					long BgL_iz00_491;

					BgL_iz00_491 = 28L;
				BgL_loopz00_492:
					if ((BgL_iz00_491 == -1L))
						{	/* Cnst/cache.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cnst/cache.scm 15 */
							{	/* Cnst/cache.scm 15 */
								obj_t BgL_arg1137z00_493;

								{	/* Cnst/cache.scm 15 */

									{	/* Cnst/cache.scm 15 */
										obj_t BgL_locationz00_495;

										BgL_locationz00_495 = BBOOL(((bool_t) 0));
										{	/* Cnst/cache.scm 15 */

											BgL_arg1137z00_493 =
												BGl_readz00zz__readerz00(BgL_cportz00_490,
												BgL_locationz00_495);
										}
									}
								}
								{	/* Cnst/cache.scm 15 */
									int BgL_tmpz00_523;

									BgL_tmpz00_523 = (int) (BgL_iz00_491);
									CNST_TABLE_SET(BgL_tmpz00_523, BgL_arg1137z00_493);
							}}
							{	/* Cnst/cache.scm 15 */
								int BgL_auxz00_496;

								BgL_auxz00_496 = (int) ((BgL_iz00_491 - 1L));
								{
									long BgL_iz00_528;

									BgL_iz00_528 = (long) (BgL_auxz00_496);
									BgL_iz00_491 = BgL_iz00_528;
									goto BgL_loopz00_492;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcnst_cachez00(void)
	{
		{	/* Cnst/cache.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcnst_cachez00(void)
	{
		{	/* Cnst/cache.scm 15 */
			BGl_za2cachezd2startedzf3za2z21zzcnst_cachez00 = ((bool_t) 0);
			BGl_za2cnstzd2tablezd2refza2z00zzcnst_cachez00 = BFALSE;
			BGl_za2cnstzd2tablezd2setz12za2z12zzcnst_cachez00 = BFALSE;
			BGl_za2consza2z00zzcnst_cachez00 = BFALSE;
			BGl_za2btrueza2z00zzcnst_cachez00 = BFALSE;
			BGl_za2bfalseza2z00zzcnst_cachez00 = BFALSE;
			BGl_za2stringzd2ze3bstringza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2stringzd2ze3ucs2stringza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2bstringzd2ze3symbolza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2bstringzd2ze3keywordza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2boolzd2ze3bboolza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2longzd2ze3intza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2makezd2fxzd2procedureza2z00zzcnst_cachez00 = BFALSE;
			BGl_za2makezd2vazd2procedureza2z00zzcnst_cachez00 = BFALSE;
			BGl_za2makezd2lzd2procedureza2z00zzcnst_cachez00 = BFALSE;
			BGl_za2doublezd2ze3realza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2elongzd2ze3belongza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2llongzd2ze3bllongza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2int32zd2ze3bint32za2z31zzcnst_cachez00 = BFALSE;
			BGl_za2uint32zd2ze3buint32za2z31zzcnst_cachez00 = BFALSE;
			BGl_za2int64zd2ze3bint64za2z31zzcnst_cachez00 = BFALSE;
			BGl_za2uint64zd2ze3buint64za2z31zzcnst_cachez00 = BFALSE;
			BGl_za2listzd2ze3vectorza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2vectorzd2tagzd2setz12za2z12zzcnst_cachez00 = BFALSE;
			BGl_za2listzd2ze3structza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2stringzd2ze3bignumza2z31zzcnst_cachez00 = BFALSE;
			return (BGl_za2fixnumzd2ze3bignumza2z31zzcnst_cachez00 = BFALSE, BUNSPEC);
		}

	}



/* start-cnst-cache! */
	BGL_EXPORTED_DEF obj_t BGl_startzd2cnstzd2cachez12z12zzcnst_cachez00(void)
	{
		{	/* Cnst/cache.scm 84 */
			if (BGl_za2cachezd2startedzf3za2z21zzcnst_cachez00)
				{	/* Cnst/cache.scm 85 */
					return BTRUE;
				}
			else
				{	/* Cnst/cache.scm 85 */
					BGl_za2cachezd2startedzf3za2z21zzcnst_cachez00 = ((bool_t) 1);
					BGl_za2cnstzd2tablezd2refza2z00zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(0),
						CNST_TABLE_REF(1));
					BGl_za2cnstzd2tablezd2setz12za2z12zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(2),
						CNST_TABLE_REF(1));
					BGl_za2consza2z00zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(3),
						CNST_TABLE_REF(1));
					BGl_za2btrueza2z00zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(4),
						CNST_TABLE_REF(1));
					BGl_za2bfalseza2z00zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(5),
						CNST_TABLE_REF(1));
					BGl_za2stringzd2ze3bstringza2z31zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(6),
						CNST_TABLE_REF(1));
					BGl_za2stringzd2ze3ucs2stringza2z31zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(7),
						CNST_TABLE_REF(1));
					BGl_za2bstringzd2ze3symbolza2z31zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(8),
						CNST_TABLE_REF(9));
					BGl_za2bstringzd2ze3keywordza2z31zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(10),
						CNST_TABLE_REF(9));
					BGl_za2boolzd2ze3bboolza2z31zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(11),
						CNST_TABLE_REF(1));
					BGl_za2longzd2ze3intza2z31zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(12),
						CNST_TABLE_REF(1));
					BGl_za2makezd2fxzd2procedureza2z00zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(13),
						CNST_TABLE_REF(1));
					BGl_za2makezd2vazd2procedureza2z00zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(14),
						CNST_TABLE_REF(1));
					BGl_za2makezd2lzd2procedureza2z00zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(15),
						CNST_TABLE_REF(1));
					BGl_za2doublezd2ze3realza2z31zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(16),
						CNST_TABLE_REF(1));
					BGl_za2elongzd2ze3belongza2z31zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(17),
						CNST_TABLE_REF(1));
					BGl_za2llongzd2ze3bllongza2z31zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(18),
						CNST_TABLE_REF(1));
					BGl_za2int32zd2ze3bint32za2z31zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(19),
						CNST_TABLE_REF(1));
					BGl_za2uint32zd2ze3buint32za2z31zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(20),
						CNST_TABLE_REF(1));
					BGl_za2int64zd2ze3bint64za2z31zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(21),
						CNST_TABLE_REF(1));
					BGl_za2uint64zd2ze3buint64za2z31zzcnst_cachez00 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(22),
						CNST_TABLE_REF(1));
					BGl_za2listzd2ze3vectorza2z31zzcnst_cachez00 =
						BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(23), BNIL);
					BGl_za2vectorzd2tagzd2setz12za2z12zzcnst_cachez00 =
						BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(24), BNIL);
					BGl_za2listzd2ze3structza2z31zzcnst_cachez00 =
						BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(25), BNIL);
					{	/* Cnst/cache.scm 137 */
						obj_t BgL__ortest_1060z00_445;

						{	/* Cnst/cache.scm 137 */
							obj_t BgL_list1096z00_446;

							BgL_list1096z00_446 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(1), BNIL);
							BgL__ortest_1060z00_445 =
								BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(26),
								BgL_list1096z00_446);
						}
						if (CBOOL(BgL__ortest_1060z00_445))
							{	/* Cnst/cache.scm 137 */
								BGl_za2stringzd2ze3bignumza2z31zzcnst_cachez00 =
									BgL__ortest_1060z00_445;
							}
						else
							{	/* Cnst/cache.scm 137 */
								BGl_za2stringzd2ze3bignumza2z31zzcnst_cachez00 =
									BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(26),
									CNST_TABLE_REF(27));
							}
					}
					{	/* Cnst/cache.scm 140 */
						obj_t BgL__ortest_1061z00_447;

						{	/* Cnst/cache.scm 140 */
							obj_t BgL_list1097z00_448;

							BgL_list1097z00_448 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(1), BNIL);
							BgL__ortest_1061z00_447 =
								BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(28),
								BgL_list1097z00_448);
						}
						if (CBOOL(BgL__ortest_1061z00_447))
							{	/* Cnst/cache.scm 140 */
								BGl_za2fixnumzd2ze3bignumza2z31zzcnst_cachez00 =
									BgL__ortest_1061z00_447;
							}
						else
							{	/* Cnst/cache.scm 140 */
								BGl_za2fixnumzd2ze3bignumza2z31zzcnst_cachez00 =
									BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(28),
									CNST_TABLE_REF(27));
							}
					}
					return BTRUE;
				}
		}

	}



/* &start-cnst-cache! */
	obj_t BGl_z62startzd2cnstzd2cachez12z70zzcnst_cachez00(obj_t BgL_envz00_488)
	{
		{	/* Cnst/cache.scm 84 */
			return BGl_startzd2cnstzd2cachez12z12zzcnst_cachez00();
		}

	}



/* stop-cnst-cache! */
	BGL_EXPORTED_DEF obj_t BGl_stopzd2cnstzd2cachez12z12zzcnst_cachez00(void)
	{
		{	/* Cnst/cache.scm 148 */
			BGl_za2fixnumzd2ze3bignumza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2stringzd2ze3bignumza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2stringzd2ze3bstringza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2stringzd2ze3ucs2stringza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2bstringzd2ze3symbolza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2bstringzd2ze3keywordza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2boolzd2ze3bboolza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2longzd2ze3intza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2makezd2fxzd2procedureza2z00zzcnst_cachez00 = BFALSE;
			BGl_za2makezd2vazd2procedureza2z00zzcnst_cachez00 = BFALSE;
			BGl_za2doublezd2ze3realza2z31zzcnst_cachez00 = BFALSE;
			BGl_za2consza2z00zzcnst_cachez00 = BFALSE;
			BGl_za2btrueza2z00zzcnst_cachez00 = BFALSE;
			BGl_za2bfalseza2z00zzcnst_cachez00 = BFALSE;
			return BTRUE;
		}

	}



/* &stop-cnst-cache! */
	obj_t BGl_z62stopzd2cnstzd2cachez12z70zzcnst_cachez00(obj_t BgL_envz00_489)
	{
		{	/* Cnst/cache.scm 148 */
			return BGl_stopzd2cnstzd2cachez12z12zzcnst_cachez00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcnst_cachez00(void)
	{
		{	/* Cnst/cache.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcnst_cachez00(void)
	{
		{	/* Cnst/cache.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcnst_cachez00(void)
	{
		{	/* Cnst/cache.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcnst_cachez00(void)
	{
		{	/* Cnst/cache.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1133z00zzcnst_cachez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1133z00zzcnst_cachez00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1133z00zzcnst_cachez00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1133z00zzcnst_cachez00));
		}

	}

#ifdef __cplusplus
}
#endif
