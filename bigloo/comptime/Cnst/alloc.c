/*===========================================================================*/
/*   (Cnst/alloc.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cnst/alloc.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CNST_ALLOC_TYPE_DEFINITIONS
#define BGL_CNST_ALLOC_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                *BgL_pragmaz00_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;


#endif													// BGL_CNST_ALLOC_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(obj_t);
	static BgL_nodez00_bglt
		BGl_z62cnstzd2alloczd2ucs2zd2stringzb0zzcnst_allocz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2symbolza2z00zztype_cachez00;
	extern BgL_globalz00_bglt
		BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_za2ucs2stringzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
	extern obj_t BGl_za2intza2z00zztype_cachez00;
	static long BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00 = 0L;
	static obj_t BGl_requirezd2initializa7ationz75zzcnst_allocz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	static obj_t BGl_za2stringzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2keywordz00zzcnst_allocz00(obj_t, obj_t);
	extern obj_t BGl_za2vectorza2z00zztype_cachez00;
	extern obj_t BGl_za2cnstzd2tablezd2refza2z00zzcnst_cachez00;
	extern obj_t BGl_za2bignumza2z00zztype_cachez00;
	BGL_IMPORT bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2llongz00zzcnst_allocz00(BGL_LONGLONG_T, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_za2llongzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
	static BgL_nodez00_bglt BGl_z62cnstzd2alloczd2uint32z62zzcnst_allocz00(obj_t,
		obj_t, obj_t);
	extern bool_t BGl_tvectorzd2Czd2staticzf3zf3zztvector_cnstz00(obj_t);
	BGL_IMPORT obj_t BGl_assocz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_za2elongzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzcnst_allocz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_za2structza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static BgL_nodez00_bglt
		BGl_z62cnstzd2alloczd2homogenouszd2vectorzb0zzcnst_allocz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t ucs2_strcmp(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2symbolz00zzcnst_allocz00(obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzcnst_allocz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern BgL_typez00_bglt BGl_getzd2defaultzd2typez00zztype_cachez00(void);
	BGL_EXPORTED_DECL obj_t BGl_cnstzd2tablezd2idz00zzcnst_allocz00(void);
	static BgL_nodez00_bglt BGl_z62cnstzd2alloczd2uint64z62zzcnst_allocz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_withzd2libraryzd2modulez12z12zzmodule_libraryz00(obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2elongz00zzcnst_allocz00(long, obj_t);
	static obj_t BGl_objectzd2initzd2zzcnst_allocz00(void);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2listz00zzcnst_allocz00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62cnstzd2alloczd2listz62zzcnst_allocz00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62cnstzd2alloczd2structz62zzcnst_allocz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_za2bignumzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2tvectorz00zzcnst_allocz00(obj_t, obj_t);
	static obj_t BGl_z62cnstzd2alloczd2bignumz62zzcnst_allocz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2int32z00zzcnst_allocz00(int32_t, obj_t);
	extern obj_t BGl_za2initzd2modeza2zd2zzengine_paramz00;
	static BgL_nodez00_bglt BGl_z62cnstzd2alloczd2llongz62zzcnst_allocz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_za2int64zd2envza2zd2zzcnst_allocz00 = BUNSPEC;
	extern obj_t BGl_za2bstringza2z00zztype_cachez00;
	static obj_t BGl_za2keywordzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2vectorz00zzcnst_allocz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_za2vectorzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
	extern obj_t BGl_za2bstringzd2ze3keywordza2z31zzcnst_cachez00;
	static obj_t BGl_methodzd2initzd2zzcnst_allocz00(void);
	BGL_EXPORTED_DECL obj_t BGl_startzd2cnstzd2allocz12z12zzcnst_allocz00(void);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2int64z00zzcnst_allocz00(int64_t, obj_t);
	extern obj_t BGl_za2listzd2ze3vectorza2z31zzcnst_cachez00;
	static BgL_nodez00_bglt BGl_z62cnstzd2alloczd2elongz62zzcnst_allocz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62getzd2cnstzd2sexpz62zzcnst_allocz00(obj_t);
	static obj_t BGl_za2int32zd2envza2zd2zzcnst_allocz00 = BUNSPEC;
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static obj_t BGl_za2cnstzd2tableza2zd2zzcnst_allocz00 = BUNSPEC;
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	static obj_t BGl_za2globalzd2sexpza2zd2zzcnst_allocz00 = BUNSPEC;
	static obj_t BGl_za2uint64zd2envza2zd2zzcnst_allocz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2stringz00zzcnst_allocz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2cnstzd2setz00zzcnst_allocz00(void);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_za2bstringzd2ze3symbolza2z31zzcnst_cachez00;
	static BgL_nodez00_bglt BGl_z62cnstzd2alloczd2int32z62zzcnst_allocz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2listzd2ze3structza2z31zzcnst_cachez00;
	extern obj_t BGl_za2pairza2z00zztype_cachez00;
	extern obj_t BGl_za2compilerzd2debugza2zd2zzengine_paramz00;
	extern BgL_typez00_bglt BGl_getzd2typezd2atomz00zztype_typeofz00(obj_t);
	extern obj_t BGl_pragmaz00zzast_nodez00;
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern BgL_globalz00_bglt
		BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2consza2z00zzcnst_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2lzd2procedurezd2zzcnst_allocz00(BgL_nodez00_bglt, obj_t);
	static obj_t BGl_za2oldzd2debugza2zd2zzcnst_allocz00 = BUNSPEC;
	static obj_t BGl_za2uint32zd2envza2zd2zzcnst_allocz00 = BUNSPEC;
	extern BgL_nodez00_bglt BGl_coercez12z12zzcoerce_coercez00(BgL_nodez00_bglt,
		obj_t, BgL_typez00_bglt, bool_t);
	static BgL_nodez00_bglt BGl_z62cnstzd2alloczd2int64z62zzcnst_allocz00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62cnstzd2alloczd2symbolz62zzcnst_allocz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2cnstzd2offsetz00zzcnst_allocz00(void);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	static obj_t BGl_z62getzd2cnstzd2offsetz62zzcnst_allocz00(obj_t);
	BGL_IMPORT obj_t BGl_bignumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(obj_t,
		long);
	BGL_IMPORT obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t);
	BGL_IMPORT obj_t string_for_read(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcnst_allocz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_libraryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_cnstz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_tvectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__srfi4z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__datez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__unicodez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__ucs2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	BGL_IMPORT obj_t ucs2_string_to_utf8_string(obj_t);
	static obj_t
		BGl_forcezd2initializa7ezd2srfi4zd2libraryzd2modulez12zb5zzcnst_allocz00
		(void);
	static obj_t BGl_typedzd2cnstzd2tablezd2idzd2zzcnst_allocz00(void);
	static obj_t BGl_z62getzd2cnstzd2tablez62zzcnst_allocz00(obj_t);
	static BgL_nodez00_bglt BGl_z62cnstzd2alloczd2keywordz62zzcnst_allocz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2ucs2stringza2z00zztype_cachez00;
	BGL_IMPORT bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_za2symbolzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
	extern obj_t BGl_kwotez00zzast_nodez00;
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static obj_t BGl_za2structzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zzcnst_allocz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcnst_allocz00(void);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2ucs2zd2stringzd2zzcnst_allocz00(obj_t, obj_t);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzcnst_allocz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzcnst_allocz00(void);
	static BgL_nodez00_bglt BGl_z62cnstzd2alloczd2vectorz62zzcnst_allocz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2homogenouszd2vectorzd2zzcnst_allocz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2uint32z00zzcnst_allocz00(uint32_t, obj_t);
	BGL_IMPORT obj_t BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stopzd2cnstzd2allocz12z12zzcnst_allocz00(void);
	extern obj_t BGl_za2vectorzd2tagzd2setz12za2z12zzcnst_cachez00;
	static obj_t BGl_z62startzd2cnstzd2allocz12z70zzcnst_allocz00(obj_t);
	extern obj_t BGl_za2bnilza2z00zztype_cachez00;
	extern obj_t BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t BGl_getzd2cnstzd2sexpz00zzcnst_allocz00(void);
	extern obj_t BGl_za2keywordza2z00zztype_cachez00;
	static bool_t BGl_cnstzd2equalzf3z21zzcnst_allocz00(obj_t, obj_t);
	static obj_t BGl_za2globalzd2setza2zd2zzcnst_allocz00 = BUNSPEC;
	static BgL_nodez00_bglt
		BGl_z62cnstzd2alloczd2procedurez62zzcnst_allocz00(obj_t, obj_t, obj_t);
	static obj_t BGl_za2realzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
	static BgL_nodez00_bglt BGl_z62cnstzd2alloczd2stringz62zzcnst_allocz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2cnstzd2tablez00zzcnst_allocz00(void);
	static obj_t BGl_za2listzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2uint64z00zzcnst_allocz00(uint64_t, obj_t);
	static BgL_nodez00_bglt BGl_z62cnstzd2alloczd2tvectorz62zzcnst_allocz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2stringzd2ze3ucs2stringza2z31zzcnst_cachez00;
	static obj_t BGl_z62cnstzd2tablezd2idz62zzcnst_allocz00(obj_t);
	extern BgL_typez00_bglt BGl_findzd2typezd2zztype_envz00(obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2structz00zzcnst_allocz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62cnstzd2alloczd2lzd2procedurezb0zzcnst_allocz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_variablez00zzast_varz00;
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2procedurez00zzcnst_allocz00(BgL_nodez00_bglt, obj_t);
	extern obj_t BGl_literalz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_cnstzd2alloczd2bignumz00zzcnst_allocz00(obj_t,
		obj_t);
	static BgL_appz00_bglt BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62getzd2cnstzd2setz62zzcnst_allocz00(obj_t);
	static BgL_nodez00_bglt BGl_loopze70ze7zzcnst_allocz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_structzd2ze3listz31zz__structurez00(obj_t);
	extern BgL_typez00_bglt BGl_getzd2typezd2kwotez00zztype_typeofz00(obj_t);
	static obj_t BGl_z62stopzd2cnstzd2allocz12z70zzcnst_allocz00(obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_cnstzd2alloczd2realz00zzcnst_allocz00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62cnstzd2alloczd2realz62zzcnst_allocz00(obj_t,
		obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_cnstz12z12zzcnst_nodez00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	static obj_t __cnst[70];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2cnstzd2tablezd2envzd2zzcnst_allocz00,
		BgL_bgl_za762getza7d2cnstza7d22436za7,
		BGl_z62getzd2cnstzd2tablez62zzcnst_allocz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cnstzd2alloczd2realzd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72437za7,
		BGl_z62cnstzd2alloczd2realz62zzcnst_allocz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cnstzd2tablezd2idzd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2tableza72438za7,
		BGl_z62cnstzd2tablezd2idz62zzcnst_allocz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2structzd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72439za7,
		BGl_z62cnstzd2alloczd2structz62zzcnst_allocz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2cnstzd2setzd2envzd2zzcnst_allocz00,
		BgL_bgl_za762getza7d2cnstza7d22440za7,
		BGl_z62getzd2cnstzd2setz62zzcnst_allocz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2llongzd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72441za7,
		BGl_z62cnstzd2alloczd2llongz62zzcnst_allocz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stopzd2cnstzd2allocz12zd2envzc0zzcnst_allocz00,
		BgL_bgl_za762stopza7d2cnstza7d2442za7,
		BGl_z62stopzd2cnstzd2allocz12z70zzcnst_allocz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2429z00zzcnst_allocz00,
		BgL_bgl_string2429za700za7za7c2443za7, "string:~a", 9);
	      DEFINE_STRING(BGl_string2430z00zzcnst_allocz00,
		BgL_bgl_string2430za700za7za7c2444za7,
		"old should be either #f or cnst-info", 36);
	      DEFINE_STRING(BGl_string2431z00zzcnst_allocz00,
		BgL_bgl_string2431za700za7za7c2445za7, "read-alloc-tvector", 18);
	      DEFINE_STRING(BGl_string2432z00zzcnst_allocz00,
		BgL_bgl_string2432za700za7za7c2446za7, "Unimplementable until bootstrap",
		31);
	      DEFINE_STRING(BGl_string2433z00zzcnst_allocz00,
		BgL_bgl_string2433za700za7za7c2447za7, "cnst_alloc", 10);
	      DEFINE_STRING(BGl_string2434z00zzcnst_allocz00,
		BgL_bgl_string2434za700za7za7c2448za7,
		"cnst-struct struct $tvector-descr-set! get-tvector-descriptor stvector tvector tvec make-s8vector hvector f64 f32 u64 s64 u32 s32 u16 s16 u8 s8 list-> vector var cnst-list pair list suint64 buint64 uint64 sint64 bint64 int64 suint32 buint32 uint32 sint32 bint32 int32 sllong bllong llong selong belong elong sreal real slfun sfun procedure proc cnst-alloc-bignum a-keyword keyword cnst-alloc-symbol a-symbol symbol lib an-ucs2-string ucs2string set! @ cnst-info sstring bstring string static bdb now cnst-vector cnst* __cnsts_table ",
		532);
	BGL_IMPORT obj_t BGl_s64vectorzd2ze3listzd2envze3zz__srfi4z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2keywordzd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72449za7,
		BGl_z62cnstzd2alloczd2keywordz62zzcnst_allocz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2vectorzd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72450za7,
		BGl_z62cnstzd2alloczd2vectorz62zzcnst_allocz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cnstzd2alloczd2listzd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72451za7,
		BGl_z62cnstzd2alloczd2listz62zzcnst_allocz00, 0L, BUNSPEC, 2);
	BGL_IMPORT obj_t BGl_f32vectorzd2ze3listzd2envze3zz__srfi4z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2lzd2procedurezd2envz00zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72452za7,
		BGl_z62cnstzd2alloczd2lzd2procedurezb0zzcnst_allocz00, 0L, BUNSPEC, 2);
	BGL_IMPORT obj_t BGl_u64vectorzd2ze3listzd2envze3zz__srfi4z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2ucs2zd2stringzd2envz00zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72453za7,
		BGl_z62cnstzd2alloczd2ucs2zd2stringzb0zzcnst_allocz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2elongzd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72454za7,
		BGl_z62cnstzd2alloczd2elongz62zzcnst_allocz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2symbolzd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72455za7,
		BGl_z62cnstzd2alloczd2symbolz62zzcnst_allocz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2procedurezd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72456za7,
		BGl_z62cnstzd2alloczd2procedurez62zzcnst_allocz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2int64zd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72457za7,
		BGl_z62cnstzd2alloczd2int64z62zzcnst_allocz00, 0L, BUNSPEC, 2);
	BGL_IMPORT obj_t BGl_s16vectorzd2ze3listzd2envze3zz__srfi4z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2cnstzd2offsetzd2envzd2zzcnst_allocz00,
		BgL_bgl_za762getza7d2cnstza7d22458za7,
		BGl_z62getzd2cnstzd2offsetz62zzcnst_allocz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2uint64zd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72459za7,
		BGl_z62cnstzd2alloczd2uint64z62zzcnst_allocz00, 0L, BUNSPEC, 2);
	BGL_IMPORT obj_t BGl_s32vectorzd2ze3listzd2envze3zz__srfi4z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2bignumzd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72460za7,
		BGl_z62cnstzd2alloczd2bignumz62zzcnst_allocz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2tvectorzd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72461za7,
		BGl_z62cnstzd2alloczd2tvectorz62zzcnst_allocz00, 0L, BUNSPEC, 2);
	BGL_IMPORT obj_t BGl_u16vectorzd2ze3listzd2envze3zz__srfi4z00;
	BGL_IMPORT obj_t BGl_s8vectorzd2ze3listzd2envze3zz__srfi4z00;
	BGL_IMPORT obj_t BGl_u32vectorzd2ze3listzd2envze3zz__srfi4z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2homogenouszd2vectorzd2envz00zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72462za7,
		BGl_z62cnstzd2alloczd2homogenouszd2vectorzb0zzcnst_allocz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_startzd2cnstzd2allocz12zd2envzc0zzcnst_allocz00,
		BgL_bgl_za762startza7d2cnstza72463za7,
		BGl_z62startzd2cnstzd2allocz12z70zzcnst_allocz00, 0L, BUNSPEC, 0);
	BGL_IMPORT obj_t BGl_u8vectorzd2ze3listzd2envze3zz__srfi4z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2int32zd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72464za7,
		BGl_z62cnstzd2alloczd2int32z62zzcnst_allocz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2stringzd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72465za7,
		BGl_z62cnstzd2alloczd2stringz62zzcnst_allocz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cnstzd2alloczd2uint32zd2envzd2zzcnst_allocz00,
		BgL_bgl_za762cnstza7d2allocza72466za7,
		BGl_z62cnstzd2alloczd2uint32z62zzcnst_allocz00, 0L, BUNSPEC, 2);
	BGL_IMPORT obj_t BGl_f64vectorzd2ze3listzd2envze3zz__srfi4z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2cnstzd2sexpzd2envzd2zzcnst_allocz00,
		BgL_bgl_za762getza7d2cnstza7d22467za7,
		BGl_z62getzd2cnstzd2sexpz62zzcnst_allocz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2ucs2stringzd2envza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2stringzd2envza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2llongzd2envza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2elongzd2envza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2bignumzd2envza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2int64zd2envza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2keywordzd2envza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2vectorzd2envza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2int32zd2envza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2cnstzd2tableza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2globalzd2sexpza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2uint64zd2envza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2oldzd2debugza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2uint32zd2envza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2symbolzd2envza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2structzd2envza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2globalzd2setza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2realzd2envza2zd2zzcnst_allocz00));
		     ADD_ROOT((void *) (&BGl_za2listzd2envza2zd2zzcnst_allocz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcnst_allocz00(long
		BgL_checksumz00_3591, char *BgL_fromz00_3592)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcnst_allocz00))
				{
					BGl_requirezd2initializa7ationz75zzcnst_allocz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcnst_allocz00();
					BGl_libraryzd2moduleszd2initz00zzcnst_allocz00();
					BGl_cnstzd2initzd2zzcnst_allocz00();
					BGl_importedzd2moduleszd2initz00zzcnst_allocz00();
					return BGl_toplevelzd2initzd2zzcnst_allocz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcnst_allocz00(void)
	{
		{	/* Cnst/alloc.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__srfi4z00(0L, "cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__hashz00(0L, "cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__datez00(0L, "cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__ucs2z00(0L, "cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__unicodez00(0L, "cnst_alloc");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "cnst_alloc");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcnst_allocz00(void)
	{
		{	/* Cnst/alloc.scm 15 */
			{	/* Cnst/alloc.scm 15 */
				obj_t BgL_cportz00_3579;

				{	/* Cnst/alloc.scm 15 */
					obj_t BgL_stringz00_3586;

					BgL_stringz00_3586 = BGl_string2434z00zzcnst_allocz00;
					{	/* Cnst/alloc.scm 15 */
						obj_t BgL_startz00_3587;

						BgL_startz00_3587 = BINT(0L);
						{	/* Cnst/alloc.scm 15 */
							obj_t BgL_endz00_3588;

							BgL_endz00_3588 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3586)));
							{	/* Cnst/alloc.scm 15 */

								BgL_cportz00_3579 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3586, BgL_startz00_3587, BgL_endz00_3588);
				}}}}
				{
					long BgL_iz00_3580;

					BgL_iz00_3580 = 69L;
				BgL_loopz00_3581:
					if ((BgL_iz00_3580 == -1L))
						{	/* Cnst/alloc.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cnst/alloc.scm 15 */
							{	/* Cnst/alloc.scm 15 */
								obj_t BgL_arg2435z00_3582;

								{	/* Cnst/alloc.scm 15 */

									{	/* Cnst/alloc.scm 15 */
										obj_t BgL_locationz00_3584;

										BgL_locationz00_3584 = BBOOL(((bool_t) 0));
										{	/* Cnst/alloc.scm 15 */

											BgL_arg2435z00_3582 =
												BGl_readz00zz__readerz00(BgL_cportz00_3579,
												BgL_locationz00_3584);
										}
									}
								}
								{	/* Cnst/alloc.scm 15 */
									int BgL_tmpz00_3631;

									BgL_tmpz00_3631 = (int) (BgL_iz00_3580);
									CNST_TABLE_SET(BgL_tmpz00_3631, BgL_arg2435z00_3582);
							}}
							{	/* Cnst/alloc.scm 15 */
								int BgL_auxz00_3585;

								BgL_auxz00_3585 = (int) ((BgL_iz00_3580 - 1L));
								{
									long BgL_iz00_3636;

									BgL_iz00_3636 = (long) (BgL_auxz00_3585);
									BgL_iz00_3580 = BgL_iz00_3636;
									goto BgL_loopz00_3581;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcnst_allocz00(void)
	{
		{	/* Cnst/alloc.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcnst_allocz00(void)
	{
		{	/* Cnst/alloc.scm 15 */
			BGl_za2cnstzd2tableza2zd2zzcnst_allocz00 = BUNSPEC;
			BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00 = -1L;
			BGl_za2stringzd2envza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2ucs2stringzd2envza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2realzd2envza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2elongzd2envza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2llongzd2envza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2int32zd2envza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2uint32zd2envza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2int64zd2envza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2uint64zd2envza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2symbolzd2envza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2keywordzd2envza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2bignumzd2envza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2listzd2envza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2vectorzd2envza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2structzd2envza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2globalzd2setza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2globalzd2sexpza2zd2zzcnst_allocz00 = BNIL;
			return (BGl_za2oldzd2debugza2zd2zzcnst_allocz00 = BUNSPEC, BUNSPEC);
		}

	}



/* get-cnst-offset */
	BGL_EXPORTED_DEF obj_t BGl_getzd2cnstzd2offsetz00zzcnst_allocz00(void)
	{
		{	/* Cnst/alloc.scm 81 */
			return BINT(BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00);
		}

	}



/* &get-cnst-offset */
	obj_t BGl_z62getzd2cnstzd2offsetz62zzcnst_allocz00(obj_t BgL_envz00_3490)
	{
		{	/* Cnst/alloc.scm 81 */
			return BGl_getzd2cnstzd2offsetz00zzcnst_allocz00();
		}

	}



/* get-cnst-set */
	BGL_EXPORTED_DEF obj_t BGl_getzd2cnstzd2setz00zzcnst_allocz00(void)
	{
		{	/* Cnst/alloc.scm 87 */
			return BGl_za2globalzd2setza2zd2zzcnst_allocz00;
		}

	}



/* &get-cnst-set */
	obj_t BGl_z62getzd2cnstzd2setz62zzcnst_allocz00(obj_t BgL_envz00_3491)
	{
		{	/* Cnst/alloc.scm 87 */
			return BGl_getzd2cnstzd2setz00zzcnst_allocz00();
		}

	}



/* get-cnst-table */
	BGL_EXPORTED_DEF obj_t BGl_getzd2cnstzd2tablez00zzcnst_allocz00(void)
	{
		{	/* Cnst/alloc.scm 93 */
			return BGl_za2cnstzd2tableza2zd2zzcnst_allocz00;
		}

	}



/* &get-cnst-table */
	obj_t BGl_z62getzd2cnstzd2tablez62zzcnst_allocz00(obj_t BgL_envz00_3492)
	{
		{	/* Cnst/alloc.scm 93 */
			return BGl_getzd2cnstzd2tablez00zzcnst_allocz00();
		}

	}



/* get-cnst-sexp */
	BGL_EXPORTED_DEF obj_t BGl_getzd2cnstzd2sexpz00zzcnst_allocz00(void)
	{
		{	/* Cnst/alloc.scm 99 */
			return bgl_reverse_bang(BGl_za2globalzd2sexpza2zd2zzcnst_allocz00);
		}

	}



/* &get-cnst-sexp */
	obj_t BGl_z62getzd2cnstzd2sexpz62zzcnst_allocz00(obj_t BgL_envz00_3493)
	{
		{	/* Cnst/alloc.scm 99 */
			return BGl_getzd2cnstzd2sexpz00zzcnst_allocz00();
		}

	}



/* cnst-table-id */
	BGL_EXPORTED_DEF obj_t BGl_cnstzd2tablezd2idz00zzcnst_allocz00(void)
	{
		{	/* Cnst/alloc.scm 145 */
			return CNST_TABLE_REF(0);
		}

	}



/* &cnst-table-id */
	obj_t BGl_z62cnstzd2tablezd2idz62zzcnst_allocz00(obj_t BgL_envz00_3494)
	{
		{	/* Cnst/alloc.scm 145 */
			return BGl_cnstzd2tablezd2idz00zzcnst_allocz00();
		}

	}



/* typed-cnst-table-id */
	obj_t BGl_typedzd2cnstzd2tablezd2idzd2zzcnst_allocz00(void)
	{
		{	/* Cnst/alloc.scm 151 */
			{	/* Cnst/alloc.scm 152 */
				obj_t BgL_arg1544z00_1629;

				BgL_arg1544z00_1629 = CNST_TABLE_REF(0);
				return
					BGl_makezd2typedzd2identz00zzast_identz00(BgL_arg1544z00_1629,
					CNST_TABLE_REF(1));
			}
		}

	}



/* start-cnst-alloc! */
	BGL_EXPORTED_DEF obj_t BGl_startzd2cnstzd2allocz12z12zzcnst_allocz00(void)
	{
		{	/* Cnst/alloc.scm 157 */
			BGl_za2oldzd2debugza2zd2zzcnst_allocz00 =
				BGl_za2compilerzd2debugza2zd2zzengine_paramz00;
			BGl_za2compilerzd2debugza2zd2zzengine_paramz00 = BINT(0L);
			{	/* Cnst/alloc.scm 163 */
				obj_t BgL_arg1546z00_1630;

				BgL_arg1546z00_1630 = BGl_typedzd2cnstzd2tablezd2idzd2zzcnst_allocz00();
				BGl_za2cnstzd2tableza2zd2zzcnst_allocz00 =
					((obj_t)
					BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2(BgL_arg1546z00_1630,
						BGl_za2moduleza2z00zzmodule_modulez00, CNST_TABLE_REF(2),
						CNST_TABLE_REF(3)));
			}
			{	/* Cnst/alloc.scm 170 */
				bool_t BgL_test2470z00_3656;

				if (((long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) >= 3L))
					{	/* Cnst/alloc.scm 171 */
						obj_t BgL_arg1559z00_1636;

						{	/* Cnst/alloc.scm 171 */
							obj_t BgL_arg1561z00_1637;

							BgL_arg1561z00_1637 = BGl_thezd2backendzd2zzbackend_backendz00();
							BgL_arg1559z00_1636 =
								(((BgL_backendz00_bglt) COBJECT(
										((BgL_backendz00_bglt) BgL_arg1561z00_1637)))->
								BgL_debugzd2supportzd2);
						}
						BgL_test2470z00_3656 =
							CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF(4),
								BgL_arg1559z00_1636));
					}
				else
					{	/* Cnst/alloc.scm 170 */
						BgL_test2470z00_3656 = ((bool_t) 0);
					}
				if (BgL_test2470z00_3656)
					{	/* Cnst/alloc.scm 172 */
						BgL_globalz00_bglt BgL_oz00_2756;
						obj_t BgL_vz00_2757;

						BgL_oz00_2756 =
							((BgL_globalz00_bglt) BGl_za2cnstzd2tableza2zd2zzcnst_allocz00);
						BgL_vz00_2757 = CNST_TABLE_REF(5);
						((((BgL_globalz00_bglt) COBJECT(BgL_oz00_2756))->BgL_importz00) =
							((obj_t) BgL_vz00_2757), BUNSPEC);
					}
				else
					{	/* Cnst/alloc.scm 170 */
						BFALSE;
					}
			}
			BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00 = 0L;
			BGl_za2stringzd2envza2zd2zzcnst_allocz00 =
				BGl_makezd2hashtablezd2zz__hashz00(BNIL);
			BGl_za2ucs2stringzd2envza2zd2zzcnst_allocz00 =
				BGl_makezd2hashtablezd2zz__hashz00(BNIL);
			BGl_za2realzd2envza2zd2zzcnst_allocz00 = BNIL;
			BGl_za2symbolzd2envza2zd2zzcnst_allocz00 =
				BGl_makezd2hashtablezd2zz__hashz00(BNIL);
			BGl_za2keywordzd2envza2zd2zzcnst_allocz00 =
				BGl_makezd2hashtablezd2zz__hashz00(BNIL);
			BGl_za2bignumzd2envza2zd2zzcnst_allocz00 = BNIL;
			return BTRUE;
		}

	}



/* &start-cnst-alloc! */
	obj_t BGl_z62startzd2cnstzd2allocz12z70zzcnst_allocz00(obj_t BgL_envz00_3495)
	{
		{	/* Cnst/alloc.scm 157 */
			return BGl_startzd2cnstzd2allocz12z12zzcnst_allocz00();
		}

	}



/* stop-cnst-alloc! */
	BGL_EXPORTED_DEF obj_t BGl_stopzd2cnstzd2allocz12z12zzcnst_allocz00(void)
	{
		{	/* Cnst/alloc.scm 185 */
			BGl_za2compilerzd2debugza2zd2zzengine_paramz00 =
				BGl_za2oldzd2debugza2zd2zzcnst_allocz00;
			BGl_za2stringzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
			BGl_za2ucs2stringzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
			BGl_za2realzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
			BGl_za2symbolzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
			BGl_za2keywordzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
			BGl_za2bignumzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
			BGl_za2listzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
			BGl_za2vectorzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
			BGl_za2structzd2envza2zd2zzcnst_allocz00 = BUNSPEC;
			return BTRUE;
		}

	}



/* &stop-cnst-alloc! */
	obj_t BGl_z62stopzd2cnstzd2allocz12z70zzcnst_allocz00(obj_t BgL_envz00_3496)
	{
		{	/* Cnst/alloc.scm 185 */
			return BGl_stopzd2cnstzd2allocz12z12zzcnst_allocz00();
		}

	}



/* make-cnst-table-ref */
	BgL_appz00_bglt BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00(obj_t
		BgL_offsetz00_35, obj_t BgL_typez00_36, obj_t BgL_locz00_37)
	{
		{	/* Cnst/alloc.scm 201 */
			{	/* Cnst/alloc.scm 202 */
				BgL_appz00_bglt BgL_new1106z00_1642;

				{	/* Cnst/alloc.scm 203 */
					BgL_appz00_bglt BgL_new1105z00_1654;

					BgL_new1105z00_1654 =
						((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_appz00_bgl))));
					{	/* Cnst/alloc.scm 203 */
						long BgL_arg1591z00_1655;

						BgL_arg1591z00_1655 = BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1105z00_1654), BgL_arg1591z00_1655);
					}
					{	/* Cnst/alloc.scm 203 */
						BgL_objectz00_bglt BgL_tmpz00_3679;

						BgL_tmpz00_3679 = ((BgL_objectz00_bglt) BgL_new1105z00_1654);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3679, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1105z00_1654);
					BgL_new1106z00_1642 = BgL_new1105z00_1654;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1106z00_1642)))->BgL_locz00) =
					((obj_t) BgL_locz00_37), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_3685;

					{	/* Cnst/alloc.scm 204 */
						BgL_typez00_bglt BgL_arg1571z00_1643;

						BgL_arg1571z00_1643 = BGl_getzd2defaultzd2typez00zztype_cachez00();
						BgL_auxz00_3685 =
							BGl_strictzd2nodezd2typez00zzast_nodez00(
							((BgL_typez00_bglt) BgL_typez00_36), BgL_arg1571z00_1643);
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1106z00_1642)))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_3685), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt) BgL_new1106z00_1642)))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1106z00_1642)))->BgL_keyz00) =
					((obj_t) BINT(-1L)), BUNSPEC);
				{
					BgL_varz00_bglt BgL_auxz00_3696;

					{	/* Cnst/alloc.scm 205 */
						BgL_refz00_bglt BgL_new1108z00_1644;

						{	/* Cnst/alloc.scm 206 */
							BgL_refz00_bglt BgL_new1107z00_1647;

							BgL_new1107z00_1647 =
								((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_refz00_bgl))));
							{	/* Cnst/alloc.scm 206 */
								long BgL_arg1576z00_1648;

								{	/* Cnst/alloc.scm 206 */
									obj_t BgL_classz00_2762;

									BgL_classz00_2762 = BGl_refz00zzast_nodez00;
									BgL_arg1576z00_1648 = BGL_CLASS_NUM(BgL_classz00_2762);
								}
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1107z00_1647),
									BgL_arg1576z00_1648);
							}
							{	/* Cnst/alloc.scm 206 */
								BgL_objectz00_bglt BgL_tmpz00_3701;

								BgL_tmpz00_3701 = ((BgL_objectz00_bglt) BgL_new1107z00_1647);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3701, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1107z00_1647);
							BgL_new1108z00_1644 = BgL_new1107z00_1647;
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_new1108z00_1644)))->BgL_locz00) =
							((obj_t) BgL_locz00_37), BUNSPEC);
						{
							BgL_typez00_bglt BgL_auxz00_3707;

							{	/* Cnst/alloc.scm 207 */
								BgL_typez00_bglt BgL_arg1573z00_1645;
								BgL_typez00_bglt BgL_arg1575z00_1646;

								BgL_arg1573z00_1645 =
									BGl_getzd2defaultzd2typez00zztype_cachez00();
								{	/* Cnst/alloc.scm 207 */
									BgL_variablez00_bglt BgL_oz00_2766;

									BgL_oz00_2766 =
										((BgL_variablez00_bglt)
										BGl_za2cnstzd2tablezd2refza2z00zzcnst_cachez00);
									BgL_arg1575z00_1646 =
										(((BgL_variablez00_bglt) COBJECT(BgL_oz00_2766))->
										BgL_typez00);
								}
								BgL_auxz00_3707 =
									BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_arg1573z00_1645,
									BgL_arg1575z00_1646);
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1108z00_1644)))->BgL_typez00) =
								((BgL_typez00_bglt) BgL_auxz00_3707), BUNSPEC);
						}
						((((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_new1108z00_1644)))->
								BgL_variablez00) =
							((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
									BGl_za2cnstzd2tablezd2refza2z00zzcnst_cachez00)), BUNSPEC);
						BgL_auxz00_3696 = ((BgL_varz00_bglt) BgL_new1108z00_1644);
					}
					((((BgL_appz00_bglt) COBJECT(BgL_new1106z00_1642))->BgL_funz00) =
						((BgL_varz00_bglt) BgL_auxz00_3696), BUNSPEC);
				}
				{
					obj_t BgL_auxz00_3719;

					{	/* Cnst/alloc.scm 209 */
						BgL_literalz00_bglt BgL_arg1584z00_1649;

						{	/* Cnst/alloc.scm 209 */
							BgL_literalz00_bglt BgL_new1110z00_1651;

							{	/* Cnst/alloc.scm 210 */
								BgL_literalz00_bglt BgL_new1109z00_1652;

								BgL_new1109z00_1652 =
									((BgL_literalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_literalz00_bgl))));
								{	/* Cnst/alloc.scm 210 */
									long BgL_arg1589z00_1653;

									{	/* Cnst/alloc.scm 210 */
										obj_t BgL_classz00_2767;

										BgL_classz00_2767 = BGl_literalz00zzast_nodez00;
										BgL_arg1589z00_1653 = BGL_CLASS_NUM(BgL_classz00_2767);
									}
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1109z00_1652),
										BgL_arg1589z00_1653);
								}
								{	/* Cnst/alloc.scm 210 */
									BgL_objectz00_bglt BgL_tmpz00_3724;

									BgL_tmpz00_3724 = ((BgL_objectz00_bglt) BgL_new1109z00_1652);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3724, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1109z00_1652);
								BgL_new1110z00_1651 = BgL_new1109z00_1652;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1110z00_1651)))->BgL_locz00) =
								((obj_t) BgL_locz00_37), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1110z00_1651)))->BgL_typez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BGl_za2intza2z00zztype_cachez00)), BUNSPEC);
							((((BgL_atomz00_bglt) COBJECT(((BgL_atomz00_bglt)
												BgL_new1110z00_1651)))->BgL_valuez00) =
								((obj_t) BgL_offsetz00_35), BUNSPEC);
							BgL_arg1584z00_1649 = BgL_new1110z00_1651;
						}
						{	/* Cnst/alloc.scm 209 */
							obj_t BgL_list1585z00_1650;

							BgL_list1585z00_1650 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_arg1584z00_1649), BNIL);
							BgL_auxz00_3719 = BgL_list1585z00_1650;
					}}
					((((BgL_appz00_bglt) COBJECT(BgL_new1106z00_1642))->BgL_argsz00) =
						((obj_t) BgL_auxz00_3719), BUNSPEC);
				}
				((((BgL_appz00_bglt) COBJECT(BgL_new1106z00_1642))->BgL_stackablez00) =
					((obj_t) BFALSE), BUNSPEC);
				return BgL_new1106z00_1642;
			}
		}

	}



/* cnst-alloc-string */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2stringz00zzcnst_allocz00(obj_t BgL_stringz00_38,
		obj_t BgL_locz00_39)
	{
		{	/* Cnst/alloc.scm 221 */
			{

				{	/* Cnst/alloc.scm 258 */
					obj_t BgL_oldz00_1658;

					{	/* Cnst/alloc.scm 258 */
						obj_t BgL__andtest_1117z00_1662;

						BgL__andtest_1117z00_1662 =
							BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00;
						if (CBOOL(BgL__andtest_1117z00_1662))
							{	/* Cnst/alloc.scm 258 */
								BgL_oldz00_1658 =
									BGl_hashtablezd2getzd2zz__hashz00
									(BGl_za2stringzd2envza2zd2zzcnst_allocz00, BgL_stringz00_38);
							}
						else
							{	/* Cnst/alloc.scm 258 */
								BgL_oldz00_1658 = BFALSE;
							}
					}
					if (CBOOL(BgL_oldz00_1658))
						{	/* Cnst/alloc.scm 261 */
							BgL_refz00_bglt BgL_new1119z00_1659;

							{	/* Cnst/alloc.scm 262 */
								BgL_refz00_bglt BgL_new1118z00_1660;

								BgL_new1118z00_1660 =
									((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_refz00_bgl))));
								{	/* Cnst/alloc.scm 262 */
									long BgL_arg1593z00_1661;

									BgL_arg1593z00_1661 = BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1118z00_1660),
										BgL_arg1593z00_1661);
								}
								{	/* Cnst/alloc.scm 262 */
									BgL_objectz00_bglt BgL_tmpz00_3748;

									BgL_tmpz00_3748 = ((BgL_objectz00_bglt) BgL_new1118z00_1660);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3748, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1118z00_1660);
								BgL_new1119z00_1659 = BgL_new1118z00_1660;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1119z00_1659)))->BgL_locz00) =
								((obj_t) BgL_locz00_39), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1119z00_1659)))->BgL_typez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BGl_za2bstringza2z00zztype_cachez00)), BUNSPEC);
							((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
												BgL_new1119z00_1659)))->BgL_variablez00) =
								((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
										STRUCT_REF(((obj_t) BgL_oldz00_1658), (int) (1L)))),
								BUNSPEC);
							return ((BgL_nodez00_bglt) BgL_new1119z00_1659);
						}
					else
						{
							BgL_refz00_bglt BgL_auxz00_3764;

							{	/* Cnst/alloc.scm 239 */
								BgL_refz00_bglt BgL_refz00_1672;

								{	/* Cnst/alloc.scm 225 */
									BgL_globalz00_bglt BgL_varz00_1664;

									{	/* Cnst/alloc.scm 226 */
										obj_t BgL_arg1605z00_1669;

										{	/* Cnst/alloc.scm 226 */
											obj_t BgL_arg1606z00_1670;

											BgL_arg1606z00_1670 =
												BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(6));
											BgL_arg1605z00_1669 =
												BGl_makezd2typedzd2identz00zzast_identz00
												(BgL_arg1606z00_1670, CNST_TABLE_REF(7));
										}
										BgL_varz00_1664 =
											BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2
											(BgL_arg1605z00_1669,
											BGl_za2moduleza2z00zzmodule_modulez00, BgL_stringz00_38,
											CNST_TABLE_REF(8), BgL_locz00_39);
									}
									if (CBOOL(BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00))
										{	/* Cnst/alloc.scm 232 */
											obj_t BgL_arg1595z00_1665;

											{	/* Cnst/alloc.scm 69 */
												obj_t BgL_newz00_2772;

												BgL_newz00_2772 =
													create_struct(CNST_TABLE_REF(9), (int) (2L));
												{	/* Cnst/alloc.scm 69 */
													obj_t BgL_auxz00_3778;
													int BgL_tmpz00_3776;

													BgL_auxz00_3778 = ((obj_t) BgL_varz00_1664);
													BgL_tmpz00_3776 = (int) (1L);
													STRUCT_SET(BgL_newz00_2772, BgL_tmpz00_3776,
														BgL_auxz00_3778);
												}
												{	/* Cnst/alloc.scm 69 */
													int BgL_tmpz00_3781;

													BgL_tmpz00_3781 = (int) (0L);
													STRUCT_SET(BgL_newz00_2772, BgL_tmpz00_3781,
														BgL_stringz00_38);
												}
												BgL_arg1595z00_1665 = BgL_newz00_2772;
											}
											BGl_hashtablezd2putz12zc0zz__hashz00
												(BGl_za2stringzd2envza2zd2zzcnst_allocz00,
												BgL_stringz00_38, BgL_arg1595z00_1665);
										}
									else
										{	/* Cnst/alloc.scm 231 */
											BFALSE;
										}
									{	/* Cnst/alloc.scm 233 */
										BgL_refz00_bglt BgL_new1113z00_1666;

										{	/* Cnst/alloc.scm 234 */
											BgL_refz00_bglt BgL_new1111z00_1667;

											BgL_new1111z00_1667 =
												((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_refz00_bgl))));
											{	/* Cnst/alloc.scm 234 */
												long BgL_arg1602z00_1668;

												BgL_arg1602z00_1668 =
													BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1111z00_1667),
													BgL_arg1602z00_1668);
											}
											{	/* Cnst/alloc.scm 234 */
												BgL_objectz00_bglt BgL_tmpz00_3789;

												BgL_tmpz00_3789 =
													((BgL_objectz00_bglt) BgL_new1111z00_1667);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3789, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1111z00_1667);
											BgL_new1113z00_1666 = BgL_new1111z00_1667;
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1113z00_1666)))->
												BgL_locz00) = ((obj_t) BgL_locz00_39), BUNSPEC);
										((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
															BgL_new1113z00_1666)))->BgL_typez00) =
											((BgL_typez00_bglt) ((BgL_typez00_bglt)
													BGl_za2bstringza2z00zztype_cachez00)), BUNSPEC);
										((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
															BgL_new1113z00_1666)))->BgL_variablez00) =
											((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
													BgL_varz00_1664)), BUNSPEC);
										BgL_refz00_1672 = BgL_new1113z00_1666;
								}}
								{	/* Cnst/alloc.scm 240 */
									bool_t BgL_test2475z00_3801;

									{	/* Cnst/alloc.scm 240 */
										obj_t BgL_arg1678z00_1696;

										BgL_arg1678z00_1696 =
											BGl_thezd2backendzd2zzbackend_backendz00();
										BgL_test2475z00_3801 =
											(((BgL_backendz00_bglt) COBJECT(
													((BgL_backendz00_bglt) BgL_arg1678z00_1696)))->
											BgL_stringzd2literalzd2supportz00);
									}
									if (BgL_test2475z00_3801)
										{	/* Cnst/alloc.scm 240 */
											BFALSE;
										}
									else
										{	/* Cnst/alloc.scm 247 */
											obj_t BgL_arg1611z00_1676;

											{	/* Cnst/alloc.scm 247 */
												obj_t BgL_arg1613z00_1677;

												{	/* Cnst/alloc.scm 247 */
													obj_t BgL_arg1615z00_1678;
													obj_t BgL_arg1616z00_1679;

													{	/* Cnst/alloc.scm 247 */
														obj_t BgL_arg1625z00_1680;

														{	/* Cnst/alloc.scm 247 */
															obj_t BgL_arg1626z00_1681;
															obj_t BgL_arg1627z00_1682;

															BgL_arg1626z00_1681 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_globalz00_bglt)
																				(((BgL_varz00_bglt) COBJECT(
																							((BgL_varz00_bglt)
																								BgL_refz00_1672)))->
																					BgL_variablez00)))))->BgL_idz00);
															{	/* Cnst/alloc.scm 247 */
																obj_t BgL_arg1630z00_1684;

																BgL_arg1630z00_1684 =
																	(((BgL_globalz00_bglt) COBJECT(
																			((BgL_globalz00_bglt)
																				(((BgL_varz00_bglt) COBJECT(
																							((BgL_varz00_bglt)
																								BgL_refz00_1672)))->
																					BgL_variablez00))))->BgL_modulez00);
																BgL_arg1627z00_1682 =
																	MAKE_YOUNG_PAIR(BgL_arg1630z00_1684, BNIL);
															}
															BgL_arg1625z00_1680 =
																MAKE_YOUNG_PAIR(BgL_arg1626z00_1681,
																BgL_arg1627z00_1682);
														}
														BgL_arg1615z00_1678 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
															BgL_arg1625z00_1680);
													}
													{	/* Cnst/alloc.scm 249 */
														BgL_nodez00_bglt BgL_arg1646z00_1686;

														{	/* Cnst/alloc.scm 249 */
															BgL_pragmaz00_bglt BgL_arg1650z00_1687;
															BgL_variablez00_bglt BgL_arg1651z00_1688;
															BgL_typez00_bglt BgL_arg1654z00_1689;

															{	/* Cnst/alloc.scm 249 */
																BgL_pragmaz00_bglt BgL_new1116z00_1690;

																{	/* Cnst/alloc.scm 251 */
																	BgL_pragmaz00_bglt BgL_new1115z00_1694;

																	BgL_new1115z00_1694 =
																		((BgL_pragmaz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_pragmaz00_bgl))));
																	{	/* Cnst/alloc.scm 251 */
																		long BgL_arg1675z00_1695;

																		BgL_arg1675z00_1695 =
																			BGL_CLASS_NUM(BGl_pragmaz00zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1115z00_1694),
																			BgL_arg1675z00_1695);
																	}
																	{	/* Cnst/alloc.scm 251 */
																		BgL_objectz00_bglt BgL_tmpz00_3822;

																		BgL_tmpz00_3822 =
																			((BgL_objectz00_bglt)
																			BgL_new1115z00_1694);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3822,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1115z00_1694);
																	BgL_new1116z00_1690 = BgL_new1115z00_1694;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1116z00_1690)))->BgL_locz00) =
																	((obj_t) BFALSE), BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1116z00_1690)))->BgL_typez00) =
																	((BgL_typez00_bglt) ((BgL_typez00_bglt)
																			BGl_za2bstringza2z00zztype_cachez00)),
																	BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1116z00_1690)))->
																		BgL_sidezd2effectzd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1116z00_1690)))->BgL_keyz00) =
																	((obj_t) BINT(-1L)), BUNSPEC);
																((((BgL_externz00_bglt)
																			COBJECT(((BgL_externz00_bglt)
																					BgL_new1116z00_1690)))->
																		BgL_exprza2za2) = ((obj_t) BNIL), BUNSPEC);
																((((BgL_externz00_bglt)
																			COBJECT(((BgL_externz00_bglt)
																					BgL_new1116z00_1690)))->
																		BgL_effectz00) =
																	((obj_t) BUNSPEC), BUNSPEC);
																{
																	obj_t BgL_auxz00_3840;

																	{	/* Cnst/alloc.scm 252 */
																		obj_t BgL_arg1661z00_1691;

																		BgL_arg1661z00_1691 =
																			string_for_read(BgL_stringz00_38);
																		{	/* Cnst/alloc.scm 252 */
																			obj_t BgL_list1662z00_1692;

																			BgL_list1662z00_1692 =
																				MAKE_YOUNG_PAIR(BgL_arg1661z00_1691,
																				BNIL);
																			BgL_auxz00_3840 =
																				BGl_formatz00zz__r4_output_6_10_3z00
																				(BGl_string2429z00zzcnst_allocz00,
																				BgL_list1662z00_1692);
																	}}
																	((((BgL_pragmaz00_bglt)
																				COBJECT(BgL_new1116z00_1690))->
																			BgL_formatz00) =
																		((obj_t) BgL_auxz00_3840), BUNSPEC);
																}
																{
																	obj_t BgL_auxz00_3845;

																	{	/* Cnst/alloc.scm 251 */
																		obj_t BgL_arg1663z00_1693;

																		BgL_arg1663z00_1693 =
																			BGl_thezd2backendzd2zzbackend_backendz00
																			();
																		BgL_auxz00_3845 =
																			(((BgL_backendz00_bglt)
																				COBJECT(((BgL_backendz00_bglt)
																						BgL_arg1663z00_1693)))->
																			BgL_srfi0z00);
																	}
																	((((BgL_pragmaz00_bglt)
																				COBJECT(BgL_new1116z00_1690))->
																			BgL_srfi0z00) =
																		((obj_t) BgL_auxz00_3845), BUNSPEC);
																}
																BgL_arg1650z00_1687 = BgL_new1116z00_1690;
															}
															BgL_arg1651z00_1688 =
																(((BgL_varz00_bglt) COBJECT(
																		((BgL_varz00_bglt) BgL_refz00_1672)))->
																BgL_variablez00);
															BgL_arg1654z00_1689 =
																BGl_strictzd2nodezd2typez00zzast_nodez00((
																	(BgL_typez00_bglt)
																	BGl_za2bstringza2z00zztype_cachez00),
																((BgL_typez00_bglt)
																	BGl_za2objza2z00zztype_cachez00));
															BgL_arg1646z00_1686 =
																BGl_coercez12z12zzcoerce_coercez00((
																	(BgL_nodez00_bglt) BgL_arg1650z00_1687),
																((obj_t) BgL_arg1651z00_1688),
																BgL_arg1654z00_1689, ((bool_t) 0));
														}
														BgL_arg1616z00_1679 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg1646z00_1686), BNIL);
													}
													BgL_arg1613z00_1677 =
														MAKE_YOUNG_PAIR(BgL_arg1615z00_1678,
														BgL_arg1616z00_1679);
												}
												BgL_arg1611z00_1676 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
													BgL_arg1613z00_1677);
											}
											BGl_za2globalzd2sexpza2zd2zzcnst_allocz00 =
												MAKE_YOUNG_PAIR(BgL_arg1611z00_1676,
												BGl_za2globalzd2sexpza2zd2zzcnst_allocz00);
								}}
								BgL_auxz00_3764 = BgL_refz00_1672;
							}
							return ((BgL_nodez00_bglt) BgL_auxz00_3764);
						}
				}
			}
		}

	}



/* &cnst-alloc-string */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2stringz62zzcnst_allocz00(obj_t
		BgL_envz00_3497, obj_t BgL_stringz00_3498, obj_t BgL_locz00_3499)
	{
		{	/* Cnst/alloc.scm 221 */
			return
				BGl_cnstzd2alloczd2stringz00zzcnst_allocz00(BgL_stringz00_3498,
				BgL_locz00_3499);
		}

	}



/* cnst-alloc-ucs2-string */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2ucs2zd2stringzd2zzcnst_allocz00(obj_t BgL_stringz00_40,
		obj_t BgL_locz00_41)
	{
		{	/* Cnst/alloc.scm 271 */
			{
				obj_t BgL_bstringz00_1716;

				{	/* Cnst/alloc.scm 303 */
					obj_t BgL_oldz00_1701;
					obj_t BgL_stringzd2aszd2bstringz00_1702;

					BgL_oldz00_1701 =
						BGl_hashtablezd2getzd2zz__hashz00
						(BGl_za2ucs2stringzd2envza2zd2zzcnst_allocz00, BgL_stringz00_40);
					BgL_stringzd2aszd2bstringz00_1702 =
						ucs2_string_to_utf8_string(BgL_stringz00_40);
					if (CBOOL(BgL_oldz00_1701))
						{	/* Cnst/alloc.scm 306 */
							if (
								(BGl_za2initzd2modeza2zd2zzengine_paramz00 ==
									CNST_TABLE_REF(14)))
								{	/* Cnst/alloc.scm 308 */
									BgL_refz00_bglt BgL_new1130z00_1703;

									{	/* Cnst/alloc.scm 309 */
										BgL_refz00_bglt BgL_new1129z00_1705;

										BgL_new1129z00_1705 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Cnst/alloc.scm 309 */
											long BgL_arg1688z00_1706;

											BgL_arg1688z00_1706 =
												BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1129z00_1705),
												BgL_arg1688z00_1706);
										}
										{	/* Cnst/alloc.scm 309 */
											BgL_objectz00_bglt BgL_tmpz00_3877;

											BgL_tmpz00_3877 =
												((BgL_objectz00_bglt) BgL_new1129z00_1705);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3877, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1129z00_1705);
										BgL_new1130z00_1703 = BgL_new1129z00_1705;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1130z00_1703)))->
											BgL_locz00) = ((obj_t) BgL_locz00_41), BUNSPEC);
									{
										BgL_typez00_bglt BgL_auxz00_3883;

										{	/* Cnst/alloc.scm 310 */
											BgL_typez00_bglt BgL_arg1681z00_1704;

											BgL_arg1681z00_1704 =
												BGl_getzd2defaultzd2typez00zztype_cachez00();
											BgL_auxz00_3883 =
												BGl_strictzd2nodezd2typez00zzast_nodez00(
												((BgL_typez00_bglt)
													BGl_za2ucs2stringza2z00zztype_cachez00),
												BgL_arg1681z00_1704);
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1130z00_1703)))->
												BgL_typez00) =
											((BgL_typez00_bglt) BgL_auxz00_3883), BUNSPEC);
									}
									((((BgL_varz00_bglt) COBJECT(
													((BgL_varz00_bglt) BgL_new1130z00_1703)))->
											BgL_variablez00) =
										((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
												STRUCT_REF(((obj_t) BgL_oldz00_1701), (int) (1L)))),
										BUNSPEC);
									return ((BgL_nodez00_bglt) BgL_new1130z00_1703);
								}
							else
								{	/* Cnst/alloc.scm 312 */
									obj_t BgL_arg1689z00_1707;

									BgL_arg1689z00_1707 =
										STRUCT_REF(((obj_t) BgL_oldz00_1701), (int) (1L));
									return
										((BgL_nodez00_bglt)
										BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00
										(BgL_arg1689z00_1707,
											BGl_za2ucs2stringza2z00zztype_cachez00, BgL_locz00_41));
								}
						}
					else
						{	/* Cnst/alloc.scm 313 */
							bool_t BgL_test2478z00_3901;

							if (
								(BGl_za2initzd2modeza2zd2zzengine_paramz00 ==
									CNST_TABLE_REF(14)))
								{	/* Cnst/alloc.scm 313 */
									BgL_test2478z00_3901 = ((bool_t) 1);
								}
							else
								{	/* Cnst/alloc.scm 313 */
									if ((STRING_LENGTH(BgL_stringzd2aszd2bstringz00_1702) == 0L))
										{	/* Cnst/alloc.scm 314 */
											BgL_test2478z00_3901 = ((bool_t) 1);
										}
									else
										{	/* Cnst/alloc.scm 314 */
											BgL_test2478z00_3901 =
												(STRING_REF(BgL_stringzd2aszd2bstringz00_1702,
													0L) == ((unsigned char) ';'));
								}}
							if (BgL_test2478z00_3901)
								{
									BgL_refz00_bglt BgL_auxz00_3910;

									BgL_bstringz00_1716 = BgL_stringzd2aszd2bstringz00_1702;
									{	/* Cnst/alloc.scm 273 */
										BgL_globalz00_bglt BgL_varz00_1718;
										BgL_nodez00_bglt BgL_vsz00_1719;

										{	/* Cnst/alloc.scm 274 */
											obj_t BgL_arg1738z00_1742;

											{	/* Cnst/alloc.scm 274 */
												obj_t BgL_arg1739z00_1743;

												BgL_arg1739z00_1743 =
													BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF
													(12));
												BgL_arg1738z00_1742 =
													BGl_makezd2typedzd2identz00zzast_identz00
													(BgL_arg1739z00_1743, CNST_TABLE_REF(12));
											}
											BgL_varz00_1718 =
												BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2
												(BgL_arg1738z00_1742,
												BGl_za2moduleza2z00zzmodule_modulez00,
												CNST_TABLE_REF(13), CNST_TABLE_REF(3));
										}
										BgL_vsz00_1719 =
											BGl_cnstzd2alloczd2stringz00zzcnst_allocz00
											(BgL_bstringz00_1716, BgL_locz00_41);
										{	/* Cnst/alloc.scm 279 */
											obj_t BgL_arg1705z00_1720;

											{	/* Cnst/alloc.scm 69 */
												obj_t BgL_newz00_2793;

												BgL_newz00_2793 =
													create_struct(CNST_TABLE_REF(9), (int) (2L));
												{	/* Cnst/alloc.scm 69 */
													obj_t BgL_auxz00_3924;
													int BgL_tmpz00_3922;

													BgL_auxz00_3924 = ((obj_t) BgL_varz00_1718);
													BgL_tmpz00_3922 = (int) (1L);
													STRUCT_SET(BgL_newz00_2793, BgL_tmpz00_3922,
														BgL_auxz00_3924);
												}
												{	/* Cnst/alloc.scm 69 */
													int BgL_tmpz00_3927;

													BgL_tmpz00_3927 = (int) (0L);
													STRUCT_SET(BgL_newz00_2793, BgL_tmpz00_3927,
														BgL_stringz00_40);
												}
												BgL_arg1705z00_1720 = BgL_newz00_2793;
											}
											BGl_hashtablezd2putz12zc0zz__hashz00
												(BGl_za2ucs2stringzd2envza2zd2zzcnst_allocz00,
												BgL_stringz00_40, BgL_arg1705z00_1720);
										}
										{	/* Cnst/alloc.scm 280 */
											obj_t BgL_arg1708z00_1721;

											{	/* Cnst/alloc.scm 280 */
												obj_t BgL_arg1709z00_1722;

												{	/* Cnst/alloc.scm 280 */
													obj_t BgL_arg1710z00_1723;
													obj_t BgL_arg1711z00_1724;

													{	/* Cnst/alloc.scm 280 */
														obj_t BgL_arg1714z00_1725;

														{	/* Cnst/alloc.scm 280 */
															obj_t BgL_arg1717z00_1726;
															obj_t BgL_arg1718z00_1727;

															BgL_arg1717z00_1726 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt) BgL_varz00_1718)))->
																BgL_idz00);
															BgL_arg1718z00_1727 =
																MAKE_YOUNG_PAIR((((BgL_globalz00_bglt)
																		COBJECT(BgL_varz00_1718))->BgL_modulez00),
																BNIL);
															BgL_arg1714z00_1725 =
																MAKE_YOUNG_PAIR(BgL_arg1717z00_1726,
																BgL_arg1718z00_1727);
														}
														BgL_arg1710z00_1723 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
															BgL_arg1714z00_1725);
													}
													{	/* Cnst/alloc.scm 282 */
														BgL_nodez00_bglt BgL_arg1722z00_1729;

														{	/* Cnst/alloc.scm 282 */
															BgL_appz00_bglt BgL_arg1724z00_1730;
															BgL_typez00_bglt BgL_arg1733z00_1731;

															{	/* Cnst/alloc.scm 282 */
																BgL_appz00_bglt BgL_new1122z00_1732;

																{	/* Cnst/alloc.scm 289 */
																	BgL_appz00_bglt BgL_new1120z00_1737;

																	BgL_new1120z00_1737 =
																		((BgL_appz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_appz00_bgl))));
																	{	/* Cnst/alloc.scm 289 */
																		long BgL_arg1736z00_1738;

																		BgL_arg1736z00_1738 =
																			BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1120z00_1737),
																			BgL_arg1736z00_1738);
																	}
																	{	/* Cnst/alloc.scm 289 */
																		BgL_objectz00_bglt BgL_tmpz00_3942;

																		BgL_tmpz00_3942 =
																			((BgL_objectz00_bglt)
																			BgL_new1120z00_1737);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3942,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1120z00_1737);
																	BgL_new1122z00_1732 = BgL_new1120z00_1737;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1122z00_1732)))->BgL_locz00) =
																	((obj_t) BgL_locz00_41), BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1122z00_1732)))->BgL_typez00) =
																	((BgL_typez00_bglt) ((BgL_typez00_bglt)
																			BGl_za2ucs2stringza2z00zztype_cachez00)),
																	BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1122z00_1732)))->
																		BgL_sidezd2effectzd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1122z00_1732)))->BgL_keyz00) =
																	((obj_t) BINT(-1L)), BUNSPEC);
																{
																	BgL_varz00_bglt BgL_auxz00_3956;

																	{	/* Cnst/alloc.scm 285 */
																		BgL_refz00_bglt BgL_new1124z00_1733;

																		{	/* Cnst/alloc.scm 288 */
																			BgL_refz00_bglt BgL_new1123z00_1734;

																			BgL_new1123z00_1734 =
																				((BgL_refz00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_refz00_bgl))));
																			{	/* Cnst/alloc.scm 288 */
																				long BgL_arg1734z00_1735;

																				{	/* Cnst/alloc.scm 288 */
																					obj_t BgL_classz00_2802;

																					BgL_classz00_2802 =
																						BGl_refz00zzast_nodez00;
																					BgL_arg1734z00_1735 =
																						BGL_CLASS_NUM(BgL_classz00_2802);
																				}
																				BGL_OBJECT_CLASS_NUM_SET(
																					((BgL_objectz00_bglt)
																						BgL_new1123z00_1734),
																					BgL_arg1734z00_1735);
																			}
																			{	/* Cnst/alloc.scm 288 */
																				BgL_objectz00_bglt BgL_tmpz00_3961;

																				BgL_tmpz00_3961 =
																					((BgL_objectz00_bglt)
																					BgL_new1123z00_1734);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3961,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1123z00_1734);
																			BgL_new1124z00_1733 = BgL_new1123z00_1734;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1124z00_1733)))->
																				BgL_locz00) =
																			((obj_t) BgL_locz00_41), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1124z00_1733)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) ((BgL_typez00_bglt)
																					BGl_za2ucs2stringza2z00zztype_cachez00)),
																			BUNSPEC);
																		((((BgL_varz00_bglt)
																					COBJECT(((BgL_varz00_bglt)
																							BgL_new1124z00_1733)))->
																				BgL_variablez00) =
																			((BgL_variablez00_bglt) (
																					(BgL_variablez00_bglt)
																					BGl_za2stringzd2ze3ucs2stringza2z31zzcnst_cachez00)),
																			BUNSPEC);
																		BgL_auxz00_3956 =
																			((BgL_varz00_bglt) BgL_new1124z00_1733);
																	}
																	((((BgL_appz00_bglt)
																				COBJECT(BgL_new1122z00_1732))->
																			BgL_funz00) =
																		((BgL_varz00_bglt) BgL_auxz00_3956),
																		BUNSPEC);
																}
																{
																	obj_t BgL_auxz00_3975;

																	{	/* Cnst/alloc.scm 289 */
																		obj_t BgL_list1735z00_1736;

																		BgL_list1735z00_1736 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_vsz00_1719), BNIL);
																		BgL_auxz00_3975 = BgL_list1735z00_1736;
																	}
																	((((BgL_appz00_bglt)
																				COBJECT(BgL_new1122z00_1732))->
																			BgL_argsz00) =
																		((obj_t) BgL_auxz00_3975), BUNSPEC);
																}
																((((BgL_appz00_bglt)
																			COBJECT(BgL_new1122z00_1732))->
																		BgL_stackablez00) =
																	((obj_t) BFALSE), BUNSPEC);
																BgL_arg1724z00_1730 = BgL_new1122z00_1732;
															}
															BgL_arg1733z00_1731 =
																BGl_strictzd2nodezd2typez00zzast_nodez00(
																((BgL_typez00_bglt)
																	BGl_za2ucs2stringza2z00zztype_cachez00),
																((BgL_typez00_bglt)
																	BGl_za2objza2z00zztype_cachez00));
															BgL_arg1722z00_1729 =
																BGl_coercez12z12zzcoerce_coercez00((
																	(BgL_nodez00_bglt) BgL_arg1724z00_1730),
																((obj_t) BgL_varz00_1718), BgL_arg1733z00_1731,
																((bool_t) 0));
														}
														BgL_arg1711z00_1724 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg1722z00_1729), BNIL);
													}
													BgL_arg1709z00_1722 =
														MAKE_YOUNG_PAIR(BgL_arg1710z00_1723,
														BgL_arg1711z00_1724);
												}
												BgL_arg1708z00_1721 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
													BgL_arg1709z00_1722);
											}
											BGl_za2globalzd2sexpza2zd2zzcnst_allocz00 =
												MAKE_YOUNG_PAIR(BgL_arg1708z00_1721,
												BGl_za2globalzd2sexpza2zd2zzcnst_allocz00);
										}
										{	/* Cnst/alloc.scm 293 */
											BgL_refz00_bglt BgL_new1127z00_1739;

											{	/* Cnst/alloc.scm 294 */
												BgL_refz00_bglt BgL_new1125z00_1740;

												BgL_new1125z00_1740 =
													((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_refz00_bgl))));
												{	/* Cnst/alloc.scm 294 */
													long BgL_arg1737z00_1741;

													BgL_arg1737z00_1741 =
														BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1125z00_1740),
														BgL_arg1737z00_1741);
												}
												{	/* Cnst/alloc.scm 294 */
													BgL_objectz00_bglt BgL_tmpz00_3996;

													BgL_tmpz00_3996 =
														((BgL_objectz00_bglt) BgL_new1125z00_1740);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3996, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1125z00_1740);
												BgL_new1127z00_1739 = BgL_new1125z00_1740;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1127z00_1739)))->
													BgL_locz00) = ((obj_t) BgL_locz00_41), BUNSPEC);
											((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_new1127z00_1739)))->BgL_typez00) =
												((BgL_typez00_bglt) (((BgL_variablez00_bglt)
															COBJECT(((BgL_variablez00_bglt)
																	BgL_varz00_1718)))->BgL_typez00)), BUNSPEC);
											((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																BgL_new1127z00_1739)))->BgL_variablez00) =
												((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
														BgL_varz00_1718)), BUNSPEC);
											BgL_auxz00_3910 = BgL_new1127z00_1739;
									}}
									return ((BgL_nodez00_bglt) BgL_auxz00_3910);
								}
							else
								{
									BgL_appz00_bglt BgL_auxz00_4010;

									{	/* Cnst/alloc.scm 298 */
										long BgL_offsetz00_1745;

										BgL_offsetz00_1745 =
											BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00;
										BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00 =
											(1L + BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00);
										BGl_za2globalzd2setza2zd2zzcnst_allocz00 =
											MAKE_YOUNG_PAIR(BgL_stringz00_40,
											BGl_za2globalzd2setza2zd2zzcnst_allocz00);
										{	/* Cnst/alloc.scm 301 */
											obj_t BgL_arg1746z00_1746;

											{	/* Cnst/alloc.scm 69 */
												obj_t BgL_newz00_2813;

												BgL_newz00_2813 =
													create_struct(CNST_TABLE_REF(9), (int) (2L));
												{	/* Cnst/alloc.scm 69 */
													obj_t BgL_auxz00_4018;
													int BgL_tmpz00_4016;

													BgL_auxz00_4018 = BINT(BgL_offsetz00_1745);
													BgL_tmpz00_4016 = (int) (1L);
													STRUCT_SET(BgL_newz00_2813, BgL_tmpz00_4016,
														BgL_auxz00_4018);
												}
												{	/* Cnst/alloc.scm 69 */
													int BgL_tmpz00_4021;

													BgL_tmpz00_4021 = (int) (0L);
													STRUCT_SET(BgL_newz00_2813, BgL_tmpz00_4021,
														BgL_stringz00_40);
												}
												BgL_arg1746z00_1746 = BgL_newz00_2813;
											}
											BGl_hashtablezd2putz12zc0zz__hashz00
												(BGl_za2ucs2stringzd2envza2zd2zzcnst_allocz00,
												BgL_stringz00_40, BgL_arg1746z00_1746);
										}
										BgL_auxz00_4010 =
											BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00(BINT
											(BgL_offsetz00_1745),
											BGl_za2ucs2stringza2z00zztype_cachez00, BgL_locz00_41);
									}
									return ((BgL_nodez00_bglt) BgL_auxz00_4010);
								}
						}
				}
			}
		}

	}



/* &cnst-alloc-ucs2-string */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2ucs2zd2stringzb0zzcnst_allocz00(obj_t
		BgL_envz00_3500, obj_t BgL_stringz00_3501, obj_t BgL_locz00_3502)
	{
		{	/* Cnst/alloc.scm 271 */
			return
				BGl_cnstzd2alloczd2ucs2zd2stringzd2zzcnst_allocz00(BgL_stringz00_3501,
				BgL_locz00_3502);
		}

	}



/* cnst-alloc-symbol */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2symbolz00zzcnst_allocz00(obj_t BgL_symbolz00_42,
		obj_t BgL_locz00_43)
	{
		{	/* Cnst/alloc.scm 323 */
			{

				{	/* Cnst/alloc.scm 354 */
					obj_t BgL_oldz00_1751;
					obj_t BgL_symbolzd2aszd2stringz00_1752;

					BgL_oldz00_1751 =
						BGl_hashtablezd2getzd2zz__hashz00
						(BGl_za2symbolzd2envza2zd2zzcnst_allocz00, BgL_symbolz00_42);
					{	/* Cnst/alloc.scm 355 */
						obj_t BgL_arg1455z00_2854;

						BgL_arg1455z00_2854 = SYMBOL_TO_STRING(BgL_symbolz00_42);
						BgL_symbolzd2aszd2stringz00_1752 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_2854);
					}
					{	/* Cnst/alloc.scm 357 */
						bool_t BgL_test2481z00_4032;

						if (STRUCTP(BgL_oldz00_1751))
							{	/* Cnst/alloc.scm 69 */
								BgL_test2481z00_4032 =
									(STRUCT_KEY(BgL_oldz00_1751) == CNST_TABLE_REF(9));
							}
						else
							{	/* Cnst/alloc.scm 69 */
								BgL_test2481z00_4032 = ((bool_t) 0);
							}
						if (BgL_test2481z00_4032)
							{	/* Cnst/alloc.scm 359 */
								bool_t BgL_test2483z00_4038;

								{	/* Cnst/alloc.scm 359 */
									obj_t BgL_arg1754z00_1763;

									BgL_arg1754z00_1763 = STRUCT_REF(BgL_oldz00_1751, (int) (1L));
									{	/* Cnst/alloc.scm 359 */
										obj_t BgL_classz00_2860;

										BgL_classz00_2860 = BGl_variablez00zzast_varz00;
										if (BGL_OBJECTP(BgL_arg1754z00_1763))
											{	/* Cnst/alloc.scm 359 */
												BgL_objectz00_bglt BgL_arg1807z00_2862;

												BgL_arg1807z00_2862 =
													(BgL_objectz00_bglt) (BgL_arg1754z00_1763);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Cnst/alloc.scm 359 */
														long BgL_idxz00_2868;

														BgL_idxz00_2868 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2862);
														BgL_test2483z00_4038 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2868 + 1L)) == BgL_classz00_2860);
													}
												else
													{	/* Cnst/alloc.scm 359 */
														bool_t BgL_res2406z00_2893;

														{	/* Cnst/alloc.scm 359 */
															obj_t BgL_oclassz00_2876;

															{	/* Cnst/alloc.scm 359 */
																obj_t BgL_arg1815z00_2884;
																long BgL_arg1816z00_2885;

																BgL_arg1815z00_2884 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Cnst/alloc.scm 359 */
																	long BgL_arg1817z00_2886;

																	BgL_arg1817z00_2886 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2862);
																	BgL_arg1816z00_2885 =
																		(BgL_arg1817z00_2886 - OBJECT_TYPE);
																}
																BgL_oclassz00_2876 =
																	VECTOR_REF(BgL_arg1815z00_2884,
																	BgL_arg1816z00_2885);
															}
															{	/* Cnst/alloc.scm 359 */
																bool_t BgL__ortest_1115z00_2877;

																BgL__ortest_1115z00_2877 =
																	(BgL_classz00_2860 == BgL_oclassz00_2876);
																if (BgL__ortest_1115z00_2877)
																	{	/* Cnst/alloc.scm 359 */
																		BgL_res2406z00_2893 =
																			BgL__ortest_1115z00_2877;
																	}
																else
																	{	/* Cnst/alloc.scm 359 */
																		long BgL_odepthz00_2878;

																		{	/* Cnst/alloc.scm 359 */
																			obj_t BgL_arg1804z00_2879;

																			BgL_arg1804z00_2879 =
																				(BgL_oclassz00_2876);
																			BgL_odepthz00_2878 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2879);
																		}
																		if ((1L < BgL_odepthz00_2878))
																			{	/* Cnst/alloc.scm 359 */
																				obj_t BgL_arg1802z00_2881;

																				{	/* Cnst/alloc.scm 359 */
																					obj_t BgL_arg1803z00_2882;

																					BgL_arg1803z00_2882 =
																						(BgL_oclassz00_2876);
																					BgL_arg1802z00_2881 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2882, 1L);
																				}
																				BgL_res2406z00_2893 =
																					(BgL_arg1802z00_2881 ==
																					BgL_classz00_2860);
																			}
																		else
																			{	/* Cnst/alloc.scm 359 */
																				BgL_res2406z00_2893 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2483z00_4038 = BgL_res2406z00_2893;
													}
											}
										else
											{	/* Cnst/alloc.scm 359 */
												BgL_test2483z00_4038 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test2483z00_4038)
									{	/* Cnst/alloc.scm 360 */
										BgL_refz00_bglt BgL_new1139z00_1756;

										{	/* Cnst/alloc.scm 361 */
											BgL_refz00_bglt BgL_new1138z00_1757;

											BgL_new1138z00_1757 =
												((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_refz00_bgl))));
											{	/* Cnst/alloc.scm 361 */
												long BgL_arg1751z00_1758;

												BgL_arg1751z00_1758 =
													BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1138z00_1757),
													BgL_arg1751z00_1758);
											}
											{	/* Cnst/alloc.scm 361 */
												BgL_objectz00_bglt BgL_tmpz00_4067;

												BgL_tmpz00_4067 =
													((BgL_objectz00_bglt) BgL_new1138z00_1757);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4067, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1138z00_1757);
											BgL_new1139z00_1756 = BgL_new1138z00_1757;
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1139z00_1756)))->
												BgL_locz00) = ((obj_t) BgL_locz00_43), BUNSPEC);
										((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
															BgL_new1139z00_1756)))->BgL_typez00) =
											((BgL_typez00_bglt) ((BgL_typez00_bglt)
													BGl_za2symbolza2z00zztype_cachez00)), BUNSPEC);
										((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
															BgL_new1139z00_1756)))->BgL_variablez00) =
											((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
													STRUCT_REF(BgL_oldz00_1751, (int) (1L)))), BUNSPEC);
										return ((BgL_nodez00_bglt) BgL_new1139z00_1756);
									}
								else
									{	/* Cnst/alloc.scm 359 */
										if (
											(BGl_za2initzd2modeza2zd2zzengine_paramz00 ==
												CNST_TABLE_REF(14)))
											{	/* Cnst/alloc.scm 365 */
												BgL_refz00_bglt BgL_new1141z00_1759;

												{	/* Cnst/alloc.scm 366 */
													BgL_refz00_bglt BgL_new1140z00_1760;

													BgL_new1140z00_1760 =
														((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_refz00_bgl))));
													{	/* Cnst/alloc.scm 366 */
														long BgL_arg1752z00_1761;

														BgL_arg1752z00_1761 =
															BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1140z00_1760),
															BgL_arg1752z00_1761);
													}
													{	/* Cnst/alloc.scm 366 */
														BgL_objectz00_bglt BgL_tmpz00_4089;

														BgL_tmpz00_4089 =
															((BgL_objectz00_bglt) BgL_new1140z00_1760);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4089, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1140z00_1760);
													BgL_new1141z00_1759 = BgL_new1140z00_1760;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1141z00_1759)))->
														BgL_locz00) = ((obj_t) BgL_locz00_43), BUNSPEC);
												((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																	BgL_new1141z00_1759)))->BgL_typez00) =
													((BgL_typez00_bglt) ((BgL_typez00_bglt)
															BGl_za2symbolza2z00zztype_cachez00)), BUNSPEC);
												((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																	BgL_new1141z00_1759)))->BgL_variablez00) =
													((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
															STRUCT_REF(BgL_oldz00_1751, (int) (1L)))),
													BUNSPEC);
												return ((BgL_nodez00_bglt) BgL_new1141z00_1759);
											}
										else
											{	/* Cnst/alloc.scm 370 */
												obj_t BgL_arg1753z00_1762;

												BgL_arg1753z00_1762 =
													STRUCT_REF(BgL_oldz00_1751, (int) (1L));
												return
													((BgL_nodez00_bglt)
													BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00
													(BgL_arg1753z00_1762,
														BGl_za2symbolza2z00zztype_cachez00, BgL_locz00_43));
											}
									}
							}
						else
							{	/* Cnst/alloc.scm 357 */
								if (CBOOL(BgL_oldz00_1751))
									{	/* Cnst/alloc.scm 374 */
										obj_t BgL_arg1755z00_1764;

										BgL_arg1755z00_1764 =
											BGl_shapez00zztools_shapez00(BgL_oldz00_1751);
										return
											((BgL_nodez00_bglt)
											BGl_internalzd2errorzd2zztools_errorz00(CNST_TABLE_REF
												(17), BGl_string2430z00zzcnst_allocz00,
												BgL_arg1755z00_1764));
									}
								else
									{	/* Cnst/alloc.scm 375 */
										bool_t BgL_test2490z00_4114;

										if (
											(BGl_za2initzd2modeza2zd2zzengine_paramz00 ==
												CNST_TABLE_REF(14)))
											{	/* Cnst/alloc.scm 375 */
												BgL_test2490z00_4114 = ((bool_t) 1);
											}
										else
											{	/* Cnst/alloc.scm 375 */
												if (
													(STRING_LENGTH(BgL_symbolzd2aszd2stringz00_1752) ==
														0L))
													{	/* Cnst/alloc.scm 376 */
														BgL_test2490z00_4114 = ((bool_t) 1);
													}
												else
													{	/* Cnst/alloc.scm 376 */
														BgL_test2490z00_4114 =
															(STRING_REF(BgL_symbolzd2aszd2stringz00_1752,
																0L) == ((unsigned char) ';'));
											}}
										if (BgL_test2490z00_4114)
											{
												BgL_refz00_bglt BgL_auxz00_4123;

												{	/* Cnst/alloc.scm 325 */
													BgL_globalz00_bglt BgL_varz00_1774;
													BgL_nodez00_bglt BgL_vsz00_1775;

													{	/* Cnst/alloc.scm 325 */
														obj_t BgL_arg1836z00_1798;

														{	/* Cnst/alloc.scm 325 */
															obj_t BgL_arg1837z00_1799;

															BgL_arg1837z00_1799 =
																BGl_gensymz00zz__r4_symbols_6_4z00
																(CNST_TABLE_REF(15));
															BgL_arg1836z00_1798 =
																BGl_makezd2typedzd2identz00zzast_identz00
																(BgL_arg1837z00_1799, CNST_TABLE_REF(15));
														}
														BgL_varz00_1774 =
															BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2
															(BgL_arg1836z00_1798,
															BGl_za2moduleza2z00zzmodule_modulez00,
															CNST_TABLE_REF(16), CNST_TABLE_REF(3));
													}
													{	/* Cnst/alloc.scm 329 */
														obj_t BgL_arg1838z00_1800;

														{	/* Cnst/alloc.scm 329 */
															obj_t BgL_arg1455z00_2829;

															BgL_arg1455z00_2829 =
																SYMBOL_TO_STRING(BgL_symbolz00_42);
															BgL_arg1838z00_1800 =
																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																(BgL_arg1455z00_2829);
														}
														BgL_vsz00_1775 =
															BGl_cnstzd2alloczd2stringz00zzcnst_allocz00
															(BgL_arg1838z00_1800, BgL_locz00_43);
													}
													{	/* Cnst/alloc.scm 330 */
														obj_t BgL_arg1773z00_1776;

														{	/* Cnst/alloc.scm 69 */
															obj_t BgL_newz00_2830;

															BgL_newz00_2830 =
																create_struct(CNST_TABLE_REF(9), (int) (2L));
															{	/* Cnst/alloc.scm 69 */
																obj_t BgL_auxz00_4139;
																int BgL_tmpz00_4137;

																BgL_auxz00_4139 = ((obj_t) BgL_varz00_1774);
																BgL_tmpz00_4137 = (int) (1L);
																STRUCT_SET(BgL_newz00_2830, BgL_tmpz00_4137,
																	BgL_auxz00_4139);
															}
															{	/* Cnst/alloc.scm 69 */
																int BgL_tmpz00_4142;

																BgL_tmpz00_4142 = (int) (0L);
																STRUCT_SET(BgL_newz00_2830, BgL_tmpz00_4142,
																	BgL_symbolz00_42);
															}
															BgL_arg1773z00_1776 = BgL_newz00_2830;
														}
														BGl_hashtablezd2putz12zc0zz__hashz00
															(BGl_za2symbolzd2envza2zd2zzcnst_allocz00,
															BgL_symbolz00_42, BgL_arg1773z00_1776);
													}
													{	/* Cnst/alloc.scm 331 */
														obj_t BgL_arg1775z00_1777;

														{	/* Cnst/alloc.scm 331 */
															obj_t BgL_arg1798z00_1778;

															{	/* Cnst/alloc.scm 331 */
																obj_t BgL_arg1799z00_1779;
																obj_t BgL_arg1805z00_1780;

																{	/* Cnst/alloc.scm 331 */
																	obj_t BgL_arg1806z00_1781;

																	{	/* Cnst/alloc.scm 331 */
																		obj_t BgL_arg1808z00_1782;
																		obj_t BgL_arg1812z00_1783;

																		BgL_arg1808z00_1782 =
																			(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						BgL_varz00_1774)))->BgL_idz00);
																		BgL_arg1812z00_1783 =
																			MAKE_YOUNG_PAIR((((BgL_globalz00_bglt)
																					COBJECT(BgL_varz00_1774))->
																				BgL_modulez00), BNIL);
																		BgL_arg1806z00_1781 =
																			MAKE_YOUNG_PAIR(BgL_arg1808z00_1782,
																			BgL_arg1812z00_1783);
																	}
																	BgL_arg1799z00_1779 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																		BgL_arg1806z00_1781);
																}
																{	/* Cnst/alloc.scm 333 */
																	BgL_nodez00_bglt BgL_arg1822z00_1785;

																	{	/* Cnst/alloc.scm 333 */
																		BgL_appz00_bglt BgL_arg1823z00_1786;
																		BgL_typez00_bglt BgL_arg1831z00_1787;

																		{	/* Cnst/alloc.scm 333 */
																			BgL_appz00_bglt BgL_new1133z00_1788;

																			{	/* Cnst/alloc.scm 340 */
																				BgL_appz00_bglt BgL_new1131z00_1793;

																				BgL_new1131z00_1793 =
																					((BgL_appz00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_appz00_bgl))));
																				{	/* Cnst/alloc.scm 340 */
																					long BgL_arg1834z00_1794;

																					BgL_arg1834z00_1794 =
																						BGL_CLASS_NUM
																						(BGl_appz00zzast_nodez00);
																					BGL_OBJECT_CLASS_NUM_SET((
																							(BgL_objectz00_bglt)
																							BgL_new1131z00_1793),
																						BgL_arg1834z00_1794);
																				}
																				{	/* Cnst/alloc.scm 340 */
																					BgL_objectz00_bglt BgL_tmpz00_4157;

																					BgL_tmpz00_4157 =
																						((BgL_objectz00_bglt)
																						BgL_new1131z00_1793);
																					BGL_OBJECT_WIDENING_SET
																						(BgL_tmpz00_4157, BFALSE);
																				}
																				((BgL_objectz00_bglt)
																					BgL_new1131z00_1793);
																				BgL_new1133z00_1788 =
																					BgL_new1131z00_1793;
																			}
																			((((BgL_nodez00_bglt) COBJECT(
																							((BgL_nodez00_bglt)
																								BgL_new1133z00_1788)))->
																					BgL_locz00) =
																				((obj_t) BgL_locz00_43), BUNSPEC);
																			((((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_new1133z00_1788)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) ((BgL_typez00_bglt)
																						BGl_za2symbolza2z00zztype_cachez00)),
																				BUNSPEC);
																			((((BgL_nodezf2effectzf2_bglt)
																						COBJECT(((BgL_nodezf2effectzf2_bglt)
																								BgL_new1133z00_1788)))->
																					BgL_sidezd2effectzd2) =
																				((obj_t) BUNSPEC), BUNSPEC);
																			((((BgL_nodezf2effectzf2_bglt)
																						COBJECT(((BgL_nodezf2effectzf2_bglt)
																								BgL_new1133z00_1788)))->
																					BgL_keyz00) =
																				((obj_t) BINT(-1L)), BUNSPEC);
																			{
																				BgL_varz00_bglt BgL_auxz00_4171;

																				{	/* Cnst/alloc.scm 336 */
																					BgL_refz00_bglt BgL_new1135z00_1789;

																					{	/* Cnst/alloc.scm 339 */
																						BgL_refz00_bglt BgL_new1134z00_1790;

																						BgL_new1134z00_1790 =
																							((BgL_refz00_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_refz00_bgl))));
																						{	/* Cnst/alloc.scm 339 */
																							long BgL_arg1832z00_1791;

																							{	/* Cnst/alloc.scm 339 */
																								obj_t BgL_classz00_2839;

																								BgL_classz00_2839 =
																									BGl_refz00zzast_nodez00;
																								BgL_arg1832z00_1791 =
																									BGL_CLASS_NUM
																									(BgL_classz00_2839);
																							}
																							BGL_OBJECT_CLASS_NUM_SET(
																								((BgL_objectz00_bglt)
																									BgL_new1134z00_1790),
																								BgL_arg1832z00_1791);
																						}
																						{	/* Cnst/alloc.scm 339 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_4176;
																							BgL_tmpz00_4176 =
																								((BgL_objectz00_bglt)
																								BgL_new1134z00_1790);
																							BGL_OBJECT_WIDENING_SET
																								(BgL_tmpz00_4176, BFALSE);
																						}
																						((BgL_objectz00_bglt)
																							BgL_new1134z00_1790);
																						BgL_new1135z00_1789 =
																							BgL_new1134z00_1790;
																					}
																					((((BgL_nodez00_bglt) COBJECT(
																									((BgL_nodez00_bglt)
																										BgL_new1135z00_1789)))->
																							BgL_locz00) =
																						((obj_t) BgL_locz00_43), BUNSPEC);
																					((((BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt)
																										BgL_new1135z00_1789)))->
																							BgL_typez00) =
																						((BgL_typez00_bglt) (
																								(BgL_typez00_bglt)
																								BGl_za2symbolza2z00zztype_cachez00)),
																						BUNSPEC);
																					((((BgL_varz00_bglt)
																								COBJECT(((BgL_varz00_bglt)
																										BgL_new1135z00_1789)))->
																							BgL_variablez00) =
																						((BgL_variablez00_bglt) (
																								(BgL_variablez00_bglt)
																								BGl_za2bstringzd2ze3symbolza2z31zzcnst_cachez00)),
																						BUNSPEC);
																					BgL_auxz00_4171 =
																						((BgL_varz00_bglt)
																						BgL_new1135z00_1789);
																				}
																				((((BgL_appz00_bglt)
																							COBJECT(BgL_new1133z00_1788))->
																						BgL_funz00) =
																					((BgL_varz00_bglt) BgL_auxz00_4171),
																					BUNSPEC);
																			}
																			{
																				obj_t BgL_auxz00_4190;

																				{	/* Cnst/alloc.scm 340 */
																					obj_t BgL_list1833z00_1792;

																					BgL_list1833z00_1792 =
																						MAKE_YOUNG_PAIR(
																						((obj_t) BgL_vsz00_1775), BNIL);
																					BgL_auxz00_4190 =
																						BgL_list1833z00_1792;
																				}
																				((((BgL_appz00_bglt)
																							COBJECT(BgL_new1133z00_1788))->
																						BgL_argsz00) =
																					((obj_t) BgL_auxz00_4190), BUNSPEC);
																			}
																			((((BgL_appz00_bglt)
																						COBJECT(BgL_new1133z00_1788))->
																					BgL_stackablez00) =
																				((obj_t) BFALSE), BUNSPEC);
																			BgL_arg1823z00_1786 = BgL_new1133z00_1788;
																		}
																		BgL_arg1831z00_1787 =
																			BGl_strictzd2nodezd2typez00zzast_nodez00(
																			((BgL_typez00_bglt)
																				BGl_za2symbolza2z00zztype_cachez00),
																			((BgL_typez00_bglt)
																				BGl_za2objza2z00zztype_cachez00));
																		BgL_arg1822z00_1785 =
																			BGl_coercez12z12zzcoerce_coercez00((
																				(BgL_nodez00_bglt) BgL_arg1823z00_1786),
																			((obj_t) BgL_varz00_1774),
																			BgL_arg1831z00_1787, ((bool_t) 0));
																	}
																	BgL_arg1805z00_1780 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_arg1822z00_1785), BNIL);
																}
																BgL_arg1798z00_1778 =
																	MAKE_YOUNG_PAIR(BgL_arg1799z00_1779,
																	BgL_arg1805z00_1780);
															}
															BgL_arg1775z00_1777 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
																BgL_arg1798z00_1778);
														}
														BGl_za2globalzd2sexpza2zd2zzcnst_allocz00 =
															MAKE_YOUNG_PAIR(BgL_arg1775z00_1777,
															BGl_za2globalzd2sexpza2zd2zzcnst_allocz00);
													}
													{	/* Cnst/alloc.scm 344 */
														BgL_refz00_bglt BgL_new1137z00_1795;

														{	/* Cnst/alloc.scm 345 */
															BgL_refz00_bglt BgL_new1136z00_1796;

															BgL_new1136z00_1796 =
																((BgL_refz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_refz00_bgl))));
															{	/* Cnst/alloc.scm 345 */
																long BgL_arg1835z00_1797;

																BgL_arg1835z00_1797 =
																	BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1136z00_1796),
																	BgL_arg1835z00_1797);
															}
															{	/* Cnst/alloc.scm 345 */
																BgL_objectz00_bglt BgL_tmpz00_4211;

																BgL_tmpz00_4211 =
																	((BgL_objectz00_bglt) BgL_new1136z00_1796);
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4211,
																	BFALSE);
															}
															((BgL_objectz00_bglt) BgL_new1136z00_1796);
															BgL_new1137z00_1795 = BgL_new1136z00_1796;
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1137z00_1795)))->
																BgL_locz00) = ((obj_t) BgL_locz00_43), BUNSPEC);
														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																			BgL_new1137z00_1795)))->BgL_typez00) =
															((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																		COBJECT(((BgL_variablez00_bglt)
																				BgL_varz00_1774)))->BgL_typez00)),
															BUNSPEC);
														((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																			BgL_new1137z00_1795)))->BgL_variablez00) =
															((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																	BgL_varz00_1774)), BUNSPEC);
														BgL_auxz00_4123 = BgL_new1137z00_1795;
												}}
												return ((BgL_nodez00_bglt) BgL_auxz00_4123);
											}
										else
											{
												BgL_appz00_bglt BgL_auxz00_4225;

												{	/* Cnst/alloc.scm 349 */
													long BgL_offsetz00_1802;

													BgL_offsetz00_1802 =
														BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00;
													BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00 =
														(1L + BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00);
													BGl_za2globalzd2setza2zd2zzcnst_allocz00 =
														MAKE_YOUNG_PAIR(BgL_symbolz00_42,
														BGl_za2globalzd2setza2zd2zzcnst_allocz00);
													{	/* Cnst/alloc.scm 352 */
														obj_t BgL_arg1840z00_1803;

														{	/* Cnst/alloc.scm 69 */
															obj_t BgL_newz00_2850;

															BgL_newz00_2850 =
																create_struct(CNST_TABLE_REF(9), (int) (2L));
															{	/* Cnst/alloc.scm 69 */
																obj_t BgL_auxz00_4233;
																int BgL_tmpz00_4231;

																BgL_auxz00_4233 = BINT(BgL_offsetz00_1802);
																BgL_tmpz00_4231 = (int) (1L);
																STRUCT_SET(BgL_newz00_2850, BgL_tmpz00_4231,
																	BgL_auxz00_4233);
															}
															{	/* Cnst/alloc.scm 69 */
																int BgL_tmpz00_4236;

																BgL_tmpz00_4236 = (int) (0L);
																STRUCT_SET(BgL_newz00_2850, BgL_tmpz00_4236,
																	BgL_symbolz00_42);
															}
															BgL_arg1840z00_1803 = BgL_newz00_2850;
														}
														BGl_hashtablezd2putz12zc0zz__hashz00
															(BGl_za2symbolzd2envza2zd2zzcnst_allocz00,
															BgL_symbolz00_42, BgL_arg1840z00_1803);
													}
													BgL_auxz00_4225 =
														BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00(BINT
														(BgL_offsetz00_1802),
														BGl_za2symbolza2z00zztype_cachez00, BgL_locz00_43);
												}
												return ((BgL_nodez00_bglt) BgL_auxz00_4225);
											}
									}
							}
					}
				}
			}
		}

	}



/* &cnst-alloc-symbol */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2symbolz62zzcnst_allocz00(obj_t
		BgL_envz00_3503, obj_t BgL_symbolz00_3504, obj_t BgL_locz00_3505)
	{
		{	/* Cnst/alloc.scm 323 */
			return
				BGl_cnstzd2alloczd2symbolz00zzcnst_allocz00(BgL_symbolz00_3504,
				BgL_locz00_3505);
		}

	}



/* cnst-alloc-keyword */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2keywordz00zzcnst_allocz00(obj_t BgL_keywordz00_44,
		obj_t BgL_locz00_45)
	{
		{	/* Cnst/alloc.scm 385 */
			{

				{	/* Cnst/alloc.scm 417 */
					obj_t BgL_oldz00_1808;
					obj_t BgL_keywordzd2aszd2stringz00_1809;

					BgL_oldz00_1808 =
						BGl_hashtablezd2getzd2zz__hashz00
						(BGl_za2keywordzd2envza2zd2zzcnst_allocz00, BgL_keywordz00_44);
					{	/* Cnst/alloc.scm 418 */
						obj_t BgL_arg1452z00_2936;

						BgL_arg1452z00_2936 = KEYWORD_TO_STRING(BgL_keywordz00_44);
						BgL_keywordzd2aszd2stringz00_1809 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1452z00_2936);
					}
					if (CBOOL(BgL_oldz00_1808))
						{	/* Cnst/alloc.scm 420 */
							if (
								(BGl_za2initzd2modeza2zd2zzengine_paramz00 ==
									CNST_TABLE_REF(14)))
								{	/* Cnst/alloc.scm 422 */
									BgL_refz00_bglt BgL_new1149z00_1810;

									{	/* Cnst/alloc.scm 423 */
										BgL_refz00_bglt BgL_new1148z00_1811;

										BgL_new1148z00_1811 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Cnst/alloc.scm 423 */
											long BgL_arg1842z00_1812;

											BgL_arg1842z00_1812 =
												BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1148z00_1811),
												BgL_arg1842z00_1812);
										}
										{	/* Cnst/alloc.scm 423 */
											BgL_objectz00_bglt BgL_tmpz00_4256;

											BgL_tmpz00_4256 =
												((BgL_objectz00_bglt) BgL_new1148z00_1811);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4256, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1148z00_1811);
										BgL_new1149z00_1810 = BgL_new1148z00_1811;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1149z00_1810)))->
											BgL_locz00) = ((obj_t) BgL_locz00_45), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1149z00_1810)))->BgL_typez00) =
										((BgL_typez00_bglt) ((BgL_typez00_bglt)
												BGl_za2keywordza2z00zztype_cachez00)), BUNSPEC);
									((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
														BgL_new1149z00_1810)))->BgL_variablez00) =
										((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
												STRUCT_REF(((obj_t) BgL_oldz00_1808), (int) (1L)))),
										BUNSPEC);
									return ((BgL_nodez00_bglt) BgL_new1149z00_1810);
								}
							else
								{	/* Cnst/alloc.scm 426 */
									obj_t BgL_arg1843z00_1813;

									BgL_arg1843z00_1813 =
										STRUCT_REF(((obj_t) BgL_oldz00_1808), (int) (1L));
									return
										((BgL_nodez00_bglt)
										BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00
										(BgL_arg1843z00_1813, BGl_za2keywordza2z00zztype_cachez00,
											BgL_locz00_45));
								}
						}
					else
						{	/* Cnst/alloc.scm 427 */
							bool_t BgL_test2495z00_4277;

							if (
								(BGl_za2initzd2modeza2zd2zzengine_paramz00 ==
									CNST_TABLE_REF(14)))
								{	/* Cnst/alloc.scm 427 */
									BgL_test2495z00_4277 = ((bool_t) 1);
								}
							else
								{	/* Cnst/alloc.scm 427 */
									if ((STRING_LENGTH(BgL_keywordzd2aszd2stringz00_1809) == 0L))
										{	/* Cnst/alloc.scm 428 */
											BgL_test2495z00_4277 = ((bool_t) 1);
										}
									else
										{	/* Cnst/alloc.scm 428 */
											BgL_test2495z00_4277 =
												(STRING_REF(BgL_keywordzd2aszd2stringz00_1809,
													0L) == ((unsigned char) ';'));
								}}
							if (BgL_test2495z00_4277)
								{
									BgL_refz00_bglt BgL_auxz00_4286;

									{	/* Cnst/alloc.scm 387 */
										BgL_globalz00_bglt BgL_varz00_1823;
										BgL_nodez00_bglt BgL_vsz00_1824;

										{	/* Cnst/alloc.scm 387 */
											obj_t BgL_arg1873z00_1847;

											{	/* Cnst/alloc.scm 387 */
												obj_t BgL_arg1874z00_1848;

												BgL_arg1874z00_1848 =
													BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF
													(18));
												BgL_arg1873z00_1847 =
													BGl_makezd2typedzd2identz00zzast_identz00
													(BgL_arg1874z00_1848, CNST_TABLE_REF(18));
											}
											BgL_varz00_1823 =
												BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2
												(BgL_arg1873z00_1847,
												BGl_za2moduleza2z00zzmodule_modulez00,
												CNST_TABLE_REF(19), CNST_TABLE_REF(3));
										}
										{	/* Cnst/alloc.scm 392 */
											obj_t BgL_arg1875z00_1849;

											{	/* Cnst/alloc.scm 392 */
												obj_t BgL_arg1452z00_2911;

												BgL_arg1452z00_2911 =
													KEYWORD_TO_STRING(BgL_keywordz00_44);
												BgL_arg1875z00_1849 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1452z00_2911);
											}
											BgL_vsz00_1824 =
												BGl_cnstzd2alloczd2stringz00zzcnst_allocz00
												(BgL_arg1875z00_1849, BgL_locz00_45);
										}
										{	/* Cnst/alloc.scm 393 */
											obj_t BgL_arg1852z00_1825;

											{	/* Cnst/alloc.scm 69 */
												obj_t BgL_newz00_2912;

												BgL_newz00_2912 =
													create_struct(CNST_TABLE_REF(9), (int) (2L));
												{	/* Cnst/alloc.scm 69 */
													obj_t BgL_auxz00_4302;
													int BgL_tmpz00_4300;

													BgL_auxz00_4302 = ((obj_t) BgL_varz00_1823);
													BgL_tmpz00_4300 = (int) (1L);
													STRUCT_SET(BgL_newz00_2912, BgL_tmpz00_4300,
														BgL_auxz00_4302);
												}
												{	/* Cnst/alloc.scm 69 */
													int BgL_tmpz00_4305;

													BgL_tmpz00_4305 = (int) (0L);
													STRUCT_SET(BgL_newz00_2912, BgL_tmpz00_4305,
														BgL_keywordz00_44);
												}
												BgL_arg1852z00_1825 = BgL_newz00_2912;
											}
											BGl_hashtablezd2putz12zc0zz__hashz00
												(BGl_za2keywordzd2envza2zd2zzcnst_allocz00,
												BgL_keywordz00_44, BgL_arg1852z00_1825);
										}
										{	/* Cnst/alloc.scm 394 */
											obj_t BgL_arg1853z00_1826;

											{	/* Cnst/alloc.scm 394 */
												obj_t BgL_arg1854z00_1827;

												{	/* Cnst/alloc.scm 394 */
													obj_t BgL_arg1856z00_1828;
													obj_t BgL_arg1857z00_1829;

													{	/* Cnst/alloc.scm 394 */
														obj_t BgL_arg1858z00_1830;

														{	/* Cnst/alloc.scm 394 */
															obj_t BgL_arg1859z00_1831;
															obj_t BgL_arg1860z00_1832;

															BgL_arg1859z00_1831 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt) BgL_varz00_1823)))->
																BgL_idz00);
															BgL_arg1860z00_1832 =
																MAKE_YOUNG_PAIR((((BgL_globalz00_bglt)
																		COBJECT(BgL_varz00_1823))->BgL_modulez00),
																BNIL);
															BgL_arg1858z00_1830 =
																MAKE_YOUNG_PAIR(BgL_arg1859z00_1831,
																BgL_arg1860z00_1832);
														}
														BgL_arg1856z00_1828 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
															BgL_arg1858z00_1830);
													}
													{	/* Cnst/alloc.scm 396 */
														BgL_nodez00_bglt BgL_arg1863z00_1834;

														{	/* Cnst/alloc.scm 396 */
															BgL_appz00_bglt BgL_arg1864z00_1835;
															BgL_typez00_bglt BgL_arg1866z00_1836;

															{	/* Cnst/alloc.scm 396 */
																BgL_appz00_bglt BgL_new1143z00_1837;

																{	/* Cnst/alloc.scm 403 */
																	BgL_appz00_bglt BgL_new1142z00_1842;

																	BgL_new1142z00_1842 =
																		((BgL_appz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_appz00_bgl))));
																	{	/* Cnst/alloc.scm 403 */
																		long BgL_arg1870z00_1843;

																		BgL_arg1870z00_1843 =
																			BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1142z00_1842),
																			BgL_arg1870z00_1843);
																	}
																	{	/* Cnst/alloc.scm 403 */
																		BgL_objectz00_bglt BgL_tmpz00_4320;

																		BgL_tmpz00_4320 =
																			((BgL_objectz00_bglt)
																			BgL_new1142z00_1842);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4320,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1142z00_1842);
																	BgL_new1143z00_1837 = BgL_new1142z00_1842;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1143z00_1837)))->BgL_locz00) =
																	((obj_t) BgL_locz00_45), BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1143z00_1837)))->BgL_typez00) =
																	((BgL_typez00_bglt) ((BgL_typez00_bglt)
																			BGl_za2keywordza2z00zztype_cachez00)),
																	BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1143z00_1837)))->
																		BgL_sidezd2effectzd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1143z00_1837)))->BgL_keyz00) =
																	((obj_t) BINT(-1L)), BUNSPEC);
																{
																	BgL_varz00_bglt BgL_auxz00_4334;

																	{	/* Cnst/alloc.scm 399 */
																		BgL_refz00_bglt BgL_new1145z00_1838;

																		{	/* Cnst/alloc.scm 402 */
																			BgL_refz00_bglt BgL_new1144z00_1839;

																			BgL_new1144z00_1839 =
																				((BgL_refz00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_refz00_bgl))));
																			{	/* Cnst/alloc.scm 402 */
																				long BgL_arg1868z00_1840;

																				{	/* Cnst/alloc.scm 402 */
																					obj_t BgL_classz00_2921;

																					BgL_classz00_2921 =
																						BGl_refz00zzast_nodez00;
																					BgL_arg1868z00_1840 =
																						BGL_CLASS_NUM(BgL_classz00_2921);
																				}
																				BGL_OBJECT_CLASS_NUM_SET(
																					((BgL_objectz00_bglt)
																						BgL_new1144z00_1839),
																					BgL_arg1868z00_1840);
																			}
																			{	/* Cnst/alloc.scm 402 */
																				BgL_objectz00_bglt BgL_tmpz00_4339;

																				BgL_tmpz00_4339 =
																					((BgL_objectz00_bglt)
																					BgL_new1144z00_1839);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4339,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1144z00_1839);
																			BgL_new1145z00_1838 = BgL_new1144z00_1839;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1145z00_1838)))->
																				BgL_locz00) =
																			((obj_t) BgL_locz00_45), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1145z00_1838)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) ((BgL_typez00_bglt)
																					BGl_za2keywordza2z00zztype_cachez00)),
																			BUNSPEC);
																		((((BgL_varz00_bglt)
																					COBJECT(((BgL_varz00_bglt)
																							BgL_new1145z00_1838)))->
																				BgL_variablez00) =
																			((BgL_variablez00_bglt) (
																					(BgL_variablez00_bglt)
																					BGl_za2bstringzd2ze3keywordza2z31zzcnst_cachez00)),
																			BUNSPEC);
																		BgL_auxz00_4334 =
																			((BgL_varz00_bglt) BgL_new1145z00_1838);
																	}
																	((((BgL_appz00_bglt)
																				COBJECT(BgL_new1143z00_1837))->
																			BgL_funz00) =
																		((BgL_varz00_bglt) BgL_auxz00_4334),
																		BUNSPEC);
																}
																{
																	obj_t BgL_auxz00_4353;

																	{	/* Cnst/alloc.scm 403 */
																		obj_t BgL_list1869z00_1841;

																		BgL_list1869z00_1841 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_vsz00_1824), BNIL);
																		BgL_auxz00_4353 = BgL_list1869z00_1841;
																	}
																	((((BgL_appz00_bglt)
																				COBJECT(BgL_new1143z00_1837))->
																			BgL_argsz00) =
																		((obj_t) BgL_auxz00_4353), BUNSPEC);
																}
																((((BgL_appz00_bglt)
																			COBJECT(BgL_new1143z00_1837))->
																		BgL_stackablez00) =
																	((obj_t) BFALSE), BUNSPEC);
																BgL_arg1864z00_1835 = BgL_new1143z00_1837;
															}
															BgL_arg1866z00_1836 =
																BGl_strictzd2nodezd2typez00zzast_nodez00(
																((BgL_typez00_bglt)
																	BGl_za2keywordza2z00zztype_cachez00),
																((BgL_typez00_bglt)
																	BGl_za2objza2z00zztype_cachez00));
															BgL_arg1863z00_1834 =
																BGl_coercez12z12zzcoerce_coercez00((
																	(BgL_nodez00_bglt) BgL_arg1864z00_1835),
																((obj_t) BgL_varz00_1823), BgL_arg1866z00_1836,
																((bool_t) 0));
														}
														BgL_arg1857z00_1829 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg1863z00_1834), BNIL);
													}
													BgL_arg1854z00_1827 =
														MAKE_YOUNG_PAIR(BgL_arg1856z00_1828,
														BgL_arg1857z00_1829);
												}
												BgL_arg1853z00_1826 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
													BgL_arg1854z00_1827);
											}
											BGl_za2globalzd2sexpza2zd2zzcnst_allocz00 =
												MAKE_YOUNG_PAIR(BgL_arg1853z00_1826,
												BGl_za2globalzd2sexpza2zd2zzcnst_allocz00);
										}
										{	/* Cnst/alloc.scm 407 */
											BgL_refz00_bglt BgL_new1147z00_1844;

											{	/* Cnst/alloc.scm 408 */
												BgL_refz00_bglt BgL_new1146z00_1845;

												BgL_new1146z00_1845 =
													((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_refz00_bgl))));
												{	/* Cnst/alloc.scm 408 */
													long BgL_arg1872z00_1846;

													BgL_arg1872z00_1846 =
														BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1146z00_1845),
														BgL_arg1872z00_1846);
												}
												{	/* Cnst/alloc.scm 408 */
													BgL_objectz00_bglt BgL_tmpz00_4374;

													BgL_tmpz00_4374 =
														((BgL_objectz00_bglt) BgL_new1146z00_1845);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4374, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1146z00_1845);
												BgL_new1147z00_1844 = BgL_new1146z00_1845;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1147z00_1844)))->
													BgL_locz00) = ((obj_t) BgL_locz00_45), BUNSPEC);
											((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_new1147z00_1844)))->BgL_typez00) =
												((BgL_typez00_bglt) (((BgL_variablez00_bglt)
															COBJECT(((BgL_variablez00_bglt)
																	BgL_varz00_1823)))->BgL_typez00)), BUNSPEC);
											((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																BgL_new1147z00_1844)))->BgL_variablez00) =
												((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
														BgL_varz00_1823)), BUNSPEC);
											BgL_auxz00_4286 = BgL_new1147z00_1844;
									}}
									return ((BgL_nodez00_bglt) BgL_auxz00_4286);
								}
							else
								{
									BgL_appz00_bglt BgL_auxz00_4388;

									{	/* Cnst/alloc.scm 412 */
										long BgL_offsetz00_1851;

										BgL_offsetz00_1851 =
											BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00;
										BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00 =
											(1L + BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00);
										BGl_za2globalzd2setza2zd2zzcnst_allocz00 =
											MAKE_YOUNG_PAIR(BgL_keywordz00_44,
											BGl_za2globalzd2setza2zd2zzcnst_allocz00);
										{	/* Cnst/alloc.scm 415 */
											obj_t BgL_arg1877z00_1852;

											{	/* Cnst/alloc.scm 69 */
												obj_t BgL_newz00_2932;

												BgL_newz00_2932 =
													create_struct(CNST_TABLE_REF(9), (int) (2L));
												{	/* Cnst/alloc.scm 69 */
													obj_t BgL_auxz00_4396;
													int BgL_tmpz00_4394;

													BgL_auxz00_4396 = BINT(BgL_offsetz00_1851);
													BgL_tmpz00_4394 = (int) (1L);
													STRUCT_SET(BgL_newz00_2932, BgL_tmpz00_4394,
														BgL_auxz00_4396);
												}
												{	/* Cnst/alloc.scm 69 */
													int BgL_tmpz00_4399;

													BgL_tmpz00_4399 = (int) (0L);
													STRUCT_SET(BgL_newz00_2932, BgL_tmpz00_4399,
														BgL_keywordz00_44);
												}
												BgL_arg1877z00_1852 = BgL_newz00_2932;
											}
											BGl_hashtablezd2putz12zc0zz__hashz00
												(BGl_za2keywordzd2envza2zd2zzcnst_allocz00,
												BgL_keywordz00_44, BgL_arg1877z00_1852);
										}
										BgL_auxz00_4388 =
											BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00(BINT
											(BgL_offsetz00_1851), BGl_za2keywordza2z00zztype_cachez00,
											BgL_locz00_45);
									}
									return ((BgL_nodez00_bglt) BgL_auxz00_4388);
								}
						}
				}
			}
		}

	}



/* &cnst-alloc-keyword */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2keywordz62zzcnst_allocz00(obj_t
		BgL_envz00_3506, obj_t BgL_keywordz00_3507, obj_t BgL_locz00_3508)
	{
		{	/* Cnst/alloc.scm 385 */
			return
				BGl_cnstzd2alloczd2keywordz00zzcnst_allocz00(BgL_keywordz00_3507,
				BgL_locz00_3508);
		}

	}



/* cnst-alloc-bignum */
	BGL_EXPORTED_DEF obj_t BGl_cnstzd2alloczd2bignumz00zzcnst_allocz00(obj_t
		BgL_bignumz00_46, obj_t BgL_locz00_47)
	{
		{	/* Cnst/alloc.scm 437 */
			{

				{	/* Cnst/alloc.scm 449 */
					obj_t BgL_oldz00_1857;
					obj_t BgL_bignumzd2aszd2stringz00_1858;

					BgL_oldz00_1857 =
						BGl_assocz00zz__r4_pairs_and_lists_6_3z00(BgL_bignumz00_46,
						BGl_za2bignumzd2envza2zd2zzcnst_allocz00);
					{	/* Cnst/alloc.scm 450 */

						BgL_bignumzd2aszd2stringz00_1858 =
							BGl_bignumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
							(BgL_bignumz00_46, 10L);
					}
					if (PAIRP(BgL_oldz00_1857))
						{	/* Cnst/alloc.scm 454 */
							bool_t BgL_test2499z00_4411;

							{	/* Cnst/alloc.scm 454 */
								obj_t BgL_arg1896z00_1879;

								BgL_arg1896z00_1879 =
									STRUCT_REF(CDR(BgL_oldz00_1857), (int) (1L));
								{	/* Cnst/alloc.scm 454 */
									obj_t BgL_classz00_2954;

									BgL_classz00_2954 = BGl_variablez00zzast_varz00;
									if (BGL_OBJECTP(BgL_arg1896z00_1879))
										{	/* Cnst/alloc.scm 454 */
											BgL_objectz00_bglt BgL_arg1807z00_2956;

											BgL_arg1807z00_2956 =
												(BgL_objectz00_bglt) (BgL_arg1896z00_1879);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Cnst/alloc.scm 454 */
													long BgL_idxz00_2962;

													BgL_idxz00_2962 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2956);
													BgL_test2499z00_4411 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2962 + 1L)) == BgL_classz00_2954);
												}
											else
												{	/* Cnst/alloc.scm 454 */
													bool_t BgL_res2408z00_2987;

													{	/* Cnst/alloc.scm 454 */
														obj_t BgL_oclassz00_2970;

														{	/* Cnst/alloc.scm 454 */
															obj_t BgL_arg1815z00_2978;
															long BgL_arg1816z00_2979;

															BgL_arg1815z00_2978 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Cnst/alloc.scm 454 */
																long BgL_arg1817z00_2980;

																BgL_arg1817z00_2980 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2956);
																BgL_arg1816z00_2979 =
																	(BgL_arg1817z00_2980 - OBJECT_TYPE);
															}
															BgL_oclassz00_2970 =
																VECTOR_REF(BgL_arg1815z00_2978,
																BgL_arg1816z00_2979);
														}
														{	/* Cnst/alloc.scm 454 */
															bool_t BgL__ortest_1115z00_2971;

															BgL__ortest_1115z00_2971 =
																(BgL_classz00_2954 == BgL_oclassz00_2970);
															if (BgL__ortest_1115z00_2971)
																{	/* Cnst/alloc.scm 454 */
																	BgL_res2408z00_2987 =
																		BgL__ortest_1115z00_2971;
																}
															else
																{	/* Cnst/alloc.scm 454 */
																	long BgL_odepthz00_2972;

																	{	/* Cnst/alloc.scm 454 */
																		obj_t BgL_arg1804z00_2973;

																		BgL_arg1804z00_2973 = (BgL_oclassz00_2970);
																		BgL_odepthz00_2972 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2973);
																	}
																	if ((1L < BgL_odepthz00_2972))
																		{	/* Cnst/alloc.scm 454 */
																			obj_t BgL_arg1802z00_2975;

																			{	/* Cnst/alloc.scm 454 */
																				obj_t BgL_arg1803z00_2976;

																				BgL_arg1803z00_2976 =
																					(BgL_oclassz00_2970);
																				BgL_arg1802z00_2975 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2976, 1L);
																			}
																			BgL_res2408z00_2987 =
																				(BgL_arg1802z00_2975 ==
																				BgL_classz00_2954);
																		}
																	else
																		{	/* Cnst/alloc.scm 454 */
																			BgL_res2408z00_2987 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2499z00_4411 = BgL_res2408z00_2987;
												}
										}
									else
										{	/* Cnst/alloc.scm 454 */
											BgL_test2499z00_4411 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test2499z00_4411)
								{	/* Cnst/alloc.scm 455 */
									BgL_refz00_bglt BgL_new1151z00_1863;

									{	/* Cnst/alloc.scm 456 */
										BgL_refz00_bglt BgL_new1150z00_1868;

										BgL_new1150z00_1868 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Cnst/alloc.scm 456 */
											long BgL_arg1887z00_1869;

											BgL_arg1887z00_1869 =
												BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1150z00_1868),
												BgL_arg1887z00_1869);
										}
										{	/* Cnst/alloc.scm 456 */
											BgL_objectz00_bglt BgL_tmpz00_4441;

											BgL_tmpz00_4441 =
												((BgL_objectz00_bglt) BgL_new1150z00_1868);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4441, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1150z00_1868);
										BgL_new1151z00_1863 = BgL_new1150z00_1868;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1151z00_1863)))->
											BgL_locz00) = ((obj_t) BgL_locz00_47), BUNSPEC);
									{
										BgL_typez00_bglt BgL_auxz00_4447;

										{	/* Cnst/alloc.scm 458 */
											BgL_typez00_bglt BgL_arg1882z00_1864;

											{
												BgL_variablez00_bglt BgL_auxz00_4449;

												{	/* Cnst/alloc.scm 69 */
													obj_t BgL_sz00_2993;

													BgL_sz00_2993 = CDR(BgL_oldz00_1857);
													BgL_auxz00_4449 =
														((BgL_variablez00_bglt)
														STRUCT_REF(BgL_sz00_2993, (int) (1L)));
												}
												BgL_arg1882z00_1864 =
													(((BgL_variablez00_bglt) COBJECT(BgL_auxz00_4449))->
													BgL_typez00);
											}
											BgL_auxz00_4447 =
												BGl_strictzd2nodezd2typez00zzast_nodez00(
												((BgL_typez00_bglt) BGl_za2bignumza2z00zztype_cachez00),
												BgL_arg1882z00_1864);
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1151z00_1863)))->
												BgL_typez00) =
											((BgL_typez00_bglt) BgL_auxz00_4447), BUNSPEC);
									}
									{
										BgL_variablez00_bglt BgL_auxz00_4458;

										{	/* Cnst/alloc.scm 69 */
											obj_t BgL_sz00_2996;

											BgL_sz00_2996 = CDR(BgL_oldz00_1857);
											BgL_auxz00_4458 =
												((BgL_variablez00_bglt)
												STRUCT_REF(BgL_sz00_2996, (int) (1L)));
										}
										((((BgL_varz00_bglt) COBJECT(
														((BgL_varz00_bglt) BgL_new1151z00_1863)))->
												BgL_variablez00) =
											((BgL_variablez00_bglt) BgL_auxz00_4458), BUNSPEC);
									}
									return ((obj_t) BgL_new1151z00_1863);
								}
							else
								{	/* Cnst/alloc.scm 454 */
									if (
										(BGl_za2initzd2modeza2zd2zzengine_paramz00 ==
											CNST_TABLE_REF(14)))
										{	/* Cnst/alloc.scm 461 */
											BgL_refz00_bglt BgL_new1153z00_1870;

											{	/* Cnst/alloc.scm 462 */
												BgL_refz00_bglt BgL_new1152z00_1875;

												BgL_new1152z00_1875 =
													((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_refz00_bgl))));
												{	/* Cnst/alloc.scm 462 */
													long BgL_arg1892z00_1876;

													BgL_arg1892z00_1876 =
														BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1152z00_1875),
														BgL_arg1892z00_1876);
												}
												{	/* Cnst/alloc.scm 462 */
													BgL_objectz00_bglt BgL_tmpz00_4473;

													BgL_tmpz00_4473 =
														((BgL_objectz00_bglt) BgL_new1152z00_1875);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4473, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1152z00_1875);
												BgL_new1153z00_1870 = BgL_new1152z00_1875;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1153z00_1870)))->
													BgL_locz00) = ((obj_t) BgL_locz00_47), BUNSPEC);
											{
												BgL_typez00_bglt BgL_auxz00_4479;

												{	/* Cnst/alloc.scm 464 */
													BgL_typez00_bglt BgL_arg1888z00_1871;

													{
														BgL_variablez00_bglt BgL_auxz00_4481;

														{	/* Cnst/alloc.scm 69 */
															obj_t BgL_sz00_3002;

															BgL_sz00_3002 = CDR(BgL_oldz00_1857);
															BgL_auxz00_4481 =
																((BgL_variablez00_bglt)
																STRUCT_REF(BgL_sz00_3002, (int) (1L)));
														}
														BgL_arg1888z00_1871 =
															(((BgL_variablez00_bglt)
																COBJECT(BgL_auxz00_4481))->BgL_typez00);
													}
													BgL_auxz00_4479 =
														BGl_strictzd2nodezd2typez00zzast_nodez00(
														((BgL_typez00_bglt)
															BGl_za2bignumza2z00zztype_cachez00),
														BgL_arg1888z00_1871);
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1153z00_1870)))->
														BgL_typez00) =
													((BgL_typez00_bglt) BgL_auxz00_4479), BUNSPEC);
											}
											{
												BgL_variablez00_bglt BgL_auxz00_4490;

												{	/* Cnst/alloc.scm 69 */
													obj_t BgL_sz00_3005;

													BgL_sz00_3005 = CDR(BgL_oldz00_1857);
													BgL_auxz00_4490 =
														((BgL_variablez00_bglt)
														STRUCT_REF(BgL_sz00_3005, (int) (1L)));
												}
												((((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_new1153z00_1870)))->
														BgL_variablez00) =
													((BgL_variablez00_bglt) BgL_auxz00_4490), BUNSPEC);
											}
											return ((obj_t) BgL_new1153z00_1870);
										}
									else
										{	/* Cnst/alloc.scm 467 */
											obj_t BgL_arg1893z00_1877;

											BgL_arg1893z00_1877 =
												STRUCT_REF(CDR(BgL_oldz00_1857), (int) (1L));
											return
												((obj_t)
												BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00
												(BgL_arg1893z00_1877,
													BGl_za2bignumza2z00zztype_cachez00, BgL_locz00_47));
										}
								}
						}
					else
						{	/* Cnst/alloc.scm 472 */
							bool_t BgL_test2505z00_4503;

							if (
								(BGl_za2initzd2modeza2zd2zzengine_paramz00 ==
									CNST_TABLE_REF(14)))
								{	/* Cnst/alloc.scm 472 */
									BgL_test2505z00_4503 = ((bool_t) 1);
								}
							else
								{	/* Cnst/alloc.scm 472 */
									if ((STRING_LENGTH(BgL_bignumzd2aszd2stringz00_1858) == 0L))
										{	/* Cnst/alloc.scm 473 */
											BgL_test2505z00_4503 = ((bool_t) 1);
										}
									else
										{	/* Cnst/alloc.scm 473 */
											BgL_test2505z00_4503 =
												(STRING_REF(BgL_bignumzd2aszd2stringz00_1858,
													0L) == ((unsigned char) ';'));
								}}
							if (BgL_test2505z00_4503)
								{	/* Cnst/alloc.scm 472 */
									return BFALSE;
								}
							else
								{
									BgL_appz00_bglt BgL_auxz00_4512;

									{	/* Cnst/alloc.scm 443 */
										long BgL_offsetz00_1897;

										BgL_offsetz00_1897 =
											BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00;
										BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00 =
											(1L + BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00);
										BGl_za2globalzd2setza2zd2zzcnst_allocz00 =
											MAKE_YOUNG_PAIR(BgL_bignumz00_46,
											BGl_za2globalzd2setza2zd2zzcnst_allocz00);
										{	/* Cnst/alloc.scm 446 */
											obj_t BgL_arg1914z00_1898;

											{	/* Cnst/alloc.scm 446 */
												obj_t BgL_arg1916z00_1899;

												{	/* Cnst/alloc.scm 69 */
													obj_t BgL_newz00_2949;

													BgL_newz00_2949 =
														create_struct(CNST_TABLE_REF(9), (int) (2L));
													{	/* Cnst/alloc.scm 69 */
														obj_t BgL_auxz00_4520;
														int BgL_tmpz00_4518;

														BgL_auxz00_4520 = BINT(BgL_offsetz00_1897);
														BgL_tmpz00_4518 = (int) (1L);
														STRUCT_SET(BgL_newz00_2949, BgL_tmpz00_4518,
															BgL_auxz00_4520);
													}
													{	/* Cnst/alloc.scm 69 */
														int BgL_tmpz00_4523;

														BgL_tmpz00_4523 = (int) (0L);
														STRUCT_SET(BgL_newz00_2949, BgL_tmpz00_4523,
															BgL_bignumz00_46);
													}
													BgL_arg1916z00_1899 = BgL_newz00_2949;
												}
												BgL_arg1914z00_1898 =
													MAKE_YOUNG_PAIR(BgL_bignumz00_46,
													BgL_arg1916z00_1899);
											}
											BGl_za2bignumzd2envza2zd2zzcnst_allocz00 =
												MAKE_YOUNG_PAIR(BgL_arg1914z00_1898,
												BGl_za2bignumzd2envza2zd2zzcnst_allocz00);
										}
										BgL_auxz00_4512 =
											BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00(BINT
											(BgL_offsetz00_1897), BGl_za2bignumza2z00zztype_cachez00,
											BgL_locz00_47);
									}
									return ((obj_t) BgL_auxz00_4512);
								}
						}
				}
			}
		}

	}



/* &cnst-alloc-bignum */
	obj_t BGl_z62cnstzd2alloczd2bignumz62zzcnst_allocz00(obj_t BgL_envz00_3509,
		obj_t BgL_bignumz00_3510, obj_t BgL_locz00_3511)
	{
		{	/* Cnst/alloc.scm 437 */
			return
				BGl_cnstzd2alloczd2bignumz00zzcnst_allocz00(BgL_bignumz00_3510,
				BgL_locz00_3511);
		}

	}



/* cnst-alloc-procedure */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2procedurez00zzcnst_allocz00(BgL_nodez00_bglt
		BgL_procedurez00_48, obj_t BgL_locz00_49)
	{
		{	/* Cnst/alloc.scm 482 */
			{	/* Cnst/alloc.scm 483 */
				BgL_globalz00_bglt BgL_varz00_1902;

				{	/* Cnst/alloc.scm 483 */
					obj_t BgL_arg1918z00_1906;

					{	/* Cnst/alloc.scm 483 */
						obj_t BgL_arg1919z00_1907;

						BgL_arg1919z00_1907 =
							BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(21));
						BgL_arg1918z00_1906 =
							BGl_makezd2typedzd2identz00zzast_identz00(BgL_arg1919z00_1907,
							CNST_TABLE_REF(22));
					}
					BgL_varz00_1902 =
						BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2
						(BgL_arg1918z00_1906, BGl_za2moduleza2z00zzmodule_modulez00,
						((obj_t) BgL_procedurez00_48), CNST_TABLE_REF(23), BgL_locz00_49);
				}
				{	/* Cnst/alloc.scm 488 */
					BgL_refz00_bglt BgL_new1156z00_1903;

					{	/* Cnst/alloc.scm 489 */
						BgL_refz00_bglt BgL_new1154z00_1904;

						BgL_new1154z00_1904 =
							((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_refz00_bgl))));
						{	/* Cnst/alloc.scm 489 */
							long BgL_arg1917z00_1905;

							BgL_arg1917z00_1905 = BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1154z00_1904),
								BgL_arg1917z00_1905);
						}
						{	/* Cnst/alloc.scm 489 */
							BgL_objectz00_bglt BgL_tmpz00_4543;

							BgL_tmpz00_4543 = ((BgL_objectz00_bglt) BgL_new1154z00_1904);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4543, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1154z00_1904);
						BgL_new1156z00_1903 = BgL_new1154z00_1904;
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1156z00_1903)))->BgL_locz00) =
						((obj_t) BgL_locz00_49), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1156z00_1903)))->BgL_typez00) =
						((BgL_typez00_bglt) (((BgL_variablez00_bglt)
									COBJECT(((BgL_variablez00_bglt) BgL_varz00_1902)))->
								BgL_typez00)), BUNSPEC);
					((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
										BgL_new1156z00_1903)))->BgL_variablez00) =
						((BgL_variablez00_bglt) ((BgL_variablez00_bglt) BgL_varz00_1902)),
						BUNSPEC);
					return ((BgL_nodez00_bglt) BgL_new1156z00_1903);
				}
			}
		}

	}



/* &cnst-alloc-procedure */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2procedurez62zzcnst_allocz00(obj_t
		BgL_envz00_3512, obj_t BgL_procedurez00_3513, obj_t BgL_locz00_3514)
	{
		{	/* Cnst/alloc.scm 482 */
			return
				BGl_cnstzd2alloczd2procedurez00zzcnst_allocz00(
				((BgL_nodez00_bglt) BgL_procedurez00_3513), BgL_locz00_3514);
		}

	}



/* cnst-alloc-l-procedure */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2lzd2procedurezd2zzcnst_allocz00(BgL_nodez00_bglt
		BgL_procedurez00_50, obj_t BgL_locz00_51)
	{
		{	/* Cnst/alloc.scm 496 */
			{	/* Cnst/alloc.scm 497 */
				BgL_globalz00_bglt BgL_varz00_1908;

				{	/* Cnst/alloc.scm 497 */
					obj_t BgL_arg1923z00_1912;

					{	/* Cnst/alloc.scm 497 */
						obj_t BgL_arg1924z00_1913;

						BgL_arg1924z00_1913 =
							BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(21));
						BgL_arg1923z00_1912 =
							BGl_makezd2typedzd2identz00zzast_identz00(BgL_arg1924z00_1913,
							CNST_TABLE_REF(22));
					}
					BgL_varz00_1908 =
						BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2
						(BgL_arg1923z00_1912, BGl_za2moduleza2z00zzmodule_modulez00,
						((obj_t) BgL_procedurez00_50), CNST_TABLE_REF(24), BgL_locz00_51);
				}
				{	/* Cnst/alloc.scm 502 */
					BgL_refz00_bglt BgL_new1158z00_1909;

					{	/* Cnst/alloc.scm 503 */
						BgL_refz00_bglt BgL_new1157z00_1910;

						BgL_new1157z00_1910 =
							((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_refz00_bgl))));
						{	/* Cnst/alloc.scm 503 */
							long BgL_arg1920z00_1911;

							BgL_arg1920z00_1911 = BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1157z00_1910),
								BgL_arg1920z00_1911);
						}
						{	/* Cnst/alloc.scm 503 */
							BgL_objectz00_bglt BgL_tmpz00_4570;

							BgL_tmpz00_4570 = ((BgL_objectz00_bglt) BgL_new1157z00_1910);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4570, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1157z00_1910);
						BgL_new1158z00_1909 = BgL_new1157z00_1910;
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1158z00_1909)))->BgL_locz00) =
						((obj_t) BgL_locz00_51), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1158z00_1909)))->BgL_typez00) =
						((BgL_typez00_bglt) (((BgL_variablez00_bglt)
									COBJECT(((BgL_variablez00_bglt) BgL_varz00_1908)))->
								BgL_typez00)), BUNSPEC);
					((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
										BgL_new1158z00_1909)))->BgL_variablez00) =
						((BgL_variablez00_bglt) ((BgL_variablez00_bglt) BgL_varz00_1908)),
						BUNSPEC);
					return ((BgL_nodez00_bglt) BgL_new1158z00_1909);
				}
			}
		}

	}



/* &cnst-alloc-l-procedure */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2lzd2procedurezb0zzcnst_allocz00(obj_t
		BgL_envz00_3515, obj_t BgL_procedurez00_3516, obj_t BgL_locz00_3517)
	{
		{	/* Cnst/alloc.scm 496 */
			return
				BGl_cnstzd2alloczd2lzd2procedurezd2zzcnst_allocz00(
				((BgL_nodez00_bglt) BgL_procedurez00_3516), BgL_locz00_3517);
		}

	}



/* cnst-alloc-real */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2realz00zzcnst_allocz00(obj_t BgL_realz00_52,
		obj_t BgL_locz00_53)
	{
		{	/* Cnst/alloc.scm 510 */
			{

				{	/* Cnst/alloc.scm 532 */
					obj_t BgL_oldz00_1916;

					{
						obj_t BgL_listz00_1930;

						BgL_listz00_1930 = BGl_za2realzd2envza2zd2zzcnst_allocz00;
					BgL_zc3z04anonymousza31932ze3z87_1931:
						if (NULLP(BgL_listz00_1930))
							{	/* Cnst/alloc.scm 525 */
								BgL_oldz00_1916 = BFALSE;
							}
						else
							{	/* Cnst/alloc.scm 527 */
								bool_t BgL_test2509z00_4588;

								{	/* Cnst/alloc.scm 527 */
									bool_t BgL_test2510z00_4589;

									{	/* Cnst/alloc.scm 527 */
										double BgL_tmpz00_4590;

										{	/* Cnst/alloc.scm 527 */
											obj_t BgL_pairz00_3030;

											BgL_pairz00_3030 = CAR(((obj_t) BgL_listz00_1930));
											BgL_tmpz00_4590 = REAL_TO_DOUBLE(CAR(BgL_pairz00_3030));
										}
										BgL_test2510z00_4589 =
											(BgL_tmpz00_4590 == REAL_TO_DOUBLE(BgL_realz00_52));
									}
									if (BgL_test2510z00_4589)
										{	/* Cnst/alloc.scm 528 */
											long BgL_tmpz00_4597;

											{	/* Cnst/alloc.scm 528 */
												double BgL_tmpz00_4598;

												{	/* Cnst/alloc.scm 528 */
													obj_t BgL_pairz00_3034;

													BgL_pairz00_3034 = CAR(((obj_t) BgL_listz00_1930));
													BgL_tmpz00_4598 =
														REAL_TO_DOUBLE(CAR(BgL_pairz00_3034));
												}
												BgL_tmpz00_4597 = BGL_SIGNBIT(BgL_tmpz00_4598);
											}
											BgL_test2509z00_4588 =
												(BgL_tmpz00_4597 ==
												BGL_SIGNBIT(REAL_TO_DOUBLE(BgL_realz00_52)));
										}
									else
										{	/* Cnst/alloc.scm 527 */
											BgL_test2509z00_4588 = ((bool_t) 0);
										}
								}
								if (BgL_test2509z00_4588)
									{	/* Cnst/alloc.scm 529 */
										obj_t BgL_pairz00_3040;

										BgL_pairz00_3040 = CAR(((obj_t) BgL_listz00_1930));
										BgL_oldz00_1916 = CDR(BgL_pairz00_3040);
									}
								else
									{
										obj_t BgL_listz00_4610;

										BgL_listz00_4610 = CDR(((obj_t) BgL_listz00_1930));
										BgL_listz00_1930 = BgL_listz00_4610;
										goto BgL_zc3z04anonymousza31932ze3z87_1931;
									}
							}
					}
					if (CBOOL(BgL_oldz00_1916))
						{	/* Cnst/alloc.scm 535 */
							BgL_refz00_bglt BgL_new1162z00_1917;

							{	/* Cnst/alloc.scm 536 */
								BgL_refz00_bglt BgL_new1161z00_1918;

								BgL_new1161z00_1918 =
									((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_refz00_bgl))));
								{	/* Cnst/alloc.scm 536 */
									long BgL_arg1925z00_1919;

									BgL_arg1925z00_1919 = BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1161z00_1918),
										BgL_arg1925z00_1919);
								}
								{	/* Cnst/alloc.scm 536 */
									BgL_objectz00_bglt BgL_tmpz00_4619;

									BgL_tmpz00_4619 = ((BgL_objectz00_bglt) BgL_new1161z00_1918);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4619, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1161z00_1918);
								BgL_new1162z00_1917 = BgL_new1161z00_1918;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1162z00_1917)))->BgL_locz00) =
								((obj_t) BgL_locz00_53), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1162z00_1917)))->BgL_typez00) =
								((BgL_typez00_bglt) (((BgL_variablez00_bglt)
											COBJECT(((BgL_variablez00_bglt) BgL_oldz00_1916)))->
										BgL_typez00)), BUNSPEC);
							((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
												BgL_new1162z00_1917)))->BgL_variablez00) =
								((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
										BgL_oldz00_1916)), BUNSPEC);
							return ((BgL_nodez00_bglt) BgL_new1162z00_1917);
						}
					else
						{
							BgL_refz00_bglt BgL_auxz00_4633;

							{	/* Cnst/alloc.scm 512 */
								BgL_globalz00_bglt BgL_varz00_1921;

								{	/* Cnst/alloc.scm 512 */
									obj_t BgL_arg1929z00_1926;

									{	/* Cnst/alloc.scm 512 */
										obj_t BgL_arg1930z00_1927;

										BgL_arg1930z00_1927 =
											BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(25));
										BgL_arg1929z00_1926 =
											BGl_makezd2typedzd2identz00zzast_identz00
											(BgL_arg1930z00_1927, CNST_TABLE_REF(25));
									}
									BgL_varz00_1921 =
										BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2
										(BgL_arg1929z00_1926, BGl_za2moduleza2z00zzmodule_modulez00,
										BgL_realz00_52, CNST_TABLE_REF(26), BgL_locz00_53);
								}
								{	/* Cnst/alloc.scm 517 */
									obj_t BgL_arg1927z00_1922;

									BgL_arg1927z00_1922 =
										MAKE_YOUNG_PAIR(BgL_realz00_52, ((obj_t) BgL_varz00_1921));
									BGl_za2realzd2envza2zd2zzcnst_allocz00 =
										MAKE_YOUNG_PAIR(BgL_arg1927z00_1922,
										BGl_za2realzd2envza2zd2zzcnst_allocz00);
								}
								{	/* Cnst/alloc.scm 518 */
									BgL_refz00_bglt BgL_new1160z00_1923;

									{	/* Cnst/alloc.scm 519 */
										BgL_refz00_bglt BgL_new1159z00_1924;

										BgL_new1159z00_1924 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Cnst/alloc.scm 519 */
											long BgL_arg1928z00_1925;

											BgL_arg1928z00_1925 =
												BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1159z00_1924),
												BgL_arg1928z00_1925);
										}
										{	/* Cnst/alloc.scm 519 */
											BgL_objectz00_bglt BgL_tmpz00_4647;

											BgL_tmpz00_4647 =
												((BgL_objectz00_bglt) BgL_new1159z00_1924);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4647, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1159z00_1924);
										BgL_new1160z00_1923 = BgL_new1159z00_1924;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1160z00_1923)))->
											BgL_locz00) = ((obj_t) BgL_locz00_53), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1160z00_1923)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_variablez00_bglt)
													COBJECT(((BgL_variablez00_bglt) BgL_varz00_1921)))->
												BgL_typez00)), BUNSPEC);
									((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
														BgL_new1160z00_1923)))->BgL_variablez00) =
										((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
												BgL_varz00_1921)), BUNSPEC);
									BgL_auxz00_4633 = BgL_new1160z00_1923;
							}}
							return ((BgL_nodez00_bglt) BgL_auxz00_4633);
						}
				}
			}
		}

	}



/* &cnst-alloc-real */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2realz62zzcnst_allocz00(obj_t
		BgL_envz00_3518, obj_t BgL_realz00_3519, obj_t BgL_locz00_3520)
	{
		{	/* Cnst/alloc.scm 510 */
			return
				BGl_cnstzd2alloczd2realz00zzcnst_allocz00(BgL_realz00_3519,
				BgL_locz00_3520);
		}

	}



/* cnst-alloc-elong */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2elongz00zzcnst_allocz00(long BgL_elongz00_54,
		obj_t BgL_locz00_55)
	{
		{	/* Cnst/alloc.scm 545 */
			{

				{	/* Cnst/alloc.scm 566 */
					obj_t BgL_oldz00_1957;

					{
						obj_t BgL_listz00_1971;

						BgL_listz00_1971 = BGl_za2elongzd2envza2zd2zzcnst_allocz00;
					BgL_zc3z04anonymousza31959ze3z87_1972:
						if (NULLP(BgL_listz00_1971))
							{	/* Cnst/alloc.scm 560 */
								BgL_oldz00_1957 = BFALSE;
							}
						else
							{	/* Cnst/alloc.scm 562 */
								bool_t BgL_test2513z00_4664;

								{	/* Cnst/alloc.scm 562 */
									long BgL_n1z00_3054;

									{	/* Cnst/alloc.scm 562 */
										obj_t BgL_pairz00_3053;

										BgL_pairz00_3053 = CAR(((obj_t) BgL_listz00_1971));
										{	/* Cnst/alloc.scm 562 */
											obj_t BgL_tmpz00_4667;

											BgL_tmpz00_4667 = CAR(BgL_pairz00_3053);
											BgL_n1z00_3054 = BELONG_TO_LONG(BgL_tmpz00_4667);
									}}
									BgL_test2513z00_4664 = (BgL_n1z00_3054 == BgL_elongz00_54);
								}
								if (BgL_test2513z00_4664)
									{	/* Cnst/alloc.scm 563 */
										obj_t BgL_pairz00_3057;

										BgL_pairz00_3057 = CAR(((obj_t) BgL_listz00_1971));
										BgL_oldz00_1957 = CDR(BgL_pairz00_3057);
									}
								else
									{
										obj_t BgL_listz00_4674;

										BgL_listz00_4674 = CDR(((obj_t) BgL_listz00_1971));
										BgL_listz00_1971 = BgL_listz00_4674;
										goto BgL_zc3z04anonymousza31959ze3z87_1972;
									}
							}
					}
					if (CBOOL(BgL_oldz00_1957))
						{	/* Cnst/alloc.scm 569 */
							BgL_refz00_bglt BgL_new1166z00_1958;

							{	/* Cnst/alloc.scm 570 */
								BgL_refz00_bglt BgL_new1165z00_1959;

								BgL_new1165z00_1959 =
									((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_refz00_bgl))));
								{	/* Cnst/alloc.scm 570 */
									long BgL_arg1952z00_1960;

									BgL_arg1952z00_1960 = BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1165z00_1959),
										BgL_arg1952z00_1960);
								}
								{	/* Cnst/alloc.scm 570 */
									BgL_objectz00_bglt BgL_tmpz00_4683;

									BgL_tmpz00_4683 = ((BgL_objectz00_bglt) BgL_new1165z00_1959);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4683, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1165z00_1959);
								BgL_new1166z00_1958 = BgL_new1165z00_1959;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1166z00_1958)))->BgL_locz00) =
								((obj_t) BgL_locz00_55), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1166z00_1958)))->BgL_typez00) =
								((BgL_typez00_bglt) (((BgL_variablez00_bglt)
											COBJECT(((BgL_variablez00_bglt) BgL_oldz00_1957)))->
										BgL_typez00)), BUNSPEC);
							((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
												BgL_new1166z00_1958)))->BgL_variablez00) =
								((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
										BgL_oldz00_1957)), BUNSPEC);
							return ((BgL_nodez00_bglt) BgL_new1166z00_1958);
						}
					else
						{
							BgL_refz00_bglt BgL_auxz00_4697;

							{	/* Cnst/alloc.scm 547 */
								BgL_globalz00_bglt BgL_varz00_1962;

								{	/* Cnst/alloc.scm 547 */
									obj_t BgL_arg1956z00_1967;

									{	/* Cnst/alloc.scm 547 */
										obj_t BgL_arg1957z00_1968;

										BgL_arg1957z00_1968 =
											BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(27));
										BgL_arg1956z00_1967 =
											BGl_makezd2typedzd2identz00zzast_identz00
											(BgL_arg1957z00_1968, CNST_TABLE_REF(28));
									}
									BgL_varz00_1962 =
										BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2
										(BgL_arg1956z00_1967, BGl_za2moduleza2z00zzmodule_modulez00,
										make_belong(BgL_elongz00_54), CNST_TABLE_REF(29),
										BgL_locz00_55);
								}
								{	/* Cnst/alloc.scm 552 */
									obj_t BgL_arg1954z00_1963;

									BgL_arg1954z00_1963 =
										MAKE_YOUNG_PAIR(make_belong(BgL_elongz00_54),
										((obj_t) BgL_varz00_1962));
									BGl_za2elongzd2envza2zd2zzcnst_allocz00 =
										MAKE_YOUNG_PAIR(BgL_arg1954z00_1963,
										BGl_za2elongzd2envza2zd2zzcnst_allocz00);
								}
								{	/* Cnst/alloc.scm 553 */
									BgL_refz00_bglt BgL_new1164z00_1964;

									{	/* Cnst/alloc.scm 554 */
										BgL_refz00_bglt BgL_new1163z00_1965;

										BgL_new1163z00_1965 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Cnst/alloc.scm 554 */
											long BgL_arg1955z00_1966;

											BgL_arg1955z00_1966 =
												BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1163z00_1965),
												BgL_arg1955z00_1966);
										}
										{	/* Cnst/alloc.scm 554 */
											BgL_objectz00_bglt BgL_tmpz00_4713;

											BgL_tmpz00_4713 =
												((BgL_objectz00_bglt) BgL_new1163z00_1965);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4713, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1163z00_1965);
										BgL_new1164z00_1964 = BgL_new1163z00_1965;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1164z00_1964)))->
											BgL_locz00) = ((obj_t) BgL_locz00_55), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1164z00_1964)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_variablez00_bglt)
													COBJECT(((BgL_variablez00_bglt) BgL_varz00_1962)))->
												BgL_typez00)), BUNSPEC);
									((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
														BgL_new1164z00_1964)))->BgL_variablez00) =
										((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
												BgL_varz00_1962)), BUNSPEC);
									BgL_auxz00_4697 = BgL_new1164z00_1964;
							}}
							return ((BgL_nodez00_bglt) BgL_auxz00_4697);
						}
				}
			}
		}

	}



/* &cnst-alloc-elong */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2elongz62zzcnst_allocz00(obj_t
		BgL_envz00_3521, obj_t BgL_elongz00_3522, obj_t BgL_locz00_3523)
	{
		{	/* Cnst/alloc.scm 545 */
			return
				BGl_cnstzd2alloczd2elongz00zzcnst_allocz00(BELONG_TO_LONG
				(BgL_elongz00_3522), BgL_locz00_3523);
		}

	}



/* cnst-alloc-llong */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2llongz00zzcnst_allocz00(BGL_LONGLONG_T BgL_llongz00_56,
		obj_t BgL_locz00_57)
	{
		{	/* Cnst/alloc.scm 579 */
			{

				{	/* Cnst/alloc.scm 600 */
					obj_t BgL_oldz00_1986;

					{
						obj_t BgL_listz00_2000;

						BgL_listz00_2000 = BGl_za2llongzd2envza2zd2zzcnst_allocz00;
					BgL_zc3z04anonymousza31975ze3z87_2001:
						if (NULLP(BgL_listz00_2000))
							{	/* Cnst/alloc.scm 594 */
								BgL_oldz00_1986 = BFALSE;
							}
						else
							{	/* Cnst/alloc.scm 596 */
								bool_t BgL_test2516z00_4731;

								{	/* Cnst/alloc.scm 596 */
									BGL_LONGLONG_T BgL_tmpz00_4732;

									{	/* Cnst/alloc.scm 596 */
										obj_t BgL_pairz00_3070;

										BgL_pairz00_3070 = CAR(((obj_t) BgL_listz00_2000));
										BgL_tmpz00_4732 = BLLONG_TO_LLONG(CAR(BgL_pairz00_3070));
									}
									BgL_test2516z00_4731 = (BgL_tmpz00_4732 == BgL_llongz00_56);
								}
								if (BgL_test2516z00_4731)
									{	/* Cnst/alloc.scm 597 */
										obj_t BgL_pairz00_3074;

										BgL_pairz00_3074 = CAR(((obj_t) BgL_listz00_2000));
										BgL_oldz00_1986 = CDR(BgL_pairz00_3074);
									}
								else
									{
										obj_t BgL_listz00_4741;

										BgL_listz00_4741 = CDR(((obj_t) BgL_listz00_2000));
										BgL_listz00_2000 = BgL_listz00_4741;
										goto BgL_zc3z04anonymousza31975ze3z87_2001;
									}
							}
					}
					if (CBOOL(BgL_oldz00_1986))
						{	/* Cnst/alloc.scm 603 */
							BgL_refz00_bglt BgL_new1170z00_1987;

							{	/* Cnst/alloc.scm 604 */
								BgL_refz00_bglt BgL_new1169z00_1988;

								BgL_new1169z00_1988 =
									((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_refz00_bgl))));
								{	/* Cnst/alloc.scm 604 */
									long BgL_arg1968z00_1989;

									BgL_arg1968z00_1989 = BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1169z00_1988),
										BgL_arg1968z00_1989);
								}
								{	/* Cnst/alloc.scm 604 */
									BgL_objectz00_bglt BgL_tmpz00_4750;

									BgL_tmpz00_4750 = ((BgL_objectz00_bglt) BgL_new1169z00_1988);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4750, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1169z00_1988);
								BgL_new1170z00_1987 = BgL_new1169z00_1988;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1170z00_1987)))->BgL_locz00) =
								((obj_t) BgL_locz00_57), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1170z00_1987)))->BgL_typez00) =
								((BgL_typez00_bglt) (((BgL_variablez00_bglt)
											COBJECT(((BgL_variablez00_bglt) BgL_oldz00_1986)))->
										BgL_typez00)), BUNSPEC);
							((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
												BgL_new1170z00_1987)))->BgL_variablez00) =
								((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
										BgL_oldz00_1986)), BUNSPEC);
							return ((BgL_nodez00_bglt) BgL_new1170z00_1987);
						}
					else
						{
							BgL_refz00_bglt BgL_auxz00_4764;

							{	/* Cnst/alloc.scm 581 */
								BgL_globalz00_bglt BgL_varz00_1991;

								{	/* Cnst/alloc.scm 581 */
									obj_t BgL_arg1972z00_1996;

									{	/* Cnst/alloc.scm 581 */
										obj_t BgL_arg1973z00_1997;

										BgL_arg1973z00_1997 =
											BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(30));
										BgL_arg1972z00_1996 =
											BGl_makezd2typedzd2identz00zzast_identz00
											(BgL_arg1973z00_1997, CNST_TABLE_REF(31));
									}
									BgL_varz00_1991 =
										BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2
										(BgL_arg1972z00_1996, BGl_za2moduleza2z00zzmodule_modulez00,
										make_bllong(BgL_llongz00_56), CNST_TABLE_REF(32),
										BgL_locz00_57);
								}
								{	/* Cnst/alloc.scm 586 */
									obj_t BgL_arg1970z00_1992;

									BgL_arg1970z00_1992 =
										MAKE_YOUNG_PAIR(make_bllong(BgL_llongz00_56),
										((obj_t) BgL_varz00_1991));
									BGl_za2llongzd2envza2zd2zzcnst_allocz00 =
										MAKE_YOUNG_PAIR(BgL_arg1970z00_1992,
										BGl_za2llongzd2envza2zd2zzcnst_allocz00);
								}
								{	/* Cnst/alloc.scm 587 */
									BgL_refz00_bglt BgL_new1168z00_1993;

									{	/* Cnst/alloc.scm 588 */
										BgL_refz00_bglt BgL_new1167z00_1994;

										BgL_new1167z00_1994 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Cnst/alloc.scm 588 */
											long BgL_arg1971z00_1995;

											BgL_arg1971z00_1995 =
												BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1167z00_1994),
												BgL_arg1971z00_1995);
										}
										{	/* Cnst/alloc.scm 588 */
											BgL_objectz00_bglt BgL_tmpz00_4780;

											BgL_tmpz00_4780 =
												((BgL_objectz00_bglt) BgL_new1167z00_1994);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4780, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1167z00_1994);
										BgL_new1168z00_1993 = BgL_new1167z00_1994;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1168z00_1993)))->
											BgL_locz00) = ((obj_t) BgL_locz00_57), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1168z00_1993)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_variablez00_bglt)
													COBJECT(((BgL_variablez00_bglt) BgL_varz00_1991)))->
												BgL_typez00)), BUNSPEC);
									((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
														BgL_new1168z00_1993)))->BgL_variablez00) =
										((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
												BgL_varz00_1991)), BUNSPEC);
									BgL_auxz00_4764 = BgL_new1168z00_1993;
							}}
							return ((BgL_nodez00_bglt) BgL_auxz00_4764);
						}
				}
			}
		}

	}



/* &cnst-alloc-llong */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2llongz62zzcnst_allocz00(obj_t
		BgL_envz00_3524, obj_t BgL_llongz00_3525, obj_t BgL_locz00_3526)
	{
		{	/* Cnst/alloc.scm 579 */
			return
				BGl_cnstzd2alloczd2llongz00zzcnst_allocz00(BLLONG_TO_LLONG
				(BgL_llongz00_3525), BgL_locz00_3526);
		}

	}



/* cnst-alloc-int32 */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2int32z00zzcnst_allocz00(int32_t BgL_int32z00_58,
		obj_t BgL_locz00_59)
	{
		{	/* Cnst/alloc.scm 613 */
			{

				{	/* Cnst/alloc.scm 634 */
					obj_t BgL_oldz00_2015;

					{
						obj_t BgL_listz00_2029;

						BgL_listz00_2029 = BGl_za2int32zd2envza2zd2zzcnst_allocz00;
					BgL_zc3z04anonymousza31991ze3z87_2030:
						if (NULLP(BgL_listz00_2029))
							{	/* Cnst/alloc.scm 628 */
								BgL_oldz00_2015 = BFALSE;
							}
						else
							{	/* Cnst/alloc.scm 630 */
								bool_t BgL_test2519z00_4798;

								{	/* Cnst/alloc.scm 630 */
									obj_t BgL_arg1998z00_2037;

									{	/* Cnst/alloc.scm 630 */
										obj_t BgL_pairz00_3087;

										BgL_pairz00_3087 = CAR(((obj_t) BgL_listz00_2029));
										BgL_arg1998z00_2037 = CAR(BgL_pairz00_3087);
									}
									{	/* Cnst/alloc.scm 630 */
										int32_t BgL_n1z00_3088;

										BgL_n1z00_3088 = BGL_BINT32_TO_INT32(BgL_arg1998z00_2037);
										BgL_test2519z00_4798 = (BgL_n1z00_3088 == BgL_int32z00_58);
									}
								}
								if (BgL_test2519z00_4798)
									{	/* Cnst/alloc.scm 631 */
										obj_t BgL_pairz00_3091;

										BgL_pairz00_3091 = CAR(((obj_t) BgL_listz00_2029));
										BgL_oldz00_2015 = CDR(BgL_pairz00_3091);
									}
								else
									{
										obj_t BgL_listz00_4807;

										BgL_listz00_4807 = CDR(((obj_t) BgL_listz00_2029));
										BgL_listz00_2029 = BgL_listz00_4807;
										goto BgL_zc3z04anonymousza31991ze3z87_2030;
									}
							}
					}
					if (CBOOL(BgL_oldz00_2015))
						{	/* Cnst/alloc.scm 637 */
							BgL_refz00_bglt BgL_new1174z00_2016;

							{	/* Cnst/alloc.scm 638 */
								BgL_refz00_bglt BgL_new1173z00_2017;

								BgL_new1173z00_2017 =
									((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_refz00_bgl))));
								{	/* Cnst/alloc.scm 638 */
									long BgL_arg1984z00_2018;

									BgL_arg1984z00_2018 = BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1173z00_2017),
										BgL_arg1984z00_2018);
								}
								{	/* Cnst/alloc.scm 638 */
									BgL_objectz00_bglt BgL_tmpz00_4816;

									BgL_tmpz00_4816 = ((BgL_objectz00_bglt) BgL_new1173z00_2017);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4816, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1173z00_2017);
								BgL_new1174z00_2016 = BgL_new1173z00_2017;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1174z00_2016)))->BgL_locz00) =
								((obj_t) BgL_locz00_59), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1174z00_2016)))->BgL_typez00) =
								((BgL_typez00_bglt) (((BgL_variablez00_bglt)
											COBJECT(((BgL_variablez00_bglt) BgL_oldz00_2015)))->
										BgL_typez00)), BUNSPEC);
							((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
												BgL_new1174z00_2016)))->BgL_variablez00) =
								((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
										BgL_oldz00_2015)), BUNSPEC);
							return ((BgL_nodez00_bglt) BgL_new1174z00_2016);
						}
					else
						{
							BgL_refz00_bglt BgL_auxz00_4830;

							{	/* Cnst/alloc.scm 615 */
								BgL_globalz00_bglt BgL_varz00_2020;

								{	/* Cnst/alloc.scm 615 */
									obj_t BgL_arg1988z00_2025;

									{	/* Cnst/alloc.scm 615 */
										obj_t BgL_arg1989z00_2026;

										BgL_arg1989z00_2026 =
											BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(33));
										BgL_arg1988z00_2025 =
											BGl_makezd2typedzd2identz00zzast_identz00
											(BgL_arg1989z00_2026, CNST_TABLE_REF(34));
									}
									BgL_varz00_2020 =
										BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2
										(BgL_arg1988z00_2025, BGl_za2moduleza2z00zzmodule_modulez00,
										BGL_INT32_TO_BINT32(BgL_int32z00_58), CNST_TABLE_REF(35),
										BgL_locz00_59);
								}
								{	/* Cnst/alloc.scm 620 */
									obj_t BgL_arg1986z00_2021;

									BgL_arg1986z00_2021 =
										MAKE_YOUNG_PAIR(BGL_INT32_TO_BINT32(BgL_int32z00_58),
										((obj_t) BgL_varz00_2020));
									BGl_za2int32zd2envza2zd2zzcnst_allocz00 =
										MAKE_YOUNG_PAIR(BgL_arg1986z00_2021,
										BGl_za2int32zd2envza2zd2zzcnst_allocz00);
								}
								{	/* Cnst/alloc.scm 621 */
									BgL_refz00_bglt BgL_new1172z00_2022;

									{	/* Cnst/alloc.scm 622 */
										BgL_refz00_bglt BgL_new1171z00_2023;

										BgL_new1171z00_2023 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Cnst/alloc.scm 622 */
											long BgL_arg1987z00_2024;

											BgL_arg1987z00_2024 =
												BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1171z00_2023),
												BgL_arg1987z00_2024);
										}
										{	/* Cnst/alloc.scm 622 */
											BgL_objectz00_bglt BgL_tmpz00_4846;

											BgL_tmpz00_4846 =
												((BgL_objectz00_bglt) BgL_new1171z00_2023);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4846, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1171z00_2023);
										BgL_new1172z00_2022 = BgL_new1171z00_2023;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1172z00_2022)))->
											BgL_locz00) = ((obj_t) BgL_locz00_59), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1172z00_2022)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_variablez00_bglt)
													COBJECT(((BgL_variablez00_bglt) BgL_varz00_2020)))->
												BgL_typez00)), BUNSPEC);
									((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
														BgL_new1172z00_2022)))->BgL_variablez00) =
										((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
												BgL_varz00_2020)), BUNSPEC);
									BgL_auxz00_4830 = BgL_new1172z00_2022;
							}}
							return ((BgL_nodez00_bglt) BgL_auxz00_4830);
						}
				}
			}
		}

	}



/* &cnst-alloc-int32 */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2int32z62zzcnst_allocz00(obj_t
		BgL_envz00_3527, obj_t BgL_int32z00_3528, obj_t BgL_locz00_3529)
	{
		{	/* Cnst/alloc.scm 613 */
			return
				BGl_cnstzd2alloczd2int32z00zzcnst_allocz00(BGL_BINT32_TO_INT32
				(BgL_int32z00_3528), BgL_locz00_3529);
		}

	}



/* cnst-alloc-uint32 */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2uint32z00zzcnst_allocz00(uint32_t BgL_uint32z00_60,
		obj_t BgL_locz00_61)
	{
		{	/* Cnst/alloc.scm 647 */
			{

				{	/* Cnst/alloc.scm 668 */
					obj_t BgL_oldz00_2044;

					{
						obj_t BgL_listz00_2058;

						BgL_listz00_2058 = BGl_za2uint32zd2envza2zd2zzcnst_allocz00;
					BgL_zc3z04anonymousza32008ze3z87_2059:
						if (NULLP(BgL_listz00_2058))
							{	/* Cnst/alloc.scm 662 */
								BgL_oldz00_2044 = BFALSE;
							}
						else
							{	/* Cnst/alloc.scm 664 */
								bool_t BgL_test2522z00_4864;

								{	/* Cnst/alloc.scm 664 */
									obj_t BgL_arg2015z00_2066;

									{	/* Cnst/alloc.scm 664 */
										obj_t BgL_pairz00_3104;

										BgL_pairz00_3104 = CAR(((obj_t) BgL_listz00_2058));
										BgL_arg2015z00_2066 = CAR(BgL_pairz00_3104);
									}
									{	/* Cnst/alloc.scm 664 */
										uint32_t BgL_n1z00_3105;

										BgL_n1z00_3105 = BGL_BUINT32_TO_UINT32(BgL_arg2015z00_2066);
										BgL_test2522z00_4864 = (BgL_n1z00_3105 == BgL_uint32z00_60);
									}
								}
								if (BgL_test2522z00_4864)
									{	/* Cnst/alloc.scm 665 */
										obj_t BgL_pairz00_3108;

										BgL_pairz00_3108 = CAR(((obj_t) BgL_listz00_2058));
										BgL_oldz00_2044 = CDR(BgL_pairz00_3108);
									}
								else
									{
										obj_t BgL_listz00_4873;

										BgL_listz00_4873 = CDR(((obj_t) BgL_listz00_2058));
										BgL_listz00_2058 = BgL_listz00_4873;
										goto BgL_zc3z04anonymousza32008ze3z87_2059;
									}
							}
					}
					if (CBOOL(BgL_oldz00_2044))
						{	/* Cnst/alloc.scm 671 */
							BgL_refz00_bglt BgL_new1178z00_2045;

							{	/* Cnst/alloc.scm 672 */
								BgL_refz00_bglt BgL_new1177z00_2046;

								BgL_new1177z00_2046 =
									((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_refz00_bgl))));
								{	/* Cnst/alloc.scm 672 */
									long BgL_arg2000z00_2047;

									BgL_arg2000z00_2047 = BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1177z00_2046),
										BgL_arg2000z00_2047);
								}
								{	/* Cnst/alloc.scm 672 */
									BgL_objectz00_bglt BgL_tmpz00_4882;

									BgL_tmpz00_4882 = ((BgL_objectz00_bglt) BgL_new1177z00_2046);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4882, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1177z00_2046);
								BgL_new1178z00_2045 = BgL_new1177z00_2046;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1178z00_2045)))->BgL_locz00) =
								((obj_t) BgL_locz00_61), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1178z00_2045)))->BgL_typez00) =
								((BgL_typez00_bglt) (((BgL_variablez00_bglt)
											COBJECT(((BgL_variablez00_bglt) BgL_oldz00_2044)))->
										BgL_typez00)), BUNSPEC);
							((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
												BgL_new1178z00_2045)))->BgL_variablez00) =
								((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
										BgL_oldz00_2044)), BUNSPEC);
							return ((BgL_nodez00_bglt) BgL_new1178z00_2045);
						}
					else
						{
							BgL_refz00_bglt BgL_auxz00_4896;

							{	/* Cnst/alloc.scm 649 */
								BgL_globalz00_bglt BgL_varz00_2049;

								{	/* Cnst/alloc.scm 649 */
									obj_t BgL_arg2004z00_2054;

									{	/* Cnst/alloc.scm 649 */
										obj_t BgL_arg2006z00_2055;

										BgL_arg2006z00_2055 =
											BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(36));
										BgL_arg2004z00_2054 =
											BGl_makezd2typedzd2identz00zzast_identz00
											(BgL_arg2006z00_2055, CNST_TABLE_REF(37));
									}
									BgL_varz00_2049 =
										BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2
										(BgL_arg2004z00_2054, BGl_za2moduleza2z00zzmodule_modulez00,
										BGL_UINT32_TO_BUINT32(BgL_uint32z00_60), CNST_TABLE_REF(38),
										BgL_locz00_61);
								}
								{	/* Cnst/alloc.scm 654 */
									obj_t BgL_arg2002z00_2050;

									BgL_arg2002z00_2050 =
										MAKE_YOUNG_PAIR(BGL_UINT32_TO_BUINT32(BgL_uint32z00_60),
										((obj_t) BgL_varz00_2049));
									BGl_za2uint32zd2envza2zd2zzcnst_allocz00 =
										MAKE_YOUNG_PAIR(BgL_arg2002z00_2050,
										BGl_za2uint32zd2envza2zd2zzcnst_allocz00);
								}
								{	/* Cnst/alloc.scm 655 */
									BgL_refz00_bglt BgL_new1176z00_2051;

									{	/* Cnst/alloc.scm 656 */
										BgL_refz00_bglt BgL_new1175z00_2052;

										BgL_new1175z00_2052 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Cnst/alloc.scm 656 */
											long BgL_arg2003z00_2053;

											BgL_arg2003z00_2053 =
												BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1175z00_2052),
												BgL_arg2003z00_2053);
										}
										{	/* Cnst/alloc.scm 656 */
											BgL_objectz00_bglt BgL_tmpz00_4912;

											BgL_tmpz00_4912 =
												((BgL_objectz00_bglt) BgL_new1175z00_2052);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4912, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1175z00_2052);
										BgL_new1176z00_2051 = BgL_new1175z00_2052;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1176z00_2051)))->
											BgL_locz00) = ((obj_t) BgL_locz00_61), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1176z00_2051)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_variablez00_bglt)
													COBJECT(((BgL_variablez00_bglt) BgL_varz00_2049)))->
												BgL_typez00)), BUNSPEC);
									((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
														BgL_new1176z00_2051)))->BgL_variablez00) =
										((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
												BgL_varz00_2049)), BUNSPEC);
									BgL_auxz00_4896 = BgL_new1176z00_2051;
							}}
							return ((BgL_nodez00_bglt) BgL_auxz00_4896);
						}
				}
			}
		}

	}



/* &cnst-alloc-uint32 */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2uint32z62zzcnst_allocz00(obj_t
		BgL_envz00_3530, obj_t BgL_uint32z00_3531, obj_t BgL_locz00_3532)
	{
		{	/* Cnst/alloc.scm 647 */
			return
				BGl_cnstzd2alloczd2uint32z00zzcnst_allocz00(BGL_BUINT32_TO_UINT32
				(BgL_uint32z00_3531), BgL_locz00_3532);
		}

	}



/* cnst-alloc-int64 */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2int64z00zzcnst_allocz00(int64_t BgL_int64z00_62,
		obj_t BgL_locz00_63)
	{
		{	/* Cnst/alloc.scm 681 */
			{

				{	/* Cnst/alloc.scm 702 */
					obj_t BgL_oldz00_2073;

					{
						obj_t BgL_listz00_2087;

						BgL_listz00_2087 = BGl_za2int64zd2envza2zd2zzcnst_allocz00;
					BgL_zc3z04anonymousza32024ze3z87_2088:
						if (NULLP(BgL_listz00_2087))
							{	/* Cnst/alloc.scm 696 */
								BgL_oldz00_2073 = BFALSE;
							}
						else
							{	/* Cnst/alloc.scm 698 */
								bool_t BgL_test2525z00_4930;

								{	/* Cnst/alloc.scm 698 */
									obj_t BgL_arg2031z00_2095;

									{	/* Cnst/alloc.scm 698 */
										obj_t BgL_pairz00_3121;

										BgL_pairz00_3121 = CAR(((obj_t) BgL_listz00_2087));
										BgL_arg2031z00_2095 = CAR(BgL_pairz00_3121);
									}
									{	/* Cnst/alloc.scm 698 */
										int64_t BgL_n1z00_3122;

										BgL_n1z00_3122 = BGL_BINT64_TO_INT64(BgL_arg2031z00_2095);
										BgL_test2525z00_4930 = (BgL_n1z00_3122 == BgL_int64z00_62);
									}
								}
								if (BgL_test2525z00_4930)
									{	/* Cnst/alloc.scm 699 */
										obj_t BgL_pairz00_3125;

										BgL_pairz00_3125 = CAR(((obj_t) BgL_listz00_2087));
										BgL_oldz00_2073 = CDR(BgL_pairz00_3125);
									}
								else
									{
										obj_t BgL_listz00_4939;

										BgL_listz00_4939 = CDR(((obj_t) BgL_listz00_2087));
										BgL_listz00_2087 = BgL_listz00_4939;
										goto BgL_zc3z04anonymousza32024ze3z87_2088;
									}
							}
					}
					if (CBOOL(BgL_oldz00_2073))
						{	/* Cnst/alloc.scm 705 */
							BgL_refz00_bglt BgL_new1183z00_2074;

							{	/* Cnst/alloc.scm 706 */
								BgL_refz00_bglt BgL_new1182z00_2075;

								BgL_new1182z00_2075 =
									((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_refz00_bgl))));
								{	/* Cnst/alloc.scm 706 */
									long BgL_arg2017z00_2076;

									BgL_arg2017z00_2076 = BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1182z00_2075),
										BgL_arg2017z00_2076);
								}
								{	/* Cnst/alloc.scm 706 */
									BgL_objectz00_bglt BgL_tmpz00_4948;

									BgL_tmpz00_4948 = ((BgL_objectz00_bglt) BgL_new1182z00_2075);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4948, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1182z00_2075);
								BgL_new1183z00_2074 = BgL_new1182z00_2075;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1183z00_2074)))->BgL_locz00) =
								((obj_t) BgL_locz00_63), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1183z00_2074)))->BgL_typez00) =
								((BgL_typez00_bglt) (((BgL_variablez00_bglt)
											COBJECT(((BgL_variablez00_bglt) BgL_oldz00_2073)))->
										BgL_typez00)), BUNSPEC);
							((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
												BgL_new1183z00_2074)))->BgL_variablez00) =
								((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
										BgL_oldz00_2073)), BUNSPEC);
							return ((BgL_nodez00_bglt) BgL_new1183z00_2074);
						}
					else
						{
							BgL_refz00_bglt BgL_auxz00_4962;

							{	/* Cnst/alloc.scm 683 */
								BgL_globalz00_bglt BgL_varz00_2078;

								{	/* Cnst/alloc.scm 683 */
									obj_t BgL_arg2021z00_2083;

									{	/* Cnst/alloc.scm 683 */
										obj_t BgL_arg2022z00_2084;

										BgL_arg2022z00_2084 =
											BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(39));
										BgL_arg2021z00_2083 =
											BGl_makezd2typedzd2identz00zzast_identz00
											(BgL_arg2022z00_2084, CNST_TABLE_REF(40));
									}
									BgL_varz00_2078 =
										BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2
										(BgL_arg2021z00_2083, BGl_za2moduleza2z00zzmodule_modulez00,
										BGL_INT64_TO_BINT64(BgL_int64z00_62), CNST_TABLE_REF(41),
										BgL_locz00_63);
								}
								{	/* Cnst/alloc.scm 688 */
									obj_t BgL_arg2019z00_2079;

									BgL_arg2019z00_2079 =
										MAKE_YOUNG_PAIR(BGL_INT64_TO_BINT64(BgL_int64z00_62),
										((obj_t) BgL_varz00_2078));
									BGl_za2int64zd2envza2zd2zzcnst_allocz00 =
										MAKE_YOUNG_PAIR(BgL_arg2019z00_2079,
										BGl_za2int64zd2envza2zd2zzcnst_allocz00);
								}
								{	/* Cnst/alloc.scm 689 */
									BgL_refz00_bglt BgL_new1180z00_2080;

									{	/* Cnst/alloc.scm 690 */
										BgL_refz00_bglt BgL_new1179z00_2081;

										BgL_new1179z00_2081 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Cnst/alloc.scm 690 */
											long BgL_arg2020z00_2082;

											BgL_arg2020z00_2082 =
												BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1179z00_2081),
												BgL_arg2020z00_2082);
										}
										{	/* Cnst/alloc.scm 690 */
											BgL_objectz00_bglt BgL_tmpz00_4978;

											BgL_tmpz00_4978 =
												((BgL_objectz00_bglt) BgL_new1179z00_2081);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4978, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1179z00_2081);
										BgL_new1180z00_2080 = BgL_new1179z00_2081;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1180z00_2080)))->
											BgL_locz00) = ((obj_t) BgL_locz00_63), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1180z00_2080)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_variablez00_bglt)
													COBJECT(((BgL_variablez00_bglt) BgL_varz00_2078)))->
												BgL_typez00)), BUNSPEC);
									((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
														BgL_new1180z00_2080)))->BgL_variablez00) =
										((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
												BgL_varz00_2078)), BUNSPEC);
									BgL_auxz00_4962 = BgL_new1180z00_2080;
							}}
							return ((BgL_nodez00_bglt) BgL_auxz00_4962);
						}
				}
			}
		}

	}



/* &cnst-alloc-int64 */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2int64z62zzcnst_allocz00(obj_t
		BgL_envz00_3533, obj_t BgL_int64z00_3534, obj_t BgL_locz00_3535)
	{
		{	/* Cnst/alloc.scm 681 */
			return
				BGl_cnstzd2alloczd2int64z00zzcnst_allocz00(BGL_BINT64_TO_INT64
				(BgL_int64z00_3534), BgL_locz00_3535);
		}

	}



/* cnst-alloc-uint64 */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2uint64z00zzcnst_allocz00(uint64_t BgL_uint64z00_64,
		obj_t BgL_locz00_65)
	{
		{	/* Cnst/alloc.scm 715 */
			{

				{	/* Cnst/alloc.scm 736 */
					obj_t BgL_oldz00_2102;

					{
						obj_t BgL_listz00_2116;

						BgL_listz00_2116 = BGl_za2uint64zd2envza2zd2zzcnst_allocz00;
					BgL_zc3z04anonymousza32041ze3z87_2117:
						if (NULLP(BgL_listz00_2116))
							{	/* Cnst/alloc.scm 730 */
								BgL_oldz00_2102 = BFALSE;
							}
						else
							{	/* Cnst/alloc.scm 732 */
								bool_t BgL_test2528z00_4996;

								{	/* Cnst/alloc.scm 732 */
									obj_t BgL_arg2049z00_2124;

									{	/* Cnst/alloc.scm 732 */
										obj_t BgL_pairz00_3138;

										BgL_pairz00_3138 = CAR(((obj_t) BgL_listz00_2116));
										BgL_arg2049z00_2124 = CAR(BgL_pairz00_3138);
									}
									{	/* Cnst/alloc.scm 732 */
										uint64_t BgL_n1z00_3139;

										BgL_n1z00_3139 = BGL_BINT64_TO_INT64(BgL_arg2049z00_2124);
										BgL_test2528z00_4996 = (BgL_n1z00_3139 == BgL_uint64z00_64);
									}
								}
								if (BgL_test2528z00_4996)
									{	/* Cnst/alloc.scm 733 */
										obj_t BgL_pairz00_3142;

										BgL_pairz00_3142 = CAR(((obj_t) BgL_listz00_2116));
										BgL_oldz00_2102 = CDR(BgL_pairz00_3142);
									}
								else
									{
										obj_t BgL_listz00_5005;

										BgL_listz00_5005 = CDR(((obj_t) BgL_listz00_2116));
										BgL_listz00_2116 = BgL_listz00_5005;
										goto BgL_zc3z04anonymousza32041ze3z87_2117;
									}
							}
					}
					if (CBOOL(BgL_oldz00_2102))
						{	/* Cnst/alloc.scm 739 */
							BgL_refz00_bglt BgL_new1187z00_2103;

							{	/* Cnst/alloc.scm 740 */
								BgL_refz00_bglt BgL_new1186z00_2104;

								BgL_new1186z00_2104 =
									((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_refz00_bgl))));
								{	/* Cnst/alloc.scm 740 */
									long BgL_arg2034z00_2105;

									BgL_arg2034z00_2105 = BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1186z00_2104),
										BgL_arg2034z00_2105);
								}
								{	/* Cnst/alloc.scm 740 */
									BgL_objectz00_bglt BgL_tmpz00_5014;

									BgL_tmpz00_5014 = ((BgL_objectz00_bglt) BgL_new1186z00_2104);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5014, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1186z00_2104);
								BgL_new1187z00_2103 = BgL_new1186z00_2104;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1187z00_2103)))->BgL_locz00) =
								((obj_t) BgL_locz00_65), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1187z00_2103)))->BgL_typez00) =
								((BgL_typez00_bglt) (((BgL_variablez00_bglt)
											COBJECT(((BgL_variablez00_bglt) BgL_oldz00_2102)))->
										BgL_typez00)), BUNSPEC);
							((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
												BgL_new1187z00_2103)))->BgL_variablez00) =
								((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
										BgL_oldz00_2102)), BUNSPEC);
							return ((BgL_nodez00_bglt) BgL_new1187z00_2103);
						}
					else
						{
							BgL_refz00_bglt BgL_auxz00_5028;

							{	/* Cnst/alloc.scm 717 */
								BgL_globalz00_bglt BgL_varz00_2107;

								{	/* Cnst/alloc.scm 717 */
									obj_t BgL_arg2038z00_2112;

									{	/* Cnst/alloc.scm 717 */
										obj_t BgL_arg2039z00_2113;

										BgL_arg2039z00_2113 =
											BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(42));
										BgL_arg2038z00_2112 =
											BGl_makezd2typedzd2identz00zzast_identz00
											(BgL_arg2039z00_2113, CNST_TABLE_REF(43));
									}
									BgL_varz00_2107 =
										BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2
										(BgL_arg2038z00_2112, BGl_za2moduleza2z00zzmodule_modulez00,
										BGL_UINT64_TO_BUINT64(BgL_uint64z00_64), CNST_TABLE_REF(44),
										BgL_locz00_65);
								}
								{	/* Cnst/alloc.scm 722 */
									obj_t BgL_arg2036z00_2108;

									BgL_arg2036z00_2108 =
										MAKE_YOUNG_PAIR(BGL_UINT64_TO_BUINT64(BgL_uint64z00_64),
										((obj_t) BgL_varz00_2107));
									BGl_za2uint64zd2envza2zd2zzcnst_allocz00 =
										MAKE_YOUNG_PAIR(BgL_arg2036z00_2108,
										BGl_za2uint64zd2envza2zd2zzcnst_allocz00);
								}
								{	/* Cnst/alloc.scm 723 */
									BgL_refz00_bglt BgL_new1185z00_2109;

									{	/* Cnst/alloc.scm 724 */
										BgL_refz00_bglt BgL_new1184z00_2110;

										BgL_new1184z00_2110 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Cnst/alloc.scm 724 */
											long BgL_arg2037z00_2111;

											BgL_arg2037z00_2111 =
												BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1184z00_2110),
												BgL_arg2037z00_2111);
										}
										{	/* Cnst/alloc.scm 724 */
											BgL_objectz00_bglt BgL_tmpz00_5044;

											BgL_tmpz00_5044 =
												((BgL_objectz00_bglt) BgL_new1184z00_2110);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5044, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1184z00_2110);
										BgL_new1185z00_2109 = BgL_new1184z00_2110;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1185z00_2109)))->
											BgL_locz00) = ((obj_t) BgL_locz00_65), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1185z00_2109)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_variablez00_bglt)
													COBJECT(((BgL_variablez00_bglt) BgL_varz00_2107)))->
												BgL_typez00)), BUNSPEC);
									((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
														BgL_new1185z00_2109)))->BgL_variablez00) =
										((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
												BgL_varz00_2107)), BUNSPEC);
									BgL_auxz00_5028 = BgL_new1185z00_2109;
							}}
							return ((BgL_nodez00_bglt) BgL_auxz00_5028);
						}
				}
			}
		}

	}



/* &cnst-alloc-uint64 */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2uint64z62zzcnst_allocz00(obj_t
		BgL_envz00_3536, obj_t BgL_uint64z00_3537, obj_t BgL_locz00_3538)
	{
		{	/* Cnst/alloc.scm 715 */
			return
				BGl_cnstzd2alloczd2uint64z00zzcnst_allocz00(BGL_BINT64_TO_INT64
				(BgL_uint64z00_3537), BgL_locz00_3538);
		}

	}



/* cnst-alloc-list */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2listz00zzcnst_allocz00(obj_t BgL_pairz00_66,
		obj_t BgL_locz00_67)
	{
		{	/* Cnst/alloc.scm 749 */
			{

				{	/* Cnst/alloc.scm 799 */
					obj_t BgL_oldz00_2132;

					{	/* Cnst/alloc.scm 799 */
						obj_t BgL__andtest_1202z00_2139;

						BgL__andtest_1202z00_2139 =
							BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00;
						if (CBOOL(BgL__andtest_1202z00_2139))
							{
								obj_t BgL_envz00_2141;

								BgL_envz00_2141 = BGl_za2listzd2envza2zd2zzcnst_allocz00;
							BgL_zc3z04anonymousza32057ze3z87_2142:
								if (NULLP(BgL_envz00_2141))
									{	/* Cnst/alloc.scm 802 */
										BgL_oldz00_2132 = BFALSE;
									}
								else
									{	/* Cnst/alloc.scm 804 */
										bool_t BgL_test2532z00_5064;

										{	/* Cnst/alloc.scm 804 */
											obj_t BgL_arg2063z00_2148;

											{	/* Cnst/alloc.scm 69 */
												obj_t BgL_sz00_3189;

												BgL_sz00_3189 = CAR(((obj_t) BgL_envz00_2141));
												BgL_arg2063z00_2148 =
													STRUCT_REF(BgL_sz00_3189, (int) (0L));
											}
											BgL_test2532z00_5064 =
												BGl_cnstzd2equalzf3z21zzcnst_allocz00
												(BgL_arg2063z00_2148, BgL_pairz00_66);
										}
										if (BgL_test2532z00_5064)
											{	/* Cnst/alloc.scm 804 */
												BgL_oldz00_2132 = CAR(((obj_t) BgL_envz00_2141));
											}
										else
											{	/* Cnst/alloc.scm 807 */
												obj_t BgL_arg2062z00_2147;

												BgL_arg2062z00_2147 = CDR(((obj_t) BgL_envz00_2141));
												{
													obj_t BgL_envz00_5074;

													BgL_envz00_5074 = BgL_arg2062z00_2147;
													BgL_envz00_2141 = BgL_envz00_5074;
													goto BgL_zc3z04anonymousza32057ze3z87_2142;
												}
											}
									}
							}
						else
							{	/* Cnst/alloc.scm 799 */
								BgL_oldz00_2132 = BFALSE;
							}
					}
					if (CBOOL(BgL_oldz00_2132))
						{	/* Cnst/alloc.scm 809 */
							if (
								(BGl_za2initzd2modeza2zd2zzengine_paramz00 ==
									CNST_TABLE_REF(14)))
								{	/* Cnst/alloc.scm 811 */
									BgL_refz00_bglt BgL_new1205z00_2133;

									{	/* Cnst/alloc.scm 812 */
										BgL_refz00_bglt BgL_new1204z00_2136;

										BgL_new1204z00_2136 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Cnst/alloc.scm 812 */
											long BgL_arg2055z00_2137;

											BgL_arg2055z00_2137 =
												BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1204z00_2136),
												BgL_arg2055z00_2137);
										}
										{	/* Cnst/alloc.scm 812 */
											BgL_objectz00_bglt BgL_tmpz00_5084;

											BgL_tmpz00_5084 =
												((BgL_objectz00_bglt) BgL_new1204z00_2136);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5084, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1204z00_2136);
										BgL_new1205z00_2133 = BgL_new1204z00_2136;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1205z00_2133)))->
											BgL_locz00) = ((obj_t) BgL_locz00_67), BUNSPEC);
									{
										BgL_typez00_bglt BgL_auxz00_5090;

										{	/* Cnst/alloc.scm 814 */
											BgL_typez00_bglt BgL_arg2051z00_2134;

											BgL_arg2051z00_2134 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															STRUCT_REF(
																((obj_t) BgL_oldz00_2132),
																(int) (1L)))))->BgL_typez00);
											BgL_auxz00_5090 =
												BGl_strictzd2nodezd2typez00zzast_nodez00(
												((BgL_typez00_bglt) BGl_za2pairza2z00zztype_cachez00),
												BgL_arg2051z00_2134);
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1205z00_2133)))->
												BgL_typez00) =
											((BgL_typez00_bglt) BgL_auxz00_5090), BUNSPEC);
									}
									((((BgL_varz00_bglt) COBJECT(
													((BgL_varz00_bglt) BgL_new1205z00_2133)))->
											BgL_variablez00) =
										((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
												STRUCT_REF(((obj_t) BgL_oldz00_2132), (int) (1L)))),
										BUNSPEC);
									return ((BgL_nodez00_bglt) BgL_new1205z00_2133);
								}
							else
								{	/* Cnst/alloc.scm 816 */
									obj_t BgL_arg2056z00_2138;

									BgL_arg2056z00_2138 =
										STRUCT_REF(((obj_t) BgL_oldz00_2132), (int) (1L));
									return
										((BgL_nodez00_bglt)
										BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00
										(BgL_arg2056z00_2138, BGl_za2pairza2z00zztype_cachez00,
											BgL_locz00_67));
								}
						}
					else
						{	/* Cnst/alloc.scm 809 */
							if (
								(BGl_za2initzd2modeza2zd2zzengine_paramz00 ==
									CNST_TABLE_REF(14)))
								{
									BgL_refz00_bglt BgL_auxz00_5115;

									{	/* Cnst/alloc.scm 779 */
										BgL_globalz00_bglt BgL_varz00_2187;

										{	/* Cnst/alloc.scm 779 */
											obj_t BgL_arg2104z00_2203;

											{	/* Cnst/alloc.scm 779 */
												obj_t BgL_arg2105z00_2204;

												BgL_arg2105z00_2204 =
													BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF
													(45));
												BgL_arg2104z00_2203 =
													BGl_makezd2typedzd2identz00zzast_identz00
													(BgL_arg2105z00_2204, CNST_TABLE_REF(46));
											}
											BgL_varz00_2187 =
												BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2
												(BgL_arg2104z00_2203,
												BGl_za2moduleza2z00zzmodule_modulez00,
												CNST_TABLE_REF(47), CNST_TABLE_REF(3));
										}
										if (CBOOL(BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00))
											{	/* Cnst/alloc.scm 784 */
												obj_t BgL_arg2090z00_2188;

												{	/* Cnst/alloc.scm 69 */
													obj_t BgL_newz00_3174;

													BgL_newz00_3174 =
														create_struct(CNST_TABLE_REF(9), (int) (2L));
													{	/* Cnst/alloc.scm 69 */
														obj_t BgL_auxz00_5130;
														int BgL_tmpz00_5128;

														BgL_auxz00_5130 = ((obj_t) BgL_varz00_2187);
														BgL_tmpz00_5128 = (int) (1L);
														STRUCT_SET(BgL_newz00_3174, BgL_tmpz00_5128,
															BgL_auxz00_5130);
													}
													{	/* Cnst/alloc.scm 69 */
														int BgL_tmpz00_5133;

														BgL_tmpz00_5133 = (int) (0L);
														STRUCT_SET(BgL_newz00_3174, BgL_tmpz00_5133,
															BgL_pairz00_66);
													}
													BgL_arg2090z00_2188 = BgL_newz00_3174;
												}
												BGl_za2listzd2envza2zd2zzcnst_allocz00 =
													MAKE_YOUNG_PAIR(BgL_arg2090z00_2188,
													BGl_za2listzd2envza2zd2zzcnst_allocz00);
											}
										else
											{	/* Cnst/alloc.scm 783 */
												BFALSE;
											}
										{	/* Cnst/alloc.scm 785 */
											obj_t BgL_arg2091z00_2189;

											{	/* Cnst/alloc.scm 785 */
												obj_t BgL_arg2093z00_2190;

												{	/* Cnst/alloc.scm 785 */
													obj_t BgL_arg2094z00_2191;
													obj_t BgL_arg2095z00_2192;

													{	/* Cnst/alloc.scm 785 */
														obj_t BgL_arg2096z00_2193;

														{	/* Cnst/alloc.scm 785 */
															obj_t BgL_arg2097z00_2194;
															obj_t BgL_arg2098z00_2195;

															BgL_arg2097z00_2194 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt) BgL_varz00_2187)))->
																BgL_idz00);
															BgL_arg2098z00_2195 =
																MAKE_YOUNG_PAIR((((BgL_globalz00_bglt)
																		COBJECT(BgL_varz00_2187))->BgL_modulez00),
																BNIL);
															BgL_arg2096z00_2193 =
																MAKE_YOUNG_PAIR(BgL_arg2097z00_2194,
																BgL_arg2098z00_2195);
														}
														BgL_arg2094z00_2191 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
															BgL_arg2096z00_2193);
													}
													{	/* Cnst/alloc.scm 786 */
														BgL_nodez00_bglt BgL_arg2100z00_2197;

														{	/* Cnst/alloc.scm 786 */
															BgL_nodez00_bglt BgL_arg2101z00_2198;
															BgL_typez00_bglt BgL_arg2102z00_2199;

															BgL_arg2101z00_2198 =
																BGl_loopze70ze7zzcnst_allocz00(BgL_locz00_67,
																BgL_pairz00_66);
															BgL_arg2102z00_2199 =
																BGl_strictzd2nodezd2typez00zzast_nodez00((
																	(BgL_typez00_bglt)
																	BGl_za2pairza2z00zztype_cachez00),
																((BgL_typez00_bglt)
																	BGl_za2objza2z00zztype_cachez00));
															BgL_arg2100z00_2197 =
																BGl_coercez12z12zzcoerce_coercez00
																(BgL_arg2101z00_2198, ((obj_t) BgL_varz00_2187),
																BgL_arg2102z00_2199, ((bool_t) 0));
														}
														BgL_arg2095z00_2192 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg2100z00_2197), BNIL);
													}
													BgL_arg2093z00_2190 =
														MAKE_YOUNG_PAIR(BgL_arg2094z00_2191,
														BgL_arg2095z00_2192);
												}
												BgL_arg2091z00_2189 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
													BgL_arg2093z00_2190);
											}
											BGl_za2globalzd2sexpza2zd2zzcnst_allocz00 =
												MAKE_YOUNG_PAIR(BgL_arg2091z00_2189,
												BGl_za2globalzd2sexpza2zd2zzcnst_allocz00);
										}
										{	/* Cnst/alloc.scm 788 */
											BgL_refz00_bglt BgL_new1201z00_2200;

											{	/* Cnst/alloc.scm 789 */
												BgL_refz00_bglt BgL_new1200z00_2201;

												BgL_new1200z00_2201 =
													((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_refz00_bgl))));
												{	/* Cnst/alloc.scm 789 */
													long BgL_arg2103z00_2202;

													BgL_arg2103z00_2202 =
														BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1200z00_2201),
														BgL_arg2103z00_2202);
												}
												{	/* Cnst/alloc.scm 789 */
													BgL_objectz00_bglt BgL_tmpz00_5160;

													BgL_tmpz00_5160 =
														((BgL_objectz00_bglt) BgL_new1200z00_2201);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5160, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1200z00_2201);
												BgL_new1201z00_2200 = BgL_new1200z00_2201;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1201z00_2200)))->
													BgL_locz00) = ((obj_t) BgL_locz00_67), BUNSPEC);
											((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_new1201z00_2200)))->BgL_typez00) =
												((BgL_typez00_bglt) (((BgL_variablez00_bglt)
															COBJECT(((BgL_variablez00_bglt)
																	BgL_varz00_2187)))->BgL_typez00)), BUNSPEC);
											((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																BgL_new1201z00_2200)))->BgL_variablez00) =
												((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
														BgL_varz00_2187)), BUNSPEC);
											BgL_auxz00_5115 = BgL_new1201z00_2200;
									}}
									return ((BgL_nodez00_bglt) BgL_auxz00_5115);
								}
							else
								{
									BgL_appz00_bglt BgL_auxz00_5174;

									{	/* Cnst/alloc.scm 793 */
										long BgL_offsetz00_2206;

										BgL_offsetz00_2206 =
											BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00;
										BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00 =
											(1L + BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00);
										BGl_za2globalzd2setza2zd2zzcnst_allocz00 =
											MAKE_YOUNG_PAIR(BgL_pairz00_66,
											BGl_za2globalzd2setza2zd2zzcnst_allocz00);
										if (CBOOL(BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00))
											{	/* Cnst/alloc.scm 797 */
												obj_t BgL_arg2107z00_2207;

												{	/* Cnst/alloc.scm 69 */
													obj_t BgL_newz00_3185;

													BgL_newz00_3185 =
														create_struct(CNST_TABLE_REF(9), (int) (2L));
													{	/* Cnst/alloc.scm 69 */
														obj_t BgL_auxz00_5184;
														int BgL_tmpz00_5182;

														BgL_auxz00_5184 = BINT(BgL_offsetz00_2206);
														BgL_tmpz00_5182 = (int) (1L);
														STRUCT_SET(BgL_newz00_3185, BgL_tmpz00_5182,
															BgL_auxz00_5184);
													}
													{	/* Cnst/alloc.scm 69 */
														int BgL_tmpz00_5187;

														BgL_tmpz00_5187 = (int) (0L);
														STRUCT_SET(BgL_newz00_3185, BgL_tmpz00_5187,
															BgL_pairz00_66);
													}
													BgL_arg2107z00_2207 = BgL_newz00_3185;
												}
												BGl_za2listzd2envza2zd2zzcnst_allocz00 =
													MAKE_YOUNG_PAIR(BgL_arg2107z00_2207,
													BGl_za2listzd2envza2zd2zzcnst_allocz00);
											}
										else
											{	/* Cnst/alloc.scm 796 */
												BFALSE;
											}
										BgL_auxz00_5174 =
											BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00(BINT
											(BgL_offsetz00_2206), BGl_za2pairza2z00zztype_cachez00,
											BgL_locz00_67);
									}
									return ((BgL_nodez00_bglt) BgL_auxz00_5174);
								}
						}
				}
			}
		}

	}



/* loop~0 */
	BgL_nodez00_bglt BGl_loopze70ze7zzcnst_allocz00(obj_t BgL_locz00_3574,
		obj_t BgL_pairz00_2154)
	{
		{	/* Cnst/alloc.scm 751 */
			if (NULLP(BgL_pairz00_2154))
				{	/* Cnst/alloc.scm 754 */
					BgL_literalz00_bglt BgL_new1190z00_2157;

					{	/* Cnst/alloc.scm 755 */
						BgL_literalz00_bglt BgL_new1189z00_2159;

						BgL_new1189z00_2159 =
							((BgL_literalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_literalz00_bgl))));
						{	/* Cnst/alloc.scm 755 */
							long BgL_arg2069z00_2160;

							BgL_arg2069z00_2160 = BGL_CLASS_NUM(BGl_literalz00zzast_nodez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1189z00_2159),
								BgL_arg2069z00_2160);
						}
						{	/* Cnst/alloc.scm 755 */
							BgL_objectz00_bglt BgL_tmpz00_5200;

							BgL_tmpz00_5200 = ((BgL_objectz00_bglt) BgL_new1189z00_2159);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5200, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1189z00_2159);
						BgL_new1190z00_2157 = BgL_new1189z00_2159;
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1190z00_2157)))->BgL_locz00) =
						((obj_t) BgL_locz00_3574), BUNSPEC);
					{
						BgL_typez00_bglt BgL_auxz00_5206;

						{	/* Cnst/alloc.scm 756 */
							BgL_typez00_bglt BgL_arg2068z00_2158;

							BgL_arg2068z00_2158 =
								BGl_getzd2typezd2atomz00zztype_typeofz00(BNIL);
							BgL_auxz00_5206 =
								BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_arg2068z00_2158,
								((BgL_typez00_bglt) BGl_za2bnilza2z00zztype_cachez00));
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_new1190z00_2157)))->BgL_typez00) =
							((BgL_typez00_bglt) BgL_auxz00_5206), BUNSPEC);
					}
					((((BgL_atomz00_bglt) COBJECT(
									((BgL_atomz00_bglt) BgL_new1190z00_2157)))->BgL_valuez00) =
						((obj_t) BNIL), BUNSPEC);
					return ((BgL_nodez00_bglt) BgL_new1190z00_2157);
				}
			else
				{	/* Cnst/alloc.scm 753 */
					if (PAIRP(BgL_pairz00_2154))
						{	/* Cnst/alloc.scm 764 */
							BgL_appz00_bglt BgL_new1194z00_2162;

							{	/* Cnst/alloc.scm 765 */
								BgL_appz00_bglt BgL_new1193z00_2178;

								BgL_new1193z00_2178 =
									((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_appz00_bgl))));
								{	/* Cnst/alloc.scm 765 */
									long BgL_arg2084z00_2179;

									BgL_arg2084z00_2179 = BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1193z00_2178),
										BgL_arg2084z00_2179);
								}
								{	/* Cnst/alloc.scm 765 */
									BgL_objectz00_bglt BgL_tmpz00_5221;

									BgL_tmpz00_5221 = ((BgL_objectz00_bglt) BgL_new1193z00_2178);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5221, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1193z00_2178);
								BgL_new1194z00_2162 = BgL_new1193z00_2178;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1194z00_2162)))->BgL_locz00) =
								((obj_t) BgL_locz00_3574), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1194z00_2162)))->BgL_typez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BGl_za2pairza2z00zztype_cachez00)), BUNSPEC);
							((((BgL_nodezf2effectzf2_bglt)
										COBJECT(((BgL_nodezf2effectzf2_bglt)
												BgL_new1194z00_2162)))->BgL_sidezd2effectzd2) =
								((obj_t) BUNSPEC), BUNSPEC);
							((((BgL_nodezf2effectzf2_bglt)
										COBJECT(((BgL_nodezf2effectzf2_bglt)
												BgL_new1194z00_2162)))->BgL_keyz00) =
								((obj_t) BINT(-1L)), BUNSPEC);
							{
								BgL_varz00_bglt BgL_auxz00_5235;

								{	/* Cnst/alloc.scm 767 */
									BgL_refz00_bglt BgL_new1196z00_2163;

									{	/* Cnst/alloc.scm 768 */
										BgL_refz00_bglt BgL_new1195z00_2164;

										BgL_new1195z00_2164 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Cnst/alloc.scm 768 */
											long BgL_arg2072z00_2165;

											{	/* Cnst/alloc.scm 768 */
												obj_t BgL_classz00_3157;

												BgL_classz00_3157 = BGl_refz00zzast_nodez00;
												BgL_arg2072z00_2165 = BGL_CLASS_NUM(BgL_classz00_3157);
											}
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1195z00_2164),
												BgL_arg2072z00_2165);
										}
										{	/* Cnst/alloc.scm 768 */
											BgL_objectz00_bglt BgL_tmpz00_5240;

											BgL_tmpz00_5240 =
												((BgL_objectz00_bglt) BgL_new1195z00_2164);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5240, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1195z00_2164);
										BgL_new1196z00_2163 = BgL_new1195z00_2164;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1196z00_2163)))->
											BgL_locz00) = ((obj_t) BgL_locz00_3574), BUNSPEC);
									{
										BgL_typez00_bglt BgL_auxz00_5246;

										{	/* Cnst/alloc.scm 769 */
											BgL_variablez00_bglt BgL_oz00_3161;

											BgL_oz00_3161 =
												((BgL_variablez00_bglt)
												BGl_za2consza2z00zzcnst_cachez00);
											BgL_auxz00_5246 =
												(((BgL_variablez00_bglt) COBJECT(BgL_oz00_3161))->
												BgL_typez00);
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1196z00_2163)))->
												BgL_typez00) =
											((BgL_typez00_bglt) BgL_auxz00_5246), BUNSPEC);
									}
									((((BgL_varz00_bglt) COBJECT(
													((BgL_varz00_bglt) BgL_new1196z00_2163)))->
											BgL_variablez00) =
										((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
												BGl_za2consza2z00zzcnst_cachez00)), BUNSPEC);
									BgL_auxz00_5235 = ((BgL_varz00_bglt) BgL_new1196z00_2163);
								}
								((((BgL_appz00_bglt) COBJECT(BgL_new1194z00_2162))->
										BgL_funz00) = ((BgL_varz00_bglt) BgL_auxz00_5235), BUNSPEC);
							}
							{
								obj_t BgL_auxz00_5256;

								{	/* Cnst/alloc.scm 771 */
									BgL_nodez00_bglt BgL_arg2074z00_2166;
									BgL_nodez00_bglt BgL_arg2075z00_2167;

									{	/* Cnst/alloc.scm 771 */
										BgL_kwotez00_bglt BgL_arg2078z00_2170;

										{	/* Cnst/alloc.scm 771 */
											BgL_kwotez00_bglt BgL_new1198z00_2171;

											{	/* Cnst/alloc.scm 772 */
												BgL_kwotez00_bglt BgL_new1197z00_2175;

												BgL_new1197z00_2175 =
													((BgL_kwotez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_kwotez00_bgl))));
												{	/* Cnst/alloc.scm 772 */
													long BgL_arg2082z00_2176;

													{	/* Cnst/alloc.scm 772 */
														obj_t BgL_classz00_3162;

														BgL_classz00_3162 = BGl_kwotez00zzast_nodez00;
														BgL_arg2082z00_2176 =
															BGL_CLASS_NUM(BgL_classz00_3162);
													}
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1197z00_2175),
														BgL_arg2082z00_2176);
												}
												{	/* Cnst/alloc.scm 772 */
													BgL_objectz00_bglt BgL_tmpz00_5261;

													BgL_tmpz00_5261 =
														((BgL_objectz00_bglt) BgL_new1197z00_2175);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5261, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1197z00_2175);
												BgL_new1198z00_2171 = BgL_new1197z00_2175;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1198z00_2171)))->
													BgL_locz00) = ((obj_t) BgL_locz00_3574), BUNSPEC);
											{
												BgL_typez00_bglt BgL_auxz00_5267;

												{	/* Cnst/alloc.scm 774 */
													BgL_typez00_bglt BgL_arg2079z00_2172;
													BgL_typez00_bglt BgL_arg2080z00_2173;

													{	/* Cnst/alloc.scm 774 */
														obj_t BgL_arg2081z00_2174;

														BgL_arg2081z00_2174 = CAR(BgL_pairz00_2154);
														BgL_arg2079z00_2172 =
															BGl_getzd2typezd2kwotez00zztype_typeofz00
															(BgL_arg2081z00_2174);
													}
													BgL_arg2080z00_2173 =
														BGl_getzd2defaultzd2typez00zztype_cachez00();
													BgL_auxz00_5267 =
														BGl_strictzd2nodezd2typez00zzast_nodez00
														(BgL_arg2079z00_2172, BgL_arg2080z00_2173);
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1198z00_2171)))->
														BgL_typez00) =
													((BgL_typez00_bglt) BgL_auxz00_5267), BUNSPEC);
											}
											((((BgL_kwotez00_bglt) COBJECT(BgL_new1198z00_2171))->
													BgL_valuez00) =
												((obj_t) CAR(BgL_pairz00_2154)), BUNSPEC);
											BgL_arg2078z00_2170 = BgL_new1198z00_2171;
										}
										BgL_arg2074z00_2166 =
											BGl_cnstz12z12zzcnst_nodez00(
											((BgL_nodez00_bglt) BgL_arg2078z00_2170));
									}
									{	/* Cnst/alloc.scm 777 */
										obj_t BgL_arg2083z00_2177;

										BgL_arg2083z00_2177 = CDR(BgL_pairz00_2154);
										BgL_arg2075z00_2167 =
											BGl_loopze70ze7zzcnst_allocz00(BgL_locz00_3574,
											BgL_arg2083z00_2177);
									}
									{	/* Cnst/alloc.scm 771 */
										obj_t BgL_list2076z00_2168;

										{	/* Cnst/alloc.scm 771 */
											obj_t BgL_arg2077z00_2169;

											BgL_arg2077z00_2169 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg2075z00_2167), BNIL);
											BgL_list2076z00_2168 =
												MAKE_YOUNG_PAIR(
												((obj_t) BgL_arg2074z00_2166), BgL_arg2077z00_2169);
										}
										BgL_auxz00_5256 = BgL_list2076z00_2168;
								}}
								((((BgL_appz00_bglt) COBJECT(BgL_new1194z00_2162))->
										BgL_argsz00) = ((obj_t) BgL_auxz00_5256), BUNSPEC);
							}
							((((BgL_appz00_bglt) COBJECT(BgL_new1194z00_2162))->
									BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
							return ((BgL_nodez00_bglt) BgL_new1194z00_2162);
						}
					else
						{	/* Cnst/alloc.scm 759 */
							BgL_kwotez00_bglt BgL_arg2086z00_2180;

							{	/* Cnst/alloc.scm 759 */
								BgL_kwotez00_bglt BgL_new1192z00_2181;

								{	/* Cnst/alloc.scm 760 */
									BgL_kwotez00_bglt BgL_new1191z00_2183;

									BgL_new1191z00_2183 =
										((BgL_kwotez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_kwotez00_bgl))));
									{	/* Cnst/alloc.scm 760 */
										long BgL_arg2088z00_2184;

										BgL_arg2088z00_2184 =
											BGL_CLASS_NUM(BGl_kwotez00zzast_nodez00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1191z00_2183),
											BgL_arg2088z00_2184);
									}
									{	/* Cnst/alloc.scm 760 */
										BgL_objectz00_bglt BgL_tmpz00_5291;

										BgL_tmpz00_5291 =
											((BgL_objectz00_bglt) BgL_new1191z00_2183);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5291, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1191z00_2183);
									BgL_new1192z00_2181 = BgL_new1191z00_2183;
								}
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_new1192z00_2181)))->
										BgL_locz00) = ((obj_t) BgL_locz00_3574), BUNSPEC);
								{
									BgL_typez00_bglt BgL_auxz00_5297;

									{	/* Cnst/alloc.scm 761 */
										BgL_typez00_bglt BgL_arg2087z00_2182;

										BgL_arg2087z00_2182 =
											BGl_getzd2typezd2kwotez00zztype_typeofz00
											(BgL_pairz00_2154);
										BgL_auxz00_5297 =
											BGl_strictzd2nodezd2typez00zzast_nodez00
											(BgL_arg2087z00_2182,
											((BgL_typez00_bglt) BGl_za2pairza2z00zztype_cachez00));
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1192z00_2181)))->
											BgL_typez00) =
										((BgL_typez00_bglt) BgL_auxz00_5297), BUNSPEC);
								}
								((((BgL_kwotez00_bglt) COBJECT(BgL_new1192z00_2181))->
										BgL_valuez00) = ((obj_t) BgL_pairz00_2154), BUNSPEC);
								BgL_arg2086z00_2180 = BgL_new1192z00_2181;
							}
							return
								BGl_cnstz12z12zzcnst_nodez00(
								((BgL_nodez00_bglt) BgL_arg2086z00_2180));
						}
				}
		}

	}



/* &cnst-alloc-list */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2listz62zzcnst_allocz00(obj_t
		BgL_envz00_3539, obj_t BgL_pairz00_3540, obj_t BgL_locz00_3541)
	{
		{	/* Cnst/alloc.scm 749 */
			return
				BGl_cnstzd2alloczd2listz00zzcnst_allocz00(BgL_pairz00_3540,
				BgL_locz00_3541);
		}

	}



/* cnst-alloc-vector */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2vectorz00zzcnst_allocz00(obj_t BgL_vecz00_68,
		obj_t BgL_locz00_69)
	{
		{	/* Cnst/alloc.scm 825 */
			{

				{	/* Cnst/alloc.scm 904 */
					obj_t BgL_oldz00_2214;

					{	/* Cnst/alloc.scm 904 */
						obj_t BgL__andtest_1229z00_2221;

						BgL__andtest_1229z00_2221 =
							BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00;
						if (CBOOL(BgL__andtest_1229z00_2221))
							{
								obj_t BgL_envz00_2223;

								BgL_envz00_2223 = BGl_za2vectorzd2envza2zd2zzcnst_allocz00;
							BgL_zc3z04anonymousza32112ze3z87_2224:
								if (NULLP(BgL_envz00_2223))
									{	/* Cnst/alloc.scm 907 */
										BgL_oldz00_2214 = BFALSE;
									}
								else
									{	/* Cnst/alloc.scm 909 */
										bool_t BgL_test2542z00_5311;

										{	/* Cnst/alloc.scm 909 */
											obj_t BgL_arg2118z00_2230;

											{	/* Cnst/alloc.scm 69 */
												obj_t BgL_sz00_3267;

												BgL_sz00_3267 = CAR(((obj_t) BgL_envz00_2223));
												BgL_arg2118z00_2230 =
													STRUCT_REF(BgL_sz00_3267, (int) (0L));
											}
											BgL_test2542z00_5311 =
												BGl_cnstzd2equalzf3z21zzcnst_allocz00
												(BgL_arg2118z00_2230, BgL_vecz00_68);
										}
										if (BgL_test2542z00_5311)
											{	/* Cnst/alloc.scm 909 */
												BgL_oldz00_2214 = CAR(((obj_t) BgL_envz00_2223));
											}
										else
											{	/* Cnst/alloc.scm 912 */
												obj_t BgL_arg2117z00_2229;

												BgL_arg2117z00_2229 = CDR(((obj_t) BgL_envz00_2223));
												{
													obj_t BgL_envz00_5321;

													BgL_envz00_5321 = BgL_arg2117z00_2229;
													BgL_envz00_2223 = BgL_envz00_5321;
													goto BgL_zc3z04anonymousza32112ze3z87_2224;
												}
											}
									}
							}
						else
							{	/* Cnst/alloc.scm 904 */
								BgL_oldz00_2214 = BFALSE;
							}
					}
					if (CBOOL(BgL_oldz00_2214))
						{	/* Cnst/alloc.scm 914 */
							if (
								(BGl_za2initzd2modeza2zd2zzengine_paramz00 ==
									CNST_TABLE_REF(14)))
								{	/* Cnst/alloc.scm 916 */
									BgL_refz00_bglt BgL_new1231z00_2215;

									{	/* Cnst/alloc.scm 917 */
										BgL_refz00_bglt BgL_new1230z00_2218;

										BgL_new1230z00_2218 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Cnst/alloc.scm 917 */
											long BgL_arg2110z00_2219;

											BgL_arg2110z00_2219 =
												BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1230z00_2218),
												BgL_arg2110z00_2219);
										}
										{	/* Cnst/alloc.scm 917 */
											BgL_objectz00_bglt BgL_tmpz00_5331;

											BgL_tmpz00_5331 =
												((BgL_objectz00_bglt) BgL_new1230z00_2218);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5331, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1230z00_2218);
										BgL_new1231z00_2215 = BgL_new1230z00_2218;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1231z00_2215)))->
											BgL_locz00) = ((obj_t) BgL_locz00_69), BUNSPEC);
									{
										BgL_typez00_bglt BgL_auxz00_5337;

										{	/* Cnst/alloc.scm 919 */
											BgL_typez00_bglt BgL_arg2108z00_2216;

											BgL_arg2108z00_2216 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															STRUCT_REF(
																((obj_t) BgL_oldz00_2214),
																(int) (1L)))))->BgL_typez00);
											BgL_auxz00_5337 =
												BGl_strictzd2nodezd2typez00zzast_nodez00(
												((BgL_typez00_bglt) BGl_za2vectorza2z00zztype_cachez00),
												BgL_arg2108z00_2216);
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1231z00_2215)))->
												BgL_typez00) =
											((BgL_typez00_bglt) BgL_auxz00_5337), BUNSPEC);
									}
									((((BgL_varz00_bglt) COBJECT(
													((BgL_varz00_bglt) BgL_new1231z00_2215)))->
											BgL_variablez00) =
										((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
												STRUCT_REF(((obj_t) BgL_oldz00_2214), (int) (1L)))),
										BUNSPEC);
									return ((BgL_nodez00_bglt) BgL_new1231z00_2215);
								}
							else
								{	/* Cnst/alloc.scm 921 */
									obj_t BgL_arg2111z00_2220;

									BgL_arg2111z00_2220 =
										STRUCT_REF(((obj_t) BgL_oldz00_2214), (int) (1L));
									return
										((BgL_nodez00_bglt)
										BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00
										(BgL_arg2111z00_2220, BGl_za2vectorza2z00zztype_cachez00,
											BgL_locz00_69));
								}
						}
					else
						{	/* Cnst/alloc.scm 914 */
							if (
								(BGl_za2initzd2modeza2zd2zzengine_paramz00 ==
									CNST_TABLE_REF(14)))
								{
									BgL_refz00_bglt BgL_auxz00_5362;

									{	/* Cnst/alloc.scm 892 */
										BgL_globalz00_bglt BgL_varz00_2292;

										{	/* Cnst/alloc.scm 892 */
											obj_t BgL_arg2170z00_2307;

											{	/* Cnst/alloc.scm 892 */
												obj_t BgL_arg2171z00_2308;

												BgL_arg2171z00_2308 =
													BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF
													(49));
												BgL_arg2170z00_2307 =
													BGl_makezd2typedzd2identz00zzast_identz00
													(BgL_arg2171z00_2308, CNST_TABLE_REF(49));
											}
											BgL_varz00_2292 =
												BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2
												(BgL_arg2170z00_2307,
												BGl_za2moduleza2z00zzmodule_modulez00,
												CNST_TABLE_REF(2), CNST_TABLE_REF(3));
										}
										if (CBOOL(BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00))
											{	/* Cnst/alloc.scm 897 */
												obj_t BgL_arg2158z00_2293;

												{	/* Cnst/alloc.scm 69 */
													obj_t BgL_newz00_3256;

													BgL_newz00_3256 =
														create_struct(CNST_TABLE_REF(9), (int) (2L));
													{	/* Cnst/alloc.scm 69 */
														obj_t BgL_auxz00_5377;
														int BgL_tmpz00_5375;

														BgL_auxz00_5377 = ((obj_t) BgL_varz00_2292);
														BgL_tmpz00_5375 = (int) (1L);
														STRUCT_SET(BgL_newz00_3256, BgL_tmpz00_5375,
															BgL_auxz00_5377);
													}
													{	/* Cnst/alloc.scm 69 */
														int BgL_tmpz00_5380;

														BgL_tmpz00_5380 = (int) (0L);
														STRUCT_SET(BgL_newz00_3256, BgL_tmpz00_5380,
															BgL_vecz00_68);
													}
													BgL_arg2158z00_2293 = BgL_newz00_3256;
												}
												BGl_za2vectorzd2envza2zd2zzcnst_allocz00 =
													MAKE_YOUNG_PAIR(BgL_arg2158z00_2293,
													BGl_za2vectorzd2envza2zd2zzcnst_allocz00);
											}
										else
											{	/* Cnst/alloc.scm 896 */
												BFALSE;
											}
										{	/* Cnst/alloc.scm 898 */
											obj_t BgL_arg2159z00_2294;

											{	/* Cnst/alloc.scm 898 */
												obj_t BgL_arg2160z00_2295;

												{	/* Cnst/alloc.scm 898 */
													obj_t BgL_arg2161z00_2296;
													obj_t BgL_arg2162z00_2297;

													{	/* Cnst/alloc.scm 898 */
														obj_t BgL_arg2163z00_2298;

														{	/* Cnst/alloc.scm 898 */
															obj_t BgL_arg2164z00_2299;
															obj_t BgL_arg2165z00_2300;

															BgL_arg2164z00_2299 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt) BgL_varz00_2292)))->
																BgL_idz00);
															BgL_arg2165z00_2300 =
																MAKE_YOUNG_PAIR((((BgL_globalz00_bglt)
																		COBJECT(BgL_varz00_2292))->BgL_modulez00),
																BNIL);
															BgL_arg2163z00_2298 =
																MAKE_YOUNG_PAIR(BgL_arg2164z00_2299,
																BgL_arg2165z00_2300);
														}
														BgL_arg2161z00_2296 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
															BgL_arg2163z00_2298);
													}
													{	/* Cnst/alloc.scm 899 */
														BgL_nodez00_bglt BgL_arg2167z00_2302;

														{	/* Cnst/alloc.scm 829 */
															BgL_nodez00_bglt BgL_arg2121z00_2234;

															{	/* Cnst/alloc.scm 829 */
																BgL_letzd2varzd2_bglt BgL_arg2122z00_2235;
																BgL_typez00_bglt BgL_arg2123z00_2236;

																{	/* Cnst/alloc.scm 829 */
																	BgL_localz00_bglt BgL_varz00_2237;
																	obj_t BgL_lz00_2238;

																	BgL_varz00_2237 =
																		BGl_makezd2localzd2svarz00zzast_localz00
																		(CNST_TABLE_REF(48),
																		((BgL_typez00_bglt)
																			BGl_za2vectorza2z00zztype_cachez00));
																	BgL_lz00_2238 =
																		BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00
																		(BgL_vecz00_68);
																	{	/* Cnst/alloc.scm 831 */
																		BgL_letzd2varzd2_bglt BgL_new1207z00_2239;

																		{	/* Cnst/alloc.scm 832 */
																			BgL_letzd2varzd2_bglt BgL_new1206z00_2286;

																			BgL_new1206z00_2286 =
																				((BgL_letzd2varzd2_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_letzd2varzd2_bgl))));
																			{	/* Cnst/alloc.scm 832 */
																				long BgL_arg2154z00_2287;

																				BgL_arg2154z00_2287 =
																					BGL_CLASS_NUM
																					(BGl_letzd2varzd2zzast_nodez00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1206z00_2286),
																					BgL_arg2154z00_2287);
																			}
																			{	/* Cnst/alloc.scm 832 */
																				BgL_objectz00_bglt BgL_tmpz00_5399;

																				BgL_tmpz00_5399 =
																					((BgL_objectz00_bglt)
																					BgL_new1206z00_2286);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5399,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1206z00_2286);
																			BgL_new1207z00_2239 = BgL_new1206z00_2286;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1207z00_2239)))->
																				BgL_locz00) =
																			((obj_t) BgL_locz00_69), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1207z00_2239)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt)
																				BGl_strictzd2nodezd2typez00zzast_nodez00
																				(((BgL_typez00_bglt)
																						BGl_za2vectorza2z00zztype_cachez00),
																					((BgL_typez00_bglt)
																						BGl_za2objza2z00zztype_cachez00))),
																			BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1207z00_2239)))->
																				BgL_sidezd2effectzd2) =
																			((obj_t) BUNSPEC), BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1207z00_2239)))->
																				BgL_keyz00) =
																			((obj_t) BINT(-1L)), BUNSPEC);
																		{
																			obj_t BgL_auxz00_5415;

																			{	/* Cnst/alloc.scm 837 */
																				obj_t BgL_arg2124z00_2240;

																				{	/* Cnst/alloc.scm 837 */
																					BgL_appz00_bglt BgL_arg2126z00_2242;

																					{	/* Cnst/alloc.scm 837 */
																						BgL_appz00_bglt BgL_new1209z00_2243;

																						{	/* Cnst/alloc.scm 844 */
																							BgL_appz00_bglt
																								BgL_new1208z00_2253;
																							BgL_new1208z00_2253 =
																								((BgL_appz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_appz00_bgl))));
																							{	/* Cnst/alloc.scm 844 */
																								long BgL_arg2133z00_2254;

																								{	/* Cnst/alloc.scm 844 */
																									obj_t BgL_classz00_3204;

																									BgL_classz00_3204 =
																										BGl_appz00zzast_nodez00;
																									BgL_arg2133z00_2254 =
																										BGL_CLASS_NUM
																										(BgL_classz00_3204);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1208z00_2253),
																									BgL_arg2133z00_2254);
																							}
																							{	/* Cnst/alloc.scm 844 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_5420;
																								BgL_tmpz00_5420 =
																									((BgL_objectz00_bglt)
																									BgL_new1208z00_2253);
																								BGL_OBJECT_WIDENING_SET
																									(BgL_tmpz00_5420, BFALSE);
																							}
																							((BgL_objectz00_bglt)
																								BgL_new1208z00_2253);
																							BgL_new1209z00_2243 =
																								BgL_new1208z00_2253;
																						}
																						((((BgL_nodez00_bglt) COBJECT(
																										((BgL_nodez00_bglt)
																											BgL_new1209z00_2243)))->
																								BgL_locz00) =
																							((obj_t) BgL_locz00_69), BUNSPEC);
																						((((BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt)
																											BgL_new1209z00_2243)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) (
																									(BgL_typez00_bglt)
																									BGl_za2vectorza2z00zztype_cachez00)),
																							BUNSPEC);
																						((((BgL_nodezf2effectzf2_bglt)
																									COBJECT((
																											(BgL_nodezf2effectzf2_bglt)
																											BgL_new1209z00_2243)))->
																								BgL_sidezd2effectzd2) =
																							((obj_t) BUNSPEC), BUNSPEC);
																						((((BgL_nodezf2effectzf2_bglt)
																									COBJECT((
																											(BgL_nodezf2effectzf2_bglt)
																											BgL_new1209z00_2243)))->
																								BgL_keyz00) =
																							((obj_t) BINT(-1L)), BUNSPEC);
																						{
																							BgL_varz00_bglt BgL_auxz00_5434;

																							{	/* Cnst/alloc.scm 840 */
																								BgL_refz00_bglt
																									BgL_new1211z00_2244;
																								{	/* Cnst/alloc.scm 843 */
																									BgL_refz00_bglt
																										BgL_new1210z00_2245;
																									BgL_new1210z00_2245 =
																										((BgL_refz00_bglt)
																										BOBJECT(GC_MALLOC(sizeof
																												(struct
																													BgL_refz00_bgl))));
																									{	/* Cnst/alloc.scm 843 */
																										long BgL_arg2127z00_2246;

																										{	/* Cnst/alloc.scm 843 */
																											obj_t BgL_classz00_3208;

																											BgL_classz00_3208 =
																												BGl_refz00zzast_nodez00;
																											BgL_arg2127z00_2246 =
																												BGL_CLASS_NUM
																												(BgL_classz00_3208);
																										}
																										BGL_OBJECT_CLASS_NUM_SET(
																											((BgL_objectz00_bglt)
																												BgL_new1210z00_2245),
																											BgL_arg2127z00_2246);
																									}
																									{	/* Cnst/alloc.scm 843 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_5439;
																										BgL_tmpz00_5439 =
																											((BgL_objectz00_bglt)
																											BgL_new1210z00_2245);
																										BGL_OBJECT_WIDENING_SET
																											(BgL_tmpz00_5439, BFALSE);
																									}
																									((BgL_objectz00_bglt)
																										BgL_new1210z00_2245);
																									BgL_new1211z00_2244 =
																										BgL_new1210z00_2245;
																								}
																								((((BgL_nodez00_bglt) COBJECT(
																												((BgL_nodez00_bglt)
																													BgL_new1211z00_2244)))->
																										BgL_locz00) =
																									((obj_t) BgL_locz00_69),
																									BUNSPEC);
																								{
																									BgL_typez00_bglt
																										BgL_auxz00_5445;
																									{	/* Cnst/alloc.scm 842 */
																										BgL_variablez00_bglt
																											BgL_oz00_3212;
																										BgL_oz00_3212 =
																											((BgL_variablez00_bglt)
																											BGl_za2listzd2ze3vectorza2z31zzcnst_cachez00);
																										BgL_auxz00_5445 =
																											(((BgL_variablez00_bglt)
																												COBJECT
																												(BgL_oz00_3212))->
																											BgL_typez00);
																									}
																									((((BgL_nodez00_bglt) COBJECT(
																													((BgL_nodez00_bglt)
																														BgL_new1211z00_2244)))->
																											BgL_typez00) =
																										((BgL_typez00_bglt)
																											BgL_auxz00_5445),
																										BUNSPEC);
																								}
																								((((BgL_varz00_bglt) COBJECT(
																												((BgL_varz00_bglt)
																													BgL_new1211z00_2244)))->
																										BgL_variablez00) =
																									((BgL_variablez00_bglt) (
																											(BgL_variablez00_bglt)
																											BGl_za2listzd2ze3vectorza2z31zzcnst_cachez00)),
																									BUNSPEC);
																								BgL_auxz00_5434 =
																									((BgL_varz00_bglt)
																									BgL_new1211z00_2244);
																							}
																							((((BgL_appz00_bglt)
																										COBJECT
																										(BgL_new1209z00_2243))->
																									BgL_funz00) =
																								((BgL_varz00_bglt)
																									BgL_auxz00_5434), BUNSPEC);
																						}
																						{
																							obj_t BgL_auxz00_5455;

																							{	/* Cnst/alloc.scm 845 */
																								BgL_kwotez00_bglt
																									BgL_arg2129z00_2247;
																								{	/* Cnst/alloc.scm 845 */
																									BgL_kwotez00_bglt
																										BgL_new1213z00_2249;
																									{	/* Cnst/alloc.scm 849 */
																										BgL_kwotez00_bglt
																											BgL_new1212z00_2251;
																										BgL_new1212z00_2251 =
																											((BgL_kwotez00_bglt)
																											BOBJECT(GC_MALLOC(sizeof
																													(struct
																														BgL_kwotez00_bgl))));
																										{	/* Cnst/alloc.scm 849 */
																											long BgL_arg2132z00_2252;

																											{	/* Cnst/alloc.scm 849 */
																												obj_t BgL_classz00_3213;

																												BgL_classz00_3213 =
																													BGl_kwotez00zzast_nodez00;
																												BgL_arg2132z00_2252 =
																													BGL_CLASS_NUM
																													(BgL_classz00_3213);
																											}
																											BGL_OBJECT_CLASS_NUM_SET(
																												((BgL_objectz00_bglt)
																													BgL_new1212z00_2251),
																												BgL_arg2132z00_2252);
																										}
																										{	/* Cnst/alloc.scm 849 */
																											BgL_objectz00_bglt
																												BgL_tmpz00_5460;
																											BgL_tmpz00_5460 =
																												((BgL_objectz00_bglt)
																												BgL_new1212z00_2251);
																											BGL_OBJECT_WIDENING_SET
																												(BgL_tmpz00_5460,
																												BFALSE);
																										}
																										((BgL_objectz00_bglt)
																											BgL_new1212z00_2251);
																										BgL_new1213z00_2249 =
																											BgL_new1212z00_2251;
																									}
																									((((BgL_nodez00_bglt) COBJECT(
																													((BgL_nodez00_bglt)
																														BgL_new1213z00_2249)))->
																											BgL_locz00) =
																										((obj_t) BgL_locz00_69),
																										BUNSPEC);
																									{
																										BgL_typez00_bglt
																											BgL_auxz00_5466;
																										{	/* Cnst/alloc.scm 848 */
																											BgL_typez00_bglt
																												BgL_arg2131z00_2250;
																											BgL_arg2131z00_2250 =
																												BGl_getzd2typezd2kwotez00zztype_typeofz00
																												(BgL_lz00_2238);
																											BgL_auxz00_5466 =
																												BGl_strictzd2nodezd2typez00zzast_nodez00
																												(BgL_arg2131z00_2250,
																												((BgL_typez00_bglt)
																													BGl_za2objza2z00zztype_cachez00));
																										}
																										((((BgL_nodez00_bglt)
																													COBJECT((
																															(BgL_nodez00_bglt)
																															BgL_new1213z00_2249)))->
																												BgL_typez00) =
																											((BgL_typez00_bglt)
																												BgL_auxz00_5466),
																											BUNSPEC);
																									}
																									((((BgL_kwotez00_bglt)
																												COBJECT
																												(BgL_new1213z00_2249))->
																											BgL_valuez00) =
																										((obj_t) BgL_lz00_2238),
																										BUNSPEC);
																									BgL_arg2129z00_2247 =
																										BgL_new1213z00_2249;
																								}
																								{	/* Cnst/alloc.scm 844 */
																									obj_t BgL_list2130z00_2248;

																									BgL_list2130z00_2248 =
																										MAKE_YOUNG_PAIR(
																										((obj_t)
																											BgL_arg2129z00_2247),
																										BNIL);
																									BgL_auxz00_5455 =
																										BgL_list2130z00_2248;
																							}}
																							((((BgL_appz00_bglt)
																										COBJECT
																										(BgL_new1209z00_2243))->
																									BgL_argsz00) =
																								((obj_t) BgL_auxz00_5455),
																								BUNSPEC);
																						}
																						((((BgL_appz00_bglt)
																									COBJECT
																									(BgL_new1209z00_2243))->
																								BgL_stackablez00) =
																							((obj_t) BFALSE), BUNSPEC);
																						BgL_arg2126z00_2242 =
																							BgL_new1209z00_2243;
																					}
																					BgL_arg2124z00_2240 =
																						MAKE_YOUNG_PAIR(
																						((obj_t) BgL_varz00_2237),
																						((obj_t) BgL_arg2126z00_2242));
																				}
																				{	/* Cnst/alloc.scm 835 */
																					obj_t BgL_list2125z00_2241;

																					BgL_list2125z00_2241 =
																						MAKE_YOUNG_PAIR(BgL_arg2124z00_2240,
																						BNIL);
																					BgL_auxz00_5415 =
																						BgL_list2125z00_2241;
																			}}
																			((((BgL_letzd2varzd2_bglt)
																						COBJECT(BgL_new1207z00_2239))->
																					BgL_bindingsz00) =
																				((obj_t) BgL_auxz00_5415), BUNSPEC);
																		}
																		{
																			BgL_nodez00_bglt BgL_auxz00_5482;

																			{	/* Cnst/alloc.scm 850 */
																				BgL_refz00_bglt BgL_varzd2bodyzd2_2255;

																				{	/* Cnst/alloc.scm 850 */
																					BgL_refz00_bglt BgL_new1215z00_2283;

																					{	/* Cnst/alloc.scm 851 */
																						BgL_refz00_bglt BgL_new1214z00_2284;

																						BgL_new1214z00_2284 =
																							((BgL_refz00_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_refz00_bgl))));
																						{	/* Cnst/alloc.scm 851 */
																							long BgL_arg2152z00_2285;

																							{	/* Cnst/alloc.scm 851 */
																								obj_t BgL_classz00_3219;

																								BgL_classz00_3219 =
																									BGl_refz00zzast_nodez00;
																								BgL_arg2152z00_2285 =
																									BGL_CLASS_NUM
																									(BgL_classz00_3219);
																							}
																							BGL_OBJECT_CLASS_NUM_SET(
																								((BgL_objectz00_bglt)
																									BgL_new1214z00_2284),
																								BgL_arg2152z00_2285);
																						}
																						{	/* Cnst/alloc.scm 851 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_5487;
																							BgL_tmpz00_5487 =
																								((BgL_objectz00_bglt)
																								BgL_new1214z00_2284);
																							BGL_OBJECT_WIDENING_SET
																								(BgL_tmpz00_5487, BFALSE);
																						}
																						((BgL_objectz00_bglt)
																							BgL_new1214z00_2284);
																						BgL_new1215z00_2283 =
																							BgL_new1214z00_2284;
																					}
																					((((BgL_nodez00_bglt) COBJECT(
																									((BgL_nodez00_bglt)
																										BgL_new1215z00_2283)))->
																							BgL_locz00) =
																						((obj_t) BgL_locz00_69), BUNSPEC);
																					((((BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt)
																										BgL_new1215z00_2283)))->
																							BgL_typez00) =
																						((BgL_typez00_bglt) ((
																									(BgL_variablez00_bglt)
																									COBJECT((
																											(BgL_variablez00_bglt)
																											BgL_varz00_2237)))->
																								BgL_typez00)), BUNSPEC);
																					((((BgL_varz00_bglt)
																								COBJECT(((BgL_varz00_bglt)
																										BgL_new1215z00_2283)))->
																							BgL_variablez00) =
																						((BgL_variablez00_bglt) (
																								(BgL_variablez00_bglt)
																								BgL_varz00_2237)), BUNSPEC);
																					BgL_varzd2bodyzd2_2255 =
																						BgL_new1215z00_2283;
																				}
																				{	/* Cnst/alloc.scm 854 */
																					bool_t BgL_test2547z00_5500;

																					{	/* Cnst/alloc.scm 854 */
																						int BgL_arg2151z00_2282;

																						BgL_arg2151z00_2282 =
																							VECTOR_TAG(BgL_vecz00_68);
																						BgL_test2547z00_5500 =
																							(
																							(long) (BgL_arg2151z00_2282) >
																							0L);
																					}
																					if (BgL_test2547z00_5500)
																						{	/* Cnst/alloc.scm 855 */
																							BgL_sequencez00_bglt
																								BgL_new1217z00_2258;
																							{	/* Cnst/alloc.scm 856 */
																								BgL_sequencez00_bglt
																									BgL_new1216z00_2280;
																								BgL_new1216z00_2280 =
																									((BgL_sequencez00_bglt)
																									BOBJECT(GC_MALLOC(sizeof
																											(struct
																												BgL_sequencez00_bgl))));
																								{	/* Cnst/alloc.scm 856 */
																									long BgL_arg2150z00_2281;

																									{	/* Cnst/alloc.scm 856 */
																										obj_t BgL_classz00_3226;

																										BgL_classz00_3226 =
																											BGl_sequencez00zzast_nodez00;
																										BgL_arg2150z00_2281 =
																											BGL_CLASS_NUM
																											(BgL_classz00_3226);
																									}
																									BGL_OBJECT_CLASS_NUM_SET(
																										((BgL_objectz00_bglt)
																											BgL_new1216z00_2280),
																										BgL_arg2150z00_2281);
																								}
																								{	/* Cnst/alloc.scm 856 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_5508;
																									BgL_tmpz00_5508 =
																										((BgL_objectz00_bglt)
																										BgL_new1216z00_2280);
																									BGL_OBJECT_WIDENING_SET
																										(BgL_tmpz00_5508, BFALSE);
																								}
																								((BgL_objectz00_bglt)
																									BgL_new1216z00_2280);
																								BgL_new1217z00_2258 =
																									BgL_new1216z00_2280;
																							}
																							((((BgL_nodez00_bglt) COBJECT(
																											((BgL_nodez00_bglt)
																												BgL_new1217z00_2258)))->
																									BgL_locz00) =
																								((obj_t) BgL_locz00_69),
																								BUNSPEC);
																							((((BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												BgL_new1217z00_2258)))->
																									BgL_typez00) =
																								((BgL_typez00_bglt) (
																										(BgL_typez00_bglt)
																										BGl_za2vectorza2z00zztype_cachez00)),
																								BUNSPEC);
																							((((BgL_nodezf2effectzf2_bglt)
																										COBJECT((
																												(BgL_nodezf2effectzf2_bglt)
																												BgL_new1217z00_2258)))->
																									BgL_sidezd2effectzd2) =
																								((obj_t) BUNSPEC), BUNSPEC);
																							((((BgL_nodezf2effectzf2_bglt)
																										COBJECT((
																												(BgL_nodezf2effectzf2_bglt)
																												BgL_new1217z00_2258)))->
																									BgL_keyz00) =
																								((obj_t) BINT(-1L)), BUNSPEC);
																							{
																								obj_t BgL_auxz00_5522;

																								{	/* Cnst/alloc.scm 860 */
																									BgL_appz00_bglt
																										BgL_arg2136z00_2259;
																									{	/* Cnst/alloc.scm 860 */
																										BgL_appz00_bglt
																											BgL_new1219z00_2262;
																										{	/* Cnst/alloc.scm 861 */
																											BgL_appz00_bglt
																												BgL_new1218z00_2278;
																											BgL_new1218z00_2278 =
																												((BgL_appz00_bglt)
																												BOBJECT(GC_MALLOC(sizeof
																														(struct
																															BgL_appz00_bgl))));
																											{	/* Cnst/alloc.scm 861 */
																												long
																													BgL_arg2149z00_2279;
																												{	/* Cnst/alloc.scm 861 */
																													obj_t
																														BgL_classz00_3230;
																													BgL_classz00_3230 =
																														BGl_appz00zzast_nodez00;
																													BgL_arg2149z00_2279 =
																														BGL_CLASS_NUM
																														(BgL_classz00_3230);
																												}
																												BGL_OBJECT_CLASS_NUM_SET
																													(((BgL_objectz00_bglt)
																														BgL_new1218z00_2278),
																													BgL_arg2149z00_2279);
																											}
																											{	/* Cnst/alloc.scm 861 */
																												BgL_objectz00_bglt
																													BgL_tmpz00_5527;
																												BgL_tmpz00_5527 =
																													((BgL_objectz00_bglt)
																													BgL_new1218z00_2278);
																												BGL_OBJECT_WIDENING_SET
																													(BgL_tmpz00_5527,
																													BFALSE);
																											}
																											((BgL_objectz00_bglt)
																												BgL_new1218z00_2278);
																											BgL_new1219z00_2262 =
																												BgL_new1218z00_2278;
																										}
																										((((BgL_nodez00_bglt)
																													COBJECT((
																															(BgL_nodez00_bglt)
																															BgL_new1219z00_2262)))->
																												BgL_locz00) =
																											((obj_t) BgL_locz00_69),
																											BUNSPEC);
																										((((BgL_nodez00_bglt)
																													COBJECT((
																															(BgL_nodez00_bglt)
																															BgL_new1219z00_2262)))->
																												BgL_typez00) =
																											((BgL_typez00_bglt) (
																													(BgL_typez00_bglt)
																													BGl_za2objza2z00zztype_cachez00)),
																											BUNSPEC);
																										((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1219z00_2262)))->BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
																										((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1219z00_2262)))->BgL_keyz00) = ((obj_t) BINT(-1L)), BUNSPEC);
																										{
																											BgL_varz00_bglt
																												BgL_auxz00_5541;
																											{	/* Cnst/alloc.scm 863 */
																												BgL_refz00_bglt
																													BgL_new1221z00_2263;
																												{	/* Cnst/alloc.scm 864 */
																													BgL_refz00_bglt
																														BgL_new1220z00_2264;
																													BgL_new1220z00_2264 =
																														((BgL_refz00_bglt)
																														BOBJECT(GC_MALLOC
																															(sizeof(struct
																																	BgL_refz00_bgl))));
																													{	/* Cnst/alloc.scm 864 */
																														long
																															BgL_arg2139z00_2265;
																														{	/* Cnst/alloc.scm 864 */
																															obj_t
																																BgL_classz00_3234;
																															BgL_classz00_3234
																																=
																																BGl_refz00zzast_nodez00;
																															BgL_arg2139z00_2265
																																=
																																BGL_CLASS_NUM
																																(BgL_classz00_3234);
																														}
																														BGL_OBJECT_CLASS_NUM_SET
																															(((BgL_objectz00_bglt) BgL_new1220z00_2264), BgL_arg2139z00_2265);
																													}
																													{	/* Cnst/alloc.scm 864 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_5546;
																														BgL_tmpz00_5546 =
																															(
																															(BgL_objectz00_bglt)
																															BgL_new1220z00_2264);
																														BGL_OBJECT_WIDENING_SET
																															(BgL_tmpz00_5546,
																															BFALSE);
																													}
																													((BgL_objectz00_bglt)
																														BgL_new1220z00_2264);
																													BgL_new1221z00_2263 =
																														BgL_new1220z00_2264;
																												}
																												((((BgL_nodez00_bglt)
																															COBJECT((
																																	(BgL_nodez00_bglt)
																																	BgL_new1221z00_2263)))->
																														BgL_locz00) =
																													((obj_t)
																														BgL_locz00_69),
																													BUNSPEC);
																												{
																													BgL_typez00_bglt
																														BgL_auxz00_5552;
																													{	/* Cnst/alloc.scm 865 */
																														BgL_variablez00_bglt
																															BgL_oz00_3238;
																														BgL_oz00_3238 =
																															(
																															(BgL_variablez00_bglt)
																															BGl_za2vectorzd2tagzd2setz12za2z12zzcnst_cachez00);
																														BgL_auxz00_5552 =
																															(((BgL_variablez00_bglt) COBJECT(BgL_oz00_3238))->BgL_typez00);
																													}
																													((((BgL_nodez00_bglt)
																																COBJECT((
																																		(BgL_nodez00_bglt)
																																		BgL_new1221z00_2263)))->
																															BgL_typez00) =
																														((BgL_typez00_bglt)
																															BgL_auxz00_5552),
																														BUNSPEC);
																												}
																												((((BgL_varz00_bglt)
																															COBJECT((
																																	(BgL_varz00_bglt)
																																	BgL_new1221z00_2263)))->
																														BgL_variablez00) =
																													((BgL_variablez00_bglt) ((BgL_variablez00_bglt) BGl_za2vectorzd2tagzd2setz12za2z12zzcnst_cachez00)), BUNSPEC);
																												BgL_auxz00_5541 =
																													((BgL_varz00_bglt)
																													BgL_new1221z00_2263);
																											}
																											((((BgL_appz00_bglt)
																														COBJECT
																														(BgL_new1219z00_2262))->
																													BgL_funz00) =
																												((BgL_varz00_bglt)
																													BgL_auxz00_5541),
																												BUNSPEC);
																										}
																										{
																											obj_t BgL_auxz00_5562;

																											{	/* Cnst/alloc.scm 869 */
																												BgL_refz00_bglt
																													BgL_arg2141z00_2266;
																												BgL_literalz00_bglt
																													BgL_arg2142z00_2267;
																												{	/* Cnst/alloc.scm 869 */
																													BgL_refz00_bglt
																														BgL_new1223z00_2270;
																													{	/* Cnst/alloc.scm 870 */
																														BgL_refz00_bglt
																															BgL_new1222z00_2271;
																														BgL_new1222z00_2271
																															=
																															((BgL_refz00_bglt)
																															BOBJECT(GC_MALLOC
																																(sizeof(struct
																																		BgL_refz00_bgl))));
																														{	/* Cnst/alloc.scm 870 */
																															long
																																BgL_arg2145z00_2272;
																															{	/* Cnst/alloc.scm 870 */
																																obj_t
																																	BgL_classz00_3239;
																																BgL_classz00_3239
																																	=
																																	BGl_refz00zzast_nodez00;
																																BgL_arg2145z00_2272
																																	=
																																	BGL_CLASS_NUM
																																	(BgL_classz00_3239);
																															}
																															BGL_OBJECT_CLASS_NUM_SET
																																(((BgL_objectz00_bglt) BgL_new1222z00_2271), BgL_arg2145z00_2272);
																														}
																														{	/* Cnst/alloc.scm 870 */
																															BgL_objectz00_bglt
																																BgL_tmpz00_5567;
																															BgL_tmpz00_5567 =
																																(
																																(BgL_objectz00_bglt)
																																BgL_new1222z00_2271);
																															BGL_OBJECT_WIDENING_SET
																																(BgL_tmpz00_5567,
																																BFALSE);
																														}
																														((BgL_objectz00_bglt) BgL_new1222z00_2271);
																														BgL_new1223z00_2270
																															=
																															BgL_new1222z00_2271;
																													}
																													((((BgL_nodez00_bglt)
																																COBJECT((
																																		(BgL_nodez00_bglt)
																																		BgL_new1223z00_2270)))->
																															BgL_locz00) =
																														((obj_t)
																															BgL_locz00_69),
																														BUNSPEC);
																													((((BgL_nodez00_bglt)
																																COBJECT((
																																		(BgL_nodez00_bglt)
																																		BgL_new1223z00_2270)))->
																															BgL_typez00) =
																														((BgL_typez00_bglt)
																															(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) BgL_varz00_2237)))->BgL_typez00)), BUNSPEC);
																													((((BgL_varz00_bglt)
																																COBJECT((
																																		(BgL_varz00_bglt)
																																		BgL_new1223z00_2270)))->
																															BgL_variablez00) =
																														((BgL_variablez00_bglt) ((BgL_variablez00_bglt) BgL_varz00_2237)), BUNSPEC);
																													BgL_arg2141z00_2266 =
																														BgL_new1223z00_2270;
																												}
																												{	/* Cnst/alloc.scm 873 */
																													BgL_literalz00_bglt
																														BgL_new1225z00_2273;
																													{	/* Cnst/alloc.scm 874 */
																														BgL_literalz00_bglt
																															BgL_new1224z00_2276;
																														BgL_new1224z00_2276
																															=
																															(
																															(BgL_literalz00_bglt)
																															BOBJECT(GC_MALLOC
																																(sizeof(struct
																																		BgL_literalz00_bgl))));
																														{	/* Cnst/alloc.scm 874 */
																															long
																																BgL_arg2148z00_2277;
																															{	/* Cnst/alloc.scm 874 */
																																obj_t
																																	BgL_classz00_3244;
																																BgL_classz00_3244
																																	=
																																	BGl_literalz00zzast_nodez00;
																																BgL_arg2148z00_2277
																																	=
																																	BGL_CLASS_NUM
																																	(BgL_classz00_3244);
																															}
																															BGL_OBJECT_CLASS_NUM_SET
																																(((BgL_objectz00_bglt) BgL_new1224z00_2276), BgL_arg2148z00_2277);
																														}
																														{	/* Cnst/alloc.scm 874 */
																															BgL_objectz00_bglt
																																BgL_tmpz00_5584;
																															BgL_tmpz00_5584 =
																																(
																																(BgL_objectz00_bglt)
																																BgL_new1224z00_2276);
																															BGL_OBJECT_WIDENING_SET
																																(BgL_tmpz00_5584,
																																BFALSE);
																														}
																														((BgL_objectz00_bglt) BgL_new1224z00_2276);
																														BgL_new1225z00_2273
																															=
																															BgL_new1224z00_2276;
																													}
																													((((BgL_nodez00_bglt)
																																COBJECT((
																																		(BgL_nodez00_bglt)
																																		BgL_new1225z00_2273)))->
																															BgL_locz00) =
																														((obj_t)
																															BgL_locz00_69),
																														BUNSPEC);
																													{
																														BgL_typez00_bglt
																															BgL_auxz00_5590;
																														{	/* Cnst/alloc.scm 876 */
																															BgL_typez00_bglt
																																BgL_arg2146z00_2274;
																															{	/* Cnst/alloc.scm 876 */
																																int
																																	BgL_arg2147z00_2275;
																																BgL_arg2147z00_2275
																																	=
																																	VECTOR_TAG
																																	(BgL_vecz00_68);
																																BgL_arg2146z00_2274
																																	=
																																	BGl_getzd2typezd2atomz00zztype_typeofz00
																																	(BINT
																																	(BgL_arg2147z00_2275));
																															}
																															BgL_auxz00_5590 =
																																BGl_strictzd2nodezd2typez00zzast_nodez00
																																(BgL_arg2146z00_2274,
																																((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
																														}
																														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1225z00_2273)))->BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_5590), BUNSPEC);
																													}
																													((((BgL_atomz00_bglt)
																																COBJECT((
																																		(BgL_atomz00_bglt)
																																		BgL_new1225z00_2273)))->
																															BgL_valuez00) =
																														((obj_t)
																															BINT(VECTOR_TAG
																																(BgL_vecz00_68))),
																														BUNSPEC);
																													BgL_arg2142z00_2267 =
																														BgL_new1225z00_2273;
																												}
																												{	/* Cnst/alloc.scm 868 */
																													obj_t
																														BgL_list2143z00_2268;
																													{	/* Cnst/alloc.scm 868 */
																														obj_t
																															BgL_arg2144z00_2269;
																														BgL_arg2144z00_2269
																															=
																															MAKE_YOUNG_PAIR((
																																(obj_t)
																																BgL_arg2142z00_2267),
																															BNIL);
																														BgL_list2143z00_2268
																															=
																															MAKE_YOUNG_PAIR((
																																(obj_t)
																																BgL_arg2141z00_2266),
																															BgL_arg2144z00_2269);
																													}
																													BgL_auxz00_5562 =
																														BgL_list2143z00_2268;
																											}}
																											((((BgL_appz00_bglt)
																														COBJECT
																														(BgL_new1219z00_2262))->
																													BgL_argsz00) =
																												((obj_t)
																													BgL_auxz00_5562),
																												BUNSPEC);
																										}
																										((((BgL_appz00_bglt)
																													COBJECT
																													(BgL_new1219z00_2262))->
																												BgL_stackablez00) =
																											((obj_t) BFALSE),
																											BUNSPEC);
																										BgL_arg2136z00_2259 =
																											BgL_new1219z00_2262;
																									}
																									{	/* Cnst/alloc.scm 859 */
																										obj_t BgL_list2137z00_2260;

																										{	/* Cnst/alloc.scm 859 */
																											obj_t BgL_arg2138z00_2261;

																											BgL_arg2138z00_2261 =
																												MAKE_YOUNG_PAIR(
																												((obj_t)
																													BgL_varzd2bodyzd2_2255),
																												BNIL);
																											BgL_list2137z00_2260 =
																												MAKE_YOUNG_PAIR(((obj_t)
																													BgL_arg2136z00_2259),
																												BgL_arg2138z00_2261);
																										}
																										BgL_auxz00_5522 =
																											BgL_list2137z00_2260;
																								}}
																								((((BgL_sequencez00_bglt)
																											COBJECT
																											(BgL_new1217z00_2258))->
																										BgL_nodesz00) =
																									((obj_t) BgL_auxz00_5522),
																									BUNSPEC);
																							}
																							((((BgL_sequencez00_bglt)
																										COBJECT
																										(BgL_new1217z00_2258))->
																									BgL_unsafez00) =
																								((bool_t) ((bool_t) 0)),
																								BUNSPEC);
																							((((BgL_sequencez00_bglt)
																										COBJECT
																										(BgL_new1217z00_2258))->
																									BgL_metaz00) =
																								((obj_t) BNIL), BUNSPEC);
																							BgL_auxz00_5482 =
																								((BgL_nodez00_bglt)
																								BgL_new1217z00_2258);
																						}
																					else
																						{	/* Cnst/alloc.scm 854 */
																							BgL_auxz00_5482 =
																								((BgL_nodez00_bglt)
																								BgL_varzd2bodyzd2_2255);
																						}
																				}
																			}
																			((((BgL_letzd2varzd2_bglt)
																						COBJECT(BgL_new1207z00_2239))->
																					BgL_bodyz00) =
																				((BgL_nodez00_bglt) BgL_auxz00_5482),
																				BUNSPEC);
																		}
																		((((BgL_letzd2varzd2_bglt)
																					COBJECT(BgL_new1207z00_2239))->
																				BgL_removablezf3zf3) =
																			((bool_t) ((bool_t) 1)), BUNSPEC);
																		BgL_arg2122z00_2235 = BgL_new1207z00_2239;
																	}
																}
																BgL_arg2123z00_2236 =
																	BGl_strictzd2nodezd2typez00zzast_nodez00(
																	((BgL_typez00_bglt)
																		BGl_za2vectorza2z00zztype_cachez00),
																	((BgL_typez00_bglt)
																		BGl_za2objza2z00zztype_cachez00));
																BgL_arg2121z00_2234 =
																	BGl_coercez12z12zzcoerce_coercez00((
																		(BgL_nodez00_bglt) BgL_arg2122z00_2235),
																	BUNSPEC, BgL_arg2123z00_2236, ((bool_t) 0));
															}
															BgL_arg2167z00_2302 =
																BGl_cnstz12z12zzcnst_nodez00
																(BgL_arg2121z00_2234);
														}
														BgL_arg2162z00_2297 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg2167z00_2302), BNIL);
													}
													BgL_arg2160z00_2295 =
														MAKE_YOUNG_PAIR(BgL_arg2161z00_2296,
														BgL_arg2162z00_2297);
												}
												BgL_arg2159z00_2294 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
													BgL_arg2160z00_2295);
											}
											BGl_za2globalzd2sexpza2zd2zzcnst_allocz00 =
												MAKE_YOUNG_PAIR(BgL_arg2159z00_2294,
												BGl_za2globalzd2sexpza2zd2zzcnst_allocz00);
										}
										{	/* Cnst/alloc.scm 900 */
											BgL_refz00_bglt BgL_new1228z00_2303;

											{	/* Cnst/alloc.scm 901 */
												BgL_refz00_bglt BgL_new1226z00_2305;

												BgL_new1226z00_2305 =
													((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_refz00_bgl))));
												{	/* Cnst/alloc.scm 901 */
													long BgL_arg2169z00_2306;

													BgL_arg2169z00_2306 =
														BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1226z00_2305),
														BgL_arg2169z00_2306);
												}
												{	/* Cnst/alloc.scm 901 */
													BgL_objectz00_bglt BgL_tmpz00_5635;

													BgL_tmpz00_5635 =
														((BgL_objectz00_bglt) BgL_new1226z00_2305);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5635, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1226z00_2305);
												BgL_new1228z00_2303 = BgL_new1226z00_2305;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1228z00_2303)))->
													BgL_locz00) = ((obj_t) BgL_locz00_69), BUNSPEC);
											{
												BgL_typez00_bglt BgL_auxz00_5641;

												{	/* Cnst/alloc.scm 902 */
													BgL_typez00_bglt BgL_arg2168z00_2304;

													BgL_arg2168z00_2304 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt) BgL_varz00_2292)))->
														BgL_typez00);
													BgL_auxz00_5641 =
														BGl_strictzd2nodezd2typez00zzast_nodez00((
															(BgL_typez00_bglt)
															BGl_za2vectorza2z00zztype_cachez00),
														BgL_arg2168z00_2304);
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1228z00_2303)))->
														BgL_typez00) =
													((BgL_typez00_bglt) BgL_auxz00_5641), BUNSPEC);
											}
											((((BgL_varz00_bglt) COBJECT(
															((BgL_varz00_bglt) BgL_new1228z00_2303)))->
													BgL_variablez00) =
												((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
														BgL_varz00_2292)), BUNSPEC);
											BgL_auxz00_5362 = BgL_new1228z00_2303;
									}}
									return ((BgL_nodez00_bglt) BgL_auxz00_5362);
								}
							else
								{
									BgL_appz00_bglt BgL_auxz00_5652;

									{	/* Cnst/alloc.scm 885 */
										long BgL_offsetz00_2289;

										BgL_offsetz00_2289 =
											BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00;
										BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00 =
											(1L + BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00);
										BGl_za2globalzd2setza2zd2zzcnst_allocz00 =
											MAKE_YOUNG_PAIR(BgL_vecz00_68,
											BGl_za2globalzd2setza2zd2zzcnst_allocz00);
										if (CBOOL(BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00))
											{	/* Cnst/alloc.scm 889 */
												obj_t BgL_arg2156z00_2290;

												{	/* Cnst/alloc.scm 69 */
													obj_t BgL_newz00_3253;

													BgL_newz00_3253 =
														create_struct(CNST_TABLE_REF(9), (int) (2L));
													{	/* Cnst/alloc.scm 69 */
														obj_t BgL_auxz00_5662;
														int BgL_tmpz00_5660;

														BgL_auxz00_5662 = BINT(BgL_offsetz00_2289);
														BgL_tmpz00_5660 = (int) (1L);
														STRUCT_SET(BgL_newz00_3253, BgL_tmpz00_5660,
															BgL_auxz00_5662);
													}
													{	/* Cnst/alloc.scm 69 */
														int BgL_tmpz00_5665;

														BgL_tmpz00_5665 = (int) (0L);
														STRUCT_SET(BgL_newz00_3253, BgL_tmpz00_5665,
															BgL_vecz00_68);
													}
													BgL_arg2156z00_2290 = BgL_newz00_3253;
												}
												BGl_za2vectorzd2envza2zd2zzcnst_allocz00 =
													MAKE_YOUNG_PAIR(BgL_arg2156z00_2290,
													BGl_za2vectorzd2envza2zd2zzcnst_allocz00);
											}
										else
											{	/* Cnst/alloc.scm 888 */
												BFALSE;
											}
										BgL_auxz00_5652 =
											BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00(BINT
											(BgL_offsetz00_2289), BGl_za2vectorza2z00zztype_cachez00,
											BgL_locz00_69);
									}
									return ((BgL_nodez00_bglt) BgL_auxz00_5652);
								}
						}
				}
			}
		}

	}



/* &cnst-alloc-vector */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2vectorz62zzcnst_allocz00(obj_t
		BgL_envz00_3542, obj_t BgL_vecz00_3543, obj_t BgL_locz00_3544)
	{
		{	/* Cnst/alloc.scm 825 */
			return
				BGl_cnstzd2alloczd2vectorz00zzcnst_allocz00(BgL_vecz00_3543,
				BgL_locz00_3544);
		}

	}



/* cnst-alloc-homogenous-vector */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2homogenouszd2vectorzd2zzcnst_allocz00(obj_t
		BgL_vecz00_70, obj_t BgL_locz00_71)
	{
		{	/* Cnst/alloc.scm 930 */
			{	/* Cnst/alloc.scm 932 */
				obj_t BgL_veczd2tagzd2_2312;

				{	/* Cnst/alloc.scm 932 */
					obj_t BgL_tagz00_2407;

					BgL_tagz00_2407 =
						BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(BgL_vecz00_70);
					{	/* Cnst/alloc.scm 933 */
						obj_t BgL__z00_2408;
						obj_t BgL__z00_2409;
						obj_t BgL__z00_2410;

						{	/* Cnst/alloc.scm 933 */
							obj_t BgL_tmpz00_3278;

							{	/* Cnst/alloc.scm 933 */
								int BgL_tmpz00_5674;

								BgL_tmpz00_5674 = (int) (1L);
								BgL_tmpz00_3278 = BGL_MVALUES_VAL(BgL_tmpz00_5674);
							}
							{	/* Cnst/alloc.scm 933 */
								int BgL_tmpz00_5677;

								BgL_tmpz00_5677 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_5677, BUNSPEC);
							}
							BgL__z00_2408 = BgL_tmpz00_3278;
						}
						{	/* Cnst/alloc.scm 933 */
							obj_t BgL_tmpz00_3279;

							{	/* Cnst/alloc.scm 933 */
								int BgL_tmpz00_5680;

								BgL_tmpz00_5680 = (int) (2L);
								BgL_tmpz00_3279 = BGL_MVALUES_VAL(BgL_tmpz00_5680);
							}
							{	/* Cnst/alloc.scm 933 */
								int BgL_tmpz00_5683;

								BgL_tmpz00_5683 = (int) (2L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_5683, BUNSPEC);
							}
							BgL__z00_2409 = BgL_tmpz00_3279;
						}
						{	/* Cnst/alloc.scm 933 */
							obj_t BgL_tmpz00_3280;

							{	/* Cnst/alloc.scm 933 */
								int BgL_tmpz00_5686;

								BgL_tmpz00_5686 = (int) (3L);
								BgL_tmpz00_3280 = BGL_MVALUES_VAL(BgL_tmpz00_5686);
							}
							{	/* Cnst/alloc.scm 933 */
								int BgL_tmpz00_5689;

								BgL_tmpz00_5689 = (int) (3L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_5689, BUNSPEC);
							}
							BgL__z00_2410 = BgL_tmpz00_3280;
						}
						BgL_veczd2tagzd2_2312 = BgL_tagz00_2407;
				}}
				{	/* Cnst/alloc.scm 932 */
					obj_t BgL_veczd2typezd2idz00_2313;

					{	/* Cnst/alloc.scm 935 */
						obj_t BgL_arg2230z00_2404;

						{	/* Cnst/alloc.scm 935 */
							obj_t BgL_arg2231z00_2405;
							obj_t BgL_arg2232z00_2406;

							{	/* Cnst/alloc.scm 935 */
								obj_t BgL_arg1455z00_3282;

								BgL_arg1455z00_3282 =
									SYMBOL_TO_STRING(((obj_t) BgL_veczd2tagzd2_2312));
								BgL_arg2231z00_2405 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1455z00_3282);
							}
							{	/* Cnst/alloc.scm 935 */
								obj_t BgL_symbolz00_3283;

								BgL_symbolz00_3283 = CNST_TABLE_REF(49);
								{	/* Cnst/alloc.scm 935 */
									obj_t BgL_arg1455z00_3284;

									BgL_arg1455z00_3284 = SYMBOL_TO_STRING(BgL_symbolz00_3283);
									BgL_arg2232z00_2406 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_3284);
							}}
							BgL_arg2230z00_2404 =
								string_append(BgL_arg2231z00_2405, BgL_arg2232z00_2406);
						}
						BgL_veczd2typezd2idz00_2313 =
							bstring_to_symbol(BgL_arg2230z00_2404);
					}
					{	/* Cnst/alloc.scm 935 */
						BgL_typez00_bglt BgL_veczd2typezd2_2314;

						BgL_veczd2typezd2_2314 =
							BGl_findzd2typezd2zztype_envz00(BgL_veczd2typezd2idz00_2313);
						{	/* Cnst/alloc.scm 936 */
							obj_t BgL_listzd2ze3vectorz31_2315;

							{	/* Cnst/alloc.scm 937 */
								obj_t BgL_list2227z00_2401;

								{	/* Cnst/alloc.scm 937 */
									obj_t BgL_arg2228z00_2402;

									{	/* Cnst/alloc.scm 937 */
										obj_t BgL_arg2229z00_2403;

										BgL_arg2229z00_2403 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(49), BNIL);
										BgL_arg2228z00_2402 =
											MAKE_YOUNG_PAIR(BgL_veczd2tagzd2_2312,
											BgL_arg2229z00_2403);
									}
									BgL_list2227z00_2401 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(50), BgL_arg2228z00_2402);
								}
								BgL_listzd2ze3vectorz31_2315 =
									BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
									(BgL_list2227z00_2401);
							}
							{	/* Cnst/alloc.scm 937 */
								obj_t BgL_vectorzd2ze3listz31_2316;

								if ((BgL_veczd2tagzd2_2312 == CNST_TABLE_REF(51)))
									{	/* Cnst/alloc.scm 938 */
										BgL_vectorzd2ze3listz31_2316 =
											BGl_s8vectorzd2ze3listzd2envze3zz__srfi4z00;
									}
								else
									{	/* Cnst/alloc.scm 938 */
										if ((BgL_veczd2tagzd2_2312 == CNST_TABLE_REF(52)))
											{	/* Cnst/alloc.scm 938 */
												BgL_vectorzd2ze3listz31_2316 =
													BGl_u8vectorzd2ze3listzd2envze3zz__srfi4z00;
											}
										else
											{	/* Cnst/alloc.scm 938 */
												if ((BgL_veczd2tagzd2_2312 == CNST_TABLE_REF(53)))
													{	/* Cnst/alloc.scm 938 */
														BgL_vectorzd2ze3listz31_2316 =
															BGl_s16vectorzd2ze3listzd2envze3zz__srfi4z00;
													}
												else
													{	/* Cnst/alloc.scm 938 */
														if ((BgL_veczd2tagzd2_2312 == CNST_TABLE_REF(54)))
															{	/* Cnst/alloc.scm 938 */
																BgL_vectorzd2ze3listz31_2316 =
																	BGl_u16vectorzd2ze3listzd2envze3zz__srfi4z00;
															}
														else
															{	/* Cnst/alloc.scm 938 */
																if (
																	(BgL_veczd2tagzd2_2312 == CNST_TABLE_REF(55)))
																	{	/* Cnst/alloc.scm 938 */
																		BgL_vectorzd2ze3listz31_2316 =
																			BGl_s32vectorzd2ze3listzd2envze3zz__srfi4z00;
																	}
																else
																	{	/* Cnst/alloc.scm 938 */
																		if (
																			(BgL_veczd2tagzd2_2312 ==
																				CNST_TABLE_REF(56)))
																			{	/* Cnst/alloc.scm 938 */
																				BgL_vectorzd2ze3listz31_2316 =
																					BGl_u32vectorzd2ze3listzd2envze3zz__srfi4z00;
																			}
																		else
																			{	/* Cnst/alloc.scm 938 */
																				if (
																					(BgL_veczd2tagzd2_2312 ==
																						CNST_TABLE_REF(57)))
																					{	/* Cnst/alloc.scm 938 */
																						BgL_vectorzd2ze3listz31_2316 =
																							BGl_s64vectorzd2ze3listzd2envze3zz__srfi4z00;
																					}
																				else
																					{	/* Cnst/alloc.scm 938 */
																						if (
																							(BgL_veczd2tagzd2_2312 ==
																								CNST_TABLE_REF(58)))
																							{	/* Cnst/alloc.scm 938 */
																								BgL_vectorzd2ze3listz31_2316 =
																									BGl_u64vectorzd2ze3listzd2envze3zz__srfi4z00;
																							}
																						else
																							{	/* Cnst/alloc.scm 938 */
																								if (
																									(BgL_veczd2tagzd2_2312 ==
																										CNST_TABLE_REF(59)))
																									{	/* Cnst/alloc.scm 938 */
																										BgL_vectorzd2ze3listz31_2316
																											=
																											BGl_f32vectorzd2ze3listzd2envze3zz__srfi4z00;
																									}
																								else
																									{	/* Cnst/alloc.scm 938 */
																										if (
																											(BgL_veczd2tagzd2_2312 ==
																												CNST_TABLE_REF(60)))
																											{	/* Cnst/alloc.scm 938 */
																												BgL_vectorzd2ze3listz31_2316
																													=
																													BGl_f64vectorzd2ze3listzd2envze3zz__srfi4z00;
																											}
																										else
																											{	/* Cnst/alloc.scm 938 */
																												BgL_vectorzd2ze3listz31_2316
																													= BUNSPEC;
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
								{	/* Cnst/alloc.scm 938 */

									{

										{	/* Cnst/alloc.scm 1005 */
											obj_t BgL_oldz00_2320;

											{	/* Cnst/alloc.scm 1005 */
												obj_t BgL__andtest_1245z00_2327;

												BgL__andtest_1245z00_2327 =
													BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00;
												if (CBOOL(BgL__andtest_1245z00_2327))
													{
														obj_t BgL_envz00_2329;

														BgL_envz00_2329 =
															BGl_za2vectorzd2envza2zd2zzcnst_allocz00;
													BgL_zc3z04anonymousza32176ze3z87_2330:
														if (NULLP(BgL_envz00_2329))
															{	/* Cnst/alloc.scm 1008 */
																BgL_oldz00_2320 = BFALSE;
															}
														else
															{	/* Cnst/alloc.scm 1010 */
																bool_t BgL_test2561z00_5741;

																{	/* Cnst/alloc.scm 1010 */
																	obj_t BgL_arg2182z00_2336;

																	{	/* Cnst/alloc.scm 69 */
																		obj_t BgL_sz00_3332;

																		BgL_sz00_3332 =
																			CAR(((obj_t) BgL_envz00_2329));
																		BgL_arg2182z00_2336 =
																			STRUCT_REF(BgL_sz00_3332, (int) (0L));
																	}
																	BgL_test2561z00_5741 =
																		BGl_cnstzd2equalzf3z21zzcnst_allocz00
																		(BgL_arg2182z00_2336, BgL_vecz00_70);
																}
																if (BgL_test2561z00_5741)
																	{	/* Cnst/alloc.scm 1010 */
																		BgL_oldz00_2320 =
																			CAR(((obj_t) BgL_envz00_2329));
																	}
																else
																	{	/* Cnst/alloc.scm 1013 */
																		obj_t BgL_arg2181z00_2335;

																		BgL_arg2181z00_2335 =
																			CDR(((obj_t) BgL_envz00_2329));
																		{
																			obj_t BgL_envz00_5751;

																			BgL_envz00_5751 = BgL_arg2181z00_2335;
																			BgL_envz00_2329 = BgL_envz00_5751;
																			goto
																				BgL_zc3z04anonymousza32176ze3z87_2330;
																		}
																	}
															}
													}
												else
													{	/* Cnst/alloc.scm 1005 */
														BgL_oldz00_2320 = BFALSE;
													}
											}
											BGl_forcezd2initializa7ezd2srfi4zd2libraryzd2modulez12zb5zzcnst_allocz00
												();
											if (CBOOL(BgL_oldz00_2320))
												{	/* Cnst/alloc.scm 1016 */
													if (
														(BGl_za2initzd2modeza2zd2zzengine_paramz00 ==
															CNST_TABLE_REF(14)))
														{	/* Cnst/alloc.scm 1018 */
															BgL_refz00_bglt BgL_new1247z00_2321;

															{	/* Cnst/alloc.scm 1019 */
																BgL_refz00_bglt BgL_new1246z00_2324;

																BgL_new1246z00_2324 =
																	((BgL_refz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_refz00_bgl))));
																{	/* Cnst/alloc.scm 1019 */
																	long BgL_arg2174z00_2325;

																	BgL_arg2174z00_2325 =
																		BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1246z00_2324),
																		BgL_arg2174z00_2325);
																}
																{	/* Cnst/alloc.scm 1019 */
																	BgL_objectz00_bglt BgL_tmpz00_5762;

																	BgL_tmpz00_5762 =
																		((BgL_objectz00_bglt) BgL_new1246z00_2324);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5762,
																		BFALSE);
																}
																((BgL_objectz00_bglt) BgL_new1246z00_2324);
																BgL_new1247z00_2321 = BgL_new1246z00_2324;
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1247z00_2321)))->BgL_locz00) =
																((obj_t) BgL_locz00_71), BUNSPEC);
															{
																BgL_typez00_bglt BgL_auxz00_5768;

																{	/* Cnst/alloc.scm 1021 */
																	BgL_typez00_bglt BgL_arg2172z00_2322;

																	BgL_arg2172z00_2322 =
																		(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					STRUCT_REF(
																						((obj_t) BgL_oldz00_2320),
																						(int) (1L)))))->BgL_typez00);
																	BgL_auxz00_5768 =
																		BGl_strictzd2nodezd2typez00zzast_nodez00
																		(BgL_veczd2typezd2_2314,
																		BgL_arg2172z00_2322);
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1247z00_2321)))->BgL_typez00) =
																	((BgL_typez00_bglt) BgL_auxz00_5768),
																	BUNSPEC);
															}
															((((BgL_varz00_bglt) COBJECT(
																			((BgL_varz00_bglt)
																				BgL_new1247z00_2321)))->
																	BgL_variablez00) =
																((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																		STRUCT_REF(((obj_t) BgL_oldz00_2320),
																			(int) (1L)))), BUNSPEC);
															return ((BgL_nodez00_bglt) BgL_new1247z00_2321);
														}
													else
														{	/* Cnst/alloc.scm 1023 */
															obj_t BgL_arg2175z00_2326;

															BgL_arg2175z00_2326 =
																STRUCT_REF(
																((obj_t) BgL_oldz00_2320), (int) (1L));
															return
																((BgL_nodez00_bglt)
																BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00
																(BgL_arg2175z00_2326,
																	((obj_t) BgL_veczd2typezd2_2314),
																	BgL_locz00_71));
														}
												}
											else
												{	/* Cnst/alloc.scm 1016 */
													if (
														(BGl_za2initzd2modeza2zd2zzengine_paramz00 ==
															CNST_TABLE_REF(14)))
														{
															BgL_refz00_bglt BgL_auxz00_5793;

															{	/* Cnst/alloc.scm 991 */
																BgL_globalz00_bglt BgL_varz00_2371;

																{	/* Cnst/alloc.scm 992 */
																	obj_t BgL_arg2215z00_2385;

																	BgL_arg2215z00_2385 =
																		BGl_makezd2typedzd2identz00zzast_identz00
																		(BGl_gensymz00zz__r4_symbols_6_4z00
																		(CNST_TABLE_REF(61)),
																		BgL_veczd2typezd2idz00_2313);
																	BgL_varz00_2371 =
																		BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2
																		(BgL_arg2215z00_2385,
																		BGl_za2moduleza2z00zzmodule_modulez00,
																		CNST_TABLE_REF(2), CNST_TABLE_REF(3));
																}
																if (CBOOL
																	(BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00))
																	{	/* Cnst/alloc.scm 997 */
																		obj_t BgL_arg2204z00_2372;

																		{	/* Cnst/alloc.scm 69 */
																			obj_t BgL_newz00_3322;

																			BgL_newz00_3322 =
																				create_struct(CNST_TABLE_REF(9),
																				(int) (2L));
																			{	/* Cnst/alloc.scm 69 */
																				obj_t BgL_auxz00_5807;
																				int BgL_tmpz00_5805;

																				BgL_auxz00_5807 =
																					((obj_t) BgL_varz00_2371);
																				BgL_tmpz00_5805 = (int) (1L);
																				STRUCT_SET(BgL_newz00_3322,
																					BgL_tmpz00_5805, BgL_auxz00_5807);
																			}
																			{	/* Cnst/alloc.scm 69 */
																				int BgL_tmpz00_5810;

																				BgL_tmpz00_5810 = (int) (0L);
																				STRUCT_SET(BgL_newz00_3322,
																					BgL_tmpz00_5810, BgL_vecz00_70);
																			}
																			BgL_arg2204z00_2372 = BgL_newz00_3322;
																		}
																		BGl_za2vectorzd2envza2zd2zzcnst_allocz00 =
																			MAKE_YOUNG_PAIR(BgL_arg2204z00_2372,
																			BGl_za2vectorzd2envza2zd2zzcnst_allocz00);
																	}
																else
																	{	/* Cnst/alloc.scm 996 */
																		BFALSE;
																	}
																{	/* Cnst/alloc.scm 998 */
																	obj_t BgL_arg2205z00_2373;

																	{	/* Cnst/alloc.scm 998 */
																		obj_t BgL_arg2206z00_2374;

																		{	/* Cnst/alloc.scm 998 */
																			obj_t BgL_arg2207z00_2375;
																			obj_t BgL_arg2208z00_2376;

																			{	/* Cnst/alloc.scm 998 */
																				obj_t BgL_arg2209z00_2377;

																				{	/* Cnst/alloc.scm 998 */
																					obj_t BgL_arg2210z00_2378;
																					obj_t BgL_arg2211z00_2379;

																					BgL_arg2210z00_2378 =
																						(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									BgL_varz00_2371)))->
																						BgL_idz00);
																					BgL_arg2211z00_2379 =
																						MAKE_YOUNG_PAIR(((
																								(BgL_globalz00_bglt)
																								COBJECT(BgL_varz00_2371))->
																							BgL_modulez00), BNIL);
																					BgL_arg2209z00_2377 =
																						MAKE_YOUNG_PAIR(BgL_arg2210z00_2378,
																						BgL_arg2211z00_2379);
																				}
																				BgL_arg2207z00_2375 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																					BgL_arg2209z00_2377);
																			}
																			{	/* Cnst/alloc.scm 999 */
																				BgL_nodez00_bglt BgL_arg2213z00_2381;

																				{	/* Cnst/alloc.scm 953 */
																					BgL_nodez00_bglt BgL_arg2185z00_2340;

																					{	/* Cnst/alloc.scm 953 */
																						BgL_letzd2varzd2_bglt
																							BgL_arg2186z00_2341;
																						BgL_typez00_bglt
																							BgL_arg2187z00_2342;
																						{	/* Cnst/alloc.scm 953 */
																							BgL_localz00_bglt BgL_varz00_2343;
																							obj_t BgL_lz00_2344;

																							BgL_varz00_2343 =
																								BGl_makezd2localzd2svarz00zzast_localz00
																								(CNST_TABLE_REF(48),
																								BgL_veczd2typezd2_2314);
																							BgL_lz00_2344 =
																								BGL_PROCEDURE_CALL1
																								(BgL_vectorzd2ze3listz31_2316,
																								BgL_vecz00_70);
																							{	/* Cnst/alloc.scm 955 */
																								BgL_letzd2varzd2_bglt
																									BgL_new1233z00_2345;
																								{	/* Cnst/alloc.scm 956 */
																									BgL_letzd2varzd2_bglt
																										BgL_new1232z00_2365;
																									BgL_new1232z00_2365 =
																										((BgL_letzd2varzd2_bglt)
																										BOBJECT(GC_MALLOC(sizeof
																												(struct
																													BgL_letzd2varzd2_bgl))));
																									{	/* Cnst/alloc.scm 956 */
																										long BgL_arg2200z00_2366;

																										BgL_arg2200z00_2366 =
																											BGL_CLASS_NUM
																											(BGl_letzd2varzd2zzast_nodez00);
																										BGL_OBJECT_CLASS_NUM_SET((
																												(BgL_objectz00_bglt)
																												BgL_new1232z00_2365),
																											BgL_arg2200z00_2366);
																									}
																									{	/* Cnst/alloc.scm 956 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_5831;
																										BgL_tmpz00_5831 =
																											((BgL_objectz00_bglt)
																											BgL_new1232z00_2365);
																										BGL_OBJECT_WIDENING_SET
																											(BgL_tmpz00_5831, BFALSE);
																									}
																									((BgL_objectz00_bglt)
																										BgL_new1232z00_2365);
																									BgL_new1233z00_2345 =
																										BgL_new1232z00_2365;
																								}
																								((((BgL_nodez00_bglt) COBJECT(
																												((BgL_nodez00_bglt)
																													BgL_new1233z00_2345)))->
																										BgL_locz00) =
																									((obj_t) BgL_locz00_71),
																									BUNSPEC);
																								((((BgL_nodez00_bglt)
																											COBJECT((
																													(BgL_nodez00_bglt)
																													BgL_new1233z00_2345)))->
																										BgL_typez00) =
																									((BgL_typez00_bglt)
																										BGl_strictzd2nodezd2typez00zzast_nodez00
																										(BgL_veczd2typezd2_2314,
																											((BgL_typez00_bglt)
																												BGl_za2objza2z00zztype_cachez00))),
																									BUNSPEC);
																								((((BgL_nodezf2effectzf2_bglt)
																											COBJECT((
																													(BgL_nodezf2effectzf2_bglt)
																													BgL_new1233z00_2345)))->
																										BgL_sidezd2effectzd2) =
																									((obj_t) BUNSPEC), BUNSPEC);
																								((((BgL_nodezf2effectzf2_bglt)
																											COBJECT((
																													(BgL_nodezf2effectzf2_bglt)
																													BgL_new1233z00_2345)))->
																										BgL_keyz00) =
																									((obj_t) BINT(-1L)), BUNSPEC);
																								{
																									obj_t BgL_auxz00_5846;

																									{	/* Cnst/alloc.scm 961 */
																										obj_t BgL_arg2188z00_2346;

																										{	/* Cnst/alloc.scm 961 */
																											BgL_appz00_bglt
																												BgL_arg2190z00_2348;
																											{	/* Cnst/alloc.scm 961 */
																												BgL_appz00_bglt
																													BgL_new1235z00_2349;
																												{	/* Cnst/alloc.scm 968 */
																													BgL_appz00_bglt
																														BgL_new1234z00_2360;
																													BgL_new1234z00_2360 =
																														((BgL_appz00_bglt)
																														BOBJECT(GC_MALLOC
																															(sizeof(struct
																																	BgL_appz00_bgl))));
																													{	/* Cnst/alloc.scm 968 */
																														long
																															BgL_arg2198z00_2361;
																														{	/* Cnst/alloc.scm 968 */
																															obj_t
																																BgL_classz00_3300;
																															BgL_classz00_3300
																																=
																																BGl_appz00zzast_nodez00;
																															BgL_arg2198z00_2361
																																=
																																BGL_CLASS_NUM
																																(BgL_classz00_3300);
																														}
																														BGL_OBJECT_CLASS_NUM_SET
																															(((BgL_objectz00_bglt) BgL_new1234z00_2360), BgL_arg2198z00_2361);
																													}
																													{	/* Cnst/alloc.scm 968 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_5851;
																														BgL_tmpz00_5851 =
																															(
																															(BgL_objectz00_bglt)
																															BgL_new1234z00_2360);
																														BGL_OBJECT_WIDENING_SET
																															(BgL_tmpz00_5851,
																															BFALSE);
																													}
																													((BgL_objectz00_bglt)
																														BgL_new1234z00_2360);
																													BgL_new1235z00_2349 =
																														BgL_new1234z00_2360;
																												}
																												((((BgL_nodez00_bglt)
																															COBJECT((
																																	(BgL_nodez00_bglt)
																																	BgL_new1235z00_2349)))->
																														BgL_locz00) =
																													((obj_t)
																														BgL_locz00_71),
																													BUNSPEC);
																												((((BgL_nodez00_bglt)
																															COBJECT((
																																	(BgL_nodez00_bglt)
																																	BgL_new1235z00_2349)))->
																														BgL_typez00) =
																													((BgL_typez00_bglt)
																														BgL_veczd2typezd2_2314),
																													BUNSPEC);
																												((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1235z00_2349)))->BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
																												((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1235z00_2349)))->BgL_keyz00) = ((obj_t) BINT(-1L)), BUNSPEC);
																												{
																													BgL_varz00_bglt
																														BgL_auxz00_5864;
																													{	/* Cnst/alloc.scm 964 */
																														BgL_refz00_bglt
																															BgL_new1237z00_2350;
																														{	/* Cnst/alloc.scm 967 */
																															BgL_refz00_bglt
																																BgL_new1236z00_2352;
																															BgL_new1236z00_2352
																																=
																																(
																																(BgL_refz00_bglt)
																																BOBJECT
																																(GC_MALLOC
																																	(sizeof(struct
																																			BgL_refz00_bgl))));
																															{	/* Cnst/alloc.scm 967 */
																																long
																																	BgL_arg2192z00_2353;
																																{	/* Cnst/alloc.scm 967 */
																																	obj_t
																																		BgL_classz00_3304;
																																	BgL_classz00_3304
																																		=
																																		BGl_refz00zzast_nodez00;
																																	BgL_arg2192z00_2353
																																		=
																																		BGL_CLASS_NUM
																																		(BgL_classz00_3304);
																																}
																																BGL_OBJECT_CLASS_NUM_SET
																																	(((BgL_objectz00_bglt) BgL_new1236z00_2352), BgL_arg2192z00_2353);
																															}
																															{	/* Cnst/alloc.scm 967 */
																																BgL_objectz00_bglt
																																	BgL_tmpz00_5869;
																																BgL_tmpz00_5869
																																	=
																																	(
																																	(BgL_objectz00_bglt)
																																	BgL_new1236z00_2352);
																																BGL_OBJECT_WIDENING_SET
																																	(BgL_tmpz00_5869,
																																	BFALSE);
																															}
																															((BgL_objectz00_bglt) BgL_new1236z00_2352);
																															BgL_new1237z00_2350
																																=
																																BgL_new1236z00_2352;
																														}
																														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1237z00_2350)))->BgL_locz00) = ((obj_t) BgL_locz00_71), BUNSPEC);
																														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1237z00_2350)))->BgL_typez00) = ((BgL_typez00_bglt) BgL_veczd2typezd2_2314), BUNSPEC);
																														((((BgL_varz00_bglt)
																																	COBJECT((
																																			(BgL_varz00_bglt)
																																			BgL_new1237z00_2350)))->
																																BgL_variablez00)
																															=
																															((BgL_variablez00_bglt) ((BgL_variablez00_bglt) BGl_findzd2globalzd2zzast_envz00(BgL_listzd2ze3vectorz31_2315, BNIL))), BUNSPEC);
																														BgL_auxz00_5864 =
																															((BgL_varz00_bglt)
																															BgL_new1237z00_2350);
																													}
																													((((BgL_appz00_bglt)
																																COBJECT
																																(BgL_new1235z00_2349))->
																															BgL_funz00) =
																														((BgL_varz00_bglt)
																															BgL_auxz00_5864),
																														BUNSPEC);
																												}
																												{
																													obj_t BgL_auxz00_5883;

																													{	/* Cnst/alloc.scm 969 */
																														BgL_kwotez00_bglt
																															BgL_arg2193z00_2354;
																														{	/* Cnst/alloc.scm 969 */
																															BgL_kwotez00_bglt
																																BgL_new1239z00_2356;
																															{	/* Cnst/alloc.scm 973 */
																																BgL_kwotez00_bglt
																																	BgL_new1238z00_2358;
																																BgL_new1238z00_2358
																																	=
																																	(
																																	(BgL_kwotez00_bglt)
																																	BOBJECT
																																	(GC_MALLOC
																																		(sizeof
																																			(struct
																																				BgL_kwotez00_bgl))));
																																{	/* Cnst/alloc.scm 973 */
																																	long
																																		BgL_arg2197z00_2359;
																																	{	/* Cnst/alloc.scm 973 */
																																		obj_t
																																			BgL_classz00_3308;
																																		BgL_classz00_3308
																																			=
																																			BGl_kwotez00zzast_nodez00;
																																		BgL_arg2197z00_2359
																																			=
																																			BGL_CLASS_NUM
																																			(BgL_classz00_3308);
																																	}
																																	BGL_OBJECT_CLASS_NUM_SET
																																		(((BgL_objectz00_bglt) BgL_new1238z00_2358), BgL_arg2197z00_2359);
																																}
																																{	/* Cnst/alloc.scm 973 */
																																	BgL_objectz00_bglt
																																		BgL_tmpz00_5888;
																																	BgL_tmpz00_5888
																																		=
																																		(
																																		(BgL_objectz00_bglt)
																																		BgL_new1238z00_2358);
																																	BGL_OBJECT_WIDENING_SET
																																		(BgL_tmpz00_5888,
																																		BFALSE);
																																}
																																((BgL_objectz00_bglt) BgL_new1238z00_2358);
																																BgL_new1239z00_2356
																																	=
																																	BgL_new1238z00_2358;
																															}
																															((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1239z00_2356)))->BgL_locz00) = ((obj_t) BgL_locz00_71), BUNSPEC);
																															{
																																BgL_typez00_bglt
																																	BgL_auxz00_5894;
																																{	/* Cnst/alloc.scm 972 */
																																	BgL_typez00_bglt
																																		BgL_arg2196z00_2357;
																																	BgL_arg2196z00_2357
																																		=
																																		BGl_getzd2typezd2kwotez00zztype_typeofz00
																																		(BgL_lz00_2344);
																																	BgL_auxz00_5894
																																		=
																																		BGl_strictzd2nodezd2typez00zzast_nodez00
																																		(BgL_arg2196z00_2357,
																																		((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
																																}
																																((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1239z00_2356)))->BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_5894), BUNSPEC);
																															}
																															((((BgL_kwotez00_bglt) COBJECT(BgL_new1239z00_2356))->BgL_valuez00) = ((obj_t) BgL_lz00_2344), BUNSPEC);
																															BgL_arg2193z00_2354
																																=
																																BgL_new1239z00_2356;
																														}
																														{	/* Cnst/alloc.scm 968 */
																															obj_t
																																BgL_list2194z00_2355;
																															BgL_list2194z00_2355
																																=
																																MAKE_YOUNG_PAIR(
																																((obj_t)
																																	BgL_arg2193z00_2354),
																																BNIL);
																															BgL_auxz00_5883 =
																																BgL_list2194z00_2355;
																													}}
																													((((BgL_appz00_bglt)
																																COBJECT
																																(BgL_new1235z00_2349))->
																															BgL_argsz00) =
																														((obj_t)
																															BgL_auxz00_5883),
																														BUNSPEC);
																												}
																												((((BgL_appz00_bglt)
																															COBJECT
																															(BgL_new1235z00_2349))->
																														BgL_stackablez00) =
																													((obj_t) BFALSE),
																													BUNSPEC);
																												BgL_arg2190z00_2348 =
																													BgL_new1235z00_2349;
																											}
																											BgL_arg2188z00_2346 =
																												MAKE_YOUNG_PAIR(
																												((obj_t)
																													BgL_varz00_2343),
																												((obj_t)
																													BgL_arg2190z00_2348));
																										}
																										{	/* Cnst/alloc.scm 959 */
																											obj_t
																												BgL_list2189z00_2347;
																											BgL_list2189z00_2347 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg2188z00_2346,
																												BNIL);
																											BgL_auxz00_5846 =
																												BgL_list2189z00_2347;
																									}}
																									((((BgL_letzd2varzd2_bglt)
																												COBJECT
																												(BgL_new1233z00_2345))->
																											BgL_bindingsz00) =
																										((obj_t) BgL_auxz00_5846),
																										BUNSPEC);
																								}
																								{
																									BgL_nodez00_bglt
																										BgL_auxz00_5910;
																									{	/* Cnst/alloc.scm 974 */
																										BgL_refz00_bglt
																											BgL_new1242z00_2362;
																										{	/* Cnst/alloc.scm 975 */
																											BgL_refz00_bglt
																												BgL_new1240z00_2363;
																											BgL_new1240z00_2363 =
																												((BgL_refz00_bglt)
																												BOBJECT(GC_MALLOC(sizeof
																														(struct
																															BgL_refz00_bgl))));
																											{	/* Cnst/alloc.scm 975 */
																												long
																													BgL_arg2199z00_2364;
																												{	/* Cnst/alloc.scm 975 */
																													obj_t
																														BgL_classz00_3314;
																													BgL_classz00_3314 =
																														BGl_refz00zzast_nodez00;
																													BgL_arg2199z00_2364 =
																														BGL_CLASS_NUM
																														(BgL_classz00_3314);
																												}
																												BGL_OBJECT_CLASS_NUM_SET
																													(((BgL_objectz00_bglt)
																														BgL_new1240z00_2363),
																													BgL_arg2199z00_2364);
																											}
																											{	/* Cnst/alloc.scm 975 */
																												BgL_objectz00_bglt
																													BgL_tmpz00_5915;
																												BgL_tmpz00_5915 =
																													((BgL_objectz00_bglt)
																													BgL_new1240z00_2363);
																												BGL_OBJECT_WIDENING_SET
																													(BgL_tmpz00_5915,
																													BFALSE);
																											}
																											((BgL_objectz00_bglt)
																												BgL_new1240z00_2363);
																											BgL_new1242z00_2362 =
																												BgL_new1240z00_2363;
																										}
																										((((BgL_nodez00_bglt)
																													COBJECT((
																															(BgL_nodez00_bglt)
																															BgL_new1242z00_2362)))->
																												BgL_locz00) =
																											((obj_t) BgL_locz00_71),
																											BUNSPEC);
																										((((BgL_nodez00_bglt)
																													COBJECT((
																															(BgL_nodez00_bglt)
																															BgL_new1242z00_2362)))->
																												BgL_typez00) =
																											((BgL_typez00_bglt)
																												BgL_veczd2typezd2_2314),
																											BUNSPEC);
																										((((BgL_varz00_bglt)
																													COBJECT((
																															(BgL_varz00_bglt)
																															BgL_new1242z00_2362)))->
																												BgL_variablez00) =
																											((BgL_variablez00_bglt) (
																													(BgL_variablez00_bglt)
																													BgL_varz00_2343)),
																											BUNSPEC);
																										BgL_auxz00_5910 =
																											((BgL_nodez00_bglt)
																											BgL_new1242z00_2362);
																									}
																									((((BgL_letzd2varzd2_bglt)
																												COBJECT
																												(BgL_new1233z00_2345))->
																											BgL_bodyz00) =
																										((BgL_nodez00_bglt)
																											BgL_auxz00_5910),
																										BUNSPEC);
																								}
																								((((BgL_letzd2varzd2_bglt)
																											COBJECT
																											(BgL_new1233z00_2345))->
																										BgL_removablezf3zf3) =
																									((bool_t) ((bool_t) 1)),
																									BUNSPEC);
																								BgL_arg2186z00_2341 =
																									BgL_new1233z00_2345;
																						}}
																						BgL_arg2187z00_2342 =
																							BGl_strictzd2nodezd2typez00zzast_nodez00
																							(BgL_veczd2typezd2_2314,
																							((BgL_typez00_bglt)
																								BGl_za2objza2z00zztype_cachez00));
																						BgL_arg2185z00_2340 =
																							BGl_coercez12z12zzcoerce_coercez00
																							(((BgL_nodez00_bglt)
																								BgL_arg2186z00_2341), BUNSPEC,
																							BgL_arg2187z00_2342,
																							((bool_t) 0));
																					}
																					BgL_arg2213z00_2381 =
																						BGl_cnstz12z12zzcnst_nodez00
																						(BgL_arg2185z00_2340);
																				}
																				BgL_arg2208z00_2376 =
																					MAKE_YOUNG_PAIR(
																					((obj_t) BgL_arg2213z00_2381), BNIL);
																			}
																			BgL_arg2206z00_2374 =
																				MAKE_YOUNG_PAIR(BgL_arg2207z00_2375,
																				BgL_arg2208z00_2376);
																		}
																		BgL_arg2205z00_2373 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
																			BgL_arg2206z00_2374);
																	}
																	BGl_za2globalzd2sexpza2zd2zzcnst_allocz00 =
																		MAKE_YOUNG_PAIR(BgL_arg2205z00_2373,
																		BGl_za2globalzd2sexpza2zd2zzcnst_allocz00);
																}
																{	/* Cnst/alloc.scm 1000 */
																	BgL_refz00_bglt BgL_new1244z00_2382;

																	{	/* Cnst/alloc.scm 1001 */
																		BgL_refz00_bglt BgL_new1243z00_2383;

																		BgL_new1243z00_2383 =
																			((BgL_refz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_refz00_bgl))));
																		{	/* Cnst/alloc.scm 1001 */
																			long BgL_arg2214z00_2384;

																			BgL_arg2214z00_2384 =
																				BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1243z00_2383),
																				BgL_arg2214z00_2384);
																		}
																		{	/* Cnst/alloc.scm 1001 */
																			BgL_objectz00_bglt BgL_tmpz00_5944;

																			BgL_tmpz00_5944 =
																				((BgL_objectz00_bglt)
																				BgL_new1243z00_2383);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5944,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1243z00_2383);
																		BgL_new1244z00_2382 = BgL_new1243z00_2383;
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1244z00_2382)))->
																			BgL_locz00) =
																		((obj_t) BgL_locz00_71), BUNSPEC);
																	((((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt)
																						BgL_new1244z00_2382)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) BgL_veczd2typezd2_2314),
																		BUNSPEC);
																	((((BgL_varz00_bglt)
																				COBJECT(((BgL_varz00_bglt)
																						BgL_new1244z00_2382)))->
																			BgL_variablez00) =
																		((BgL_variablez00_bglt) (
																				(BgL_variablez00_bglt)
																				BgL_varz00_2371)), BUNSPEC);
																	BgL_auxz00_5793 = BgL_new1244z00_2382;
															}}
															return ((BgL_nodez00_bglt) BgL_auxz00_5793);
														}
													else
														{
															BgL_appz00_bglt BgL_auxz00_5956;

															{	/* Cnst/alloc.scm 983 */
																long BgL_offsetz00_2368;

																BgL_offsetz00_2368 =
																	BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00;
																BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00 =
																	(1L +
																	BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00);
																BGl_za2globalzd2setza2zd2zzcnst_allocz00 =
																	MAKE_YOUNG_PAIR(BgL_vecz00_70,
																	BGl_za2globalzd2setza2zd2zzcnst_allocz00);
																if (CBOOL
																	(BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00))
																	{	/* Cnst/alloc.scm 987 */
																		obj_t BgL_arg2202z00_2369;

																		{	/* Cnst/alloc.scm 69 */
																			obj_t BgL_newz00_3319;

																			BgL_newz00_3319 =
																				create_struct(CNST_TABLE_REF(9),
																				(int) (2L));
																			{	/* Cnst/alloc.scm 69 */
																				obj_t BgL_auxz00_5966;
																				int BgL_tmpz00_5964;

																				BgL_auxz00_5966 =
																					BINT(BgL_offsetz00_2368);
																				BgL_tmpz00_5964 = (int) (1L);
																				STRUCT_SET(BgL_newz00_3319,
																					BgL_tmpz00_5964, BgL_auxz00_5966);
																			}
																			{	/* Cnst/alloc.scm 69 */
																				int BgL_tmpz00_5969;

																				BgL_tmpz00_5969 = (int) (0L);
																				STRUCT_SET(BgL_newz00_3319,
																					BgL_tmpz00_5969, BgL_vecz00_70);
																			}
																			BgL_arg2202z00_2369 = BgL_newz00_3319;
																		}
																		BGl_za2vectorzd2envza2zd2zzcnst_allocz00 =
																			MAKE_YOUNG_PAIR(BgL_arg2202z00_2369,
																			BGl_za2vectorzd2envza2zd2zzcnst_allocz00);
																	}
																else
																	{	/* Cnst/alloc.scm 986 */
																		BFALSE;
																	}
																BgL_auxz00_5956 =
																	BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00
																	(BINT(BgL_offsetz00_2368),
																	((obj_t) BgL_veczd2typezd2_2314),
																	BgL_locz00_71);
															}
															return ((BgL_nodez00_bglt) BgL_auxz00_5956);
														}
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &cnst-alloc-homogenous-vector */
	BgL_nodez00_bglt
		BGl_z62cnstzd2alloczd2homogenouszd2vectorzb0zzcnst_allocz00(obj_t
		BgL_envz00_3545, obj_t BgL_vecz00_3546, obj_t BgL_locz00_3547)
	{
		{	/* Cnst/alloc.scm 930 */
			return
				BGl_cnstzd2alloczd2homogenouszd2vectorzd2zzcnst_allocz00
				(BgL_vecz00_3546, BgL_locz00_3547);
		}

	}



/* force-initialize-srfi4-library-module! */
	obj_t
		BGl_forcezd2initializa7ezd2srfi4zd2libraryzd2modulez12zb5zzcnst_allocz00
		(void)
	{
		{	/* Cnst/alloc.scm 1032 */
			{	/* Cnst/alloc.scm 1035 */
				obj_t BgL_gz00_2411;

				BgL_gz00_2411 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(62), BNIL);
				{	/* Cnst/alloc.scm 1037 */
					obj_t BgL_arg2233z00_2412;

					BgL_arg2233z00_2412 =
						(((BgL_globalz00_bglt) COBJECT(
								((BgL_globalz00_bglt) BgL_gz00_2411)))->BgL_modulez00);
					return
						BGl_withzd2libraryzd2modulez12z12zzmodule_libraryz00
						(BgL_arg2233z00_2412);
				}
			}
		}

	}



/* cnst-alloc-tvector */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2tvectorz00zzcnst_allocz00(obj_t BgL_tvecz00_72,
		obj_t BgL_locz00_73)
	{
		{	/* Cnst/alloc.scm 1042 */
			{

				if (BGl_tvectorzd2Czd2staticzf3zf3zztvector_cnstz00(BgL_tvecz00_72))
					{
						BgL_refz00_bglt BgL_auxz00_5985;

						{	/* Cnst/alloc.scm 1044 */
							BgL_globalz00_bglt BgL_varz00_2418;

							{	/* Cnst/alloc.scm 1044 */
								obj_t BgL_arg2253z00_2441;

								{	/* Cnst/alloc.scm 1044 */
									obj_t BgL_arg2254z00_2442;

									BgL_arg2254z00_2442 =
										BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(63));
									BgL_arg2253z00_2441 =
										BGl_makezd2typedzd2identz00zzast_identz00
										(BgL_arg2254z00_2442, CNST_TABLE_REF(64));
								}
								BgL_varz00_2418 =
									BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2
									(BgL_arg2253z00_2441, BGl_za2moduleza2z00zzmodule_modulez00,
									BgL_tvecz00_72, CNST_TABLE_REF(65), BgL_locz00_73);
							}
							{	/* Cnst/alloc.scm 1049 */
								obj_t BgL_idz00_2419;

								BgL_idz00_2419 =
									(((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt)
												STRUCT_REF(
													((obj_t) BgL_tvecz00_72), (int) (0L)))))->BgL_idz00);
								{	/* Cnst/alloc.scm 1049 */
									BgL_nodez00_bglt BgL_aidz00_2420;

									{	/* Cnst/alloc.scm 1050 */
										BgL_kwotez00_bglt BgL_arg2247z00_2431;

										{	/* Cnst/alloc.scm 1050 */
											BgL_kwotez00_bglt BgL_new1249z00_2432;

											{	/* Cnst/alloc.scm 1051 */
												BgL_kwotez00_bglt BgL_new1248z00_2435;

												BgL_new1248z00_2435 =
													((BgL_kwotez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_kwotez00_bgl))));
												{	/* Cnst/alloc.scm 1051 */
													long BgL_arg2250z00_2436;

													BgL_arg2250z00_2436 =
														BGL_CLASS_NUM(BGl_kwotez00zzast_nodez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1248z00_2435),
														BgL_arg2250z00_2436);
												}
												{	/* Cnst/alloc.scm 1051 */
													BgL_objectz00_bglt BgL_tmpz00_6001;

													BgL_tmpz00_6001 =
														((BgL_objectz00_bglt) BgL_new1248z00_2435);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6001, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1248z00_2435);
												BgL_new1249z00_2432 = BgL_new1248z00_2435;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1249z00_2432)))->
													BgL_locz00) = ((obj_t) BgL_locz00_73), BUNSPEC);
											{
												BgL_typez00_bglt BgL_auxz00_6007;

												{	/* Cnst/alloc.scm 1053 */
													BgL_typez00_bglt BgL_arg2248z00_2433;
													BgL_typez00_bglt BgL_arg2249z00_2434;

													BgL_arg2248z00_2433 =
														BGl_getzd2typezd2kwotez00zztype_typeofz00
														(BgL_idz00_2419);
													BgL_arg2249z00_2434 =
														BGl_getzd2defaultzd2typez00zztype_cachez00();
													BgL_auxz00_6007 =
														BGl_strictzd2nodezd2typez00zzast_nodez00
														(BgL_arg2248z00_2433, BgL_arg2249z00_2434);
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1249z00_2432)))->
														BgL_typez00) =
													((BgL_typez00_bglt) BgL_auxz00_6007), BUNSPEC);
											}
											((((BgL_kwotez00_bglt) COBJECT(BgL_new1249z00_2432))->
													BgL_valuez00) = ((obj_t) BgL_idz00_2419), BUNSPEC);
											BgL_arg2247z00_2431 = BgL_new1249z00_2432;
										}
										BgL_aidz00_2420 =
											BGl_cnstz12z12zzcnst_nodez00(
											((BgL_nodez00_bglt) BgL_arg2247z00_2431));
									}
									{	/* Cnst/alloc.scm 1050 */

										{	/* Cnst/alloc.scm 1059 */
											obj_t BgL_arg2237z00_2421;

											{	/* Cnst/alloc.scm 1059 */
												obj_t BgL_arg2238z00_2422;

												{	/* Cnst/alloc.scm 1059 */
													obj_t BgL_arg2239z00_2423;
													obj_t BgL_arg2240z00_2424;

													{	/* Cnst/alloc.scm 1059 */
														obj_t BgL_arg2241z00_2425;

														{	/* Cnst/alloc.scm 1059 */
															obj_t BgL_arg2242z00_2426;
															obj_t BgL_arg2243z00_2427;

															BgL_arg2242z00_2426 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt) BgL_varz00_2418)))->
																BgL_idz00);
															BgL_arg2243z00_2427 =
																MAKE_YOUNG_PAIR((((BgL_globalz00_bglt)
																		COBJECT(BgL_varz00_2418))->BgL_modulez00),
																BNIL);
															BgL_arg2241z00_2425 =
																MAKE_YOUNG_PAIR(BgL_arg2242z00_2426,
																BgL_arg2243z00_2427);
														}
														BgL_arg2239z00_2423 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
															BgL_arg2241z00_2425);
													}
													{	/* Cnst/alloc.scm 1060 */
														obj_t BgL_arg2245z00_2429;

														{	/* Cnst/alloc.scm 1060 */
															obj_t BgL_arg2246z00_2430;

															BgL_arg2246z00_2430 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_aidz00_2420), BNIL);
															BgL_arg2245z00_2429 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(66),
																BgL_arg2246z00_2430);
														}
														BgL_arg2240z00_2424 =
															MAKE_YOUNG_PAIR(BgL_arg2245z00_2429, BNIL);
													}
													BgL_arg2238z00_2422 =
														MAKE_YOUNG_PAIR(BgL_arg2239z00_2423,
														BgL_arg2240z00_2424);
												}
												BgL_arg2237z00_2421 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(67),
													BgL_arg2238z00_2422);
											}
											BGl_za2globalzd2sexpza2zd2zzcnst_allocz00 =
												MAKE_YOUNG_PAIR(BgL_arg2237z00_2421,
												BGl_za2globalzd2sexpza2zd2zzcnst_allocz00);
							}}}}
							{	/* Cnst/alloc.scm 1061 */
								BgL_refz00_bglt BgL_new1252z00_2438;

								{	/* Cnst/alloc.scm 1062 */
									BgL_refz00_bglt BgL_new1251z00_2439;

									BgL_new1251z00_2439 =
										((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_refz00_bgl))));
									{	/* Cnst/alloc.scm 1062 */
										long BgL_arg2252z00_2440;

										BgL_arg2252z00_2440 =
											BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1251z00_2439),
											BgL_arg2252z00_2440);
									}
									{	/* Cnst/alloc.scm 1062 */
										BgL_objectz00_bglt BgL_tmpz00_6036;

										BgL_tmpz00_6036 =
											((BgL_objectz00_bglt) BgL_new1251z00_2439);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6036, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1251z00_2439);
									BgL_new1252z00_2438 = BgL_new1251z00_2439;
								}
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_new1252z00_2438)))->
										BgL_locz00) = ((obj_t) BgL_locz00_73), BUNSPEC);
								((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
													BgL_new1252z00_2438)))->BgL_typez00) =
									((BgL_typez00_bglt) (((BgL_variablez00_bglt)
												COBJECT(((BgL_variablez00_bglt) BgL_varz00_2418)))->
											BgL_typez00)), BUNSPEC);
								((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
													BgL_new1252z00_2438)))->BgL_variablez00) =
									((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
											BgL_varz00_2418)), BUNSPEC);
								BgL_auxz00_5985 = BgL_new1252z00_2438;
						}}
						return ((BgL_nodez00_bglt) BgL_auxz00_5985);
					}
				else
					{
						BgL_appz00_bglt BgL_auxz00_6050;

						BGl_internalzd2errorzd2zztools_errorz00
							(BGl_string2431z00zzcnst_allocz00,
							BGl_string2432z00zzcnst_allocz00, BgL_tvecz00_72);
						{	/* Cnst/alloc.scm 1069 */
							long BgL_offsetz00_2444;

							BgL_offsetz00_2444 = BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00;
							BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00 =
								(1L + BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00);
							BGl_za2globalzd2setza2zd2zzcnst_allocz00 =
								MAKE_YOUNG_PAIR(BgL_tvecz00_72,
								BGl_za2globalzd2setza2zd2zzcnst_allocz00);
							{	/* Cnst/alloc.scm 1072 */
								obj_t BgL_arg2256z00_2445;

								BgL_arg2256z00_2445 =
									STRUCT_REF(((obj_t) BgL_tvecz00_72), (int) (0L));
								BgL_auxz00_6050 =
									BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00(BINT
									(BgL_offsetz00_2444), BgL_arg2256z00_2445, BgL_locz00_73);
						}}
						return ((BgL_nodez00_bglt) BgL_auxz00_6050);
					}
			}
		}

	}



/* &cnst-alloc-tvector */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2tvectorz62zzcnst_allocz00(obj_t
		BgL_envz00_3568, obj_t BgL_tvecz00_3569, obj_t BgL_locz00_3570)
	{
		{	/* Cnst/alloc.scm 1042 */
			return
				BGl_cnstzd2alloczd2tvectorz00zzcnst_allocz00(BgL_tvecz00_3569,
				BgL_locz00_3570);
		}

	}



/* cnst-alloc-struct */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_cnstzd2alloczd2structz00zzcnst_allocz00(obj_t BgL_structz00_74,
		obj_t BgL_locz00_75)
	{
		{	/* Cnst/alloc.scm 1080 */
			{

				{	/* Cnst/alloc.scm 1132 */
					obj_t BgL_oldz00_2451;

					{	/* Cnst/alloc.scm 1132 */
						obj_t BgL__andtest_1266z00_2457;

						BgL__andtest_1266z00_2457 =
							BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00;
						if (CBOOL(BgL__andtest_1266z00_2457))
							{
								obj_t BgL_envz00_2459;

								BgL_envz00_2459 = BGl_za2structzd2envza2zd2zzcnst_allocz00;
							BgL_zc3z04anonymousza32260ze3z87_2460:
								if (NULLP(BgL_envz00_2459))
									{	/* Cnst/alloc.scm 1135 */
										BgL_oldz00_2451 = BFALSE;
									}
								else
									{	/* Cnst/alloc.scm 1137 */
										bool_t BgL_test2570z00_6065;

										{	/* Cnst/alloc.scm 1137 */
											obj_t BgL_arg2266z00_2466;

											{	/* Cnst/alloc.scm 69 */
												obj_t BgL_sz00_3398;

												BgL_sz00_3398 = CAR(((obj_t) BgL_envz00_2459));
												BgL_arg2266z00_2466 =
													STRUCT_REF(BgL_sz00_3398, (int) (0L));
											}
											BgL_test2570z00_6065 =
												BGl_cnstzd2equalzf3z21zzcnst_allocz00
												(BgL_arg2266z00_2466, BgL_structz00_74);
										}
										if (BgL_test2570z00_6065)
											{	/* Cnst/alloc.scm 1137 */
												BgL_oldz00_2451 = CAR(((obj_t) BgL_envz00_2459));
											}
										else
											{	/* Cnst/alloc.scm 1140 */
												obj_t BgL_arg2265z00_2465;

												BgL_arg2265z00_2465 = CDR(((obj_t) BgL_envz00_2459));
												{
													obj_t BgL_envz00_6075;

													BgL_envz00_6075 = BgL_arg2265z00_2465;
													BgL_envz00_2459 = BgL_envz00_6075;
													goto BgL_zc3z04anonymousza32260ze3z87_2460;
												}
											}
									}
							}
						else
							{	/* Cnst/alloc.scm 1132 */
								BgL_oldz00_2451 = BFALSE;
							}
					}
					if (CBOOL(BgL_oldz00_2451))
						{	/* Cnst/alloc.scm 1142 */
							if (
								(BGl_za2initzd2modeza2zd2zzengine_paramz00 ==
									CNST_TABLE_REF(14)))
								{	/* Cnst/alloc.scm 1144 */
									BgL_refz00_bglt BgL_new1268z00_2452;

									{	/* Cnst/alloc.scm 1145 */
										BgL_refz00_bglt BgL_new1267z00_2454;

										BgL_new1267z00_2454 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Cnst/alloc.scm 1145 */
											long BgL_arg2258z00_2455;

											BgL_arg2258z00_2455 =
												BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1267z00_2454),
												BgL_arg2258z00_2455);
										}
										{	/* Cnst/alloc.scm 1145 */
											BgL_objectz00_bglt BgL_tmpz00_6085;

											BgL_tmpz00_6085 =
												((BgL_objectz00_bglt) BgL_new1267z00_2454);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6085, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1267z00_2454);
										BgL_new1268z00_2452 = BgL_new1267z00_2454;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1268z00_2452)))->
											BgL_locz00) = ((obj_t) BgL_locz00_75), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1268z00_2452)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_variablez00_bglt)
													COBJECT(((BgL_variablez00_bglt) STRUCT_REF(((obj_t)
																	BgL_oldz00_2451),
																(int) (1L)))))->BgL_typez00)), BUNSPEC);
									((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
														BgL_new1268z00_2452)))->BgL_variablez00) =
										((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
												STRUCT_REF(((obj_t) BgL_oldz00_2451), (int) (1L)))),
										BUNSPEC);
									return ((BgL_nodez00_bglt) BgL_new1268z00_2452);
								}
							else
								{	/* Cnst/alloc.scm 1148 */
									obj_t BgL_arg2259z00_2456;

									BgL_arg2259z00_2456 =
										STRUCT_REF(((obj_t) BgL_oldz00_2451), (int) (1L));
									return
										((BgL_nodez00_bglt)
										BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00
										(BgL_arg2259z00_2456, BGl_za2structza2z00zztype_cachez00,
											BgL_locz00_75));
								}
						}
					else
						{	/* Cnst/alloc.scm 1142 */
							if (
								(BGl_za2initzd2modeza2zd2zzengine_paramz00 ==
									CNST_TABLE_REF(14)))
								{
									BgL_refz00_bglt BgL_auxz00_6113;

									{	/* Cnst/alloc.scm 1120 */
										BgL_globalz00_bglt BgL_varz00_2500;

										{	/* Cnst/alloc.scm 1120 */
											obj_t BgL_arg2299z00_2514;

											{	/* Cnst/alloc.scm 1120 */
												obj_t BgL_arg2301z00_2515;

												BgL_arg2301z00_2515 =
													BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF
													(68));
												BgL_arg2299z00_2514 =
													BGl_makezd2typedzd2identz00zzast_identz00
													(BgL_arg2301z00_2515, CNST_TABLE_REF(68));
											}
											BgL_varz00_2500 =
												BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2
												(BgL_arg2299z00_2514,
												BGl_za2moduleza2z00zzmodule_modulez00,
												CNST_TABLE_REF(69), CNST_TABLE_REF(3));
										}
										if (CBOOL(BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00))
											{	/* Cnst/alloc.scm 1125 */
												obj_t BgL_arg2288z00_2501;

												{	/* Cnst/alloc.scm 69 */
													obj_t BgL_newz00_3387;

													BgL_newz00_3387 =
														create_struct(CNST_TABLE_REF(9), (int) (2L));
													{	/* Cnst/alloc.scm 69 */
														obj_t BgL_auxz00_6128;
														int BgL_tmpz00_6126;

														BgL_auxz00_6128 = ((obj_t) BgL_varz00_2500);
														BgL_tmpz00_6126 = (int) (1L);
														STRUCT_SET(BgL_newz00_3387, BgL_tmpz00_6126,
															BgL_auxz00_6128);
													}
													{	/* Cnst/alloc.scm 69 */
														int BgL_tmpz00_6131;

														BgL_tmpz00_6131 = (int) (0L);
														STRUCT_SET(BgL_newz00_3387, BgL_tmpz00_6131,
															BgL_structz00_74);
													}
													BgL_arg2288z00_2501 = BgL_newz00_3387;
												}
												BGl_za2structzd2envza2zd2zzcnst_allocz00 =
													MAKE_YOUNG_PAIR(BgL_arg2288z00_2501,
													BGl_za2structzd2envza2zd2zzcnst_allocz00);
											}
										else
											{	/* Cnst/alloc.scm 1124 */
												BFALSE;
											}
										{	/* Cnst/alloc.scm 1126 */
											obj_t BgL_arg2289z00_2502;

											{	/* Cnst/alloc.scm 1126 */
												obj_t BgL_arg2290z00_2503;

												{	/* Cnst/alloc.scm 1126 */
													obj_t BgL_arg2291z00_2504;
													obj_t BgL_arg2292z00_2505;

													{	/* Cnst/alloc.scm 1126 */
														obj_t BgL_arg2293z00_2506;

														{	/* Cnst/alloc.scm 1126 */
															obj_t BgL_arg2294z00_2507;
															obj_t BgL_arg2295z00_2508;

															BgL_arg2294z00_2507 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt) BgL_varz00_2500)))->
																BgL_idz00);
															BgL_arg2295z00_2508 =
																MAKE_YOUNG_PAIR((((BgL_globalz00_bglt)
																		COBJECT(BgL_varz00_2500))->BgL_modulez00),
																BNIL);
															BgL_arg2293z00_2506 =
																MAKE_YOUNG_PAIR(BgL_arg2294z00_2507,
																BgL_arg2295z00_2508);
														}
														BgL_arg2291z00_2504 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
															BgL_arg2293z00_2506);
													}
													{	/* Cnst/alloc.scm 1127 */
														BgL_nodez00_bglt BgL_arg2297z00_2510;

														{	/* Cnst/alloc.scm 1084 */
															BgL_nodez00_bglt BgL_arg2269z00_2470;

															{	/* Cnst/alloc.scm 1084 */
																BgL_letzd2varzd2_bglt BgL_arg2270z00_2471;
																BgL_typez00_bglt BgL_arg2271z00_2472;

																{	/* Cnst/alloc.scm 1084 */
																	BgL_localz00_bglt BgL_varz00_2473;
																	obj_t BgL_lz00_2474;

																	BgL_varz00_2473 =
																		BGl_makezd2localzd2svarz00zzast_localz00
																		(CNST_TABLE_REF(48),
																		((BgL_typez00_bglt)
																			BGl_za2structza2z00zztype_cachez00));
																	BgL_lz00_2474 =
																		BGl_structzd2ze3listz31zz__structurez00
																		(BgL_structz00_74);
																	{	/* Cnst/alloc.scm 1086 */
																		BgL_letzd2varzd2_bglt BgL_new1254z00_2475;

																		{	/* Cnst/alloc.scm 1087 */
																			BgL_letzd2varzd2_bglt BgL_new1253z00_2494;

																			BgL_new1253z00_2494 =
																				((BgL_letzd2varzd2_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_letzd2varzd2_bgl))));
																			{	/* Cnst/alloc.scm 1087 */
																				long BgL_arg2283z00_2495;

																				BgL_arg2283z00_2495 =
																					BGL_CLASS_NUM
																					(BGl_letzd2varzd2zzast_nodez00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1253z00_2494),
																					BgL_arg2283z00_2495);
																			}
																			{	/* Cnst/alloc.scm 1087 */
																				BgL_objectz00_bglt BgL_tmpz00_6150;

																				BgL_tmpz00_6150 =
																					((BgL_objectz00_bglt)
																					BgL_new1253z00_2494);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6150,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1253z00_2494);
																			BgL_new1254z00_2475 = BgL_new1253z00_2494;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1254z00_2475)))->
																				BgL_locz00) =
																			((obj_t) BgL_locz00_75), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1254z00_2475)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt)
																				BGl_strictzd2nodezd2typez00zzast_nodez00
																				(((BgL_typez00_bglt)
																						BGl_za2structza2z00zztype_cachez00),
																					((BgL_typez00_bglt)
																						BGl_za2objza2z00zztype_cachez00))),
																			BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1254z00_2475)))->
																				BgL_sidezd2effectzd2) =
																			((obj_t) BUNSPEC), BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1254z00_2475)))->
																				BgL_keyz00) =
																			((obj_t) BINT(-1L)), BUNSPEC);
																		{
																			obj_t BgL_auxz00_6166;

																			{	/* Cnst/alloc.scm 1092 */
																				obj_t BgL_arg2272z00_2476;

																				{	/* Cnst/alloc.scm 1092 */
																					BgL_appz00_bglt BgL_arg2274z00_2478;

																					{	/* Cnst/alloc.scm 1092 */
																						BgL_appz00_bglt BgL_new1256z00_2479;

																						{	/* Cnst/alloc.scm 1099 */
																							BgL_appz00_bglt
																								BgL_new1255z00_2489;
																							BgL_new1255z00_2489 =
																								((BgL_appz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_appz00_bgl))));
																							{	/* Cnst/alloc.scm 1099 */
																								long BgL_arg2281z00_2490;

																								{	/* Cnst/alloc.scm 1099 */
																									obj_t BgL_classz00_3363;

																									BgL_classz00_3363 =
																										BGl_appz00zzast_nodez00;
																									BgL_arg2281z00_2490 =
																										BGL_CLASS_NUM
																										(BgL_classz00_3363);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1255z00_2489),
																									BgL_arg2281z00_2490);
																							}
																							{	/* Cnst/alloc.scm 1099 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_6171;
																								BgL_tmpz00_6171 =
																									((BgL_objectz00_bglt)
																									BgL_new1255z00_2489);
																								BGL_OBJECT_WIDENING_SET
																									(BgL_tmpz00_6171, BFALSE);
																							}
																							((BgL_objectz00_bglt)
																								BgL_new1255z00_2489);
																							BgL_new1256z00_2479 =
																								BgL_new1255z00_2489;
																						}
																						((((BgL_nodez00_bglt) COBJECT(
																										((BgL_nodez00_bglt)
																											BgL_new1256z00_2479)))->
																								BgL_locz00) =
																							((obj_t) BgL_locz00_75), BUNSPEC);
																						((((BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt)
																											BgL_new1256z00_2479)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) (
																									(BgL_typez00_bglt)
																									BGl_za2structza2z00zztype_cachez00)),
																							BUNSPEC);
																						((((BgL_nodezf2effectzf2_bglt)
																									COBJECT((
																											(BgL_nodezf2effectzf2_bglt)
																											BgL_new1256z00_2479)))->
																								BgL_sidezd2effectzd2) =
																							((obj_t) BUNSPEC), BUNSPEC);
																						((((BgL_nodezf2effectzf2_bglt)
																									COBJECT((
																											(BgL_nodezf2effectzf2_bglt)
																											BgL_new1256z00_2479)))->
																								BgL_keyz00) =
																							((obj_t) BINT(-1L)), BUNSPEC);
																						{
																							BgL_varz00_bglt BgL_auxz00_6185;

																							{	/* Cnst/alloc.scm 1095 */
																								BgL_refz00_bglt
																									BgL_new1258z00_2480;
																								{	/* Cnst/alloc.scm 1098 */
																									BgL_refz00_bglt
																										BgL_new1257z00_2481;
																									BgL_new1257z00_2481 =
																										((BgL_refz00_bglt)
																										BOBJECT(GC_MALLOC(sizeof
																												(struct
																													BgL_refz00_bgl))));
																									{	/* Cnst/alloc.scm 1098 */
																										long BgL_arg2275z00_2482;

																										{	/* Cnst/alloc.scm 1098 */
																											obj_t BgL_classz00_3367;

																											BgL_classz00_3367 =
																												BGl_refz00zzast_nodez00;
																											BgL_arg2275z00_2482 =
																												BGL_CLASS_NUM
																												(BgL_classz00_3367);
																										}
																										BGL_OBJECT_CLASS_NUM_SET(
																											((BgL_objectz00_bglt)
																												BgL_new1257z00_2481),
																											BgL_arg2275z00_2482);
																									}
																									{	/* Cnst/alloc.scm 1098 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_6190;
																										BgL_tmpz00_6190 =
																											((BgL_objectz00_bglt)
																											BgL_new1257z00_2481);
																										BGL_OBJECT_WIDENING_SET
																											(BgL_tmpz00_6190, BFALSE);
																									}
																									((BgL_objectz00_bglt)
																										BgL_new1257z00_2481);
																									BgL_new1258z00_2480 =
																										BgL_new1257z00_2481;
																								}
																								((((BgL_nodez00_bglt) COBJECT(
																												((BgL_nodez00_bglt)
																													BgL_new1258z00_2480)))->
																										BgL_locz00) =
																									((obj_t) BgL_locz00_75),
																									BUNSPEC);
																								{
																									BgL_typez00_bglt
																										BgL_auxz00_6196;
																									{	/* Cnst/alloc.scm 1097 */
																										BgL_variablez00_bglt
																											BgL_oz00_3371;
																										BgL_oz00_3371 =
																											((BgL_variablez00_bglt)
																											BGl_za2listzd2ze3structza2z31zzcnst_cachez00);
																										BgL_auxz00_6196 =
																											(((BgL_variablez00_bglt)
																												COBJECT
																												(BgL_oz00_3371))->
																											BgL_typez00);
																									}
																									((((BgL_nodez00_bglt) COBJECT(
																													((BgL_nodez00_bglt)
																														BgL_new1258z00_2480)))->
																											BgL_typez00) =
																										((BgL_typez00_bglt)
																											BgL_auxz00_6196),
																										BUNSPEC);
																								}
																								((((BgL_varz00_bglt) COBJECT(
																												((BgL_varz00_bglt)
																													BgL_new1258z00_2480)))->
																										BgL_variablez00) =
																									((BgL_variablez00_bglt) (
																											(BgL_variablez00_bglt)
																											BGl_za2listzd2ze3structza2z31zzcnst_cachez00)),
																									BUNSPEC);
																								BgL_auxz00_6185 =
																									((BgL_varz00_bglt)
																									BgL_new1258z00_2480);
																							}
																							((((BgL_appz00_bglt)
																										COBJECT
																										(BgL_new1256z00_2479))->
																									BgL_funz00) =
																								((BgL_varz00_bglt)
																									BgL_auxz00_6185), BUNSPEC);
																						}
																						{
																							obj_t BgL_auxz00_6206;

																							{	/* Cnst/alloc.scm 1100 */
																								BgL_kwotez00_bglt
																									BgL_arg2276z00_2483;
																								{	/* Cnst/alloc.scm 1100 */
																									BgL_kwotez00_bglt
																										BgL_new1261z00_2485;
																									{	/* Cnst/alloc.scm 1104 */
																										BgL_kwotez00_bglt
																											BgL_new1260z00_2487;
																										BgL_new1260z00_2487 =
																											((BgL_kwotez00_bglt)
																											BOBJECT(GC_MALLOC(sizeof
																													(struct
																														BgL_kwotez00_bgl))));
																										{	/* Cnst/alloc.scm 1104 */
																											long BgL_arg2280z00_2488;

																											{	/* Cnst/alloc.scm 1104 */
																												obj_t BgL_classz00_3372;

																												BgL_classz00_3372 =
																													BGl_kwotez00zzast_nodez00;
																												BgL_arg2280z00_2488 =
																													BGL_CLASS_NUM
																													(BgL_classz00_3372);
																											}
																											BGL_OBJECT_CLASS_NUM_SET(
																												((BgL_objectz00_bglt)
																													BgL_new1260z00_2487),
																												BgL_arg2280z00_2488);
																										}
																										{	/* Cnst/alloc.scm 1104 */
																											BgL_objectz00_bglt
																												BgL_tmpz00_6211;
																											BgL_tmpz00_6211 =
																												((BgL_objectz00_bglt)
																												BgL_new1260z00_2487);
																											BGL_OBJECT_WIDENING_SET
																												(BgL_tmpz00_6211,
																												BFALSE);
																										}
																										((BgL_objectz00_bglt)
																											BgL_new1260z00_2487);
																										BgL_new1261z00_2485 =
																											BgL_new1260z00_2487;
																									}
																									((((BgL_nodez00_bglt) COBJECT(
																													((BgL_nodez00_bglt)
																														BgL_new1261z00_2485)))->
																											BgL_locz00) =
																										((obj_t) BgL_locz00_75),
																										BUNSPEC);
																									{
																										BgL_typez00_bglt
																											BgL_auxz00_6217;
																										{	/* Cnst/alloc.scm 1103 */
																											BgL_typez00_bglt
																												BgL_arg2279z00_2486;
																											BgL_arg2279z00_2486 =
																												BGl_getzd2typezd2kwotez00zztype_typeofz00
																												(BgL_lz00_2474);
																											BgL_auxz00_6217 =
																												BGl_strictzd2nodezd2typez00zzast_nodez00
																												(BgL_arg2279z00_2486,
																												((BgL_typez00_bglt)
																													BGl_za2objza2z00zztype_cachez00));
																										}
																										((((BgL_nodez00_bglt)
																													COBJECT((
																															(BgL_nodez00_bglt)
																															BgL_new1261z00_2485)))->
																												BgL_typez00) =
																											((BgL_typez00_bglt)
																												BgL_auxz00_6217),
																											BUNSPEC);
																									}
																									((((BgL_kwotez00_bglt)
																												COBJECT
																												(BgL_new1261z00_2485))->
																											BgL_valuez00) =
																										((obj_t) BgL_lz00_2474),
																										BUNSPEC);
																									BgL_arg2276z00_2483 =
																										BgL_new1261z00_2485;
																								}
																								{	/* Cnst/alloc.scm 1099 */
																									obj_t BgL_list2277z00_2484;

																									BgL_list2277z00_2484 =
																										MAKE_YOUNG_PAIR(
																										((obj_t)
																											BgL_arg2276z00_2483),
																										BNIL);
																									BgL_auxz00_6206 =
																										BgL_list2277z00_2484;
																							}}
																							((((BgL_appz00_bglt)
																										COBJECT
																										(BgL_new1256z00_2479))->
																									BgL_argsz00) =
																								((obj_t) BgL_auxz00_6206),
																								BUNSPEC);
																						}
																						((((BgL_appz00_bglt)
																									COBJECT
																									(BgL_new1256z00_2479))->
																								BgL_stackablez00) =
																							((obj_t) BFALSE), BUNSPEC);
																						BgL_arg2274z00_2478 =
																							BgL_new1256z00_2479;
																					}
																					BgL_arg2272z00_2476 =
																						MAKE_YOUNG_PAIR(
																						((obj_t) BgL_varz00_2473),
																						((obj_t) BgL_arg2274z00_2478));
																				}
																				{	/* Cnst/alloc.scm 1090 */
																					obj_t BgL_list2273z00_2477;

																					BgL_list2273z00_2477 =
																						MAKE_YOUNG_PAIR(BgL_arg2272z00_2476,
																						BNIL);
																					BgL_auxz00_6166 =
																						BgL_list2273z00_2477;
																			}}
																			((((BgL_letzd2varzd2_bglt)
																						COBJECT(BgL_new1254z00_2475))->
																					BgL_bindingsz00) =
																				((obj_t) BgL_auxz00_6166), BUNSPEC);
																		}
																		{
																			BgL_nodez00_bglt BgL_auxz00_6233;

																			{	/* Cnst/alloc.scm 1105 */
																				BgL_refz00_bglt BgL_new1263z00_2491;

																				{	/* Cnst/alloc.scm 1106 */
																					BgL_refz00_bglt BgL_new1262z00_2492;

																					BgL_new1262z00_2492 =
																						((BgL_refz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_refz00_bgl))));
																					{	/* Cnst/alloc.scm 1106 */
																						long BgL_arg2282z00_2493;

																						{	/* Cnst/alloc.scm 1106 */
																							obj_t BgL_classz00_3378;

																							BgL_classz00_3378 =
																								BGl_refz00zzast_nodez00;
																							BgL_arg2282z00_2493 =
																								BGL_CLASS_NUM
																								(BgL_classz00_3378);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1262z00_2492),
																							BgL_arg2282z00_2493);
																					}
																					{	/* Cnst/alloc.scm 1106 */
																						BgL_objectz00_bglt BgL_tmpz00_6238;

																						BgL_tmpz00_6238 =
																							((BgL_objectz00_bglt)
																							BgL_new1262z00_2492);
																						BGL_OBJECT_WIDENING_SET
																							(BgL_tmpz00_6238, BFALSE);
																					}
																					((BgL_objectz00_bglt)
																						BgL_new1262z00_2492);
																					BgL_new1263z00_2491 =
																						BgL_new1262z00_2492;
																				}
																				((((BgL_nodez00_bglt) COBJECT(
																								((BgL_nodez00_bglt)
																									BgL_new1263z00_2491)))->
																						BgL_locz00) =
																					((obj_t) BgL_locz00_75), BUNSPEC);
																				((((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt)
																									BgL_new1263z00_2491)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) ((
																								(BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										BgL_varz00_2473)))->
																							BgL_typez00)), BUNSPEC);
																				((((BgL_varz00_bglt)
																							COBJECT(((BgL_varz00_bglt)
																									BgL_new1263z00_2491)))->
																						BgL_variablez00) =
																					((BgL_variablez00_bglt) (
																							(BgL_variablez00_bglt)
																							BgL_varz00_2473)), BUNSPEC);
																				BgL_auxz00_6233 =
																					((BgL_nodez00_bglt)
																					BgL_new1263z00_2491);
																			}
																			((((BgL_letzd2varzd2_bglt)
																						COBJECT(BgL_new1254z00_2475))->
																					BgL_bodyz00) =
																				((BgL_nodez00_bglt) BgL_auxz00_6233),
																				BUNSPEC);
																		}
																		((((BgL_letzd2varzd2_bglt)
																					COBJECT(BgL_new1254z00_2475))->
																				BgL_removablezf3zf3) =
																			((bool_t) ((bool_t) 1)), BUNSPEC);
																		BgL_arg2270z00_2471 = BgL_new1254z00_2475;
																}}
																BgL_arg2271z00_2472 =
																	BGl_strictzd2nodezd2typez00zzast_nodez00(
																	((BgL_typez00_bglt)
																		BGl_za2structza2z00zztype_cachez00),
																	((BgL_typez00_bglt)
																		BGl_za2objza2z00zztype_cachez00));
																BgL_arg2269z00_2470 =
																	BGl_coercez12z12zzcoerce_coercez00((
																		(BgL_nodez00_bglt) BgL_arg2270z00_2471),
																	BUNSPEC, BgL_arg2271z00_2472, ((bool_t) 0));
															}
															BgL_arg2297z00_2510 =
																BGl_cnstz12z12zzcnst_nodez00
																(BgL_arg2269z00_2470);
														}
														BgL_arg2292z00_2505 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg2297z00_2510), BNIL);
													}
													BgL_arg2290z00_2503 =
														MAKE_YOUNG_PAIR(BgL_arg2291z00_2504,
														BgL_arg2292z00_2505);
												}
												BgL_arg2289z00_2502 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
													BgL_arg2290z00_2503);
											}
											BGl_za2globalzd2sexpza2zd2zzcnst_allocz00 =
												MAKE_YOUNG_PAIR(BgL_arg2289z00_2502,
												BGl_za2globalzd2sexpza2zd2zzcnst_allocz00);
										}
										{	/* Cnst/alloc.scm 1128 */
											BgL_refz00_bglt BgL_new1265z00_2511;

											{	/* Cnst/alloc.scm 1129 */
												BgL_refz00_bglt BgL_new1264z00_2512;

												BgL_new1264z00_2512 =
													((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_refz00_bgl))));
												{	/* Cnst/alloc.scm 1129 */
													long BgL_arg2298z00_2513;

													BgL_arg2298z00_2513 =
														BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1264z00_2512),
														BgL_arg2298z00_2513);
												}
												{	/* Cnst/alloc.scm 1129 */
													BgL_objectz00_bglt BgL_tmpz00_6270;

													BgL_tmpz00_6270 =
														((BgL_objectz00_bglt) BgL_new1264z00_2512);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6270, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1264z00_2512);
												BgL_new1265z00_2511 = BgL_new1264z00_2512;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1265z00_2511)))->
													BgL_locz00) = ((obj_t) BgL_locz00_75), BUNSPEC);
											((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_new1265z00_2511)))->BgL_typez00) =
												((BgL_typez00_bglt) (((BgL_variablez00_bglt)
															COBJECT(((BgL_variablez00_bglt)
																	BgL_varz00_2500)))->BgL_typez00)), BUNSPEC);
											((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																BgL_new1265z00_2511)))->BgL_variablez00) =
												((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
														BgL_varz00_2500)), BUNSPEC);
											BgL_auxz00_6113 = BgL_new1265z00_2511;
									}}
									return ((BgL_nodez00_bglt) BgL_auxz00_6113);
								}
							else
								{
									BgL_appz00_bglt BgL_auxz00_6284;

									{	/* Cnst/alloc.scm 1113 */
										long BgL_offsetz00_2497;

										BgL_offsetz00_2497 =
											BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00;
										BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00 =
											(1L + BGl_za2cnstzd2offsetza2zd2zzcnst_allocz00);
										BGl_za2globalzd2setza2zd2zzcnst_allocz00 =
											MAKE_YOUNG_PAIR(BgL_structz00_74,
											BGl_za2globalzd2setza2zd2zzcnst_allocz00);
										if (CBOOL(BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00))
											{	/* Cnst/alloc.scm 1117 */
												obj_t BgL_arg2286z00_2498;

												{	/* Cnst/alloc.scm 69 */
													obj_t BgL_newz00_3384;

													BgL_newz00_3384 =
														create_struct(CNST_TABLE_REF(9), (int) (2L));
													{	/* Cnst/alloc.scm 69 */
														obj_t BgL_auxz00_6294;
														int BgL_tmpz00_6292;

														BgL_auxz00_6294 = BINT(BgL_offsetz00_2497);
														BgL_tmpz00_6292 = (int) (1L);
														STRUCT_SET(BgL_newz00_3384, BgL_tmpz00_6292,
															BgL_auxz00_6294);
													}
													{	/* Cnst/alloc.scm 69 */
														int BgL_tmpz00_6297;

														BgL_tmpz00_6297 = (int) (0L);
														STRUCT_SET(BgL_newz00_3384, BgL_tmpz00_6297,
															BgL_structz00_74);
													}
													BgL_arg2286z00_2498 = BgL_newz00_3384;
												}
												BGl_za2structzd2envza2zd2zzcnst_allocz00 =
													MAKE_YOUNG_PAIR(BgL_arg2286z00_2498,
													BGl_za2structzd2envza2zd2zzcnst_allocz00);
											}
										else
											{	/* Cnst/alloc.scm 1116 */
												BFALSE;
											}
										BgL_auxz00_6284 =
											BGl_makezd2cnstzd2tablezd2refzd2zzcnst_allocz00(BINT
											(BgL_offsetz00_2497), BGl_za2structza2z00zztype_cachez00,
											BgL_locz00_75);
									}
									return ((BgL_nodez00_bglt) BgL_auxz00_6284);
								}
						}
				}
			}
		}

	}



/* &cnst-alloc-struct */
	BgL_nodez00_bglt BGl_z62cnstzd2alloczd2structz62zzcnst_allocz00(obj_t
		BgL_envz00_3571, obj_t BgL_structz00_3572, obj_t BgL_locz00_3573)
	{
		{	/* Cnst/alloc.scm 1080 */
			return
				BGl_cnstzd2alloczd2structz00zzcnst_allocz00(BgL_structz00_3572,
				BgL_locz00_3573);
		}

	}



/* cnst-equal? */
	bool_t BGl_cnstzd2equalzf3z21zzcnst_allocz00(obj_t BgL_obj1z00_76,
		obj_t BgL_obj2z00_77)
	{
		{	/* Cnst/alloc.scm 1157 */
		BGl_cnstzd2equalzf3z21zzcnst_allocz00:
			if ((BgL_obj1z00_76 == BgL_obj2z00_77))
				{	/* Cnst/alloc.scm 1159 */
					return ((bool_t) 1);
				}
			else
				{	/* Cnst/alloc.scm 1159 */
					if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_obj1z00_76))
						{	/* Cnst/alloc.scm 1161 */
							return ((bool_t) 0);
						}
					else
						{	/* Cnst/alloc.scm 1161 */
							if (STRINGP(BgL_obj1z00_76))
								{	/* Cnst/alloc.scm 1163 */
									if (STRINGP(BgL_obj2z00_77))
										{	/* Cnst/alloc.scm 1164 */
											long BgL_l1z00_3411;

											BgL_l1z00_3411 = STRING_LENGTH(BgL_obj1z00_76);
											if ((BgL_l1z00_3411 == STRING_LENGTH(BgL_obj2z00_77)))
												{	/* Cnst/alloc.scm 1164 */
													int BgL_arg1282z00_3414;

													{	/* Cnst/alloc.scm 1164 */
														char *BgL_auxz00_6319;
														char *BgL_tmpz00_6317;

														BgL_auxz00_6319 = BSTRING_TO_STRING(BgL_obj2z00_77);
														BgL_tmpz00_6317 = BSTRING_TO_STRING(BgL_obj1z00_76);
														BgL_arg1282z00_3414 =
															memcmp(BgL_tmpz00_6317, BgL_auxz00_6319,
															BgL_l1z00_3411);
													}
													return ((long) (BgL_arg1282z00_3414) == 0L);
												}
											else
												{	/* Cnst/alloc.scm 1164 */
													return ((bool_t) 0);
												}
										}
									else
										{	/* Cnst/alloc.scm 1164 */
											return ((bool_t) 0);
										}
								}
							else
								{	/* Cnst/alloc.scm 1163 */
									if (SYMBOLP(BgL_obj1z00_76))
										{	/* Cnst/alloc.scm 1165 */
											return ((bool_t) 0);
										}
									else
										{	/* Cnst/alloc.scm 1165 */
											if (PAIRP(BgL_obj1z00_76))
												{	/* Cnst/alloc.scm 1167 */
													if (PAIRP(BgL_obj2z00_77))
														{	/* Cnst/alloc.scm 1168 */
															if (BGl_cnstzd2equalzf3z21zzcnst_allocz00(CAR
																	(BgL_obj1z00_76), CAR(BgL_obj2z00_77)))
																{
																	obj_t BgL_obj2z00_6336;
																	obj_t BgL_obj1z00_6334;

																	BgL_obj1z00_6334 = CDR(BgL_obj1z00_76);
																	BgL_obj2z00_6336 = CDR(BgL_obj2z00_77);
																	BgL_obj2z00_77 = BgL_obj2z00_6336;
																	BgL_obj1z00_76 = BgL_obj1z00_6334;
																	goto BGl_cnstzd2equalzf3z21zzcnst_allocz00;
																}
															else
																{	/* Cnst/alloc.scm 1169 */
																	return ((bool_t) 0);
																}
														}
													else
														{	/* Cnst/alloc.scm 1168 */
															return ((bool_t) 0);
														}
												}
											else
												{	/* Cnst/alloc.scm 1167 */
													if (VECTORP(BgL_obj1z00_76))
														{	/* Cnst/alloc.scm 1171 */
															if (VECTORP(BgL_obj2z00_77))
																{	/* Cnst/alloc.scm 1173 */
																	if (
																		(VECTOR_LENGTH(BgL_obj2z00_77) ==
																			VECTOR_LENGTH(BgL_obj1z00_76)))
																		{	/* Cnst/alloc.scm 1175 */
																			bool_t BgL_test2588z00_6346;

																			{	/* Cnst/alloc.scm 1175 */
																				int BgL_arg2315z00_2544;
																				int BgL_arg2316z00_2545;

																				BgL_arg2315z00_2544 =
																					VECTOR_TAG(BgL_obj1z00_76);
																				BgL_arg2316z00_2545 =
																					VECTOR_TAG(BgL_obj2z00_77);
																				BgL_test2588z00_6346 =
																					(
																					(long) (BgL_arg2315z00_2544) ==
																					(long) (BgL_arg2316z00_2545));
																			}
																			if (BgL_test2588z00_6346)
																				{
																					long BgL_iz00_2536;

																					BgL_iz00_2536 = 0L;
																				BgL_zc3z04anonymousza32311ze3z87_2537:
																					{	/* Cnst/alloc.scm 1177 */
																						bool_t BgL__ortest_1275z00_2538;

																						BgL__ortest_1275z00_2538 =
																							(BgL_iz00_2536 ==
																							VECTOR_LENGTH(BgL_obj1z00_76));
																						if (BgL__ortest_1275z00_2538)
																							{	/* Cnst/alloc.scm 1177 */
																								return BgL__ortest_1275z00_2538;
																							}
																						else
																							{	/* Cnst/alloc.scm 1178 */
																								bool_t BgL_test2590z00_6355;

																								{	/* Cnst/alloc.scm 1178 */
																									obj_t BgL_arg2313z00_2541;
																									obj_t BgL_arg2314z00_2542;

																									BgL_arg2313z00_2541 =
																										VECTOR_REF(
																										((obj_t) BgL_obj1z00_76),
																										BgL_iz00_2536);
																									BgL_arg2314z00_2542 =
																										VECTOR_REF(((obj_t)
																											BgL_obj2z00_77),
																										BgL_iz00_2536);
																									BgL_test2590z00_6355 =
																										BGl_cnstzd2equalzf3z21zzcnst_allocz00
																										(BgL_arg2313z00_2541,
																										BgL_arg2314z00_2542);
																								}
																								if (BgL_test2590z00_6355)
																									{
																										long BgL_iz00_6361;

																										BgL_iz00_6361 =
																											(BgL_iz00_2536 + 1L);
																										BgL_iz00_2536 =
																											BgL_iz00_6361;
																										goto
																											BgL_zc3z04anonymousza32311ze3z87_2537;
																									}
																								else
																									{	/* Cnst/alloc.scm 1178 */
																										return ((bool_t) 0);
																									}
																							}
																					}
																				}
																			else
																				{	/* Cnst/alloc.scm 1175 */
																					return ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Cnst/alloc.scm 1174 */
																			return ((bool_t) 0);
																		}
																}
															else
																{	/* Cnst/alloc.scm 1173 */
																	return ((bool_t) 0);
																}
														}
													else
														{	/* Cnst/alloc.scm 1171 */
															if (BGL_HVECTORP(BgL_obj1z00_76))
																{	/* Cnst/alloc.scm 1181 */
																	BGL_TAIL return
																		BGl_equalzf3zf3zz__r4_equivalence_6_2z00
																		(BgL_obj1z00_76, BgL_obj2z00_77);
																}
															else
																{	/* Cnst/alloc.scm 1181 */
																	if (STRUCTP(BgL_obj1z00_76))
																		{	/* Cnst/alloc.scm 1184 */
																			int BgL_lobj1z00_2549;

																			BgL_lobj1z00_2549 =
																				STRUCT_LENGTH(BgL_obj1z00_76);
																			if (STRUCTP(BgL_obj2z00_77))
																				{	/* Cnst/alloc.scm 1185 */
																					if (
																						((long) (STRUCT_LENGTH
																								(BgL_obj2z00_77)) ==
																							(long) (BgL_lobj1z00_2549)))
																						{
																							long BgL_iz00_2553;

																							BgL_iz00_2553 = 0L;
																						BgL_zc3z04anonymousza32320ze3z87_2554:
																							{	/* Cnst/alloc.scm 1188 */
																								bool_t BgL__ortest_1279z00_2555;

																								BgL__ortest_1279z00_2555 =
																									(BgL_iz00_2553 ==
																									(long) (BgL_lobj1z00_2549));
																								if (BgL__ortest_1279z00_2555)
																									{	/* Cnst/alloc.scm 1188 */
																										return
																											BgL__ortest_1279z00_2555;
																									}
																								else
																									{	/* Cnst/alloc.scm 1189 */
																										bool_t BgL_test2596z00_6379;

																										{	/* Cnst/alloc.scm 1189 */
																											obj_t BgL_arg2323z00_2558;
																											obj_t BgL_arg2324z00_2559;

																											BgL_arg2323z00_2558 =
																												STRUCT_REF(
																												((obj_t)
																													BgL_obj1z00_76),
																												(int) (BgL_iz00_2553));
																											BgL_arg2324z00_2559 =
																												STRUCT_REF(((obj_t)
																													BgL_obj2z00_77),
																												(int) (BgL_iz00_2553));
																											BgL_test2596z00_6379 =
																												BGl_cnstzd2equalzf3z21zzcnst_allocz00
																												(BgL_arg2323z00_2558,
																												BgL_arg2324z00_2559);
																										}
																										if (BgL_test2596z00_6379)
																											{
																												long BgL_iz00_6387;

																												BgL_iz00_6387 =
																													(BgL_iz00_2553 + 1L);
																												BgL_iz00_2553 =
																													BgL_iz00_6387;
																												goto
																													BgL_zc3z04anonymousza32320ze3z87_2554;
																											}
																										else
																											{	/* Cnst/alloc.scm 1189 */
																												return ((bool_t) 0);
																											}
																									}
																							}
																						}
																					else
																						{	/* Cnst/alloc.scm 1186 */
																							return ((bool_t) 0);
																						}
																				}
																			else
																				{	/* Cnst/alloc.scm 1185 */
																					return ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Cnst/alloc.scm 1183 */
																			if (CELLP(BgL_obj1z00_76))
																				{	/* Cnst/alloc.scm 1191 */
																					if (CELLP(BgL_obj2z00_77))
																						{
																							obj_t BgL_obj2z00_6395;
																							obj_t BgL_obj1z00_6393;

																							BgL_obj1z00_6393 =
																								CELL_REF(BgL_obj1z00_76);
																							BgL_obj2z00_6395 =
																								CELL_REF(BgL_obj2z00_77);
																							BgL_obj2z00_77 = BgL_obj2z00_6395;
																							BgL_obj1z00_76 = BgL_obj1z00_6393;
																							goto
																								BGl_cnstzd2equalzf3z21zzcnst_allocz00;
																						}
																					else
																						{	/* Cnst/alloc.scm 1192 */
																							return ((bool_t) 0);
																						}
																				}
																			else
																				{	/* Cnst/alloc.scm 1191 */
																					if (UCS2_STRINGP(BgL_obj1z00_76))
																						{	/* Cnst/alloc.scm 1193 */
																							if (UCS2_STRINGP(BgL_obj2z00_77))
																								{	/* Cnst/alloc.scm 1194 */
																									return
																										ucs2_strcmp(BgL_obj1z00_76,
																										BgL_obj2z00_77);
																								}
																							else
																								{	/* Cnst/alloc.scm 1194 */
																									return ((bool_t) 0);
																								}
																						}
																					else
																						{	/* Cnst/alloc.scm 1193 */
																							if (UCS2P(BgL_obj1z00_76))
																								{	/* Cnst/alloc.scm 1195 */
																									if (UCS2P(BgL_obj2z00_77))
																										{	/* Cnst/alloc.scm 1196 */
																											return
																												(CUCS2(BgL_obj1z00_76)
																												==
																												CUCS2(BgL_obj2z00_77));
																										}
																									else
																										{	/* Cnst/alloc.scm 1196 */
																											return ((bool_t) 0);
																										}
																								}
																							else
																								{	/* Cnst/alloc.scm 1195 */
																									if (BGL_DATEP(BgL_obj1z00_76))
																										{	/* Cnst/alloc.scm 1197 */
																											BGL_TAIL return
																												BGl_equalzf3zf3zz__r4_equivalence_6_2z00
																												(BgL_obj1z00_76,
																												BgL_obj2z00_77);
																										}
																									else
																										{	/* Cnst/alloc.scm 1197 */
																											return ((bool_t) 0);
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcnst_allocz00(void)
	{
		{	/* Cnst/alloc.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcnst_allocz00(void)
	{
		{	/* Cnst/alloc.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcnst_allocz00(void)
	{
		{	/* Cnst/alloc.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcnst_allocz00(void)
	{
		{	/* Cnst/alloc.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zztvector_tvectorz00(501518119L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zztvector_cnstz00(263487728L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(44601789L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zzcoerce_coercez00(361167184L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zzcnst_cachez00(156818605L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zzcnst_nodez00(89013043L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
			return
				BGl_modulezd2initializa7ationz75zzmodule_libraryz00(292140514L,
				BSTRING_TO_STRING(BGl_string2433z00zzcnst_allocz00));
		}

	}

#ifdef __cplusplus
}
#endif
