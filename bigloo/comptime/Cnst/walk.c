/*===========================================================================*/
/*   (Cnst/walk.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cnst/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CNST_WALK_TYPE_DEFINITIONS
#define BGL_CNST_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;


#endif													// BGL_CNST_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2unsafezd2arityza2zd2zzengine_paramz00;
	static obj_t BGl_requirezd2initializa7ationz75zzcnst_walkz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_cnstzd2walkz12zc0zzcnst_walkz00(obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static obj_t BGl_za2oldzd2unsafezd2versionza2z00zzcnst_walkz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzcnst_walkz00(void);
	static obj_t BGl_z62cnstzd2walkz12za2zzcnst_walkz00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcnst_walkz00(void);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	static obj_t BGl_objectzd2initzd2zzcnst_walkz00(void);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_za2oldzd2unsafezd2structza2z00zzcnst_walkz00 = BUNSPEC;
	extern obj_t BGl_za2initzd2modeza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzcnst_walkz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcnst_walkz00(void);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static obj_t BGl_za2oldzd2unsafezd2arityza2z00zzcnst_walkz00 = BUNSPEC;
	extern obj_t BGl_removezd2varzd2zzast_removez00(obj_t, obj_t);
	extern obj_t BGl_za2unsafezd2structza2zd2zzengine_paramz00;
	extern obj_t BGl_za2unsafezd2rangeza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcnst_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_initializa7eza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_allocz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62safez12z70zzcnst_walkz00(obj_t);
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	static obj_t BGl_z62unsafez12z70zzcnst_walkz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzcnst_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcnst_walkz00(void);
	extern obj_t BGl_za2unsafezd2typeza2zd2zzengine_paramz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzcnst_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzcnst_walkz00(void);
	static obj_t BGl_za2oldzd2unsafezd2typeza2z00zzcnst_walkz00 = BUNSPEC;
	extern obj_t BGl_initializa7ezd2astz75zzcnst_initializa7eza7(void);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	static obj_t BGl_za2oldzd2unsafezd2rangeza2z00zzcnst_walkz00 = BUNSPEC;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	extern obj_t BGl_za2unsafezd2versionza2zd2zzengine_paramz00;
	extern BgL_nodez00_bglt BGl_cnstz12z12zzcnst_nodez00(BgL_nodez00_bglt);
	static obj_t __cnst[4];


	extern obj_t BGl_stopzd2cnstzd2allocz12zd2envzc0zzcnst_allocz00;
	   
		 
		DEFINE_STRING(BGl_string1574z00zzcnst_walkz00,
		BgL_bgl_string1574za700za7za7c1586za7, "Cnst", 4);
	      DEFINE_STRING(BGl_string1575z00zzcnst_walkz00,
		BgL_bgl_string1575za700za7za7c1587za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1576z00zzcnst_walkz00,
		BgL_bgl_string1576za700za7za7c1588za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1577z00zzcnst_walkz00,
		BgL_bgl_string1577za700za7za7c1589za7, "      [", 7);
	      DEFINE_STRING(BGl_string1578z00zzcnst_walkz00,
		BgL_bgl_string1578za700za7za7c1590za7, " error", 6);
	      DEFINE_STRING(BGl_string1579z00zzcnst_walkz00,
		BgL_bgl_string1579za700za7za7c1591za7, "s", 1);
	      DEFINE_STRING(BGl_string1580z00zzcnst_walkz00,
		BgL_bgl_string1580za700za7za7c1592za7, "", 0);
	      DEFINE_STRING(BGl_string1581z00zzcnst_walkz00,
		BgL_bgl_string1581za700za7za7c1593za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1582z00zzcnst_walkz00,
		BgL_bgl_string1582za700za7za7c1594za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1583z00zzcnst_walkz00,
		BgL_bgl_string1583za700za7za7c1595za7, "cnst_walk", 9);
	      DEFINE_STRING(BGl_string1584z00zzcnst_walkz00,
		BgL_bgl_string1584za700za7za7c1596za7,
		"(stop-cnst-alloc! safe!) cnst pass-started (start-cnst-cache! start-cnst-alloc! unsafe!) ",
		89);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_safez12zd2envzc0zzcnst_walkz00,
		BgL_bgl_za762safeza712za770za7za7c1597za7, BGl_z62safez12z70zzcnst_walkz00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_unsafez12zd2envzc0zzcnst_walkz00,
		BgL_bgl_za762unsafeza712za770za71598z00, BGl_z62unsafez12z70zzcnst_walkz00,
		0L, BUNSPEC, 0);
	extern obj_t BGl_startzd2cnstzd2cachez12zd2envzc0zzcnst_cachez00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cnstzd2walkz12zd2envz12zzcnst_walkz00,
		BgL_bgl_za762cnstza7d2walkza711599za7,
		BGl_z62cnstzd2walkz12za2zzcnst_walkz00, 0L, BUNSPEC, 1);
	extern obj_t BGl_startzd2cnstzd2allocz12zd2envzc0zzcnst_allocz00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcnst_walkz00));
		   
			 ADD_ROOT((void *) (&BGl_za2oldzd2unsafezd2versionza2z00zzcnst_walkz00));
		     ADD_ROOT((void *) (&BGl_za2oldzd2unsafezd2structza2z00zzcnst_walkz00));
		     ADD_ROOT((void *) (&BGl_za2oldzd2unsafezd2arityza2z00zzcnst_walkz00));
		     ADD_ROOT((void *) (&BGl_za2oldzd2unsafezd2typeza2z00zzcnst_walkz00));
		     ADD_ROOT((void *) (&BGl_za2oldzd2unsafezd2rangeza2z00zzcnst_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcnst_walkz00(long
		BgL_checksumz00_1619, char *BgL_fromz00_1620)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcnst_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzcnst_walkz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcnst_walkz00();
					BGl_libraryzd2moduleszd2initz00zzcnst_walkz00();
					BGl_cnstzd2initzd2zzcnst_walkz00();
					BGl_importedzd2moduleszd2initz00zzcnst_walkz00();
					return BGl_toplevelzd2initzd2zzcnst_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcnst_walkz00(void)
	{
		{	/* Cnst/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "cnst_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cnst_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "cnst_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cnst_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cnst_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cnst_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cnst_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cnst_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcnst_walkz00(void)
	{
		{	/* Cnst/walk.scm 15 */
			{	/* Cnst/walk.scm 15 */
				obj_t BgL_cportz00_1608;

				{	/* Cnst/walk.scm 15 */
					obj_t BgL_stringz00_1615;

					BgL_stringz00_1615 = BGl_string1584z00zzcnst_walkz00;
					{	/* Cnst/walk.scm 15 */
						obj_t BgL_startz00_1616;

						BgL_startz00_1616 = BINT(0L);
						{	/* Cnst/walk.scm 15 */
							obj_t BgL_endz00_1617;

							BgL_endz00_1617 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1615)));
							{	/* Cnst/walk.scm 15 */

								BgL_cportz00_1608 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1615, BgL_startz00_1616, BgL_endz00_1617);
				}}}}
				{
					long BgL_iz00_1609;

					BgL_iz00_1609 = 3L;
				BgL_loopz00_1610:
					if ((BgL_iz00_1609 == -1L))
						{	/* Cnst/walk.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cnst/walk.scm 15 */
							{	/* Cnst/walk.scm 15 */
								obj_t BgL_arg1585z00_1611;

								{	/* Cnst/walk.scm 15 */

									{	/* Cnst/walk.scm 15 */
										obj_t BgL_locationz00_1613;

										BgL_locationz00_1613 = BBOOL(((bool_t) 0));
										{	/* Cnst/walk.scm 15 */

											BgL_arg1585z00_1611 =
												BGl_readz00zz__readerz00(BgL_cportz00_1608,
												BgL_locationz00_1613);
										}
									}
								}
								{	/* Cnst/walk.scm 15 */
									int BgL_tmpz00_1646;

									BgL_tmpz00_1646 = (int) (BgL_iz00_1609);
									CNST_TABLE_SET(BgL_tmpz00_1646, BgL_arg1585z00_1611);
							}}
							{	/* Cnst/walk.scm 15 */
								int BgL_auxz00_1614;

								BgL_auxz00_1614 = (int) ((BgL_iz00_1609 - 1L));
								{
									long BgL_iz00_1651;

									BgL_iz00_1651 = (long) (BgL_auxz00_1614);
									BgL_iz00_1609 = BgL_iz00_1651;
									goto BgL_loopz00_1610;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcnst_walkz00(void)
	{
		{	/* Cnst/walk.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcnst_walkz00(void)
	{
		{	/* Cnst/walk.scm 15 */
			BGl_za2oldzd2unsafezd2typeza2z00zzcnst_walkz00 = BFALSE;
			BGl_za2oldzd2unsafezd2arityza2z00zzcnst_walkz00 = BFALSE;
			BGl_za2oldzd2unsafezd2rangeza2z00zzcnst_walkz00 = BFALSE;
			BGl_za2oldzd2unsafezd2structza2z00zzcnst_walkz00 = BFALSE;
			return (BGl_za2oldzd2unsafezd2versionza2z00zzcnst_walkz00 =
				BFALSE, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzcnst_walkz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1345;

				BgL_headz00_1345 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1346;
					obj_t BgL_tailz00_1347;

					BgL_prevz00_1346 = BgL_headz00_1345;
					BgL_tailz00_1347 = BgL_l1z00_1;
				BgL_loopz00_1348:
					if (PAIRP(BgL_tailz00_1347))
						{
							obj_t BgL_newzd2prevzd2_1350;

							BgL_newzd2prevzd2_1350 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1347), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1346, BgL_newzd2prevzd2_1350);
							{
								obj_t BgL_tailz00_1661;
								obj_t BgL_prevz00_1660;

								BgL_prevz00_1660 = BgL_newzd2prevzd2_1350;
								BgL_tailz00_1661 = CDR(BgL_tailz00_1347);
								BgL_tailz00_1347 = BgL_tailz00_1661;
								BgL_prevz00_1346 = BgL_prevz00_1660;
								goto BgL_loopz00_1348;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1345);
				}
			}
		}

	}



/* cnst-walk! */
	BGL_EXPORTED_DEF obj_t BGl_cnstzd2walkz12zc0zzcnst_walkz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Cnst/walk.scm 34 */
			{	/* Cnst/walk.scm 35 */
				obj_t BgL_list1239z00_1353;

				{	/* Cnst/walk.scm 35 */
					obj_t BgL_arg1242z00_1354;

					{	/* Cnst/walk.scm 35 */
						obj_t BgL_arg1244z00_1355;

						BgL_arg1244z00_1355 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1242z00_1354 =
							MAKE_YOUNG_PAIR(BGl_string1574z00zzcnst_walkz00,
							BgL_arg1244z00_1355);
					}
					BgL_list1239z00_1353 =
						MAKE_YOUNG_PAIR(BGl_string1575z00zzcnst_walkz00,
						BgL_arg1242z00_1354);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1239z00_1353);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1574z00zzcnst_walkz00;
			{	/* Cnst/walk.scm 35 */
				obj_t BgL_g1110z00_1356;
				obj_t BgL_g1111z00_1357;

				{	/* Cnst/walk.scm 35 */
					obj_t BgL_list1270z00_1370;

					{	/* Cnst/walk.scm 35 */
						obj_t BgL_arg1272z00_1371;

						{	/* Cnst/walk.scm 35 */
							obj_t BgL_arg1284z00_1372;

							BgL_arg1284z00_1372 =
								MAKE_YOUNG_PAIR(BGl_unsafez12zd2envzc0zzcnst_walkz00, BNIL);
							BgL_arg1272z00_1371 =
								MAKE_YOUNG_PAIR
								(BGl_startzd2cnstzd2allocz12zd2envzc0zzcnst_allocz00,
								BgL_arg1284z00_1372);
						}
						BgL_list1270z00_1370 =
							MAKE_YOUNG_PAIR
							(BGl_startzd2cnstzd2cachez12zd2envzc0zzcnst_cachez00,
							BgL_arg1272z00_1371);
					}
					BgL_g1110z00_1356 = BgL_list1270z00_1370;
				}
				BgL_g1111z00_1357 = CNST_TABLE_REF(0);
				{
					obj_t BgL_hooksz00_1359;
					obj_t BgL_hnamesz00_1360;

					BgL_hooksz00_1359 = BgL_g1110z00_1356;
					BgL_hnamesz00_1360 = BgL_g1111z00_1357;
				BgL_zc3z04anonymousza31245ze3z87_1361:
					if (NULLP(BgL_hooksz00_1359))
						{	/* Cnst/walk.scm 35 */
							CNST_TABLE_REF(1);
						}
					else
						{	/* Cnst/walk.scm 35 */
							bool_t BgL_test1604z00_1678;

							{	/* Cnst/walk.scm 35 */
								obj_t BgL_fun1269z00_1368;

								BgL_fun1269z00_1368 = CAR(((obj_t) BgL_hooksz00_1359));
								BgL_test1604z00_1678 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1269z00_1368));
							}
							if (BgL_test1604z00_1678)
								{	/* Cnst/walk.scm 35 */
									obj_t BgL_arg1249z00_1365;
									obj_t BgL_arg1252z00_1366;

									BgL_arg1249z00_1365 = CDR(((obj_t) BgL_hooksz00_1359));
									BgL_arg1252z00_1366 = CDR(((obj_t) BgL_hnamesz00_1360));
									{
										obj_t BgL_hnamesz00_1690;
										obj_t BgL_hooksz00_1689;

										BgL_hooksz00_1689 = BgL_arg1249z00_1365;
										BgL_hnamesz00_1690 = BgL_arg1252z00_1366;
										BgL_hnamesz00_1360 = BgL_hnamesz00_1690;
										BgL_hooksz00_1359 = BgL_hooksz00_1689;
										goto BgL_zc3z04anonymousza31245ze3z87_1361;
									}
								}
							else
								{	/* Cnst/walk.scm 35 */
									obj_t BgL_arg1268z00_1367;

									BgL_arg1268z00_1367 = CAR(((obj_t) BgL_hnamesz00_1360));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1574z00zzcnst_walkz00,
										BGl_string1576z00zzcnst_walkz00, BgL_arg1268z00_1367);
								}
						}
				}
			}
			{	/* Cnst/walk.scm 36 */
				obj_t BgL_list1285z00_1373;

				{	/* Cnst/walk.scm 36 */
					obj_t BgL_arg1304z00_1374;

					{	/* Cnst/walk.scm 36 */
						obj_t BgL_arg1305z00_1375;

						{	/* Cnst/walk.scm 36 */
							obj_t BgL_arg1306z00_1376;

							BgL_arg1306z00_1376 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
							BgL_arg1305z00_1375 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ']')),
								BgL_arg1306z00_1376);
						}
						BgL_arg1304z00_1374 =
							MAKE_YOUNG_PAIR(BGl_za2initzd2modeza2zd2zzengine_paramz00,
							BgL_arg1305z00_1375);
					}
					BgL_list1285z00_1373 =
						MAKE_YOUNG_PAIR(BGl_string1577z00zzcnst_walkz00,
						BgL_arg1304z00_1374);
				}
				BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1285z00_1373);
			}
			{
				obj_t BgL_l1231z00_1378;

				BgL_l1231z00_1378 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31307ze3z87_1379:
				if (PAIRP(BgL_l1231z00_1378))
					{	/* Cnst/walk.scm 37 */
						{	/* Cnst/walk.scm 38 */
							obj_t BgL_globalz00_1381;

							BgL_globalz00_1381 = CAR(BgL_l1231z00_1378);
							{	/* Cnst/walk.scm 39 */
								obj_t BgL_arg1310z00_1382;

								BgL_arg1310z00_1382 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_1381))))->
									BgL_idz00);
								BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1310z00_1382);
							}
							{	/* Cnst/walk.scm 40 */
								BgL_valuez00_bglt BgL_funz00_1383;

								BgL_funz00_1383 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_1381))))->
									BgL_valuez00);
								{	/* Cnst/walk.scm 40 */
									BgL_nodez00_bglt BgL_newzd2bodyzd2_1384;

									{	/* Cnst/walk.scm 41 */
										obj_t BgL_arg1311z00_1385;

										BgL_arg1311z00_1385 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_1383)))->BgL_bodyz00);
										BgL_newzd2bodyzd2_1384 =
											BGl_cnstz12z12zzcnst_nodez00(
											((BgL_nodez00_bglt) BgL_arg1311z00_1385));
									}
									{	/* Cnst/walk.scm 41 */

										((((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_1383)))->
												BgL_bodyz00) =
											((obj_t) ((obj_t) BgL_newzd2bodyzd2_1384)), BUNSPEC);
									}
								}
							}
							BGl_leavezd2functionzd2zztools_errorz00();
						}
						{
							obj_t BgL_l1231z00_1720;

							BgL_l1231z00_1720 = CDR(BgL_l1231z00_1378);
							BgL_l1231z00_1378 = BgL_l1231z00_1720;
							goto BgL_zc3z04anonymousza31307ze3z87_1379;
						}
					}
				else
					{	/* Cnst/walk.scm 37 */
						((bool_t) 1);
					}
			}
			{	/* Cnst/walk.scm 45 */
				obj_t BgL_astz00_1388;

				BgL_astz00_1388 =
					BGl_appendzd221011zd2zzcnst_walkz00
					(BGl_initializa7ezd2astz75zzcnst_initializa7eza7(), BgL_globalsz00_3);
				{	/* Cnst/walk.scm 46 */
					obj_t BgL_valuez00_1389;

					BgL_valuez00_1389 =
						BGl_removezd2varzd2zzast_removez00(CNST_TABLE_REF(2),
						BgL_astz00_1388);
					if (((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
							> 0L))
						{	/* Cnst/walk.scm 46 */
							{	/* Cnst/walk.scm 46 */
								obj_t BgL_port1233z00_1391;

								{	/* Cnst/walk.scm 46 */
									obj_t BgL_tmpz00_1729;

									BgL_tmpz00_1729 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_port1233z00_1391 =
										BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_1729);
								}
								bgl_display_obj
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
									BgL_port1233z00_1391);
								bgl_display_string(BGl_string1578z00zzcnst_walkz00,
									BgL_port1233z00_1391);
								{	/* Cnst/walk.scm 46 */
									obj_t BgL_arg1314z00_1392;

									{	/* Cnst/walk.scm 46 */
										bool_t BgL_test1607z00_1734;

										if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Cnst/walk.scm 46 */
												if (INTEGERP
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
													{	/* Cnst/walk.scm 46 */
														BgL_test1607z00_1734 =
															(
															(long)
															CINT
															(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
															> 1L);
													}
												else
													{	/* Cnst/walk.scm 46 */
														BgL_test1607z00_1734 =
															BGl_2ze3ze3zz__r4_numbers_6_5z00
															(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
															BINT(1L));
													}
											}
										else
											{	/* Cnst/walk.scm 46 */
												BgL_test1607z00_1734 = ((bool_t) 0);
											}
										if (BgL_test1607z00_1734)
											{	/* Cnst/walk.scm 46 */
												BgL_arg1314z00_1392 = BGl_string1579z00zzcnst_walkz00;
											}
										else
											{	/* Cnst/walk.scm 46 */
												BgL_arg1314z00_1392 = BGl_string1580z00zzcnst_walkz00;
											}
									}
									bgl_display_obj(BgL_arg1314z00_1392, BgL_port1233z00_1391);
								}
								bgl_display_string(BGl_string1581z00zzcnst_walkz00,
									BgL_port1233z00_1391);
								bgl_display_char(((unsigned char) 10), BgL_port1233z00_1391);
							}
							{	/* Cnst/walk.scm 46 */
								obj_t BgL_list1317z00_1396;

								BgL_list1317z00_1396 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
								BGL_TAIL return BGl_exitz00zz__errorz00(BgL_list1317z00_1396);
							}
						}
					else
						{	/* Cnst/walk.scm 46 */
							obj_t BgL_g1112z00_1397;
							obj_t BgL_g1113z00_1398;

							{	/* Cnst/walk.scm 46 */
								obj_t BgL_list1327z00_1411;

								{	/* Cnst/walk.scm 46 */
									obj_t BgL_arg1328z00_1412;

									BgL_arg1328z00_1412 =
										MAKE_YOUNG_PAIR(BGl_safez12zd2envzc0zzcnst_walkz00, BNIL);
									BgL_list1327z00_1411 =
										MAKE_YOUNG_PAIR
										(BGl_stopzd2cnstzd2allocz12zd2envzc0zzcnst_allocz00,
										BgL_arg1328z00_1412);
								}
								BgL_g1112z00_1397 = BgL_list1327z00_1411;
							}
							BgL_g1113z00_1398 = CNST_TABLE_REF(3);
							{
								obj_t BgL_hooksz00_1400;
								obj_t BgL_hnamesz00_1401;

								BgL_hooksz00_1400 = BgL_g1112z00_1397;
								BgL_hnamesz00_1401 = BgL_g1113z00_1398;
							BgL_zc3z04anonymousza31318ze3z87_1402:
								if (NULLP(BgL_hooksz00_1400))
									{	/* Cnst/walk.scm 46 */
										return BgL_valuez00_1389;
									}
								else
									{	/* Cnst/walk.scm 46 */
										bool_t BgL_test1611z00_1754;

										{	/* Cnst/walk.scm 46 */
											obj_t BgL_fun1326z00_1409;

											BgL_fun1326z00_1409 = CAR(((obj_t) BgL_hooksz00_1400));
											BgL_test1611z00_1754 =
												CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1326z00_1409));
										}
										if (BgL_test1611z00_1754)
											{	/* Cnst/walk.scm 46 */
												obj_t BgL_arg1322z00_1406;
												obj_t BgL_arg1323z00_1407;

												BgL_arg1322z00_1406 = CDR(((obj_t) BgL_hooksz00_1400));
												BgL_arg1323z00_1407 = CDR(((obj_t) BgL_hnamesz00_1401));
												{
													obj_t BgL_hnamesz00_1766;
													obj_t BgL_hooksz00_1765;

													BgL_hooksz00_1765 = BgL_arg1322z00_1406;
													BgL_hnamesz00_1766 = BgL_arg1323z00_1407;
													BgL_hnamesz00_1401 = BgL_hnamesz00_1766;
													BgL_hooksz00_1400 = BgL_hooksz00_1765;
													goto BgL_zc3z04anonymousza31318ze3z87_1402;
												}
											}
										else
											{	/* Cnst/walk.scm 46 */
												obj_t BgL_arg1325z00_1408;

												BgL_arg1325z00_1408 = CAR(((obj_t) BgL_hnamesz00_1401));
												return
													BGl_internalzd2errorzd2zztools_errorz00
													(BGl_za2currentzd2passza2zd2zzengine_passz00,
													BGl_string1582z00zzcnst_walkz00, BgL_arg1325z00_1408);
											}
									}
							}
						}
				}
			}
		}

	}



/* &cnst-walk! */
	obj_t BGl_z62cnstzd2walkz12za2zzcnst_walkz00(obj_t BgL_envz00_1601,
		obj_t BgL_globalsz00_1602)
	{
		{	/* Cnst/walk.scm 34 */
			return BGl_cnstzd2walkz12zc0zzcnst_walkz00(BgL_globalsz00_1602);
		}

	}



/* &unsafe! */
	obj_t BGl_z62unsafez12z70zzcnst_walkz00(obj_t BgL_envz00_1603)
	{
		{	/* Cnst/walk.scm 53 */
			BGl_za2oldzd2unsafezd2typeza2z00zzcnst_walkz00 =
				BGl_za2unsafezd2typeza2zd2zzengine_paramz00;
			BGl_za2unsafezd2typeza2zd2zzengine_paramz00 = BTRUE;
			BGl_za2oldzd2unsafezd2arityza2z00zzcnst_walkz00 =
				BGl_za2unsafezd2arityza2zd2zzengine_paramz00;
			BGl_za2unsafezd2arityza2zd2zzengine_paramz00 = BTRUE;
			BGl_za2oldzd2unsafezd2rangeza2z00zzcnst_walkz00 =
				BGl_za2unsafezd2rangeza2zd2zzengine_paramz00;
			BGl_za2unsafezd2rangeza2zd2zzengine_paramz00 = BTRUE;
			BGl_za2oldzd2unsafezd2structza2z00zzcnst_walkz00 =
				BGl_za2unsafezd2structza2zd2zzengine_paramz00;
			BGl_za2unsafezd2structza2zd2zzengine_paramz00 = BTRUE;
			BGl_za2oldzd2unsafezd2versionza2z00zzcnst_walkz00 =
				BGl_za2unsafezd2versionza2zd2zzengine_paramz00;
			return (BGl_za2unsafezd2versionza2zd2zzengine_paramz00 = BTRUE, BUNSPEC);
		}

	}



/* &safe! */
	obj_t BGl_z62safez12z70zzcnst_walkz00(obj_t BgL_envz00_1606)
	{
		{	/* Cnst/walk.scm 68 */
			BGl_za2unsafezd2typeza2zd2zzengine_paramz00 =
				BGl_za2oldzd2unsafezd2typeza2z00zzcnst_walkz00;
			BGl_za2unsafezd2arityza2zd2zzengine_paramz00 =
				BGl_za2oldzd2unsafezd2arityza2z00zzcnst_walkz00;
			BGl_za2unsafezd2rangeza2zd2zzengine_paramz00 =
				BGl_za2oldzd2unsafezd2rangeza2z00zzcnst_walkz00;
			BGl_za2unsafezd2structza2zd2zzengine_paramz00 =
				BGl_za2oldzd2unsafezd2structza2z00zzcnst_walkz00;
			return (BGl_za2unsafezd2versionza2zd2zzengine_paramz00 =
				BGl_za2oldzd2unsafezd2versionza2z00zzcnst_walkz00, BUNSPEC);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcnst_walkz00(void)
	{
		{	/* Cnst/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcnst_walkz00(void)
	{
		{	/* Cnst/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcnst_walkz00(void)
	{
		{	/* Cnst/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcnst_walkz00(void)
	{
		{	/* Cnst/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1583z00zzcnst_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1583z00zzcnst_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1583z00zzcnst_walkz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1583z00zzcnst_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1583z00zzcnst_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1583z00zzcnst_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1583z00zzcnst_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1583z00zzcnst_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1583z00zzcnst_walkz00));
			BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string1583z00zzcnst_walkz00));
			BGl_modulezd2initializa7ationz75zzcnst_allocz00(192699986L,
				BSTRING_TO_STRING(BGl_string1583z00zzcnst_walkz00));
			BGl_modulezd2initializa7ationz75zzcnst_cachez00(156818605L,
				BSTRING_TO_STRING(BGl_string1583z00zzcnst_walkz00));
			BGl_modulezd2initializa7ationz75zzcnst_nodez00(89013043L,
				BSTRING_TO_STRING(BGl_string1583z00zzcnst_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzcnst_initializa7eza7(259844936L,
				BSTRING_TO_STRING(BGl_string1583z00zzcnst_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
