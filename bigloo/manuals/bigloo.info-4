This is bigloo.info, produced by makeinfo version 7.1 from bigloo.texi.

INFO-DIR-SECTION Programming Language
START-INFO-DIR-ENTRY
* bigloo 4.6a: (bigloo).  The Bigloo Scheme compiler
END-INFO-DIR-ENTRY

4.6a

  This file documents Bigloo, a Functional Programming Language.

  Copyright (C) 1992-2024 Manuel Serrano

     This program is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public
     License as published by the Free Software Foundation; either
     version 2 of the License, or (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public
     License along with this program; if not, write to the Free
     Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
     MA 02111-1307, USA.


File: bigloo.info,  Node: Library Index,  Next: Bibliography,  Prev: Global Index,  Up: Top

37 Library Index
****************


File: bigloo.info,  Node: Bibliography,  Prev: Library Index,  Up: Top

Bibliography
************

   • [Bobrow et al.  88] D. Bobrow, L. DeMichiel, R. Gabriel, S. Keene,
     G. Kiczales and D. Moon.  ‘Common lisp object system
     specification.’ In _special issue_, number 23 in SIGPLAN Notices,
     September 1988.

   • [BoehmWeiser88] H.J. Boehm and M. Weiser.  ‘Garbage collection in
     an unco-operative environment.’ _Software--Practice and
     Experience_, 18(9):807-820, Sept-ember 1988.

   • [Boehm91] H.J. Boehm.  ‘Space efficient conservative garbage
     collection.’ In _Conference on Programming Language Design and
     Implementation_, number 28, 6 in SIGPLAN Notices, pages 197-206,
     1991.

   • [Caml-light] P. Weis and X. Leroy.  ‘Le langage CAML’.
     _InterEditions, Paris, 1993._

   • [Dsssl96]  ISO/IEC. ‘Information technology, Processing Languages,
     Document Style Semantics and Specification Languages (dsssl).’
     _Technical Report 10179 :1996(E), ISO, 1996._

   • [Dybvig et al.  86] K. Dybvig, D. Friedman, and C. Haynes.
     ‘Expansion-passing style: Beyond conventional macros.’ In
     _Conference on Lisp and Functional Programming_, pages 143-150,
     1986.

   • [Gallesio95] E. Gallesio.  ‘STk Reference Manual.’ Technical Report
     RT 95-31a, I3S-CNRS/University of Nice-Sophia Antipolis, July 1995.

   • [IsoC] ISO/IEC. ‘9899 programming language --- C.’ _Technical
     Report DIS 9899, ISO, July 1990._

   • [Les75] M.E. Lesk.  ‘Lex --- a lexical analyzer generator.’
     Computing Science Technical Report 39~39, AT&T Bell Laboratories,
     Murray Hill, N.J., 1975.

   • [Queinnec93] C. Queinnec.  ‘Designing MEROON v3.’ In _Workshop on
     Object-Oriented Programming in Lisp_, 1993.

   • [QueinnecGeffroy92] C. Queinnec and J-M. Geffroy.  ‘Partial
     evaluation applied to symbolic pattern matching with intelligent
     backtrack.’ In M. Billaud, P. Casteran, MM. Corsini, K. Musumbu,
     and A. Rauzy: Editors, _Workshop on Static Analysis_, number 81-82
     in bigre, pages 109-117, Bordeaux (France), September 1992.

   • [R5RS] R Kelsey, W. Clinger and J. Rees: Editors.  ‘The Revised(5)
     Report on the Algorithmic Language Scheme’.

   • [Stallman95] R. Stallman.  ‘Using and Porting GNU CC.’ for version
     2.7.2 ISBN 1-882114-66-3, Free Software Foundation, Inc., 59 Temple
     Place - Suite 330, Boston, MA 02111-1307, USA, November 1995.

   • [SerranoWeis94] M. Serrano and P. Weis.  ‘1+1=1: an optimizing Caml
     compiler.’ In _ACM SIGPLAN_ Workshop on ML and its Applications,
     pages 101-111, Orlando (Florida, USA), June 1994.  ACM SIGPLAN,
     INRIA RR 2265.

   • [Steele90] G. Steele.  ‘COMMON LISP (The language)’.  _Digital
     Press (DEC)_, Burlington MA (USA), 2nd edition, 1990.

