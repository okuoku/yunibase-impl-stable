This is bigloo.info, produced by makeinfo version 7.1 from bigloo.texi.

INFO-DIR-SECTION Programming Language
START-INFO-DIR-ENTRY
* bigloo 4.6a: (bigloo).  The Bigloo Scheme compiler
END-INFO-DIR-ENTRY

4.6a

  This file documents Bigloo, a Functional Programming Language.

  Copyright (C) 1992-2024 Manuel Serrano

     This program is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public
     License as published by the Free Software Foundation; either
     version 2 of the License, or (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public
     License along with this program; if not, write to the Free
     Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
     MA 02111-1307, USA.


Indirect:
bigloo.info-1: 1057
bigloo.info-2: 301949
bigloo.info-3: 603502
bigloo.info-4: 923863

Tag Table:
(Indirect)
Node: Top1057
Node: Acknowledgements3963
Node: Overview5401
Node: Modules10505
Node: Program Structure11518
Node: Module Declaration12458
Node: Module Initialization29416
Node: Qualified Notation31960
Node: Inline Procedures33001
Node: Module Access File35311
Node: Reading Path36521
Node: Core Language36927
Node: Syntax37395
Node: Comments38492
Node: Expressions39290
Node: Definitions50622
Node: DSSSL Support51243
Node: Standard Library55915
Node: Scheme Library56949
Node: Booleans57508
Node: Equivalence Predicates58515
Node: Pairs And Lists62970
Node: Symbols70872
Node: Keywords73927
Node: Numbers74583
Node: Characters86359
Node: UCS-2 Characters87799
Node: Strings89144
Node: Unicode (UCS-2) Strings102190
Node: Vectors109835
Node: Homogeneous Vectors (SRFI-4)113593
Node: Control Features117534
Node: Input and Output126993
Node: Library functions127358
Node: Memory mapped area162704
Node: Zip165850
Node: Tar167312
Node: Serialization170372
Node: Bit Manipulation176703
Node: Weak Pointers178469
Node: Hash Tables180276
Node: Misc. Algorithms189203
Node: Single Source Reachability Graphs189398
Node: System Programming191984
Node: Operating System Interface192222
Node: Files200707
Node: Process210485
Node: Socket218025
Node: Socket-Footnotes234858
Ref: Socket-Footnote-1234917
Ref: Socket-Footnote-2235052
Node: SSL235272
Node: SSL Sockets235667
Node: Certificates245975
Node: Private Keys247035
Node: Date247511
Node: Digest252757
Node: CRC256464
Node: Internet263063
Node: URLs263275
Node: HTTP265835
Node: Pattern Matching271023
Node: Pattern Matching-Footnotes272291
Ref: Pattern Matching-Footnote-1272370
Node: Bigloo Pattern Matching Facilities272467
Node: The Pattern Language274262
Node: Fast search277374
Node: Knuth - Morris - Pratt277669
Node: Boyer - Moore279836
Node: Boyer - Moore - Horspool281993
Node: Structures and Records282772
Node: Structures283255
Node: Records (SRFI-9)285137
Node: Object System289271
Node: Class declaration289918
Node: Creating and accessing objects295232
Node: Generic functions301949
Node: Widening and shrinking304312
Node: Object library311157
Node: Object serialization313026
Node: Equality313645
Node: Introspection313993
Node: Regular Parsing318922
Node: A New Way of Reading319815
Node: The Syntax of The Regular Grammar320827
Node: The Semantics Actions332446
Node: Options and user definitions336405
Node: Examples of Regular Grammar337434
Node: Lalr Parsing339608
Node: Grammar Definition340537
Node: Precedence and Associativity343146
Node: The Parsing Function346040
Node: The Regular Grammar346965
Node: Debugging Lalr Grammars348410
Node: A Simple Example348833
Node: Posix Regular Expressions350323
Node: Posix Regular Expressions-Footnotes353121
Ref: Posix Regular Expressions-Footnote-1353218
Node: Regular Expressions Procedures353688
Node: The Regular Expressions Pattern Language359649
Node: Basic assertions360169
Node: Characters and character classes361357
Node: Characters and character classes-Footnotes366724
Ref: Characters and character classes-Footnote-1366835
Ref: Characters and character classes-Footnote-2367000
Node: Quantifiers367165
Node: Clusters369606
Node: Clusters-Footnotes375967
Ref: Clusters-Footnote-1376030
Ref: Clusters-Footnote-2376206
Node: Alternation376277
Node: Backtracking378227
Node: Looking ahead and behind380426
Node: An Extended Example382741
Node: An Extended Example-Footnotes385438
Ref: An Extended Example-Footnote-1385523
Node: Command Line Parsing385791
Node: Cryptography389424
Node: Symmetric Block Ciphers390542
Node: String to Key402006
Node: Public Key Cryptography404708
Node: RSA404933
Node: DSA414155
Node: ElGamal415690
Node: PEM417369
Node: OpenPGP418947
Node: Examples428631
Node: Signatures428792
Node: Email Usage432468
Node: Encryption432974
Node: Development436363
Node: Errors Assertions and Traces437314
Node: Errors and Warnings437800
Node: Exceptions441378
Node: Try446129
Node: Assertions447926
Node: Tracing450449
Node: Threads454257
Node: Thread Common Functions455114
Node: Thread API455738
Node: Mutexes459477
Node: Condition Variables463120
Node: Fair Threads465687
Node: Introduction467388
Node: Fair Threads Api469240
Node: Thread469845
Node: Scheduler484127
Node: Signal488592
Node: SRFI-18 compatibility490758
Node: Posix Threads491459
Node: Using Posix Threads492386
Node: Threads API492926
Node: Mutexes API496001
Node: Condition Variables API496424
Node: Semaphores API497341
Node: SRFI-18498105
Node: Mixing Thread APIs499092
Node: Database501561
Node: SQLite501777
Node: Multimedia509868
Node: Photography510723
Node: Music513403
Node: Metadata and Playlist513889
Node: Mixer515963
Node: Playback517496
Node: MPD526695
Node: Color528619
Node: Mail529514
Node: RFC 2045530129
Node: RFC 2047532999
Node: RFC 2426534132
Node: RFC 2822537149
Node: Mail servers538578
Node: mailbox539073
Node: imap544893
Node: maildir546374
Node: Text546943
Node: BibTeX547157
Node: Character strings548658
Node: Character encodings552010
Node: CSV552276
Node: CSV Overview552618
Node: API Reference553067
Node: read-csv-record553409
Node: read-csv-records553945
Node: csv-for-each554515
Node: csv-map555074
Node: make-csv-lexer555661
Node: +csv-lexer+555960
Node: +tsv-lexer+556205
Node: +psv-lexer+556436
Node: Example556648
Node: Web557169
Node: Web Overview557556
Node: XML557820
Node: WebDAV561453
Node: CSS563559
Node: Web Date565149
Node: JSON565474
Node: Eval566921
Node: Eval compliance567235
Node: Eval standard functions568417
Node: Eval command line options578902
Node: Eval and the foreign interface579984
Node: Macro Expansion580484
Node: Parameters582608
Node: Explicit Typing586153
Node: C Interface589183
Node: The syntax of the foreign declarations591491
Node: Automatic extern clauses generation592503
Node: Importing an extern variable593092
Node: Importing an extern function594217
Node: Including an extern file595468
Node: Exporting a Scheme variable595826
Node: Defining an extern type596622
Node: Atomic types598021
Node: C structures and unions599536
Node: C pointers603502
Node: C null pointers605603
Node: C arrays606440
Node: C functions608488
Node: C enums609673
Node: C opaques610853
Node: The very dangerous pragma Bigloo special forms612551
Node: Name mangling615002
Node: Embedded Bigloo applications616702
Node: Using C bindings within the interpreter618019
Node: Java Interface619409
Node: Compiling with the JVM back-end620132
Node: JVM back-end and SRFI-0622275
Node: Limitation of the JVM back-end623097
Node: Connecting Scheme and Java code624501
Node: Automatic Java clauses generation626225
Node: Declaring Java classes626808
Node: Declaring abstract Java classes628550
Node: Extending Java classes629022
Node: Declaring Java arrays629716
Node: Exporting Scheme variables631090
Node: Bigloo module initialization634927
Node: Performance of the JVM back-end635511
Node: Bigloo Libraries636287
Node: Extending the Runtime System663345
Node: SRFIs663744
Node: Compiler Description674689
Node: Cross Compilation724154
Node: User Extensions731702
Node: Memory Profiling734258
Node: Bee738114
Node: Installing the Bee740089
Node: Entering the Bee740882
Node: The Bee Root Directory741265
Node: Building a Makefile742017
Node: Compiling742850
Node: Interpreting743225
Node: Pretty Printing744044
Node: Expansing744626
Node: On-line documentation745412
Node: Searching for source code746471
Node: Importing and Exporting747204
Node: Debugging748389
Node: Profiling748891
Node: Revision control749494
Node: Literate Programming750425
Node: Global Index752461
Node: Library Index923863
Node: Bibliography923993

End Tag Table


Local Variables:
coding: utf-8
End:
