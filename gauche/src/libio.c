/* Generated automatically from libio.scm.  DO NOT EDIT */
#define LIBGAUCHE_BODY 
#include <gauche.h>
#include <gauche/code.h>
#include "gauche/priv/configP.h"
#include "gauche/vminsn.h"
#include "gauche/exception.h"
#include "gauche/priv/portP.h"
#include "gauche/priv/writerP.h"
#include <stdlib.h>
#include <fcntl.h>
static ScmObj libioinput_portP(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libioinput_portP__STUB, 1, 0,SCM_FALSE,libioinput_portP, NULL, NULL);

static ScmObj libiooutput_portP(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libiooutput_portP__STUB, 1, 0,SCM_FALSE,libiooutput_portP, NULL, NULL);

static ScmObj libioportP(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libioportP__STUB, 1, 0,SCM_FALSE,libioportP, NULL, NULL);

static ScmObj libioport_closedP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_closedP__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_closedP, NULL, NULL);


static ScmObj libiostandard_input_port(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libiostandard_input_port__STUB, 0, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libiostandard_input_port, NULL, NULL);

static ScmObj libiostandard_output_port(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libiostandard_output_port__STUB, 0, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libiostandard_output_port, NULL, NULL);

static ScmObj libiostandard_error_port(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libiostandard_error_port__STUB, 0, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libiostandard_error_port, NULL, NULL);

static ScmObj libioport_name(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_name__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_name, NULL, NULL);

static ScmObj libioport_current_line(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_current_line__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_current_line, NULL, NULL);

static ScmObj libioport_file_number(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_file_number__STUB, 1, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_file_number, NULL, NULL);

static ScmObj libioport_fd_dupX(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_fd_dupX__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_fd_dupX, NULL, NULL);

static ScmObj libioport_attribute_setX(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libioport_attribute_setX__STUB, 3, 0,SCM_FALSE,libioport_attribute_setX, NULL, NULL);

static ScmObj libioport_attribute_ref(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libioport_attribute_ref__STUB, 2, 2,SCM_FALSE,libioport_attribute_ref, NULL, NULL);

static ScmObj libioport_attribute_deleteX(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libioport_attribute_deleteX__STUB, 2, 0,SCM_FALSE,libioport_attribute_deleteX, NULL, NULL);

static ScmObj libioport_attributes(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_attributes__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_attributes, NULL, NULL);

static ScmObj libioport_type(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_type__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_type, NULL, NULL);

static ScmObj libioport_buffering_SETTER(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libioport_buffering_SETTER__STUB, 2, 0,SCM_FALSE,libioport_buffering_SETTER, NULL, NULL);

static ScmObj libioport_buffering(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_buffering__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_buffering, NULL, NULL);

static ScmObj libioport_linkX(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_linkX__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_linkX, NULL, NULL);

static ScmObj libioport_unlinkX(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_unlinkX__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_unlinkX, NULL, NULL);

static ScmObj libioport_case_fold_SETTER(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libioport_case_fold_SETTER__STUB, 2, 0,SCM_FALSE,libioport_case_fold_SETTER, NULL, NULL);

static ScmObj libioport_case_fold(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_case_fold__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_case_fold, NULL, NULL);

static ScmObj libioclose_input_port(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioclose_input_port__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioclose_input_port, NULL, NULL);

static ScmObj libioclose_output_port(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioclose_output_port__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioclose_output_port, NULL, NULL);

static ScmObj libioclose_port(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioclose_port__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioclose_port, NULL, NULL);

#if defined(GAUCHE_WINDOWS)

#endif /* defined(GAUCHE_WINDOWS) */
#if !(defined(GAUCHE_WINDOWS))

#endif /* !(defined(GAUCHE_WINDOWS)) */
static ScmObj libio_25open_input_file(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libio_25open_input_file__STUB, 1, 1,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libio_25open_input_file, NULL, NULL);

static ScmObj libio_25open_output_file(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libio_25open_output_file__STUB, 1, 1,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libio_25open_output_file, NULL, NULL);

static ScmObj libioopen_input_fd_port(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioopen_input_fd_port__STUB, 1, 1,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioopen_input_fd_port, NULL, NULL);

static ScmObj libioopen_output_fd_port(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioopen_output_fd_port__STUB, 1, 1,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioopen_output_fd_port, NULL, NULL);

static void bufport_closer(ScmPort* p);
static ScmSize bufport_filler(ScmPort* p,ScmSize cnt);
static ScmObj libioopen_input_buffered_port(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioopen_input_buffered_port__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioopen_input_buffered_port, NULL, NULL);

static ScmSize bufport_flusher(ScmPort* p,ScmSize cnt,int G1792 SCM_UNUSED);
static ScmObj libioopen_output_buffered_port(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioopen_output_buffered_port__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioopen_output_buffered_port, NULL, NULL);

static ScmObj libioopen_input_string(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioopen_input_string__STUB, 1, 1,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioopen_input_string, NULL, NULL);

static ScmObj libioopen_output_string(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioopen_output_string__STUB, 0, 1,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioopen_output_string, NULL, NULL);

static ScmObj libioget_output_string(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioget_output_string__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioget_output_string, NULL, NULL);

static ScmObj libioget_output_byte_string(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioget_output_byte_string__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioget_output_byte_string, NULL, NULL);

static ScmObj libioget_remaining_input_string(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioget_remaining_input_string__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioget_remaining_input_string, NULL, NULL);

static ScmObj libioopen_coding_aware_port(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioopen_coding_aware_port__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioopen_coding_aware_port, NULL, NULL);

static ScmObj libioport_has_port_positionP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_has_port_positionP__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_has_port_positionP, NULL, NULL);

static ScmObj libioport_has_set_port_positionXP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_has_set_port_positionXP__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_has_set_port_positionXP, NULL, NULL);

static ScmObj libioport_position(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_position__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_position, NULL, NULL);

static ScmObj libioset_port_positionX(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libioset_port_positionX__STUB, 2, 0,SCM_FALSE,libioset_port_positionX, NULL, NULL);

static ScmObj libioport_seek(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_seek__STUB, 2, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_seek, NULL, NULL);

static unsigned char uvector__00001[] = {
 0u, 3u, 129u, 134u, 8u, 2u, 1u, 224u, 16u, 17u, 35u, 128u, 6u, 4u,
128u, 72u, 98u, 12u, 12u, 98u, 1u, 33u, 48u, 28u, 36u, 128u,};
static unsigned char uvector__00002[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 1u, 192u, 36u, 64u, 16u, 15u, 0u, 128u,
137u, 35u, 132u, 134u, 6u, 48u, 152u, 24u, 166u, 7u, 196u, 112u, 112u,
192u, 152u, 14u, 17u, 192u, 131u, 2u, 96u, 56u, 71u, 0u, 12u, 9u,
128u, 225u, 32u,};
static unsigned char uvector__00003[] = {
 0u, 3u, 154u, 6u, 8u, 16u, 48u, 134u, 238u, 3u, 2u, 146u, 31u, 91u,
160u, 104u, 28u, 146u, 57u, 152u, 96u, 76u, 22u, 136u, 230u, 65u,
129u, 32u, 114u, 57u, 88u, 96u, 76u, 7u, 8u, 228u, 225u, 130u, 4u,
12u, 33u, 187u, 128u, 192u, 164u, 135u, 214u, 232u, 34u, 7u, 130u,
73u, 35u, 147u, 6u, 4u, 197u, 144u, 142u, 74u, 24u, 18u, 9u, 35u,
146u, 6u, 4u, 129u, 200u, 227u, 161u, 129u, 49u, 38u, 35u, 141u, 134u,
6u, 81u, 5u, 65u, 36u, 65u, 109u, 208u, 68u, 15u, 4u, 145u, 5u, 183u,
64u, 208u, 57u, 36u, 113u, 144u, 192u, 153u, 55u, 145u, 198u, 3u, 2u,
65u, 36u, 113u, 96u, 192u, 153u, 55u, 145u, 197u, 67u, 3u, 32u, 16u,
72u, 131u, 32u, 210u, 72u, 76u, 153u, 32u, 226u, 71u, 19u, 12u, 9u,
157u, 65u, 28u, 72u, 48u, 36u, 26u, 71u, 16u, 12u, 9u, 157u, 65u, 28u,
56u, 48u, 50u, 192u, 225u, 182u, 9u, 9u, 157u, 64u, 76u, 153u, 32u,
226u, 72u, 225u, 161u, 129u, 32u, 114u, 56u, 96u, 96u, 100u, 2u, 7u,
16u, 124u, 26u, 73u, 9u, 161u, 33u, 36u, 142u, 20u, 24u, 19u, 66u,
162u, 200u, 225u, 33u, 129u, 32u, 210u, 56u, 56u, 96u, 77u, 10u, 139u,
35u, 130u, 134u, 6u, 81u, 8u, 65u, 164u, 66u, 48u, 56u, 77u, 10u,
139u, 9u, 161u, 35u, 198u, 18u, 36u, 112u, 48u, 192u, 154u, 24u, 190u,
71u, 2u, 12u, 9u, 6u, 145u, 192u, 3u, 2u, 104u, 98u, 249u, 12u, 65u,
129u, 140u, 65u, 164u, 38u, 134u, 42u, 18u, 64u,};
static unsigned char uvector__00004[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 9u, 193u, 164u, 50u, 136u, 66u, 13u, 34u,
17u, 129u, 196u, 31u, 6u, 144u, 219u, 4u, 136u, 50u, 13u, 33u, 148u,
65u, 80u, 73u, 16u, 91u, 116u, 17u, 3u, 193u, 36u, 65u, 109u, 208u,
52u, 14u, 76u, 28u, 76u, 36u, 73u, 28u, 36u, 48u, 49u, 132u, 192u,
197u, 48u, 62u, 35u, 131u, 134u, 4u, 192u, 112u, 142u, 4u, 24u, 19u,
1u, 194u, 56u, 0u, 96u, 76u, 7u, 9u, 0u,};
static ScmObj libio_25port_walkingP_SETTER(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libio_25port_walkingP_SETTER__STUB, 2, 0,SCM_FALSE,libio_25port_walkingP_SETTER, NULL, NULL);

static ScmObj libio_25port_walkingP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libio_25port_walkingP__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libio_25port_walkingP, NULL, NULL);

static ScmObj libio_25port_writing_sharedP_SETTER(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libio_25port_writing_sharedP_SETTER__STUB, 2, 0,SCM_FALSE,libio_25port_writing_sharedP_SETTER, NULL, NULL);

static ScmObj libio_25port_writing_sharedP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libio_25port_writing_sharedP__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libio_25port_writing_sharedP, NULL, NULL);

static ScmObj write_state_allocate(ScmClass* G1797 SCM_UNUSED,ScmObj G1798 SCM_UNUSED);
static void write_state_print(ScmObj obj,ScmPort* port,ScmWriteContext* G1801 SCM_UNUSED);
static ScmObj libio_25port_write_state_SETTER(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libio_25port_write_state_SETTER__STUB, 2, 0,SCM_FALSE,libio_25port_write_state_SETTER, NULL, NULL);

static ScmObj libio_25port_write_state(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libio_25port_write_state__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libio_25port_write_state, NULL, NULL);

static ScmObj libio_25port_lockX(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libio_25port_lockX__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libio_25port_lockX, NULL, NULL);

static ScmObj libio_25port_unlockX(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libio_25port_unlockX__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libio_25port_unlockX, NULL, NULL);

static unsigned char uvector__00005[] = {
 0u, 3u, 131u, 134u, 7u, 88u, 82u, 21u, 36u, 112u, 96u, 192u, 144u,
169u, 28u, 20u, 48u, 36u, 40u, 71u, 3u, 12u, 16u, 180u, 26u, 72u,
224u, 65u, 129u, 32u, 210u, 56u, 0u, 96u, 76u, 42u, 200u, 98u, 12u,
12u, 106u, 30u, 130u, 97u, 86u, 19u, 1u, 194u, 73u, 0u,};
static unsigned char uvector__00006[] = {
 0u, 3u, 128u, 134u, 8u, 94u, 13u, 36u, 112u, 0u, 192u, 144u, 105u,
12u, 65u, 129u, 141u, 66u, 96u, 56u, 73u, 0u,};
static unsigned char uvector__00007[] = {
 0u, 3u, 133u, 6u, 8u, 96u, 49u, 168u, 122u, 16u, 180u, 26u, 67u,
172u, 41u, 10u, 146u, 67u, 26u, 136u, 94u, 13u, 36u, 48u, 195u, 36u,
48u, 136u, 107u, 199u, 166u, 73u, 35u, 129u, 134u, 6u, 52u, 194u, 52u,
142u, 0u, 24u, 24u, 211u, 3u, 18u, 24u, 131u, 3u, 24u, 131u, 97u, 68u,
133u, 68u, 54u, 19u, 3u, 132u, 194u, 68u, 146u,};
static unsigned char uvector__00008[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 16u, 244u, 27u, 10u, 36u, 42u, 33u,
176u, 244u, 33u, 104u, 52u, 135u, 88u, 82u, 21u, 36u, 66u, 240u, 105u,
36u, 142u, 18u, 24u, 24u, 194u, 96u, 122u, 152u, 47u, 145u, 193u,
195u, 2u, 96u, 56u, 71u, 2u, 12u, 9u, 128u, 225u, 28u, 0u, 48u, 38u,
3u, 132u, 128u,};
static unsigned char uvector__00009[] = {
 0u, 3u, 161u, 6u, 7u, 88u, 126u, 21u, 36u, 116u, 16u, 192u, 144u,
169u, 29u, 0u, 48u, 36u, 62u, 71u, 63u, 12u, 2u, 32u, 136u, 76u, 26u,
220u, 72u, 231u, 193u, 129u, 48u, 174u, 35u, 158u, 6u, 4u, 136u, 72u,
231u, 65u, 129u, 32u, 210u, 57u, 192u, 96u, 76u, 42u, 200u, 230u,
225u, 129u, 214u, 34u, 133u, 73u, 28u, 216u, 48u, 36u, 42u, 71u, 53u,
12u, 9u, 17u, 17u, 204u, 195u, 2u, 99u, 76u, 71u, 50u, 12u, 2u, 32u,
136u, 76u, 26u, 252u, 72u, 230u, 33u, 129u, 50u, 102u, 35u, 151u,
134u, 4u, 136u, 72u, 229u, 129u, 129u, 32u, 210u, 57u, 80u, 96u, 76u,
152u, 200u, 229u, 33u, 128u, 68u, 17u, 25u, 131u, 68u, 73u, 18u, 196u,
194u, 39u, 12u, 47u, 100u, 146u, 71u, 40u, 12u, 9u, 155u, 9u, 28u,
152u, 48u, 36u, 70u, 71u, 36u, 12u, 9u, 156u, 41u, 28u, 136u, 48u,
38u, 116u, 196u, 113u, 224u, 192u, 153u, 211u, 17u, 198u, 131u, 2u,
68u, 164u, 113u, 128u, 192u, 153u, 194u, 145u, 197u, 195u, 2u, 65u,
164u, 113u, 80u, 192u, 153u, 175u, 17u, 196u, 67u, 3u, 60u, 81u, 6u,
146u, 56u, 128u, 96u, 72u, 52u, 142u, 24u, 24u, 19u, 66u, 183u, 72u,
225u, 65u, 129u, 224u, 68u, 112u, 105u, 9u, 161u, 91u, 164u, 142u,
16u, 24u, 19u, 67u, 16u, 72u, 224u, 225u, 129u, 32u, 210u, 56u, 40u,
96u, 77u, 12u, 65u, 35u, 129u, 134u, 8u, 90u, 13u, 36u, 112u, 32u,
192u, 144u, 105u, 28u, 0u, 48u, 38u, 135u, 2u, 16u, 196u, 24u, 24u,
212u, 61u, 4u, 208u, 224u, 64u, 154u, 24u, 44u, 34u, 145u, 17u, 193u,
164u, 38u, 112u, 164u, 69u, 34u, 33u, 131u, 75u, 241u, 9u, 141u, 48u,
138u, 68u, 67u, 6u, 150u, 226u, 19u, 1u, 194u, 73u, 0u,};
static unsigned char uvector__00010[] = {
 0u, 3u, 136u, 134u, 8u, 94u, 13u, 36u, 113u, 0u, 192u, 144u, 105u,
28u, 60u, 48u, 8u, 130u, 35u, 48u, 107u, 113u, 35u, 135u, 6u, 4u,
193u, 240u, 142u, 24u, 24u, 18u, 35u, 35u, 133u, 6u, 4u, 131u, 72u,
225u, 1u, 129u, 48u, 121u, 35u, 131u, 134u, 1u, 16u, 68u, 38u, 13u,
110u, 36u, 112u, 96u, 192u, 152u, 187u, 145u, 193u, 3u, 2u, 68u, 36u,
112u, 32u, 192u, 144u, 105u, 28u, 0u, 48u, 38u, 46u, 132u, 49u, 6u,
6u, 53u, 17u, 72u, 136u, 96u, 210u, 220u, 68u, 82u, 34u, 56u, 52u,
183u, 16u, 152u, 14u, 18u, 64u,};
static unsigned char uvector__00011[] = {
 0u, 3u, 133u, 6u, 8u, 96u, 49u, 168u, 122u, 16u, 180u, 26u, 67u,
192u, 136u, 224u, 210u, 25u, 226u, 136u, 52u, 145u, 20u, 136u, 142u,
13u, 34u, 36u, 137u, 98u, 97u, 19u, 134u, 23u, 178u, 73u, 17u, 72u,
136u, 96u, 210u, 252u, 67u, 172u, 69u, 10u, 145u, 20u, 136u, 134u,
13u, 45u, 196u, 58u, 195u, 240u, 169u, 36u, 49u, 168u, 138u, 68u, 67u,
6u, 150u, 226u, 34u, 145u, 17u, 193u, 165u, 184u, 136u, 94u, 13u, 36u,
48u, 195u, 36u, 48u, 136u, 107u, 199u, 188u, 73u, 35u, 129u, 134u, 6u,
52u, 199u, 68u, 142u, 0u, 24u, 24u, 211u, 3u, 18u, 24u, 131u, 3u, 24u,
131u, 98u, 40u, 125u, 33u, 81u, 13u, 132u, 192u, 225u, 49u, 213u, 36u,
128u,};
static unsigned char uvector__00012[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 42u, 17u, 92u, 27u, 17u, 67u, 233u, 10u,
136u, 108u, 61u, 8u, 90u, 13u, 33u, 224u, 68u, 112u, 105u, 12u, 241u,
68u, 26u, 72u, 138u, 68u, 71u, 6u, 145u, 18u, 68u, 177u, 48u, 137u,
195u, 11u, 217u, 36u, 136u, 164u, 68u, 48u, 105u, 126u, 33u, 214u,
34u, 133u, 72u, 138u, 68u, 67u, 6u, 150u, 226u, 29u, 97u, 248u, 84u,
145u, 20u, 136u, 134u, 13u, 45u, 196u, 69u, 34u, 35u, 131u, 75u, 113u,
16u, 188u, 26u, 73u, 35u, 132u, 134u, 6u, 48u, 152u, 30u, 166u, 13u,
36u, 112u, 112u, 192u, 152u, 14u, 17u, 192u, 131u, 2u, 96u, 56u, 71u,
0u, 12u, 9u, 128u, 225u, 32u,};
static ScmObj libioport_column(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_column__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_column, NULL, NULL);

static ScmObj libioread(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioread__STUB, 0, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioread, NULL, NULL);

static ScmObj libioread_char(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioread_char__STUB, 0, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioread_char, SCM_MAKE_INT(SCM_VM_READ_CHAR), NULL);

static ScmObj libiopeek_char(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libiopeek_char__STUB, 0, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libiopeek_char, SCM_MAKE_INT(SCM_VM_PEEK_CHAR), NULL);

static ScmObj libioeof_objectP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioeof_objectP__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioeof_objectP, SCM_MAKE_INT(SCM_VM_EOFP), NULL);

static ScmObj libiochar_readyP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libiochar_readyP__STUB, 0, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libiochar_readyP, NULL, NULL);

static ScmObj libioeof_object(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioeof_object__STUB, 0, 0,1, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioeof_object, NULL, NULL);

static ScmObj libiobyte_readyP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libiobyte_readyP__STUB, 0, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libiobyte_readyP, NULL, NULL);

static unsigned char uvector__00013[] = {
 0u, 3u, 134u, 6u, 6u, 104u, 178u, 45u, 36u, 112u, 144u, 192u, 145u,
105u, 28u, 28u, 48u, 38u, 3u, 132u, 112u, 32u, 192u, 152u, 14u, 17u,
192u, 3u, 2u, 96u, 56u, 72u,};
static ScmObj libioread_byte(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioread_byte__STUB, 0, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioread_byte, NULL, NULL);

static unsigned char uvector__00014[] = {
 0u, 3u, 134u, 6u, 6u, 104u, 186u, 47u, 36u, 112u, 144u, 192u, 145u,
121u, 28u, 28u, 48u, 38u, 3u, 132u, 112u, 32u, 192u, 152u, 14u, 17u,
192u, 3u, 2u, 96u, 56u, 72u,};
static ScmObj libiopeek_byte(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libiopeek_byte__STUB, 0, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libiopeek_byte, NULL, NULL);

static unsigned char uvector__00015[] = {
 0u, 3u, 134u, 6u, 6u, 104u, 194u, 49u, 36u, 112u, 144u, 192u, 145u,
137u, 28u, 28u, 48u, 38u, 3u, 132u, 112u, 32u, 192u, 152u, 14u, 17u,
192u, 3u, 2u, 96u, 56u, 72u,};
static ScmObj libioread_line(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libioread_line__STUB, 0, 3,SCM_FALSE,libioread_line, NULL, NULL);

static unsigned char uvector__00016[] = {
 0u, 3u, 162u, 134u, 7u, 16u, 233u, 25u, 112u, 18u, 72u, 232u, 129u,
129u, 48u, 39u, 35u, 161u, 134u, 8u, 206u, 52u, 141u, 73u, 29u, 8u,
48u, 36u, 106u, 71u, 65u, 12u, 9u, 26u, 17u, 207u, 131u, 4u, 109u,
26u, 146u, 57u, 232u, 96u, 72u, 212u, 142u, 114u, 24u, 35u, 120u,
203u, 128u, 18u, 57u, 192u, 96u, 72u, 200u, 142u, 108u, 24u, 25u, 68u,
113u, 26u, 16u, 202u, 19u, 24u, 33u, 28u, 144u, 152u, 146u, 144u,
244u, 19u, 9u, 64u, 152u, 14u, 18u, 71u, 53u, 12u, 9u, 143u, 169u,
28u, 208u, 48u, 36u, 104u, 71u, 51u, 12u, 12u, 128u, 70u, 130u, 58u,
131u, 73u, 33u, 49u, 234u, 36u, 115u, 32u, 192u, 153u, 143u, 17u,
204u, 67u, 2u, 65u, 164u, 114u, 224u, 193u, 27u, 70u, 164u, 142u, 90u,
24u, 18u, 53u, 35u, 149u, 134u, 8u, 238u, 50u, 129u, 201u, 28u, 168u,
48u, 36u, 14u, 71u, 37u, 12u, 17u, 228u, 122u, 252u, 72u, 227u, 225u,
129u, 52u, 36u, 44u, 142u, 54u, 24u, 25u, 227u, 224u, 194u, 24u, 196u,
15u, 31u, 136u, 52u, 72u, 4u, 144u, 205u, 26u, 132u, 208u, 144u, 178u,
25u, 28u, 64u, 140u, 184u, 1u, 33u, 148u, 38u, 132u, 34u, 4u, 207u,
28u, 54u, 198u, 129u, 51u, 30u, 9u, 143u, 81u, 36u, 146u, 71u, 21u,
12u, 9u, 161u, 59u, 68u, 113u, 16u, 192u, 215u, 32u, 146u, 56u, 128u,
96u, 73u, 4u, 142u, 30u, 24u, 25u, 0u, 131u, 67u, 40u, 107u, 144u,
136u, 77u, 10u, 137u, 13u, 18u, 17u, 36u, 72u, 33u, 148u, 53u, 200u,
68u, 48u, 169u, 13u, 82u, 17u, 36u, 134u, 240u, 154u, 26u, 62u, 19u,
66u, 118u, 136u, 100u, 84u, 208u, 169u, 194u, 71u, 14u, 12u, 9u, 161u,
234u, 100u, 112u, 144u, 192u, 154u, 30u, 18u, 71u, 8u, 12u, 9u, 33u,
145u, 193u, 131u, 2u, 104u, 116u, 153u, 28u, 12u, 48u, 38u, 133u, 68u,
145u, 192u, 67u, 2u, 104u, 114u, 1u, 28u, 0u, 48u, 36u, 134u, 67u,
16u, 96u, 77u, 10u, 26u, 36u,};
static unsigned char uvector__00017[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 34u, 64u, 241u, 248u, 131u, 68u, 128u,
73u, 12u, 209u, 168u, 143u, 35u, 215u, 226u, 67u, 35u, 136u, 17u,
151u, 0u, 36u, 50u, 136u, 238u, 50u, 129u, 200u, 141u, 163u, 82u, 27u,
99u, 65u, 29u, 65u, 164u, 50u, 136u, 226u, 52u, 33u, 148u, 70u, 241u,
151u, 0u, 34u, 57u, 34u, 54u, 141u, 73u, 15u, 66u, 51u, 141u, 35u,
82u, 28u, 67u, 164u, 101u, 192u, 73u, 36u, 146u, 72u, 225u, 33u, 129u,
140u, 38u, 6u, 41u, 131u, 65u, 28u, 28u, 48u, 38u, 3u, 132u, 112u,
32u, 192u, 152u, 14u, 17u, 192u, 3u, 2u, 96u, 56u, 72u,};
static unsigned char uvector__00018[] = {
 0u, 3u, 163u, 6u, 9u, 20u, 72u, 210u, 60u, 145u, 36u, 152u, 52u,
145u, 209u, 67u, 2u, 65u, 164u, 116u, 32u, 192u, 152u, 20u, 145u,
208u, 67u, 2u, 73u, 36u, 116u, 0u, 192u, 146u, 65u, 28u, 252u, 48u,
36u, 142u, 71u, 61u, 12u, 9u, 129u, 73u, 28u, 236u, 48u, 51u, 199u,
193u, 132u, 49u, 137u, 30u, 63u, 16u, 104u, 146u, 137u, 18u, 71u, 0u,
34u, 73u, 146u, 201u, 9u, 128u, 225u, 36u, 142u, 106u, 24u, 19u, 24u,
18u, 57u, 136u, 96u, 107u, 147u, 9u, 28u, 192u, 48u, 36u, 152u, 71u,
47u, 12u, 12u, 128u, 73u, 33u, 148u, 53u, 201u, 166u, 75u, 13u, 18u,
105u, 36u, 73u, 129u, 148u, 53u, 201u, 164u, 48u, 169u, 13u, 82u,
105u, 36u, 134u, 240u, 153u, 64u, 4u, 198u, 4u, 134u, 69u, 76u, 134u,
9u, 28u, 184u, 48u, 38u, 112u, 100u, 114u, 144u, 192u, 153u, 172u,
145u, 202u, 3u, 2u, 73u, 164u, 114u, 96u, 192u, 153u, 145u, 145u,
200u, 67u, 2u, 102u, 2u, 71u, 32u, 12u, 9u, 38u, 145u, 199u, 195u, 3u,
32u, 18u, 64u, 101u, 13u, 114u, 113u, 224u, 1u, 162u, 78u, 36u, 137u,
52u, 50u, 134u, 185u, 56u, 134u, 21u, 33u, 170u, 78u, 36u, 144u, 153u,
109u, 146u, 56u, 240u, 96u, 77u, 11u, 142u, 35u, 140u, 134u, 4u, 208u,
180u, 66u, 56u, 192u, 96u, 73u, 56u, 142u, 44u, 24u, 19u, 66u, 179u,
136u, 226u, 33u, 129u, 52u, 42u, 44u, 142u, 32u, 24u, 18u, 78u, 35u,
135u, 134u, 6u, 64u, 32u, 208u, 202u, 26u, 228u, 242u, 19u, 29u, 0u,
209u, 39u, 146u, 68u, 156u, 25u, 67u, 92u, 158u, 67u, 10u, 144u, 213u,
39u, 146u, 72u, 77u, 10u, 69u, 36u, 112u, 224u, 192u, 154u, 31u, 174u,
71u, 9u, 12u, 9u, 161u, 241u, 164u, 112u, 128u, 192u, 146u, 129u, 28u,
24u, 48u, 38u, 135u, 139u, 145u, 192u, 195u, 2u, 99u, 160u, 71u, 1u,
12u, 9u, 161u, 217u, 68u, 112u, 0u, 192u, 146u, 129u, 12u, 65u, 129u,
49u, 161u, 36u,};
static unsigned char uvector__00019[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 40u, 200u, 241u, 248u, 131u, 68u, 148u,
72u, 146u, 56u, 1u, 18u, 76u, 150u, 72u, 145u, 68u, 141u, 35u, 201u,
18u, 73u, 131u, 73u, 35u, 132u, 134u, 6u, 48u, 152u, 24u, 166u, 19u,
164u, 112u, 112u, 192u, 152u, 14u, 17u, 192u, 131u, 2u, 96u, 56u, 71u,
0u, 12u, 9u, 128u, 225u, 32u,};
static unsigned char uvector__00020[] = {
 0u, 3u, 173u, 134u, 8u, 190u, 13u, 36u, 117u, 160u, 192u, 144u, 105u,
29u, 92u, 48u, 74u, 82u, 159u, 10u, 36u, 117u, 96u, 192u, 146u, 153u,
29u, 68u, 48u, 69u, 240u, 105u, 35u, 168u, 6u, 4u, 131u, 72u, 233u,
161u, 130u, 82u, 17u, 140u, 26u, 120u, 81u, 35u, 165u, 134u, 4u, 197u,
44u, 142u, 148u, 24u, 18u, 13u, 35u, 164u, 6u, 4u, 197u, 44u, 142u,
140u, 24u, 30u, 3u, 128u, 139u, 96u, 210u, 19u, 19u, 226u, 19u, 14u,
18u, 71u, 68u, 12u, 9u, 145u, 65u, 29u, 12u, 48u, 36u, 26u, 71u, 65u,
12u, 9u, 145u, 65u, 28u, 252u, 48u, 69u, 240u, 105u, 35u, 159u, 6u,
4u, 131u, 72u, 231u, 129u, 129u, 51u, 17u, 35u, 156u, 134u, 9u, 74u,
83u, 225u, 164u, 142u, 112u, 24u, 18u, 83u, 35u, 154u, 134u, 7u, 18u,
71u, 51u, 12u, 17u, 124u, 26u, 72u, 230u, 65u, 129u, 32u, 210u, 57u,
128u, 96u, 77u, 8u, 75u, 35u, 151u, 6u, 1u, 42u, 74u, 97u, 132u, 112u,
158u, 64u, 73u, 9u, 161u, 9u, 97u, 52u, 32u, 16u, 145u, 203u, 67u, 2u,
104u, 74u, 249u, 28u, 168u, 48u, 36u, 166u, 71u, 41u, 12u, 12u, 128u,
74u, 98u, 49u, 131u, 73u, 33u, 176u, 38u, 132u, 174u, 0u, 153u, 206u,
4u, 204u, 68u, 38u, 66u, 68u, 4u, 193u, 228u, 38u, 3u, 132u, 146u,
57u, 56u, 96u, 77u, 11u, 142u, 35u, 147u, 6u, 4u, 131u, 72u, 228u,
129u, 129u, 52u, 46u, 56u, 142u, 68u, 24u, 30u, 4u, 91u, 6u, 144u,
219u, 41u, 132u, 208u, 184u, 224u, 154u, 23u, 156u, 73u, 28u, 128u,
48u, 38u, 135u, 66u, 145u, 199u, 195u, 2u, 65u, 164u, 113u, 208u,
192u, 154u, 29u, 10u, 71u, 27u, 12u, 12u, 241u, 240u, 97u, 12u, 98u,
63u, 16u, 104u, 144u, 9u, 33u, 145u, 197u, 66u, 104u, 114u, 209u, 36u,
145u, 197u, 67u, 2u, 104u, 132u, 217u, 28u, 68u, 48u, 53u, 202u, 164u,
142u, 32u, 24u, 18u, 85u, 35u, 135u, 134u, 6u, 64u, 32u, 208u, 202u,
26u, 229u, 98u, 19u, 68u, 80u, 3u, 68u, 172u, 73u, 18u, 168u, 101u,
13u, 114u, 177u, 12u, 42u, 67u, 84u, 172u, 73u, 33u, 188u, 38u, 137u,
64u, 132u, 209u, 9u, 178u, 25u, 21u, 52u, 69u, 76u, 145u, 195u, 131u,
2u, 104u, 164u, 169u, 28u, 36u, 48u, 38u, 138u, 37u, 145u, 194u, 3u,
2u, 74u, 228u, 112u, 96u, 192u, 154u, 39u, 170u, 71u, 3u, 12u, 9u,
162u, 40u, 4u, 112u, 16u, 192u, 154u, 39u, 4u, 71u, 0u, 12u, 9u, 43u,
144u, 196u, 24u, 19u, 68u, 54u, 201u, 0u,};
static unsigned char uvector__00021[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 44u, 71u, 226u, 13u, 18u, 1u, 36u, 50u,
56u, 168u, 120u, 17u, 108u, 26u, 67u, 108u, 166u, 35u, 24u, 52u, 134u,
192u, 37u, 73u, 76u, 48u, 142u, 19u, 200u, 9u, 34u, 47u, 131u, 72u,
113u, 36u, 9u, 74u, 83u, 225u, 164u, 69u, 240u, 105u, 15u, 1u, 192u,
69u, 176u, 105u, 18u, 144u, 140u, 96u, 211u, 194u, 137u, 17u, 124u,
26u, 73u, 2u, 82u, 148u, 248u, 81u, 17u, 124u, 26u, 73u, 36u, 146u,
56u, 72u, 96u, 99u, 9u, 129u, 138u, 96u, 188u, 71u, 7u, 12u, 9u, 128u,
225u, 28u, 8u, 48u, 38u, 3u, 132u, 112u, 0u, 192u, 152u, 14u, 18u,};
static ScmObj libiochar_word_constituentP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libiochar_word_constituentP__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libiochar_word_constituentP, NULL, NULL);

static ScmObj libioread_block(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioread_block__STUB, 1, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioread_block, NULL, NULL);

static ScmObj libioread_list(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libioread_list__STUB, 1, 2,SCM_FALSE,libioread_list, NULL, NULL);

static ScmObj libioport_TObyte_string(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioport_TObyte_string__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioport_TObyte_string, NULL, NULL);

static unsigned char uvector__00022[] = {
 0u, 3u, 138u, 6u, 8u, 218u, 89u, 36u, 113u, 48u, 192u, 146u, 201u,
28u, 68u, 48u, 75u, 80u, 108u, 179u, 45u, 134u, 25u, 112u, 146u, 56u,
96u, 96u, 73u, 100u, 142u, 22u, 24u, 18u, 13u, 35u, 132u, 134u, 4u,
193u, 228u, 142u, 16u, 24u, 25u, 0u, 150u, 68u, 121u, 30u, 191u, 18u,
66u, 96u, 242u, 19u, 1u, 194u, 71u, 6u, 12u, 9u, 139u, 169u, 28u, 0u,
48u, 38u, 46u, 164u, 49u, 6u, 6u, 49u, 6u, 144u, 219u, 44u, 132u,
197u, 212u, 38u, 15u, 33u, 48u, 28u, 36u, 144u,};
static unsigned char uvector__00023[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 46u, 193u, 164u, 54u, 203u, 34u, 60u,
143u, 95u, 136u, 150u, 160u, 217u, 102u, 91u, 12u, 50u, 225u, 34u,
54u, 150u, 73u, 36u, 112u, 144u, 192u, 198u, 19u, 3u, 20u, 192u, 248u,
142u, 14u, 24u, 19u, 1u, 194u, 56u, 16u, 96u, 76u, 7u, 8u, 224u, 1u,
129u, 48u, 28u, 36u,};
static unsigned char uvector__00024[] = {
 0u, 3u, 140u, 134u, 7u, 17u, 47u, 65u, 164u, 57u, 203u, 243u, 1u,
36u, 113u, 128u, 192u, 152u, 32u, 145u, 197u, 195u, 2u, 76u, 4u, 113u,
96u, 192u, 146u, 249u, 28u, 80u, 48u, 38u, 4u, 228u, 113u, 48u, 192u,
146u, 241u, 28u, 72u, 48u, 36u, 26u, 71u, 16u, 12u, 9u, 129u, 57u,
28u, 52u, 48u, 76u, 51u, 1u, 35u, 134u, 6u, 4u, 152u, 8u, 225u, 65u,
129u, 148u, 71u, 18u, 249u, 9u, 141u, 136u, 76u, 7u, 9u, 28u, 36u,
48u, 38u, 67u, 36u, 112u, 128u, 192u, 146u, 249u, 28u, 16u, 48u, 75u,
208u, 105u, 35u, 129u, 134u, 4u, 151u, 136u, 224u, 65u, 129u, 32u,
210u, 56u, 0u, 96u, 76u, 180u, 72u, 98u, 12u, 12u, 106u, 25u, 28u,
64u, 151u, 194u, 101u, 162u, 68u, 192u, 24u, 84u, 146u, 19u, 32u,
226u, 73u, 0u,};
static unsigned char uvector__00025[] = {
 0u, 3u, 129u, 134u, 8u, 122u, 13u, 14u, 74u, 25u, 28u, 64u, 151u,
196u, 189u, 6u, 146u, 38u, 0u, 194u, 164u, 144u, 202u, 35u, 137u,
124u, 137u, 134u, 96u, 33u, 196u, 75u, 208u, 105u, 14u, 114u, 252u,
192u, 73u, 36u, 145u, 192u, 67u, 3u, 26u, 132u, 193u, 8u, 145u, 192u,
3u, 2u, 65u, 164u, 49u, 6u, 6u, 49u, 47u, 65u, 164u, 38u, 3u, 132u,
144u,};
static unsigned char uvector__00026[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 49u, 75u, 208u, 105u, 16u, 244u, 26u, 28u,
148u, 50u, 56u, 129u, 47u, 137u, 122u, 13u, 36u, 76u, 1u, 133u, 73u,
33u, 148u, 71u, 18u, 249u, 19u, 12u, 192u, 67u, 136u, 151u, 160u,
210u, 28u, 229u, 249u, 128u, 146u, 73u, 36u, 112u, 144u, 192u, 198u,
19u, 3u, 20u, 193u, 32u, 142u, 14u, 24u, 19u, 1u, 194u, 56u, 16u, 96u,
76u, 7u, 8u, 224u, 1u, 129u, 48u, 28u, 36u,};
static unsigned char uvector__00027[] = {
 0u, 3u, 129u, 134u, 9u, 142u, 100u, 126u, 36u, 112u, 0u, 192u, 147u,
33u, 12u, 65u, 129u, 140u, 76u, 132u, 38u, 3u, 132u, 144u,};
static unsigned char uvector__00028[] = {
 0u, 3u, 129u, 6u, 9u, 136u, 76u, 179u, 28u, 204u, 252u, 96u, 210u,
71u, 1u, 12u, 9u, 6u, 145u, 192u, 3u, 2u, 76u, 228u, 49u, 6u, 6u, 49u,
6u, 144u, 152u, 14u, 18u, 64u,};
static unsigned char uvector__00029[] = {
 0u, 3u, 135u, 6u, 6u, 97u, 52u, 65u, 164u, 76u, 66u, 101u, 152u,
230u, 103u, 227u, 6u, 146u, 71u, 11u, 12u, 12u, 97u, 48u, 49u, 76u,
15u, 136u, 225u, 33u, 129u, 48u, 28u, 35u, 130u, 6u, 4u, 192u, 112u,
142u, 4u, 24u, 19u, 1u, 194u, 64u,};
static unsigned char uvector__00030[] = {
 0u, 3u, 129u, 134u, 9u, 138u, 105u, 131u, 73u, 28u, 8u, 48u, 36u,
26u, 71u, 0u, 12u, 9u, 52u, 144u, 196u, 24u, 24u, 196u, 26u, 66u, 96u,
56u, 73u, 0u,};
static unsigned char uvector__00031[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 53u, 65u, 164u, 76u, 83u, 76u, 26u, 73u,
28u, 36u, 48u, 49u, 132u, 192u, 197u, 48u, 62u, 35u, 131u, 134u, 4u,
192u, 112u, 142u, 4u, 24u, 19u, 1u, 194u, 56u, 0u, 96u, 76u, 7u, 9u,
0u,};
static ScmObj libioreader_lexical_mode(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libioreader_lexical_mode__STUB, 0, 2,SCM_FALSE,libioreader_lexical_mode, NULL, NULL);

static ScmObj libio_25port_ungotten_chars(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libio_25port_ungotten_chars__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libio_25port_ungotten_chars, NULL, NULL);

static ScmObj libio_25port_ungotten_bytes(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libio_25port_ungotten_bytes__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libio_25port_ungotten_bytes, NULL, NULL);

static ScmObj libiodefine_reader_ctor(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libiodefine_reader_ctor__STUB, 2, 2,SCM_FALSE,libiodefine_reader_ctor, NULL, NULL);

static ScmObj libio_25get_reader_ctor(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libio_25get_reader_ctor__STUB, 1, 0,SCM_FALSE,libio_25get_reader_ctor, NULL, NULL);

static ScmObj libiodefine_reader_directive(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libiodefine_reader_directive__STUB, 2, 0,SCM_FALSE,libiodefine_reader_directive, NULL, NULL);

static ScmObj libiocurrent_read_context(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libiocurrent_read_context__STUB, 0, 2,SCM_FALSE,libiocurrent_read_context, NULL, NULL);

static ScmObj libioread_referenceP(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libioread_referenceP__STUB, 1, 0,SCM_FALSE,libioread_referenceP, NULL, NULL);

static ScmObj libioread_reference_has_valueP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioread_reference_has_valueP__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioread_reference_has_valueP, NULL, NULL);

static ScmObj libioread_reference_value(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioread_reference_value__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioread_reference_value, NULL, NULL);

static unsigned char uvector__00032[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 154u, 230u, 146u, 71u, 9u, 12u, 9u,
52u, 145u, 193u, 195u, 2u, 96u, 56u, 71u, 2u, 12u, 9u, 128u, 225u,
28u, 0u, 48u, 38u, 3u, 132u, 128u,};
static unsigned char uvector__00033[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 155u, 38u, 146u, 71u, 9u, 12u, 9u,
52u, 145u, 193u, 195u, 2u, 96u, 56u, 71u, 2u, 12u, 9u, 128u, 225u,
28u, 0u, 48u, 38u, 3u, 132u, 128u,};
static void parse_write_optionals(ScmObj opt1,ScmObj opt2,ScmPort** pp,const ScmWriteControls** pc);
static ScmObj libiowrite(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libiowrite__STUB, 1, 3,SCM_FALSE,libiowrite, NULL, NULL);

static ScmObj libiowrite_simple(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libiowrite_simple__STUB, 1, 2,SCM_FALSE,libiowrite_simple, NULL, NULL);

static ScmObj libiowrite_shared(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libiowrite_shared__STUB, 1, 3,SCM_FALSE,libiowrite_shared, NULL, NULL);

static ScmObj libiodisplay(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libiodisplay__STUB, 1, 3,SCM_FALSE,libiodisplay, NULL, NULL);

static ScmObj libionewline(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libionewline__STUB, 0, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libionewline, NULL, NULL);

static ScmObj libiofresh_line(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libiofresh_line__STUB, 0, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libiofresh_line, NULL, NULL);

static ScmObj libiowrite_char(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libiowrite_char__STUB, 1, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libiowrite_char, SCM_MAKE_INT(SCM_VM_WRITE_CHAR), NULL);

static ScmObj libiowrite_byte(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libiowrite_byte__STUB, 1, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libiowrite_byte, NULL, NULL);

static unsigned char uvector__00034[] = {
 0u, 3u, 134u, 6u, 6u, 105u, 182u, 110u, 36u, 112u, 144u, 192u, 147u,
113u, 28u, 28u, 48u, 38u, 3u, 132u, 112u, 32u, 192u, 152u, 14u, 17u,
192u, 3u, 2u, 96u, 56u, 72u,};
static ScmObj libiowrite_limited(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libiowrite_limited__STUB, 2, 2,SCM_FALSE,libiowrite_limited, NULL, NULL);

static unsigned char uvector__00035[] = {
 0u, 3u, 134u, 6u, 6u, 105u, 190u, 112u, 36u, 112u, 144u, 192u, 147u,
129u, 28u, 28u, 48u, 38u, 3u, 132u, 112u, 32u, 192u, 152u, 14u, 17u,
192u, 3u, 2u, 96u, 56u, 72u,};
static ScmObj libioflush(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioflush__STUB, 0, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioflush, NULL, NULL);

static ScmObj libioflush_all_ports(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libioflush_all_ports__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libioflush_all_ports, NULL, NULL);

static ScmObj libiowrite_need_recurseP(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libiowrite_need_recurseP__STUB, 1, 0,SCM_FALSE,libiowrite_need_recurseP, NULL, NULL);

static unsigned char uvector__00036[] = {
 0u, 3u, 137u, 6u, 9u, 198u, 95u, 131u, 68u, 229u, 57u, 206u, 132u,
145u, 196u, 3u, 2u, 96u, 122u, 71u, 14u, 12u, 9u, 58u, 17u, 195u, 67u,
2u, 78u, 100u, 112u, 176u, 192u, 152u, 30u, 145u, 194u, 131u, 2u, 65u,
164u, 112u, 144u, 192u, 146u, 249u, 28u, 28u, 48u, 50u, 206u, 97u,
48u, 28u, 36u, 112u, 96u, 192u, 147u, 153u, 28u, 20u, 48u, 50u, 1u,
57u, 136u, 142u, 13u, 36u, 132u, 198u, 4u, 145u, 192u, 195u, 2u, 100u,
24u, 71u, 2u, 12u, 9u, 6u, 145u, 192u, 3u, 2u, 100u, 24u, 67u, 16u,
96u, 99u, 18u, 252u, 26u, 68u, 35u, 57u, 132u, 200u, 48u, 38u, 3u,
132u, 146u,};
static unsigned char uvector__00037[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 58u, 203u, 240u, 105u, 16u, 140u, 230u,
34u, 56u, 52u, 137u, 198u, 95u, 131u, 68u, 229u, 57u, 206u, 132u,
146u, 71u, 9u, 12u, 12u, 97u, 48u, 49u, 76u, 18u, 8u, 224u, 225u,
129u, 48u, 28u, 35u, 129u, 6u, 4u, 192u, 112u, 142u, 0u, 24u, 19u, 1u,
194u, 64u,};
static unsigned char uvector__00038[] = {
 0u, 3u, 128u, 6u, 7u, 73u, 219u, 128u, 146u, 24u, 131u, 3u, 24u,
157u, 136u, 76u, 7u, 9u, 32u,};
static unsigned char uvector__00039[] = {
 0u, 3u, 194u, 148u, 24u, 39u, 121u, 226u, 95u, 36u, 120u, 82u, 67u,
2u, 75u, 228u, 120u, 82u, 3u, 2u, 79u, 4u, 120u, 81u, 67u, 4u, 164u,
39u, 153u, 226u, 95u, 110u, 60u, 4u, 145u, 225u, 67u, 12u, 9u, 134u,
145u, 30u, 20u, 16u, 192u, 146u, 249u, 30u, 20u, 0u, 192u, 147u, 193u,
30u, 17u, 224u, 192u, 152u, 105u, 17u, 225u, 27u, 12u, 13u, 226u,
122u, 131u, 72u, 120u, 9u, 134u, 40u, 76u, 8u, 137u, 35u, 194u, 50u,
24u, 19u, 31u, 226u, 60u, 35u, 1u, 129u, 32u, 210u, 60u, 34u, 193u,
129u, 49u, 254u, 35u, 194u, 40u, 24u, 39u, 185u, 126u, 13u, 36u, 120u,
68u, 195u, 2u, 65u, 164u, 120u, 68u, 131u, 2u, 75u, 228u, 120u, 68u,
3u, 2u, 102u, 20u, 71u, 132u, 44u, 48u, 79u, 128u, 79u, 162u, 126u,
151u, 201u, 17u, 151u, 0u, 14u, 145u, 151u, 1u, 36u, 129u, 29u, 198u,
83u, 233u, 159u, 200u, 156u, 69u, 8u, 4u, 191u, 25u, 24u, 54u, 120u,
36u, 143u, 8u, 80u, 96u, 77u, 8u, 83u, 35u, 194u, 18u, 24u, 18u, 125u,
35u, 194u, 14u, 24u, 19u, 66u, 68u, 8u, 240u, 131u, 6u, 4u, 158u, 8u,
240u, 130u, 134u, 4u, 131u, 72u, 240u, 129u, 6u, 4u, 208u, 145u, 210u,
60u, 32u, 33u, 129u, 35u, 34u, 60u, 32u, 1u, 129u, 37u, 242u, 59u,
240u, 96u, 77u, 9u, 29u, 35u, 190u, 6u, 4u, 208u, 145u, 2u, 59u, 200u,
96u, 77u, 8u, 159u, 35u, 188u, 6u, 4u, 159u, 72u, 238u, 225u, 129u,
51u, 225u, 35u, 185u, 134u, 4u, 207u, 248u, 142u, 228u, 24u, 18u, 95u,
35u, 184u, 6u, 4u, 207u, 248u, 142u, 220u, 24u, 19u, 62u, 18u, 59u,
96u, 96u, 20u, 32u, 50u, 249u, 20u, 32u, 66u, 50u, 9u, 159u, 241u, 9u,
161u, 34u, 4u, 145u, 218u, 131u, 2u, 104u, 132u, 25u, 29u, 164u, 48u,
36u, 190u, 71u, 103u, 12u, 9u, 162u, 16u, 100u, 118u, 32u, 193u, 62u,
1u, 66u, 6u, 40u, 65u, 37u, 242u, 68u, 101u, 192u, 3u, 164u, 101u,
192u, 73u, 32u, 71u, 113u, 149u, 8u, 25u, 159u, 200u, 156u, 69u, 8u,
44u, 191u, 25u, 24u, 54u, 120u, 36u, 142u, 194u, 24u, 19u, 68u, 225u,
72u, 236u, 1u, 129u, 40u, 64u, 200u, 235u, 193u, 129u, 52u, 81u, 32u,
142u, 186u, 24u, 18u, 120u, 35u, 174u, 6u, 4u, 131u, 72u, 235u, 65u,
129u, 52u, 81u, 84u, 142u, 178u, 24u, 18u, 50u, 35u, 172u, 6u, 4u,
151u, 200u, 234u, 193u, 129u, 52u, 81u, 32u, 142u, 166u, 24u, 19u,
68u, 244u, 72u, 234u, 65u, 129u, 40u, 64u, 200u, 234u, 33u, 129u, 52u,
75u, 28u, 142u, 156u, 24u, 19u, 68u, 194u, 136u, 233u, 161u, 129u,
37u, 242u, 58u, 88u, 96u, 77u, 18u, 199u, 35u, 164u, 134u, 1u, 66u,
13u, 47u, 145u, 66u, 4u, 35u, 32u, 154u, 38u, 20u, 66u, 104u, 162u,
65u, 36u, 116u, 128u, 192u, 154u, 53u, 142u, 71u, 71u, 12u, 9u, 47u,
145u, 208u, 195u, 4u, 226u, 26u, 165u, 243u, 6u, 207u, 4u, 142u, 132u,
24u, 18u, 120u, 35u, 160u, 134u, 4u, 131u, 72u, 231u, 225u, 129u, 52u,
115u, 40u, 142u, 122u, 24u, 19u, 71u, 39u, 72u, 231u, 97u, 130u, 113u,
13u, 18u, 249u, 131u, 103u, 130u, 71u, 58u, 12u, 9u, 60u, 17u, 206u,
67u, 2u, 65u, 164u, 115u, 112u, 192u, 154u, 61u, 182u, 71u, 53u, 12u,
9u, 163u, 217u, 196u, 115u, 48u, 192u, 40u, 65u, 229u, 242u, 19u, 71u,
179u, 130u, 104u, 228u, 233u, 35u, 153u, 6u, 4u, 210u, 16u, 34u, 57u,
136u, 96u, 73u, 124u, 142u, 90u, 24u, 5u, 8u, 68u, 190u, 73u, 28u,
172u, 48u, 38u, 145u, 74u, 145u, 202u, 131u, 2u, 75u, 228u, 114u,
128u, 192u, 154u, 69u, 42u, 71u, 36u, 12u, 2u, 132u, 38u, 95u, 36u,
142u, 70u, 24u, 19u, 73u, 17u, 136u, 228u, 65u, 129u, 37u, 242u, 56u,
240u, 96u, 20u, 33u, 82u, 249u, 36u, 113u, 208u, 192u, 154u, 75u, 6u,
71u, 28u, 12u, 9u, 47u, 145u, 198u, 131u, 5u, 8u, 92u, 241u, 47u,
240u, 18u, 71u, 24u, 12u, 9u, 47u, 145u, 197u, 195u, 2u, 79u, 4u,
113u, 80u, 192u, 154u, 77u, 58u, 71u, 18u, 12u, 20u, 33u, 147u, 196u,
190u, 38u, 87u, 73u, 155u, 128u, 146u, 71u, 17u, 12u, 9u, 66u, 26u,
71u, 16u, 12u, 9u, 47u, 145u, 195u, 195u, 2u, 79u, 4u, 112u, 208u,
192u, 202u, 40u, 67u, 167u, 137u, 124u, 132u, 210u, 133u, 48u, 244u,
19u, 73u, 167u, 67u, 96u, 77u, 37u, 128u, 9u, 164u, 136u, 97u, 52u,
138u, 72u, 38u, 144u, 111u, 132u, 209u, 172u, 64u, 154u, 33u, 0u, 26u,
66u, 102u, 20u, 72u, 76u, 124u, 201u, 35u, 133u, 134u, 4u, 210u, 165u,
34u, 56u, 80u, 96u, 73u, 124u, 142u, 18u, 24u, 18u, 120u, 35u, 131u,
134u, 4u, 210u, 165u, 34u, 56u, 40u, 96u, 120u, 20u, 33u, 242u, 249u,
9u, 165u, 72u, 228u, 142u, 6u, 24u, 19u, 75u, 178u, 200u, 224u, 65u,
129u, 37u, 242u, 56u, 0u, 96u, 77u, 46u, 203u, 33u, 136u, 48u, 49u,
137u, 126u, 13u, 158u, 8u, 77u, 46u, 192u, 36u, 128u,};
static unsigned char uvector__00040[] = {
 0u, 3u, 135u, 6u, 6u, 97u, 56u, 203u, 240u, 108u, 240u, 67u, 192u,
161u, 15u, 151u, 200u, 101u, 20u, 33u, 211u, 196u, 190u, 69u, 8u,
100u, 241u, 47u, 137u, 149u, 210u, 102u, 224u, 36u, 135u, 161u, 66u,
23u, 60u, 75u, 252u, 4u, 134u, 192u, 40u, 66u, 165u, 242u, 64u, 161u,
9u, 151u, 201u, 2u, 132u, 34u, 95u, 36u, 10u, 16u, 121u, 124u, 137u,
196u, 52u, 75u, 230u, 13u, 158u, 8u, 156u, 67u, 84u, 190u, 96u, 217u,
224u, 144u, 40u, 65u, 165u, 242u, 40u, 64u, 132u, 100u, 40u, 65u, 37u,
242u, 68u, 226u, 40u, 65u, 101u, 248u, 200u, 193u, 179u, 193u, 36u,
10u, 16u, 25u, 124u, 138u, 16u, 33u, 25u, 9u, 250u, 95u, 36u, 78u,
34u, 132u, 2u, 95u, 140u, 140u, 27u, 60u, 18u, 67u, 72u, 158u, 229u,
248u, 52u, 146u, 27u, 196u, 245u, 6u, 144u, 240u, 37u, 33u, 60u, 207u,
18u, 251u, 113u, 224u, 36u, 78u, 243u, 196u, 190u, 73u, 36u, 146u,
56u, 88u, 96u, 99u, 9u, 129u, 138u, 96u, 164u, 71u, 9u, 12u, 9u, 128u,
225u, 28u, 16u, 48u, 38u, 3u, 132u, 112u, 32u, 192u, 152u, 14u, 18u,};
static unsigned char uvector__00041[] = {
 0u, 3u, 143u, 134u, 9u, 190u, 95u, 131u, 73u, 28u, 120u, 48u, 36u,
26u, 71u, 29u, 12u, 9u, 47u, 145u, 198u, 195u, 3u, 60u, 124u, 24u,
67u, 24u, 151u, 227u, 241u, 6u, 137u, 40u, 146u, 19u, 1u, 194u, 73u,
28u, 84u, 48u, 38u, 21u, 164u, 113u, 16u, 192u, 215u, 66u, 32u, 72u,
226u, 1u, 129u, 40u, 68u, 8u, 225u, 225u, 129u, 144u, 8u, 52u, 50u,
134u, 186u, 17u, 18u, 19u, 15u, 192u, 209u, 66u, 34u, 73u, 20u, 34u,
1u, 148u, 53u, 208u, 136u, 144u, 194u, 164u, 53u, 80u, 136u, 146u,
72u, 111u, 9u, 139u, 184u, 76u, 43u, 72u, 100u, 84u, 196u, 60u, 145u,
195u, 131u, 2u, 101u, 82u, 71u, 9u, 12u, 9u, 147u, 201u, 28u, 32u,
48u, 37u, 8u, 145u, 28u, 24u, 48u, 38u, 70u, 228u, 112u, 48u, 192u,
152u, 126u, 17u, 192u, 67u, 2u, 100u, 8u, 71u, 0u, 12u, 9u, 66u, 36u,
67u, 16u, 96u, 76u, 51u, 73u, 0u,};
static unsigned char uvector__00042[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 20u, 34u, 114u, 252u, 126u, 32u,
209u, 37u, 18u, 68u, 223u, 47u, 193u, 164u, 145u, 194u, 67u, 3u, 24u,
76u, 16u, 211u, 7u, 162u, 56u, 56u, 96u, 76u, 7u, 8u, 224u, 65u, 129u,
48u, 28u, 35u, 128u, 6u, 4u, 192u, 112u, 144u,};
static unsigned char uvector__00043[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 161u, 20u, 161u, 19u, 36u, 112u,
144u, 192u, 148u, 34u, 100u, 112u, 112u, 192u, 152u, 14u, 17u, 192u,
131u, 2u, 96u, 56u, 71u, 0u, 12u, 9u, 128u, 225u, 32u,};
static unsigned char uvector__00044[] = {
 0u, 3u, 131u, 134u, 10u, 17u, 82u, 71u, 5u, 12u, 20u, 34u, 210u, 44u,
42u, 72u, 224u, 129u, 129u, 33u, 82u, 56u, 16u, 96u, 73u, 20u, 142u,
0u, 24u, 19u, 4u, 178u, 24u, 131u, 3u, 28u, 42u, 19u, 4u, 176u, 152u,
14u, 18u, 64u,};
static unsigned char uvector__00045[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 20u, 34u, 233u, 10u, 138u, 17u,
105u, 22u, 21u, 34u, 132u, 84u, 146u, 56u, 72u, 96u, 99u, 133u, 83u,
5u, 18u, 56u, 56u, 96u, 76u, 7u, 8u, 224u, 65u, 129u, 48u, 28u, 35u,
128u, 6u, 4u, 192u, 112u, 144u,};
static ScmObj write_controls_allocate(ScmClass* G1823 SCM_UNUSED,ScmObj G1824 SCM_UNUSED);
static unsigned char uvector__00046[] = {
 0u, 3u, 202u, 128u, 24u, 40u, 70u, 3u, 229u, 8u, 201u, 161u, 26u,
161u, 27u, 161u, 28u, 161u, 29u, 161u, 30u, 161u, 31u, 161u, 64u,
161u, 65u, 161u, 66u, 161u, 67u, 161u, 68u, 161u, 69u, 161u, 70u,
161u, 71u, 36u, 121u, 71u, 195u, 2u, 80u, 163u, 145u, 229u, 30u, 12u,
9u, 66u, 140u, 71u, 148u, 116u, 48u, 37u, 10u, 41u, 30u, 81u, 192u,
192u, 148u, 40u, 132u, 121u, 70u, 195u, 2u, 80u, 161u, 145u, 229u,
26u, 12u, 9u, 66u, 132u, 71u, 148u, 100u, 48u, 37u, 10u, 9u, 30u, 81u,
128u, 192u, 148u, 40u, 4u, 121u, 69u, 195u, 2u, 80u, 143u, 145u, 229u,
22u, 12u, 9u, 66u, 60u, 71u, 148u, 84u, 48u, 37u, 8u, 233u, 30u, 81u,
64u, 192u, 148u, 35u, 132u, 121u, 68u, 195u, 2u, 80u, 141u, 145u,
229u, 18u, 12u, 9u, 66u, 52u, 71u, 148u, 68u, 48u, 38u, 6u, 164u,
121u, 68u, 3u, 2u, 80u, 140u, 145u, 229u, 14u, 12u, 20u, 41u, 20u,
41u, 33u, 162u, 132u, 100u, 146u, 60u, 161u, 161u, 129u, 52u, 38u,
88u, 143u, 40u, 96u, 96u, 74u, 17u, 146u, 60u, 161u, 1u, 129u, 52u,
37u, 76u, 143u, 40u, 40u, 96u, 161u, 24u, 15u, 148u, 35u, 38u, 132u,
106u, 132u, 110u, 132u, 114u, 132u, 118u, 132u, 122u, 132u, 126u,
133u, 2u, 133u, 6u, 133u, 10u, 133u, 14u, 133u, 18u, 133u, 22u, 133u,
24u, 59u, 208u, 140u, 146u, 71u, 148u, 16u, 48u, 38u, 134u, 192u,
145u, 229u, 3u, 12u, 9u, 66u, 50u, 71u, 148u, 8u, 48u, 37u, 10u, 49u,
30u, 80u, 16u, 192u, 148u, 40u, 164u, 121u, 64u, 3u, 2u, 80u, 162u,
17u, 228u, 223u, 12u, 9u, 66u, 134u, 71u, 147u, 120u, 48u, 37u, 10u,
17u, 30u, 77u, 208u, 192u, 148u, 40u, 36u, 121u, 55u, 3u, 2u, 80u,
160u, 17u, 228u, 219u, 12u, 9u, 66u, 62u, 71u, 147u, 104u, 48u, 37u,
8u, 241u, 30u, 77u, 144u, 192u, 148u, 35u, 164u, 121u, 54u, 3u, 2u,
80u, 142u, 17u, 228u, 215u, 12u, 9u, 66u, 54u, 71u, 147u, 88u, 48u,
37u, 8u, 209u, 30u, 77u, 80u, 192u, 154u, 23u, 38u, 71u, 147u, 80u,
48u, 37u, 8u, 201u, 30u, 77u, 16u, 192u, 40u, 82u, 136u, 77u, 11u,
128u, 36u, 121u, 52u, 3u, 2u, 80u, 165u, 145u, 228u, 205u, 12u, 20u,
35u, 1u, 242u, 132u, 100u, 208u, 141u, 80u, 141u, 208u, 142u, 80u,
142u, 208u, 143u, 80u, 143u, 208u, 160u, 80u, 160u, 208u, 161u, 80u,
161u, 208u, 162u, 80u, 162u, 135u, 122u, 17u, 147u, 66u, 142u, 72u,
242u, 102u, 6u, 4u, 161u, 71u, 35u, 201u, 150u, 24u, 19u, 70u, 7u, 8u,
242u, 101u, 6u, 4u, 161u, 25u, 35u, 201u, 146u, 24u, 18u, 133u, 20u,
143u, 38u, 64u, 96u, 74u, 20u, 66u, 60u, 152u, 225u, 129u, 40u, 80u,
200u, 242u, 99u, 6u, 4u, 161u, 66u, 35u, 201u, 138u, 24u, 18u, 133u,
4u, 143u, 38u, 32u, 96u, 74u, 20u, 2u, 60u, 152u, 97u, 129u, 40u, 71u,
200u, 242u, 97u, 6u, 4u, 161u, 30u, 35u, 201u, 130u, 24u, 18u, 132u,
116u, 143u, 38u, 0u, 96u, 74u, 17u, 194u, 60u, 147u, 225u, 129u, 40u,
70u, 200u, 242u, 79u, 6u, 4u, 161u, 26u, 35u, 201u, 58u, 24u, 19u,
69u, 151u, 136u, 242u, 78u, 6u, 4u, 161u, 25u, 35u, 201u, 50u, 24u,
5u, 10u, 97u, 9u, 162u, 201u, 100u, 143u, 36u, 192u, 96u, 74u, 20u,
178u, 60u, 146u, 161u, 130u, 132u, 96u, 62u, 80u, 140u, 154u, 17u,
170u, 17u, 186u, 17u, 202u, 17u, 218u, 17u, 234u, 17u, 250u, 20u, 10u,
20u, 26u, 20u, 42u, 20u, 58u, 20u, 64u, 239u, 66u, 50u, 104u, 81u,
168u, 81u, 201u, 30u, 73u, 64u, 192u, 148u, 40u, 228u, 121u, 36u,
195u, 2u, 80u, 163u, 17u, 228u, 146u, 12u, 9u, 164u, 90u, 228u, 121u,
36u, 67u, 2u, 80u, 140u, 145u, 228u, 144u, 12u, 9u, 66u, 136u, 71u,
146u, 60u, 48u, 37u, 10u, 25u, 30u, 72u, 224u, 192u, 148u, 40u, 68u,
121u, 35u, 67u, 2u, 80u, 160u, 145u, 228u, 140u, 12u, 9u, 66u, 128u,
71u, 146u, 44u, 48u, 37u, 8u, 249u, 30u, 72u, 160u, 192u, 148u, 35u,
196u, 121u, 34u, 67u, 2u, 80u, 142u, 145u, 228u, 136u, 12u, 9u, 66u,
56u, 71u, 146u, 28u, 48u, 37u, 8u, 217u, 30u, 72u, 96u, 192u, 148u,
35u, 68u, 121u, 33u, 67u, 2u, 105u, 10u, 73u, 30u, 72u, 64u, 192u,
148u, 35u, 36u, 121u, 32u, 67u, 0u, 161u, 77u, 33u, 52u, 132u, 88u,
145u, 228u, 128u, 12u, 9u, 66u, 150u, 71u, 145u, 116u, 48u, 80u, 140u,
7u, 202u, 17u, 147u, 66u, 53u, 66u, 55u, 66u, 57u, 66u, 59u, 66u, 61u,
66u, 63u, 66u, 129u, 66u, 131u, 66u, 133u, 66u, 134u, 29u, 232u, 70u,
77u, 10u, 45u, 10u, 53u, 10u, 57u, 35u, 200u, 184u, 24u, 18u, 133u,
28u, 143u, 34u, 216u, 96u, 74u, 20u, 98u, 60u, 139u, 65u, 129u, 40u,
81u, 72u, 242u, 44u, 134u, 4u, 210u, 217u, 34u, 60u, 139u, 1u, 129u,
40u, 70u, 72u, 242u, 43u, 134u, 4u, 161u, 67u, 35u, 200u, 172u, 24u,
18u, 133u, 8u, 143u, 34u, 168u, 96u, 74u, 20u, 18u, 60u, 138u, 129u,
129u, 40u, 80u, 8u, 242u, 41u, 134u, 4u, 161u, 31u, 35u, 200u, 164u,
24u, 18u, 132u, 120u, 143u, 34u, 136u, 96u, 74u, 17u, 210u, 60u, 138u,
1u, 129u, 40u, 71u, 8u, 242u, 39u, 134u, 4u, 161u, 27u, 35u, 200u,
156u, 24u, 18u, 132u, 104u, 143u, 34u, 104u, 96u, 77u, 44u, 20u, 35u,
200u, 152u, 24u, 18u, 132u, 100u, 143u, 34u, 72u, 96u, 20u, 41u, 196u,
38u, 150u, 0u, 146u, 60u, 137u, 1u, 129u, 40u, 82u, 200u, 242u, 34u,
134u, 10u, 17u, 128u, 249u, 66u, 50u, 104u, 70u, 168u, 70u, 232u, 71u,
40u, 71u, 104u, 71u, 168u, 71u, 232u, 80u, 40u, 80u, 104u, 80u, 131u,
189u, 8u, 201u, 161u, 68u, 161u, 69u, 161u, 70u, 161u, 71u, 36u, 121u,
17u, 3u, 2u, 80u, 163u, 145u, 228u, 67u, 12u, 9u, 66u, 140u, 71u,
145u, 8u, 48u, 37u, 10u, 41u, 30u, 68u, 16u, 192u, 148u, 40u, 132u,
121u, 16u, 3u, 2u, 105u, 194u, 105u, 30u, 65u, 240u, 192u, 148u, 35u,
36u, 121u, 7u, 131u, 2u, 80u, 161u, 17u, 228u, 29u, 12u, 9u, 66u,
130u, 71u, 144u, 112u, 48u, 37u, 10u, 1u, 30u, 65u, 176u, 192u, 148u,
35u, 228u, 121u, 6u, 131u, 2u, 80u, 143u, 17u, 228u, 25u, 12u, 9u,
66u, 58u, 71u, 144u, 96u, 48u, 37u, 8u, 225u, 30u, 65u, 112u, 192u,
148u, 35u, 100u, 121u, 5u, 131u, 2u, 80u, 141u, 17u, 228u, 21u, 12u,
9u, 166u, 219u, 228u, 121u, 5u, 3u, 2u, 80u, 140u, 145u, 228u, 17u,
12u, 2u, 133u, 60u, 132u, 211u, 108u, 194u, 71u, 144u, 64u, 48u, 37u,
10u, 89u, 30u, 64u, 208u, 193u, 66u, 48u, 31u, 40u, 70u, 77u, 8u,
213u, 8u, 221u, 8u, 229u, 8u, 237u, 8u, 245u, 8u, 253u, 10u, 5u, 10u,
8u, 119u, 161u, 25u, 52u, 40u, 116u, 40u, 148u, 40u, 180u, 40u, 212u,
40u, 228u, 143u, 32u, 96u, 96u, 74u, 20u, 114u, 60u, 129u, 97u, 129u,
40u, 81u, 136u, 242u, 5u, 6u, 4u, 161u, 69u, 35u, 200u, 18u, 24u, 18u,
133u, 16u, 143u, 32u, 64u, 96u, 74u, 20u, 50u, 60u, 128u, 225u, 129u,
53u, 12u, 32u, 143u, 32u, 48u, 96u, 74u, 17u, 146u, 60u, 128u, 161u,
129u, 40u, 80u, 72u, 242u, 2u, 6u, 4u, 161u, 64u, 35u, 200u, 6u, 24u,
18u, 132u, 124u, 143u, 32u, 16u, 96u, 74u, 17u, 226u, 60u, 128u, 33u,
129u, 40u, 71u, 72u, 242u, 0u, 6u, 4u, 161u, 28u, 35u, 199u, 190u,
24u, 18u, 132u, 108u, 143u, 30u, 240u, 96u, 74u, 17u, 162u, 60u, 123u,
161u, 129u, 53u, 7u, 40u, 143u, 30u, 224u, 96u, 74u, 17u, 146u, 60u,
123u, 33u, 128u, 80u, 168u, 16u, 154u, 131u, 46u, 72u, 241u, 236u, 6u,
4u, 161u, 75u, 35u, 199u, 170u, 24u, 40u, 70u, 3u, 229u, 8u, 201u,
161u, 26u, 161u, 27u, 161u, 28u, 161u, 29u, 161u, 30u, 161u, 31u,
161u, 64u, 14u, 244u, 35u, 38u, 133u, 10u, 133u, 14u, 133u, 18u, 133u,
22u, 133u, 26u, 133u, 28u, 145u, 227u, 212u, 12u, 9u, 66u, 142u, 71u,
143u, 76u, 48u, 37u, 10u, 49u, 30u, 61u, 32u, 192u, 148u, 40u, 164u,
120u, 244u, 67u, 2u, 80u, 162u, 17u, 227u, 208u, 12u, 9u, 66u, 134u,
71u, 143u, 60u, 48u, 37u, 10u, 17u, 30u, 60u, 224u, 192u, 154u, 155u,
134u, 71u, 143u, 52u, 48u, 37u, 8u, 201u, 30u, 60u, 192u, 192u, 148u,
40u, 4u, 120u, 242u, 195u, 2u, 80u, 143u, 145u, 227u, 202u, 12u, 9u,
66u, 60u, 71u, 143u, 36u, 48u, 37u, 8u, 233u, 30u, 60u, 128u, 192u,
148u, 35u, 132u, 120u, 241u, 195u, 2u, 80u, 141u, 145u, 227u, 198u,
12u, 9u, 66u, 52u, 71u, 143u, 20u, 48u, 38u, 166u, 74u, 145u, 227u,
196u, 12u, 9u, 66u, 50u, 71u, 143u, 4u, 48u, 10u, 21u, 18u, 19u, 83u,
32u, 137u, 30u, 60u, 0u, 192u, 148u, 41u, 100u, 120u, 231u, 67u, 5u,
8u, 192u, 124u, 161u, 25u, 52u, 35u, 84u, 35u, 116u, 35u, 148u, 35u,
180u, 35u, 212u, 35u, 225u, 222u, 132u, 100u, 208u, 160u, 208u, 161u,
80u, 161u, 208u, 162u, 80u, 162u, 208u, 163u, 80u, 163u, 146u, 60u,
115u, 129u, 129u, 40u, 81u, 200u, 241u, 205u, 134u, 4u, 161u, 70u,
35u, 199u, 52u, 24u, 18u, 133u, 20u, 143u, 28u, 200u, 96u, 74u, 20u,
66u, 60u, 115u, 1u, 129u, 40u, 80u, 200u, 241u, 203u, 134u, 4u, 161u,
66u, 35u, 199u, 44u, 24u, 18u, 133u, 4u, 143u, 28u, 168u, 96u, 77u,
88u, 94u, 35u, 199u, 40u, 24u, 18u, 132u, 100u, 143u, 28u, 152u, 96u,
74u, 17u, 242u, 60u, 114u, 65u, 129u, 40u, 71u, 136u, 241u, 200u,
134u, 4u, 161u, 29u, 35u, 199u, 32u, 24u, 18u, 132u, 112u, 143u, 28u,
120u, 96u, 74u, 17u, 178u, 60u, 113u, 193u, 129u, 40u, 70u, 136u,
241u, 198u, 134u, 4u, 213u, 120u, 2u, 60u, 113u, 129u, 129u, 40u, 70u,
72u, 241u, 196u, 134u, 1u, 66u, 164u, 66u, 106u, 186u, 105u, 35u,
199u, 16u, 24u, 18u, 133u, 44u, 143u, 28u, 40u, 96u, 161u, 24u, 15u,
148u, 35u, 38u, 132u, 106u, 132u, 110u, 132u, 114u, 132u, 118u, 132u,
120u, 59u, 208u, 140u, 154u, 20u, 10u, 20u, 26u, 20u, 42u, 20u, 58u,
20u, 74u, 20u, 90u, 20u, 106u, 20u, 114u, 71u, 142u, 16u, 48u, 37u,
10u, 57u, 30u, 56u, 48u, 192u, 148u, 40u, 196u, 120u, 224u, 131u, 2u,
80u, 162u, 145u, 227u, 129u, 12u, 9u, 66u, 136u, 71u, 142u, 0u, 48u,
37u, 10u, 25u, 30u, 53u, 240u, 192u, 148u, 40u, 68u, 120u, 215u, 131u,
2u, 80u, 160u, 145u, 227u, 93u, 12u, 9u, 66u, 128u, 71u, 141u, 112u,
48u, 38u, 177u, 140u, 145u, 227u, 91u, 12u, 9u, 66u, 50u, 71u, 141u,
104u, 48u, 37u, 8u, 241u, 30u, 53u, 144u, 192u, 148u, 35u, 164u, 120u,
214u, 3u, 2u, 80u, 142u, 17u, 227u, 87u, 12u, 9u, 66u, 54u, 71u, 141u,
88u, 48u, 37u, 8u, 209u, 30u, 53u, 80u, 192u, 154u, 196u, 150u, 71u,
141u, 80u, 48u, 37u, 8u, 201u, 30u, 53u, 16u, 192u, 40u, 84u, 200u,
77u, 98u, 24u, 36u, 120u, 212u, 3u, 2u, 80u, 165u, 145u, 227u, 77u,
12u, 20u, 35u, 1u, 242u, 132u, 100u, 208u, 141u, 80u, 141u, 208u,
142u, 80u, 142u, 135u, 122u, 17u, 147u, 66u, 63u, 66u, 129u, 66u,
131u, 66u, 133u, 66u, 135u, 66u, 137u, 66u, 139u, 66u, 141u, 66u,
142u, 72u, 241u, 166u, 6u, 4u, 161u, 71u, 35u, 198u, 150u, 24u, 18u,
133u, 24u, 143u, 26u, 80u, 96u, 74u, 20u, 82u, 60u, 105u, 33u, 129u,
40u, 81u, 8u, 241u, 164u, 6u, 4u, 161u, 67u, 35u, 198u, 142u, 24u,
18u, 133u, 8u, 143u, 26u, 48u, 96u, 74u, 20u, 18u, 60u, 104u, 161u,
129u, 40u, 80u, 8u, 241u, 162u, 6u, 4u, 161u, 31u, 35u, 198u, 134u,
24u, 19u, 91u, 117u, 8u, 241u, 161u, 6u, 4u, 161u, 25u, 35u, 198u,
130u, 24u, 18u, 132u, 116u, 143u, 26u, 0u, 96u, 74u, 17u, 194u, 60u,
99u, 225u, 129u, 40u, 70u, 200u, 241u, 143u, 6u, 4u, 161u, 26u, 35u,
198u, 58u, 24u, 19u, 91u, 69u, 136u, 241u, 142u, 6u, 4u, 161u, 25u,
35u, 198u, 50u, 24u, 5u, 10u, 161u, 9u, 173u, 160u, 100u, 143u, 24u,
192u, 96u, 74u, 20u, 178u, 60u, 98u, 161u, 130u, 132u, 96u, 62u, 80u,
140u, 154u, 17u, 170u, 17u, 186u, 17u, 192u, 239u, 66u, 50u, 104u,
71u, 168u, 71u, 232u, 80u, 40u, 80u, 104u, 80u, 168u, 80u, 232u, 81u,
40u, 81u, 104u, 81u, 168u, 81u, 201u, 30u, 49u, 64u, 192u, 148u, 40u,
228u, 120u, 196u, 195u, 2u, 80u, 163u, 17u, 227u, 18u, 12u, 9u, 66u,
138u, 71u, 140u, 68u, 48u, 37u, 10u, 33u, 30u, 49u, 0u, 192u, 148u,
40u, 100u, 120u, 195u, 195u, 2u, 80u, 161u, 17u, 227u, 14u, 12u, 9u,
66u, 130u, 71u, 140u, 52u, 48u, 37u, 10u, 1u, 30u, 48u, 192u, 192u,
148u, 35u, 228u, 120u, 194u, 195u, 2u, 80u, 143u, 17u, 227u, 10u, 12u,
9u, 175u, 17u, 228u, 120u, 194u, 67u, 2u, 80u, 140u, 145u, 227u, 8u,
12u, 9u, 66u, 56u, 71u, 140u, 28u, 48u, 37u, 8u, 217u, 30u, 48u, 96u,
192u, 148u, 35u, 68u, 120u, 193u, 67u, 2u, 107u, 192u, 9u, 30u, 48u,
64u, 192u, 148u, 35u, 36u, 120u, 192u, 67u, 0u, 161u, 85u, 33u, 53u,
223u, 56u, 145u, 227u, 0u, 12u, 9u, 66u, 150u, 71u, 139u, 116u, 48u,
80u, 140u, 7u, 202u, 17u, 147u, 66u, 53u, 66u, 54u, 29u, 232u, 70u,
77u, 8u, 237u, 8u, 245u, 8u, 253u, 10u, 5u, 10u, 13u, 10u, 21u, 10u,
29u, 10u, 37u, 10u, 45u, 10u, 53u, 10u, 57u, 35u, 197u, 184u, 24u,
18u, 133u, 28u, 143u, 22u, 216u, 96u, 74u, 20u, 98u, 60u, 91u, 65u,
129u, 40u, 81u, 72u, 241u, 108u, 134u, 4u, 161u, 68u, 35u, 197u, 176u,
24u, 18u, 133u, 12u, 143u, 22u, 184u, 96u, 74u, 20u, 34u, 60u, 90u,
193u, 129u, 40u, 80u, 72u, 241u, 106u, 134u, 4u, 161u, 64u, 35u, 197u,
168u, 24u, 18u, 132u, 124u, 143u, 22u, 152u, 96u, 74u, 17u, 226u, 60u,
90u, 65u, 129u, 40u, 71u, 72u, 241u, 104u, 134u, 4u, 216u, 52u, 162u,
60u, 90u, 1u, 129u, 40u, 70u, 72u, 241u, 103u, 134u, 4u, 161u, 27u,
35u, 197u, 156u, 24u, 18u, 132u, 104u, 143u, 22u, 104u, 96u, 77u,
130u, 204u, 35u, 197u, 152u, 24u, 18u, 132u, 100u, 143u, 22u, 72u,
96u, 20u, 42u, 196u, 38u, 193u, 76u, 146u, 60u, 89u, 1u, 129u, 40u,
82u, 200u, 241u, 98u, 134u, 10u, 17u, 128u, 249u, 66u, 50u, 104u, 70u,
131u, 189u, 8u, 201u, 161u, 28u, 161u, 29u, 161u, 30u, 161u, 31u,
161u, 64u, 161u, 65u, 161u, 66u, 161u, 67u, 161u, 68u, 161u, 69u,
161u, 70u, 161u, 71u, 36u, 120u, 177u, 3u, 2u, 80u, 163u, 145u, 226u,
195u, 12u, 9u, 66u, 140u, 71u, 139u, 8u, 48u, 37u, 10u, 41u, 30u, 44u,
16u, 192u, 148u, 40u, 132u, 120u, 176u, 3u, 2u, 80u, 161u, 145u, 226u,
159u, 12u, 9u, 66u, 132u, 71u, 138u, 120u, 48u, 37u, 10u, 9u, 30u,
41u, 208u, 192u, 148u, 40u, 4u, 120u, 167u, 3u, 2u, 80u, 143u, 145u,
226u, 155u, 12u, 9u, 66u, 60u, 71u, 138u, 104u, 48u, 37u, 8u, 233u,
30u, 41u, 144u, 192u, 148u, 35u, 132u, 120u, 166u, 3u, 2u, 108u, 112u,
41u, 30u, 41u, 112u, 192u, 148u, 35u, 36u, 120u, 165u, 131u, 2u, 80u,
141u, 17u, 226u, 149u, 12u, 9u, 177u, 178u, 228u, 120u, 165u, 3u, 2u,
80u, 140u, 145u, 226u, 145u, 12u, 2u, 133u, 92u, 132u, 216u, 216u,
66u, 71u, 138u, 64u, 48u, 37u, 10u, 89u, 30u, 40u, 208u, 193u, 66u,
48u, 31u, 40u, 70u, 72u, 119u, 161u, 25u, 52u, 35u, 116u, 35u, 148u,
35u, 180u, 35u, 212u, 35u, 244u, 40u, 20u, 40u, 52u, 40u, 84u, 40u,
116u, 40u, 148u, 40u, 180u, 40u, 212u, 40u, 228u, 143u, 20u, 96u, 96u,
74u, 20u, 114u, 60u, 81u, 97u, 129u, 40u, 81u, 136u, 241u, 69u, 6u,
4u, 161u, 69u, 35u, 197u, 18u, 24u, 18u, 133u, 16u, 143u, 20u, 64u,
96u, 74u, 20u, 50u, 60u, 80u, 225u, 129u, 40u, 80u, 136u, 241u, 67u,
6u, 4u, 161u, 65u, 35u, 197u, 10u, 24u, 18u, 133u, 0u, 143u, 20u, 32u,
96u, 74u, 17u, 242u, 60u, 80u, 97u, 129u, 40u, 71u, 136u, 241u, 65u,
6u, 4u, 161u, 29u, 35u, 197u, 2u, 24u, 18u, 132u, 112u, 143u, 20u, 0u,
96u, 74u, 17u, 178u, 60u, 75u, 225u, 129u, 54u, 99u, 0u, 143u, 18u,
240u, 96u, 74u, 17u, 146u, 60u, 75u, 161u, 129u, 54u, 98u, 8u, 143u,
18u, 224u, 96u, 74u, 17u, 146u, 60u, 75u, 33u, 128u, 80u, 172u, 16u,
155u, 48u, 158u, 72u, 241u, 44u, 6u, 4u, 161u, 75u, 35u, 196u, 174u,
24u, 40u, 86u, 69u, 10u, 208u, 104u, 161u, 25u, 36u, 38u, 209u, 7u,
4u, 217u, 116u, 48u, 155u, 24u, 176u, 19u, 96u, 99u, 66u, 107u, 182u,
16u, 77u, 107u, 215u, 9u, 172u, 33u, 129u, 53u, 89u, 4u, 38u, 165u,
171u, 4u, 212u, 8u, 176u, 154u, 107u, 128u, 19u, 74u, 181u, 66u, 105u,
0u, 80u, 77u, 21u, 31u, 13u, 33u, 232u, 38u, 132u, 169u, 132u, 192u,
136u, 146u, 71u, 137u, 84u, 48u, 38u, 209u, 166u, 145u, 226u, 84u,
12u, 9u, 180u, 112u, 4u, 120u, 148u, 195u, 2u, 80u, 140u, 145u, 226u,
81u, 12u, 9u, 180u, 105u, 164u, 120u, 147u, 131u, 3u, 61u, 10u, 221u,
8u, 201u, 35u, 196u, 154u, 24u, 18u, 132u, 100u, 143u, 18u, 72u, 96u,
107u, 13u, 84u, 35u, 36u, 145u, 226u, 72u, 12u, 9u, 181u, 75u, 100u,
120u, 145u, 195u, 2u, 80u, 140u, 145u, 226u, 68u, 12u, 17u, 37u, 10u,
229u, 10u, 192u, 161u, 93u, 161u, 94u, 161u, 95u, 52u, 42u, 226u,
133u, 118u, 134u, 2u, 134u, 4u, 208u, 171u, 10u, 21u, 218u, 24u, 42u,
24u, 51u, 66u, 170u, 40u, 87u, 104u, 97u, 40u, 97u, 77u, 10u, 160u,
161u, 93u, 161u, 134u, 161u, 135u, 52u, 42u, 98u, 133u, 118u, 134u,
34u, 134u, 36u, 208u, 168u, 208u, 197u, 80u, 169u, 80u, 197u, 146u,
60u, 72u, 97u, 129u, 40u, 98u, 200u, 241u, 32u, 6u, 4u, 161u, 138u,
35u, 196u, 56u, 24u, 18u, 134u, 36u, 143u, 16u, 200u, 96u, 74u, 24u,
130u, 60u, 66u, 225u, 129u, 148u, 80u, 198u, 80u, 198u, 154u, 24u,
234u, 24u, 210u, 71u, 136u, 84u, 48u, 38u, 216u, 232u, 145u, 226u,
20u, 12u, 9u, 67u, 18u, 71u, 136u, 72u, 48u, 38u, 216u, 232u, 145u,
226u, 14u, 12u, 9u, 67u, 14u, 71u, 136u, 44u, 48u, 37u, 12u, 49u, 30u,
32u, 144u, 192u, 155u, 99u, 140u, 71u, 136u, 28u, 48u, 38u, 216u,
232u, 145u, 226u, 6u, 12u, 9u, 67u, 14u, 71u, 136u, 16u, 48u, 38u,
216u, 232u, 145u, 226u, 0u, 12u, 9u, 67u, 10u, 71u, 135u, 116u, 48u,
37u, 12u, 33u, 30u, 29u, 176u, 192u, 155u, 99u, 140u, 71u, 135u, 100u,
48u, 38u, 216u, 232u, 145u, 225u, 216u, 12u, 9u, 67u, 10u, 71u, 135u,
88u, 48u, 38u, 216u, 232u, 145u, 225u, 210u, 12u, 9u, 67u, 6u, 71u,
135u, 60u, 48u, 37u, 12u, 17u, 30u, 28u, 208u, 192u, 155u, 99u, 140u,
71u, 135u, 44u, 48u, 38u, 216u, 232u, 145u, 225u, 202u, 12u, 9u, 67u,
6u, 71u, 135u, 32u, 48u, 38u, 216u, 232u, 145u, 225u, 196u, 12u, 9u,
67u, 2u, 71u, 135u, 4u, 48u, 37u, 12u, 1u, 30u, 25u, 240u, 192u, 155u,
99u, 140u, 71u, 134u, 116u, 48u, 38u, 216u, 232u, 145u, 225u, 156u,
12u, 9u, 67u, 2u, 71u, 134u, 104u, 48u, 38u, 216u, 232u, 145u, 225u,
150u, 12u, 9u, 66u, 190u, 71u, 134u, 76u, 48u, 37u, 10u, 241u, 30u,
25u, 16u, 192u, 155u, 99u, 140u, 71u, 134u, 60u, 48u, 38u, 216u, 232u,
145u, 225u, 142u, 12u, 9u, 66u, 190u, 71u, 134u, 48u, 48u, 38u, 216u,
232u, 145u, 225u, 136u, 12u, 9u, 66u, 184u, 71u, 134u, 24u, 48u, 37u,
10u, 57u, 30u, 24u, 16u, 192u, 202u, 40u, 99u, 40u, 81u, 204u, 255u,
66u, 142u, 72u, 240u, 175u, 134u, 4u, 220u, 32u, 162u, 60u, 43u, 193u,
129u, 40u, 81u, 200u, 240u, 174u, 6u, 4u, 220u, 32u, 162u, 60u, 43u,
65u, 129u, 40u, 81u, 136u, 240u, 170u, 134u, 6u, 81u, 67u, 25u, 66u,
140u, 103u, 250u, 20u, 98u, 71u, 133u, 76u, 48u, 38u, 226u, 75u, 145u,
225u, 82u, 12u, 9u, 66u, 140u, 71u, 133u, 64u, 48u, 38u, 226u, 75u,
145u, 225u, 78u, 12u, 9u, 66u, 138u, 71u, 133u, 36u, 48u, 50u, 138u,
24u, 202u, 20u, 83u, 63u, 208u, 162u, 146u, 60u, 40u, 225u, 129u, 55u,
29u, 16u, 143u, 10u, 48u, 96u, 74u, 20u, 82u, 60u, 40u, 129u, 129u,
55u, 29u, 16u, 143u, 10u, 16u, 96u, 74u, 20u, 66u, 60u, 35u, 161u,
129u, 148u, 80u, 198u, 80u, 162u, 25u, 254u, 133u, 16u, 145u, 225u,
27u, 12u, 9u, 185u, 58u, 36u, 120u, 70u, 131u, 2u, 80u, 162u, 17u,
225u, 24u, 12u, 9u, 185u, 58u, 36u, 120u, 69u, 131u, 2u, 80u, 161u,
145u, 225u, 17u, 12u, 12u, 162u, 134u, 50u, 133u, 12u, 207u, 244u,
40u, 100u, 143u, 8u, 120u, 96u, 77u, 204u, 94u, 35u, 194u, 28u, 24u,
18u, 133u, 12u, 143u, 8u, 96u, 96u, 77u, 204u, 94u, 35u, 194u, 20u,
24u, 18u, 133u, 8u, 143u, 8u, 40u, 96u, 101u, 20u, 49u, 148u, 40u,
70u, 127u, 161u, 66u, 36u, 120u, 64u, 195u, 2u, 110u, 120u, 89u, 30u,
16u, 32u, 192u, 148u, 40u, 68u, 120u, 64u, 3u, 2u, 110u, 120u, 89u,
29u, 248u, 48u, 37u, 10u, 9u, 29u, 228u, 48u, 50u, 138u, 24u, 202u,
20u, 19u, 63u, 208u, 160u, 146u, 59u, 184u, 96u, 77u, 209u, 140u, 35u,
187u, 6u, 4u, 161u, 65u, 35u, 186u, 6u, 4u, 221u, 24u, 194u, 59u,
144u, 96u, 74u, 20u, 2u, 59u, 104u, 96u, 101u, 20u, 49u, 148u, 40u,
6u, 127u, 161u, 64u, 36u, 118u, 176u, 192u, 155u, 167u, 182u, 71u,
106u, 12u, 9u, 66u, 128u, 71u, 104u, 12u, 9u, 186u, 123u, 100u, 118u,
96u, 192u, 148u, 35u, 228u, 118u, 16u, 192u, 202u, 40u, 99u, 40u, 71u,
204u, 255u, 66u, 62u, 72u, 235u, 225u, 129u, 55u, 89u, 40u, 142u,
188u, 24u, 18u, 132u, 124u, 142u, 184u, 24u, 19u, 117u, 146u, 136u,
235u, 65u, 129u, 40u, 71u, 136u, 234u, 161u, 129u, 148u, 80u, 198u,
80u, 143u, 25u, 254u, 132u, 120u, 145u, 212u, 195u, 2u, 110u, 196u,
201u, 29u, 72u, 48u, 37u, 8u, 241u, 29u, 64u, 48u, 38u, 236u, 76u,
145u, 211u, 131u, 2u, 80u, 142u, 145u, 210u, 67u, 3u, 40u, 161u, 140u,
161u, 29u, 51u, 253u, 8u, 233u, 35u, 163u, 134u, 4u, 221u, 176u, 130u,
58u, 48u, 96u, 74u, 17u, 210u, 58u, 32u, 96u, 77u, 219u, 8u, 35u,
161u, 6u, 4u, 161u, 28u, 35u, 158u, 134u, 6u, 81u, 67u, 25u, 66u, 56u,
103u, 250u, 17u, 194u, 71u, 59u, 12u, 9u, 187u, 170u, 228u, 115u,
160u, 192u, 148u, 35u, 132u, 115u, 128u, 192u, 155u, 186u, 174u, 71u,
54u, 12u, 9u, 66u, 54u, 71u, 49u, 12u, 12u, 162u, 134u, 50u, 132u,
108u, 207u, 244u, 35u, 100u, 142u, 94u, 24u, 19u, 119u, 241u, 136u,
229u, 193u, 129u, 40u, 70u, 200u, 229u, 129u, 129u, 55u, 127u, 24u,
142u, 84u, 24u, 18u, 132u, 104u, 142u, 74u, 24u, 25u, 69u, 12u, 101u,
8u, 209u, 159u, 232u, 70u, 137u, 28u, 140u, 48u, 38u, 241u, 10u, 145u,
200u, 131u, 2u, 80u, 141u, 17u, 200u, 3u, 2u, 111u, 16u, 169u, 28u,
120u, 48u, 53u, 208u, 140u, 146u, 56u, 232u, 96u, 74u, 17u, 146u, 56u,
0u, 96u, 74u, 24u, 242u, 24u, 131u, 3u, 24u, 161u, 144u, 161u, 94u,
161u, 128u, 161u, 130u, 161u, 132u, 161u, 134u, 161u, 136u, 161u,
139u, 161u, 138u, 161u, 95u, 161u, 129u, 161u, 131u, 161u, 133u, 161u,
135u, 161u, 137u, 33u, 152u, 80u, 174u, 208u, 199u, 80u, 198u, 144u,
155u, 99u, 140u, 66u, 109u, 94u, 89u, 36u,};
static unsigned char uvector__00047[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 67u, 35u, 67u, 33u, 66u, 189u, 67u, 1u,
67u, 5u, 67u, 9u, 67u, 13u, 67u, 17u, 67u, 23u, 67u, 21u, 66u, 191u,
67u, 3u, 67u, 7u, 67u, 11u, 67u, 15u, 67u, 18u, 67u, 48u, 161u, 93u,
161u, 142u, 161u, 141u, 33u, 148u, 80u, 198u, 80u, 198u, 154u, 24u,
234u, 24u, 210u, 68u, 73u, 66u, 185u, 66u, 176u, 40u, 87u, 104u, 87u,
168u, 87u, 205u, 10u, 184u, 161u, 93u, 161u, 128u, 161u, 129u, 52u,
42u, 194u, 133u, 118u, 134u, 10u, 134u, 12u, 208u, 170u, 138u, 21u,
218u, 24u, 74u, 24u, 83u, 66u, 168u, 40u, 87u, 104u, 97u, 168u, 97u,
205u, 10u, 152u, 161u, 93u, 161u, 136u, 161u, 137u, 52u, 42u, 52u,
49u, 84u, 42u, 84u, 49u, 100u, 145u, 194u, 67u, 3u, 24u, 76u, 13u,
211u, 18u, 162u, 56u, 56u, 96u, 76u, 7u, 8u, 224u, 65u, 129u, 48u,
28u, 35u, 128u, 6u, 4u, 192u, 112u, 144u,};
static unsigned char uvector__00048[] = {
 0u, 3u, 205u, 18u, 24u, 40u, 100u, 131u, 229u, 12u, 153u, 161u, 148u,
161u, 149u, 161u, 150u, 161u, 151u, 161u, 152u, 161u, 153u, 161u,
154u, 161u, 155u, 161u, 156u, 161u, 157u, 161u, 158u, 161u, 159u,
161u, 192u, 161u, 193u, 36u, 121u, 162u, 3u, 2u, 80u, 224u, 145u,
230u, 135u, 12u, 9u, 67u, 128u, 71u, 154u, 24u, 48u, 37u, 12u, 249u,
30u, 104u, 80u, 192u, 148u, 51u, 196u, 121u, 161u, 3u, 2u, 80u, 206u,
145u, 230u, 131u, 12u, 9u, 67u, 56u, 71u, 154u, 8u, 48u, 37u, 12u,
217u, 30u, 104u, 16u, 192u, 148u, 51u, 68u, 121u, 160u, 3u, 2u, 80u,
204u, 145u, 230u, 95u, 12u, 9u, 67u, 48u, 71u, 153u, 120u, 48u, 37u,
12u, 185u, 30u, 101u, 208u, 192u, 148u, 50u, 196u, 121u, 151u, 3u, 2u,
80u, 202u, 145u, 230u, 91u, 12u, 9u, 67u, 40u, 71u, 153u, 104u, 48u,
38u, 6u, 164u, 121u, 150u, 67u, 2u, 80u, 201u, 145u, 230u, 87u, 12u,
20u, 41u, 20u, 41u, 33u, 162u, 134u, 76u, 146u, 60u, 202u, 193u, 129u,
52u, 38u, 88u, 143u, 50u, 168u, 96u, 74u, 25u, 50u, 60u, 202u, 33u,
129u, 52u, 37u, 76u, 143u, 50u, 112u, 96u, 161u, 146u, 15u, 148u, 50u,
102u, 134u, 82u, 134u, 86u, 134u, 90u, 134u, 94u, 134u, 98u, 134u,
102u, 134u, 106u, 134u, 110u, 134u, 114u, 134u, 118u, 134u, 122u,
134u, 126u, 135u, 0u, 59u, 208u, 201u, 146u, 71u, 153u, 52u, 48u, 38u,
134u, 192u, 145u, 230u, 76u, 12u, 9u, 67u, 38u, 71u, 153u, 44u, 48u,
37u, 14u, 1u, 30u, 100u, 160u, 192u, 148u, 51u, 228u, 121u, 146u, 67u,
2u, 80u, 207u, 17u, 230u, 72u, 12u, 9u, 67u, 58u, 71u, 153u, 28u, 48u,
37u, 12u, 225u, 30u, 100u, 96u, 192u, 148u, 51u, 100u, 121u, 145u,
67u, 2u, 80u, 205u, 17u, 230u, 68u, 12u, 9u, 67u, 50u, 71u, 153u, 12u,
48u, 37u, 12u, 193u, 30u, 100u, 32u, 192u, 148u, 50u, 228u, 121u,
144u, 67u, 2u, 80u, 203u, 17u, 230u, 64u, 12u, 9u, 67u, 42u, 71u,
152u, 124u, 48u, 37u, 12u, 161u, 30u, 97u, 224u, 192u, 154u, 23u, 38u,
71u, 152u, 116u, 48u, 37u, 12u, 153u, 30u, 97u, 160u, 192u, 40u, 82u,
136u, 77u, 11u, 128u, 36u, 121u, 134u, 67u, 2u, 80u, 165u, 145u, 230u,
22u, 12u, 20u, 50u, 65u, 242u, 134u, 76u, 208u, 202u, 80u, 202u, 208u,
203u, 80u, 203u, 208u, 204u, 80u, 204u, 208u, 205u, 80u, 205u, 208u,
206u, 80u, 206u, 208u, 207u, 80u, 207u, 135u, 122u, 25u, 51u, 67u,
130u, 72u, 243u, 10u, 134u, 4u, 161u, 193u, 35u, 204u, 40u, 24u, 19u,
70u, 7u, 8u, 243u, 9u, 134u, 4u, 161u, 147u, 35u, 204u, 36u, 24u, 18u,
134u, 124u, 143u, 48u, 136u, 96u, 74u, 25u, 226u, 60u, 194u, 1u, 129u,
40u, 103u, 72u, 243u, 7u, 134u, 4u, 161u, 156u, 35u, 204u, 28u, 24u,
18u, 134u, 108u, 143u, 48u, 104u, 96u, 74u, 25u, 162u, 60u, 193u,
129u, 129u, 40u, 102u, 72u, 243u, 5u, 134u, 4u, 161u, 152u, 35u, 204u,
20u, 24u, 18u, 134u, 92u, 143u, 48u, 72u, 96u, 74u, 25u, 98u, 60u,
193u, 1u, 129u, 40u, 101u, 72u, 243u, 3u, 134u, 4u, 161u, 148u, 35u,
204u, 12u, 24u, 19u, 69u, 151u, 136u, 243u, 2u, 134u, 4u, 161u, 147u,
35u, 204u, 4u, 24u, 5u, 10u, 97u, 9u, 162u, 201u, 100u, 143u, 48u, 8u,
96u, 74u, 20u, 178u, 60u, 187u, 193u, 130u, 134u, 72u, 62u, 80u, 201u,
154u, 25u, 74u, 25u, 90u, 25u, 106u, 25u, 122u, 25u, 138u, 25u, 154u,
25u, 170u, 25u, 186u, 25u, 202u, 25u, 218u, 25u, 224u, 239u, 67u, 38u,
104u, 112u, 40u, 112u, 73u, 30u, 93u, 208u, 192u, 148u, 56u, 36u,
121u, 119u, 3u, 2u, 80u, 224u, 17u, 229u, 219u, 12u, 9u, 164u, 90u,
228u, 121u, 118u, 131u, 2u, 80u, 201u, 145u, 229u, 217u, 12u, 9u, 67u,
60u, 71u, 151u, 96u, 48u, 37u, 12u, 233u, 30u, 93u, 112u, 192u, 148u,
51u, 132u, 121u, 117u, 131u, 2u, 80u, 205u, 145u, 229u, 213u, 12u, 9u,
67u, 52u, 71u, 151u, 80u, 48u, 37u, 12u, 201u, 30u, 93u, 48u, 192u,
148u, 51u, 4u, 121u, 116u, 131u, 2u, 80u, 203u, 145u, 229u, 209u, 12u,
9u, 67u, 44u, 71u, 151u, 64u, 48u, 37u, 12u, 169u, 30u, 92u, 240u,
192u, 148u, 50u, 132u, 121u, 115u, 131u, 2u, 105u, 10u, 73u, 30u, 92u,
208u, 192u, 148u, 50u, 100u, 121u, 114u, 131u, 0u, 161u, 77u, 33u,
52u, 132u, 88u, 145u, 229u, 201u, 12u, 9u, 66u, 150u, 71u, 151u, 24u,
48u, 80u, 201u, 7u, 202u, 25u, 51u, 67u, 41u, 67u, 43u, 67u, 45u, 67u,
47u, 67u, 49u, 67u, 51u, 67u, 53u, 67u, 55u, 67u, 57u, 67u, 58u, 29u,
232u, 100u, 205u, 12u, 253u, 14u, 5u, 14u, 9u, 35u, 203u, 138u, 24u,
18u, 135u, 4u, 143u, 46u, 32u, 96u, 74u, 28u, 2u, 60u, 184u, 97u,
129u, 40u, 103u, 200u, 242u, 225u, 6u, 4u, 210u, 217u, 34u, 60u, 184u,
33u, 129u, 40u, 100u, 200u, 242u, 224u, 6u, 4u, 161u, 157u, 35u, 203u,
62u, 24u, 18u, 134u, 112u, 143u, 44u, 240u, 96u, 74u, 25u, 178u, 60u,
179u, 161u, 129u, 40u, 102u, 136u, 242u, 206u, 6u, 4u, 161u, 153u,
35u, 203u, 54u, 24u, 18u, 134u, 96u, 143u, 44u, 208u, 96u, 74u, 25u,
114u, 60u, 179u, 33u, 129u, 40u, 101u, 136u, 242u, 204u, 6u, 4u, 161u,
149u, 35u, 203u, 46u, 24u, 18u, 134u, 80u, 143u, 44u, 176u, 96u, 77u,
44u, 20u, 35u, 203u, 42u, 24u, 18u, 134u, 76u, 143u, 44u, 144u, 96u,
20u, 41u, 196u, 38u, 150u, 0u, 146u, 60u, 178u, 33u, 129u, 40u, 82u,
200u, 242u, 199u, 6u, 10u, 25u, 32u, 249u, 67u, 38u, 104u, 101u, 40u,
101u, 104u, 101u, 168u, 101u, 232u, 102u, 40u, 102u, 104u, 102u, 168u,
102u, 232u, 103u, 3u, 189u, 12u, 153u, 161u, 158u, 161u, 159u, 161u,
192u, 161u, 193u, 36u, 121u, 99u, 67u, 2u, 80u, 224u, 145u, 229u,
140u, 12u, 9u, 67u, 128u, 71u, 150u, 44u, 48u, 37u, 12u, 249u, 30u,
88u, 160u, 192u, 148u, 51u, 196u, 121u, 98u, 67u, 2u, 105u, 194u,
105u, 30u, 88u, 128u, 192u, 148u, 50u, 100u, 121u, 97u, 195u, 2u, 80u,
206u, 17u, 229u, 134u, 12u, 9u, 67u, 54u, 71u, 150u, 20u, 48u, 37u,
12u, 209u, 30u, 88u, 64u, 192u, 148u, 51u, 36u, 121u, 96u, 195u, 2u,
80u, 204u, 17u, 229u, 130u, 12u, 9u, 67u, 46u, 71u, 150u, 4u, 48u,
37u, 12u, 177u, 30u, 88u, 0u, 192u, 148u, 50u, 164u, 121u, 87u, 195u,
2u, 80u, 202u, 17u, 229u, 94u, 12u, 9u, 166u, 219u, 228u, 121u, 87u,
67u, 2u, 80u, 201u, 145u, 229u, 90u, 12u, 2u, 133u, 60u, 132u, 211u,
108u, 194u, 71u, 149u, 100u, 48u, 37u, 10u, 89u, 30u, 85u, 96u, 193u,
67u, 36u, 31u, 40u, 100u, 205u, 12u, 165u, 12u, 173u, 12u, 181u, 12u,
189u, 12u, 197u, 12u, 205u, 12u, 213u, 12u, 216u, 119u, 161u, 147u,
52u, 51u, 180u, 51u, 212u, 51u, 244u, 56u, 20u, 56u, 36u, 143u, 42u,
168u, 96u, 74u, 28u, 18u, 60u, 170u, 129u, 129u, 40u, 112u, 8u, 242u,
169u, 134u, 4u, 161u, 159u, 35u, 202u, 164u, 24u, 18u, 134u, 120u,
143u, 42u, 136u, 96u, 74u, 25u, 210u, 60u, 170u, 1u, 129u, 53u, 12u,
32u, 143u, 42u, 120u, 96u, 74u, 25u, 50u, 60u, 169u, 193u, 129u, 40u,
102u, 200u, 242u, 166u, 134u, 4u, 161u, 154u, 35u, 202u, 152u, 24u,
18u, 134u, 100u, 143u, 42u, 88u, 96u, 74u, 25u, 130u, 60u, 169u, 65u,
129u, 40u, 101u, 200u, 242u, 164u, 134u, 4u, 161u, 150u, 35u, 202u,
144u, 24u, 18u, 134u, 84u, 143u, 42u, 56u, 96u, 74u, 25u, 66u, 60u,
168u, 193u, 129u, 53u, 7u, 40u, 143u, 42u, 40u, 96u, 74u, 25u, 50u,
60u, 168u, 65u, 128u, 80u, 168u, 16u, 154u, 131u, 46u, 72u, 242u,
160u, 134u, 4u, 161u, 75u, 35u, 202u, 60u, 24u, 40u, 100u, 131u, 229u,
12u, 153u, 161u, 148u, 161u, 149u, 161u, 150u, 161u, 151u, 161u, 152u,
161u, 153u, 161u, 154u, 14u, 244u, 50u, 102u, 134u, 114u, 134u, 118u,
134u, 122u, 134u, 126u, 135u, 2u, 135u, 4u, 145u, 229u, 29u, 12u, 9u,
67u, 130u, 71u, 148u, 112u, 48u, 37u, 14u, 1u, 30u, 81u, 176u, 192u,
148u, 51u, 228u, 121u, 70u, 131u, 2u, 80u, 207u, 17u, 229u, 25u, 12u,
9u, 67u, 58u, 71u, 148u, 96u, 48u, 37u, 12u, 225u, 30u, 81u, 112u,
192u, 154u, 155u, 134u, 71u, 148u, 88u, 48u, 37u, 12u, 153u, 30u, 81u,
80u, 192u, 148u, 51u, 68u, 121u, 69u, 3u, 2u, 80u, 204u, 145u, 229u,
19u, 12u, 9u, 67u, 48u, 71u, 148u, 72u, 48u, 37u, 12u, 185u, 30u, 81u,
16u, 192u, 148u, 50u, 196u, 121u, 68u, 3u, 2u, 80u, 202u, 145u, 229u,
15u, 12u, 9u, 67u, 40u, 71u, 148u, 56u, 48u, 38u, 166u, 74u, 145u,
229u, 13u, 12u, 9u, 67u, 38u, 71u, 148u, 40u, 48u, 10u, 21u, 18u, 19u,
83u, 32u, 137u, 30u, 80u, 144u, 192u, 148u, 41u, 100u, 121u, 65u,
131u, 5u, 12u, 144u, 124u, 161u, 147u, 52u, 50u, 148u, 50u, 180u, 50u,
212u, 50u, 244u, 51u, 20u, 51u, 33u, 222u, 134u, 76u, 208u, 205u,
208u, 206u, 80u, 206u, 208u, 207u, 80u, 207u, 208u, 224u, 80u, 224u,
146u, 60u, 160u, 161u, 129u, 40u, 112u, 72u, 242u, 130u, 6u, 4u, 161u,
192u, 35u, 202u, 6u, 24u, 18u, 134u, 124u, 143u, 40u, 16u, 96u, 74u,
25u, 226u, 60u, 160u, 33u, 129u, 40u, 103u, 72u, 242u, 128u, 6u, 4u,
161u, 156u, 35u, 201u, 190u, 24u, 18u, 134u, 108u, 143u, 38u, 240u,
96u, 77u, 88u, 94u, 35u, 201u, 186u, 24u, 18u, 134u, 76u, 143u, 38u,
224u, 96u, 74u, 25u, 146u, 60u, 155u, 97u, 129u, 40u, 102u, 8u, 242u,
109u, 6u, 4u, 161u, 151u, 35u, 201u, 178u, 24u, 18u, 134u, 88u, 143u,
38u, 192u, 96u, 74u, 25u, 82u, 60u, 154u, 225u, 129u, 40u, 101u, 8u,
242u, 107u, 6u, 4u, 213u, 120u, 2u, 60u, 154u, 161u, 129u, 40u, 100u,
200u, 242u, 105u, 6u, 1u, 66u, 164u, 66u, 106u, 186u, 105u, 35u, 201u,
162u, 24u, 18u, 133u, 44u, 143u, 38u, 112u, 96u, 161u, 146u, 15u,
148u, 50u, 102u, 134u, 82u, 134u, 86u, 134u, 90u, 134u, 94u, 134u,
96u, 59u, 208u, 201u, 154u, 25u, 170u, 25u, 186u, 25u, 202u, 25u,
218u, 25u, 234u, 25u, 250u, 28u, 10u, 28u, 18u, 71u, 147u, 52u, 48u,
37u, 14u, 9u, 30u, 76u, 192u, 192u, 148u, 56u, 4u, 121u, 50u, 195u,
2u, 80u, 207u, 145u, 228u, 202u, 12u, 9u, 67u, 60u, 71u, 147u, 36u,
48u, 37u, 12u, 233u, 30u, 76u, 128u, 192u, 148u, 51u, 132u, 121u, 49u,
195u, 2u, 80u, 205u, 145u, 228u, 198u, 12u, 9u, 67u, 52u, 71u, 147u,
20u, 48u, 38u, 177u, 140u, 145u, 228u, 196u, 12u, 9u, 67u, 38u, 71u,
147u, 12u, 48u, 37u, 12u, 193u, 30u, 76u, 32u, 192u, 148u, 50u, 228u,
121u, 48u, 67u, 2u, 80u, 203u, 17u, 228u, 192u, 12u, 9u, 67u, 42u,
71u, 146u, 124u, 48u, 37u, 12u, 161u, 30u, 73u, 224u, 192u, 154u,
196u, 150u, 71u, 146u, 116u, 48u, 37u, 12u, 153u, 30u, 73u, 160u,
192u, 40u, 84u, 200u, 77u, 98u, 24u, 36u, 121u, 38u, 67u, 2u, 80u,
165u, 145u, 228u, 150u, 12u, 20u, 50u, 65u, 242u, 134u, 76u, 208u,
202u, 80u, 202u, 208u, 203u, 80u, 203u, 135u, 122u, 25u, 51u, 67u,
51u, 67u, 53u, 67u, 55u, 67u, 57u, 67u, 59u, 67u, 61u, 67u, 63u, 67u,
129u, 67u, 130u, 72u, 242u, 74u, 134u, 4u, 161u, 193u, 35u, 201u, 40u,
24u, 18u, 135u, 0u, 143u, 36u, 152u, 96u, 74u, 25u, 242u, 60u, 146u,
65u, 129u, 40u, 103u, 136u, 242u, 72u, 134u, 4u, 161u, 157u, 35u,
201u, 32u, 24u, 18u, 134u, 112u, 143u, 36u, 120u, 96u, 74u, 25u, 178u,
60u, 145u, 193u, 129u, 40u, 102u, 136u, 242u, 70u, 134u, 4u, 161u,
153u, 35u, 201u, 24u, 24u, 19u, 91u, 117u, 8u, 242u, 69u, 134u, 4u,
161u, 147u, 35u, 201u, 20u, 24u, 18u, 134u, 92u, 143u, 36u, 72u, 96u,
74u, 25u, 98u, 60u, 145u, 1u, 129u, 40u, 101u, 72u, 242u, 67u, 134u,
4u, 161u, 148u, 35u, 201u, 12u, 24u, 19u, 91u, 69u, 136u, 242u, 66u,
134u, 4u, 161u, 147u, 35u, 201u, 4u, 24u, 5u, 10u, 161u, 9u, 173u,
160u, 100u, 143u, 36u, 8u, 96u, 74u, 20u, 178u, 60u, 139u, 193u, 130u,
134u, 72u, 62u, 80u, 201u, 154u, 25u, 74u, 25u, 90u, 25u, 96u, 239u,
67u, 38u, 104u, 102u, 40u, 102u, 104u, 102u, 168u, 102u, 232u, 103u,
40u, 103u, 104u, 103u, 168u, 103u, 232u, 112u, 40u, 112u, 73u, 30u,
69u, 208u, 192u, 148u, 56u, 36u, 121u, 23u, 3u, 2u, 80u, 224u, 17u,
228u, 91u, 12u, 9u, 67u, 62u, 71u, 145u, 104u, 48u, 37u, 12u, 241u,
30u, 69u, 144u, 192u, 148u, 51u, 164u, 121u, 22u, 3u, 2u, 80u, 206u,
17u, 228u, 87u, 12u, 9u, 67u, 54u, 71u, 145u, 88u, 48u, 37u, 12u,
209u, 30u, 69u, 80u, 192u, 148u, 51u, 36u, 121u, 21u, 3u, 2u, 80u,
204u, 17u, 228u, 83u, 12u, 9u, 175u, 17u, 228u, 121u, 20u, 131u, 2u,
80u, 201u, 145u, 228u, 81u, 12u, 9u, 67u, 44u, 71u, 145u, 64u, 48u,
37u, 12u, 169u, 30u, 68u, 240u, 192u, 148u, 50u, 132u, 121u, 19u,
131u, 2u, 107u, 192u, 9u, 30u, 68u, 208u, 192u, 148u, 50u, 100u, 121u,
18u, 131u, 0u, 161u, 85u, 33u, 53u, 223u, 56u, 145u, 228u, 73u, 12u,
9u, 66u, 150u, 71u, 145u, 24u, 48u, 80u, 201u, 7u, 202u, 25u, 51u,
67u, 41u, 67u, 42u, 29u, 232u, 100u, 205u, 12u, 189u, 12u, 197u, 12u,
205u, 12u, 213u, 12u, 221u, 12u, 229u, 12u, 237u, 12u, 245u, 12u,
253u, 14u, 5u, 14u, 9u, 35u, 200u, 138u, 24u, 18u, 135u, 4u, 143u,
34u, 32u, 96u, 74u, 28u, 2u, 60u, 136u, 97u, 129u, 40u, 103u, 200u,
242u, 33u, 6u, 4u, 161u, 158u, 35u, 200u, 130u, 24u, 18u, 134u, 116u,
143u, 34u, 0u, 96u, 74u, 25u, 194u, 60u, 131u, 225u, 129u, 40u, 102u,
200u, 242u, 15u, 6u, 4u, 161u, 154u, 35u, 200u, 58u, 24u, 18u, 134u,
100u, 143u, 32u, 224u, 96u, 74u, 25u, 130u, 60u, 131u, 97u, 129u, 40u,
101u, 200u, 242u, 13u, 6u, 4u, 216u, 52u, 162u, 60u, 131u, 33u, 129u,
40u, 100u, 200u, 242u, 12u, 6u, 4u, 161u, 149u, 35u, 200u, 46u, 24u,
18u, 134u, 80u, 143u, 32u, 176u, 96u, 77u, 130u, 204u, 35u, 200u, 42u,
24u, 18u, 134u, 76u, 143u, 32u, 144u, 96u, 20u, 42u, 196u, 38u, 193u,
76u, 146u, 60u, 130u, 33u, 129u, 40u, 82u, 200u, 242u, 7u, 6u, 10u,
25u, 32u, 249u, 67u, 38u, 104u, 101u, 3u, 189u, 12u, 153u, 161u, 150u,
161u, 151u, 161u, 152u, 161u, 153u, 161u, 154u, 161u, 155u, 161u,
156u, 161u, 157u, 161u, 158u, 161u, 159u, 161u, 192u, 161u, 193u, 36u,
121u, 3u, 67u, 2u, 80u, 224u, 145u, 228u, 12u, 12u, 9u, 67u, 128u,
71u, 144u, 44u, 48u, 37u, 12u, 249u, 30u, 64u, 160u, 192u, 148u, 51u,
196u, 121u, 2u, 67u, 2u, 80u, 206u, 145u, 228u, 8u, 12u, 9u, 67u, 56u,
71u, 144u, 28u, 48u, 37u, 12u, 217u, 30u, 64u, 96u, 192u, 148u, 51u,
68u, 121u, 1u, 67u, 2u, 80u, 204u, 145u, 228u, 4u, 12u, 9u, 67u, 48u,
71u, 144u, 12u, 48u, 37u, 12u, 185u, 30u, 64u, 32u, 192u, 148u, 50u,
196u, 121u, 0u, 67u, 2u, 108u, 112u, 41u, 30u, 64u, 0u, 192u, 148u,
50u, 100u, 120u, 247u, 195u, 2u, 80u, 202u, 17u, 227u, 222u, 12u, 9u,
177u, 178u, 228u, 120u, 247u, 67u, 2u, 80u, 201u, 145u, 227u, 218u,
12u, 2u, 133u, 92u, 132u, 216u, 216u, 66u, 71u, 143u, 100u, 48u, 37u,
10u, 89u, 30u, 61u, 96u, 193u, 67u, 36u, 31u, 40u, 100u, 200u, 119u,
161u, 147u, 52u, 50u, 180u, 50u, 212u, 50u, 244u, 51u, 20u, 51u, 52u,
51u, 84u, 51u, 116u, 51u, 148u, 51u, 180u, 51u, 212u, 51u, 244u, 56u,
20u, 56u, 36u, 143u, 30u, 168u, 96u, 74u, 28u, 18u, 60u, 122u, 129u,
129u, 40u, 112u, 8u, 241u, 233u, 134u, 4u, 161u, 159u, 35u, 199u,
164u, 24u, 18u, 134u, 120u, 143u, 30u, 136u, 96u, 74u, 25u, 210u, 60u,
122u, 1u, 129u, 40u, 103u, 8u, 241u, 231u, 134u, 4u, 161u, 155u, 35u,
199u, 156u, 24u, 18u, 134u, 104u, 143u, 30u, 104u, 96u, 74u, 25u,
146u, 60u, 121u, 129u, 129u, 40u, 102u, 8u, 241u, 229u, 134u, 4u,
161u, 151u, 35u, 199u, 148u, 24u, 18u, 134u, 88u, 143u, 30u, 72u, 96u,
74u, 25u, 82u, 60u, 121u, 1u, 129u, 54u, 99u, 0u, 143u, 30u, 56u, 96u,
74u, 25u, 50u, 60u, 120u, 193u, 129u, 54u, 98u, 8u, 143u, 30u, 40u,
96u, 74u, 25u, 50u, 60u, 120u, 65u, 128u, 80u, 172u, 16u, 155u, 48u,
158u, 72u, 241u, 224u, 134u, 4u, 161u, 75u, 35u, 199u, 128u, 24u, 40u,
86u, 69u, 10u, 208u, 104u, 161u, 147u, 36u, 38u, 209u, 7u, 4u, 217u,
116u, 48u, 155u, 24u, 176u, 19u, 96u, 99u, 66u, 107u, 182u, 16u, 77u,
107u, 215u, 9u, 172u, 33u, 129u, 53u, 89u, 4u, 38u, 165u, 171u, 4u,
212u, 8u, 176u, 154u, 107u, 128u, 19u, 74u, 181u, 66u, 105u, 0u, 80u,
77u, 21u, 31u, 13u, 33u, 232u, 38u, 132u, 169u, 132u, 192u, 136u,
146u, 71u, 142u, 120u, 48u, 38u, 209u, 166u, 145u, 227u, 157u, 12u,
9u, 180u, 112u, 4u, 120u, 231u, 3u, 2u, 80u, 201u, 145u, 227u, 154u,
12u, 9u, 180u, 105u, 164u, 120u, 229u, 195u, 3u, 61u, 10u, 221u, 12u,
153u, 35u, 199u, 44u, 24u, 18u, 134u, 76u, 143u, 28u, 144u, 96u, 107u,
13u, 84u, 50u, 100u, 145u, 227u, 145u, 12u, 9u, 181u, 75u, 100u, 120u,
228u, 3u, 2u, 80u, 201u, 145u, 227u, 88u, 12u, 17u, 37u, 10u, 229u,
10u, 197u, 10u, 245u, 10u, 189u, 12u, 5u, 10u, 181u, 12u, 21u, 10u,
173u, 12u, 37u, 10u, 165u, 12u, 53u, 10u, 157u, 12u, 69u, 10u, 149u,
12u, 93u, 10u, 141u, 12u, 81u, 35u, 198u, 174u, 24u, 18u, 134u, 40u,
143u, 26u, 160u, 96u, 74u, 24u, 178u, 60u, 106u, 33u, 129u, 40u, 98u,
8u, 241u, 167u, 6u, 4u, 161u, 134u, 35u, 198u, 150u, 24u, 18u, 134u,
16u, 143u, 26u, 64u, 96u, 74u, 24u, 34u, 60u, 104u, 161u, 129u, 40u,
96u, 8u, 241u, 161u, 6u, 4u, 161u, 94u, 35u, 198u, 60u, 24u, 18u,
133u, 112u, 143u, 24u, 232u, 96u, 74u, 28u, 34u, 60u, 99u, 97u, 130u,
82u, 161u, 138u, 20u, 56u, 116u, 56u, 65u, 134u, 134u, 40u, 146u, 71u,
140u, 100u, 48u, 38u, 217u, 97u, 145u, 227u, 24u, 12u, 9u, 67u, 132u,
71u, 140u, 92u, 48u, 37u, 12u, 81u, 30u, 49u, 80u, 193u, 41u, 80u,
197u, 138u, 28u, 58u, 28u, 32u, 195u, 67u, 22u, 73u, 35u, 198u, 38u,
24u, 19u, 109u, 67u, 8u, 241u, 137u, 6u, 4u, 161u, 194u, 35u, 198u,
34u, 24u, 18u, 134u, 44u, 143u, 24u, 120u, 96u, 148u, 168u, 98u, 5u,
14u, 29u, 14u, 16u, 97u, 161u, 136u, 36u, 145u, 227u, 13u, 12u, 9u,
182u, 234u, 164u, 120u, 195u, 3u, 2u, 80u, 225u, 17u, 227u, 11u, 12u,
9u, 67u, 16u, 71u, 140u, 36u, 48u, 74u, 84u, 48u, 194u, 135u, 14u,
135u, 8u, 48u, 208u, 195u, 18u, 72u, 241u, 131u, 134u, 4u, 219u, 153u,
226u, 60u, 96u, 193u, 129u, 40u, 112u, 136u, 241u, 130u, 134u, 4u,
161u, 134u, 35u, 198u, 6u, 24u, 37u, 42u, 24u, 65u, 67u, 135u, 67u,
132u, 24u, 104u, 97u, 9u, 36u, 120u, 192u, 67u, 2u, 109u, 224u, 57u,
30u, 48u, 0u, 192u, 148u, 56u, 68u, 120u, 183u, 195u, 2u, 80u, 194u,
17u, 226u, 221u, 12u, 18u, 149u, 12u, 16u, 161u, 195u, 161u, 194u,
12u, 52u, 48u, 68u, 146u, 60u, 91u, 97u, 129u, 54u, 249u, 64u, 143u,
22u, 208u, 96u, 74u, 28u, 34u, 60u, 91u, 33u, 129u, 40u, 96u, 136u,
241u, 107u, 134u, 9u, 74u, 134u, 0u, 80u, 225u, 208u, 225u, 6u, 26u,
24u, 2u, 73u, 30u, 45u, 80u, 192u, 155u, 129u, 50u, 71u, 139u, 80u,
48u, 37u, 14u, 17u, 30u, 45u, 48u, 192u, 148u, 48u, 4u, 120u, 180u,
67u, 4u, 165u, 66u, 188u, 40u, 112u, 232u, 112u, 131u, 13u, 10u, 241u,
36u, 143u, 22u, 120u, 96u, 77u, 195u, 2u, 35u, 197u, 156u, 24u, 18u,
135u, 8u, 143u, 22u, 104u, 96u, 74u, 21u, 226u, 60u, 89u, 129u, 129u,
144u, 10u, 21u, 225u, 67u, 137u, 66u, 189u, 66u, 190u, 72u, 161u,
128u, 20u, 56u, 148u, 48u, 20u, 48u, 36u, 138u, 24u, 33u, 67u, 137u,
67u, 5u, 67u, 6u, 72u, 161u, 132u, 20u, 56u, 148u, 48u, 148u, 48u,
164u, 138u, 24u, 97u, 67u, 137u, 67u, 13u, 67u, 14u, 72u, 161u, 136u,
20u, 56u, 148u, 49u, 20u, 49u, 36u, 138u, 24u, 176u, 202u, 40u, 99u,
40u, 98u, 200u, 161u, 195u, 161u, 194u, 12u, 52u, 49u, 100u, 208u,
197u, 146u, 40u, 98u, 131u, 40u, 161u, 140u, 161u, 138u, 34u, 135u,
14u, 135u, 8u, 48u, 208u, 197u, 19u, 67u, 20u, 73u, 12u, 161u, 192u,
38u, 225u, 98u, 132u, 220u, 5u, 192u, 155u, 124u, 38u, 19u, 110u,
242u, 130u, 109u, 204u, 8u, 77u, 183u, 24u, 9u, 182u, 153u, 225u, 54u,
202u, 24u, 208u, 225u, 4u, 218u, 188u, 178u, 72u, 241u, 101u, 134u,
4u, 161u, 138u, 35u, 197u, 142u, 24u, 19u, 114u, 161u, 136u, 241u,
99u, 6u, 4u, 161u, 194u, 35u, 197u, 136u, 24u, 19u, 114u, 133u, 72u,
241u, 97u, 6u, 4u, 220u, 164u, 2u, 60u, 88u, 33u, 129u, 40u, 98u,
136u, 241u, 79u, 134u, 4u, 220u, 164u, 2u, 60u, 83u, 193u, 129u, 40u,
98u, 200u, 241u, 77u, 6u, 4u, 220u, 148u, 130u, 60u, 83u, 33u, 129u,
40u, 112u, 136u, 241u, 75u, 134u, 4u, 220u, 141u, 114u, 60u, 82u,
161u, 129u, 55u, 36u, 8u, 143u, 20u, 160u, 96u, 74u, 24u, 178u, 60u,
82u, 65u, 129u, 55u, 36u, 8u, 143u, 20u, 136u, 96u, 74u, 24u, 130u,
60u, 81u, 193u, 129u, 40u, 98u, 72u, 241u, 69u, 6u, 10u, 28u, 58u,
28u, 32u, 195u, 67u, 16u, 73u, 30u, 40u, 144u, 192u, 148u, 56u, 68u,
120u, 161u, 195u, 3u, 40u, 161u, 140u, 161u, 137u, 33u, 55u, 83u, 18u,
134u, 36u, 145u, 226u, 133u, 12u, 9u, 186u, 193u, 4u, 120u, 161u, 3u,
2u, 80u, 196u, 145u, 226u, 130u, 12u, 9u, 186u, 193u, 4u, 120u, 160u,
3u, 3u, 40u, 161u, 140u, 161u, 136u, 33u, 55u, 87u, 118u, 134u, 32u,
145u, 226u, 94u, 12u, 9u, 187u, 10u, 4u, 120u, 151u, 67u, 2u, 80u,
196u, 17u, 226u, 91u, 12u, 9u, 187u, 10u, 4u, 120u, 150u, 131u, 2u,
80u, 195u, 17u, 226u, 87u, 12u, 9u, 67u, 14u, 71u, 137u, 76u, 48u,
80u, 225u, 208u, 225u, 6u, 26u, 24u, 98u, 72u, 241u, 41u, 6u, 4u,
161u, 194u, 35u, 196u, 160u, 24u, 25u, 69u, 12u, 101u, 12u, 57u, 9u,
187u, 107u, 52u, 48u, 228u, 143u, 18u, 112u, 96u, 77u, 220u, 157u,
35u, 196u, 154u, 24u, 18u, 134u, 28u, 143u, 18u, 88u, 96u, 77u, 220u,
157u, 35u, 196u, 146u, 24u, 25u, 69u, 12u, 101u, 12u, 49u, 9u, 187u,
146u, 84u, 48u, 196u, 143u, 18u, 56u, 96u, 77u, 223u, 5u, 35u, 196u,
140u, 24u, 18u, 134u, 24u, 143u, 18u, 32u, 96u, 77u, 223u, 5u, 35u,
196u, 134u, 24u, 18u, 134u, 16u, 143u, 18u, 0u, 96u, 74u, 24u, 82u,
60u, 67u, 129u, 130u, 135u, 14u, 135u, 8u, 48u, 208u, 194u, 18u, 71u,
136u, 108u, 48u, 37u, 14u, 17u, 30u, 33u, 144u, 192u, 202u, 40u, 99u,
40u, 97u, 72u, 77u, 226u, 14u, 161u, 133u, 36u, 120u, 133u, 195u, 2u,
111u, 26u, 145u, 30u, 33u, 96u, 192u, 148u, 48u, 164u, 120u, 133u, 3u,
2u, 111u, 26u, 145u, 30u, 33u, 32u, 192u, 202u, 40u, 99u, 40u, 97u,
8u, 77u, 227u, 71u, 161u, 132u, 36u, 120u, 132u, 3u, 2u, 111u, 44u,
209u, 30u, 32u, 240u, 192u, 148u, 48u, 132u, 120u, 131u, 67u, 2u,
111u, 44u, 209u, 30u, 32u, 192u, 192u, 148u, 48u, 68u, 120u, 130u,
67u, 2u, 80u, 193u, 145u, 226u, 5u, 12u, 20u, 56u, 116u, 56u, 65u,
134u, 134u, 8u, 146u, 60u, 64u, 129u, 129u, 40u, 112u, 136u, 241u, 1u,
6u, 6u, 81u, 67u, 25u, 67u, 6u, 66u, 111u, 70u, 29u, 12u, 25u, 35u,
196u, 0u, 24u, 19u, 122u, 129u, 200u, 240u, 239u, 134u, 4u, 161u,
131u, 35u, 195u, 186u, 24u, 19u, 122u, 129u, 200u, 240u, 237u, 134u,
6u, 81u, 67u, 25u, 67u, 4u, 66u, 111u, 78u, 229u, 12u, 17u, 35u, 195u,
178u, 24u, 19u, 123u, 19u, 200u, 240u, 236u, 6u, 4u, 161u, 130u, 35u,
195u, 172u, 24u, 19u, 123u, 19u, 200u, 240u, 234u, 134u, 4u, 161u,
128u, 35u, 195u, 164u, 24u, 18u, 134u, 4u, 143u, 14u, 112u, 96u, 161u,
195u, 161u, 194u, 12u, 52u, 48u, 4u, 145u, 225u, 205u, 12u, 9u, 67u,
132u, 71u, 135u, 44u, 48u, 50u, 138u, 24u, 202u, 24u, 18u, 19u, 123u,
214u, 40u, 96u, 73u, 30u, 28u, 144u, 192u, 155u, 225u, 56u, 71u, 135u,
32u, 48u, 37u, 12u, 9u, 30u, 28u, 96u, 192u, 155u, 225u, 56u, 71u,
135u, 16u, 48u, 50u, 138u, 24u, 202u, 24u, 2u, 19u, 124u, 36u, 104u,
96u, 9u, 30u, 28u, 32u, 192u, 155u, 230u, 8u, 71u, 135u, 4u, 48u, 37u,
12u, 1u, 30u, 25u, 240u, 192u, 155u, 230u, 8u, 71u, 134u, 120u, 48u,
37u, 10u, 241u, 30u, 25u, 176u, 192u, 148u, 43u, 228u, 120u, 101u,
195u, 5u, 14u, 29u, 14u, 16u, 97u, 161u, 94u, 36u, 143u, 12u, 176u,
96u, 74u, 28u, 34u, 60u, 50u, 129u, 129u, 148u, 80u, 198u, 80u, 175u,
144u, 155u, 236u, 27u, 66u, 190u, 72u, 240u, 201u, 6u, 4u, 223u, 117u,
18u, 60u, 50u, 33u, 129u, 40u, 87u, 200u, 240u, 199u, 134u, 4u, 223u,
117u, 18u, 60u, 49u, 161u, 129u, 148u, 80u, 198u, 80u, 175u, 16u,
155u, 238u, 141u, 66u, 188u, 72u, 240u, 197u, 134u, 4u, 223u, 153u,
146u, 60u, 49u, 65u, 129u, 40u, 87u, 136u, 240u, 196u, 6u, 4u, 223u,
153u, 146u, 60u, 48u, 193u, 129u, 40u, 112u, 72u, 240u, 192u, 134u,
6u, 81u, 67u, 25u, 67u, 130u, 103u, 250u, 28u, 18u, 71u, 133u, 124u,
48u, 38u, 254u, 43u, 145u, 225u, 94u, 12u, 9u, 67u, 130u, 71u, 133u,
112u, 48u, 38u, 254u, 43u, 145u, 225u, 90u, 12u, 9u, 67u, 128u, 71u,
133u, 84u, 48u, 50u, 138u, 24u, 202u, 28u, 3u, 63u, 208u, 224u, 18u,
60u, 42u, 97u, 129u, 55u, 252u, 16u, 143u, 10u, 144u, 96u, 74u, 28u,
2u, 60u, 42u, 1u, 129u, 55u, 252u, 16u, 143u, 10u, 112u, 96u, 74u,
25u, 242u, 60u, 41u, 33u, 129u, 148u, 80u, 198u, 80u, 207u, 153u,
254u, 134u, 124u, 145u, 225u, 71u, 12u, 9u, 161u, 4u, 200u, 145u,
225u, 70u, 12u, 9u, 67u, 62u, 71u, 133u, 16u, 48u, 38u, 132u, 19u,
34u, 71u, 133u, 8u, 48u, 37u, 12u, 241u, 30u, 17u, 208u, 192u, 202u,
40u, 99u, 40u, 103u, 140u, 255u, 67u, 60u, 72u, 240u, 141u, 134u, 4u,
208u, 131u, 18u, 136u, 240u, 141u, 6u, 4u, 161u, 158u, 35u, 194u, 48u,
24u, 19u, 66u, 12u, 74u, 35u, 194u, 44u, 24u, 18u, 134u, 116u, 143u,
8u, 136u, 96u, 101u, 20u, 49u, 148u, 51u, 166u, 127u, 161u, 157u, 36u,
120u, 67u, 195u, 2u, 104u, 65u, 224u, 100u, 120u, 67u, 131u, 2u, 80u,
206u, 145u, 225u, 12u, 12u, 9u, 161u, 7u, 129u, 145u, 225u, 10u, 12u,
9u, 67u, 56u, 71u, 132u, 20u, 48u, 50u, 138u, 24u, 202u, 25u, 195u,
63u, 208u, 206u, 18u, 60u, 32u, 97u, 129u, 52u, 33u, 153u, 194u, 60u,
32u, 65u, 129u, 40u, 103u, 8u, 240u, 128u, 6u, 4u, 208u, 134u, 103u,
8u, 239u, 193u, 129u, 40u, 102u, 200u, 239u, 33u, 129u, 148u, 80u,
198u, 80u, 205u, 153u, 254u, 134u, 108u, 145u, 221u, 195u, 2u, 104u,
67u, 137u, 36u, 119u, 96u, 192u, 148u, 51u, 100u, 119u, 64u, 192u,
154u, 16u, 226u, 73u, 29u, 200u, 48u, 37u, 12u, 209u, 29u, 180u, 48u,
50u, 138u, 24u, 202u, 25u, 163u, 63u, 208u, 205u, 18u, 59u, 88u, 96u,
77u, 8u, 123u, 16u, 142u, 212u, 24u, 18u, 134u, 104u, 142u, 208u, 24u,
19u, 66u, 30u, 196u, 35u, 179u, 6u, 4u, 161u, 153u, 35u, 176u, 134u,
6u, 81u, 67u, 25u, 67u, 50u, 103u, 250u, 25u, 146u, 71u, 95u, 12u, 9u,
161u, 20u, 143u, 145u, 215u, 131u, 2u, 80u, 204u, 145u, 215u, 3u, 2u,
104u, 69u, 35u, 228u, 117u, 160u, 192u, 148u, 51u, 4u, 117u, 80u,
192u, 202u, 40u, 99u, 40u, 102u, 12u, 255u, 67u, 48u, 72u, 234u, 97u,
129u, 52u, 34u, 185u, 162u, 58u, 144u, 96u, 74u, 25u, 130u, 58u, 128u,
96u, 77u, 8u, 174u, 104u, 142u, 156u, 24u, 18u, 134u, 92u, 142u, 146u,
24u, 25u, 69u, 12u, 101u, 12u, 185u, 159u, 232u, 101u, 201u, 29u, 28u,
48u, 38u, 132u, 92u, 42u, 71u, 70u, 12u, 9u, 67u, 46u, 71u, 68u, 12u,
9u, 161u, 23u, 10u, 145u, 208u, 131u, 2u, 80u, 203u, 17u, 207u, 67u,
3u, 40u, 161u, 140u, 161u, 150u, 51u, 253u, 12u, 177u, 35u, 157u,
134u, 4u, 208u, 142u, 36u, 8u, 231u, 65u, 129u, 40u, 101u, 136u, 231u,
1u, 129u, 52u, 35u, 137u, 2u, 57u, 176u, 96u, 74u, 25u, 82u, 57u,
136u, 96u, 101u, 20u, 49u, 148u, 50u, 166u, 127u, 161u, 149u, 36u,
114u, 240u, 192u, 154u, 17u, 216u, 89u, 28u, 184u, 48u, 37u, 12u,
169u, 28u, 176u, 48u, 38u, 132u, 118u, 22u, 71u, 42u, 12u, 9u, 67u,
40u, 71u, 37u, 12u, 12u, 162u, 134u, 50u, 134u, 80u, 207u, 244u, 50u,
132u, 142u, 70u, 24u, 19u, 66u, 61u, 134u, 35u, 145u, 6u, 4u, 161u,
148u, 35u, 144u, 6u, 4u, 208u, 143u, 97u, 136u, 227u, 193u, 129u,
174u, 134u, 76u, 145u, 199u, 67u, 2u, 80u, 201u, 145u, 192u, 3u, 2u,
80u, 226u, 144u, 196u, 24u, 24u, 197u, 14u, 21u, 12u, 133u, 10u, 245u,
12u, 5u, 12u, 21u, 12u, 37u, 12u, 53u, 12u, 69u, 12u, 93u, 12u, 85u,
10u, 253u, 12u, 13u, 12u, 29u, 12u, 45u, 12u, 61u, 12u, 73u, 20u, 56u,
192u, 80u, 226u, 10u, 28u, 117u, 2u, 135u, 34u, 134u, 58u, 134u, 52u,
134u, 81u, 67u, 25u, 67u, 28u, 67u, 40u, 161u, 140u, 161u, 141u, 34u,
135u, 14u, 135u, 8u, 48u, 208u, 199u, 19u, 67u, 26u, 104u, 99u, 137u,
36u, 132u, 220u, 80u, 226u, 73u, 0u,};
static unsigned char uvector__00049[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 67u, 147u, 67u, 133u, 67u, 33u, 66u, 189u,
67u, 1u, 67u, 5u, 67u, 9u, 67u, 13u, 67u, 17u, 67u, 23u, 67u, 21u,
66u, 191u, 67u, 3u, 67u, 7u, 67u, 11u, 67u, 15u, 67u, 18u, 69u, 14u,
48u, 20u, 56u, 130u, 135u, 29u, 64u, 161u, 200u, 161u, 142u, 161u,
141u, 33u, 148u, 80u, 198u, 80u, 199u, 16u, 202u, 40u, 99u, 40u, 99u,
72u, 161u, 195u, 161u, 194u, 12u, 52u, 49u, 196u, 208u, 198u, 154u,
24u, 226u, 73u, 33u, 144u, 10u, 21u, 225u, 67u, 137u, 66u, 189u, 66u,
190u, 72u, 161u, 128u, 20u, 56u, 148u, 48u, 20u, 48u, 36u, 138u, 24u,
33u, 67u, 137u, 67u, 5u, 67u, 6u, 72u, 161u, 132u, 20u, 56u, 148u,
48u, 148u, 48u, 164u, 138u, 24u, 97u, 67u, 137u, 67u, 13u, 67u, 14u,
72u, 161u, 136u, 20u, 56u, 148u, 49u, 20u, 49u, 36u, 138u, 24u, 176u,
202u, 40u, 99u, 40u, 98u, 200u, 161u, 195u, 161u, 194u, 12u, 52u, 49u,
100u, 208u, 197u, 146u, 40u, 98u, 131u, 40u, 161u, 140u, 161u, 138u,
34u, 135u, 14u, 135u, 8u, 48u, 208u, 197u, 19u, 67u, 20u, 73u, 12u,
161u, 192u, 74u, 84u, 43u, 194u, 135u, 14u, 135u, 8u, 48u, 208u, 175u,
18u, 68u, 165u, 67u, 0u, 40u, 112u, 232u, 112u, 131u, 13u, 12u, 1u,
36u, 74u, 84u, 48u, 66u, 135u, 14u, 135u, 8u, 48u, 208u, 193u, 18u,
68u, 165u, 67u, 8u, 40u, 112u, 232u, 112u, 131u, 13u, 12u, 33u, 36u,
74u, 84u, 48u, 194u, 135u, 14u, 135u, 8u, 48u, 208u, 195u, 18u, 68u,
165u, 67u, 16u, 40u, 112u, 232u, 112u, 131u, 13u, 12u, 65u, 36u, 74u,
84u, 49u, 98u, 135u, 14u, 135u, 8u, 48u, 208u, 197u, 146u, 68u, 165u,
67u, 20u, 40u, 112u, 232u, 112u, 131u, 13u, 12u, 81u, 36u, 208u, 225u,
8u, 146u, 133u, 114u, 133u, 98u, 133u, 122u, 133u, 94u, 134u, 2u,
133u, 90u, 134u, 10u, 133u, 86u, 134u, 18u, 133u, 82u, 134u, 26u,
133u, 78u, 134u, 34u, 133u, 74u, 134u, 46u, 133u, 70u, 134u, 40u,
146u, 73u, 28u, 36u, 48u, 49u, 132u, 192u, 221u, 49u, 58u, 35u, 131u,
134u, 4u, 192u, 112u, 142u, 4u, 24u, 19u, 1u, 194u, 56u, 0u, 96u, 76u,
7u, 9u, 0u,};
static unsigned char uvector__00050[] = {
 0u, 3u, 157u, 134u, 7u, 90u, 28u, 170u, 28u, 184u, 84u, 145u, 206u,
131u, 2u, 66u, 164u, 115u, 144u, 192u, 148u, 57u, 100u, 115u, 112u,
192u, 148u, 57u, 68u, 115u, 64u, 193u, 67u, 153u, 0u, 146u, 57u, 152u,
96u, 72u, 4u, 142u, 100u, 24u, 40u, 115u, 64u, 128u, 67u, 173u, 14u,
117u, 14u, 88u, 161u, 207u, 161u, 208u, 133u, 73u, 36u, 38u, 30u,
228u, 142u, 98u, 24u, 18u, 1u, 35u, 152u, 6u, 4u, 197u, 104u, 142u,
94u, 24u, 19u, 23u, 210u, 57u, 104u, 96u, 76u, 106u, 8u, 229u, 129u,
129u, 33u, 82u, 57u, 64u, 96u, 76u, 106u, 8u, 228u, 225u, 129u, 40u,
114u, 200u, 228u, 161u, 129u, 40u, 115u, 136u, 228u, 97u, 129u, 49u,
125u, 35u, 144u, 6u, 7u, 186u, 29u, 23u, 226u, 71u, 31u, 12u, 9u, 67u,
162u, 71u, 29u, 12u, 14u, 180u, 57u, 212u, 57u, 98u, 135u, 62u, 135u,
66u, 21u, 36u, 142u, 54u, 24u, 19u, 66u, 33u, 136u, 227u, 65u, 129u,
33u, 82u, 56u, 176u, 96u, 77u, 8u, 134u, 35u, 138u, 134u, 4u, 161u,
203u, 35u, 137u, 134u, 4u, 161u, 206u, 35u, 136u, 134u, 7u, 186u, 29u,
17u, 67u, 164u, 73u, 28u, 60u, 48u, 38u, 133u, 143u, 145u, 195u, 67u,
2u, 104u, 88u, 249u, 28u, 48u, 48u, 37u, 14u, 137u, 28u, 44u, 48u,
50u, 1u, 67u, 162u, 40u, 116u, 232u, 116u, 33u, 81u, 67u, 168u, 73u,
33u, 176u, 4u, 208u, 176u, 64u, 154u, 16u, 54u, 64u, 76u, 235u, 2u,
98u, 180u, 67u, 72u, 76u, 7u, 9u, 36u, 112u, 144u, 192u, 154u, 26u,
20u, 71u, 7u, 12u, 9u, 161u, 170u, 228u, 112u, 80u, 192u, 154u, 26u,
174u, 71u, 4u, 12u, 9u, 10u, 145u, 192u, 3u, 2u, 104u, 104u, 81u, 12u,
65u, 129u, 140u, 80u, 229u, 164u, 42u, 27u, 104u, 116u, 66u, 104u,
104u, 80u, 77u, 13u, 150u, 36u, 144u,};
static unsigned char uvector__00051[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 135u, 84u, 80u, 235u, 80u, 229u, 164u,
42u, 27u, 104u, 116u, 69u, 14u, 157u, 14u, 132u, 42u, 40u, 117u, 9u,
13u, 128u, 61u, 208u, 232u, 138u, 29u, 34u, 67u, 173u, 14u, 117u, 14u,
88u, 161u, 207u, 161u, 208u, 133u, 73u, 32u, 61u, 208u, 232u, 191u,
17u, 67u, 154u, 4u, 2u, 29u, 104u, 115u, 168u, 114u, 197u, 14u, 125u,
14u, 132u, 42u, 73u, 34u, 135u, 50u, 1u, 36u, 134u, 144u, 235u, 67u,
149u, 67u, 151u, 10u, 146u, 73u, 35u, 132u, 134u, 6u, 48u, 152u, 36u,
166u, 12u, 228u, 112u, 112u, 192u, 152u, 14u, 17u, 192u, 131u, 2u,
96u, 56u, 71u, 0u, 12u, 9u, 128u, 225u, 32u,};
static unsigned char uvector__00052[] = {
 0u, 3u, 144u, 6u, 7u, 90u, 29u, 122u, 28u, 184u, 84u, 145u, 199u,
195u, 2u, 66u, 164u, 113u, 224u, 192u, 148u, 57u, 100u, 113u, 192u,
192u, 148u, 58u, 228u, 113u, 160u, 192u, 246u, 40u, 117u, 8u, 161u,
210u, 36u, 142u, 48u, 24u, 19u, 17u, 130u, 56u, 176u, 96u, 76u, 70u,
8u, 226u, 129u, 129u, 49u, 2u, 35u, 137u, 6u, 4u, 196u, 8u, 142u, 32u,
24u, 29u, 104u, 118u, 40u, 114u, 225u, 82u, 71u, 15u, 12u, 9u, 10u,
145u, 195u, 131u, 2u, 80u, 229u, 145u, 195u, 3u, 2u, 80u, 236u, 17u,
194u, 131u, 3u, 40u, 112u, 15u, 52u, 58u, 36u, 38u, 30u, 228u, 38u,
3u, 129u, 50u, 2u, 36u, 112u, 144u, 192u, 148u, 58u, 36u, 112u, 128u,
192u, 200u, 5u, 14u, 136u, 161u, 211u, 161u, 208u, 133u, 91u, 137u,
33u, 50u, 221u, 36u, 112u, 96u, 192u, 153u, 224u, 17u, 193u, 3u, 2u,
66u, 164u, 112u, 0u, 192u, 153u, 224u, 16u, 196u, 24u, 24u, 197u, 14u,
90u, 66u, 161u, 182u, 135u, 68u, 38u, 120u, 1u, 50u, 221u, 36u, 144u,};
static unsigned char uvector__00053[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 135u, 84u, 80u, 236u, 208u, 229u, 164u,
42u, 27u, 104u, 116u, 69u, 14u, 157u, 14u, 132u, 42u, 220u, 67u, 40u,
112u, 15u, 52u, 58u, 36u, 61u, 138u, 29u, 66u, 40u, 116u, 137u, 33u,
214u, 135u, 94u, 135u, 46u, 21u, 33u, 214u, 135u, 98u, 135u, 46u, 21u,
36u, 146u, 56u, 72u, 96u, 99u, 9u, 130u, 74u, 96u, 206u, 71u, 7u, 12u,
9u, 128u, 225u, 28u, 8u, 48u, 38u, 3u, 132u, 112u, 0u, 192u, 152u,
14u, 18u,};
static unsigned char uvector__00054[] = {
 0u, 3u, 129u, 6u, 8u, 82u, 13u, 36u, 112u, 16u, 192u, 144u, 161u,
28u, 0u, 48u, 36u, 26u, 67u, 16u, 96u, 99u, 80u, 152u, 14u, 18u, 64u,};
static unsigned char uvector__00055[] = {
 0u, 3u, 128u, 134u, 10u, 29u, 168u, 52u, 145u, 192u, 3u, 2u, 65u,
164u, 49u, 6u, 6u, 53u, 9u, 128u, 225u, 36u,};
static unsigned char uvector__00056[] = {
 0u, 3u, 133u, 6u, 8u, 96u, 49u, 168u, 133u, 32u, 210u, 67u, 26u,
138u, 29u, 168u, 52u, 144u, 195u, 12u, 144u, 194u, 33u, 175u, 64u,
65u, 36u, 142u, 6u, 24u, 24u, 211u, 5u, 210u, 56u, 0u, 96u, 99u, 76u,
12u, 72u, 98u, 12u, 12u, 98u, 13u, 133u, 8u, 134u, 194u, 96u, 112u,
152u, 48u, 146u, 64u,};
static unsigned char uvector__00057[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 20u, 59u, 112u, 108u, 40u, 68u, 54u,
33u, 72u, 52u, 138u, 29u, 168u, 52u, 146u, 71u, 9u, 12u, 12u, 97u,
48u, 67u, 76u, 22u, 136u, 224u, 225u, 129u, 48u, 28u, 35u, 129u, 6u,
4u, 192u, 112u, 142u, 0u, 24u, 19u, 1u, 194u, 64u,};
static unsigned char uvector__00058[] = {
 0u, 3u, 130u, 6u, 10u, 29u, 200u, 52u, 145u, 192u, 195u, 2u, 65u,
164u, 112u, 16u, 192u, 241u, 6u, 132u, 192u, 112u, 145u, 192u, 3u, 2u,
65u, 164u, 49u, 6u, 6u, 53u, 9u, 131u, 249u, 36u,};
static unsigned char uvector__00059[] = {
 0u, 3u, 137u, 6u, 8u, 96u, 49u, 168u, 133u, 32u, 210u, 67u, 26u,
135u, 136u, 52u, 80u, 238u, 65u, 164u, 144u, 195u, 12u, 144u, 194u,
33u, 175u, 64u, 121u, 36u, 142u, 22u, 24u, 24u, 211u, 5u, 210u, 56u,
64u, 96u, 99u, 76u, 12u, 72u, 224u, 225u, 129u, 144u, 8u, 52u, 58u,
208u, 235u, 80u, 229u, 208u, 238u, 146u, 68u, 54u, 19u, 3u, 132u,
193u, 132u, 145u, 193u, 131u, 2u, 99u, 18u, 71u, 5u, 12u, 9u, 67u,
186u, 71u, 4u, 12u, 9u, 67u, 150u, 71u, 2u, 12u, 9u, 67u, 172u, 71u,
0u, 12u, 9u, 140u, 73u, 12u, 65u, 129u, 140u, 80u, 229u, 194u, 137u,
67u, 186u, 27u, 96u, 208u, 152u, 196u, 132u, 199u, 52u, 146u, 64u,};
static unsigned char uvector__00060[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 135u, 84u, 80u, 239u, 80u, 229u, 194u,
137u, 67u, 186u, 27u, 96u, 208u, 235u, 67u, 173u, 67u, 151u, 67u,
186u, 68u, 54u, 33u, 72u, 52u, 135u, 136u, 52u, 80u, 238u, 65u, 164u,
146u, 72u, 225u, 33u, 129u, 140u, 38u, 9u, 41u, 131u, 185u, 28u, 28u,
48u, 38u, 3u, 132u, 112u, 32u, 192u, 152u, 14u, 17u, 192u, 3u, 2u,
96u, 56u, 72u,};
static unsigned char uvector__00061[] = {
 0u, 3u, 130u, 6u, 10u, 29u, 248u, 52u, 145u, 192u, 195u, 2u, 65u,
164u, 112u, 16u, 192u, 241u, 6u, 132u, 192u, 112u, 145u, 192u, 3u, 2u,
65u, 164u, 49u, 6u, 6u, 53u, 9u, 131u, 249u, 36u,};
static unsigned char uvector__00062[] = {
 0u, 3u, 137u, 6u, 8u, 96u, 49u, 168u, 133u, 32u, 210u, 67u, 26u,
135u, 136u, 52u, 80u, 239u, 193u, 164u, 144u, 195u, 12u, 144u, 194u,
33u, 175u, 64u, 161u, 36u, 142u, 22u, 24u, 24u, 211u, 5u, 210u, 56u,
64u, 96u, 99u, 76u, 12u, 72u, 224u, 225u, 129u, 144u, 8u, 52u, 58u,
208u, 236u, 208u, 229u, 208u, 238u, 146u, 68u, 54u, 19u, 3u, 132u,
193u, 132u, 145u, 193u, 131u, 2u, 99u, 18u, 71u, 5u, 12u, 9u, 67u,
186u, 71u, 4u, 12u, 9u, 67u, 150u, 71u, 2u, 12u, 9u, 67u, 178u, 71u,
0u, 12u, 9u, 140u, 73u, 12u, 65u, 129u, 140u, 80u, 229u, 194u, 137u,
67u, 186u, 27u, 96u, 208u, 152u, 196u, 132u, 199u, 52u, 146u, 64u,};
static unsigned char uvector__00063[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 135u, 84u, 81u, 0u, 80u, 229u, 194u,
137u, 67u, 186u, 27u, 96u, 208u, 235u, 67u, 179u, 67u, 151u, 67u,
186u, 68u, 54u, 33u, 72u, 52u, 135u, 136u, 52u, 80u, 239u, 193u, 164u,
146u, 72u, 225u, 33u, 129u, 140u, 38u, 9u, 41u, 131u, 185u, 28u, 28u,
48u, 38u, 3u, 132u, 112u, 32u, 192u, 152u, 14u, 17u, 192u, 3u, 2u,
96u, 56u, 72u,};
static unsigned char uvector__00064[] = {
 0u, 3u, 129u, 6u, 10u, 32u, 24u, 54u, 136u, 8u, 145u, 192u, 67u, 2u,
81u, 1u, 17u, 192u, 3u, 2u, 65u, 164u, 49u, 6u, 6u, 53u, 9u, 128u,
225u, 36u,};
static unsigned char uvector__00065[] = {
 0u, 3u, 128u, 134u, 10u, 29u, 200u, 52u, 145u, 192u, 3u, 2u, 65u,
164u, 49u, 6u, 6u, 53u, 9u, 128u, 225u, 36u,};
static unsigned char uvector__00066[] = {
 0u, 3u, 138u, 6u, 8u, 96u, 49u, 168u, 162u, 1u, 131u, 104u, 128u,
137u, 12u, 106u, 40u, 119u, 32u, 210u, 67u, 12u, 50u, 67u, 8u, 134u,
189u, 3u, 68u, 146u, 56u, 104u, 96u, 99u, 76u, 28u, 200u, 225u, 65u,
129u, 141u, 48u, 49u, 35u, 132u, 6u, 4u, 131u, 72u, 224u, 225u, 129u,
144u, 8u, 52u, 58u, 208u, 235u, 80u, 229u, 208u, 238u, 146u, 67u,
132u, 26u, 33u, 176u, 152u, 28u, 38u, 14u, 228u, 145u, 193u, 131u, 2u,
99u, 98u, 71u, 5u, 12u, 9u, 67u, 186u, 71u, 4u, 12u, 9u, 67u, 150u,
71u, 2u, 12u, 9u, 67u, 172u, 71u, 0u, 12u, 9u, 141u, 137u, 12u, 65u,
129u, 140u, 80u, 229u, 209u, 1u, 37u, 14u, 232u, 109u, 131u, 66u, 99u,
98u, 19u, 31u, 82u, 73u, 0u,};
static unsigned char uvector__00067[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 135u, 84u, 81u, 1u, 208u, 229u, 209u, 1u,
37u, 14u, 232u, 109u, 131u, 67u, 173u, 14u, 181u, 14u, 93u, 14u, 233u,
14u, 16u, 104u, 134u, 197u, 16u, 12u, 27u, 68u, 4u, 69u, 14u, 228u,
26u, 73u, 36u, 142u, 18u, 24u, 24u, 194u, 96u, 146u, 152u, 62u, 145u,
193u, 195u, 2u, 96u, 56u, 71u, 2u, 12u, 9u, 128u, 225u, 28u, 0u, 48u,
38u, 3u, 132u, 128u,};
static unsigned char uvector__00068[] = {
 0u, 3u, 129u, 6u, 10u, 32u, 72u, 54u, 136u, 8u, 145u, 192u, 67u, 2u,
81u, 1u, 17u, 192u, 3u, 2u, 65u, 164u, 49u, 6u, 6u, 53u, 9u, 128u,
225u, 36u,};
static unsigned char uvector__00069[] = {
 0u, 3u, 128u, 134u, 10u, 29u, 248u, 52u, 145u, 192u, 3u, 2u, 65u,
164u, 49u, 6u, 6u, 53u, 9u, 128u, 225u, 36u,};
static unsigned char uvector__00070[] = {
 0u, 3u, 138u, 6u, 8u, 96u, 49u, 168u, 162u, 4u, 131u, 104u, 128u,
137u, 12u, 106u, 40u, 119u, 224u, 210u, 67u, 12u, 50u, 67u, 8u, 134u,
189u, 8u, 4u, 146u, 56u, 104u, 96u, 99u, 76u, 28u, 200u, 225u, 65u,
129u, 141u, 48u, 49u, 35u, 132u, 6u, 4u, 131u, 72u, 224u, 225u, 129u,
144u, 8u, 52u, 58u, 208u, 236u, 208u, 229u, 208u, 238u, 146u, 67u,
132u, 26u, 33u, 176u, 152u, 28u, 38u, 14u, 228u, 145u, 193u, 131u, 2u,
99u, 98u, 71u, 5u, 12u, 9u, 67u, 186u, 71u, 4u, 12u, 9u, 67u, 150u,
71u, 2u, 12u, 9u, 67u, 178u, 71u, 0u, 12u, 9u, 141u, 137u, 12u, 65u,
129u, 140u, 80u, 229u, 209u, 1u, 37u, 14u, 232u, 109u, 131u, 66u, 99u,
98u, 19u, 31u, 82u, 73u, 0u,};
static unsigned char uvector__00071[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 135u, 84u, 81u, 2u, 208u, 229u, 209u, 1u,
37u, 14u, 232u, 109u, 131u, 67u, 173u, 14u, 205u, 14u, 93u, 14u, 233u,
14u, 16u, 104u, 134u, 197u, 16u, 36u, 27u, 68u, 4u, 69u, 14u, 252u,
26u, 73u, 36u, 142u, 18u, 24u, 24u, 194u, 96u, 146u, 152u, 62u, 145u,
193u, 195u, 2u, 96u, 56u, 71u, 2u, 12u, 9u, 128u, 225u, 28u, 0u, 48u,
38u, 3u, 132u, 128u,};
static unsigned char uvector__00072[] = {
 0u, 3u, 134u, 6u, 8u, 218u, 89u, 36u, 112u, 176u, 192u, 146u, 201u,
28u, 36u, 48u, 81u, 2u, 75u, 52u, 64u, 68u, 142u, 16u, 24u, 18u, 136u,
8u, 142u, 14u, 24u, 18u, 89u, 35u, 130u, 134u, 4u, 193u, 228u, 142u,
8u, 24u, 25u, 0u, 150u, 68u, 120u, 73u, 9u, 131u, 200u, 76u, 7u, 9u,
28u, 8u, 48u, 38u, 44u, 164u, 112u, 0u, 192u, 152u, 178u, 144u, 196u,
24u, 24u, 197u, 16u, 17u, 13u, 178u, 200u, 76u, 89u, 66u, 96u, 242u,
19u, 1u, 194u, 73u, 0u,};
static unsigned char uvector__00073[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 20u, 64u, 212u, 64u, 68u, 54u, 203u,
34u, 60u, 34u, 136u, 18u, 89u, 162u, 2u, 34u, 54u, 150u, 73u, 36u,
112u, 144u, 192u, 198u, 19u, 4u, 52u, 193u, 88u, 142u, 14u, 24u, 19u,
1u, 194u, 56u, 16u, 96u, 76u, 7u, 8u, 224u, 1u, 129u, 48u, 28u, 36u,};
static unsigned char uvector__00074[] = {
 0u, 3u, 131u, 134u, 10u, 32u, 17u, 68u, 15u, 68u, 16u, 104u, 128u,
137u, 28u, 24u, 48u, 37u, 16u, 17u, 28u, 12u, 48u, 38u, 5u, 228u,
112u, 32u, 192u, 148u, 65u, 4u, 112u, 0u, 192u, 152u, 23u, 144u, 196u,
24u, 24u, 197u, 16u, 69u, 16u, 17u, 9u, 128u, 225u, 36u,};
static unsigned char uvector__00075[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 20u, 65u, 52u, 65u, 20u, 64u, 68u,
81u, 0u, 138u, 32u, 122u, 32u, 131u, 68u, 4u, 73u, 28u, 36u, 48u, 49u,
132u, 193u, 13u, 48u, 102u, 35u, 131u, 134u, 4u, 192u, 112u, 142u, 4u,
24u, 19u, 1u, 194u, 56u, 0u, 96u, 76u, 7u, 9u, 0u,};
static unsigned char uvector__00076[] = {
 0u, 3u, 133u, 134u, 8u, 218u, 89u, 36u, 112u, 160u, 192u, 146u, 201u,
28u, 36u, 48u, 66u, 146u, 201u, 35u, 132u, 6u, 4u, 133u, 8u, 224u,
225u, 129u, 37u, 146u, 56u, 40u, 96u, 76u, 30u, 72u, 224u, 129u, 129u,
144u, 9u, 100u, 71u, 132u, 144u, 152u, 60u, 132u, 192u, 112u, 145u,
192u, 131u, 2u, 98u, 146u, 71u, 0u, 12u, 9u, 138u, 73u, 12u, 65u,
129u, 140u, 66u, 132u, 54u, 203u, 33u, 49u, 73u, 9u, 131u, 200u, 76u,
7u, 9u, 36u,};
static unsigned char uvector__00077[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 20u, 65u, 80u, 161u, 13u, 178u,
200u, 143u, 8u, 133u, 37u, 146u, 35u, 105u, 100u, 146u, 71u, 9u, 12u,
12u, 97u, 48u, 67u, 76u, 20u, 8u, 224u, 225u, 129u, 48u, 28u, 35u,
129u, 6u, 4u, 192u, 112u, 142u, 0u, 24u, 19u, 1u, 194u, 64u,};
static unsigned char uvector__00078[] = {
 0u, 3u, 131u, 134u, 8u, 80u, 81u, 3u, 209u, 4u, 18u, 71u, 6u, 12u,
9u, 10u, 17u, 192u, 195u, 2u, 96u, 82u, 71u, 2u, 12u, 9u, 68u, 16u,
71u, 0u, 12u, 9u, 129u, 73u, 12u, 65u, 129u, 140u, 81u, 4u, 66u, 132u,
38u, 3u, 132u, 144u,};
static unsigned char uvector__00079[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 20u, 65u, 116u, 65u, 16u, 161u, 16u,
160u, 162u, 7u, 162u, 8u, 36u, 145u, 194u, 67u, 3u, 24u, 76u, 16u,
211u, 6u, 2u, 56u, 56u, 96u, 76u, 7u, 8u, 224u, 65u, 129u, 48u, 28u,
35u, 128u, 6u, 4u, 192u, 112u, 144u,};
static unsigned char uvector__00080[] = {
 0u, 3u, 136u, 134u, 8u, 218u, 89u, 36u, 113u, 0u, 192u, 146u, 201u,
28u, 60u, 48u, 66u, 148u, 65u, 146u, 201u, 35u, 135u, 6u, 4u, 133u,
8u, 225u, 161u, 129u, 37u, 146u, 56u, 96u, 96u, 74u, 32u, 194u, 56u,
80u, 96u, 76u, 30u, 72u, 225u, 33u, 129u, 144u, 9u, 100u, 71u, 132u,
138u, 32u, 193u, 68u, 15u, 68u, 16u, 73u, 9u, 131u, 200u, 76u, 7u, 9u,
28u, 28u, 48u, 38u, 53u, 228u, 112u, 96u, 192u, 148u, 65u, 4u, 112u,
64u, 192u, 152u, 215u, 145u, 192u, 131u, 2u, 99u, 18u, 71u, 0u, 12u,
9u, 140u, 73u, 12u, 65u, 129u, 140u, 81u, 4u, 66u, 132u, 38u, 45u,
196u, 144u,};
static unsigned char uvector__00081[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 20u, 65u, 180u, 65u, 16u, 161u, 12u,
128u, 75u, 34u, 60u, 36u, 81u, 6u, 10u, 32u, 122u, 32u, 130u, 72u,
133u, 40u, 131u, 37u, 146u, 35u, 105u, 100u, 146u, 71u, 9u, 12u, 12u,
97u, 48u, 67u, 76u, 24u, 8u, 224u, 225u, 129u, 48u, 28u, 35u, 129u,
6u, 4u, 192u, 112u, 142u, 0u, 24u, 19u, 1u, 194u, 64u,};
static unsigned char uvector__00082[] = {
 0u, 3u, 129u, 6u, 10u, 32u, 154u, 32u, 138u, 32u, 34u, 71u, 1u, 12u,
9u, 68u, 4u, 71u, 0u, 12u, 9u, 68u, 16u, 67u, 16u, 96u, 99u, 80u,
152u, 14u, 18u, 64u,};
static unsigned char uvector__00083[] = {
 0u, 3u, 129u, 6u, 10u, 32u, 97u, 50u, 209u, 4u, 209u, 4u, 81u, 1u,
18u, 71u, 0u, 12u, 12u, 106u, 40u, 130u, 104u, 130u, 40u, 128u, 137u,
33u, 136u, 48u, 49u, 138u, 32u, 138u, 32u, 34u, 19u, 1u, 194u, 72u,};
static unsigned char uvector__00084[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 20u, 65u, 212u, 65u, 20u, 64u, 68u,
81u, 3u, 9u, 150u, 136u, 38u, 136u, 34u, 136u, 8u, 146u, 71u, 9u, 12u,
12u, 97u, 48u, 67u, 76u, 25u, 136u, 224u, 225u, 129u, 48u, 28u, 35u,
129u, 6u, 4u, 192u, 112u, 142u, 0u, 24u, 19u, 1u, 194u, 64u,};
static unsigned char uvector__00085[] = {
 0u, 3u, 129u, 6u, 10u, 32u, 249u, 124u, 145u, 192u, 67u, 2u, 81u, 7u,
145u, 192u, 3u, 2u, 75u, 228u, 49u, 6u, 6u, 53u, 9u, 128u, 225u, 36u,};
static unsigned char uvector__00086[] = {
 0u, 3u, 144u, 6u, 10u, 32u, 97u, 50u, 209u, 7u, 203u, 228u, 145u,
199u, 131u, 3u, 26u, 138u, 32u, 249u, 124u, 146u, 56u, 224u, 96u,
103u, 143u, 131u, 8u, 99u, 18u, 252u, 126u, 40u, 131u, 232u, 132u, 9u,
9u, 128u, 225u, 36u, 142u, 44u, 24u, 19u, 12u, 2u, 56u, 144u, 96u,
107u, 162u, 17u, 36u, 113u, 16u, 192u, 148u, 66u, 36u, 113u, 0u, 192u,
200u, 5u, 16u, 120u, 101u, 13u, 116u, 66u, 70u, 136u, 64u, 52u, 81u,
9u, 18u, 69u, 16u, 136u, 101u, 13u, 116u, 66u, 68u, 48u, 169u, 13u,
84u, 66u, 68u, 146u, 27u, 194u, 99u, 32u, 19u, 12u, 2u, 25u, 21u, 49u,
40u, 36u, 112u, 240u, 192u, 153u, 97u, 145u, 194u, 131u, 2u, 101u,
38u, 71u, 9u, 12u, 9u, 68u, 38u, 71u, 7u, 12u, 9u, 146u, 137u, 28u,
12u, 48u, 37u, 16u, 129u, 28u, 4u, 48u, 38u, 68u, 100u, 112u, 0u,
192u, 148u, 66u, 100u, 49u, 6u, 4u, 195u, 128u, 144u,};
static unsigned char uvector__00087[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 20u, 66u, 146u, 252u, 126u, 40u,
131u, 232u, 132u, 9u, 20u, 64u, 194u, 101u, 162u, 15u, 151u, 201u,
36u, 112u, 144u, 192u, 198u, 19u, 4u, 52u, 194u, 0u, 142u, 14u, 24u,
19u, 1u, 194u, 56u, 16u, 96u, 76u, 7u, 8u, 224u, 1u, 129u, 48u, 28u,
36u,};
static unsigned char uvector__00088[] = {
 0u, 3u, 135u, 134u, 10u, 32u, 144u, 202u, 26u, 225u, 83u, 35u, 135u,
89u, 26u, 71u, 133u, 73u, 154u, 73u, 28u, 52u, 48u, 36u, 210u, 71u,
12u, 12u, 9u, 130u, 225u, 28u, 44u, 48u, 36u, 42u, 71u, 10u, 12u, 9u,
35u, 145u, 194u, 3u, 2u, 72u, 196u, 112u, 96u, 192u, 152u, 46u, 17u,
192u, 195u, 2u, 72u, 228u, 112u, 16u, 192u, 152u, 29u, 17u, 192u, 3u,
2u, 66u, 164u, 49u, 6u, 6u, 49u, 35u, 164u, 42u, 19u, 1u, 194u, 72u,};
static unsigned char uvector__00089[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 20u, 66u, 178u, 58u, 66u, 162u,
136u, 36u, 50u, 134u, 184u, 84u, 200u, 225u, 214u, 70u, 145u, 225u,
82u, 102u, 146u, 72u, 225u, 33u, 129u, 140u, 38u, 8u, 105u, 130u,
217u, 28u, 28u, 48u, 38u, 3u, 132u, 112u, 32u, 192u, 152u, 14u, 17u,
192u, 3u, 2u, 96u, 56u, 72u,};
static unsigned char uvector__00090[] = {
 0u, 3u, 130u, 6u, 9u, 2u, 133u, 44u, 145u, 192u, 195u, 2u, 80u, 165u,
145u, 192u, 67u, 3u, 197u, 10u, 88u, 76u, 7u, 9u, 28u, 0u, 48u, 37u,
10u, 89u, 12u, 65u, 129u, 141u, 66u, 97u, 10u, 73u, 0u,};
static unsigned char uvector__00091[] = {
 0u, 3u, 132u, 6u, 4u, 161u, 75u, 35u, 131u, 6u, 9u, 2u, 13u, 36u,
112u, 80u, 192u, 144u, 105u, 28u, 12u, 48u, 38u, 9u, 132u, 112u, 16u,
192u, 241u, 6u, 136u, 166u, 133u, 44u, 38u, 9u, 132u, 145u, 192u, 3u,
2u, 65u, 164u, 49u, 6u, 6u, 53u, 9u, 134u, 185u, 36u,};
static unsigned char uvector__00092[] = {
 0u, 3u, 142u, 6u, 4u, 161u, 75u, 35u, 141u, 6u, 4u, 162u, 22u, 35u,
140u, 134u, 6u, 53u, 15u, 20u, 41u, 98u, 64u, 161u, 75u, 36u, 145u,
198u, 3u, 2u, 81u, 11u, 145u, 197u, 131u, 2u, 96u, 248u, 71u, 21u,
12u, 20u, 67u, 1u, 201u, 67u, 196u, 26u, 34u, 154u, 20u, 177u, 32u,
65u, 164u, 147u, 68u, 4u, 28u, 148u, 38u, 17u, 100u, 145u, 196u, 195u,
2u, 81u, 1u, 17u, 196u, 131u, 2u, 81u, 1u, 17u, 196u, 3u, 2u, 81u, 1u,
17u, 195u, 195u, 2u, 98u, 134u, 71u, 14u, 12u, 9u, 68u, 46u, 71u, 13u,
12u, 9u, 68u, 50u, 71u, 12u, 12u, 12u, 106u, 19u, 22u, 82u, 71u, 11u,
12u, 9u, 68u, 50u, 71u, 9u, 12u, 9u, 153u, 89u, 28u, 32u, 48u, 38u,
40u, 100u, 112u, 96u, 192u, 153u, 149u, 145u, 192u, 195u, 2u, 96u,
248u, 71u, 2u, 12u, 9u, 66u, 150u, 71u, 1u, 12u, 12u, 128u, 80u, 165u,
183u, 18u, 19u, 20u, 50u, 67u, 16u, 96u, 99u, 16u, 109u, 16u, 17u,
20u, 67u, 64u, 72u, 16u, 105u, 52u, 64u, 68u, 146u,};
static unsigned char uvector__00093[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 20u, 64u, 48u, 109u, 16u, 17u, 20u,
67u, 64u, 72u, 16u, 105u, 52u, 64u, 68u, 145u, 194u, 67u, 3u, 24u,
76u, 16u, 211u, 6u, 2u, 56u, 56u, 96u, 76u, 7u, 8u, 224u, 65u, 129u,
48u, 28u, 35u, 128u, 6u, 4u, 192u, 112u, 144u,};
static unsigned char uvector__00094[] = {
 0u, 3u, 130u, 6u, 9u, 42u, 133u, 44u, 145u, 192u, 195u, 2u, 80u,
165u, 145u, 192u, 67u, 3u, 197u, 10u, 88u, 76u, 7u, 9u, 28u, 0u, 48u,
37u, 10u, 89u, 12u, 65u, 129u, 141u, 66u, 97u, 10u, 73u, 0u,};
static unsigned char uvector__00095[] = {
 0u, 3u, 132u, 6u, 4u, 161u, 75u, 35u, 131u, 6u, 9u, 42u, 13u, 36u,
112u, 80u, 192u, 144u, 105u, 28u, 12u, 48u, 38u, 9u, 132u, 112u, 16u,
192u, 241u, 6u, 136u, 166u, 133u, 44u, 38u, 9u, 132u, 145u, 192u, 3u,
2u, 65u, 164u, 49u, 6u, 6u, 53u, 9u, 134u, 185u, 36u,};
static unsigned char uvector__00096[] = {
 0u, 3u, 142u, 6u, 4u, 161u, 75u, 35u, 141u, 6u, 4u, 162u, 22u, 35u,
140u, 134u, 6u, 53u, 15u, 20u, 41u, 98u, 74u, 161u, 75u, 36u, 145u,
198u, 3u, 2u, 81u, 11u, 145u, 197u, 131u, 2u, 96u, 248u, 71u, 21u,
12u, 20u, 67u, 1u, 201u, 67u, 196u, 26u, 34u, 154u, 20u, 177u, 37u,
65u, 164u, 147u, 68u, 4u, 28u, 148u, 38u, 17u, 100u, 145u, 196u, 195u,
2u, 81u, 1u, 17u, 196u, 131u, 2u, 81u, 1u, 17u, 196u, 3u, 2u, 81u, 1u,
17u, 195u, 195u, 2u, 98u, 134u, 71u, 14u, 12u, 9u, 68u, 46u, 71u, 13u,
12u, 9u, 68u, 50u, 71u, 12u, 12u, 12u, 106u, 19u, 22u, 82u, 71u, 11u,
12u, 9u, 68u, 50u, 71u, 9u, 12u, 9u, 153u, 89u, 28u, 32u, 48u, 38u,
40u, 100u, 112u, 96u, 192u, 153u, 149u, 145u, 192u, 195u, 2u, 96u,
248u, 71u, 2u, 12u, 9u, 66u, 150u, 71u, 1u, 12u, 12u, 128u, 80u, 165u,
183u, 18u, 19u, 20u, 50u, 67u, 16u, 96u, 99u, 16u, 109u, 16u, 17u,
20u, 67u, 64u, 73u, 80u, 105u, 52u, 64u, 68u, 146u,};
static unsigned char uvector__00097[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 20u, 64u, 144u, 109u, 16u, 17u, 20u,
67u, 64u, 73u, 80u, 105u, 52u, 64u, 68u, 145u, 194u, 67u, 3u, 24u,
76u, 16u, 211u, 6u, 2u, 56u, 56u, 96u, 76u, 7u, 8u, 224u, 65u, 129u,
48u, 28u, 35u, 128u, 6u, 4u, 192u, 112u, 144u,};
static unsigned char uvector__00098[] = {
 0u, 3u, 130u, 6u, 10u, 33u, 186u, 20u, 178u, 71u, 3u, 12u, 9u, 66u,
150u, 71u, 1u, 12u, 15u, 20u, 41u, 97u, 48u, 28u, 36u, 112u, 0u, 192u,
148u, 41u, 100u, 49u, 6u, 6u, 53u, 9u, 132u, 89u, 36u,};
static unsigned char uvector__00099[] = {
 0u, 3u, 132u, 6u, 4u, 161u, 75u, 35u, 131u, 6u, 10u, 33u, 184u, 52u,
145u, 193u, 67u, 2u, 65u, 164u, 112u, 48u, 192u, 152u, 38u, 17u, 192u,
67u, 3u, 196u, 26u, 34u, 154u, 20u, 176u, 152u, 38u, 18u, 71u, 0u,
12u, 9u, 6u, 144u, 196u, 24u, 24u, 212u, 38u, 27u, 164u, 144u,};
static unsigned char uvector__00100[] = {
 0u, 3u, 142u, 6u, 4u, 161u, 75u, 35u, 141u, 6u, 4u, 162u, 22u, 35u,
140u, 134u, 6u, 53u, 15u, 20u, 41u, 98u, 136u, 110u, 133u, 44u, 146u,
71u, 24u, 12u, 9u, 68u, 46u, 71u, 22u, 12u, 9u, 131u, 225u, 28u, 84u,
48u, 81u, 12u, 7u, 37u, 15u, 16u, 104u, 138u, 104u, 82u, 197u, 16u,
220u, 26u, 73u, 52u, 64u, 65u, 201u, 66u, 97u, 22u, 73u, 28u, 76u,
48u, 37u, 16u, 17u, 28u, 72u, 48u, 37u, 16u, 17u, 28u, 64u, 48u, 37u,
16u, 17u, 28u, 60u, 48u, 38u, 41u, 36u, 112u, 224u, 192u, 148u, 66u,
228u, 112u, 208u, 192u, 148u, 67u, 36u, 112u, 192u, 192u, 198u, 161u,
49u, 107u, 36u, 112u, 176u, 192u, 148u, 67u, 36u, 112u, 144u, 192u,
153u, 155u, 145u, 194u, 3u, 2u, 98u, 146u, 71u, 6u, 12u, 9u, 153u,
185u, 28u, 12u, 48u, 38u, 15u, 132u, 112u, 32u, 192u, 148u, 41u, 100u,
112u, 16u, 192u, 200u, 5u, 10u, 91u, 113u, 33u, 49u, 73u, 36u, 49u,
6u, 6u, 49u, 6u, 209u, 1u, 17u, 68u, 52u, 5u, 16u, 220u, 26u, 77u,
16u, 17u, 36u, 128u,};
static unsigned char uvector__00101[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 20u, 67u, 144u, 109u, 16u, 17u, 20u,
67u, 64u, 81u, 13u, 193u, 164u, 209u, 1u, 18u, 71u, 9u, 12u, 12u, 97u,
48u, 67u, 76u, 24u, 8u, 224u, 225u, 129u, 48u, 28u, 35u, 129u, 6u, 4u,
192u, 112u, 142u, 0u, 24u, 19u, 1u, 194u, 64u,};
static unsigned char uvector__00102[] = {
 0u, 3u, 140u, 6u, 10u, 33u, 186u, 20u, 178u, 71u, 23u, 12u, 9u, 66u,
150u, 71u, 21u, 12u, 15u, 20u, 41u, 97u, 48u, 28u, 36u, 113u, 64u,
192u, 148u, 41u, 100u, 113u, 0u, 193u, 37u, 80u, 165u, 146u, 56u,
120u, 96u, 74u, 20u, 178u, 56u, 104u, 96u, 76u, 65u, 200u, 225u, 97u,
129u, 226u, 133u, 44u, 38u, 32u, 228u, 142u, 20u, 24u, 18u, 133u, 44u,
142u, 12u, 24u, 36u, 10u, 20u, 178u, 71u, 5u, 12u, 9u, 66u, 150u, 71u,
3u, 12u, 9u, 144u, 209u, 28u, 4u, 48u, 60u, 80u, 165u, 132u, 200u,
104u, 145u, 192u, 3u, 2u, 80u, 165u, 144u, 196u, 24u, 24u, 212u, 38u,
86u, 33u, 49u, 158u, 9u, 132u, 89u, 36u,};
static unsigned char uvector__00103[] = {
 0u, 3u, 143u, 6u, 4u, 161u, 75u, 35u, 142u, 6u, 10u, 33u, 186u, 33u,
210u, 71u, 27u, 12u, 9u, 68u, 58u, 71u, 25u, 12u, 9u, 130u, 97u, 28u,
92u, 48u, 60u, 81u, 14u, 136u, 166u, 133u, 44u, 38u, 9u, 132u, 145u,
197u, 131u, 2u, 81u, 14u, 145u, 196u, 195u, 2u, 80u, 165u, 145u, 196u,
67u, 4u, 149u, 68u, 60u, 72u, 226u, 1u, 129u, 40u, 135u, 136u, 225u,
193u, 129u, 49u, 181u, 35u, 134u, 6u, 7u, 138u, 33u, 225u, 20u, 208u,
165u, 132u, 198u, 212u, 146u, 56u, 88u, 96u, 74u, 33u, 226u, 56u, 64u,
96u, 74u, 20u, 178u, 56u, 48u, 96u, 144u, 40u, 135u, 201u, 28u, 20u,
48u, 37u, 16u, 249u, 28u, 12u, 48u, 38u, 99u, 4u, 112u, 16u, 192u,
241u, 68u, 62u, 34u, 154u, 20u, 176u, 153u, 140u, 18u, 71u, 0u, 12u,
9u, 68u, 62u, 67u, 16u, 96u, 99u, 80u, 153u, 215u, 132u, 201u, 48u,
38u, 29u, 36u, 144u,};
static unsigned char uvector__00104[] = {
 0u, 3u, 144u, 6u, 4u, 161u, 75u, 35u, 143u, 6u, 4u, 162u, 22u, 35u,
142u, 134u, 6u, 53u, 15u, 20u, 41u, 98u, 64u, 161u, 75u, 36u, 60u,
80u, 165u, 137u, 42u, 133u, 44u, 144u, 241u, 66u, 150u, 40u, 134u,
232u, 82u, 201u, 36u, 113u, 192u, 192u, 148u, 66u, 228u, 113u, 160u,
192u, 152u, 62u, 17u, 198u, 67u, 5u, 16u, 192u, 114u, 80u, 241u, 68u,
62u, 34u, 154u, 20u, 177u, 32u, 81u, 15u, 146u, 67u, 197u, 16u, 240u,
138u, 104u, 82u, 196u, 149u, 68u, 60u, 73u, 15u, 20u, 67u, 162u, 41u,
161u, 75u, 20u, 67u, 116u, 67u, 164u, 147u, 68u, 4u, 28u, 148u, 38u,
17u, 97u, 48u, 201u, 9u, 136u, 57u, 36u, 113u, 112u, 192u, 148u, 64u,
68u, 113u, 96u, 192u, 148u, 64u, 68u, 113u, 64u, 192u, 148u, 64u, 68u,
113u, 48u, 192u, 152u, 226u, 145u, 196u, 131u, 2u, 81u, 11u, 145u,
196u, 67u, 2u, 81u, 12u, 145u, 196u, 3u, 3u, 26u, 132u, 199u, 156u,
38u, 72u, 161u, 50u, 163u, 36u, 112u, 240u, 192u, 148u, 67u, 36u,
112u, 208u, 192u, 154u, 18u, 138u, 71u, 12u, 12u, 9u, 142u, 41u, 28u,
40u, 48u, 38u, 132u, 162u, 145u, 193u, 195u, 2u, 96u, 248u, 71u, 6u,
12u, 9u, 66u, 150u, 71u, 5u, 12u, 9u, 66u, 150u, 71u, 4u, 12u, 9u,
66u, 150u, 71u, 3u, 12u, 12u, 128u, 80u, 165u, 183u, 17u, 66u, 150u,
220u, 69u, 10u, 91u, 113u, 33u, 49u, 197u, 36u, 49u, 6u, 6u, 49u, 68u,
63u, 68u, 61u, 68u, 59u, 68u, 4u, 69u, 16u, 208u, 18u, 5u, 16u, 249u,
18u, 85u, 16u, 241u, 20u, 67u, 116u, 67u, 164u, 209u, 1u, 18u, 72u,};
static unsigned char uvector__00105[] = {
 0u, 3u, 134u, 6u, 8u, 114u, 29u, 20u, 72u, 20u, 67u, 244u, 67u, 212u,
67u, 180u, 64u, 68u, 81u, 13u, 1u, 32u, 81u, 15u, 145u, 37u, 81u, 15u,
17u, 68u, 55u, 68u, 58u, 77u, 16u, 17u, 36u, 112u, 144u, 192u, 198u,
19u, 4u, 52u, 194u, 24u, 142u, 14u, 24u, 19u, 1u, 194u, 56u, 16u, 96u,
76u, 7u, 8u, 224u, 1u, 129u, 48u, 28u, 36u,};
static unsigned char uvector__00106[] = {
 0u, 3u, 131u, 6u, 10u, 33u, 98u, 71u, 4u, 12u, 20u, 72u, 52u, 72u,
68u, 142u, 0u, 24u, 19u, 4u, 178u, 24u, 131u, 3u, 24u, 162u, 67u,
131u, 104u, 145u, 8u, 76u, 18u, 194u, 96u, 56u, 73u, 0u,};
static unsigned char uvector__00107[] = {
 0u, 3u, 130u, 6u, 10u, 36u, 80u, 195u, 68u, 140u, 67u, 144u, 162u,
67u, 131u, 104u, 145u, 8u, 162u, 65u, 162u, 66u, 34u, 136u, 88u, 146u,
71u, 2u, 12u, 12u, 97u, 48u, 88u, 9u, 132u, 64u, 76u, 43u, 137u, 32u,};
static unsigned char uvector__00108[] = {
 0u, 3u, 132u, 134u, 10u, 33u, 98u, 71u, 8u, 12u, 2u, 32u, 15u, 241u,
85u, 18u, 57u, 48u, 107u, 241u, 35u, 131u, 134u, 4u, 193u, 56u, 142u,
10u, 24u, 18u, 137u, 28u, 142u, 4u, 24u, 18u, 13u, 35u, 128u, 6u, 4u,
193u, 44u, 134u, 32u, 192u, 198u, 40u, 144u, 224u, 218u, 36u, 66u,
34u, 144u, 19u, 5u, 184u, 52u, 191u, 16u, 152u, 14u, 18u, 64u,};
static unsigned char uvector__00109[] = {
 0u, 3u, 130u, 6u, 10u, 36u, 80u, 195u, 68u, 144u, 67u, 144u, 162u,
67u, 131u, 104u, 145u, 8u, 138u, 64u, 127u, 138u, 168u, 145u, 204u,
26u, 95u, 136u, 162u, 22u, 36u, 145u, 192u, 131u, 3u, 24u, 76u, 22u,
2u, 97u, 16u, 19u, 13u, 130u, 72u,};
static unsigned char uvector__00110[] = {
 0u, 3u, 132u, 6u, 10u, 33u, 98u, 71u, 7u, 12u, 2u, 32u, 15u, 241u,
85u, 18u, 57u, 48u, 107u, 113u, 35u, 131u, 6u, 4u, 193u, 56u, 142u,
8u, 24u, 18u, 137u, 28u, 142u, 4u, 24u, 18u, 13u, 35u, 128u, 6u, 4u,
193u, 44u, 134u, 32u, 192u, 198u, 40u, 144u, 224u, 218u, 36u, 66u,
34u, 144u, 19u, 5u, 184u, 52u, 183u, 16u, 152u, 14u, 18u, 64u,};
static unsigned char uvector__00111[] = {
 0u, 3u, 130u, 6u, 10u, 36u, 80u, 195u, 68u, 146u, 67u, 144u, 162u,
67u, 131u, 104u, 145u, 8u, 138u, 64u, 127u, 138u, 168u, 145u, 204u,
26u, 91u, 136u, 162u, 22u, 36u, 145u, 192u, 131u, 3u, 24u, 76u, 22u,
2u, 97u, 16u, 19u, 13u, 130u, 72u,};
static unsigned char uvector__00112[] = {
 0u, 3u, 132u, 134u, 10u, 33u, 98u, 71u, 7u, 12u, 20u, 73u, 80u, 104u,
97u, 162u, 75u, 33u, 134u, 137u, 48u, 146u, 56u, 16u, 96u, 72u, 52u,
142u, 0u, 24u, 19u, 4u, 178u, 24u, 131u, 3u, 24u, 162u, 67u, 131u,
104u, 145u, 8u, 76u, 18u, 194u, 96u, 56u, 73u, 0u,};
static unsigned char uvector__00113[] = {
 0u, 3u, 130u, 6u, 10u, 36u, 80u, 195u, 68u, 154u, 67u, 144u, 162u,
67u, 131u, 104u, 145u, 8u, 162u, 74u, 131u, 67u, 13u, 18u, 89u, 12u,
52u, 73u, 132u, 138u, 33u, 98u, 73u, 28u, 8u, 48u, 49u, 132u, 193u,
96u, 38u, 17u, 1u, 48u, 228u, 36u, 128u,};
static unsigned char uvector__00114[] = {
 0u, 3u, 132u, 134u, 10u, 33u, 98u, 71u, 7u, 12u, 20u, 73u, 80u, 104u,
97u, 162u, 75u, 33u, 134u, 137u, 56u, 146u, 56u, 16u, 96u, 72u, 52u,
142u, 0u, 24u, 19u, 4u, 178u, 24u, 131u, 3u, 24u, 162u, 67u, 131u,
104u, 145u, 8u, 76u, 18u, 194u, 96u, 56u, 73u, 0u,};
static unsigned char uvector__00115[] = {
 0u, 3u, 130u, 6u, 10u, 36u, 80u, 195u, 68u, 158u, 67u, 144u, 162u,
67u, 131u, 104u, 145u, 8u, 162u, 74u, 131u, 67u, 13u, 18u, 89u, 12u,
52u, 73u, 196u, 138u, 33u, 98u, 73u, 28u, 8u, 48u, 49u, 132u, 193u,
96u, 38u, 17u, 1u, 48u, 228u, 36u, 128u,};
static ScmObj SCM_debug_info_const_vector();
#if defined(__CYGWIN__) || defined(GAUCHE_WINDOWS)
#define SCM_CGEN_CONST /*empty*/
#else
#define SCM_CGEN_CONST const
#endif
static SCM_CGEN_CONST struct scm__scRec {
  ScmString d1785[412];
} scm__sc SCM_UNUSED = {
  {   /* ScmString d1785 */
      SCM_STRING_CONST_INITIALIZER("input-port\077", 11, 11),
      SCM_STRING_CONST_INITIALIZER("obj", 3, 3),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libio.scm", 9, 9),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("scheme", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<top>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<boolean>", 9, 9),
      SCM_STRING_CONST_INITIALIZER("output-port\077", 12, 12),
      SCM_STRING_CONST_INITIALIZER("port\077", 5, 5),
      SCM_STRING_CONST_INITIALIZER("port-closed\077", 12, 12),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<port>", 6, 6),
      SCM_STRING_CONST_INITIALIZER("standard-input-port", 19, 19),
      SCM_STRING_CONST_INITIALIZER("optional", 8, 8),
      SCM_STRING_CONST_INITIALIZER("p", 1, 1),
      SCM_STRING_CONST_INITIALIZER("*", 1, 1),
      SCM_STRING_CONST_INITIALIZER("standard-output-port", 20, 20),
      SCM_STRING_CONST_INITIALIZER("standard-error-port", 19, 19),
      SCM_STRING_CONST_INITIALIZER("port-name", 9, 9),
      SCM_STRING_CONST_INITIALIZER("port", 4, 4),
      SCM_STRING_CONST_INITIALIZER("port-current-line", 17, 17),
      SCM_STRING_CONST_INITIALIZER("<fixnum>", 8, 8),
      SCM_STRING_CONST_INITIALIZER("port-file-number", 16, 16),
      SCM_STRING_CONST_INITIALIZER("dup\077", 4, 4),
      SCM_STRING_CONST_INITIALIZER("port-fd-dup!", 12, 12),
      SCM_STRING_CONST_INITIALIZER("dst", 3, 3),
      SCM_STRING_CONST_INITIALIZER("src", 3, 3),
      SCM_STRING_CONST_INITIALIZER("<void>", 6, 6),
      SCM_STRING_CONST_INITIALIZER("port-attribute-set!", 19, 19),
      SCM_STRING_CONST_INITIALIZER("key", 3, 3),
      SCM_STRING_CONST_INITIALIZER("val", 3, 3),
      SCM_STRING_CONST_INITIALIZER("port-attribute-ref", 18, 18),
      SCM_STRING_CONST_INITIALIZER("fallback", 8, 8),
      SCM_STRING_CONST_INITIALIZER("port-attribute-delete!", 22, 22),
      SCM_STRING_CONST_INITIALIZER("port-attributes", 15, 15),
      SCM_STRING_CONST_INITIALIZER("port-type", 9, 9),
      SCM_STRING_CONST_INITIALIZER("file", 4, 4),
      SCM_STRING_CONST_INITIALIZER("proc", 4, 4),
      SCM_STRING_CONST_INITIALIZER("string", 6, 6),
      SCM_STRING_CONST_INITIALIZER("port-buffering", 14, 14),
      SCM_STRING_CONST_INITIALIZER("(setter port-buffering)", 23, 23),
      SCM_STRING_CONST_INITIALIZER("port-link!", 10, 10),
      SCM_STRING_CONST_INITIALIZER("iport", 5, 5),
      SCM_STRING_CONST_INITIALIZER("oport", 5, 5),
      SCM_STRING_CONST_INITIALIZER("<input-port>", 12, 12),
      SCM_STRING_CONST_INITIALIZER("<output-port>", 13, 13),
      SCM_STRING_CONST_INITIALIZER("port-unlink!", 12, 12),
      SCM_STRING_CONST_INITIALIZER("port-case-fold", 14, 14),
      SCM_STRING_CONST_INITIALIZER("(setter port-case-fold)", 23, 23),
      SCM_STRING_CONST_INITIALIZER("gauche.internal", 15, 15),
      SCM_STRING_CONST_INITIALIZER("close-input-port", 16, 16),
      SCM_STRING_CONST_INITIALIZER("close-output-port", 17, 17),
      SCM_STRING_CONST_INITIALIZER("close-port", 10, 10),
      SCM_STRING_CONST_INITIALIZER("error", 5, 5),
      SCM_STRING_CONST_INITIALIZER("if-does-not-exist", 17, 17),
      SCM_STRING_CONST_INITIALIZER("buffering", 9, 9),
      SCM_STRING_CONST_INITIALIZER("binary", 6, 6),
      SCM_STRING_CONST_INITIALIZER("element-type", 12, 12),
      SCM_STRING_CONST_INITIALIZER("%open-input-file", 16, 16),
      SCM_STRING_CONST_INITIALIZER("character", 9, 9),
      SCM_STRING_CONST_INITIALIZER("path", 4, 4),
      SCM_STRING_CONST_INITIALIZER("<string>", 8, 8),
      SCM_STRING_CONST_INITIALIZER("supersede", 9, 9),
      SCM_STRING_CONST_INITIALIZER("if-exists", 9, 9),
      SCM_STRING_CONST_INITIALIZER("create", 6, 6),
      SCM_STRING_CONST_INITIALIZER("mode", 4, 4),
      SCM_STRING_CONST_INITIALIZER("%open-output-file", 17, 17),
      SCM_STRING_CONST_INITIALIZER("overwrite", 9, 9),
      SCM_STRING_CONST_INITIALIZER("append", 6, 6),
      SCM_STRING_CONST_INITIALIZER("owner\077", 6, 6),
      SCM_STRING_CONST_INITIALIZER("name", 4, 4),
      SCM_STRING_CONST_INITIALIZER("open-input-fd-port", 18, 18),
      SCM_STRING_CONST_INITIALIZER("dup", 3, 3),
      SCM_STRING_CONST_INITIALIZER("fd", 2, 2),
      SCM_STRING_CONST_INITIALIZER("open-output-fd-port", 19, 19),
      SCM_STRING_CONST_INITIALIZER("open-input-buffered-port", 24, 24),
      SCM_STRING_CONST_INITIALIZER("filler", 6, 6),
      SCM_STRING_CONST_INITIALIZER("buffer-size", 11, 11),
      SCM_STRING_CONST_INITIALIZER("<procedure>", 11, 11),
      SCM_STRING_CONST_INITIALIZER("open-output-buffered-port", 25, 25),
      SCM_STRING_CONST_INITIALIZER("flusher", 7, 7),
      SCM_STRING_CONST_INITIALIZER("private\077", 8, 8),
      SCM_STRING_CONST_INITIALIZER("(input string port)", 19, 19),
      SCM_STRING_CONST_INITIALIZER("open-input-string", 17, 17),
      SCM_STRING_CONST_INITIALIZER("(output string port)", 20, 20),
      SCM_STRING_CONST_INITIALIZER("open-output-string", 18, 18),
      SCM_STRING_CONST_INITIALIZER("get-output-string", 17, 17),
      SCM_STRING_CONST_INITIALIZER("get-output-byte-string", 22, 22),
      SCM_STRING_CONST_INITIALIZER("get-remaining-input-string", 26, 26),
      SCM_STRING_CONST_INITIALIZER("open-coding-aware-port", 22, 22),
      SCM_STRING_CONST_INITIALIZER("port-has-port-position\077", 23, 23),
      SCM_STRING_CONST_INITIALIZER("port-has-set-port-position!\077", 28, 28),
      SCM_STRING_CONST_INITIALIZER("port-position", 13, 13),
      SCM_STRING_CONST_INITIALIZER("set-port-position!", 18, 18),
      SCM_STRING_CONST_INITIALIZER("pos", 3, 3),
      SCM_STRING_CONST_INITIALIZER("SEEK_SET", 8, 8),
      SCM_STRING_CONST_INITIALIZER("SEEK_CUR", 8, 8),
      SCM_STRING_CONST_INITIALIZER("SEEK_END", 8, 8),
      SCM_STRING_CONST_INITIALIZER("port-seek", 9, 9),
      SCM_STRING_CONST_INITIALIZER("offset", 6, 6),
      SCM_STRING_CONST_INITIALIZER("whence", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<integer>", 9, 9),
      SCM_STRING_CONST_INITIALIZER("%expression-name-mark-key", 25, 25),
      SCM_STRING_CONST_INITIALIZER("port-tell", 9, 9),
      SCM_STRING_CONST_INITIALIZER("%toplevel", 9, 9),
      SCM_STRING_CONST_INITIALIZER("port-position-prefix", 20, 20),
      SCM_STRING_CONST_INITIALIZER("positive\077", 9, 9),
      SCM_STRING_CONST_INITIALIZER("~s:line ~a: ", 12, 12),
      SCM_STRING_CONST_INITIALIZER("format-internal", 15, 15),
      SCM_STRING_CONST_INITIALIZER("gauche.format", 13, 13),
      SCM_STRING_CONST_INITIALIZER("~s: ", 4, 4),
      SCM_STRING_CONST_INITIALIZER("", 0, 0),
      SCM_STRING_CONST_INITIALIZER("\077\077\077", 3, 3),
      SCM_STRING_CONST_INITIALIZER("%port-walking\077", 14, 14),
      SCM_STRING_CONST_INITIALIZER("(setter %port-walking\077)", 23, 23),
      SCM_STRING_CONST_INITIALIZER("%port-writing-shared\077", 21, 21),
      SCM_STRING_CONST_INITIALIZER("(setter %port-writing-shared\077)", 30, 30),
      SCM_STRING_CONST_INITIALIZER("%port-write-state", 17, 17),
      SCM_STRING_CONST_INITIALIZER("(setter %port-write-state)", 26, 26),
      SCM_STRING_CONST_INITIALIZER("%port-lock!", 11, 11),
      SCM_STRING_CONST_INITIALIZER("%port-unlock!", 13, 13),
      SCM_STRING_CONST_INITIALIZER("with-port-locking", 17, 17),
      SCM_STRING_CONST_INITIALIZER("%unwind-protect", 15, 15),
      SCM_STRING_CONST_INITIALIZER("args", 4, 4),
      SCM_STRING_CONST_INITIALIZER("%with-2pass-setup", 17, 17),
      SCM_STRING_CONST_INITIALIZER("[internal] %with-2pass-setup called recursively on port:", 56, 56),
      SCM_STRING_CONST_INITIALIZER("<write-state>", 13, 13),
      SCM_STRING_CONST_INITIALIZER("shared-table", 12, 12),
      SCM_STRING_CONST_INITIALIZER("eq\077", 3, 3),
      SCM_STRING_CONST_INITIALIZER("make-hash-table", 15, 15),
      SCM_STRING_CONST_INITIALIZER("make", 4, 4),
      SCM_STRING_CONST_INITIALIZER("walker", 6, 6),
      SCM_STRING_CONST_INITIALIZER("emitter", 7, 7),
      SCM_STRING_CONST_INITIALIZER("port-column", 11, 11),
      SCM_STRING_CONST_INITIALIZER("read", 4, 4),
      SCM_STRING_CONST_INITIALIZER("read-char", 9, 9),
      SCM_STRING_CONST_INITIALIZER("peek-char", 9, 9),
      SCM_STRING_CONST_INITIALIZER("eof-object\077", 11, 11),
      SCM_STRING_CONST_INITIALIZER("char-ready\077", 11, 11),
      SCM_STRING_CONST_INITIALIZER("eof-object", 10, 10),
      SCM_STRING_CONST_INITIALIZER("byte-ready\077", 11, 11),
      SCM_STRING_CONST_INITIALIZER("u8-ready\077", 9, 9),
      SCM_STRING_CONST_INITIALIZER("read-byte", 9, 9),
      SCM_STRING_CONST_INITIALIZER("read-u8", 7, 7),
      SCM_STRING_CONST_INITIALIZER("peek-byte", 9, 9),
      SCM_STRING_CONST_INITIALIZER("peek-u8", 7, 7),
      SCM_STRING_CONST_INITIALIZER("read-line", 9, 9),
      SCM_STRING_CONST_INITIALIZER("allowbytestr", 12, 12),
      SCM_STRING_CONST_INITIALIZER("read-string", 11, 11),
      SCM_STRING_CONST_INITIALIZER("too many arguments for", 22, 22),
      SCM_STRING_CONST_INITIALIZER("lambda", 6, 6),
      SCM_STRING_CONST_INITIALIZER("n", 1, 1),
      SCM_STRING_CONST_INITIALIZER("current-input-port", 18, 18),
      SCM_STRING_CONST_INITIALIZER("define", 6, 6),
      SCM_STRING_CONST_INITIALIZER("o", 1, 1),
      SCM_STRING_CONST_INITIALIZER("let", 3, 3),
      SCM_STRING_CONST_INITIALIZER("loop", 4, 4),
      SCM_STRING_CONST_INITIALIZER("i", 1, 1),
      SCM_STRING_CONST_INITIALIZER("if", 2, 2),
      SCM_STRING_CONST_INITIALIZER(">=", 2, 2),
      SCM_STRING_CONST_INITIALIZER("let1", 4, 4),
      SCM_STRING_CONST_INITIALIZER("c", 1, 1),
      SCM_STRING_CONST_INITIALIZER("=", 1, 1),
      SCM_STRING_CONST_INITIALIZER("begin", 5, 5),
      SCM_STRING_CONST_INITIALIZER("write-char", 10, 10),
      SCM_STRING_CONST_INITIALIZER("+", 1, 1),
      SCM_STRING_CONST_INITIALIZER("write-string", 12, 12),
      SCM_STRING_CONST_INITIALIZER("current-output-port", 19, 19),
      SCM_STRING_CONST_INITIALIZER("start", 5, 5),
      SCM_STRING_CONST_INITIALIZER("end", 3, 3),
      SCM_STRING_CONST_INITIALIZER("display", 7, 7),
      SCM_STRING_CONST_INITIALIZER("opt-substring", 13, 13),
      SCM_STRING_CONST_INITIALIZER("consume-trailing-whitespaces", 28, 28),
      SCM_STRING_CONST_INITIALIZER("when", 4, 4),
      SCM_STRING_CONST_INITIALIZER("b", 1, 1),
      SCM_STRING_CONST_INITIALIZER("cond", 4, 4),
      SCM_STRING_CONST_INITIALIZER("memv", 4, 4),
      SCM_STRING_CONST_INITIALIZER("quote", 5, 5),
      SCM_STRING_CONST_INITIALIZER("eqv\077", 4, 4),
      SCM_STRING_CONST_INITIALIZER("and", 3, 3),
      SCM_STRING_CONST_INITIALIZER("char-word-constituent\077", 22, 22),
      SCM_STRING_CONST_INITIALIZER("ch", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<char>", 6, 6),
      SCM_STRING_CONST_INITIALIZER("read-block", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bytes", 5, 5),
      SCM_STRING_CONST_INITIALIZER("read-list", 9, 9),
      SCM_STRING_CONST_INITIALIZER("closer", 6, 6),
      SCM_STRING_CONST_INITIALIZER("port->byte-string", 17, 17),
      SCM_STRING_CONST_INITIALIZER("port->string", 12, 12),
      SCM_STRING_CONST_INITIALIZER("unit", 4, 4),
      SCM_STRING_CONST_INITIALIZER("byte", 4, 4),
      SCM_STRING_CONST_INITIALIZER("copy-port", 9, 9),
      SCM_STRING_CONST_INITIALIZER("port->list", 10, 10),
      SCM_STRING_CONST_INITIALIZER("reverse!", 8, 8),
      SCM_STRING_CONST_INITIALIZER("reader", 6, 6),
      SCM_STRING_CONST_INITIALIZER("port->string-list", 17, 17),
      SCM_STRING_CONST_INITIALIZER("G1815", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1814", 5, 5),
      SCM_STRING_CONST_INITIALIZER("port->sexp-list", 15, 15),
      SCM_STRING_CONST_INITIALIZER("reader-lexical-mode", 19, 19),
      SCM_STRING_CONST_INITIALIZER("k", 1, 1),
      SCM_STRING_CONST_INITIALIZER("%port-ungotten-chars", 20, 20),
      SCM_STRING_CONST_INITIALIZER("%port-ungotten-bytes", 20, 20),
      SCM_STRING_CONST_INITIALIZER("define-reader-ctor", 18, 18),
      SCM_STRING_CONST_INITIALIZER("symbol", 6, 6),
      SCM_STRING_CONST_INITIALIZER("finisher", 8, 8),
      SCM_STRING_CONST_INITIALIZER("%get-reader-ctor", 16, 16),
      SCM_STRING_CONST_INITIALIZER("define-reader-directive", 23, 23),
      SCM_STRING_CONST_INITIALIZER("current-read-context", 20, 20),
      SCM_STRING_CONST_INITIALIZER("ctx", 3, 3),
      SCM_STRING_CONST_INITIALIZER("read-reference\077", 15, 15),
      SCM_STRING_CONST_INITIALIZER("read-reference-has-value\077", 25, 25),
      SCM_STRING_CONST_INITIALIZER("ref", 3, 3),
      SCM_STRING_CONST_INITIALIZER("<read-reference>", 16, 16),
      SCM_STRING_CONST_INITIALIZER("read-reference-value", 20, 20),
      SCM_STRING_CONST_INITIALIZER("read-with-shared-structure", 26, 26),
      SCM_STRING_CONST_INITIALIZER("read/ss", 7, 7),
      SCM_STRING_CONST_INITIALIZER("write", 5, 5),
      SCM_STRING_CONST_INITIALIZER("port-or-control-1", 17, 17),
      SCM_STRING_CONST_INITIALIZER("port-or-control-2", 17, 17),
      SCM_STRING_CONST_INITIALIZER("write-simple", 12, 12),
      SCM_STRING_CONST_INITIALIZER("write-shared", 12, 12),
      SCM_STRING_CONST_INITIALIZER("newline", 7, 7),
      SCM_STRING_CONST_INITIALIZER("fresh-line", 10, 10),
      SCM_STRING_CONST_INITIALIZER("write-byte", 10, 10),
      SCM_STRING_CONST_INITIALIZER("<int>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("write-u8", 8, 8),
      SCM_STRING_CONST_INITIALIZER("write-limited", 13, 13),
      SCM_STRING_CONST_INITIALIZER("limit", 5, 5),
      SCM_STRING_CONST_INITIALIZER("write*", 6, 6),
      SCM_STRING_CONST_INITIALIZER("flush", 5, 5),
      SCM_STRING_CONST_INITIALIZER("flush-all-ports", 15, 15),
      SCM_STRING_CONST_INITIALIZER("write-need-recurse\077", 19, 19),
      SCM_STRING_CONST_INITIALIZER("write-walk", 10, 10),
      SCM_STRING_CONST_INITIALIZER("~", 1, 1),
      SCM_STRING_CONST_INITIALIZER("%write-walk-rec", 15, 15),
      SCM_STRING_CONST_INITIALIZER("G1819", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1816", 5, 5),
      SCM_STRING_CONST_INITIALIZER("hash-table-exists\077", 18, 18),
      SCM_STRING_CONST_INITIALIZER("hash-table-update!", 18, 18),
      SCM_STRING_CONST_INITIALIZER("hash-table-put!", 15, 15),
      SCM_STRING_CONST_INITIALIZER("uvector\077", 8, 8),
      SCM_STRING_CONST_INITIALIZER("box\077", 4, 4),
      SCM_STRING_CONST_INITIALIZER("box-arity", 9, 9),
      SCM_STRING_CONST_INITIALIZER("unbox-value", 11, 11),
      SCM_STRING_CONST_INITIALIZER("write-object", 12, 12),
      SCM_STRING_CONST_INITIALIZER("hash-table-get", 14, 14),
      SCM_STRING_CONST_INITIALIZER("hash-table-delete!", 18, 18),
      SCM_STRING_CONST_INITIALIZER("tab", 3, 3),
      SCM_STRING_CONST_INITIALIZER("write-with-shared-structure", 27, 27),
      SCM_STRING_CONST_INITIALIZER("write/ss", 8, 8),
      SCM_STRING_CONST_INITIALIZER("print", 5, 5),
      SCM_STRING_CONST_INITIALIZER("for-each", 8, 8),
      SCM_STRING_CONST_INITIALIZER("make-write-controls", 19, 19),
      SCM_STRING_CONST_INITIALIZER("undefined\077", 10, 10),
      SCM_STRING_CONST_INITIALIZER("<write-controls>", 16, 16),
      SCM_STRING_CONST_INITIALIZER("length", 6, 6),
      SCM_STRING_CONST_INITIALIZER("level", 5, 5),
      SCM_STRING_CONST_INITIALIZER("width", 5, 5),
      SCM_STRING_CONST_INITIALIZER("base", 4, 4),
      SCM_STRING_CONST_INITIALIZER("radix", 5, 5),
      SCM_STRING_CONST_INITIALIZER("pretty", 6, 6),
      SCM_STRING_CONST_INITIALIZER("string-length", 13, 13),
      SCM_STRING_CONST_INITIALIZER("indent", 6, 6),
      SCM_STRING_CONST_INITIALIZER("keyword list not even", 21, 21),
      SCM_STRING_CONST_INITIALIZER("unwrap-syntax-1", 15, 15),
      SCM_STRING_CONST_INITIALIZER("print-length", 12, 12),
      SCM_STRING_CONST_INITIALIZER("print-level", 11, 11),
      SCM_STRING_CONST_INITIALIZER("print-width", 11, 11),
      SCM_STRING_CONST_INITIALIZER("print-base", 10, 10),
      SCM_STRING_CONST_INITIALIZER("print-radix", 11, 11),
      SCM_STRING_CONST_INITIALIZER("print-pretty", 12, 12),
      SCM_STRING_CONST_INITIALIZER("unknown keyword ~S", 18, 18),
      SCM_STRING_CONST_INITIALIZER("errorf", 6, 6),
      SCM_STRING_CONST_INITIALIZER("write-controls-copy", 19, 19),
      SCM_STRING_CONST_INITIALIZER("wc", 2, 2),
      SCM_STRING_CONST_INITIALIZER("open-input-file", 15, 15),
      SCM_STRING_CONST_INITIALIZER("encoding", 8, 8),
      SCM_STRING_CONST_INITIALIZER("default-file-encoding", 21, 21),
      SCM_STRING_CONST_INITIALIZER("get-keyword", 11, 11),
      SCM_STRING_CONST_INITIALIZER("gauche-character-encoding", 25, 25),
      SCM_STRING_CONST_INITIALIZER("delete-keyword", 14, 14),
      SCM_STRING_CONST_INITIALIZER("%open-input-file/conv", 21, 21),
      SCM_STRING_CONST_INITIALIZER("filename", 8, 8),
      SCM_STRING_CONST_INITIALIZER("open-output-file", 16, 16),
      SCM_STRING_CONST_INITIALIZER("%open-output-file/conv", 22, 22),
      SCM_STRING_CONST_INITIALIZER("call-with-port", 14, 14),
      SCM_STRING_CONST_INITIALIZER("call-with-input-file", 20, 20),
      SCM_STRING_CONST_INITIALIZER("flags", 5, 5),
      SCM_STRING_CONST_INITIALIZER("call-with-output-file", 21, 21),
      SCM_STRING_CONST_INITIALIZER("with-input-from-file", 20, 20),
      SCM_STRING_CONST_INITIALIZER("with-input-from-port", 20, 20),
      SCM_STRING_CONST_INITIALIZER("thunk", 5, 5),
      SCM_STRING_CONST_INITIALIZER("with-output-to-file", 19, 19),
      SCM_STRING_CONST_INITIALIZER("with-output-to-port", 19, 19),
      SCM_STRING_CONST_INITIALIZER("with-output-to-string", 21, 21),
      SCM_STRING_CONST_INITIALIZER("with-input-from-string", 22, 22),
      SCM_STRING_CONST_INITIALIZER("str", 3, 3),
      SCM_STRING_CONST_INITIALIZER("call-with-output-string", 23, 23),
      SCM_STRING_CONST_INITIALIZER("call-with-input-string", 22, 22),
      SCM_STRING_CONST_INITIALIZER("call-with-string-io", 19, 19),
      SCM_STRING_CONST_INITIALIZER("with-string-io", 14, 14),
      SCM_STRING_CONST_INITIALIZER("write-to-string", 15, 15),
      SCM_STRING_CONST_INITIALIZER("writer", 6, 6),
      SCM_STRING_CONST_INITIALIZER("cut", 3, 3),
      SCM_STRING_CONST_INITIALIZER("read-from-string", 16, 16),
      SCM_STRING_CONST_INITIALIZER("values", 6, 6),
      SCM_STRING_CONST_INITIALIZER("with-error-to-port", 18, 18),
      SCM_STRING_CONST_INITIALIZER("current-error-port", 18, 18),
      SCM_STRING_CONST_INITIALIZER("with-ports", 10, 10),
      SCM_STRING_CONST_INITIALIZER("eport", 5, 5),
      SCM_STRING_CONST_INITIALIZER("r6rs", 4, 4),
      SCM_STRING_CONST_INITIALIZER("Reading R6RS source file.  Note that Gauche is not R6RS compliant.", 66, 66),
      SCM_STRING_CONST_INITIALIZER("warn", 4, 4),
      SCM_STRING_CONST_INITIALIZER("sym", 3, 3),
      SCM_STRING_CONST_INITIALIZER("unused-args", 11, 11),
      SCM_STRING_CONST_INITIALIZER("fold-case", 9, 9),
      SCM_STRING_CONST_INITIALIZER("no-fold-case", 12, 12),
      SCM_STRING_CONST_INITIALIZER("gauche-legacy", 13, 13),
      SCM_STRING_CONST_INITIALIZER("legacy", 6, 6),
      SCM_STRING_CONST_INITIALIZER("r7rs", 4, 4),
      SCM_STRING_CONST_INITIALIZER("strict-r7", 9, 9),
      SCM_STRING_CONST_INITIALIZER("l", 1, 1),
      SCM_STRING_CONST_INITIALIZER("format", 6, 6),
      SCM_STRING_CONST_INITIALIZER("if-let1", 7, 7),
      SCM_STRING_CONST_INITIALIZER("unwind-protect", 14, 14),
      SCM_STRING_CONST_INITIALIZER("define-in-module", 16, 16),
      SCM_STRING_CONST_INITIALIZER("setter", 6, 6),
      SCM_STRING_CONST_INITIALIZER("set!", 4, 4),
      SCM_STRING_CONST_INITIALIZER("G1804", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1805", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest1803", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G1807", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1810", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1809", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1808", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest1806", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G1812", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1813", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest1811", 8, 8),
      SCM_STRING_CONST_INITIALIZER("out", 3, 3),
      SCM_STRING_CONST_INITIALIZER("result", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<>", 2, 2),
      SCM_STRING_CONST_INITIALIZER("s", 1, 1),
      SCM_STRING_CONST_INITIALIZER("do", 2, 2),
      SCM_STRING_CONST_INITIALIZER("limit1818", 9, 9),
      SCM_STRING_CONST_INITIALIZER("dotimes", 7, 7),
      SCM_STRING_CONST_INITIALIZER("limit1817", 9, 9),
      SCM_STRING_CONST_INITIALIZER("vector-length", 13, 13),
      SCM_STRING_CONST_INITIALIZER("vector-ref", 10, 10),
      SCM_STRING_CONST_INITIALIZER("vector\077", 7, 7),
      SCM_STRING_CONST_INITIALIZER("pair\077", 5, 5),
      SCM_STRING_CONST_INITIALIZER("string\077", 7, 7),
      SCM_STRING_CONST_INITIALIZER("symbol\077", 7, 7),
      SCM_STRING_CONST_INITIALIZER("G1821", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1822", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest1820", 8, 8),
      SCM_STRING_CONST_INITIALIZER("loop1829", 8, 8),
      SCM_STRING_CONST_INITIALIZER("args1828", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G1830", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1831", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1832", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1833", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1834", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1835", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1836", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1837", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1838", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1839", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1840", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1841", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1842", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1843", 5, 5),
      SCM_STRING_CONST_INITIALIZER("tmp", 3, 3),
      SCM_STRING_CONST_INITIALIZER("case", 4, 4),
      SCM_STRING_CONST_INITIALIZER("arg", 3, 3),
      SCM_STRING_CONST_INITIALIZER("k-alt", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest1827", 8, 8),
      SCM_STRING_CONST_INITIALIZER("loop1846", 8, 8),
      SCM_STRING_CONST_INITIALIZER("args1845", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G1847", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1848", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1849", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1850", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1851", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1852", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1853", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1854", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1855", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1856", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1857", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1858", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1859", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1860", 5, 5),
      SCM_STRING_CONST_INITIALIZER("slot-ref", 8, 8),
      SCM_STRING_CONST_INITIALIZER("select", 6, 6),
      SCM_STRING_CONST_INITIALIZER("rest1844", 8, 8),
      SCM_STRING_CONST_INITIALIZER("let-syntax", 10, 10),
      SCM_STRING_CONST_INITIALIZER("syntax-rules", 12, 12),
      SCM_STRING_CONST_INITIALIZER("_", 1, 1),
      SCM_STRING_CONST_INITIALIZER("and-let*", 8, 8),
      SCM_STRING_CONST_INITIALIZER("e", 1, 1),
      SCM_STRING_CONST_INITIALIZER("in", 2, 2),
      SCM_STRING_CONST_INITIALIZER("G1862", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1863", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest1861", 8, 8),
      SCM_STRING_CONST_INITIALIZER("after", 5, 5),
      SCM_STRING_CONST_INITIALIZER("dynamic-wind", 12, 12),
      SCM_STRING_CONST_INITIALIZER("before", 6, 6),
      SCM_STRING_CONST_INITIALIZER("%with-ports", 11, 11),
  },
};
static struct scm__rcRec {
  ScmUVector d1796[115];
  ScmCompiledCode d1795[117];
  ScmWord d1794[3649];
  ScmPair d1787[1231] SCM_ALIGN_PAIR;
  ScmObj d1786[1460];
} scm__rc SCM_UNUSED = {
  {   /* ScmUVector d1796 */
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 26, uvector__00001, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 43, uvector__00002, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 245, uvector__00003, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 75, uvector__00004, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 50, uvector__00005, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 22, uvector__00006, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 62, uvector__00007, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 56, uvector__00008, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 281, uvector__00009, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 97, uvector__00010, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 117, uvector__00011, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 110, uvector__00012, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 32, uvector__00013, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 32, uvector__00014, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 32, uvector__00015, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 315, uvector__00016, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 101, uvector__00017, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 312, uvector__00018, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 58, uvector__00019, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 400, uvector__00020, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 105, uvector__00021, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 88, uvector__00022, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 56, uvector__00023, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 132, uvector__00024, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 65, uvector__00025, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 74, uvector__00026, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 24, uvector__00027, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 34, uvector__00028, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 46, uvector__00029, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 30, uvector__00030, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 42, uvector__00031, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 34, uvector__00032, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 34, uvector__00033, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 32, uvector__00034, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 32, uvector__00035, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 107, uvector__00036, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 54, uvector__00037, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 19, uvector__00038, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 790, uvector__00039, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 179, uvector__00040, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 150, uvector__00041, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 49, uvector__00042, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 36, uvector__00043, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 44, uvector__00044, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 46, uvector__00045, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 3717, uvector__00046, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 150, uvector__00047, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 4702, uvector__00048, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 352, uvector__00049, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 291, uvector__00050, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 111, uvector__00051, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 167, uvector__00052, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 81, uvector__00053, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 27, uvector__00054, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 22, uvector__00055, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 56, uvector__00056, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 50, uvector__00057, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 36, uvector__00058, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 115, uvector__00059, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 67, uvector__00060, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 36, uvector__00061, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 115, uvector__00062, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 67, uvector__00063, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 30, uvector__00064, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 22, uvector__00065, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 124, uvector__00066, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 71, uvector__00067, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 30, uvector__00068, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 22, uvector__00069, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 124, uvector__00070, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 71, uvector__00071, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 85, uvector__00072, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 54, uvector__00073, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 50, uvector__00074, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 52, uvector__00075, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 80, uvector__00076, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 51, uvector__00077, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 46, uvector__00078, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 48, uvector__00079, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 107, uvector__00080, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 64, uvector__00081, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 32, uvector__00082, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 40, uvector__00083, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 53, uvector__00084, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 28, uvector__00085, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 153, uvector__00086, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 53, uvector__00087, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 80, uvector__00088, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 56, uvector__00089, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 39, uvector__00090, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 51, uvector__00091, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 172, uvector__00092, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 50, uvector__00093, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 39, uvector__00094, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 51, uvector__00095, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 172, uvector__00096, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 50, uvector__00097, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 39, uvector__00098, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 52, uvector__00099, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 175, uvector__00100, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 51, uvector__00101, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 113, uvector__00102, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 149, uvector__00103, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 260, uvector__00104, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 64, uvector__00105, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 38, uvector__00106, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 40, uvector__00107, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 65, uvector__00108, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 45, uvector__00109, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 65, uvector__00110, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 45, uvector__00111, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 50, uvector__00112, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 47, uvector__00113, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 50, uvector__00114, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 47, uvector__00115, 0, NULL),
  },
  {   /* ScmCompiledCode d1795 */
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* port-tell */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[0])), 6,
            6, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[373]),
            SCM_OBJ(&scm__rc.d1795[1]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[6])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* port-position-prefix */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[21])), 59,
            20, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[384]),
            SCM_OBJ(&scm__rc.d1795[3]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[80])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[95])), 9,
            11, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[428]),
            SCM_OBJ(&scm__rc.d1795[6]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[104])), 4,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[428]),
            SCM_OBJ(&scm__rc.d1795[6]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-port-locking */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[108])), 13,
            7, 2, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[437]),
            SCM_OBJ(&scm__rc.d1795[7]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[121])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[136])), 68,
            25, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[440]),
            SCM_OBJ(&scm__rc.d1795[10]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[204])), 20,
            12, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[440]),
            SCM_OBJ(&scm__rc.d1795[10]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %with-2pass-setup */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[224])), 13,
            7, 3, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[450]),
            SCM_OBJ(&scm__rc.d1795[11]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[237])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[252])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[267])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[282])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* read-string */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[297])), 73,
            28, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[602]),
            SCM_OBJ(&scm__rc.d1795[16]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[370])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* write-string */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[385])), 73,
            29, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[627]),
            SCM_OBJ(&scm__rc.d1795[18]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[458])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* consume-trailing-whitespaces */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[473])), 97,
            27, 0, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[682]),
            SCM_OBJ(&scm__rc.d1795[20]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[570])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* port->string */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[585])), 23,
            18, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[722]),
            SCM_OBJ(&scm__rc.d1795[22]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[608])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[623])), 29,
            24, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[728]),
            SCM_OBJ(&scm__rc.d1795[24]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* port->list */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[652])), 6,
            5, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[734]),
            SCM_OBJ(&scm__rc.d1795[25]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[658])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* (port->string-list G1815) */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[673])), 6,
            5, 1, 0, SCM_OBJ(&scm__rc.d1787[736]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[742]),
            SCM_OBJ(&scm__rc.d1795[28]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* port->string-list */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[679])), 5,
            5, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[744]),
            SCM_OBJ(&scm__rc.d1795[28]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[684])), 17,
            14, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* port->sexp-list */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[701])), 6,
            5, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[749]),
            SCM_OBJ(&scm__rc.d1795[30]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[707])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[722])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[737])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[752])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[767])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* write-walk */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[782])), 22,
            16, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[949]),
            SCM_OBJ(&scm__rc.d1795[36]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[804])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* (%write-walk-rec G1819) */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[819])), 2,
            0, 1, 0, SCM_OBJ(&scm__rc.d1787[951]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[957]),
            SCM_OBJ(&scm__rc.d1795[39]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %write-walk-rec */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[821])), 175,
            32, 3, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[966]),
            SCM_OBJ(&scm__rc.d1795[39]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[996])), 17,
            14, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* write-with-shared-structure */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1013])), 34,
            17, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[978]),
            SCM_OBJ(&scm__rc.d1795[41]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1047])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1062])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* print */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1077])), 10,
            12, 0, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[983]),
            SCM_OBJ(&scm__rc.d1795[44]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1087])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* make-write-controls */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1102])), 676,
            105, 0, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1003]),
            SCM_OBJ(&scm__rc.d1795[46]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1778])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* write-controls-copy */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1793])), 845,
            112, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1009]),
            SCM_OBJ(&scm__rc.d1795[48]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2638])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* open-input-file */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2653])), 61,
            26, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1015]),
            SCM_OBJ(&scm__rc.d1795[50]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2714])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* open-output-file */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2729])), 37,
            14, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1020]),
            SCM_OBJ(&scm__rc.d1795[52]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2766])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2781])), 4,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1023]),
            SCM_OBJ(&scm__rc.d1795[55]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2785])), 4,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1023]),
            SCM_OBJ(&scm__rc.d1795[55]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* call-with-port */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2789])), 13,
            7, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1031]),
            SCM_OBJ(&scm__rc.d1795[56]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2802])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2817])), 4,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1034]),
            SCM_OBJ(&scm__rc.d1795[59]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2821])), 8,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1034]),
            SCM_OBJ(&scm__rc.d1795[59]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* call-with-input-file */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2829])), 21,
            11, 2, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1043]),
            SCM_OBJ(&scm__rc.d1795[60]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2850])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2865])), 4,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1046]),
            SCM_OBJ(&scm__rc.d1795[63]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2869])), 8,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1046]),
            SCM_OBJ(&scm__rc.d1795[63]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* call-with-output-file */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2877])), 21,
            11, 2, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1053]),
            SCM_OBJ(&scm__rc.d1795[64]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2898])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2913])), 5,
            5, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1056]),
            SCM_OBJ(&scm__rc.d1795[67]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2918])), 4,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1056]),
            SCM_OBJ(&scm__rc.d1795[67]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-input-from-file */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2922])), 23,
            11, 2, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1065]),
            SCM_OBJ(&scm__rc.d1795[68]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2945])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2960])), 5,
            5, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1068]),
            SCM_OBJ(&scm__rc.d1795[71]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2965])), 4,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1068]),
            SCM_OBJ(&scm__rc.d1795[71]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-output-to-file */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2969])), 23,
            11, 2, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1075]),
            SCM_OBJ(&scm__rc.d1795[72]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2992])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-output-to-string */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3007])), 15,
            16, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1081]),
            SCM_OBJ(&scm__rc.d1795[74]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3022])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-input-from-string */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3037])), 10,
            12, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1087]),
            SCM_OBJ(&scm__rc.d1795[76]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3047])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* call-with-output-string */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3062])), 14,
            15, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1092]),
            SCM_OBJ(&scm__rc.d1795[78]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3076])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* call-with-input-string */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3091])), 9,
            11, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1098]),
            SCM_OBJ(&scm__rc.d1795[80]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3100])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* call-with-string-io */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3115])), 20,
            17, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1103]),
            SCM_OBJ(&scm__rc.d1795[82]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3135])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3150])), 5,
            5, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1109]),
            SCM_OBJ(&scm__rc.d1795[84]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-string-io */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3155])), 5,
            4, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1114]),
            SCM_OBJ(&scm__rc.d1795[85]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3160])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3175])), 4,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1131]),
            SCM_OBJ(&scm__rc.d1795[87]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* write-to-string */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3179])), 35,
            17, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1136]),
            SCM_OBJ(&scm__rc.d1795[88]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3214])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* read-from-string */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3229])), 18,
            11, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1142]),
            SCM_OBJ(&scm__rc.d1795[90]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3247])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-input-from-port */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3262])), 8,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1149]),
            SCM_OBJ(&scm__rc.d1795[93]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-input-from-port */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3270])), 11,
            11, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1150]),
            SCM_OBJ(&scm__rc.d1795[93]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-input-from-port */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3281])), 31,
            20, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1156]),
            SCM_OBJ(&scm__rc.d1795[94]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3312])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-output-to-port */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3327])), 8,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1163]),
            SCM_OBJ(&scm__rc.d1795[97]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-output-to-port */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3335])), 11,
            11, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1164]),
            SCM_OBJ(&scm__rc.d1795[97]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-output-to-port */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3346])), 31,
            20, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1169]),
            SCM_OBJ(&scm__rc.d1795[98]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3377])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-error-to-port */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3392])), 8,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1176]),
            SCM_OBJ(&scm__rc.d1795[101]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-error-to-port */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3400])), 11,
            11, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1177]),
            SCM_OBJ(&scm__rc.d1795[101]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-error-to-port */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3411])), 31,
            20, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1182]),
            SCM_OBJ(&scm__rc.d1795[102]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3442])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-ports */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3457])), 28,
            11, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1189]),
            SCM_OBJ(&scm__rc.d1795[105]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-ports */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3485])), 33,
            11, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1190]),
            SCM_OBJ(&scm__rc.d1795[105]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* with-ports */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3518])), 35,
            22, 4, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1198]),
            SCM_OBJ(&scm__rc.d1795[106]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3553])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3568])), 7,
            11, 3, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1207]),
            SCM_OBJ(&scm__rc.d1795[108]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3575])), 7,
            5, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3582])), 10,
            12, 3, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1215]),
            SCM_OBJ(&scm__rc.d1795[110]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3592])), 7,
            5, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3599])), 9,
            12, 3, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1220]),
            SCM_OBJ(&scm__rc.d1795[112]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3608])), 7,
            5, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3615])), 10,
            13, 3, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1225]),
            SCM_OBJ(&scm__rc.d1795[114]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3625])), 7,
            5, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3632])), 10,
            13, 3, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1230]),
            SCM_OBJ(&scm__rc.d1795[116]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[3642])), 7,
            5, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
  },
  {   /* ScmWord d1794 */
    /* port-tell */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00000007    /*   1 (CONSTI-PUSH 0) */,
    0x00001007    /*   2 (CONSTI-PUSH 1) */,
    0x00003060    /*   3 (GREF-TAIL-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port-seek.d4eb7a0> */,
    0x00000014    /*   5 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[6]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* port-tell */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[6]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[0])) /* #<compiled-code port-tell@0x7f4b0d4e1300> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port-tell.d4eb8a0> */,
    0x00000014    /*  14 (RET) */,
    /* port-position-prefix */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[21]) + 5),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port?.d5673a0> */,
    0x0000001e    /*   5 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[21]) + 57),
    0x0000100e    /*   7 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[21]) + 12),
    0x00000048    /*   9 (LREF0-PUSH) */,
    0x0000105f    /*  10 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port-name.d56be40> */,
    0x00001018    /*  12 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  13 (LREF0) */,
    0x0000001e    /*  14 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[21]) + 56),
    0x0000100e    /*  16 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[21]) + 21),
    0x0000004c    /*  18 (LREF10-PUSH) */,
    0x0000105f    /*  19 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port-current-line.d56b9a0> */,
    0x00001018    /*  21 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  22 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[21]) + 27),
    0x00000048    /*  24 (LREF0-PUSH) */,
    0x0000105f    /*  25 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#positive?.d56b8e0> */,
    0x0000001e    /*  27 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[21]) + 43),
    0x0000200e    /*  29 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[21]) + 54),
    0x00000006    /*  31 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[376])) /* (#f 1 #<box #f>) */,
    0x00000009    /*  33 (CONSTF-PUSH) */,
    0x00000006    /*  34 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[109])) /* "~s:line ~a: " */,
    0x0000004c    /*  36 (LREF10-PUSH) */,
    0x0000003d    /*  37 (LREF0) */,
    0x00004088    /*  38 (LIST 4) */,
    0x00002062    /*  39 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.format#format-internal.d56b760> */,
    0x00000013    /*  41 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[21]) + 54),
    0x0000200e    /*  43 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[21]) + 54),
    0x00000006    /*  45 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[379])) /* (#f 1 #<box #f>) */,
    0x00000009    /*  47 (CONSTF-PUSH) */,
    0x00000006    /*  48 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[112])) /* "~s: " */,
    0x00000041    /*  50 (LREF10) */,
    0x00003088    /*  51 (LIST 3) */,
    0x00002062    /*  52 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.format#format-internal.d56b3e0> */,
    0x0000000a    /*  54 (CONST-RET) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[113])) /* "" */,
    0x0000000c    /*  56 (CONSTU-RET) */,
    0x0000000a    /*  57 (CONST-RET) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[114])) /* "???" */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[80]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* port-position-prefix */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[80]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[2])) /* #<compiled-code port-position-prefix@0x7f4b0d569cc0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port-position-prefix.d5674c0> */,
    0x00000014    /*  14 (RET) */,
    /* (with-port-locking #f) */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[95]) + 5),
    0x0000004a    /*   2 (LREF2-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%port-lock!.e8fdaa0> */,
    0x00000049    /*   5 (LREF1-PUSH) */,
    0x0000003d    /*   6 (LREF0) */,
    0x00002095    /*   7 (TAIL-APPLY 2) */,
    0x00000014    /*   8 (RET) */,
    /* (with-port-locking #f) */
    0x0000004a    /*   0 (LREF2-PUSH) */,
    0x00001060    /*   1 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%port-unlock!.e8fd8c0> */,
    0x00000014    /*   3 (RET) */,
    /* with-port-locking */
    0x00000016    /*   0 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[4])) /* #<compiled-code (with-port-locking #f)@0x7f4b0d969600> */,
    0x0000000d    /*   2 (PUSH) */,
    0x00000016    /*   3 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[5])) /* #<compiled-code (with-port-locking #f)@0x7f4b0d9695a0> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000006    /*   6 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :source-info */,
    0x00000006    /*   8 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[430])) /* ("libio.scm" 499) */,
    0x00004060    /*  10 (GREF-TAIL-CALL 4) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%unwind-protect.e8fdc00> */,
    0x00000014    /*  12 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[121]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* with-port-locking */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[121]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[6])) /* #<compiled-code with-port-locking@0x7f4b0d969660> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#with-port-locking.e8f8200> */,
    0x00000014    /*  14 (RET) */,
    /* (%with-2pass-setup #f) */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]) + 5),
    0x0000004b    /*   2 (LREF3-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%port-lock!.e4fc4e0> */,
    0x0000100e    /*   5 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]) + 10),
    0x0000004b    /*   7 (LREF3-PUSH) */,
    0x0000105f    /*   8 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%port-write-state.e4fc460> */,
    0x0000001e    /*  10 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]) + 21),
    0x0000200e    /*  12 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]) + 21),
    0x00000006    /*  14 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[127])) /* "[internal] %with-2pass-setup called recursively on port:" */,
    0x0000004b    /*  16 (LREF3-PUSH) */,
    0x0000205f    /*  17 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.e4fc400> */,
    0x00000013    /*  19 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]) + 21),
    0x0000200e    /*  21 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]) + 42),
    0x0000004b    /*  23 (LREF3-PUSH) */,
    0x0000300e    /*  24 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]) + 38),
    0x0000005e    /*  26 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<write-state>.e4fc1a0> */,
    0x00000006    /*  28 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :shared-table */,
    0x0000100e    /*  30 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]) + 36),
    0x00000006    /*  32 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* eq? */,
    0x0000105f    /*  34 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-hash-table.e4fef80> */,
    0x00003062    /*  36 (PUSH-GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make.e4fc1e0> */,
    0x00000061    /*  38 (PUSH-GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%port-write-state.e4fc240> */,
    0x000000a1    /*  40 (SETTER) */,
    0x00002011    /*  41 (CALL 2) */,
    0x0000200e    /*  42 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]) + 51),
    0x0000004b    /*  44 (LREF3-PUSH) */,
    0x00000006    /*  45 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x0000005d    /*  47 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%port-walking?.e4fede0> */,
    0x000000a1    /*  49 (SETTER) */,
    0x00002011    /*  50 (CALL 2) */,
    0x0000000e    /*  51 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]) + 56),
    0x0000004a    /*  53 (LREF2-PUSH) */,
    0x0000003d    /*  54 (LREF0) */,
    0x00002095    /*  55 (TAIL-APPLY 2) */,
    0x0000200e    /*  56 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]) + 64),
    0x0000004b    /*  58 (LREF3-PUSH) */,
    0x00000009    /*  59 (CONSTF-PUSH) */,
    0x0000005d    /*  60 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%port-walking?.e4fec40> */,
    0x000000a1    /*  62 (SETTER) */,
    0x00002011    /*  63 (CALL 2) */,
    0x00000049    /*  64 (LREF1-PUSH) */,
    0x0000003d    /*  65 (LREF0) */,
    0x00002095    /*  66 (TAIL-APPLY 2) */,
    0x00000014    /*  67 (RET) */,
    /* (%with-2pass-setup #f) */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[204]) + 8),
    0x0000004b    /*   2 (LREF3-PUSH) */,
    0x00000009    /*   3 (CONSTF-PUSH) */,
    0x0000005d    /*   4 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%port-walking?.e4fe3e0> */,
    0x000000a1    /*   6 (SETTER) */,
    0x00002011    /*   7 (CALL 2) */,
    0x0000200e    /*   8 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[204]) + 16),
    0x0000004b    /*  10 (LREF3-PUSH) */,
    0x00000009    /*  11 (CONSTF-PUSH) */,
    0x0000005d    /*  12 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%port-write-state.e506d20> */,
    0x000000a1    /*  14 (SETTER) */,
    0x00002011    /*  15 (CALL 2) */,
    0x0000004b    /*  16 (LREF3-PUSH) */,
    0x00001060    /*  17 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%port-unlock!.e506c60> */,
    0x00000014    /*  19 (RET) */,
    /* %with-2pass-setup */
    0x00000016    /*   0 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[8])) /* #<compiled-code (%with-2pass-setup #f)@0x7f4b0db254e0> */,
    0x0000000d    /*   2 (PUSH) */,
    0x00000016    /*   3 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[9])) /* #<compiled-code (%with-2pass-setup #f)@0x7f4b0db25300> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000006    /*   6 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :source-info */,
    0x00000006    /*   8 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[442])) /* ("libio.scm" 510) */,
    0x00004060    /*  10 (GREF-TAIL-CALL 4) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%unwind-protect.e4fc640> */,
    0x00000014    /*  12 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[237]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* %with-2pass-setup */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[237]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[10])) /* #<compiled-code %with-2pass-setup@0x7f4b0db25540> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%with-2pass-setup.e4fa320> */,
    0x00000014    /*  14 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[252]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* u8-ready? */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[252]) + 12),
    0x0000005d    /*   9 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#byte-ready?.dc0a560> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#u8-ready?.dc0a5e0> */,
    0x00000014    /*  14 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[267]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* read-u8 */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[267]) + 12),
    0x0000005d    /*   9 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read-byte.dca5900> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read-u8.dca5980> */,
    0x00000014    /*  14 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[282]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* peek-u8 */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[282]) + 12),
    0x0000005d    /*   9 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#peek-byte.dd25620> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#peek-u8.dd256a0> */,
    0x00000014    /*  14 (RET) */,
    /* read-string */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]) + 6),
    0x000000de    /*   3 (CURIN) */,
    0x00000013    /*   4 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]) + 7),
    0x0000006a    /*   6 (LREF0-CAR) */,
    0x0000000d    /*   7 (PUSH) */,
    0x0000003d    /*   8 (LREF0) */,
    0x00000022    /*   9 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]) + 14),
    0x00000003    /*  11 (CONSTN) */,
    0x00000013    /*  12 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]) + 15),
    0x00000076    /*  14 (LREF0-CDR) */,
    0x00002018    /*  15 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  16 (LREF0) */,
    0x00000022    /*  17 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]) + 21),
    0x00000013    /*  19 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]) + 29),
    0x0000200e    /*  21 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]) + 29),
    0x00000006    /*  23 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[151])) /* "too many arguments for" */,
    0x00000006    /*  25 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[596])) /* (lambda (n :optional (port (current-input-port))) (define o (open-output-string :private? #t)) (let loop ((i 0)) (if (>= i n) (get-output-string o) (let1 c (read-char port) (if (eof-object? c) (if (= i 0) (eof-object) (get-output-string o)) (begin (write-char c o) (loop (+ i 1)))))))) */,
    0x0000205f    /*  27 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.f79a860> */,
    0x00001019    /*  29 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[597])) /* (#<undef>) */,
    0x0000200e    /*  31 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]) + 39),
    0x00000006    /*  33 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :private? */,
    0x00000006    /*  35 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x0000205f    /*  37 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#open-output-string.de5ff20> */,
    0x000000e8    /*  39 (ENV-SET 0) */,
    0x00000007    /*  40 (CONSTI-PUSH 0) */,
    0x00001017    /*  41 (LOCAL-ENV 1) */,
    0x0040303c    /*  42 (LREF 3 1) */,
    0x0000002c    /*  43 (LREF-VAL0-BNGE 0 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]) + 49),
    0x0000004c    /*  45 (LREF10-PUSH) */,
    0x00001060    /*  46 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#get-output-string.de5f720> */,
    0x00000014    /*  48 (RET) */,
    0x00000045    /*  49 (LREF21) */,
    0x000010db    /*  50 (READ-CHAR 1) */,
    0x00001018    /*  51 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  52 (LREF0) */,
    0x0000009a    /*  53 (EOFP) */,
    0x0000001e    /*  54 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]) + 65),
    0x00000041    /*  56 (LREF10) */,
    0x0000002d    /*  57 (BNUMNEI 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]) + 61),
    0x0000000a    /*  59 (CONST-RET) */,
    SCM_WORD(SCM_EOF) /* #<eof> */,
    0x0000004f    /*  61 (LREF20-PUSH) */,
    0x00001060    /*  62 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#get-output-string.de61f40> */,
    0x00000014    /*  64 (RET) */,
    0x00000048    /*  65 (LREF0-PUSH) */,
    0x00000044    /*  66 (LREF20) */,
    0x000020dd    /*  67 (WRITE-CHAR 2) */,
    0x000010cb    /*  68 (LREF10-NUMADDI-PUSH 1) */,
    0x0000201b    /*  69 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]) + 42),
    0x00000014    /*  71 (RET) */,
    0x00000014    /*  72 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[370]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* read-string */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[370]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[15])) /* #<compiled-code read-string@0x7f4b0d4039c0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read-string.de3f540> */,
    0x00000014    /*  14 (RET) */,
    /* write-string */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]) + 6),
    0x000000df    /*   3 (CUROUT) */,
    0x00000013    /*   4 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]) + 7),
    0x0000006a    /*   6 (LREF0-CAR) */,
    0x0000000d    /*   7 (PUSH) */,
    0x0000003d    /*   8 (LREF0) */,
    0x00000022    /*   9 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]) + 14),
    0x00000003    /*  11 (CONSTN) */,
    0x00000013    /*  12 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]) + 15),
    0x00000076    /*  14 (LREF0-CDR) */,
    0x00002018    /*  15 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  16 (LREF0) */,
    0x00000022    /*  17 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]) + 22),
    0x00000002    /*  19 (CONSTI 0) */,
    0x00000013    /*  20 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]) + 23),
    0x0000006a    /*  22 (LREF0-CAR) */,
    0x0000000d    /*  23 (PUSH) */,
    0x0000003d    /*  24 (LREF0) */,
    0x00000022    /*  25 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]) + 30),
    0x00000003    /*  27 (CONSTN) */,
    0x00000013    /*  28 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]) + 31),
    0x00000076    /*  30 (LREF0-CDR) */,
    0x00002018    /*  31 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  32 (LREF0) */,
    0x00000022    /*  33 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]) + 38),
    -0x00000ffe   /*  35 (CONSTI -1) */,
    0x00000013    /*  36 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]) + 39),
    0x0000006a    /*  38 (LREF0-CAR) */,
    0x0000000d    /*  39 (PUSH) */,
    0x0000003d    /*  40 (LREF0) */,
    0x00000022    /*  41 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]) + 46),
    0x00000003    /*  43 (CONSTN) */,
    0x00000013    /*  44 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]) + 47),
    0x00000076    /*  46 (LREF0-CDR) */,
    0x00002018    /*  47 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  48 (LREF0) */,
    0x00000022    /*  49 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]) + 53),
    0x00000013    /*  51 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]) + 61),
    0x0000200e    /*  53 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]) + 61),
    0x00000006    /*  55 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[151])) /* "too many arguments for" */,
    0x00000006    /*  57 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[622])) /* (lambda (string :optional (port (current-output-port)) (start 0) (end -1)) (display (opt-substring string start end) port)) */,
    0x0000205f    /*  59 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.f79a860> */,
    0x0000300e    /*  61 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]) + 68),
    0x00403047    /*  63 (LREF-PUSH 3 1) */,
    0x0000004d    /*  64 (LREF11-PUSH) */,
    0x00000049    /*  65 (LREF1-PUSH) */,
    0x0000305f    /*  66 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#opt-substring.d987cc0> */,
    0x0000000d    /*  68 (PUSH) */,
    0x00000050    /*  69 (LREF21-PUSH) */,
    0x00002060    /*  70 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#display.d987d00> */,
    0x00000014    /*  72 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[458]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* write-string */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[458]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[17])) /* #<compiled-code write-string@0x7f4b0cfed840> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#write-string.d97f380> */,
    0x00000014    /*  14 (RET) */,
    /* consume-trailing-whitespaces */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 6),
    0x000000de    /*   3 (CURIN) */,
    0x00000013    /*   4 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 7),
    0x0000006a    /*   6 (LREF0-CAR) */,
    0x0000000d    /*   7 (PUSH) */,
    0x0000003d    /*   8 (LREF0) */,
    0x00000022    /*   9 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 14),
    0x00000003    /*  11 (CONSTN) */,
    0x00000013    /*  12 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 15),
    0x00000076    /*  14 (LREF0-CDR) */,
    0x00002018    /*  15 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  16 (LREF0) */,
    0x00000022    /*  17 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 21),
    0x00000013    /*  19 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 29),
    0x0000200e    /*  21 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 29),
    0x00000006    /*  23 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[151])) /* "too many arguments for" */,
    0x00000006    /*  25 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[677])) /* (lambda (:optional (port (current-input-port))) (let loop () (when (byte-ready? port) (let1 b (peek-byte port) (cond ((memv b '(9 32)) (read-byte port) (loop)) ((eqv? b 13) (read-byte port) (when (and (byte-ready? port) (eqv? (peek-byte port) 10)) (read-byte port))) ((eqv? b 10) (read-byte port))))))) */,
    0x0000205f    /*  27 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.f79a860> */,
    0x0000100e    /*  29 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 34),
    0x00000049    /*  31 (LREF1-PUSH) */,
    0x0000105f    /*  32 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#byte-ready?.db85160> */,
    0x0000001e    /*  34 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 95),
    0x0000100e    /*  36 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 41),
    0x00000049    /*  38 (LREF1-PUSH) */,
    0x0000105f    /*  39 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#peek-byte.db888e0> */,
    0x00001018    /*  41 (PUSH-LOCAL-ENV 1) */,
    0x00000048    /*  42 (LREF0-PUSH) */,
    0x00000001    /*  43 (CONST) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[631])) /* (9 32) */,
    0x0000008c    /*  45 (MEMV) */,
    0x0000001e    /*  46 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 56),
    0x0000100e    /*  48 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 53),
    0x0000004d    /*  50 (LREF11-PUSH) */,
    0x0000105f    /*  51 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read-byte.db886a0> */,
    0x0000101b    /*  53 (LOCAL-ENV-JUMP 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 29),
    0x00000014    /*  55 (RET) */,
    0x0000003d    /*  56 (LREF0) */,
    0x0000002f    /*  57 (BNEQVC) */,
    SCM_WORD(SCM_MAKE_INT(13U)) /* 13 */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 86)  /*     86 */,
    0x0000100e    /*  60 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 65),
    0x0000004d    /*  62 (LREF11-PUSH) */,
    0x0000105f    /*  63 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read-byte.db88580> */,
    0x0000100e    /*  65 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 70),
    0x0000004d    /*  67 (LREF11-PUSH) */,
    0x0000105f    /*  68 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#byte-ready?.db884c0> */,
    0x0000001e    /*  70 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 85),
    0x0000100e    /*  72 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 77),
    0x0000004d    /*  74 (LREF11-PUSH) */,
    0x0000105f    /*  75 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#peek-byte.db88440> */,
    0x0000002f    /*  77 (BNEQVC) */,
    SCM_WORD(SCM_MAKE_INT(10U)) /* 10 */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 84)  /*     84 */,
    0x0000004d    /*  80 (LREF11-PUSH) */,
    0x00001060    /*  81 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read-byte.db883c0> */,
    0x00000014    /*  83 (RET) */,
    0x0000000c    /*  84 (CONSTU-RET) */,
    0x0000000c    /*  85 (CONSTU-RET) */,
    0x0000003d    /*  86 (LREF0) */,
    0x0000002f    /*  87 (BNEQVC) */,
    SCM_WORD(SCM_MAKE_INT(10U)) /* 10 */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]) + 94)  /*     94 */,
    0x0000004d    /*  90 (LREF11-PUSH) */,
    0x00001060    /*  91 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read-byte.db882c0> */,
    0x00000014    /*  93 (RET) */,
    0x0000000c    /*  94 (CONSTU-RET) */,
    0x0000000c    /*  95 (CONSTU-RET) */,
    0x00000014    /*  96 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[570]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* consume-trailing-whitespaces */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[570]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[19])) /* #<compiled-code consume-trailing-whitespaces@0x7f4b0d308240> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#consume-trailing-whitespaces.db71020> */,
    0x00000014    /*  14 (RET) */,
    /* port->string */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[585]) + 8),
    0x00000006    /*   2 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :private? */,
    0x00000006    /*   4 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x0000205f    /*   6 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#open-output-string.d340620> */,
    0x00001018    /*   8 (PUSH-LOCAL-ENV 1) */,
    0x0000400e    /*   9 (PRE-CALL 4) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[585]) + 19),
    0x0000004c    /*  11 (LREF10-PUSH) */,
    0x00000048    /*  12 (LREF0-PUSH) */,
    0x00000006    /*  13 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :unit */,
    0x00000006    /*  15 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* byte */,
    0x0000405f    /*  17 (GREF-CALL 4) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#copy-port.d340520> */,
    0x00000048    /*  19 (LREF0-PUSH) */,
    0x00001060    /*  20 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#get-output-string.d3403e0> */,
    0x00000014    /*  22 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[608]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* port->string */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[608]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[21])) /* #<compiled-code port->string@0x7f4b0cfcdc60> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port->string.d340a80> */,
    0x00000014    /*  14 (RET) */,
    /* (port->list #f) */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[623]) + 5),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000003e    /*   3 (LREF1) */,
    0x00001011    /*   4 (CALL 1) */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000008    /*   6 (CONSTN-PUSH) */,
    0x00002017    /*   7 (LOCAL-ENV 2) */,
    0x0000003e    /*   8 (LREF1) */,
    0x0000009a    /*   9 (EOFP) */,
    0x0000001e    /*  10 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[623]) + 16),
    0x00000048    /*  12 (LREF0-PUSH) */,
    0x00001060    /*  13 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#reverse!.f1f8e80> */,
    0x00000014    /*  15 (RET) */,
    0x0000100e    /*  16 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[623]) + 21),
    0x0000004c    /*  18 (LREF10-PUSH) */,
    0x00000042    /*  19 (LREF11) */,
    0x00001011    /*  20 (CALL 1) */,
    0x0000000d    /*  21 (PUSH) */,
    0x00000049    /*  22 (LREF1-PUSH) */,
    0x0000003d    /*  23 (LREF0) */,
    0x00000067    /*  24 (CONS-PUSH) */,
    0x0000101b    /*  25 (LOCAL-ENV-JUMP 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[623]) + 8),
    0x00000014    /*  27 (RET) */,
    0x00000014    /*  28 (RET) */,
    /* port->list */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00000016    /*   1 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[23])) /* #<compiled-code (port->list #f)@0x7f4b0ec273c0> */,
    0x00002063    /*   3 (PUSH-GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#with-port-locking.f1f57e0> */,
    0x00000014    /*   5 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[658]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* port->list */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[658]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[24])) /* #<compiled-code port->list@0x7f4b0ec27420> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port->list.f1f5900> */,
    0x00000014    /*  14 (RET) */,
    /* (port->string-list G1815) */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00000006    /*   1 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x00002060    /*   3 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read-line.e890360> */,
    0x00000014    /*   5 (RET) */,
    /* port->string-list */
    0x0000004c    /*   0 (LREF10-PUSH) */,
    0x00000048    /*   1 (LREF0-PUSH) */,
    0x00002060    /*   2 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port->list.e890860> */,
    0x00000014    /*   4 (RET) */,
    /* %toplevel */
    0x00001019    /*   0 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[743])) /* (#<compiled-code (port->string-list #:G1815)@0x7f4b0e05e240>) */,
    0x0000000e    /*   2 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[684]) + 6),
    0x0000005f    /*   4 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000001    /*   7 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* port->string-list */,
    0x000000ee    /*   9 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[684]) + 14),
    0x00000016    /*  11 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[27])) /* #<compiled-code port->string-list@0x7f4b0e05e000> */,
    0x00000014    /*  13 (RET) */,
    0x00000015    /*  14 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port->string-list.e8909c0> */,
    0x00000014    /*  16 (RET) */,
    /* port->sexp-list */
    0x0000005e    /*   0 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read.e414e20> */,
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x00002060    /*   3 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port->list.e414e80> */,
    0x00000014    /*   5 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[707]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* port->sexp-list */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[707]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[29])) /* #<compiled-code port->sexp-list@0x7f4b0e33b660> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port->sexp-list.e4110c0> */,
    0x00000014    /*  14 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[722]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* read-with-shared-structure */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[722]) + 12),
    0x0000005d    /*   9 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read.e2388e0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#read-with-shared-structure.e238940> */,
    0x00000014    /*  14 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[737]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* read/ss */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[737]) + 12),
    0x0000005d    /*   9 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read.e2c0300> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#read/ss.e2c0380> */,
    0x00000014    /*  14 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[752]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* write-u8 */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[752]) + 12),
    0x0000005d    /*   9 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#write-byte.de9d580> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#write-u8.de9d6c0> */,
    0x00000014    /*  14 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[767]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* write* */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[767]) + 12),
    0x0000005d    /*   9 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#write-shared.df3fa20> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#write*.df3faa0> */,
    0x00000014    /*  14 (RET) */,
    /* write-walk */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[782]) + 5),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%port-write-state.d8de820> */,
    0x00001018    /*   5 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*   6 (LREF0) */,
    0x0000001e    /*   7 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[782]) + 21),
    0x0000004d    /*   9 (LREF11-PUSH) */,
    0x0000004c    /*  10 (LREF10-PUSH) */,
    0x0000200e    /*  11 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[782]) + 18),
    0x00000048    /*  13 (LREF0-PUSH) */,
    0x0000005e    /*  14 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#shared-table.d8de6a0> */,
    0x0000205f    /*  16 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#~.d8de700> */,
    0x00003063    /*  18 (PUSH-GREF-TAIL-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%write-walk-rec.d8de780> */,
    0x00000014    /*  20 (RET) */,
    0x0000000c    /*  21 (CONSTU-RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[804]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* write-walk */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[804]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[35])) /* #<compiled-code write-walk@0x7f4b0d47dc00> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#write-walk.d8dec60> */,
    0x00000014    /*  14 (RET) */,
    /* (%write-walk-rec G1819) */
    0x000010bd    /*   0 (LREF0-NUMADDI 1) */,
    0x00000014    /*   1 (RET) */,
    /* %write-walk-rec */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 5),
    0x0000004a    /*   2 (LREF2-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#write-need-recurse?.d9ff8e0> */,
    0x0000001e    /*   5 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 174),
    0x0000200e    /*   7 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 13),
    0x00000048    /*   9 (LREF0-PUSH) */,
    0x0000004a    /*  10 (LREF2-PUSH) */,
    0x0000205f    /*  11 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#hash-table-exists?.d9ff6e0> */,
    0x0000001e    /*  13 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 21),
    0x00000048    /*  15 (LREF0-PUSH) */,
    0x0000004a    /*  16 (LREF2-PUSH) */,
    0x0000004c    /*  17 (LREF10-PUSH) */,
    0x00003060    /*  18 (GREF-TAIL-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#hash-table-update!.d9ff660> */,
    0x00000014    /*  20 (RET) */,
    0x0000300e    /*  21 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 28),
    0x00000048    /*  23 (LREF0-PUSH) */,
    0x0000004a    /*  24 (LREF2-PUSH) */,
    0x00001007    /*  25 (CONSTI-PUSH 1) */,
    0x0000305f    /*  26 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#hash-table-put!.da03c80> */,
    0x0000003f    /*  28 (LREF2) */,
    0x0000009c    /*  29 (SYMBOLP) */,
    0x0000001e    /*  30 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 34),
    0x00000013    /*  32 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 150),
    0x0000003f    /*  34 (LREF2) */,
    0x0000009b    /*  35 (STRINGP) */,
    0x0000001e    /*  36 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 40),
    0x00000013    /*  38 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 150),
    0x0000100e    /*  40 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 45),
    0x0000004a    /*  42 (LREF2-PUSH) */,
    0x0000105f    /*  43 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#uvector?.da03a60> */,
    0x0000001e    /*  45 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 49),
    0x00000013    /*  47 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 150),
    0x0000003f    /*  49 (LREF2) */,
    0x00000098    /*  50 (PAIRP) */,
    0x0000001e    /*  51 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 71),
    0x0000300e    /*  53 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 61),
    0x0000006c    /*  55 (LREF2-CAR) */,
    0x0000000d    /*  56 (PUSH) */,
    0x00000049    /*  57 (LREF1-PUSH) */,
    0x00000048    /*  58 (LREF0-PUSH) */,
    0x0000305f    /*  59 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%write-walk-rec.da039a0> */,
    0x0000300e    /*  61 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 150),
    0x00000078    /*  63 (LREF2-CDR) */,
    0x0000000d    /*  64 (PUSH) */,
    0x00000049    /*  65 (LREF1-PUSH) */,
    0x00000048    /*  66 (LREF0-PUSH) */,
    0x0000305f    /*  67 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%write-walk-rec.da038e0> */,
    0x00000013    /*  69 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 150),
    0x0000003f    /*  71 (LREF2) */,
    0x0000009d    /*  72 (VECTORP) */,
    0x0000001e    /*  73 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 103),
    0x0000200e    /*  75 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 150),
    0x0000003f    /*  77 (LREF2) */,
    0x000000a7    /*  78 (VEC-LEN) */,
    0x0000000d    /*  79 (PUSH) */,
    0x00000007    /*  80 (CONSTI-PUSH 0) */,
    0x00002017    /*  81 (LOCAL-ENV 2) */,
    0x0000003e    /*  82 (LREF1) */,
    0x0000002c    /*  83 (LREF-VAL0-BNGE 0 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 86),
    0x0000000c    /*  85 (CONSTU-RET) */,
    0x0000300e    /*  86 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 96),
    0x0000004e    /*  88 (LREF12-PUSH) */,
    0x0000003d    /*  89 (LREF0) */,
    0x000000a8    /*  90 (VEC-REF) */,
    0x0000000d    /*  91 (PUSH) */,
    0x0000004d    /*  92 (LREF11-PUSH) */,
    0x0000004c    /*  93 (LREF10-PUSH) */,
    0x0000305f    /*  94 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%write-walk-rec.da03380> */,
    0x00000049    /*  96 (LREF1-PUSH) */,
    0x000010c7    /*  97 (LREF0-NUMADDI-PUSH 1) */,
    0x0000101b    /*  98 (LOCAL-ENV-JUMP 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 82),
    0x00000014    /* 100 (RET) */,
    0x00000013    /* 101 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 150),
    0x0000100e    /* 103 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 108),
    0x0000004a    /* 105 (LREF2-PUSH) */,
    0x0000105f    /* 106 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#box?.da030e0> */,
    0x0000001e    /* 108 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 144),
    0x0000200e    /* 110 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 150),
    0x0000100e    /* 112 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 117),
    0x0000004a    /* 114 (LREF2-PUSH) */,
    0x0000105f    /* 115 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#box-arity.da0a920> */,
    0x0000000d    /* 117 (PUSH) */,
    0x00000007    /* 118 (CONSTI-PUSH 0) */,
    0x00002017    /* 119 (LOCAL-ENV 2) */,
    0x0000003e    /* 120 (LREF1) */,
    0x0000002c    /* 121 (LREF-VAL0-BNGE 0 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 124),
    0x0000000c    /* 123 (CONSTU-RET) */,
    0x0000300e    /* 124 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 137),
    0x0000200e    /* 126 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 132),
    0x0000004e    /* 128 (LREF12-PUSH) */,
    0x00000048    /* 129 (LREF0-PUSH) */,
    0x0000205f    /* 130 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#unbox-value.da0abc0> */,
    0x0000000d    /* 132 (PUSH) */,
    0x0000004d    /* 133 (LREF11-PUSH) */,
    0x0000004c    /* 134 (LREF10-PUSH) */,
    0x0000305f    /* 135 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%write-walk-rec.da0ac00> */,
    0x00000049    /* 137 (LREF1-PUSH) */,
    0x000010c7    /* 138 (LREF0-NUMADDI-PUSH 1) */,
    0x0000101b    /* 139 (LOCAL-ENV-JUMP 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 120),
    0x00000014    /* 141 (RET) */,
    0x00000013    /* 142 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 150),
    0x0000200e    /* 144 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 150),
    0x0000004a    /* 146 (LREF2-PUSH) */,
    0x00000049    /* 147 (LREF1-PUSH) */,
    0x0000205f    /* 148 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#write-object.da0a860> */,
    0x0000100e    /* 150 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 155),
    0x00000049    /* 152 (LREF1-PUSH) */,
    0x0000105f    /* 153 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%port-writing-shared?.da0a7c0> */,
    0x0000001e    /* 155 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 158),
    0x0000000c    /* 157 (CONSTU-RET) */,
    0x0000300e    /* 158 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 165),
    0x00000048    /* 160 (LREF0-PUSH) */,
    0x0000004a    /* 161 (LREF2-PUSH) */,
    0x00000009    /* 162 (CONSTF-PUSH) */,
    0x0000305f    /* 163 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#hash-table-get.da0a6e0> */,
    0x0000002f    /* 165 (BNEQVC) */,
    SCM_WORD(SCM_MAKE_INT(1U)) /* 1 */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]) + 173)  /*    173 */,
    0x00000048    /* 168 (LREF0-PUSH) */,
    0x0000004a    /* 169 (LREF2-PUSH) */,
    0x00002060    /* 170 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#hash-table-delete!.da0a620> */,
    0x00000014    /* 172 (RET) */,
    0x0000000c    /* 173 (CONSTU-RET) */,
    0x0000000c    /* 174 (CONSTU-RET) */,
    /* %toplevel */
    0x00001019    /*   0 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[958])) /* (#<compiled-code (%write-walk-rec #:G1819)@0x7f4b0d4d7840>) */,
    0x0000000e    /*   2 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[996]) + 6),
    0x0000005f    /*   4 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000001    /*   7 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* %write-walk-rec */,
    0x000000ee    /*   9 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[996]) + 14),
    0x00000016    /*  11 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[38])) /* #<compiled-code %write-walk-rec@0x7f4b0d4d77e0> */,
    0x00000014    /*  13 (RET) */,
    0x00000015    /*  14 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%write-walk-rec.d9ffb20> */,
    0x00000014    /*  16 (RET) */,
    /* write-with-shared-structure */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1013]) + 6),
    0x000000df    /*   3 (CUROUT) */,
    0x00000013    /*   4 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1013]) + 7),
    0x0000006a    /*   6 (LREF0-CAR) */,
    0x0000000d    /*   7 (PUSH) */,
    0x0000003d    /*   8 (LREF0) */,
    0x00000022    /*   9 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1013]) + 14),
    0x00000003    /*  11 (CONSTN) */,
    0x00000013    /*  12 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1013]) + 15),
    0x00000076    /*  14 (LREF0-CDR) */,
    0x00002018    /*  15 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  16 (LREF0) */,
    0x00000022    /*  17 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1013]) + 21),
    0x00000013    /*  19 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1013]) + 29),
    0x0000200e    /*  21 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1013]) + 29),
    0x00000006    /*  23 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[151])) /* "too many arguments for" */,
    0x00000006    /*  25 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[973])) /* (lambda (obj :optional (port (current-output-port))) (write* obj port)) */,
    0x0000205f    /*  27 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.f79a860> */,
    0x0000004d    /*  29 (LREF11-PUSH) */,
    0x00000049    /*  30 (LREF1-PUSH) */,
    0x00002060    /*  31 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#write*.d7acfc0> */,
    0x00000014    /*  33 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1047]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* write-with-shared-structure */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1047]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[40])) /* #<compiled-code write-with-shared-structure@0x7f4b0d4844e0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#write-with-shared-structure.d6fde20> */,
    0x00000014    /*  14 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1062]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* write/ss */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1062]) + 12),
    0x0000005d    /*   9 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#write-with-shared-structure.d2ac360> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#write/ss.d2ac3c0> */,
    0x00000014    /*  14 (RET) */,
    /* print */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1077]) + 7),
    0x0000005e    /*   2 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#display.d365b40> */,
    0x00000048    /*   4 (LREF0-PUSH) */,
    0x0000205f    /*   5 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#for-each.d365b80> */,
    0x00000060    /*   7 (GREF-TAIL-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#newline.d365ae0> */,
    0x00000014    /*   9 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1087]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* print */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1087]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[43])) /* #<compiled-code print@0x7f4b0cfb95a0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#print.d365cc0> */,
    0x00000014    /*  14 (RET) */,
    /* make-write-controls */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00000005    /*   1 (CONSTU) */,
    0x0000000d    /*   2 (PUSH) */,
    0x00000005    /*   3 (CONSTU) */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000005    /*   5 (CONSTU) */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000005    /*   7 (CONSTU) */,
    0x0000000d    /*   8 (PUSH) */,
    0x00000005    /*   9 (CONSTU) */,
    0x0000000d    /*  10 (PUSH) */,
    0x00000005    /*  11 (CONSTU) */,
    0x0000000d    /*  12 (PUSH) */,
    0x00000005    /*  13 (CONSTU) */,
    0x0000000d    /*  14 (PUSH) */,
    0x00000005    /*  15 (CONSTU) */,
    0x0000000d    /*  16 (PUSH) */,
    0x00000005    /*  17 (CONSTU) */,
    0x0000000d    /*  18 (PUSH) */,
    0x00000005    /*  19 (CONSTU) */,
    0x0000000d    /*  20 (PUSH) */,
    0x00000005    /*  21 (CONSTU) */,
    0x0000000d    /*  22 (PUSH) */,
    0x00000005    /*  23 (CONSTU) */,
    0x0000000d    /*  24 (PUSH) */,
    0x00000005    /*  25 (CONSTU) */,
    0x0000000d    /*  26 (PUSH) */,
    0x00000005    /*  27 (CONSTU) */,
    0x0000f018    /*  28 (PUSH-LOCAL-ENV 15) */,
    0x0380003c    /*  29 (LREF 0 14) */,
    0x00000022    /*  30 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 295),
    0x0000100e    /*  32 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 37),
    0x03400047    /*  34 (LREF-PUSH 0 13) */,
    0x0000105f    /*  35 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.d22f1a0> */,
    0x0000001e    /*  37 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 42),
    0x00000005    /*  39 (CONSTU) */,
    0x00000013    /*  40 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 43),
    0x0340003c    /*  42 (LREF 0 13) */,
    0x00001018    /*  43 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  44 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 49),
    0x03001047    /*  46 (LREF-PUSH 1 12) */,
    0x0000105f    /*  47 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.d22f1a0> */,
    0x0000001e    /*  49 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 54),
    0x00000005    /*  51 (CONSTU) */,
    0x00000013    /*  52 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 55),
    0x0300103c    /*  54 (LREF 1 12) */,
    0x00001018    /*  55 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  56 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 61),
    0x02c02047    /*  58 (LREF-PUSH 2 11) */,
    0x0000105f    /*  59 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.d22f1a0> */,
    0x0000001e    /*  61 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 66),
    0x00000005    /*  63 (CONSTU) */,
    0x00000013    /*  64 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 67),
    0x02c0203c    /*  66 (LREF 2 11) */,
    0x00001018    /*  67 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  68 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 73),
    0x02803047    /*  70 (LREF-PUSH 3 10) */,
    0x0000105f    /*  71 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.d22f1a0> */,
    0x0000001e    /*  73 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 78),
    0x00000005    /*  75 (CONSTU) */,
    0x00000013    /*  76 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 79),
    0x0280303c    /*  78 (LREF 3 10) */,
    0x00001018    /*  79 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  80 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 85),
    0x02404047    /*  82 (LREF-PUSH 4 9) */,
    0x0000105f    /*  83 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.d22f1a0> */,
    0x0000001e    /*  85 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 90),
    0x00000005    /*  87 (CONSTU) */,
    0x00000013    /*  88 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 91),
    0x0240403c    /*  90 (LREF 4 9) */,
    0x00001018    /*  91 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  92 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 97),
    0x02005047    /*  94 (LREF-PUSH 5 8) */,
    0x0000105f    /*  95 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.d22f1a0> */,
    0x0000001e    /*  97 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 102),
    0x00000005    /*  99 (CONSTU) */,
    0x00000013    /* 100 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 103),
    0x0200503c    /* 102 (LREF 5 8) */,
    0x00001018    /* 103 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 104 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 109),
    0x01c06047    /* 106 (LREF-PUSH 6 7) */,
    0x0000105f    /* 107 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.d22f1a0> */,
    0x0000001e    /* 109 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 114),
    0x00000005    /* 111 (CONSTU) */,
    0x00000013    /* 112 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 115),
    0x01c0603c    /* 114 (LREF 6 7) */,
    0x00001018    /* 115 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 116 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 121),
    0x01807047    /* 118 (LREF-PUSH 7 6) */,
    0x0000105f    /* 119 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.d22f1a0> */,
    0x0000001e    /* 121 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 126),
    0x00000005    /* 123 (CONSTU) */,
    0x00000013    /* 124 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 127),
    0x0180703c    /* 126 (LREF 7 6) */,
    0x00001018    /* 127 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 128 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 133),
    0x01408047    /* 130 (LREF-PUSH 8 5) */,
    0x0000105f    /* 131 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.d22f1a0> */,
    0x0000001e    /* 133 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 138),
    0x00000005    /* 135 (CONSTU) */,
    0x00000013    /* 136 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 139),
    0x0140803c    /* 138 (LREF 8 5) */,
    0x00001018    /* 139 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 140 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 145),
    0x01009047    /* 142 (LREF-PUSH 9 4) */,
    0x0000105f    /* 143 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.d22f1a0> */,
    0x0000001e    /* 145 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 150),
    0x00000005    /* 147 (CONSTU) */,
    0x00000013    /* 148 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 151),
    0x0100903c    /* 150 (LREF 9 4) */,
    0x00001018    /* 151 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 152 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 157),
    0x00c0a047    /* 154 (LREF-PUSH 10 3) */,
    0x0000105f    /* 155 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.d22f1a0> */,
    0x0000001e    /* 157 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 162),
    0x00000005    /* 159 (CONSTU) */,
    0x00000013    /* 160 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 163),
    0x00c0a03c    /* 162 (LREF 10 3) */,
    0x00001018    /* 163 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 164 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 169),
    0x0080b047    /* 166 (LREF-PUSH 11 2) */,
    0x0000105f    /* 167 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.d22f1a0> */,
    0x0000001e    /* 169 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 174),
    0x00000005    /* 171 (CONSTU) */,
    0x00000013    /* 172 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 175),
    0x0080b03c    /* 174 (LREF 11 2) */,
    0x00001018    /* 175 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 176 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 181),
    0x0040c047    /* 178 (LREF-PUSH 12 1) */,
    0x0000105f    /* 179 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.d22f1a0> */,
    0x0000001e    /* 181 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 186),
    0x00000005    /* 183 (CONSTU) */,
    0x00000013    /* 184 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 187),
    0x0040c03c    /* 186 (LREF 12 1) */,
    0x00001018    /* 187 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 188 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 193),
    0x0000d047    /* 190 (LREF-PUSH 13 0) */,
    0x0000105f    /* 191 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.d22f1a0> */,
    0x0000001e    /* 193 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 198),
    0x00000005    /* 195 (CONSTU) */,
    0x00000013    /* 196 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 199),
    0x0000d03c    /* 198 (LREF 13 0) */,
    0x00001018    /* 199 (PUSH-LOCAL-ENV 1) */,
    0x0000005e    /* 200 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<write-controls>.f50c0c0> */,
    0x00000006    /* 202 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :length */,
    0x0000100e    /* 204 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 209),
    0x00005047    /* 206 (LREF-PUSH 5 0) */,
    0x0000105f    /* 207 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.f50c780> */,
    0x0000001e    /* 209 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 214),
    0x0000d03c    /* 211 (LREF 13 0) */,
    0x00000013    /* 212 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 215),
    0x0000503c    /* 214 (LREF 5 0) */,
    0x0000000d    /* 215 (PUSH) */,
    0x00000006    /* 216 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :level */,
    0x0000100e    /* 218 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 223),
    0x00004047    /* 220 (LREF-PUSH 4 0) */,
    0x0000105f    /* 221 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.f50c780> */,
    0x0000001e    /* 223 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 228),
    0x0000c03c    /* 225 (LREF 12 0) */,
    0x00000013    /* 226 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 229),
    0x0000403c    /* 228 (LREF 4 0) */,
    0x0000000d    /* 229 (PUSH) */,
    0x00000006    /* 230 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :width */,
    0x0000100e    /* 232 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 237),
    0x00000051    /* 234 (LREF30-PUSH) */,
    0x0000105f    /* 235 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.f50c780> */,
    0x0000001e    /* 237 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 242),
    0x0000b03c    /* 239 (LREF 11 0) */,
    0x00000013    /* 240 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 243),
    0x00000046    /* 242 (LREF30) */,
    0x0000000d    /* 243 (PUSH) */,
    0x00000006    /* 244 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :base */,
    0x0000100e    /* 246 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 251),
    0x0000004f    /* 248 (LREF20-PUSH) */,
    0x0000105f    /* 249 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.f50c780> */,
    0x0000001e    /* 251 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 256),
    0x0000a03c    /* 253 (LREF 10 0) */,
    0x00000013    /* 254 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 257),
    0x00000044    /* 256 (LREF20) */,
    0x0000000d    /* 257 (PUSH) */,
    0x00000006    /* 258 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :radix */,
    0x0000100e    /* 260 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 265),
    0x0000004c    /* 262 (LREF10-PUSH) */,
    0x0000105f    /* 263 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.f50c780> */,
    0x0000001e    /* 265 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 270),
    0x0000903c    /* 267 (LREF 9 0) */,
    0x00000013    /* 268 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 271),
    0x00000041    /* 270 (LREF10) */,
    0x0000000d    /* 271 (PUSH) */,
    0x00000006    /* 272 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :pretty */,
    0x0000100e    /* 274 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 279),
    0x00000048    /* 276 (LREF0-PUSH) */,
    0x0000105f    /* 277 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.f50c780> */,
    0x0000001e    /* 279 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 284),
    0x0000803c    /* 281 (LREF 8 0) */,
    0x00000013    /* 282 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 285),
    0x0000003d    /* 284 (LREF0) */,
    0x0000000d    /* 285 (PUSH) */,
    0x00000006    /* 286 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :string-length */,
    0x00006047    /* 288 (LREF-PUSH 6 0) */,
    0x00000006    /* 289 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :indent */,
    0x00007047    /* 291 (LREF-PUSH 7 0) */,
    0x00011060    /* 292 (GREF-TAIL-CALL 17) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make.f50c600> */,
    0x00000014    /* 294 (RET) */,
    0x0380003c    /* 295 (LREF 0 14) */,
    0x00000074    /* 296 (CDR) */,
    0x00000022    /* 297 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 305),
    0x00000006    /* 299 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[266])) /* "keyword list not even" */,
    0x03800047    /* 301 (LREF-PUSH 0 14) */,
    0x00002060    /* 302 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.d233d80> */,
    0x00000014    /* 304 (RET) */,
    0x0000100e    /* 305 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 311),
    0x0380003c    /* 307 (LREF 0 14) */,
    0x00000069    /* 308 (CAR-PUSH) */,
    0x0000105f    /* 309 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#unwrap-syntax-1.d233ce0> */,
    0x00001018    /* 311 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /* 312 (LREF0) */,
    0x0000002e    /* 313 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :length */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 336)  /*    336 */,
    0x0380103c    /* 316 (LREF 1 14) */,
    0x00000087    /* 317 (CDDR-PUSH) */,
    0x0380103c    /* 318 (LREF 1 14) */,
    0x00000083    /* 319 (CADR-PUSH) */,
    0x03001047    /* 320 (LREF-PUSH 1 12) */,
    0x02c01047    /* 321 (LREF-PUSH 1 11) */,
    0x02801047    /* 322 (LREF-PUSH 1 10) */,
    0x02401047    /* 323 (LREF-PUSH 1 9) */,
    0x02001047    /* 324 (LREF-PUSH 1 8) */,
    0x01c01047    /* 325 (LREF-PUSH 1 7) */,
    0x01801047    /* 326 (LREF-PUSH 1 6) */,
    0x01401047    /* 327 (LREF-PUSH 1 5) */,
    0x01001047    /* 328 (LREF-PUSH 1 4) */,
    0x00c01047    /* 329 (LREF-PUSH 1 3) */,
    0x0000004e    /* 330 (LREF12-PUSH) */,
    0x0000004d    /* 331 (LREF11-PUSH) */,
    0x0000004c    /* 332 (LREF10-PUSH) */,
    0x0000201b    /* 333 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 29),
    0x00000014    /* 335 (RET) */,
    0x0000003d    /* 336 (LREF0) */,
    0x0000002e    /* 337 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :level */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 360)  /*    360 */,
    0x0380103c    /* 340 (LREF 1 14) */,
    0x00000087    /* 341 (CDDR-PUSH) */,
    0x03401047    /* 342 (LREF-PUSH 1 13) */,
    0x0380103c    /* 343 (LREF 1 14) */,
    0x00000083    /* 344 (CADR-PUSH) */,
    0x02c01047    /* 345 (LREF-PUSH 1 11) */,
    0x02801047    /* 346 (LREF-PUSH 1 10) */,
    0x02401047    /* 347 (LREF-PUSH 1 9) */,
    0x02001047    /* 348 (LREF-PUSH 1 8) */,
    0x01c01047    /* 349 (LREF-PUSH 1 7) */,
    0x01801047    /* 350 (LREF-PUSH 1 6) */,
    0x01401047    /* 351 (LREF-PUSH 1 5) */,
    0x01001047    /* 352 (LREF-PUSH 1 4) */,
    0x00c01047    /* 353 (LREF-PUSH 1 3) */,
    0x0000004e    /* 354 (LREF12-PUSH) */,
    0x0000004d    /* 355 (LREF11-PUSH) */,
    0x0000004c    /* 356 (LREF10-PUSH) */,
    0x0000201b    /* 357 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 29),
    0x00000014    /* 359 (RET) */,
    0x0000003d    /* 360 (LREF0) */,
    0x0000002e    /* 361 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :width */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 384)  /*    384 */,
    0x0380103c    /* 364 (LREF 1 14) */,
    0x00000087    /* 365 (CDDR-PUSH) */,
    0x03401047    /* 366 (LREF-PUSH 1 13) */,
    0x03001047    /* 367 (LREF-PUSH 1 12) */,
    0x0380103c    /* 368 (LREF 1 14) */,
    0x00000083    /* 369 (CADR-PUSH) */,
    0x02801047    /* 370 (LREF-PUSH 1 10) */,
    0x02401047    /* 371 (LREF-PUSH 1 9) */,
    0x02001047    /* 372 (LREF-PUSH 1 8) */,
    0x01c01047    /* 373 (LREF-PUSH 1 7) */,
    0x01801047    /* 374 (LREF-PUSH 1 6) */,
    0x01401047    /* 375 (LREF-PUSH 1 5) */,
    0x01001047    /* 376 (LREF-PUSH 1 4) */,
    0x00c01047    /* 377 (LREF-PUSH 1 3) */,
    0x0000004e    /* 378 (LREF12-PUSH) */,
    0x0000004d    /* 379 (LREF11-PUSH) */,
    0x0000004c    /* 380 (LREF10-PUSH) */,
    0x0000201b    /* 381 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 29),
    0x00000014    /* 383 (RET) */,
    0x0000003d    /* 384 (LREF0) */,
    0x0000002e    /* 385 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :base */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 408)  /*    408 */,
    0x0380103c    /* 388 (LREF 1 14) */,
    0x00000087    /* 389 (CDDR-PUSH) */,
    0x03401047    /* 390 (LREF-PUSH 1 13) */,
    0x03001047    /* 391 (LREF-PUSH 1 12) */,
    0x02c01047    /* 392 (LREF-PUSH 1 11) */,
    0x0380103c    /* 393 (LREF 1 14) */,
    0x00000083    /* 394 (CADR-PUSH) */,
    0x02401047    /* 395 (LREF-PUSH 1 9) */,
    0x02001047    /* 396 (LREF-PUSH 1 8) */,
    0x01c01047    /* 397 (LREF-PUSH 1 7) */,
    0x01801047    /* 398 (LREF-PUSH 1 6) */,
    0x01401047    /* 399 (LREF-PUSH 1 5) */,
    0x01001047    /* 400 (LREF-PUSH 1 4) */,
    0x00c01047    /* 401 (LREF-PUSH 1 3) */,
    0x0000004e    /* 402 (LREF12-PUSH) */,
    0x0000004d    /* 403 (LREF11-PUSH) */,
    0x0000004c    /* 404 (LREF10-PUSH) */,
    0x0000201b    /* 405 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 29),
    0x00000014    /* 407 (RET) */,
    0x0000003d    /* 408 (LREF0) */,
    0x0000002e    /* 409 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :radix */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 432)  /*    432 */,
    0x0380103c    /* 412 (LREF 1 14) */,
    0x00000087    /* 413 (CDDR-PUSH) */,
    0x03401047    /* 414 (LREF-PUSH 1 13) */,
    0x03001047    /* 415 (LREF-PUSH 1 12) */,
    0x02c01047    /* 416 (LREF-PUSH 1 11) */,
    0x02801047    /* 417 (LREF-PUSH 1 10) */,
    0x0380103c    /* 418 (LREF 1 14) */,
    0x00000083    /* 419 (CADR-PUSH) */,
    0x02001047    /* 420 (LREF-PUSH 1 8) */,
    0x01c01047    /* 421 (LREF-PUSH 1 7) */,
    0x01801047    /* 422 (LREF-PUSH 1 6) */,
    0x01401047    /* 423 (LREF-PUSH 1 5) */,
    0x01001047    /* 424 (LREF-PUSH 1 4) */,
    0x00c01047    /* 425 (LREF-PUSH 1 3) */,
    0x0000004e    /* 426 (LREF12-PUSH) */,
    0x0000004d    /* 427 (LREF11-PUSH) */,
    0x0000004c    /* 428 (LREF10-PUSH) */,
    0x0000201b    /* 429 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 29),
    0x00000014    /* 431 (RET) */,
    0x0000003d    /* 432 (LREF0) */,
    0x0000002e    /* 433 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :pretty */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 456)  /*    456 */,
    0x0380103c    /* 436 (LREF 1 14) */,
    0x00000087    /* 437 (CDDR-PUSH) */,
    0x03401047    /* 438 (LREF-PUSH 1 13) */,
    0x03001047    /* 439 (LREF-PUSH 1 12) */,
    0x02c01047    /* 440 (LREF-PUSH 1 11) */,
    0x02801047    /* 441 (LREF-PUSH 1 10) */,
    0x02401047    /* 442 (LREF-PUSH 1 9) */,
    0x0380103c    /* 443 (LREF 1 14) */,
    0x00000083    /* 444 (CADR-PUSH) */,
    0x01c01047    /* 445 (LREF-PUSH 1 7) */,
    0x01801047    /* 446 (LREF-PUSH 1 6) */,
    0x01401047    /* 447 (LREF-PUSH 1 5) */,
    0x01001047    /* 448 (LREF-PUSH 1 4) */,
    0x00c01047    /* 449 (LREF-PUSH 1 3) */,
    0x0000004e    /* 450 (LREF12-PUSH) */,
    0x0000004d    /* 451 (LREF11-PUSH) */,
    0x0000004c    /* 452 (LREF10-PUSH) */,
    0x0000201b    /* 453 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 29),
    0x00000014    /* 455 (RET) */,
    0x0000003d    /* 456 (LREF0) */,
    0x0000002e    /* 457 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :indent */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 480)  /*    480 */,
    0x0380103c    /* 460 (LREF 1 14) */,
    0x00000087    /* 461 (CDDR-PUSH) */,
    0x03401047    /* 462 (LREF-PUSH 1 13) */,
    0x03001047    /* 463 (LREF-PUSH 1 12) */,
    0x02c01047    /* 464 (LREF-PUSH 1 11) */,
    0x02801047    /* 465 (LREF-PUSH 1 10) */,
    0x02401047    /* 466 (LREF-PUSH 1 9) */,
    0x02001047    /* 467 (LREF-PUSH 1 8) */,
    0x0380103c    /* 468 (LREF 1 14) */,
    0x00000083    /* 469 (CADR-PUSH) */,
    0x01801047    /* 470 (LREF-PUSH 1 6) */,
    0x01401047    /* 471 (LREF-PUSH 1 5) */,
    0x01001047    /* 472 (LREF-PUSH 1 4) */,
    0x00c01047    /* 473 (LREF-PUSH 1 3) */,
    0x0000004e    /* 474 (LREF12-PUSH) */,
    0x0000004d    /* 475 (LREF11-PUSH) */,
    0x0000004c    /* 476 (LREF10-PUSH) */,
    0x0000201b    /* 477 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 29),
    0x00000014    /* 479 (RET) */,
    0x0000003d    /* 480 (LREF0) */,
    0x0000002e    /* 481 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :string-length */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 504)  /*    504 */,
    0x0380103c    /* 484 (LREF 1 14) */,
    0x00000087    /* 485 (CDDR-PUSH) */,
    0x03401047    /* 486 (LREF-PUSH 1 13) */,
    0x03001047    /* 487 (LREF-PUSH 1 12) */,
    0x02c01047    /* 488 (LREF-PUSH 1 11) */,
    0x02801047    /* 489 (LREF-PUSH 1 10) */,
    0x02401047    /* 490 (LREF-PUSH 1 9) */,
    0x02001047    /* 491 (LREF-PUSH 1 8) */,
    0x01c01047    /* 492 (LREF-PUSH 1 7) */,
    0x0380103c    /* 493 (LREF 1 14) */,
    0x00000083    /* 494 (CADR-PUSH) */,
    0x01401047    /* 495 (LREF-PUSH 1 5) */,
    0x01001047    /* 496 (LREF-PUSH 1 4) */,
    0x00c01047    /* 497 (LREF-PUSH 1 3) */,
    0x0000004e    /* 498 (LREF12-PUSH) */,
    0x0000004d    /* 499 (LREF11-PUSH) */,
    0x0000004c    /* 500 (LREF10-PUSH) */,
    0x0000201b    /* 501 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 29),
    0x00000014    /* 503 (RET) */,
    0x0000003d    /* 504 (LREF0) */,
    0x0000002e    /* 505 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :print-length */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 528)  /*    528 */,
    0x0380103c    /* 508 (LREF 1 14) */,
    0x00000087    /* 509 (CDDR-PUSH) */,
    0x03401047    /* 510 (LREF-PUSH 1 13) */,
    0x03001047    /* 511 (LREF-PUSH 1 12) */,
    0x02c01047    /* 512 (LREF-PUSH 1 11) */,
    0x02801047    /* 513 (LREF-PUSH 1 10) */,
    0x02401047    /* 514 (LREF-PUSH 1 9) */,
    0x02001047    /* 515 (LREF-PUSH 1 8) */,
    0x01c01047    /* 516 (LREF-PUSH 1 7) */,
    0x01801047    /* 517 (LREF-PUSH 1 6) */,
    0x0380103c    /* 518 (LREF 1 14) */,
    0x00000083    /* 519 (CADR-PUSH) */,
    0x01001047    /* 520 (LREF-PUSH 1 4) */,
    0x00c01047    /* 521 (LREF-PUSH 1 3) */,
    0x0000004e    /* 522 (LREF12-PUSH) */,
    0x0000004d    /* 523 (LREF11-PUSH) */,
    0x0000004c    /* 524 (LREF10-PUSH) */,
    0x0000201b    /* 525 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 29),
    0x00000014    /* 527 (RET) */,
    0x0000003d    /* 528 (LREF0) */,
    0x0000002e    /* 529 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :print-level */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 552)  /*    552 */,
    0x0380103c    /* 532 (LREF 1 14) */,
    0x00000087    /* 533 (CDDR-PUSH) */,
    0x03401047    /* 534 (LREF-PUSH 1 13) */,
    0x03001047    /* 535 (LREF-PUSH 1 12) */,
    0x02c01047    /* 536 (LREF-PUSH 1 11) */,
    0x02801047    /* 537 (LREF-PUSH 1 10) */,
    0x02401047    /* 538 (LREF-PUSH 1 9) */,
    0x02001047    /* 539 (LREF-PUSH 1 8) */,
    0x01c01047    /* 540 (LREF-PUSH 1 7) */,
    0x01801047    /* 541 (LREF-PUSH 1 6) */,
    0x01401047    /* 542 (LREF-PUSH 1 5) */,
    0x0380103c    /* 543 (LREF 1 14) */,
    0x00000083    /* 544 (CADR-PUSH) */,
    0x00c01047    /* 545 (LREF-PUSH 1 3) */,
    0x0000004e    /* 546 (LREF12-PUSH) */,
    0x0000004d    /* 547 (LREF11-PUSH) */,
    0x0000004c    /* 548 (LREF10-PUSH) */,
    0x0000201b    /* 549 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 29),
    0x00000014    /* 551 (RET) */,
    0x0000003d    /* 552 (LREF0) */,
    0x0000002e    /* 553 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :print-width */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 576)  /*    576 */,
    0x0380103c    /* 556 (LREF 1 14) */,
    0x00000087    /* 557 (CDDR-PUSH) */,
    0x03401047    /* 558 (LREF-PUSH 1 13) */,
    0x03001047    /* 559 (LREF-PUSH 1 12) */,
    0x02c01047    /* 560 (LREF-PUSH 1 11) */,
    0x02801047    /* 561 (LREF-PUSH 1 10) */,
    0x02401047    /* 562 (LREF-PUSH 1 9) */,
    0x02001047    /* 563 (LREF-PUSH 1 8) */,
    0x01c01047    /* 564 (LREF-PUSH 1 7) */,
    0x01801047    /* 565 (LREF-PUSH 1 6) */,
    0x01401047    /* 566 (LREF-PUSH 1 5) */,
    0x01001047    /* 567 (LREF-PUSH 1 4) */,
    0x0380103c    /* 568 (LREF 1 14) */,
    0x00000083    /* 569 (CADR-PUSH) */,
    0x0000004e    /* 570 (LREF12-PUSH) */,
    0x0000004d    /* 571 (LREF11-PUSH) */,
    0x0000004c    /* 572 (LREF10-PUSH) */,
    0x0000201b    /* 573 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 29),
    0x00000014    /* 575 (RET) */,
    0x0000003d    /* 576 (LREF0) */,
    0x0000002e    /* 577 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :print-base */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 600)  /*    600 */,
    0x0380103c    /* 580 (LREF 1 14) */,
    0x00000087    /* 581 (CDDR-PUSH) */,
    0x03401047    /* 582 (LREF-PUSH 1 13) */,
    0x03001047    /* 583 (LREF-PUSH 1 12) */,
    0x02c01047    /* 584 (LREF-PUSH 1 11) */,
    0x02801047    /* 585 (LREF-PUSH 1 10) */,
    0x02401047    /* 586 (LREF-PUSH 1 9) */,
    0x02001047    /* 587 (LREF-PUSH 1 8) */,
    0x01c01047    /* 588 (LREF-PUSH 1 7) */,
    0x01801047    /* 589 (LREF-PUSH 1 6) */,
    0x01401047    /* 590 (LREF-PUSH 1 5) */,
    0x01001047    /* 591 (LREF-PUSH 1 4) */,
    0x00c01047    /* 592 (LREF-PUSH 1 3) */,
    0x0380103c    /* 593 (LREF 1 14) */,
    0x00000083    /* 594 (CADR-PUSH) */,
    0x0000004d    /* 595 (LREF11-PUSH) */,
    0x0000004c    /* 596 (LREF10-PUSH) */,
    0x0000201b    /* 597 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 29),
    0x00000014    /* 599 (RET) */,
    0x0000003d    /* 600 (LREF0) */,
    0x0000002e    /* 601 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :print-radix */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 624)  /*    624 */,
    0x0380103c    /* 604 (LREF 1 14) */,
    0x00000087    /* 605 (CDDR-PUSH) */,
    0x03401047    /* 606 (LREF-PUSH 1 13) */,
    0x03001047    /* 607 (LREF-PUSH 1 12) */,
    0x02c01047    /* 608 (LREF-PUSH 1 11) */,
    0x02801047    /* 609 (LREF-PUSH 1 10) */,
    0x02401047    /* 610 (LREF-PUSH 1 9) */,
    0x02001047    /* 611 (LREF-PUSH 1 8) */,
    0x01c01047    /* 612 (LREF-PUSH 1 7) */,
    0x01801047    /* 613 (LREF-PUSH 1 6) */,
    0x01401047    /* 614 (LREF-PUSH 1 5) */,
    0x01001047    /* 615 (LREF-PUSH 1 4) */,
    0x00c01047    /* 616 (LREF-PUSH 1 3) */,
    0x0000004e    /* 617 (LREF12-PUSH) */,
    0x0380103c    /* 618 (LREF 1 14) */,
    0x00000083    /* 619 (CADR-PUSH) */,
    0x0000004c    /* 620 (LREF10-PUSH) */,
    0x0000201b    /* 621 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 29),
    0x00000014    /* 623 (RET) */,
    0x0000003d    /* 624 (LREF0) */,
    0x0000002e    /* 625 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :print-pretty */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 648)  /*    648 */,
    0x0380103c    /* 628 (LREF 1 14) */,
    0x00000087    /* 629 (CDDR-PUSH) */,
    0x03401047    /* 630 (LREF-PUSH 1 13) */,
    0x03001047    /* 631 (LREF-PUSH 1 12) */,
    0x02c01047    /* 632 (LREF-PUSH 1 11) */,
    0x02801047    /* 633 (LREF-PUSH 1 10) */,
    0x02401047    /* 634 (LREF-PUSH 1 9) */,
    0x02001047    /* 635 (LREF-PUSH 1 8) */,
    0x01c01047    /* 636 (LREF-PUSH 1 7) */,
    0x01801047    /* 637 (LREF-PUSH 1 6) */,
    0x01401047    /* 638 (LREF-PUSH 1 5) */,
    0x01001047    /* 639 (LREF-PUSH 1 4) */,
    0x00c01047    /* 640 (LREF-PUSH 1 3) */,
    0x0000004e    /* 641 (LREF12-PUSH) */,
    0x0000004d    /* 642 (LREF11-PUSH) */,
    0x0380103c    /* 643 (LREF 1 14) */,
    0x00000083    /* 644 (CADR-PUSH) */,
    0x0000201b    /* 645 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 29),
    0x00000014    /* 647 (RET) */,
    0x0000200e    /* 648 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 656),
    0x00000006    /* 650 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[274])) /* "unknown keyword ~S" */,
    0x0380103c    /* 652 (LREF 1 14) */,
    0x00000069    /* 653 (CAR-PUSH) */,
    0x0000205f    /* 654 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#errorf.d233500> */,
    0x0380103c    /* 656 (LREF 1 14) */,
    0x00000087    /* 657 (CDDR-PUSH) */,
    0x03401047    /* 658 (LREF-PUSH 1 13) */,
    0x03001047    /* 659 (LREF-PUSH 1 12) */,
    0x02c01047    /* 660 (LREF-PUSH 1 11) */,
    0x02801047    /* 661 (LREF-PUSH 1 10) */,
    0x02401047    /* 662 (LREF-PUSH 1 9) */,
    0x02001047    /* 663 (LREF-PUSH 1 8) */,
    0x01c01047    /* 664 (LREF-PUSH 1 7) */,
    0x01801047    /* 665 (LREF-PUSH 1 6) */,
    0x01401047    /* 666 (LREF-PUSH 1 5) */,
    0x01001047    /* 667 (LREF-PUSH 1 4) */,
    0x00c01047    /* 668 (LREF-PUSH 1 3) */,
    0x0000004e    /* 669 (LREF12-PUSH) */,
    0x0000004d    /* 670 (LREF11-PUSH) */,
    0x0000004c    /* 671 (LREF10-PUSH) */,
    0x0000201b    /* 672 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]) + 29),
    0x00000014    /* 674 (RET) */,
    0x00000014    /* 675 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1778]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* make-write-controls */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1778]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[45])) /* #<compiled-code make-write-controls@0x7f4b0d22ede0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-write-controls.d2273a0> */,
    0x00000014    /*  14 (RET) */,
    /* write-controls-copy */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00000005    /*   1 (CONSTU) */,
    0x0000000d    /*   2 (PUSH) */,
    0x00000005    /*   3 (CONSTU) */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000005    /*   5 (CONSTU) */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000005    /*   7 (CONSTU) */,
    0x0000000d    /*   8 (PUSH) */,
    0x00000005    /*   9 (CONSTU) */,
    0x0000000d    /*  10 (PUSH) */,
    0x00000005    /*  11 (CONSTU) */,
    0x0000000d    /*  12 (PUSH) */,
    0x00000005    /*  13 (CONSTU) */,
    0x0000000d    /*  14 (PUSH) */,
    0x00000005    /*  15 (CONSTU) */,
    0x0000000d    /*  16 (PUSH) */,
    0x00000005    /*  17 (CONSTU) */,
    0x0000000d    /*  18 (PUSH) */,
    0x00000005    /*  19 (CONSTU) */,
    0x0000000d    /*  20 (PUSH) */,
    0x00000005    /*  21 (CONSTU) */,
    0x0000000d    /*  22 (PUSH) */,
    0x00000005    /*  23 (CONSTU) */,
    0x0000000d    /*  24 (PUSH) */,
    0x00000005    /*  25 (CONSTU) */,
    0x0000000d    /*  26 (PUSH) */,
    0x00000005    /*  27 (CONSTU) */,
    0x0000f018    /*  28 (PUSH-LOCAL-ENV 15) */,
    0x0380003c    /*  29 (LREF 0 14) */,
    0x00000022    /*  30 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 464),
    0x0000100e    /*  32 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 37),
    0x03400047    /*  34 (LREF-PUSH 0 13) */,
    0x0000105f    /*  35 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.ec9aa80> */,
    0x0000001e    /*  37 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 42),
    0x00000005    /*  39 (CONSTU) */,
    0x00000013    /*  40 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 43),
    0x0340003c    /*  42 (LREF 0 13) */,
    0x00001018    /*  43 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  44 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 49),
    0x03001047    /*  46 (LREF-PUSH 1 12) */,
    0x0000105f    /*  47 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.ec9aa80> */,
    0x0000001e    /*  49 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 54),
    0x00000005    /*  51 (CONSTU) */,
    0x00000013    /*  52 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 55),
    0x0300103c    /*  54 (LREF 1 12) */,
    0x00001018    /*  55 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  56 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 61),
    0x02c02047    /*  58 (LREF-PUSH 2 11) */,
    0x0000105f    /*  59 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.ec9aa80> */,
    0x0000001e    /*  61 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 66),
    0x00000005    /*  63 (CONSTU) */,
    0x00000013    /*  64 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 67),
    0x02c0203c    /*  66 (LREF 2 11) */,
    0x00001018    /*  67 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  68 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 73),
    0x02803047    /*  70 (LREF-PUSH 3 10) */,
    0x0000105f    /*  71 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.ec9aa80> */,
    0x0000001e    /*  73 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 78),
    0x00000005    /*  75 (CONSTU) */,
    0x00000013    /*  76 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 79),
    0x0280303c    /*  78 (LREF 3 10) */,
    0x00001018    /*  79 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  80 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 85),
    0x02404047    /*  82 (LREF-PUSH 4 9) */,
    0x0000105f    /*  83 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.ec9aa80> */,
    0x0000001e    /*  85 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 90),
    0x00000005    /*  87 (CONSTU) */,
    0x00000013    /*  88 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 91),
    0x0240403c    /*  90 (LREF 4 9) */,
    0x00001018    /*  91 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  92 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 97),
    0x02005047    /*  94 (LREF-PUSH 5 8) */,
    0x0000105f    /*  95 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.ec9aa80> */,
    0x0000001e    /*  97 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 102),
    0x00000005    /*  99 (CONSTU) */,
    0x00000013    /* 100 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 103),
    0x0200503c    /* 102 (LREF 5 8) */,
    0x00001018    /* 103 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 104 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 109),
    0x01c06047    /* 106 (LREF-PUSH 6 7) */,
    0x0000105f    /* 107 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.ec9aa80> */,
    0x0000001e    /* 109 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 114),
    0x00000005    /* 111 (CONSTU) */,
    0x00000013    /* 112 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 115),
    0x01c0603c    /* 114 (LREF 6 7) */,
    0x00001018    /* 115 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 116 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 121),
    0x01807047    /* 118 (LREF-PUSH 7 6) */,
    0x0000105f    /* 119 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.ec9aa80> */,
    0x0000001e    /* 121 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 126),
    0x00000005    /* 123 (CONSTU) */,
    0x00000013    /* 124 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 127),
    0x0180703c    /* 126 (LREF 7 6) */,
    0x00001018    /* 127 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 128 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 133),
    0x01408047    /* 130 (LREF-PUSH 8 5) */,
    0x0000105f    /* 131 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.ec9aa80> */,
    0x0000001e    /* 133 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 138),
    0x00000005    /* 135 (CONSTU) */,
    0x00000013    /* 136 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 139),
    0x0140803c    /* 138 (LREF 8 5) */,
    0x00001018    /* 139 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 140 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 145),
    0x01009047    /* 142 (LREF-PUSH 9 4) */,
    0x0000105f    /* 143 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.ec9aa80> */,
    0x0000001e    /* 145 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 150),
    0x00000005    /* 147 (CONSTU) */,
    0x00000013    /* 148 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 151),
    0x0100903c    /* 150 (LREF 9 4) */,
    0x00001018    /* 151 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 152 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 157),
    0x00c0a047    /* 154 (LREF-PUSH 10 3) */,
    0x0000105f    /* 155 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.ec9aa80> */,
    0x0000001e    /* 157 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 162),
    0x00000005    /* 159 (CONSTU) */,
    0x00000013    /* 160 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 163),
    0x00c0a03c    /* 162 (LREF 10 3) */,
    0x00001018    /* 163 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 164 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 169),
    0x0080b047    /* 166 (LREF-PUSH 11 2) */,
    0x0000105f    /* 167 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.ec9aa80> */,
    0x0000001e    /* 169 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 174),
    0x00000005    /* 171 (CONSTU) */,
    0x00000013    /* 172 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 175),
    0x0080b03c    /* 174 (LREF 11 2) */,
    0x00001018    /* 175 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 176 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 181),
    0x0040c047    /* 178 (LREF-PUSH 12 1) */,
    0x0000105f    /* 179 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.ec9aa80> */,
    0x0000001e    /* 181 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 186),
    0x00000005    /* 183 (CONSTU) */,
    0x00000013    /* 184 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 187),
    0x0040c03c    /* 186 (LREF 12 1) */,
    0x00001018    /* 187 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 188 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 193),
    0x0000d047    /* 190 (LREF-PUSH 13 0) */,
    0x0000105f    /* 191 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.ec9aa80> */,
    0x0000001e    /* 193 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 198),
    0x00000005    /* 195 (CONSTU) */,
    0x00000013    /* 196 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 199),
    0x0000d03c    /* 198 (LREF 13 0) */,
    0x00001018    /* 199 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /* 200 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 205),
    0x0000d047    /* 202 (LREF-PUSH 13 0) */,
    0x0000105f    /* 203 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.ecb5660> */,
    0x0000001e    /* 205 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 222),
    0x0000100e    /* 207 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 212),
    0x00005047    /* 209 (LREF-PUSH 5 0) */,
    0x0000105f    /* 210 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.ecb5660> */,
    0x0000001e    /* 212 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 219),
    0x0040f03c    /* 214 (LREF 15 1) */,
    0x000000e3    /* 215 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* length */,
    0x00000013    /* 217 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 223),
    0x0000503c    /* 219 (LREF 5 0) */,
    0x00000013    /* 220 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 223),
    0x0000d03c    /* 222 (LREF 13 0) */,
    0x0000100f    /* 223 (PUSH-PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 228),
    0x0000c047    /* 225 (LREF-PUSH 12 0) */,
    0x0000105f    /* 226 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.ecb5100> */,
    0x0000001e    /* 228 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 245),
    0x0000100e    /* 230 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 235),
    0x00004047    /* 232 (LREF-PUSH 4 0) */,
    0x0000105f    /* 233 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.ecb5100> */,
    0x0000001e    /* 235 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 242),
    0x0040f03c    /* 237 (LREF 15 1) */,
    0x000000e3    /* 238 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* level */,
    0x00000013    /* 240 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 246),
    0x0000403c    /* 242 (LREF 4 0) */,
    0x00000013    /* 243 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 246),
    0x0000c03c    /* 245 (LREF 12 0) */,
    0x0000100f    /* 246 (PUSH-PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 251),
    0x0000b047    /* 248 (LREF-PUSH 11 0) */,
    0x0000105f    /* 249 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.ecb7dc0> */,
    0x0000001e    /* 251 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 268),
    0x0000100e    /* 253 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 258),
    0x00000051    /* 255 (LREF30-PUSH) */,
    0x0000105f    /* 256 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.ecb7dc0> */,
    0x0000001e    /* 258 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 265),
    0x0040f03c    /* 260 (LREF 15 1) */,
    0x000000e3    /* 261 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* width */,
    0x00000013    /* 263 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 269),
    0x00000046    /* 265 (LREF30) */,
    0x00000013    /* 266 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 269),
    0x0000b03c    /* 268 (LREF 11 0) */,
    0x0000100f    /* 269 (PUSH-PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 274),
    0x0000a047    /* 271 (LREF-PUSH 10 0) */,
    0x0000105f    /* 272 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.ecb79e0> */,
    0x0000001e    /* 274 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 291),
    0x0000100e    /* 276 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 281),
    0x0000004f    /* 278 (LREF20-PUSH) */,
    0x0000105f    /* 279 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.ecb79e0> */,
    0x0000001e    /* 281 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 288),
    0x0040f03c    /* 283 (LREF 15 1) */,
    0x000000e3    /* 284 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* base */,
    0x00000013    /* 286 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 292),
    0x00000044    /* 288 (LREF20) */,
    0x00000013    /* 289 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 292),
    0x0000a03c    /* 291 (LREF 10 0) */,
    0x0000100f    /* 292 (PUSH-PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 297),
    0x00009047    /* 294 (LREF-PUSH 9 0) */,
    0x0000105f    /* 295 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.ecb75c0> */,
    0x0000001e    /* 297 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 314),
    0x0000100e    /* 299 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 304),
    0x0000004c    /* 301 (LREF10-PUSH) */,
    0x0000105f    /* 302 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.ecb75c0> */,
    0x0000001e    /* 304 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 311),
    0x0040f03c    /* 306 (LREF 15 1) */,
    0x000000e3    /* 307 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* radix */,
    0x00000013    /* 309 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 315),
    0x00000041    /* 311 (LREF10) */,
    0x00000013    /* 312 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 315),
    0x0000903c    /* 314 (LREF 9 0) */,
    0x0000100f    /* 315 (PUSH-PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 320),
    0x00008047    /* 317 (LREF-PUSH 8 0) */,
    0x0000105f    /* 318 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.ecbfb00> */,
    0x0000001e    /* 320 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 337),
    0x0000100e    /* 322 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 327),
    0x00000048    /* 324 (LREF0-PUSH) */,
    0x0000105f    /* 325 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.ecbfb00> */,
    0x0000001e    /* 327 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 334),
    0x0040f03c    /* 329 (LREF 15 1) */,
    0x000000e3    /* 330 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* pretty */,
    0x00000013    /* 332 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 338),
    0x0000003d    /* 334 (LREF0) */,
    0x00000013    /* 335 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 338),
    0x0000803c    /* 337 (LREF 8 0) */,
    0x0000100f    /* 338 (PUSH-PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 343),
    0x00007047    /* 340 (LREF-PUSH 7 0) */,
    0x0000105f    /* 341 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.ecbf7e0> */,
    0x0000001e    /* 343 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 350),
    0x0040f03c    /* 345 (LREF 15 1) */,
    0x000000e3    /* 346 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* indent */,
    0x00000013    /* 348 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 351),
    0x0000703c    /* 350 (LREF 7 0) */,
    0x0000100f    /* 351 (PUSH-PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 356),
    0x00006047    /* 353 (LREF-PUSH 6 0) */,
    0x0000105f    /* 354 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#undefined?.ecbf5c0> */,
    0x0000001e    /* 356 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 363),
    0x0040f03c    /* 358 (LREF 15 1) */,
    0x000000e3    /* 359 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* string-length */,
    0x00000013    /* 361 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 364),
    0x0000603c    /* 363 (LREF 6 0) */,
    0x00008018    /* 364 (PUSH-LOCAL-ENV 8) */,
    0x01c00047    /* 365 (LREF-PUSH 0 7) */,
    0x0041003c    /* 366 (LREF 16 1) */,
    0x000000e3    /* 367 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* length */,
    0x00000021    /* 369 (BNEQV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 414),
    0x01800047    /* 371 (LREF-PUSH 0 6) */,
    0x0041003c    /* 372 (LREF 16 1) */,
    0x000000e3    /* 373 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* level */,
    0x00000021    /* 375 (BNEQV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 414),
    0x01400047    /* 377 (LREF-PUSH 0 5) */,
    0x0041003c    /* 378 (LREF 16 1) */,
    0x000000e3    /* 379 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* width */,
    0x00000021    /* 381 (BNEQV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 414),
    0x01000047    /* 383 (LREF-PUSH 0 4) */,
    0x0041003c    /* 384 (LREF 16 1) */,
    0x000000e3    /* 385 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* base */,
    0x00000021    /* 387 (BNEQV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 414),
    0x0000004b    /* 389 (LREF3-PUSH) */,
    0x0041003c    /* 390 (LREF 16 1) */,
    0x000000e3    /* 391 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* radix */,
    0x00000021    /* 393 (BNEQV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 414),
    0x0000004a    /* 395 (LREF2-PUSH) */,
    0x0041003c    /* 396 (LREF 16 1) */,
    0x000000e3    /* 397 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* pretty */,
    0x00000021    /* 399 (BNEQV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 414),
    0x00000049    /* 401 (LREF1-PUSH) */,
    0x0041003c    /* 402 (LREF 16 1) */,
    0x000000e3    /* 403 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* indent */,
    0x00000021    /* 405 (BNEQV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 414),
    0x00000048    /* 407 (LREF0-PUSH) */,
    0x0041003c    /* 408 (LREF 16 1) */,
    0x000000e3    /* 409 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* string-length */,
    0x00000021    /* 411 (BNEQV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 414),
    0x00410052    /* 413 (LREF-RET 16 1) */,
    0x0000005e    /* 414 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<write-controls>.eccaec0> */,
    0x00000006    /* 416 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :length */,
    0x01c00047    /* 418 (LREF-PUSH 0 7) */,
    0x00000006    /* 419 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :level */,
    0x01800047    /* 421 (LREF-PUSH 0 6) */,
    0x00000006    /* 422 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :width */,
    0x01400047    /* 424 (LREF-PUSH 0 5) */,
    0x00000006    /* 425 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :base */,
    0x01000047    /* 427 (LREF-PUSH 0 4) */,
    0x00000006    /* 428 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :radix */,
    0x0000004b    /* 430 (LREF3-PUSH) */,
    0x00000006    /* 431 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :pretty */,
    0x0000004a    /* 433 (LREF2-PUSH) */,
    0x00000006    /* 434 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :indent */,
    0x00000049    /* 436 (LREF1-PUSH) */,
    0x00000006    /* 437 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :string-length */,
    0x00000048    /* 439 (LREF0-PUSH) */,
    0x00011060    /* 440 (GREF-TAIL-CALL 17) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make.ecc2000> */,
    0x00000014    /* 442 (RET) */,
    0x00000013    /* 443 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 414),
    0x00000014    /* 445 (RET) */,
    0x00000013    /* 446 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 414),
    0x00000014    /* 448 (RET) */,
    0x00000013    /* 449 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 414),
    0x00000014    /* 451 (RET) */,
    0x00000013    /* 452 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 414),
    0x00000014    /* 454 (RET) */,
    0x00000013    /* 455 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 414),
    0x00000014    /* 457 (RET) */,
    0x00000013    /* 458 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 414),
    0x00000014    /* 460 (RET) */,
    0x00000013    /* 461 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 414),
    0x00000014    /* 463 (RET) */,
    0x0380003c    /* 464 (LREF 0 14) */,
    0x00000074    /* 465 (CDR) */,
    0x00000022    /* 466 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 474),
    0x00000006    /* 468 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[266])) /* "keyword list not even" */,
    0x03800047    /* 470 (LREF-PUSH 0 14) */,
    0x00002060    /* 471 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.ec9a340> */,
    0x00000014    /* 473 (RET) */,
    0x0000100e    /* 474 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 480),
    0x0380003c    /* 476 (LREF 0 14) */,
    0x00000069    /* 477 (CAR-PUSH) */,
    0x0000105f    /* 478 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#unwrap-syntax-1.ec9a240> */,
    0x00001018    /* 480 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /* 481 (LREF0) */,
    0x0000002e    /* 482 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :length */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 505)  /*    505 */,
    0x0380103c    /* 485 (LREF 1 14) */,
    0x00000087    /* 486 (CDDR-PUSH) */,
    0x0380103c    /* 487 (LREF 1 14) */,
    0x00000083    /* 488 (CADR-PUSH) */,
    0x03001047    /* 489 (LREF-PUSH 1 12) */,
    0x02c01047    /* 490 (LREF-PUSH 1 11) */,
    0x02801047    /* 491 (LREF-PUSH 1 10) */,
    0x02401047    /* 492 (LREF-PUSH 1 9) */,
    0x02001047    /* 493 (LREF-PUSH 1 8) */,
    0x01c01047    /* 494 (LREF-PUSH 1 7) */,
    0x01801047    /* 495 (LREF-PUSH 1 6) */,
    0x01401047    /* 496 (LREF-PUSH 1 5) */,
    0x01001047    /* 497 (LREF-PUSH 1 4) */,
    0x00c01047    /* 498 (LREF-PUSH 1 3) */,
    0x0000004e    /* 499 (LREF12-PUSH) */,
    0x0000004d    /* 500 (LREF11-PUSH) */,
    0x0000004c    /* 501 (LREF10-PUSH) */,
    0x0000201b    /* 502 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 29),
    0x00000014    /* 504 (RET) */,
    0x0000003d    /* 505 (LREF0) */,
    0x0000002e    /* 506 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :level */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 529)  /*    529 */,
    0x0380103c    /* 509 (LREF 1 14) */,
    0x00000087    /* 510 (CDDR-PUSH) */,
    0x03401047    /* 511 (LREF-PUSH 1 13) */,
    0x0380103c    /* 512 (LREF 1 14) */,
    0x00000083    /* 513 (CADR-PUSH) */,
    0x02c01047    /* 514 (LREF-PUSH 1 11) */,
    0x02801047    /* 515 (LREF-PUSH 1 10) */,
    0x02401047    /* 516 (LREF-PUSH 1 9) */,
    0x02001047    /* 517 (LREF-PUSH 1 8) */,
    0x01c01047    /* 518 (LREF-PUSH 1 7) */,
    0x01801047    /* 519 (LREF-PUSH 1 6) */,
    0x01401047    /* 520 (LREF-PUSH 1 5) */,
    0x01001047    /* 521 (LREF-PUSH 1 4) */,
    0x00c01047    /* 522 (LREF-PUSH 1 3) */,
    0x0000004e    /* 523 (LREF12-PUSH) */,
    0x0000004d    /* 524 (LREF11-PUSH) */,
    0x0000004c    /* 525 (LREF10-PUSH) */,
    0x0000201b    /* 526 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 29),
    0x00000014    /* 528 (RET) */,
    0x0000003d    /* 529 (LREF0) */,
    0x0000002e    /* 530 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :width */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 553)  /*    553 */,
    0x0380103c    /* 533 (LREF 1 14) */,
    0x00000087    /* 534 (CDDR-PUSH) */,
    0x03401047    /* 535 (LREF-PUSH 1 13) */,
    0x03001047    /* 536 (LREF-PUSH 1 12) */,
    0x0380103c    /* 537 (LREF 1 14) */,
    0x00000083    /* 538 (CADR-PUSH) */,
    0x02801047    /* 539 (LREF-PUSH 1 10) */,
    0x02401047    /* 540 (LREF-PUSH 1 9) */,
    0x02001047    /* 541 (LREF-PUSH 1 8) */,
    0x01c01047    /* 542 (LREF-PUSH 1 7) */,
    0x01801047    /* 543 (LREF-PUSH 1 6) */,
    0x01401047    /* 544 (LREF-PUSH 1 5) */,
    0x01001047    /* 545 (LREF-PUSH 1 4) */,
    0x00c01047    /* 546 (LREF-PUSH 1 3) */,
    0x0000004e    /* 547 (LREF12-PUSH) */,
    0x0000004d    /* 548 (LREF11-PUSH) */,
    0x0000004c    /* 549 (LREF10-PUSH) */,
    0x0000201b    /* 550 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 29),
    0x00000014    /* 552 (RET) */,
    0x0000003d    /* 553 (LREF0) */,
    0x0000002e    /* 554 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :base */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 577)  /*    577 */,
    0x0380103c    /* 557 (LREF 1 14) */,
    0x00000087    /* 558 (CDDR-PUSH) */,
    0x03401047    /* 559 (LREF-PUSH 1 13) */,
    0x03001047    /* 560 (LREF-PUSH 1 12) */,
    0x02c01047    /* 561 (LREF-PUSH 1 11) */,
    0x0380103c    /* 562 (LREF 1 14) */,
    0x00000083    /* 563 (CADR-PUSH) */,
    0x02401047    /* 564 (LREF-PUSH 1 9) */,
    0x02001047    /* 565 (LREF-PUSH 1 8) */,
    0x01c01047    /* 566 (LREF-PUSH 1 7) */,
    0x01801047    /* 567 (LREF-PUSH 1 6) */,
    0x01401047    /* 568 (LREF-PUSH 1 5) */,
    0x01001047    /* 569 (LREF-PUSH 1 4) */,
    0x00c01047    /* 570 (LREF-PUSH 1 3) */,
    0x0000004e    /* 571 (LREF12-PUSH) */,
    0x0000004d    /* 572 (LREF11-PUSH) */,
    0x0000004c    /* 573 (LREF10-PUSH) */,
    0x0000201b    /* 574 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 29),
    0x00000014    /* 576 (RET) */,
    0x0000003d    /* 577 (LREF0) */,
    0x0000002e    /* 578 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :radix */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 601)  /*    601 */,
    0x0380103c    /* 581 (LREF 1 14) */,
    0x00000087    /* 582 (CDDR-PUSH) */,
    0x03401047    /* 583 (LREF-PUSH 1 13) */,
    0x03001047    /* 584 (LREF-PUSH 1 12) */,
    0x02c01047    /* 585 (LREF-PUSH 1 11) */,
    0x02801047    /* 586 (LREF-PUSH 1 10) */,
    0x0380103c    /* 587 (LREF 1 14) */,
    0x00000083    /* 588 (CADR-PUSH) */,
    0x02001047    /* 589 (LREF-PUSH 1 8) */,
    0x01c01047    /* 590 (LREF-PUSH 1 7) */,
    0x01801047    /* 591 (LREF-PUSH 1 6) */,
    0x01401047    /* 592 (LREF-PUSH 1 5) */,
    0x01001047    /* 593 (LREF-PUSH 1 4) */,
    0x00c01047    /* 594 (LREF-PUSH 1 3) */,
    0x0000004e    /* 595 (LREF12-PUSH) */,
    0x0000004d    /* 596 (LREF11-PUSH) */,
    0x0000004c    /* 597 (LREF10-PUSH) */,
    0x0000201b    /* 598 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 29),
    0x00000014    /* 600 (RET) */,
    0x0000003d    /* 601 (LREF0) */,
    0x0000002e    /* 602 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :pretty */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 625)  /*    625 */,
    0x0380103c    /* 605 (LREF 1 14) */,
    0x00000087    /* 606 (CDDR-PUSH) */,
    0x03401047    /* 607 (LREF-PUSH 1 13) */,
    0x03001047    /* 608 (LREF-PUSH 1 12) */,
    0x02c01047    /* 609 (LREF-PUSH 1 11) */,
    0x02801047    /* 610 (LREF-PUSH 1 10) */,
    0x02401047    /* 611 (LREF-PUSH 1 9) */,
    0x0380103c    /* 612 (LREF 1 14) */,
    0x00000083    /* 613 (CADR-PUSH) */,
    0x01c01047    /* 614 (LREF-PUSH 1 7) */,
    0x01801047    /* 615 (LREF-PUSH 1 6) */,
    0x01401047    /* 616 (LREF-PUSH 1 5) */,
    0x01001047    /* 617 (LREF-PUSH 1 4) */,
    0x00c01047    /* 618 (LREF-PUSH 1 3) */,
    0x0000004e    /* 619 (LREF12-PUSH) */,
    0x0000004d    /* 620 (LREF11-PUSH) */,
    0x0000004c    /* 621 (LREF10-PUSH) */,
    0x0000201b    /* 622 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 29),
    0x00000014    /* 624 (RET) */,
    0x0000003d    /* 625 (LREF0) */,
    0x0000002e    /* 626 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :indent */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 649)  /*    649 */,
    0x0380103c    /* 629 (LREF 1 14) */,
    0x00000087    /* 630 (CDDR-PUSH) */,
    0x03401047    /* 631 (LREF-PUSH 1 13) */,
    0x03001047    /* 632 (LREF-PUSH 1 12) */,
    0x02c01047    /* 633 (LREF-PUSH 1 11) */,
    0x02801047    /* 634 (LREF-PUSH 1 10) */,
    0x02401047    /* 635 (LREF-PUSH 1 9) */,
    0x02001047    /* 636 (LREF-PUSH 1 8) */,
    0x0380103c    /* 637 (LREF 1 14) */,
    0x00000083    /* 638 (CADR-PUSH) */,
    0x01801047    /* 639 (LREF-PUSH 1 6) */,
    0x01401047    /* 640 (LREF-PUSH 1 5) */,
    0x01001047    /* 641 (LREF-PUSH 1 4) */,
    0x00c01047    /* 642 (LREF-PUSH 1 3) */,
    0x0000004e    /* 643 (LREF12-PUSH) */,
    0x0000004d    /* 644 (LREF11-PUSH) */,
    0x0000004c    /* 645 (LREF10-PUSH) */,
    0x0000201b    /* 646 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 29),
    0x00000014    /* 648 (RET) */,
    0x0000003d    /* 649 (LREF0) */,
    0x0000002e    /* 650 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :string-length */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 673)  /*    673 */,
    0x0380103c    /* 653 (LREF 1 14) */,
    0x00000087    /* 654 (CDDR-PUSH) */,
    0x03401047    /* 655 (LREF-PUSH 1 13) */,
    0x03001047    /* 656 (LREF-PUSH 1 12) */,
    0x02c01047    /* 657 (LREF-PUSH 1 11) */,
    0x02801047    /* 658 (LREF-PUSH 1 10) */,
    0x02401047    /* 659 (LREF-PUSH 1 9) */,
    0x02001047    /* 660 (LREF-PUSH 1 8) */,
    0x01c01047    /* 661 (LREF-PUSH 1 7) */,
    0x0380103c    /* 662 (LREF 1 14) */,
    0x00000083    /* 663 (CADR-PUSH) */,
    0x01401047    /* 664 (LREF-PUSH 1 5) */,
    0x01001047    /* 665 (LREF-PUSH 1 4) */,
    0x00c01047    /* 666 (LREF-PUSH 1 3) */,
    0x0000004e    /* 667 (LREF12-PUSH) */,
    0x0000004d    /* 668 (LREF11-PUSH) */,
    0x0000004c    /* 669 (LREF10-PUSH) */,
    0x0000201b    /* 670 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 29),
    0x00000014    /* 672 (RET) */,
    0x0000003d    /* 673 (LREF0) */,
    0x0000002e    /* 674 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :print-length */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 697)  /*    697 */,
    0x0380103c    /* 677 (LREF 1 14) */,
    0x00000087    /* 678 (CDDR-PUSH) */,
    0x03401047    /* 679 (LREF-PUSH 1 13) */,
    0x03001047    /* 680 (LREF-PUSH 1 12) */,
    0x02c01047    /* 681 (LREF-PUSH 1 11) */,
    0x02801047    /* 682 (LREF-PUSH 1 10) */,
    0x02401047    /* 683 (LREF-PUSH 1 9) */,
    0x02001047    /* 684 (LREF-PUSH 1 8) */,
    0x01c01047    /* 685 (LREF-PUSH 1 7) */,
    0x01801047    /* 686 (LREF-PUSH 1 6) */,
    0x0380103c    /* 687 (LREF 1 14) */,
    0x00000083    /* 688 (CADR-PUSH) */,
    0x01001047    /* 689 (LREF-PUSH 1 4) */,
    0x00c01047    /* 690 (LREF-PUSH 1 3) */,
    0x0000004e    /* 691 (LREF12-PUSH) */,
    0x0000004d    /* 692 (LREF11-PUSH) */,
    0x0000004c    /* 693 (LREF10-PUSH) */,
    0x0000201b    /* 694 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 29),
    0x00000014    /* 696 (RET) */,
    0x0000003d    /* 697 (LREF0) */,
    0x0000002e    /* 698 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :print-level */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 721)  /*    721 */,
    0x0380103c    /* 701 (LREF 1 14) */,
    0x00000087    /* 702 (CDDR-PUSH) */,
    0x03401047    /* 703 (LREF-PUSH 1 13) */,
    0x03001047    /* 704 (LREF-PUSH 1 12) */,
    0x02c01047    /* 705 (LREF-PUSH 1 11) */,
    0x02801047    /* 706 (LREF-PUSH 1 10) */,
    0x02401047    /* 707 (LREF-PUSH 1 9) */,
    0x02001047    /* 708 (LREF-PUSH 1 8) */,
    0x01c01047    /* 709 (LREF-PUSH 1 7) */,
    0x01801047    /* 710 (LREF-PUSH 1 6) */,
    0x01401047    /* 711 (LREF-PUSH 1 5) */,
    0x0380103c    /* 712 (LREF 1 14) */,
    0x00000083    /* 713 (CADR-PUSH) */,
    0x00c01047    /* 714 (LREF-PUSH 1 3) */,
    0x0000004e    /* 715 (LREF12-PUSH) */,
    0x0000004d    /* 716 (LREF11-PUSH) */,
    0x0000004c    /* 717 (LREF10-PUSH) */,
    0x0000201b    /* 718 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 29),
    0x00000014    /* 720 (RET) */,
    0x0000003d    /* 721 (LREF0) */,
    0x0000002e    /* 722 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :print-width */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 745)  /*    745 */,
    0x0380103c    /* 725 (LREF 1 14) */,
    0x00000087    /* 726 (CDDR-PUSH) */,
    0x03401047    /* 727 (LREF-PUSH 1 13) */,
    0x03001047    /* 728 (LREF-PUSH 1 12) */,
    0x02c01047    /* 729 (LREF-PUSH 1 11) */,
    0x02801047    /* 730 (LREF-PUSH 1 10) */,
    0x02401047    /* 731 (LREF-PUSH 1 9) */,
    0x02001047    /* 732 (LREF-PUSH 1 8) */,
    0x01c01047    /* 733 (LREF-PUSH 1 7) */,
    0x01801047    /* 734 (LREF-PUSH 1 6) */,
    0x01401047    /* 735 (LREF-PUSH 1 5) */,
    0x01001047    /* 736 (LREF-PUSH 1 4) */,
    0x0380103c    /* 737 (LREF 1 14) */,
    0x00000083    /* 738 (CADR-PUSH) */,
    0x0000004e    /* 739 (LREF12-PUSH) */,
    0x0000004d    /* 740 (LREF11-PUSH) */,
    0x0000004c    /* 741 (LREF10-PUSH) */,
    0x0000201b    /* 742 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 29),
    0x00000014    /* 744 (RET) */,
    0x0000003d    /* 745 (LREF0) */,
    0x0000002e    /* 746 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :print-base */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 769)  /*    769 */,
    0x0380103c    /* 749 (LREF 1 14) */,
    0x00000087    /* 750 (CDDR-PUSH) */,
    0x03401047    /* 751 (LREF-PUSH 1 13) */,
    0x03001047    /* 752 (LREF-PUSH 1 12) */,
    0x02c01047    /* 753 (LREF-PUSH 1 11) */,
    0x02801047    /* 754 (LREF-PUSH 1 10) */,
    0x02401047    /* 755 (LREF-PUSH 1 9) */,
    0x02001047    /* 756 (LREF-PUSH 1 8) */,
    0x01c01047    /* 757 (LREF-PUSH 1 7) */,
    0x01801047    /* 758 (LREF-PUSH 1 6) */,
    0x01401047    /* 759 (LREF-PUSH 1 5) */,
    0x01001047    /* 760 (LREF-PUSH 1 4) */,
    0x00c01047    /* 761 (LREF-PUSH 1 3) */,
    0x0380103c    /* 762 (LREF 1 14) */,
    0x00000083    /* 763 (CADR-PUSH) */,
    0x0000004d    /* 764 (LREF11-PUSH) */,
    0x0000004c    /* 765 (LREF10-PUSH) */,
    0x0000201b    /* 766 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 29),
    0x00000014    /* 768 (RET) */,
    0x0000003d    /* 769 (LREF0) */,
    0x0000002e    /* 770 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :print-radix */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 793)  /*    793 */,
    0x0380103c    /* 773 (LREF 1 14) */,
    0x00000087    /* 774 (CDDR-PUSH) */,
    0x03401047    /* 775 (LREF-PUSH 1 13) */,
    0x03001047    /* 776 (LREF-PUSH 1 12) */,
    0x02c01047    /* 777 (LREF-PUSH 1 11) */,
    0x02801047    /* 778 (LREF-PUSH 1 10) */,
    0x02401047    /* 779 (LREF-PUSH 1 9) */,
    0x02001047    /* 780 (LREF-PUSH 1 8) */,
    0x01c01047    /* 781 (LREF-PUSH 1 7) */,
    0x01801047    /* 782 (LREF-PUSH 1 6) */,
    0x01401047    /* 783 (LREF-PUSH 1 5) */,
    0x01001047    /* 784 (LREF-PUSH 1 4) */,
    0x00c01047    /* 785 (LREF-PUSH 1 3) */,
    0x0000004e    /* 786 (LREF12-PUSH) */,
    0x0380103c    /* 787 (LREF 1 14) */,
    0x00000083    /* 788 (CADR-PUSH) */,
    0x0000004c    /* 789 (LREF10-PUSH) */,
    0x0000201b    /* 790 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 29),
    0x00000014    /* 792 (RET) */,
    0x0000003d    /* 793 (LREF0) */,
    0x0000002e    /* 794 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :print-pretty */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 817)  /*    817 */,
    0x0380103c    /* 797 (LREF 1 14) */,
    0x00000087    /* 798 (CDDR-PUSH) */,
    0x03401047    /* 799 (LREF-PUSH 1 13) */,
    0x03001047    /* 800 (LREF-PUSH 1 12) */,
    0x02c01047    /* 801 (LREF-PUSH 1 11) */,
    0x02801047    /* 802 (LREF-PUSH 1 10) */,
    0x02401047    /* 803 (LREF-PUSH 1 9) */,
    0x02001047    /* 804 (LREF-PUSH 1 8) */,
    0x01c01047    /* 805 (LREF-PUSH 1 7) */,
    0x01801047    /* 806 (LREF-PUSH 1 6) */,
    0x01401047    /* 807 (LREF-PUSH 1 5) */,
    0x01001047    /* 808 (LREF-PUSH 1 4) */,
    0x00c01047    /* 809 (LREF-PUSH 1 3) */,
    0x0000004e    /* 810 (LREF12-PUSH) */,
    0x0000004d    /* 811 (LREF11-PUSH) */,
    0x0380103c    /* 812 (LREF 1 14) */,
    0x00000083    /* 813 (CADR-PUSH) */,
    0x0000201b    /* 814 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 29),
    0x00000014    /* 816 (RET) */,
    0x0000200e    /* 817 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 825),
    0x00000006    /* 819 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[274])) /* "unknown keyword ~S" */,
    0x0380103c    /* 821 (LREF 1 14) */,
    0x00000069    /* 822 (CAR-PUSH) */,
    0x0000205f    /* 823 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#errorf.eca24e0> */,
    0x0380103c    /* 825 (LREF 1 14) */,
    0x00000087    /* 826 (CDDR-PUSH) */,
    0x03401047    /* 827 (LREF-PUSH 1 13) */,
    0x03001047    /* 828 (LREF-PUSH 1 12) */,
    0x02c01047    /* 829 (LREF-PUSH 1 11) */,
    0x02801047    /* 830 (LREF-PUSH 1 10) */,
    0x02401047    /* 831 (LREF-PUSH 1 9) */,
    0x02001047    /* 832 (LREF-PUSH 1 8) */,
    0x01c01047    /* 833 (LREF-PUSH 1 7) */,
    0x01801047    /* 834 (LREF-PUSH 1 6) */,
    0x01401047    /* 835 (LREF-PUSH 1 5) */,
    0x01001047    /* 836 (LREF-PUSH 1 4) */,
    0x00c01047    /* 837 (LREF-PUSH 1 3) */,
    0x0000004e    /* 838 (LREF12-PUSH) */,
    0x0000004d    /* 839 (LREF11-PUSH) */,
    0x0000004c    /* 840 (LREF10-PUSH) */,
    0x0000201b    /* 841 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]) + 29),
    0x00000014    /* 843 (RET) */,
    0x00000014    /* 844 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2638]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* write-controls-copy */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2638]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[47])) /* #<compiled-code write-controls-copy@0x7f4b0eceb900> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#write-controls-copy.ec7b3a0> */,
    0x00000014    /*  14 (RET) */,
    /* open-input-file */
    0x0000300e    /*   0 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]) + 11),
    0x00000006    /*   2 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :encoding */,
    0x00000048    /*   4 (LREF0-PUSH) */,
    0x0000000e    /*   5 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]) + 9),
    0x0000005f    /*   7 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#default-file-encoding.e858ee0> */,
    0x00003062    /*   9 (PUSH-GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#get-keyword.e858f80> */,
    0x00001018    /*  11 (PUSH-LOCAL-ENV 1) */,
    0x00000048    /*  12 (LREF0-PUSH) */,
    0x0000000e    /*  13 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]) + 17),
    0x0000005f    /*  15 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#gauche-character-encoding.e858dc0> */,
    0x00000020    /*  17 (BNEQ) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]) + 31),
    0x0000005e    /*  19 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%open-input-file.e858d40> */,
    0x0000004d    /*  21 (LREF11-PUSH) */,
    0x0000200e    /*  22 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]) + 29),
    0x00000006    /*  24 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :encoding */,
    0x0000004c    /*  26 (LREF10-PUSH) */,
    0x0000205f    /*  27 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#delete-keyword.e858ce0> */,
    0x00003095    /*  29 (TAIL-APPLY 3) */,
    0x00000014    /*  30 (RET) */,
    0x0000003d    /*  31 (LREF0) */,
    0x0000002e    /*  32 (BNEQC) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]) + 55)  /*     55 */,
    0x0000000e    /*  35 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]) + 48),
    0x0000005e    /*  37 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%open-input-file.e858a00> */,
    0x0000004d    /*  39 (LREF11-PUSH) */,
    0x0000200e    /*  40 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]) + 47),
    0x00000006    /*  42 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :encoding */,
    0x0000004c    /*  44 (LREF10-PUSH) */,
    0x0000205f    /*  45 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#delete-keyword.e858960> */,
    0x00003095    /*  47 (TAIL-APPLY 3) */,
    0x00001018    /*  48 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  49 (LREF0) */,
    0x00000030    /*  50 (RF) */,
    0x00000048    /*  51 (LREF0-PUSH) */,
    0x00001060    /*  52 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#open-coding-aware-port.e858740> */,
    0x00000014    /*  54 (RET) */,
    0x0000005e    /*  55 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%open-input-file/conv.e858600> */,
    0x0000004d    /*  57 (LREF11-PUSH) */,
    0x00000041    /*  58 (LREF10) */,
    0x00003095    /*  59 (TAIL-APPLY 3) */,
    0x00000014    /*  60 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2714]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* open-input-file */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2714]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[49])) /* #<compiled-code open-input-file@0x7f4b0ec4b600> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier scheme#open-input-file.e856d20> */,
    0x00000014    /*  14 (RET) */,
    /* open-output-file */
    0x0000300e    /*   0 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2729]) + 8),
    0x00000006    /*   2 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :encoding */,
    0x00000048    /*   4 (LREF0-PUSH) */,
    0x00000009    /*   5 (CONSTF-PUSH) */,
    0x0000305f    /*   6 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#get-keyword.e51a1a0> */,
    0x00001018    /*   8 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*   9 (LREF0) */,
    0x0000001e    /*  10 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2729]) + 18),
    0x0000005e    /*  12 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%open-output-file/conv.e51cd40> */,
    0x0000004d    /*  14 (LREF11-PUSH) */,
    0x00000041    /*  15 (LREF10) */,
    0x00003095    /*  16 (TAIL-APPLY 3) */,
    0x00000014    /*  17 (RET) */,
    0x0000000e    /*  18 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2729]) + 22),
    0x0000005f    /*  20 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#default-file-encoding.e51cec0> */,
    0x0000000f    /*  22 (PUSH-PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2729]) + 26),
    0x0000005f    /*  24 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#gauche-character-encoding.e51ce80> */,
    0x00000020    /*  26 (BNEQ) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2729]) + 12),
    0x0000005e    /*  28 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%open-output-file.e51ce20> */,
    0x0000004d    /*  30 (LREF11-PUSH) */,
    0x00000041    /*  31 (LREF10) */,
    0x00003095    /*  32 (TAIL-APPLY 3) */,
    0x00000014    /*  33 (RET) */,
    0x00000013    /*  34 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2729]) + 12),
    0x00000014    /*  36 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2766]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* open-output-file */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2766]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[51])) /* #<compiled-code open-output-file@0x7f4b0e1855a0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier scheme#open-output-file.e51a960> */,
    0x00000014    /*  14 (RET) */,
    /* (call-with-port #f) */
    0x00000049    /*   0 (LREF1-PUSH) */,
    0x0000003d    /*   1 (LREF0) */,
    0x00001012    /*   2 (TAIL-CALL 1) */,
    0x00000014    /*   3 (RET) */,
    /* (call-with-port #f) */
    0x00000049    /*   0 (LREF1-PUSH) */,
    0x00001060    /*   1 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#close-port.e741fa0> */,
    0x00000014    /*   3 (RET) */,
    /* call-with-port */
    0x00000016    /*   0 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[53])) /* #<compiled-code (call-with-port #f)@0x7f4b0e261cc0> */,
    0x0000000d    /*   2 (PUSH) */,
    0x00000016    /*   3 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[54])) /* #<compiled-code (call-with-port #f)@0x7f4b0e261c60> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000006    /*   6 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :source-info */,
    0x00000006    /*   8 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1025])) /* ("libio.scm" 1032) */,
    0x00004060    /*  10 (GREF-TAIL-CALL 4) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%unwind-protect.e740220> */,
    0x00000014    /*  12 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2802]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* call-with-port */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2802]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[55])) /* #<compiled-code call-with-port@0x7f4b0e261d20> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#call-with-port.e7404e0> */,
    0x00000014    /*  14 (RET) */,
    /* (call-with-input-file #f) */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00000042    /*   1 (LREF11) */,
    0x00001012    /*   2 (TAIL-CALL 1) */,
    0x00000014    /*   3 (RET) */,
    /* (call-with-input-file #f) */
    0x0000003d    /*   0 (LREF0) */,
    0x0000001e    /*   1 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2821]) + 7),
    0x00000048    /*   3 (LREF0-PUSH) */,
    0x00001060    /*   4 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#close-input-port.e0814a0> */,
    0x00000014    /*   6 (RET) */,
    0x0000000c    /*   7 (CONSTU-RET) */,
    /* call-with-input-file */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2829]) + 7),
    0x0000005e    /*   2 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#open-input-file.e081bc0> */,
    0x0000004a    /*   4 (LREF2-PUSH) */,
    0x0000003d    /*   5 (LREF0) */,
    0x00003095    /*   6 (TAIL-APPLY 3) */,
    0x00001018    /*   7 (PUSH-LOCAL-ENV 1) */,
    0x00000016    /*   8 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[57])) /* #<compiled-code (call-with-input-file #f)@0x7f4b0dd054e0> */,
    0x0000000d    /*  10 (PUSH) */,
    0x00000016    /*  11 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[58])) /* #<compiled-code (call-with-input-file #f)@0x7f4b0dd05480> */,
    0x0000000d    /*  13 (PUSH) */,
    0x00000006    /*  14 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :source-info */,
    0x00000006    /*  16 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1036])) /* ("libio.scm" 1039) */,
    0x00004060    /*  18 (GREF-TAIL-CALL 4) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%unwind-protect.e081780> */,
    0x00000014    /*  20 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2850]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* call-with-input-file */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2850]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[59])) /* #<compiled-code call-with-input-file@0x7f4b0dd05540> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier scheme#call-with-input-file.e0779a0> */,
    0x00000014    /*  14 (RET) */,
    /* (call-with-output-file #f) */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00000042    /*   1 (LREF11) */,
    0x00001012    /*   2 (TAIL-CALL 1) */,
    0x00000014    /*   3 (RET) */,
    /* (call-with-output-file #f) */
    0x0000003d    /*   0 (LREF0) */,
    0x0000001e    /*   1 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2869]) + 7),
    0x00000048    /*   3 (LREF0-PUSH) */,
    0x00001060    /*   4 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#close-output-port.e3396a0> */,
    0x00000014    /*   6 (RET) */,
    0x0000000c    /*   7 (CONSTU-RET) */,
    /* call-with-output-file */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2877]) + 7),
    0x0000005e    /*   2 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#open-output-file.e339d20> */,
    0x0000004a    /*   4 (LREF2-PUSH) */,
    0x0000003d    /*   5 (LREF0) */,
    0x00003095    /*   6 (TAIL-APPLY 3) */,
    0x00001018    /*   7 (PUSH-LOCAL-ENV 1) */,
    0x00000016    /*   8 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[61])) /* #<compiled-code (call-with-output-file #f)@0x7f4b0df38ae0> */,
    0x0000000d    /*  10 (PUSH) */,
    0x00000016    /*  11 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[62])) /* #<compiled-code (call-with-output-file #f)@0x7f4b0df38a80> */,
    0x0000000d    /*  13 (PUSH) */,
    0x00000006    /*  14 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :source-info */,
    0x00000006    /*  16 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1048])) /* ("libio.scm" 1044) */,
    0x00004060    /*  18 (GREF-TAIL-CALL 4) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%unwind-protect.e339940> */,
    0x00000014    /*  20 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2898]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* call-with-output-file */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2898]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[63])) /* #<compiled-code call-with-output-file@0x7f4b0df38b40> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier scheme#call-with-output-file.e331a40> */,
    0x00000014    /*  14 (RET) */,
    /* (with-input-from-file #f) */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x0000004d    /*   1 (LREF11-PUSH) */,
    0x00002060    /*   2 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#with-input-from-port.dc6a2e0> */,
    0x00000014    /*   4 (RET) */,
    /* (with-input-from-file #f) */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00001060    /*   1 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#close-input-port.dc6a180> */,
    0x00000014    /*   3 (RET) */,
    /* with-input-from-file */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2922]) + 7),
    0x0000005e    /*   2 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#open-input-file.dc6a780> */,
    0x0000004a    /*   4 (LREF2-PUSH) */,
    0x0000003d    /*   5 (LREF0) */,
    0x00003095    /*   6 (TAIL-APPLY 3) */,
    0x00001018    /*   7 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*   8 (LREF0) */,
    0x00000030    /*   9 (RF) */,
    0x00000016    /*  10 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[65])) /* #<compiled-code (with-input-from-file #f)@0x7f4b0d8b8cc0> */,
    0x0000000d    /*  12 (PUSH) */,
    0x00000016    /*  13 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[66])) /* #<compiled-code (with-input-from-file #f)@0x7f4b0d8b8c60> */,
    0x0000000d    /*  15 (PUSH) */,
    0x00000006    /*  16 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :source-info */,
    0x00000006    /*  18 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1058])) /* ("libio.scm" 1050) */,
    0x00004060    /*  20 (GREF-TAIL-CALL 4) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%unwind-protect.dc6a3e0> */,
    0x00000014    /*  22 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2945]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* with-input-from-file */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2945]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[67])) /* #<compiled-code with-input-from-file@0x7f4b0d8b8d20> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier scheme#with-input-from-file.dc69a00> */,
    0x00000014    /*  14 (RET) */,
    /* (with-output-to-file #f) */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x0000004d    /*   1 (LREF11-PUSH) */,
    0x00002060    /*   2 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#with-output-to-port.de28100> */,
    0x00000014    /*   4 (RET) */,
    /* (with-output-to-file #f) */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00001060    /*   1 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#close-output-port.de2afa0> */,
    0x00000014    /*   3 (RET) */,
    /* with-output-to-file */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2969]) + 7),
    0x0000005e    /*   2 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#open-output-file.de28d60> */,
    0x0000004a    /*   4 (LREF2-PUSH) */,
    0x0000003d    /*   5 (LREF0) */,
    0x00003095    /*   6 (TAIL-APPLY 3) */,
    0x00001018    /*   7 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*   8 (LREF0) */,
    0x00000030    /*   9 (RF) */,
    0x00000016    /*  10 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[69])) /* #<compiled-code (with-output-to-file #f)@0x7f4b0d9be900> */,
    0x0000000d    /*  12 (PUSH) */,
    0x00000016    /*  13 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[70])) /* #<compiled-code (with-output-to-file #f)@0x7f4b0d9be6c0> */,
    0x0000000d    /*  15 (PUSH) */,
    0x00000006    /*  16 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :source-info */,
    0x00000006    /*  18 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1070])) /* ("libio.scm" 1056) */,
    0x00004060    /*  20 (GREF-TAIL-CALL 4) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%unwind-protect.de285a0> */,
    0x00000014    /*  22 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2992]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* with-output-to-file */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2992]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[71])) /* #<compiled-code with-output-to-file@0x7f4b0d9be9c0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier scheme#with-output-to-file.de25760> */,
    0x00000014    /*  14 (RET) */,
    /* with-output-to-string */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3007]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#open-output-string.df551e0> */,
    0x00001018    /*   4 (PUSH-LOCAL-ENV 1) */,
    0x0000200e    /*   5 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3007]) + 11),
    0x00000048    /*   7 (LREF0-PUSH) */,
    0x0000004c    /*   8 (LREF10-PUSH) */,
    0x0000205f    /*   9 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#with-output-to-port.df55160> */,
    0x00000048    /*  11 (LREF0-PUSH) */,
    0x00001060    /*  12 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#get-output-string.df550e0> */,
    0x00000014    /*  14 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3022]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* with-output-to-string */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3022]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[73])) /* #<compiled-code with-output-to-string@0x7f4b0dae5120> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#with-output-to-string.df55800> */,
    0x00000014    /*  14 (RET) */,
    /* with-input-from-string */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3037]) + 5),
    0x00000049    /*   2 (LREF1-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#open-input-string.ec526a0> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000048    /*   6 (LREF0-PUSH) */,
    0x00002060    /*   7 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#with-input-from-port.ec526e0> */,
    0x00000014    /*   9 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3047]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* with-input-from-string */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3047]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[75])) /* #<compiled-code with-input-from-string@0x7f4b0ec4bf00> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#with-input-from-string.ec52f40> */,
    0x00000014    /*  14 (RET) */,
    /* call-with-output-string */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3062]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#open-output-string.e858840> */,
    0x00001018    /*   4 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*   5 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3062]) + 10),
    0x00000048    /*   7 (LREF0-PUSH) */,
    0x00000041    /*   8 (LREF10) */,
    0x00001011    /*   9 (CALL 1) */,
    0x00000048    /*  10 (LREF0-PUSH) */,
    0x00001060    /*  11 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#get-output-string.e858720> */,
    0x00000014    /*  13 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3076]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* call-with-output-string */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3076]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[77])) /* #<compiled-code call-with-output-string@0x7f4b0e5df060> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#call-with-output-string.e858ec0> */,
    0x00000014    /*  14 (RET) */,
    /* call-with-input-string */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3091]) + 5),
    0x00000049    /*   2 (LREF1-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#open-input-string.eb67280> */,
    0x0000000d    /*   5 (PUSH) */,
    0x0000003d    /*   6 (LREF0) */,
    0x00001012    /*   7 (TAIL-CALL 1) */,
    0x00000014    /*   8 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3100]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* call-with-input-string */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3100]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[79])) /* #<compiled-code call-with-input-string@0x7f4b0e185900> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#call-with-input-string.eb67360> */,
    0x00000014    /*  14 (RET) */,
    /* call-with-string-io */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3115]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#open-output-string.e466e20> */,
    0x0000100f    /*   4 (PUSH-PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3115]) + 9),
    0x00000049    /*   6 (LREF1-PUSH) */,
    0x0000105f    /*   7 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#open-input-string.e466de0> */,
    0x00002018    /*   9 (PUSH-LOCAL-ENV 2) */,
    0x0000200e    /*  10 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3115]) + 16),
    0x00000048    /*  12 (LREF0-PUSH) */,
    0x00000049    /*  13 (LREF1-PUSH) */,
    0x00000041    /*  14 (LREF10) */,
    0x00002011    /*  15 (CALL 2) */,
    0x00000049    /*  16 (LREF1-PUSH) */,
    0x00001060    /*  17 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#get-output-string.e469d00> */,
    0x00000014    /*  19 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3135]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* call-with-string-io */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3135]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[81])) /* #<compiled-code call-with-string-io@0x7f4b0e234480> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#call-with-string-io.e464020> */,
    0x00000014    /*  14 (RET) */,
    /* (with-string-io #f) */
    0x00000049    /*   0 (LREF1-PUSH) */,
    0x00000048    /*   1 (LREF0-PUSH) */,
    0x00002060    /*   2 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#with-input-from-string.e568c40> */,
    0x00000014    /*   4 (RET) */,
    /* with-string-io */
    0x00000016    /*   0 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[83])) /* #<compiled-code (with-string-io #f)@0x7f4b0dc1eae0> */,
    0x00001063    /*   2 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#with-output-to-string.e565300> */,
    0x00000014    /*   4 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3160]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* with-string-io */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3160]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[84])) /* #<compiled-code with-string-io@0x7f4b0dc1ec00> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#with-string-io.e565400> */,
    0x00000014    /*  14 (RET) */,
    /* (write-to-string #f) */
    0x0000004d    /*   0 (LREF11-PUSH) */,
    0x0000003e    /*   1 (LREF1) */,
    0x00001012    /*   2 (TAIL-CALL 1) */,
    0x00000014    /*   3 (RET) */,
    /* write-to-string */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3179]) + 7),
    0x0000005d    /*   3 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#write.e7406e0> */,
    0x00000013    /*   5 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3179]) + 8),
    0x0000006a    /*   7 (LREF0-CAR) */,
    0x0000000d    /*   8 (PUSH) */,
    0x0000003d    /*   9 (LREF0) */,
    0x00000022    /*  10 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3179]) + 15),
    0x00000003    /*  12 (CONSTN) */,
    0x00000013    /*  13 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3179]) + 16),
    0x00000076    /*  15 (LREF0-CDR) */,
    0x00002018    /*  16 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  17 (LREF0) */,
    0x00000022    /*  18 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3179]) + 22),
    0x00000013    /*  20 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3179]) + 30),
    0x0000200e    /*  22 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3179]) + 30),
    0x00000006    /*  24 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[151])) /* "too many arguments for" */,
    0x00000006    /*  26 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1125])) /* (lambda (obj :optional (writer write)) (with-output-to-string (cut writer obj))) */,
    0x0000205f    /*  28 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.f79a860> */,
    0x00000016    /*  30 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[86])) /* #<compiled-code (write-to-string #f)@0x7f4b0de4a180> */,
    0x00001063    /*  32 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#with-output-to-string.e741b40> */,
    0x00000014    /*  34 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3214]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* write-to-string */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3214]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[87])) /* #<compiled-code write-to-string@0x7f4b0de4a2a0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#write-to-string.e73d600> */,
    0x00000014    /*  14 (RET) */,
    /* read-from-string */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3229]) + 6),
    0x0000003e    /*   3 (LREF1) */,
    0x00000013    /*   4 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3229]) + 13),
    0x0000000e    /*   6 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3229]) + 13),
    0x0000005e    /*   8 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#opt-substring.e1dbde0> */,
    0x00000049    /*  10 (LREF1-PUSH) */,
    0x0000003d    /*  11 (LREF0) */,
    0x00003095    /*  12 (TAIL-APPLY 3) */,
    0x00000061    /*  13 (PUSH-GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read.e1dbd40> */,
    0x00002063    /*  15 (PUSH-GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#with-input-from-string.e1d70c0> */,
    0x00000014    /*  17 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3247]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* read-from-string */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3247]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[89])) /* #<compiled-code read-from-string@0x7f4b0dffd1e0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#read-from-string.e1d71a0> */,
    0x00000014    /*  14 (RET) */,
    /* (with-input-from-port with-input-from-port) */
    0x000000ea    /*   0 (LREF-UNBOX 0 0) */,
    0x0000001e    /*   1 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3262]) + 7),
    0x000000ea    /*   3 (LREF-UNBOX 0 0) */,
    0x00001063    /*   4 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-input-port.e356da0> */,
    0x00000014    /*   6 (RET) */,
    0x0000000c    /*   7 (CONSTU-RET) */,
    /* (with-input-from-port with-input-from-port) */
    0x00000042    /*   0 (LREF11) */,
    0x0000001e    /*   1 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3270]) + 10),
    0x0000100e    /*   3 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3270]) + 8),
    0x0000004d    /*   5 (LREF11-PUSH) */,
    0x0000105f    /*   6 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-input-port.e353120> */,
    0x0000003a    /*   8 (LSET 0 0) */,
    0x00000014    /*   9 (RET) */,
    0x0000000c    /*  10 (CONSTU-RET) */,
    /* with-input-from-port */
    0x00000009    /*   0 (CONSTF-PUSH) */,
    0x00001017    /*   1 (LOCAL-ENV 1) */,
    0x000010e7    /*   2 (BOX 1) */,
    0x00000016    /*   3 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[91])) /* #<compiled-code (with-input-from-port with-input-from-port)@0x7f4b0d969420> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000016    /*   6 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[92])) /* #<compiled-code (with-input-from-port with-input-from-port)@0x7f4b0d969360> */,
    0x00002018    /*   8 (PUSH-LOCAL-ENV 2) */,
    0x0000000e    /*   9 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3281]) + 13),
    0x0000003d    /*  11 (LREF0) */,
    0x0000001c    /*  12 (LOCAL-ENV-CALL 0) */,
    0x00000048    /*  13 (LREF0-PUSH) */,
    0x0000003e    /*  14 (LREF1) */,
    0x000000e5    /*  15 (PUSH-HANDLERS 0) */,
    0x0000000e    /*  16 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3281]) + 20),
    0x00000044    /*  18 (LREF20) */,
    0x00000011    /*  19 (CALL 0) */,
    0x00400036    /*  20 (TAIL-RECEIVE 0 1) */,
    0x000000e6    /*  21 (POP-HANDLERS) */,
    0x0000000e    /*  22 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3281]) + 26),
    0x00000042    /*  24 (LREF11) */,
    0x0000001c    /*  25 (LOCAL-ENV-CALL 0) */,
    0x0000005e    /*  26 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#values.f79a3e0> */,
    0x0000003d    /*  28 (LREF0) */,
    0x00002095    /*  29 (TAIL-APPLY 2) */,
    0x00000014    /*  30 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3312]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* with-input-from-port */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3312]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[93])) /* #<compiled-code with-input-from-port@0x7f4b0d969480> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#with-input-from-port.e352020> */,
    0x00000014    /*  14 (RET) */,
    /* (with-output-to-port with-output-to-port) */
    0x000000ea    /*   0 (LREF-UNBOX 0 0) */,
    0x0000001e    /*   1 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3327]) + 7),
    0x000000ea    /*   3 (LREF-UNBOX 0 0) */,
    0x00001063    /*   4 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-output-port.dcf13c0> */,
    0x00000014    /*   6 (RET) */,
    0x0000000c    /*   7 (CONSTU-RET) */,
    /* (with-output-to-port with-output-to-port) */
    0x00000042    /*   0 (LREF11) */,
    0x0000001e    /*   1 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3335]) + 10),
    0x0000100e    /*   3 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3335]) + 8),
    0x0000004d    /*   5 (LREF11-PUSH) */,
    0x0000105f    /*   6 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-output-port.dcf17c0> */,
    0x0000003a    /*   8 (LSET 0 0) */,
    0x00000014    /*   9 (RET) */,
    0x0000000c    /*  10 (CONSTU-RET) */,
    /* with-output-to-port */
    0x00000009    /*   0 (CONSTF-PUSH) */,
    0x00001017    /*   1 (LOCAL-ENV 1) */,
    0x000010e7    /*   2 (BOX 1) */,
    0x00000016    /*   3 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[95])) /* #<compiled-code (with-output-to-port with-output-to-port)@0x7f4b0dae4180> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000016    /*   6 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[96])) /* #<compiled-code (with-output-to-port with-output-to-port)@0x7f4b0dae4120> */,
    0x00002018    /*   8 (PUSH-LOCAL-ENV 2) */,
    0x0000000e    /*   9 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3346]) + 13),
    0x0000003d    /*  11 (LREF0) */,
    0x0000001c    /*  12 (LOCAL-ENV-CALL 0) */,
    0x00000048    /*  13 (LREF0-PUSH) */,
    0x0000003e    /*  14 (LREF1) */,
    0x000000e5    /*  15 (PUSH-HANDLERS 0) */,
    0x0000000e    /*  16 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3346]) + 20),
    0x00000044    /*  18 (LREF20) */,
    0x00000011    /*  19 (CALL 0) */,
    0x00400036    /*  20 (TAIL-RECEIVE 0 1) */,
    0x000000e6    /*  21 (POP-HANDLERS) */,
    0x0000000e    /*  22 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3346]) + 26),
    0x00000042    /*  24 (LREF11) */,
    0x0000001c    /*  25 (LOCAL-ENV-CALL 0) */,
    0x0000005e    /*  26 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#values.f79a3e0> */,
    0x0000003d    /*  28 (LREF0) */,
    0x00002095    /*  29 (TAIL-APPLY 2) */,
    0x00000014    /*  30 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3377]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* with-output-to-port */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3377]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[97])) /* #<compiled-code with-output-to-port@0x7f4b0dae41e0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#with-output-to-port.dcee240> */,
    0x00000014    /*  14 (RET) */,
    /* (with-error-to-port with-error-to-port) */
    0x000000ea    /*   0 (LREF-UNBOX 0 0) */,
    0x0000001e    /*   1 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3392]) + 7),
    0x000000ea    /*   3 (LREF-UNBOX 0 0) */,
    0x00001063    /*   4 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-error-port.df087e0> */,
    0x00000014    /*   6 (RET) */,
    0x0000000c    /*   7 (CONSTU-RET) */,
    /* (with-error-to-port with-error-to-port) */
    0x00000042    /*   0 (LREF11) */,
    0x0000001e    /*   1 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3400]) + 10),
    0x0000100e    /*   3 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3400]) + 8),
    0x0000004d    /*   5 (LREF11-PUSH) */,
    0x0000105f    /*   6 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-error-port.df08d40> */,
    0x0000003a    /*   8 (LSET 0 0) */,
    0x00000014    /*   9 (RET) */,
    0x0000000c    /*  10 (CONSTU-RET) */,
    /* with-error-to-port */
    0x00000009    /*   0 (CONSTF-PUSH) */,
    0x00001017    /*   1 (LOCAL-ENV 1) */,
    0x000010e7    /*   2 (BOX 1) */,
    0x00000016    /*   3 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[99])) /* #<compiled-code (with-error-to-port with-error-to-port)@0x7f4b0dbe78a0> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000016    /*   6 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[100])) /* #<compiled-code (with-error-to-port with-error-to-port)@0x7f4b0dbe7840> */,
    0x00002018    /*   8 (PUSH-LOCAL-ENV 2) */,
    0x0000000e    /*   9 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3411]) + 13),
    0x0000003d    /*  11 (LREF0) */,
    0x0000001c    /*  12 (LOCAL-ENV-CALL 0) */,
    0x00000048    /*  13 (LREF0-PUSH) */,
    0x0000003e    /*  14 (LREF1) */,
    0x000000e5    /*  15 (PUSH-HANDLERS 0) */,
    0x0000000e    /*  16 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3411]) + 20),
    0x00000044    /*  18 (LREF20) */,
    0x00000011    /*  19 (CALL 0) */,
    0x00400036    /*  20 (TAIL-RECEIVE 0 1) */,
    0x000000e6    /*  21 (POP-HANDLERS) */,
    0x0000000e    /*  22 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3411]) + 26),
    0x00000042    /*  24 (LREF11) */,
    0x0000001c    /*  25 (LOCAL-ENV-CALL 0) */,
    0x0000005e    /*  26 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#values.f79a3e0> */,
    0x0000003d    /*  28 (LREF0) */,
    0x00002095    /*  29 (TAIL-APPLY 2) */,
    0x00000014    /*  30 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3442]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* with-error-to-port */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3442]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[101])) /* #<compiled-code with-error-to-port@0x7f4b0dbe7900> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#with-error-to-port.defe1e0> */,
    0x00000014    /*  14 (RET) */,
    /* (with-ports with-ports) */
    0x008000ea    /*   0 (LREF-UNBOX 0 2) */,
    0x0000001e    /*   1 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3457]) + 10),
    0x0000100e    /*   3 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3457]) + 10),
    0x008000ea    /*   5 (LREF-UNBOX 0 2) */,
    0x00001062    /*   6 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-input-port.d96ee40> */,
    0x00000013    /*   8 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3457]) + 10),
    0x004000ea    /*  10 (LREF-UNBOX 0 1) */,
    0x0000001e    /*  11 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3457]) + 20),
    0x0000100e    /*  13 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3457]) + 20),
    0x004000ea    /*  15 (LREF-UNBOX 0 1) */,
    0x00001062    /*  16 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-output-port.d96eda0> */,
    0x00000013    /*  18 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3457]) + 20),
    0x000000ea    /*  20 (LREF-UNBOX 0 0) */,
    0x0000001e    /*  21 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3457]) + 27),
    0x000000ea    /*  23 (LREF-UNBOX 0 0) */,
    0x00001063    /*  24 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-error-port.d96ec20> */,
    0x00000014    /*  26 (RET) */,
    0x0000000c    /*  27 (CONSTU-RET) */,
    /* (with-ports with-ports) */
    0x00c0103c    /*   0 (LREF 1 3) */,
    0x0000001e    /*   1 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3485]) + 11),
    0x0000100e    /*   3 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3485]) + 8),
    0x00c01047    /*   5 (LREF-PUSH 1 3) */,
    0x0000105f    /*   6 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-input-port.d9668c0> */,
    0x0080003a    /*   8 (LSET 0 2) */,
    0x00000013    /*   9 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3485]) + 11),
    0x00000043    /*  11 (LREF12) */,
    0x0000001e    /*  12 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3485]) + 22),
    0x0000100e    /*  14 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3485]) + 19),
    0x0000004e    /*  16 (LREF12-PUSH) */,
    0x0000105f    /*  17 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-output-port.d966800> */,
    0x0040003a    /*  19 (LSET 0 1) */,
    0x00000013    /*  20 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3485]) + 22),
    0x00000042    /*  22 (LREF11) */,
    0x0000001e    /*  23 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3485]) + 32),
    0x0000100e    /*  25 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3485]) + 30),
    0x0000004d    /*  27 (LREF11-PUSH) */,
    0x0000105f    /*  28 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-error-port.d966740> */,
    0x0000003a    /*  30 (LSET 0 0) */,
    0x00000014    /*  31 (RET) */,
    0x0000000c    /*  32 (CONSTU-RET) */,
    /* with-ports */
    0x00000009    /*   0 (CONSTF-PUSH) */,
    0x00000009    /*   1 (CONSTF-PUSH) */,
    0x00000009    /*   2 (CONSTF-PUSH) */,
    0x00003017    /*   3 (LOCAL-ENV 3) */,
    0x000030e7    /*   4 (BOX 3) */,
    0x000020e7    /*   5 (BOX 2) */,
    0x000010e7    /*   6 (BOX 1) */,
    0x00000016    /*   7 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[103])) /* #<compiled-code (with-ports with-ports)@0x7f4b0d4d7720> */,
    0x0000000d    /*   9 (PUSH) */,
    0x00000016    /*  10 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[104])) /* #<compiled-code (with-ports with-ports)@0x7f4b0d4d76c0> */,
    0x00002018    /*  12 (PUSH-LOCAL-ENV 2) */,
    0x0000000e    /*  13 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3518]) + 17),
    0x0000003d    /*  15 (LREF0) */,
    0x0000001c    /*  16 (LOCAL-ENV-CALL 0) */,
    0x00000048    /*  17 (LREF0-PUSH) */,
    0x0000003e    /*  18 (LREF1) */,
    0x000000e5    /*  19 (PUSH-HANDLERS 0) */,
    0x0000000e    /*  20 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3518]) + 24),
    0x00000044    /*  22 (LREF20) */,
    0x00000011    /*  23 (CALL 0) */,
    0x00400036    /*  24 (TAIL-RECEIVE 0 1) */,
    0x000000e6    /*  25 (POP-HANDLERS) */,
    0x0000000e    /*  26 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3518]) + 30),
    0x00000042    /*  28 (LREF11) */,
    0x0000001c    /*  29 (LOCAL-ENV-CALL 0) */,
    0x0000005e    /*  30 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#values.f79a3e0> */,
    0x0000003d    /*  32 (LREF0) */,
    0x00002095    /*  33 (TAIL-APPLY 2) */,
    0x00000014    /*  34 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3553]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.f79aba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* with-ports */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3553]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[105])) /* #<compiled-code with-ports@0x7f4b0d4d7780> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#with-ports.d964aa0> */,
    0x00000014    /*  14 (RET) */,
    /* #f */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3568]) + 6),
    0x00000006    /*   2 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[314])) /* "Reading R6RS source file.  Note that Gauche is not R6RS compliant." */,
    0x0000105f    /*   4 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#warn.dbc6ba0> */,
    0x000000a3    /*   6 (VALUES-RET 0) */,
    /* %toplevel */
    0x00000006    /*   0 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* r6rs */,
    0x00000016    /*   2 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[107])) /* #<compiled-code #f@0x7f4b0d22e6c0> */,
    0x00002063    /*   4 (PUSH-GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#define-reader-directive.dbc2280> */,
    0x00000014    /*   6 (RET) */,
    /* #f */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3582]) + 9),
    0x00000049    /*   2 (LREF1-PUSH) */,
    0x00000006    /*   3 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x0000005d    /*   5 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#port-case-fold.d436600> */,
    0x000000a1    /*   7 (SETTER) */,
    0x00002011    /*   8 (CALL 2) */,
    0x000000a3    /*   9 (VALUES-RET 0) */,
    /* %toplevel */
    0x00000006    /*   0 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* fold-case */,
    0x00000016    /*   2 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[109])) /* #<compiled-code #f@0x7f4b0cfb9cc0> */,
    0x00002063    /*   4 (PUSH-GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#define-reader-directive.d436a80> */,
    0x00000014    /*   6 (RET) */,
    /* #f */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3599]) + 8),
    0x00000049    /*   2 (LREF1-PUSH) */,
    0x00000009    /*   3 (CONSTF-PUSH) */,
    0x0000005d    /*   4 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#port-case-fold.d4ac120> */,
    0x000000a1    /*   6 (SETTER) */,
    0x00002011    /*   7 (CALL 2) */,
    0x000000a3    /*   8 (VALUES-RET 0) */,
    /* %toplevel */
    0x00000006    /*   0 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* no-fold-case */,
    0x00000016    /*   2 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[111])) /* #<compiled-code #f@0x7f4b0cfede40> */,
    0x00002063    /*   4 (PUSH-GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#define-reader-directive.d4ac580> */,
    0x00000014    /*   6 (RET) */,
    /* #f */
    0x0000300e    /*   0 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3615]) + 9),
    0x00000049    /*   2 (LREF1-PUSH) */,
    0x00000006    /*   3 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* reader-lexical-mode */,
    0x00000006    /*   5 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* legacy */,
    0x0000305f    /*   7 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port-attribute-set!.ec38380> */,
    0x000000a3    /*   9 (VALUES-RET 0) */,
    /* %toplevel */
    0x00000006    /*   0 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* gauche-legacy */,
    0x00000016    /*   2 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[113])) /* #<compiled-code #f@0x7f4b0ed3bd80> */,
    0x00002063    /*   4 (PUSH-GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#define-reader-directive.ec38ac0> */,
    0x00000014    /*   6 (RET) */,
    /* #f */
    0x0000300e    /*   0 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[3632]) + 9),
    0x00000049    /*   2 (LREF1-PUSH) */,
    0x00000006    /*   3 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* reader-lexical-mode */,
    0x00000006    /*   5 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* strict-r7 */,
    0x0000305f    /*   7 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port-attribute-set!.e823040> */,
    0x000000a3    /*   9 (VALUES-RET 0) */,
    /* %toplevel */
    0x00000006    /*   0 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* r7rs */,
    0x00000016    /*   2 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[115])) /* #<compiled-code #f@0x7f4b0e5df120> */,
    0x00002063    /*   4 (PUSH-GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#define-reader-directive.e8234a0> */,
    0x00000014    /*   6 (RET) */,
  },
  {   /* ScmPair d1787 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(54U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[2])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[3])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[5])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[6])},
       { SCM_OBJ(&scm__rc.d1787[7]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[4]), SCM_OBJ(&scm__rc.d1787[8])},
       { SCM_MAKE_INT(55U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[10])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[11])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[13])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[14])},
       { SCM_OBJ(&scm__rc.d1787[15]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[12]), SCM_OBJ(&scm__rc.d1787[16])},
       { SCM_MAKE_INT(56U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[18])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[19])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[21])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[22])},
       { SCM_OBJ(&scm__rc.d1787[23]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[20]), SCM_OBJ(&scm__rc.d1787[24])},
       { SCM_MAKE_INT(59U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[26])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[27])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[29])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[30])},
       { SCM_OBJ(&scm__rc.d1787[31]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[28]), SCM_OBJ(&scm__rc.d1787[32])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[34])},
       { SCM_MAKE_INT(80U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[36])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[37])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[39])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[40])},
       { SCM_OBJ(&scm__rc.d1787[41]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[38]), SCM_OBJ(&scm__rc.d1787[42])},
       { SCM_MAKE_INT(82U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[44])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[45])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[47])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[48])},
       { SCM_OBJ(&scm__rc.d1787[49]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[46]), SCM_OBJ(&scm__rc.d1787[50])},
       { SCM_MAKE_INT(84U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[52])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[53])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[55])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[56])},
       { SCM_OBJ(&scm__rc.d1787[57]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[54]), SCM_OBJ(&scm__rc.d1787[58])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(99U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[61])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[62])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[64])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[65])},
       { SCM_OBJ(&scm__rc.d1787[66]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[63]), SCM_OBJ(&scm__rc.d1787[67])},
       { SCM_MAKE_INT(100U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[69])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[70])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[72])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[73])},
       { SCM_OBJ(&scm__rc.d1787[74]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[71]), SCM_OBJ(&scm__rc.d1787[75])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[77])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[78])},
       { SCM_MAKE_INT(102U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[80])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[81])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[83])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[84])},
       { SCM_OBJ(&scm__rc.d1787[85]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[82]), SCM_OBJ(&scm__rc.d1787[86])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[88])},
       { SCM_MAKE_INT(111U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[90])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[91])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[93])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[94])},
       { SCM_OBJ(&scm__rc.d1787[95]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[92]), SCM_OBJ(&scm__rc.d1787[96])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[98])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[99])},
       { SCM_MAKE_INT(113U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[101])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[102])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[104])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[105])},
       { SCM_OBJ(&scm__rc.d1787[106]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[103]), SCM_OBJ(&scm__rc.d1787[107])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[109])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[110])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[111])},
       { SCM_MAKE_INT(115U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[113])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[114])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[116])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[117])},
       { SCM_OBJ(&scm__rc.d1787[118]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[115]), SCM_OBJ(&scm__rc.d1787[119])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[121])},
       { SCM_MAKE_INT(118U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[123])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[124])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[126])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[127])},
       { SCM_OBJ(&scm__rc.d1787[128]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[125]), SCM_OBJ(&scm__rc.d1787[129])},
       { SCM_MAKE_INT(120U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[131])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[132])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[134])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[135])},
       { SCM_OBJ(&scm__rc.d1787[136]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[133]), SCM_OBJ(&scm__rc.d1787[137])},
       { SCM_MAKE_INT(124U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[139])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[140])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[142])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[143])},
       { SCM_OBJ(&scm__rc.d1787[144]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[141]), SCM_OBJ(&scm__rc.d1787[145])},
       { SCM_MAKE_INT(131U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[147])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[148])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[150])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[151])},
       { SCM_OBJ(&scm__rc.d1787[152]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[149]), SCM_OBJ(&scm__rc.d1787[153])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[155])},
       { SCM_MAKE_INT(139U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[157])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[158])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[160])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[161])},
       { SCM_OBJ(&scm__rc.d1787[162]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[159]), SCM_OBJ(&scm__rc.d1787[163])},
       { SCM_MAKE_INT(142U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[165])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[166])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[168])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[169])},
       { SCM_OBJ(&scm__rc.d1787[170]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[167]), SCM_OBJ(&scm__rc.d1787[171])},
       { SCM_MAKE_INT(145U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[173])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[174])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[176])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[177])},
       { SCM_OBJ(&scm__rc.d1787[178]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[175]), SCM_OBJ(&scm__rc.d1787[179])},
       { SCM_MAKE_INT(158U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[181])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[182])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[184])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[185])},
       { SCM_OBJ(&scm__rc.d1787[186]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[183]), SCM_OBJ(&scm__rc.d1787[187])},
       { SCM_MAKE_INT(159U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[189])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[190])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[192])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[193])},
       { SCM_OBJ(&scm__rc.d1787[194]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[191]), SCM_OBJ(&scm__rc.d1787[195])},
       { SCM_MAKE_INT(161U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[197])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[198])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[200])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[201])},
       { SCM_OBJ(&scm__rc.d1787[202]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[199]), SCM_OBJ(&scm__rc.d1787[203])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[205])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[206])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[207])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[208])},
       { SCM_MAKE_INT(190U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[210])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[211])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[213])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[214])},
       { SCM_OBJ(&scm__rc.d1787[215]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[212]), SCM_OBJ(&scm__rc.d1787[216])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[206])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[218])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[219])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[220])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[221])},
       { SCM_MAKE_INT(217U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[223])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[224])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[226])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[227])},
       { SCM_OBJ(&scm__rc.d1787[228]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[225]), SCM_OBJ(&scm__rc.d1787[229])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[231])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[232])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[233])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[234])},
       { SCM_MAKE_INT(266U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[236])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[237])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[239])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[240])},
       { SCM_OBJ(&scm__rc.d1787[241]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[238]), SCM_OBJ(&scm__rc.d1787[242])},
       { SCM_MAKE_INT(285U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[244])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[245])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[247])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[248])},
       { SCM_OBJ(&scm__rc.d1787[249]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[246]), SCM_OBJ(&scm__rc.d1787[250])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[252])},
       { SCM_MAKE_INT(335U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[254])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[255])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[257])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[258])},
       { SCM_OBJ(&scm__rc.d1787[259]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[256]), SCM_OBJ(&scm__rc.d1787[260])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[252])},
       { SCM_MAKE_INT(359U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[263])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[264])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[266])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[267])},
       { SCM_OBJ(&scm__rc.d1787[268]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[265]), SCM_OBJ(&scm__rc.d1787[269])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[231])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[271])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[272])},
       { SCM_MAKE_INT(382U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[274])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[275])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[277])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[278])},
       { SCM_OBJ(&scm__rc.d1787[279]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[276]), SCM_OBJ(&scm__rc.d1787[280])},
       { SCM_MAKE_INT(388U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[282])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[283])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[285])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[286])},
       { SCM_OBJ(&scm__rc.d1787[287]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[284]), SCM_OBJ(&scm__rc.d1787[288])},
       { SCM_MAKE_INT(393U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[290])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[291])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[293])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[294])},
       { SCM_OBJ(&scm__rc.d1787[295]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[292]), SCM_OBJ(&scm__rc.d1787[296])},
       { SCM_MAKE_INT(396U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[298])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[299])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[301])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[302])},
       { SCM_OBJ(&scm__rc.d1787[303]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[300]), SCM_OBJ(&scm__rc.d1787[304])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(399U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[307])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[308])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[310])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[311])},
       { SCM_OBJ(&scm__rc.d1787[312]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[309]), SCM_OBJ(&scm__rc.d1787[313])},
       { SCM_MAKE_INT(405U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[315])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[316])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[318])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[319])},
       { SCM_OBJ(&scm__rc.d1787[320]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[317]), SCM_OBJ(&scm__rc.d1787[321])},
       { SCM_MAKE_INT(413U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[323])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[324])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[326])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[327])},
       { SCM_OBJ(&scm__rc.d1787[328]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[325]), SCM_OBJ(&scm__rc.d1787[329])},
       { SCM_MAKE_INT(415U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[331])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[332])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[334])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[335])},
       { SCM_OBJ(&scm__rc.d1787[336]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[333]), SCM_OBJ(&scm__rc.d1787[337])},
       { SCM_MAKE_INT(418U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[339])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[340])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[342])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[343])},
       { SCM_OBJ(&scm__rc.d1787[344]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[341]), SCM_OBJ(&scm__rc.d1787[345])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[347])},
       { SCM_MAKE_INT(420U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[349])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[350])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[352])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[353])},
       { SCM_OBJ(&scm__rc.d1787[354]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[351]), SCM_OBJ(&scm__rc.d1787[355])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[357])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[358])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[359])},
       { SCM_MAKE_INT(431U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[361])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[362])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[364])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[365])},
       { SCM_OBJ(&scm__rc.d1787[366]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[363]), SCM_OBJ(&scm__rc.d1787[367])},
       { SCM_MAKE_INT(436U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[369])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[370])},
       { SCM_OBJ(&scm__rc.d1787[371]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1U), SCM_OBJ(&scm__rc.d1787[374])},
       { SCM_FALSE, SCM_OBJ(&scm__rc.d1787[375])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1U), SCM_OBJ(&scm__rc.d1787[377])},
       { SCM_FALSE, SCM_OBJ(&scm__rc.d1787[378])},
       { SCM_MAKE_INT(439U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[380])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[381])},
       { SCM_OBJ(&scm__rc.d1787[382]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(452U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[385])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[386])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[388])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[389])},
       { SCM_OBJ(&scm__rc.d1787[390]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[387]), SCM_OBJ(&scm__rc.d1787[391])},
       { SCM_MAKE_INT(458U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[393])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[394])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[396])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[397])},
       { SCM_OBJ(&scm__rc.d1787[398]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[395]), SCM_OBJ(&scm__rc.d1787[399])},
       { SCM_MAKE_INT(482U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[401])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[402])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[404])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[405])},
       { SCM_OBJ(&scm__rc.d1787[406]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[403]), SCM_OBJ(&scm__rc.d1787[407])},
       { SCM_MAKE_INT(490U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[409])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[410])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[412])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[413])},
       { SCM_OBJ(&scm__rc.d1787[414]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[411]), SCM_OBJ(&scm__rc.d1787[415])},
       { SCM_MAKE_INT(493U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[417])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[418])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[420])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[421])},
       { SCM_OBJ(&scm__rc.d1787[422]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[419]), SCM_OBJ(&scm__rc.d1787[423])},
       { SCM_FALSE, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[425])},
       { SCM_OBJ(&scm__rc.d1787[426]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[427]), SCM_NIL},
       { SCM_MAKE_INT(499U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[429])},
       { SCM_UNDEFINED, SCM_UNDEFINED},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[431])},
       { SCM_MAKE_INT(498U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[433])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[434])},
       { SCM_OBJ(&scm__rc.d1787[435]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[425])},
       { SCM_OBJ(&scm__rc.d1787[438]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[439]), SCM_NIL},
       { SCM_MAKE_INT(510U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[441])},
       { SCM_UNDEFINED, SCM_UNDEFINED},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[443])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[444])},
       { SCM_MAKE_INT(504U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[446])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[447])},
       { SCM_OBJ(&scm__rc.d1787[448]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(528U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[451])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[452])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[454])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[455])},
       { SCM_OBJ(&scm__rc.d1787[456]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[453]), SCM_OBJ(&scm__rc.d1787[457])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[60])},
       { SCM_MAKE_INT(538U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[460])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[461])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[463])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[464])},
       { SCM_OBJ(&scm__rc.d1787[465]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[462]), SCM_OBJ(&scm__rc.d1787[466])},
       { SCM_MAKE_INT(541U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[468])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[469])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[471])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[472])},
       { SCM_OBJ(&scm__rc.d1787[473]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[470]), SCM_OBJ(&scm__rc.d1787[474])},
       { SCM_MAKE_INT(547U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[476])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[477])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[479])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[480])},
       { SCM_OBJ(&scm__rc.d1787[481]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[478]), SCM_OBJ(&scm__rc.d1787[482])},
       { SCM_MAKE_INT(552U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[484])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[485])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[487])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[488])},
       { SCM_OBJ(&scm__rc.d1787[489]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[486]), SCM_OBJ(&scm__rc.d1787[490])},
       { SCM_MAKE_INT(555U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[492])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[493])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[495])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[496])},
       { SCM_OBJ(&scm__rc.d1787[497]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[494]), SCM_OBJ(&scm__rc.d1787[498])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[500])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[501])},
       { SCM_OBJ(&scm__rc.d1787[502]), SCM_NIL},
       { SCM_MAKE_INT(563U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[504])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[505])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[507])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[508])},
       { SCM_OBJ(&scm__rc.d1787[509]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[506]), SCM_OBJ(&scm__rc.d1787[510])},
       { SCM_MAKE_INT(568U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[512])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[513])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[515])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[516])},
       { SCM_OBJ(&scm__rc.d1787[517]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[514]), SCM_OBJ(&scm__rc.d1787[518])},
       { SCM_MAKE_INT(575U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[520])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[521])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[523])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[524])},
       { SCM_OBJ(&scm__rc.d1787[525]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[522]), SCM_OBJ(&scm__rc.d1787[526])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[528])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[529])},
       { SCM_MAKE_INT(581U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[531])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[532])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[534])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[535])},
       { SCM_OBJ(&scm__rc.d1787[536]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[533]), SCM_OBJ(&scm__rc.d1787[537])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[539]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[540])},
       { SCM_OBJ(&scm__rc.d1787[541]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[542])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[543])},
       { SCM_TRUE, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[545])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[546])},
       { SCM_OBJ(&scm__rc.d1787[547]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[548])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[549])},
       { SCM_MAKE_INT(0), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[551])},
       { SCM_OBJ(&scm__rc.d1787[552]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[554])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[555])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[557])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[60])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[560])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[552])},
       { SCM_OBJ(&scm__rc.d1787[558]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[500]), SCM_OBJ(&scm__rc.d1787[563])},
       { SCM_OBJ(&scm__rc.d1787[562]), SCM_OBJ(&scm__rc.d1787[564])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[565])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[557])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[567])},
       { SCM_MAKE_INT(1U), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[569])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[570])},
       { SCM_OBJ(&scm__rc.d1787[571]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[572])},
       { SCM_OBJ(&scm__rc.d1787[573]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[568]), SCM_OBJ(&scm__rc.d1787[574])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[575])},
       { SCM_OBJ(&scm__rc.d1787[576]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[566]), SCM_OBJ(&scm__rc.d1787[577])},
       { SCM_OBJ(&scm__rc.d1787[561]), SCM_OBJ(&scm__rc.d1787[578])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[579])},
       { SCM_OBJ(&scm__rc.d1787[580]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[559]), SCM_OBJ(&scm__rc.d1787[581])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[582])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[583])},
       { SCM_OBJ(&scm__rc.d1787[584]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[558]), SCM_OBJ(&scm__rc.d1787[585])},
       { SCM_OBJ(&scm__rc.d1787[556]), SCM_OBJ(&scm__rc.d1787[586])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[587])},
       { SCM_OBJ(&scm__rc.d1787[588]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[553]), SCM_OBJ(&scm__rc.d1787[589])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[590])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[591])},
       { SCM_OBJ(&scm__rc.d1787[592]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[550]), SCM_OBJ(&scm__rc.d1787[593])},
       { SCM_OBJ(&scm__rc.d1787[544]), SCM_OBJ(&scm__rc.d1787[594])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[595])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(590U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[598])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[599])},
       { SCM_OBJ(&scm__rc.d1787[600]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[603]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[604])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[551])},
       { SCM_MAKE_INT(-1), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[607])},
       { SCM_OBJ(&scm__rc.d1787[608]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[606]), SCM_OBJ(&scm__rc.d1787[609])},
       { SCM_OBJ(&scm__rc.d1787[605]), SCM_OBJ(&scm__rc.d1787[610])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[611])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[612])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[614])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[615])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[616])},
       { SCM_OBJ(&scm__rc.d1787[617]), SCM_OBJ(&scm__rc.d1787[60])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[618])},
       { SCM_OBJ(&scm__rc.d1787[619]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[613]), SCM_OBJ(&scm__rc.d1787[620])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[621])},
       { SCM_MAKE_INT(602U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[623])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[624])},
       { SCM_OBJ(&scm__rc.d1787[625]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[60])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[60])},
       { SCM_MAKE_INT(32U), SCM_NIL},
       { SCM_MAKE_INT(9U), SCM_OBJ(&scm__rc.d1787[630])},
       { SCM_OBJ(&scm__rc.d1787[631]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[632])},
       { SCM_OBJ(&scm__rc.d1787[633]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[634])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[635])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[60])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[638]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[637]), SCM_OBJ(&scm__rc.d1787[639])},
       { SCM_OBJ(&scm__rc.d1787[636]), SCM_OBJ(&scm__rc.d1787[640])},
       { SCM_MAKE_INT(13U), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[642])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[643])},
       { SCM_MAKE_INT(10U), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[629]), SCM_OBJ(&scm__rc.d1787[645])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[646])},
       { SCM_OBJ(&scm__rc.d1787[647]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[628]), SCM_OBJ(&scm__rc.d1787[648])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[649])},
       { SCM_OBJ(&scm__rc.d1787[637]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[650]), SCM_OBJ(&scm__rc.d1787[651])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[652])},
       { SCM_OBJ(&scm__rc.d1787[653]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[637]), SCM_OBJ(&scm__rc.d1787[654])},
       { SCM_OBJ(&scm__rc.d1787[644]), SCM_OBJ(&scm__rc.d1787[655])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[645])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[657])},
       { SCM_OBJ(&scm__rc.d1787[658]), SCM_OBJ(&scm__rc.d1787[651])},
       { SCM_OBJ(&scm__rc.d1787[659]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[656]), SCM_OBJ(&scm__rc.d1787[660])},
       { SCM_OBJ(&scm__rc.d1787[641]), SCM_OBJ(&scm__rc.d1787[661])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[662])},
       { SCM_OBJ(&scm__rc.d1787[663]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[629]), SCM_OBJ(&scm__rc.d1787[664])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[665])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[666])},
       { SCM_OBJ(&scm__rc.d1787[667]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[628]), SCM_OBJ(&scm__rc.d1787[668])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[669])},
       { SCM_OBJ(&scm__rc.d1787[670]), SCM_NIL},
       { SCM_NIL, SCM_OBJ(&scm__rc.d1787[671])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[672])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[673])},
       { SCM_OBJ(&scm__rc.d1787[674]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[543]), SCM_OBJ(&scm__rc.d1787[675])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[676])},
       { SCM_MAKE_INT(618U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[678])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[679])},
       { SCM_OBJ(&scm__rc.d1787[680]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(630U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[684])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[685])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[687])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[688])},
       { SCM_OBJ(&scm__rc.d1787[689]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[686]), SCM_OBJ(&scm__rc.d1787[690])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[459])},
       { SCM_MAKE_INT(634U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[693])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[694])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[696])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[697])},
       { SCM_OBJ(&scm__rc.d1787[698]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[695]), SCM_OBJ(&scm__rc.d1787[699])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[459])},
       { SCM_MAKE_INT(649U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[702])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[703])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[705])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[706])},
       { SCM_OBJ(&scm__rc.d1787[707]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[704]), SCM_OBJ(&scm__rc.d1787[708])},
       { SCM_MAKE_INT(653U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[710])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[711])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[713])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[714])},
       { SCM_OBJ(&scm__rc.d1787[715]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[712]), SCM_OBJ(&scm__rc.d1787[716])},
       { SCM_MAKE_INT(661U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[718])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[719])},
       { SCM_OBJ(&scm__rc.d1787[720]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[425])},
       { SCM_MAKE_INT(668U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[724])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[725])},
       { SCM_OBJ(&scm__rc.d1787[726]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[60])},
       { SCM_MAKE_INT(666U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[730])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[731])},
       { SCM_OBJ(&scm__rc.d1787[732]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[735])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(675U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[738])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[739])},
       { SCM_OBJ(&scm__rc.d1787[740]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1795[26]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(676U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[745])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[746])},
       { SCM_OBJ(&scm__rc.d1787[747]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[750])},
       { SCM_MAKE_INT(679U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[752])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[753])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[755])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[756])},
       { SCM_OBJ(&scm__rc.d1787[757]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[754]), SCM_OBJ(&scm__rc.d1787[758])},
       { SCM_MAKE_INT(685U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[760])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[761])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[763])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[764])},
       { SCM_OBJ(&scm__rc.d1787[765]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[762]), SCM_OBJ(&scm__rc.d1787[766])},
       { SCM_MAKE_INT(687U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[768])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[769])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[771])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[772])},
       { SCM_OBJ(&scm__rc.d1787[773]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[770]), SCM_OBJ(&scm__rc.d1787[774])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[776])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[777])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[778])},
       { SCM_MAKE_INT(693U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[780])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[781])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[783])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[784])},
       { SCM_OBJ(&scm__rc.d1787[785]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[782]), SCM_OBJ(&scm__rc.d1787[786])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(696U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[789])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[790])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[792])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[793])},
       { SCM_OBJ(&scm__rc.d1787[794]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[791]), SCM_OBJ(&scm__rc.d1787[795])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[797])},
       { SCM_MAKE_INT(699U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[799])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[800])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[802])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[803])},
       { SCM_OBJ(&scm__rc.d1787[804]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[801]), SCM_OBJ(&scm__rc.d1787[805])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[807])},
       { SCM_MAKE_INT(710U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[809])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[810])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[812])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[813])},
       { SCM_OBJ(&scm__rc.d1787[814]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[811]), SCM_OBJ(&scm__rc.d1787[815])},
       { SCM_MAKE_INT(718U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[817])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[818])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[820])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[821])},
       { SCM_OBJ(&scm__rc.d1787[822]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[819]), SCM_OBJ(&scm__rc.d1787[823])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(720U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[826])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[827])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[829])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[830])},
       { SCM_OBJ(&scm__rc.d1787[831]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[828]), SCM_OBJ(&scm__rc.d1787[832])},
       { SCM_MAKE_INT(723U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[834])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[835])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[837])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[838])},
       { SCM_OBJ(&scm__rc.d1787[839]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[836]), SCM_OBJ(&scm__rc.d1787[840])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[842])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[843])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[844])},
       { SCM_MAKE_INT(766U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[846])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[847])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[849])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[850])},
       { SCM_OBJ(&scm__rc.d1787[851]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[848]), SCM_OBJ(&scm__rc.d1787[852])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[459])},
       { SCM_MAKE_INT(772U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[855])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[856])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[858])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[859])},
       { SCM_OBJ(&scm__rc.d1787[860]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[857]), SCM_OBJ(&scm__rc.d1787[861])},
       { SCM_MAKE_INT(777U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[863])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[864])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[866])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[867])},
       { SCM_OBJ(&scm__rc.d1787[868]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[865]), SCM_OBJ(&scm__rc.d1787[869])},
       { SCM_MAKE_INT(783U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[871])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[872])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[874])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[875])},
       { SCM_OBJ(&scm__rc.d1787[876]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[873]), SCM_OBJ(&scm__rc.d1787[877])},
       { SCM_MAKE_INT(789U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[879])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[880])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[882])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[883])},
       { SCM_OBJ(&scm__rc.d1787[884]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[881]), SCM_OBJ(&scm__rc.d1787[885])},
       { SCM_MAKE_INT(792U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[887])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[888])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[890])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[891])},
       { SCM_OBJ(&scm__rc.d1787[892]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[889]), SCM_OBJ(&scm__rc.d1787[893])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[459])},
       { SCM_MAKE_INT(799U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[896])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[897])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[899])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[900])},
       { SCM_OBJ(&scm__rc.d1787[901]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[898]), SCM_OBJ(&scm__rc.d1787[902])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[459])},
       { SCM_MAKE_INT(805U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[905])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[906])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[908])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[909])},
       { SCM_OBJ(&scm__rc.d1787[910]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[907]), SCM_OBJ(&scm__rc.d1787[911])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[459])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[913])},
       { SCM_MAKE_INT(815U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[915])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[916])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[918])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[919])},
       { SCM_OBJ(&scm__rc.d1787[920]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[917]), SCM_OBJ(&scm__rc.d1787[921])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[155])},
       { SCM_MAKE_INT(821U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[924])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[925])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[927])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[928])},
       { SCM_OBJ(&scm__rc.d1787[929]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[926]), SCM_OBJ(&scm__rc.d1787[930])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[932])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[933])},
       { SCM_OBJ(&scm__rc.d1787[934]), SCM_NIL},
       { SCM_MAKE_INT(831U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[936])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[937])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[939])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[940])},
       { SCM_OBJ(&scm__rc.d1787[941]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[938]), SCM_OBJ(&scm__rc.d1787[942])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[60])},
       { SCM_MAKE_INT(839U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[945])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[946])},
       { SCM_OBJ(&scm__rc.d1787[947]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[950])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(846U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[953])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[954])},
       { SCM_OBJ(&scm__rc.d1787[955]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1795[37]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[959])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[960])},
       { SCM_MAKE_INT(843U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[962])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[963])},
       { SCM_OBJ(&scm__rc.d1787[964]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[605]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[967])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[968])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[944])},
       { SCM_OBJ(&scm__rc.d1787[970]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[969]), SCM_OBJ(&scm__rc.d1787[971])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[972])},
       { SCM_MAKE_INT(873U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[974])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[975])},
       { SCM_OBJ(&scm__rc.d1787[976]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(877U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[979])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[980])},
       { SCM_OBJ(&scm__rc.d1787[981]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[984])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[985])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[986])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[987])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[988])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[989])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[990])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[991])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[992])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[993])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[994])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[995])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[996])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[997])},
       { SCM_MAKE_INT(941U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[999])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1000])},
       { SCM_OBJ(&scm__rc.d1787[1001]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[998])},
       { SCM_MAKE_INT(962U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1005])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1006])},
       { SCM_OBJ(&scm__rc.d1787[1007]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_UNDEFINED},
       { SCM_MAKE_INT(1012U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1011])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1012])},
       { SCM_OBJ(&scm__rc.d1787[1013]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1022U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1016])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1017])},
       { SCM_OBJ(&scm__rc.d1787[1018]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[425])},
       { SCM_OBJ(&scm__rc.d1787[1021]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[1022]), SCM_NIL},
       { SCM_MAKE_INT(1032U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1024])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[797])},
       { SCM_MAKE_INT(1031U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1027])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1028])},
       { SCM_OBJ(&scm__rc.d1787[1029]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[425])},
       { SCM_OBJ(&scm__rc.d1787[1032]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[1033]), SCM_NIL},
       { SCM_MAKE_INT(1039U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1035])},
       { SCM_UNDEFINED, SCM_UNDEFINED},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1037])},
       { SCM_MAKE_INT(1037U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1039])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1040])},
       { SCM_OBJ(&scm__rc.d1787[1041]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[425])},
       { SCM_OBJ(&scm__rc.d1787[1044]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[1045]), SCM_NIL},
       { SCM_MAKE_INT(1044U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1047])},
       { SCM_MAKE_INT(1042U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1049])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1050])},
       { SCM_OBJ(&scm__rc.d1787[1051]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[425])},
       { SCM_OBJ(&scm__rc.d1787[1054]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[1055]), SCM_NIL},
       { SCM_MAKE_INT(1050U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1057])},
       { SCM_UNDEFINED, SCM_UNDEFINED},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1059])},
       { SCM_MAKE_INT(1047U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1061])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1062])},
       { SCM_OBJ(&scm__rc.d1787[1063]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[425])},
       { SCM_OBJ(&scm__rc.d1787[1066]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[1067]), SCM_NIL},
       { SCM_MAKE_INT(1056U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1069])},
       { SCM_MAKE_INT(1053U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1071])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1072])},
       { SCM_OBJ(&scm__rc.d1787[1073]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1060U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1077])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1078])},
       { SCM_OBJ(&scm__rc.d1787[1079]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1076])},
       { SCM_MAKE_INT(1065U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1083])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1084])},
       { SCM_OBJ(&scm__rc.d1787[1085]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1068U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1088])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1089])},
       { SCM_OBJ(&scm__rc.d1787[1090]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[797])},
       { SCM_MAKE_INT(1073U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1094])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1095])},
       { SCM_OBJ(&scm__rc.d1787[1096]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1076U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1099])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1100])},
       { SCM_OBJ(&scm__rc.d1787[1101]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[425])},
       { SCM_MAKE_INT(1083U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1105])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1106])},
       { SCM_OBJ(&scm__rc.d1787[1107]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1082U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1110])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1111])},
       { SCM_OBJ(&scm__rc.d1787[1112]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[849])},
       { SCM_OBJ(&scm__rc.d1787[1115]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1116])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1117])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1119])},
       { SCM_OBJ(&scm__rc.d1787[1120]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1121])},
       { SCM_OBJ(&scm__rc.d1787[1122]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[1118]), SCM_OBJ(&scm__rc.d1787[1123])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1124])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[425])},
       { SCM_MAKE_INT(1086U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1127])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1128])},
       { SCM_OBJ(&scm__rc.d1787[1129]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1085U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1132])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1133])},
       { SCM_OBJ(&scm__rc.d1787[1134]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_UNDEFINED},
       { SCM_MAKE_INT(1088U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1138])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1139])},
       { SCM_OBJ(&scm__rc.d1787[1140]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1143])},
       { SCM_MAKE_INT(1109U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1145])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1146])},
       { SCM_OBJ(&scm__rc.d1787[1147]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1076])},
       { SCM_MAKE_INT(1108U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1152])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1153])},
       { SCM_OBJ(&scm__rc.d1787[1154]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1157])},
       { SCM_MAKE_INT(1112U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1159])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1160])},
       { SCM_OBJ(&scm__rc.d1787[1161]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1111U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1165])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1166])},
       { SCM_OBJ(&scm__rc.d1787[1167]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1170])},
       { SCM_MAKE_INT(1115U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1172])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1173])},
       { SCM_OBJ(&scm__rc.d1787[1174]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1114U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1178])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1179])},
       { SCM_OBJ(&scm__rc.d1787[1180]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1183])},
       { SCM_MAKE_INT(1118U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1185])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1186])},
       { SCM_OBJ(&scm__rc.d1787[1187]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1076])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1191])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1192])},
       { SCM_MAKE_INT(1117U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1194])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1195])},
       { SCM_OBJ(&scm__rc.d1787[1196]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[807])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1199])},
       { SCM_MAKE_INT(1128U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1201])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1202])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1200])},
       { SCM_OBJ(&scm__rc.d1787[1204]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[1203]), SCM_OBJ(&scm__rc.d1787[1205])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1135U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1208])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1209])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[807])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1211])},
       { SCM_OBJ(&scm__rc.d1787[1212]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[1210]), SCM_OBJ(&scm__rc.d1787[1213])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1140U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1216])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1217])},
       { SCM_OBJ(&scm__rc.d1787[1218]), SCM_OBJ(&scm__rc.d1787[1213])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1145U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1221])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1222])},
       { SCM_OBJ(&scm__rc.d1787[1223]), SCM_OBJ(&scm__rc.d1787[1213])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1150U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1226])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1227])},
       { SCM_OBJ(&scm__rc.d1787[1228]), SCM_OBJ(&scm__rc.d1787[1213])},
       { SCM_UNDEFINED, SCM_NIL},
  },
  {   /* ScmObj d1786 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(4, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(4, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(304, FALSE),
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
  },
};

static ScmObj libioinput_portP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("input-port?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(SCM_IPORTP(obj));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libiooutput_portP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("output-port?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(SCM_OPORTP(obj));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libioportP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("port?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(SCM_PORTP(obj));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libioport_closedP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmPort* obj;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("port-closed?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(obj_scm)) Scm_Error("<port> required, but got %S", obj_scm);
  obj = SCM_PORT(obj_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(SCM_PORT_CLOSED_P(obj));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}

static ScmPrimitiveParameter* default_file_encoding;

static ScmObj libiostandard_input_port(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj p_scm;
  ScmPort* p;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("standard-input-port");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    p_scm = SCM_SUBRARGS[0];
  } else {
    p_scm = SCM_FALSE;
  }
  if (!SCM_MAYBE_P(SCM_IPORTP, p_scm)) Scm_Error("<input-port> or #f required, but got %S", p_scm);
  p = SCM_MAYBE(SCM_PORT, p_scm);
  {
{
ScmObj SCM_RESULT;

#line 81 "libio.scm"
{SCM_RESULT=(((p)?(Scm_SetStdin(p)):(Scm_Stdin())));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libiostandard_output_port(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj p_scm;
  ScmPort* p;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("standard-output-port");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    p_scm = SCM_SUBRARGS[0];
  } else {
    p_scm = SCM_FALSE;
  }
  if (!SCM_MAYBE_P(SCM_OPORTP, p_scm)) Scm_Error("<output-port> or #f required, but got %S", p_scm);
  p = SCM_MAYBE(SCM_PORT, p_scm);
  {
{
ScmObj SCM_RESULT;

#line 83 "libio.scm"
{SCM_RESULT=(((p)?(Scm_SetStdout(p)):(Scm_Stdout())));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libiostandard_error_port(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj p_scm;
  ScmPort* p;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("standard-error-port");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    p_scm = SCM_SUBRARGS[0];
  } else {
    p_scm = SCM_FALSE;
  }
  if (!SCM_MAYBE_P(SCM_OPORTP, p_scm)) Scm_Error("<output-port> or #f required, but got %S", p_scm);
  p = SCM_MAYBE(SCM_PORT, p_scm);
  {
{
ScmObj SCM_RESULT;

#line 85 "libio.scm"
{SCM_RESULT=(((p)?(Scm_SetStderr(p)):(Scm_Stderr())));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioport_name(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("port-name");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_PortName(port));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioport_current_line(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("port-current-line");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmSmallInt SCM_RESULT;
{SCM_RESULT=(Scm_PortLine(port));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_INT(SCM_RESULT));
}
  }
}


static ScmObj libioport_file_number(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj dupP_scm;
  int dupP;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("port-file-number");
  if (SCM_ARGCNT >= 3
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 2 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  if (SCM_ARGCNT > 1+1) {
    dupP_scm = SCM_SUBRARGS[1];
  } else {
    dupP_scm = SCM_FALSE;
  }
  if (!SCM_BOOLP(dupP_scm)) Scm_Error("boolean required, but got %S", dupP_scm);
  dupP = SCM_BOOL_VALUE(dupP_scm);
  {
{
ScmObj SCM_RESULT;

#line 103 "libio.scm"
{int i=Scm_PortFileNo(port);
if ((i)<(0)){{{SCM_RESULT=(SCM_FALSE);goto SCM_STUB_RETURN;}}}
if (dupP){{
{int r=0;
SCM_SYSCALL(r,dup(i));
if ((r)<(0)){{Scm_SysError("dup(2) failed");}}
i=(r);}}}
{SCM_RESULT=(Scm_MakeInteger(i));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioport_fd_dupX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj dst_scm;
  ScmPort* dst;
  ScmObj src_scm;
  ScmPort* src;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("port-fd-dup!");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  dst_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(dst_scm)) Scm_Error("<port> required, but got %S", dst_scm);
  dst = SCM_PORT(dst_scm);
  src_scm = SCM_SUBRARGS[1];
  if (!SCM_PORTP(src_scm)) Scm_Error("<port> required, but got %S", src_scm);
  src = SCM_PORT(src_scm);
  {
Scm_PortFdDup(dst,src);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libioport_attribute_setX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj key_scm;
  ScmObj key;
  ScmObj val_scm;
  ScmObj val;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("port-attribute-set!");
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  key_scm = SCM_SUBRARGS[1];
  if (!(key_scm)) Scm_Error("scheme object required, but got %S", key_scm);
  key = (key_scm);
  val_scm = SCM_SUBRARGS[2];
  if (!(val_scm)) Scm_Error("scheme object required, but got %S", val_scm);
  val = (val_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_PortAttrSet(port,key,val));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioport_attribute_ref(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj key_scm;
  ScmObj key;
  ScmObj fallback_scm;
  ScmObj fallback;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("port-attribute-ref");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  key_scm = SCM_SUBRARGS[1];
  if (!(key_scm)) Scm_Error("scheme object required, but got %S", key_scm);
  key = (key_scm);
  if (SCM_ARGCNT > 2+1) {
    fallback_scm = SCM_SUBRARGS[2];
  } else {
    fallback_scm = SCM_UNBOUND;
  }
  if (!(fallback_scm)) Scm_Error("scheme object required, but got %S", fallback_scm);
  fallback = (fallback_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_PortAttrGet(port,key,fallback));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioport_attribute_deleteX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj key_scm;
  ScmObj key;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("port-attribute-delete!");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  key_scm = SCM_SUBRARGS[1];
  if (!(key_scm)) Scm_Error("scheme object required, but got %S", key_scm);
  key = (key_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_PortAttrDelete(port,key));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioport_attributes(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("port-attributes");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_PortAttrs(port));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioport_type(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("port-type");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmObj SCM_RESULT;

#line 125 "libio.scm"
switch (SCM_PORT_TYPE(port)) {
case SCM_PORT_FILE : {{SCM_RESULT=(scm__rc.d1786[128]);goto SCM_STUB_RETURN;}
#line 853 "../lib/gauche/cgen/cise.scm"
break;}
#line 127 "libio.scm"
case SCM_PORT_PROC : {{SCM_RESULT=(scm__rc.d1786[129]);goto SCM_STUB_RETURN;}
#line 853 "../lib/gauche/cgen/cise.scm"
break;}
#line 128 "libio.scm"
case SCM_PORT_OSTR : case SCM_PORT_ISTR : {{SCM_RESULT=(scm__rc.d1786[130]);goto SCM_STUB_RETURN;}
#line 853 "../lib/gauche/cgen/cise.scm"
break;}default: {
#line 129 "libio.scm"
{SCM_RESULT=(SCM_FALSE);goto SCM_STUB_RETURN;}
#line 853 "../lib/gauche/cgen/cise.scm"
break;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioport_buffering_SETTER(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj mode_scm;
  ScmObj mode;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("(setter port-buffering)");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  mode_scm = SCM_SUBRARGS[1];
  if (!(mode_scm)) Scm_Error("scheme object required, but got %S", mode_scm);
  mode = (mode_scm);
  {

#line 133 "libio.scm"
if (!((SCM_PORT_TYPE(port))==(SCM_PORT_FILE))){{
Scm_Error("can't set buffering mode to non-buffered port: %S",port);}}

#line 135 "libio.scm"
Scm_SetPortBufferingMode(port,
Scm_BufferingMode(mode,(port)->direction,-1));
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libioport_buffering(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("port-buffering");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmObj SCM_RESULT;

#line 137 "libio.scm"
{SCM_RESULT=(Scm_GetPortBufferingModeAsKeyword(port));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioport_linkX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj iport_scm;
  ScmPort* iport;
  ScmObj oport_scm;
  ScmPort* oport;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("port-link!");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  iport_scm = SCM_SUBRARGS[0];
  if (!SCM_IPORTP(iport_scm)) Scm_Error("<input-port> required, but got %S", iport_scm);
  iport = SCM_PORT(iport_scm);
  oport_scm = SCM_SUBRARGS[1];
  if (!SCM_OPORTP(oport_scm)) Scm_Error("<output-port> required, but got %S", oport_scm);
  oport = SCM_PORT(oport_scm);
  {
Scm_LinkPorts(iport,oport);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libioport_unlinkX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("port-unlink!");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
Scm_UnlinkPorts(port);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libioport_case_fold_SETTER(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj flag_scm;
  int flag;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("(setter port-case-fold)");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  flag_scm = SCM_SUBRARGS[1];
  if (!SCM_BOOLP(flag_scm)) Scm_Error("boolean required, but got %S", flag_scm);
  flag = SCM_BOOL_VALUE(flag_scm);
  {

#line 147 "libio.scm"
if (flag){
(SCM_PORT_FLAGS(port))|=(SCM_PORT_CASE_FOLD);} else {
(SCM_PORT_FLAGS(port))&=(~(SCM_PORT_CASE_FOLD));}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libioport_case_fold(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("port-case-fold");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
int SCM_RESULT;

#line 150 "libio.scm"
{SCM_RESULT=((SCM_PORT_FLAGS(port))&(SCM_PORT_CASE_FOLD));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libioclose_input_port(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("close-input-port");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_IPORTP(port_scm)) Scm_Error("<input-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
Scm_ClosePort(port);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libioclose_output_port(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("close-output-port");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_OPORTP(port_scm)) Scm_Error("<output-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
Scm_ClosePort(port);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libioclose_port(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("close-port");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
Scm_ClosePort(port);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}

#if defined(GAUCHE_WINDOWS)
#define DIRECTORY_GETS_IN_WAY(x) ((x)==(EACCES))

#endif /* defined(GAUCHE_WINDOWS) */
#if !(defined(GAUCHE_WINDOWS))
#define DIRECTORY_GETS_IN_WAY(x) (FALSE)

#endif /* !(defined(GAUCHE_WINDOWS)) */

static ScmObj libio_25open_input_file(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj path_scm;
  ScmString* path;
  ScmObj if_does_not_exist_scm = scm__rc.d1786[188];
  ScmObj if_does_not_exist;
  ScmObj buffering_scm = SCM_FALSE;
  ScmObj buffering;
  ScmObj element_type_scm = scm__rc.d1786[191];
  ScmObj element_type;
  ScmObj SCM_SUBRARGS[1];
  ScmObj SCM_KEYARGS = SCM_ARGREF(SCM_ARGCNT-1);
  SCM_ENTER_SUBR("%open-input-file");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  path_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(path_scm)) Scm_Error("<string> required, but got %S", path_scm);
  path = SCM_STRING(path_scm);
  if (Scm_Length(SCM_KEYARGS) % 2)
    Scm_Error("keyword list not even: %S", SCM_KEYARGS);
  while (!SCM_NULLP(SCM_KEYARGS)) {
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[189])) {
      if_does_not_exist_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[190])) {
      buffering_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[192])) {
      element_type_scm = SCM_CADR(SCM_KEYARGS);
    }
    else Scm_Warn("unknown keyword %S", SCM_CAR(SCM_KEYARGS));
    SCM_KEYARGS = SCM_CDDR(SCM_KEYARGS);
  }
  if (!(if_does_not_exist_scm)) Scm_Error("scheme object required, but got %S", if_does_not_exist_scm);
  if_does_not_exist = (if_does_not_exist_scm);
  if (!(buffering_scm)) Scm_Error("scheme object required, but got %S", buffering_scm);
  buffering = (buffering_scm);
  if (!(element_type_scm)) Scm_Error("scheme object required, but got %S", element_type_scm);
  element_type = (element_type_scm);
  {
{
ScmObj SCM_RESULT;

#line 194 "libio.scm"
{int ignerr=FALSE;int flags=O_RDONLY;
#line 196 "libio.scm"
if (SCM_FALSEP(if_does_not_exist)){ignerr=(TRUE);}else if(
!(SCM_EQ(if_does_not_exist,scm__rc.d1786[188]))){
Scm_TypeError(":if-does-not-exist",":error or #f",if_does_not_exist);}
#line 200 "libio.scm"
if (!((SCM_EQ(element_type,scm__rc.d1786[193]))||(
SCM_EQ(element_type,scm__rc.d1786[191])))){{
Scm_Error("bad element-type argument: either :character or :binary expected, but got %S",element_type);}}
#line 204 "libio.scm"

#if (defined(O_BINARY))&&(defined(O_TEXT))

#line 205 "libio.scm"
if (SCM_EQ(element_type,scm__rc.d1786[193])){
(flags)|=(O_TEXT);} else {
(flags)|=(O_BINARY);}
#endif /* (defined(O_BINARY))&&(defined(O_TEXT)) */

#line 208 "libio.scm"
{int bufmode=Scm_BufferingMode(buffering,SCM_PORT_INPUT,SCM_PORT_BUFFER_FULL);ScmObj o=
#line 210 "libio.scm"
Scm_OpenFilePort(Scm_GetStringConst(path),flags,bufmode,0);
#line 212 "libio.scm"
if ((SCM_FALSEP(o))&&(!((ignerr)&&(((((errno)==(ENOENT))||((errno)==(ENODEV)))||((errno)==(ENXIO)))||((errno)==(ENOTDIR)))))){{
Scm_SysError("couldn't open input file: %S",path);}}
{SCM_RESULT=(o);goto SCM_STUB_RETURN;}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libio_25open_output_file(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj path_scm;
  ScmString* path;
  ScmObj if_exists_scm = scm__rc.d1786[210];
  ScmObj if_exists;
  ScmObj if_does_not_exist_scm = scm__rc.d1786[212];
  ScmObj if_does_not_exist;
  ScmObj mode_scm = SCM_MAKE_INT(438U);
  ScmSmallInt mode;
  ScmObj buffering_scm = SCM_FALSE;
  ScmObj buffering;
  ScmObj element_type_scm = scm__rc.d1786[191];
  ScmObj element_type;
  ScmObj SCM_SUBRARGS[1];
  ScmObj SCM_KEYARGS = SCM_ARGREF(SCM_ARGCNT-1);
  SCM_ENTER_SUBR("%open-output-file");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  path_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(path_scm)) Scm_Error("<string> required, but got %S", path_scm);
  path = SCM_STRING(path_scm);
  if (Scm_Length(SCM_KEYARGS) % 2)
    Scm_Error("keyword list not even: %S", SCM_KEYARGS);
  while (!SCM_NULLP(SCM_KEYARGS)) {
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[211])) {
      if_exists_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[189])) {
      if_does_not_exist_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[213])) {
      mode_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[190])) {
      buffering_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[192])) {
      element_type_scm = SCM_CADR(SCM_KEYARGS);
    }
    else Scm_Warn("unknown keyword %S", SCM_CAR(SCM_KEYARGS));
    SCM_KEYARGS = SCM_CDDR(SCM_KEYARGS);
  }
  if (!(if_exists_scm)) Scm_Error("scheme object required, but got %S", if_exists_scm);
  if_exists = (if_exists_scm);
  if (!(if_does_not_exist_scm)) Scm_Error("scheme object required, but got %S", if_does_not_exist_scm);
  if_does_not_exist = (if_does_not_exist_scm);
  if (!SCM_INTP(mode_scm)) Scm_Error("ScmSmallInt required, but got %S", mode_scm);
  mode = SCM_INT_VALUE(mode_scm);
  if (!(buffering_scm)) Scm_Error("scheme object required, but got %S", buffering_scm);
  buffering = (buffering_scm);
  if (!(element_type_scm)) Scm_Error("scheme object required, but got %S", element_type_scm);
  element_type = (element_type_scm);
  {
{
ScmObj SCM_RESULT;

#line 223 "libio.scm"
{int ignerr_noexist=FALSE;int ignerr_exist=FALSE;int flags=O_WRONLY;
#line 226 "libio.scm"
if (!((SCM_EQ(element_type,scm__rc.d1786[193]))||(
SCM_EQ(element_type,scm__rc.d1786[191])))){{
Scm_Error("bad element-type argument: either :character or :binary expected, but got %S",element_type);}}
#line 230 "libio.scm"

#if (defined(O_BINARY))&&(defined(O_TEXT))

#line 231 "libio.scm"
if (SCM_EQ(element_type,scm__rc.d1786[193])){
(flags)|=(O_TEXT);} else {
(flags)|=(O_BINARY);}
#endif /* (defined(O_BINARY))&&(defined(O_TEXT)) */

#line 235 "libio.scm"
if (
SCM_EQ(if_exists,scm__rc.d1786[215])){(flags)|=(O_APPEND);}else if(
SCM_EQ(if_exists,scm__rc.d1786[188])){
(flags)|=(O_EXCL);
if (SCM_EQ(if_does_not_exist,scm__rc.d1786[188])){{
Scm_Error("bad flag combination: :if-exists and :if-does-not-exist can't be :error the same time.");}}}else if(
SCM_EQ(if_exists,scm__rc.d1786[210])){(flags)|=(O_TRUNC);}else if(
SCM_EQ(if_exists,scm__rc.d1786[214])){}else if(
SCM_FALSEP(if_exists)){(flags)|=(O_EXCL);ignerr_exist=(TRUE);} else {
#line 245 "libio.scm"
Scm_TypeError(":if-exists",":supersede, :overwrite, :append, :error or #f",if_exists);}
#line 247 "libio.scm"
if (
SCM_EQ(if_does_not_exist,scm__rc.d1786[212])){(flags)|=(O_CREAT);}else if(
SCM_FALSEP(if_does_not_exist)){ignerr_noexist=(TRUE);}else if(
SCM_EQ(if_does_not_exist,scm__rc.d1786[188])){} else {
Scm_TypeError(":if-does-not-exist",":error, :create or #f",if_does_not_exist);}
#line 253 "libio.scm"
{int bufmode=
Scm_BufferingMode(buffering,SCM_PORT_OUTPUT,SCM_PORT_BUFFER_FULL);ScmObj o=
Scm_OpenFilePort(Scm_GetStringConst(path),flags,bufmode,mode);
#line 257 "libio.scm"
if (((SCM_FALSEP(o))&&(
!((ignerr_noexist)&&(((((errno)==(ENOENT))||((errno)==(ENODEV)))||((errno)==(ENXIO)))||((errno)==(ENOTDIR))))))&&(
!((ignerr_exist)&&((((errno)==(EEXIST))||((errno)==(ENOTDIR)))||(DIRECTORY_GETS_IN_WAY(errno)))))){{
Scm_Error("couldn't open output file: %S",path);}}
{SCM_RESULT=(o);goto SCM_STUB_RETURN;}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioopen_input_fd_port(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj fd_scm;
  ScmSmallInt fd;
  ScmObj buffering_scm = SCM_FALSE;
  ScmObj buffering;
  ScmObj ownerP_scm = SCM_FALSE;
  ScmObj ownerP;
  ScmObj name_scm = SCM_FALSE;
  ScmObj name;
  ScmObj SCM_SUBRARGS[1];
  ScmObj SCM_KEYARGS = SCM_ARGREF(SCM_ARGCNT-1);
  SCM_ENTER_SUBR("open-input-fd-port");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  fd_scm = SCM_SUBRARGS[0];
  if (!SCM_INTP(fd_scm)) Scm_Error("ScmSmallInt required, but got %S", fd_scm);
  fd = SCM_INT_VALUE(fd_scm);
  if (Scm_Length(SCM_KEYARGS) % 2)
    Scm_Error("keyword list not even: %S", SCM_KEYARGS);
  while (!SCM_NULLP(SCM_KEYARGS)) {
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[190])) {
      buffering_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[220])) {
      ownerP_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[221])) {
      name_scm = SCM_CADR(SCM_KEYARGS);
    }
    else Scm_Warn("unknown keyword %S", SCM_CAR(SCM_KEYARGS));
    SCM_KEYARGS = SCM_CDDR(SCM_KEYARGS);
  }
  if (!(buffering_scm)) Scm_Error("scheme object required, but got %S", buffering_scm);
  buffering = (buffering_scm);
  if (!(ownerP_scm)) Scm_Error("scheme object required, but got %S", ownerP_scm);
  ownerP = (ownerP_scm);
  if (!(name_scm)) Scm_Error("scheme object required, but got %S", name_scm);
  name = (name_scm);
  {
{
ScmObj SCM_RESULT;

#line 270 "libio.scm"
{int bufmode=Scm_BufferingMode(buffering,SCM_PORT_INPUT,SCM_PORT_BUFFER_FULL);
#line 272 "libio.scm"
if ((fd)<(0)){{Scm_Error("bad file descriptor: %ld",fd);}}
if (
SCM_EQ(ownerP,scm__rc.d1786[222])){
{int r=0;
SCM_SYSCALL(r,dup(fd));
if ((r)<(0)){{Scm_SysError("dup(2) failed");}}
fd=(r);}}else if(
!(SCM_BOOLP(ownerP))){
Scm_Error("owner? argument must be either #f, #t or a symbol dup, \nbut go t%S",ownerP);}
#line 282 "libio.scm"
{SCM_RESULT=(Scm_MakePortWithFd(name,SCM_PORT_INPUT,fd,bufmode,
!(SCM_FALSEP(ownerP))));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioopen_output_fd_port(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj fd_scm;
  ScmSmallInt fd;
  ScmObj buffering_scm = SCM_FALSE;
  ScmObj buffering;
  ScmObj ownerP_scm = SCM_FALSE;
  ScmObj ownerP;
  ScmObj name_scm = SCM_FALSE;
  ScmObj name;
  ScmObj SCM_SUBRARGS[1];
  ScmObj SCM_KEYARGS = SCM_ARGREF(SCM_ARGCNT-1);
  SCM_ENTER_SUBR("open-output-fd-port");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  fd_scm = SCM_SUBRARGS[0];
  if (!SCM_INTP(fd_scm)) Scm_Error("ScmSmallInt required, but got %S", fd_scm);
  fd = SCM_INT_VALUE(fd_scm);
  if (Scm_Length(SCM_KEYARGS) % 2)
    Scm_Error("keyword list not even: %S", SCM_KEYARGS);
  while (!SCM_NULLP(SCM_KEYARGS)) {
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[190])) {
      buffering_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[220])) {
      ownerP_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[221])) {
      name_scm = SCM_CADR(SCM_KEYARGS);
    }
    else Scm_Warn("unknown keyword %S", SCM_CAR(SCM_KEYARGS));
    SCM_KEYARGS = SCM_CDDR(SCM_KEYARGS);
  }
  if (!(buffering_scm)) Scm_Error("scheme object required, but got %S", buffering_scm);
  buffering = (buffering_scm);
  if (!(ownerP_scm)) Scm_Error("scheme object required, but got %S", ownerP_scm);
  ownerP = (ownerP_scm);
  if (!(name_scm)) Scm_Error("scheme object required, but got %S", name_scm);
  name = (name_scm);
  {
{
ScmObj SCM_RESULT;

#line 289 "libio.scm"
{int bufmode=Scm_BufferingMode(buffering,SCM_PORT_OUTPUT,SCM_PORT_BUFFER_FULL);
#line 291 "libio.scm"
if ((fd)<(0)){{Scm_Error("bad file descriptor: %d",fd);}}
if (
SCM_EQ(ownerP,scm__rc.d1786[222])){
{int r=0;
SCM_SYSCALL(r,dup(fd));
if ((r)<(0)){{Scm_SysError("dup(2) failed");}}
fd=(r);}}else if(
!(SCM_BOOLP(ownerP))){
Scm_Error("owner? argument must be either #f, #t or a symbol dup, \nbut go t%S",ownerP);}
#line 301 "libio.scm"
{SCM_RESULT=(Scm_MakePortWithFd(name,SCM_PORT_OUTPUT,fd,bufmode,
!(SCM_FALSEP(ownerP))));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

static void bufport_closer(ScmPort* p){{
#line 310 "libio.scm"
if ((SCM_PORT_DIR(p))==(SCM_PORT_OUTPUT)){{
{ScmObj scmflusher=SCM_OBJ((PORT_BUF(p))->data);int siz=
((int )(((PORT_BUF(p))->current)-(
(PORT_BUF(p))->buffer)));
if ((siz)>(0)){{
Scm_ApplyRec1(scmflusher,
Scm_MakeString((PORT_BUF(p))->buffer,siz,siz,
(SCM_STRING_INCOMPLETE)|(SCM_STRING_COPYING)));}}
#line 319 "libio.scm"
Scm_ApplyRec1(scmflusher,SCM_FALSE);}}}}}
static ScmSize bufport_filler(ScmPort* p,ScmSize cnt){{
#line 322 "libio.scm"
{ScmObj scmfiller=SCM_OBJ((PORT_BUF(p))->data);ScmObj r=
Scm_ApplyRec1(scmfiller,Scm_MakeInteger(cnt));
if ((SCM_EOFP(r))||(SCM_FALSEP(r))){return (0);}else if(
!(SCM_STRINGP(r))){
Scm_Error("buffered port callback procedure returned non-string: %S",r);}
{const ScmStringBody* b=SCM_STRING_BODY(r);ScmSize siz=
SCM_STRING_BODY_SIZE(b);
if ((siz)>(cnt)){{siz=(cnt);}}
memcpy((PORT_BUF(p))->end,SCM_STRING_BODY_START(b),siz);
return (SCM_STRING_BODY_SIZE(b));}}}}

static ScmObj libioopen_input_buffered_port(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj filler_scm;
  ScmProcedure* filler;
  ScmObj buffer_size_scm;
  ScmSmallInt buffer_size;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("open-input-buffered-port");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  filler_scm = SCM_SUBRARGS[0];
  if (!SCM_PROCEDUREP(filler_scm)) Scm_Error("<procedure> required, but got %S", filler_scm);
  filler = SCM_PROCEDURE(filler_scm);
  buffer_size_scm = SCM_SUBRARGS[1];
  if (!SCM_INTP(buffer_size_scm)) Scm_Error("ScmSmallInt required, but got %S", buffer_size_scm);
  buffer_size = SCM_INT_VALUE(buffer_size_scm);
  {
{
ScmObj SCM_RESULT;

#line 336 "libio.scm"
{ScmPortBuffer bufrec;
(bufrec).size=(buffer_size),
(bufrec).buffer=(NULL),
(bufrec).mode=(SCM_PORT_BUFFER_FULL),
(bufrec).filler=(bufport_filler),
(bufrec).flusher=(NULL),
(bufrec).closer=(bufport_closer),
(bufrec).ready=(NULL),
(bufrec).filenum=(NULL),
(bufrec).data=(((void* )(filler)));
{SCM_RESULT=(Scm_MakeBufferedPort(SCM_CLASS_PORT,SCM_FALSE,SCM_PORT_INPUT,TRUE,&(bufrec)));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

static ScmSize bufport_flusher(ScmPort* p,ScmSize cnt,int G1793 SCM_UNUSED){{
#line 351 "libio.scm"
{ScmObj scmflusher=SCM_OBJ((PORT_BUF(p))->data);ScmObj s=
Scm_MakeString((PORT_BUF(p))->buffer,cnt,cnt,
(SCM_STRING_INCOMPLETE)|(SCM_STRING_COPYING));
Scm_ApplyRec1(scmflusher,s);
return (cnt);}}}

static ScmObj libioopen_output_buffered_port(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj flusher_scm;
  ScmProcedure* flusher;
  ScmObj buffer_size_scm;
  ScmSmallInt buffer_size;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("open-output-buffered-port");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  flusher_scm = SCM_SUBRARGS[0];
  if (!SCM_PROCEDUREP(flusher_scm)) Scm_Error("<procedure> required, but got %S", flusher_scm);
  flusher = SCM_PROCEDURE(flusher_scm);
  buffer_size_scm = SCM_SUBRARGS[1];
  if (!SCM_INTP(buffer_size_scm)) Scm_Error("ScmSmallInt required, but got %S", buffer_size_scm);
  buffer_size = SCM_INT_VALUE(buffer_size_scm);
  {
{
ScmObj SCM_RESULT;

#line 360 "libio.scm"
{ScmPortBuffer bufrec;
(bufrec).size=(buffer_size),
(bufrec).buffer=(NULL),
(bufrec).mode=(SCM_PORT_BUFFER_FULL),
(bufrec).filler=(NULL),
(bufrec).flusher=(bufport_flusher),
(bufrec).closer=(bufport_closer),
(bufrec).ready=(NULL),
(bufrec).filenum=(NULL),
(bufrec).data=(((void* )(flusher)));
{SCM_RESULT=(Scm_MakeBufferedPort(SCM_CLASS_PORT,SCM_FALSE,SCM_PORT_OUTPUT,TRUE,
&(bufrec)));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioopen_input_string(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj string_scm;
  ScmString* string;
  ScmObj privateP_scm = SCM_FALSE;
  int privateP;
  ScmObj name_scm = SCM_OBJ(&scm__sc.d1785[84]);
  ScmObj name;
  ScmObj SCM_SUBRARGS[1];
  ScmObj SCM_KEYARGS = SCM_ARGREF(SCM_ARGCNT-1);
  SCM_ENTER_SUBR("open-input-string");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  string_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(string_scm)) Scm_Error("<string> required, but got %S", string_scm);
  string = SCM_STRING(string_scm);
  if (Scm_Length(SCM_KEYARGS) % 2)
    Scm_Error("keyword list not even: %S", SCM_KEYARGS);
  while (!SCM_NULLP(SCM_KEYARGS)) {
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[254])) {
      privateP_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[221])) {
      name_scm = SCM_CADR(SCM_KEYARGS);
    }
    else Scm_Warn("unknown keyword %S", SCM_CAR(SCM_KEYARGS));
    SCM_KEYARGS = SCM_CDDR(SCM_KEYARGS);
  }
  if (!SCM_BOOLP(privateP_scm)) Scm_Error("boolean required, but got %S", privateP_scm);
  privateP = SCM_BOOL_VALUE(privateP_scm);
  if (!(name_scm)) Scm_Error("scheme object required, but got %S", name_scm);
  name = (name_scm);
  {
{
ScmObj SCM_RESULT;

#line 385 "libio.scm"
{u_long flags=((privateP)?(SCM_PORT_STRING_PRIVATE):(0));
{SCM_RESULT=(Scm_MakeInputStringPortFull(string,name,flags));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioopen_output_string(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj privateP_scm = SCM_FALSE;
  int privateP;
  ScmObj name_scm = SCM_OBJ(&scm__sc.d1785[86]);
  ScmObj name;
  ScmObj SCM_KEYARGS = SCM_ARGREF(SCM_ARGCNT-1);
  SCM_ENTER_SUBR("open-output-string");
  if (Scm_Length(SCM_KEYARGS) % 2)
    Scm_Error("keyword list not even: %S", SCM_KEYARGS);
  while (!SCM_NULLP(SCM_KEYARGS)) {
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[254])) {
      privateP_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[221])) {
      name_scm = SCM_CADR(SCM_KEYARGS);
    }
    else Scm_Warn("unknown keyword %S", SCM_CAR(SCM_KEYARGS));
    SCM_KEYARGS = SCM_CDDR(SCM_KEYARGS);
  }
  if (!SCM_BOOLP(privateP_scm)) Scm_Error("boolean required, but got %S", privateP_scm);
  privateP = SCM_BOOL_VALUE(privateP_scm);
  if (!(name_scm)) Scm_Error("scheme object required, but got %S", name_scm);
  name = (name_scm);
  {
{
ScmObj SCM_RESULT;

#line 390 "libio.scm"
{u_long flags=((privateP)?(SCM_PORT_STRING_PRIVATE):(0));
{SCM_RESULT=(Scm_MakeOutputStringPortFull(name,flags));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioget_output_string(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj oport_scm;
  ScmPort* oport;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("get-output-string");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  oport_scm = SCM_SUBRARGS[0];
  if (!SCM_OPORTP(oport_scm)) Scm_Error("<output-port> required, but got %S", oport_scm);
  oport = SCM_PORT(oport_scm);
  {
{
ScmObj SCM_RESULT;

#line 394 "libio.scm"
{SCM_RESULT=(Scm_GetOutputString(oport,0));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioget_output_byte_string(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj oport_scm;
  ScmPort* oport;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("get-output-byte-string");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  oport_scm = SCM_SUBRARGS[0];
  if (!SCM_OPORTP(oport_scm)) Scm_Error("<output-port> required, but got %S", oport_scm);
  oport = SCM_PORT(oport_scm);
  {
{
ScmObj SCM_RESULT;

#line 397 "libio.scm"
{SCM_RESULT=(Scm_GetOutputString(oport,SCM_STRING_INCOMPLETE));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioget_remaining_input_string(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj iport_scm;
  ScmPort* iport;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("get-remaining-input-string");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  iport_scm = SCM_SUBRARGS[0];
  if (!SCM_IPORTP(iport_scm)) Scm_Error("<input-port> required, but got %S", iport_scm);
  iport = SCM_PORT(iport_scm);
  {
{
ScmObj SCM_RESULT;

#line 400 "libio.scm"
{SCM_RESULT=(Scm_GetRemainingInputString(iport,0));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioopen_coding_aware_port(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj iport_scm;
  ScmPort* iport;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("open-coding-aware-port");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  iport_scm = SCM_SUBRARGS[0];
  if (!SCM_IPORTP(iport_scm)) Scm_Error("<input-port> required, but got %S", iport_scm);
  iport = SCM_PORT(iport_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_MakeCodingAwarePort(iport));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioport_has_port_positionP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("port-has-port-position?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
int SCM_RESULT;

#line 414 "libio.scm"
{SCM_RESULT=(Scm_PortPositionable(port,FALSE));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libioport_has_set_port_positionXP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("port-has-set-port-position!?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
int SCM_RESULT;

#line 416 "libio.scm"
{SCM_RESULT=(Scm_PortPositionable(port,TRUE));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libioport_position(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("port-position");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmObj SCM_RESULT;

#line 419 "libio.scm"
{SCM_RESULT=(Scm_GetPortPosition(port));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioset_port_positionX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj pos_scm;
  ScmObj pos;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("set-port-position!");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  pos_scm = SCM_SUBRARGS[1];
  if (!(pos_scm)) Scm_Error("scheme object required, but got %S", pos_scm);
  pos = (pos_scm);
  {
{
ScmObj SCM_RESULT;

#line 421 "libio.scm"
{SCM_RESULT=(Scm_SetPortPosition(port,pos));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioport_seek(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj offset_scm;
  ScmObj offset;
  ScmObj whence_scm;
  ScmSmallInt whence;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("port-seek");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  offset_scm = SCM_SUBRARGS[1];
  if (!SCM_INTEGERP(offset_scm)) Scm_Error("exact integer required, but got %S", offset_scm);
  offset = (offset_scm);
  if (SCM_ARGCNT > 2+1) {
    whence_scm = SCM_SUBRARGS[2];
  } else {
    whence_scm = SCM_MAKE_INT(SEEK_SET);
  }
  if (!SCM_INTP(whence_scm)) Scm_Error("ScmSmallInt required, but got %S", whence_scm);
  whence = SCM_INT_VALUE(whence_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_PortSeek(port,offset,whence));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libio_25port_walkingP_SETTER(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj flag_scm;
  int flag;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("(setter %port-walking?)");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  flag_scm = SCM_SUBRARGS[1];
  if (!SCM_BOOLP(flag_scm)) Scm_Error("boolean required, but got %S", flag_scm);
  flag = SCM_BOOL_VALUE(flag_scm);
  {

#line 454 "libio.scm"
if (flag){
((port)->flags)|=(SCM_PORT_WALKING);} else {
((port)->flags)&=(~(SCM_PORT_WALKING));}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libio_25port_walkingP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("%port-walking?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(PORT_WALKER_P(port));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libio_25port_writing_sharedP_SETTER(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj flag_scm;
  int flag;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("(setter %port-writing-shared?)");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  flag_scm = SCM_SUBRARGS[1];
  if (!SCM_BOOLP(flag_scm)) Scm_Error("boolean required, but got %S", flag_scm);
  flag = SCM_BOOL_VALUE(flag_scm);
  {

#line 460 "libio.scm"
if (flag){
((port)->flags)|=(SCM_PORT_WRITESS);} else {
((port)->flags)&=(~(SCM_PORT_WRITESS));}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libio_25port_writing_sharedP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("%port-writing-shared?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(PORT_WRITESS_P(port));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}

static ScmObj write_state_allocate(ScmClass* G1799 SCM_UNUSED,ScmObj G1800 SCM_UNUSED){{
#line 467 "libio.scm"
return (SCM_OBJ(Scm_MakeWriteState(NULL)));}}
static void write_state_print(ScmObj obj,ScmPort* port,ScmWriteContext* G1802 SCM_UNUSED){{
#line 471 "libio.scm"
Scm_Printf(port,"#<write-state %p>",obj);}}
static ScmClass *Scm_WriteStateClass_CPL[] = {
  SCM_CLASS_STATIC_PTR(Scm_TopClass),
  NULL
};
SCM_DEFINE_BUILTIN_CLASS(Scm_WriteStateClass, write_state_print, NULL, NULL, write_state_allocate, Scm_WriteStateClass_CPL);

static ScmObj Scm_WriteStateClass_shared_table_GET(ScmObj OBJARG)
{
  ScmWriteState* obj = SCM_WRITE_STATE(OBJARG);
  return SCM_MAKE_MAYBE(SCM_OBJ_SAFE, obj->sharedTable);
}

static void Scm_WriteStateClass_shared_table_SET(ScmObj OBJARG, ScmObj value)
{
  ScmWriteState* obj = SCM_WRITE_STATE(OBJARG);
  if (!SCM_MAYBE_P(SCM_HASH_TABLE_P, value)) Scm_Error("ScmHashTable* required, but got %S", value);
  obj->sharedTable = SCM_MAYBE(SCM_HASH_TABLE, value);
}

static ScmObj Scm_WriteStateClass_shared_counter_GET(ScmObj OBJARG)
{
  ScmWriteState* obj = SCM_WRITE_STATE(OBJARG);
  return Scm_MakeInteger(obj->sharedCounter);
}

static void Scm_WriteStateClass_shared_counter_SET(ScmObj OBJARG, ScmObj value)
{
  ScmWriteState* obj = SCM_WRITE_STATE(OBJARG);
  if (!SCM_INTEGERP(value)) Scm_Error("int required, but got %S", value);
  obj->sharedCounter = Scm_GetInteger(value);
}

static ScmClassStaticSlotSpec Scm_WriteStateClass__SLOTS[] = {
  SCM_CLASS_SLOT_SPEC("shared-table", Scm_WriteStateClass_shared_table_GET, Scm_WriteStateClass_shared_table_SET),
  SCM_CLASS_SLOT_SPEC("shared-counter", Scm_WriteStateClass_shared_counter_GET, Scm_WriteStateClass_shared_counter_SET),
  SCM_CLASS_SLOT_SPEC_END()
};


static ScmObj libio_25port_write_state_SETTER(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("(setter %port-write-state)");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  obj_scm = SCM_SUBRARGS[1];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {

#line 484 "libio.scm"
if (SCM_WRITE_STATE_P(obj)){
Scm_PortWriteStateSet(port,SCM_WRITE_STATE(obj));} else {
Scm_PortWriteStateSet(port,NULL);}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libio_25port_write_state(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("%port-write-state");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmObj SCM_RESULT;

#line 487 "libio.scm"
{ScmWriteState* r=Scm_PortWriteState(port);
{SCM_RESULT=(((r)?(SCM_OBJ(r)):(SCM_FALSE)));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libio_25port_lockX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("%port-lock!");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {

#line 491 "libio.scm"
{ScmVM* vm=Scm_VM();
PORT_LOCK(port,vm);}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libio_25port_unlockX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("%port-unlock!");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {

#line 494 "libio.scm"
PORT_UNLOCK(port);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libioport_column(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("port-column");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_PORTP(port_scm)) Scm_Error("<port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmSmallInt SCM_RESULT;

#line 529 "libio.scm"
{SCM_RESULT=(Scm_PortColumn(port));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_INT(SCM_RESULT));
}
  }
}


static ScmObj libioread(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("read");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    port_scm = SCM_SUBRARGS[0];
  } else {
    port_scm = SCM_OBJ(SCM_CURIN);
  }
  if (!SCM_IPORTP(port_scm)) Scm_Error("<input-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmObj SCM_RESULT;

#line 539 "libio.scm"
{SCM_RESULT=(Scm_Read(SCM_OBJ(port)));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioread_char(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("read-char");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    port_scm = SCM_SUBRARGS[0];
  } else {
    port_scm = SCM_OBJ(SCM_CURIN);
  }
  if (!SCM_IPORTP(port_scm)) Scm_Error("<input-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmObj SCM_RESULT;

#line 543 "libio.scm"
{int ch;
SCM_GETC(ch,port);
{SCM_RESULT=((((ch)==(EOF))?(SCM_EOF):(SCM_MAKE_CHAR(ch))));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libiopeek_char(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("peek-char");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    port_scm = SCM_SUBRARGS[0];
  } else {
    port_scm = SCM_OBJ(SCM_CURIN);
  }
  if (!SCM_IPORTP(port_scm)) Scm_Error("<input-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmObj SCM_RESULT;

#line 549 "libio.scm"
{ScmChar ch=Scm_Peekc(port);
{SCM_RESULT=((((ch)==(EOF))?(SCM_EOF):(SCM_MAKE_CHAR(ch))));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioeof_objectP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("eof-object?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(SCM_EOFP(obj));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libiochar_readyP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("char-ready?");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    port_scm = SCM_SUBRARGS[0];
  } else {
    port_scm = SCM_OBJ(SCM_CURIN);
  }
  if (!SCM_IPORTP(port_scm)) Scm_Error("<input-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(Scm_CharReady(port));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libioeof_object(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("eof-object");
  {
{
ScmObj SCM_RESULT;

#line 561 "libio.scm"
{SCM_RESULT=(SCM_EOF);goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libiobyte_readyP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("byte-ready?");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    port_scm = SCM_SUBRARGS[0];
  } else {
    port_scm = SCM_OBJ(SCM_CURIN);
  }
  if (!SCM_IPORTP(port_scm)) Scm_Error("<input-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(Scm_ByteReady(port));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libioread_byte(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("read-byte");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    port_scm = SCM_SUBRARGS[0];
  } else {
    port_scm = SCM_OBJ(SCM_CURIN);
  }
  if (!SCM_IPORTP(port_scm)) Scm_Error("<input-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmObj SCM_RESULT;

#line 569 "libio.scm"
{int b;
SCM_GETB(b,port);
{SCM_RESULT=((((b)<(0))?(SCM_EOF):(SCM_MAKE_INT(b))));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libiopeek_byte(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("peek-byte");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    port_scm = SCM_SUBRARGS[0];
  } else {
    port_scm = SCM_OBJ(SCM_CURIN);
  }
  if (!SCM_IPORTP(port_scm)) Scm_Error("<input-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmObj SCM_RESULT;

#line 576 "libio.scm"
{int b=Scm_Peekb(port);
{SCM_RESULT=((((b)<(0))?(SCM_EOF):(SCM_MAKE_INT(b))));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioread_line(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj allowbytestr_scm;
  ScmObj allowbytestr;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("read-line");
  if (SCM_ARGCNT >= 3
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 2 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    port_scm = SCM_SUBRARGS[0];
  } else {
    port_scm = SCM_OBJ(SCM_CURIN);
  }
  if (!SCM_IPORTP(port_scm)) Scm_Error("<input-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  if (SCM_ARGCNT > 1+1) {
    allowbytestr_scm = SCM_SUBRARGS[1];
  } else {
    allowbytestr_scm = SCM_FALSE;
  }
  if (!(allowbytestr_scm)) Scm_Error("scheme object required, but got %S", allowbytestr_scm);
  allowbytestr = (allowbytestr_scm);
  {
{
ScmObj SCM_RESULT;

#line 583 "libio.scm"
{ScmObj r=Scm_ReadLine(port);
if (((SCM_FALSEP(allowbytestr))&&(
SCM_STRINGP(r)))&&(
SCM_STRING_INCOMPLETE_P(r))){{
Scm_ReadError(port,"read-line: encountered illegal byte sequence: %S",r);}}
{SCM_RESULT=(r);goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libiochar_word_constituentP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj ch_scm;
  ScmChar ch;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("char-word-constituent?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  ch_scm = SCM_SUBRARGS[0];
  if (!SCM_CHARP(ch_scm)) Scm_Error("character required, but got %S", ch_scm);
  ch = SCM_CHAR_VALUE(ch_scm);
  {
{
int SCM_RESULT;

#line 631 "libio.scm"
{SCM_RESULT=(Scm_CharWordConstituent(ch,0));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libioread_block(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj bytes_scm;
  ScmSmallInt bytes;
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("read-block");
  if (SCM_ARGCNT >= 3
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 2 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  bytes_scm = SCM_SUBRARGS[0];
  if (!SCM_INTP(bytes_scm)) Scm_Error("ScmSmallInt required, but got %S", bytes_scm);
  bytes = SCM_INT_VALUE(bytes_scm);
  if (SCM_ARGCNT > 1+1) {
    port_scm = SCM_SUBRARGS[1];
  } else {
    port_scm = SCM_OBJ(SCM_CURIN);
  }
  if (!SCM_IPORTP(port_scm)) Scm_Error("<input-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmObj SCM_RESULT;

#line 636 "libio.scm"
if ((bytes)<(0)){{
Scm_Error("bytes must be non-negative integer: %ld",bytes);}}

#line 638 "libio.scm"
if ((bytes)==(0)){
{SCM_RESULT=(Scm_MakeString("",0,0,0));goto SCM_STUB_RETURN;}} else {
{char* buf=SCM_NEW_ATOMIC2(char*,(bytes)+(1));int nread=
Scm_Getz(buf,bytes,port);
if ((nread)<=(0)){{SCM_RESULT=(SCM_EOF);goto SCM_STUB_RETURN;}} else {
#line 644 "libio.scm"
SCM_ASSERT((nread)<=(bytes));
(buf)[nread]=('\x00');
{SCM_RESULT=(Scm_MakeString(buf,nread,nread,SCM_STRING_INCOMPLETE));goto SCM_STUB_RETURN;}}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioread_list(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj closer_scm;
  ScmChar closer;
  ScmObj port_scm;
  ScmObj port;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("read-list");
  if (SCM_ARGCNT >= 3
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 2 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  closer_scm = SCM_SUBRARGS[0];
  if (!SCM_CHARP(closer_scm)) Scm_Error("character required, but got %S", closer_scm);
  closer = SCM_CHAR_VALUE(closer_scm);
  if (SCM_ARGCNT > 1+1) {
    port_scm = SCM_SUBRARGS[1];
  } else {
    port_scm = SCM_OBJ(SCM_CURIN);
  }
  if (!(port_scm)) Scm_Error("scheme object required, but got %S", port_scm);
  port = (port_scm);
  {
{
ScmObj SCM_RESULT;

#line 651 "libio.scm"
{SCM_RESULT=(Scm_ReadList(port,closer));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioport_TObyte_string(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("port->byte-string");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_IPORTP(port_scm)) Scm_Error("<input-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmObj SCM_RESULT;

#line 654 "libio.scm"
{ScmDString ds;char buf[1024];
Scm_DStringInit(&(ds));
for (;;){{int nbytes=Scm_Getz(buf,1024,port);
if ((nbytes)<=(0)){{break;}}
Scm_DStringPutz(&(ds),buf,nbytes);}}
{SCM_RESULT=(Scm_DStringGet(&(ds),SCM_STRING_INCOMPLETE));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioreader_lexical_mode(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj k_scm;
  ScmObj k;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("reader-lexical-mode");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    k_scm = SCM_SUBRARGS[0];
  } else {
    k_scm = SCM_UNBOUND;
  }
  if (!(k_scm)) Scm_Error("scheme object required, but got %S", k_scm);
  k = (k_scm);
  {
{
ScmObj SCM_RESULT;

#line 680 "libio.scm"
if (SCM_UNBOUNDP(k)){
{SCM_RESULT=(Scm_ReaderLexicalMode());goto SCM_STUB_RETURN;}} else {
{SCM_RESULT=(Scm_SetReaderLexicalMode(k));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libio_25port_ungotten_chars(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("%port-ungotten-chars");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_IPORTP(port_scm)) Scm_Error("<input-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_UngottenChars(port));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libio_25port_ungotten_bytes(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("%port-ungotten-bytes");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_IPORTP(port_scm)) Scm_Error("<input-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_UngottenBytes(port));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libiodefine_reader_ctor(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj symbol_scm;
  ScmObj symbol;
  ScmObj proc_scm;
  ScmObj proc;
  ScmObj finisher_scm;
  ScmObj finisher;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("define-reader-ctor");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  symbol_scm = SCM_SUBRARGS[0];
  if (!(symbol_scm)) Scm_Error("scheme object required, but got %S", symbol_scm);
  symbol = (symbol_scm);
  proc_scm = SCM_SUBRARGS[1];
  if (!(proc_scm)) Scm_Error("scheme object required, but got %S", proc_scm);
  proc = (proc_scm);
  if (SCM_ARGCNT > 2+1) {
    finisher_scm = SCM_SUBRARGS[2];
  } else {
    finisher_scm = SCM_FALSE;
  }
  if (!(finisher_scm)) Scm_Error("scheme object required, but got %S", finisher_scm);
  finisher = (finisher_scm);
  {
{
ScmObj SCM_RESULT;

#line 694 "libio.scm"
{SCM_RESULT=(Scm_DefineReaderCtor(symbol,proc,finisher,SCM_FALSE));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libio_25get_reader_ctor(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj symbol_scm;
  ScmObj symbol;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("%get-reader-ctor");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  symbol_scm = SCM_SUBRARGS[0];
  if (!(symbol_scm)) Scm_Error("scheme object required, but got %S", symbol_scm);
  symbol = (symbol_scm);
  {
{
ScmObj SCM_RESULT;

#line 697 "libio.scm"
{SCM_RESULT=(Scm_GetReaderCtor(symbol,SCM_FALSE));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libiodefine_reader_directive(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj symbol_scm;
  ScmObj symbol;
  ScmObj proc_scm;
  ScmObj proc;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("define-reader-directive");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  symbol_scm = SCM_SUBRARGS[0];
  if (!(symbol_scm)) Scm_Error("scheme object required, but got %S", symbol_scm);
  symbol = (symbol_scm);
  proc_scm = SCM_SUBRARGS[1];
  if (!(proc_scm)) Scm_Error("scheme object required, but got %S", proc_scm);
  proc = (proc_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_DefineReaderDirective(symbol,proc));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libiocurrent_read_context(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj ctx_scm;
  ScmObj ctx;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("current-read-context");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    ctx_scm = SCM_SUBRARGS[0];
  } else {
    ctx_scm = SCM_UNBOUND;
  }
  if (!(ctx_scm)) Scm_Error("scheme object required, but got %S", ctx_scm);
  ctx = (ctx_scm);
  {
{
ScmObj SCM_RESULT;

#line 711 "libio.scm"
if (SCM_UNBOUNDP(ctx)){
{SCM_RESULT=(SCM_OBJ(Scm_CurrentReadContext()));goto SCM_STUB_RETURN;}} else {
if (SCM_READ_CONTEXT_P(ctx)){
{SCM_RESULT=(SCM_OBJ(Scm_SetCurrentReadContext(SCM_READ_CONTEXT(ctx))));goto SCM_STUB_RETURN;}} else {
{Scm_Error("<read-context> required, but got:",ctx);
{SCM_RESULT=(SCM_UNDEFINED);goto SCM_STUB_RETURN;}}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libioread_referenceP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("read-reference?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(SCM_READ_REFERENCE_P(obj));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libioread_reference_has_valueP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj ref_scm;
  ScmReadReference* ref;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("read-reference-has-value?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  ref_scm = SCM_SUBRARGS[0];
  if (!SCM_READ_REFERENCE_P(ref_scm)) Scm_Error("read reference required, but got %S", ref_scm);
  ref = SCM_READ_REFERENCE(ref_scm);
  {
{
int SCM_RESULT;

#line 721 "libio.scm"
{SCM_RESULT=(!(SCM_UNBOUNDP((ref)->value)));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libioread_reference_value(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj ref_scm;
  ScmReadReference* ref;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("read-reference-value");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  ref_scm = SCM_SUBRARGS[0];
  if (!SCM_READ_REFERENCE_P(ref_scm)) Scm_Error("read reference required, but got %S", ref_scm);
  ref = SCM_READ_REFERENCE(ref_scm);
  {
{
ScmObj SCM_RESULT;

#line 724 "libio.scm"
if (SCM_UNBOUNDP((ref)->value)){{
Scm_Error("read reference hasn't been resolved");}}

#line 726 "libio.scm"
{SCM_RESULT=((ref)->value);goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

static void parse_write_optionals(ScmObj opt1,ScmObj opt2,ScmPort** pp,const ScmWriteControls** pc){{
#line 744 "libio.scm"
{ScmPort* p=SCM_CUROUT;const ScmWriteControls* c=
Scm_DefaultWriteControls();
if (!(SCM_UNBOUNDP(opt1))){{
if (SCM_PORTP(opt1)){
p=(SCM_PORT(opt1));
if (!(SCM_UNBOUNDP(opt2))){{
if (SCM_WRITE_CONTROLS_P(opt2)){
c=(SCM_WRITE_CONTROLS(opt2));} else {
Scm_Error("Expected write-controls, but got: %S",opt2);}}}}else if(
SCM_WRITE_CONTROLS_P(opt1)){
c=(SCM_WRITE_CONTROLS(opt1));
if (!(SCM_UNBOUNDP(opt2))){{
if (SCM_PORTP(opt2)){
p=(SCM_PORT(opt2));} else {
Scm_Error("Expected port, but got: %S",opt2);}}}} else {
#line 760 "libio.scm"
Scm_Error("Expected port or write-controls, but got: %S",opt1);}}}
*(pp)=(p);
*(pc)=(c);}}}

static ScmObj libiowrite(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj port_or_control_1_scm;
  ScmObj port_or_control_1;
  ScmObj port_or_control_2_scm;
  ScmObj port_or_control_2;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("write");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  if (SCM_ARGCNT > 1+1) {
    port_or_control_1_scm = SCM_SUBRARGS[1];
  } else {
    port_or_control_1_scm = SCM_UNBOUND;
  }
  if (!(port_or_control_1_scm)) Scm_Error("scheme object required, but got %S", port_or_control_1_scm);
  port_or_control_1 = (port_or_control_1_scm);
  if (SCM_ARGCNT > 2+1) {
    port_or_control_2_scm = SCM_SUBRARGS[2];
  } else {
    port_or_control_2_scm = SCM_UNBOUND;
  }
  if (!(port_or_control_2_scm)) Scm_Error("scheme object required, but got %S", port_or_control_2_scm);
  port_or_control_2 = (port_or_control_2_scm);
  {

#line 768 "libio.scm"
{ScmPort* p;const ScmWriteControls* c;
parse_write_optionals(port_or_control_1,port_or_control_2,&(p),&(c));
Scm_WriteWithControls(obj,SCM_OBJ(p),SCM_WRITE_WRITE,c);}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libiowrite_simple(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("write-simple");
  if (SCM_ARGCNT >= 3
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 2 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  if (SCM_ARGCNT > 1+1) {
    port_scm = SCM_SUBRARGS[1];
  } else {
    port_scm = SCM_OBJ(SCM_CUROUT);
  }
  if (!SCM_OPORTP(port_scm)) Scm_Error("<output-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {

#line 775 "libio.scm"
Scm_Write(obj,SCM_OBJ(port),SCM_WRITE_SIMPLE);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libiowrite_shared(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj port_or_control_1_scm;
  ScmObj port_or_control_1;
  ScmObj port_or_control_2_scm;
  ScmObj port_or_control_2;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("write-shared");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  if (SCM_ARGCNT > 1+1) {
    port_or_control_1_scm = SCM_SUBRARGS[1];
  } else {
    port_or_control_1_scm = SCM_UNBOUND;
  }
  if (!(port_or_control_1_scm)) Scm_Error("scheme object required, but got %S", port_or_control_1_scm);
  port_or_control_1 = (port_or_control_1_scm);
  if (SCM_ARGCNT > 2+1) {
    port_or_control_2_scm = SCM_SUBRARGS[2];
  } else {
    port_or_control_2_scm = SCM_UNBOUND;
  }
  if (!(port_or_control_2_scm)) Scm_Error("scheme object required, but got %S", port_or_control_2_scm);
  port_or_control_2 = (port_or_control_2_scm);
  {

#line 779 "libio.scm"
{ScmPort* p;const ScmWriteControls* c;
parse_write_optionals(port_or_control_1,port_or_control_2,&(p),&(c));
Scm_WriteWithControls(obj,SCM_OBJ(p),SCM_WRITE_SHARED,c);}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libiodisplay(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj port_or_control_1_scm;
  ScmObj port_or_control_1;
  ScmObj port_or_control_2_scm;
  ScmObj port_or_control_2;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("display");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  if (SCM_ARGCNT > 1+1) {
    port_or_control_1_scm = SCM_SUBRARGS[1];
  } else {
    port_or_control_1_scm = SCM_UNBOUND;
  }
  if (!(port_or_control_1_scm)) Scm_Error("scheme object required, but got %S", port_or_control_1_scm);
  port_or_control_1 = (port_or_control_1_scm);
  if (SCM_ARGCNT > 2+1) {
    port_or_control_2_scm = SCM_SUBRARGS[2];
  } else {
    port_or_control_2_scm = SCM_UNBOUND;
  }
  if (!(port_or_control_2_scm)) Scm_Error("scheme object required, but got %S", port_or_control_2_scm);
  port_or_control_2 = (port_or_control_2_scm);
  {

#line 785 "libio.scm"
{ScmPort* p;const ScmWriteControls* c;
parse_write_optionals(port_or_control_1,port_or_control_2,&(p),&(c));
Scm_WriteWithControls(obj,SCM_OBJ(p),SCM_WRITE_DISPLAY,c);}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libionewline(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("newline");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    port_scm = SCM_SUBRARGS[0];
  } else {
    port_scm = SCM_OBJ(SCM_CUROUT);
  }
  if (!SCM_OPORTP(port_scm)) Scm_Error("<output-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {

#line 790 "libio.scm"
SCM_PUTC('\n',port);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libiofresh_line(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("fresh-line");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    port_scm = SCM_SUBRARGS[0];
  } else {
    port_scm = SCM_OBJ(SCM_CUROUT);
  }
  if (!SCM_OPORTP(port_scm)) Scm_Error("<output-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
int SCM_RESULT;

#line 794 "libio.scm"
if ((Scm_PortColumn(port))==(0)){
{SCM_RESULT=(FALSE);goto SCM_STUB_RETURN;}} else {
{SCM_PUTC('\n',port);{SCM_RESULT=(TRUE);goto SCM_STUB_RETURN;}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libiowrite_char(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj ch_scm;
  ScmChar ch;
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("write-char");
  if (SCM_ARGCNT >= 3
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 2 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  ch_scm = SCM_SUBRARGS[0];
  if (!SCM_CHARP(ch_scm)) Scm_Error("character required, but got %S", ch_scm);
  ch = SCM_CHAR_VALUE(ch_scm);
  if (SCM_ARGCNT > 1+1) {
    port_scm = SCM_SUBRARGS[1];
  } else {
    port_scm = SCM_OBJ(SCM_CUROUT);
  }
  if (!SCM_OPORTP(port_scm)) Scm_Error("<output-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {

#line 800 "libio.scm"
SCM_PUTC(ch,port);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libiowrite_byte(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj byte_scm;
  ScmSmallInt byte;
  ScmObj port_scm;
  ScmPort* port;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("write-byte");
  if (SCM_ARGCNT >= 3
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 2 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  byte_scm = SCM_SUBRARGS[0];
  if (!SCM_INTP(byte_scm)) Scm_Error("ScmSmallInt required, but got %S", byte_scm);
  byte = SCM_INT_VALUE(byte_scm);
  if (SCM_ARGCNT > 1+1) {
    port_scm = SCM_SUBRARGS[1];
  } else {
    port_scm = SCM_OBJ(SCM_CUROUT);
  }
  if (!SCM_OPORTP(port_scm)) Scm_Error("<output-port> required, but got %S", port_scm);
  port = SCM_PORT(port_scm);
  {
{
int SCM_RESULT;

#line 808 "libio.scm"
if (((byte)<(0))||((byte)>(255))){{
Scm_Error("argument out of range: %ld",byte);}}

#line 810 "libio.scm"
SCM_PUTB(byte,port);

#line 811 "libio.scm"
{SCM_RESULT=(1);goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libiowrite_limited(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj limit_scm;
  ScmSmallInt limit;
  ScmObj port_scm;
  ScmObj port;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("write-limited");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  limit_scm = SCM_SUBRARGS[1];
  if (!SCM_INTP(limit_scm)) Scm_Error("ScmSmallInt required, but got %S", limit_scm);
  limit = SCM_INT_VALUE(limit_scm);
  if (SCM_ARGCNT > 2+1) {
    port_scm = SCM_SUBRARGS[2];
  } else {
    port_scm = SCM_OBJ(SCM_CUROUT);
  }
  if (!(port_scm)) Scm_Error("scheme object required, but got %S", port_scm);
  port = (port_scm);
  {
{
int SCM_RESULT;

#line 817 "libio.scm"
{SCM_RESULT=(Scm_WriteLimited(obj,port,SCM_WRITE_WRITE,limit));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libioflush(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj oport_scm;
  ScmPort* oport;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("flush");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    oport_scm = SCM_SUBRARGS[0];
  } else {
    oport_scm = SCM_OBJ(SCM_CUROUT);
  }
  if (!SCM_OPORTP(oport_scm)) Scm_Error("<output-port> required, but got %S", oport_scm);
  oport = SCM_PORT(oport_scm);
  {
Scm_Flush(oport);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libioflush_all_ports(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("flush-all-ports");
  {

#line 824 "libio.scm"
Scm_FlushAllPorts(FALSE);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libiowrite_need_recurseP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("write-need-recurse?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {
{
int SCM_RESULT;

#line 832 "libio.scm"
{SCM_RESULT=(!((((((!(SCM_PTRP(obj)))||(
SCM_NUMBERP(obj)))||(
SCM_KEYWORDP(obj)))||(
(SCM_SYMBOLP(obj))&&(SCM_SYMBOL_INTERNED(obj))))||(
(SCM_STRINGP(obj))&&((SCM_STRING_SIZE(obj))==(0))))||(
(SCM_VECTORP(obj))&&((SCM_VECTOR_SIZE(obj))==(0)))));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}

static ScmObj write_controls_allocate(ScmClass* G1825 SCM_UNUSED,ScmObj G1826 SCM_UNUSED){{
#line 887 "libio.scm"
return (SCM_OBJ(Scm_MakeWriteControls(NULL)));}}
static ScmClass *Scm_WriteControlsClass_CPL[] = {
  SCM_CLASS_STATIC_PTR(Scm_TopClass),
  NULL
};
SCM_DEFINE_BUILTIN_CLASS(Scm_WriteControlsClass, NULL, NULL, NULL, write_controls_allocate, Scm_WriteControlsClass_CPL);

static ScmObj Scm_WriteControlsClass_length_GET(ScmObj OBJARG)
{
  ScmWriteControls* obj = SCM_WRITE_CONTROLS(OBJARG);
if (obj->printLength < 0) return SCM_FALSE; else return SCM_MAKE_INT(obj->printLength);
}

static void Scm_WriteControlsClass_length_SET(ScmObj OBJARG, ScmObj value)
{
  ScmWriteControls* obj = SCM_WRITE_CONTROLS(OBJARG);
if (SCM_INTP(value) && SCM_INT_VALUE(value) >= 0) obj->printLength = SCM_INT_VALUE(value); else obj->printLength = -1;
}

static ScmObj Scm_WriteControlsClass_level_GET(ScmObj OBJARG)
{
  ScmWriteControls* obj = SCM_WRITE_CONTROLS(OBJARG);
if (obj->printLevel < 0) return SCM_FALSE; else return SCM_MAKE_INT(obj->printLevel);
}

static void Scm_WriteControlsClass_level_SET(ScmObj OBJARG, ScmObj value)
{
  ScmWriteControls* obj = SCM_WRITE_CONTROLS(OBJARG);
if (SCM_INTP(value) && SCM_INT_VALUE(value) >= 0) obj->printLevel = SCM_INT_VALUE(value); else obj->printLevel = -1;
}

static ScmObj Scm_WriteControlsClass_width_GET(ScmObj OBJARG)
{
  ScmWriteControls* obj = SCM_WRITE_CONTROLS(OBJARG);
if (obj->printWidth < 0) return SCM_FALSE; else return SCM_MAKE_INT(obj->printWidth);
}

static void Scm_WriteControlsClass_width_SET(ScmObj OBJARG, ScmObj value)
{
  ScmWriteControls* obj = SCM_WRITE_CONTROLS(OBJARG);
if (SCM_INTP(value) && SCM_INT_VALUE(value) >= 0) obj->printWidth = SCM_INT_VALUE(value); else obj->printWidth = -1;
}

static ScmObj Scm_WriteControlsClass_base_GET(ScmObj OBJARG)
{
  ScmWriteControls* obj = SCM_WRITE_CONTROLS(OBJARG);
  return Scm_MakeInteger(obj->printBase);
}

static void Scm_WriteControlsClass_base_SET(ScmObj OBJARG, ScmObj value)
{
  ScmWriteControls* obj = SCM_WRITE_CONTROLS(OBJARG);
if (SCM_INTP(value) && SCM_INT_VALUE(value) >= SCM_RADIX_MIN && SCM_INT_VALUE(value) <= SCM_RADIX_MAX) obj->printBase = SCM_INT_VALUE(value); else Scm_Error("print-base must be an integer between %d and %d, but got: %S", SCM_RADIX_MIN, SCM_RADIX_MAX, value);
}

static ScmObj Scm_WriteControlsClass_radix_GET(ScmObj OBJARG)
{
  ScmWriteControls* obj = SCM_WRITE_CONTROLS(OBJARG);
  return SCM_MAKE_BOOL(obj->printRadix);
}

static void Scm_WriteControlsClass_radix_SET(ScmObj OBJARG, ScmObj value)
{
  ScmWriteControls* obj = SCM_WRITE_CONTROLS(OBJARG);
obj->printRadix = !SCM_FALSEP(value);
}

static ScmObj Scm_WriteControlsClass_pretty_GET(ScmObj OBJARG)
{
  ScmWriteControls* obj = SCM_WRITE_CONTROLS(OBJARG);
  return SCM_MAKE_BOOL(obj->printPretty);
}

static void Scm_WriteControlsClass_pretty_SET(ScmObj OBJARG, ScmObj value)
{
  ScmWriteControls* obj = SCM_WRITE_CONTROLS(OBJARG);
obj->printPretty = !SCM_FALSEP(value);
}

static ScmObj Scm_WriteControlsClass_indent_GET(ScmObj OBJARG)
{
  ScmWriteControls* obj = SCM_WRITE_CONTROLS(OBJARG);
  return Scm_MakeInteger(obj->printIndent);
}

static void Scm_WriteControlsClass_indent_SET(ScmObj OBJARG, ScmObj value)
{
  ScmWriteControls* obj = SCM_WRITE_CONTROLS(OBJARG);
if (SCM_INTP(value) && SCM_INT_VALUE(value) >= 0) obj->printIndent = SCM_INT_VALUE(value); else obj->printIndent = 0;
}

static ScmObj Scm_WriteControlsClass_string_length_GET(ScmObj OBJARG)
{
  ScmWriteControls* obj = SCM_WRITE_CONTROLS(OBJARG);
if (obj->stringLength < 0) return SCM_FALSE; else return SCM_MAKE_INT(obj->stringLength);
}

static void Scm_WriteControlsClass_string_length_SET(ScmObj OBJARG, ScmObj value)
{
  ScmWriteControls* obj = SCM_WRITE_CONTROLS(OBJARG);
if (SCM_INTP(value) && SCM_INT_VALUE(value) >= 0) obj->stringLength = SCM_INT_VALUE(value); else obj->stringLength = -1;
}

static ScmClassStaticSlotSpec Scm_WriteControlsClass__SLOTS[] = {
  SCM_CLASS_SLOT_SPEC("length", Scm_WriteControlsClass_length_GET, Scm_WriteControlsClass_length_SET),
  SCM_CLASS_SLOT_SPEC("level", Scm_WriteControlsClass_level_GET, Scm_WriteControlsClass_level_SET),
  SCM_CLASS_SLOT_SPEC("width", Scm_WriteControlsClass_width_GET, Scm_WriteControlsClass_width_SET),
  SCM_CLASS_SLOT_SPEC("base", Scm_WriteControlsClass_base_GET, Scm_WriteControlsClass_base_SET),
  SCM_CLASS_SLOT_SPEC("radix", Scm_WriteControlsClass_radix_GET, Scm_WriteControlsClass_radix_SET),
  SCM_CLASS_SLOT_SPEC("pretty", Scm_WriteControlsClass_pretty_GET, Scm_WriteControlsClass_pretty_SET),
  SCM_CLASS_SLOT_SPEC("indent", Scm_WriteControlsClass_indent_GET, Scm_WriteControlsClass_indent_SET),
  SCM_CLASS_SLOT_SPEC("string-length", Scm_WriteControlsClass_string_length_GET, Scm_WriteControlsClass_string_length_SET),
  SCM_CLASS_SLOT_SPEC_END()
};

static ScmCompiledCode *toplevels[] = {
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[1])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[3])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[7])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[11])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[12])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[13])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[14])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[16])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[18])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[20])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[22])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[25])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[28])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[30])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[31])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[32])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[33])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[34])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[36])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[39])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[41])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[42])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[44])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[46])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[48])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[50])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[52])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[56])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[60])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[64])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[68])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[72])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[74])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[76])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[78])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[80])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[82])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[85])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[88])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[90])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[94])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[98])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[102])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[106])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[108])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[110])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[112])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[114])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[116])),
 NULL /*termination*/
};
ScmObj SCM_debug_info_const_vector()
{
  static _Bool initialized = FALSE;
  if (!initialized) {
    int i = 0;
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[302];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[33];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[300];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[318];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[334];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[337];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = SCM_OBJ(&scm__sc.d1785[112]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[472];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = SCM_OBJ(&scm__sc.d1785[109]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1372];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[331];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1373];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[57];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[48];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = SCM_OBJ(&scm__sc.d1785[113]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[47];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[18];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1374];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = SCM_OBJ(&scm__sc.d1785[114]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[326];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[129];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[375];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[355];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[364];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[374];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[372];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = SCM_OBJ(&scm__sc.d1785[3]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1375];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1376];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[21];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[366];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[396];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1377];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[342];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[395];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[346];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[391];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[385];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[386];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[389];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = SCM_OBJ(&scm__sc.d1785[127]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1378];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[159];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[380];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[451];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[442];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[457];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[455];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[463];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[461];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[478];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[485];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[482];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[475];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[268];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[483];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[423];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[434];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[419];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[480];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[266];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[254];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = SCM_OBJ(&scm__sc.d1785[151]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[32];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[473];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1379];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1380];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1381];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[470];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[498];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[499];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[130];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[496];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[497];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[495];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = SCM_MAKE_INT(-1);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1382];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1383];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1384];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1385];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1386];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[494];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[512];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[508];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[510];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1387];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1388];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1389];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[506];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1390];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[549];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[546];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[547];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[545];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[560];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1391];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[556];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[554];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[467];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[568];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[982];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1392];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[567];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[566];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[410];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[575];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[659];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[662];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[713];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[702];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[729];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[679];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[765];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[763];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1393];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[761];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[759];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[771];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[795];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[796];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[793];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[344];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[790];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1394];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1395];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[786];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = SCM_UNDEFINED;
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[788];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[784];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1396];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1397];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1398];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1399];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1400];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1401];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[782];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1402];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1403];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[780];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[778];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[770];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[776];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[750];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1404];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1405];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1406];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[800];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[806];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[682];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[813];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[810];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1407];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1408];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1409];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1410];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1411];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1412];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1413];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1414];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1415];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1416];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1417];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1418];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1419];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1420];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1421];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1422];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[843];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = SCM_OBJ(&scm__sc.d1785[274]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[841];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1423];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[840];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[839];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[838];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[837];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[836];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[831];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[832];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[830];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[829];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[828];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[827];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[826];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[824];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1424];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[835];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = SCM_OBJ(&scm__sc.d1785[266]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[823];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1425];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[844];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[852];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[845];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[853];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[846];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[854];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[847];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[855];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[848];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[856];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[849];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[857];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[851];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[850];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[821];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1426];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[582];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1427];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[196];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[819];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1428];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1429];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1430];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1431];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1432];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1433];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1434];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1435];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1436];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1437];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1438];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1439];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1440];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1441];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1442];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1443];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[863];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1444];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1445];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1446];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1447];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1448];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1449];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[862];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[881];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[882];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[288];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1450];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[194];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[878];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[869];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1451];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[875];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[873];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[871];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[4];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[868];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[216];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[889];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[887];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[186];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[895];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[168];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[908];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[903];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[177];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[913];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[924];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[927];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[922];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[934];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[932];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[941];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[255];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[950];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[948];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[955];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[960];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1452];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[965];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[970];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[981];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[665];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1453];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1454];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1455];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[979];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[989];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1002];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1456];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1457];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1458];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1459];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1018];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1016];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1032];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[137];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[136];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1027];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1039];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = SCM_OBJ(&scm__sc.d1785[314]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1040];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[628];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[617];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1037];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[158];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1046];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1051];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[91];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[581];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1056];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1055];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1062];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1066]), i++) = scm__rc.d1786[1061];
    initialized = TRUE;
  }
  return SCM_OBJ(&scm__rc.d1786[1066]);
}
void Scm_Init_libio() {
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())));
  scm__rc.d1786[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[0])),TRUE); /* input-port? */
  scm__rc.d1786[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[1])),TRUE); /* obj */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1]), scm__rc.d1786[1]);
  scm__rc.d1786[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[2])),TRUE); /* source-info */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[4]), scm__rc.d1786[2]);
  scm__rc.d1786[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[4])),TRUE); /* bind-info */
  scm__rc.d1786[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[5])),TRUE); /* scheme */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[5]), scm__rc.d1786[0]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[6]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[7]), scm__rc.d1786[3]);
  scm__rc.d1786[5] = Scm_MakeExtendedPair(scm__rc.d1786[0], SCM_OBJ(&scm__rc.d1787[1]), SCM_OBJ(&scm__rc.d1787[9]));
  scm__rc.d1786[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[6])),TRUE); /* <top> */
  scm__rc.d1786[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[7])),TRUE); /* -> */
  scm__rc.d1786[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[8])),TRUE); /* <boolean> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[9]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[9]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[9]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[9]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("input-port?")), SCM_OBJ(&libioinput_portP__STUB), 0);
  libioinput_portP__STUB.common.info = scm__rc.d1786[5];
  libioinput_portP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[9]);
  scm__rc.d1786[16] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[9])),TRUE); /* output-port? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[12]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[13]), scm__rc.d1786[16]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[14]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[15]), scm__rc.d1786[3]);
  scm__rc.d1786[17] = Scm_MakeExtendedPair(scm__rc.d1786[16], SCM_OBJ(&scm__rc.d1787[1]), SCM_OBJ(&scm__rc.d1787[17]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("output-port?")), SCM_OBJ(&libiooutput_portP__STUB), 0);
  libiooutput_portP__STUB.common.info = scm__rc.d1786[17];
  libiooutput_portP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[9]);
  scm__rc.d1786[18] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[10])),TRUE); /* port? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[20]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[21]), scm__rc.d1786[18]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[22]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[23]), scm__rc.d1786[3]);
  scm__rc.d1786[19] = Scm_MakeExtendedPair(scm__rc.d1786[18], SCM_OBJ(&scm__rc.d1787[1]), SCM_OBJ(&scm__rc.d1787[25]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("port?")), SCM_OBJ(&libioportP__STUB), 0);
  libioportP__STUB.common.info = scm__rc.d1786[19];
  libioportP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[9]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[20] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[11])),TRUE); /* port-closed? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[28]), scm__rc.d1786[2]);
  scm__rc.d1786[21] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[12])),TRUE); /* gauche */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[29]), scm__rc.d1786[20]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[30]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[31]), scm__rc.d1786[3]);
  scm__rc.d1786[22] = Scm_MakeExtendedPair(scm__rc.d1786[20], SCM_OBJ(&scm__rc.d1787[1]), SCM_OBJ(&scm__rc.d1787[33]));
  scm__rc.d1786[23] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[13])),TRUE); /* <port> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[24]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[24]))[4] = scm__rc.d1786[23];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[24]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[24]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-closed?")), SCM_OBJ(&libioport_closedP__STUB), 0);
  libioport_closedP__STUB.common.info = scm__rc.d1786[22];
  libioport_closedP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[24]);

#line 68 "libio.scm"
default_file_encoding=(
Scm_BindPrimitiveParameter(Scm_GaucheModule(),"default-file-encoding",
#line 71 "libio.scm"
Scm_CharEncodingName(),0));
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[31] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[14])),TRUE); /* standard-input-port */
  scm__rc.d1786[32] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[15]))); /* :optional */
  scm__rc.d1786[33] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[16])),TRUE); /* p */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[34]), scm__rc.d1786[33]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[35]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[38]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[39]), scm__rc.d1786[31]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[40]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[41]), scm__rc.d1786[3]);
  scm__rc.d1786[34] = Scm_MakeExtendedPair(scm__rc.d1786[31], SCM_OBJ(&scm__rc.d1787[35]), SCM_OBJ(&scm__rc.d1787[43]));
  scm__rc.d1786[35] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[17])),TRUE); /* * */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[36]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[36]))[4] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[36]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[36]))[6] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("standard-input-port")), SCM_OBJ(&libiostandard_input_port__STUB), 0);
  libiostandard_input_port__STUB.common.info = scm__rc.d1786[34];
  libiostandard_input_port__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[36]);
  scm__rc.d1786[43] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[18])),TRUE); /* standard-output-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[46]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[47]), scm__rc.d1786[43]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[48]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[49]), scm__rc.d1786[3]);
  scm__rc.d1786[44] = Scm_MakeExtendedPair(scm__rc.d1786[43], SCM_OBJ(&scm__rc.d1787[35]), SCM_OBJ(&scm__rc.d1787[51]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("standard-output-port")), SCM_OBJ(&libiostandard_output_port__STUB), 0);
  libiostandard_output_port__STUB.common.info = scm__rc.d1786[44];
  libiostandard_output_port__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[36]);
  scm__rc.d1786[45] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[19])),TRUE); /* standard-error-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[54]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[55]), scm__rc.d1786[45]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[56]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[57]), scm__rc.d1786[3]);
  scm__rc.d1786[46] = Scm_MakeExtendedPair(scm__rc.d1786[45], SCM_OBJ(&scm__rc.d1787[35]), SCM_OBJ(&scm__rc.d1787[59]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("standard-error-port")), SCM_OBJ(&libiostandard_error_port__STUB), 0);
  libiostandard_error_port__STUB.common.info = scm__rc.d1786[46];
  libiostandard_error_port__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[36]);

#line 89 "libio.scm"
Scm_BindPrimitiveParameter(Scm_GaucheModule(),"current-trace-port",
#line 91 "libio.scm"
Scm_Stderr(),0);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[47] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[20])),TRUE); /* port-name */
  scm__rc.d1786[48] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[21])),TRUE); /* port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[60]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[63]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[64]), scm__rc.d1786[47]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[65]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[66]), scm__rc.d1786[3]);
  scm__rc.d1786[49] = Scm_MakeExtendedPair(scm__rc.d1786[47], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[68]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[50]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[50]))[4] = scm__rc.d1786[23];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[50]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[50]))[6] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-name")), SCM_OBJ(&libioport_name__STUB), 0);
  libioport_name__STUB.common.info = scm__rc.d1786[49];
  libioport_name__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[50]);
  scm__rc.d1786[57] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[22])),TRUE); /* port-current-line */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[71]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[72]), scm__rc.d1786[57]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[73]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[74]), scm__rc.d1786[3]);
  scm__rc.d1786[58] = Scm_MakeExtendedPair(scm__rc.d1786[57], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[76]));
  scm__rc.d1786[59] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[23])),TRUE); /* <fixnum> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[60]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[60]))[4] = scm__rc.d1786[23];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[60]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[60]))[6] = scm__rc.d1786[59];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-current-line")), SCM_OBJ(&libioport_current_line__STUB), 0);
  libioport_current_line__STUB.common.info = scm__rc.d1786[58];
  libioport_current_line__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[60]);
  scm__rc.d1786[67] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[24])),TRUE); /* port-file-number */
  scm__rc.d1786[68] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[25])),TRUE); /* dup? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[77]), scm__rc.d1786[68]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[78]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[79]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[82]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[83]), scm__rc.d1786[67]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[84]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[85]), scm__rc.d1786[3]);
  scm__rc.d1786[69] = Scm_MakeExtendedPair(scm__rc.d1786[67], SCM_OBJ(&scm__rc.d1787[79]), SCM_OBJ(&scm__rc.d1787[87]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[70]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[70]))[4] = scm__rc.d1786[23];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[70]))[5] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[70]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[70]))[7] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-file-number")), SCM_OBJ(&libioport_file_number__STUB), 0);
  libioport_file_number__STUB.common.info = scm__rc.d1786[69];
  libioport_file_number__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[70]);
  scm__rc.d1786[78] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[26])),TRUE); /* port-fd-dup! */
  scm__rc.d1786[79] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[27])),TRUE); /* dst */
  scm__rc.d1786[80] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[28])),TRUE); /* src */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[88]), scm__rc.d1786[80]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[89]), scm__rc.d1786[79]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[92]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[93]), scm__rc.d1786[78]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[94]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[95]), scm__rc.d1786[3]);
  scm__rc.d1786[81] = Scm_MakeExtendedPair(scm__rc.d1786[78], SCM_OBJ(&scm__rc.d1787[89]), SCM_OBJ(&scm__rc.d1787[97]));
  scm__rc.d1786[82] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[29])),TRUE); /* <void> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[83]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[83]))[4] = scm__rc.d1786[23];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[83]))[5] = scm__rc.d1786[23];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[83]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[83]))[7] = scm__rc.d1786[82];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-fd-dup!")), SCM_OBJ(&libioport_fd_dupX__STUB), 0);
  libioport_fd_dupX__STUB.common.info = scm__rc.d1786[81];
  libioport_fd_dupX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[83]);
  scm__rc.d1786[91] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[30])),TRUE); /* port-attribute-set! */
  scm__rc.d1786[92] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[31])),TRUE); /* key */
  scm__rc.d1786[93] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[32])),TRUE); /* val */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[98]), scm__rc.d1786[93]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[99]), scm__rc.d1786[92]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[100]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[103]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[104]), scm__rc.d1786[91]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[105]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[106]), scm__rc.d1786[3]);
  scm__rc.d1786[94] = Scm_MakeExtendedPair(scm__rc.d1786[91], SCM_OBJ(&scm__rc.d1787[100]), SCM_OBJ(&scm__rc.d1787[108]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[95]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[95]))[4] = scm__rc.d1786[23];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[95]))[5] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[95]))[6] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[95]))[7] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[95]))[8] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-attribute-set!")), SCM_OBJ(&libioport_attribute_setX__STUB), 0);
  libioport_attribute_setX__STUB.common.info = scm__rc.d1786[94];
  libioport_attribute_setX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[95]);
  scm__rc.d1786[104] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[33])),TRUE); /* port-attribute-ref */
  scm__rc.d1786[105] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[34])),TRUE); /* fallback */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[109]), scm__rc.d1786[105]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[110]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[111]), scm__rc.d1786[92]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[112]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[115]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[116]), scm__rc.d1786[104]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[117]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[118]), scm__rc.d1786[3]);
  scm__rc.d1786[106] = Scm_MakeExtendedPair(scm__rc.d1786[104], SCM_OBJ(&scm__rc.d1787[112]), SCM_OBJ(&scm__rc.d1787[120]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[107]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[107]))[4] = scm__rc.d1786[23];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[107]))[5] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[107]))[6] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[107]))[7] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[107]))[8] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-attribute-ref")), SCM_OBJ(&libioport_attribute_ref__STUB), 0);
  libioport_attribute_ref__STUB.common.info = scm__rc.d1786[106];
  libioport_attribute_ref__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[107]);
  Scm_SetterSet(SCM_PROCEDURE(&libioport_attribute_ref__STUB), SCM_PROCEDURE(&libioport_attribute_setX__STUB), TRUE);
  scm__rc.d1786[116] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[35])),TRUE); /* port-attribute-delete! */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[121]), scm__rc.d1786[92]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[122]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[125]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[126]), scm__rc.d1786[116]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[127]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[128]), scm__rc.d1786[3]);
  scm__rc.d1786[117] = Scm_MakeExtendedPair(scm__rc.d1786[116], SCM_OBJ(&scm__rc.d1787[122]), SCM_OBJ(&scm__rc.d1787[130]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[118]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[118]))[4] = scm__rc.d1786[23];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[118]))[5] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[118]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[118]))[7] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-attribute-delete!")), SCM_OBJ(&libioport_attribute_deleteX__STUB), 0);
  libioport_attribute_deleteX__STUB.common.info = scm__rc.d1786[117];
  libioport_attribute_deleteX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[118]);
  scm__rc.d1786[126] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[36])),TRUE); /* port-attributes */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[133]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[134]), scm__rc.d1786[126]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[135]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[136]), scm__rc.d1786[3]);
  scm__rc.d1786[127] = Scm_MakeExtendedPair(scm__rc.d1786[126], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[138]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-attributes")), SCM_OBJ(&libioport_attributes__STUB), 0);
  libioport_attributes__STUB.common.info = scm__rc.d1786[127];
  libioport_attributes__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[50]);
  scm__rc.d1786[128] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[38])),TRUE); /* file */
  scm__rc.d1786[129] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[39])),TRUE); /* proc */
  scm__rc.d1786[130] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[40])),TRUE); /* string */
  scm__rc.d1786[131] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[37])),TRUE); /* port-type */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[141]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[142]), scm__rc.d1786[131]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[143]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[144]), scm__rc.d1786[3]);
  scm__rc.d1786[132] = Scm_MakeExtendedPair(scm__rc.d1786[131], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[146]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-type")), SCM_OBJ(&libioport_type__STUB), 0);
  libioport_type__STUB.common.info = scm__rc.d1786[132];
  libioport_type__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[50]);
  scm__rc.d1786[133] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[41])),TRUE); /* port-buffering */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[149]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[150]), scm__rc.d1786[133]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[151]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[152]), scm__rc.d1786[3]);
  scm__rc.d1786[134] = Scm_MakeExtendedPair(scm__rc.d1786[133], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[154]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-buffering")), SCM_OBJ(&libioport_buffering__STUB), 0);
  libioport_buffering__STUB.common.info = scm__rc.d1786[134];
  libioport_buffering__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[50]);
  Scm_SetterSet(SCM_PROCEDURE(&libioport_buffering__STUB), SCM_PROCEDURE(&libioport_buffering_SETTER__STUB), TRUE);
  scm__rc.d1786[135] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[43])),TRUE); /* port-link! */
  scm__rc.d1786[136] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[44])),TRUE); /* iport */
  scm__rc.d1786[137] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[45])),TRUE); /* oport */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[155]), scm__rc.d1786[137]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[156]), scm__rc.d1786[136]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[159]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[160]), scm__rc.d1786[135]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[161]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[162]), scm__rc.d1786[3]);
  scm__rc.d1786[138] = Scm_MakeExtendedPair(scm__rc.d1786[135], SCM_OBJ(&scm__rc.d1787[156]), SCM_OBJ(&scm__rc.d1787[164]));
  scm__rc.d1786[139] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[46])),TRUE); /* <input-port> */
  scm__rc.d1786[140] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[47])),TRUE); /* <output-port> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[141]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[141]))[4] = scm__rc.d1786[139];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[141]))[5] = scm__rc.d1786[140];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[141]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[141]))[7] = scm__rc.d1786[82];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-link!")), SCM_OBJ(&libioport_linkX__STUB), 0);
  libioport_linkX__STUB.common.info = scm__rc.d1786[138];
  libioport_linkX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[141]);
  scm__rc.d1786[149] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[48])),TRUE); /* port-unlink! */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[167]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[168]), scm__rc.d1786[149]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[169]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[170]), scm__rc.d1786[3]);
  scm__rc.d1786[150] = Scm_MakeExtendedPair(scm__rc.d1786[149], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[172]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[151]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[151]))[4] = scm__rc.d1786[23];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[151]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[151]))[6] = scm__rc.d1786[82];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-unlink!")), SCM_OBJ(&libioport_unlinkX__STUB), 0);
  libioport_unlinkX__STUB.common.info = scm__rc.d1786[150];
  libioport_unlinkX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[151]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
  scm__rc.d1786[158] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[49])),TRUE); /* port-case-fold */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[175]), scm__rc.d1786[2]);
  scm__rc.d1786[159] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[51])),TRUE); /* gauche.internal */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[176]), scm__rc.d1786[158]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[177]), scm__rc.d1786[159]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[178]), scm__rc.d1786[3]);
  scm__rc.d1786[160] = Scm_MakeExtendedPair(scm__rc.d1786[158], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[180]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[161]))[3] = scm__rc.d1786[159];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[161]))[4] = scm__rc.d1786[23];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[161]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[161]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("port-case-fold")), SCM_OBJ(&libioport_case_fold__STUB), 0);
  libioport_case_fold__STUB.common.info = scm__rc.d1786[160];
  libioport_case_fold__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[161]);
  Scm_SetterSet(SCM_PROCEDURE(&libioport_case_fold__STUB), SCM_PROCEDURE(&libioport_case_fold_SETTER__STUB), TRUE);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())));
  scm__rc.d1786[168] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[52])),TRUE); /* close-input-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[183]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[184]), scm__rc.d1786[168]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[185]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[186]), scm__rc.d1786[3]);
  scm__rc.d1786[169] = Scm_MakeExtendedPair(scm__rc.d1786[168], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[188]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[170]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[170]))[4] = scm__rc.d1786[139];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[170]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[170]))[6] = scm__rc.d1786[82];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("close-input-port")), SCM_OBJ(&libioclose_input_port__STUB), 0);
  libioclose_input_port__STUB.common.info = scm__rc.d1786[169];
  libioclose_input_port__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[170]);
  scm__rc.d1786[177] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[53])),TRUE); /* close-output-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[191]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[192]), scm__rc.d1786[177]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[193]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[194]), scm__rc.d1786[3]);
  scm__rc.d1786[178] = Scm_MakeExtendedPair(scm__rc.d1786[177], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[196]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[179]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[179]))[4] = scm__rc.d1786[140];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[179]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[179]))[6] = scm__rc.d1786[82];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("close-output-port")), SCM_OBJ(&libioclose_output_port__STUB), 0);
  libioclose_output_port__STUB.common.info = scm__rc.d1786[178];
  libioclose_output_port__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[179]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[186] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[54])),TRUE); /* close-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[199]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[200]), scm__rc.d1786[186]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[201]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[202]), scm__rc.d1786[3]);
  scm__rc.d1786[187] = Scm_MakeExtendedPair(scm__rc.d1786[186], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[204]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("close-port")), SCM_OBJ(&libioclose_port__STUB), 0);
  libioclose_port__STUB.common.info = scm__rc.d1786[187];
  libioclose_port__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[151]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
  scm__rc.d1786[188] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[55]))); /* :error */
  scm__rc.d1786[189] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[56]))); /* :if-does-not-exist */
  scm__rc.d1786[190] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[57]))); /* :buffering */
  scm__rc.d1786[191] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[58]))); /* :binary */
  scm__rc.d1786[192] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[59]))); /* :element-type */
  scm__rc.d1786[193] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[61]))); /* :character */
  scm__rc.d1786[194] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[60])),TRUE); /* %open-input-file */
  scm__rc.d1786[195] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[62])),TRUE); /* path */
  scm__rc.d1786[196] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[31]))); /* :key */
  scm__rc.d1786[197] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[56])),TRUE); /* if-does-not-exist */
  scm__rc.d1786[198] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[57])),TRUE); /* buffering */
  scm__rc.d1786[199] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[59])),TRUE); /* element-type */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[205]), scm__rc.d1786[199]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[206]), scm__rc.d1786[198]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[207]), scm__rc.d1786[197]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[208]), scm__rc.d1786[196]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[209]), scm__rc.d1786[195]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[212]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[213]), scm__rc.d1786[194]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[214]), scm__rc.d1786[159]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[215]), scm__rc.d1786[3]);
  scm__rc.d1786[200] = Scm_MakeExtendedPair(scm__rc.d1786[194], SCM_OBJ(&scm__rc.d1787[209]), SCM_OBJ(&scm__rc.d1787[217]));
  scm__rc.d1786[201] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[63])),TRUE); /* <string> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[202]))[3] = scm__rc.d1786[159];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[202]))[4] = scm__rc.d1786[201];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[202]))[5] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[202]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[202]))[7] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("%open-input-file")), SCM_OBJ(&libio_25open_input_file__STUB), 0);
  libio_25open_input_file__STUB.common.info = scm__rc.d1786[200];
  libio_25open_input_file__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[202]);
  scm__rc.d1786[210] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[64]))); /* :supersede */
  scm__rc.d1786[211] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[65]))); /* :if-exists */
  scm__rc.d1786[212] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[66]))); /* :create */
  scm__rc.d1786[213] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[67]))); /* :mode */
  scm__rc.d1786[214] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[69]))); /* :overwrite */
  scm__rc.d1786[215] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[70]))); /* :append */
  scm__rc.d1786[216] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[68])),TRUE); /* %open-output-file */
  scm__rc.d1786[217] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[65])),TRUE); /* if-exists */
  scm__rc.d1786[218] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[67])),TRUE); /* mode */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[218]), scm__rc.d1786[218]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[219]), scm__rc.d1786[197]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[220]), scm__rc.d1786[217]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[221]), scm__rc.d1786[196]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[222]), scm__rc.d1786[195]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[225]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[226]), scm__rc.d1786[216]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[227]), scm__rc.d1786[159]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[228]), scm__rc.d1786[3]);
  scm__rc.d1786[219] = Scm_MakeExtendedPair(scm__rc.d1786[216], SCM_OBJ(&scm__rc.d1787[222]), SCM_OBJ(&scm__rc.d1787[230]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("%open-output-file")), SCM_OBJ(&libio_25open_output_file__STUB), 0);
  libio_25open_output_file__STUB.common.info = scm__rc.d1786[219];
  libio_25open_output_file__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[202]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[220] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[71]))); /* :owner? */
  scm__rc.d1786[221] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[72]))); /* :name */
  scm__rc.d1786[222] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[74])),TRUE); /* dup */
  scm__rc.d1786[223] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[73])),TRUE); /* open-input-fd-port */
  scm__rc.d1786[224] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[75])),TRUE); /* fd */
  scm__rc.d1786[225] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[71])),TRUE); /* owner? */
  scm__rc.d1786[226] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[72])),TRUE); /* name */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[231]), scm__rc.d1786[226]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[232]), scm__rc.d1786[225]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[233]), scm__rc.d1786[198]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[234]), scm__rc.d1786[196]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[235]), scm__rc.d1786[224]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[238]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[239]), scm__rc.d1786[223]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[240]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[241]), scm__rc.d1786[3]);
  scm__rc.d1786[227] = Scm_MakeExtendedPair(scm__rc.d1786[223], SCM_OBJ(&scm__rc.d1787[235]), SCM_OBJ(&scm__rc.d1787[243]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[228]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[228]))[4] = scm__rc.d1786[59];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[228]))[5] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[228]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[228]))[7] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("open-input-fd-port")), SCM_OBJ(&libioopen_input_fd_port__STUB), 0);
  libioopen_input_fd_port__STUB.common.info = scm__rc.d1786[227];
  libioopen_input_fd_port__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[228]);
  scm__rc.d1786[236] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[76])),TRUE); /* open-output-fd-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[246]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[247]), scm__rc.d1786[236]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[248]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[249]), scm__rc.d1786[3]);
  scm__rc.d1786[237] = Scm_MakeExtendedPair(scm__rc.d1786[236], SCM_OBJ(&scm__rc.d1787[235]), SCM_OBJ(&scm__rc.d1787[251]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("open-output-fd-port")), SCM_OBJ(&libioopen_output_fd_port__STUB), 0);
  libioopen_output_fd_port__STUB.common.info = scm__rc.d1786[237];
  libioopen_output_fd_port__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[228]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[238] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[77])),TRUE); /* open-input-buffered-port */
  scm__rc.d1786[239] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[78])),TRUE); /* filler */
  scm__rc.d1786[240] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[79])),TRUE); /* buffer-size */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[252]), scm__rc.d1786[240]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[253]), scm__rc.d1786[239]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[256]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[257]), scm__rc.d1786[238]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[258]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[259]), scm__rc.d1786[3]);
  scm__rc.d1786[241] = Scm_MakeExtendedPair(scm__rc.d1786[238], SCM_OBJ(&scm__rc.d1787[253]), SCM_OBJ(&scm__rc.d1787[261]));
  scm__rc.d1786[242] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[80])),TRUE); /* <procedure> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[243]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[243]))[4] = scm__rc.d1786[242];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[243]))[5] = scm__rc.d1786[59];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[243]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[243]))[7] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("open-input-buffered-port")), SCM_OBJ(&libioopen_input_buffered_port__STUB), 0);
  libioopen_input_buffered_port__STUB.common.info = scm__rc.d1786[241];
  libioopen_input_buffered_port__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[243]);
  scm__rc.d1786[251] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[81])),TRUE); /* open-output-buffered-port */
  scm__rc.d1786[252] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[82])),TRUE); /* flusher */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[262]), scm__rc.d1786[252]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[265]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[266]), scm__rc.d1786[251]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[267]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[268]), scm__rc.d1786[3]);
  scm__rc.d1786[253] = Scm_MakeExtendedPair(scm__rc.d1786[251], SCM_OBJ(&scm__rc.d1787[262]), SCM_OBJ(&scm__rc.d1787[270]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("open-output-buffered-port")), SCM_OBJ(&libioopen_output_buffered_port__STUB), 0);
  libioopen_output_buffered_port__STUB.common.info = scm__rc.d1786[253];
  libioopen_output_buffered_port__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[243]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[254] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[83]))); /* :private? */
  scm__rc.d1786[255] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[85])),TRUE); /* open-input-string */
  scm__rc.d1786[256] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[83])),TRUE); /* private? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[271]), scm__rc.d1786[256]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[272]), scm__rc.d1786[196]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[273]), scm__rc.d1786[130]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[276]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[277]), scm__rc.d1786[255]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[278]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[279]), scm__rc.d1786[3]);
  scm__rc.d1786[257] = Scm_MakeExtendedPair(scm__rc.d1786[255], SCM_OBJ(&scm__rc.d1787[273]), SCM_OBJ(&scm__rc.d1787[281]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[258]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[258]))[4] = scm__rc.d1786[201];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[258]))[5] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[258]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[258]))[7] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("open-input-string")), SCM_OBJ(&libioopen_input_string__STUB), 0);
  libioopen_input_string__STUB.common.info = scm__rc.d1786[257];
  libioopen_input_string__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[258]);
  scm__rc.d1786[266] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[87])),TRUE); /* open-output-string */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[284]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[285]), scm__rc.d1786[266]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[286]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[287]), scm__rc.d1786[3]);
  scm__rc.d1786[267] = Scm_MakeExtendedPair(scm__rc.d1786[266], SCM_OBJ(&scm__rc.d1787[272]), SCM_OBJ(&scm__rc.d1787[289]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("open-output-string")), SCM_OBJ(&libioopen_output_string__STUB), 0);
  libioopen_output_string__STUB.common.info = scm__rc.d1786[267];
  libioopen_output_string__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[36]);
  scm__rc.d1786[268] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[88])),TRUE); /* get-output-string */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[292]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[293]), scm__rc.d1786[268]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[294]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[295]), scm__rc.d1786[3]);
  scm__rc.d1786[269] = Scm_MakeExtendedPair(scm__rc.d1786[268], SCM_OBJ(&scm__rc.d1787[155]), SCM_OBJ(&scm__rc.d1787[297]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[270]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[270]))[4] = scm__rc.d1786[140];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[270]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[270]))[6] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("get-output-string")), SCM_OBJ(&libioget_output_string__STUB), 0);
  libioget_output_string__STUB.common.info = scm__rc.d1786[269];
  libioget_output_string__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[270]);
  scm__rc.d1786[277] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[89])),TRUE); /* get-output-byte-string */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[300]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[301]), scm__rc.d1786[277]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[302]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[303]), scm__rc.d1786[3]);
  scm__rc.d1786[278] = Scm_MakeExtendedPair(scm__rc.d1786[277], SCM_OBJ(&scm__rc.d1787[155]), SCM_OBJ(&scm__rc.d1787[305]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("get-output-byte-string")), SCM_OBJ(&libioget_output_byte_string__STUB), 0);
  libioget_output_byte_string__STUB.common.info = scm__rc.d1786[278];
  libioget_output_byte_string__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[270]);
  scm__rc.d1786[279] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[90])),TRUE); /* get-remaining-input-string */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[306]), scm__rc.d1786[136]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[309]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[310]), scm__rc.d1786[279]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[311]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[312]), scm__rc.d1786[3]);
  scm__rc.d1786[280] = Scm_MakeExtendedPair(scm__rc.d1786[279], SCM_OBJ(&scm__rc.d1787[306]), SCM_OBJ(&scm__rc.d1787[314]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[281]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[281]))[4] = scm__rc.d1786[139];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[281]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[281]))[6] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("get-remaining-input-string")), SCM_OBJ(&libioget_remaining_input_string__STUB), 0);
  libioget_remaining_input_string__STUB.common.info = scm__rc.d1786[280];
  libioget_remaining_input_string__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[281]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[288] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[91])),TRUE); /* open-coding-aware-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[317]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[318]), scm__rc.d1786[288]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[319]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[320]), scm__rc.d1786[3]);
  scm__rc.d1786[289] = Scm_MakeExtendedPair(scm__rc.d1786[288], SCM_OBJ(&scm__rc.d1787[306]), SCM_OBJ(&scm__rc.d1787[322]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("open-coding-aware-port")), SCM_OBJ(&libioopen_coding_aware_port__STUB), 0);
  libioopen_coding_aware_port__STUB.common.info = scm__rc.d1786[289];
  libioopen_coding_aware_port__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[281]);
  scm__rc.d1786[290] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[92])),TRUE); /* port-has-port-position? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[325]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[326]), scm__rc.d1786[290]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[327]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[328]), scm__rc.d1786[3]);
  scm__rc.d1786[291] = Scm_MakeExtendedPair(scm__rc.d1786[290], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[330]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-has-port-position?")), SCM_OBJ(&libioport_has_port_positionP__STUB), 0);
  libioport_has_port_positionP__STUB.common.info = scm__rc.d1786[291];
  libioport_has_port_positionP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[24]);
  scm__rc.d1786[292] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[93])),TRUE); /* port-has-set-port-position!? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[333]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[334]), scm__rc.d1786[292]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[335]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[336]), scm__rc.d1786[3]);
  scm__rc.d1786[293] = Scm_MakeExtendedPair(scm__rc.d1786[292], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[338]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-has-set-port-position!?")), SCM_OBJ(&libioport_has_set_port_positionXP__STUB), 0);
  libioport_has_set_port_positionXP__STUB.common.info = scm__rc.d1786[293];
  libioport_has_set_port_positionXP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[24]);
  scm__rc.d1786[294] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[94])),TRUE); /* port-position */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[341]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[342]), scm__rc.d1786[294]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[343]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[344]), scm__rc.d1786[3]);
  scm__rc.d1786[295] = Scm_MakeExtendedPair(scm__rc.d1786[294], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[346]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-position")), SCM_OBJ(&libioport_position__STUB), 0);
  libioport_position__STUB.common.info = scm__rc.d1786[295];
  libioport_position__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[50]);
  scm__rc.d1786[296] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[95])),TRUE); /* set-port-position! */
  scm__rc.d1786[297] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[96])),TRUE); /* pos */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[347]), scm__rc.d1786[297]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[348]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[351]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[352]), scm__rc.d1786[296]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[353]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[354]), scm__rc.d1786[3]);
  scm__rc.d1786[298] = Scm_MakeExtendedPair(scm__rc.d1786[296], SCM_OBJ(&scm__rc.d1787[348]), SCM_OBJ(&scm__rc.d1787[356]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("set-port-position!")), SCM_OBJ(&libioset_port_positionX__STUB), 0);
  libioset_port_positionX__STUB.common.info = scm__rc.d1786[298];
  libioset_port_positionX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[118]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[299] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[97])),TRUE); /* SEEK_SET */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[299]), Scm_MakeInteger(SEEK_SET), SCM_BINDING_CONST);

  scm__rc.d1786[300] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[98])),TRUE); /* SEEK_CUR */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[300]), Scm_MakeInteger(SEEK_CUR), SCM_BINDING_CONST);

  scm__rc.d1786[301] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[99])),TRUE); /* SEEK_END */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[301]), Scm_MakeInteger(SEEK_END), SCM_BINDING_CONST);

  scm__rc.d1786[302] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[100])),TRUE); /* port-seek */
  scm__rc.d1786[303] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[101])),TRUE); /* offset */
  scm__rc.d1786[304] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[102])),TRUE); /* whence */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[357]), scm__rc.d1786[304]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[358]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[359]), scm__rc.d1786[303]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[360]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[363]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[364]), scm__rc.d1786[302]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[365]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[366]), scm__rc.d1786[3]);
  scm__rc.d1786[305] = Scm_MakeExtendedPair(scm__rc.d1786[302], SCM_OBJ(&scm__rc.d1787[360]), SCM_OBJ(&scm__rc.d1787[368]));
  scm__rc.d1786[306] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[103])),TRUE); /* <integer> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[307]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[307]))[4] = scm__rc.d1786[23];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[307]))[5] = scm__rc.d1786[306];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[307]))[6] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[307]))[7] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[307]))[8] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port-seek")), SCM_OBJ(&libioport_seek__STUB), 0);
  libioport_seek__STUB.common.info = scm__rc.d1786[305];
  libioport_seek__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[307]);
  scm__rc.d1786[317] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[104])),TRUE); /* %expression-name-mark-key */
  scm__rc.d1786[316] = Scm_MakeIdentifier(scm__rc.d1786[317], SCM_MODULE(Scm_GaucheInternalModule()), SCM_NIL); /* gauche.internal#%expression-name-mark-key */
  scm__rc.d1786[318] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[105])),TRUE); /* port-tell */
  scm__rc.d1786[320] = SCM_OBJ(Scm_FindModule(SCM_SYMBOL(scm__rc.d1786[21]), SCM_FIND_MODULE_CREATE|SCM_FIND_MODULE_PLACEHOLDING)); /* module gauche */
  scm__rc.d1786[319] = Scm_MakeIdentifier(scm__rc.d1786[302], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#port-seek */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[371]), scm__rc.d1786[2]);
  scm__rc.d1786[321] = Scm_MakeExtendedPair(scm__rc.d1786[318], SCM_OBJ(&scm__rc.d1787[34]), SCM_OBJ(&scm__rc.d1787[372]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[373]), scm__rc.d1786[321]);
  scm__rc.d1786[322] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[0])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[0]))->name = scm__rc.d1786[318];/* port-tell */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[0]))->debugInfo = scm__rc.d1786[322];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[4] = SCM_WORD(scm__rc.d1786[319]);
  scm__rc.d1786[323] = Scm_MakeIdentifier(scm__rc.d1786[318], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#port-tell */
  scm__rc.d1786[324] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[106])),TRUE); /* %toplevel */
  scm__rc.d1786[325] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[1])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[1]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[1]))->debugInfo = scm__rc.d1786[325];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[6]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[6]))[6] = SCM_WORD(scm__rc.d1786[318]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[6]))[13] = SCM_WORD(scm__rc.d1786[323]);
  scm__rc.d1786[326] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[107])),TRUE); /* port-position-prefix */
  scm__rc.d1786[327] = Scm_MakeIdentifier(scm__rc.d1786[18], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#port? */
  scm__rc.d1786[328] = Scm_MakeIdentifier(scm__rc.d1786[47], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#port-name */
  scm__rc.d1786[329] = Scm_MakeIdentifier(scm__rc.d1786[57], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#port-current-line */
  scm__rc.d1786[331] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[108])),TRUE); /* positive? */
  scm__rc.d1786[330] = Scm_MakeIdentifier(scm__rc.d1786[331], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#positive? */
  scm__rc.d1786[332] = SCM_OBJ(Scm_MakeBox(SCM_FALSE));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[374]), scm__rc.d1786[332]);
  scm__rc.d1786[334] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[110])),TRUE); /* format-internal */
  scm__rc.d1786[336] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[111])),TRUE); /* gauche.format */
  scm__rc.d1786[335] = SCM_OBJ(Scm_FindModule(SCM_SYMBOL(scm__rc.d1786[336]), SCM_FIND_MODULE_CREATE|SCM_FIND_MODULE_PLACEHOLDING)); /* module gauche.format */
  scm__rc.d1786[333] = Scm_MakeIdentifier(scm__rc.d1786[334], SCM_MODULE(scm__rc.d1786[335]), SCM_NIL); /* gauche.format#format-internal */
  scm__rc.d1786[337] = SCM_OBJ(Scm_MakeBox(SCM_FALSE));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[377]), scm__rc.d1786[337]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[382]), scm__rc.d1786[2]);
  scm__rc.d1786[338] = Scm_MakeExtendedPair(scm__rc.d1786[326], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[383]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[384]), scm__rc.d1786[338]);
  scm__rc.d1786[339] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[2])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[2]))->name = scm__rc.d1786[326];/* port-position-prefix */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[2]))->debugInfo = scm__rc.d1786[339];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[21]))[4] = SCM_WORD(scm__rc.d1786[327]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[21]))[11] = SCM_WORD(scm__rc.d1786[328]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[21]))[20] = SCM_WORD(scm__rc.d1786[329]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[21]))[26] = SCM_WORD(scm__rc.d1786[330]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[21]))[40] = SCM_WORD(scm__rc.d1786[333]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[21]))[53] = SCM_WORD(scm__rc.d1786[333]);
  scm__rc.d1786[340] = Scm_MakeIdentifier(scm__rc.d1786[326], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#port-position-prefix */
  scm__rc.d1786[341] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[3])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[3]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[3]))->debugInfo = scm__rc.d1786[341];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[80]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[80]))[6] = SCM_WORD(scm__rc.d1786[326]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[80]))[13] = SCM_WORD(scm__rc.d1786[340]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
  scm__rc.d1786[342] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[115])),TRUE); /* %port-walking? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[387]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[388]), scm__rc.d1786[342]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[389]), scm__rc.d1786[159]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[390]), scm__rc.d1786[3]);
  scm__rc.d1786[343] = Scm_MakeExtendedPair(scm__rc.d1786[342], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[392]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("%port-walking?")), SCM_OBJ(&libio_25port_walkingP__STUB), 0);
  libio_25port_walkingP__STUB.common.info = scm__rc.d1786[343];
  libio_25port_walkingP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[161]);
  Scm_SetterSet(SCM_PROCEDURE(&libio_25port_walkingP__STUB), SCM_PROCEDURE(&libio_25port_walkingP_SETTER__STUB), TRUE);
  scm__rc.d1786[344] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[117])),TRUE); /* %port-writing-shared? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[395]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[396]), scm__rc.d1786[344]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[397]), scm__rc.d1786[159]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[398]), scm__rc.d1786[3]);
  scm__rc.d1786[345] = Scm_MakeExtendedPair(scm__rc.d1786[344], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[400]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("%port-writing-shared?")), SCM_OBJ(&libio_25port_writing_sharedP__STUB), 0);
  libio_25port_writing_sharedP__STUB.common.info = scm__rc.d1786[345];
  libio_25port_writing_sharedP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[161]);
  Scm_SetterSet(SCM_PROCEDURE(&libio_25port_writing_sharedP__STUB), SCM_PROCEDURE(&libio_25port_writing_sharedP_SETTER__STUB), TRUE);
  Scm_InitStaticClassWithMeta(&Scm_WriteStateClass, "<write-state>", SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), NULL, SCM_FALSE, Scm_WriteStateClass__SLOTS, 0);
  scm__rc.d1786[346] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[119])),TRUE); /* %port-write-state */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[403]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[404]), scm__rc.d1786[346]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[405]), scm__rc.d1786[159]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[406]), scm__rc.d1786[3]);
  scm__rc.d1786[347] = Scm_MakeExtendedPair(scm__rc.d1786[346], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[408]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[348]))[3] = scm__rc.d1786[159];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[348]))[4] = scm__rc.d1786[23];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[348]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[348]))[6] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("%port-write-state")), SCM_OBJ(&libio_25port_write_state__STUB), 0);
  libio_25port_write_state__STUB.common.info = scm__rc.d1786[347];
  libio_25port_write_state__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[348]);
  Scm_SetterSet(SCM_PROCEDURE(&libio_25port_write_state__STUB), SCM_PROCEDURE(&libio_25port_write_state_SETTER__STUB), TRUE);
  scm__rc.d1786[355] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[121])),TRUE); /* %port-lock! */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[411]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[412]), scm__rc.d1786[355]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[413]), scm__rc.d1786[159]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[414]), scm__rc.d1786[3]);
  scm__rc.d1786[356] = Scm_MakeExtendedPair(scm__rc.d1786[355], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[416]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[357]))[3] = scm__rc.d1786[159];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[357]))[4] = scm__rc.d1786[23];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[357]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[357]))[6] = scm__rc.d1786[82];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("%port-lock!")), SCM_OBJ(&libio_25port_lockX__STUB), 0);
  libio_25port_lockX__STUB.common.info = scm__rc.d1786[356];
  libio_25port_lockX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[357]);
  scm__rc.d1786[364] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[122])),TRUE); /* %port-unlock! */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[419]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[420]), scm__rc.d1786[364]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[421]), scm__rc.d1786[159]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[422]), scm__rc.d1786[3]);
  scm__rc.d1786[365] = Scm_MakeExtendedPair(scm__rc.d1786[364], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[424]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("%port-unlock!")), SCM_OBJ(&libio_25port_unlockX__STUB), 0);
  libio_25port_unlockX__STUB.common.info = scm__rc.d1786[365];
  libio_25port_unlockX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[357]);
  scm__rc.d1786[366] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[123])),TRUE); /* with-port-locking */
  scm__rc.d1786[368] = SCM_OBJ(Scm_FindModule(SCM_SYMBOL(scm__rc.d1786[159]), SCM_FIND_MODULE_CREATE|SCM_FIND_MODULE_PLACEHOLDING)); /* module gauche.internal */
  scm__rc.d1786[367] = Scm_MakeIdentifier(scm__rc.d1786[355], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#%port-lock! */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[426]), scm__rc.d1786[366]);
  scm__rc.d1786[369] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[4])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[4]))->debugInfo = scm__rc.d1786[369];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[95]))[4] = SCM_WORD(scm__rc.d1786[367]);
  scm__rc.d1786[370] = Scm_MakeIdentifier(scm__rc.d1786[364], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#%port-unlock! */
  scm__rc.d1786[371] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[5])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[5]))->debugInfo = scm__rc.d1786[371];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[104]))[2] = SCM_WORD(scm__rc.d1786[370]);
  scm__rc.d1786[372] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[2]))); /* :source-info */
  scm__rc.d1786[374] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[124])),TRUE); /* %unwind-protect */
  scm__rc.d1786[373] = Scm_MakeIdentifier(scm__rc.d1786[374], SCM_MODULE(Scm_GaucheInternalModule()), SCM_NIL); /* gauche.internal#%unwind-protect */
  scm__rc.d1786[375] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[125])),TRUE); /* args */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[431]), scm__rc.d1786[129]);
  SCM_SET_CDR(SCM_OBJ(&scm__rc.d1787[431]), scm__rc.d1786[375]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[432]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[435]), scm__rc.d1786[2]);
  scm__rc.d1786[376] = Scm_MakeExtendedPair(scm__rc.d1786[366], SCM_OBJ(&scm__rc.d1787[432]), SCM_OBJ(&scm__rc.d1787[436]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[437]), scm__rc.d1786[376]);
  scm__rc.d1786[377] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[6])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[6]))->name = scm__rc.d1786[366];/* with-port-locking */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[6]))->debugInfo = scm__rc.d1786[377];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[108]))[7] = SCM_WORD(scm__rc.d1786[372]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[108]))[11] = SCM_WORD(scm__rc.d1786[373]);
  scm__rc.d1786[378] = Scm_MakeIdentifier(scm__rc.d1786[366], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#with-port-locking */
  scm__rc.d1786[379] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[7])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[7]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[7]))->debugInfo = scm__rc.d1786[379];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[121]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[121]))[6] = SCM_WORD(scm__rc.d1786[366]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[121]))[13] = SCM_WORD(scm__rc.d1786[378]);
  scm__rc.d1786[380] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[126])),TRUE); /* %with-2pass-setup */
  scm__rc.d1786[381] = Scm_MakeIdentifier(scm__rc.d1786[346], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#%port-write-state */
  scm__rc.d1786[383] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[55])),TRUE); /* error */
  scm__rc.d1786[382] = Scm_MakeIdentifier(scm__rc.d1786[383], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#error */
  scm__rc.d1786[385] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[128])),TRUE); /* <write-state> */
  scm__rc.d1786[384] = Scm_MakeIdentifier(scm__rc.d1786[385], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#<write-state> */
  scm__rc.d1786[386] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[129]))); /* :shared-table */
  scm__rc.d1786[387] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[130])),TRUE); /* eq? */
  scm__rc.d1786[389] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[131])),TRUE); /* make-hash-table */
  scm__rc.d1786[388] = Scm_MakeIdentifier(scm__rc.d1786[389], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#make-hash-table */
  scm__rc.d1786[391] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[132])),TRUE); /* make */
  scm__rc.d1786[390] = Scm_MakeIdentifier(scm__rc.d1786[391], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#make */
  scm__rc.d1786[392] = Scm_MakeIdentifier(scm__rc.d1786[342], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#%port-walking? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[438]), scm__rc.d1786[380]);
  scm__rc.d1786[393] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[8])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[8]))->debugInfo = scm__rc.d1786[393];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]))[4] = SCM_WORD(scm__rc.d1786[367]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]))[9] = SCM_WORD(scm__rc.d1786[381]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]))[18] = SCM_WORD(scm__rc.d1786[382]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]))[27] = SCM_WORD(scm__rc.d1786[384]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]))[29] = SCM_WORD(scm__rc.d1786[386]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]))[33] = SCM_WORD(scm__rc.d1786[387]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]))[35] = SCM_WORD(scm__rc.d1786[388]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]))[37] = SCM_WORD(scm__rc.d1786[390]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]))[39] = SCM_WORD(scm__rc.d1786[381]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]))[48] = SCM_WORD(scm__rc.d1786[392]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[136]))[61] = SCM_WORD(scm__rc.d1786[392]);
  scm__rc.d1786[394] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[9])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[9]))->debugInfo = scm__rc.d1786[394];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[204]))[5] = SCM_WORD(scm__rc.d1786[392]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[204]))[13] = SCM_WORD(scm__rc.d1786[381]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[204]))[18] = SCM_WORD(scm__rc.d1786[370]);
  scm__rc.d1786[395] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[133])),TRUE); /* walker */
  scm__rc.d1786[396] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[134])),TRUE); /* emitter */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[443]), scm__rc.d1786[396]);
  SCM_SET_CDR(SCM_OBJ(&scm__rc.d1787[443]), scm__rc.d1786[375]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[444]), scm__rc.d1786[395]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[445]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[448]), scm__rc.d1786[2]);
  scm__rc.d1786[397] = Scm_MakeExtendedPair(scm__rc.d1786[380], SCM_OBJ(&scm__rc.d1787[445]), SCM_OBJ(&scm__rc.d1787[449]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[450]), scm__rc.d1786[397]);
  scm__rc.d1786[398] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[10])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[10]))->name = scm__rc.d1786[380];/* %with-2pass-setup */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[10]))->debugInfo = scm__rc.d1786[398];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[224]))[7] = SCM_WORD(scm__rc.d1786[372]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[224]))[11] = SCM_WORD(scm__rc.d1786[373]);
  scm__rc.d1786[399] = Scm_MakeIdentifier(scm__rc.d1786[380], SCM_MODULE(Scm_GaucheInternalModule()), SCM_NIL); /* gauche.internal#%with-2pass-setup */
  scm__rc.d1786[400] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[11])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[11]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[11]))->debugInfo = scm__rc.d1786[400];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[237]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[237]))[6] = SCM_WORD(scm__rc.d1786[380]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[237]))[13] = SCM_WORD(scm__rc.d1786[399]);
  scm__rc.d1786[401] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[135])),TRUE); /* port-column */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[453]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[454]), scm__rc.d1786[401]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[455]), scm__rc.d1786[159]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[456]), scm__rc.d1786[3]);
  scm__rc.d1786[402] = Scm_MakeExtendedPair(scm__rc.d1786[401], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[458]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[403]))[3] = scm__rc.d1786[159];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[403]))[4] = scm__rc.d1786[23];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[403]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[403]))[6] = scm__rc.d1786[59];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("port-column")), SCM_OBJ(&libioport_column__STUB), 0);
  libioport_column__STUB.common.info = scm__rc.d1786[402];
  libioport_column__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[403]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())));
  scm__rc.d1786[410] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[136])),TRUE); /* read */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[459]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[462]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[463]), scm__rc.d1786[410]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[464]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[465]), scm__rc.d1786[3]);
  scm__rc.d1786[411] = Scm_MakeExtendedPair(scm__rc.d1786[410], SCM_OBJ(&scm__rc.d1787[459]), SCM_OBJ(&scm__rc.d1787[467]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[412]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[412]))[4] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[412]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[412]))[6] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("read")), SCM_OBJ(&libioread__STUB), 0);
  libioread__STUB.common.info = scm__rc.d1786[411];
  libioread__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[412]);
  scm__rc.d1786[419] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[137])),TRUE); /* read-char */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[470]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[471]), scm__rc.d1786[419]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[472]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[473]), scm__rc.d1786[3]);
  scm__rc.d1786[420] = Scm_MakeExtendedPair(scm__rc.d1786[419], SCM_OBJ(&scm__rc.d1787[459]), SCM_OBJ(&scm__rc.d1787[475]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("read-char")), SCM_OBJ(&libioread_char__STUB), SCM_BINDING_INLINABLE);
  libioread_char__STUB.common.info = scm__rc.d1786[420];
  libioread_char__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[412]);
  scm__rc.d1786[421] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[138])),TRUE); /* peek-char */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[478]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[479]), scm__rc.d1786[421]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[480]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[481]), scm__rc.d1786[3]);
  scm__rc.d1786[422] = Scm_MakeExtendedPair(scm__rc.d1786[421], SCM_OBJ(&scm__rc.d1787[459]), SCM_OBJ(&scm__rc.d1787[483]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("peek-char")), SCM_OBJ(&libiopeek_char__STUB), SCM_BINDING_INLINABLE);
  libiopeek_char__STUB.common.info = scm__rc.d1786[422];
  libiopeek_char__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[412]);
  scm__rc.d1786[423] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[139])),TRUE); /* eof-object? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[486]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[487]), scm__rc.d1786[423]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[488]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[489]), scm__rc.d1786[3]);
  scm__rc.d1786[424] = Scm_MakeExtendedPair(scm__rc.d1786[423], SCM_OBJ(&scm__rc.d1787[1]), SCM_OBJ(&scm__rc.d1787[491]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("eof-object?")), SCM_OBJ(&libioeof_objectP__STUB), SCM_BINDING_INLINABLE);
  libioeof_objectP__STUB.common.info = scm__rc.d1786[424];
  libioeof_objectP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[9]);
  scm__rc.d1786[425] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[140])),TRUE); /* char-ready? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[494]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[495]), scm__rc.d1786[425]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[496]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[497]), scm__rc.d1786[3]);
  scm__rc.d1786[426] = Scm_MakeExtendedPair(scm__rc.d1786[425], SCM_OBJ(&scm__rc.d1787[459]), SCM_OBJ(&scm__rc.d1787[499]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[427]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[427]))[4] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[427]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[427]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("char-ready?")), SCM_OBJ(&libiochar_readyP__STUB), 0);
  libiochar_readyP__STUB.common.info = scm__rc.d1786[426];
  libiochar_readyP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[427]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[434] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[141])),TRUE); /* eof-object */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[500]), scm__rc.d1786[434]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[501]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[502]), scm__rc.d1786[3]);
  scm__rc.d1786[435] = Scm_MakeExtendedPair(scm__rc.d1786[434], SCM_NIL, SCM_OBJ(&scm__rc.d1787[503]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[436]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[436]))[4] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[436]))[5] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("eof-object")), SCM_OBJ(&libioeof_object__STUB), SCM_BINDING_INLINABLE);
  libioeof_object__STUB.common.info = scm__rc.d1786[435];
  libioeof_object__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[436]);
  scm__rc.d1786[442] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[142])),TRUE); /* byte-ready? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[506]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[507]), scm__rc.d1786[442]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[508]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[509]), scm__rc.d1786[3]);
  scm__rc.d1786[443] = Scm_MakeExtendedPair(scm__rc.d1786[442], SCM_OBJ(&scm__rc.d1787[459]), SCM_OBJ(&scm__rc.d1787[511]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[444]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[444]))[4] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[444]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[444]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("byte-ready?")), SCM_OBJ(&libiobyte_readyP__STUB), 0);
  libiobyte_readyP__STUB.common.info = scm__rc.d1786[443];
  libiobyte_readyP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[444]);
  scm__rc.d1786[451] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[143])),TRUE); /* u8-ready? */
  scm__rc.d1786[452] = Scm_MakeIdentifier(scm__rc.d1786[442], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#byte-ready? */
  scm__rc.d1786[453] = Scm_MakeIdentifier(scm__rc.d1786[451], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#u8-ready? */
  scm__rc.d1786[454] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[12])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[12]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[12]))->debugInfo = scm__rc.d1786[454];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[252]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[252]))[6] = SCM_WORD(scm__rc.d1786[451]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[252]))[10] = SCM_WORD(scm__rc.d1786[452]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[252]))[13] = SCM_WORD(scm__rc.d1786[453]);
  scm__rc.d1786[455] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[144])),TRUE); /* read-byte */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[514]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[515]), scm__rc.d1786[455]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[516]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[517]), scm__rc.d1786[3]);
  scm__rc.d1786[456] = Scm_MakeExtendedPair(scm__rc.d1786[455], SCM_OBJ(&scm__rc.d1787[459]), SCM_OBJ(&scm__rc.d1787[519]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("read-byte")), SCM_OBJ(&libioread_byte__STUB), 0);
  libioread_byte__STUB.common.info = scm__rc.d1786[456];
  libioread_byte__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[36]);
  scm__rc.d1786[457] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[145])),TRUE); /* read-u8 */
  scm__rc.d1786[458] = Scm_MakeIdentifier(scm__rc.d1786[455], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#read-byte */
  scm__rc.d1786[459] = Scm_MakeIdentifier(scm__rc.d1786[457], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#read-u8 */
  scm__rc.d1786[460] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[13])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[13]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[13]))->debugInfo = scm__rc.d1786[460];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[267]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[267]))[6] = SCM_WORD(scm__rc.d1786[457]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[267]))[10] = SCM_WORD(scm__rc.d1786[458]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[267]))[13] = SCM_WORD(scm__rc.d1786[459]);
  scm__rc.d1786[461] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[146])),TRUE); /* peek-byte */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[522]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[523]), scm__rc.d1786[461]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[524]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[525]), scm__rc.d1786[3]);
  scm__rc.d1786[462] = Scm_MakeExtendedPair(scm__rc.d1786[461], SCM_OBJ(&scm__rc.d1787[459]), SCM_OBJ(&scm__rc.d1787[527]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("peek-byte")), SCM_OBJ(&libiopeek_byte__STUB), 0);
  libiopeek_byte__STUB.common.info = scm__rc.d1786[462];
  libiopeek_byte__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[36]);
  scm__rc.d1786[463] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[147])),TRUE); /* peek-u8 */
  scm__rc.d1786[464] = Scm_MakeIdentifier(scm__rc.d1786[461], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#peek-byte */
  scm__rc.d1786[465] = Scm_MakeIdentifier(scm__rc.d1786[463], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#peek-u8 */
  scm__rc.d1786[466] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[14])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[14]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[14]))->debugInfo = scm__rc.d1786[466];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[282]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[282]))[6] = SCM_WORD(scm__rc.d1786[463]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[282]))[10] = SCM_WORD(scm__rc.d1786[464]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[282]))[13] = SCM_WORD(scm__rc.d1786[465]);
  scm__rc.d1786[467] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[148])),TRUE); /* read-line */
  scm__rc.d1786[468] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[149])),TRUE); /* allowbytestr */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[528]), scm__rc.d1786[468]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[529]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[530]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[533]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[534]), scm__rc.d1786[467]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[535]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[536]), scm__rc.d1786[3]);
  scm__rc.d1786[469] = Scm_MakeExtendedPair(scm__rc.d1786[467], SCM_OBJ(&scm__rc.d1787[530]), SCM_OBJ(&scm__rc.d1787[538]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("read-line")), SCM_OBJ(&libioread_line__STUB), 0);
  libioread_line__STUB.common.info = scm__rc.d1786[469];
  libioread_line__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[36]);
  scm__rc.d1786[470] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[150])),TRUE); /* read-string */
  scm__rc.d1786[471] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[152])),TRUE); /* lambda */
  scm__rc.d1786[472] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[153])),TRUE); /* n */
  scm__rc.d1786[473] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[154])),TRUE); /* current-input-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[539]), scm__rc.d1786[473]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[541]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[543]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[544]), scm__rc.d1786[472]);
  scm__rc.d1786[474] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[155])),TRUE); /* define */
  scm__rc.d1786[475] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[156])),TRUE); /* o */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[546]), scm__rc.d1786[254]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[547]), scm__rc.d1786[266]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[549]), scm__rc.d1786[475]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[550]), scm__rc.d1786[474]);
  scm__rc.d1786[476] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[157])),TRUE); /* let */
  scm__rc.d1786[477] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[158])),TRUE); /* loop */
  scm__rc.d1786[478] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[159])),TRUE); /* i */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[552]), scm__rc.d1786[478]);
  scm__rc.d1786[479] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[160])),TRUE); /* if */
  scm__rc.d1786[480] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[161])),TRUE); /* >= */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[554]), scm__rc.d1786[472]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[555]), scm__rc.d1786[478]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[556]), scm__rc.d1786[480]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[557]), scm__rc.d1786[475]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[558]), scm__rc.d1786[268]);
  scm__rc.d1786[481] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[162])),TRUE); /* let1 */
  scm__rc.d1786[482] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[163])),TRUE); /* c */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[559]), scm__rc.d1786[419]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[560]), scm__rc.d1786[482]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[561]), scm__rc.d1786[423]);
  scm__rc.d1786[483] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[164])),TRUE); /* = */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[562]), scm__rc.d1786[483]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[566]), scm__rc.d1786[479]);
  scm__rc.d1786[484] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[165])),TRUE); /* begin */
  scm__rc.d1786[485] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[166])),TRUE); /* write-char */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[567]), scm__rc.d1786[482]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[568]), scm__rc.d1786[485]);
  scm__rc.d1786[486] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[167])),TRUE); /* + */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[570]), scm__rc.d1786[478]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[571]), scm__rc.d1786[486]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[573]), scm__rc.d1786[477]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[576]), scm__rc.d1786[484]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[580]), scm__rc.d1786[479]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[583]), scm__rc.d1786[482]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[584]), scm__rc.d1786[481]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[588]), scm__rc.d1786[479]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[591]), scm__rc.d1786[477]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[592]), scm__rc.d1786[476]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[596]), scm__rc.d1786[471]);
  scm__rc.d1786[487] = Scm_MakeIdentifier(scm__rc.d1786[383], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#error */
  scm__rc.d1786[488] = Scm_MakeIdentifier(scm__rc.d1786[266], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#open-output-string */
  scm__rc.d1786[489] = Scm_MakeIdentifier(scm__rc.d1786[268], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#get-output-string */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[600]), scm__rc.d1786[2]);
  scm__rc.d1786[490] = Scm_MakeExtendedPair(scm__rc.d1786[470], SCM_OBJ(&scm__rc.d1787[544]), SCM_OBJ(&scm__rc.d1787[601]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[602]), scm__rc.d1786[490]);
  scm__rc.d1786[491] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[15])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[15]))->name = scm__rc.d1786[470];/* read-string */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[15]))->debugInfo = scm__rc.d1786[491];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]))[28] = SCM_WORD(scm__rc.d1786[487]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]))[34] = SCM_WORD(scm__rc.d1786[254]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]))[38] = SCM_WORD(scm__rc.d1786[488]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]))[47] = SCM_WORD(scm__rc.d1786[489]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[297]))[63] = SCM_WORD(scm__rc.d1786[489]);
  scm__rc.d1786[492] = Scm_MakeIdentifier(scm__rc.d1786[470], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#read-string */
  scm__rc.d1786[493] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[16])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[16]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[16]))->debugInfo = scm__rc.d1786[493];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[370]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[370]))[6] = SCM_WORD(scm__rc.d1786[470]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[370]))[13] = SCM_WORD(scm__rc.d1786[492]);
  scm__rc.d1786[494] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[168])),TRUE); /* write-string */
  scm__rc.d1786[495] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[169])),TRUE); /* current-output-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[603]), scm__rc.d1786[495]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[605]), scm__rc.d1786[48]);
  scm__rc.d1786[496] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[170])),TRUE); /* start */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[606]), scm__rc.d1786[496]);
  scm__rc.d1786[497] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[171])),TRUE); /* end */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[608]), scm__rc.d1786[497]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[612]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[613]), scm__rc.d1786[130]);
  scm__rc.d1786[498] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[172])),TRUE); /* display */
  scm__rc.d1786[499] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[173])),TRUE); /* opt-substring */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[614]), scm__rc.d1786[497]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[615]), scm__rc.d1786[496]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[616]), scm__rc.d1786[130]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[617]), scm__rc.d1786[499]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[619]), scm__rc.d1786[498]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[622]), scm__rc.d1786[471]);
  scm__rc.d1786[500] = Scm_MakeIdentifier(scm__rc.d1786[499], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#opt-substring */
  scm__rc.d1786[501] = Scm_MakeIdentifier(scm__rc.d1786[498], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#display */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[625]), scm__rc.d1786[2]);
  scm__rc.d1786[502] = Scm_MakeExtendedPair(scm__rc.d1786[494], SCM_OBJ(&scm__rc.d1787[613]), SCM_OBJ(&scm__rc.d1787[626]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[627]), scm__rc.d1786[502]);
  scm__rc.d1786[503] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[17])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[17]))->name = scm__rc.d1786[494];/* write-string */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[17]))->debugInfo = scm__rc.d1786[503];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]))[60] = SCM_WORD(scm__rc.d1786[487]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]))[67] = SCM_WORD(scm__rc.d1786[500]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[385]))[71] = SCM_WORD(scm__rc.d1786[501]);
  scm__rc.d1786[504] = Scm_MakeIdentifier(scm__rc.d1786[494], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#write-string */
  scm__rc.d1786[505] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[18])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[18]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[18]))->debugInfo = scm__rc.d1786[505];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[458]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[458]))[6] = SCM_WORD(scm__rc.d1786[494]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[458]))[13] = SCM_WORD(scm__rc.d1786[504]);
  scm__rc.d1786[506] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[174])),TRUE); /* consume-trailing-whitespaces */
  scm__rc.d1786[507] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[175])),TRUE); /* when */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[628]), scm__rc.d1786[442]);
  scm__rc.d1786[508] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[176])),TRUE); /* b */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[629]), scm__rc.d1786[461]);
  scm__rc.d1786[509] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[177])),TRUE); /* cond */
  scm__rc.d1786[510] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[178])),TRUE); /* memv */
  scm__rc.d1786[511] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[179])),TRUE); /* quote */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[633]), scm__rc.d1786[511]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[635]), scm__rc.d1786[508]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[636]), scm__rc.d1786[510]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[637]), scm__rc.d1786[455]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[638]), scm__rc.d1786[477]);
  scm__rc.d1786[512] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[180])),TRUE); /* eqv? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[643]), scm__rc.d1786[508]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[644]), scm__rc.d1786[512]);
  scm__rc.d1786[513] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[181])),TRUE); /* and */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[647]), scm__rc.d1786[512]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[650]), scm__rc.d1786[513]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[653]), scm__rc.d1786[507]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[657]), scm__rc.d1786[508]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[658]), scm__rc.d1786[512]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[663]), scm__rc.d1786[509]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[666]), scm__rc.d1786[508]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[667]), scm__rc.d1786[481]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[670]), scm__rc.d1786[507]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[673]), scm__rc.d1786[477]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[674]), scm__rc.d1786[476]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[677]), scm__rc.d1786[471]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[680]), scm__rc.d1786[2]);
  scm__rc.d1786[514] = Scm_MakeExtendedPair(scm__rc.d1786[506], SCM_OBJ(&scm__rc.d1787[543]), SCM_OBJ(&scm__rc.d1787[681]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[682]), scm__rc.d1786[514]);
  scm__rc.d1786[515] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[19])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[19]))->name = scm__rc.d1786[506];/* consume-trailing-whitespaces */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[19]))->debugInfo = scm__rc.d1786[515];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]))[28] = SCM_WORD(scm__rc.d1786[487]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]))[33] = SCM_WORD(scm__rc.d1786[452]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]))[40] = SCM_WORD(scm__rc.d1786[464]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]))[52] = SCM_WORD(scm__rc.d1786[458]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]))[64] = SCM_WORD(scm__rc.d1786[458]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]))[69] = SCM_WORD(scm__rc.d1786[452]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]))[76] = SCM_WORD(scm__rc.d1786[464]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]))[82] = SCM_WORD(scm__rc.d1786[458]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[473]))[92] = SCM_WORD(scm__rc.d1786[458]);
  scm__rc.d1786[516] = Scm_MakeIdentifier(scm__rc.d1786[506], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#consume-trailing-whitespaces */
  scm__rc.d1786[517] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[20])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[20]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[20]))->debugInfo = scm__rc.d1786[517];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[570]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[570]))[6] = SCM_WORD(scm__rc.d1786[506]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[570]))[13] = SCM_WORD(scm__rc.d1786[516]);
  scm__rc.d1786[518] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[182])),TRUE); /* char-word-constituent? */
  scm__rc.d1786[519] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[183])),TRUE); /* ch */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[683]), scm__rc.d1786[519]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[686]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[687]), scm__rc.d1786[518]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[688]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[689]), scm__rc.d1786[3]);
  scm__rc.d1786[520] = Scm_MakeExtendedPair(scm__rc.d1786[518], SCM_OBJ(&scm__rc.d1787[683]), SCM_OBJ(&scm__rc.d1787[691]));
  scm__rc.d1786[521] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[184])),TRUE); /* <char> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[522]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[522]))[4] = scm__rc.d1786[521];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[522]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[522]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("char-word-constituent?")), SCM_OBJ(&libiochar_word_constituentP__STUB), 0);
  libiochar_word_constituentP__STUB.common.info = scm__rc.d1786[520];
  libiochar_word_constituentP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[522]);
  scm__rc.d1786[529] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[185])),TRUE); /* read-block */
  scm__rc.d1786[530] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[186])),TRUE); /* bytes */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[692]), scm__rc.d1786[530]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[695]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[696]), scm__rc.d1786[529]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[697]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[698]), scm__rc.d1786[3]);
  scm__rc.d1786[531] = Scm_MakeExtendedPair(scm__rc.d1786[529], SCM_OBJ(&scm__rc.d1787[692]), SCM_OBJ(&scm__rc.d1787[700]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("read-block")), SCM_OBJ(&libioread_block__STUB), 0);
  libioread_block__STUB.common.info = scm__rc.d1786[531];
  libioread_block__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[228]);
  scm__rc.d1786[532] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[187])),TRUE); /* read-list */
  scm__rc.d1786[533] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[188])),TRUE); /* closer */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[701]), scm__rc.d1786[533]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[704]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[705]), scm__rc.d1786[532]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[706]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[707]), scm__rc.d1786[3]);
  scm__rc.d1786[534] = Scm_MakeExtendedPair(scm__rc.d1786[532], SCM_OBJ(&scm__rc.d1787[701]), SCM_OBJ(&scm__rc.d1787[709]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[535]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[535]))[4] = scm__rc.d1786[521];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[535]))[5] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[535]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[535]))[7] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("read-list")), SCM_OBJ(&libioread_list__STUB), 0);
  libioread_list__STUB.common.info = scm__rc.d1786[534];
  libioread_list__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[535]);
  scm__rc.d1786[543] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[189])),TRUE); /* port->byte-string */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[712]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[713]), scm__rc.d1786[543]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[714]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[715]), scm__rc.d1786[3]);
  scm__rc.d1786[544] = Scm_MakeExtendedPair(scm__rc.d1786[543], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[717]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("port->byte-string")), SCM_OBJ(&libioport_TObyte_string__STUB), 0);
  libioport_TObyte_string__STUB.common.info = scm__rc.d1786[544];
  libioport_TObyte_string__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[281]);
  scm__rc.d1786[545] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[190])),TRUE); /* port->string */
  scm__rc.d1786[546] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[191]))); /* :unit */
  scm__rc.d1786[547] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[192])),TRUE); /* byte */
  scm__rc.d1786[549] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[193])),TRUE); /* copy-port */
  scm__rc.d1786[548] = Scm_MakeIdentifier(scm__rc.d1786[549], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#copy-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[720]), scm__rc.d1786[2]);
  scm__rc.d1786[550] = Scm_MakeExtendedPair(scm__rc.d1786[545], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[721]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[722]), scm__rc.d1786[550]);
  scm__rc.d1786[551] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[21])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[21]))->name = scm__rc.d1786[545];/* port->string */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[21]))->debugInfo = scm__rc.d1786[551];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[585]))[3] = SCM_WORD(scm__rc.d1786[254]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[585]))[7] = SCM_WORD(scm__rc.d1786[488]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[585]))[14] = SCM_WORD(scm__rc.d1786[546]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[585]))[16] = SCM_WORD(scm__rc.d1786[547]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[585]))[18] = SCM_WORD(scm__rc.d1786[548]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[585]))[21] = SCM_WORD(scm__rc.d1786[489]);
  scm__rc.d1786[552] = Scm_MakeIdentifier(scm__rc.d1786[545], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#port->string */
  scm__rc.d1786[553] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[22])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[22]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[22]))->debugInfo = scm__rc.d1786[553];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[608]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[608]))[6] = SCM_WORD(scm__rc.d1786[545]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[608]))[13] = SCM_WORD(scm__rc.d1786[552]);
  scm__rc.d1786[554] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[194])),TRUE); /* port->list */
  scm__rc.d1786[556] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[195])),TRUE); /* reverse! */
  scm__rc.d1786[555] = Scm_MakeIdentifier(scm__rc.d1786[556], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#reverse! */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[723]), scm__rc.d1786[554]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[726]), scm__rc.d1786[2]);
  scm__rc.d1786[557] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[723]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[727]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[728]), scm__rc.d1786[557]);
  scm__rc.d1786[558] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[23])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[23]))->debugInfo = scm__rc.d1786[558];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[623]))[14] = SCM_WORD(scm__rc.d1786[555]);
  scm__rc.d1786[559] = Scm_MakeIdentifier(scm__rc.d1786[366], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#with-port-locking */
  scm__rc.d1786[560] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[196])),TRUE); /* reader */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[729]), scm__rc.d1786[560]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[732]), scm__rc.d1786[2]);
  scm__rc.d1786[561] = Scm_MakeExtendedPair(scm__rc.d1786[554], SCM_OBJ(&scm__rc.d1787[729]), SCM_OBJ(&scm__rc.d1787[733]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[734]), scm__rc.d1786[561]);
  scm__rc.d1786[562] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[24])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[24]))->name = scm__rc.d1786[554];/* port->list */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[24]))->debugInfo = scm__rc.d1786[562];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[652]))[4] = SCM_WORD(scm__rc.d1786[559]);
  scm__rc.d1786[563] = Scm_MakeIdentifier(scm__rc.d1786[554], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#port->list */
  scm__rc.d1786[564] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[25])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[25]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[25]))->debugInfo = scm__rc.d1786[564];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[658]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[658]))[6] = SCM_WORD(scm__rc.d1786[554]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[658]))[13] = SCM_WORD(scm__rc.d1786[563]);
  scm__rc.d1786[565] = Scm_MakeIdentifier(scm__rc.d1786[467], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#read-line */
  scm__rc.d1786[566] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[197])),TRUE); /* port->string-list */
  scm__rc.d1786[567] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[198])),FALSE); /* G1815 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[735]), scm__rc.d1786[567]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[736]), scm__rc.d1786[566]);
  scm__rc.d1786[568] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[199])),FALSE); /* G1814 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[737]), scm__rc.d1786[568]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[740]), scm__rc.d1786[2]);
  scm__rc.d1786[569] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[736]), SCM_OBJ(&scm__rc.d1787[737]), SCM_OBJ(&scm__rc.d1787[741]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[742]), scm__rc.d1786[569]);
  scm__rc.d1786[570] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[26])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[26]))->debugInfo = scm__rc.d1786[570];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[673]))[4] = SCM_WORD(scm__rc.d1786[565]);
  scm__rc.d1786[571] = Scm_MakeExtendedPair(scm__rc.d1786[566], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[741]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[744]), scm__rc.d1786[571]);
  scm__rc.d1786[572] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[27])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[27]))->name = scm__rc.d1786[566];/* port->string-list */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[27]))->debugInfo = scm__rc.d1786[572];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[679]))[3] = SCM_WORD(scm__rc.d1786[563]);
  scm__rc.d1786[573] = Scm_MakeIdentifier(scm__rc.d1786[566], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#port->string-list */
  scm__rc.d1786[574] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[28])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[28]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[28]))->debugInfo = scm__rc.d1786[574];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[684]))[5] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[684]))[8] = SCM_WORD(scm__rc.d1786[566]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[684]))[15] = SCM_WORD(scm__rc.d1786[573]);
  scm__rc.d1786[575] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[200])),TRUE); /* port->sexp-list */
  scm__rc.d1786[576] = Scm_MakeIdentifier(scm__rc.d1786[410], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#read */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[747]), scm__rc.d1786[2]);
  scm__rc.d1786[577] = Scm_MakeExtendedPair(scm__rc.d1786[575], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[748]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[749]), scm__rc.d1786[577]);
  scm__rc.d1786[578] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[29])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[29]))->name = scm__rc.d1786[575];/* port->sexp-list */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[29]))->debugInfo = scm__rc.d1786[578];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[701]))[1] = SCM_WORD(scm__rc.d1786[576]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[701]))[4] = SCM_WORD(scm__rc.d1786[563]);
  scm__rc.d1786[579] = Scm_MakeIdentifier(scm__rc.d1786[575], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#port->sexp-list */
  scm__rc.d1786[580] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[30])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[30]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[30]))->debugInfo = scm__rc.d1786[580];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[707]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[707]))[6] = SCM_WORD(scm__rc.d1786[575]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[707]))[13] = SCM_WORD(scm__rc.d1786[579]);
  scm__rc.d1786[581] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[201])),TRUE); /* reader-lexical-mode */
  scm__rc.d1786[582] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[202])),TRUE); /* k */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[750]), scm__rc.d1786[582]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[751]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[754]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[755]), scm__rc.d1786[581]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[756]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[757]), scm__rc.d1786[3]);
  scm__rc.d1786[583] = Scm_MakeExtendedPair(scm__rc.d1786[581], SCM_OBJ(&scm__rc.d1787[751]), SCM_OBJ(&scm__rc.d1787[759]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("reader-lexical-mode")), SCM_OBJ(&libioreader_lexical_mode__STUB), 0);
  libioreader_lexical_mode__STUB.common.info = scm__rc.d1786[583];
  libioreader_lexical_mode__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[36]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
  scm__rc.d1786[584] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[203])),TRUE); /* %port-ungotten-chars */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[762]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[763]), scm__rc.d1786[584]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[764]), scm__rc.d1786[159]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[765]), scm__rc.d1786[3]);
  scm__rc.d1786[585] = Scm_MakeExtendedPair(scm__rc.d1786[584], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[767]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[586]))[3] = scm__rc.d1786[159];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[586]))[4] = scm__rc.d1786[139];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[586]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[586]))[6] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("%port-ungotten-chars")), SCM_OBJ(&libio_25port_ungotten_chars__STUB), 0);
  libio_25port_ungotten_chars__STUB.common.info = scm__rc.d1786[585];
  libio_25port_ungotten_chars__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[586]);
  scm__rc.d1786[593] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[204])),TRUE); /* %port-ungotten-bytes */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[770]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[771]), scm__rc.d1786[593]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[772]), scm__rc.d1786[159]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[773]), scm__rc.d1786[3]);
  scm__rc.d1786[594] = Scm_MakeExtendedPair(scm__rc.d1786[593], SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[775]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("%port-ungotten-bytes")), SCM_OBJ(&libio_25port_ungotten_bytes__STUB), 0);
  libio_25port_ungotten_bytes__STUB.common.info = scm__rc.d1786[594];
  libio_25port_ungotten_bytes__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[586]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[595] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[205])),TRUE); /* define-reader-ctor */
  scm__rc.d1786[596] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[206])),TRUE); /* symbol */
  scm__rc.d1786[597] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[207])),TRUE); /* finisher */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[776]), scm__rc.d1786[597]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[777]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[778]), scm__rc.d1786[129]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[779]), scm__rc.d1786[596]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[782]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[783]), scm__rc.d1786[595]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[784]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[785]), scm__rc.d1786[3]);
  scm__rc.d1786[598] = Scm_MakeExtendedPair(scm__rc.d1786[595], SCM_OBJ(&scm__rc.d1787[779]), SCM_OBJ(&scm__rc.d1787[787]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[599]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[599]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[599]))[5] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[599]))[6] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[599]))[7] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[599]))[8] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("define-reader-ctor")), SCM_OBJ(&libiodefine_reader_ctor__STUB), 0);
  libiodefine_reader_ctor__STUB.common.info = scm__rc.d1786[598];
  libiodefine_reader_ctor__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[599]);
  scm__rc.d1786[608] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[208])),TRUE); /* %get-reader-ctor */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[788]), scm__rc.d1786[596]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[791]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[792]), scm__rc.d1786[608]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[793]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[794]), scm__rc.d1786[3]);
  scm__rc.d1786[609] = Scm_MakeExtendedPair(scm__rc.d1786[608], SCM_OBJ(&scm__rc.d1787[788]), SCM_OBJ(&scm__rc.d1787[796]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[610]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[610]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[610]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[610]))[6] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("%get-reader-ctor")), SCM_OBJ(&libio_25get_reader_ctor__STUB), 0);
  libio_25get_reader_ctor__STUB.common.info = scm__rc.d1786[609];
  libio_25get_reader_ctor__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[610]);
  scm__rc.d1786[617] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[209])),TRUE); /* define-reader-directive */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[797]), scm__rc.d1786[129]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[798]), scm__rc.d1786[596]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[801]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[802]), scm__rc.d1786[617]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[803]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[804]), scm__rc.d1786[3]);
  scm__rc.d1786[618] = Scm_MakeExtendedPair(scm__rc.d1786[617], SCM_OBJ(&scm__rc.d1787[798]), SCM_OBJ(&scm__rc.d1787[806]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[619]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[619]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[619]))[5] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[619]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[619]))[7] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("define-reader-directive")), SCM_OBJ(&libiodefine_reader_directive__STUB), 0);
  libiodefine_reader_directive__STUB.common.info = scm__rc.d1786[618];
  libiodefine_reader_directive__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[619]);
  scm__rc.d1786[627] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[210])),TRUE); /* current-read-context */
  scm__rc.d1786[628] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[211])),TRUE); /* ctx */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[807]), scm__rc.d1786[628]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[808]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[811]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[812]), scm__rc.d1786[627]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[813]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[814]), scm__rc.d1786[3]);
  scm__rc.d1786[629] = Scm_MakeExtendedPair(scm__rc.d1786[627], SCM_OBJ(&scm__rc.d1787[808]), SCM_OBJ(&scm__rc.d1787[816]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("current-read-context")), SCM_OBJ(&libiocurrent_read_context__STUB), 0);
  libiocurrent_read_context__STUB.common.info = scm__rc.d1786[629];
  libiocurrent_read_context__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[36]);
  scm__rc.d1786[630] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[212])),TRUE); /* read-reference? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[819]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[820]), scm__rc.d1786[630]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[821]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[822]), scm__rc.d1786[3]);
  scm__rc.d1786[631] = Scm_MakeExtendedPair(scm__rc.d1786[630], SCM_OBJ(&scm__rc.d1787[1]), SCM_OBJ(&scm__rc.d1787[824]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[632]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[632]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[632]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[632]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("read-reference?")), SCM_OBJ(&libioread_referenceP__STUB), 0);
  libioread_referenceP__STUB.common.info = scm__rc.d1786[631];
  libioread_referenceP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[632]);
  scm__rc.d1786[639] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[213])),TRUE); /* read-reference-has-value? */
  scm__rc.d1786[640] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[214])),TRUE); /* ref */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[825]), scm__rc.d1786[640]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[828]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[829]), scm__rc.d1786[639]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[830]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[831]), scm__rc.d1786[3]);
  scm__rc.d1786[641] = Scm_MakeExtendedPair(scm__rc.d1786[639], SCM_OBJ(&scm__rc.d1787[825]), SCM_OBJ(&scm__rc.d1787[833]));
  scm__rc.d1786[642] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[215])),TRUE); /* <read-reference> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[643]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[643]))[4] = scm__rc.d1786[642];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[643]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[643]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("read-reference-has-value?")), SCM_OBJ(&libioread_reference_has_valueP__STUB), 0);
  libioread_reference_has_valueP__STUB.common.info = scm__rc.d1786[641];
  libioread_reference_has_valueP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[643]);
  scm__rc.d1786[650] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[216])),TRUE); /* read-reference-value */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[836]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[837]), scm__rc.d1786[650]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[838]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[839]), scm__rc.d1786[3]);
  scm__rc.d1786[651] = Scm_MakeExtendedPair(scm__rc.d1786[650], SCM_OBJ(&scm__rc.d1787[825]), SCM_OBJ(&scm__rc.d1787[841]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[652]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[652]))[4] = scm__rc.d1786[642];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[652]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[652]))[6] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("read-reference-value")), SCM_OBJ(&libioread_reference_value__STUB), 0);
  libioread_reference_value__STUB.common.info = scm__rc.d1786[651];
  libioread_reference_value__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[652]);
  scm__rc.d1786[659] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[217])),TRUE); /* read-with-shared-structure */
  scm__rc.d1786[660] = Scm_MakeIdentifier(scm__rc.d1786[659], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#read-with-shared-structure */
  scm__rc.d1786[661] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[31])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[31]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[31]))->debugInfo = scm__rc.d1786[661];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[722]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[722]))[6] = SCM_WORD(scm__rc.d1786[659]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[722]))[10] = SCM_WORD(scm__rc.d1786[576]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[722]))[13] = SCM_WORD(scm__rc.d1786[660]);
  scm__rc.d1786[662] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[218])),TRUE); /* read/ss */
  scm__rc.d1786[663] = Scm_MakeIdentifier(scm__rc.d1786[662], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#read/ss */
  scm__rc.d1786[664] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[32])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[32]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[32]))->debugInfo = scm__rc.d1786[664];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[737]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[737]))[6] = SCM_WORD(scm__rc.d1786[662]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[737]))[10] = SCM_WORD(scm__rc.d1786[576]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[737]))[13] = SCM_WORD(scm__rc.d1786[663]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())));
  scm__rc.d1786[665] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[219])),TRUE); /* write */
  scm__rc.d1786[666] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[220])),TRUE); /* port-or-control-1 */
  scm__rc.d1786[667] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[221])),TRUE); /* port-or-control-2 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[842]), scm__rc.d1786[667]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[843]), scm__rc.d1786[666]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[844]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[845]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[848]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[849]), scm__rc.d1786[665]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[850]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[851]), scm__rc.d1786[3]);
  scm__rc.d1786[668] = Scm_MakeExtendedPair(scm__rc.d1786[665], SCM_OBJ(&scm__rc.d1787[845]), SCM_OBJ(&scm__rc.d1787[853]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[669]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[669]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[669]))[5] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[669]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[669]))[7] = scm__rc.d1786[82];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("write")), SCM_OBJ(&libiowrite__STUB), 0);
  libiowrite__STUB.common.info = scm__rc.d1786[668];
  libiowrite__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[669]);
  scm__rc.d1786[677] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[222])),TRUE); /* write-simple */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[854]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[857]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[858]), scm__rc.d1786[677]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[859]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[860]), scm__rc.d1786[3]);
  scm__rc.d1786[678] = Scm_MakeExtendedPair(scm__rc.d1786[677], SCM_OBJ(&scm__rc.d1787[854]), SCM_OBJ(&scm__rc.d1787[862]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("write-simple")), SCM_OBJ(&libiowrite_simple__STUB), 0);
  libiowrite_simple__STUB.common.info = scm__rc.d1786[678];
  libiowrite_simple__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[669]);
  scm__rc.d1786[679] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[223])),TRUE); /* write-shared */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[865]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[866]), scm__rc.d1786[679]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[867]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[868]), scm__rc.d1786[3]);
  scm__rc.d1786[680] = Scm_MakeExtendedPair(scm__rc.d1786[679], SCM_OBJ(&scm__rc.d1787[845]), SCM_OBJ(&scm__rc.d1787[870]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("write-shared")), SCM_OBJ(&libiowrite_shared__STUB), 0);
  libiowrite_shared__STUB.common.info = scm__rc.d1786[680];
  libiowrite_shared__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[669]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[873]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[874]), scm__rc.d1786[498]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[875]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[876]), scm__rc.d1786[3]);
  scm__rc.d1786[681] = Scm_MakeExtendedPair(scm__rc.d1786[498], SCM_OBJ(&scm__rc.d1787[845]), SCM_OBJ(&scm__rc.d1787[878]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("display")), SCM_OBJ(&libiodisplay__STUB), 0);
  libiodisplay__STUB.common.info = scm__rc.d1786[681];
  libiodisplay__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[669]);
  scm__rc.d1786[682] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[224])),TRUE); /* newline */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[881]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[882]), scm__rc.d1786[682]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[883]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[884]), scm__rc.d1786[3]);
  scm__rc.d1786[683] = Scm_MakeExtendedPair(scm__rc.d1786[682], SCM_OBJ(&scm__rc.d1787[459]), SCM_OBJ(&scm__rc.d1787[886]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[684]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[684]))[4] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[684]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[684]))[6] = scm__rc.d1786[82];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("newline")), SCM_OBJ(&libionewline__STUB), 0);
  libionewline__STUB.common.info = scm__rc.d1786[683];
  libionewline__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[684]);
  scm__rc.d1786[691] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[225])),TRUE); /* fresh-line */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[889]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[890]), scm__rc.d1786[691]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[891]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[892]), scm__rc.d1786[3]);
  scm__rc.d1786[692] = Scm_MakeExtendedPair(scm__rc.d1786[691], SCM_OBJ(&scm__rc.d1787[459]), SCM_OBJ(&scm__rc.d1787[894]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("fresh-line")), SCM_OBJ(&libiofresh_line__STUB), 0);
  libiofresh_line__STUB.common.info = scm__rc.d1786[692];
  libiofresh_line__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[427]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[895]), scm__rc.d1786[519]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[898]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[899]), scm__rc.d1786[485]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[900]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[901]), scm__rc.d1786[3]);
  scm__rc.d1786[693] = Scm_MakeExtendedPair(scm__rc.d1786[485], SCM_OBJ(&scm__rc.d1787[895]), SCM_OBJ(&scm__rc.d1787[903]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[694]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[694]))[4] = scm__rc.d1786[521];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[694]))[5] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[694]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[694]))[7] = scm__rc.d1786[82];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("write-char")), SCM_OBJ(&libiowrite_char__STUB), SCM_BINDING_INLINABLE);
  libiowrite_char__STUB.common.info = scm__rc.d1786[693];
  libiowrite_char__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[694]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[702] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[226])),TRUE); /* write-byte */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[904]), scm__rc.d1786[547]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[907]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[908]), scm__rc.d1786[702]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[909]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[910]), scm__rc.d1786[3]);
  scm__rc.d1786[703] = Scm_MakeExtendedPair(scm__rc.d1786[702], SCM_OBJ(&scm__rc.d1787[904]), SCM_OBJ(&scm__rc.d1787[912]));
  scm__rc.d1786[704] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[227])),TRUE); /* <int> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[705]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[705]))[4] = scm__rc.d1786[59];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[705]))[5] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[705]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[705]))[7] = scm__rc.d1786[704];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("write-byte")), SCM_OBJ(&libiowrite_byte__STUB), 0);
  libiowrite_byte__STUB.common.info = scm__rc.d1786[703];
  libiowrite_byte__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[705]);
  scm__rc.d1786[713] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[228])),TRUE); /* write-u8 */
  scm__rc.d1786[714] = Scm_MakeIdentifier(scm__rc.d1786[702], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#write-byte */
  scm__rc.d1786[715] = Scm_MakeIdentifier(scm__rc.d1786[713], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#write-u8 */
  scm__rc.d1786[716] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[33])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[33]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[33]))->debugInfo = scm__rc.d1786[716];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[752]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[752]))[6] = SCM_WORD(scm__rc.d1786[713]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[752]))[10] = SCM_WORD(scm__rc.d1786[714]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[752]))[13] = SCM_WORD(scm__rc.d1786[715]);
  scm__rc.d1786[717] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[229])),TRUE); /* write-limited */
  scm__rc.d1786[718] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[230])),TRUE); /* limit */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[913]), scm__rc.d1786[718]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[914]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[917]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[918]), scm__rc.d1786[717]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[919]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[920]), scm__rc.d1786[3]);
  scm__rc.d1786[719] = Scm_MakeExtendedPair(scm__rc.d1786[717], SCM_OBJ(&scm__rc.d1787[914]), SCM_OBJ(&scm__rc.d1787[922]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[720]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[720]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[720]))[5] = scm__rc.d1786[59];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[720]))[6] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[720]))[7] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[720]))[8] = scm__rc.d1786[704];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("write-limited")), SCM_OBJ(&libiowrite_limited__STUB), 0);
  libiowrite_limited__STUB.common.info = scm__rc.d1786[719];
  libiowrite_limited__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[720]);
  scm__rc.d1786[729] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[231])),TRUE); /* write* */
  scm__rc.d1786[730] = Scm_MakeIdentifier(scm__rc.d1786[679], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#write-shared */
  scm__rc.d1786[731] = Scm_MakeIdentifier(scm__rc.d1786[729], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#write* */
  scm__rc.d1786[732] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[34])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[34]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[34]))->debugInfo = scm__rc.d1786[732];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[767]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[767]))[6] = SCM_WORD(scm__rc.d1786[729]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[767]))[10] = SCM_WORD(scm__rc.d1786[730]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[767]))[13] = SCM_WORD(scm__rc.d1786[731]);
  scm__rc.d1786[733] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[232])),TRUE); /* flush */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[923]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[926]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[927]), scm__rc.d1786[733]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[928]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[929]), scm__rc.d1786[3]);
  scm__rc.d1786[734] = Scm_MakeExtendedPair(scm__rc.d1786[733], SCM_OBJ(&scm__rc.d1787[923]), SCM_OBJ(&scm__rc.d1787[931]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[735]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[735]))[4] = scm__rc.d1786[35];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[735]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[735]))[6] = scm__rc.d1786[82];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("flush")), SCM_OBJ(&libioflush__STUB), 0);
  libioflush__STUB.common.info = scm__rc.d1786[734];
  libioflush__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[735]);
  scm__rc.d1786[742] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[233])),TRUE); /* flush-all-ports */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[932]), scm__rc.d1786[742]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[933]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[934]), scm__rc.d1786[3]);
  scm__rc.d1786[743] = Scm_MakeExtendedPair(scm__rc.d1786[742], SCM_NIL, SCM_OBJ(&scm__rc.d1787[935]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[744]))[3] = scm__rc.d1786[21];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[744]))[4] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[744]))[5] = scm__rc.d1786[82];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("flush-all-ports")), SCM_OBJ(&libioflush_all_ports__STUB), 0);
  libioflush_all_ports__STUB.common.info = scm__rc.d1786[743];
  libioflush_all_ports__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[744]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
  scm__rc.d1786[750] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[234])),TRUE); /* write-need-recurse? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[938]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[939]), scm__rc.d1786[750]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[940]), scm__rc.d1786[159]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[941]), scm__rc.d1786[3]);
  scm__rc.d1786[751] = Scm_MakeExtendedPair(scm__rc.d1786[750], SCM_OBJ(&scm__rc.d1787[1]), SCM_OBJ(&scm__rc.d1787[943]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[752]))[3] = scm__rc.d1786[159];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[752]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[752]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[752]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("write-need-recurse?")), SCM_OBJ(&libiowrite_need_recurseP__STUB), 0);
  libiowrite_need_recurseP__STUB.common.info = scm__rc.d1786[751];
  libiowrite_need_recurseP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[752]);
  scm__rc.d1786[759] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[235])),TRUE); /* write-walk */
  scm__rc.d1786[761] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[129])),TRUE); /* shared-table */
  scm__rc.d1786[760] = Scm_MakeIdentifier(scm__rc.d1786[761], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#shared-table */
  scm__rc.d1786[763] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[236])),TRUE); /* ~ */
  scm__rc.d1786[762] = Scm_MakeIdentifier(scm__rc.d1786[763], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#~ */
  scm__rc.d1786[765] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[237])),TRUE); /* %write-walk-rec */
  scm__rc.d1786[764] = Scm_MakeIdentifier(scm__rc.d1786[765], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#%write-walk-rec */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[944]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[947]), scm__rc.d1786[2]);
  scm__rc.d1786[766] = Scm_MakeExtendedPair(scm__rc.d1786[759], SCM_OBJ(&scm__rc.d1787[944]), SCM_OBJ(&scm__rc.d1787[948]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[949]), scm__rc.d1786[766]);
  scm__rc.d1786[767] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[35])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[35]))->name = scm__rc.d1786[759];/* write-walk */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[35]))->debugInfo = scm__rc.d1786[767];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[782]))[4] = SCM_WORD(scm__rc.d1786[381]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[782]))[15] = SCM_WORD(scm__rc.d1786[760]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[782]))[17] = SCM_WORD(scm__rc.d1786[762]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[782]))[19] = SCM_WORD(scm__rc.d1786[764]);
  scm__rc.d1786[768] = Scm_MakeIdentifier(scm__rc.d1786[759], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#write-walk */
  scm__rc.d1786[769] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[36])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[36]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[36]))->debugInfo = scm__rc.d1786[769];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[804]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[804]))[6] = SCM_WORD(scm__rc.d1786[759]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[804]))[13] = SCM_WORD(scm__rc.d1786[768]);
  scm__rc.d1786[770] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[238])),FALSE); /* G1819 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[950]), scm__rc.d1786[770]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[951]), scm__rc.d1786[765]);
  scm__rc.d1786[771] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[239])),FALSE); /* G1816 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[952]), scm__rc.d1786[771]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[955]), scm__rc.d1786[2]);
  scm__rc.d1786[772] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[951]), SCM_OBJ(&scm__rc.d1787[952]), SCM_OBJ(&scm__rc.d1787[956]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[957]), scm__rc.d1786[772]);
  scm__rc.d1786[773] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[37])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[37]))->debugInfo = scm__rc.d1786[773];
  scm__rc.d1786[774] = Scm_MakeIdentifier(scm__rc.d1786[750], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#write-need-recurse? */
  scm__rc.d1786[776] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[240])),TRUE); /* hash-table-exists? */
  scm__rc.d1786[775] = Scm_MakeIdentifier(scm__rc.d1786[776], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#hash-table-exists? */
  scm__rc.d1786[778] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[241])),TRUE); /* hash-table-update! */
  scm__rc.d1786[777] = Scm_MakeIdentifier(scm__rc.d1786[778], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#hash-table-update! */
  scm__rc.d1786[780] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[242])),TRUE); /* hash-table-put! */
  scm__rc.d1786[779] = Scm_MakeIdentifier(scm__rc.d1786[780], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#hash-table-put! */
  scm__rc.d1786[782] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[243])),TRUE); /* uvector? */
  scm__rc.d1786[781] = Scm_MakeIdentifier(scm__rc.d1786[782], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#uvector? */
  scm__rc.d1786[784] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[244])),TRUE); /* box? */
  scm__rc.d1786[783] = Scm_MakeIdentifier(scm__rc.d1786[784], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#box? */
  scm__rc.d1786[786] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[245])),TRUE); /* box-arity */
  scm__rc.d1786[785] = Scm_MakeIdentifier(scm__rc.d1786[786], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#box-arity */
  scm__rc.d1786[788] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[246])),TRUE); /* unbox-value */
  scm__rc.d1786[787] = Scm_MakeIdentifier(scm__rc.d1786[788], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#unbox-value */
  scm__rc.d1786[790] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[247])),TRUE); /* write-object */
  scm__rc.d1786[789] = Scm_MakeIdentifier(scm__rc.d1786[790], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#write-object */
  scm__rc.d1786[791] = Scm_MakeIdentifier(scm__rc.d1786[344], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#%port-writing-shared? */
  scm__rc.d1786[793] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[248])),TRUE); /* hash-table-get */
  scm__rc.d1786[792] = Scm_MakeIdentifier(scm__rc.d1786[793], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#hash-table-get */
  scm__rc.d1786[795] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[249])),TRUE); /* hash-table-delete! */
  scm__rc.d1786[794] = Scm_MakeIdentifier(scm__rc.d1786[795], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#hash-table-delete! */
  scm__rc.d1786[796] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[250])),TRUE); /* tab */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[959]), scm__rc.d1786[796]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[960]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[961]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[964]), scm__rc.d1786[2]);
  scm__rc.d1786[797] = Scm_MakeExtendedPair(scm__rc.d1786[765], SCM_OBJ(&scm__rc.d1787[961]), SCM_OBJ(&scm__rc.d1787[965]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[966]), scm__rc.d1786[797]);
  scm__rc.d1786[798] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[38])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[38]))->name = scm__rc.d1786[765];/* %write-walk-rec */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[38]))->debugInfo = scm__rc.d1786[798];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]))[4] = SCM_WORD(scm__rc.d1786[774]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]))[12] = SCM_WORD(scm__rc.d1786[775]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]))[19] = SCM_WORD(scm__rc.d1786[777]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]))[27] = SCM_WORD(scm__rc.d1786[779]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]))[44] = SCM_WORD(scm__rc.d1786[781]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]))[60] = SCM_WORD(scm__rc.d1786[764]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]))[68] = SCM_WORD(scm__rc.d1786[764]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]))[95] = SCM_WORD(scm__rc.d1786[764]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]))[107] = SCM_WORD(scm__rc.d1786[783]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]))[116] = SCM_WORD(scm__rc.d1786[785]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]))[131] = SCM_WORD(scm__rc.d1786[787]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]))[136] = SCM_WORD(scm__rc.d1786[764]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]))[149] = SCM_WORD(scm__rc.d1786[789]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]))[154] = SCM_WORD(scm__rc.d1786[791]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]))[164] = SCM_WORD(scm__rc.d1786[792]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[821]))[171] = SCM_WORD(scm__rc.d1786[794]);
  scm__rc.d1786[799] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[39])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[39]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[39]))->debugInfo = scm__rc.d1786[799];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[996]))[5] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[996]))[8] = SCM_WORD(scm__rc.d1786[765]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[996]))[15] = SCM_WORD(scm__rc.d1786[764]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
  scm__rc.d1786[800] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[251])),TRUE); /* write-with-shared-structure */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[968]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[969]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[970]), scm__rc.d1786[729]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[973]), scm__rc.d1786[471]);
  scm__rc.d1786[801] = Scm_MakeIdentifier(scm__rc.d1786[729], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#write* */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[976]), scm__rc.d1786[2]);
  scm__rc.d1786[802] = Scm_MakeExtendedPair(scm__rc.d1786[800], SCM_OBJ(&scm__rc.d1787[969]), SCM_OBJ(&scm__rc.d1787[977]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[978]), scm__rc.d1786[802]);
  scm__rc.d1786[803] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[40])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[40]))->name = scm__rc.d1786[800];/* write-with-shared-structure */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[40]))->debugInfo = scm__rc.d1786[803];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1013]))[28] = SCM_WORD(scm__rc.d1786[487]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1013]))[32] = SCM_WORD(scm__rc.d1786[801]);
  scm__rc.d1786[804] = Scm_MakeIdentifier(scm__rc.d1786[800], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#write-with-shared-structure */
  scm__rc.d1786[805] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[41])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[41]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[41]))->debugInfo = scm__rc.d1786[805];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1047]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1047]))[6] = SCM_WORD(scm__rc.d1786[800]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1047]))[13] = SCM_WORD(scm__rc.d1786[804]);
  scm__rc.d1786[806] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[252])),TRUE); /* write/ss */
  scm__rc.d1786[807] = Scm_MakeIdentifier(scm__rc.d1786[800], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#write-with-shared-structure */
  scm__rc.d1786[808] = Scm_MakeIdentifier(scm__rc.d1786[806], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#write/ss */
  scm__rc.d1786[809] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[42])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[42]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[42]))->debugInfo = scm__rc.d1786[809];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1062]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1062]))[6] = SCM_WORD(scm__rc.d1786[806]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1062]))[10] = SCM_WORD(scm__rc.d1786[807]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1062]))[13] = SCM_WORD(scm__rc.d1786[808]);
  scm__rc.d1786[810] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[253])),TRUE); /* print */
  scm__rc.d1786[811] = Scm_MakeIdentifier(scm__rc.d1786[498], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#display */
  scm__rc.d1786[813] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[254])),TRUE); /* for-each */
  scm__rc.d1786[812] = Scm_MakeIdentifier(scm__rc.d1786[813], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#for-each */
  scm__rc.d1786[814] = Scm_MakeIdentifier(scm__rc.d1786[682], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#newline */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[981]), scm__rc.d1786[2]);
  scm__rc.d1786[815] = Scm_MakeExtendedPair(scm__rc.d1786[810], scm__rc.d1786[375], SCM_OBJ(&scm__rc.d1787[982]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[983]), scm__rc.d1786[815]);
  scm__rc.d1786[816] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[43])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[43]))->name = scm__rc.d1786[810];/* print */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[43]))->debugInfo = scm__rc.d1786[816];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1077]))[3] = SCM_WORD(scm__rc.d1786[811]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1077]))[6] = SCM_WORD(scm__rc.d1786[812]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1077]))[8] = SCM_WORD(scm__rc.d1786[814]);
  scm__rc.d1786[817] = Scm_MakeIdentifier(scm__rc.d1786[810], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#print */
  scm__rc.d1786[818] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[44])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[44]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[44]))->debugInfo = scm__rc.d1786[818];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1087]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1087]))[6] = SCM_WORD(scm__rc.d1786[810]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1087]))[13] = SCM_WORD(scm__rc.d1786[817]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  Scm_InitStaticClassWithMeta(&Scm_WriteControlsClass, "<write-controls>", SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), NULL, SCM_FALSE, Scm_WriteControlsClass__SLOTS, 0);
  scm__rc.d1786[819] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[255])),TRUE); /* make-write-controls */
  scm__rc.d1786[821] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[256])),TRUE); /* undefined? */
  scm__rc.d1786[820] = Scm_MakeIdentifier(scm__rc.d1786[821], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#undefined? */
  scm__rc.d1786[823] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[257])),TRUE); /* <write-controls> */
  scm__rc.d1786[822] = Scm_MakeIdentifier(scm__rc.d1786[823], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#<write-controls> */
  scm__rc.d1786[824] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[258]))); /* :length */
  scm__rc.d1786[825] = Scm_MakeIdentifier(scm__rc.d1786[821], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#undefined? */
  scm__rc.d1786[826] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[259]))); /* :level */
  scm__rc.d1786[827] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[260]))); /* :width */
  scm__rc.d1786[828] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[261]))); /* :base */
  scm__rc.d1786[829] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[262]))); /* :radix */
  scm__rc.d1786[830] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[263]))); /* :pretty */
  scm__rc.d1786[831] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[264]))); /* :string-length */
  scm__rc.d1786[832] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[265]))); /* :indent */
  scm__rc.d1786[833] = Scm_MakeIdentifier(scm__rc.d1786[391], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#make */
  scm__rc.d1786[835] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[267])),TRUE); /* unwrap-syntax-1 */
  scm__rc.d1786[834] = Scm_MakeIdentifier(scm__rc.d1786[835], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#unwrap-syntax-1 */
  scm__rc.d1786[836] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[268]))); /* :print-length */
  scm__rc.d1786[837] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[269]))); /* :print-level */
  scm__rc.d1786[838] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[270]))); /* :print-width */
  scm__rc.d1786[839] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[271]))); /* :print-base */
  scm__rc.d1786[840] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[272]))); /* :print-radix */
  scm__rc.d1786[841] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[273]))); /* :print-pretty */
  scm__rc.d1786[843] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[275])),TRUE); /* errorf */
  scm__rc.d1786[842] = Scm_MakeIdentifier(scm__rc.d1786[843], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#errorf */
  scm__rc.d1786[844] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[258])),TRUE); /* length */
  scm__rc.d1786[845] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[259])),TRUE); /* level */
  scm__rc.d1786[846] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[260])),TRUE); /* width */
  scm__rc.d1786[847] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[261])),TRUE); /* base */
  scm__rc.d1786[848] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[262])),TRUE); /* radix */
  scm__rc.d1786[849] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[263])),TRUE); /* pretty */
  scm__rc.d1786[850] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[265])),TRUE); /* indent */
  scm__rc.d1786[851] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[264])),TRUE); /* string-length */
  scm__rc.d1786[852] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[268])),TRUE); /* print-length */
  scm__rc.d1786[853] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[269])),TRUE); /* print-level */
  scm__rc.d1786[854] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[270])),TRUE); /* print-width */
  scm__rc.d1786[855] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[271])),TRUE); /* print-base */
  scm__rc.d1786[856] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[272])),TRUE); /* print-radix */
  scm__rc.d1786[857] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[273])),TRUE); /* print-pretty */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[984]), scm__rc.d1786[857]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[985]), scm__rc.d1786[856]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[986]), scm__rc.d1786[855]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[987]), scm__rc.d1786[854]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[988]), scm__rc.d1786[853]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[989]), scm__rc.d1786[852]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[990]), scm__rc.d1786[851]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[991]), scm__rc.d1786[850]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[992]), scm__rc.d1786[849]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[993]), scm__rc.d1786[848]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[994]), scm__rc.d1786[847]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[995]), scm__rc.d1786[846]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[996]), scm__rc.d1786[845]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[997]), scm__rc.d1786[844]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[998]), scm__rc.d1786[196]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1001]), scm__rc.d1786[2]);
  scm__rc.d1786[858] = Scm_MakeExtendedPair(scm__rc.d1786[819], SCM_OBJ(&scm__rc.d1787[998]), SCM_OBJ(&scm__rc.d1787[1002]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1003]), scm__rc.d1786[858]);
  scm__rc.d1786[859] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[45])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[45]))->name = scm__rc.d1786[819];/* make-write-controls */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[45]))->debugInfo = scm__rc.d1786[859];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[36] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[48] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[60] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[72] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[84] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[96] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[108] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[120] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[132] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[144] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[156] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[168] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[180] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[192] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[201] = SCM_WORD(scm__rc.d1786[822]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[203] = SCM_WORD(scm__rc.d1786[824]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[208] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[217] = SCM_WORD(scm__rc.d1786[826]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[222] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[231] = SCM_WORD(scm__rc.d1786[827]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[236] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[245] = SCM_WORD(scm__rc.d1786[828]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[250] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[259] = SCM_WORD(scm__rc.d1786[829]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[264] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[273] = SCM_WORD(scm__rc.d1786[830]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[278] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[287] = SCM_WORD(scm__rc.d1786[831]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[290] = SCM_WORD(scm__rc.d1786[832]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[293] = SCM_WORD(scm__rc.d1786[833]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[303] = SCM_WORD(scm__rc.d1786[487]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[310] = SCM_WORD(scm__rc.d1786[834]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[314] = SCM_WORD(scm__rc.d1786[824]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[338] = SCM_WORD(scm__rc.d1786[826]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[362] = SCM_WORD(scm__rc.d1786[827]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[386] = SCM_WORD(scm__rc.d1786[828]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[410] = SCM_WORD(scm__rc.d1786[829]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[434] = SCM_WORD(scm__rc.d1786[830]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[458] = SCM_WORD(scm__rc.d1786[832]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[482] = SCM_WORD(scm__rc.d1786[831]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[506] = SCM_WORD(scm__rc.d1786[836]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[530] = SCM_WORD(scm__rc.d1786[837]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[554] = SCM_WORD(scm__rc.d1786[838]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[578] = SCM_WORD(scm__rc.d1786[839]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[602] = SCM_WORD(scm__rc.d1786[840]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[626] = SCM_WORD(scm__rc.d1786[841]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1102]))[655] = SCM_WORD(scm__rc.d1786[842]);
  scm__rc.d1786[860] = Scm_MakeIdentifier(scm__rc.d1786[819], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#make-write-controls */
  scm__rc.d1786[861] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[46])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[46]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[46]))->debugInfo = scm__rc.d1786[861];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1778]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1778]))[6] = SCM_WORD(scm__rc.d1786[819]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1778]))[13] = SCM_WORD(scm__rc.d1786[860]);
  scm__rc.d1786[862] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[276])),TRUE); /* write-controls-copy */
  scm__rc.d1786[863] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[277])),TRUE); /* wc */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1004]), scm__rc.d1786[863]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1007]), scm__rc.d1786[2]);
  scm__rc.d1786[864] = Scm_MakeExtendedPair(scm__rc.d1786[862], SCM_OBJ(&scm__rc.d1787[1004]), SCM_OBJ(&scm__rc.d1787[1008]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1009]), scm__rc.d1786[864]);
  scm__rc.d1786[865] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[47])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[47]))->name = scm__rc.d1786[862];/* write-controls-copy */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[47]))->debugInfo = scm__rc.d1786[865];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[36] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[48] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[60] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[72] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[84] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[96] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[108] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[120] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[132] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[144] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[156] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[168] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[180] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[192] = SCM_WORD(scm__rc.d1786[820]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[204] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[211] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[216] = SCM_WORD(scm__rc.d1786[844]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[227] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[234] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[239] = SCM_WORD(scm__rc.d1786[845]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[250] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[257] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[262] = SCM_WORD(scm__rc.d1786[846]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[273] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[280] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[285] = SCM_WORD(scm__rc.d1786[847]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[296] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[303] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[308] = SCM_WORD(scm__rc.d1786[848]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[319] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[326] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[331] = SCM_WORD(scm__rc.d1786[849]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[342] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[347] = SCM_WORD(scm__rc.d1786[850]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[355] = SCM_WORD(scm__rc.d1786[825]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[360] = SCM_WORD(scm__rc.d1786[851]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[368] = SCM_WORD(scm__rc.d1786[844]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[374] = SCM_WORD(scm__rc.d1786[845]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[380] = SCM_WORD(scm__rc.d1786[846]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[386] = SCM_WORD(scm__rc.d1786[847]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[392] = SCM_WORD(scm__rc.d1786[848]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[398] = SCM_WORD(scm__rc.d1786[849]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[404] = SCM_WORD(scm__rc.d1786[850]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[410] = SCM_WORD(scm__rc.d1786[851]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[415] = SCM_WORD(scm__rc.d1786[822]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[417] = SCM_WORD(scm__rc.d1786[824]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[420] = SCM_WORD(scm__rc.d1786[826]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[423] = SCM_WORD(scm__rc.d1786[827]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[426] = SCM_WORD(scm__rc.d1786[828]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[429] = SCM_WORD(scm__rc.d1786[829]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[432] = SCM_WORD(scm__rc.d1786[830]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[435] = SCM_WORD(scm__rc.d1786[832]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[438] = SCM_WORD(scm__rc.d1786[831]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[441] = SCM_WORD(scm__rc.d1786[833]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[472] = SCM_WORD(scm__rc.d1786[487]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[479] = SCM_WORD(scm__rc.d1786[834]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[483] = SCM_WORD(scm__rc.d1786[824]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[507] = SCM_WORD(scm__rc.d1786[826]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[531] = SCM_WORD(scm__rc.d1786[827]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[555] = SCM_WORD(scm__rc.d1786[828]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[579] = SCM_WORD(scm__rc.d1786[829]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[603] = SCM_WORD(scm__rc.d1786[830]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[627] = SCM_WORD(scm__rc.d1786[832]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[651] = SCM_WORD(scm__rc.d1786[831]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[675] = SCM_WORD(scm__rc.d1786[836]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[699] = SCM_WORD(scm__rc.d1786[837]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[723] = SCM_WORD(scm__rc.d1786[838]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[747] = SCM_WORD(scm__rc.d1786[839]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[771] = SCM_WORD(scm__rc.d1786[840]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[795] = SCM_WORD(scm__rc.d1786[841]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1793]))[824] = SCM_WORD(scm__rc.d1786[842]);
  scm__rc.d1786[866] = Scm_MakeIdentifier(scm__rc.d1786[862], SCM_MODULE(scm__rc.d1786[320]), SCM_NIL); /* gauche#write-controls-copy */
  scm__rc.d1786[867] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[48])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[48]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[48]))->debugInfo = scm__rc.d1786[867];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2638]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2638]))[6] = SCM_WORD(scm__rc.d1786[862]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2638]))[13] = SCM_WORD(scm__rc.d1786[866]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
  scm__rc.d1786[868] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[278])),TRUE); /* open-input-file */
  scm__rc.d1786[869] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[279]))); /* :encoding */
  scm__rc.d1786[871] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[280])),TRUE); /* default-file-encoding */
  scm__rc.d1786[870] = Scm_MakeIdentifier(scm__rc.d1786[871], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#default-file-encoding */
  scm__rc.d1786[873] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[281])),TRUE); /* get-keyword */
  scm__rc.d1786[872] = Scm_MakeIdentifier(scm__rc.d1786[873], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#get-keyword */
  scm__rc.d1786[875] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[282])),TRUE); /* gauche-character-encoding */
  scm__rc.d1786[874] = Scm_MakeIdentifier(scm__rc.d1786[875], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#gauche-character-encoding */
  scm__rc.d1786[876] = Scm_MakeIdentifier(scm__rc.d1786[194], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#%open-input-file */
  scm__rc.d1786[878] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[283])),TRUE); /* delete-keyword */
  scm__rc.d1786[877] = Scm_MakeIdentifier(scm__rc.d1786[878], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#delete-keyword */
  scm__rc.d1786[879] = Scm_MakeIdentifier(scm__rc.d1786[288], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#open-coding-aware-port */
  scm__rc.d1786[881] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[284])),TRUE); /* %open-input-file/conv */
  scm__rc.d1786[880] = Scm_MakeIdentifier(scm__rc.d1786[881], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#%open-input-file/conv */
  scm__rc.d1786[882] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[285])),TRUE); /* filename */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1010]), scm__rc.d1786[882]);
  SCM_SET_CDR(SCM_OBJ(&scm__rc.d1787[1010]), scm__rc.d1786[375]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1013]), scm__rc.d1786[2]);
  scm__rc.d1786[883] = Scm_MakeExtendedPair(scm__rc.d1786[868], SCM_OBJ(&scm__rc.d1787[1010]), SCM_OBJ(&scm__rc.d1787[1014]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1015]), scm__rc.d1786[883]);
  scm__rc.d1786[884] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[49])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[49]))->name = scm__rc.d1786[868];/* open-input-file */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[49]))->debugInfo = scm__rc.d1786[884];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]))[3] = SCM_WORD(scm__rc.d1786[869]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]))[8] = SCM_WORD(scm__rc.d1786[870]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]))[10] = SCM_WORD(scm__rc.d1786[872]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]))[16] = SCM_WORD(scm__rc.d1786[874]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]))[20] = SCM_WORD(scm__rc.d1786[876]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]))[25] = SCM_WORD(scm__rc.d1786[869]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]))[28] = SCM_WORD(scm__rc.d1786[877]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]))[38] = SCM_WORD(scm__rc.d1786[876]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]))[43] = SCM_WORD(scm__rc.d1786[869]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]))[46] = SCM_WORD(scm__rc.d1786[877]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]))[53] = SCM_WORD(scm__rc.d1786[879]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2653]))[56] = SCM_WORD(scm__rc.d1786[880]);
  scm__rc.d1786[885] = Scm_MakeIdentifier(scm__rc.d1786[868], SCM_MODULE(Scm_SchemeModule()), SCM_NIL); /* scheme#open-input-file */
  scm__rc.d1786[886] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[50])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[50]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[50]))->debugInfo = scm__rc.d1786[886];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2714]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2714]))[6] = SCM_WORD(scm__rc.d1786[868]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2714]))[13] = SCM_WORD(scm__rc.d1786[885]);
  scm__rc.d1786[887] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[286])),TRUE); /* open-output-file */
  scm__rc.d1786[889] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[287])),TRUE); /* %open-output-file/conv */
  scm__rc.d1786[888] = Scm_MakeIdentifier(scm__rc.d1786[889], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#%open-output-file/conv */
  scm__rc.d1786[890] = Scm_MakeIdentifier(scm__rc.d1786[216], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#%open-output-file */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1018]), scm__rc.d1786[2]);
  scm__rc.d1786[891] = Scm_MakeExtendedPair(scm__rc.d1786[887], SCM_OBJ(&scm__rc.d1787[1010]), SCM_OBJ(&scm__rc.d1787[1019]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1020]), scm__rc.d1786[891]);
  scm__rc.d1786[892] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[51])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[51]))->name = scm__rc.d1786[887];/* open-output-file */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[51]))->debugInfo = scm__rc.d1786[892];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2729]))[3] = SCM_WORD(scm__rc.d1786[869]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2729]))[7] = SCM_WORD(scm__rc.d1786[872]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2729]))[13] = SCM_WORD(scm__rc.d1786[888]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2729]))[21] = SCM_WORD(scm__rc.d1786[870]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2729]))[25] = SCM_WORD(scm__rc.d1786[874]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2729]))[29] = SCM_WORD(scm__rc.d1786[890]);
  scm__rc.d1786[893] = Scm_MakeIdentifier(scm__rc.d1786[887], SCM_MODULE(Scm_SchemeModule()), SCM_NIL); /* scheme#open-output-file */
  scm__rc.d1786[894] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[52])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[52]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[52]))->debugInfo = scm__rc.d1786[894];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2766]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2766]))[6] = SCM_WORD(scm__rc.d1786[887]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2766]))[13] = SCM_WORD(scm__rc.d1786[893]);
  scm__rc.d1786[895] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[288])),TRUE); /* call-with-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1021]), scm__rc.d1786[895]);
  scm__rc.d1786[896] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[53])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[53]))->debugInfo = scm__rc.d1786[896];
  scm__rc.d1786[897] = Scm_MakeIdentifier(scm__rc.d1786[186], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#close-port */
  scm__rc.d1786[898] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[54])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[54]))->debugInfo = scm__rc.d1786[898];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2785]))[2] = SCM_WORD(scm__rc.d1786[897]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1026]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1029]), scm__rc.d1786[2]);
  scm__rc.d1786[899] = Scm_MakeExtendedPair(scm__rc.d1786[895], SCM_OBJ(&scm__rc.d1787[1026]), SCM_OBJ(&scm__rc.d1787[1030]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1031]), scm__rc.d1786[899]);
  scm__rc.d1786[900] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[55])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[55]))->name = scm__rc.d1786[895];/* call-with-port */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[55]))->debugInfo = scm__rc.d1786[900];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2789]))[7] = SCM_WORD(scm__rc.d1786[372]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2789]))[11] = SCM_WORD(scm__rc.d1786[373]);
  scm__rc.d1786[901] = Scm_MakeIdentifier(scm__rc.d1786[895], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#call-with-port */
  scm__rc.d1786[902] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[56])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[56]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[56]))->debugInfo = scm__rc.d1786[902];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2802]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2802]))[6] = SCM_WORD(scm__rc.d1786[895]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2802]))[13] = SCM_WORD(scm__rc.d1786[901]);
  scm__rc.d1786[903] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[289])),TRUE); /* call-with-input-file */
  scm__rc.d1786[904] = Scm_MakeIdentifier(scm__rc.d1786[868], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#open-input-file */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1032]), scm__rc.d1786[903]);
  scm__rc.d1786[905] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[53])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[57]))->debugInfo = scm__rc.d1786[905];
  scm__rc.d1786[906] = Scm_MakeIdentifier(scm__rc.d1786[168], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#close-input-port */
  scm__rc.d1786[907] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[57])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[58]))->debugInfo = scm__rc.d1786[907];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2821]))[5] = SCM_WORD(scm__rc.d1786[906]);
  scm__rc.d1786[908] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[290])),TRUE); /* flags */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1037]), scm__rc.d1786[129]);
  SCM_SET_CDR(SCM_OBJ(&scm__rc.d1787[1037]), scm__rc.d1786[908]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1038]), scm__rc.d1786[882]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1041]), scm__rc.d1786[2]);
  scm__rc.d1786[909] = Scm_MakeExtendedPair(scm__rc.d1786[903], SCM_OBJ(&scm__rc.d1787[1038]), SCM_OBJ(&scm__rc.d1787[1042]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1043]), scm__rc.d1786[909]);
  scm__rc.d1786[910] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[58])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[59]))->name = scm__rc.d1786[903];/* call-with-input-file */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[59]))->debugInfo = scm__rc.d1786[910];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2829]))[3] = SCM_WORD(scm__rc.d1786[904]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2829]))[15] = SCM_WORD(scm__rc.d1786[372]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2829]))[19] = SCM_WORD(scm__rc.d1786[373]);
  scm__rc.d1786[911] = Scm_MakeIdentifier(scm__rc.d1786[903], SCM_MODULE(Scm_SchemeModule()), SCM_NIL); /* scheme#call-with-input-file */
  scm__rc.d1786[912] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[59])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[60]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[60]))->debugInfo = scm__rc.d1786[912];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2850]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2850]))[6] = SCM_WORD(scm__rc.d1786[903]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2850]))[13] = SCM_WORD(scm__rc.d1786[911]);
  scm__rc.d1786[913] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[291])),TRUE); /* call-with-output-file */
  scm__rc.d1786[914] = Scm_MakeIdentifier(scm__rc.d1786[887], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#open-output-file */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1044]), scm__rc.d1786[913]);
  scm__rc.d1786[915] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[53])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[61]))->debugInfo = scm__rc.d1786[915];
  scm__rc.d1786[916] = Scm_MakeIdentifier(scm__rc.d1786[177], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#close-output-port */
  scm__rc.d1786[917] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[60])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[62]))->debugInfo = scm__rc.d1786[917];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2869]))[5] = SCM_WORD(scm__rc.d1786[916]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1051]), scm__rc.d1786[2]);
  scm__rc.d1786[918] = Scm_MakeExtendedPair(scm__rc.d1786[913], SCM_OBJ(&scm__rc.d1787[1038]), SCM_OBJ(&scm__rc.d1787[1052]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1053]), scm__rc.d1786[918]);
  scm__rc.d1786[919] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[61])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[63]))->name = scm__rc.d1786[913];/* call-with-output-file */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[63]))->debugInfo = scm__rc.d1786[919];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2877]))[3] = SCM_WORD(scm__rc.d1786[914]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2877]))[15] = SCM_WORD(scm__rc.d1786[372]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2877]))[19] = SCM_WORD(scm__rc.d1786[373]);
  scm__rc.d1786[920] = Scm_MakeIdentifier(scm__rc.d1786[913], SCM_MODULE(Scm_SchemeModule()), SCM_NIL); /* scheme#call-with-output-file */
  scm__rc.d1786[921] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[62])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[64]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[64]))->debugInfo = scm__rc.d1786[921];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2898]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2898]))[6] = SCM_WORD(scm__rc.d1786[913]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2898]))[13] = SCM_WORD(scm__rc.d1786[920]);
  scm__rc.d1786[922] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[292])),TRUE); /* with-input-from-file */
  scm__rc.d1786[924] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[293])),TRUE); /* with-input-from-port */
  scm__rc.d1786[923] = Scm_MakeIdentifier(scm__rc.d1786[924], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#with-input-from-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1054]), scm__rc.d1786[922]);
  scm__rc.d1786[925] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[63])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[65]))->debugInfo = scm__rc.d1786[925];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2913]))[3] = SCM_WORD(scm__rc.d1786[923]);
  scm__rc.d1786[926] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[64])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[66]))->debugInfo = scm__rc.d1786[926];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2918]))[2] = SCM_WORD(scm__rc.d1786[906]);
  scm__rc.d1786[927] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[294])),TRUE); /* thunk */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1059]), scm__rc.d1786[927]);
  SCM_SET_CDR(SCM_OBJ(&scm__rc.d1787[1059]), scm__rc.d1786[908]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1060]), scm__rc.d1786[882]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1063]), scm__rc.d1786[2]);
  scm__rc.d1786[928] = Scm_MakeExtendedPair(scm__rc.d1786[922], SCM_OBJ(&scm__rc.d1787[1060]), SCM_OBJ(&scm__rc.d1787[1064]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1065]), scm__rc.d1786[928]);
  scm__rc.d1786[929] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[65])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[67]))->name = scm__rc.d1786[922];/* with-input-from-file */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[67]))->debugInfo = scm__rc.d1786[929];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2922]))[3] = SCM_WORD(scm__rc.d1786[904]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2922]))[17] = SCM_WORD(scm__rc.d1786[372]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2922]))[21] = SCM_WORD(scm__rc.d1786[373]);
  scm__rc.d1786[930] = Scm_MakeIdentifier(scm__rc.d1786[922], SCM_MODULE(Scm_SchemeModule()), SCM_NIL); /* scheme#with-input-from-file */
  scm__rc.d1786[931] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[66])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[68]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[68]))->debugInfo = scm__rc.d1786[931];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2945]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2945]))[6] = SCM_WORD(scm__rc.d1786[922]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2945]))[13] = SCM_WORD(scm__rc.d1786[930]);
  scm__rc.d1786[932] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[295])),TRUE); /* with-output-to-file */
  scm__rc.d1786[934] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[296])),TRUE); /* with-output-to-port */
  scm__rc.d1786[933] = Scm_MakeIdentifier(scm__rc.d1786[934], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#with-output-to-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1066]), scm__rc.d1786[932]);
  scm__rc.d1786[935] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[67])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[69]))->debugInfo = scm__rc.d1786[935];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2960]))[3] = SCM_WORD(scm__rc.d1786[933]);
  scm__rc.d1786[936] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[68])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[70]))->debugInfo = scm__rc.d1786[936];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2965]))[2] = SCM_WORD(scm__rc.d1786[916]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1073]), scm__rc.d1786[2]);
  scm__rc.d1786[937] = Scm_MakeExtendedPair(scm__rc.d1786[932], SCM_OBJ(&scm__rc.d1787[1060]), SCM_OBJ(&scm__rc.d1787[1074]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1075]), scm__rc.d1786[937]);
  scm__rc.d1786[938] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[69])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[71]))->name = scm__rc.d1786[932];/* with-output-to-file */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[71]))->debugInfo = scm__rc.d1786[938];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2969]))[3] = SCM_WORD(scm__rc.d1786[914]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2969]))[17] = SCM_WORD(scm__rc.d1786[372]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2969]))[21] = SCM_WORD(scm__rc.d1786[373]);
  scm__rc.d1786[939] = Scm_MakeIdentifier(scm__rc.d1786[932], SCM_MODULE(Scm_SchemeModule()), SCM_NIL); /* scheme#with-output-to-file */
  scm__rc.d1786[940] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[70])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[72]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[72]))->debugInfo = scm__rc.d1786[940];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2992]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2992]))[6] = SCM_WORD(scm__rc.d1786[932]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2992]))[13] = SCM_WORD(scm__rc.d1786[939]);
  scm__rc.d1786[941] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[297])),TRUE); /* with-output-to-string */
  scm__rc.d1786[942] = Scm_MakeIdentifier(scm__rc.d1786[266], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#open-output-string */
  scm__rc.d1786[943] = Scm_MakeIdentifier(scm__rc.d1786[268], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#get-output-string */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1076]), scm__rc.d1786[927]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1079]), scm__rc.d1786[2]);
  scm__rc.d1786[944] = Scm_MakeExtendedPair(scm__rc.d1786[941], SCM_OBJ(&scm__rc.d1787[1076]), SCM_OBJ(&scm__rc.d1787[1080]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1081]), scm__rc.d1786[944]);
  scm__rc.d1786[945] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[71])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[73]))->name = scm__rc.d1786[941];/* with-output-to-string */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[73]))->debugInfo = scm__rc.d1786[945];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3007]))[3] = SCM_WORD(scm__rc.d1786[942]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3007]))[10] = SCM_WORD(scm__rc.d1786[933]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3007]))[13] = SCM_WORD(scm__rc.d1786[943]);
  scm__rc.d1786[946] = Scm_MakeIdentifier(scm__rc.d1786[941], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#with-output-to-string */
  scm__rc.d1786[947] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[72])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[74]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[74]))->debugInfo = scm__rc.d1786[947];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3022]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3022]))[6] = SCM_WORD(scm__rc.d1786[941]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3022]))[13] = SCM_WORD(scm__rc.d1786[946]);
  scm__rc.d1786[948] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[298])),TRUE); /* with-input-from-string */
  scm__rc.d1786[949] = Scm_MakeIdentifier(scm__rc.d1786[255], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#open-input-string */
  scm__rc.d1786[950] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[299])),TRUE); /* str */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1082]), scm__rc.d1786[950]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1085]), scm__rc.d1786[2]);
  scm__rc.d1786[951] = Scm_MakeExtendedPair(scm__rc.d1786[948], SCM_OBJ(&scm__rc.d1787[1082]), SCM_OBJ(&scm__rc.d1787[1086]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1087]), scm__rc.d1786[951]);
  scm__rc.d1786[952] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[73])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[75]))->name = scm__rc.d1786[948];/* with-input-from-string */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[75]))->debugInfo = scm__rc.d1786[952];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3037]))[4] = SCM_WORD(scm__rc.d1786[949]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3037]))[8] = SCM_WORD(scm__rc.d1786[923]);
  scm__rc.d1786[953] = Scm_MakeIdentifier(scm__rc.d1786[948], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#with-input-from-string */
  scm__rc.d1786[954] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[74])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[76]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[76]))->debugInfo = scm__rc.d1786[954];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3047]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3047]))[6] = SCM_WORD(scm__rc.d1786[948]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3047]))[13] = SCM_WORD(scm__rc.d1786[953]);
  scm__rc.d1786[955] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[300])),TRUE); /* call-with-output-string */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1090]), scm__rc.d1786[2]);
  scm__rc.d1786[956] = Scm_MakeExtendedPair(scm__rc.d1786[955], SCM_OBJ(&scm__rc.d1787[797]), SCM_OBJ(&scm__rc.d1787[1091]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1092]), scm__rc.d1786[956]);
  scm__rc.d1786[957] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[75])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[77]))->name = scm__rc.d1786[955];/* call-with-output-string */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[77]))->debugInfo = scm__rc.d1786[957];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3062]))[3] = SCM_WORD(scm__rc.d1786[942]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3062]))[12] = SCM_WORD(scm__rc.d1786[943]);
  scm__rc.d1786[958] = Scm_MakeIdentifier(scm__rc.d1786[955], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#call-with-output-string */
  scm__rc.d1786[959] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[76])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[78]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[78]))->debugInfo = scm__rc.d1786[959];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3076]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3076]))[6] = SCM_WORD(scm__rc.d1786[955]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3076]))[13] = SCM_WORD(scm__rc.d1786[958]);
  scm__rc.d1786[960] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[301])),TRUE); /* call-with-input-string */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1093]), scm__rc.d1786[950]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1096]), scm__rc.d1786[2]);
  scm__rc.d1786[961] = Scm_MakeExtendedPair(scm__rc.d1786[960], SCM_OBJ(&scm__rc.d1787[1093]), SCM_OBJ(&scm__rc.d1787[1097]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1098]), scm__rc.d1786[961]);
  scm__rc.d1786[962] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[77])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[79]))->name = scm__rc.d1786[960];/* call-with-input-string */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[79]))->debugInfo = scm__rc.d1786[962];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3091]))[4] = SCM_WORD(scm__rc.d1786[949]);
  scm__rc.d1786[963] = Scm_MakeIdentifier(scm__rc.d1786[960], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#call-with-input-string */
  scm__rc.d1786[964] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[78])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[80]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[80]))->debugInfo = scm__rc.d1786[964];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3100]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3100]))[6] = SCM_WORD(scm__rc.d1786[960]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3100]))[13] = SCM_WORD(scm__rc.d1786[963]);
  scm__rc.d1786[965] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[302])),TRUE); /* call-with-string-io */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1101]), scm__rc.d1786[2]);
  scm__rc.d1786[966] = Scm_MakeExtendedPair(scm__rc.d1786[965], SCM_OBJ(&scm__rc.d1787[1093]), SCM_OBJ(&scm__rc.d1787[1102]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1103]), scm__rc.d1786[966]);
  scm__rc.d1786[967] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[79])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[81]))->name = scm__rc.d1786[965];/* call-with-string-io */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[81]))->debugInfo = scm__rc.d1786[967];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3115]))[3] = SCM_WORD(scm__rc.d1786[942]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3115]))[8] = SCM_WORD(scm__rc.d1786[949]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3115]))[18] = SCM_WORD(scm__rc.d1786[943]);
  scm__rc.d1786[968] = Scm_MakeIdentifier(scm__rc.d1786[965], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#call-with-string-io */
  scm__rc.d1786[969] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[80])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[82]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[82]))->debugInfo = scm__rc.d1786[969];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3135]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3135]))[6] = SCM_WORD(scm__rc.d1786[965]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3135]))[13] = SCM_WORD(scm__rc.d1786[968]);
  scm__rc.d1786[970] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[303])),TRUE); /* with-string-io */
  scm__rc.d1786[971] = Scm_MakeIdentifier(scm__rc.d1786[948], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#with-input-from-string */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1104]), scm__rc.d1786[970]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1107]), scm__rc.d1786[2]);
  scm__rc.d1786[972] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1104]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[1108]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1109]), scm__rc.d1786[972]);
  scm__rc.d1786[973] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[81])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[83]))->debugInfo = scm__rc.d1786[973];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3150]))[3] = SCM_WORD(scm__rc.d1786[971]);
  scm__rc.d1786[974] = Scm_MakeIdentifier(scm__rc.d1786[941], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#with-output-to-string */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1112]), scm__rc.d1786[2]);
  scm__rc.d1786[975] = Scm_MakeExtendedPair(scm__rc.d1786[970], SCM_OBJ(&scm__rc.d1787[1082]), SCM_OBJ(&scm__rc.d1787[1113]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1114]), scm__rc.d1786[975]);
  scm__rc.d1786[976] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[82])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[84]))->name = scm__rc.d1786[970];/* with-string-io */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[84]))->debugInfo = scm__rc.d1786[976];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3155]))[3] = SCM_WORD(scm__rc.d1786[974]);
  scm__rc.d1786[977] = Scm_MakeIdentifier(scm__rc.d1786[970], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#with-string-io */
  scm__rc.d1786[978] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[83])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[85]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[85]))->debugInfo = scm__rc.d1786[978];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3160]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3160]))[6] = SCM_WORD(scm__rc.d1786[970]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3160]))[13] = SCM_WORD(scm__rc.d1786[977]);
  scm__rc.d1786[979] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[304])),TRUE); /* write-to-string */
  scm__rc.d1786[980] = Scm_MakeIdentifier(scm__rc.d1786[665], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#write */
  scm__rc.d1786[981] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[305])),TRUE); /* writer */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1115]), scm__rc.d1786[981]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1117]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1118]), scm__rc.d1786[1]);
  scm__rc.d1786[982] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[306])),TRUE); /* cut */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1119]), scm__rc.d1786[981]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1120]), scm__rc.d1786[982]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1122]), scm__rc.d1786[941]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1125]), scm__rc.d1786[471]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1126]), scm__rc.d1786[979]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1129]), scm__rc.d1786[2]);
  scm__rc.d1786[983] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1126]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[1130]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1131]), scm__rc.d1786[983]);
  scm__rc.d1786[984] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[84])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[86]))->debugInfo = scm__rc.d1786[984];
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1134]), scm__rc.d1786[2]);
  scm__rc.d1786[985] = Scm_MakeExtendedPair(scm__rc.d1786[979], SCM_OBJ(&scm__rc.d1787[1118]), SCM_OBJ(&scm__rc.d1787[1135]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1136]), scm__rc.d1786[985]);
  scm__rc.d1786[986] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[85])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[87]))->name = scm__rc.d1786[979];/* write-to-string */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[87]))->debugInfo = scm__rc.d1786[986];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3179]))[4] = SCM_WORD(scm__rc.d1786[980]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3179]))[29] = SCM_WORD(scm__rc.d1786[487]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3179]))[33] = SCM_WORD(scm__rc.d1786[974]);
  scm__rc.d1786[987] = Scm_MakeIdentifier(scm__rc.d1786[979], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#write-to-string */
  scm__rc.d1786[988] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[86])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[88]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[88]))->debugInfo = scm__rc.d1786[988];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3214]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3214]))[6] = SCM_WORD(scm__rc.d1786[979]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3214]))[13] = SCM_WORD(scm__rc.d1786[987]);
  scm__rc.d1786[989] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[307])),TRUE); /* read-from-string */
  scm__rc.d1786[990] = Scm_MakeIdentifier(scm__rc.d1786[499], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#opt-substring */
  scm__rc.d1786[991] = Scm_MakeIdentifier(scm__rc.d1786[410], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#read */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1137]), scm__rc.d1786[130]);
  SCM_SET_CDR(SCM_OBJ(&scm__rc.d1787[1137]), scm__rc.d1786[375]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1140]), scm__rc.d1786[2]);
  scm__rc.d1786[992] = Scm_MakeExtendedPair(scm__rc.d1786[989], SCM_OBJ(&scm__rc.d1787[1137]), SCM_OBJ(&scm__rc.d1787[1141]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1142]), scm__rc.d1786[992]);
  scm__rc.d1786[993] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[87])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[89]))->name = scm__rc.d1786[989];/* read-from-string */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[89]))->debugInfo = scm__rc.d1786[993];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3229]))[9] = SCM_WORD(scm__rc.d1786[990]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3229]))[14] = SCM_WORD(scm__rc.d1786[991]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3229]))[16] = SCM_WORD(scm__rc.d1786[971]);
  scm__rc.d1786[994] = Scm_MakeIdentifier(scm__rc.d1786[989], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#read-from-string */
  scm__rc.d1786[995] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[88])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[90]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[90]))->debugInfo = scm__rc.d1786[995];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3247]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3247]))[6] = SCM_WORD(scm__rc.d1786[989]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3247]))[13] = SCM_WORD(scm__rc.d1786[994]);
  scm__rc.d1786[996] = Scm_MakeIdentifier(scm__rc.d1786[473], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#current-input-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1143]), scm__rc.d1786[924]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1144]), scm__rc.d1786[924]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1147]), scm__rc.d1786[2]);
  scm__rc.d1786[997] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1144]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[1148]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1149]), scm__rc.d1786[997]);
  scm__rc.d1786[998] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[89])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[91]))->name = scm__rc.d1786[924];/* (with-input-from-port with-input-from-port) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[91]))->debugInfo = scm__rc.d1786[998];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3262]))[5] = SCM_WORD(scm__rc.d1786[996]);
  scm__rc.d1786[999] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1144]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[1148]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1150]), scm__rc.d1786[999]);
  scm__rc.d1786[1000] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[90])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[92]))->name = scm__rc.d1786[924];/* (with-input-from-port with-input-from-port) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[92]))->debugInfo = scm__rc.d1786[1000];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3270]))[7] = SCM_WORD(scm__rc.d1786[996]);
  scm__rc.d1786[1002] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[308])),TRUE); /* values */
  scm__rc.d1786[1001] = Scm_MakeIdentifier(scm__rc.d1786[1002], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#values */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1151]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1154]), scm__rc.d1786[2]);
  scm__rc.d1786[1003] = Scm_MakeExtendedPair(scm__rc.d1786[924], SCM_OBJ(&scm__rc.d1787[1151]), SCM_OBJ(&scm__rc.d1787[1155]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1156]), scm__rc.d1786[1003]);
  scm__rc.d1786[1004] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[91])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[93]))->name = scm__rc.d1786[924];/* with-input-from-port */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[93]))->debugInfo = scm__rc.d1786[1004];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3281]))[27] = SCM_WORD(scm__rc.d1786[1001]);
  scm__rc.d1786[1005] = Scm_MakeIdentifier(scm__rc.d1786[924], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#with-input-from-port */
  scm__rc.d1786[1006] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[92])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[94]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[94]))->debugInfo = scm__rc.d1786[1006];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3312]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3312]))[6] = SCM_WORD(scm__rc.d1786[924]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3312]))[13] = SCM_WORD(scm__rc.d1786[1005]);
  scm__rc.d1786[1007] = Scm_MakeIdentifier(scm__rc.d1786[495], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#current-output-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1157]), scm__rc.d1786[934]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1158]), scm__rc.d1786[934]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1161]), scm__rc.d1786[2]);
  scm__rc.d1786[1008] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1158]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[1162]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1163]), scm__rc.d1786[1008]);
  scm__rc.d1786[1009] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[93])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[95]))->name = scm__rc.d1786[934];/* (with-output-to-port with-output-to-port) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[95]))->debugInfo = scm__rc.d1786[1009];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3327]))[5] = SCM_WORD(scm__rc.d1786[1007]);
  scm__rc.d1786[1010] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1158]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[1162]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1164]), scm__rc.d1786[1010]);
  scm__rc.d1786[1011] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[94])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[96]))->name = scm__rc.d1786[934];/* (with-output-to-port with-output-to-port) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[96]))->debugInfo = scm__rc.d1786[1011];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3335]))[7] = SCM_WORD(scm__rc.d1786[1007]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1167]), scm__rc.d1786[2]);
  scm__rc.d1786[1012] = Scm_MakeExtendedPair(scm__rc.d1786[934], SCM_OBJ(&scm__rc.d1787[1151]), SCM_OBJ(&scm__rc.d1787[1168]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1169]), scm__rc.d1786[1012]);
  scm__rc.d1786[1013] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[95])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[97]))->name = scm__rc.d1786[934];/* with-output-to-port */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[97]))->debugInfo = scm__rc.d1786[1013];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3346]))[27] = SCM_WORD(scm__rc.d1786[1001]);
  scm__rc.d1786[1014] = Scm_MakeIdentifier(scm__rc.d1786[934], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#with-output-to-port */
  scm__rc.d1786[1015] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[96])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[98]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[98]))->debugInfo = scm__rc.d1786[1015];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3377]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3377]))[6] = SCM_WORD(scm__rc.d1786[934]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3377]))[13] = SCM_WORD(scm__rc.d1786[1014]);
  scm__rc.d1786[1016] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[309])),TRUE); /* with-error-to-port */
  scm__rc.d1786[1018] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[310])),TRUE); /* current-error-port */
  scm__rc.d1786[1017] = Scm_MakeIdentifier(scm__rc.d1786[1018], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#current-error-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1170]), scm__rc.d1786[1016]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1171]), scm__rc.d1786[1016]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1174]), scm__rc.d1786[2]);
  scm__rc.d1786[1019] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1171]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[1175]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1176]), scm__rc.d1786[1019]);
  scm__rc.d1786[1020] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[97])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[99]))->name = scm__rc.d1786[1016];/* (with-error-to-port with-error-to-port) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[99]))->debugInfo = scm__rc.d1786[1020];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3392]))[5] = SCM_WORD(scm__rc.d1786[1017]);
  scm__rc.d1786[1021] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1171]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[1175]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1177]), scm__rc.d1786[1021]);
  scm__rc.d1786[1022] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[98])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[100]))->name = scm__rc.d1786[1016];/* (with-error-to-port with-error-to-port) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[100]))->debugInfo = scm__rc.d1786[1022];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3400]))[7] = SCM_WORD(scm__rc.d1786[1017]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1180]), scm__rc.d1786[2]);
  scm__rc.d1786[1023] = Scm_MakeExtendedPair(scm__rc.d1786[1016], SCM_OBJ(&scm__rc.d1787[1151]), SCM_OBJ(&scm__rc.d1787[1181]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1182]), scm__rc.d1786[1023]);
  scm__rc.d1786[1024] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[99])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[101]))->name = scm__rc.d1786[1016];/* with-error-to-port */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[101]))->debugInfo = scm__rc.d1786[1024];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3411]))[27] = SCM_WORD(scm__rc.d1786[1001]);
  scm__rc.d1786[1025] = Scm_MakeIdentifier(scm__rc.d1786[1016], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#with-error-to-port */
  scm__rc.d1786[1026] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[100])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[102]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[102]))->debugInfo = scm__rc.d1786[1026];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3442]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3442]))[6] = SCM_WORD(scm__rc.d1786[1016]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3442]))[13] = SCM_WORD(scm__rc.d1786[1025]);
  scm__rc.d1786[1027] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[311])),TRUE); /* with-ports */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1183]), scm__rc.d1786[1027]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1184]), scm__rc.d1786[1027]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1187]), scm__rc.d1786[2]);
  scm__rc.d1786[1028] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1184]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[1188]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1189]), scm__rc.d1786[1028]);
  scm__rc.d1786[1029] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[101])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[103]))->name = scm__rc.d1786[1027];/* (with-ports with-ports) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[103]))->debugInfo = scm__rc.d1786[1029];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3457]))[7] = SCM_WORD(scm__rc.d1786[996]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3457]))[17] = SCM_WORD(scm__rc.d1786[1007]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3457]))[25] = SCM_WORD(scm__rc.d1786[1017]);
  scm__rc.d1786[1030] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1184]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[1188]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1190]), scm__rc.d1786[1030]);
  scm__rc.d1786[1031] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[102])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[104]))->name = scm__rc.d1786[1027];/* (with-ports with-ports) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[104]))->debugInfo = scm__rc.d1786[1031];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3485]))[7] = SCM_WORD(scm__rc.d1786[996]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3485]))[18] = SCM_WORD(scm__rc.d1786[1007]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3485]))[29] = SCM_WORD(scm__rc.d1786[1017]);
  scm__rc.d1786[1032] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[312])),TRUE); /* eport */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1191]), scm__rc.d1786[1032]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1192]), scm__rc.d1786[137]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1193]), scm__rc.d1786[136]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1196]), scm__rc.d1786[2]);
  scm__rc.d1786[1033] = Scm_MakeExtendedPair(scm__rc.d1786[1027], SCM_OBJ(&scm__rc.d1787[1193]), SCM_OBJ(&scm__rc.d1787[1197]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1198]), scm__rc.d1786[1033]);
  scm__rc.d1786[1034] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[103])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[105]))->name = scm__rc.d1786[1027];/* with-ports */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[105]))->debugInfo = scm__rc.d1786[1034];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3518]))[31] = SCM_WORD(scm__rc.d1786[1001]);
  scm__rc.d1786[1035] = Scm_MakeIdentifier(scm__rc.d1786[1027], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#with-ports */
  scm__rc.d1786[1036] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[104])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[106]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[106]))->debugInfo = scm__rc.d1786[1036];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3553]))[3] = SCM_WORD(scm__rc.d1786[316]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3553]))[6] = SCM_WORD(scm__rc.d1786[1027]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3553]))[13] = SCM_WORD(scm__rc.d1786[1035]);
  scm__rc.d1786[1037] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[313])),TRUE); /* r6rs */
  scm__rc.d1786[1039] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[315])),TRUE); /* warn */
  scm__rc.d1786[1038] = Scm_MakeIdentifier(scm__rc.d1786[1039], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#warn */
  scm__rc.d1786[1040] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[316])),TRUE); /* sym */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1199]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1200]), scm__rc.d1786[1040]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1203]), scm__rc.d1786[2]);
  scm__rc.d1786[1041] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[317])),TRUE); /* unused-args */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1204]), scm__rc.d1786[1041]);
  scm__rc.d1786[1042] = Scm_MakeExtendedPair(SCM_FALSE, SCM_OBJ(&scm__rc.d1787[1200]), SCM_OBJ(&scm__rc.d1787[1206]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1207]), scm__rc.d1786[1042]);
  scm__rc.d1786[1043] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[105])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[107]))->debugInfo = scm__rc.d1786[1043];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3568]))[5] = SCM_WORD(scm__rc.d1786[1038]);
  scm__rc.d1786[1044] = Scm_MakeIdentifier(scm__rc.d1786[617], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#define-reader-directive */
  scm__rc.d1786[1045] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[106])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[108]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[108]))->debugInfo = scm__rc.d1786[1045];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3575]))[1] = SCM_WORD(scm__rc.d1786[1037]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3575]))[5] = SCM_WORD(scm__rc.d1786[1044]);
  scm__rc.d1786[1046] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[318])),TRUE); /* fold-case */
  scm__rc.d1786[1047] = Scm_MakeIdentifier(scm__rc.d1786[158], SCM_MODULE(Scm_GaucheInternalModule()), SCM_NIL); /* gauche.internal#port-case-fold */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1210]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1211]), scm__rc.d1786[1040]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1212]), scm__rc.d1786[1041]);
  scm__rc.d1786[1048] = Scm_MakeExtendedPair(SCM_FALSE, SCM_OBJ(&scm__rc.d1787[1200]), SCM_OBJ(&scm__rc.d1787[1214]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1215]), scm__rc.d1786[1048]);
  scm__rc.d1786[1049] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[107])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[109]))->debugInfo = scm__rc.d1786[1049];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3582]))[6] = SCM_WORD(scm__rc.d1786[1047]);
  scm__rc.d1786[1050] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[108])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[110]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[110]))->debugInfo = scm__rc.d1786[1050];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3592]))[1] = SCM_WORD(scm__rc.d1786[1046]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3592]))[5] = SCM_WORD(scm__rc.d1786[1044]);
  scm__rc.d1786[1051] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[319])),TRUE); /* no-fold-case */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1218]), scm__rc.d1786[2]);
  scm__rc.d1786[1052] = Scm_MakeExtendedPair(SCM_FALSE, SCM_OBJ(&scm__rc.d1787[1200]), SCM_OBJ(&scm__rc.d1787[1219]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1220]), scm__rc.d1786[1052]);
  scm__rc.d1786[1053] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[109])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[111]))->debugInfo = scm__rc.d1786[1053];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3599]))[5] = SCM_WORD(scm__rc.d1786[1047]);
  scm__rc.d1786[1054] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[110])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[112]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[112]))->debugInfo = scm__rc.d1786[1054];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3608]))[1] = SCM_WORD(scm__rc.d1786[1051]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3608]))[5] = SCM_WORD(scm__rc.d1786[1044]);
  scm__rc.d1786[1055] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[320])),TRUE); /* gauche-legacy */
  scm__rc.d1786[1056] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[321])),TRUE); /* legacy */
  scm__rc.d1786[1057] = Scm_MakeIdentifier(scm__rc.d1786[91], SCM_MODULE(scm__rc.d1786[368]), SCM_NIL); /* gauche.internal#port-attribute-set! */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1223]), scm__rc.d1786[2]);
  scm__rc.d1786[1058] = Scm_MakeExtendedPair(SCM_FALSE, SCM_OBJ(&scm__rc.d1787[1200]), SCM_OBJ(&scm__rc.d1787[1224]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1225]), scm__rc.d1786[1058]);
  scm__rc.d1786[1059] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[111])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[113]))->debugInfo = scm__rc.d1786[1059];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3615]))[4] = SCM_WORD(scm__rc.d1786[581]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3615]))[6] = SCM_WORD(scm__rc.d1786[1056]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3615]))[8] = SCM_WORD(scm__rc.d1786[1057]);
  scm__rc.d1786[1060] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[112])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[114]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[114]))->debugInfo = scm__rc.d1786[1060];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3625]))[1] = SCM_WORD(scm__rc.d1786[1055]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3625]))[5] = SCM_WORD(scm__rc.d1786[1044]);
  scm__rc.d1786[1061] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[322])),TRUE); /* r7rs */
  scm__rc.d1786[1062] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[323])),TRUE); /* strict-r7 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1228]), scm__rc.d1786[2]);
  scm__rc.d1786[1063] = Scm_MakeExtendedPair(SCM_FALSE, SCM_OBJ(&scm__rc.d1787[1200]), SCM_OBJ(&scm__rc.d1787[1229]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1230]), scm__rc.d1786[1063]);
  scm__rc.d1786[1064] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[113])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[115]))->debugInfo = scm__rc.d1786[1064];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3632]))[4] = SCM_WORD(scm__rc.d1786[581]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3632]))[6] = SCM_WORD(scm__rc.d1786[1062]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3632]))[8] = SCM_WORD(scm__rc.d1786[1057]);
  scm__rc.d1786[1065] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[114])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[116]))->name = scm__rc.d1786[324];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[116]))->debugInfo = scm__rc.d1786[1065];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3642]))[1] = SCM_WORD(scm__rc.d1786[1061]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[3642]))[5] = SCM_WORD(scm__rc.d1786[1044]);
  Scm_VMExecuteToplevels(toplevels);
  scm__rc.d1786[1372] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[324])),TRUE); /* l */
  scm__rc.d1786[1373] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[325])),TRUE); /* format */
  scm__rc.d1786[1374] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[326])),TRUE); /* if-let1 */
  scm__rc.d1786[1375] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[327])),TRUE); /* unwind-protect */
  scm__rc.d1786[1376] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[328])),TRUE); /* define-in-module */
  scm__rc.d1786[1377] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[329])),TRUE); /* setter */
  scm__rc.d1786[1378] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[330])),TRUE); /* set! */
  scm__rc.d1786[1379] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[331])),FALSE); /* G1804 */
  scm__rc.d1786[1380] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[332])),FALSE); /* G1805 */
  scm__rc.d1786[1381] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[333])),FALSE); /* rest1803 */
  scm__rc.d1786[1382] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[334])),FALSE); /* G1807 */
  scm__rc.d1786[1383] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[335])),FALSE); /* G1810 */
  scm__rc.d1786[1384] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[336])),FALSE); /* G1809 */
  scm__rc.d1786[1385] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[337])),FALSE); /* G1808 */
  scm__rc.d1786[1386] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[338])),FALSE); /* rest1806 */
  scm__rc.d1786[1387] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[339])),FALSE); /* G1812 */
  scm__rc.d1786[1388] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[340])),FALSE); /* G1813 */
  scm__rc.d1786[1389] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[341])),FALSE); /* rest1811 */
  scm__rc.d1786[1390] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[342])),TRUE); /* out */
  scm__rc.d1786[1391] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[343])),TRUE); /* result */
  scm__rc.d1786[1392] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[344])),TRUE); /* <> */
  scm__rc.d1786[1393] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[345])),TRUE); /* s */
  scm__rc.d1786[1394] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[346])),TRUE); /* do */
  scm__rc.d1786[1395] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[347])),FALSE); /* limit1818 */
  scm__rc.d1786[1396] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[348])),TRUE); /* dotimes */
  scm__rc.d1786[1397] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[349])),FALSE); /* limit1817 */
  scm__rc.d1786[1398] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[350])),TRUE); /* vector-length */
  scm__rc.d1786[1399] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[351])),TRUE); /* vector-ref */
  scm__rc.d1786[1400] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[352])),TRUE); /* vector? */
  scm__rc.d1786[1401] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[353])),TRUE); /* pair? */
  scm__rc.d1786[1402] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[354])),TRUE); /* string? */
  scm__rc.d1786[1403] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[355])),TRUE); /* symbol? */
  scm__rc.d1786[1404] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[356])),FALSE); /* G1821 */
  scm__rc.d1786[1405] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[357])),FALSE); /* G1822 */
  scm__rc.d1786[1406] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[358])),FALSE); /* rest1820 */
  scm__rc.d1786[1407] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[359])),FALSE); /* loop1829 */
  scm__rc.d1786[1408] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[360])),FALSE); /* args1828 */
  scm__rc.d1786[1409] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[361])),FALSE); /* G1830 */
  scm__rc.d1786[1410] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[362])),FALSE); /* G1831 */
  scm__rc.d1786[1411] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[363])),FALSE); /* G1832 */
  scm__rc.d1786[1412] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[364])),FALSE); /* G1833 */
  scm__rc.d1786[1413] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[365])),FALSE); /* G1834 */
  scm__rc.d1786[1414] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[366])),FALSE); /* G1835 */
  scm__rc.d1786[1415] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[367])),FALSE); /* G1836 */
  scm__rc.d1786[1416] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[368])),FALSE); /* G1837 */
  scm__rc.d1786[1417] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[369])),FALSE); /* G1838 */
  scm__rc.d1786[1418] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[370])),FALSE); /* G1839 */
  scm__rc.d1786[1419] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[371])),FALSE); /* G1840 */
  scm__rc.d1786[1420] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[372])),FALSE); /* G1841 */
  scm__rc.d1786[1421] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[373])),FALSE); /* G1842 */
  scm__rc.d1786[1422] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[374])),FALSE); /* G1843 */
  scm__rc.d1786[1423] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[375])),TRUE); /* tmp */
  scm__rc.d1786[1424] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[376])),TRUE); /* case */
  scm__rc.d1786[1425] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[377])),TRUE); /* arg */
  scm__rc.d1786[1426] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[378])),TRUE); /* k-alt */
  scm__rc.d1786[1427] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[379])),FALSE); /* rest1827 */
  scm__rc.d1786[1428] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[380])),FALSE); /* loop1846 */
  scm__rc.d1786[1429] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[381])),FALSE); /* args1845 */
  scm__rc.d1786[1430] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[382])),FALSE); /* G1847 */
  scm__rc.d1786[1431] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[383])),FALSE); /* G1848 */
  scm__rc.d1786[1432] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[384])),FALSE); /* G1849 */
  scm__rc.d1786[1433] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[385])),FALSE); /* G1850 */
  scm__rc.d1786[1434] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[386])),FALSE); /* G1851 */
  scm__rc.d1786[1435] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[387])),FALSE); /* G1852 */
  scm__rc.d1786[1436] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[388])),FALSE); /* G1853 */
  scm__rc.d1786[1437] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[389])),FALSE); /* G1854 */
  scm__rc.d1786[1438] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[390])),FALSE); /* G1855 */
  scm__rc.d1786[1439] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[391])),FALSE); /* G1856 */
  scm__rc.d1786[1440] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[392])),FALSE); /* G1857 */
  scm__rc.d1786[1441] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[393])),FALSE); /* G1858 */
  scm__rc.d1786[1442] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[394])),FALSE); /* G1859 */
  scm__rc.d1786[1443] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[395])),FALSE); /* G1860 */
  scm__rc.d1786[1444] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[396])),TRUE); /* slot-ref */
  scm__rc.d1786[1445] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[397])),TRUE); /* select */
  scm__rc.d1786[1446] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[398])),FALSE); /* rest1844 */
  scm__rc.d1786[1447] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[399])),TRUE); /* let-syntax */
  scm__rc.d1786[1448] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[400])),TRUE); /* syntax-rules */
  scm__rc.d1786[1449] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[401])),TRUE); /* _ */
  scm__rc.d1786[1450] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[402])),TRUE); /* and-let* */
  scm__rc.d1786[1451] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[403])),TRUE); /* e */
  scm__rc.d1786[1452] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[404])),TRUE); /* in */
  scm__rc.d1786[1453] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[405])),FALSE); /* G1862 */
  scm__rc.d1786[1454] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[406])),FALSE); /* G1863 */
  scm__rc.d1786[1455] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[407])),FALSE); /* rest1861 */
  scm__rc.d1786[1456] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[408])),TRUE); /* after */
  scm__rc.d1786[1457] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[409])),TRUE); /* dynamic-wind */
  scm__rc.d1786[1458] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[410])),TRUE); /* before */
  scm__rc.d1786[1459] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[411])),TRUE); /* %with-ports */
}
