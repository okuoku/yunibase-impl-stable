/* Generated automatically from libnet.scm.  DO NOT EDIT */
#define LIBGAUCHE_BODY 
#include <gauche.h>
#include <gauche/code.h>
static unsigned char uvector__00001[] = {
 0u, 3u, 133u, 134u, 0u, 255u, 0u, 64u, 36u, 64u, 68u, 48u, 192u,
100u, 48u, 136u, 18u, 5u, 129u, 160u, 120u, 34u, 9u, 130u, 137u, 36u,
112u, 64u, 193u, 5u, 134u, 24u, 48u, 146u, 56u, 0u, 96u, 76u, 55u,
201u, 0u,};
#include "gauche/priv/configP.h"
#include "gauche/net.h"
#if defined(HAVE_IPV6)
static ScmObj in6_addr(ScmSockAddrIn6* a);
#endif /* defined(HAVE_IPV6) */
static ScmObj libnetmake_socket(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetmake_socket__STUB, 2, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetmake_socket, NULL, NULL);

static ScmObj libnetsocket_address(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_address__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_address, NULL, NULL);

static ScmObj libnetsocket_status(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_status__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_status, NULL, NULL);

static ScmObj libnetsocket_fd(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_fd__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_fd, NULL, NULL);

static ScmObj libnetsocket_input_port(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_input_port__STUB, 1, 1,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_input_port, NULL, NULL);

static ScmObj libnetsocket_output_port(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_output_port__STUB, 1, 1,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_output_port, NULL, NULL);

static ScmObj libnetsocket_shutdown(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_shutdown__STUB, 1, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_shutdown, NULL, NULL);

static ScmObj libnetsocket_close(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_close__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_close, NULL, NULL);

static ScmObj libnetsocket_bind(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_bind__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_bind, NULL, NULL);

static ScmObj libnetsocket_listen(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_listen__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_listen, NULL, NULL);

static ScmObj libnetsocket_accept(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_accept__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_accept, NULL, NULL);

static ScmObj libnetsocket_connect(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_connect__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_connect, NULL, NULL);

static ScmObj libnetsocket_getsockname(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_getsockname__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_getsockname, NULL, NULL);

static ScmObj libnetsocket_getpeername(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_getpeername__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_getpeername, NULL, NULL);

static ScmObj libnetsocket_send(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libnetsocket_send__STUB, 2, 2,SCM_FALSE,libnetsocket_send, NULL, NULL);

static ScmObj libnetsocket_sendto(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libnetsocket_sendto__STUB, 3, 2,SCM_FALSE,libnetsocket_sendto, NULL, NULL);

static ScmObj libnetsocket_sendmsg(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libnetsocket_sendmsg__STUB, 2, 2,SCM_FALSE,libnetsocket_sendmsg, NULL, NULL);

static ScmObj libnetsocket_recv(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_recv__STUB, 2, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_recv, NULL, NULL);

static ScmObj libnetsocket_recvX(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_recvX__STUB, 2, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_recvX, NULL, NULL);

static ScmObj libnetsocket_recvfrom(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_recvfrom__STUB, 2, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_recvfrom, NULL, NULL);

static ScmObj libnetsocket_recvfromX(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libnetsocket_recvfromX__STUB, 3, 2,SCM_FALSE,libnetsocket_recvfromX, NULL, NULL);

static ScmObj libnetsocket_buildmsg(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libnetsocket_buildmsg__STUB, 4, 2,SCM_FALSE,libnetsocket_buildmsg, NULL, NULL);

static ScmObj libnetsocket_setsockopt(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libnetsocket_setsockopt__STUB, 4, 0,SCM_FALSE,libnetsocket_setsockopt, NULL, NULL);

static ScmObj libnetsocket_getsockopt(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsocket_getsockopt__STUB, 4, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsocket_getsockopt, NULL, NULL);

static ScmObj libnetsocket_ioctl(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libnetsocket_ioctl__STUB, 3, 0,SCM_FALSE,libnetsocket_ioctl, NULL, NULL);

static ScmObj libnetsys_gethostbyname(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsys_gethostbyname__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsys_gethostbyname, NULL, NULL);

static ScmObj libnetsys_gethostbyaddr(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsys_gethostbyaddr__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsys_gethostbyaddr, NULL, NULL);

static ScmObj libnetsys_getprotobyname(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsys_getprotobyname__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsys_getprotobyname, NULL, NULL);

static ScmObj libnetsys_getprotobynumber(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsys_getprotobynumber__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsys_getprotobynumber, NULL, NULL);

static ScmObj libnetsys_getservbyname(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsys_getservbyname__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsys_getservbyname, NULL, NULL);

static ScmObj libnetsys_getservbyport(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsys_getservbyport__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsys_getservbyport, NULL, NULL);

static ScmObj libnetsys_ntohl(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsys_ntohl__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsys_ntohl, NULL, NULL);

static ScmObj libnetsys_ntohs(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsys_ntohs__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsys_ntohs, NULL, NULL);

static ScmObj libnetsys_htonl(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsys_htonl__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsys_htonl, NULL, NULL);

static ScmObj libnetsys_htons(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsys_htons__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsys_htons, NULL, NULL);

extern ScmObj addrinfo_allocate(ScmClass* klass,ScmObj intargs);
#if defined(HAVE_IPV6)
static ScmObj libnetsys_getaddrinfo(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libnetsys_getaddrinfo__STUB, 3, 0,SCM_FALSE,libnetsys_getaddrinfo, NULL, NULL);

#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
static ScmObj libnetsys_getnameinfo(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetsys_getnameinfo__STUB, 1, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetsys_getnameinfo, NULL, NULL);

#endif /* defined(HAVE_IPV6) */
static void export_bindings();
static ScmObj libnetinet_checksum(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetinet_checksum__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetinet_checksum, NULL, NULL);

static ScmObj libnetinet_string_TOaddress(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetinet_string_TOaddress__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetinet_string_TOaddress, NULL, NULL);

static ScmObj libnetinet_string_TOaddressX(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libnetinet_string_TOaddressX__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libnetinet_string_TOaddressX, NULL, NULL);

static ScmObj libnetinet_address_TOstring(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libnetinet_address_TOstring__STUB, 2, 0,SCM_FALSE,libnetinet_address_TOstring, NULL, NULL);

static unsigned char uvector__00002[] = {
 0u, 3u, 133u, 134u, 0u, 255u, 6u, 193u, 198u, 15u, 224u, 164u, 142u,
14u, 24u, 19u, 1u, 194u, 56u, 16u, 96u, 76u, 7u, 8u, 224u, 1u, 129u,
48u, 28u, 36u,};
static unsigned char uvector__00003[] = {
 0u, 3u, 136u, 6u, 6u, 104u, 64u, 66u, 33u, 134u, 12u, 33u, 134u, 18u,
36u, 145u, 195u, 67u, 2u, 96u, 98u, 71u, 7u, 12u, 9u, 128u, 225u, 28u,
8u, 48u, 38u, 3u, 132u, 112u, 0u, 192u, 152u, 14u, 18u,};
static unsigned char uvector__00004[] = {
 0u, 3u, 133u, 6u, 6u, 104u, 76u, 66u, 130u, 21u, 126u, 33u, 165u,
184u, 146u, 71u, 7u, 12u, 9u, 128u, 225u, 28u, 8u, 48u, 38u, 3u, 132u,
112u, 0u, 192u, 152u, 14u, 18u,};
static unsigned char uvector__00005[] = {
 0u, 3u, 194u, 188u, 24u, 33u, 96u, 249u, 11u, 152u, 98u, 25u, 134u,
161u, 178u, 71u, 133u, 116u, 48u, 36u, 54u, 71u, 133u, 112u, 48u, 36u,
52u, 71u, 133u, 108u, 48u, 36u, 50u, 71u, 133u, 104u, 48u, 36u, 48u,
71u, 133u, 100u, 48u, 38u, 5u, 228u, 120u, 86u, 3u, 2u, 66u, 228u,
120u, 85u, 131u, 4u, 57u, 14u, 134u, 136u, 92u, 146u, 60u, 42u, 161u,
129u, 49u, 204u, 35u, 194u, 168u, 24u, 18u, 23u, 35u, 194u, 160u, 24u,
19u, 27u, 82u, 60u, 41u, 161u, 130u, 22u, 15u, 144u, 185u, 134u, 33u,
152u, 104u, 59u, 194u, 228u, 145u, 225u, 76u, 12u, 9u, 151u, 9u, 30u,
20u, 176u, 192u, 144u, 185u, 30u, 20u, 160u, 192u, 144u, 209u, 30u,
20u, 144u, 192u, 144u, 201u, 30u, 20u, 128u, 192u, 144u, 193u, 30u,
20u, 112u, 192u, 153u, 85u, 145u, 225u, 70u, 12u, 9u, 11u, 145u, 225u,
67u, 12u, 2u, 30u, 33u, 50u, 158u, 36u, 120u, 80u, 131u, 2u, 67u,
228u, 120u, 71u, 195u, 4u, 44u, 31u, 33u, 115u, 12u, 67u, 33u, 222u,
23u, 48u, 217u, 35u, 194u, 60u, 24u, 18u, 27u, 35u, 194u, 58u, 24u,
19u, 66u, 145u, 136u, 240u, 142u, 6u, 4u, 133u, 200u, 240u, 141u,
134u, 4u, 134u, 72u, 240u, 141u, 6u, 4u, 134u, 8u, 240u, 140u, 134u,
4u, 208u, 157u, 162u, 60u, 35u, 1u, 129u, 33u, 114u, 60u, 34u, 161u,
128u, 68u, 4u, 38u, 132u, 230u, 146u, 60u, 34u, 129u, 129u, 33u, 242u,
60u, 34u, 33u, 130u, 22u, 15u, 144u, 185u, 134u, 3u, 188u, 46u, 97u,
168u, 108u, 145u, 225u, 16u, 12u, 9u, 13u, 145u, 225u, 15u, 12u, 9u,
13u, 17u, 225u, 14u, 12u, 9u, 161u, 219u, 164u, 120u, 67u, 67u, 2u,
66u, 228u, 120u, 67u, 3u, 2u, 67u, 4u, 120u, 66u, 195u, 2u, 104u,
116u, 217u, 30u, 16u, 160u, 192u, 144u, 185u, 30u, 16u, 112u, 192u,
34u, 18u, 19u, 67u, 163u, 137u, 30u, 16u, 96u, 192u, 144u, 249u, 30u,
16u, 48u, 193u, 11u, 7u, 200u, 92u, 135u, 120u, 92u, 195u, 48u, 212u,
54u, 72u, 240u, 129u, 6u, 4u, 134u, 200u, 240u, 128u, 134u, 4u, 134u,
136u, 240u, 128u, 6u, 4u, 134u, 72u, 239u, 225u, 129u, 52u, 78u, 80u,
142u, 252u, 24u, 18u, 23u, 35u, 190u, 134u, 4u, 209u, 53u, 194u, 59u,
224u, 96u, 72u, 92u, 142u, 242u, 24u, 4u, 68u, 66u, 104u, 154u, 121u,
35u, 188u, 6u, 4u, 135u, 200u, 238u, 225u, 130u, 35u, 17u, 32u, 104u,
133u, 201u, 9u, 162u, 218u, 65u, 52u, 73u, 60u, 38u, 134u, 199u, 4u,
208u, 141u, 48u, 210u, 30u, 130u, 99u, 106u, 19u, 2u, 34u, 73u, 29u,
212u, 48u, 38u, 139u, 233u, 145u, 221u, 3u, 2u, 104u, 192u, 1u, 29u,
204u, 48u, 36u, 46u, 71u, 113u, 12u, 9u, 162u, 250u, 100u, 118u, 224u,
192u, 207u, 18u, 194u, 228u, 142u, 218u, 24u, 18u, 23u, 35u, 180u,
134u, 6u, 176u, 213u, 11u, 146u, 71u, 104u, 12u, 9u, 163u, 131u, 228u,
118u, 112u, 192u, 144u, 185u, 29u, 144u, 48u, 51u, 196u, 196u, 142u,
190u, 24u, 34u, 120u, 162u, 34u, 12u, 162u, 41u, 138u, 136u, 117u,
138u, 226u, 163u, 21u, 24u, 134u, 44u, 136u, 34u, 216u, 122u, 46u,
36u, 117u, 224u, 192u, 145u, 113u, 29u, 108u, 48u, 36u, 90u, 71u, 88u,
12u, 9u, 22u, 17u, 213u, 3u, 2u, 69u, 68u, 117u, 16u, 192u, 154u, 60u,
144u, 71u, 80u, 12u, 9u, 21u, 17u, 211u, 131u, 2u, 69u, 100u, 116u,
192u, 192u, 154u, 60u, 144u, 71u, 74u, 12u, 9u, 163u, 192u, 100u,
116u, 128u, 192u, 154u, 60u, 28u, 71u, 71u, 12u, 9u, 21u, 17u, 209u,
67u, 2u, 104u, 240u, 113u, 29u, 4u, 48u, 36u, 80u, 71u, 63u, 12u, 12u,
176u, 128u, 77u, 29u, 194u, 9u, 163u, 170u, 36u, 142u, 122u, 24u, 18u,
16u, 35u, 157u, 134u, 4u, 134u, 200u, 230u, 193u, 129u, 148u, 69u,
240u, 217u, 224u, 16u, 217u, 35u, 154u, 6u, 4u, 210u, 88u, 146u, 57u,
152u, 96u, 72u, 108u, 142u, 98u, 24u, 19u, 73u, 98u, 72u, 229u, 225u,
129u, 33u, 162u, 57u, 80u, 96u, 101u, 17u, 124u, 52u, 120u, 4u, 52u,
72u, 229u, 1u, 129u, 52u, 158u, 108u, 142u, 78u, 24u, 18u, 26u, 35u,
146u, 134u, 4u, 210u, 121u, 178u, 57u, 24u, 96u, 72u, 100u, 142u, 62u,
24u, 18u, 48u, 35u, 142u, 134u, 6u, 81u, 23u, 195u, 38u, 48u, 134u,
73u, 28u, 108u, 48u, 38u, 149u, 11u, 145u, 198u, 131u, 2u, 67u, 36u,
113u, 128u, 192u, 154u, 84u, 46u, 71u, 22u, 12u, 9u, 12u, 17u, 196u,
67u, 3u, 40u, 139u, 225u, 131u, 192u, 33u, 130u, 71u, 15u, 12u, 9u,
165u, 137u, 4u, 112u, 224u, 192u, 144u, 193u, 28u, 48u, 48u, 38u,
150u, 36u, 17u, 194u, 131u, 3u, 92u, 46u, 72u, 225u, 33u, 129u, 33u,
114u, 56u, 0u, 96u, 72u, 196u, 134u, 32u, 192u, 198u, 35u, 33u, 21u,
112u, 2u, 34u, 200u, 192u, 136u, 183u, 128u, 17u, 23u, 112u, 2u, 66u,
105u, 32u, 25u, 36u,};
static unsigned char uvector__00006[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 25u, 198u, 66u, 42u, 224u, 4u, 69u, 145u,
129u, 17u, 111u, 0u, 34u, 46u, 224u, 4u, 134u, 88u, 64u, 68u, 241u,
68u, 68u, 25u, 68u, 83u, 21u, 16u, 235u, 21u, 197u, 70u, 42u, 49u,
12u, 89u, 16u, 69u, 176u, 244u, 92u, 67u, 60u, 76u, 73u, 35u, 132u,
134u, 6u, 48u, 152u, 24u, 166u, 21u, 36u, 112u, 112u, 192u, 152u, 14u,
17u, 192u, 131u, 2u, 96u, 56u, 71u, 0u, 12u, 9u, 128u, 225u, 32u,};
static unsigned char uvector__00007[] = {
 0u, 3u, 143u, 6u, 6u, 120u, 210u, 53u, 36u, 113u, 208u, 192u, 145u,
169u, 28u, 96u, 48u, 36u, 108u, 71u, 21u, 12u, 2u, 55u, 49u, 177u,
35u, 138u, 6u, 4u, 135u, 200u, 226u, 33u, 129u, 35u, 130u, 56u, 112u,
96u, 17u, 201u, 142u, 9u, 28u, 52u, 48u, 36u, 62u, 71u, 10u, 12u, 9u,
29u, 17u, 193u, 195u, 0u, 142u, 204u, 116u, 72u, 224u, 193u, 129u,
33u, 242u, 56u, 40u, 96u, 136u, 196u, 121u, 26u, 144u, 152u, 226u,
132u, 196u, 224u, 38u, 21u, 97u, 164u, 38u, 3u, 132u, 145u, 192u,
195u, 2u, 100u, 106u, 71u, 2u, 12u, 9u, 26u, 145u, 192u, 3u, 2u, 100u,
106u, 67u, 16u, 96u, 99u, 17u, 169u, 9u, 145u, 65u, 36u,};
static unsigned char uvector__00008[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 30u, 198u, 164u, 68u, 98u, 60u, 141u, 72u,
17u, 217u, 142u, 136u, 17u, 201u, 142u, 8u, 17u, 185u, 141u, 136u,
105u, 12u, 241u, 164u, 106u, 73u, 36u, 112u, 144u, 192u, 198u, 19u,
3u, 20u, 192u, 248u, 142u, 14u, 24u, 19u, 1u, 194u, 56u, 16u, 96u,
76u, 7u, 8u, 224u, 1u, 129u, 48u, 28u, 36u,};
static unsigned char uvector__00009[] = {
 0u, 3u, 191u, 6u, 6u, 120u, 250u, 63u, 36u, 119u, 208u, 192u, 145u,
249u, 29u, 224u, 48u, 72u, 17u, 248u, 104u, 144u, 73u, 35u, 187u,
134u, 4u, 194u, 96u, 142u, 236u, 24u, 18u, 63u, 35u, 185u, 6u, 9u, 8u,
52u, 72u, 36u, 145u, 220u, 67u, 2u, 98u, 98u, 71u, 111u, 12u, 9u,
137u, 33u, 29u, 176u, 48u, 72u, 114u, 9u, 35u, 181u, 134u, 4u, 144u,
72u, 237u, 33u, 128u, 56u, 9u, 18u, 63u, 33u, 49u, 193u, 9u, 137u,
33u, 9u, 132u, 9u, 35u, 180u, 6u, 4u, 200u, 176u, 142u, 206u, 24u,
18u, 63u, 35u, 178u, 6u, 9u, 22u, 63u, 36u, 118u, 48u, 192u, 145u,
249u, 29u, 132u, 48u, 9u, 26u, 63u, 145u, 200u, 76u, 191u, 9u, 29u,
128u, 48u, 38u, 107u, 132u, 117u, 224u, 192u, 146u, 57u, 29u, 116u,
48u, 36u, 126u, 71u, 90u, 12u, 18u, 4u, 145u, 36u, 146u, 58u, 200u,
96u, 73u, 36u, 142u, 176u, 24u, 18u, 72u, 35u, 169u, 6u, 8u, 114u,
74u, 146u, 36u, 146u, 71u, 81u, 12u, 9u, 36u, 145u, 212u, 3u, 2u, 73u,
4u, 116u, 192u, 192u, 154u, 19u, 8u, 71u, 71u, 12u, 18u, 36u, 146u,
72u, 232u, 193u, 129u, 36u, 146u, 58u, 0u, 96u, 144u, 164u, 146u, 71u,
63u, 12u, 9u, 36u, 145u, 207u, 67u, 2u, 104u, 94u, 129u, 28u, 236u,
48u, 55u, 135u, 1u, 34u, 73u, 4u, 59u, 4u, 208u, 189u, 0u, 154u, 22u,
38u, 72u, 77u, 9u, 132u, 36u, 115u, 160u, 192u, 154u, 26u, 46u, 71u,
57u, 12u, 9u, 36u, 17u, 206u, 3u, 3u, 32u, 18u, 72u, 101u, 13u, 114u,
89u, 110u, 13u, 18u, 89u, 36u, 132u, 208u, 208u, 16u, 154u, 16u, 166u,
72u, 230u, 225u, 129u, 52u, 62u, 76u, 142u, 100u, 24u, 19u, 67u, 212u,
200u, 230u, 33u, 129u, 36u, 178u, 57u, 128u, 96u, 100u, 2u, 72u, 12u,
161u, 174u, 76u, 45u, 193u, 162u, 76u, 36u, 137u, 44u, 50u, 134u,
185u, 48u, 134u, 21u, 33u, 170u, 76u, 36u, 144u, 154u, 30u, 26u, 72u,
229u, 225u, 129u, 52u, 75u, 120u, 142u, 84u, 24u, 19u, 68u, 165u, 8u,
229u, 33u, 129u, 36u, 18u, 57u, 56u, 96u, 77u, 18u, 30u, 35u, 145u,
6u, 4u, 209u, 29u, 226u, 57u, 8u, 96u, 73u, 4u, 142u, 60u, 24u, 30u,
227u, 240u, 195u, 28u, 146u, 71u, 29u, 12u, 9u, 31u, 145u, 198u, 131u,
4u, 155u, 39u, 18u, 56u, 200u, 96u, 73u, 56u, 142u, 46u, 24u, 25u,
228u, 249u, 56u, 145u, 197u, 131u, 2u, 73u, 196u, 113u, 32u, 192u,
154u, 47u, 20u, 71u, 14u, 12u, 13u, 226u, 68u, 147u, 136u, 77u, 23u,
138u, 36u, 112u, 208u, 192u, 154u, 49u, 156u, 71u, 12u, 12u, 9u, 39u,
17u, 194u, 195u, 3u, 32u, 18u, 112u, 101u, 13u, 114u, 129u, 110u, 13u,
18u, 129u, 36u, 132u, 209u, 140u, 48u, 154u, 45u, 154u, 72u, 225u,
65u, 129u, 52u, 107u, 12u, 142u, 10u, 24u, 19u, 70u, 160u, 200u, 224u,
129u, 129u, 36u, 18u, 56u, 8u, 96u, 123u, 143u, 195u, 12u, 118u, 73u,
28u, 0u, 48u, 36u, 126u, 67u, 16u, 96u, 99u, 17u, 250u, 72u, 33u,
176u, 4u, 209u, 204u, 129u, 40u, 200u, 32u, 73u, 205u, 196u, 132u,
209u, 140u, 48u, 154u, 45u, 154u, 72u, 9u, 162u, 187u, 66u, 81u, 144u,
64u, 146u, 27u, 136u, 146u, 91u, 137u, 9u, 161u, 160u, 33u, 52u, 33u,
76u, 144u, 153u, 172u, 132u, 200u, 120u, 52u, 132u, 192u, 112u, 146u,
72u,};
static unsigned char uvector__00010[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 41u, 71u, 233u, 32u, 134u, 192u, 30u,
227u, 240u, 195u, 29u, 146u, 37u, 25u, 4u, 9u, 57u, 184u, 144u, 222u,
36u, 73u, 56u, 134u, 121u, 62u, 78u, 36u, 73u, 178u, 113u, 36u, 7u,
184u, 252u, 48u, 199u, 36u, 137u, 70u, 65u, 2u, 72u, 110u, 34u, 73u,
110u, 36u, 55u, 135u, 1u, 34u, 73u, 4u, 59u, 9u, 10u, 73u, 34u, 68u,
146u, 73u, 34u, 28u, 146u, 164u, 137u, 36u, 145u, 32u, 73u, 18u, 73u,
36u, 9u, 26u, 63u, 145u, 200u, 145u, 99u, 242u, 64u, 112u, 18u, 36u,
126u, 68u, 135u, 32u, 145u, 33u, 6u, 137u, 4u, 146u, 36u, 8u, 252u,
52u, 72u, 36u, 144u, 210u, 25u, 227u, 232u, 252u, 146u, 72u, 225u,
33u, 129u, 140u, 38u, 6u, 41u, 130u, 73u, 28u, 28u, 48u, 38u, 3u,
132u, 112u, 32u, 192u, 152u, 14u, 17u, 192u, 3u, 2u, 96u, 56u, 72u,};
static unsigned char uvector__00011[] = {
 0u, 3u, 137u, 6u, 4u, 148u, 200u, 226u, 1u, 130u, 84u, 148u, 227u,
82u, 71u, 15u, 12u, 9u, 26u, 145u, 195u, 131u, 2u, 74u, 100u, 112u,
192u, 192u, 152u, 35u, 17u, 194u, 195u, 3u, 32u, 18u, 152u, 149u, 68u,
123u, 26u, 153u, 88u, 146u, 19u, 4u, 105u, 76u, 145u, 194u, 67u, 2u,
98u, 64u, 71u, 7u, 12u, 9u, 43u, 17u, 193u, 67u, 2u, 98u, 90u, 71u,
4u, 12u, 9u, 26u, 145u, 192u, 131u, 2u, 98u, 90u, 71u, 0u, 12u, 9u,
137u, 1u, 12u, 65u, 129u, 140u, 70u, 164u, 74u, 242u, 152u, 76u, 72u,
2u, 96u, 140u, 73u, 32u,};
static unsigned char uvector__00012[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 34u, 198u, 164u, 74u, 242u, 152u, 149u,
68u, 123u, 26u, 153u, 88u, 137u, 82u, 83u, 141u, 73u, 36u, 112u, 144u,
192u, 198u, 19u, 3u, 20u, 192u, 248u, 142u, 14u, 24u, 19u, 1u, 194u,
56u, 16u, 96u, 76u, 7u, 8u, 224u, 1u, 129u, 48u, 28u, 36u,};
static unsigned char uvector__00013[] = {
 0u, 3u, 139u, 134u, 4u, 148u, 200u, 226u, 161u, 130u, 84u, 148u,
196u, 79u, 44u, 75u, 50u, 113u, 36u, 113u, 48u, 192u, 152u, 46u, 145u,
196u, 131u, 2u, 73u, 196u, 112u, 224u, 192u, 146u, 193u, 28u, 48u,
48u, 38u, 11u, 164u, 112u, 176u, 192u, 146u, 153u, 28u, 36u, 48u, 38u,
8u, 196u, 112u, 128u, 192u, 200u, 4u, 166u, 37u, 88u, 234u, 86u, 36u,
132u, 193u, 26u, 83u, 36u, 112u, 96u, 192u, 152u, 229u, 17u, 193u, 3u,
2u, 74u, 196u, 112u, 32u, 192u, 145u, 209u, 28u, 0u, 48u, 38u, 57u,
68u, 49u, 6u, 6u, 49u, 39u, 17u, 43u, 202u, 97u, 49u, 202u, 9u, 130u,
49u, 36u, 128u,};
static unsigned char uvector__00014[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 38u, 201u, 196u, 74u, 242u, 152u, 149u,
99u, 169u, 88u, 137u, 82u, 83u, 17u, 60u, 177u, 44u, 201u, 196u, 146u,
71u, 9u, 12u, 12u, 97u, 48u, 49u, 76u, 15u, 136u, 224u, 225u, 129u,
48u, 28u, 35u, 129u, 6u, 4u, 192u, 112u, 142u, 0u, 24u, 19u, 1u, 194u,
64u,};
static unsigned char uvector__00015[] = {
 0u, 3u, 128u, 134u, 4u, 150u, 136u, 224u, 1u, 129u, 37u, 178u, 24u,
131u, 3u, 24u, 150u, 200u, 108u, 13u, 34u, 92u, 150u, 165u, 178u,
220u, 73u, 36u,};
static unsigned char uvector__00016[] = {
 0u, 3u, 137u, 6u, 4u, 148u, 200u, 226u, 1u, 130u, 84u, 148u, 229u,
210u, 71u, 15u, 12u, 9u, 46u, 145u, 195u, 131u, 2u, 74u, 100u, 112u,
192u, 192u, 152u, 35u, 17u, 194u, 195u, 3u, 32u, 18u, 152u, 149u, 68u,
123u, 46u, 153u, 88u, 146u, 19u, 4u, 105u, 76u, 145u, 194u, 67u, 2u,
98u, 64u, 71u, 7u, 12u, 9u, 43u, 17u, 193u, 67u, 2u, 98u, 90u, 71u,
4u, 12u, 9u, 46u, 145u, 192u, 131u, 2u, 98u, 90u, 71u, 0u, 12u, 9u,
137u, 1u, 12u, 65u, 129u, 141u, 68u, 175u, 41u, 132u, 196u, 128u, 38u,
8u, 196u, 146u,};
static unsigned char uvector__00017[] = {
 0u, 3u, 133u, 6u, 9u, 120u, 49u, 137u, 108u, 134u, 192u, 210u, 37u,
201u, 106u, 91u, 45u, 196u, 144u, 198u, 162u, 87u, 148u, 196u, 170u,
35u, 217u, 116u, 202u, 196u, 74u, 146u, 156u, 186u, 73u, 151u, 223u,
137u, 28u, 12u, 48u, 38u, 17u, 228u, 112u, 0u, 192u, 152u, 20u, 144u,
196u, 24u, 24u, 196u, 186u, 68u, 192u, 37u, 176u, 152u, 39u, 144u,
152u, 79u, 18u, 72u,};
static unsigned char uvector__00018[] = {
 0u, 3u, 141u, 134u, 4u, 148u, 200u, 227u, 33u, 130u, 97u, 150u, 137u,
28u, 96u, 48u, 36u, 180u, 71u, 22u, 12u, 9u, 130u, 49u, 28u, 72u, 48u,
55u, 202u, 97u, 48u, 70u, 36u, 113u, 16u, 192u, 146u, 153u, 28u, 64u,
48u, 50u, 1u, 41u, 137u, 138u, 99u, 19u, 36u, 145u, 36u, 146u, 72u,
76u, 52u, 101u, 50u, 71u, 14u, 12u, 9u, 138u, 225u, 28u, 48u, 48u,
38u, 46u, 100u, 112u, 176u, 192u, 146u, 73u, 28u, 40u, 48u, 36u, 144u,
71u, 8u, 12u, 9u, 139u, 153u, 28u, 28u, 48u, 36u, 198u, 71u, 5u, 12u,
9u, 138u, 225u, 28u, 8u, 48u, 36u, 180u, 71u, 1u, 12u, 12u, 128u, 75u,
77u, 196u, 134u, 97u, 49u, 203u, 164u, 76u, 2u, 91u, 13u, 34u, 92u,
150u, 165u, 178u, 220u, 72u, 149u, 229u, 49u, 42u, 136u, 246u, 93u,
50u, 177u, 18u, 164u, 167u, 46u, 146u, 72u, 149u, 229u, 48u, 152u,
174u, 4u, 195u, 68u, 146u, 24u, 131u, 3u, 24u, 146u, 36u, 146u, 27u,
101u, 166u, 224u, 153u, 178u, 4u, 208u, 137u, 130u, 73u, 0u,};
static unsigned char uvector__00019[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 32u, 73u, 18u, 73u, 13u, 178u, 211u, 112u,
102u, 19u, 28u, 186u, 68u, 192u, 37u, 176u, 210u, 37u, 201u, 106u,
91u, 45u, 196u, 137u, 94u, 83u, 18u, 168u, 143u, 101u, 211u, 43u, 17u,
42u, 74u, 114u, 233u, 36u, 137u, 94u, 83u, 19u, 20u, 198u, 38u, 73u,
34u, 73u, 36u, 55u, 202u, 98u, 97u, 150u, 137u, 36u, 145u, 194u, 67u,
3u, 24u, 76u, 12u, 83u, 4u, 130u, 56u, 56u, 96u, 76u, 7u, 8u, 224u,
65u, 129u, 48u, 28u, 35u, 128u, 6u, 4u, 192u, 112u, 144u,};
static unsigned char uvector__00020[] = {
 0u, 3u, 194u, 8u, 24u, 25u, 227u, 232u, 252u, 145u, 225u, 3u, 12u,
9u, 31u, 145u, 223u, 195u, 3u, 172u, 203u, 31u, 200u, 36u, 142u, 252u,
24u, 18u, 65u, 35u, 190u, 134u, 4u, 143u, 200u, 239u, 97u, 129u, 38u,
82u, 59u, 200u, 96u, 18u, 20u, 126u, 66u, 97u, 26u, 72u, 238u, 225u,
129u, 49u, 83u, 35u, 187u, 6u, 4u, 143u, 200u, 238u, 129u, 129u, 49u,
83u, 35u, 185u, 6u, 7u, 89u, 154u, 63u, 144u, 73u, 29u, 196u, 48u,
36u, 130u, 71u, 112u, 12u, 9u, 31u, 145u, 219u, 131u, 2u, 76u, 196u,
118u, 192u, 192u, 36u, 104u, 254u, 71u, 33u, 50u, 36u, 36u, 118u,
176u, 192u, 153u, 117u, 17u, 218u, 67u, 2u, 72u, 228u, 118u, 128u,
192u, 145u, 249u, 29u, 148u, 48u, 51u, 204u, 242u, 73u, 35u, 178u, 6u,
4u, 146u, 72u, 235u, 225u, 130u, 68u, 146u, 73u, 29u, 120u, 48u, 36u,
146u, 71u, 92u, 12u, 14u, 179u, 44u, 146u, 26u, 164u, 18u, 72u, 235u,
97u, 129u, 52u, 37u, 8u, 142u, 180u, 24u, 18u, 73u, 35u, 172u, 6u, 4u,
153u, 72u, 234u, 193u, 128u, 59u, 9u, 10u, 73u, 33u, 52u, 33u, 24u,
132u, 208u, 144u, 50u, 71u, 84u, 12u, 9u, 161u, 89u, 132u, 117u, 48u,
192u, 146u, 73u, 29u, 68u, 48u, 38u, 133u, 102u, 17u, 211u, 195u, 3u,
172u, 209u, 36u, 134u, 169u, 4u, 146u, 58u, 112u, 96u, 77u, 13u, 12u,
35u, 166u, 134u, 4u, 146u, 72u, 233u, 97u, 129u, 38u, 130u, 58u, 72u,
96u, 76u, 240u, 72u, 233u, 1u, 129u, 36u, 146u, 58u, 32u, 96u, 76u,
240u, 72u, 232u, 1u, 129u, 188u, 77u, 34u, 106u, 144u, 164u, 67u, 36u,
145u, 45u, 18u, 71u, 62u, 12u, 9u, 161u, 243u, 36u, 115u, 208u, 192u,
146u, 73u, 28u, 232u, 48u, 38u, 135u, 227u, 17u, 206u, 3u, 2u, 72u,
132u, 115u, 96u, 192u, 146u, 17u, 28u, 208u, 48u, 38u, 135u, 227u,
17u, 204u, 131u, 2u, 104u, 124u, 201u, 28u, 192u, 48u, 8u, 166u, 73u,
33u, 52u, 62u, 56u, 38u, 134u, 102u, 146u, 57u, 112u, 96u, 77u, 19u,
20u, 35u, 150u, 134u, 4u, 146u, 72u, 229u, 97u, 129u, 52u, 76u, 80u,
142u, 84u, 24u, 25u, 0u, 146u, 67u, 40u, 107u, 154u, 203u, 112u, 104u,
154u, 201u, 36u, 51u, 9u, 104u, 132u, 207u, 4u, 134u, 192u, 154u, 38u,
34u, 19u, 66u, 167u, 131u, 72u, 150u, 137u, 36u, 142u, 82u, 24u, 19u,
69u, 102u, 8u, 228u, 129u, 129u, 52u, 85u, 96u, 142u, 70u, 24u, 18u,
65u, 35u, 144u, 6u, 7u, 184u, 252u, 48u, 199u, 36u, 145u, 199u, 195u,
2u, 71u, 228u, 113u, 208u, 192u, 235u, 54u, 73u, 193u, 170u, 65u, 36u,
142u, 56u, 24u, 19u, 70u, 99u, 136u, 227u, 97u, 129u, 36u, 226u, 56u,
200u, 96u, 73u, 176u, 142u, 46u, 24u, 25u, 228u, 249u, 56u, 145u,
197u, 131u, 2u, 73u, 196u, 113u, 32u, 192u, 154u, 54u, 20u, 71u, 14u,
12u, 13u, 226u, 68u, 147u, 136u, 77u, 27u, 10u, 36u, 112u, 208u, 192u,
154u, 56u, 156u, 71u, 12u, 12u, 9u, 39u, 17u, 194u, 195u, 3u, 32u,
18u, 112u, 101u, 13u, 115u, 105u, 110u, 13u, 19u, 105u, 36u, 132u,
209u, 196u, 48u, 154u, 50u, 158u, 72u, 225u, 65u, 129u, 52u, 121u,
12u, 142u, 10u, 24u, 19u, 71u, 128u, 200u, 224u, 129u, 129u, 36u, 18u,
56u, 8u, 96u, 123u, 143u, 195u, 12u, 118u, 73u, 28u, 0u, 48u, 36u,
126u, 67u, 16u, 96u, 99u, 17u, 250u, 72u, 33u, 176u, 4u, 210u, 4u,
129u, 40u, 200u, 32u, 73u, 205u, 196u, 132u, 209u, 196u, 48u, 154u,
50u, 158u, 72u, 9u, 163u, 11u, 130u, 81u, 144u, 64u, 146u, 91u, 137u,
9u, 162u, 187u, 33u, 52u, 89u, 48u, 144u, 153u, 115u, 132u, 197u, 64u,
52u, 132u, 192u, 136u, 146u, 72u,};
static unsigned char uvector__00021[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 55u, 71u, 233u, 32u, 134u, 192u, 30u,
227u, 240u, 195u, 29u, 146u, 37u, 25u, 4u, 9u, 57u, 184u, 144u, 222u,
36u, 73u, 56u, 134u, 121u, 62u, 78u, 36u, 58u, 205u, 146u, 112u, 106u,
144u, 73u, 36u, 7u, 184u, 252u, 48u, 199u, 36u, 137u, 70u, 65u, 2u,
73u, 110u, 36u, 51u, 9u, 104u, 134u, 121u, 158u, 73u, 36u, 54u, 1u,
20u, 201u, 36u, 55u, 137u, 164u, 77u, 82u, 20u, 136u, 100u, 146u, 37u,
162u, 67u, 172u, 209u, 36u, 134u, 169u, 4u, 146u, 3u, 176u, 144u,
164u, 146u, 36u, 73u, 36u, 144u, 235u, 50u, 201u, 33u, 170u, 65u, 36u,
134u, 145u, 45u, 18u, 73u, 2u, 70u, 143u, 228u, 114u, 29u, 102u, 104u,
254u, 65u, 36u, 9u, 10u, 63u, 33u, 214u, 101u, 143u, 228u, 18u, 67u,
72u, 103u, 143u, 163u, 242u, 73u, 35u, 132u, 134u, 6u, 48u, 152u, 24u,
166u, 9u, 36u, 112u, 112u, 192u, 152u, 14u, 17u, 192u, 131u, 2u, 96u,
56u, 71u, 0u, 12u, 9u, 128u, 225u, 32u,};
static unsigned char uvector__00022[] = {
 0u, 3u, 194u, 144u, 24u, 38u, 240u, 249u, 56u, 25u, 198u, 114u, 156u,
201u, 30u, 20u, 112u, 192u, 147u, 153u, 30u, 20u, 96u, 192u, 147u,
145u, 30u, 20u, 80u, 192u, 147u, 137u, 30u, 20u, 64u, 192u, 152u, 23u,
145u, 225u, 67u, 12u, 9u, 56u, 17u, 225u, 65u, 12u, 16u, 228u, 58u,
26u, 39u, 2u, 72u, 240u, 160u, 6u, 4u, 198u, 72u, 143u, 8u, 248u, 96u,
73u, 192u, 143u, 8u, 216u, 96u, 76u, 94u, 200u, 240u, 140u, 6u, 9u,
188u, 62u, 78u, 6u, 113u, 156u, 131u, 188u, 224u, 73u, 30u, 17u, 112u,
192u, 153u, 78u, 145u, 225u, 22u, 12u, 9u, 56u, 17u, 225u, 21u, 12u,
9u, 57u, 17u, 225u, 20u, 12u, 9u, 56u, 145u, 225u, 19u, 12u, 9u, 147u,
137u, 30u, 17u, 32u, 192u, 147u, 129u, 30u, 16u, 240u, 192u, 39u, 66u,
19u, 38u, 66u, 71u, 132u, 56u, 48u, 36u, 62u, 71u, 132u, 44u, 48u,
77u, 225u, 242u, 112u, 51u, 136u, 119u, 156u, 12u, 230u, 72u, 240u,
133u, 6u, 4u, 156u, 200u, 240u, 132u, 134u, 4u, 208u, 148u, 130u, 60u,
33u, 1u, 129u, 39u, 2u, 60u, 32u, 225u, 129u, 39u, 18u, 60u, 32u,
193u, 129u, 52u, 36u, 24u, 143u, 8u, 40u, 96u, 73u, 192u, 143u, 8u,
16u, 96u, 19u, 169u, 9u, 161u, 27u, 36u, 143u, 8u, 8u, 96u, 72u, 124u,
142u, 252u, 24u, 38u, 240u, 249u, 56u, 16u, 239u, 56u, 25u, 202u,
115u, 36u, 119u, 208u, 192u, 147u, 153u, 29u, 240u, 48u, 36u, 228u,
71u, 123u, 12u, 9u, 161u, 171u, 228u, 119u, 160u, 192u, 147u, 129u,
29u, 228u, 48u, 38u, 134u, 163u, 145u, 222u, 3u, 2u, 78u, 4u, 119u,
80u, 192u, 39u, 98u, 19u, 67u, 70u, 137u, 29u, 208u, 48u, 36u, 62u,
71u, 115u, 12u, 17u, 24u, 137u, 3u, 68u, 224u, 72u, 77u, 16u, 23u, 9u,
161u, 136u, 1u, 51u, 255u, 13u, 33u, 232u, 38u, 47u, 97u, 48u, 34u,
36u, 145u, 220u, 67u, 2u, 104u, 136u, 193u, 29u, 192u, 48u, 38u, 136u,
162u, 145u, 219u, 131u, 2u, 104u, 136u, 193u, 29u, 172u, 48u, 51u,
196u, 179u, 129u, 35u, 181u, 6u, 4u, 156u, 8u, 236u, 193u, 129u, 172u,
53u, 78u, 4u, 145u, 217u, 67u, 2u, 104u, 166u, 201u, 29u, 144u, 48u,
36u, 166u, 71u, 98u, 12u, 19u, 188u, 167u, 60u, 18u, 59u, 8u, 96u,
73u, 224u, 142u, 192u, 24u, 18u, 83u, 35u, 175u, 6u, 4u, 209u, 96u,
178u, 58u, 224u, 96u, 158u, 101u, 56u, 212u, 145u, 214u, 195u, 2u,
70u, 164u, 117u, 160u, 192u, 146u, 153u, 29u, 96u, 48u, 38u, 139u,
200u, 17u, 213u, 3u, 4u, 245u, 41u, 207u, 115u, 231u, 1u, 36u, 117u,
16u, 192u, 147u, 225u, 29u, 60u, 48u, 36u, 246u, 71u, 78u, 12u, 9u,
41u, 145u, 211u, 3u, 2u, 104u, 200u, 169u, 29u, 40u, 48u, 60u, 79u,
161u, 52u, 100u, 84u, 145u, 210u, 67u, 2u, 79u, 164u, 116u, 96u, 193u,
63u, 74u, 113u, 169u, 35u, 162u, 134u, 4u, 159u, 136u, 232u, 129u,
129u, 35u, 82u, 58u, 24u, 96u, 73u, 76u, 142u, 130u, 24u, 19u, 71u,
3u, 200u, 231u, 225u, 129u, 224u, 79u, 243u, 241u, 9u, 163u, 129u,
228u, 142u, 122u, 24u, 19u, 71u, 130u, 72u, 231u, 129u, 129u, 39u,
226u, 57u, 208u, 96u, 77u, 30u, 9u, 35u, 156u, 134u, 6u, 64u, 37u,
49u, 42u, 136u, 246u, 53u, 50u, 177u, 36u, 38u, 142u, 239u, 4u, 209u,
177u, 144u, 154u, 47u, 32u, 19u, 69u, 130u, 229u, 50u, 71u, 55u, 12u,
9u, 164u, 0u, 228u, 115u, 80u, 192u, 146u, 177u, 28u, 204u, 48u, 38u,
144u, 10u, 17u, 204u, 131u, 2u, 70u, 164u, 115u, 0u, 192u, 154u, 64u,
40u, 71u, 46u, 12u, 9u, 164u, 0u, 228u, 114u, 192u, 192u, 147u, 153u,
28u, 156u, 48u, 50u, 136u, 190u, 115u, 48u, 124u, 230u, 72u, 228u,
161u, 129u, 52u, 145u, 48u, 142u, 72u, 24u, 18u, 115u, 35u, 145u, 6u,
4u, 210u, 68u, 194u, 57u, 0u, 96u, 73u, 200u, 142u, 54u, 24u, 25u,
68u, 95u, 57u, 22u, 233u, 200u, 145u, 198u, 67u, 2u, 105u, 50u, 233u,
28u, 96u, 48u, 36u, 228u, 71u, 22u, 12u, 9u, 164u, 203u, 164u, 113u,
64u, 192u, 147u, 137u, 28u, 60u, 48u, 50u, 136u, 190u, 113u, 45u,
211u, 137u, 35u, 134u, 134u, 4u, 210u, 136u, 194u, 56u, 96u, 96u, 73u,
196u, 142u, 20u, 24u, 19u, 74u, 35u, 8u, 225u, 1u, 129u, 174u, 112u,
36u, 112u, 112u, 192u, 147u, 129u, 28u, 0u, 48u, 37u, 8u, 1u, 12u,
65u, 129u, 140u, 70u, 177u, 144u, 159u, 91u, 136u, 159u, 155u, 136u,
158u, 32u, 242u, 68u, 175u, 41u, 132u, 210u, 0u, 112u, 154u, 59u,
188u, 19u, 70u, 198u, 66u, 104u, 188u, 128u, 77u, 22u, 11u, 36u, 144u,};
static unsigned char uvector__00023[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 51u, 70u, 177u, 144u, 159u, 91u, 136u,
159u, 155u, 136u, 158u, 32u, 242u, 68u, 175u, 41u, 137u, 84u, 71u,
177u, 169u, 149u, 136u, 120u, 19u, 252u, 252u, 68u, 253u, 41u, 198u,
164u, 135u, 137u, 244u, 79u, 82u, 156u, 247u, 62u, 112u, 18u, 68u,
243u, 41u, 198u, 164u, 78u, 242u, 156u, 240u, 73u, 35u, 132u, 134u,
6u, 48u, 152u, 24u, 166u, 18u, 68u, 112u, 112u, 192u, 152u, 14u, 17u,
192u, 131u, 2u, 96u, 56u, 71u, 0u, 12u, 9u, 128u, 225u, 32u,};
static unsigned char uvector__00024[] = {
 0u, 3u, 170u, 6u, 10u, 16u, 16u, 249u, 66u, 4u, 104u, 64u, 201u, 29u,
76u, 48u, 37u, 8u, 25u, 29u, 72u, 48u, 38u, 5u, 228u, 117u, 16u, 192u,
148u, 32u, 68u, 116u, 240u, 193u, 14u, 67u, 161u, 162u, 132u, 8u,
146u, 58u, 112u, 96u, 76u, 71u, 136u, 233u, 65u, 129u, 49u, 7u, 35u,
163u, 134u, 10u, 16u, 16u, 249u, 66u, 4u, 67u, 189u, 8u, 17u, 36u,
116u, 96u, 192u, 152u, 242u, 145u, 209u, 67u, 2u, 80u, 129u, 17u,
209u, 3u, 2u, 99u, 142u, 71u, 67u, 12u, 9u, 66u, 4u, 71u, 64u, 12u,
2u, 116u, 33u, 49u, 180u, 36u, 115u, 240u, 192u, 144u, 249u, 28u,
248u, 48u, 68u, 98u, 36u, 13u, 20u, 32u, 68u, 132u, 203u, 108u, 52u,
135u, 160u, 152u, 131u, 132u, 192u, 112u, 146u, 71u, 60u, 12u, 9u,
154u, 177u, 28u, 236u, 48u, 38u, 108u, 100u, 115u, 144u, 192u, 153u,
171u, 17u, 205u, 131u, 3u, 60u, 75u, 66u, 4u, 72u, 230u, 161u, 129u,
40u, 64u, 136u, 230u, 33u, 129u, 172u, 53u, 80u, 129u, 18u, 71u, 48u,
12u, 9u, 161u, 64u, 36u, 114u, 240u, 192u, 146u, 153u, 28u, 180u, 48u,
78u, 242u, 156u, 240u, 72u, 229u, 129u, 129u, 39u, 130u, 57u, 88u,
96u, 73u, 76u, 142u, 82u, 24u, 19u, 66u, 198u, 72u, 228u, 225u, 130u,
121u, 148u, 196u, 79u, 44u, 75u, 50u, 113u, 36u, 114u, 80u, 192u,
154u, 25u, 170u, 71u, 36u, 12u, 9u, 39u, 17u, 200u, 3u, 2u, 75u, 4u,
113u, 224u, 192u, 154u, 25u, 170u, 71u, 29u, 12u, 9u, 41u, 145u, 198u,
195u, 2u, 104u, 100u, 241u, 28u, 104u, 48u, 50u, 1u, 41u, 137u, 86u,
58u, 149u, 137u, 33u, 52u, 50u, 120u, 38u, 133u, 140u, 202u, 100u,
142u, 48u, 24u, 19u, 67u, 245u, 8u, 226u, 193u, 129u, 37u, 98u, 56u,
160u, 96u, 72u, 232u, 142u, 36u, 24u, 19u, 67u, 245u, 8u, 226u, 1u,
129u, 40u, 64u, 200u, 225u, 97u, 129u, 148u, 69u, 244u, 32u, 102u,
15u, 161u, 3u, 36u, 112u, 144u, 192u, 154u, 37u, 162u, 71u, 8u, 12u,
9u, 66u, 6u, 71u, 6u, 12u, 9u, 162u, 90u, 36u, 112u, 64u, 192u, 215u,
66u, 4u, 72u, 224u, 97u, 129u, 40u, 64u, 136u, 224u, 1u, 129u, 40u,
65u, 8u, 98u, 12u, 12u, 98u, 78u, 140u, 132u, 241u, 7u, 146u, 37u,
121u, 76u, 38u, 135u, 234u, 4u, 208u, 201u, 224u, 154u, 22u, 50u, 73u,
32u,};
static unsigned char uvector__00025[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 54u, 73u, 209u, 144u, 158u, 32u, 242u,
68u, 175u, 41u, 137u, 86u, 58u, 149u, 136u, 158u, 101u, 49u, 19u,
203u, 18u, 204u, 156u, 72u, 157u, 229u, 57u, 224u, 146u, 71u, 9u, 12u,
12u, 97u, 48u, 49u, 76u, 24u, 136u, 224u, 225u, 129u, 48u, 28u, 35u,
129u, 6u, 4u, 192u, 112u, 142u, 0u, 24u, 19u, 1u, 194u, 64u,};
static unsigned char uvector__00026[] = {
 0u, 3u, 133u, 6u, 7u, 89u, 152u, 52u, 9u, 145u, 186u, 73u, 38u, 65u,
36u, 112u, 144u, 192u, 146u, 9u, 28u, 32u, 48u, 38u, 6u, 36u, 112u,
96u, 192u, 152u, 30u, 17u, 193u, 67u, 2u, 73u, 36u, 112u, 32u, 192u,
152u, 30u, 17u, 192u, 3u, 2u, 76u, 196u, 49u, 6u, 6u, 49u, 36u, 164u,
130u, 19u, 1u, 194u, 72u,};
static unsigned char uvector__00027[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 50u, 201u, 41u, 32u, 135u, 89u, 152u, 52u,
9u, 145u, 186u, 73u, 38u, 65u, 36u, 142u, 18u, 24u, 24u, 194u, 96u,
98u, 152u, 36u, 145u, 193u, 195u, 2u, 96u, 56u, 71u, 2u, 12u, 9u,
128u, 225u, 28u, 0u, 48u, 38u, 3u, 132u, 128u,};
static unsigned char uvector__00028[] = {
 0u, 3u, 129u, 6u, 9u, 145u, 186u, 132u, 20u, 145u, 192u, 67u, 2u,
80u, 130u, 144u, 196u, 24u, 24u, 197u, 8u, 41u, 9u, 128u, 225u, 36u,};
static unsigned char uvector__00029[] = {
 0u, 3u, 130u, 6u, 7u, 89u, 154u, 93u, 144u, 73u, 28u, 12u, 48u, 36u,
130u, 71u, 2u, 12u, 9u, 46u, 145u, 192u, 3u, 2u, 76u, 196u, 49u, 6u,
6u, 53u, 9u, 128u, 225u, 36u,};
static unsigned char uvector__00030[] = {
 0u, 3u, 133u, 6u, 9u, 120u, 49u, 137u, 108u, 134u, 192u, 210u, 37u,
201u, 106u, 91u, 45u, 196u, 144u, 198u, 161u, 214u, 102u, 151u, 100u,
18u, 101u, 247u, 226u, 71u, 3u, 12u, 9u, 132u, 121u, 28u, 0u, 48u,
38u, 5u, 36u, 49u, 6u, 6u, 49u, 46u, 145u, 48u, 9u, 108u, 38u, 9u,
228u, 38u, 19u, 196u, 146u,};
static unsigned char uvector__00031[] = {
 0u, 3u, 141u, 134u, 4u, 148u, 200u, 227u, 33u, 130u, 97u, 150u, 137u,
28u, 96u, 48u, 36u, 180u, 71u, 22u, 12u, 9u, 130u, 49u, 28u, 72u, 48u,
55u, 202u, 97u, 48u, 70u, 36u, 113u, 16u, 192u, 146u, 153u, 28u, 64u,
48u, 50u, 1u, 41u, 137u, 138u, 132u, 24u, 80u, 131u, 208u, 132u, 10u,
16u, 153u, 145u, 186u, 132u, 40u, 208u, 133u, 146u, 72u, 76u, 52u,
101u, 50u, 71u, 14u, 12u, 9u, 138u, 225u, 28u, 48u, 48u, 80u, 132u,
4u, 198u, 114u, 132u, 44u, 145u, 194u, 195u, 2u, 80u, 133u, 145u,
194u, 131u, 2u, 80u, 134u, 17u, 194u, 3u, 2u, 100u, 178u, 71u, 7u,
12u, 9u, 66u, 12u, 71u, 5u, 12u, 9u, 138u, 225u, 28u, 8u, 48u, 36u,
180u, 71u, 1u, 12u, 12u, 128u, 75u, 77u, 196u, 134u, 97u, 66u, 13u,
46u, 145u, 48u, 9u, 108u, 52u, 137u, 114u, 90u, 150u, 203u, 113u, 33u,
214u, 102u, 151u, 100u, 18u, 72u, 149u, 229u, 48u, 152u, 174u, 4u,
195u, 68u, 146u, 24u, 131u, 3u, 24u, 161u, 11u, 73u, 4u, 54u, 203u,
77u, 193u, 51u, 236u, 9u, 161u, 40u, 4u, 146u,};
static unsigned char uvector__00032[] = {
 0u, 3u, 135u, 6u, 6u, 97u, 52u, 80u, 133u, 164u, 130u, 27u, 101u,
166u, 224u, 204u, 40u, 65u, 165u, 210u, 38u, 1u, 45u, 134u, 145u, 46u,
75u, 82u, 217u, 110u, 36u, 58u, 204u, 210u, 236u, 130u, 73u, 18u,
188u, 166u, 38u, 42u, 16u, 97u, 66u, 15u, 66u, 16u, 40u, 66u, 102u,
70u, 234u, 16u, 163u, 66u, 22u, 72u, 111u, 148u, 196u, 195u, 45u, 18u,
73u, 35u, 133u, 134u, 6u, 48u, 152u, 24u, 166u, 9u, 228u, 112u, 144u,
192u, 152u, 14u, 17u, 193u, 3u, 2u, 96u, 56u, 71u, 2u, 12u, 9u, 128u,
225u, 32u,};
static unsigned char uvector__00033[] = {
 0u, 3u, 132u, 6u, 7u, 177u, 30u, 80u, 134u, 144u, 195u, 27u, 146u,
71u, 3u, 12u, 9u, 129u, 57u, 28u, 8u, 48u, 37u, 8u, 105u, 28u, 0u,
48u, 38u, 4u, 228u, 49u, 6u, 6u, 49u, 66u, 26u, 66u, 96u, 56u, 73u,
0u,};
static unsigned char uvector__00034[] = {
 0u, 3u, 132u, 6u, 7u, 177u, 30u, 80u, 134u, 144u, 195u, 28u, 146u,
71u, 3u, 12u, 9u, 129u, 57u, 28u, 8u, 48u, 37u, 8u, 105u, 28u, 0u,
48u, 38u, 4u, 228u, 49u, 6u, 6u, 49u, 66u, 26u, 66u, 96u, 56u, 73u,
0u,};
static unsigned char uvector__00035[] = {
 0u, 3u, 136u, 134u, 10u, 16u, 225u, 66u, 31u, 45u, 134u, 26u, 17u,
2u, 69u, 8u, 136u, 20u, 34u, 84u, 34u, 100u, 80u, 137u, 80u, 138u,
18u, 73u, 28u, 60u, 48u, 37u, 8u, 161u, 28u, 56u, 48u, 37u, 8u, 153u,
28u, 44u, 48u, 38u, 5u, 228u, 112u, 128u, 192u, 146u, 217u, 28u, 24u,
48u, 38u, 5u, 228u, 112u, 48u, 193u, 66u, 43u, 45u, 146u, 56u, 16u,
96u, 73u, 108u, 142u, 0u, 24u, 19u, 29u, 114u, 24u, 131u, 3u, 24u,
150u, 200u, 112u, 9u, 142u, 184u, 76u, 7u, 9u, 36u,};
static unsigned char uvector__00036[] = {
 0u, 3u, 130u, 6u, 7u, 89u, 186u, 132u, 90u, 65u, 36u, 112u, 48u,
192u, 146u, 9u, 28u, 8u, 48u, 37u, 8u, 177u, 28u, 0u, 48u, 36u, 220u,
67u, 16u, 96u, 99u, 20u, 34u, 196u, 38u, 3u, 132u, 144u,};
static unsigned char uvector__00037[] = {
 0u, 3u, 133u, 134u, 9u, 134u, 91u, 36u, 112u, 160u, 192u, 146u, 217u,
28u, 32u, 48u, 62u, 208u, 139u, 146u, 56u, 56u, 96u, 74u, 17u, 130u,
56u, 40u, 96u, 20u, 35u, 50u, 217u, 9u, 131u, 201u, 35u, 130u, 6u, 4u,
195u, 140u, 142u, 6u, 24u, 18u, 132u, 100u, 142u, 4u, 24u, 18u, 91u,
35u, 128u, 6u, 4u, 195u, 140u, 134u, 32u, 192u, 198u, 37u, 178u, 27u,
2u, 97u, 192u, 26u, 66u, 96u, 56u, 73u, 36u,};
static unsigned char uvector__00038[] = {
 0u, 3u, 130u, 6u, 7u, 89u, 186u, 132u, 106u, 65u, 36u, 112u, 48u,
192u, 146u, 9u, 28u, 8u, 48u, 37u, 8u, 209u, 28u, 0u, 48u, 36u, 220u,
67u, 16u, 96u, 99u, 20u, 35u, 68u, 38u, 3u, 132u, 144u,};
static unsigned char uvector__00039[] = {
 0u, 3u, 132u, 134u, 7u, 58u, 17u, 113u, 66u, 54u, 40u, 66u, 93u,
102u, 234u, 16u, 169u, 4u, 208u, 142u, 18u, 71u, 7u, 12u, 9u, 129u,
185u, 28u, 24u, 48u, 37u, 8u, 225u, 28u, 12u, 48u, 49u, 138u, 17u,
162u, 29u, 102u, 234u, 17u, 169u, 4u, 146u, 56u, 8u, 96u, 76u, 13u,
200u, 224u, 1u, 129u, 40u, 70u, 8u, 98u, 12u, 12u, 106u, 19u, 1u,
194u, 72u,};
static unsigned char uvector__00040[] = {
 0u, 3u, 150u, 6u, 9u, 120u, 49u, 137u, 108u, 134u, 192u, 40u, 70u,
101u, 178u, 31u, 104u, 69u, 201u, 13u, 34u, 97u, 150u, 201u, 36u, 49u,
168u, 115u, 161u, 23u, 20u, 35u, 98u, 132u, 37u, 214u, 110u, 161u,
10u, 144u, 77u, 8u, 225u, 38u, 95u, 126u, 36u, 114u, 80u, 192u, 152u,
96u, 145u, 200u, 131u, 2u, 96u, 82u, 71u, 33u, 12u, 12u, 128u, 80u,
142u, 6u, 81u, 66u, 59u, 36u, 145u, 66u, 15u, 66u, 61u, 66u, 15u, 50u,
73u, 20u, 32u, 244u, 35u, 244u, 32u, 244u, 40u, 20u, 34u, 228u, 80u,
143u, 80u, 160u, 146u, 72u, 152u, 4u, 182u, 19u, 4u, 240u, 152u, 73u,
144u, 152u, 104u, 18u, 71u, 31u, 12u, 20u, 40u, 66u, 133u, 12u, 61u,
136u, 242u, 132u, 52u, 134u, 24u, 228u, 147u, 66u, 136u, 72u, 227u,
193u, 129u, 40u, 81u, 8u, 227u, 161u, 129u, 40u, 81u, 72u, 227u, 97u,
129u, 51u, 115u, 35u, 139u, 6u, 4u, 205u, 204u, 142u, 42u, 24u, 18u,
133u, 16u, 142u, 40u, 24u, 18u, 133u, 24u, 142u, 36u, 24u, 19u, 55u,
50u, 56u, 136u, 96u, 99u, 20u, 40u, 132u, 38u, 110u, 100u, 142u, 30u,
24u, 38u, 73u, 32u, 80u, 143u, 138u, 20u, 10u, 17u, 114u, 73u, 28u,
52u, 48u, 38u, 133u, 192u, 145u, 194u, 195u, 2u, 104u, 92u, 161u, 28u,
40u, 48u, 37u, 8u, 193u, 28u, 32u, 48u, 38u, 133u, 202u, 17u, 193u,
131u, 2u, 104u, 92u, 9u, 28u, 20u, 48u, 36u, 144u, 71u, 3u, 12u, 9u,
161u, 105u, 68u, 112u, 16u, 192u, 153u, 7u, 145u, 192u, 3u, 2u, 73u,
36u, 49u, 6u, 6u, 49u, 66u, 48u, 69u, 10u, 61u, 8u, 197u, 10u, 33u,
36u, 128u,};
static unsigned char uvector__00041[] = {
 0u, 3u, 133u, 6u, 10u, 20u, 130u, 71u, 9u, 12u, 20u, 41u, 45u, 212u,
41u, 68u, 142u, 16u, 24u, 18u, 73u, 35u, 130u, 134u, 1u, 66u, 51u,
45u, 144u, 152u, 37u, 146u, 56u, 32u, 96u, 76u, 47u, 200u, 224u, 97u,
129u, 40u, 70u, 72u, 224u, 65u, 129u, 37u, 178u, 56u, 0u, 96u, 76u,
47u, 200u, 98u, 12u, 12u, 98u, 91u, 33u, 176u, 38u, 23u, 129u, 164u,
38u, 3u, 132u, 146u, 64u,};
static unsigned char uvector__00042[] = {
 0u, 3u, 140u, 6u, 10u, 20u, 154u, 17u, 112u, 202u, 40u, 71u, 100u,
146u, 40u, 71u, 197u, 10u, 5u, 8u, 185u, 45u, 196u, 145u, 196u, 195u,
2u, 96u, 212u, 71u, 17u, 12u, 9u, 131u, 233u, 28u, 64u, 48u, 37u, 8u,
185u, 28u, 56u, 48u, 38u, 15u, 164u, 112u, 192u, 192u, 152u, 53u, 17u,
194u, 131u, 2u, 96u, 148u, 71u, 9u, 12u, 9u, 36u, 145u, 194u, 3u, 2u,
80u, 139u, 145u, 193u, 195u, 3u, 32u, 20u, 34u, 225u, 214u, 110u,
141u, 100u, 18u, 72u, 76u, 7u, 9u, 28u, 24u, 48u, 38u, 75u, 196u,
112u, 80u, 192u, 146u, 9u, 28u, 16u, 48u, 36u, 106u, 71u, 2u, 12u, 9u,
55u, 17u, 192u, 3u, 2u, 100u, 188u, 67u, 16u, 96u, 99u, 80u, 219u,
66u, 46u, 19u, 37u, 224u, 152u, 14u, 18u, 72u,};
static unsigned char uvector__00043[] = {
 0u, 3u, 194u, 54u, 24u, 40u, 66u, 5u, 8u, 77u, 10u, 61u, 8u, 85u,
10u, 33u, 20u, 41u, 98u, 133u, 50u, 133u, 16u, 146u, 71u, 132u, 96u,
48u, 56u, 201u, 33u, 170u, 133u, 4u, 134u, 90u, 20u, 208u, 231u, 66u,
155u, 66u, 156u, 104u, 83u, 137u, 35u, 194u, 44u, 24u, 18u, 133u, 56u,
143u, 8u, 152u, 96u, 76u, 73u, 200u, 240u, 137u, 6u, 4u, 161u, 78u,
35u, 194u, 34u, 24u, 18u, 133u, 52u, 143u, 8u, 120u, 96u, 76u, 67u,
8u, 240u, 135u, 6u, 4u, 161u, 77u, 35u, 194u, 24u, 24u, 19u, 14u,
226u, 60u, 33u, 97u, 129u, 36u, 146u, 60u, 33u, 65u, 130u, 133u, 60u,
80u, 166u, 201u, 36u, 80u, 168u, 6u, 138u, 20u, 19u, 36u, 144u, 152u,
108u, 146u, 60u, 32u, 193u, 130u, 94u, 12u, 98u, 91u, 33u, 176u, 10u,
17u, 153u, 108u, 138u, 20u, 150u, 234u, 20u, 162u, 67u, 72u, 161u,
72u, 36u, 144u, 198u, 161u, 182u, 132u, 92u, 58u, 205u, 209u, 172u,
130u, 69u, 10u, 77u, 8u, 184u, 101u, 20u, 35u, 178u, 73u, 20u, 35u,
226u, 133u, 2u, 132u, 92u, 150u, 226u, 73u, 151u, 223u, 137u, 29u,
252u, 48u, 38u, 132u, 131u, 145u, 223u, 3u, 2u, 103u, 198u, 71u, 122u,
12u, 9u, 158u, 177u, 29u, 228u, 48u, 50u, 1u, 26u, 134u, 90u, 20u,
161u, 19u, 208u, 168u, 208u, 169u, 10u, 21u, 56u, 212u, 208u, 170u,
80u, 165u, 24u, 212u, 146u, 38u, 1u, 45u, 132u, 208u, 128u, 146u, 19u,
66u, 69u, 137u, 35u, 188u, 6u, 4u, 141u, 72u, 238u, 129u, 129u, 52u,
51u, 60u, 142u, 230u, 24u, 18u, 73u, 35u, 183u, 6u, 4u, 208u, 209u,
194u, 59u, 104u, 96u, 72u, 212u, 142u, 214u, 24u, 19u, 67u, 71u, 8u,
237u, 1u, 129u, 40u, 84u, 72u, 236u, 193u, 129u, 52u, 51u, 60u, 142u,
200u, 24u, 19u, 67u, 37u, 8u, 236u, 97u, 129u, 36u, 146u, 59u, 16u,
96u, 76u, 216u, 72u, 236u, 33u, 129u, 51u, 116u, 35u, 175u, 134u, 10u,
21u, 90u, 20u, 226u, 71u, 94u, 12u, 9u, 66u, 156u, 71u, 92u, 12u, 13u,
116u, 40u, 36u, 142u, 182u, 24u, 18u, 133u, 4u, 142u, 172u, 24u, 40u,
80u, 133u, 10u, 24u, 123u, 17u, 229u, 8u, 105u, 12u, 49u, 185u, 38u,
133u, 16u, 145u, 213u, 67u, 2u, 80u, 162u, 17u, 213u, 3u, 2u, 80u,
171u, 17u, 212u, 131u, 2u, 104u, 166u, 145u, 29u, 52u, 48u, 49u, 138u,
17u, 130u, 40u, 81u, 232u, 70u, 40u, 81u, 9u, 35u, 165u, 6u, 4u, 161u,
81u, 35u, 163u, 134u, 4u, 161u, 19u, 35u, 162u, 6u, 10u, 21u, 113u,
66u, 18u, 235u, 55u, 80u, 133u, 72u, 38u, 133u, 16u, 145u, 208u, 195u,
2u, 80u, 162u, 17u, 208u, 3u, 3u, 24u, 161u, 22u, 33u, 214u, 110u,
161u, 22u, 144u, 73u, 35u, 159u, 6u, 6u, 186u, 21u, 130u, 71u, 61u,
12u, 9u, 66u, 176u, 71u, 58u, 12u, 9u, 162u, 154u, 68u, 115u, 144u,
192u, 148u, 40u, 132u, 115u, 128u, 192u, 148u, 43u, 36u, 115u, 96u,
192u, 154u, 41u, 164u, 71u, 51u, 12u, 19u, 36u, 145u, 36u, 146u, 57u,
144u, 96u, 73u, 36u, 142u, 98u, 24u, 18u, 72u, 35u, 151u, 134u, 4u,
209u, 216u, 50u, 57u, 96u, 96u, 161u, 90u, 16u, 88u, 97u, 131u, 9u,
12u, 52u, 42u, 37u, 184u, 145u, 201u, 131u, 2u, 104u, 248u, 217u, 28u,
136u, 48u, 38u, 143u, 141u, 145u, 200u, 3u, 2u, 104u, 248u, 65u, 28u,
116u, 48u, 80u, 173u, 8u, 44u, 48u, 193u, 164u, 134u, 26u, 17u, 66u,
220u, 72u, 226u, 225u, 129u, 52u, 133u, 80u, 142u, 38u, 24u, 19u, 72u,
85u, 8u, 226u, 33u, 129u, 52u, 133u, 4u, 142u, 28u, 24u, 40u, 86u,
132u, 22u, 24u, 96u, 210u, 67u, 13u, 8u, 153u, 110u, 36u, 112u, 128u,
192u, 154u, 71u, 26u, 71u, 4u, 12u, 9u, 164u, 113u, 164u, 112u, 32u,
192u, 154u, 70u, 180u, 67u, 16u, 96u, 99u, 18u, 68u, 146u, 146u, 8u,
102u, 20u, 35u, 212u, 40u, 132u, 80u, 161u, 10u, 20u, 48u, 246u, 35u,
202u, 16u, 210u, 24u, 99u, 146u, 77u, 10u, 33u, 33u, 152u, 80u, 166u,
80u, 162u, 16u, 154u, 41u, 164u, 67u, 53u, 8u, 152u, 77u, 35u, 90u,
33u, 154u, 132u, 80u, 38u, 144u, 160u, 144u, 205u, 66u, 162u, 19u,
71u, 194u, 8u, 102u, 20u, 35u, 50u, 217u, 14u, 2u, 132u, 86u, 91u,
34u, 132u, 56u, 80u, 135u, 203u, 97u, 134u, 132u, 64u, 145u, 66u, 34u,
5u, 8u, 149u, 8u, 153u, 20u, 34u, 84u, 34u, 132u, 146u, 72u, 102u,
20u, 40u, 244u, 34u, 244u, 40u, 36u, 54u, 208u, 142u, 6u, 81u, 66u,
59u, 36u, 145u, 66u, 15u, 66u, 61u, 66u, 15u, 50u, 73u, 20u, 32u,
244u, 35u, 244u, 32u, 244u, 40u, 20u, 34u, 228u, 80u, 143u, 80u, 160u,
146u, 38u, 1u, 45u, 129u, 66u, 51u, 45u, 144u, 251u, 66u, 46u, 72u,
105u, 19u, 12u, 182u, 73u, 14u, 116u, 34u, 226u, 132u, 108u, 80u,
132u, 186u, 205u, 212u, 33u, 82u, 9u, 161u, 28u, 36u, 146u, 25u, 133u,
10u, 132u, 107u, 66u, 148u, 67u, 108u, 106u, 19u, 67u, 37u, 2u, 104u,
110u, 145u, 33u, 152u, 80u, 165u, 208u, 172u, 16u, 200u, 226u, 4u,
146u, 220u, 69u, 10u, 13u, 10u, 193u, 20u, 41u, 193u, 133u, 73u, 33u,
148u, 38u, 138u, 5u, 132u, 209u, 49u, 192u, 153u, 151u, 18u, 69u, 10u,
216u, 20u, 40u, 129u, 52u, 118u, 12u, 138u, 21u, 129u, 66u, 153u, 66u,
136u, 73u, 12u, 161u, 216u, 38u, 141u, 138u, 7u, 154u, 17u, 50u, 30u,
104u, 84u, 73u, 9u, 163u, 26u, 129u, 48u, 34u, 36u, 146u,};
static unsigned char uvector__00044[] = {
 0u, 3u, 135u, 6u, 6u, 97u, 66u, 185u, 36u, 73u, 41u, 32u, 134u, 97u,
66u, 61u, 66u, 136u, 69u, 10u, 16u, 161u, 67u, 15u, 98u, 60u, 161u,
13u, 33u, 134u, 57u, 36u, 208u, 162u, 18u, 25u, 133u, 10u, 101u, 10u,
33u, 20u, 40u, 66u, 133u, 12u, 61u, 136u, 242u, 132u, 52u, 134u, 24u,
220u, 147u, 66u, 136u, 72u, 102u, 161u, 19u, 20u, 43u, 66u, 11u, 12u,
48u, 105u, 33u, 134u, 132u, 76u, 183u, 18u, 25u, 168u, 69u, 5u, 10u,
208u, 130u, 195u, 12u, 26u, 72u, 97u, 161u, 20u, 45u, 196u, 134u,
106u, 21u, 17u, 66u, 180u, 32u, 176u, 195u, 6u, 18u, 24u, 104u, 84u,
75u, 113u, 33u, 152u, 80u, 140u, 203u, 100u, 56u, 10u, 17u, 89u, 108u,
138u, 16u, 225u, 66u, 31u, 45u, 134u, 26u, 17u, 2u, 69u, 8u, 136u,
20u, 34u, 84u, 34u, 100u, 80u, 137u, 80u, 138u, 18u, 73u, 33u, 152u,
80u, 163u, 208u, 139u, 208u, 160u, 144u, 219u, 66u, 56u, 25u, 69u, 8u,
236u, 146u, 69u, 8u, 61u, 8u, 245u, 8u, 60u, 201u, 36u, 80u, 131u,
208u, 143u, 208u, 131u, 208u, 160u, 80u, 139u, 145u, 66u, 61u, 66u,
130u, 72u, 152u, 4u, 182u, 5u, 8u, 204u, 182u, 67u, 237u, 8u, 185u,
33u, 164u, 76u, 50u, 217u, 36u, 57u, 208u, 139u, 138u, 17u, 177u, 66u,
18u, 235u, 55u, 80u, 133u, 72u, 38u, 132u, 112u, 146u, 72u, 102u, 20u,
42u, 17u, 173u, 10u, 81u, 13u, 177u, 168u, 101u, 161u, 74u, 17u, 61u,
10u, 141u, 10u, 144u, 161u, 83u, 141u, 77u, 10u, 165u, 10u, 81u, 141u,
72u, 152u, 4u, 182u, 5u, 8u, 204u, 182u, 69u, 10u, 75u, 117u, 10u,
81u, 36u, 54u, 208u, 139u, 135u, 89u, 186u, 53u, 144u, 72u, 161u, 73u,
161u, 23u, 12u, 162u, 132u, 118u, 73u, 34u, 132u, 124u, 80u, 160u,
80u, 139u, 146u, 220u, 73u, 36u, 134u, 97u, 66u, 151u, 66u, 176u, 67u,
35u, 136u, 18u, 75u, 113u, 20u, 40u, 52u, 43u, 4u, 80u, 167u, 6u, 21u,
36u, 134u, 80u, 215u, 66u, 130u, 69u, 10u, 173u, 10u, 113u, 20u, 41u,
226u, 133u, 54u, 73u, 34u, 133u, 64u, 52u, 80u, 160u, 153u, 36u, 135u,
25u, 36u, 53u, 80u, 160u, 144u, 203u, 66u, 154u, 28u, 232u, 83u, 104u,
83u, 141u, 10u, 113u, 36u, 146u, 40u, 86u, 192u, 161u, 68u, 19u, 36u,
145u, 36u, 146u, 40u, 86u, 5u, 10u, 101u, 10u, 33u, 36u, 50u, 135u,
96u, 215u, 66u, 176u, 67u, 205u, 8u, 153u, 15u, 52u, 42u, 36u, 138u,
21u, 113u, 66u, 18u, 235u, 55u, 80u, 133u, 72u, 38u, 133u, 16u, 138u,
16u, 129u, 66u, 19u, 66u, 143u, 66u, 21u, 66u, 136u, 69u, 10u, 88u,
161u, 76u, 161u, 68u, 36u, 146u, 72u, 225u, 97u, 129u, 140u, 38u, 6u,
233u, 130u, 201u, 28u, 36u, 48u, 38u, 3u, 132u, 112u, 64u, 192u, 152u,
14u, 17u, 192u, 131u, 2u, 96u, 56u, 72u,};
static unsigned char uvector__00045[] = {
 0u, 3u, 128u, 134u, 10u, 21u, 218u, 21u, 224u, 195u, 26u, 146u, 71u,
0u, 12u, 9u, 66u, 188u, 67u, 16u, 96u, 99u, 20u, 43u, 196u, 38u, 3u,
132u, 144u,};
static unsigned char uvector__00046[] = {
 0u, 3u, 132u, 6u, 8u, 158u, 133u, 126u, 133u, 74u, 134u, 2u, 133u,
82u, 73u, 36u, 112u, 112u, 192u, 146u, 73u, 28u, 16u, 48u, 37u, 12u,
1u, 28u, 0u, 48u, 37u, 10u, 249u, 12u, 65u, 129u, 140u, 80u, 192u,
16u, 152u, 14u, 18u, 64u,};
static unsigned char uvector__00047[] = {
 0u, 3u, 194u, 180u, 24u, 31u, 68u, 79u, 66u, 191u, 66u, 165u, 67u,
3u, 66u, 169u, 36u, 146u, 71u, 133u, 96u, 48u, 38u, 5u, 164u, 120u,
85u, 195u, 2u, 73u, 36u, 120u, 83u, 195u, 2u, 80u, 175u, 145u, 225u,
77u, 12u, 9u, 129u, 105u, 30u, 20u, 160u, 193u, 66u, 174u, 40u, 66u,
98u, 122u, 21u, 250u, 21u, 42u, 16u, 170u, 21u, 73u, 36u, 138u, 21u,
218u, 24u, 32u, 195u, 67u, 6u, 73u, 35u, 194u, 144u, 24u, 19u, 31u,
66u, 60u, 40u, 225u, 129u, 40u, 96u, 136u, 240u, 162u, 6u, 6u, 49u,
67u, 0u, 68u, 79u, 66u, 191u, 66u, 165u, 67u, 1u, 66u, 169u, 36u,
146u, 71u, 133u, 8u, 48u, 51u, 208u, 194u, 73u, 4u, 143u, 10u, 8u,
96u, 73u, 32u, 143u, 8u, 232u, 96u, 76u, 216u, 72u, 240u, 140u, 134u,
6u, 250u, 24u, 32u, 153u, 176u, 146u, 60u, 35u, 1u, 129u, 40u, 96u,
136u, 240u, 139u, 134u, 6u, 64u, 40u, 96u, 133u, 12u, 44u, 144u, 73u,
9u, 161u, 1u, 129u, 49u, 119u, 36u, 120u, 69u, 67u, 2u, 104u, 74u,
169u, 30u, 17u, 64u, 192u, 146u, 65u, 30u, 17u, 32u, 192u, 154u, 18u,
170u, 71u, 132u, 64u, 48u, 50u, 201u, 1u, 182u, 134u, 8u, 38u, 132u,
170u, 132u, 208u, 128u, 192u, 152u, 187u, 144u, 152u, 17u, 18u, 60u,
33u, 225u, 129u, 36u, 130u, 60u, 33u, 193u, 129u, 144u, 9u, 36u, 54u,
1u, 67u, 13u, 36u, 153u, 36u, 129u, 67u, 15u, 36u, 138u, 24u, 136u,
252u, 154u, 24u, 145u, 66u, 19u, 66u, 187u, 66u, 20u, 24u, 100u, 146u,
72u, 105u, 12u, 244u, 49u, 82u, 73u, 36u, 144u, 154u, 23u, 34u, 72u,
240u, 133u, 134u, 4u, 208u, 245u, 178u, 60u, 33u, 65u, 129u, 36u,
146u, 60u, 32u, 193u, 129u, 52u, 61u, 108u, 143u, 8u, 16u, 96u, 161u,
93u, 161u, 139u, 9u, 161u, 226u, 68u, 143u, 8u, 8u, 96u, 72u, 124u,
142u, 254u, 24u, 19u, 67u, 131u, 8u, 239u, 193u, 129u, 33u, 242u, 59u,
232u, 96u, 77u, 14u, 12u, 35u, 189u, 134u, 4u, 208u, 224u, 242u, 59u,
200u, 96u, 77u, 14u, 76u, 35u, 188u, 6u, 4u, 143u, 200u, 238u, 193u,
129u, 52u, 57u, 48u, 142u, 234u, 24u, 18u, 73u, 35u, 185u, 134u, 4u,
208u, 224u, 242u, 59u, 128u, 96u, 73u, 36u, 142u, 220u, 24u, 19u, 67u,
103u, 8u, 237u, 161u, 129u, 52u, 54u, 124u, 142u, 216u, 24u, 18u, 73u,
35u, 181u, 134u, 4u, 161u, 68u, 35u, 180u, 134u, 10u, 24u, 193u, 66u,
132u, 40u, 80u, 195u, 216u, 143u, 40u, 67u, 72u, 97u, 142u, 73u, 52u,
40u, 132u, 80u, 198u, 138u, 20u, 48u, 246u, 35u, 202u, 16u, 210u, 24u,
99u, 146u, 77u, 10u, 33u, 36u, 118u, 112u, 192u, 154u, 50u, 34u, 71u,
102u, 12u, 9u, 66u, 136u, 71u, 101u, 12u, 9u, 67u, 28u, 71u, 99u, 12u,
9u, 163u, 34u, 36u, 118u, 16u, 192u, 154u, 48u, 30u, 71u, 96u, 12u,
9u, 66u, 136u, 71u, 95u, 12u, 9u, 67u, 30u, 71u, 93u, 12u, 9u, 163u,
1u, 228u, 117u, 176u, 192u, 203u, 9u, 132u, 209u, 125u, 202u, 20u,
66u, 71u, 89u, 12u, 9u, 9u, 145u, 213u, 131u, 5u, 10u, 184u, 161u, 9u,
161u, 93u, 161u, 10u, 12u, 49u, 169u, 34u, 18u, 146u, 36u, 154u, 25u,
2u, 72u, 234u, 129u, 129u, 52u, 125u, 24u, 142u, 166u, 24u, 18u, 134u,
64u, 142u, 164u, 24u, 18u, 73u, 35u, 168u, 134u, 4u, 146u, 8u, 233u,
225u, 129u, 52u, 125u, 24u, 142u, 156u, 24u, 18u, 134u, 68u, 142u,
152u, 24u, 19u, 71u, 161u, 72u, 233u, 33u, 130u, 51u, 136u, 168u,
100u, 162u, 8u, 180u, 145u, 210u, 3u, 2u, 69u, 164u, 116u, 64u, 192u,
148u, 50u, 68u, 116u, 0u, 192u, 154u, 69u, 12u, 71u, 61u, 12u, 20u,
50u, 114u, 73u, 35u, 158u, 6u, 4u, 146u, 72u, 231u, 65u, 129u, 52u,
145u, 44u, 142u, 110u, 24u, 25u, 227u, 232u, 252u, 145u, 205u, 131u,
2u, 71u, 228u, 115u, 32u, 192u, 154u, 75u, 4u, 71u, 46u, 12u, 9u, 67u,
40u, 71u, 43u, 12u, 2u, 134u, 84u, 208u, 202u, 18u, 57u, 80u, 96u,
72u, 252u, 142u, 76u, 24u, 18u, 86u, 35u, 145u, 134u, 1u, 67u, 44u,
101u, 98u, 71u, 34u, 12u, 9u, 31u, 145u, 200u, 3u, 3u, 44u, 32u, 40u,
86u, 192u, 139u, 68u, 71u, 31u, 132u, 210u, 132u, 64u, 154u, 78u, 22u,
26u, 66u, 105u, 44u, 17u, 36u, 73u, 33u, 52u, 145u, 44u, 138u, 25u,
0u, 154u, 69u, 12u, 69u, 10u, 32u, 77u, 30u, 133u, 36u, 38u, 142u,
175u, 144u, 219u, 36u, 132u, 208u, 217u, 16u, 154u, 23u, 34u, 73u,
28u, 120u, 48u, 36u, 32u, 71u, 28u, 12u, 12u, 244u, 50u, 225u, 132u,
49u, 137u, 34u, 73u, 161u, 152u, 17u, 248u, 97u, 161u, 150u, 36u,
132u, 210u, 144u, 210u, 73u, 28u, 88u, 48u, 38u, 150u, 129u, 145u,
196u, 131u, 3u, 93u, 12u, 201u, 35u, 136u, 134u, 4u, 161u, 153u, 35u,
136u, 6u, 6u, 64u, 35u, 240u, 202u, 26u, 232u, 102u, 136u, 77u, 45u,
200u, 13u, 20u, 51u, 68u, 145u, 67u, 50u, 25u, 67u, 93u, 12u, 209u,
12u, 42u, 67u, 85u, 12u, 209u, 36u, 134u, 240u, 154u, 93u, 186u, 19u,
75u, 64u, 200u, 100u, 84u, 210u, 224u, 146u, 71u, 15u, 12u, 9u, 166u,
42u, 164u, 112u, 160u, 192u, 154u, 98u, 10u, 71u, 9u, 12u, 9u, 67u,
54u, 71u, 7u, 12u, 9u, 166u, 16u, 100u, 112u, 16u, 192u, 154u, 96u,
20u, 71u, 0u, 12u, 9u, 67u, 54u, 67u, 16u, 96u, 77u, 45u, 73u, 36u,};
static unsigned char uvector__00048[] = {
 0u, 3u, 135u, 6u, 6u, 97u, 50u, 73u, 18u, 77u, 12u, 192u, 143u, 195u,
13u, 12u, 177u, 36u, 50u, 194u, 2u, 133u, 108u, 8u, 180u, 68u, 113u,
248u, 20u, 50u, 198u, 86u, 32u, 80u, 202u, 154u, 25u, 66u, 26u, 67u,
60u, 125u, 31u, 146u, 72u, 146u, 69u, 12u, 156u, 146u, 72u, 161u,
144u, 17u, 156u, 69u, 67u, 37u, 16u, 69u, 164u, 138u, 20u, 65u, 66u,
174u, 40u, 66u, 104u, 87u, 104u, 66u, 131u, 12u, 106u, 72u, 132u,
164u, 137u, 38u, 134u, 64u, 146u, 67u, 44u, 38u, 40u, 99u, 5u, 10u,
16u, 161u, 67u, 15u, 98u, 60u, 161u, 13u, 33u, 134u, 57u, 36u, 208u,
162u, 17u, 67u, 26u, 40u, 80u, 195u, 216u, 143u, 40u, 67u, 72u, 97u,
142u, 73u, 52u, 40u, 132u, 208u, 162u, 18u, 27u, 100u, 144u, 216u, 5u,
12u, 52u, 146u, 100u, 146u, 5u, 12u, 60u, 146u, 40u, 98u, 35u, 242u,
104u, 98u, 69u, 8u, 77u, 10u, 237u, 8u, 80u, 97u, 146u, 73u, 33u,
164u, 51u, 208u, 197u, 73u, 36u, 144u, 203u, 36u, 6u, 218u, 24u, 33u,
67u, 11u, 36u, 16u, 223u, 67u, 4u, 25u, 232u, 97u, 36u, 130u, 69u,
10u, 184u, 161u, 9u, 137u, 232u, 87u, 232u, 84u, 168u, 66u, 168u, 85u,
36u, 146u, 40u, 87u, 104u, 96u, 131u, 13u, 12u, 25u, 36u, 135u, 209u,
19u, 208u, 175u, 208u, 169u, 80u, 192u, 208u, 170u, 73u, 36u, 146u,
73u, 28u, 44u, 48u, 49u, 132u, 192u, 197u, 48u, 134u, 35u, 132u, 134u,
4u, 192u, 112u, 142u, 8u, 24u, 19u, 1u, 194u, 56u, 16u, 96u, 76u, 7u,
9u, 0u,};
static unsigned char uvector__00049[] = {
 0u, 3u, 147u, 134u, 10u, 25u, 192u, 203u, 67u, 58u, 40u, 103u, 165u,
58u, 25u, 250u, 25u, 210u, 40u, 103u, 165u, 50u, 67u, 45u, 14u, 0u,
161u, 193u, 148u, 232u, 103u, 232u, 112u, 8u, 161u, 193u, 148u, 201u,
36u, 114u, 96u, 192u, 148u, 51u, 132u, 114u, 48u, 192u, 152u, 132u,
17u, 200u, 131u, 2u, 74u, 100u, 114u, 0u, 192u, 152u, 132u, 17u, 199u,
3u, 2u, 97u, 144u, 71u, 27u, 12u, 9u, 67u, 128u, 71u, 24u, 12u, 9u,
41u, 145u, 197u, 131u, 2u, 97u, 144u, 71u, 20u, 12u, 9u, 133u, 105u,
28u, 76u, 48u, 37u, 14u, 1u, 28u, 64u, 48u, 38u, 17u, 68u, 112u, 240u,
192u, 146u, 153u, 28u, 52u, 48u, 38u, 17u, 68u, 112u, 144u, 192u,
152u, 37u, 17u, 194u, 3u, 2u, 80u, 206u, 145u, 193u, 67u, 2u, 74u,
100u, 112u, 48u, 192u, 152u, 37u, 17u, 192u, 67u, 2u, 96u, 94u, 71u,
0u, 12u, 9u, 67u, 58u, 67u, 16u, 96u, 99u, 80u, 152u, 14u, 18u, 64u,};
static unsigned char uvector__00050[] = {
 0u, 3u, 128u, 134u, 10u, 28u, 41u, 76u, 145u, 192u, 3u, 2u, 74u,
100u, 49u, 6u, 6u, 53u, 9u, 128u, 225u, 36u,};
static unsigned char uvector__00051[] = {
 0u, 3u, 175u, 134u, 10u, 28u, 48u, 249u, 67u, 136u, 104u, 113u, 104u,
113u, 137u, 29u, 120u, 48u, 37u, 14u, 49u, 29u, 116u, 48u, 37u, 14u,
41u, 29u, 112u, 48u, 38u, 5u, 228u, 117u, 176u, 192u, 148u, 56u, 132u,
117u, 144u, 193u, 14u, 67u, 161u, 162u, 135u, 16u, 146u, 58u, 192u,
96u, 76u, 87u, 136u, 234u, 129u, 129u, 49u, 71u, 35u, 168u, 134u, 10u,
28u, 48u, 249u, 67u, 136u, 104u, 113u, 67u, 189u, 14u, 33u, 36u, 117u,
0u, 192u, 153u, 26u, 145u, 211u, 195u, 2u, 80u, 226u, 17u, 211u, 131u,
2u, 80u, 226u, 145u, 211u, 67u, 2u, 100u, 14u, 71u, 76u, 12u, 9u, 67u,
136u, 71u, 73u, 12u, 2u, 135u, 28u, 132u, 199u, 208u, 145u, 210u, 3u,
2u, 67u, 228u, 116u, 80u, 193u, 67u, 134u, 31u, 40u, 113u, 8u, 119u,
161u, 196u, 52u, 56u, 196u, 142u, 136u, 24u, 18u, 135u, 24u, 142u,
134u, 24u, 19u, 66u, 0u, 8u, 232u, 65u, 129u, 40u, 113u, 8u, 232u,
33u, 129u, 51u, 226u, 35u, 160u, 6u, 4u, 161u, 196u, 35u, 158u, 134u,
1u, 67u, 144u, 66u, 103u, 158u, 72u, 231u, 129u, 129u, 33u, 242u, 57u,
216u, 96u, 136u, 196u, 72u, 26u, 40u, 113u, 9u, 9u, 161u, 75u, 129u,
51u, 91u, 13u, 33u, 232u, 38u, 40u, 225u, 48u, 28u, 36u, 145u, 206u,
67u, 2u, 104u, 90u, 233u, 28u, 224u, 48u, 38u, 133u, 197u, 17u, 205u,
131u, 2u, 104u, 90u, 233u, 28u, 204u, 48u, 51u, 196u, 180u, 56u, 132u,
142u, 100u, 24u, 18u, 135u, 16u, 142u, 92u, 24u, 26u, 195u, 85u, 14u,
33u, 36u, 114u, 208u, 192u, 154u, 30u, 42u, 71u, 42u, 12u, 20u, 57u,
33u, 141u, 69u, 12u, 224u, 101u, 161u, 157u, 20u, 51u, 210u, 157u,
12u, 253u, 12u, 233u, 20u, 51u, 210u, 153u, 33u, 150u, 135u, 0u, 80u,
224u, 202u, 116u, 51u, 244u, 56u, 4u, 80u, 224u, 202u, 100u, 146u,
24u, 212u, 80u, 225u, 74u, 100u, 134u, 26u, 28u, 162u, 24u, 69u, 14u,
95u, 155u, 56u, 146u, 71u, 35u, 12u, 12u, 105u, 162u, 80u, 68u, 114u,
0u, 192u, 198u, 154u, 32u, 60u, 71u, 30u, 12u, 9u, 67u, 140u, 71u,
25u, 12u, 12u, 162u, 47u, 161u, 198u, 45u, 212u, 56u, 196u, 142u, 46u,
24u, 19u, 69u, 67u, 136u, 226u, 193u, 129u, 40u, 113u, 136u, 226u,
129u, 129u, 52u, 84u, 56u, 142u, 36u, 24u, 18u, 135u, 20u, 142u, 26u,
24u, 25u, 68u, 95u, 67u, 138u, 91u, 168u, 113u, 73u, 28u, 44u, 48u,
38u, 139u, 170u, 145u, 194u, 131u, 2u, 80u, 226u, 145u, 194u, 3u, 2u,
104u, 186u, 169u, 28u, 24u, 48u, 53u, 208u, 226u, 18u, 56u, 40u, 96u,
74u, 28u, 66u, 56u, 0u, 96u, 74u, 28u, 194u, 24u, 131u, 3u, 24u, 148u,
232u, 103u, 35u, 33u, 67u, 58u, 220u, 69u, 14u, 3u, 113u, 34u, 135u,
52u, 38u, 136u, 34u, 166u, 137u, 67u, 18u, 64u,};
static unsigned char uvector__00052[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 67u, 157u, 41u, 208u, 206u, 70u, 66u,
134u, 117u, 184u, 138u, 28u, 6u, 226u, 69u, 14u, 104u, 161u, 156u,
12u, 180u, 51u, 162u, 134u, 122u, 83u, 161u, 159u, 161u, 157u, 34u,
134u, 122u, 83u, 36u, 50u, 208u, 224u, 10u, 28u, 25u, 78u, 134u, 126u,
135u, 0u, 138u, 28u, 25u, 76u, 146u, 40u, 112u, 165u, 50u, 73u, 28u,
36u, 48u, 49u, 132u, 192u, 221u, 48u, 154u, 35u, 131u, 134u, 4u, 192u,
112u, 142u, 4u, 24u, 19u, 1u, 194u, 56u, 0u, 96u, 76u, 7u, 9u, 0u,};
static ScmObj SCM_debug_info_const_vector();
#if defined(__CYGWIN__) || defined(GAUCHE_WINDOWS)
#define SCM_CGEN_CONST /*empty*/
#else
#define SCM_CGEN_CONST const
#endif
static SCM_CGEN_CONST struct scm__scRec {
#if defined(NI_DGRAM)
  ScmString d2057[1];
#endif /*defined(NI_DGRAM)*/
#if defined(NI_NUMERICSERV)
  ScmString d2055[1];
#endif /*defined(NI_NUMERICSERV)*/
#if defined(NI_NAMEREQD)
  ScmString d2053[1];
#endif /*defined(NI_NAMEREQD)*/
#if defined(NI_NUMERICHOST)
  ScmString d2051[1];
#endif /*defined(NI_NUMERICHOST)*/
#if defined(NI_NOFQDN)
  ScmString d2049[1];
#endif /*defined(NI_NOFQDN)*/
#if defined(AI_ADDRCONFIG)
  ScmString d2047[1];
#endif /*defined(AI_ADDRCONFIG)*/
#if defined(AI_ALL)
  ScmString d2045[1];
#endif /*defined(AI_ALL)*/
#if defined(AI_V4MAPPED)
  ScmString d2043[1];
#endif /*defined(AI_V4MAPPED)*/
#if defined(AI_NUMERICSERV)
  ScmString d2041[1];
#endif /*defined(AI_NUMERICSERV)*/
#if defined(AI_NUMERICHOST)
  ScmString d2039[1];
#endif /*defined(AI_NUMERICHOST)*/
#if defined(AI_CANONNAME)
  ScmString d2037[1];
#endif /*defined(AI_CANONNAME)*/
#if defined(AI_PASSIVE)
  ScmString d2035[1];
#endif /*defined(AI_PASSIVE)*/
#if defined(IPV6_V6ONLY)
  ScmString d2033[1];
#endif /*defined(IPV6_V6ONLY)*/
#if defined(IPV6_LEAVE_GROUP)
  ScmString d2031[1];
#endif /*defined(IPV6_LEAVE_GROUP)*/
#if defined(IPV6_JOIN_GROUP)
  ScmString d2029[1];
#endif /*defined(IPV6_JOIN_GROUP)*/
#if defined(IPV6_MULTICAST_LOOP)
  ScmString d2027[1];
#endif /*defined(IPV6_MULTICAST_LOOP)*/
#if defined(IPV6_MULTICAST_HOPS)
  ScmString d2025[1];
#endif /*defined(IPV6_MULTICAST_HOPS)*/
#if defined(IPV6_MULTICAST_IF)
  ScmString d2023[1];
#endif /*defined(IPV6_MULTICAST_IF)*/
#if defined(IPV6_UNICAST_HOPS)
  ScmString d2021[1];
#endif /*defined(IPV6_UNICAST_HOPS)*/
#if defined(IPPROTO_IPV6)
  ScmString d2019[1];
#endif /*defined(IPPROTO_IPV6)*/
#if defined(PF_INET6)
  ScmString d2017[1];
#endif /*defined(PF_INET6)*/
#if defined(AF_INET6)
  ScmString d2015[1];
#endif /*defined(AF_INET6)*/
#if defined(IFF_DYNAMIC)
  ScmString d2012[1];
#endif /*defined(IFF_DYNAMIC)*/
#if defined(IFF_AUTOMEDIA)
  ScmString d2010[1];
#endif /*defined(IFF_AUTOMEDIA)*/
#if defined(IFF_PORTSEL)
  ScmString d2008[1];
#endif /*defined(IFF_PORTSEL)*/
#if defined(IFF_MULTICAST)
  ScmString d2006[1];
#endif /*defined(IFF_MULTICAST)*/
#if defined(IFF_SLAVE)
  ScmString d2004[1];
#endif /*defined(IFF_SLAVE)*/
#if defined(IFF_MASTER)
  ScmString d2002[1];
#endif /*defined(IFF_MASTER)*/
#if defined(IFF_ALLMULTI)
  ScmString d2000[1];
#endif /*defined(IFF_ALLMULTI)*/
#if defined(IFF_NOTRAILERS)
  ScmString d1998[1];
#endif /*defined(IFF_NOTRAILERS)*/
#if defined(IFF_PROMISC)
  ScmString d1996[1];
#endif /*defined(IFF_PROMISC)*/
#if defined(IFF_NOARP)
  ScmString d1994[1];
#endif /*defined(IFF_NOARP)*/
#if defined(IFF_RUNNING)
  ScmString d1992[1];
#endif /*defined(IFF_RUNNING)*/
#if defined(IFF_POINTTOPOINT)
  ScmString d1990[1];
#endif /*defined(IFF_POINTTOPOINT)*/
#if defined(IFF_LOOPBACK)
  ScmString d1988[1];
#endif /*defined(IFF_LOOPBACK)*/
#if defined(IFF_DEBUG)
  ScmString d1986[1];
#endif /*defined(IFF_DEBUG)*/
#if defined(IFF_BROADCAST)
  ScmString d1984[1];
#endif /*defined(IFF_BROADCAST)*/
#if defined(IFF_UP)
  ScmString d1982[1];
#endif /*defined(IFF_UP)*/
#if defined(SIOCGIFCONF)
  ScmString d1980[1];
#endif /*defined(SIOCGIFCONF)*/
#if defined(SIOSIFTXQLEN)
  ScmString d1978[1];
#endif /*defined(SIOSIFTXQLEN)*/
#if defined(SIOGIFTXQLEN)
  ScmString d1976[1];
#endif /*defined(SIOGIFTXQLEN)*/
#if defined(SIOCDELMULTI)
  ScmString d1974[1];
#endif /*defined(SIOCDELMULTI)*/
#if defined(SIOCADDMULTI)
  ScmString d1972[1];
#endif /*defined(SIOCADDMULTI)*/
#if defined(SIOCSIFMAP)
  ScmString d1970[1];
#endif /*defined(SIOCSIFMAP)*/
#if defined(SIOCGIFMAP)
  ScmString d1968[1];
#endif /*defined(SIOCGIFMAP)*/
#if defined(SIOCSIFHWBROADCAST)
  ScmString d1966[1];
#endif /*defined(SIOCSIFHWBROADCAST)*/
#if defined(SIOCSIFHWADDR)
  ScmString d1964[1];
#endif /*defined(SIOCSIFHWADDR)*/
#if defined(SIOCGIFHWADDR)
  ScmString d1962[1];
#endif /*defined(SIOCGIFHWADDR)*/
#if defined(SIOCSIFMTU)
  ScmString d1960[1];
#endif /*defined(SIOCSIFMTU)*/
#if defined(SIOCGIFMTU)
  ScmString d1958[1];
#endif /*defined(SIOCGIFMTU)*/
#if defined(SIOCSIFMETRIC)
  ScmString d1956[1];
#endif /*defined(SIOCSIFMETRIC)*/
#if defined(SIOCGIFMETRIC)
  ScmString d1954[1];
#endif /*defined(SIOCGIFMETRIC)*/
#if defined(SIOCSIFFLAGS)
  ScmString d1952[1];
#endif /*defined(SIOCSIFFLAGS)*/
#if defined(SIOCGIFFLAGS)
  ScmString d1950[1];
#endif /*defined(SIOCGIFFLAGS)*/
#if defined(SIOCSIFNETMASK)
  ScmString d1948[1];
#endif /*defined(SIOCSIFNETMASK)*/
#if defined(SIOCGIFNETMASK)
  ScmString d1946[1];
#endif /*defined(SIOCGIFNETMASK)*/
#if defined(SIOCSIFBRDADDR)
  ScmString d1944[1];
#endif /*defined(SIOCSIFBRDADDR)*/
#if defined(SIOCGIFBRDADDR)
  ScmString d1942[1];
#endif /*defined(SIOCGIFBRDADDR)*/
#if defined(SIOCSIFDSTADDR)
  ScmString d1940[1];
#endif /*defined(SIOCSIFDSTADDR)*/
#if defined(SIOCGIFDSTADDR)
  ScmString d1938[1];
#endif /*defined(SIOCGIFDSTADDR)*/
#if defined(SIOCSIFADDR)
  ScmString d1936[1];
#endif /*defined(SIOCSIFADDR)*/
#if defined(SIOCGIFADDR)
  ScmString d1934[1];
#endif /*defined(SIOCGIFADDR)*/
#if defined(SIOCGIFINDEX)
  ScmString d1932[1];
#endif /*defined(SIOCGIFINDEX)*/
#if defined(SIOCSIFNAME)
  ScmString d1930[1];
#endif /*defined(SIOCSIFNAME)*/
#if defined(SIOCGIFNAME)
  ScmString d1928[1];
#endif /*defined(SIOCGIFNAME)*/
#if defined(IP_MULTICAST_IF)
  ScmString d1926[1];
#endif /*defined(IP_MULTICAST_IF)*/
#if defined(IP_DROP_MEMBERSHIP)
  ScmString d1924[1];
#endif /*defined(IP_DROP_MEMBERSHIP)*/
#if defined(IP_ADD_MEMBERSHIP)
  ScmString d1922[1];
#endif /*defined(IP_ADD_MEMBERSHIP)*/
#if defined(IP_MULTICAST_LOOP)
  ScmString d1920[1];
#endif /*defined(IP_MULTICAST_LOOP)*/
#if defined(IP_MULTICAST_TTL)
  ScmString d1918[1];
#endif /*defined(IP_MULTICAST_TTL)*/
#if defined(IP_ROUTER_ALERT)
  ScmString d1916[1];
#endif /*defined(IP_ROUTER_ALERT)*/
#if defined(IP_MTU)
  ScmString d1914[1];
#endif /*defined(IP_MTU)*/
#if defined(IP_MTU_DISCOVER)
  ScmString d1912[1];
#endif /*defined(IP_MTU_DISCOVER)*/
#if defined(IP_RECVERR)
  ScmString d1910[1];
#endif /*defined(IP_RECVERR)*/
#if defined(IP_HDRINCL)
  ScmString d1908[1];
#endif /*defined(IP_HDRINCL)*/
#if defined(IP_TTL)
  ScmString d1906[1];
#endif /*defined(IP_TTL)*/
#if defined(IP_TOS)
  ScmString d1904[1];
#endif /*defined(IP_TOS)*/
#if defined(IP_RECVOPTS)
  ScmString d1902[1];
#endif /*defined(IP_RECVOPTS)*/
#if defined(IP_RECVTTL)
  ScmString d1900[1];
#endif /*defined(IP_RECVTTL)*/
#if defined(IP_RECVTOS)
  ScmString d1898[1];
#endif /*defined(IP_RECVTOS)*/
#if defined(IP_PKTINFO)
  ScmString d1896[1];
#endif /*defined(IP_PKTINFO)*/
#if defined(IP_OPTIONS)
  ScmString d1894[1];
#endif /*defined(IP_OPTIONS)*/
#if defined(SOL_IP)
  ScmString d1892[1];
#endif /*defined(SOL_IP)*/
#if defined(TCP_CORK)
  ScmString d1890[1];
#endif /*defined(TCP_CORK)*/
#if defined(TCP_MAXSEG)
  ScmString d1888[1];
#endif /*defined(TCP_MAXSEG)*/
#if defined(TCP_NODELAY)
  ScmString d1886[1];
#endif /*defined(TCP_NODELAY)*/
#if defined(SOL_TCP)
  ScmString d1884[1];
#endif /*defined(SOL_TCP)*/
#if defined(SO_TYPE)
  ScmString d1882[1];
#endif /*defined(SO_TYPE)*/
#if defined(SO_TIMESTAMP)
  ScmString d1880[1];
#endif /*defined(SO_TIMESTAMP)*/
#if defined(SO_SNDTIMEO)
  ScmString d1878[1];
#endif /*defined(SO_SNDTIMEO)*/
#if defined(SO_SNDLOWAT)
  ScmString d1876[1];
#endif /*defined(SO_SNDLOWAT)*/
#if defined(SO_SNDBUF)
  ScmString d1874[1];
#endif /*defined(SO_SNDBUF)*/
#if defined(SO_REUSEPORT)
  ScmString d1872[1];
#endif /*defined(SO_REUSEPORT)*/
#if defined(SO_REUSEADDR)
  ScmString d1870[1];
#endif /*defined(SO_REUSEADDR)*/
#if defined(SO_RCVTIMEO)
  ScmString d1868[1];
#endif /*defined(SO_RCVTIMEO)*/
#if defined(SO_RCVLOWAT)
  ScmString d1866[1];
#endif /*defined(SO_RCVLOWAT)*/
#if defined(SO_RCVBUF)
  ScmString d1864[1];
#endif /*defined(SO_RCVBUF)*/
#if defined(SO_PRIORITY)
  ScmString d1862[1];
#endif /*defined(SO_PRIORITY)*/
#if defined(SO_PEERCRED)
  ScmString d1860[1];
#endif /*defined(SO_PEERCRED)*/
#if defined(SO_PASSCRED)
  ScmString d1858[1];
#endif /*defined(SO_PASSCRED)*/
#if defined(SO_OOBINLINE)
  ScmString d1856[1];
#endif /*defined(SO_OOBINLINE)*/
#if defined(SO_LINGER)
  ScmString d1854[1];
#endif /*defined(SO_LINGER)*/
#if defined(SO_KEEPALIVE)
  ScmString d1852[1];
#endif /*defined(SO_KEEPALIVE)*/
#if defined(SO_ERROR)
  ScmString d1850[1];
#endif /*defined(SO_ERROR)*/
#if defined(SO_DONTROUTE)
  ScmString d1848[1];
#endif /*defined(SO_DONTROUTE)*/
#if defined(SO_DEBUG)
  ScmString d1846[1];
#endif /*defined(SO_DEBUG)*/
#if defined(SO_BROADCAST)
  ScmString d1844[1];
#endif /*defined(SO_BROADCAST)*/
#if defined(SO_BINDTODEVICE)
  ScmString d1842[1];
#endif /*defined(SO_BINDTODEVICE)*/
#if defined(SO_ACCEPTCONN)
  ScmString d1840[1];
#endif /*defined(SO_ACCEPTCONN)*/
#if defined(SOL_SOCKET)
  ScmString d1838[1];
#endif /*defined(SOL_SOCKET)*/
#if !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))
  ScmString d1836[3];
#endif /*!((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))*/
#if (defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))
  ScmString d1834[3];
#endif /*(defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))*/
#if defined(SOMAXCONN)
  ScmString d1832[1];
#endif /*defined(SOMAXCONN)*/
#if defined(IPPROTO_UDP)
  ScmString d1830[1];
#endif /*defined(IPPROTO_UDP)*/
#if defined(IPPROTO_TCP)
  ScmString d1828[1];
#endif /*defined(IPPROTO_TCP)*/
#if defined(IPPROTO_ICMPV6)
  ScmString d1826[1];
#endif /*defined(IPPROTO_ICMPV6)*/
#if defined(IPPROTO_ICMP)
  ScmString d1824[1];
#endif /*defined(IPPROTO_ICMP)*/
#if defined(IPPROTO_IP)
  ScmString d1822[1];
#endif /*defined(IPPROTO_IP)*/
#if defined(MSG_WAITALL)
  ScmString d1820[1];
#endif /*defined(MSG_WAITALL)*/
#if defined(MSG_TRUNC)
  ScmString d1818[1];
#endif /*defined(MSG_TRUNC)*/
#if defined(MSG_PEEK)
  ScmString d1816[1];
#endif /*defined(MSG_PEEK)*/
#if defined(MSG_OOB)
  ScmString d1814[1];
#endif /*defined(MSG_OOB)*/
#if defined(MSG_EOR)
  ScmString d1812[1];
#endif /*defined(MSG_EOR)*/
#if defined(MSG_DONTROUTE)
  ScmString d1810[1];
#endif /*defined(MSG_DONTROUTE)*/
#if defined(MSG_CTRUNC)
  ScmString d1808[1];
#endif /*defined(MSG_CTRUNC)*/
#if defined(HAVE_IPV6)
  ScmString d1802[18];
#endif /*defined(HAVE_IPV6)*/
  ScmString d1787[441];
} scm__sc SCM_UNUSED = {
#if defined(NI_DGRAM)
  {   /* ScmString d2057 */
      SCM_STRING_CONST_INITIALIZER("NI_DGRAM", 8, 8),
  },
#endif /*defined(NI_DGRAM)*/
#if defined(NI_NUMERICSERV)
  {   /* ScmString d2055 */
      SCM_STRING_CONST_INITIALIZER("NI_NUMERICSERV", 14, 14),
  },
#endif /*defined(NI_NUMERICSERV)*/
#if defined(NI_NAMEREQD)
  {   /* ScmString d2053 */
      SCM_STRING_CONST_INITIALIZER("NI_NAMEREQD", 11, 11),
  },
#endif /*defined(NI_NAMEREQD)*/
#if defined(NI_NUMERICHOST)
  {   /* ScmString d2051 */
      SCM_STRING_CONST_INITIALIZER("NI_NUMERICHOST", 14, 14),
  },
#endif /*defined(NI_NUMERICHOST)*/
#if defined(NI_NOFQDN)
  {   /* ScmString d2049 */
      SCM_STRING_CONST_INITIALIZER("NI_NOFQDN", 9, 9),
  },
#endif /*defined(NI_NOFQDN)*/
#if defined(AI_ADDRCONFIG)
  {   /* ScmString d2047 */
      SCM_STRING_CONST_INITIALIZER("AI_ADDRCONFIG", 13, 13),
  },
#endif /*defined(AI_ADDRCONFIG)*/
#if defined(AI_ALL)
  {   /* ScmString d2045 */
      SCM_STRING_CONST_INITIALIZER("AI_ALL", 6, 6),
  },
#endif /*defined(AI_ALL)*/
#if defined(AI_V4MAPPED)
  {   /* ScmString d2043 */
      SCM_STRING_CONST_INITIALIZER("AI_V4MAPPED", 11, 11),
  },
#endif /*defined(AI_V4MAPPED)*/
#if defined(AI_NUMERICSERV)
  {   /* ScmString d2041 */
      SCM_STRING_CONST_INITIALIZER("AI_NUMERICSERV", 14, 14),
  },
#endif /*defined(AI_NUMERICSERV)*/
#if defined(AI_NUMERICHOST)
  {   /* ScmString d2039 */
      SCM_STRING_CONST_INITIALIZER("AI_NUMERICHOST", 14, 14),
  },
#endif /*defined(AI_NUMERICHOST)*/
#if defined(AI_CANONNAME)
  {   /* ScmString d2037 */
      SCM_STRING_CONST_INITIALIZER("AI_CANONNAME", 12, 12),
  },
#endif /*defined(AI_CANONNAME)*/
#if defined(AI_PASSIVE)
  {   /* ScmString d2035 */
      SCM_STRING_CONST_INITIALIZER("AI_PASSIVE", 10, 10),
  },
#endif /*defined(AI_PASSIVE)*/
#if defined(IPV6_V6ONLY)
  {   /* ScmString d2033 */
      SCM_STRING_CONST_INITIALIZER("IPV6_V6ONLY", 11, 11),
  },
#endif /*defined(IPV6_V6ONLY)*/
#if defined(IPV6_LEAVE_GROUP)
  {   /* ScmString d2031 */
      SCM_STRING_CONST_INITIALIZER("IPV6_LEAVE_GROUP", 16, 16),
  },
#endif /*defined(IPV6_LEAVE_GROUP)*/
#if defined(IPV6_JOIN_GROUP)
  {   /* ScmString d2029 */
      SCM_STRING_CONST_INITIALIZER("IPV6_JOIN_GROUP", 15, 15),
  },
#endif /*defined(IPV6_JOIN_GROUP)*/
#if defined(IPV6_MULTICAST_LOOP)
  {   /* ScmString d2027 */
      SCM_STRING_CONST_INITIALIZER("IPV6_MULTICAST_LOOP", 19, 19),
  },
#endif /*defined(IPV6_MULTICAST_LOOP)*/
#if defined(IPV6_MULTICAST_HOPS)
  {   /* ScmString d2025 */
      SCM_STRING_CONST_INITIALIZER("IPV6_MULTICAST_HOPS", 19, 19),
  },
#endif /*defined(IPV6_MULTICAST_HOPS)*/
#if defined(IPV6_MULTICAST_IF)
  {   /* ScmString d2023 */
      SCM_STRING_CONST_INITIALIZER("IPV6_MULTICAST_IF", 17, 17),
  },
#endif /*defined(IPV6_MULTICAST_IF)*/
#if defined(IPV6_UNICAST_HOPS)
  {   /* ScmString d2021 */
      SCM_STRING_CONST_INITIALIZER("IPV6_UNICAST_HOPS", 17, 17),
  },
#endif /*defined(IPV6_UNICAST_HOPS)*/
#if defined(IPPROTO_IPV6)
  {   /* ScmString d2019 */
      SCM_STRING_CONST_INITIALIZER("IPPROTO_IPV6", 12, 12),
  },
#endif /*defined(IPPROTO_IPV6)*/
#if defined(PF_INET6)
  {   /* ScmString d2017 */
      SCM_STRING_CONST_INITIALIZER("PF_INET6", 8, 8),
  },
#endif /*defined(PF_INET6)*/
#if defined(AF_INET6)
  {   /* ScmString d2015 */
      SCM_STRING_CONST_INITIALIZER("AF_INET6", 8, 8),
  },
#endif /*defined(AF_INET6)*/
#if defined(IFF_DYNAMIC)
  {   /* ScmString d2012 */
      SCM_STRING_CONST_INITIALIZER("IFF_DYNAMIC", 11, 11),
  },
#endif /*defined(IFF_DYNAMIC)*/
#if defined(IFF_AUTOMEDIA)
  {   /* ScmString d2010 */
      SCM_STRING_CONST_INITIALIZER("IFF_AUTOMEDIA", 13, 13),
  },
#endif /*defined(IFF_AUTOMEDIA)*/
#if defined(IFF_PORTSEL)
  {   /* ScmString d2008 */
      SCM_STRING_CONST_INITIALIZER("IFF_PORTSEL", 11, 11),
  },
#endif /*defined(IFF_PORTSEL)*/
#if defined(IFF_MULTICAST)
  {   /* ScmString d2006 */
      SCM_STRING_CONST_INITIALIZER("IFF_MULTICAST", 13, 13),
  },
#endif /*defined(IFF_MULTICAST)*/
#if defined(IFF_SLAVE)
  {   /* ScmString d2004 */
      SCM_STRING_CONST_INITIALIZER("IFF_SLAVE", 9, 9),
  },
#endif /*defined(IFF_SLAVE)*/
#if defined(IFF_MASTER)
  {   /* ScmString d2002 */
      SCM_STRING_CONST_INITIALIZER("IFF_MASTER", 10, 10),
  },
#endif /*defined(IFF_MASTER)*/
#if defined(IFF_ALLMULTI)
  {   /* ScmString d2000 */
      SCM_STRING_CONST_INITIALIZER("IFF_ALLMULTI", 12, 12),
  },
#endif /*defined(IFF_ALLMULTI)*/
#if defined(IFF_NOTRAILERS)
  {   /* ScmString d1998 */
      SCM_STRING_CONST_INITIALIZER("IFF_NOTRAILERS", 14, 14),
  },
#endif /*defined(IFF_NOTRAILERS)*/
#if defined(IFF_PROMISC)
  {   /* ScmString d1996 */
      SCM_STRING_CONST_INITIALIZER("IFF_PROMISC", 11, 11),
  },
#endif /*defined(IFF_PROMISC)*/
#if defined(IFF_NOARP)
  {   /* ScmString d1994 */
      SCM_STRING_CONST_INITIALIZER("IFF_NOARP", 9, 9),
  },
#endif /*defined(IFF_NOARP)*/
#if defined(IFF_RUNNING)
  {   /* ScmString d1992 */
      SCM_STRING_CONST_INITIALIZER("IFF_RUNNING", 11, 11),
  },
#endif /*defined(IFF_RUNNING)*/
#if defined(IFF_POINTTOPOINT)
  {   /* ScmString d1990 */
      SCM_STRING_CONST_INITIALIZER("IFF_POINTTOPOINT", 16, 16),
  },
#endif /*defined(IFF_POINTTOPOINT)*/
#if defined(IFF_LOOPBACK)
  {   /* ScmString d1988 */
      SCM_STRING_CONST_INITIALIZER("IFF_LOOPBACK", 12, 12),
  },
#endif /*defined(IFF_LOOPBACK)*/
#if defined(IFF_DEBUG)
  {   /* ScmString d1986 */
      SCM_STRING_CONST_INITIALIZER("IFF_DEBUG", 9, 9),
  },
#endif /*defined(IFF_DEBUG)*/
#if defined(IFF_BROADCAST)
  {   /* ScmString d1984 */
      SCM_STRING_CONST_INITIALIZER("IFF_BROADCAST", 13, 13),
  },
#endif /*defined(IFF_BROADCAST)*/
#if defined(IFF_UP)
  {   /* ScmString d1982 */
      SCM_STRING_CONST_INITIALIZER("IFF_UP", 6, 6),
  },
#endif /*defined(IFF_UP)*/
#if defined(SIOCGIFCONF)
  {   /* ScmString d1980 */
      SCM_STRING_CONST_INITIALIZER("SIOCGIFCONF", 11, 11),
  },
#endif /*defined(SIOCGIFCONF)*/
#if defined(SIOSIFTXQLEN)
  {   /* ScmString d1978 */
      SCM_STRING_CONST_INITIALIZER("SIOSIFTXQLEN", 12, 12),
  },
#endif /*defined(SIOSIFTXQLEN)*/
#if defined(SIOGIFTXQLEN)
  {   /* ScmString d1976 */
      SCM_STRING_CONST_INITIALIZER("SIOGIFTXQLEN", 12, 12),
  },
#endif /*defined(SIOGIFTXQLEN)*/
#if defined(SIOCDELMULTI)
  {   /* ScmString d1974 */
      SCM_STRING_CONST_INITIALIZER("SIOCDELMULTI", 12, 12),
  },
#endif /*defined(SIOCDELMULTI)*/
#if defined(SIOCADDMULTI)
  {   /* ScmString d1972 */
      SCM_STRING_CONST_INITIALIZER("SIOCADDMULTI", 12, 12),
  },
#endif /*defined(SIOCADDMULTI)*/
#if defined(SIOCSIFMAP)
  {   /* ScmString d1970 */
      SCM_STRING_CONST_INITIALIZER("SIOCSIFMAP", 10, 10),
  },
#endif /*defined(SIOCSIFMAP)*/
#if defined(SIOCGIFMAP)
  {   /* ScmString d1968 */
      SCM_STRING_CONST_INITIALIZER("SIOCGIFMAP", 10, 10),
  },
#endif /*defined(SIOCGIFMAP)*/
#if defined(SIOCSIFHWBROADCAST)
  {   /* ScmString d1966 */
      SCM_STRING_CONST_INITIALIZER("SIOCSIFHWBROADCAST", 18, 18),
  },
#endif /*defined(SIOCSIFHWBROADCAST)*/
#if defined(SIOCSIFHWADDR)
  {   /* ScmString d1964 */
      SCM_STRING_CONST_INITIALIZER("SIOCSIFHWADDR", 13, 13),
  },
#endif /*defined(SIOCSIFHWADDR)*/
#if defined(SIOCGIFHWADDR)
  {   /* ScmString d1962 */
      SCM_STRING_CONST_INITIALIZER("SIOCGIFHWADDR", 13, 13),
  },
#endif /*defined(SIOCGIFHWADDR)*/
#if defined(SIOCSIFMTU)
  {   /* ScmString d1960 */
      SCM_STRING_CONST_INITIALIZER("SIOCSIFMTU", 10, 10),
  },
#endif /*defined(SIOCSIFMTU)*/
#if defined(SIOCGIFMTU)
  {   /* ScmString d1958 */
      SCM_STRING_CONST_INITIALIZER("SIOCGIFMTU", 10, 10),
  },
#endif /*defined(SIOCGIFMTU)*/
#if defined(SIOCSIFMETRIC)
  {   /* ScmString d1956 */
      SCM_STRING_CONST_INITIALIZER("SIOCSIFMETRIC", 13, 13),
  },
#endif /*defined(SIOCSIFMETRIC)*/
#if defined(SIOCGIFMETRIC)
  {   /* ScmString d1954 */
      SCM_STRING_CONST_INITIALIZER("SIOCGIFMETRIC", 13, 13),
  },
#endif /*defined(SIOCGIFMETRIC)*/
#if defined(SIOCSIFFLAGS)
  {   /* ScmString d1952 */
      SCM_STRING_CONST_INITIALIZER("SIOCSIFFLAGS", 12, 12),
  },
#endif /*defined(SIOCSIFFLAGS)*/
#if defined(SIOCGIFFLAGS)
  {   /* ScmString d1950 */
      SCM_STRING_CONST_INITIALIZER("SIOCGIFFLAGS", 12, 12),
  },
#endif /*defined(SIOCGIFFLAGS)*/
#if defined(SIOCSIFNETMASK)
  {   /* ScmString d1948 */
      SCM_STRING_CONST_INITIALIZER("SIOCSIFNETMASK", 14, 14),
  },
#endif /*defined(SIOCSIFNETMASK)*/
#if defined(SIOCGIFNETMASK)
  {   /* ScmString d1946 */
      SCM_STRING_CONST_INITIALIZER("SIOCGIFNETMASK", 14, 14),
  },
#endif /*defined(SIOCGIFNETMASK)*/
#if defined(SIOCSIFBRDADDR)
  {   /* ScmString d1944 */
      SCM_STRING_CONST_INITIALIZER("SIOCSIFBRDADDR", 14, 14),
  },
#endif /*defined(SIOCSIFBRDADDR)*/
#if defined(SIOCGIFBRDADDR)
  {   /* ScmString d1942 */
      SCM_STRING_CONST_INITIALIZER("SIOCGIFBRDADDR", 14, 14),
  },
#endif /*defined(SIOCGIFBRDADDR)*/
#if defined(SIOCSIFDSTADDR)
  {   /* ScmString d1940 */
      SCM_STRING_CONST_INITIALIZER("SIOCSIFDSTADDR", 14, 14),
  },
#endif /*defined(SIOCSIFDSTADDR)*/
#if defined(SIOCGIFDSTADDR)
  {   /* ScmString d1938 */
      SCM_STRING_CONST_INITIALIZER("SIOCGIFDSTADDR", 14, 14),
  },
#endif /*defined(SIOCGIFDSTADDR)*/
#if defined(SIOCSIFADDR)
  {   /* ScmString d1936 */
      SCM_STRING_CONST_INITIALIZER("SIOCSIFADDR", 11, 11),
  },
#endif /*defined(SIOCSIFADDR)*/
#if defined(SIOCGIFADDR)
  {   /* ScmString d1934 */
      SCM_STRING_CONST_INITIALIZER("SIOCGIFADDR", 11, 11),
  },
#endif /*defined(SIOCGIFADDR)*/
#if defined(SIOCGIFINDEX)
  {   /* ScmString d1932 */
      SCM_STRING_CONST_INITIALIZER("SIOCGIFINDEX", 12, 12),
  },
#endif /*defined(SIOCGIFINDEX)*/
#if defined(SIOCSIFNAME)
  {   /* ScmString d1930 */
      SCM_STRING_CONST_INITIALIZER("SIOCSIFNAME", 11, 11),
  },
#endif /*defined(SIOCSIFNAME)*/
#if defined(SIOCGIFNAME)
  {   /* ScmString d1928 */
      SCM_STRING_CONST_INITIALIZER("SIOCGIFNAME", 11, 11),
  },
#endif /*defined(SIOCGIFNAME)*/
#if defined(IP_MULTICAST_IF)
  {   /* ScmString d1926 */
      SCM_STRING_CONST_INITIALIZER("IP_MULTICAST_IF", 15, 15),
  },
#endif /*defined(IP_MULTICAST_IF)*/
#if defined(IP_DROP_MEMBERSHIP)
  {   /* ScmString d1924 */
      SCM_STRING_CONST_INITIALIZER("IP_DROP_MEMBERSHIP", 18, 18),
  },
#endif /*defined(IP_DROP_MEMBERSHIP)*/
#if defined(IP_ADD_MEMBERSHIP)
  {   /* ScmString d1922 */
      SCM_STRING_CONST_INITIALIZER("IP_ADD_MEMBERSHIP", 17, 17),
  },
#endif /*defined(IP_ADD_MEMBERSHIP)*/
#if defined(IP_MULTICAST_LOOP)
  {   /* ScmString d1920 */
      SCM_STRING_CONST_INITIALIZER("IP_MULTICAST_LOOP", 17, 17),
  },
#endif /*defined(IP_MULTICAST_LOOP)*/
#if defined(IP_MULTICAST_TTL)
  {   /* ScmString d1918 */
      SCM_STRING_CONST_INITIALIZER("IP_MULTICAST_TTL", 16, 16),
  },
#endif /*defined(IP_MULTICAST_TTL)*/
#if defined(IP_ROUTER_ALERT)
  {   /* ScmString d1916 */
      SCM_STRING_CONST_INITIALIZER("IP_ROUTER_ALERT", 15, 15),
  },
#endif /*defined(IP_ROUTER_ALERT)*/
#if defined(IP_MTU)
  {   /* ScmString d1914 */
      SCM_STRING_CONST_INITIALIZER("IP_MTU", 6, 6),
  },
#endif /*defined(IP_MTU)*/
#if defined(IP_MTU_DISCOVER)
  {   /* ScmString d1912 */
      SCM_STRING_CONST_INITIALIZER("IP_MTU_DISCOVER", 15, 15),
  },
#endif /*defined(IP_MTU_DISCOVER)*/
#if defined(IP_RECVERR)
  {   /* ScmString d1910 */
      SCM_STRING_CONST_INITIALIZER("IP_RECVERR", 10, 10),
  },
#endif /*defined(IP_RECVERR)*/
#if defined(IP_HDRINCL)
  {   /* ScmString d1908 */
      SCM_STRING_CONST_INITIALIZER("IP_HDRINCL", 10, 10),
  },
#endif /*defined(IP_HDRINCL)*/
#if defined(IP_TTL)
  {   /* ScmString d1906 */
      SCM_STRING_CONST_INITIALIZER("IP_TTL", 6, 6),
  },
#endif /*defined(IP_TTL)*/
#if defined(IP_TOS)
  {   /* ScmString d1904 */
      SCM_STRING_CONST_INITIALIZER("IP_TOS", 6, 6),
  },
#endif /*defined(IP_TOS)*/
#if defined(IP_RECVOPTS)
  {   /* ScmString d1902 */
      SCM_STRING_CONST_INITIALIZER("IP_RECVOPTS", 11, 11),
  },
#endif /*defined(IP_RECVOPTS)*/
#if defined(IP_RECVTTL)
  {   /* ScmString d1900 */
      SCM_STRING_CONST_INITIALIZER("IP_RECVTTL", 10, 10),
  },
#endif /*defined(IP_RECVTTL)*/
#if defined(IP_RECVTOS)
  {   /* ScmString d1898 */
      SCM_STRING_CONST_INITIALIZER("IP_RECVTOS", 10, 10),
  },
#endif /*defined(IP_RECVTOS)*/
#if defined(IP_PKTINFO)
  {   /* ScmString d1896 */
      SCM_STRING_CONST_INITIALIZER("IP_PKTINFO", 10, 10),
  },
#endif /*defined(IP_PKTINFO)*/
#if defined(IP_OPTIONS)
  {   /* ScmString d1894 */
      SCM_STRING_CONST_INITIALIZER("IP_OPTIONS", 10, 10),
  },
#endif /*defined(IP_OPTIONS)*/
#if defined(SOL_IP)
  {   /* ScmString d1892 */
      SCM_STRING_CONST_INITIALIZER("SOL_IP", 6, 6),
  },
#endif /*defined(SOL_IP)*/
#if defined(TCP_CORK)
  {   /* ScmString d1890 */
      SCM_STRING_CONST_INITIALIZER("TCP_CORK", 8, 8),
  },
#endif /*defined(TCP_CORK)*/
#if defined(TCP_MAXSEG)
  {   /* ScmString d1888 */
      SCM_STRING_CONST_INITIALIZER("TCP_MAXSEG", 10, 10),
  },
#endif /*defined(TCP_MAXSEG)*/
#if defined(TCP_NODELAY)
  {   /* ScmString d1886 */
      SCM_STRING_CONST_INITIALIZER("TCP_NODELAY", 11, 11),
  },
#endif /*defined(TCP_NODELAY)*/
#if defined(SOL_TCP)
  {   /* ScmString d1884 */
      SCM_STRING_CONST_INITIALIZER("SOL_TCP", 7, 7),
  },
#endif /*defined(SOL_TCP)*/
#if defined(SO_TYPE)
  {   /* ScmString d1882 */
      SCM_STRING_CONST_INITIALIZER("SO_TYPE", 7, 7),
  },
#endif /*defined(SO_TYPE)*/
#if defined(SO_TIMESTAMP)
  {   /* ScmString d1880 */
      SCM_STRING_CONST_INITIALIZER("SO_TIMESTAMP", 12, 12),
  },
#endif /*defined(SO_TIMESTAMP)*/
#if defined(SO_SNDTIMEO)
  {   /* ScmString d1878 */
      SCM_STRING_CONST_INITIALIZER("SO_SNDTIMEO", 11, 11),
  },
#endif /*defined(SO_SNDTIMEO)*/
#if defined(SO_SNDLOWAT)
  {   /* ScmString d1876 */
      SCM_STRING_CONST_INITIALIZER("SO_SNDLOWAT", 11, 11),
  },
#endif /*defined(SO_SNDLOWAT)*/
#if defined(SO_SNDBUF)
  {   /* ScmString d1874 */
      SCM_STRING_CONST_INITIALIZER("SO_SNDBUF", 9, 9),
  },
#endif /*defined(SO_SNDBUF)*/
#if defined(SO_REUSEPORT)
  {   /* ScmString d1872 */
      SCM_STRING_CONST_INITIALIZER("SO_REUSEPORT", 12, 12),
  },
#endif /*defined(SO_REUSEPORT)*/
#if defined(SO_REUSEADDR)
  {   /* ScmString d1870 */
      SCM_STRING_CONST_INITIALIZER("SO_REUSEADDR", 12, 12),
  },
#endif /*defined(SO_REUSEADDR)*/
#if defined(SO_RCVTIMEO)
  {   /* ScmString d1868 */
      SCM_STRING_CONST_INITIALIZER("SO_RCVTIMEO", 11, 11),
  },
#endif /*defined(SO_RCVTIMEO)*/
#if defined(SO_RCVLOWAT)
  {   /* ScmString d1866 */
      SCM_STRING_CONST_INITIALIZER("SO_RCVLOWAT", 11, 11),
  },
#endif /*defined(SO_RCVLOWAT)*/
#if defined(SO_RCVBUF)
  {   /* ScmString d1864 */
      SCM_STRING_CONST_INITIALIZER("SO_RCVBUF", 9, 9),
  },
#endif /*defined(SO_RCVBUF)*/
#if defined(SO_PRIORITY)
  {   /* ScmString d1862 */
      SCM_STRING_CONST_INITIALIZER("SO_PRIORITY", 11, 11),
  },
#endif /*defined(SO_PRIORITY)*/
#if defined(SO_PEERCRED)
  {   /* ScmString d1860 */
      SCM_STRING_CONST_INITIALIZER("SO_PEERCRED", 11, 11),
  },
#endif /*defined(SO_PEERCRED)*/
#if defined(SO_PASSCRED)
  {   /* ScmString d1858 */
      SCM_STRING_CONST_INITIALIZER("SO_PASSCRED", 11, 11),
  },
#endif /*defined(SO_PASSCRED)*/
#if defined(SO_OOBINLINE)
  {   /* ScmString d1856 */
      SCM_STRING_CONST_INITIALIZER("SO_OOBINLINE", 12, 12),
  },
#endif /*defined(SO_OOBINLINE)*/
#if defined(SO_LINGER)
  {   /* ScmString d1854 */
      SCM_STRING_CONST_INITIALIZER("SO_LINGER", 9, 9),
  },
#endif /*defined(SO_LINGER)*/
#if defined(SO_KEEPALIVE)
  {   /* ScmString d1852 */
      SCM_STRING_CONST_INITIALIZER("SO_KEEPALIVE", 12, 12),
  },
#endif /*defined(SO_KEEPALIVE)*/
#if defined(SO_ERROR)
  {   /* ScmString d1850 */
      SCM_STRING_CONST_INITIALIZER("SO_ERROR", 8, 8),
  },
#endif /*defined(SO_ERROR)*/
#if defined(SO_DONTROUTE)
  {   /* ScmString d1848 */
      SCM_STRING_CONST_INITIALIZER("SO_DONTROUTE", 12, 12),
  },
#endif /*defined(SO_DONTROUTE)*/
#if defined(SO_DEBUG)
  {   /* ScmString d1846 */
      SCM_STRING_CONST_INITIALIZER("SO_DEBUG", 8, 8),
  },
#endif /*defined(SO_DEBUG)*/
#if defined(SO_BROADCAST)
  {   /* ScmString d1844 */
      SCM_STRING_CONST_INITIALIZER("SO_BROADCAST", 12, 12),
  },
#endif /*defined(SO_BROADCAST)*/
#if defined(SO_BINDTODEVICE)
  {   /* ScmString d1842 */
      SCM_STRING_CONST_INITIALIZER("SO_BINDTODEVICE", 15, 15),
  },
#endif /*defined(SO_BINDTODEVICE)*/
#if defined(SO_ACCEPTCONN)
  {   /* ScmString d1840 */
      SCM_STRING_CONST_INITIALIZER("SO_ACCEPTCONN", 13, 13),
  },
#endif /*defined(SO_ACCEPTCONN)*/
#if defined(SOL_SOCKET)
  {   /* ScmString d1838 */
      SCM_STRING_CONST_INITIALIZER("SOL_SOCKET", 10, 10),
  },
#endif /*defined(SOL_SOCKET)*/
#if !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))
  {   /* ScmString d1836 */
      SCM_STRING_CONST_INITIALIZER("SHUT_RD", 7, 7),
      SCM_STRING_CONST_INITIALIZER("SHUT_WR", 7, 7),
      SCM_STRING_CONST_INITIALIZER("SHUT_RDWR", 9, 9),
  },
#endif /*!((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))*/
#if (defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))
  {   /* ScmString d1834 */
      SCM_STRING_CONST_INITIALIZER("SHUT_RD", 7, 7),
      SCM_STRING_CONST_INITIALIZER("SHUT_WR", 7, 7),
      SCM_STRING_CONST_INITIALIZER("SHUT_RDWR", 9, 9),
  },
#endif /*(defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))*/
#if defined(SOMAXCONN)
  {   /* ScmString d1832 */
      SCM_STRING_CONST_INITIALIZER("SOMAXCONN", 9, 9),
  },
#endif /*defined(SOMAXCONN)*/
#if defined(IPPROTO_UDP)
  {   /* ScmString d1830 */
      SCM_STRING_CONST_INITIALIZER("IPPROTO_UDP", 11, 11),
  },
#endif /*defined(IPPROTO_UDP)*/
#if defined(IPPROTO_TCP)
  {   /* ScmString d1828 */
      SCM_STRING_CONST_INITIALIZER("IPPROTO_TCP", 11, 11),
  },
#endif /*defined(IPPROTO_TCP)*/
#if defined(IPPROTO_ICMPV6)
  {   /* ScmString d1826 */
      SCM_STRING_CONST_INITIALIZER("IPPROTO_ICMPV6", 14, 14),
  },
#endif /*defined(IPPROTO_ICMPV6)*/
#if defined(IPPROTO_ICMP)
  {   /* ScmString d1824 */
      SCM_STRING_CONST_INITIALIZER("IPPROTO_ICMP", 12, 12),
  },
#endif /*defined(IPPROTO_ICMP)*/
#if defined(IPPROTO_IP)
  {   /* ScmString d1822 */
      SCM_STRING_CONST_INITIALIZER("IPPROTO_IP", 10, 10),
  },
#endif /*defined(IPPROTO_IP)*/
#if defined(MSG_WAITALL)
  {   /* ScmString d1820 */
      SCM_STRING_CONST_INITIALIZER("MSG_WAITALL", 11, 11),
  },
#endif /*defined(MSG_WAITALL)*/
#if defined(MSG_TRUNC)
  {   /* ScmString d1818 */
      SCM_STRING_CONST_INITIALIZER("MSG_TRUNC", 9, 9),
  },
#endif /*defined(MSG_TRUNC)*/
#if defined(MSG_PEEK)
  {   /* ScmString d1816 */
      SCM_STRING_CONST_INITIALIZER("MSG_PEEK", 8, 8),
  },
#endif /*defined(MSG_PEEK)*/
#if defined(MSG_OOB)
  {   /* ScmString d1814 */
      SCM_STRING_CONST_INITIALIZER("MSG_OOB", 7, 7),
  },
#endif /*defined(MSG_OOB)*/
#if defined(MSG_EOR)
  {   /* ScmString d1812 */
      SCM_STRING_CONST_INITIALIZER("MSG_EOR", 7, 7),
  },
#endif /*defined(MSG_EOR)*/
#if defined(MSG_DONTROUTE)
  {   /* ScmString d1810 */
      SCM_STRING_CONST_INITIALIZER("MSG_DONTROUTE", 13, 13),
  },
#endif /*defined(MSG_DONTROUTE)*/
#if defined(MSG_CTRUNC)
  {   /* ScmString d1808 */
      SCM_STRING_CONST_INITIALIZER("MSG_CTRUNC", 10, 10),
  },
#endif /*defined(MSG_CTRUNC)*/
#if defined(HAVE_IPV6)
  {   /* ScmString d1802 */
      SCM_STRING_CONST_INITIALIZER("inet6", 5, 5),
      SCM_STRING_CONST_INITIALIZER("sys-getaddrinfo", 15, 15),
      SCM_STRING_CONST_INITIALIZER("nodename", 8, 8),
      SCM_STRING_CONST_INITIALIZER("servname", 8, 8),
      SCM_STRING_CONST_INITIALIZER("hints", 5, 5),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libnet.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche.net", 10, 10),
      SCM_STRING_CONST_INITIALIZER("<const-cstring>\077", 16, 16),
      SCM_STRING_CONST_INITIALIZER("<top>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("sys-getnameinfo", 15, 15),
      SCM_STRING_CONST_INITIALIZER("addr", 4, 4),
      SCM_STRING_CONST_INITIALIZER("optional", 8, 8),
      SCM_STRING_CONST_INITIALIZER("flags", 5, 5),
      SCM_STRING_CONST_INITIALIZER("<socket-address>", 16, 16),
      SCM_STRING_CONST_INITIALIZER("*", 1, 1),
  },
#endif /*defined(HAVE_IPV6)*/
  {   /* ScmString d1787 */
      SCM_STRING_CONST_INITIALIZER("gauche.net", 10, 10),
      SCM_STRING_CONST_INITIALIZER("<socket>", 8, 8),
      SCM_STRING_CONST_INITIALIZER("make-socket", 11, 11),
      SCM_STRING_CONST_INITIALIZER("PF_UNSPEC", 9, 9),
      SCM_STRING_CONST_INITIALIZER("PF_UNIX", 7, 7),
      SCM_STRING_CONST_INITIALIZER("PF_INET", 7, 7),
      SCM_STRING_CONST_INITIALIZER("AF_UNSPEC", 9, 9),
      SCM_STRING_CONST_INITIALIZER("AF_UNIX", 7, 7),
      SCM_STRING_CONST_INITIALIZER("AF_INET", 7, 7),
      SCM_STRING_CONST_INITIALIZER("SOCK_STREAM", 11, 11),
      SCM_STRING_CONST_INITIALIZER("SOCK_DGRAM", 10, 10),
      SCM_STRING_CONST_INITIALIZER("SOCK_RAW", 8, 8),
      SCM_STRING_CONST_INITIALIZER("SHUT_RD", 7, 7),
      SCM_STRING_CONST_INITIALIZER("SHUT_WR", 7, 7),
      SCM_STRING_CONST_INITIALIZER("SHUT_RDWR", 9, 9),
      SCM_STRING_CONST_INITIALIZER("socket-address", 14, 14),
      SCM_STRING_CONST_INITIALIZER("socket-status", 13, 13),
      SCM_STRING_CONST_INITIALIZER("socket-input-port", 17, 17),
      SCM_STRING_CONST_INITIALIZER("socket-output-port", 18, 18),
      SCM_STRING_CONST_INITIALIZER("socket-shutdown", 15, 15),
      SCM_STRING_CONST_INITIALIZER("socket-close", 12, 12),
      SCM_STRING_CONST_INITIALIZER("socket-bind", 11, 11),
      SCM_STRING_CONST_INITIALIZER("socket-connect", 14, 14),
      SCM_STRING_CONST_INITIALIZER("socket-fd", 9, 9),
      SCM_STRING_CONST_INITIALIZER("socket-listen", 13, 13),
      SCM_STRING_CONST_INITIALIZER("socket-accept", 13, 13),
      SCM_STRING_CONST_INITIALIZER("socket-setsockopt", 17, 17),
      SCM_STRING_CONST_INITIALIZER("socket-getsockopt", 17, 17),
      SCM_STRING_CONST_INITIALIZER("socket-getsockname", 18, 18),
      SCM_STRING_CONST_INITIALIZER("socket-getpeername", 18, 18),
      SCM_STRING_CONST_INITIALIZER("socket-ioctl", 12, 12),
      SCM_STRING_CONST_INITIALIZER("socket-send", 11, 11),
      SCM_STRING_CONST_INITIALIZER("socket-sendto", 13, 13),
      SCM_STRING_CONST_INITIALIZER("socket-sendmsg", 14, 14),
      SCM_STRING_CONST_INITIALIZER("socket-buildmsg", 15, 15),
      SCM_STRING_CONST_INITIALIZER("socket-recv", 11, 11),
      SCM_STRING_CONST_INITIALIZER("socket-recv!", 12, 12),
      SCM_STRING_CONST_INITIALIZER("socket-recvfrom", 15, 15),
      SCM_STRING_CONST_INITIALIZER("socket-recvfrom!", 16, 16),
      SCM_STRING_CONST_INITIALIZER("<sockaddr>", 10, 10),
      SCM_STRING_CONST_INITIALIZER("<sockaddr-in>", 13, 13),
      SCM_STRING_CONST_INITIALIZER("<sockaddr-un>", 13, 13),
      SCM_STRING_CONST_INITIALIZER("make-sockaddrs", 14, 14),
      SCM_STRING_CONST_INITIALIZER("sockaddr-name", 13, 13),
      SCM_STRING_CONST_INITIALIZER("sockaddr-family", 15, 15),
      SCM_STRING_CONST_INITIALIZER("sockaddr-addr", 13, 13),
      SCM_STRING_CONST_INITIALIZER("sockaddr-port", 13, 13),
      SCM_STRING_CONST_INITIALIZER("make-client-socket", 18, 18),
      SCM_STRING_CONST_INITIALIZER("make-server-socket", 18, 18),
      SCM_STRING_CONST_INITIALIZER("make-server-sockets", 19, 19),
      SCM_STRING_CONST_INITIALIZER("call-with-client-socket", 23, 23),
      SCM_STRING_CONST_INITIALIZER("<sys-hostent>", 13, 13),
      SCM_STRING_CONST_INITIALIZER("sys-gethostbyname", 17, 17),
      SCM_STRING_CONST_INITIALIZER("sys-gethostbyaddr", 17, 17),
      SCM_STRING_CONST_INITIALIZER("<sys-protoent>", 14, 14),
      SCM_STRING_CONST_INITIALIZER("sys-getprotobyname", 18, 18),
      SCM_STRING_CONST_INITIALIZER("sys-getprotobynumber", 20, 20),
      SCM_STRING_CONST_INITIALIZER("<sys-servent>", 13, 13),
      SCM_STRING_CONST_INITIALIZER("sys-getservbyname", 17, 17),
      SCM_STRING_CONST_INITIALIZER("sys-getservbyport", 17, 17),
      SCM_STRING_CONST_INITIALIZER("sys-htonl", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-htons", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-ntohl", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-ntohs", 9, 9),
      SCM_STRING_CONST_INITIALIZER("inet-checksum", 13, 13),
      SCM_STRING_CONST_INITIALIZER("inet-string->address", 20, 20),
      SCM_STRING_CONST_INITIALIZER("inet-string->address!", 21, 21),
      SCM_STRING_CONST_INITIALIZER("inet-address->string", 20, 20),
      SCM_STRING_CONST_INITIALIZER("connection-self-address", 23, 23),
      SCM_STRING_CONST_INITIALIZER("connection-peer-address", 23, 23),
      SCM_STRING_CONST_INITIALIZER("connection-input-port", 21, 21),
      SCM_STRING_CONST_INITIALIZER("connection-output-port", 22, 22),
      SCM_STRING_CONST_INITIALIZER("connection-shutdown", 19, 19),
      SCM_STRING_CONST_INITIALIZER("connection-close", 16, 16),
      SCM_STRING_CONST_INITIALIZER("connection-address-name", 23, 23),
      SCM_STRING_CONST_INITIALIZER("find-module", 11, 11),
      SCM_STRING_CONST_INITIALIZER("gauche/netutil", 14, 14),
      SCM_STRING_CONST_INITIALIZER("%autoload", 9, 9),
      SCM_STRING_CONST_INITIALIZER("%toplevel", 9, 9),
      SCM_STRING_CONST_INITIALIZER("unknown", 7, 7),
      SCM_STRING_CONST_INITIALIZER("unix", 4, 4),
      SCM_STRING_CONST_INITIALIZER("inet", 4, 4),
      SCM_STRING_CONST_INITIALIZER("domain", 6, 6),
      SCM_STRING_CONST_INITIALIZER("type", 4, 4),
      SCM_STRING_CONST_INITIALIZER("optional", 8, 8),
      SCM_STRING_CONST_INITIALIZER("protocol", 8, 8),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libnet.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("<fixnum>", 8, 8),
      SCM_STRING_CONST_INITIALIZER("*", 1, 1),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<top>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("sock", 4, 4),
      SCM_STRING_CONST_INITIALIZER("none", 4, 4),
      SCM_STRING_CONST_INITIALIZER("bound", 5, 5),
      SCM_STRING_CONST_INITIALIZER("listening", 9, 9),
      SCM_STRING_CONST_INITIALIZER("connected", 9, 9),
      SCM_STRING_CONST_INITIALIZER("shutdown", 8, 8),
      SCM_STRING_CONST_INITIALIZER("closed", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<long>", 6, 6),
      SCM_STRING_CONST_INITIALIZER("buffering", 9, 9),
      SCM_STRING_CONST_INITIALIZER("buffered\077", 9, 9),
      SCM_STRING_CONST_INITIALIZER("key", 3, 3),
      SCM_STRING_CONST_INITIALIZER("how", 3, 3),
      SCM_STRING_CONST_INITIALIZER("addr", 4, 4),
      SCM_STRING_CONST_INITIALIZER("<socket-address>", 16, 16),
      SCM_STRING_CONST_INITIALIZER("backlog", 7, 7),
      SCM_STRING_CONST_INITIALIZER("msg", 3, 3),
      SCM_STRING_CONST_INITIALIZER("flags", 5, 5),
      SCM_STRING_CONST_INITIALIZER("to", 2, 2),
      SCM_STRING_CONST_INITIALIZER("bytes", 5, 5),
      SCM_STRING_CONST_INITIALIZER("buf", 3, 3),
      SCM_STRING_CONST_INITIALIZER("<uvector>", 9, 9),
      SCM_STRING_CONST_INITIALIZER("addrs", 5, 5),
      SCM_STRING_CONST_INITIALIZER("name", 4, 4),
      SCM_STRING_CONST_INITIALIZER("iov", 3, 3),
      SCM_STRING_CONST_INITIALIZER("control", 7, 7),
      SCM_STRING_CONST_INITIALIZER("<socket-address>\077", 17, 17),
      SCM_STRING_CONST_INITIALIZER("<vector>\077", 9, 9),
      SCM_STRING_CONST_INITIALIZER("<int>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("level", 5, 5),
      SCM_STRING_CONST_INITIALIZER("option", 6, 6),
      SCM_STRING_CONST_INITIALIZER("value", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rsize", 5, 5),
      SCM_STRING_CONST_INITIALIZER("request", 7, 7),
      SCM_STRING_CONST_INITIALIZER("data", 4, 4),
      SCM_STRING_CONST_INITIALIZER("<integer>", 9, 9),
      SCM_STRING_CONST_INITIALIZER("<const-cstring>", 15, 15),
      SCM_STRING_CONST_INITIALIZER("number", 6, 6),
      SCM_STRING_CONST_INITIALIZER("proto", 5, 5),
      SCM_STRING_CONST_INITIALIZER("port", 4, 4),
      SCM_STRING_CONST_INITIALIZER("x", 1, 1),
      SCM_STRING_CONST_INITIALIZER("<uint32>", 8, 8),
      SCM_STRING_CONST_INITIALIZER("<uint16>", 8, 8),
      SCM_STRING_CONST_INITIALIZER("IPPROTO_IP", 10, 10),
      SCM_STRING_CONST_INITIALIZER("IPPROTO_ICMP", 12, 12),
      SCM_STRING_CONST_INITIALIZER("IPPROTO_TCP", 11, 11),
      SCM_STRING_CONST_INITIALIZER("IPPROTO_UDP", 11, 11),
      SCM_STRING_CONST_INITIALIZER("IPPROTO_IPV6", 12, 12),
      SCM_STRING_CONST_INITIALIZER("IPPROTO_ICMPV6", 14, 14),
      SCM_STRING_CONST_INITIALIZER("SOL_SOCKET", 10, 10),
      SCM_STRING_CONST_INITIALIZER("SOMAXCONN", 9, 9),
      SCM_STRING_CONST_INITIALIZER("SO_ACCEPTCONN", 13, 13),
      SCM_STRING_CONST_INITIALIZER("SO_BINDTODEVICE", 15, 15),
      SCM_STRING_CONST_INITIALIZER("SO_BROADCAST", 12, 12),
      SCM_STRING_CONST_INITIALIZER("SO_DEBUG", 8, 8),
      SCM_STRING_CONST_INITIALIZER("SO_DONTROUTE", 12, 12),
      SCM_STRING_CONST_INITIALIZER("SO_ERROR", 8, 8),
      SCM_STRING_CONST_INITIALIZER("SO_KEEPALIVE", 12, 12),
      SCM_STRING_CONST_INITIALIZER("SO_LINGER", 9, 9),
      SCM_STRING_CONST_INITIALIZER("SO_OOBINLINE", 12, 12),
      SCM_STRING_CONST_INITIALIZER("SO_PASSCRED", 11, 11),
      SCM_STRING_CONST_INITIALIZER("SO_PEERCRED", 11, 11),
      SCM_STRING_CONST_INITIALIZER("SO_PRIORITY", 11, 11),
      SCM_STRING_CONST_INITIALIZER("SO_RCVBUF", 9, 9),
      SCM_STRING_CONST_INITIALIZER("SO_RCVLOWAT", 11, 11),
      SCM_STRING_CONST_INITIALIZER("SO_RCVTIMEO", 11, 11),
      SCM_STRING_CONST_INITIALIZER("SO_REUSEADDR", 12, 12),
      SCM_STRING_CONST_INITIALIZER("SO_REUSEPORT", 12, 12),
      SCM_STRING_CONST_INITIALIZER("SO_SNDBUF", 9, 9),
      SCM_STRING_CONST_INITIALIZER("SO_SNDLOWAT", 11, 11),
      SCM_STRING_CONST_INITIALIZER("SO_SNDTIMEO", 11, 11),
      SCM_STRING_CONST_INITIALIZER("SO_TIMESTAMP", 12, 12),
      SCM_STRING_CONST_INITIALIZER("SO_TYPE", 7, 7),
      SCM_STRING_CONST_INITIALIZER("SOL_TCP", 7, 7),
      SCM_STRING_CONST_INITIALIZER("TCP_NODELAY", 11, 11),
      SCM_STRING_CONST_INITIALIZER("TCP_MAXSEG", 10, 10),
      SCM_STRING_CONST_INITIALIZER("TCP_CORK", 8, 8),
      SCM_STRING_CONST_INITIALIZER("SOL_IP", 6, 6),
      SCM_STRING_CONST_INITIALIZER("IP_OPTIONS", 10, 10),
      SCM_STRING_CONST_INITIALIZER("IP_PKTINFO", 10, 10),
      SCM_STRING_CONST_INITIALIZER("IP_RECVTOS", 10, 10),
      SCM_STRING_CONST_INITIALIZER("IP_RECVTTL", 10, 10),
      SCM_STRING_CONST_INITIALIZER("IP_RECVOPTS", 11, 11),
      SCM_STRING_CONST_INITIALIZER("IP_TOS", 6, 6),
      SCM_STRING_CONST_INITIALIZER("IP_TTL", 6, 6),
      SCM_STRING_CONST_INITIALIZER("IP_HDRINCL", 10, 10),
      SCM_STRING_CONST_INITIALIZER("IP_RECVERR", 10, 10),
      SCM_STRING_CONST_INITIALIZER("IP_MTU_DISCOVER", 15, 15),
      SCM_STRING_CONST_INITIALIZER("IP_MTU", 6, 6),
      SCM_STRING_CONST_INITIALIZER("IP_ROUTER_ALERT", 15, 15),
      SCM_STRING_CONST_INITIALIZER("IP_MULTICAST_TTL", 16, 16),
      SCM_STRING_CONST_INITIALIZER("IP_MULTICAST_LOOP", 17, 17),
      SCM_STRING_CONST_INITIALIZER("IP_ADD_MEMBERSHIP", 17, 17),
      SCM_STRING_CONST_INITIALIZER("IP_DROP_MEMBERSHIP", 18, 18),
      SCM_STRING_CONST_INITIALIZER("IP_MULTICAST_IF", 15, 15),
      SCM_STRING_CONST_INITIALIZER("MSG_CTRUNC", 10, 10),
      SCM_STRING_CONST_INITIALIZER("MSG_DONTROUTE", 13, 13),
      SCM_STRING_CONST_INITIALIZER("MSG_EOR", 7, 7),
      SCM_STRING_CONST_INITIALIZER("MSG_OOB", 7, 7),
      SCM_STRING_CONST_INITIALIZER("MSG_PEEK", 8, 8),
      SCM_STRING_CONST_INITIALIZER("MSG_TRUNC", 9, 9),
      SCM_STRING_CONST_INITIALIZER("MSG_WAITALL", 11, 11),
      SCM_STRING_CONST_INITIALIZER("SIOCGIFNAME", 11, 11),
      SCM_STRING_CONST_INITIALIZER("SIOCSIFNAME", 11, 11),
      SCM_STRING_CONST_INITIALIZER("SIOCGIFINDEX", 12, 12),
      SCM_STRING_CONST_INITIALIZER("SIOCGIFFLAGS", 12, 12),
      SCM_STRING_CONST_INITIALIZER("SIOCSIFFLAGS", 12, 12),
      SCM_STRING_CONST_INITIALIZER("SIOCGIFMETRIC", 13, 13),
      SCM_STRING_CONST_INITIALIZER("SIOCSIFMETRIC", 13, 13),
      SCM_STRING_CONST_INITIALIZER("SIOCGIFMTU", 10, 10),
      SCM_STRING_CONST_INITIALIZER("SIOCSIFMTU", 10, 10),
      SCM_STRING_CONST_INITIALIZER("SIOCGIFHWADDR", 13, 13),
      SCM_STRING_CONST_INITIALIZER("SIOCSIFHWADDR", 13, 13),
      SCM_STRING_CONST_INITIALIZER("SIOCSIFHWBROADCAST", 18, 18),
      SCM_STRING_CONST_INITIALIZER("SIOCGIFMAP", 10, 10),
      SCM_STRING_CONST_INITIALIZER("SIOCSIFMAP", 10, 10),
      SCM_STRING_CONST_INITIALIZER("SIOCADDMULTI", 12, 12),
      SCM_STRING_CONST_INITIALIZER("SIOCDELMULTI", 12, 12),
      SCM_STRING_CONST_INITIALIZER("SIOGIFTXQLEN", 12, 12),
      SCM_STRING_CONST_INITIALIZER("SIOSIFTXQLEN", 12, 12),
      SCM_STRING_CONST_INITIALIZER("SIOCGIFCONF", 11, 11),
      SCM_STRING_CONST_INITIALIZER("SIOCGIFADDR", 11, 11),
      SCM_STRING_CONST_INITIALIZER("SIOCSIFADDR", 11, 11),
      SCM_STRING_CONST_INITIALIZER("SIOCGIFDSTADDR", 14, 14),
      SCM_STRING_CONST_INITIALIZER("SIOCSIFDSTADDR", 14, 14),
      SCM_STRING_CONST_INITIALIZER("SIOCGIFBRDADDR", 14, 14),
      SCM_STRING_CONST_INITIALIZER("SIOCSIFBRDADDR", 14, 14),
      SCM_STRING_CONST_INITIALIZER("SIOCGIFNETMASK", 14, 14),
      SCM_STRING_CONST_INITIALIZER("SIOCSIFNETMASK", 14, 14),
      SCM_STRING_CONST_INITIALIZER("IFF_UP", 6, 6),
      SCM_STRING_CONST_INITIALIZER("IFF_BROADCAST", 13, 13),
      SCM_STRING_CONST_INITIALIZER("IFF_DEBUG", 9, 9),
      SCM_STRING_CONST_INITIALIZER("IFF_LOOPBACK", 12, 12),
      SCM_STRING_CONST_INITIALIZER("IFF_POINTTOPOINT", 16, 16),
      SCM_STRING_CONST_INITIALIZER("IFF_RUNNING", 11, 11),
      SCM_STRING_CONST_INITIALIZER("IFF_NOARP", 9, 9),
      SCM_STRING_CONST_INITIALIZER("IFF_PROMISC", 11, 11),
      SCM_STRING_CONST_INITIALIZER("IFF_NOTRAILERS", 14, 14),
      SCM_STRING_CONST_INITIALIZER("IFF_ALLMULTI", 12, 12),
      SCM_STRING_CONST_INITIALIZER("IFF_MASTER", 10, 10),
      SCM_STRING_CONST_INITIALIZER("IFF_SLAVE", 9, 9),
      SCM_STRING_CONST_INITIALIZER("IFF_MULTICAST", 13, 13),
      SCM_STRING_CONST_INITIALIZER("IFF_PORTSEL", 11, 11),
      SCM_STRING_CONST_INITIALIZER("IFF_AUTOMEDIA", 13, 13),
      SCM_STRING_CONST_INITIALIZER("IFF_DYNAMIC", 11, 11),
      SCM_STRING_CONST_INITIALIZER("PF_INET6", 8, 8),
      SCM_STRING_CONST_INITIALIZER("AF_INET6", 8, 8),
      SCM_STRING_CONST_INITIALIZER("<sockaddr-in6>", 14, 14),
      SCM_STRING_CONST_INITIALIZER("<sys-addrinfo>", 14, 14),
      SCM_STRING_CONST_INITIALIZER("sys-getaddrinfo", 15, 15),
      SCM_STRING_CONST_INITIALIZER("make-sys-addrinfo", 17, 17),
      SCM_STRING_CONST_INITIALIZER("AI_PASSIVE", 10, 10),
      SCM_STRING_CONST_INITIALIZER("AI_CANONNAME", 12, 12),
      SCM_STRING_CONST_INITIALIZER("AI_NUMERICHOST", 14, 14),
      SCM_STRING_CONST_INITIALIZER("AI_NUMERICSERV", 14, 14),
      SCM_STRING_CONST_INITIALIZER("AI_V4MAPPED", 11, 11),
      SCM_STRING_CONST_INITIALIZER("AI_ALL", 6, 6),
      SCM_STRING_CONST_INITIALIZER("AI_ADDRCONFIG", 13, 13),
      SCM_STRING_CONST_INITIALIZER("IPV6_UNICAST_HOPS", 17, 17),
      SCM_STRING_CONST_INITIALIZER("IPV6_MULTICAST_IF", 17, 17),
      SCM_STRING_CONST_INITIALIZER("IPV6_MULTICAST_HOPS", 19, 19),
      SCM_STRING_CONST_INITIALIZER("IPV6_MULTICAST_LOOP", 19, 19),
      SCM_STRING_CONST_INITIALIZER("IPV6_JOIN_GROUP", 15, 15),
      SCM_STRING_CONST_INITIALIZER("IPV6_LEAVE_GROUP", 16, 16),
      SCM_STRING_CONST_INITIALIZER("IPV6_V6ONLY", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sys-getnameinfo", 15, 15),
      SCM_STRING_CONST_INITIALIZER("NI_NOFQDN", 9, 9),
      SCM_STRING_CONST_INITIALIZER("NI_NUMERICHOST", 14, 14),
      SCM_STRING_CONST_INITIALIZER("NI_NAMEREQD", 11, 11),
      SCM_STRING_CONST_INITIALIZER("NI_NUMERICSERV", 14, 14),
      SCM_STRING_CONST_INITIALIZER("NI_DGRAM", 8, 8),
      SCM_STRING_CONST_INITIALIZER("size", 4, 4),
      SCM_STRING_CONST_INITIALIZER("<uint>", 6, 6),
      SCM_STRING_CONST_INITIALIZER("s", 1, 1),
      SCM_STRING_CONST_INITIALIZER("%expression-name-mark-key", 25, 25),
      SCM_STRING_CONST_INITIALIZER("DEFAULT_BACKLOG", 15, 15),
      SCM_STRING_CONST_INITIALIZER("ipv6-capable", 12, 12),
      SCM_STRING_CONST_INITIALIZER("module-binds\077", 13, 13),
      SCM_STRING_CONST_INITIALIZER("ipv4-preferred", 14, 14),
      SCM_STRING_CONST_INITIALIZER("undefined\077", 10, 10),
      SCM_STRING_CONST_INITIALIZER("list\077", 5, 5),
      SCM_STRING_CONST_INITIALIZER("logior", 6, 6),
      SCM_STRING_CONST_INITIALIZER("family", 6, 6),
      SCM_STRING_CONST_INITIALIZER("socktype", 8, 8),
      SCM_STRING_CONST_INITIALIZER("make", 4, 4),
      SCM_STRING_CONST_INITIALIZER("make-sys-addrinfo is available on IPv6-enabled platform", 55, 55),
      SCM_STRING_CONST_INITIALIZER("error", 5, 5),
      SCM_STRING_CONST_INITIALIZER("keyword list not even", 21, 21),
      SCM_STRING_CONST_INITIALIZER("unwrap-syntax-1", 15, 15),
      SCM_STRING_CONST_INITIALIZER("unknown keyword ~S", 18, 18),
      SCM_STRING_CONST_INITIALIZER("errorf", 6, 6),
      SCM_STRING_CONST_INITIALIZER("address->protocol-family", 24, 24),
      SCM_STRING_CONST_INITIALIZER("inet6", 5, 5),
      SCM_STRING_CONST_INITIALIZER("unknown family of socket address", 32, 32),
      SCM_STRING_CONST_INITIALIZER("unix socket requires pathname, but got", 38, 38),
      SCM_STRING_CONST_INITIALIZER("make-client-socket-unix", 23, 23),
      SCM_STRING_CONST_INITIALIZER("integer\077", 8, 8),
      SCM_STRING_CONST_INITIALIZER("inet socket requires host name and port, but got ~s and ~s", 58, 58),
      SCM_STRING_CONST_INITIALIZER("make-client-socket-inet", 23, 23),
      SCM_STRING_CONST_INITIALIZER("make-client-socket-from-addr", 28, 28),
      SCM_STRING_CONST_INITIALIZER("unsupported protocol:", 21, 21),
      SCM_STRING_CONST_INITIALIZER("args", 4, 4),
      SCM_STRING_CONST_INITIALIZER("path", 4, 4),
      SCM_STRING_CONST_INITIALIZER("try-connect", 11, 11),
      SCM_STRING_CONST_INITIALIZER("e", 1, 1),
      SCM_STRING_CONST_INITIALIZER("rewind-before", 13, 13),
      SCM_STRING_CONST_INITIALIZER("with-error-handler", 18, 18),
      SCM_STRING_CONST_INITIALIZER("address", 7, 7),
      SCM_STRING_CONST_INITIALIZER("any", 3, 3),
      SCM_STRING_CONST_INITIALIZER("raise", 5, 5),
      SCM_STRING_CONST_INITIALIZER("host", 4, 4),
      SCM_STRING_CONST_INITIALIZER("make-server-socket-unix", 23, 23),
      SCM_STRING_CONST_INITIALIZER("string\077", 7, 7),
      SCM_STRING_CONST_INITIALIZER("any-pred", 8, 8),
      SCM_STRING_CONST_INITIALIZER("every", 5, 5),
      SCM_STRING_CONST_INITIALIZER("inet socket requires integer port number or string service name, or a list of them, but got:", 92, 92),
      SCM_STRING_CONST_INITIALIZER("make-server-socket-inet*", 24, 24),
      SCM_STRING_CONST_INITIALIZER("make-server-socket-inet", 23, 23),
      SCM_STRING_CONST_INITIALIZER("make-server-socket-from-addr", 28, 28),
      SCM_STRING_CONST_INITIALIZER("procedure\077", 10, 10),
      SCM_STRING_CONST_INITIALIZER("reuse-addr\077", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sock-init", 9, 9),
      SCM_STRING_CONST_INITIALIZER("G2084", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2083", 5, 5),
      SCM_STRING_CONST_INITIALIZER("try-bind", 8, 8),
      SCM_STRING_CONST_INITIALIZER("append-map", 10, 10),
      SCM_STRING_CONST_INITIALIZER("ports", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2091", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2090", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2089", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2088", 5, 5),
      SCM_STRING_CONST_INITIALIZER("<system-error>", 14, 14),
      SCM_STRING_CONST_INITIALIZER("errno", 5, 5),
      SCM_STRING_CONST_INITIALIZER("~", 1, 1),
      SCM_STRING_CONST_INITIALIZER("bind-failed\077", 12, 12),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EADDRINUSE", 10, 10),
      SCM_STRING_CONST_INITIALIZER("global-variable-ref", 19, 19),
      SCM_STRING_CONST_INITIALIZER("EADDRNOTAVAIL", 13, 13),
      SCM_STRING_CONST_INITIALIZER("filter", 6, 6),
      SCM_STRING_CONST_INITIALIZER("G2086", 5, 5),
      SCM_STRING_CONST_INITIALIZER("map", 3, 3),
      SCM_STRING_CONST_INITIALIZER("G2085", 5, 5),
      SCM_STRING_CONST_INITIALIZER("filter-map", 10, 10),
      SCM_STRING_CONST_INITIALIZER("G2087", 5, 5),
      SCM_STRING_CONST_INITIALIZER("%reraise", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G2100", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2095", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2099", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2098", 5, 5),
      SCM_STRING_CONST_INITIALIZER("tcp", 3, 3),
      SCM_STRING_CONST_INITIALIZER("too many arguments for", 22, 22),
      SCM_STRING_CONST_INITIALIZER("lambda", 6, 6),
      SCM_STRING_CONST_INITIALIZER("quote", 5, 5),
      SCM_STRING_CONST_INITIALIZER("if", 2, 2),
      SCM_STRING_CONST_INITIALIZER("let*", 4, 4),
      SCM_STRING_CONST_INITIALIZER("case", 4, 4),
      SCM_STRING_CONST_INITIALIZER("udp", 3, 3),
      SCM_STRING_CONST_INITIALIZER("else", 4, 4),
      SCM_STRING_CONST_INITIALIZER("x->string", 9, 9),
      SCM_STRING_CONST_INITIALIZER("hints", 5, 5),
      SCM_STRING_CONST_INITIALIZER("ss", 2, 2),
      SCM_STRING_CONST_INITIALIZER("cut", 3, 3),
      SCM_STRING_CONST_INITIALIZER("slot-ref", 8, 8),
      SCM_STRING_CONST_INITIALIZER("<>", 2, 2),
      SCM_STRING_CONST_INITIALIZER("append", 6, 6),
      SCM_STRING_CONST_INITIALIZER("^s", 2, 2),
      SCM_STRING_CONST_INITIALIZER("eq\077", 3, 3),
      SCM_STRING_CONST_INITIALIZER("remove", 6, 6),
      SCM_STRING_CONST_INITIALIZER("let1", 4, 4),
      SCM_STRING_CONST_INITIALIZER("cond", 4, 4),
      SCM_STRING_CONST_INITIALIZER("number\077", 7, 7),
      SCM_STRING_CONST_INITIALIZER("symbol->string", 14, 14),
      SCM_STRING_CONST_INITIALIZER("=>", 2, 2),
      SCM_STRING_CONST_INITIALIZER("couldn't find a port number of service:", 39, 39),
      SCM_STRING_CONST_INITIALIZER("hh", 2, 2),
      SCM_STRING_CONST_INITIALIZER("unless", 6, 6),
      SCM_STRING_CONST_INITIALIZER("couldn't find host: ", 20, 20),
      SCM_STRING_CONST_INITIALIZER("addresses", 9, 9),
      SCM_STRING_CONST_INITIALIZER("list", 4, 4),
      SCM_STRING_CONST_INITIALIZER("G2097", 5, 5),
      SCM_STRING_CONST_INITIALIZER("%unwind-protect", 15, 15),
      SCM_STRING_CONST_INITIALIZER("input-buffering", 15, 15),
      SCM_STRING_CONST_INITIALIZER("output-buffering", 16, 16),
      SCM_STRING_CONST_INITIALIZER("socket", 6, 6),
      SCM_STRING_CONST_INITIALIZER("proc", 4, 4),
      SCM_STRING_CONST_INITIALIZER("gauche.internal", 15, 15),
      SCM_STRING_CONST_INITIALIZER("current-module", 14, 14),
      SCM_STRING_CONST_INITIALIZER("define-constant", 15, 15),
      SCM_STRING_CONST_INITIALIZER("cond-expand", 11, 11),
      SCM_STRING_CONST_INITIALIZER("gauche.os.windows", 17, 17),
      SCM_STRING_CONST_INITIALIZER("loop2063", 8, 8),
      SCM_STRING_CONST_INITIALIZER("args2062", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G2064", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2065", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2066", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2067", 5, 5),
      SCM_STRING_CONST_INITIALIZER("tmp", 3, 3),
      SCM_STRING_CONST_INITIALIZER("rest2061", 8, 8),
      SCM_STRING_CONST_INITIALIZER("pair\077", 5, 5),
      SCM_STRING_CONST_INITIALIZER("is-a\077", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2070", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2069", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2068", 5, 5),
      SCM_STRING_CONST_INITIALIZER("let-optionals*", 14, 14),
      SCM_STRING_CONST_INITIALIZER("rlet1", 5, 5),
      SCM_STRING_CONST_INITIALIZER("err", 3, 3),
      SCM_STRING_CONST_INITIALIZER("set!", 4, 4),
      SCM_STRING_CONST_INITIALIZER("guard", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2072", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2071", 5, 5),
      SCM_STRING_CONST_INITIALIZER("loop2075", 8, 8),
      SCM_STRING_CONST_INITIALIZER("args2074", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G2076", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2077", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2078", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest2073", 8, 8),
      SCM_STRING_CONST_INITIALIZER("loop2081", 8, 8),
      SCM_STRING_CONST_INITIALIZER("args2080", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G2082", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest2079", 8, 8),
      SCM_STRING_CONST_INITIALIZER("$", 1, 1),
      SCM_STRING_CONST_INITIALIZER("memv", 4, 4),
      SCM_STRING_CONST_INITIALIZER("quasiquote", 10, 10),
      SCM_STRING_CONST_INITIALIZER("unquote", 7, 7),
      SCM_STRING_CONST_INITIALIZER("s6", 2, 2),
      SCM_STRING_CONST_INITIALIZER("a4s", 3, 3),
      SCM_STRING_CONST_INITIALIZER("zero\077", 5, 5),
      SCM_STRING_CONST_INITIALIZER("v4addrs", 7, 7),
      SCM_STRING_CONST_INITIALIZER("try-v4", 6, 6),
      SCM_STRING_CONST_INITIALIZER("values", 6, 6),
      SCM_STRING_CONST_INITIALIZER("actual-port", 11, 11),
      SCM_STRING_CONST_INITIALIZER("make-v6socks", 12, 12),
      SCM_STRING_CONST_INITIALIZER("v6addrs", 7, 7),
      SCM_STRING_CONST_INITIALIZER("sockets", 7, 7),
      SCM_STRING_CONST_INITIALIZER("receive", 7, 7),
      SCM_STRING_CONST_INITIALIZER("try-v6", 6, 6),
      SCM_STRING_CONST_INITIALIZER("reverse", 7, 7),
      SCM_STRING_CONST_INITIALIZER("a6s", 3, 3),
      SCM_STRING_CONST_INITIALIZER("G2096", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2093", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2094", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest2092", 8, 8),
      SCM_STRING_CONST_INITIALIZER("loop2103", 8, 8),
      SCM_STRING_CONST_INITIALIZER("args2102", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G2104", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G2105", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest2101", 8, 8),
      SCM_STRING_CONST_INITIALIZER("unwind-protect", 14, 14),
  },
};
static struct scm__rcRec {
#if defined(NI_DGRAM)
  ScmObj d2056[1];
#endif /*defined(NI_DGRAM)*/
#if defined(NI_NUMERICSERV)
  ScmObj d2054[1];
#endif /*defined(NI_NUMERICSERV)*/
#if defined(NI_NAMEREQD)
  ScmObj d2052[1];
#endif /*defined(NI_NAMEREQD)*/
#if defined(NI_NUMERICHOST)
  ScmObj d2050[1];
#endif /*defined(NI_NUMERICHOST)*/
#if defined(NI_NOFQDN)
  ScmObj d2048[1];
#endif /*defined(NI_NOFQDN)*/
#if defined(AI_ADDRCONFIG)
  ScmObj d2046[1];
#endif /*defined(AI_ADDRCONFIG)*/
#if defined(AI_ALL)
  ScmObj d2044[1];
#endif /*defined(AI_ALL)*/
#if defined(AI_V4MAPPED)
  ScmObj d2042[1];
#endif /*defined(AI_V4MAPPED)*/
#if defined(AI_NUMERICSERV)
  ScmObj d2040[1];
#endif /*defined(AI_NUMERICSERV)*/
#if defined(AI_NUMERICHOST)
  ScmObj d2038[1];
#endif /*defined(AI_NUMERICHOST)*/
#if defined(AI_CANONNAME)
  ScmObj d2036[1];
#endif /*defined(AI_CANONNAME)*/
#if defined(AI_PASSIVE)
  ScmObj d2034[1];
#endif /*defined(AI_PASSIVE)*/
#if defined(IPV6_V6ONLY)
  ScmObj d2032[1];
#endif /*defined(IPV6_V6ONLY)*/
#if defined(IPV6_LEAVE_GROUP)
  ScmObj d2030[1];
#endif /*defined(IPV6_LEAVE_GROUP)*/
#if defined(IPV6_JOIN_GROUP)
  ScmObj d2028[1];
#endif /*defined(IPV6_JOIN_GROUP)*/
#if defined(IPV6_MULTICAST_LOOP)
  ScmObj d2026[1];
#endif /*defined(IPV6_MULTICAST_LOOP)*/
#if defined(IPV6_MULTICAST_HOPS)
  ScmObj d2024[1];
#endif /*defined(IPV6_MULTICAST_HOPS)*/
#if defined(IPV6_MULTICAST_IF)
  ScmObj d2022[1];
#endif /*defined(IPV6_MULTICAST_IF)*/
#if defined(IPV6_UNICAST_HOPS)
  ScmObj d2020[1];
#endif /*defined(IPV6_UNICAST_HOPS)*/
#if defined(IPPROTO_IPV6)
  ScmObj d2018[1];
#endif /*defined(IPPROTO_IPV6)*/
#if defined(PF_INET6)
  ScmObj d2016[1];
#endif /*defined(PF_INET6)*/
#if defined(AF_INET6)
  ScmObj d2014[1];
#endif /*defined(AF_INET6)*/
#if defined(HAVE_IPV6)
  ScmPair d2013[23] SCM_ALIGN_PAIR;
#endif /*defined(HAVE_IPV6)*/
#if defined(IFF_DYNAMIC)
  ScmObj d2011[1];
#endif /*defined(IFF_DYNAMIC)*/
#if defined(IFF_AUTOMEDIA)
  ScmObj d2009[1];
#endif /*defined(IFF_AUTOMEDIA)*/
#if defined(IFF_PORTSEL)
  ScmObj d2007[1];
#endif /*defined(IFF_PORTSEL)*/
#if defined(IFF_MULTICAST)
  ScmObj d2005[1];
#endif /*defined(IFF_MULTICAST)*/
#if defined(IFF_SLAVE)
  ScmObj d2003[1];
#endif /*defined(IFF_SLAVE)*/
#if defined(IFF_MASTER)
  ScmObj d2001[1];
#endif /*defined(IFF_MASTER)*/
#if defined(IFF_ALLMULTI)
  ScmObj d1999[1];
#endif /*defined(IFF_ALLMULTI)*/
#if defined(IFF_NOTRAILERS)
  ScmObj d1997[1];
#endif /*defined(IFF_NOTRAILERS)*/
#if defined(IFF_PROMISC)
  ScmObj d1995[1];
#endif /*defined(IFF_PROMISC)*/
#if defined(IFF_NOARP)
  ScmObj d1993[1];
#endif /*defined(IFF_NOARP)*/
#if defined(IFF_RUNNING)
  ScmObj d1991[1];
#endif /*defined(IFF_RUNNING)*/
#if defined(IFF_POINTTOPOINT)
  ScmObj d1989[1];
#endif /*defined(IFF_POINTTOPOINT)*/
#if defined(IFF_LOOPBACK)
  ScmObj d1987[1];
#endif /*defined(IFF_LOOPBACK)*/
#if defined(IFF_DEBUG)
  ScmObj d1985[1];
#endif /*defined(IFF_DEBUG)*/
#if defined(IFF_BROADCAST)
  ScmObj d1983[1];
#endif /*defined(IFF_BROADCAST)*/
#if defined(IFF_UP)
  ScmObj d1981[1];
#endif /*defined(IFF_UP)*/
#if defined(SIOCGIFCONF)
  ScmObj d1979[1];
#endif /*defined(SIOCGIFCONF)*/
#if defined(SIOSIFTXQLEN)
  ScmObj d1977[1];
#endif /*defined(SIOSIFTXQLEN)*/
#if defined(SIOGIFTXQLEN)
  ScmObj d1975[1];
#endif /*defined(SIOGIFTXQLEN)*/
#if defined(SIOCDELMULTI)
  ScmObj d1973[1];
#endif /*defined(SIOCDELMULTI)*/
#if defined(SIOCADDMULTI)
  ScmObj d1971[1];
#endif /*defined(SIOCADDMULTI)*/
#if defined(SIOCSIFMAP)
  ScmObj d1969[1];
#endif /*defined(SIOCSIFMAP)*/
#if defined(SIOCGIFMAP)
  ScmObj d1967[1];
#endif /*defined(SIOCGIFMAP)*/
#if defined(SIOCSIFHWBROADCAST)
  ScmObj d1965[1];
#endif /*defined(SIOCSIFHWBROADCAST)*/
#if defined(SIOCSIFHWADDR)
  ScmObj d1963[1];
#endif /*defined(SIOCSIFHWADDR)*/
#if defined(SIOCGIFHWADDR)
  ScmObj d1961[1];
#endif /*defined(SIOCGIFHWADDR)*/
#if defined(SIOCSIFMTU)
  ScmObj d1959[1];
#endif /*defined(SIOCSIFMTU)*/
#if defined(SIOCGIFMTU)
  ScmObj d1957[1];
#endif /*defined(SIOCGIFMTU)*/
#if defined(SIOCSIFMETRIC)
  ScmObj d1955[1];
#endif /*defined(SIOCSIFMETRIC)*/
#if defined(SIOCGIFMETRIC)
  ScmObj d1953[1];
#endif /*defined(SIOCGIFMETRIC)*/
#if defined(SIOCSIFFLAGS)
  ScmObj d1951[1];
#endif /*defined(SIOCSIFFLAGS)*/
#if defined(SIOCGIFFLAGS)
  ScmObj d1949[1];
#endif /*defined(SIOCGIFFLAGS)*/
#if defined(SIOCSIFNETMASK)
  ScmObj d1947[1];
#endif /*defined(SIOCSIFNETMASK)*/
#if defined(SIOCGIFNETMASK)
  ScmObj d1945[1];
#endif /*defined(SIOCGIFNETMASK)*/
#if defined(SIOCSIFBRDADDR)
  ScmObj d1943[1];
#endif /*defined(SIOCSIFBRDADDR)*/
#if defined(SIOCGIFBRDADDR)
  ScmObj d1941[1];
#endif /*defined(SIOCGIFBRDADDR)*/
#if defined(SIOCSIFDSTADDR)
  ScmObj d1939[1];
#endif /*defined(SIOCSIFDSTADDR)*/
#if defined(SIOCGIFDSTADDR)
  ScmObj d1937[1];
#endif /*defined(SIOCGIFDSTADDR)*/
#if defined(SIOCSIFADDR)
  ScmObj d1935[1];
#endif /*defined(SIOCSIFADDR)*/
#if defined(SIOCGIFADDR)
  ScmObj d1933[1];
#endif /*defined(SIOCGIFADDR)*/
#if defined(SIOCGIFINDEX)
  ScmObj d1931[1];
#endif /*defined(SIOCGIFINDEX)*/
#if defined(SIOCSIFNAME)
  ScmObj d1929[1];
#endif /*defined(SIOCSIFNAME)*/
#if defined(SIOCGIFNAME)
  ScmObj d1927[1];
#endif /*defined(SIOCGIFNAME)*/
#if defined(IP_MULTICAST_IF)
  ScmObj d1925[1];
#endif /*defined(IP_MULTICAST_IF)*/
#if defined(IP_DROP_MEMBERSHIP)
  ScmObj d1923[1];
#endif /*defined(IP_DROP_MEMBERSHIP)*/
#if defined(IP_ADD_MEMBERSHIP)
  ScmObj d1921[1];
#endif /*defined(IP_ADD_MEMBERSHIP)*/
#if defined(IP_MULTICAST_LOOP)
  ScmObj d1919[1];
#endif /*defined(IP_MULTICAST_LOOP)*/
#if defined(IP_MULTICAST_TTL)
  ScmObj d1917[1];
#endif /*defined(IP_MULTICAST_TTL)*/
#if defined(IP_ROUTER_ALERT)
  ScmObj d1915[1];
#endif /*defined(IP_ROUTER_ALERT)*/
#if defined(IP_MTU)
  ScmObj d1913[1];
#endif /*defined(IP_MTU)*/
#if defined(IP_MTU_DISCOVER)
  ScmObj d1911[1];
#endif /*defined(IP_MTU_DISCOVER)*/
#if defined(IP_RECVERR)
  ScmObj d1909[1];
#endif /*defined(IP_RECVERR)*/
#if defined(IP_HDRINCL)
  ScmObj d1907[1];
#endif /*defined(IP_HDRINCL)*/
#if defined(IP_TTL)
  ScmObj d1905[1];
#endif /*defined(IP_TTL)*/
#if defined(IP_TOS)
  ScmObj d1903[1];
#endif /*defined(IP_TOS)*/
#if defined(IP_RECVOPTS)
  ScmObj d1901[1];
#endif /*defined(IP_RECVOPTS)*/
#if defined(IP_RECVTTL)
  ScmObj d1899[1];
#endif /*defined(IP_RECVTTL)*/
#if defined(IP_RECVTOS)
  ScmObj d1897[1];
#endif /*defined(IP_RECVTOS)*/
#if defined(IP_PKTINFO)
  ScmObj d1895[1];
#endif /*defined(IP_PKTINFO)*/
#if defined(IP_OPTIONS)
  ScmObj d1893[1];
#endif /*defined(IP_OPTIONS)*/
#if defined(SOL_IP)
  ScmObj d1891[1];
#endif /*defined(SOL_IP)*/
#if defined(TCP_CORK)
  ScmObj d1889[1];
#endif /*defined(TCP_CORK)*/
#if defined(TCP_MAXSEG)
  ScmObj d1887[1];
#endif /*defined(TCP_MAXSEG)*/
#if defined(TCP_NODELAY)
  ScmObj d1885[1];
#endif /*defined(TCP_NODELAY)*/
#if defined(SOL_TCP)
  ScmObj d1883[1];
#endif /*defined(SOL_TCP)*/
#if defined(SO_TYPE)
  ScmObj d1881[1];
#endif /*defined(SO_TYPE)*/
#if defined(SO_TIMESTAMP)
  ScmObj d1879[1];
#endif /*defined(SO_TIMESTAMP)*/
#if defined(SO_SNDTIMEO)
  ScmObj d1877[1];
#endif /*defined(SO_SNDTIMEO)*/
#if defined(SO_SNDLOWAT)
  ScmObj d1875[1];
#endif /*defined(SO_SNDLOWAT)*/
#if defined(SO_SNDBUF)
  ScmObj d1873[1];
#endif /*defined(SO_SNDBUF)*/
#if defined(SO_REUSEPORT)
  ScmObj d1871[1];
#endif /*defined(SO_REUSEPORT)*/
#if defined(SO_REUSEADDR)
  ScmObj d1869[1];
#endif /*defined(SO_REUSEADDR)*/
#if defined(SO_RCVTIMEO)
  ScmObj d1867[1];
#endif /*defined(SO_RCVTIMEO)*/
#if defined(SO_RCVLOWAT)
  ScmObj d1865[1];
#endif /*defined(SO_RCVLOWAT)*/
#if defined(SO_RCVBUF)
  ScmObj d1863[1];
#endif /*defined(SO_RCVBUF)*/
#if defined(SO_PRIORITY)
  ScmObj d1861[1];
#endif /*defined(SO_PRIORITY)*/
#if defined(SO_PEERCRED)
  ScmObj d1859[1];
#endif /*defined(SO_PEERCRED)*/
#if defined(SO_PASSCRED)
  ScmObj d1857[1];
#endif /*defined(SO_PASSCRED)*/
#if defined(SO_OOBINLINE)
  ScmObj d1855[1];
#endif /*defined(SO_OOBINLINE)*/
#if defined(SO_LINGER)
  ScmObj d1853[1];
#endif /*defined(SO_LINGER)*/
#if defined(SO_KEEPALIVE)
  ScmObj d1851[1];
#endif /*defined(SO_KEEPALIVE)*/
#if defined(SO_ERROR)
  ScmObj d1849[1];
#endif /*defined(SO_ERROR)*/
#if defined(SO_DONTROUTE)
  ScmObj d1847[1];
#endif /*defined(SO_DONTROUTE)*/
#if defined(SO_DEBUG)
  ScmObj d1845[1];
#endif /*defined(SO_DEBUG)*/
#if defined(SO_BROADCAST)
  ScmObj d1843[1];
#endif /*defined(SO_BROADCAST)*/
#if defined(SO_BINDTODEVICE)
  ScmObj d1841[1];
#endif /*defined(SO_BINDTODEVICE)*/
#if defined(SO_ACCEPTCONN)
  ScmObj d1839[1];
#endif /*defined(SO_ACCEPTCONN)*/
#if defined(SOL_SOCKET)
  ScmObj d1837[1];
#endif /*defined(SOL_SOCKET)*/
#if !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))
  ScmObj d1835[3];
#endif /*!((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))*/
#if (defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))
  ScmObj d1833[3];
#endif /*(defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))*/
#if defined(SOMAXCONN)
  ScmObj d1831[1];
#endif /*defined(SOMAXCONN)*/
#if defined(IPPROTO_UDP)
  ScmObj d1829[1];
#endif /*defined(IPPROTO_UDP)*/
#if defined(IPPROTO_TCP)
  ScmObj d1827[1];
#endif /*defined(IPPROTO_TCP)*/
#if defined(IPPROTO_ICMPV6)
  ScmObj d1825[1];
#endif /*defined(IPPROTO_ICMPV6)*/
#if defined(IPPROTO_ICMP)
  ScmObj d1823[1];
#endif /*defined(IPPROTO_ICMP)*/
#if defined(IPPROTO_IP)
  ScmObj d1821[1];
#endif /*defined(IPPROTO_IP)*/
#if defined(MSG_WAITALL)
  ScmObj d1819[1];
#endif /*defined(MSG_WAITALL)*/
#if defined(MSG_TRUNC)
  ScmObj d1817[1];
#endif /*defined(MSG_TRUNC)*/
#if defined(MSG_PEEK)
  ScmObj d1815[1];
#endif /*defined(MSG_PEEK)*/
#if defined(MSG_OOB)
  ScmObj d1813[1];
#endif /*defined(MSG_OOB)*/
#if defined(MSG_EOR)
  ScmObj d1811[1];
#endif /*defined(MSG_EOR)*/
#if defined(MSG_DONTROUTE)
  ScmObj d1809[1];
#endif /*defined(MSG_DONTROUTE)*/
#if defined(MSG_CTRUNC)
  ScmObj d1807[1];
#endif /*defined(MSG_CTRUNC)*/
#if defined(HAVE_IPV6)
  ScmObj d1801[36];
#endif /*defined(HAVE_IPV6)*/
  ScmUVector d1791[52];
  ScmCompiledCode d1790[57];
  ScmWord d1789[1918];
  ScmPair d1788[829] SCM_ALIGN_PAIR;
  ScmObj d1786[1105];
} scm__rc SCM_UNUSED = {
#if defined(NI_DGRAM)
  {   /* ScmObj d2056 */
    SCM_UNBOUND,
  },
#endif /*defined(NI_DGRAM)*/
#if defined(NI_NUMERICSERV)
  {   /* ScmObj d2054 */
    SCM_UNBOUND,
  },
#endif /*defined(NI_NUMERICSERV)*/
#if defined(NI_NAMEREQD)
  {   /* ScmObj d2052 */
    SCM_UNBOUND,
  },
#endif /*defined(NI_NAMEREQD)*/
#if defined(NI_NUMERICHOST)
  {   /* ScmObj d2050 */
    SCM_UNBOUND,
  },
#endif /*defined(NI_NUMERICHOST)*/
#if defined(NI_NOFQDN)
  {   /* ScmObj d2048 */
    SCM_UNBOUND,
  },
#endif /*defined(NI_NOFQDN)*/
#if defined(AI_ADDRCONFIG)
  {   /* ScmObj d2046 */
    SCM_UNBOUND,
  },
#endif /*defined(AI_ADDRCONFIG)*/
#if defined(AI_ALL)
  {   /* ScmObj d2044 */
    SCM_UNBOUND,
  },
#endif /*defined(AI_ALL)*/
#if defined(AI_V4MAPPED)
  {   /* ScmObj d2042 */
    SCM_UNBOUND,
  },
#endif /*defined(AI_V4MAPPED)*/
#if defined(AI_NUMERICSERV)
  {   /* ScmObj d2040 */
    SCM_UNBOUND,
  },
#endif /*defined(AI_NUMERICSERV)*/
#if defined(AI_NUMERICHOST)
  {   /* ScmObj d2038 */
    SCM_UNBOUND,
  },
#endif /*defined(AI_NUMERICHOST)*/
#if defined(AI_CANONNAME)
  {   /* ScmObj d2036 */
    SCM_UNBOUND,
  },
#endif /*defined(AI_CANONNAME)*/
#if defined(AI_PASSIVE)
  {   /* ScmObj d2034 */
    SCM_UNBOUND,
  },
#endif /*defined(AI_PASSIVE)*/
#if defined(IPV6_V6ONLY)
  {   /* ScmObj d2032 */
    SCM_UNBOUND,
  },
#endif /*defined(IPV6_V6ONLY)*/
#if defined(IPV6_LEAVE_GROUP)
  {   /* ScmObj d2030 */
    SCM_UNBOUND,
  },
#endif /*defined(IPV6_LEAVE_GROUP)*/
#if defined(IPV6_JOIN_GROUP)
  {   /* ScmObj d2028 */
    SCM_UNBOUND,
  },
#endif /*defined(IPV6_JOIN_GROUP)*/
#if defined(IPV6_MULTICAST_LOOP)
  {   /* ScmObj d2026 */
    SCM_UNBOUND,
  },
#endif /*defined(IPV6_MULTICAST_LOOP)*/
#if defined(IPV6_MULTICAST_HOPS)
  {   /* ScmObj d2024 */
    SCM_UNBOUND,
  },
#endif /*defined(IPV6_MULTICAST_HOPS)*/
#if defined(IPV6_MULTICAST_IF)
  {   /* ScmObj d2022 */
    SCM_UNBOUND,
  },
#endif /*defined(IPV6_MULTICAST_IF)*/
#if defined(IPV6_UNICAST_HOPS)
  {   /* ScmObj d2020 */
    SCM_UNBOUND,
  },
#endif /*defined(IPV6_UNICAST_HOPS)*/
#if defined(IPPROTO_IPV6)
  {   /* ScmObj d2018 */
    SCM_UNBOUND,
  },
#endif /*defined(IPPROTO_IPV6)*/
#if defined(PF_INET6)
  {   /* ScmObj d2016 */
    SCM_UNBOUND,
  },
#endif /*defined(PF_INET6)*/
#if defined(AF_INET6)
  {   /* ScmObj d2014 */
    SCM_UNBOUND,
  },
#endif /*defined(AF_INET6)*/
#if defined(HAVE_IPV6)
  {   /* ScmPair d2013 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d2013[1])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d2013[2])},
       { SCM_MAKE_INT(487U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1802[6]), SCM_OBJ(&scm__rc.d2013[4])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d2013[5])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d2013[7])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d2013[8])},
       { SCM_OBJ(&scm__rc.d2013[9]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d2013[6]), SCM_OBJ(&scm__rc.d2013[10])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d2013[12])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d2013[13])},
       { SCM_MAKE_INT(502U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1802[6]), SCM_OBJ(&scm__rc.d2013[15])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d2013[16])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d2013[18])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d2013[19])},
       { SCM_OBJ(&scm__rc.d2013[20]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d2013[17]), SCM_OBJ(&scm__rc.d2013[21])},
  },
#endif /*defined(HAVE_IPV6)*/
#if defined(IFF_DYNAMIC)
  {   /* ScmObj d2011 */
    SCM_UNBOUND,
  },
#endif /*defined(IFF_DYNAMIC)*/
#if defined(IFF_AUTOMEDIA)
  {   /* ScmObj d2009 */
    SCM_UNBOUND,
  },
#endif /*defined(IFF_AUTOMEDIA)*/
#if defined(IFF_PORTSEL)
  {   /* ScmObj d2007 */
    SCM_UNBOUND,
  },
#endif /*defined(IFF_PORTSEL)*/
#if defined(IFF_MULTICAST)
  {   /* ScmObj d2005 */
    SCM_UNBOUND,
  },
#endif /*defined(IFF_MULTICAST)*/
#if defined(IFF_SLAVE)
  {   /* ScmObj d2003 */
    SCM_UNBOUND,
  },
#endif /*defined(IFF_SLAVE)*/
#if defined(IFF_MASTER)
  {   /* ScmObj d2001 */
    SCM_UNBOUND,
  },
#endif /*defined(IFF_MASTER)*/
#if defined(IFF_ALLMULTI)
  {   /* ScmObj d1999 */
    SCM_UNBOUND,
  },
#endif /*defined(IFF_ALLMULTI)*/
#if defined(IFF_NOTRAILERS)
  {   /* ScmObj d1997 */
    SCM_UNBOUND,
  },
#endif /*defined(IFF_NOTRAILERS)*/
#if defined(IFF_PROMISC)
  {   /* ScmObj d1995 */
    SCM_UNBOUND,
  },
#endif /*defined(IFF_PROMISC)*/
#if defined(IFF_NOARP)
  {   /* ScmObj d1993 */
    SCM_UNBOUND,
  },
#endif /*defined(IFF_NOARP)*/
#if defined(IFF_RUNNING)
  {   /* ScmObj d1991 */
    SCM_UNBOUND,
  },
#endif /*defined(IFF_RUNNING)*/
#if defined(IFF_POINTTOPOINT)
  {   /* ScmObj d1989 */
    SCM_UNBOUND,
  },
#endif /*defined(IFF_POINTTOPOINT)*/
#if defined(IFF_LOOPBACK)
  {   /* ScmObj d1987 */
    SCM_UNBOUND,
  },
#endif /*defined(IFF_LOOPBACK)*/
#if defined(IFF_DEBUG)
  {   /* ScmObj d1985 */
    SCM_UNBOUND,
  },
#endif /*defined(IFF_DEBUG)*/
#if defined(IFF_BROADCAST)
  {   /* ScmObj d1983 */
    SCM_UNBOUND,
  },
#endif /*defined(IFF_BROADCAST)*/
#if defined(IFF_UP)
  {   /* ScmObj d1981 */
    SCM_UNBOUND,
  },
#endif /*defined(IFF_UP)*/
#if defined(SIOCGIFCONF)
  {   /* ScmObj d1979 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCGIFCONF)*/
#if defined(SIOSIFTXQLEN)
  {   /* ScmObj d1977 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOSIFTXQLEN)*/
#if defined(SIOGIFTXQLEN)
  {   /* ScmObj d1975 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOGIFTXQLEN)*/
#if defined(SIOCDELMULTI)
  {   /* ScmObj d1973 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCDELMULTI)*/
#if defined(SIOCADDMULTI)
  {   /* ScmObj d1971 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCADDMULTI)*/
#if defined(SIOCSIFMAP)
  {   /* ScmObj d1969 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCSIFMAP)*/
#if defined(SIOCGIFMAP)
  {   /* ScmObj d1967 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCGIFMAP)*/
#if defined(SIOCSIFHWBROADCAST)
  {   /* ScmObj d1965 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCSIFHWBROADCAST)*/
#if defined(SIOCSIFHWADDR)
  {   /* ScmObj d1963 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCSIFHWADDR)*/
#if defined(SIOCGIFHWADDR)
  {   /* ScmObj d1961 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCGIFHWADDR)*/
#if defined(SIOCSIFMTU)
  {   /* ScmObj d1959 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCSIFMTU)*/
#if defined(SIOCGIFMTU)
  {   /* ScmObj d1957 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCGIFMTU)*/
#if defined(SIOCSIFMETRIC)
  {   /* ScmObj d1955 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCSIFMETRIC)*/
#if defined(SIOCGIFMETRIC)
  {   /* ScmObj d1953 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCGIFMETRIC)*/
#if defined(SIOCSIFFLAGS)
  {   /* ScmObj d1951 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCSIFFLAGS)*/
#if defined(SIOCGIFFLAGS)
  {   /* ScmObj d1949 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCGIFFLAGS)*/
#if defined(SIOCSIFNETMASK)
  {   /* ScmObj d1947 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCSIFNETMASK)*/
#if defined(SIOCGIFNETMASK)
  {   /* ScmObj d1945 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCGIFNETMASK)*/
#if defined(SIOCSIFBRDADDR)
  {   /* ScmObj d1943 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCSIFBRDADDR)*/
#if defined(SIOCGIFBRDADDR)
  {   /* ScmObj d1941 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCGIFBRDADDR)*/
#if defined(SIOCSIFDSTADDR)
  {   /* ScmObj d1939 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCSIFDSTADDR)*/
#if defined(SIOCGIFDSTADDR)
  {   /* ScmObj d1937 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCGIFDSTADDR)*/
#if defined(SIOCSIFADDR)
  {   /* ScmObj d1935 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCSIFADDR)*/
#if defined(SIOCGIFADDR)
  {   /* ScmObj d1933 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCGIFADDR)*/
#if defined(SIOCGIFINDEX)
  {   /* ScmObj d1931 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCGIFINDEX)*/
#if defined(SIOCSIFNAME)
  {   /* ScmObj d1929 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCSIFNAME)*/
#if defined(SIOCGIFNAME)
  {   /* ScmObj d1927 */
    SCM_UNBOUND,
  },
#endif /*defined(SIOCGIFNAME)*/
#if defined(IP_MULTICAST_IF)
  {   /* ScmObj d1925 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_MULTICAST_IF)*/
#if defined(IP_DROP_MEMBERSHIP)
  {   /* ScmObj d1923 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_DROP_MEMBERSHIP)*/
#if defined(IP_ADD_MEMBERSHIP)
  {   /* ScmObj d1921 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_ADD_MEMBERSHIP)*/
#if defined(IP_MULTICAST_LOOP)
  {   /* ScmObj d1919 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_MULTICAST_LOOP)*/
#if defined(IP_MULTICAST_TTL)
  {   /* ScmObj d1917 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_MULTICAST_TTL)*/
#if defined(IP_ROUTER_ALERT)
  {   /* ScmObj d1915 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_ROUTER_ALERT)*/
#if defined(IP_MTU)
  {   /* ScmObj d1913 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_MTU)*/
#if defined(IP_MTU_DISCOVER)
  {   /* ScmObj d1911 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_MTU_DISCOVER)*/
#if defined(IP_RECVERR)
  {   /* ScmObj d1909 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_RECVERR)*/
#if defined(IP_HDRINCL)
  {   /* ScmObj d1907 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_HDRINCL)*/
#if defined(IP_TTL)
  {   /* ScmObj d1905 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_TTL)*/
#if defined(IP_TOS)
  {   /* ScmObj d1903 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_TOS)*/
#if defined(IP_RECVOPTS)
  {   /* ScmObj d1901 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_RECVOPTS)*/
#if defined(IP_RECVTTL)
  {   /* ScmObj d1899 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_RECVTTL)*/
#if defined(IP_RECVTOS)
  {   /* ScmObj d1897 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_RECVTOS)*/
#if defined(IP_PKTINFO)
  {   /* ScmObj d1895 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_PKTINFO)*/
#if defined(IP_OPTIONS)
  {   /* ScmObj d1893 */
    SCM_UNBOUND,
  },
#endif /*defined(IP_OPTIONS)*/
#if defined(SOL_IP)
  {   /* ScmObj d1891 */
    SCM_UNBOUND,
  },
#endif /*defined(SOL_IP)*/
#if defined(TCP_CORK)
  {   /* ScmObj d1889 */
    SCM_UNBOUND,
  },
#endif /*defined(TCP_CORK)*/
#if defined(TCP_MAXSEG)
  {   /* ScmObj d1887 */
    SCM_UNBOUND,
  },
#endif /*defined(TCP_MAXSEG)*/
#if defined(TCP_NODELAY)
  {   /* ScmObj d1885 */
    SCM_UNBOUND,
  },
#endif /*defined(TCP_NODELAY)*/
#if defined(SOL_TCP)
  {   /* ScmObj d1883 */
    SCM_UNBOUND,
  },
#endif /*defined(SOL_TCP)*/
#if defined(SO_TYPE)
  {   /* ScmObj d1881 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_TYPE)*/
#if defined(SO_TIMESTAMP)
  {   /* ScmObj d1879 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_TIMESTAMP)*/
#if defined(SO_SNDTIMEO)
  {   /* ScmObj d1877 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_SNDTIMEO)*/
#if defined(SO_SNDLOWAT)
  {   /* ScmObj d1875 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_SNDLOWAT)*/
#if defined(SO_SNDBUF)
  {   /* ScmObj d1873 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_SNDBUF)*/
#if defined(SO_REUSEPORT)
  {   /* ScmObj d1871 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_REUSEPORT)*/
#if defined(SO_REUSEADDR)
  {   /* ScmObj d1869 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_REUSEADDR)*/
#if defined(SO_RCVTIMEO)
  {   /* ScmObj d1867 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_RCVTIMEO)*/
#if defined(SO_RCVLOWAT)
  {   /* ScmObj d1865 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_RCVLOWAT)*/
#if defined(SO_RCVBUF)
  {   /* ScmObj d1863 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_RCVBUF)*/
#if defined(SO_PRIORITY)
  {   /* ScmObj d1861 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_PRIORITY)*/
#if defined(SO_PEERCRED)
  {   /* ScmObj d1859 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_PEERCRED)*/
#if defined(SO_PASSCRED)
  {   /* ScmObj d1857 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_PASSCRED)*/
#if defined(SO_OOBINLINE)
  {   /* ScmObj d1855 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_OOBINLINE)*/
#if defined(SO_LINGER)
  {   /* ScmObj d1853 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_LINGER)*/
#if defined(SO_KEEPALIVE)
  {   /* ScmObj d1851 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_KEEPALIVE)*/
#if defined(SO_ERROR)
  {   /* ScmObj d1849 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_ERROR)*/
#if defined(SO_DONTROUTE)
  {   /* ScmObj d1847 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_DONTROUTE)*/
#if defined(SO_DEBUG)
  {   /* ScmObj d1845 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_DEBUG)*/
#if defined(SO_BROADCAST)
  {   /* ScmObj d1843 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_BROADCAST)*/
#if defined(SO_BINDTODEVICE)
  {   /* ScmObj d1841 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_BINDTODEVICE)*/
#if defined(SO_ACCEPTCONN)
  {   /* ScmObj d1839 */
    SCM_UNBOUND,
  },
#endif /*defined(SO_ACCEPTCONN)*/
#if defined(SOL_SOCKET)
  {   /* ScmObj d1837 */
    SCM_UNBOUND,
  },
#endif /*defined(SOL_SOCKET)*/
#if !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))
  {   /* ScmObj d1835 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
  },
#endif /*!((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))*/
#if (defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))
  {   /* ScmObj d1833 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
  },
#endif /*(defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))*/
#if defined(SOMAXCONN)
  {   /* ScmObj d1831 */
    SCM_UNBOUND,
  },
#endif /*defined(SOMAXCONN)*/
#if defined(IPPROTO_UDP)
  {   /* ScmObj d1829 */
    SCM_UNBOUND,
  },
#endif /*defined(IPPROTO_UDP)*/
#if defined(IPPROTO_TCP)
  {   /* ScmObj d1827 */
    SCM_UNBOUND,
  },
#endif /*defined(IPPROTO_TCP)*/
#if defined(IPPROTO_ICMPV6)
  {   /* ScmObj d1825 */
    SCM_UNBOUND,
  },
#endif /*defined(IPPROTO_ICMPV6)*/
#if defined(IPPROTO_ICMP)
  {   /* ScmObj d1823 */
    SCM_UNBOUND,
  },
#endif /*defined(IPPROTO_ICMP)*/
#if defined(IPPROTO_IP)
  {   /* ScmObj d1821 */
    SCM_UNBOUND,
  },
#endif /*defined(IPPROTO_IP)*/
#if defined(MSG_WAITALL)
  {   /* ScmObj d1819 */
    SCM_UNBOUND,
  },
#endif /*defined(MSG_WAITALL)*/
#if defined(MSG_TRUNC)
  {   /* ScmObj d1817 */
    SCM_UNBOUND,
  },
#endif /*defined(MSG_TRUNC)*/
#if defined(MSG_PEEK)
  {   /* ScmObj d1815 */
    SCM_UNBOUND,
  },
#endif /*defined(MSG_PEEK)*/
#if defined(MSG_OOB)
  {   /* ScmObj d1813 */
    SCM_UNBOUND,
  },
#endif /*defined(MSG_OOB)*/
#if defined(MSG_EOR)
  {   /* ScmObj d1811 */
    SCM_UNBOUND,
  },
#endif /*defined(MSG_EOR)*/
#if defined(MSG_DONTROUTE)
  {   /* ScmObj d1809 */
    SCM_UNBOUND,
  },
#endif /*defined(MSG_DONTROUTE)*/
#if defined(MSG_CTRUNC)
  {   /* ScmObj d1807 */
    SCM_UNBOUND,
  },
#endif /*defined(MSG_CTRUNC)*/
#if defined(HAVE_IPV6)
  {   /* ScmObj d1801 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
  },
#endif /*defined(HAVE_IPV6)*/
  {   /* ScmUVector d1791 */
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 41, uvector__00001, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 30, uvector__00002, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 39, uvector__00003, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 33, uvector__00004, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 775, uvector__00005, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 79, uvector__00006, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 114, uvector__00007, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 60, uvector__00008, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 532, uvector__00009, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 143, uvector__00010, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 98, uvector__00011, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 51, uvector__00012, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 106, uvector__00013, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 54, uvector__00014, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 28, uvector__00015, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 96, uvector__00016, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 66, uvector__00017, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 168, uvector__00018, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 90, uvector__00019, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 585, uvector__00020, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 163, uvector__00021, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 730, uvector__00022, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 86, uvector__00023, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 374, uvector__00024, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 63, uvector__00025, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 59, uvector__00026, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 49, uvector__00027, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 26, uvector__00028, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 34, uvector__00029, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 57, uvector__00030, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 177, uvector__00031, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 93, uvector__00032, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 42, uvector__00033, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 42, uvector__00034, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 89, uvector__00035, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 38, uvector__00036, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 75, uvector__00037, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 38, uvector__00038, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 67, uvector__00039, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 263, uvector__00040, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 71, uvector__00041, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 128, uvector__00042, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 864, uvector__00043, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 449, uvector__00044, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 29, uvector__00045, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 44, uvector__00046, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 855, uvector__00047, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 248, uvector__00048, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 156, uvector__00049, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 22, uvector__00050, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 448, uvector__00051, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 90, uvector__00052, 0, NULL),
  },
  {   /* ScmCompiledCode d1790 */
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[0])), 14,
            12, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[14])), 14,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[28])), 19,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[47])), 13,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* make-sys-addrinfo */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[60])), 194,
            45, 0, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[462]),
            SCM_OBJ(&scm__rc.d1790[5]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[254])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* address->protocol-family */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[269])), 33,
            11, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[467]),
            SCM_OBJ(&scm__rc.d1790[7]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[302])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* make-client-socket */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[317])), 135,
            22, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[473]),
            SCM_OBJ(&scm__rc.d1790[9]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[452])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* make-client-socket-from-addr */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[467])), 19,
            19, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[478]),
            SCM_OBJ(&scm__rc.d1790[11]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[486])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* make-client-socket-unix */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[501])), 24,
            24, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[484]),
            SCM_OBJ(&scm__rc.d1790[13]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[525])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[540])), 3,
            0, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[490]),
            SCM_OBJ(&scm__rc.d1790[16]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[543])), 19,
            19, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[492]),
            SCM_OBJ(&scm__rc.d1790[16]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* try-connect */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[562])), 13,
            7, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[497]),
            SCM_OBJ(&scm__rc.d1790[17]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* make-client-socket-inet */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[575])), 28,
            27, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[505]),
            SCM_OBJ(&scm__rc.d1790[18]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[603])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* make-server-socket */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[618])), 135,
            24, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[510]),
            SCM_OBJ(&scm__rc.d1790[20]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[753])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* make-server-socket-from-addr */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[768])), 172,
            45, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[524]),
            SCM_OBJ(&scm__rc.d1790[22]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[940])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* make-server-socket-unix */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[955])), 88,
            40, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[531]),
            SCM_OBJ(&scm__rc.d1790[24]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1043])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* make-server-socket-inet */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1058])), 12,
            14, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[537]),
            SCM_OBJ(&scm__rc.d1790[26]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1070])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* (make-server-socket-inet* G2084) */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1085])), 5,
            5, 1, 0, SCM_OBJ(&scm__rc.d1788[539]), SCM_NIL, SCM_OBJ(&scm__rc.d1788[545]),
            SCM_OBJ(&scm__rc.d1790[32]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1090])), 3,
            0, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[550]),
            SCM_OBJ(&scm__rc.d1790[30]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1093])), 6,
            3, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[552]),
            SCM_OBJ(&scm__rc.d1790[30]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* try-bind */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1099])), 13,
            7, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[556]),
            SCM_OBJ(&scm__rc.d1790[31]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* make-server-socket-inet* */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1112])), 28,
            27, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[563]),
            SCM_OBJ(&scm__rc.d1790[32]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1140])), 17,
            14, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* (make-server-sockets G2091) */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1157])), 10,
            11, 1, 0, SCM_OBJ(&scm__rc.d1788[565]), SCM_NIL, SCM_OBJ(&scm__rc.d1788[570]),
            SCM_OBJ(&scm__rc.d1790[46]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* (make-server-sockets G2090) */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1167])), 10,
            11, 1, 0, SCM_OBJ(&scm__rc.d1788[572]), SCM_NIL, SCM_OBJ(&scm__rc.d1788[577]),
            SCM_OBJ(&scm__rc.d1790[46]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* (make-server-sockets G2089) */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1177])), 10,
            11, 1, 0, SCM_OBJ(&scm__rc.d1788[579]), SCM_NIL, SCM_OBJ(&scm__rc.d1788[580]),
            SCM_OBJ(&scm__rc.d1790[46]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* (make-server-sockets G2088) */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1187])), 10,
            11, 1, 0, SCM_OBJ(&scm__rc.d1788[582]), SCM_NIL, SCM_OBJ(&scm__rc.d1788[583]),
            SCM_OBJ(&scm__rc.d1790[46]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* bind-failed? */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1197])), 19,
            12, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[591]),
            SCM_OBJ(&scm__rc.d1790[45]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1216])), 6,
            3, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[602]),
            SCM_OBJ(&scm__rc.d1790[45]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1222])), 14,
            11, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[606]),
            SCM_OBJ(&scm__rc.d1790[42]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1236])), 6,
            3, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[614]),
            SCM_OBJ(&scm__rc.d1790[41]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1242])), 11,
            13, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[616]),
            SCM_OBJ(&scm__rc.d1790[42]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1253])), 47,
            25, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[622]),
            SCM_OBJ(&scm__rc.d1790[45]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1300])), 13,
            11, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[624]),
            SCM_OBJ(&scm__rc.d1790[45]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1313])), 25,
            23, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[626]),
            SCM_OBJ(&scm__rc.d1790[45]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* make-server-sockets */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1338])), 164,
            53, 2, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[632]),
            SCM_OBJ(&scm__rc.d1790[46]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1502])), 17,
            17, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* (make-sockaddrs G2100) */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1519])), 4,
            0, 1, 0, SCM_OBJ(&scm__rc.d1788[634]), SCM_NIL, SCM_OBJ(&scm__rc.d1788[640]),
            SCM_OBJ(&scm__rc.d1790[52]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* (make-sockaddrs G2099) */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1523])), 10,
            11, 1, 0, SCM_OBJ(&scm__rc.d1788[642]), SCM_NIL, SCM_OBJ(&scm__rc.d1788[647]),
            SCM_OBJ(&scm__rc.d1790[52]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* (make-sockaddrs G2098) */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1533])), 10,
            11, 1, 0, SCM_OBJ(&scm__rc.d1788[649]), SCM_NIL, SCM_OBJ(&scm__rc.d1788[654]),
            SCM_OBJ(&scm__rc.d1790[52]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1543])), 11,
            8, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[806]),
            SCM_OBJ(&scm__rc.d1790[51]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* make-sockaddrs */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1554])), 188,
            37, 2, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[811]),
            SCM_OBJ(&scm__rc.d1790[52]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1742])), 17,
            16, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1759])), 41,
            14, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[814]),
            SCM_OBJ(&scm__rc.d1790[55]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1800])), 4,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[814]),
            SCM_OBJ(&scm__rc.d1790[55]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* call-with-client-socket */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1804])), 99,
            29, 2, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1788[828]),
            SCM_OBJ(&scm__rc.d1790[56]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1789[1903])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
  },
  {   /* ScmWord d1789 */
    /* %toplevel */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[0]) + 6),
    0x00000006    /*   2 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* gauche.net */,
    0x0000105f    /*   4 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#find-module.81804e0> */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000006    /*   7 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[76])) /* "gauche/netutil" */,
    0x00000006    /*   9 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1788[7])) /* (connection-self-address connection-peer-address connection-input-port connection-output-port connection-shutdown connection-close connection-address-name) */,
    0x00003060    /*  11 (GREF-TAIL-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%autoload.8180560> */,
    0x00000014    /*  13 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[14]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* DEFAULT_BACKLOG */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[14]) + 11),
    0x00005002    /*   9 (CONSTI 5) */,
    0x00000014    /*  10 (RET) */,
    0x00002015    /*  11 (DEFINE 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#DEFAULT_BACKLOG.8a14e00> */,
    0x00000014    /*  13 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[28]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* ipv6-capable */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[28]) + 16),
    0x00000006    /*   9 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* gauche.net */,
    0x00000006    /*  11 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* sys-getaddrinfo */,
    0x00002060    /*  13 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#module-binds?.8a51ae0> */,
    0x00000014    /*  15 (RET) */,
    0x00000015    /*  16 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#ipv6-capable.8a51b80> */,
    0x00000014    /*  18 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[47]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* ipv4-preferred */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[47]) + 10),
    0x0000000b    /*   9 (CONSTF-RET) */,
    0x00000015    /*  10 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#ipv4-preferred.8478a20> */,
    0x00000014    /*  12 (RET) */,
    /* make-sys-addrinfo */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00000005    /*   1 (CONSTU) */,
    0x0000000d    /*   2 (PUSH) */,
    0x00000005    /*   3 (CONSTU) */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000005    /*   5 (CONSTU) */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000005    /*   7 (CONSTU) */,
    0x00005018    /*   8 (PUSH-LOCAL-ENV 5) */,
    0x0100003c    /*   9 (LREF 0 4) */,
    0x00000022    /*  10 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 103),
    0x0000100e    /*  12 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 17),
    0x0000004b    /*  14 (LREF3-PUSH) */,
    0x0000105f    /*  15 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.84ca3c0> */,
    0x0000001e    /*  17 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 22),
    0x00000002    /*  19 (CONSTI 0) */,
    0x00000013    /*  20 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 23),
    0x00000040    /*  22 (LREF3) */,
    0x00001018    /*  23 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  24 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 29),
    0x0000004e    /*  26 (LREF12-PUSH) */,
    0x0000105f    /*  27 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.84ca3c0> */,
    0x0000001e    /*  29 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 35),
    0x0000005d    /*  31 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#AF_UNSPEC.84cea40> */,
    0x00000013    /*  33 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 36),
    0x00000043    /*  35 (LREF12) */,
    0x00001018    /*  36 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  37 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 42),
    0x00000050    /*  39 (LREF21-PUSH) */,
    0x0000105f    /*  40 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.84ca3c0> */,
    0x0000001e    /*  42 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 47),
    0x00000002    /*  44 (CONSTI 0) */,
    0x00000013    /*  45 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 48),
    0x00000045    /*  47 (LREF21) */,
    0x00001018    /*  48 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  49 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 54),
    0x00000051    /*  51 (LREF30-PUSH) */,
    0x0000105f    /*  52 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.84ca3c0> */,
    0x0000001e    /*  54 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 59),
    0x00000002    /*  56 (CONSTI 0) */,
    0x00000013    /*  57 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 60),
    0x00000046    /*  59 (LREF30) */,
    0x00001018    /*  60 (PUSH-LOCAL-ENV 1) */,
    0x0000005d    /*  61 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#ipv6-capable.84ce880> */,
    0x0000001e    /*  63 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 98),
    0x0000005e    /*  65 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<sys-addrinfo>.84ce760> */,
    0x00000006    /*  67 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :flags */,
    0x0000100e    /*  69 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 74),
    0x00000051    /*  71 (LREF30-PUSH) */,
    0x0000105f    /*  72 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#list?.84ce6c0> */,
    0x0000001e    /*  74 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 84),
    0x0000000e    /*  76 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 85),
    0x0000005e    /*  78 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#logior.84ce600> */,
    0x00000046    /*  80 (LREF30) */,
    0x00002095    /*  81 (TAIL-APPLY 2) */,
    0x00000013    /*  82 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 85),
    0x00000046    /*  84 (LREF30) */,
    0x0000000d    /*  85 (PUSH) */,
    0x00000006    /*  86 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :family */,
    0x0000004f    /*  88 (LREF20-PUSH) */,
    0x00000006    /*  89 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :socktype */,
    0x0000004c    /*  91 (LREF10-PUSH) */,
    0x00000006    /*  92 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :protocol */,
    0x00000048    /*  94 (LREF0-PUSH) */,
    0x00009060    /*  95 (GREF-TAIL-CALL 9) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make.84ce820> */,
    0x00000014    /*  97 (RET) */,
    0x00000006    /*  98 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[277])) /* "make-sys-addrinfo is available on IPv6-enabled platform" */,
    0x00001060    /* 100 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.84cffe0> */,
    0x00000014    /* 102 (RET) */,
    0x0100003c    /* 103 (LREF 0 4) */,
    0x00000074    /* 104 (CDR) */,
    0x00000022    /* 105 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 113),
    0x00000006    /* 107 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[279])) /* "keyword list not even" */,
    0x01000047    /* 109 (LREF-PUSH 0 4) */,
    0x00002060    /* 110 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.84ca220> */,
    0x00000014    /* 112 (RET) */,
    0x0000100e    /* 113 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 119),
    0x0100003c    /* 115 (LREF 0 4) */,
    0x00000069    /* 116 (CAR-PUSH) */,
    0x0000105f    /* 117 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#unwrap-syntax-1.84ca120> */,
    0x00001018    /* 119 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /* 120 (LREF0) */,
    0x0000002e    /* 121 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :flags */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 134)  /*    134 */,
    0x0100103c    /* 124 (LREF 1 4) */,
    0x00000087    /* 125 (CDDR-PUSH) */,
    0x0100103c    /* 126 (LREF 1 4) */,
    0x00000083    /* 127 (CADR-PUSH) */,
    0x0000004e    /* 128 (LREF12-PUSH) */,
    0x0000004d    /* 129 (LREF11-PUSH) */,
    0x0000004c    /* 130 (LREF10-PUSH) */,
    0x0000201b    /* 131 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 9),
    0x00000014    /* 133 (RET) */,
    0x0000003d    /* 134 (LREF0) */,
    0x0000002e    /* 135 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :family */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 148)  /*    148 */,
    0x0100103c    /* 138 (LREF 1 4) */,
    0x00000087    /* 139 (CDDR-PUSH) */,
    0x00c01047    /* 140 (LREF-PUSH 1 3) */,
    0x0100103c    /* 141 (LREF 1 4) */,
    0x00000083    /* 142 (CADR-PUSH) */,
    0x0000004d    /* 143 (LREF11-PUSH) */,
    0x0000004c    /* 144 (LREF10-PUSH) */,
    0x0000201b    /* 145 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 9),
    0x00000014    /* 147 (RET) */,
    0x0000003d    /* 148 (LREF0) */,
    0x0000002e    /* 149 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :socktype */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 162)  /*    162 */,
    0x0100103c    /* 152 (LREF 1 4) */,
    0x00000087    /* 153 (CDDR-PUSH) */,
    0x00c01047    /* 154 (LREF-PUSH 1 3) */,
    0x0000004e    /* 155 (LREF12-PUSH) */,
    0x0100103c    /* 156 (LREF 1 4) */,
    0x00000083    /* 157 (CADR-PUSH) */,
    0x0000004c    /* 158 (LREF10-PUSH) */,
    0x0000201b    /* 159 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 9),
    0x00000014    /* 161 (RET) */,
    0x0000003d    /* 162 (LREF0) */,
    0x0000002e    /* 163 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :protocol */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 176)  /*    176 */,
    0x0100103c    /* 166 (LREF 1 4) */,
    0x00000087    /* 167 (CDDR-PUSH) */,
    0x00c01047    /* 168 (LREF-PUSH 1 3) */,
    0x0000004e    /* 169 (LREF12-PUSH) */,
    0x0000004d    /* 170 (LREF11-PUSH) */,
    0x0100103c    /* 171 (LREF 1 4) */,
    0x00000083    /* 172 (CADR-PUSH) */,
    0x0000201b    /* 173 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 9),
    0x00000014    /* 175 (RET) */,
    0x0000200e    /* 176 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 184),
    0x00000006    /* 178 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[281])) /* "unknown keyword ~S" */,
    0x0100103c    /* 180 (LREF 1 4) */,
    0x00000069    /* 181 (CAR-PUSH) */,
    0x0000205f    /* 182 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#errorf.84ced60> */,
    0x0100103c    /* 184 (LREF 1 4) */,
    0x00000087    /* 185 (CDDR-PUSH) */,
    0x00c01047    /* 186 (LREF-PUSH 1 3) */,
    0x0000004e    /* 187 (LREF12-PUSH) */,
    0x0000004d    /* 188 (LREF11-PUSH) */,
    0x0000004c    /* 189 (LREF10-PUSH) */,
    0x0000201b    /* 190 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]) + 9),
    0x00000014    /* 192 (RET) */,
    0x00000014    /* 193 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[254]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* make-sys-addrinfo */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[254]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[4])) /* #<compiled-code make-sys-addrinfo@0x7f0607d66c00> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-sys-addrinfo.84caea0> */,
    0x00000014    /*  14 (RET) */,
    /* address->protocol-family */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[269]) + 5),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sockaddr-family.81db440> */,
    0x00001018    /*   5 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*   6 (LREF0) */,
    0x0000002e    /*   7 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* unix */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[269]) + 13)  /*     13 */,
    0x0000005d    /*  10 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#PF_UNIX.81db340> */,
    0x00000014    /*  12 (RET) */,
    0x0000003d    /*  13 (LREF0) */,
    0x0000002e    /*  14 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* inet */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[269]) + 20)  /*     20 */,
    0x0000005d    /*  17 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#PF_INET.81db220> */,
    0x00000014    /*  19 (RET) */,
    0x0000003d    /*  20 (LREF0) */,
    0x0000002e    /*  21 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* inet6 */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[269]) + 27)  /*     27 */,
    0x0000005d    /*  24 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#PF_INET6.81db0e0> */,
    0x00000014    /*  26 (RET) */,
    0x00000006    /*  27 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[285])) /* "unknown family of socket address" */,
    0x0000004c    /*  29 (LREF10-PUSH) */,
    0x00002060    /*  30 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.81db080> */,
    0x00000014    /*  32 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[302]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* address->protocol-family */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[302]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[6])) /* #<compiled-code address->protocol-family@0x7f0607ebd300> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#address->protocol-family.81db5c0> */,
    0x00000014    /*  14 (RET) */,
    /* make-client-socket */
    0x0000003e    /*   0 (LREF1) */,
    0x0000002e    /*   1 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* unix */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 29)  /*     29 */,
    0x0000003d    /*   4 (LREF0) */,
    0x00000022    /*   5 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 10),
    0x00000004    /*   7 (CONSTF) */,
    0x00000013    /*   8 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 11),
    0x0000006a    /*  10 (LREF0-CAR) */,
    0x00001018    /*  11 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  12 (LREF0) */,
    0x0000009b    /*  13 (STRINGP) */,
    0x0000001e    /*  14 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 18),
    0x00000013    /*  16 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 25),
    0x0000200e    /*  18 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 25),
    0x00000006    /*  20 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[286])) /* "unix socket requires pathname, but got" */,
    0x00000048    /*  22 (LREF0-PUSH) */,
    0x0000205f    /*  23 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.7c0b8c0> */,
    0x00000048    /*  25 (LREF0-PUSH) */,
    0x00001060    /*  26 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-client-socket-unix.7c0b820> */,
    0x00000014    /*  28 (RET) */,
    0x0000003e    /*  29 (LREF1) */,
    0x0000002e    /*  30 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* inet */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 93)  /*     93 */,
    0x0000003d    /*  33 (LREF0) */,
    0x00000022    /*  34 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 39),
    0x00000004    /*  36 (CONSTF) */,
    0x00000013    /*  37 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 40),
    0x0000006a    /*  39 (LREF0-CAR) */,
    0x0000000d    /*  40 (PUSH) */,
    0x0000003d    /*  41 (LREF0) */,
    0x00000022    /*  42 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 47),
    0x00000003    /*  44 (CONSTN) */,
    0x00000013    /*  45 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 48),
    0x00000076    /*  47 (LREF0-CDR) */,
    0x00002018    /*  48 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  49 (LREF0) */,
    0x00000022    /*  50 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 55),
    0x00000004    /*  52 (CONSTF) */,
    0x00000013    /*  53 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 56),
    0x0000006a    /*  55 (LREF0-CAR) */,
    0x00001018    /*  56 (PUSH-LOCAL-ENV 1) */,
    0x00000042    /*  57 (LREF11) */,
    0x0000009b    /*  58 (STRINGP) */,
    0x0000001e    /*  59 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 76),
    0x0000100e    /*  61 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 66),
    0x00000048    /*  63 (LREF0-PUSH) */,
    0x0000105f    /*  64 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#integer?.7c22fa0> */,
    0x0000001e    /*  66 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 70),
    0x00000013    /*  68 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 88),
    0x0000003d    /*  70 (LREF0) */,
    0x0000009b    /*  71 (STRINGP) */,
    0x0000001e    /*  72 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 76),
    0x00000013    /*  74 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 88),
    0x0000300e    /*  76 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 88),
    0x00000006    /*  78 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[289])) /* "inet socket requires host name and port, but got ~s and ~s" */,
    0x0000004d    /*  80 (LREF11-PUSH) */,
    0x00000048    /*  81 (LREF0-PUSH) */,
    0x0000305f    /*  82 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#errorf.7c22ee0> */,
    0x00000013    /*  84 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 88),
    0x00000013    /*  86 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 76),
    0x0000004d    /*  88 (LREF11-PUSH) */,
    0x00000048    /*  89 (LREF0-PUSH) */,
    0x00002060    /*  90 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-client-socket-inet.7c22dc0> */,
    0x00000014    /*  92 (RET) */,
    0x00000049    /*  93 (LREF1-PUSH) */,
    0x0000005d    /*  94 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<sockaddr>.7c22c60> */,
    0x00000096    /*  96 (IS-A) */,
    0x0000001e    /*  97 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 103),
    0x00000049    /*  99 (LREF1-PUSH) */,
    0x00001060    /* 100 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-client-socket-from-addr.7c22c00> */,
    0x00000014    /* 102 (RET) */,
    0x0000003e    /* 103 (LREF1) */,
    0x0000009b    /* 104 (STRINGP) */,
    0x0000001e    /* 105 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 123),
    0x0000003d    /* 107 (LREF0) */,
    0x00000098    /* 108 (PAIRP) */,
    0x0000001e    /* 109 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 123),
    0x0000100e    /* 111 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 116),
    0x0000006a    /* 113 (LREF0-CAR) */,
    0x00001062    /* 114 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#integer?.7c22a80> */,
    0x0000001e    /* 116 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 123),
    0x00000049    /* 118 (LREF1-PUSH) */,
    0x0000006a    /* 119 (LREF0-CAR) */,
    0x00002063    /* 120 (PUSH-GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-client-socket-inet.7c22a00> */,
    0x00000014    /* 122 (RET) */,
    0x00000006    /* 123 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[292])) /* "unsupported protocol:" */,
    0x00000049    /* 125 (LREF1-PUSH) */,
    0x00002060    /* 126 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.7c22920> */,
    0x00000014    /* 128 (RET) */,
    0x00000013    /* 129 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 123),
    0x00000014    /* 131 (RET) */,
    0x00000013    /* 132 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]) + 123),
    0x00000014    /* 134 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[452]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* make-client-socket */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[452]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[8])) /* #<compiled-code make-client-socket@0x7f0607b092a0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-client-socket.7c08320> */,
    0x00000014    /*  14 (RET) */,
    /* make-client-socket-from-addr */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[467]) + 11),
    0x0000100e    /*   2 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[467]) + 7),
    0x00000048    /*   4 (LREF0-PUSH) */,
    0x0000105f    /*   5 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#address->protocol-family.7ec23c0> */,
    0x00000061    /*   7 (PUSH-GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#SOCK_STREAM.7ec2300> */,
    0x00002062    /*   9 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-socket.7ec2400> */,
    0x00001018    /*  11 (PUSH-LOCAL-ENV 1) */,
    0x0000200e    /*  12 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[467]) + 18),
    0x00000048    /*  14 (LREF0-PUSH) */,
    0x0000004c    /*  15 (LREF10-PUSH) */,
    0x0000205f    /*  16 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#socket-connect.7ec2280> */,
    0x00000053    /*  18 (LREF0-RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[486]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* make-client-socket-from-addr */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[486]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[10])) /* #<compiled-code make-client-socket-from-addr@0x7f060745e8a0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-client-socket-from-addr.7ec2960> */,
    0x00000014    /*  14 (RET) */,
    /* make-client-socket-unix */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[501]) + 8),
    0x0000005e    /*   2 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#PF_UNIX.8db6cc0> */,
    0x0000005e    /*   4 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#SOCK_STREAM.8db6c60> */,
    0x0000205f    /*   6 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-socket.8db6d00> */,
    0x00001018    /*   8 (PUSH-LOCAL-ENV 1) */,
    0x0000200e    /*   9 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[501]) + 23),
    0x00000048    /*  11 (LREF0-PUSH) */,
    0x0000300e    /*  12 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[501]) + 21),
    0x0000005e    /*  14 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<sockaddr-un>.8db6a60> */,
    0x00000006    /*  16 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :path */,
    0x0000004c    /*  18 (LREF10-PUSH) */,
    0x0000305f    /*  19 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make.8db6aa0> */,
    0x00002062    /*  21 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#socket-connect.8db6b40> */,
    0x00000053    /*  23 (LREF0-RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[525]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* make-client-socket-unix */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[525]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[12])) /* #<compiled-code make-client-socket-unix@0x7f0608d23c00> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-client-socket-unix.8db3260> */,
    0x00000014    /*  14 (RET) */,
    /* (make-client-socket-inet try-connect #f) */
    0x0000003d    /*   0 (LREF0) */,
    0x0000303a    /*   1 (LSET 3 0) */,
    0x0000000b    /*   2 (CONSTF-RET) */,
    /* (make-client-socket-inet try-connect #f) */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[543]) + 11),
    0x0000100e    /*   2 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[543]) + 7),
    0x00000048    /*   4 (LREF0-PUSH) */,
    0x0000105f    /*   5 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#address->protocol-family.898b720> */,
    0x00000061    /*   7 (PUSH-GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#SOCK_STREAM.898b6c0> */,
    0x00002062    /*   9 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-socket.898b780> */,
    0x00001018    /*  11 (PUSH-LOCAL-ENV 1) */,
    0x0000200e    /*  12 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[543]) + 18),
    0x00000048    /*  14 (LREF0-PUSH) */,
    0x0000004c    /*  15 (LREF10-PUSH) */,
    0x0000205f    /*  16 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#socket-connect.898b4a0> */,
    0x00000053    /*  18 (LREF0-RET) */,
    /* (make-client-socket-inet try-connect) */
    0x00000016    /*   0 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[14])) /* #<compiled-code (make-client-socket-inet try-connect #f)@0x7f06086820c0> */,
    0x0000000d    /*   2 (PUSH) */,
    0x00000016    /*   3 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[15])) /* #<compiled-code (make-client-socket-inet try-connect #f)@0x7f0608682060> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000006    /*   6 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :rewind-before */,
    0x00000006    /*   8 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x00004060    /*  10 (GREF-TAIL-CALL 4) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#with-error-handler.8988000> */,
    0x00000014    /*  12 (RET) */,
    /* make-client-socket-inet */
    0x00000009    /*   0 (CONSTF-PUSH) */,
    0x00001017    /*   1 (LOCAL-ENV 1) */,
    0x000010e7    /*   2 (BOX 1) */,
    0x00001019    /*   3 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1788[498])) /* (#<compiled-code (make-client-socket-inet try-connect)@0x7f06086821e0>) */,
    0x0000200e    /*   5 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[575]) + 16),
    0x00000048    /*   7 (LREF0-PUSH) */,
    0x0000200e    /*   8 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[575]) + 14),
    0x00000050    /*  10 (LREF21-PUSH) */,
    0x0000004f    /*  11 (LREF20-PUSH) */,
    0x0000205f    /*  12 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-sockaddrs.898b280> */,
    0x00002062    /*  14 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#any.898b2e0> */,
    0x00001018    /*  16 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  17 (LREF0) */,
    0x0000001e    /*  18 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[575]) + 22),
    0x00000013    /*  20 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[575]) + 27),
    0x0000100e    /*  22 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[575]) + 27),
    0x000020ea    /*  24 (LREF-UNBOX 2 0) */,
    0x00001062    /*  25 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#raise.898b180> */,
    0x00000053    /*  27 (LREF0-RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[603]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* make-client-socket-inet */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[603]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[17])) /* #<compiled-code make-client-socket-inet@0x7f0608682360> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-client-socket-inet.897e8e0> */,
    0x00000014    /*  14 (RET) */,
    /* make-server-socket */
    0x0000003e    /*   0 (LREF1) */,
    0x0000002e    /*   1 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* unix */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 31)  /*     31 */,
    0x0000003d    /*   4 (LREF0) */,
    0x00000022    /*   5 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 10),
    0x00000004    /*   7 (CONSTF) */,
    0x00000013    /*   8 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 11),
    0x0000006a    /*  10 (LREF0-CAR) */,
    0x00001018    /*  11 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  12 (LREF0) */,
    0x0000009b    /*  13 (STRINGP) */,
    0x0000001e    /*  14 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 18),
    0x00000013    /*  16 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 25),
    0x0000200e    /*  18 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 25),
    0x00000006    /*  20 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[286])) /* "unix socket requires pathname, but got" */,
    0x00000048    /*  22 (LREF0-PUSH) */,
    0x0000205f    /*  23 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.854f6e0> */,
    0x0000005e    /*  25 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-server-socket-unix.854f620> */,
    0x00000048    /*  27 (LREF0-PUSH) */,
    0x0000007a    /*  28 (LREF10-CDR) */,
    0x00003095    /*  29 (TAIL-APPLY 3) */,
    0x00000014    /*  30 (RET) */,
    0x0000003e    /*  31 (LREF1) */,
    0x0000002e    /*  32 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* inet */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 104)  /*    104 */,
    0x0000003d    /*  35 (LREF0) */,
    0x00000022    /*  36 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 41),
    0x00000004    /*  38 (CONSTF) */,
    0x00000013    /*  39 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 42),
    0x0000006a    /*  41 (LREF0-CAR) */,
    0x00001018    /*  42 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  43 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 48),
    0x00000048    /*  45 (LREF0-PUSH) */,
    0x0000105f    /*  46 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#list?.8550260> */,
    0x0000001e    /*  48 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 81),
    0x0000200e    /*  50 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 64),
    0x0000200e    /*  52 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 60),
    0x0000005e    /*  54 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#integer?.8550000> */,
    0x0000005e    /*  56 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string?.8551f80> */,
    0x0000205f    /*  58 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#any-pred.8550060> */,
    0x0000000d    /*  60 (PUSH) */,
    0x00000048    /*  61 (LREF0-PUSH) */,
    0x0000205f    /*  62 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#every.8550160> */,
    0x0000001e    /*  64 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 68),
    0x00000013    /*  66 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 75),
    0x0000200e    /*  68 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 75),
    0x00000006    /*  70 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[307])) /* "inet socket requires integer port number or string service name, or a list of them, but got:" */,
    0x00000048    /*  72 (LREF0-PUSH) */,
    0x0000205f    /*  73 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.8550420> */,
    0x0000005e    /*  75 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-server-socket-inet*.8551e00> */,
    0x00000048    /*  77 (LREF0-PUSH) */,
    0x0000007a    /*  78 (LREF10-CDR) */,
    0x00003095    /*  79 (TAIL-APPLY 3) */,
    0x00000014    /*  80 (RET) */,
    0x0000100e    /*  81 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 86),
    0x00000048    /*  83 (LREF0-PUSH) */,
    0x0000105f    /*  84 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#integer?.8551c60> */,
    0x0000001e    /*  86 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 94),
    0x0000005e    /*  88 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-server-socket-inet.8551b80> */,
    0x00000048    /*  90 (LREF0-PUSH) */,
    0x0000007a    /*  91 (LREF10-CDR) */,
    0x00003095    /*  92 (TAIL-APPLY 3) */,
    0x00000014    /*  93 (RET) */,
    0x0000003d    /*  94 (LREF0) */,
    0x0000009b    /*  95 (STRINGP) */,
    0x0000001f    /*  96 (BT) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 88),
    0x00000006    /*  98 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[307])) /* "inet socket requires integer port number or string service name, or a list of them, but got:" */,
    0x00000048    /* 100 (LREF0-PUSH) */,
    0x00002060    /* 101 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.8550420> */,
    0x00000014    /* 103 (RET) */,
    0x00000049    /* 104 (LREF1-PUSH) */,
    0x0000005d    /* 105 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<sockaddr>.85519e0> */,
    0x00000096    /* 107 (IS-A) */,
    0x0000001e    /* 108 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 116),
    0x0000005e    /* 110 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-server-socket-from-addr.8551980> */,
    0x00000049    /* 112 (LREF1-PUSH) */,
    0x0000003d    /* 113 (LREF0) */,
    0x00003095    /* 114 (TAIL-APPLY 3) */,
    0x00000014    /* 115 (RET) */,
    0x0000100e    /* 116 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 121),
    0x00000049    /* 118 (LREF1-PUSH) */,
    0x0000105f    /* 119 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#integer?.85518c0> */,
    0x0000001e    /* 121 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]) + 129),
    0x0000005e    /* 123 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-server-socket-inet.8552e60> */,
    0x00000049    /* 125 (LREF1-PUSH) */,
    0x0000003d    /* 126 (LREF0) */,
    0x00003095    /* 127 (TAIL-APPLY 3) */,
    0x00000014    /* 128 (RET) */,
    0x00000006    /* 129 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[292])) /* "unsupported protocol:" */,
    0x00000049    /* 131 (LREF1-PUSH) */,
    0x00002060    /* 132 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.8552c80> */,
    0x00000014    /* 134 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[753]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* make-server-socket */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[753]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[19])) /* #<compiled-code make-server-socket@0x7f06080fbc60> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-server-socket.854d8e0> */,
    0x00000014    /*  14 (RET) */,
    /* make-server-socket-from-addr */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00000005    /*   1 (CONSTU) */,
    0x0000000d    /*   2 (PUSH) */,
    0x00000005    /*   3 (CONSTU) */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000005    /*   5 (CONSTU) */,
    0x00004018    /*   6 (PUSH-LOCAL-ENV 4) */,
    0x00000040    /*   7 (LREF3) */,
    0x00000022    /*   8 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 101),
    0x0000100e    /*  10 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 15),
    0x0000004a    /*  12 (LREF2-PUSH) */,
    0x0000105f    /*  13 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.80f4a40> */,
    0x0000001e    /*  15 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 20),
    0x00000004    /*  17 (CONSTF) */,
    0x00000013    /*  18 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 21),
    0x0000003f    /*  20 (LREF2) */,
    0x00001018    /*  21 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  22 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 27),
    0x0000004d    /*  24 (LREF11-PUSH) */,
    0x0000105f    /*  25 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.80f4a40> */,
    0x0000001e    /*  27 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 32),
    0x00000004    /*  29 (CONSTF) */,
    0x00000013    /*  30 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 33),
    0x00000042    /*  32 (LREF11) */,
    0x00001018    /*  33 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  34 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 39),
    0x0000004f    /*  36 (LREF20-PUSH) */,
    0x0000105f    /*  37 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.80f4a40> */,
    0x0000001e    /*  39 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 44),
    0x00005002    /*  41 (CONSTI 5) */,
    0x00000013    /*  42 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 45),
    0x00000044    /*  44 (LREF20) */,
    0x00001018    /*  45 (PUSH-LOCAL-ENV 1) */,
    0x0000200e    /*  46 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 57),
    0x0000100e    /*  48 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 53),
    0x00404047    /*  50 (LREF-PUSH 4 1) */,
    0x0000105f    /*  51 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#address->protocol-family.80ffcc0> */,
    0x00000061    /*  53 (PUSH-GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#SOCK_STREAM.80ffb00> */,
    0x00002062    /*  55 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-socket.80ffd00> */,
    0x00001018    /*  57 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  58 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 63),
    0x0000004f    /*  60 (LREF20-PUSH) */,
    0x0000105f    /*  61 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#procedure?.80ffa60> */,
    0x0000001e    /*  63 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 73),
    0x0000200e    /*  65 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 73),
    0x00000048    /*  67 (LREF0-PUSH) */,
    0x00405047    /*  68 (LREF-PUSH 5 1) */,
    0x00000044    /*  69 (LREF20) */,
    0x00002011    /*  70 (CALL 2) */,
    0x00000013    /*  71 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 73),
    0x00000046    /*  73 (LREF30) */,
    0x0000001e    /*  74 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 88),
    0x0000400e    /*  76 (PRE-CALL 4) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 88),
    0x00000048    /*  78 (LREF0-PUSH) */,
    0x0000005e    /*  79 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#SOL_SOCKET.80ff800> */,
    0x0000005e    /*  81 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#SO_REUSEADDR.80ff7a0> */,
    0x00001007    /*  83 (CONSTI-PUSH 1) */,
    0x0000405f    /*  84 (GREF-CALL 4) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#socket-setsockopt.80ff8a0> */,
    0x00000013    /*  86 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 88),
    0x0000200e    /*  88 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 94),
    0x00000048    /*  90 (LREF0-PUSH) */,
    0x00405047    /*  91 (LREF-PUSH 5 1) */,
    0x0000205f    /*  92 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#socket-bind.80ff740> */,
    0x0000200e    /*  94 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 100),
    0x00000048    /*  96 (LREF0-PUSH) */,
    0x0000004c    /*  97 (LREF10-PUSH) */,
    0x0000205f    /*  98 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#socket-listen.80ff5e0> */,
    0x00000053    /* 100 (LREF0-RET) */,
    0x00000079    /* 101 (LREF3-CDR) */,
    0x00000022    /* 102 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 110),
    0x00000006    /* 104 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[279])) /* "keyword list not even" */,
    0x0000004b    /* 106 (LREF3-PUSH) */,
    0x00002060    /* 107 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.80f4880> */,
    0x00000014    /* 109 (RET) */,
    0x0000100e    /* 110 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 115),
    0x0000006d    /* 112 (LREF3-CAR) */,
    0x00001062    /* 113 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#unwrap-syntax-1.80f47e0> */,
    0x00001018    /* 115 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /* 116 (LREF0) */,
    0x0000002e    /* 117 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :reuse-addr? */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 129)  /*    129 */,
    0x00c0103c    /* 120 (LREF 1 3) */,
    0x00000087    /* 121 (CDDR-PUSH) */,
    0x00c0103c    /* 122 (LREF 1 3) */,
    0x00000083    /* 123 (CADR-PUSH) */,
    0x0000004d    /* 124 (LREF11-PUSH) */,
    0x0000004c    /* 125 (LREF10-PUSH) */,
    0x0000201b    /* 126 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 7),
    0x00000014    /* 128 (RET) */,
    0x0000003d    /* 129 (LREF0) */,
    0x0000002e    /* 130 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :sock-init */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 142)  /*    142 */,
    0x00c0103c    /* 133 (LREF 1 3) */,
    0x00000087    /* 134 (CDDR-PUSH) */,
    0x0000004e    /* 135 (LREF12-PUSH) */,
    0x00c0103c    /* 136 (LREF 1 3) */,
    0x00000083    /* 137 (CADR-PUSH) */,
    0x0000004c    /* 138 (LREF10-PUSH) */,
    0x0000201b    /* 139 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 7),
    0x00000014    /* 141 (RET) */,
    0x0000003d    /* 142 (LREF0) */,
    0x0000002e    /* 143 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :backlog */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 155)  /*    155 */,
    0x00c0103c    /* 146 (LREF 1 3) */,
    0x00000087    /* 147 (CDDR-PUSH) */,
    0x0000004e    /* 148 (LREF12-PUSH) */,
    0x0000004d    /* 149 (LREF11-PUSH) */,
    0x00c0103c    /* 150 (LREF 1 3) */,
    0x00000083    /* 151 (CADR-PUSH) */,
    0x0000201b    /* 152 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 7),
    0x00000014    /* 154 (RET) */,
    0x0000200e    /* 155 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 163),
    0x00000006    /* 157 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[281])) /* "unknown keyword ~S" */,
    0x00c0103c    /* 159 (LREF 1 3) */,
    0x00000069    /* 160 (CAR-PUSH) */,
    0x0000205f    /* 161 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#errorf.80f4320> */,
    0x00c0103c    /* 163 (LREF 1 3) */,
    0x00000087    /* 164 (CDDR-PUSH) */,
    0x0000004e    /* 165 (LREF12-PUSH) */,
    0x0000004d    /* 166 (LREF11-PUSH) */,
    0x0000004c    /* 167 (LREF10-PUSH) */,
    0x0000201b    /* 168 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]) + 7),
    0x00000014    /* 170 (RET) */,
    0x00000014    /* 171 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[940]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* make-server-socket-from-addr */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[940]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[21])) /* #<compiled-code make-server-socket-from-addr@0x7f060828f180> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-server-socket-from-addr.80eb400> */,
    0x00000014    /*  14 (RET) */,
    /* make-server-socket-unix */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00000005    /*   1 (CONSTU) */,
    0x00002018    /*   2 (PUSH-LOCAL-ENV 2) */,
    0x0000003e    /*   3 (LREF1) */,
    0x00000022    /*   4 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]) + 48),
    0x0000100e    /*   6 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]) + 11),
    0x00000048    /*   8 (LREF0-PUSH) */,
    0x0000105f    /*   9 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.7dea300> */,
    0x0000001e    /*  11 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]) + 16),
    0x00005002    /*  13 (CONSTI 5) */,
    0x00000013    /*  14 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]) + 17),
    0x0000003d    /*  16 (LREF0) */,
    0x00001018    /*  17 (PUSH-LOCAL-ENV 1) */,
    0x0000200e    /*  18 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]) + 26),
    0x0000005e    /*  20 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#PF_UNIX.7df41e0> */,
    0x0000005e    /*  22 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#SOCK_STREAM.7df41a0> */,
    0x0000205f    /*  24 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-socket.7df4220> */,
    0x00001018    /*  26 (PUSH-LOCAL-ENV 1) */,
    0x0000200e    /*  27 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]) + 41),
    0x00000048    /*  29 (LREF0-PUSH) */,
    0x0000300e    /*  30 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]) + 39),
    0x0000005e    /*  32 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<sockaddr-un>.7df4080> */,
    0x00000006    /*  34 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :path */,
    0x00403047    /*  36 (LREF-PUSH 3 1) */,
    0x0000305f    /*  37 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make.7df40c0> */,
    0x00002062    /*  39 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#socket-bind.7df4120> */,
    0x0000200e    /*  41 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]) + 47),
    0x00000048    /*  43 (LREF0-PUSH) */,
    0x0000004c    /*  44 (LREF10-PUSH) */,
    0x0000205f    /*  45 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#socket-listen.7df6fe0> */,
    0x00000053    /*  47 (LREF0-RET) */,
    0x00000077    /*  48 (LREF1-CDR) */,
    0x00000022    /*  49 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]) + 57),
    0x00000006    /*  51 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[279])) /* "keyword list not even" */,
    0x00000049    /*  53 (LREF1-PUSH) */,
    0x00002060    /*  54 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.7dea220> */,
    0x00000014    /*  56 (RET) */,
    0x0000100e    /*  57 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]) + 62),
    0x0000006b    /*  59 (LREF1-CAR) */,
    0x00001062    /*  60 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#unwrap-syntax-1.7dea180> */,
    0x00001018    /*  62 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  63 (LREF0) */,
    0x0000002e    /*  64 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :backlog */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]) + 74)  /*     74 */,
    0x00000042    /*  67 (LREF11) */,
    0x00000087    /*  68 (CDDR-PUSH) */,
    0x00000042    /*  69 (LREF11) */,
    0x00000083    /*  70 (CADR-PUSH) */,
    0x0000201b    /*  71 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]) + 3),
    0x00000014    /*  73 (RET) */,
    0x0000200e    /*  74 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]) + 81),
    0x00000006    /*  76 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[281])) /* "unknown keyword ~S" */,
    0x0000006f    /*  78 (LREF11-CAR) */,
    0x00002062    /*  79 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#errorf.7dea000> */,
    0x00000042    /*  81 (LREF11) */,
    0x00000087    /*  82 (CDDR-PUSH) */,
    0x0000004c    /*  83 (LREF10-PUSH) */,
    0x0000201b    /*  84 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]) + 3),
    0x00000014    /*  86 (RET) */,
    0x00000014    /*  87 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1043]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* make-server-socket-unix */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1043]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[23])) /* #<compiled-code make-server-socket-unix@0x7f0607d69600> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-server-socket-unix.7ddf4e0> */,
    0x00000014    /*  14 (RET) */,
    /* make-server-socket-inet */
    0x0000005e    /*   0 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-server-socket-from-addr.89b3a00> */,
    0x0000200e    /*   2 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1058]) + 8),
    0x00000009    /*   4 (CONSTF-PUSH) */,
    0x00000049    /*   5 (LREF1-PUSH) */,
    0x0000205f    /*   6 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-sockaddrs.89b39a0> */,
    0x00000069    /*   8 (CAR-PUSH) */,
    0x0000003d    /*   9 (LREF0) */,
    0x00003095    /*  10 (TAIL-APPLY 3) */,
    0x00000014    /*  11 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1070]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* make-server-socket-inet */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1070]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[25])) /* #<compiled-code make-server-socket-inet@0x7f0608de6000> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-server-socket-inet.89b3b40> */,
    0x00000014    /*  14 (RET) */,
    /* (make-server-socket-inet* G2084) */
    0x00000009    /*   0 (CONSTF-PUSH) */,
    0x00000048    /*   1 (LREF0-PUSH) */,
    0x00002060    /*   2 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-sockaddrs.8a2e2c0> */,
    0x00000014    /*   4 (RET) */,
    /* (make-server-socket-inet* try-bind #f) */
    0x0000003d    /*   0 (LREF0) */,
    0x0000303a    /*   1 (LSET 3 0) */,
    0x0000000b    /*   2 (CONSTF-RET) */,
    /* (make-server-socket-inet* try-bind #f) */
    0x0000005e    /*   0 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-server-socket-from-addr.8a2d780> */,
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x00000046    /*   3 (LREF30) */,
    0x00003095    /*   4 (TAIL-APPLY 3) */,
    0x00000014    /*   5 (RET) */,
    /* (make-server-socket-inet* try-bind) */
    0x00000016    /*   0 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[28])) /* #<compiled-code (make-server-socket-inet* try-bind #f)@0x7f06086b1cc0> */,
    0x0000000d    /*   2 (PUSH) */,
    0x00000016    /*   3 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[29])) /* #<compiled-code (make-server-socket-inet* try-bind #f)@0x7f06086b16c0> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000006    /*   6 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :rewind-before */,
    0x00000006    /*   8 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x00004060    /*  10 (GREF-TAIL-CALL 4) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#with-error-handler.8a2db80> */,
    0x00000014    /*  12 (RET) */,
    /* make-server-socket-inet* */
    0x00000009    /*   0 (CONSTF-PUSH) */,
    0x00001017    /*   1 (LOCAL-ENV 1) */,
    0x000010e7    /*   2 (BOX 1) */,
    0x00001019    /*   3 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1788[557])) /* (#<compiled-code (make-server-socket-inet* try-bind)@0x7f06086b1d20>) */,
    0x0000200e    /*   5 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1112]) + 16),
    0x00000048    /*   7 (LREF0-PUSH) */,
    0x0000200e    /*   8 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1112]) + 14),
    0x00000051    /*  10 (LREF30-PUSH) */,
    0x00000050    /*  11 (LREF21-PUSH) */,
    0x0000205f    /*  12 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#append-map.8a2d120> */,
    0x00002062    /*  14 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#any.8a2d640> */,
    0x00001018    /*  16 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  17 (LREF0) */,
    0x0000001e    /*  18 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1112]) + 22),
    0x00000013    /*  20 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1112]) + 27),
    0x0000100e    /*  22 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1112]) + 27),
    0x000020ea    /*  24 (LREF-UNBOX 2 0) */,
    0x00001062    /*  25 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#raise.8a2e080> */,
    0x00000053    /*  27 (LREF0-RET) */,
    /* %toplevel */
    0x00001019    /*   0 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1788[546])) /* (#<compiled-code (make-server-socket-inet* #:G2084)@0x7f0608682840>) */,
    0x0000000e    /*   2 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1140]) + 6),
    0x0000005f    /*   4 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000001    /*   7 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* make-server-socket-inet* */,
    0x000000ee    /*   9 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1140]) + 14),
    0x00000016    /*  11 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[31])) /* #<compiled-code make-server-socket-inet*@0x7f06086826c0> */,
    0x00000014    /*  13 (RET) */,
    0x00000015    /*  14 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-server-socket-inet*.8a246e0> */,
    0x00000014    /*  16 (RET) */,
    /* (make-server-sockets G2091) */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1157]) + 5),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sockaddr-family.85fe8c0> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000001    /*   6 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* inet6 */,
    0x0000008f    /*   8 (EQ) */,
    0x00000014    /*   9 (RET) */,
    /* (make-server-sockets G2090) */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1167]) + 5),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sockaddr-family.85f8940> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000001    /*   6 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* inet */,
    0x0000008f    /*   8 (EQ) */,
    0x00000014    /*   9 (RET) */,
    /* (make-server-sockets G2089) */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1177]) + 5),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sockaddr-family.85f8940> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000001    /*   6 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* inet */,
    0x0000008f    /*   8 (EQ) */,
    0x00000014    /*   9 (RET) */,
    /* (make-server-sockets G2088) */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1187]) + 5),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sockaddr-family.85fe8c0> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000001    /*   6 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* inet6 */,
    0x0000008f    /*   8 (EQ) */,
    0x00000014    /*   9 (RET) */,
    /* (make-server-sockets bind-failed?) */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1197]) + 5),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<system-error>.8605ac0> */,
    0x00000030    /*   5 (RF) */,
    0x0000200e    /*   6 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1197]) + 13),
    0x00000048    /*   8 (LREF0-PUSH) */,
    0x00000006    /*   9 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* errno */,
    0x0000205f    /*  11 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#~.86052e0> */,
    0x0000000d    /*  13 (PUSH) */,
    0x00c01047    /*  14 (LREF-PUSH 1 3) */,
    0x00000043    /*  15 (LREF12) */,
    0x00002088    /*  16 (LIST 2) */,
    0x0000008c    /*  17 (MEMV) */,
    0x00000014    /*  18 (RET) */,
    /* (make-server-sockets #f) */
    0x0000005e    /*   0 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-server-socket.8643880> */,
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000403c    /*   3 (LREF 4 0) */,
    0x00003095    /*   4 (TAIL-APPLY 3) */,
    0x00000014    /*   5 (RET) */,
    /* (make-server-sockets #f #f) */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1222]) + 5),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000503c    /*   3 (LREF 5 0) */,
    0x0000101c    /*   4 (LOCAL-ENV-CALL 1) */,
    0x0000001e    /*   5 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1222]) + 10),
    0x00000044    /*   7 (LREF20) */,
    0x00001088    /*   8 (LIST 1) */,
    0x00000014    /*   9 (RET) */,
    0x00000048    /*  10 (LREF0-PUSH) */,
    0x00001060    /*  11 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#raise.8633c40> */,
    0x00000014    /*  13 (RET) */,
    /* (make-server-sockets #f #f #f) */
    0x0000005e    /*   0 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-server-socket.8636be0> */,
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000603c    /*   3 (LREF 6 0) */,
    0x00003095    /*   4 (TAIL-APPLY 3) */,
    0x00000014    /*   5 (RET) */,
    /* (make-server-sockets #f #f) */
    0x0000004c    /*   0 (LREF10-PUSH) */,
    0x0000200e    /*   1 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1242]) + 9),
    0x00000016    /*   3 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[40])) /* #<compiled-code (make-server-sockets #f #f #f)@0x7f0608161540> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000048    /*   6 (LREF0-PUSH) */,
    0x0000205f    /*   7 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#filter-map.8633440> */,
    0x00000066    /*   9 (CONS) */,
    0x00000014    /*  10 (RET) */,
    /* (make-server-sockets #f) */
    0x0040403c    /*   0 (LREF 4 1) */,
    0x0000002d    /*   1 (BNUMNEI 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1253]) + 27),
    0x0000200e    /*   3 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1253]) + 17),
    0x00804047    /*   5 (LREF-PUSH 4 2) */,
    0x0000100e    /*   6 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1253]) + 15),
    0x0000100e    /*   8 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1253]) + 13),
    0x00000048    /*  10 (LREF0-PUSH) */,
    0x0000105f    /*  11 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#socket-address.8627300> */,
    0x00001062    /*  13 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sockaddr-port.8627340> */,
    0x00002062    /*  15 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-sockaddrs.86273a0> */,
    0x00001018    /*  17 (PUSH-LOCAL-ENV 1) */,
    0x0000200e    /*  18 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1253]) + 24),
    0x00806047    /*  20 (LREF-PUSH 6 2) */,
    0x00000048    /*  21 (LREF0-PUSH) */,
    0x0000205f    /*  22 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#filter.85f5a00> */,
    0x0000001a    /*  24 (POP-LOCAL-ENV) */,
    0x00000013    /*  25 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1253]) + 33),
    0x0000200e    /*  27 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1253]) + 33),
    0x00405047    /*  29 (LREF-PUSH 5 1) */,
    0x0000004f    /*  30 (LREF20-PUSH) */,
    0x0000205f    /*  31 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#filter.85f5a00> */,
    0x00001018    /*  33 (PUSH-LOCAL-ENV 1) */,
    0x00000016    /*  34 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[39])) /* #<compiled-code (make-server-sockets #f #f)@0x7f0608161900> */,
    0x0000000d    /*  36 (PUSH) */,
    0x00000016    /*  37 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[41])) /* #<compiled-code (make-server-sockets #f #f)@0x7f06081618a0> */,
    0x0000000d    /*  39 (PUSH) */,
    0x00000006    /*  40 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :rewind-before */,
    0x00000006    /*  42 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x00004060    /*  44 (GREF-TAIL-CALL 4) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#with-error-handler.862b6e0> */,
    0x00000014    /*  46 (RET) */,
    /* (make-server-sockets #f) */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1300]) + 5),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000603c    /*   3 (LREF 6 0) */,
    0x0000101c    /*   4 (LOCAL-ENV-CALL 1) */,
    0x0000001e    /*   5 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1300]) + 10),
    0x00000009    /*   7 (CONSTF-PUSH) */,
    0x0080303c    /*   8 (LREF 3 2) */,
    0x000020a3    /*   9 (VALUES-RET 2) */,
    0x00000060    /*  10 (GREF-TAIL-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%reraise.8639d60> */,
    0x00000014    /*  12 (RET) */,
    /* (make-server-sockets #f) */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1313]) + 7),
    0x0000005e    /*   2 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-server-socket.8639280> */,
    0x00000048    /*   4 (LREF0-PUSH) */,
    0x0000603c    /*   5 (LREF 6 0) */,
    0x00003095    /*   6 (TAIL-APPLY 3) */,
    0x00001018    /*   7 (PUSH-LOCAL-ENV 1) */,
    0x00000048    /*   8 (LREF0-PUSH) */,
    0x0040703c    /*   9 (LREF 7 1) */,
    0x0000002d    /*  10 (BNUMNEI 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1313]) + 23),
    0x0000100e    /*  12 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1313]) + 24),
    0x0000100e    /*  14 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1313]) + 19),
    0x00000048    /*  16 (LREF0-PUSH) */,
    0x0000105f    /*  17 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#socket-address.863afc0> */,
    0x00001062    /*  19 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sockaddr-port.8639020> */,
    0x00000013    /*  21 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1313]) + 24),
    0x00000004    /*  23 (CONSTF) */,
    0x000020a3    /*  24 (VALUES-RET 2) */,
    /* make-server-sockets */
    0x00004019    /*   0 (LOCAL-ENV-CLOSURES 4) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1788[595])) /* (#<undef> #<undef> #<undef> #<compiled-code (make-server-sockets bind-failed?)@0x7f0608161de0>) */,
    0x0000300e    /*   2 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 16),
    0x0000100e    /*   4 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 10),
    0x00000006    /*   6 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* gauche */,
    0x0000105f    /*   8 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#find-module.85fe700> */,
    0x0000000d    /*  10 (PUSH) */,
    0x00000006    /*  11 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* EADDRINUSE */,
    0x00000009    /*  13 (CONSTF-PUSH) */,
    0x0000305f    /*  14 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#global-variable-ref.85fe740> */,
    0x000030e8    /*  16 (ENV-SET 3) */,
    0x0000300e    /*  17 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 31),
    0x0000100e    /*  19 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 25),
    0x00000006    /*  21 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* gauche */,
    0x0000105f    /*  23 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#find-module.8601ea0> */,
    0x0000000d    /*  25 (PUSH) */,
    0x00000006    /*  26 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* EADDRNOTAVAIL */,
    0x00000009    /*  28 (CONSTF-PUSH) */,
    0x0000305f    /*  29 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#global-variable-ref.85fe440> */,
    0x000020e8    /*  31 (ENV-SET 2) */,
    0x0000300e    /*  32 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 46),
    0x0000100e    /*  34 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 40),
    0x00000006    /*  36 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* gauche.net */,
    0x0000105f    /*  38 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#find-module.86012e0> */,
    0x0000000d    /*  40 (PUSH) */,
    0x00000006    /*  41 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* <sockaddr-in6> */,
    0x00000009    /*  43 (CONSTF-PUSH) */,
    0x0000305f    /*  44 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#global-variable-ref.8601bc0> */,
    0x000010e8    /*  46 (ENV-SET 1) */,
    0x0000200e    /*  47 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 53),
    0x0000004e    /*  49 (LREF12-PUSH) */,
    0x0000004d    /*  50 (LREF11-PUSH) */,
    0x0000205f    /*  51 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-sockaddrs.863a700> */,
    0x00001018    /*  53 (PUSH-LOCAL-ENV 1) */,
    0x0000200e    /*  54 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 60),
    0x00c03047    /*  56 (LREF-PUSH 3 3) */,
    0x00000048    /*  57 (LREF0-PUSH) */,
    0x0000205f    /*  58 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#filter.85f8680> */,
    0x00001018    /*  60 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  61 (LREF0) */,
    0x00000022    /*  62 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 71),
    0x00000016    /*  64 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[38])) /* #<compiled-code (make-server-sockets #f)@0x7f06081619c0> */,
    0x0000000d    /*  66 (PUSH) */,
    0x0000004c    /*  67 (LREF10-PUSH) */,
    0x00002060    /*  68 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#map.863a4e0> */,
    0x00000014    /*  70 (RET) */,
    0x00c0203c    /*  71 (LREF 2 3) */,
    0x0000001e    /*  72 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 64),
    0x00000045    /*  74 (LREF21) */,
    0x0000001e    /*  75 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 64),
    0x00000016    /*  77 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[42])) /* #<compiled-code (make-server-sockets #f)@0x7f0608161960> */,
    0x0000300f    /*  79 (PUSH-PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 155),
    0x00000009    /*  81 (CONSTF-PUSH) */,
    0x0000200e    /*  82 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 88),
    0x00004047    /*  84 (LREF-PUSH 4 0) */,
    0x0000004c    /*  85 (LREF10-PUSH) */,
    0x0000205f    /*  86 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#filter.85f8680> */,
    0x0000000d    /*  88 (PUSH) */,
    0x00000008    /*  89 (CONSTN-PUSH) */,
    0x00003017    /*  90 (LOCAL-ENV 3) */,
    0x0000003e    /*  91 (LREF1) */,
    0x00000022    /*  92 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 97),
    0x0000003d    /*  94 (LREF0) */,
    0x00000093    /*  95 (REVERSE) */,
    0x00000014    /*  96 (RET) */,
    0x0000006b    /*  97 (LREF1-CAR) */,
    0x00001018    /*  98 (PUSH-LOCAL-ENV 1) */,
    0x00000043    /*  99 (LREF12) */,
    0x0000001e    /* 100 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 120),
    0x0000500e    /* 102 (PRE-CALL 5) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 121),
    0x00404047    /* 104 (LREF-PUSH 4 1) */,
    0x00000006    /* 105 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :host */,
    0x0000100e    /* 107 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 112),
    0x00000048    /* 109 (LREF0-PUSH) */,
    0x0000105f    /* 110 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sockaddr-addr.86376e0> */,
    0x0000000d    /* 112 (PUSH) */,
    0x00000006    /* 113 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :port */,
    0x0000004e    /* 115 (LREF12-PUSH) */,
    0x0000505f    /* 116 (GREF-CALL 5) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make.8637780> */,
    0x00000013    /* 118 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 121),
    0x0000003d    /* 120 (LREF0) */,
    0x00001018    /* 121 (PUSH-LOCAL-ENV 1) */,
    0x0000400e    /* 122 (PRE-CALL 4) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 136),
    0x00000016    /* 124 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[43])) /* #<compiled-code (make-server-sockets #f)@0x7f06081614e0> */,
    0x0000000d    /* 126 (PUSH) */,
    0x00000016    /* 127 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[44])) /* #<compiled-code (make-server-sockets #f)@0x7f0608161480> */,
    0x0000000d    /* 129 (PUSH) */,
    0x00000006    /* 130 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :rewind-before */,
    0x00000006    /* 132 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x0000405f    /* 134 (GREF-CALL 4) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#with-error-handler.8639ce0> */,
    0x0000001a    /* 136 (POP-LOCAL-ENV) */,
    0x0000001a    /* 137 (POP-LOCAL-ENV) */,
    0x00002036    /* 138 (TAIL-RECEIVE 2 0) */,
    0x00000048    /* 139 (LREF0-PUSH) */,
    0x0000007b    /* 140 (LREF11-CDR) */,
    0x0000000d    /* 141 (PUSH) */,
    0x0000003e    /* 142 (LREF1) */,
    0x0000001e    /* 143 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 150),
    0x00000049    /* 145 (LREF1-PUSH) */,
    0x00000041    /* 146 (LREF10) */,
    0x00000066    /* 147 (CONS) */,
    0x00000013    /* 148 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 151),
    0x00000041    /* 150 (LREF10) */,
    0x0000000d    /* 151 (PUSH) */,
    0x0000201b    /* 152 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 91),
    0x00000014    /* 154 (RET) */,
    0x00002063    /* 155 (PUSH-GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#append-map.86437a0> */,
    0x00000014    /* 157 (RET) */,
    0x00000013    /* 158 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 64),
    0x00000014    /* 160 (RET) */,
    0x00000013    /* 161 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]) + 64),
    0x00000014    /* 163 (RET) */,
    /* %toplevel */
    0x00004019    /*   0 (LOCAL-ENV-CLOSURES 4) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1788[587])) /* (#<compiled-code (make-server-sockets #:G2091)@0x7f0608130480> #<compiled-code (make-server-sockets #:G2090)@0x7f0608130300> #<compiled-code (make-server-sockets #:G2089)@0x7f06081301e0> #<compiled-code (make-server-sockets #:G2088)@0x7f0608130180>) */,
    0x0000000e    /*   2 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1502]) + 6),
    0x0000005f    /*   4 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000001    /*   7 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* make-server-sockets */,
    0x000000ee    /*   9 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1502]) + 14),
    0x00000016    /*  11 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[45])) /* #<compiled-code make-server-sockets@0x7f0608130120> */,
    0x00000014    /*  13 (RET) */,
    0x00000015    /*  14 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-server-sockets.85f3180> */,
    0x00000014    /*  16 (RET) */,
    /* (make-sockaddrs G2100) */
    0x0000003d    /*   0 (LREF0) */,
    0x000000e3    /*   1 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* addr */,
    0x00000014    /*   3 (RET) */,
    /* (make-sockaddrs G2099) */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1523]) + 5),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sockaddr-family.89f5ce0> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000001    /*   6 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* inet */,
    0x0000008f    /*   8 (EQ) */,
    0x00000014    /*   9 (RET) */,
    /* (make-sockaddrs G2098) */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1533]) + 5),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sockaddr-family.89f5880> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000001    /*   6 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* inet */,
    0x0000008f    /*   8 (EQ) */,
    0x00000014    /*   9 (RET) */,
    /* (make-sockaddrs #f) */
    0x0000005e    /*   0 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<sockaddr-in>.89fd6a0> */,
    0x00000006    /*   2 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :host */,
    0x00000048    /*   4 (LREF0-PUSH) */,
    0x00000006    /*   5 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :port */,
    0x0000004f    /*   7 (LREF20-PUSH) */,
    0x00005060    /*   8 (GREF-TAIL-CALL 5) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make.89fd6e0> */,
    0x00000014    /*  10 (RET) */,
    /* make-sockaddrs */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 7),
    0x00000001    /*   3 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* tcp */,
    0x00000013    /*   5 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 8),
    0x0000006a    /*   7 (LREF0-CAR) */,
    0x0000000d    /*   8 (PUSH) */,
    0x0000003d    /*   9 (LREF0) */,
    0x00000022    /*  10 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 15),
    0x00000003    /*  12 (CONSTN) */,
    0x00000013    /*  13 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 16),
    0x00000076    /*  15 (LREF0-CDR) */,
    0x00002018    /*  16 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  17 (LREF0) */,
    0x00000022    /*  18 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 22),
    0x00000013    /*  20 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 30),
    0x0000200e    /*  22 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 30),
    0x00000006    /*  24 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[343])) /* "too many arguments for" */,
    0x00000006    /*  26 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1788[799])) /* (lambda (host port :optional (proto 'tcp)) (if ipv6-capable (let* ((socktype (case proto ((tcp) SOCK_STREAM) ((udp) SOCK_DGRAM) (else (error "unsupported protocol:" proto)))) (port (x->string port)) (hints (make-sys-addrinfo :flags AI_PASSIVE :socktype socktype)) (ss (map (cut slot-ref <> 'addr) (sys-getaddrinfo host port hints)))) (if ipv4-preferred (append (filter (^s (eq? (sockaddr-family s) 'inet)) ss) (remove (^s (eq? (sockaddr-family s) 'inet)) ss)) ss)) (let1 port (cond ((number? port) port) ((sys-getservbyname port (symbol->string proto)) => (cut slot-ref <> 'port)) (else (error "couldn't find a port number of service:" port))) (if host (let1 hh (sys-gethostbyname host) (unless hh (error "couldn't find host: " host)) (map (cut make <sockaddr-in> :host <> :port port) (slot-ref hh 'addresses))) (list (make <sockaddr-in> :host :any :port port)))))) */,
    0x0000205f    /*  28 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.9935860> */,
    0x0000005d    /*  30 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#ipv6-capable.89f2460> */,
    0x0000001e    /*  32 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 108),
    0x0000003e    /*  34 (LREF1) */,
    0x0000002e    /*  35 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* tcp */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 42)  /*     42 */,
    0x0000005d    /*  38 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#SOCK_STREAM.89f22e0> */,
    0x00000013    /*  40 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 57),
    0x0000003e    /*  42 (LREF1) */,
    0x0000002e    /*  43 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* udp */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 50)  /*     50 */,
    0x0000005d    /*  46 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#SOCK_DGRAM.89f2200> */,
    0x00000013    /*  48 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 57),
    0x0000200e    /*  50 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 57),
    0x00000006    /*  52 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[292])) /* "unsupported protocol:" */,
    0x00000049    /*  54 (LREF1-PUSH) */,
    0x0000205f    /*  55 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.89f21a0> */,
    0x00001018    /*  57 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  58 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 63),
    0x00000050    /*  60 (LREF21-PUSH) */,
    0x0000105f    /*  61 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#x->string.89f2120> */,
    0x00001018    /*  63 (PUSH-LOCAL-ENV 1) */,
    0x0000400e    /*  64 (PRE-CALL 4) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 75),
    0x00000006    /*  66 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :flags */,
    0x0000005e    /*  68 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#AI_PASSIVE.89f3fc0> */,
    0x00000006    /*  70 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :socktype */,
    0x0000004c    /*  72 (LREF10-PUSH) */,
    0x0000405f    /*  73 (GREF-CALL 4) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-sys-addrinfo.89f20a0> */,
    0x00001018    /*  75 (PUSH-LOCAL-ENV 1) */,
    0x0000200e    /*  76 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 88),
    0x00805047    /*  78 (LREF-PUSH 5 2) */,
    0x0000300e    /*  79 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 86),
    0x00804047    /*  81 (LREF-PUSH 4 2) */,
    0x0000004c    /*  82 (LREF10-PUSH) */,
    0x00000048    /*  83 (LREF0-PUSH) */,
    0x0000305f    /*  84 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-getaddrinfo.89f3600> */,
    0x00002062    /*  86 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#map.89f3f00> */,
    0x00001018    /*  88 (PUSH-LOCAL-ENV 1) */,
    0x0000005d    /*  89 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#ipv4-preferred.89f3440> */,
    0x0000001e    /*  91 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 107),
    0x0000200e    /*  93 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 99),
    0x00406047    /*  95 (LREF-PUSH 6 1) */,
    0x00000048    /*  96 (LREF0-PUSH) */,
    0x0000205f    /*  97 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#filter.89f33e0> */,
    0x0000200f    /*  99 (PUSH-PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 105),
    0x00006047    /* 101 (LREF-PUSH 6 0) */,
    0x00000048    /* 102 (LREF0-PUSH) */,
    0x0000205f    /* 103 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#remove.89f5b80> */,
    0x00002091    /* 105 (APPEND 2) */,
    0x00000014    /* 106 (RET) */,
    0x00000053    /* 107 (LREF0-RET) */,
    0x00000042    /* 108 (LREF11) */,
    0x0000009e    /* 109 (NUMBERP) */,
    0x0000001e    /* 110 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 115),
    0x00000042    /* 112 (LREF11) */,
    0x00000013    /* 113 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 142),
    0x0000200e    /* 115 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 125),
    0x0000004d    /* 117 (LREF11-PUSH) */,
    0x0000100e    /* 118 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 123),
    0x00000049    /* 120 (LREF1-PUSH) */,
    0x0000105f    /* 121 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#symbol->string.89f62c0> */,
    0x00002062    /* 123 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-getservbyname.89f6320> */,
    0x00001018    /* 125 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /* 126 (LREF0) */,
    0x0000001e    /* 127 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 134),
    0x0000003d    /* 129 (LREF0) */,
    0x000000e3    /* 130 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* port */,
    0x00000013    /* 132 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 141),
    0x0000200e    /* 134 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 141),
    0x00000006    /* 136 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[366])) /* "couldn't find a port number of service:" */,
    0x00000050    /* 138 (LREF21-PUSH) */,
    0x0000205f    /* 139 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.89f76c0> */,
    0x0000001a    /* 141 (POP-LOCAL-ENV) */,
    0x00001018    /* 142 (PUSH-LOCAL-ENV 1) */,
    0x0080203c    /* 143 (LREF 2 2) */,
    0x0000001e    /* 144 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 173),
    0x0000100e    /* 146 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 151),
    0x00802047    /* 148 (LREF-PUSH 2 2) */,
    0x0000105f    /* 149 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-gethostbyname.89fa720> */,
    0x00001018    /* 151 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /* 152 (LREF0) */,
    0x0000001e    /* 153 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 157),
    0x00000013    /* 155 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 164),
    0x0000200e    /* 157 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 164),
    0x00000006    /* 159 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[369])) /* "couldn't find host: " */,
    0x00803047    /* 161 (LREF-PUSH 3 2) */,
    0x0000205f    /* 162 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.89fa5e0> */,
    0x00000016    /* 164 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[50])) /* #<compiled-code (make-sockaddrs #f)@0x7f0608d23540> */,
    0x0000000d    /* 166 (PUSH) */,
    0x0000003d    /* 167 (LREF0) */,
    0x000000e3    /* 168 (SLOT-REFC) */,
    SCM_WORD(SCM_UNDEFINED) /* addresses */,
    0x00002063    /* 170 (PUSH-GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#map.89fa2c0> */,
    0x00000014    /* 172 (RET) */,
    0x0000500e    /* 173 (PRE-CALL 5) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]) + 186),
    0x0000005e    /* 175 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<sockaddr-in>.89fd2a0> */,
    0x00000006    /* 177 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :host */,
    0x00000006    /* 179 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :any */,
    0x00000006    /* 181 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :port */,
    0x00000048    /* 183 (LREF0-PUSH) */,
    0x0000505f    /* 184 (GREF-CALL 5) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make.89fd300> */,
    0x00001088    /* 186 (LIST 1) */,
    0x00000014    /* 187 (RET) */,
    /* %toplevel */
    0x00003019    /*   0 (LOCAL-ENV-CLOSURES 3) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1788[657])) /* (#<compiled-code (make-sockaddrs #:G2100)@0x7f0608d23f00> #<compiled-code (make-sockaddrs #:G2099)@0x7f0608d23ea0> #<compiled-code (make-sockaddrs #:G2098)@0x7f0608d23b40>) */,
    0x0000000e    /*   2 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1742]) + 6),
    0x0000005f    /*   4 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000001    /*   7 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* make-sockaddrs */,
    0x000000ee    /*   9 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1742]) + 14),
    0x00000016    /*  11 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[51])) /* #<compiled-code make-sockaddrs@0x7f0608d23960> */,
    0x00000014    /*  13 (RET) */,
    0x00000015    /*  14 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-sockaddrs.89ce4a0> */,
    0x00000014    /*  16 (RET) */,
    /* (call-with-client-socket #f) */
    0x00000041    /*   0 (LREF10) */,
    0x0000001e    /*   1 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1759]) + 13),
    0x0000300e    /*   3 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1759]) + 18),
    0x00803047    /*   5 (LREF-PUSH 3 2) */,
    0x00000006    /*   6 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :buffering */,
    0x0000004c    /*   8 (LREF10-PUSH) */,
    0x0000305f    /*   9 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#socket-input-port.7c88180> */,
    0x00000013    /*  11 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1759]) + 18),
    0x0000100e    /*  13 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1759]) + 18),
    0x00803047    /*  15 (LREF-PUSH 3 2) */,
    0x0000105f    /*  16 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#socket-input-port.7c880c0> */,
    0x0000000d    /*  18 (PUSH) */,
    0x0000003d    /*  19 (LREF0) */,
    0x0000001e    /*  20 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1759]) + 32),
    0x0000300e    /*  22 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1759]) + 37),
    0x00803047    /*  24 (LREF-PUSH 3 2) */,
    0x00000006    /*  25 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :buffering */,
    0x00000048    /*  27 (LREF0-PUSH) */,
    0x0000305f    /*  28 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#socket-output-port.7c88000> */,
    0x00000013    /*  30 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1759]) + 37),
    0x0000100e    /*  32 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1759]) + 37),
    0x00803047    /*  34 (LREF-PUSH 3 2) */,
    0x0000105f    /*  35 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#socket-output-port.7c8cf40> */,
    0x0000000d    /*  37 (PUSH) */,
    0x0040303c    /*  38 (LREF 3 1) */,
    0x00002012    /*  39 (TAIL-CALL 2) */,
    0x00000014    /*  40 (RET) */,
    /* (call-with-client-socket #f) */
    0x00803047    /*   0 (LREF-PUSH 3 2) */,
    0x00001060    /*   1 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#socket-close.7c8cd00> */,
    0x00000014    /*   3 (RET) */,
    /* call-with-client-socket */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00000005    /*   1 (CONSTU) */,
    0x0000000d    /*   2 (PUSH) */,
    0x00000005    /*   3 (CONSTU) */,
    0x00003018    /*   4 (PUSH-LOCAL-ENV 3) */,
    0x0000003f    /*   5 (LREF2) */,
    0x00000022    /*   6 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]) + 45),
    0x0000100e    /*   8 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]) + 13),
    0x00000049    /*  10 (LREF1-PUSH) */,
    0x0000105f    /*  11 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.7c88e20> */,
    0x0000001e    /*  13 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]) + 18),
    0x00000004    /*  15 (CONSTF) */,
    0x00000013    /*  16 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]) + 19),
    0x0000003e    /*  18 (LREF1) */,
    0x00001018    /*  19 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  20 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]) + 25),
    0x0000004c    /*  22 (LREF10-PUSH) */,
    0x0000105f    /*  23 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.7c88e20> */,
    0x0000001e    /*  25 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]) + 30),
    0x00000004    /*  27 (CONSTF) */,
    0x00000013    /*  28 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]) + 31),
    0x00000041    /*  30 (LREF10) */,
    0x00001018    /*  31 (PUSH-LOCAL-ENV 1) */,
    0x00000016    /*  32 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[53])) /* #<compiled-code (call-with-client-socket #f)@0x7f0607486a20> */,
    0x0000000d    /*  34 (PUSH) */,
    0x00000016    /*  35 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[54])) /* #<compiled-code (call-with-client-socket #f)@0x7f06074869c0> */,
    0x0000000d    /*  37 (PUSH) */,
    0x00000006    /*  38 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :source-info */,
    0x00000006    /*  40 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1788[816])) /* ("libnet.scm" 878) */,
    0x00004060    /*  42 (GREF-TAIL-CALL 4) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%unwind-protect.7c88300> */,
    0x00000014    /*  44 (RET) */,
    0x00000078    /*  45 (LREF2-CDR) */,
    0x00000022    /*  46 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]) + 54),
    0x00000006    /*  48 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[279])) /* "keyword list not even" */,
    0x0000004a    /*  50 (LREF2-PUSH) */,
    0x00002060    /*  51 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.7c88d00> */,
    0x00000014    /*  53 (RET) */,
    0x0000100e    /*  54 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]) + 59),
    0x0000006c    /*  56 (LREF2-CAR) */,
    0x00001062    /*  57 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#unwrap-syntax-1.7c88c60> */,
    0x00001018    /*  59 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  60 (LREF0) */,
    0x0000002e    /*  61 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :input-buffering */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]) + 72)  /*     72 */,
    0x00000043    /*  64 (LREF12) */,
    0x00000087    /*  65 (CDDR-PUSH) */,
    0x00000043    /*  66 (LREF12) */,
    0x00000083    /*  67 (CADR-PUSH) */,
    0x0000004c    /*  68 (LREF10-PUSH) */,
    0x0000201b    /*  69 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]) + 5),
    0x00000014    /*  71 (RET) */,
    0x0000003d    /*  72 (LREF0) */,
    0x0000002e    /*  73 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :output-buffering */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]) + 84)  /*     84 */,
    0x00000043    /*  76 (LREF12) */,
    0x00000087    /*  77 (CDDR-PUSH) */,
    0x0000004d    /*  78 (LREF11-PUSH) */,
    0x00000043    /*  79 (LREF12) */,
    0x00000083    /*  80 (CADR-PUSH) */,
    0x0000201b    /*  81 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]) + 5),
    0x00000014    /*  83 (RET) */,
    0x0000200e    /*  84 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]) + 91),
    0x00000006    /*  86 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1787[281])) /* "unknown keyword ~S" */,
    0x00000070    /*  88 (LREF12-CAR) */,
    0x00002062    /*  89 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#errorf.7c88a80> */,
    0x00000043    /*  91 (LREF12) */,
    0x00000087    /*  92 (CDDR-PUSH) */,
    0x0000004d    /*  93 (LREF11-PUSH) */,
    0x0000004c    /*  94 (LREF10-PUSH) */,
    0x0000201b    /*  95 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]) + 5),
    0x00000014    /*  97 (RET) */,
    0x00000014    /*  98 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1903]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.9935ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* call-with-client-socket */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1789[1903]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[55])) /* #<compiled-code call-with-client-socket@0x7f0607486a80> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#call-with-client-socket.7c86680> */,
    0x00000014    /*  14 (RET) */,
  },
  {   /* ScmPair d1788 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[1])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[2])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[3])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[4])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[5])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[6])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[7])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[8])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[9])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[10])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[11])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[12])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[13])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[14])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[15])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[16])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[17])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[18])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[19])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[20])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[21])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[22])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[23])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[24])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[25])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[26])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[27])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[28])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[29])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[30])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[31])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[32])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[33])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[34])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[35])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[36])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[37])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[38])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[39])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[40])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[41])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[42])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[43])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[44])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[45])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[46])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[47])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[48])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[49])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[50])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[51])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[52])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[53])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[54])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[55])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[56])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[57])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[58])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[59])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[60])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[61])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[62])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[63])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[64])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[65])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[66])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[67])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[68])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[69])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[70])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[71])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[72])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[73])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[75])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[76])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[77])},
       { SCM_MAKE_INT(182U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[79])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[80])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[82])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[83])},
       { SCM_OBJ(&scm__rc.d1788[84]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[81]), SCM_OBJ(&scm__rc.d1788[85])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(214U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[88])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[89])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[91])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[92])},
       { SCM_OBJ(&scm__rc.d1788[93]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[90]), SCM_OBJ(&scm__rc.d1788[94])},
       { SCM_MAKE_INT(219U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[96])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[97])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[99])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[100])},
       { SCM_OBJ(&scm__rc.d1788[101]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[98]), SCM_OBJ(&scm__rc.d1788[102])},
       { SCM_MAKE_INT(231U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[104])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[105])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[107])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[108])},
       { SCM_OBJ(&scm__rc.d1788[109]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[106]), SCM_OBJ(&scm__rc.d1788[110])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[112])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[113])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[114])},
       { SCM_MAKE_INT(236U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[116])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[117])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[119])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[120])},
       { SCM_OBJ(&scm__rc.d1788[121]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[118]), SCM_OBJ(&scm__rc.d1788[122])},
       { SCM_MAKE_INT(247U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[124])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[125])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[127])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[128])},
       { SCM_OBJ(&scm__rc.d1788[129]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[126]), SCM_OBJ(&scm__rc.d1788[130])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[132])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[133])},
       { SCM_MAKE_INT(272U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[135])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[136])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[138])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[139])},
       { SCM_OBJ(&scm__rc.d1788[140]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[137]), SCM_OBJ(&scm__rc.d1788[141])},
       { SCM_MAKE_INT(275U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[143])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[144])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[146])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[147])},
       { SCM_OBJ(&scm__rc.d1788[148]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[145]), SCM_OBJ(&scm__rc.d1788[149])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[151])},
       { SCM_MAKE_INT(278U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[153])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[154])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[156])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[157])},
       { SCM_OBJ(&scm__rc.d1788[158]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[155]), SCM_OBJ(&scm__rc.d1788[159])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[161])},
       { SCM_MAKE_INT(281U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[163])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[164])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[166])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[167])},
       { SCM_OBJ(&scm__rc.d1788[168]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[165]), SCM_OBJ(&scm__rc.d1788[169])},
       { SCM_MAKE_INT(284U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[171])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[172])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[174])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[175])},
       { SCM_OBJ(&scm__rc.d1788[176]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[173]), SCM_OBJ(&scm__rc.d1788[177])},
       { SCM_MAKE_INT(287U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[179])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[180])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[182])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[183])},
       { SCM_OBJ(&scm__rc.d1788[184]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[181]), SCM_OBJ(&scm__rc.d1788[185])},
       { SCM_MAKE_INT(290U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[187])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[188])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[190])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[191])},
       { SCM_OBJ(&scm__rc.d1788[192]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[189]), SCM_OBJ(&scm__rc.d1788[193])},
       { SCM_MAKE_INT(293U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[195])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[196])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[198])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[199])},
       { SCM_OBJ(&scm__rc.d1788[200]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[197]), SCM_OBJ(&scm__rc.d1788[201])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[203])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[204])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[205])},
       { SCM_MAKE_INT(296U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[207])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[208])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[210])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[211])},
       { SCM_OBJ(&scm__rc.d1788[212]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[209]), SCM_OBJ(&scm__rc.d1788[213])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[204])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[215])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[216])},
       { SCM_MAKE_INT(300U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[218])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[219])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[221])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[222])},
       { SCM_OBJ(&scm__rc.d1788[223]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[220]), SCM_OBJ(&scm__rc.d1788[224])},
       { SCM_MAKE_INT(304U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[226])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[227])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[229])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[230])},
       { SCM_OBJ(&scm__rc.d1788[231]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[228]), SCM_OBJ(&scm__rc.d1788[232])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[204])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[234])},
       { SCM_MAKE_INT(307U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[236])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[237])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[239])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[240])},
       { SCM_OBJ(&scm__rc.d1788[241]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[238]), SCM_OBJ(&scm__rc.d1788[242])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[204])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[244])},
       { SCM_MAKE_INT(311U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[246])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[247])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[249])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[250])},
       { SCM_OBJ(&scm__rc.d1788[251]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[248]), SCM_OBJ(&scm__rc.d1788[252])},
       { SCM_MAKE_INT(315U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[254])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[255])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[257])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[258])},
       { SCM_OBJ(&scm__rc.d1788[259]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[256]), SCM_OBJ(&scm__rc.d1788[260])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[204])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[262])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[263])},
       { SCM_MAKE_INT(319U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[265])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[266])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[268])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[269])},
       { SCM_OBJ(&scm__rc.d1788[270]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[267]), SCM_OBJ(&scm__rc.d1788[271])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[273])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[274])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[275])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[276])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[277])},
       { SCM_MAKE_INT(324U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[279])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[280])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[282])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[283])},
       { SCM_OBJ(&scm__rc.d1788[284]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[281]), SCM_OBJ(&scm__rc.d1788[285])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[287])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[288])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[289])},
       { SCM_MAKE_INT(332U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[291])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[292])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[294])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[295])},
       { SCM_OBJ(&scm__rc.d1788[296]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[293]), SCM_OBJ(&scm__rc.d1788[297])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[299])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[300])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[301])},
       { SCM_MAKE_INT(336U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[303])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[304])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[306])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[307])},
       { SCM_OBJ(&scm__rc.d1788[308]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[305]), SCM_OBJ(&scm__rc.d1788[309])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[311])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[312])},
       { SCM_MAKE_INT(392U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[314])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[315])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[317])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[318])},
       { SCM_OBJ(&scm__rc.d1788[319]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[316]), SCM_OBJ(&scm__rc.d1788[320])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(443U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[323])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[324])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[326])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[327])},
       { SCM_OBJ(&scm__rc.d1788[328]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[325]), SCM_OBJ(&scm__rc.d1788[329])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[331])},
       { SCM_MAKE_INT(446U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[333])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[334])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[336])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[337])},
       { SCM_OBJ(&scm__rc.d1788[338]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[335]), SCM_OBJ(&scm__rc.d1788[339])},
       { SCM_MAKE_INT(449U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[341])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[342])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[344])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[345])},
       { SCM_OBJ(&scm__rc.d1788[346]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[343]), SCM_OBJ(&scm__rc.d1788[347])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(452U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[350])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[351])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[353])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[354])},
       { SCM_OBJ(&scm__rc.d1788[355]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[352]), SCM_OBJ(&scm__rc.d1788[356])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[358])},
       { SCM_MAKE_INT(455U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[360])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[361])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[363])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[364])},
       { SCM_OBJ(&scm__rc.d1788[365]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[362]), SCM_OBJ(&scm__rc.d1788[366])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[358])},
       { SCM_MAKE_INT(458U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[369])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[370])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[372])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[373])},
       { SCM_OBJ(&scm__rc.d1788[374]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[371]), SCM_OBJ(&scm__rc.d1788[375])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(461U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[378])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[379])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[381])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[382])},
       { SCM_OBJ(&scm__rc.d1788[383]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[380]), SCM_OBJ(&scm__rc.d1788[384])},
       { SCM_MAKE_INT(462U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[386])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[387])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[389])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[390])},
       { SCM_OBJ(&scm__rc.d1788[391]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[388]), SCM_OBJ(&scm__rc.d1788[392])},
       { SCM_MAKE_INT(463U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[394])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[395])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[397])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[398])},
       { SCM_OBJ(&scm__rc.d1788[399]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[396]), SCM_OBJ(&scm__rc.d1788[400])},
       { SCM_MAKE_INT(464U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[402])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[403])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[405])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[406])},
       { SCM_OBJ(&scm__rc.d1788[407]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[404]), SCM_OBJ(&scm__rc.d1788[408])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[410])},
       { SCM_MAKE_INT(608U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[412])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[413])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[415])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[416])},
       { SCM_OBJ(&scm__rc.d1788[417]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[414]), SCM_OBJ(&scm__rc.d1788[418])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(629U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[421])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[422])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[424])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[425])},
       { SCM_OBJ(&scm__rc.d1788[426]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[423]), SCM_OBJ(&scm__rc.d1788[427])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[273])},
       { SCM_MAKE_INT(635U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[430])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[431])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[433])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[434])},
       { SCM_OBJ(&scm__rc.d1788[435]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[432]), SCM_OBJ(&scm__rc.d1788[436])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[358])},
       { SCM_MAKE_INT(639U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[439])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[440])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[442])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[443])},
       { SCM_OBJ(&scm__rc.d1788[444]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[441]), SCM_OBJ(&scm__rc.d1788[445])},
       { SCM_MAKE_INT(0), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[447])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[449])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[447])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[447])},
       { SCM_OBJ(&scm__rc.d1788[452]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[451]), SCM_OBJ(&scm__rc.d1788[453])},
       { SCM_OBJ(&scm__rc.d1788[450]), SCM_OBJ(&scm__rc.d1788[454])},
       { SCM_OBJ(&scm__rc.d1788[448]), SCM_OBJ(&scm__rc.d1788[455])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[456])},
       { SCM_MAKE_INT(655U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[458])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[459])},
       { SCM_OBJ(&scm__rc.d1788[460]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(664U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[463])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[464])},
       { SCM_OBJ(&scm__rc.d1788[465]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_UNDEFINED},
       { SCM_MAKE_INT(673U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[469])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[470])},
       { SCM_OBJ(&scm__rc.d1788[471]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(696U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[474])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[475])},
       { SCM_OBJ(&scm__rc.d1788[476]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(700U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[480])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[481])},
       { SCM_OBJ(&scm__rc.d1788[482]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_FALSE, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[485])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[486])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[487]), SCM_OBJ(&scm__rc.d1788[488])},
       { SCM_OBJ(&scm__rc.d1788[489]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[487]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[491]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[493])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[494]), SCM_OBJ(&scm__rc.d1788[495])},
       { SCM_OBJ(&scm__rc.d1788[496]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[16]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[499])},
       { SCM_MAKE_INT(704U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[501])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[502])},
       { SCM_OBJ(&scm__rc.d1788[503]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(715U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[506])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[507])},
       { SCM_OBJ(&scm__rc.d1788[508]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[485])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[485])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[513])},
       { SCM_OBJ(&scm__rc.d1788[514]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[512]), SCM_OBJ(&scm__rc.d1788[515])},
       { SCM_OBJ(&scm__rc.d1788[511]), SCM_OBJ(&scm__rc.d1788[516])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[517])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[518])},
       { SCM_MAKE_INT(741U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[520])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[521])},
       { SCM_OBJ(&scm__rc.d1788[522]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[515])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[525])},
       { SCM_MAKE_INT(753U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[527])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[528])},
       { SCM_OBJ(&scm__rc.d1788[529]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_UNDEFINED},
       { SCM_MAKE_INT(758U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[533])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[534])},
       { SCM_OBJ(&scm__rc.d1788[535]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[538])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(767U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[541])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[542])},
       { SCM_OBJ(&scm__rc.d1788[543]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[27]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[485])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[547])},
       { SCM_OBJ(&scm__rc.d1788[548]), SCM_OBJ(&scm__rc.d1788[488])},
       { SCM_OBJ(&scm__rc.d1788[549]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[548]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[551]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[553])},
       { SCM_OBJ(&scm__rc.d1788[554]), SCM_OBJ(&scm__rc.d1788[495])},
       { SCM_OBJ(&scm__rc.d1788[555]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[30]), SCM_NIL},
       { SCM_UNDEFINED, SCM_UNDEFINED},
       { SCM_MAKE_INT(761U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[559])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[560])},
       { SCM_OBJ(&scm__rc.d1788[561]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[564])},
       { SCM_MAKE_INT(783U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[566])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[567])},
       { SCM_OBJ(&scm__rc.d1788[568]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[571])},
       { SCM_MAKE_INT(781U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[573])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[574])},
       { SCM_OBJ(&scm__rc.d1788[575]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[578])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[581])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[36]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[35]), SCM_OBJ(&scm__rc.d1788[584])},
       { SCM_OBJ(&scm__rc.d1790[34]), SCM_OBJ(&scm__rc.d1788[585])},
       { SCM_OBJ(&scm__rc.d1790[33]), SCM_OBJ(&scm__rc.d1788[586])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[588])},
       { SCM_OBJ(&scm__rc.d1788[589]), SCM_OBJ(&scm__rc.d1788[488])},
       { SCM_OBJ(&scm__rc.d1788[590]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[37]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[592])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[593])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[594])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[485])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(846U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[598])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[599])},
       { SCM_OBJ(&scm__rc.d1788[600]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_FALSE, SCM_OBJ(&scm__rc.d1788[485])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[603])},
       { SCM_OBJ(&scm__rc.d1788[604]), SCM_OBJ(&scm__rc.d1788[488])},
       { SCM_OBJ(&scm__rc.d1788[605]), SCM_NIL},
       { SCM_FALSE, SCM_OBJ(&scm__rc.d1788[603])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[607])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(811U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[610])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[611])},
       { SCM_OBJ(&scm__rc.d1788[612]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[604]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[615]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(847U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[618])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[619])},
       { SCM_OBJ(&scm__rc.d1788[620]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[596]), SCM_OBJ(&scm__rc.d1788[488])},
       { SCM_OBJ(&scm__rc.d1788[623]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[596]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[625]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[532])},
       { SCM_MAKE_INT(779U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[628])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[629])},
       { SCM_OBJ(&scm__rc.d1788[630]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[633])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(858U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[636])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[637])},
       { SCM_OBJ(&scm__rc.d1788[638]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[641])},
       { SCM_MAKE_INT(860U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[643])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[644])},
       { SCM_OBJ(&scm__rc.d1788[645]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[648])},
       { SCM_MAKE_INT(861U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[650])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[651])},
       { SCM_OBJ(&scm__rc.d1788[652]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[49]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[48]), SCM_OBJ(&scm__rc.d1788[655])},
       { SCM_OBJ(&scm__rc.d1790[47]), SCM_OBJ(&scm__rc.d1788[656])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[658])},
       { SCM_OBJ(&scm__rc.d1788[659]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[660])},
       { SCM_OBJ(&scm__rc.d1788[661]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[662])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[663])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[664])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[658]), SCM_OBJ(&scm__rc.d1788[666])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[668]), SCM_OBJ(&scm__rc.d1788[669])},
       { SCM_OBJ(&scm__sc.d1787[292]), SCM_OBJ(&scm__rc.d1788[358])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[671])},
       { SCM_OBJ(&scm__rc.d1788[672]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[673])},
       { SCM_OBJ(&scm__rc.d1788[674]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[670]), SCM_OBJ(&scm__rc.d1788[675])},
       { SCM_OBJ(&scm__rc.d1788[667]), SCM_OBJ(&scm__rc.d1788[676])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[677])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[678])},
       { SCM_OBJ(&scm__rc.d1788[679]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[680])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[499])},
       { SCM_OBJ(&scm__rc.d1788[682]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[683])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[685])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[686])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[687])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[688])},
       { SCM_OBJ(&scm__rc.d1788[689]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[690])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[151])},
       { SCM_OBJ(&scm__rc.d1788[692]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[693])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[694])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[695])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[697])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[698])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[699])},
       { SCM_OBJ(&scm__rc.d1788[700]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[696]), SCM_OBJ(&scm__rc.d1788[701])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[702])},
       { SCM_OBJ(&scm__rc.d1788[703]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[704])},
       { SCM_OBJ(&scm__rc.d1788[705]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[691]), SCM_OBJ(&scm__rc.d1788[706])},
       { SCM_OBJ(&scm__rc.d1788[684]), SCM_OBJ(&scm__rc.d1788[707])},
       { SCM_OBJ(&scm__rc.d1788[681]), SCM_OBJ(&scm__rc.d1788[708])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[420])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[711])},
       { SCM_OBJ(&scm__rc.d1788[712]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[710]), SCM_OBJ(&scm__rc.d1788[713])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[714])},
       { SCM_OBJ(&scm__rc.d1788[715]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[716])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[717]), SCM_OBJ(&scm__rc.d1788[718])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[719])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[719])},
       { SCM_OBJ(&scm__rc.d1788[721]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[720]), SCM_OBJ(&scm__rc.d1788[722])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[723])},
       { SCM_OBJ(&scm__rc.d1788[724]), SCM_OBJ(&scm__rc.d1788[718])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[725])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[726])},
       { SCM_OBJ(&scm__rc.d1788[727]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[709]), SCM_OBJ(&scm__rc.d1788[728])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[729])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[499])},
       { SCM_OBJ(&scm__rc.d1788[731]), SCM_OBJ(&scm__rc.d1788[499])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[358])},
       { SCM_OBJ(&scm__rc.d1788[733]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[734])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[735])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[499])},
       { SCM_OBJ(&scm__rc.d1788[737]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[738])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[739])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[740])},
       { SCM_OBJ(&scm__rc.d1788[741]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[742])},
       { SCM_OBJ(&scm__rc.d1788[736]), SCM_OBJ(&scm__rc.d1788[743])},
       { SCM_OBJ(&scm__sc.d1787[366]), SCM_OBJ(&scm__rc.d1788[499])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[745])},
       { SCM_OBJ(&scm__rc.d1788[746]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[747])},
       { SCM_OBJ(&scm__rc.d1788[748]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[744]), SCM_OBJ(&scm__rc.d1788[749])},
       { SCM_OBJ(&scm__rc.d1788[732]), SCM_OBJ(&scm__rc.d1788[750])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[751])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[753])},
       { SCM_OBJ(&scm__sc.d1787[369]), SCM_OBJ(&scm__rc.d1788[753])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[755])},
       { SCM_OBJ(&scm__rc.d1788[756]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[757])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[758])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[499])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[760])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[761])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[762])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[763])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[764])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[766])},
       { SCM_OBJ(&scm__rc.d1788[767]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[768])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[769])},
       { SCM_OBJ(&scm__rc.d1788[770]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[765]), SCM_OBJ(&scm__rc.d1788[771])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[772])},
       { SCM_OBJ(&scm__rc.d1788[773]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[759]), SCM_OBJ(&scm__rc.d1788[774])},
       { SCM_OBJ(&scm__rc.d1788[754]), SCM_OBJ(&scm__rc.d1788[775])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[776])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[777])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[760])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[779])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[780])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[781])},
       { SCM_OBJ(&scm__rc.d1788[782]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[783])},
       { SCM_OBJ(&scm__rc.d1788[784]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[778]), SCM_OBJ(&scm__rc.d1788[785])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[786])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[787])},
       { SCM_OBJ(&scm__rc.d1788[788]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[752]), SCM_OBJ(&scm__rc.d1788[789])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[790])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[791])},
       { SCM_OBJ(&scm__rc.d1788[792]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[730]), SCM_OBJ(&scm__rc.d1788[793])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[794])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[795])},
       { SCM_OBJ(&scm__rc.d1788[796]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[665]), SCM_OBJ(&scm__rc.d1788[797])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[798])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[485])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(871U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[802])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[803])},
       { SCM_OBJ(&scm__rc.d1788[804]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(850U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[807])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[808])},
       { SCM_OBJ(&scm__rc.d1788[809]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[485])},
       { SCM_OBJ(&scm__rc.d1788[812]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[813]), SCM_NIL},
       { SCM_MAKE_INT(878U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[815])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[485])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[485])},
       { SCM_OBJ(&scm__rc.d1788[818]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1788[817]), SCM_OBJ(&scm__rc.d1788[819])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[820])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[821])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[822])},
       { SCM_MAKE_INT(876U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1787[87]), SCM_OBJ(&scm__rc.d1788[824])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1788[825])},
       { SCM_OBJ(&scm__rc.d1788[826]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
  },
  {   /* ScmObj d1786 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(8, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(8, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(9, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(8, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(8, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(239, FALSE),
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
  },
};
SCM_DEFINE_GENERIC(Scm_GenericSockAddrName, NULL, NULL);

SCM_DEFINE_GENERIC(Scm_GenericSockAddrFamily, NULL, NULL);

SCM_DEFINE_GENERIC(Scm_GenericSockAddrAddr, NULL, NULL);

SCM_DEFINE_GENERIC(Scm_GenericSockAddrPort, NULL, NULL);


static ScmObj libnetsockaddr_name1792(ScmNextMethod *nm_ SCM_UNUSED, ScmObj *SCM_FP, int SCM_ARGCNT SCM_UNUSED, void *d_ SCM_UNUSED)
{
  ScmObj addr_scm;
  ScmObj addr;
  ScmObj SCM_SUBRARGS[1];
  int SCM_i;
  for (SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  addr_scm = SCM_SUBRARGS[0];
  if (!(addr_scm)) Scm_Error("scheme object required, but got %S", addr_scm);
  addr = (addr_scm);
  {
{
ScmObj SCM_RESULT;

#line 103 "libnet.scm"
((void )(addr));

#line 104 "libnet.scm"
{SCM_RESULT=(SCM_OBJ(&scm__sc.d1787[79]));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

static ScmClass *libnetsockaddr_name1792__SPEC[] = { 
SCM_CLASS_STATIC_PTR(Scm_SockAddrClass), 
};
static SCM_DEFINE_METHOD(libnetsockaddr_name1792__STUB, &Scm_GenericSockAddrName, 1, 0, libnetsockaddr_name1792__SPEC, libnetsockaddr_name1792, NULL);


static ScmObj libnetsockaddr_family1793(ScmNextMethod *nm_ SCM_UNUSED, ScmObj *SCM_FP, int SCM_ARGCNT SCM_UNUSED, void *d_ SCM_UNUSED)
{
  ScmObj addr_scm;
  ScmObj addr;
  ScmObj SCM_SUBRARGS[1];
  int SCM_i;
  for (SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  addr_scm = SCM_SUBRARGS[0];
  if (!(addr_scm)) Scm_Error("scheme object required, but got %S", addr_scm);
  addr = (addr_scm);
  {
{
ScmObj SCM_RESULT;

#line 107 "libnet.scm"
((void )(addr));

#line 108 "libnet.scm"
{SCM_RESULT=(scm__rc.d1786[83]);goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

static ScmClass *libnetsockaddr_family1793__SPEC[] = { 
SCM_CLASS_STATIC_PTR(Scm_SockAddrClass), 
};
static SCM_DEFINE_METHOD(libnetsockaddr_family1793__STUB, &Scm_GenericSockAddrFamily, 1, 0, libnetsockaddr_family1793__SPEC, libnetsockaddr_family1793, NULL);


static ScmObj libnetsockaddr_name1794(ScmNextMethod *nm_ SCM_UNUSED, ScmObj *SCM_FP, int SCM_ARGCNT SCM_UNUSED, void *d_ SCM_UNUSED)
{
  ScmObj addr_scm;
  ScmObj addr;
  ScmObj SCM_SUBRARGS[1];
  int SCM_i;
  for (SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  addr_scm = SCM_SUBRARGS[0];
  if (!(addr_scm)) Scm_Error("scheme object required, but got %S", addr_scm);
  addr = (addr_scm);
  {
{
ScmObj SCM_RESULT;

#line 111 "libnet.scm"
{SCM_RESULT=(
(((((unsigned )((((ScmSockAddr* )(addr)))->addrlen)))>(
sizeof(struct sockaddr )))?(
SCM_MAKE_STR(((((ScmSockAddrUn* )(addr)))->addr).sun_path)):(
SCM_MAKE_STR(""))));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

static ScmClass *libnetsockaddr_name1794__SPEC[] = { 
SCM_CLASS_STATIC_PTR(Scm_SockAddrUnClass), 
};
static SCM_DEFINE_METHOD(libnetsockaddr_name1794__STUB, &Scm_GenericSockAddrName, 1, 0, libnetsockaddr_name1794__SPEC, libnetsockaddr_name1794, NULL);


static ScmObj libnetsockaddr_name1795(ScmNextMethod *nm_ SCM_UNUSED, ScmObj *SCM_FP, int SCM_ARGCNT SCM_UNUSED, void *d_ SCM_UNUSED)
{
  ScmObj addr_scm;
  ScmObj addr;
  ScmObj SCM_SUBRARGS[1];
  int SCM_i;
  for (SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  addr_scm = SCM_SUBRARGS[0];
  if (!(addr_scm)) Scm_Error("scheme object required, but got %S", addr_scm);
  addr = (addr_scm);
  {
{
ScmObj SCM_RESULT;

#line 120 "libnet.scm"
{ScmSockAddrIn* a=((ScmSockAddrIn* )(addr));u_long addr=
ntohl(((a)->addr).sin_addr.s_addr);u_short port=
ntohs(((a)->addr).sin_port);char buf[10];
#line 124 "libnet.scm"
snprintf(buf,10,":%d",port);
{SCM_RESULT=(
Scm_StringAppendC(
SCM_STRING(Scm_InetAddressToString(Scm_MakeIntegerU(addr),AF_INET)),buf,-1,-1));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

static ScmClass *libnetsockaddr_name1795__SPEC[] = { 
SCM_CLASS_STATIC_PTR(Scm_SockAddrInClass), 
};
static SCM_DEFINE_METHOD(libnetsockaddr_name1795__STUB, &Scm_GenericSockAddrName, 1, 0, libnetsockaddr_name1795__SPEC, libnetsockaddr_name1795, NULL);


static ScmObj libnetsockaddr_family1796(ScmNextMethod *nm_ SCM_UNUSED, ScmObj *SCM_FP, int SCM_ARGCNT SCM_UNUSED, void *d_ SCM_UNUSED)
{
  ScmObj addr_scm;
  ScmObj addr;
  ScmObj SCM_SUBRARGS[1];
  int SCM_i;
  for (SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  addr_scm = SCM_SUBRARGS[0];
  if (!(addr_scm)) Scm_Error("scheme object required, but got %S", addr_scm);
  addr = (addr_scm);
  {
{
ScmObj SCM_RESULT;

#line 131 "libnet.scm"
((void )(addr));

#line 132 "libnet.scm"
{SCM_RESULT=(scm__rc.d1786[84]);goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

static ScmClass *libnetsockaddr_family1796__SPEC[] = { 
SCM_CLASS_STATIC_PTR(Scm_SockAddrUnClass), 
};
static SCM_DEFINE_METHOD(libnetsockaddr_family1796__STUB, &Scm_GenericSockAddrFamily, 1, 0, libnetsockaddr_family1796__SPEC, libnetsockaddr_family1796, NULL);


static ScmObj libnetsockaddr_family1797(ScmNextMethod *nm_ SCM_UNUSED, ScmObj *SCM_FP, int SCM_ARGCNT SCM_UNUSED, void *d_ SCM_UNUSED)
{
  ScmObj addr_scm;
  ScmObj addr;
  ScmObj SCM_SUBRARGS[1];
  int SCM_i;
  for (SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  addr_scm = SCM_SUBRARGS[0];
  if (!(addr_scm)) Scm_Error("scheme object required, but got %S", addr_scm);
  addr = (addr_scm);
  {
{
ScmObj SCM_RESULT;

#line 135 "libnet.scm"
((void )(addr));

#line 136 "libnet.scm"
{SCM_RESULT=(scm__rc.d1786[85]);goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

static ScmClass *libnetsockaddr_family1797__SPEC[] = { 
SCM_CLASS_STATIC_PTR(Scm_SockAddrInClass), 
};
static SCM_DEFINE_METHOD(libnetsockaddr_family1797__STUB, &Scm_GenericSockAddrFamily, 1, 0, libnetsockaddr_family1797__SPEC, libnetsockaddr_family1797, NULL);


static ScmObj libnetsockaddr_addr1798(ScmNextMethod *nm_ SCM_UNUSED, ScmObj *SCM_FP, int SCM_ARGCNT SCM_UNUSED, void *d_ SCM_UNUSED)
{
  ScmObj addr_scm;
  ScmObj addr;
  ScmObj SCM_SUBRARGS[1];
  int SCM_i;
  for (SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  addr_scm = SCM_SUBRARGS[0];
  if (!(addr_scm)) Scm_Error("scheme object required, but got %S", addr_scm);
  addr = (addr_scm);
  {
{
u_long SCM_RESULT;

#line 139 "libnet.scm"
{ScmSockAddrIn* a=((ScmSockAddrIn* )(addr));
{SCM_RESULT=(ntohl(((a)->addr).sin_addr.s_addr));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeIntegerU(SCM_RESULT));
}
  }
}

static ScmClass *libnetsockaddr_addr1798__SPEC[] = { 
SCM_CLASS_STATIC_PTR(Scm_SockAddrInClass), 
};
static SCM_DEFINE_METHOD(libnetsockaddr_addr1798__STUB, &Scm_GenericSockAddrAddr, 1, 0, libnetsockaddr_addr1798__SPEC, libnetsockaddr_addr1798, NULL);


static ScmObj libnetsockaddr_port1799(ScmNextMethod *nm_ SCM_UNUSED, ScmObj *SCM_FP, int SCM_ARGCNT SCM_UNUSED, void *d_ SCM_UNUSED)
{
  ScmObj addr_scm;
  ScmObj addr;
  ScmObj SCM_SUBRARGS[1];
  int SCM_i;
  for (SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  addr_scm = SCM_SUBRARGS[0];
  if (!(addr_scm)) Scm_Error("scheme object required, but got %S", addr_scm);
  addr = (addr_scm);
  {
{
u_short SCM_RESULT;

#line 143 "libnet.scm"
{ScmSockAddrIn* a=((ScmSockAddrIn* )(addr));
{SCM_RESULT=(ntohs(((a)->addr).sin_port));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeIntegerU(SCM_RESULT));
}
  }
}

static ScmClass *libnetsockaddr_port1799__SPEC[] = { 
SCM_CLASS_STATIC_PTR(Scm_SockAddrInClass), 
};
static SCM_DEFINE_METHOD(libnetsockaddr_port1799__STUB, &Scm_GenericSockAddrPort, 1, 0, libnetsockaddr_port1799__SPEC, libnetsockaddr_port1799, NULL);

#if defined(HAVE_IPV6)

static ScmObj libnetsockaddr_family1800(ScmNextMethod *nm_ SCM_UNUSED, ScmObj *SCM_FP, int SCM_ARGCNT SCM_UNUSED, void *d_ SCM_UNUSED)
{
  ScmObj addr_scm;
  ScmObj addr;
  ScmObj SCM_SUBRARGS[1];
  int SCM_i;
  for (SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  addr_scm = SCM_SUBRARGS[0];
  if (!(addr_scm)) Scm_Error("scheme object required, but got %S", addr_scm);
  addr = (addr_scm);
  {
{
ScmObj SCM_RESULT;

#line 148 "libnet.scm"
((void )(addr));

#line 149 "libnet.scm"
{SCM_RESULT=(scm__rc.d1801[0]);goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

static ScmClass *libnetsockaddr_family1800__SPEC[] = { 
SCM_CLASS_STATIC_PTR(Scm_SockAddrIn6Class), 
};
static SCM_DEFINE_METHOD(libnetsockaddr_family1800__STUB, &Scm_GenericSockAddrFamily, 1, 0, libnetsockaddr_family1800__SPEC, libnetsockaddr_family1800, NULL);

#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
static ScmObj in6_addr(ScmSockAddrIn6* a){{
#line 152 "libnet.scm"
{uint32_t* p=
((uint32_t* )(((a)->addr).sin6_addr.s6_addr));ScmObj n=
Scm_MakeIntegerFromUI(ntohl(*((p)++)));
{int i=0;int cise__1803=3;for (; (i)<(cise__1803); (i)++){
n=(Scm_LogIor(Scm_Ash(n,32),
Scm_MakeIntegerFromUI(ntohl(*((p)++)))));}}
return (n);}}}
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)

static ScmObj libnetsockaddr_addr1804(ScmNextMethod *nm_ SCM_UNUSED, ScmObj *SCM_FP, int SCM_ARGCNT SCM_UNUSED, void *d_ SCM_UNUSED)
{
  ScmObj addr_scm;
  ScmObj addr;
  ScmObj SCM_SUBRARGS[1];
  int SCM_i;
  for (SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  addr_scm = SCM_SUBRARGS[0];
  if (!(addr_scm)) Scm_Error("scheme object required, but got %S", addr_scm);
  addr = (addr_scm);
  {
{
ScmObj SCM_RESULT;

#line 161 "libnet.scm"
{SCM_RESULT=(in6_addr(((ScmSockAddrIn6* )(addr))));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

static ScmClass *libnetsockaddr_addr1804__SPEC[] = { 
SCM_CLASS_STATIC_PTR(Scm_SockAddrIn6Class), 
};
static SCM_DEFINE_METHOD(libnetsockaddr_addr1804__STUB, &Scm_GenericSockAddrAddr, 1, 0, libnetsockaddr_addr1804__SPEC, libnetsockaddr_addr1804, NULL);

#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)

static ScmObj libnetsockaddr_port1805(ScmNextMethod *nm_ SCM_UNUSED, ScmObj *SCM_FP, int SCM_ARGCNT SCM_UNUSED, void *d_ SCM_UNUSED)
{
  ScmObj addr_scm;
  ScmObj addr;
  ScmObj SCM_SUBRARGS[1];
  int SCM_i;
  for (SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  addr_scm = SCM_SUBRARGS[0];
  if (!(addr_scm)) Scm_Error("scheme object required, but got %S", addr_scm);
  addr = (addr_scm);
  {
{
u_short SCM_RESULT;

#line 164 "libnet.scm"
{ScmSockAddrIn6* a=((ScmSockAddrIn6* )(addr));
{SCM_RESULT=(ntohs(((a)->addr).sin6_port));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeIntegerU(SCM_RESULT));
}
  }
}

static ScmClass *libnetsockaddr_port1805__SPEC[] = { 
SCM_CLASS_STATIC_PTR(Scm_SockAddrIn6Class), 
};
static SCM_DEFINE_METHOD(libnetsockaddr_port1805__STUB, &Scm_GenericSockAddrPort, 1, 0, libnetsockaddr_port1805__SPEC, libnetsockaddr_port1805, NULL);

#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)

static ScmObj libnetsockaddr_name1806(ScmNextMethod *nm_ SCM_UNUSED, ScmObj *SCM_FP, int SCM_ARGCNT SCM_UNUSED, void *d_ SCM_UNUSED)
{
  ScmObj addr_scm;
  ScmObj addr;
  ScmObj SCM_SUBRARGS[1];
  int SCM_i;
  for (SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  addr_scm = SCM_SUBRARGS[0];
  if (!(addr_scm)) Scm_Error("scheme object required, but got %S", addr_scm);
  addr = (addr_scm);
  {
{
ScmObj SCM_RESULT;

#line 168 "libnet.scm"
{ScmSockAddrIn6* a=((ScmSockAddrIn6* )(addr));ScmObj addr=
in6_addr(a);u_short port=
ntohs(((a)->addr).sin6_port);ScmObj out=
Scm_MakeOutputStringPort(TRUE);
Scm_Printf(SCM_PORT(out),"[%A]:%d",
#line 174 "libnet.scm"
Scm_InetAddressToString(addr,AF_INET6),port);
{SCM_RESULT=(Scm_GetOutputString(SCM_PORT(out),0));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

static ScmClass *libnetsockaddr_name1806__SPEC[] = { 
SCM_CLASS_STATIC_PTR(Scm_SockAddrIn6Class), 
};
static SCM_DEFINE_METHOD(libnetsockaddr_name1806__STUB, &Scm_GenericSockAddrName, 1, 0, libnetsockaddr_name1806__SPEC, libnetsockaddr_name1806, NULL);

#endif /* defined(HAVE_IPV6) */

static ScmObj libnetmake_socket(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj domain_scm;
  ScmSmallInt domain;
  ScmObj type_scm;
  ScmSmallInt type;
  ScmObj protocol_scm;
  ScmSmallInt protocol;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("make-socket");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  domain_scm = SCM_SUBRARGS[0];
  if (!SCM_INTP(domain_scm)) Scm_Error("ScmSmallInt required, but got %S", domain_scm);
  domain = SCM_INT_VALUE(domain_scm);
  type_scm = SCM_SUBRARGS[1];
  if (!SCM_INTP(type_scm)) Scm_Error("ScmSmallInt required, but got %S", type_scm);
  type = SCM_INT_VALUE(type_scm);
  if (SCM_ARGCNT > 2+1) {
    protocol_scm = SCM_SUBRARGS[2];
  } else {
    protocol_scm = SCM_MAKE_INT(0);
  }
  if (!SCM_INTP(protocol_scm)) Scm_Error("ScmSmallInt required, but got %S", protocol_scm);
  protocol = SCM_INT_VALUE(protocol_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_MakeSocket(domain,type,protocol));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_address(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("socket-address");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  {
{
ScmObj SCM_RESULT;

#line 215 "libnet.scm"
if ((sock)->address){
{SCM_RESULT=(SCM_OBJ((sock)->address));goto SCM_STUB_RETURN;}} else {
{SCM_RESULT=(SCM_FALSE);goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_status(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("socket-status");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  {
{
ScmObj SCM_RESULT;

#line 220 "libnet.scm"
switch ((sock)->status) {
case SCM_SOCKET_STATUS_NONE : {{SCM_RESULT=(scm__rc.d1786[115]);goto SCM_STUB_RETURN;}
#line 853 "../lib/gauche/cgen/cise.scm"
break;}
#line 222 "libnet.scm"
case SCM_SOCKET_STATUS_BOUND : {{SCM_RESULT=(scm__rc.d1786[116]);goto SCM_STUB_RETURN;}
#line 853 "../lib/gauche/cgen/cise.scm"
break;}
#line 223 "libnet.scm"
case SCM_SOCKET_STATUS_LISTENING : {{SCM_RESULT=(scm__rc.d1786[117]);goto SCM_STUB_RETURN;}
#line 853 "../lib/gauche/cgen/cise.scm"
break;}
#line 224 "libnet.scm"
case SCM_SOCKET_STATUS_CONNECTED : {{SCM_RESULT=(scm__rc.d1786[118]);goto SCM_STUB_RETURN;}
#line 853 "../lib/gauche/cgen/cise.scm"
break;}
#line 225 "libnet.scm"
case SCM_SOCKET_STATUS_SHUTDOWN : {{SCM_RESULT=(scm__rc.d1786[119]);goto SCM_STUB_RETURN;}
#line 853 "../lib/gauche/cgen/cise.scm"
break;}
#line 226 "libnet.scm"
case SCM_SOCKET_STATUS_CLOSED : {{SCM_RESULT=(scm__rc.d1786[120]);goto SCM_STUB_RETURN;}
#line 853 "../lib/gauche/cgen/cise.scm"
break;}default: {
#line 228 "libnet.scm"
Scm_Error("invalid state of socket %S: implementation bugs?",sock);
{SCM_RESULT=(SCM_UNDEFINED);goto SCM_STUB_RETURN;}
#line 853 "../lib/gauche/cgen/cise.scm"
break;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_fd(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("socket-fd");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  {
{
long SCM_RESULT;

#line 232 "libnet.scm"
{SCM_RESULT=(((long )((sock)->fd)));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_input_port(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj buffering_scm = SCM_FALSE;
  ScmObj buffering;
  ScmObj bufferedP_scm = SCM_FALSE;
  ScmObj bufferedP;
  ScmObj SCM_SUBRARGS[1];
  ScmObj SCM_KEYARGS = SCM_ARGREF(SCM_ARGCNT-1);
  SCM_ENTER_SUBR("socket-input-port");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  if (Scm_Length(SCM_KEYARGS) % 2)
    Scm_Error("keyword list not even: %S", SCM_KEYARGS);
  while (!SCM_NULLP(SCM_KEYARGS)) {
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[131])) {
      buffering_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[132])) {
      bufferedP_scm = SCM_CADR(SCM_KEYARGS);
    }
    else Scm_Warn("unknown keyword %S", SCM_CAR(SCM_KEYARGS));
    SCM_KEYARGS = SCM_CDDR(SCM_KEYARGS);
  }
  if (!(buffering_scm)) Scm_Error("scheme object required, but got %S", buffering_scm);
  buffering = (buffering_scm);
  if (!(bufferedP_scm)) Scm_Error("scheme object required, but got %S", bufferedP_scm);
  bufferedP = (bufferedP_scm);
  {
{
ScmObj SCM_RESULT;

#line 238 "libnet.scm"
{int bufmode;
if (!(SCM_FALSEP(bufferedP))){
bufmode=(SCM_PORT_BUFFER_FULL);} else {
#line 242 "libnet.scm"
bufmode=(Scm_BufferingMode(buffering,SCM_PORT_INPUT,SCM_PORT_BUFFER_LINE));}
#line 245 "libnet.scm"
{SCM_RESULT=(Scm_SocketInputPort(sock,bufmode));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_output_port(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj buffering_scm = SCM_FALSE;
  ScmObj buffering;
  ScmObj bufferedP_scm = SCM_FALSE;
  ScmObj bufferedP;
  ScmObj SCM_SUBRARGS[1];
  ScmObj SCM_KEYARGS = SCM_ARGREF(SCM_ARGCNT-1);
  SCM_ENTER_SUBR("socket-output-port");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  if (Scm_Length(SCM_KEYARGS) % 2)
    Scm_Error("keyword list not even: %S", SCM_KEYARGS);
  while (!SCM_NULLP(SCM_KEYARGS)) {
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[131])) {
      buffering_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[132])) {
      bufferedP_scm = SCM_CADR(SCM_KEYARGS);
    }
    else Scm_Warn("unknown keyword %S", SCM_CAR(SCM_KEYARGS));
    SCM_KEYARGS = SCM_CDDR(SCM_KEYARGS);
  }
  if (!(buffering_scm)) Scm_Error("scheme object required, but got %S", buffering_scm);
  buffering = (buffering_scm);
  if (!(bufferedP_scm)) Scm_Error("scheme object required, but got %S", bufferedP_scm);
  bufferedP = (bufferedP_scm);
  {
{
ScmObj SCM_RESULT;

#line 249 "libnet.scm"
{int bufmode;
if (!(SCM_FALSEP(bufferedP))){
bufmode=(SCM_PORT_BUFFER_FULL);} else {
#line 253 "libnet.scm"
bufmode=(Scm_BufferingMode(buffering,SCM_PORT_OUTPUT,SCM_PORT_BUFFER_LINE));}
#line 256 "libnet.scm"
{SCM_RESULT=(Scm_SocketOutputPort(sock,bufmode));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_shutdown(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj how_scm;
  ScmSmallInt how;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("socket-shutdown");
  if (SCM_ARGCNT >= 3
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 2 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  if (SCM_ARGCNT > 1+1) {
    how_scm = SCM_SUBRARGS[1];
  } else {
    how_scm = SCM_MAKE_INT(2U);
  }
  if (!SCM_INTP(how_scm)) Scm_Error("ScmSmallInt required, but got %S", how_scm);
  how = SCM_INT_VALUE(how_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketShutdown(sock,how));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_close(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("socket-close");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketClose(sock));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_bind(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj addr_scm;
  ScmSockAddr* addr;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("socket-bind");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  addr_scm = SCM_SUBRARGS[1];
  if (!Scm_SockAddrP(addr_scm)) Scm_Error("socket address required, but got %S", addr_scm);
  addr = SCM_SOCKADDR(addr_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketBind(sock,addr));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_listen(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj backlog_scm;
  ScmSmallInt backlog;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("socket-listen");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  backlog_scm = SCM_SUBRARGS[1];
  if (!SCM_INTP(backlog_scm)) Scm_Error("ScmSmallInt required, but got %S", backlog_scm);
  backlog = SCM_INT_VALUE(backlog_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketListen(sock,backlog));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_accept(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("socket-accept");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketAccept(sock));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_connect(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj addr_scm;
  ScmSockAddr* addr;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("socket-connect");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  addr_scm = SCM_SUBRARGS[1];
  if (!Scm_SockAddrP(addr_scm)) Scm_Error("socket address required, but got %S", addr_scm);
  addr = SCM_SOCKADDR(addr_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketConnect(sock,addr));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_getsockname(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("socket-getsockname");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketGetSockName(sock));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_getpeername(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("socket-getpeername");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketGetPeerName(sock));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_send(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj msg_scm;
  ScmObj msg;
  ScmObj flags_scm;
  ScmSmallInt flags;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("socket-send");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  msg_scm = SCM_SUBRARGS[1];
  if (!(msg_scm)) Scm_Error("scheme object required, but got %S", msg_scm);
  msg = (msg_scm);
  if (SCM_ARGCNT > 2+1) {
    flags_scm = SCM_SUBRARGS[2];
  } else {
    flags_scm = SCM_MAKE_INT(0);
  }
  if (!SCM_INTP(flags_scm)) Scm_Error("ScmSmallInt required, but got %S", flags_scm);
  flags = SCM_INT_VALUE(flags_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketSend(sock,msg,flags));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_sendto(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj msg_scm;
  ScmObj msg;
  ScmObj to_scm;
  ScmSockAddr* to;
  ScmObj flags_scm;
  ScmSmallInt flags;
  ScmObj SCM_SUBRARGS[4];
  SCM_ENTER_SUBR("socket-sendto");
  if (SCM_ARGCNT >= 5
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 4 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<4; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  msg_scm = SCM_SUBRARGS[1];
  if (!(msg_scm)) Scm_Error("scheme object required, but got %S", msg_scm);
  msg = (msg_scm);
  to_scm = SCM_SUBRARGS[2];
  if (!Scm_SockAddrP(to_scm)) Scm_Error("socket address required, but got %S", to_scm);
  to = SCM_SOCKADDR(to_scm);
  if (SCM_ARGCNT > 3+1) {
    flags_scm = SCM_SUBRARGS[3];
  } else {
    flags_scm = SCM_MAKE_INT(0);
  }
  if (!SCM_INTP(flags_scm)) Scm_Error("ScmSmallInt required, but got %S", flags_scm);
  flags = SCM_INT_VALUE(flags_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketSendTo(sock,msg,to,flags));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_sendmsg(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj msg_scm;
  ScmObj msg;
  ScmObj flags_scm;
  ScmSmallInt flags;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("socket-sendmsg");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  msg_scm = SCM_SUBRARGS[1];
  if (!(msg_scm)) Scm_Error("scheme object required, but got %S", msg_scm);
  msg = (msg_scm);
  if (SCM_ARGCNT > 2+1) {
    flags_scm = SCM_SUBRARGS[2];
  } else {
    flags_scm = SCM_MAKE_INT(0);
  }
  if (!SCM_INTP(flags_scm)) Scm_Error("ScmSmallInt required, but got %S", flags_scm);
  flags = SCM_INT_VALUE(flags_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketSendMsg(sock,msg,flags));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_recv(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj bytes_scm;
  ScmSmallInt bytes;
  ScmObj flags_scm;
  ScmSmallInt flags;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("socket-recv");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  bytes_scm = SCM_SUBRARGS[1];
  if (!SCM_INTP(bytes_scm)) Scm_Error("ScmSmallInt required, but got %S", bytes_scm);
  bytes = SCM_INT_VALUE(bytes_scm);
  if (SCM_ARGCNT > 2+1) {
    flags_scm = SCM_SUBRARGS[2];
  } else {
    flags_scm = SCM_MAKE_INT(0);
  }
  if (!SCM_INTP(flags_scm)) Scm_Error("ScmSmallInt required, but got %S", flags_scm);
  flags = SCM_INT_VALUE(flags_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketRecv(sock,bytes,flags));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_recvX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj buf_scm;
  ScmUVector* buf;
  ScmObj flags_scm;
  ScmSmallInt flags;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("socket-recv!");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  buf_scm = SCM_SUBRARGS[1];
  if (!SCM_UVECTORP(buf_scm)) Scm_Error("<uvector> required, but got %S", buf_scm);
  buf = SCM_UVECTOR(buf_scm);
  if (SCM_ARGCNT > 2+1) {
    flags_scm = SCM_SUBRARGS[2];
  } else {
    flags_scm = SCM_MAKE_INT(0);
  }
  if (!SCM_INTP(flags_scm)) Scm_Error("ScmSmallInt required, but got %S", flags_scm);
  flags = SCM_INT_VALUE(flags_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketRecvX(sock,buf,flags));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_recvfrom(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj bytes_scm;
  ScmSmallInt bytes;
  ScmObj flags_scm;
  ScmSmallInt flags;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("socket-recvfrom");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  bytes_scm = SCM_SUBRARGS[1];
  if (!SCM_INTP(bytes_scm)) Scm_Error("ScmSmallInt required, but got %S", bytes_scm);
  bytes = SCM_INT_VALUE(bytes_scm);
  if (SCM_ARGCNT > 2+1) {
    flags_scm = SCM_SUBRARGS[2];
  } else {
    flags_scm = SCM_MAKE_INT(0);
  }
  if (!SCM_INTP(flags_scm)) Scm_Error("ScmSmallInt required, but got %S", flags_scm);
  flags = SCM_INT_VALUE(flags_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketRecvFrom(sock,bytes,flags));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_recvfromX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj buf_scm;
  ScmUVector* buf;
  ScmObj addrs_scm;
  ScmObj addrs;
  ScmObj flags_scm;
  ScmSmallInt flags;
  ScmObj SCM_SUBRARGS[4];
  SCM_ENTER_SUBR("socket-recvfrom!");
  if (SCM_ARGCNT >= 5
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 4 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<4; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  buf_scm = SCM_SUBRARGS[1];
  if (!SCM_UVECTORP(buf_scm)) Scm_Error("<uvector> required, but got %S", buf_scm);
  buf = SCM_UVECTOR(buf_scm);
  addrs_scm = SCM_SUBRARGS[2];
  if (!(addrs_scm)) Scm_Error("scheme object required, but got %S", addrs_scm);
  addrs = (addrs_scm);
  if (SCM_ARGCNT > 3+1) {
    flags_scm = SCM_SUBRARGS[3];
  } else {
    flags_scm = SCM_MAKE_INT(0);
  }
  if (!SCM_INTP(flags_scm)) Scm_Error("ScmSmallInt required, but got %S", flags_scm);
  flags = SCM_INT_VALUE(flags_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketRecvFromX(sock,buf,addrs,flags));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_buildmsg(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj name_scm;
  ScmSockAddr* name;
  ScmObj iov_scm;
  ScmVector* iov;
  ScmObj control_scm;
  ScmObj control;
  ScmObj flags_scm;
  int flags;
  ScmObj buf_scm;
  ScmUVector* buf;
  ScmObj SCM_SUBRARGS[5];
  SCM_ENTER_SUBR("socket-buildmsg");
  if (SCM_ARGCNT >= 6
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 5 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<5; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  name_scm = SCM_SUBRARGS[0];
  if (!SCM_MAYBE_P(Scm_SockAddrP, name_scm)) Scm_Error("socket address or #f required, but got %S", name_scm);
  name = SCM_MAYBE(SCM_SOCKADDR, name_scm);
  iov_scm = SCM_SUBRARGS[1];
  if (!SCM_MAYBE_P(SCM_VECTORP, iov_scm)) Scm_Error("<vector> or #f required, but got %S", iov_scm);
  iov = SCM_MAYBE(SCM_VECTOR, iov_scm);
  control_scm = SCM_SUBRARGS[2];
  if (!(control_scm)) Scm_Error("scheme object required, but got %S", control_scm);
  control = (control_scm);
  flags_scm = SCM_SUBRARGS[3];
  if (!SCM_INTEGERP(flags_scm)) Scm_Error("int required, but got %S", flags_scm);
  flags = Scm_GetInteger(flags_scm);
  if (SCM_ARGCNT > 4+1) {
    buf_scm = SCM_SUBRARGS[4];
  } else {
    buf_scm = SCM_FALSE;
  }
  if (!SCM_MAYBE_P(SCM_UVECTORP, buf_scm)) Scm_Error("<uvector> or #f required, but got %S", buf_scm);
  buf = SCM_MAYBE(SCM_UVECTOR, buf_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketBuildMsg(name,iov,control,flags,buf));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_setsockopt(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj level_scm;
  ScmSmallInt level;
  ScmObj option_scm;
  ScmSmallInt option;
  ScmObj value_scm;
  ScmObj value;
  ScmObj SCM_SUBRARGS[4];
  SCM_ENTER_SUBR("socket-setsockopt");
  for (int SCM_i=0; SCM_i<4; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  level_scm = SCM_SUBRARGS[1];
  if (!SCM_INTP(level_scm)) Scm_Error("ScmSmallInt required, but got %S", level_scm);
  level = SCM_INT_VALUE(level_scm);
  option_scm = SCM_SUBRARGS[2];
  if (!SCM_INTP(option_scm)) Scm_Error("ScmSmallInt required, but got %S", option_scm);
  option = SCM_INT_VALUE(option_scm);
  value_scm = SCM_SUBRARGS[3];
  if (!(value_scm)) Scm_Error("scheme object required, but got %S", value_scm);
  value = (value_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketSetOpt(sock,level,option,value));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_getsockopt(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj level_scm;
  ScmSmallInt level;
  ScmObj option_scm;
  ScmSmallInt option;
  ScmObj rsize_scm;
  ScmSmallInt rsize;
  ScmObj SCM_SUBRARGS[4];
  SCM_ENTER_SUBR("socket-getsockopt");
  for (int SCM_i=0; SCM_i<4; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  level_scm = SCM_SUBRARGS[1];
  if (!SCM_INTP(level_scm)) Scm_Error("ScmSmallInt required, but got %S", level_scm);
  level = SCM_INT_VALUE(level_scm);
  option_scm = SCM_SUBRARGS[2];
  if (!SCM_INTP(option_scm)) Scm_Error("ScmSmallInt required, but got %S", option_scm);
  option = SCM_INT_VALUE(option_scm);
  rsize_scm = SCM_SUBRARGS[3];
  if (!SCM_INTP(rsize_scm)) Scm_Error("ScmSmallInt required, but got %S", rsize_scm);
  rsize = SCM_INT_VALUE(rsize_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SocketGetOpt(sock,level,option,rsize));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsocket_ioctl(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sock_scm;
  ScmSocket* sock;
  ScmObj request_scm;
  ScmObj request;
  ScmObj data_scm;
  ScmObj data;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("socket-ioctl");
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sock_scm = SCM_SUBRARGS[0];
  if (!SCM_SOCKETP(sock_scm)) Scm_Error("<socket> required, but got %S", sock_scm);
  sock = SCM_SOCKET(sock_scm);
  request_scm = SCM_SUBRARGS[1];
  if (!SCM_INTEGERP(request_scm)) Scm_Error("exact integer required, but got %S", request_scm);
  request = (request_scm);
  data_scm = SCM_SUBRARGS[2];
  if (!(data_scm)) Scm_Error("scheme object required, but got %S", data_scm);
  data = (data_scm);
  {
{
ScmObj SCM_RESULT;

#line 393 "libnet.scm"
{SCM_RESULT=(Scm_SocketIoctl(sock,Scm_GetIntegerU(request),data));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsys_gethostbyname(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj name_scm;
  const char* name;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-gethostbyname");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  name_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(name_scm)) Scm_Error("const char* required, but got %S", name_scm);
  name = SCM_STRING_CONST_CSTRING(name_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_GetHostByName(name));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsys_gethostbyaddr(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj addr_scm;
  const char* addr;
  ScmObj type_scm;
  ScmSmallInt type;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-gethostbyaddr");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  addr_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(addr_scm)) Scm_Error("const char* required, but got %S", addr_scm);
  addr = SCM_STRING_CONST_CSTRING(addr_scm);
  type_scm = SCM_SUBRARGS[1];
  if (!SCM_INTP(type_scm)) Scm_Error("ScmSmallInt required, but got %S", type_scm);
  type = SCM_INT_VALUE(type_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_GetHostByAddr(addr,type));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsys_getprotobyname(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj name_scm;
  const char* name;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-getprotobyname");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  name_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(name_scm)) Scm_Error("const char* required, but got %S", name_scm);
  name = SCM_STRING_CONST_CSTRING(name_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_GetProtoByName(name));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsys_getprotobynumber(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj number_scm;
  ScmSmallInt number;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-getprotobynumber");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  number_scm = SCM_SUBRARGS[0];
  if (!SCM_INTP(number_scm)) Scm_Error("ScmSmallInt required, but got %S", number_scm);
  number = SCM_INT_VALUE(number_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_GetProtoByNumber(number));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsys_getservbyname(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj name_scm;
  const char* name;
  ScmObj proto_scm;
  const char* proto;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-getservbyname");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  name_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(name_scm)) Scm_Error("const char* required, but got %S", name_scm);
  name = SCM_STRING_CONST_CSTRING(name_scm);
  proto_scm = SCM_SUBRARGS[1];
  if (!SCM_STRINGP(proto_scm)) Scm_Error("const char* required, but got %S", proto_scm);
  proto = SCM_STRING_CONST_CSTRING(proto_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_GetServByName(name,proto));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsys_getservbyport(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_scm;
  ScmSmallInt port;
  ScmObj proto_scm;
  const char* proto;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-getservbyport");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_scm = SCM_SUBRARGS[0];
  if (!SCM_INTP(port_scm)) Scm_Error("ScmSmallInt required, but got %S", port_scm);
  port = SCM_INT_VALUE(port_scm);
  proto_scm = SCM_SUBRARGS[1];
  if (!SCM_STRINGP(proto_scm)) Scm_Error("const char* required, but got %S", proto_scm);
  proto = SCM_STRING_CONST_CSTRING(proto_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_GetServByPort(port,proto));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetsys_ntohl(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj x_scm;
  uint32_t x;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-ntohl");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  x_scm = SCM_SUBRARGS[0];
  if (!SCM_UINTEGERP(x_scm)) Scm_Error("uint32_t required, but got %S", x_scm);
  x = Scm_GetIntegerU32(x_scm);
  {
{
uint32_t SCM_RESULT;
{SCM_RESULT=(ntohl(x));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeIntegerU(SCM_RESULT));
}
  }
}


static ScmObj libnetsys_ntohs(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj x_scm;
  uint16_t x;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-ntohs");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  x_scm = SCM_SUBRARGS[0];
  if (!SCM_UINTP(x_scm)) Scm_Error("uint16_t required, but got %S", x_scm);
  x = Scm_GetIntegerU16(x_scm);
  {
{
uint16_t SCM_RESULT;
{SCM_RESULT=(ntohs(x));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeIntegerU(SCM_RESULT));
}
  }
}


static ScmObj libnetsys_htonl(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj x_scm;
  uint32_t x;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-htonl");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  x_scm = SCM_SUBRARGS[0];
  if (!SCM_UINTEGERP(x_scm)) Scm_Error("uint32_t required, but got %S", x_scm);
  x = Scm_GetIntegerU32(x_scm);
  {
{
uint32_t SCM_RESULT;
{SCM_RESULT=(htonl(x));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeIntegerU(SCM_RESULT));
}
  }
}


static ScmObj libnetsys_htons(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj x_scm;
  uint16_t x;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-htons");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  x_scm = SCM_SUBRARGS[0];
  if (!SCM_UINTP(x_scm)) Scm_Error("uint16_t required, but got %S", x_scm);
  x = Scm_GetIntegerU16(x_scm);
  {
{
uint16_t SCM_RESULT;
{SCM_RESULT=(htons(x));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeIntegerU(SCM_RESULT));
}
  }
}


#if defined(HAVE_IPV6)
SCM_DEFINE_BUILTIN_CLASS(Scm_SysAddrinfoClass, NULL, NULL, NULL, addrinfo_allocate, SCM_CLASS_DEFAULT_CPL);

static ScmObj Scm_SysAddrinfoClass_flags_GET(ScmObj OBJARG)
{
  ScmSysAddrinfo* obj = SCM_SYS_ADDRINFO(OBJARG);
  return Scm_MakeInteger(obj->flags);
}

static void Scm_SysAddrinfoClass_flags_SET(ScmObj OBJARG, ScmObj value)
{
  ScmSysAddrinfo* obj = SCM_SYS_ADDRINFO(OBJARG);
  if (!SCM_INTEGERP(value)) Scm_Error("int required, but got %S", value);
  obj->flags = Scm_GetInteger(value);
}

static ScmObj Scm_SysAddrinfoClass_family_GET(ScmObj OBJARG)
{
  ScmSysAddrinfo* obj = SCM_SYS_ADDRINFO(OBJARG);
  return Scm_MakeInteger(obj->family);
}

static void Scm_SysAddrinfoClass_family_SET(ScmObj OBJARG, ScmObj value)
{
  ScmSysAddrinfo* obj = SCM_SYS_ADDRINFO(OBJARG);
  if (!SCM_INTEGERP(value)) Scm_Error("int required, but got %S", value);
  obj->family = Scm_GetInteger(value);
}

static ScmObj Scm_SysAddrinfoClass_socktype_GET(ScmObj OBJARG)
{
  ScmSysAddrinfo* obj = SCM_SYS_ADDRINFO(OBJARG);
  return Scm_MakeInteger(obj->socktype);
}

static void Scm_SysAddrinfoClass_socktype_SET(ScmObj OBJARG, ScmObj value)
{
  ScmSysAddrinfo* obj = SCM_SYS_ADDRINFO(OBJARG);
  if (!SCM_INTEGERP(value)) Scm_Error("int required, but got %S", value);
  obj->socktype = Scm_GetInteger(value);
}

static ScmObj Scm_SysAddrinfoClass_protocol_GET(ScmObj OBJARG)
{
  ScmSysAddrinfo* obj = SCM_SYS_ADDRINFO(OBJARG);
  return Scm_MakeInteger(obj->protocol);
}

static void Scm_SysAddrinfoClass_protocol_SET(ScmObj OBJARG, ScmObj value)
{
  ScmSysAddrinfo* obj = SCM_SYS_ADDRINFO(OBJARG);
  if (!SCM_INTEGERP(value)) Scm_Error("int required, but got %S", value);
  obj->protocol = Scm_GetInteger(value);
}

static ScmObj Scm_SysAddrinfoClass_addrlen_GET(ScmObj OBJARG)
{
  ScmSysAddrinfo* obj = SCM_SYS_ADDRINFO(OBJARG);
  return Scm_MakeIntegerU(obj->addrlen);
}

static void Scm_SysAddrinfoClass_addrlen_SET(ScmObj OBJARG, ScmObj value)
{
  ScmSysAddrinfo* obj = SCM_SYS_ADDRINFO(OBJARG);
  if (!SCM_UINTEGERP(value)) Scm_Error("uint32_t required, but got %S", value);
  obj->addrlen = Scm_GetIntegerU32(value);
}

static ScmObj Scm_SysAddrinfoClass_canonname_GET(ScmObj OBJARG)
{
  ScmSysAddrinfo* obj = SCM_SYS_ADDRINFO(OBJARG);
  return SCM_OBJ_SAFE(obj->canonname);
}

static void Scm_SysAddrinfoClass_canonname_SET(ScmObj OBJARG, ScmObj value)
{
  ScmSysAddrinfo* obj = SCM_SYS_ADDRINFO(OBJARG);
  if (!SCM_STRINGP(value)) Scm_Error("ScmString* required, but got %S", value);
  obj->canonname = SCM_STRING(value);
}

static ScmObj Scm_SysAddrinfoClass_addr_GET(ScmObj OBJARG)
{
  ScmSysAddrinfo* obj = SCM_SYS_ADDRINFO(OBJARG);
  return SCM_OBJ_SAFE(obj->addr);
}

static void Scm_SysAddrinfoClass_addr_SET(ScmObj OBJARG, ScmObj value)
{
  ScmSysAddrinfo* obj = SCM_SYS_ADDRINFO(OBJARG);
  if (!SCM_SOCKADDRP(value)) Scm_Error("ScmSockAddr* required, but got %S", value);
  obj->addr = SCM_SOCKADDR(value);
}

static ScmClassStaticSlotSpec Scm_SysAddrinfoClass__SLOTS[] = {
  SCM_CLASS_SLOT_SPEC("flags", Scm_SysAddrinfoClass_flags_GET, Scm_SysAddrinfoClass_flags_SET),
  SCM_CLASS_SLOT_SPEC("family", Scm_SysAddrinfoClass_family_GET, Scm_SysAddrinfoClass_family_SET),
  SCM_CLASS_SLOT_SPEC("socktype", Scm_SysAddrinfoClass_socktype_GET, Scm_SysAddrinfoClass_socktype_SET),
  SCM_CLASS_SLOT_SPEC("protocol", Scm_SysAddrinfoClass_protocol_GET, Scm_SysAddrinfoClass_protocol_SET),
  SCM_CLASS_SLOT_SPEC("addrlen", Scm_SysAddrinfoClass_addrlen_GET, Scm_SysAddrinfoClass_addrlen_SET),
  SCM_CLASS_SLOT_SPEC("canonname", Scm_SysAddrinfoClass_canonname_GET, Scm_SysAddrinfoClass_canonname_SET),
  SCM_CLASS_SLOT_SPEC("addr", Scm_SysAddrinfoClass_addr_GET, Scm_SysAddrinfoClass_addr_SET),
  SCM_CLASS_SLOT_SPEC_END()
};

#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)

static ScmObj libnetsys_getaddrinfo(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj nodename_scm;
  const char* nodename;
  ScmObj servname_scm;
  const char* servname;
  ScmObj hints_scm;
  ScmObj hints;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("sys-getaddrinfo");
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  nodename_scm = SCM_SUBRARGS[0];
  if (!SCM_MAYBE_P(SCM_STRINGP, nodename_scm)) Scm_Error("const char* or #f required, but got %S", nodename_scm);
  nodename = SCM_MAYBE(SCM_STRING_CONST_CSTRING, nodename_scm);
  servname_scm = SCM_SUBRARGS[1];
  if (!SCM_MAYBE_P(SCM_STRINGP, servname_scm)) Scm_Error("const char* or #f required, but got %S", servname_scm);
  servname = SCM_MAYBE(SCM_STRING_CONST_CSTRING, servname_scm);
  hints_scm = SCM_SUBRARGS[2];
  if (!(hints_scm)) Scm_Error("scheme object required, but got %S", hints_scm);
  hints = (hints_scm);
  {
{
ScmObj SCM_RESULT;

#line 490 "libnet.scm"
{struct addrinfo ai;
if (!((SCM_SYS_ADDRINFO_P(hints))||(SCM_FALSEP(hints)))){{
SCM_TYPE_ERROR(hints,"<sys-addrinfo> or #f");}}
if (!(SCM_FALSEP(hints))){{
memset(&(ai),0,sizeof(ai));
(ai).ai_flags=((SCM_SYS_ADDRINFO(hints))->flags),
(ai).ai_family=((SCM_SYS_ADDRINFO(hints))->family),
(ai).ai_socktype=((SCM_SYS_ADDRINFO(hints))->socktype),
(ai).ai_protocol=((SCM_SYS_ADDRINFO(hints))->protocol);}}
{SCM_RESULT=(Scm_GetAddrinfo(nodename,servname,
((SCM_FALSEP(hints))?(NULL):(&(ai)))));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)

static ScmObj libnetsys_getnameinfo(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj addr_scm;
  ScmSockAddr* addr;
  ScmObj flags_scm;
  ScmSmallInt flags;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-getnameinfo");
  if (SCM_ARGCNT >= 3
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 2 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  addr_scm = SCM_SUBRARGS[0];
  if (!Scm_SockAddrP(addr_scm)) Scm_Error("socket address required, but got %S", addr_scm);
  addr = SCM_SOCKADDR(addr_scm);
  if (SCM_ARGCNT > 1+1) {
    flags_scm = SCM_SUBRARGS[1];
  } else {
    flags_scm = SCM_UNBOUND;
  }
  if (!SCM_INTP(flags_scm)) Scm_Error("ScmSmallInt required, but got %S", flags_scm);
  flags = SCM_INT_VALUE(flags_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_GetNameinfo(addr,flags));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#endif /* defined(HAVE_IPV6) */
static void export_bindings(){{
#line 553 "libnet.scm"
{ScmModule* mod=Scm_CurrentModule();if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[361]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[361]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[362]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[362]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[363]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[363]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[364]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[364]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[365]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[365]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[366]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[366]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[367]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[367]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[368]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[368]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[369]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[369]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[370]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[370]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[371]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[371]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[372]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[372]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[373]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[373]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[374]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[374]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[375]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[375]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[376]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[376]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[377]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[377]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[378]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[378]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[379]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[379]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[380]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[380]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[381]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[381]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[382]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[382]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[383]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[383]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[384]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[384]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[385]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[385]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[386]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[386]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[387]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[387]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[388]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[388]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[389]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[389]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[390]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[390]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[391]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[391]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[392]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[392]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[393]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[393]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[394]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[394]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[395]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[395]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[396]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[396]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[397]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[397]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[398]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[398]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[399]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[399]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[400]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[400]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[401]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[401]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[402]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[402]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[403]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[403]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[404]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[404]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[405]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[405]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[406]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[406]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[407]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[407]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[408]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[408]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[409]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[409]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[410]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[410]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[411]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[411]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[412]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[412]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[413]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[413]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[414]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[414]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[415]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[415]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[416]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[416]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[417]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[417]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[418]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[418]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[419]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[419]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[420]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[420]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[421]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[421]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[422]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[422]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[423]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[423]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[424]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[424]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[425]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[425]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[426]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[426]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[427]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[427]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[428]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[428]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[429]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[429]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[430]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[430]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[431]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[431]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[432]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[432]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[433]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[433]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[434]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[434]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[435]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[435]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[436]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[436]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[437]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[437]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[438]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[438]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[439]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[439]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[440]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[440]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[441]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[441]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[442]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[442]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[443]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[443]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[444]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[444]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[445]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[445]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[446]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[446]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[447]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[447]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[448]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[448]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[449]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[449]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[450]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[450]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[451]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[451]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[452]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[452]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[453]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[453]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[454]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[454]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[455]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[455]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[456]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[456]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[457]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[457]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[458]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[458]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[459]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[459]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[460]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[460]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[461]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[461]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[462]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[462]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[463]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[463]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[464]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[464]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[465]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[465]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[466]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[466]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[467]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[467]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[468]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[468]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[469]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[469]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[470]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[470]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[471]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[471]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[472]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[472]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[473]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[473]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[474]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[474]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[475]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[475]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[476]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[476]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[477]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[477]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[478]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[478]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[479]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[479]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[480]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[480]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[481]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[481]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[482]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[482]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[483]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[483]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[484]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[484]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[485]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[485]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[486]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[486]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[487]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[487]));}}if (!(SCM_UNBOUNDP(Scm_GlobalVariableRef(mod,SCM_SYMBOL(scm__rc.d1786[488]),0)))){{Scm_ExportSymbols(mod,SCM_LIST1(scm__rc.d1786[488]));}}}}}

static ScmObj libnetinet_checksum(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj buf_scm;
  ScmUVector* buf;
  ScmObj size_scm;
  int size;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("inet-checksum");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  buf_scm = SCM_SUBRARGS[0];
  if (!SCM_UVECTORP(buf_scm)) Scm_Error("<uvector> required, but got %S", buf_scm);
  buf = SCM_UVECTOR(buf_scm);
  size_scm = SCM_SUBRARGS[1];
  if (!SCM_INTEGERP(size_scm)) Scm_Error("int required, but got %S", size_scm);
  size = Scm_GetInteger(size_scm);
  {
{
u_int SCM_RESULT;

#line 609 "libnet.scm"
{uint16_t* wp=((uint16_t* )(SCM_UVECTOR_ELEMENTS(buf)));u_long sum=0;uint16_t result;
#line 612 "libnet.scm"
if ((size)>(Scm_UVectorSizeInBytes(buf))){{
Scm_Error("uniform vector buffer too short: %S",buf);}}
for (; (size)>(0); (size)-=(2)){
if ((size)==(1)){{

#if WORDS_BIGENDIAN

#line 617 "libnet.scm"
(sum)+=((*(((u_char* )(wp))))<<(8));
#else /* !WORDS_BIGENDIAN */

#line 618 "libnet.scm"
(sum)+=(*(((u_char* )(wp))));
#endif /* WORDS_BIGENDIAN */

#line 619 "libnet.scm"
break;}}
(sum)+=(*((wp)++));}
sum=(((sum)>>(16))+((sum)&(65535)));
(sum)+=((sum)>>(16));
result=(((uint16_t )(~(sum))));

#if !(WORDS_BIGENDIAN)

#line 625 "libnet.scm"
result=(((result)>>(8))|((result)<<(8)));
#endif /* ! WORDS_BIGENDIAN */

#line 626 "libnet.scm"
{SCM_RESULT=(result);goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeIntegerU(SCM_RESULT));
}
  }
}


static ScmObj libnetinet_string_TOaddress(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj s_scm;
  const char* s;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("inet-string->address");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  s_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(s_scm)) Scm_Error("const char* required, but got %S", s_scm);
  s = SCM_STRING_CONST_CSTRING(s_scm);
  {
ScmObj SCM_RESULT0;
ScmObj SCM_RESULT1;
{

#line 630 "libnet.scm"
{int proto;ScmObj r=Scm_InetStringToAddress(s,&(proto),NULL);
if (SCM_FALSEP(r)){
{SCM_RESULT0=(SCM_FALSE),SCM_RESULT1=(SCM_FALSE);goto SCM_STUB_RETURN;}} else {
{SCM_RESULT0=(r),SCM_RESULT1=(SCM_MAKE_INT(proto));goto SCM_STUB_RETURN;}}}
}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_Values2(SCM_OBJ_SAFE(SCM_RESULT0),SCM_OBJ_SAFE(SCM_RESULT1)));
  }
}


static ScmObj libnetinet_string_TOaddressX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj s_scm;
  const char* s;
  ScmObj buf_scm;
  ScmUVector* buf;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("inet-string->address!");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  s_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(s_scm)) Scm_Error("const char* required, but got %S", s_scm);
  s = SCM_STRING_CONST_CSTRING(s_scm);
  buf_scm = SCM_SUBRARGS[1];
  if (!SCM_UVECTORP(buf_scm)) Scm_Error("<uvector> required, but got %S", buf_scm);
  buf = SCM_UVECTOR(buf_scm);
  {
{
ScmObj SCM_RESULT;

#line 636 "libnet.scm"
{int proto;ScmObj r=Scm_InetStringToAddress(s,&(proto),buf);
if (SCM_FALSEP(r)){{SCM_RESULT=(SCM_FALSE);goto SCM_STUB_RETURN;}} else {{SCM_RESULT=(SCM_MAKE_INT(proto));goto SCM_STUB_RETURN;}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libnetinet_address_TOstring(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj addr_scm;
  ScmObj addr;
  ScmObj proto_scm;
  int proto;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("inet-address->string");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  addr_scm = SCM_SUBRARGS[0];
  if (!(addr_scm)) Scm_Error("scheme object required, but got %S", addr_scm);
  addr = (addr_scm);
  proto_scm = SCM_SUBRARGS[1];
  if (!SCM_INTEGERP(proto_scm)) Scm_Error("int required, but got %S", proto_scm);
  proto = Scm_GetInteger(proto_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_InetAddressToString(addr,proto));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

static ScmCompiledCode *toplevels[] = {
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[0])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[1])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[2])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[3])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[5])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[7])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[9])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[11])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[13])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[18])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[20])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[22])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[24])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[26])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[32])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[46])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[52])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[56])),
 NULL /*termination*/
};
ScmObj SCM_debug_info_const_vector()
{
  static _Bool initialized = FALSE;
  if (!initialized) {
    int i = 0;
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1042];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[80];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1043];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = SCM_OBJ(&scm__sc.d1787[76]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[69];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[70];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[71];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[72];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[73];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[74];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[75];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[77];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[695];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1044];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[530];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[533];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[535];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[467];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[538];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1045];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1046];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1047];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1048];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1049];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1050];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1051];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1052];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[561];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = SCM_OBJ(&scm__sc.d1787[281]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[552];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1053];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[551];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[550];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[545];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[745];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[559];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = SCM_OBJ(&scm__sc.d1787[279]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = SCM_OBJ(&scm__sc.d1787[277]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[554];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[466];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[547];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[175];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[549];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[562];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[563];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[89];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[542];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[7];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1054];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[133];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[468];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = SCM_OBJ(&scm__sc.d1787[285]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[149];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[463];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[572];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[6];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[85];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[5];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[84];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[45];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[568];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = SCM_OBJ(&scm__sc.d1787[292]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[320];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[584];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[588];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[581];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1055];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[626];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[587];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1056];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[40];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[619];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[330];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = SCM_OBJ(&scm__sc.d1787[289]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1057];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1058];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[579];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[601];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = SCM_OBJ(&scm__sc.d1787[286]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1059];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1060];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[48];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[793];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[23];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[3];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[10];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1061];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[42];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[600];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1062];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[606];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1063];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[612];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[611];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[609];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1064];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[618];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[616];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[605];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[43];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[634];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[636];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = SCM_OBJ(&scm__sc.d1787[307]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[632];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[630];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[628];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1065];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[624];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1066];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[49];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1067];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1068];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1069];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1070];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1071];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[650];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[649];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[648];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[25];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[160];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[22];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[27];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[367];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[384];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[651];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[652];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[642];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1072];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1073];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1074];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1075];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1076];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[663];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[666];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1077];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[671];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[751];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[753];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[672];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[662];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[500];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1078];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[692];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[690];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1079];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1080];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[696];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[699];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[689];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[702];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1081];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[716];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[693];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[710];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[714];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1082];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1083];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1084];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[47];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[16];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[223];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[701];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[755];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[750];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[682];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[679];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1085];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[723];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1086];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1087];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1088];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1089];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[106];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1090];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1091];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1092];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[465];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[719];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[46];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[721];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1093];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[685];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[706];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1094];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[676];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[698];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[744];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[50];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[752];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[731];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[41];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[777];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[767];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[763];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[765];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = SCM_OBJ(&scm__sc.d1787[369]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[53];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[760];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[59];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[761];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[762];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = SCM_OBJ(&scm__sc.d1787[366]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1095];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[754];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[757];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[737];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[734];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[749];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[730];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[469];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[748];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[11];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[746];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[740];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = SCM_OBJ(&scm__sc.d1787[343]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[88];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1096];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1097];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1098];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[794];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[795];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[18];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[131];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[796];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[19];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[21];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1099];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1100];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1101];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1102];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[792];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[791];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[790];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[788];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = SCM_OBJ(&scm__sc.d1787[87]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1103];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[1104];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[801]), i++) = scm__rc.d1786[51];
    initialized = TRUE;
  }
  return SCM_OBJ(&scm__rc.d1786[801]);
}
void Scm_Init_libnet() {
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[0])),TRUE); /* gauche.net */
  scm__rc.d1786[0] = SCM_OBJ(Scm_FindModule(SCM_SYMBOL(scm__rc.d1786[1]), SCM_FIND_MODULE_CREATE)); /* module gauche.net */
   Scm_SelectModule(SCM_MODULE(scm__rc.d1786[0]));
  scm__rc.d1786[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[1])),TRUE); /* <socket> */
  scm__rc.d1786[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[2])),TRUE); /* make-socket */
  scm__rc.d1786[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[3])),TRUE); /* PF_UNSPEC */
  scm__rc.d1786[5] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[4])),TRUE); /* PF_UNIX */
  scm__rc.d1786[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[5])),TRUE); /* PF_INET */
  scm__rc.d1786[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[6])),TRUE); /* AF_UNSPEC */
  scm__rc.d1786[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[7])),TRUE); /* AF_UNIX */
  scm__rc.d1786[9] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[8])),TRUE); /* AF_INET */
  scm__rc.d1786[10] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[9])),TRUE); /* SOCK_STREAM */
  scm__rc.d1786[11] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[10])),TRUE); /* SOCK_DGRAM */
  scm__rc.d1786[12] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[11])),TRUE); /* SOCK_RAW */
  scm__rc.d1786[13] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[12])),TRUE); /* SHUT_RD */
  scm__rc.d1786[14] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[13])),TRUE); /* SHUT_WR */
  scm__rc.d1786[15] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[14])),TRUE); /* SHUT_RDWR */
  scm__rc.d1786[16] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[15])),TRUE); /* socket-address */
  scm__rc.d1786[17] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[16])),TRUE); /* socket-status */
  scm__rc.d1786[18] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[17])),TRUE); /* socket-input-port */
  scm__rc.d1786[19] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[18])),TRUE); /* socket-output-port */
  scm__rc.d1786[20] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[19])),TRUE); /* socket-shutdown */
  scm__rc.d1786[21] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[20])),TRUE); /* socket-close */
  scm__rc.d1786[22] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[21])),TRUE); /* socket-bind */
  scm__rc.d1786[23] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[22])),TRUE); /* socket-connect */
  scm__rc.d1786[24] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[23])),TRUE); /* socket-fd */
  scm__rc.d1786[25] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[24])),TRUE); /* socket-listen */
  scm__rc.d1786[26] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[25])),TRUE); /* socket-accept */
  scm__rc.d1786[27] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[26])),TRUE); /* socket-setsockopt */
  scm__rc.d1786[28] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[27])),TRUE); /* socket-getsockopt */
  scm__rc.d1786[29] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[28])),TRUE); /* socket-getsockname */
  scm__rc.d1786[30] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[29])),TRUE); /* socket-getpeername */
  scm__rc.d1786[31] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[30])),TRUE); /* socket-ioctl */
  scm__rc.d1786[32] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[31])),TRUE); /* socket-send */
  scm__rc.d1786[33] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[32])),TRUE); /* socket-sendto */
  scm__rc.d1786[34] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[33])),TRUE); /* socket-sendmsg */
  scm__rc.d1786[35] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[34])),TRUE); /* socket-buildmsg */
  scm__rc.d1786[36] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[35])),TRUE); /* socket-recv */
  scm__rc.d1786[37] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[36])),TRUE); /* socket-recv! */
  scm__rc.d1786[38] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[37])),TRUE); /* socket-recvfrom */
  scm__rc.d1786[39] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[38])),TRUE); /* socket-recvfrom! */
  scm__rc.d1786[40] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[39])),TRUE); /* <sockaddr> */
  scm__rc.d1786[41] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[40])),TRUE); /* <sockaddr-in> */
  scm__rc.d1786[42] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[41])),TRUE); /* <sockaddr-un> */
  scm__rc.d1786[43] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[42])),TRUE); /* make-sockaddrs */
  scm__rc.d1786[44] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[43])),TRUE); /* sockaddr-name */
  scm__rc.d1786[45] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[44])),TRUE); /* sockaddr-family */
  scm__rc.d1786[46] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[45])),TRUE); /* sockaddr-addr */
  scm__rc.d1786[47] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[46])),TRUE); /* sockaddr-port */
  scm__rc.d1786[48] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[47])),TRUE); /* make-client-socket */
  scm__rc.d1786[49] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[48])),TRUE); /* make-server-socket */
  scm__rc.d1786[50] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[49])),TRUE); /* make-server-sockets */
  scm__rc.d1786[51] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[50])),TRUE); /* call-with-client-socket */
  scm__rc.d1786[52] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[51])),TRUE); /* <sys-hostent> */
  scm__rc.d1786[53] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[52])),TRUE); /* sys-gethostbyname */
  scm__rc.d1786[54] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[53])),TRUE); /* sys-gethostbyaddr */
  scm__rc.d1786[55] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[54])),TRUE); /* <sys-protoent> */
  scm__rc.d1786[56] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[55])),TRUE); /* sys-getprotobyname */
  scm__rc.d1786[57] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[56])),TRUE); /* sys-getprotobynumber */
  scm__rc.d1786[58] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[57])),TRUE); /* <sys-servent> */
  scm__rc.d1786[59] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[58])),TRUE); /* sys-getservbyname */
  scm__rc.d1786[60] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[59])),TRUE); /* sys-getservbyport */
  scm__rc.d1786[61] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[60])),TRUE); /* sys-htonl */
  scm__rc.d1786[62] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[61])),TRUE); /* sys-htons */
  scm__rc.d1786[63] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[62])),TRUE); /* sys-ntohl */
  scm__rc.d1786[64] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[63])),TRUE); /* sys-ntohs */
  scm__rc.d1786[65] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[64])),TRUE); /* inet-checksum */
  scm__rc.d1786[66] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[65])),TRUE); /* inet-string->address */
  scm__rc.d1786[67] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[66])),TRUE); /* inet-string->address! */
  scm__rc.d1786[68] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[67])),TRUE); /* inet-address->string */
  scm__rc.d1786[69] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[68])),TRUE); /* connection-self-address */
  scm__rc.d1786[70] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[69])),TRUE); /* connection-peer-address */
  scm__rc.d1786[71] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[70])),TRUE); /* connection-input-port */
  scm__rc.d1786[72] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[71])),TRUE); /* connection-output-port */
  scm__rc.d1786[73] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[72])),TRUE); /* connection-shutdown */
  scm__rc.d1786[74] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[73])),TRUE); /* connection-close */
  scm__rc.d1786[75] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[74])),TRUE); /* connection-address-name */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[1]), scm__rc.d1786[75]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[2]), scm__rc.d1786[74]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[3]), scm__rc.d1786[73]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[4]), scm__rc.d1786[72]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[5]), scm__rc.d1786[71]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[6]), scm__rc.d1786[70]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[7]), scm__rc.d1786[69]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[8]), scm__rc.d1786[68]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[9]), scm__rc.d1786[67]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[10]), scm__rc.d1786[66]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[11]), scm__rc.d1786[65]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[12]), scm__rc.d1786[64]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[13]), scm__rc.d1786[63]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[14]), scm__rc.d1786[62]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[15]), scm__rc.d1786[61]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[16]), scm__rc.d1786[60]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[17]), scm__rc.d1786[59]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[18]), scm__rc.d1786[58]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[19]), scm__rc.d1786[57]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[20]), scm__rc.d1786[56]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[21]), scm__rc.d1786[55]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[22]), scm__rc.d1786[54]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[23]), scm__rc.d1786[53]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[24]), scm__rc.d1786[52]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[25]), scm__rc.d1786[51]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[26]), scm__rc.d1786[50]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[27]), scm__rc.d1786[49]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[28]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[29]), scm__rc.d1786[47]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[30]), scm__rc.d1786[46]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[31]), scm__rc.d1786[45]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[32]), scm__rc.d1786[44]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[33]), scm__rc.d1786[43]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[34]), scm__rc.d1786[42]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[35]), scm__rc.d1786[41]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[36]), scm__rc.d1786[40]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[37]), scm__rc.d1786[39]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[38]), scm__rc.d1786[38]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[39]), scm__rc.d1786[37]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[40]), scm__rc.d1786[36]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[41]), scm__rc.d1786[35]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[42]), scm__rc.d1786[34]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[43]), scm__rc.d1786[33]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[44]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[45]), scm__rc.d1786[31]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[46]), scm__rc.d1786[30]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[47]), scm__rc.d1786[29]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[48]), scm__rc.d1786[28]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[49]), scm__rc.d1786[27]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[50]), scm__rc.d1786[26]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[51]), scm__rc.d1786[25]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[52]), scm__rc.d1786[24]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[53]), scm__rc.d1786[23]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[54]), scm__rc.d1786[22]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[55]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[56]), scm__rc.d1786[20]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[57]), scm__rc.d1786[19]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[58]), scm__rc.d1786[18]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[59]), scm__rc.d1786[17]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[60]), scm__rc.d1786[16]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[61]), scm__rc.d1786[15]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[62]), scm__rc.d1786[14]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[63]), scm__rc.d1786[13]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[64]), scm__rc.d1786[12]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[65]), scm__rc.d1786[11]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[66]), scm__rc.d1786[10]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[67]), scm__rc.d1786[9]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[68]), scm__rc.d1786[8]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[69]), scm__rc.d1786[7]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[70]), scm__rc.d1786[6]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[71]), scm__rc.d1786[5]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[72]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[73]), scm__rc.d1786[3]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[74]), scm__rc.d1786[2]);
  (void)Scm_ExportSymbols(Scm_CurrentModule(), SCM_OBJ(&scm__rc.d1788[74]));
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
   Scm_SelectModule(SCM_MODULE(scm__rc.d1786[0]));
  scm__rc.d1786[77] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[75])),TRUE); /* find-module */
  scm__rc.d1786[78] = SCM_OBJ(Scm_FindModule(SCM_SYMBOL(scm__rc.d1786[1]), SCM_FIND_MODULE_CREATE|SCM_FIND_MODULE_PLACEHOLDING)); /* module gauche.net */
  scm__rc.d1786[76] = Scm_MakeIdentifier(scm__rc.d1786[77], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#find-module */
  scm__rc.d1786[80] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[77])),TRUE); /* %autoload */
  scm__rc.d1786[79] = Scm_MakeIdentifier(scm__rc.d1786[80], SCM_MODULE(Scm_GaucheInternalModule()), SCM_NIL); /* gauche.internal#%autoload */
  scm__rc.d1786[81] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[78])),TRUE); /* %toplevel */
  scm__rc.d1786[82] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[0])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[0]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[0]))->debugInfo = scm__rc.d1786[82];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[0]))[3] = SCM_WORD(scm__rc.d1786[1]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[0]))[5] = SCM_WORD(scm__rc.d1786[76]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[0]))[12] = SCM_WORD(scm__rc.d1786[79]);
  Scm_InitBuiltinGeneric(&Scm_GenericSockAddrName, "sockaddr-name", SCM_MODULE(scm__rc.d1786[0]));
  Scm_InitBuiltinGeneric(&Scm_GenericSockAddrFamily, "sockaddr-family", SCM_MODULE(scm__rc.d1786[0]));
  Scm_InitBuiltinGeneric(&Scm_GenericSockAddrAddr, "sockaddr-addr", SCM_MODULE(scm__rc.d1786[0]));
  Scm_InitBuiltinGeneric(&Scm_GenericSockAddrPort, "sockaddr-port", SCM_MODULE(scm__rc.d1786[0]));
  Scm_InitBuiltinMethod(&libnetsockaddr_name1792__STUB);
  scm__rc.d1786[83] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[79])),TRUE); /* unknown */
  Scm_InitBuiltinMethod(&libnetsockaddr_family1793__STUB);
  Scm_InitBuiltinMethod(&libnetsockaddr_name1794__STUB);
  Scm_InitBuiltinMethod(&libnetsockaddr_name1795__STUB);
  scm__rc.d1786[84] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[80])),TRUE); /* unix */
  Scm_InitBuiltinMethod(&libnetsockaddr_family1796__STUB);
  scm__rc.d1786[85] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[81])),TRUE); /* inet */
  Scm_InitBuiltinMethod(&libnetsockaddr_family1797__STUB);
  Scm_InitBuiltinMethod(&libnetsockaddr_addr1798__STUB);
  Scm_InitBuiltinMethod(&libnetsockaddr_port1799__STUB);
#if defined(HAVE_IPV6)
  scm__rc.d1801[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1802[0])),TRUE); /* inet6 */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  Scm_InitBuiltinMethod(&libnetsockaddr_family1800__STUB);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  Scm_InitBuiltinMethod(&libnetsockaddr_addr1804__STUB);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  Scm_InitBuiltinMethod(&libnetsockaddr_port1805__STUB);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  Scm_InitBuiltinMethod(&libnetsockaddr_name1806__STUB);
#endif /* defined(HAVE_IPV6) */
  scm__rc.d1786[86] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[82])),TRUE); /* domain */
  scm__rc.d1786[87] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[83])),TRUE); /* type */
  scm__rc.d1786[88] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[84]))); /* :optional */
  scm__rc.d1786[89] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[85])),TRUE); /* protocol */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[75]), scm__rc.d1786[89]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[76]), scm__rc.d1786[88]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[77]), scm__rc.d1786[87]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[78]), scm__rc.d1786[86]);
  scm__rc.d1786[90] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[86])),TRUE); /* source-info */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[81]), scm__rc.d1786[90]);
  scm__rc.d1786[91] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[88])),TRUE); /* bind-info */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[82]), scm__rc.d1786[3]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[83]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[84]), scm__rc.d1786[91]);
  scm__rc.d1786[92] = Scm_MakeExtendedPair(scm__rc.d1786[3], SCM_OBJ(&scm__rc.d1788[78]), SCM_OBJ(&scm__rc.d1788[86]));
  scm__rc.d1786[93] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[89])),TRUE); /* <fixnum> */
  scm__rc.d1786[94] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[90])),TRUE); /* * */
  scm__rc.d1786[95] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[91])),TRUE); /* -> */
  scm__rc.d1786[96] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[92])),TRUE); /* <top> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[97]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[97]))[4] = scm__rc.d1786[93];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[97]))[5] = scm__rc.d1786[93];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[97]))[6] = scm__rc.d1786[94];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[97]))[7] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[97]))[8] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("make-socket")), SCM_OBJ(&libnetmake_socket__STUB), 0);
  libnetmake_socket__STUB.common.info = scm__rc.d1786[92];
  libnetmake_socket__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[97]);
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1786[4]), Scm_MakeInteger(PF_UNSPEC), SCM_BINDING_CONST);

  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1786[5]), Scm_MakeInteger(PF_UNIX), SCM_BINDING_CONST);

  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1786[6]), Scm_MakeInteger(PF_INET), SCM_BINDING_CONST);

  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1786[7]), Scm_MakeInteger(AF_UNSPEC), SCM_BINDING_CONST);

  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1786[8]), Scm_MakeInteger(AF_UNIX), SCM_BINDING_CONST);

  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1786[9]), Scm_MakeInteger(AF_INET), SCM_BINDING_CONST);

  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1786[10]), Scm_MakeInteger(SOCK_STREAM), SCM_BINDING_CONST);

  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1786[11]), Scm_MakeInteger(SOCK_DGRAM), SCM_BINDING_CONST);

  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1786[12]), Scm_MakeInteger(SOCK_RAW), SCM_BINDING_CONST);

#if defined(MSG_CTRUNC)
  scm__rc.d1807[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1808[0])),TRUE); /* MSG_CTRUNC */
#endif /* defined(MSG_CTRUNC) */
#if defined(MSG_CTRUNC)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1807[0]), Scm_MakeInteger(MSG_CTRUNC), SCM_BINDING_CONST);

#endif /* defined(MSG_CTRUNC) */
#if defined(MSG_DONTROUTE)
  scm__rc.d1809[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1810[0])),TRUE); /* MSG_DONTROUTE */
#endif /* defined(MSG_DONTROUTE) */
#if defined(MSG_DONTROUTE)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1809[0]), Scm_MakeInteger(MSG_DONTROUTE), SCM_BINDING_CONST);

#endif /* defined(MSG_DONTROUTE) */
#if defined(MSG_EOR)
  scm__rc.d1811[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1812[0])),TRUE); /* MSG_EOR */
#endif /* defined(MSG_EOR) */
#if defined(MSG_EOR)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1811[0]), Scm_MakeInteger(MSG_EOR), SCM_BINDING_CONST);

#endif /* defined(MSG_EOR) */
#if defined(MSG_OOB)
  scm__rc.d1813[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1814[0])),TRUE); /* MSG_OOB */
#endif /* defined(MSG_OOB) */
#if defined(MSG_OOB)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1813[0]), Scm_MakeInteger(MSG_OOB), SCM_BINDING_CONST);

#endif /* defined(MSG_OOB) */
#if defined(MSG_PEEK)
  scm__rc.d1815[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1816[0])),TRUE); /* MSG_PEEK */
#endif /* defined(MSG_PEEK) */
#if defined(MSG_PEEK)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1815[0]), Scm_MakeInteger(MSG_PEEK), SCM_BINDING_CONST);

#endif /* defined(MSG_PEEK) */
#if defined(MSG_TRUNC)
  scm__rc.d1817[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1818[0])),TRUE); /* MSG_TRUNC */
#endif /* defined(MSG_TRUNC) */
#if defined(MSG_TRUNC)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1817[0]), Scm_MakeInteger(MSG_TRUNC), SCM_BINDING_CONST);

#endif /* defined(MSG_TRUNC) */
#if defined(MSG_WAITALL)
  scm__rc.d1819[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1820[0])),TRUE); /* MSG_WAITALL */
#endif /* defined(MSG_WAITALL) */
#if defined(MSG_WAITALL)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1819[0]), Scm_MakeInteger(MSG_WAITALL), SCM_BINDING_CONST);

#endif /* defined(MSG_WAITALL) */
#if defined(IPPROTO_IP)
  scm__rc.d1821[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1822[0])),TRUE); /* IPPROTO_IP */
#endif /* defined(IPPROTO_IP) */
#if defined(IPPROTO_IP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1821[0]), Scm_MakeInteger(IPPROTO_IP), SCM_BINDING_CONST);

#endif /* defined(IPPROTO_IP) */
#if defined(IPPROTO_ICMP)
  scm__rc.d1823[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1824[0])),TRUE); /* IPPROTO_ICMP */
#endif /* defined(IPPROTO_ICMP) */
#if defined(IPPROTO_ICMP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1823[0]), Scm_MakeInteger(IPPROTO_ICMP), SCM_BINDING_CONST);

#endif /* defined(IPPROTO_ICMP) */
#if defined(IPPROTO_ICMPV6)
  scm__rc.d1825[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1826[0])),TRUE); /* IPPROTO_ICMPV6 */
#endif /* defined(IPPROTO_ICMPV6) */
#if defined(IPPROTO_ICMPV6)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1825[0]), Scm_MakeInteger(IPPROTO_ICMPV6), SCM_BINDING_CONST);

#endif /* defined(IPPROTO_ICMPV6) */
#if defined(IPPROTO_TCP)
  scm__rc.d1827[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1828[0])),TRUE); /* IPPROTO_TCP */
#endif /* defined(IPPROTO_TCP) */
#if defined(IPPROTO_TCP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1827[0]), Scm_MakeInteger(IPPROTO_TCP), SCM_BINDING_CONST);

#endif /* defined(IPPROTO_TCP) */
#if defined(IPPROTO_UDP)
  scm__rc.d1829[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1830[0])),TRUE); /* IPPROTO_UDP */
#endif /* defined(IPPROTO_UDP) */
#if defined(IPPROTO_UDP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1829[0]), Scm_MakeInteger(IPPROTO_UDP), SCM_BINDING_CONST);

#endif /* defined(IPPROTO_UDP) */
#if defined(SOMAXCONN)
  scm__rc.d1831[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1832[0])),TRUE); /* SOMAXCONN */
#endif /* defined(SOMAXCONN) */
#if defined(SOMAXCONN)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1831[0]), Scm_MakeInteger(SOMAXCONN), SCM_BINDING_CONST);

#endif /* defined(SOMAXCONN) */
  scm__rc.d1786[106] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[93])),TRUE); /* sock */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[87]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[90]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[91]), scm__rc.d1786[16]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[92]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[93]), scm__rc.d1786[91]);
  scm__rc.d1786[107] = Scm_MakeExtendedPair(scm__rc.d1786[16], SCM_OBJ(&scm__rc.d1788[87]), SCM_OBJ(&scm__rc.d1788[95]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[108]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[108]))[4] = scm__rc.d1786[2];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[108]))[5] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[108]))[6] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-address")), SCM_OBJ(&libnetsocket_address__STUB), 0);
  libnetsocket_address__STUB.common.info = scm__rc.d1786[107];
  libnetsocket_address__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[108]);
  scm__rc.d1786[115] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[94])),TRUE); /* none */
  scm__rc.d1786[116] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[95])),TRUE); /* bound */
  scm__rc.d1786[117] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[96])),TRUE); /* listening */
  scm__rc.d1786[118] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[97])),TRUE); /* connected */
  scm__rc.d1786[119] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[98])),TRUE); /* shutdown */
  scm__rc.d1786[120] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[99])),TRUE); /* closed */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[98]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[99]), scm__rc.d1786[17]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[100]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[101]), scm__rc.d1786[91]);
  scm__rc.d1786[121] = Scm_MakeExtendedPair(scm__rc.d1786[17], SCM_OBJ(&scm__rc.d1788[87]), SCM_OBJ(&scm__rc.d1788[103]));
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-status")), SCM_OBJ(&libnetsocket_status__STUB), 0);
  libnetsocket_status__STUB.common.info = scm__rc.d1786[121];
  libnetsocket_status__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[108]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[106]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[107]), scm__rc.d1786[24]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[108]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[109]), scm__rc.d1786[91]);
  scm__rc.d1786[122] = Scm_MakeExtendedPair(scm__rc.d1786[24], SCM_OBJ(&scm__rc.d1788[87]), SCM_OBJ(&scm__rc.d1788[111]));
  scm__rc.d1786[123] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[100])),TRUE); /* <long> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[124]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[124]))[4] = scm__rc.d1786[2];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[124]))[5] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[124]))[6] = scm__rc.d1786[123];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-fd")), SCM_OBJ(&libnetsocket_fd__STUB), 0);
  libnetsocket_fd__STUB.common.info = scm__rc.d1786[122];
  libnetsocket_fd__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[124]);
  scm__rc.d1786[131] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[101]))); /* :buffering */
  scm__rc.d1786[132] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[102]))); /* :buffered? */
  scm__rc.d1786[133] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[103]))); /* :key */
  scm__rc.d1786[134] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[101])),TRUE); /* buffering */
  scm__rc.d1786[135] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[102])),TRUE); /* buffered? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[112]), scm__rc.d1786[135]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[113]), scm__rc.d1786[134]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[114]), scm__rc.d1786[133]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[115]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[118]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[119]), scm__rc.d1786[18]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[120]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[121]), scm__rc.d1786[91]);
  scm__rc.d1786[136] = Scm_MakeExtendedPair(scm__rc.d1786[18], SCM_OBJ(&scm__rc.d1788[115]), SCM_OBJ(&scm__rc.d1788[123]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[137]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[137]))[4] = scm__rc.d1786[2];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[137]))[5] = scm__rc.d1786[94];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[137]))[6] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[137]))[7] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-input-port")), SCM_OBJ(&libnetsocket_input_port__STUB), 0);
  libnetsocket_input_port__STUB.common.info = scm__rc.d1786[136];
  libnetsocket_input_port__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[137]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[126]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[127]), scm__rc.d1786[19]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[128]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[129]), scm__rc.d1786[91]);
  scm__rc.d1786[145] = Scm_MakeExtendedPair(scm__rc.d1786[19], SCM_OBJ(&scm__rc.d1788[115]), SCM_OBJ(&scm__rc.d1788[131]));
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-output-port")), SCM_OBJ(&libnetsocket_output_port__STUB), 0);
  libnetsocket_output_port__STUB.common.info = scm__rc.d1786[145];
  libnetsocket_output_port__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[137]);
#if (defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))
  scm__rc.d1833[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1834[0])),TRUE); /* SHUT_RD */
#endif /* (defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)) */
#if (defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1833[0]), Scm_MakeInteger(SHUT_RD), SCM_BINDING_CONST);

#endif /* (defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)) */
#if (defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))
  scm__rc.d1833[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1834[1])),TRUE); /* SHUT_WR */
#endif /* (defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)) */
#if (defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1833[1]), Scm_MakeInteger(SHUT_WR), SCM_BINDING_CONST);

#endif /* (defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)) */
#if (defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))
  scm__rc.d1833[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1834[2])),TRUE); /* SHUT_RDWR */
#endif /* (defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)) */
#if (defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1833[2]), Scm_MakeInteger(SHUT_RDWR), SCM_BINDING_CONST);

#endif /* (defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)) */
#if !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))
  scm__rc.d1835[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1836[0])),TRUE); /* SHUT_RD */
#endif /* !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))) */
#if !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1835[0]), SCM_MAKE_INT(0), SCM_BINDING_CONST);

#endif /* !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))) */
#if !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))
#endif /* !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))) */
#if !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))
  scm__rc.d1835[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1836[1])),TRUE); /* SHUT_WR */
#endif /* !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))) */
#if !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1835[1]), SCM_MAKE_INT(1U), SCM_BINDING_CONST);

#endif /* !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))) */
#if !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))
#endif /* !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))) */
#if !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))
  scm__rc.d1835[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1836[2])),TRUE); /* SHUT_RDWR */
#endif /* !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))) */
#if !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1835[2]), SCM_MAKE_INT(2U), SCM_BINDING_CONST);

#endif /* !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))) */
#if !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR)))
#endif /* !((defined(SHUT_RD))&&(defined(SHUT_WR))&&(defined(SHUT_RDWR))) */
  scm__rc.d1786[146] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[104])),TRUE); /* how */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[132]), scm__rc.d1786[146]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[133]), scm__rc.d1786[88]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[134]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[137]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[138]), scm__rc.d1786[20]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[139]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[140]), scm__rc.d1786[91]);
  scm__rc.d1786[147] = Scm_MakeExtendedPair(scm__rc.d1786[20], SCM_OBJ(&scm__rc.d1788[134]), SCM_OBJ(&scm__rc.d1788[142]));
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-shutdown")), SCM_OBJ(&libnetsocket_shutdown__STUB), 0);
  libnetsocket_shutdown__STUB.common.info = scm__rc.d1786[147];
  libnetsocket_shutdown__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[137]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[145]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[146]), scm__rc.d1786[21]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[147]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[148]), scm__rc.d1786[91]);
  scm__rc.d1786[148] = Scm_MakeExtendedPair(scm__rc.d1786[21], SCM_OBJ(&scm__rc.d1788[87]), SCM_OBJ(&scm__rc.d1788[150]));
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-close")), SCM_OBJ(&libnetsocket_close__STUB), 0);
  libnetsocket_close__STUB.common.info = scm__rc.d1786[148];
  libnetsocket_close__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[108]);
  scm__rc.d1786[149] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[105])),TRUE); /* addr */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[151]), scm__rc.d1786[149]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[152]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[155]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[156]), scm__rc.d1786[22]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[157]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[158]), scm__rc.d1786[91]);
  scm__rc.d1786[150] = Scm_MakeExtendedPair(scm__rc.d1786[22], SCM_OBJ(&scm__rc.d1788[152]), SCM_OBJ(&scm__rc.d1788[160]));
  scm__rc.d1786[151] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[106])),TRUE); /* <socket-address> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[152]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[152]))[4] = scm__rc.d1786[2];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[152]))[5] = scm__rc.d1786[151];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[152]))[6] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[152]))[7] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-bind")), SCM_OBJ(&libnetsocket_bind__STUB), 0);
  libnetsocket_bind__STUB.common.info = scm__rc.d1786[150];
  libnetsocket_bind__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[152]);
  scm__rc.d1786[160] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[107])),TRUE); /* backlog */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[161]), scm__rc.d1786[160]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[162]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[165]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[166]), scm__rc.d1786[25]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[167]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[168]), scm__rc.d1786[91]);
  scm__rc.d1786[161] = Scm_MakeExtendedPair(scm__rc.d1786[25], SCM_OBJ(&scm__rc.d1788[162]), SCM_OBJ(&scm__rc.d1788[170]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[162]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[162]))[4] = scm__rc.d1786[2];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[162]))[5] = scm__rc.d1786[93];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[162]))[6] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[162]))[7] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-listen")), SCM_OBJ(&libnetsocket_listen__STUB), 0);
  libnetsocket_listen__STUB.common.info = scm__rc.d1786[161];
  libnetsocket_listen__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[162]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[173]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[174]), scm__rc.d1786[26]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[175]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[176]), scm__rc.d1786[91]);
  scm__rc.d1786[170] = Scm_MakeExtendedPair(scm__rc.d1786[26], SCM_OBJ(&scm__rc.d1788[87]), SCM_OBJ(&scm__rc.d1788[178]));
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-accept")), SCM_OBJ(&libnetsocket_accept__STUB), 0);
  libnetsocket_accept__STUB.common.info = scm__rc.d1786[170];
  libnetsocket_accept__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[108]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[181]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[182]), scm__rc.d1786[23]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[183]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[184]), scm__rc.d1786[91]);
  scm__rc.d1786[171] = Scm_MakeExtendedPair(scm__rc.d1786[23], SCM_OBJ(&scm__rc.d1788[152]), SCM_OBJ(&scm__rc.d1788[186]));
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-connect")), SCM_OBJ(&libnetsocket_connect__STUB), 0);
  libnetsocket_connect__STUB.common.info = scm__rc.d1786[171];
  libnetsocket_connect__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[152]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[189]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[190]), scm__rc.d1786[29]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[191]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[192]), scm__rc.d1786[91]);
  scm__rc.d1786[172] = Scm_MakeExtendedPair(scm__rc.d1786[29], SCM_OBJ(&scm__rc.d1788[87]), SCM_OBJ(&scm__rc.d1788[194]));
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-getsockname")), SCM_OBJ(&libnetsocket_getsockname__STUB), 0);
  libnetsocket_getsockname__STUB.common.info = scm__rc.d1786[172];
  libnetsocket_getsockname__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[108]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[197]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[198]), scm__rc.d1786[30]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[199]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[200]), scm__rc.d1786[91]);
  scm__rc.d1786[173] = Scm_MakeExtendedPair(scm__rc.d1786[30], SCM_OBJ(&scm__rc.d1788[87]), SCM_OBJ(&scm__rc.d1788[202]));
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-getpeername")), SCM_OBJ(&libnetsocket_getpeername__STUB), 0);
  libnetsocket_getpeername__STUB.common.info = scm__rc.d1786[173];
  libnetsocket_getpeername__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[108]);
  scm__rc.d1786[174] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[108])),TRUE); /* msg */
  scm__rc.d1786[175] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[109])),TRUE); /* flags */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[203]), scm__rc.d1786[175]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[204]), scm__rc.d1786[88]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[205]), scm__rc.d1786[174]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[206]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[209]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[210]), scm__rc.d1786[32]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[211]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[212]), scm__rc.d1786[91]);
  scm__rc.d1786[176] = Scm_MakeExtendedPair(scm__rc.d1786[32], SCM_OBJ(&scm__rc.d1788[206]), SCM_OBJ(&scm__rc.d1788[214]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[177]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[177]))[4] = scm__rc.d1786[2];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[177]))[5] = scm__rc.d1786[96];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[177]))[6] = scm__rc.d1786[94];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[177]))[7] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[177]))[8] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-send")), SCM_OBJ(&libnetsocket_send__STUB), 0);
  libnetsocket_send__STUB.common.info = scm__rc.d1786[176];
  libnetsocket_send__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[177]);
  scm__rc.d1786[186] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[110])),TRUE); /* to */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[215]), scm__rc.d1786[186]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[216]), scm__rc.d1786[174]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[217]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[220]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[221]), scm__rc.d1786[33]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[222]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[223]), scm__rc.d1786[91]);
  scm__rc.d1786[187] = Scm_MakeExtendedPair(scm__rc.d1786[33], SCM_OBJ(&scm__rc.d1788[217]), SCM_OBJ(&scm__rc.d1788[225]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[188]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[188]))[4] = scm__rc.d1786[2];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[188]))[5] = scm__rc.d1786[96];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[188]))[6] = scm__rc.d1786[151];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[188]))[7] = scm__rc.d1786[94];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[188]))[8] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[188]))[9] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-sendto")), SCM_OBJ(&libnetsocket_sendto__STUB), 0);
  libnetsocket_sendto__STUB.common.info = scm__rc.d1786[187];
  libnetsocket_sendto__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[188]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[228]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[229]), scm__rc.d1786[34]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[230]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[231]), scm__rc.d1786[91]);
  scm__rc.d1786[198] = Scm_MakeExtendedPair(scm__rc.d1786[34], SCM_OBJ(&scm__rc.d1788[206]), SCM_OBJ(&scm__rc.d1788[233]));
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-sendmsg")), SCM_OBJ(&libnetsocket_sendmsg__STUB), 0);
  libnetsocket_sendmsg__STUB.common.info = scm__rc.d1786[198];
  libnetsocket_sendmsg__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[177]);
  scm__rc.d1786[199] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[111])),TRUE); /* bytes */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[234]), scm__rc.d1786[199]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[235]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[238]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[239]), scm__rc.d1786[36]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[240]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[241]), scm__rc.d1786[91]);
  scm__rc.d1786[200] = Scm_MakeExtendedPair(scm__rc.d1786[36], SCM_OBJ(&scm__rc.d1788[235]), SCM_OBJ(&scm__rc.d1788[243]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[201]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[201]))[4] = scm__rc.d1786[2];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[201]))[5] = scm__rc.d1786[93];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[201]))[6] = scm__rc.d1786[94];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[201]))[7] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[201]))[8] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-recv")), SCM_OBJ(&libnetsocket_recv__STUB), 0);
  libnetsocket_recv__STUB.common.info = scm__rc.d1786[200];
  libnetsocket_recv__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[201]);
  scm__rc.d1786[210] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[112])),TRUE); /* buf */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[244]), scm__rc.d1786[210]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[245]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[248]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[249]), scm__rc.d1786[37]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[250]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[251]), scm__rc.d1786[91]);
  scm__rc.d1786[211] = Scm_MakeExtendedPair(scm__rc.d1786[37], SCM_OBJ(&scm__rc.d1788[245]), SCM_OBJ(&scm__rc.d1788[253]));
  scm__rc.d1786[212] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[113])),TRUE); /* <uvector> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[213]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[213]))[4] = scm__rc.d1786[2];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[213]))[5] = scm__rc.d1786[212];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[213]))[6] = scm__rc.d1786[94];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[213]))[7] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[213]))[8] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-recv!")), SCM_OBJ(&libnetsocket_recvX__STUB), 0);
  libnetsocket_recvX__STUB.common.info = scm__rc.d1786[211];
  libnetsocket_recvX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[213]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[256]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[257]), scm__rc.d1786[38]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[258]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[259]), scm__rc.d1786[91]);
  scm__rc.d1786[222] = Scm_MakeExtendedPair(scm__rc.d1786[38], SCM_OBJ(&scm__rc.d1788[235]), SCM_OBJ(&scm__rc.d1788[261]));
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-recvfrom")), SCM_OBJ(&libnetsocket_recvfrom__STUB), 0);
  libnetsocket_recvfrom__STUB.common.info = scm__rc.d1786[222];
  libnetsocket_recvfrom__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[201]);
  scm__rc.d1786[223] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[114])),TRUE); /* addrs */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[262]), scm__rc.d1786[223]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[263]), scm__rc.d1786[210]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[264]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[267]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[268]), scm__rc.d1786[39]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[269]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[270]), scm__rc.d1786[91]);
  scm__rc.d1786[224] = Scm_MakeExtendedPair(scm__rc.d1786[39], SCM_OBJ(&scm__rc.d1788[264]), SCM_OBJ(&scm__rc.d1788[272]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[225]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[225]))[4] = scm__rc.d1786[2];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[225]))[5] = scm__rc.d1786[212];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[225]))[6] = scm__rc.d1786[96];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[225]))[7] = scm__rc.d1786[94];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[225]))[8] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[225]))[9] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-recvfrom!")), SCM_OBJ(&libnetsocket_recvfromX__STUB), 0);
  libnetsocket_recvfromX__STUB.common.info = scm__rc.d1786[224];
  libnetsocket_recvfromX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[225]);
  scm__rc.d1786[235] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[115])),TRUE); /* name */
  scm__rc.d1786[236] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[116])),TRUE); /* iov */
  scm__rc.d1786[237] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[117])),TRUE); /* control */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[273]), scm__rc.d1786[210]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[274]), scm__rc.d1786[88]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[275]), scm__rc.d1786[175]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[276]), scm__rc.d1786[237]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[277]), scm__rc.d1786[236]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[278]), scm__rc.d1786[235]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[281]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[282]), scm__rc.d1786[35]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[283]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[284]), scm__rc.d1786[91]);
  scm__rc.d1786[238] = Scm_MakeExtendedPair(scm__rc.d1786[35], SCM_OBJ(&scm__rc.d1788[278]), SCM_OBJ(&scm__rc.d1788[286]));
  scm__rc.d1786[239] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[118])),TRUE); /* <socket-address>? */
  scm__rc.d1786[240] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[119])),TRUE); /* <vector>? */
  scm__rc.d1786[241] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[120])),TRUE); /* <int> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[242]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[242]))[4] = scm__rc.d1786[239];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[242]))[5] = scm__rc.d1786[240];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[242]))[6] = scm__rc.d1786[96];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[242]))[7] = scm__rc.d1786[241];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[242]))[8] = scm__rc.d1786[94];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[242]))[9] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[242]))[10] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-buildmsg")), SCM_OBJ(&libnetsocket_buildmsg__STUB), 0);
  libnetsocket_buildmsg__STUB.common.info = scm__rc.d1786[238];
  libnetsocket_buildmsg__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[242]);
  scm__rc.d1786[253] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[121])),TRUE); /* level */
  scm__rc.d1786[254] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[122])),TRUE); /* option */
  scm__rc.d1786[255] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[123])),TRUE); /* value */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[287]), scm__rc.d1786[255]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[288]), scm__rc.d1786[254]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[289]), scm__rc.d1786[253]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[290]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[293]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[294]), scm__rc.d1786[27]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[295]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[296]), scm__rc.d1786[91]);
  scm__rc.d1786[256] = Scm_MakeExtendedPair(scm__rc.d1786[27], SCM_OBJ(&scm__rc.d1788[290]), SCM_OBJ(&scm__rc.d1788[298]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[257]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[257]))[4] = scm__rc.d1786[2];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[257]))[5] = scm__rc.d1786[93];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[257]))[6] = scm__rc.d1786[93];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[257]))[7] = scm__rc.d1786[96];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[257]))[8] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[257]))[9] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-setsockopt")), SCM_OBJ(&libnetsocket_setsockopt__STUB), 0);
  libnetsocket_setsockopt__STUB.common.info = scm__rc.d1786[256];
  libnetsocket_setsockopt__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[257]);
  scm__rc.d1786[267] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[124])),TRUE); /* rsize */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[299]), scm__rc.d1786[267]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[300]), scm__rc.d1786[254]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[301]), scm__rc.d1786[253]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[302]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[305]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[306]), scm__rc.d1786[28]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[307]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[308]), scm__rc.d1786[91]);
  scm__rc.d1786[268] = Scm_MakeExtendedPair(scm__rc.d1786[28], SCM_OBJ(&scm__rc.d1788[302]), SCM_OBJ(&scm__rc.d1788[310]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[269]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[269]))[4] = scm__rc.d1786[2];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[269]))[5] = scm__rc.d1786[93];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[269]))[6] = scm__rc.d1786[93];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[269]))[7] = scm__rc.d1786[93];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[269]))[8] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[269]))[9] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-getsockopt")), SCM_OBJ(&libnetsocket_getsockopt__STUB), 0);
  libnetsocket_getsockopt__STUB.common.info = scm__rc.d1786[268];
  libnetsocket_getsockopt__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[269]);
#if defined(SOL_SOCKET)
  scm__rc.d1837[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1838[0])),TRUE); /* SOL_SOCKET */
#endif /* defined(SOL_SOCKET) */
#if defined(SOL_SOCKET)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1837[0]), Scm_MakeInteger(SOL_SOCKET), SCM_BINDING_CONST);

#endif /* defined(SOL_SOCKET) */
#if defined(SO_ACCEPTCONN)
  scm__rc.d1839[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1840[0])),TRUE); /* SO_ACCEPTCONN */
#endif /* defined(SO_ACCEPTCONN) */
#if defined(SO_ACCEPTCONN)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1839[0]), Scm_MakeInteger(SO_ACCEPTCONN), SCM_BINDING_CONST);

#endif /* defined(SO_ACCEPTCONN) */
#if defined(SO_BINDTODEVICE)
  scm__rc.d1841[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1842[0])),TRUE); /* SO_BINDTODEVICE */
#endif /* defined(SO_BINDTODEVICE) */
#if defined(SO_BINDTODEVICE)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1841[0]), Scm_MakeInteger(SO_BINDTODEVICE), SCM_BINDING_CONST);

#endif /* defined(SO_BINDTODEVICE) */
#if defined(SO_BROADCAST)
  scm__rc.d1843[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1844[0])),TRUE); /* SO_BROADCAST */
#endif /* defined(SO_BROADCAST) */
#if defined(SO_BROADCAST)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1843[0]), Scm_MakeInteger(SO_BROADCAST), SCM_BINDING_CONST);

#endif /* defined(SO_BROADCAST) */
#if defined(SO_DEBUG)
  scm__rc.d1845[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1846[0])),TRUE); /* SO_DEBUG */
#endif /* defined(SO_DEBUG) */
#if defined(SO_DEBUG)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1845[0]), Scm_MakeInteger(SO_DEBUG), SCM_BINDING_CONST);

#endif /* defined(SO_DEBUG) */
#if defined(SO_DONTROUTE)
  scm__rc.d1847[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1848[0])),TRUE); /* SO_DONTROUTE */
#endif /* defined(SO_DONTROUTE) */
#if defined(SO_DONTROUTE)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1847[0]), Scm_MakeInteger(SO_DONTROUTE), SCM_BINDING_CONST);

#endif /* defined(SO_DONTROUTE) */
#if defined(SO_ERROR)
  scm__rc.d1849[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1850[0])),TRUE); /* SO_ERROR */
#endif /* defined(SO_ERROR) */
#if defined(SO_ERROR)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1849[0]), Scm_MakeInteger(SO_ERROR), SCM_BINDING_CONST);

#endif /* defined(SO_ERROR) */
#if defined(SO_KEEPALIVE)
  scm__rc.d1851[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1852[0])),TRUE); /* SO_KEEPALIVE */
#endif /* defined(SO_KEEPALIVE) */
#if defined(SO_KEEPALIVE)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1851[0]), Scm_MakeInteger(SO_KEEPALIVE), SCM_BINDING_CONST);

#endif /* defined(SO_KEEPALIVE) */
#if defined(SO_LINGER)
  scm__rc.d1853[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1854[0])),TRUE); /* SO_LINGER */
#endif /* defined(SO_LINGER) */
#if defined(SO_LINGER)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1853[0]), Scm_MakeInteger(SO_LINGER), SCM_BINDING_CONST);

#endif /* defined(SO_LINGER) */
#if defined(SO_OOBINLINE)
  scm__rc.d1855[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1856[0])),TRUE); /* SO_OOBINLINE */
#endif /* defined(SO_OOBINLINE) */
#if defined(SO_OOBINLINE)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1855[0]), Scm_MakeInteger(SO_OOBINLINE), SCM_BINDING_CONST);

#endif /* defined(SO_OOBINLINE) */
#if defined(SO_PASSCRED)
  scm__rc.d1857[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1858[0])),TRUE); /* SO_PASSCRED */
#endif /* defined(SO_PASSCRED) */
#if defined(SO_PASSCRED)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1857[0]), Scm_MakeInteger(SO_PASSCRED), SCM_BINDING_CONST);

#endif /* defined(SO_PASSCRED) */
#if defined(SO_PEERCRED)
  scm__rc.d1859[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1860[0])),TRUE); /* SO_PEERCRED */
#endif /* defined(SO_PEERCRED) */
#if defined(SO_PEERCRED)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1859[0]), Scm_MakeInteger(SO_PEERCRED), SCM_BINDING_CONST);

#endif /* defined(SO_PEERCRED) */
#if defined(SO_PRIORITY)
  scm__rc.d1861[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1862[0])),TRUE); /* SO_PRIORITY */
#endif /* defined(SO_PRIORITY) */
#if defined(SO_PRIORITY)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1861[0]), Scm_MakeInteger(SO_PRIORITY), SCM_BINDING_CONST);

#endif /* defined(SO_PRIORITY) */
#if defined(SO_RCVBUF)
  scm__rc.d1863[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1864[0])),TRUE); /* SO_RCVBUF */
#endif /* defined(SO_RCVBUF) */
#if defined(SO_RCVBUF)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1863[0]), Scm_MakeInteger(SO_RCVBUF), SCM_BINDING_CONST);

#endif /* defined(SO_RCVBUF) */
#if defined(SO_RCVLOWAT)
  scm__rc.d1865[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1866[0])),TRUE); /* SO_RCVLOWAT */
#endif /* defined(SO_RCVLOWAT) */
#if defined(SO_RCVLOWAT)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1865[0]), Scm_MakeInteger(SO_RCVLOWAT), SCM_BINDING_CONST);

#endif /* defined(SO_RCVLOWAT) */
#if defined(SO_RCVTIMEO)
  scm__rc.d1867[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1868[0])),TRUE); /* SO_RCVTIMEO */
#endif /* defined(SO_RCVTIMEO) */
#if defined(SO_RCVTIMEO)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1867[0]), Scm_MakeInteger(SO_RCVTIMEO), SCM_BINDING_CONST);

#endif /* defined(SO_RCVTIMEO) */
#if defined(SO_REUSEADDR)
  scm__rc.d1869[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1870[0])),TRUE); /* SO_REUSEADDR */
#endif /* defined(SO_REUSEADDR) */
#if defined(SO_REUSEADDR)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1869[0]), Scm_MakeInteger(SO_REUSEADDR), SCM_BINDING_CONST);

#endif /* defined(SO_REUSEADDR) */
#if defined(SO_REUSEPORT)
  scm__rc.d1871[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1872[0])),TRUE); /* SO_REUSEPORT */
#endif /* defined(SO_REUSEPORT) */
#if defined(SO_REUSEPORT)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1871[0]), Scm_MakeInteger(SO_REUSEPORT), SCM_BINDING_CONST);

#endif /* defined(SO_REUSEPORT) */
#if defined(SO_SNDBUF)
  scm__rc.d1873[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1874[0])),TRUE); /* SO_SNDBUF */
#endif /* defined(SO_SNDBUF) */
#if defined(SO_SNDBUF)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1873[0]), Scm_MakeInteger(SO_SNDBUF), SCM_BINDING_CONST);

#endif /* defined(SO_SNDBUF) */
#if defined(SO_SNDLOWAT)
  scm__rc.d1875[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1876[0])),TRUE); /* SO_SNDLOWAT */
#endif /* defined(SO_SNDLOWAT) */
#if defined(SO_SNDLOWAT)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1875[0]), Scm_MakeInteger(SO_SNDLOWAT), SCM_BINDING_CONST);

#endif /* defined(SO_SNDLOWAT) */
#if defined(SO_SNDTIMEO)
  scm__rc.d1877[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1878[0])),TRUE); /* SO_SNDTIMEO */
#endif /* defined(SO_SNDTIMEO) */
#if defined(SO_SNDTIMEO)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1877[0]), Scm_MakeInteger(SO_SNDTIMEO), SCM_BINDING_CONST);

#endif /* defined(SO_SNDTIMEO) */
#if defined(SO_TIMESTAMP)
  scm__rc.d1879[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1880[0])),TRUE); /* SO_TIMESTAMP */
#endif /* defined(SO_TIMESTAMP) */
#if defined(SO_TIMESTAMP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1879[0]), Scm_MakeInteger(SO_TIMESTAMP), SCM_BINDING_CONST);

#endif /* defined(SO_TIMESTAMP) */
#if defined(SO_TYPE)
  scm__rc.d1881[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1882[0])),TRUE); /* SO_TYPE */
#endif /* defined(SO_TYPE) */
#if defined(SO_TYPE)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1881[0]), Scm_MakeInteger(SO_TYPE), SCM_BINDING_CONST);

#endif /* defined(SO_TYPE) */
#if defined(SOL_TCP)
  scm__rc.d1883[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1884[0])),TRUE); /* SOL_TCP */
#endif /* defined(SOL_TCP) */
#if defined(SOL_TCP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1883[0]), Scm_MakeInteger(SOL_TCP), SCM_BINDING_CONST);

#endif /* defined(SOL_TCP) */
#if defined(TCP_NODELAY)
  scm__rc.d1885[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1886[0])),TRUE); /* TCP_NODELAY */
#endif /* defined(TCP_NODELAY) */
#if defined(TCP_NODELAY)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1885[0]), Scm_MakeInteger(TCP_NODELAY), SCM_BINDING_CONST);

#endif /* defined(TCP_NODELAY) */
#if defined(TCP_MAXSEG)
  scm__rc.d1887[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1888[0])),TRUE); /* TCP_MAXSEG */
#endif /* defined(TCP_MAXSEG) */
#if defined(TCP_MAXSEG)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1887[0]), Scm_MakeInteger(TCP_MAXSEG), SCM_BINDING_CONST);

#endif /* defined(TCP_MAXSEG) */
#if defined(TCP_CORK)
  scm__rc.d1889[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1890[0])),TRUE); /* TCP_CORK */
#endif /* defined(TCP_CORK) */
#if defined(TCP_CORK)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1889[0]), Scm_MakeInteger(TCP_CORK), SCM_BINDING_CONST);

#endif /* defined(TCP_CORK) */
#if defined(SOL_IP)
  scm__rc.d1891[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1892[0])),TRUE); /* SOL_IP */
#endif /* defined(SOL_IP) */
#if defined(SOL_IP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1891[0]), Scm_MakeInteger(SOL_IP), SCM_BINDING_CONST);

#endif /* defined(SOL_IP) */
#if defined(IP_OPTIONS)
  scm__rc.d1893[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1894[0])),TRUE); /* IP_OPTIONS */
#endif /* defined(IP_OPTIONS) */
#if defined(IP_OPTIONS)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1893[0]), Scm_MakeInteger(IP_OPTIONS), SCM_BINDING_CONST);

#endif /* defined(IP_OPTIONS) */
#if defined(IP_PKTINFO)
  scm__rc.d1895[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1896[0])),TRUE); /* IP_PKTINFO */
#endif /* defined(IP_PKTINFO) */
#if defined(IP_PKTINFO)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1895[0]), Scm_MakeInteger(IP_PKTINFO), SCM_BINDING_CONST);

#endif /* defined(IP_PKTINFO) */
#if defined(IP_RECVTOS)
  scm__rc.d1897[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1898[0])),TRUE); /* IP_RECVTOS */
#endif /* defined(IP_RECVTOS) */
#if defined(IP_RECVTOS)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1897[0]), Scm_MakeInteger(IP_RECVTOS), SCM_BINDING_CONST);

#endif /* defined(IP_RECVTOS) */
#if defined(IP_RECVTTL)
  scm__rc.d1899[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1900[0])),TRUE); /* IP_RECVTTL */
#endif /* defined(IP_RECVTTL) */
#if defined(IP_RECVTTL)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1899[0]), Scm_MakeInteger(IP_RECVTTL), SCM_BINDING_CONST);

#endif /* defined(IP_RECVTTL) */
#if defined(IP_RECVOPTS)
  scm__rc.d1901[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1902[0])),TRUE); /* IP_RECVOPTS */
#endif /* defined(IP_RECVOPTS) */
#if defined(IP_RECVOPTS)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1901[0]), Scm_MakeInteger(IP_RECVOPTS), SCM_BINDING_CONST);

#endif /* defined(IP_RECVOPTS) */
#if defined(IP_TOS)
  scm__rc.d1903[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1904[0])),TRUE); /* IP_TOS */
#endif /* defined(IP_TOS) */
#if defined(IP_TOS)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1903[0]), Scm_MakeInteger(IP_TOS), SCM_BINDING_CONST);

#endif /* defined(IP_TOS) */
#if defined(IP_TTL)
  scm__rc.d1905[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1906[0])),TRUE); /* IP_TTL */
#endif /* defined(IP_TTL) */
#if defined(IP_TTL)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1905[0]), Scm_MakeInteger(IP_TTL), SCM_BINDING_CONST);

#endif /* defined(IP_TTL) */
#if defined(IP_HDRINCL)
  scm__rc.d1907[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1908[0])),TRUE); /* IP_HDRINCL */
#endif /* defined(IP_HDRINCL) */
#if defined(IP_HDRINCL)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1907[0]), Scm_MakeInteger(IP_HDRINCL), SCM_BINDING_CONST);

#endif /* defined(IP_HDRINCL) */
#if defined(IP_RECVERR)
  scm__rc.d1909[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1910[0])),TRUE); /* IP_RECVERR */
#endif /* defined(IP_RECVERR) */
#if defined(IP_RECVERR)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1909[0]), Scm_MakeInteger(IP_RECVERR), SCM_BINDING_CONST);

#endif /* defined(IP_RECVERR) */
#if defined(IP_MTU_DISCOVER)
  scm__rc.d1911[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1912[0])),TRUE); /* IP_MTU_DISCOVER */
#endif /* defined(IP_MTU_DISCOVER) */
#if defined(IP_MTU_DISCOVER)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1911[0]), Scm_MakeInteger(IP_MTU_DISCOVER), SCM_BINDING_CONST);

#endif /* defined(IP_MTU_DISCOVER) */
#if defined(IP_MTU)
  scm__rc.d1913[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1914[0])),TRUE); /* IP_MTU */
#endif /* defined(IP_MTU) */
#if defined(IP_MTU)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1913[0]), Scm_MakeInteger(IP_MTU), SCM_BINDING_CONST);

#endif /* defined(IP_MTU) */
#if defined(IP_ROUTER_ALERT)
  scm__rc.d1915[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1916[0])),TRUE); /* IP_ROUTER_ALERT */
#endif /* defined(IP_ROUTER_ALERT) */
#if defined(IP_ROUTER_ALERT)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1915[0]), Scm_MakeInteger(IP_ROUTER_ALERT), SCM_BINDING_CONST);

#endif /* defined(IP_ROUTER_ALERT) */
#if defined(IP_MULTICAST_TTL)
  scm__rc.d1917[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1918[0])),TRUE); /* IP_MULTICAST_TTL */
#endif /* defined(IP_MULTICAST_TTL) */
#if defined(IP_MULTICAST_TTL)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1917[0]), Scm_MakeInteger(IP_MULTICAST_TTL), SCM_BINDING_CONST);

#endif /* defined(IP_MULTICAST_TTL) */
#if defined(IP_MULTICAST_LOOP)
  scm__rc.d1919[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1920[0])),TRUE); /* IP_MULTICAST_LOOP */
#endif /* defined(IP_MULTICAST_LOOP) */
#if defined(IP_MULTICAST_LOOP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1919[0]), Scm_MakeInteger(IP_MULTICAST_LOOP), SCM_BINDING_CONST);

#endif /* defined(IP_MULTICAST_LOOP) */
#if defined(IP_ADD_MEMBERSHIP)
  scm__rc.d1921[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1922[0])),TRUE); /* IP_ADD_MEMBERSHIP */
#endif /* defined(IP_ADD_MEMBERSHIP) */
#if defined(IP_ADD_MEMBERSHIP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1921[0]), Scm_MakeInteger(IP_ADD_MEMBERSHIP), SCM_BINDING_CONST);

#endif /* defined(IP_ADD_MEMBERSHIP) */
#if defined(IP_DROP_MEMBERSHIP)
  scm__rc.d1923[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1924[0])),TRUE); /* IP_DROP_MEMBERSHIP */
#endif /* defined(IP_DROP_MEMBERSHIP) */
#if defined(IP_DROP_MEMBERSHIP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1923[0]), Scm_MakeInteger(IP_DROP_MEMBERSHIP), SCM_BINDING_CONST);

#endif /* defined(IP_DROP_MEMBERSHIP) */
#if defined(IP_MULTICAST_IF)
  scm__rc.d1925[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1926[0])),TRUE); /* IP_MULTICAST_IF */
#endif /* defined(IP_MULTICAST_IF) */
#if defined(IP_MULTICAST_IF)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1925[0]), Scm_MakeInteger(IP_MULTICAST_IF), SCM_BINDING_CONST);

#endif /* defined(IP_MULTICAST_IF) */
  scm__rc.d1786[279] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[125])),TRUE); /* request */
  scm__rc.d1786[280] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[126])),TRUE); /* data */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[311]), scm__rc.d1786[280]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[312]), scm__rc.d1786[279]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[313]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[316]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[317]), scm__rc.d1786[31]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[318]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[319]), scm__rc.d1786[91]);
  scm__rc.d1786[281] = Scm_MakeExtendedPair(scm__rc.d1786[31], SCM_OBJ(&scm__rc.d1788[313]), SCM_OBJ(&scm__rc.d1788[321]));
  scm__rc.d1786[282] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[127])),TRUE); /* <integer> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[283]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[283]))[4] = scm__rc.d1786[2];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[283]))[5] = scm__rc.d1786[282];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[283]))[6] = scm__rc.d1786[96];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[283]))[7] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[283]))[8] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("socket-ioctl")), SCM_OBJ(&libnetsocket_ioctl__STUB), 0);
  libnetsocket_ioctl__STUB.common.info = scm__rc.d1786[281];
  libnetsocket_ioctl__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[283]);
#if defined(SIOCGIFNAME)
  scm__rc.d1927[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1928[0])),TRUE); /* SIOCGIFNAME */
#endif /* defined(SIOCGIFNAME) */
#if defined(SIOCGIFNAME)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1927[0]), Scm_MakeInteger(SIOCGIFNAME), SCM_BINDING_CONST);

#endif /* defined(SIOCGIFNAME) */
#if defined(SIOCSIFNAME)
  scm__rc.d1929[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1930[0])),TRUE); /* SIOCSIFNAME */
#endif /* defined(SIOCSIFNAME) */
#if defined(SIOCSIFNAME)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1929[0]), Scm_MakeInteger(SIOCSIFNAME), SCM_BINDING_CONST);

#endif /* defined(SIOCSIFNAME) */
#if defined(SIOCGIFINDEX)
  scm__rc.d1931[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1932[0])),TRUE); /* SIOCGIFINDEX */
#endif /* defined(SIOCGIFINDEX) */
#if defined(SIOCGIFINDEX)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1931[0]), Scm_MakeInteger(SIOCGIFINDEX), SCM_BINDING_CONST);

#endif /* defined(SIOCGIFINDEX) */
#if defined(SIOCGIFADDR)
  scm__rc.d1933[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1934[0])),TRUE); /* SIOCGIFADDR */
#endif /* defined(SIOCGIFADDR) */
#if defined(SIOCGIFADDR)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1933[0]), Scm_MakeInteger(SIOCGIFADDR), SCM_BINDING_CONST);

#endif /* defined(SIOCGIFADDR) */
#if defined(SIOCSIFADDR)
  scm__rc.d1935[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1936[0])),TRUE); /* SIOCSIFADDR */
#endif /* defined(SIOCSIFADDR) */
#if defined(SIOCSIFADDR)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1935[0]), Scm_MakeInteger(SIOCSIFADDR), SCM_BINDING_CONST);

#endif /* defined(SIOCSIFADDR) */
#if defined(SIOCGIFDSTADDR)
  scm__rc.d1937[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1938[0])),TRUE); /* SIOCGIFDSTADDR */
#endif /* defined(SIOCGIFDSTADDR) */
#if defined(SIOCGIFDSTADDR)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1937[0]), Scm_MakeInteger(SIOCGIFDSTADDR), SCM_BINDING_CONST);

#endif /* defined(SIOCGIFDSTADDR) */
#if defined(SIOCSIFDSTADDR)
  scm__rc.d1939[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1940[0])),TRUE); /* SIOCSIFDSTADDR */
#endif /* defined(SIOCSIFDSTADDR) */
#if defined(SIOCSIFDSTADDR)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1939[0]), Scm_MakeInteger(SIOCSIFDSTADDR), SCM_BINDING_CONST);

#endif /* defined(SIOCSIFDSTADDR) */
#if defined(SIOCGIFBRDADDR)
  scm__rc.d1941[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1942[0])),TRUE); /* SIOCGIFBRDADDR */
#endif /* defined(SIOCGIFBRDADDR) */
#if defined(SIOCGIFBRDADDR)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1941[0]), Scm_MakeInteger(SIOCGIFBRDADDR), SCM_BINDING_CONST);

#endif /* defined(SIOCGIFBRDADDR) */
#if defined(SIOCSIFBRDADDR)
  scm__rc.d1943[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1944[0])),TRUE); /* SIOCSIFBRDADDR */
#endif /* defined(SIOCSIFBRDADDR) */
#if defined(SIOCSIFBRDADDR)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1943[0]), Scm_MakeInteger(SIOCSIFBRDADDR), SCM_BINDING_CONST);

#endif /* defined(SIOCSIFBRDADDR) */
#if defined(SIOCGIFNETMASK)
  scm__rc.d1945[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1946[0])),TRUE); /* SIOCGIFNETMASK */
#endif /* defined(SIOCGIFNETMASK) */
#if defined(SIOCGIFNETMASK)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1945[0]), Scm_MakeInteger(SIOCGIFNETMASK), SCM_BINDING_CONST);

#endif /* defined(SIOCGIFNETMASK) */
#if defined(SIOCSIFNETMASK)
  scm__rc.d1947[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1948[0])),TRUE); /* SIOCSIFNETMASK */
#endif /* defined(SIOCSIFNETMASK) */
#if defined(SIOCSIFNETMASK)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1947[0]), Scm_MakeInteger(SIOCSIFNETMASK), SCM_BINDING_CONST);

#endif /* defined(SIOCSIFNETMASK) */
#if defined(SIOCGIFFLAGS)
  scm__rc.d1949[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1950[0])),TRUE); /* SIOCGIFFLAGS */
#endif /* defined(SIOCGIFFLAGS) */
#if defined(SIOCGIFFLAGS)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1949[0]), Scm_MakeInteger(SIOCGIFFLAGS), SCM_BINDING_CONST);

#endif /* defined(SIOCGIFFLAGS) */
#if defined(SIOCSIFFLAGS)
  scm__rc.d1951[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1952[0])),TRUE); /* SIOCSIFFLAGS */
#endif /* defined(SIOCSIFFLAGS) */
#if defined(SIOCSIFFLAGS)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1951[0]), Scm_MakeInteger(SIOCSIFFLAGS), SCM_BINDING_CONST);

#endif /* defined(SIOCSIFFLAGS) */
#if defined(SIOCGIFMETRIC)
  scm__rc.d1953[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1954[0])),TRUE); /* SIOCGIFMETRIC */
#endif /* defined(SIOCGIFMETRIC) */
#if defined(SIOCGIFMETRIC)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1953[0]), Scm_MakeInteger(SIOCGIFMETRIC), SCM_BINDING_CONST);

#endif /* defined(SIOCGIFMETRIC) */
#if defined(SIOCSIFMETRIC)
  scm__rc.d1955[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1956[0])),TRUE); /* SIOCSIFMETRIC */
#endif /* defined(SIOCSIFMETRIC) */
#if defined(SIOCSIFMETRIC)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1955[0]), Scm_MakeInteger(SIOCSIFMETRIC), SCM_BINDING_CONST);

#endif /* defined(SIOCSIFMETRIC) */
#if defined(SIOCGIFMTU)
  scm__rc.d1957[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1958[0])),TRUE); /* SIOCGIFMTU */
#endif /* defined(SIOCGIFMTU) */
#if defined(SIOCGIFMTU)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1957[0]), Scm_MakeInteger(SIOCGIFMTU), SCM_BINDING_CONST);

#endif /* defined(SIOCGIFMTU) */
#if defined(SIOCSIFMTU)
  scm__rc.d1959[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1960[0])),TRUE); /* SIOCSIFMTU */
#endif /* defined(SIOCSIFMTU) */
#if defined(SIOCSIFMTU)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1959[0]), Scm_MakeInteger(SIOCSIFMTU), SCM_BINDING_CONST);

#endif /* defined(SIOCSIFMTU) */
#if defined(SIOCGIFHWADDR)
  scm__rc.d1961[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1962[0])),TRUE); /* SIOCGIFHWADDR */
#endif /* defined(SIOCGIFHWADDR) */
#if defined(SIOCGIFHWADDR)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1961[0]), Scm_MakeInteger(SIOCGIFHWADDR), SCM_BINDING_CONST);

#endif /* defined(SIOCGIFHWADDR) */
#if defined(SIOCSIFHWADDR)
  scm__rc.d1963[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1964[0])),TRUE); /* SIOCSIFHWADDR */
#endif /* defined(SIOCSIFHWADDR) */
#if defined(SIOCSIFHWADDR)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1963[0]), Scm_MakeInteger(SIOCSIFHWADDR), SCM_BINDING_CONST);

#endif /* defined(SIOCSIFHWADDR) */
#if defined(SIOCSIFHWBROADCAST)
  scm__rc.d1965[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1966[0])),TRUE); /* SIOCSIFHWBROADCAST */
#endif /* defined(SIOCSIFHWBROADCAST) */
#if defined(SIOCSIFHWBROADCAST)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1965[0]), Scm_MakeInteger(SIOCSIFHWBROADCAST), SCM_BINDING_CONST);

#endif /* defined(SIOCSIFHWBROADCAST) */
#if defined(SIOCGIFMAP)
  scm__rc.d1967[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1968[0])),TRUE); /* SIOCGIFMAP */
#endif /* defined(SIOCGIFMAP) */
#if defined(SIOCGIFMAP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1967[0]), Scm_MakeInteger(SIOCGIFMAP), SCM_BINDING_CONST);

#endif /* defined(SIOCGIFMAP) */
#if defined(SIOCSIFMAP)
  scm__rc.d1969[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1970[0])),TRUE); /* SIOCSIFMAP */
#endif /* defined(SIOCSIFMAP) */
#if defined(SIOCSIFMAP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1969[0]), Scm_MakeInteger(SIOCSIFMAP), SCM_BINDING_CONST);

#endif /* defined(SIOCSIFMAP) */
#if defined(SIOCADDMULTI)
  scm__rc.d1971[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1972[0])),TRUE); /* SIOCADDMULTI */
#endif /* defined(SIOCADDMULTI) */
#if defined(SIOCADDMULTI)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1971[0]), Scm_MakeInteger(SIOCADDMULTI), SCM_BINDING_CONST);

#endif /* defined(SIOCADDMULTI) */
#if defined(SIOCDELMULTI)
  scm__rc.d1973[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1974[0])),TRUE); /* SIOCDELMULTI */
#endif /* defined(SIOCDELMULTI) */
#if defined(SIOCDELMULTI)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1973[0]), Scm_MakeInteger(SIOCDELMULTI), SCM_BINDING_CONST);

#endif /* defined(SIOCDELMULTI) */
#if defined(SIOGIFTXQLEN)
  scm__rc.d1975[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1976[0])),TRUE); /* SIOGIFTXQLEN */
#endif /* defined(SIOGIFTXQLEN) */
#if defined(SIOGIFTXQLEN)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1975[0]), Scm_MakeInteger(SIOGIFTXQLEN), SCM_BINDING_CONST);

#endif /* defined(SIOGIFTXQLEN) */
#if defined(SIOSIFTXQLEN)
  scm__rc.d1977[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1978[0])),TRUE); /* SIOSIFTXQLEN */
#endif /* defined(SIOSIFTXQLEN) */
#if defined(SIOSIFTXQLEN)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1977[0]), Scm_MakeInteger(SIOSIFTXQLEN), SCM_BINDING_CONST);

#endif /* defined(SIOSIFTXQLEN) */
#if defined(SIOCGIFCONF)
  scm__rc.d1979[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1980[0])),TRUE); /* SIOCGIFCONF */
#endif /* defined(SIOCGIFCONF) */
#if defined(SIOCGIFCONF)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1979[0]), Scm_MakeInteger(SIOCGIFCONF), SCM_BINDING_CONST);

#endif /* defined(SIOCGIFCONF) */
#if defined(IFF_UP)
  scm__rc.d1981[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1982[0])),TRUE); /* IFF_UP */
#endif /* defined(IFF_UP) */
#if defined(IFF_UP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1981[0]), Scm_MakeInteger(IFF_UP), SCM_BINDING_CONST);

#endif /* defined(IFF_UP) */
#if defined(IFF_BROADCAST)
  scm__rc.d1983[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1984[0])),TRUE); /* IFF_BROADCAST */
#endif /* defined(IFF_BROADCAST) */
#if defined(IFF_BROADCAST)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1983[0]), Scm_MakeInteger(IFF_BROADCAST), SCM_BINDING_CONST);

#endif /* defined(IFF_BROADCAST) */
#if defined(IFF_DEBUG)
  scm__rc.d1985[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1986[0])),TRUE); /* IFF_DEBUG */
#endif /* defined(IFF_DEBUG) */
#if defined(IFF_DEBUG)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1985[0]), Scm_MakeInteger(IFF_DEBUG), SCM_BINDING_CONST);

#endif /* defined(IFF_DEBUG) */
#if defined(IFF_LOOPBACK)
  scm__rc.d1987[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1988[0])),TRUE); /* IFF_LOOPBACK */
#endif /* defined(IFF_LOOPBACK) */
#if defined(IFF_LOOPBACK)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1987[0]), Scm_MakeInteger(IFF_LOOPBACK), SCM_BINDING_CONST);

#endif /* defined(IFF_LOOPBACK) */
#if defined(IFF_POINTTOPOINT)
  scm__rc.d1989[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1990[0])),TRUE); /* IFF_POINTTOPOINT */
#endif /* defined(IFF_POINTTOPOINT) */
#if defined(IFF_POINTTOPOINT)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1989[0]), Scm_MakeInteger(IFF_POINTTOPOINT), SCM_BINDING_CONST);

#endif /* defined(IFF_POINTTOPOINT) */
#if defined(IFF_RUNNING)
  scm__rc.d1991[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1992[0])),TRUE); /* IFF_RUNNING */
#endif /* defined(IFF_RUNNING) */
#if defined(IFF_RUNNING)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1991[0]), Scm_MakeInteger(IFF_RUNNING), SCM_BINDING_CONST);

#endif /* defined(IFF_RUNNING) */
#if defined(IFF_NOARP)
  scm__rc.d1993[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1994[0])),TRUE); /* IFF_NOARP */
#endif /* defined(IFF_NOARP) */
#if defined(IFF_NOARP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1993[0]), Scm_MakeInteger(IFF_NOARP), SCM_BINDING_CONST);

#endif /* defined(IFF_NOARP) */
#if defined(IFF_PROMISC)
  scm__rc.d1995[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1996[0])),TRUE); /* IFF_PROMISC */
#endif /* defined(IFF_PROMISC) */
#if defined(IFF_PROMISC)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1995[0]), Scm_MakeInteger(IFF_PROMISC), SCM_BINDING_CONST);

#endif /* defined(IFF_PROMISC) */
#if defined(IFF_NOTRAILERS)
  scm__rc.d1997[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1998[0])),TRUE); /* IFF_NOTRAILERS */
#endif /* defined(IFF_NOTRAILERS) */
#if defined(IFF_NOTRAILERS)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1997[0]), Scm_MakeInteger(IFF_NOTRAILERS), SCM_BINDING_CONST);

#endif /* defined(IFF_NOTRAILERS) */
#if defined(IFF_ALLMULTI)
  scm__rc.d1999[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2000[0])),TRUE); /* IFF_ALLMULTI */
#endif /* defined(IFF_ALLMULTI) */
#if defined(IFF_ALLMULTI)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d1999[0]), Scm_MakeInteger(IFF_ALLMULTI), SCM_BINDING_CONST);

#endif /* defined(IFF_ALLMULTI) */
#if defined(IFF_MASTER)
  scm__rc.d2001[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2002[0])),TRUE); /* IFF_MASTER */
#endif /* defined(IFF_MASTER) */
#if defined(IFF_MASTER)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2001[0]), Scm_MakeInteger(IFF_MASTER), SCM_BINDING_CONST);

#endif /* defined(IFF_MASTER) */
#if defined(IFF_SLAVE)
  scm__rc.d2003[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2004[0])),TRUE); /* IFF_SLAVE */
#endif /* defined(IFF_SLAVE) */
#if defined(IFF_SLAVE)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2003[0]), Scm_MakeInteger(IFF_SLAVE), SCM_BINDING_CONST);

#endif /* defined(IFF_SLAVE) */
#if defined(IFF_MULTICAST)
  scm__rc.d2005[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2006[0])),TRUE); /* IFF_MULTICAST */
#endif /* defined(IFF_MULTICAST) */
#if defined(IFF_MULTICAST)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2005[0]), Scm_MakeInteger(IFF_MULTICAST), SCM_BINDING_CONST);

#endif /* defined(IFF_MULTICAST) */
#if defined(IFF_PORTSEL)
  scm__rc.d2007[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2008[0])),TRUE); /* IFF_PORTSEL */
#endif /* defined(IFF_PORTSEL) */
#if defined(IFF_PORTSEL)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2007[0]), Scm_MakeInteger(IFF_PORTSEL), SCM_BINDING_CONST);

#endif /* defined(IFF_PORTSEL) */
#if defined(IFF_AUTOMEDIA)
  scm__rc.d2009[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2010[0])),TRUE); /* IFF_AUTOMEDIA */
#endif /* defined(IFF_AUTOMEDIA) */
#if defined(IFF_AUTOMEDIA)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2009[0]), Scm_MakeInteger(IFF_AUTOMEDIA), SCM_BINDING_CONST);

#endif /* defined(IFF_AUTOMEDIA) */
#if defined(IFF_DYNAMIC)
  scm__rc.d2011[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2012[0])),TRUE); /* IFF_DYNAMIC */
#endif /* defined(IFF_DYNAMIC) */
#if defined(IFF_DYNAMIC)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2011[0]), Scm_MakeInteger(IFF_DYNAMIC), SCM_BINDING_CONST);

#endif /* defined(IFF_DYNAMIC) */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[322]), scm__rc.d1786[235]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[325]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[326]), scm__rc.d1786[53]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[327]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[328]), scm__rc.d1786[91]);
  scm__rc.d1786[292] = Scm_MakeExtendedPair(scm__rc.d1786[53], SCM_OBJ(&scm__rc.d1788[322]), SCM_OBJ(&scm__rc.d1788[330]));
  scm__rc.d1786[293] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[128])),TRUE); /* <const-cstring> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[294]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[294]))[4] = scm__rc.d1786[293];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[294]))[5] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[294]))[6] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("sys-gethostbyname")), SCM_OBJ(&libnetsys_gethostbyname__STUB), 0);
  libnetsys_gethostbyname__STUB.common.info = scm__rc.d1786[292];
  libnetsys_gethostbyname__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[294]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[331]), scm__rc.d1786[87]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[332]), scm__rc.d1786[149]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[335]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[336]), scm__rc.d1786[54]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[337]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[338]), scm__rc.d1786[91]);
  scm__rc.d1786[301] = Scm_MakeExtendedPair(scm__rc.d1786[54], SCM_OBJ(&scm__rc.d1788[332]), SCM_OBJ(&scm__rc.d1788[340]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[302]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[302]))[4] = scm__rc.d1786[293];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[302]))[5] = scm__rc.d1786[93];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[302]))[6] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[302]))[7] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("sys-gethostbyaddr")), SCM_OBJ(&libnetsys_gethostbyaddr__STUB), 0);
  libnetsys_gethostbyaddr__STUB.common.info = scm__rc.d1786[301];
  libnetsys_gethostbyaddr__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[302]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[343]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[344]), scm__rc.d1786[56]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[345]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[346]), scm__rc.d1786[91]);
  scm__rc.d1786[310] = Scm_MakeExtendedPair(scm__rc.d1786[56], SCM_OBJ(&scm__rc.d1788[322]), SCM_OBJ(&scm__rc.d1788[348]));
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("sys-getprotobyname")), SCM_OBJ(&libnetsys_getprotobyname__STUB), 0);
  libnetsys_getprotobyname__STUB.common.info = scm__rc.d1786[310];
  libnetsys_getprotobyname__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[294]);
  scm__rc.d1786[311] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[129])),TRUE); /* number */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[349]), scm__rc.d1786[311]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[352]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[353]), scm__rc.d1786[57]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[354]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[355]), scm__rc.d1786[91]);
  scm__rc.d1786[312] = Scm_MakeExtendedPair(scm__rc.d1786[57], SCM_OBJ(&scm__rc.d1788[349]), SCM_OBJ(&scm__rc.d1788[357]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[313]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[313]))[4] = scm__rc.d1786[93];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[313]))[5] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[313]))[6] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("sys-getprotobynumber")), SCM_OBJ(&libnetsys_getprotobynumber__STUB), 0);
  libnetsys_getprotobynumber__STUB.common.info = scm__rc.d1786[312];
  libnetsys_getprotobynumber__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[313]);
  scm__rc.d1786[320] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[130])),TRUE); /* proto */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[358]), scm__rc.d1786[320]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[359]), scm__rc.d1786[235]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[362]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[363]), scm__rc.d1786[59]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[364]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[365]), scm__rc.d1786[91]);
  scm__rc.d1786[321] = Scm_MakeExtendedPair(scm__rc.d1786[59], SCM_OBJ(&scm__rc.d1788[359]), SCM_OBJ(&scm__rc.d1788[367]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[322]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[322]))[4] = scm__rc.d1786[293];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[322]))[5] = scm__rc.d1786[293];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[322]))[6] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[322]))[7] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("sys-getservbyname")), SCM_OBJ(&libnetsys_getservbyname__STUB), 0);
  libnetsys_getservbyname__STUB.common.info = scm__rc.d1786[321];
  libnetsys_getservbyname__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[322]);
  scm__rc.d1786[330] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[131])),TRUE); /* port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[368]), scm__rc.d1786[330]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[371]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[372]), scm__rc.d1786[60]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[373]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[374]), scm__rc.d1786[91]);
  scm__rc.d1786[331] = Scm_MakeExtendedPair(scm__rc.d1786[60], SCM_OBJ(&scm__rc.d1788[368]), SCM_OBJ(&scm__rc.d1788[376]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[332]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[332]))[4] = scm__rc.d1786[93];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[332]))[5] = scm__rc.d1786[293];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[332]))[6] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[332]))[7] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("sys-getservbyport")), SCM_OBJ(&libnetsys_getservbyport__STUB), 0);
  libnetsys_getservbyport__STUB.common.info = scm__rc.d1786[331];
  libnetsys_getservbyport__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[332]);
  scm__rc.d1786[340] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[132])),TRUE); /* x */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[377]), scm__rc.d1786[340]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[380]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[381]), scm__rc.d1786[63]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[382]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[383]), scm__rc.d1786[91]);
  scm__rc.d1786[341] = Scm_MakeExtendedPair(scm__rc.d1786[63], SCM_OBJ(&scm__rc.d1788[377]), SCM_OBJ(&scm__rc.d1788[385]));
  scm__rc.d1786[342] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[133])),TRUE); /* <uint32> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[343]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[343]))[4] = scm__rc.d1786[342];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[343]))[5] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[343]))[6] = scm__rc.d1786[342];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("sys-ntohl")), SCM_OBJ(&libnetsys_ntohl__STUB), 0);
  libnetsys_ntohl__STUB.common.info = scm__rc.d1786[341];
  libnetsys_ntohl__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[343]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[388]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[389]), scm__rc.d1786[64]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[390]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[391]), scm__rc.d1786[91]);
  scm__rc.d1786[350] = Scm_MakeExtendedPair(scm__rc.d1786[64], SCM_OBJ(&scm__rc.d1788[377]), SCM_OBJ(&scm__rc.d1788[393]));
  scm__rc.d1786[351] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[134])),TRUE); /* <uint16> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[352]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[352]))[4] = scm__rc.d1786[351];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[352]))[5] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[352]))[6] = scm__rc.d1786[351];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("sys-ntohs")), SCM_OBJ(&libnetsys_ntohs__STUB), 0);
  libnetsys_ntohs__STUB.common.info = scm__rc.d1786[350];
  libnetsys_ntohs__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[352]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[396]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[397]), scm__rc.d1786[61]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[398]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[399]), scm__rc.d1786[91]);
  scm__rc.d1786[359] = Scm_MakeExtendedPair(scm__rc.d1786[61], SCM_OBJ(&scm__rc.d1788[377]), SCM_OBJ(&scm__rc.d1788[401]));
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("sys-htonl")), SCM_OBJ(&libnetsys_htonl__STUB), 0);
  libnetsys_htonl__STUB.common.info = scm__rc.d1786[359];
  libnetsys_htonl__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[343]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[404]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[405]), scm__rc.d1786[62]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[406]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[407]), scm__rc.d1786[91]);
  scm__rc.d1786[360] = Scm_MakeExtendedPair(scm__rc.d1786[62], SCM_OBJ(&scm__rc.d1788[377]), SCM_OBJ(&scm__rc.d1788[409]));
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("sys-htons")), SCM_OBJ(&libnetsys_htons__STUB), 0);
  libnetsys_htons__STUB.common.info = scm__rc.d1786[360];
  libnetsys_htons__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[352]);
#if defined(HAVE_IPV6)
  Scm_InitStaticClassWithMeta(&Scm_SysAddrinfoClass, "<sys-addrinfo>", SCM_MODULE(scm__rc.d1786[0]), NULL, SCM_FALSE, Scm_SysAddrinfoClass__SLOTS, 0);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1802[1])),TRUE); /* sys-getaddrinfo */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1802[2])),TRUE); /* nodename */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1802[3])),TRUE); /* servname */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1802[4])),TRUE); /* hints */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d2013[1]), scm__rc.d1801[4]);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d2013[2]), scm__rc.d1801[3]);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d2013[3]), scm__rc.d1801[2]);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[5] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1802[5])),TRUE); /* source-info */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d2013[6]), scm__rc.d1801[5]);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1802[7])),TRUE); /* bind-info */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1802[8])),TRUE); /* gauche.net */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d2013[7]), scm__rc.d1801[1]);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d2013[8]), scm__rc.d1801[7]);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d2013[9]), scm__rc.d1801[6]);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[8] = Scm_MakeExtendedPair(scm__rc.d1801[1], SCM_OBJ(&scm__rc.d2013[3]), SCM_OBJ(&scm__rc.d2013[11]));
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[9] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1802[9])),TRUE); /* <const-cstring>? */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[10] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1802[10])),TRUE); /* <top> */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[11] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1802[11])),TRUE); /* -> */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1801[12]))[3] = scm__rc.d1801[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1801[12]))[4] = scm__rc.d1801[9];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1801[12]))[5] = scm__rc.d1801[9];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1801[12]))[6] = scm__rc.d1801[10];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1801[12]))[7] = scm__rc.d1801[11];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1801[12]))[8] = scm__rc.d1801[10];
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("sys-getaddrinfo")), SCM_OBJ(&libnetsys_getaddrinfo__STUB), 0);
  libnetsys_getaddrinfo__STUB.common.info = scm__rc.d1801[8];
  libnetsys_getaddrinfo__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1801[12]);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[21] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1802[12])),TRUE); /* sys-getnameinfo */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[22] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1802[13])),TRUE); /* addr */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[23] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1802[14]))); /* :optional */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[24] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1802[15])),TRUE); /* flags */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d2013[12]), scm__rc.d1801[24]);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d2013[13]), scm__rc.d1801[23]);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d2013[14]), scm__rc.d1801[22]);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d2013[17]), scm__rc.d1801[5]);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d2013[18]), scm__rc.d1801[21]);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d2013[19]), scm__rc.d1801[7]);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d2013[20]), scm__rc.d1801[6]);
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[25] = Scm_MakeExtendedPair(scm__rc.d1801[21], SCM_OBJ(&scm__rc.d2013[14]), SCM_OBJ(&scm__rc.d2013[22]));
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[26] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1802[16])),TRUE); /* <socket-address> */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  scm__rc.d1801[27] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1802[17])),TRUE); /* * */
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1801[28]))[3] = scm__rc.d1801[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1801[28]))[4] = scm__rc.d1801[26];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1801[28]))[5] = scm__rc.d1801[27];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1801[28]))[6] = scm__rc.d1801[11];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1801[28]))[7] = scm__rc.d1801[10];
#endif /* defined(HAVE_IPV6) */
#if defined(HAVE_IPV6)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("sys-getnameinfo")), SCM_OBJ(&libnetsys_getnameinfo__STUB), 0);
  libnetsys_getnameinfo__STUB.common.info = scm__rc.d1801[25];
  libnetsys_getnameinfo__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1801[28]);
#endif /* defined(HAVE_IPV6) */
#if defined(AF_INET6)
  scm__rc.d2014[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2015[0])),TRUE); /* AF_INET6 */
#endif /* defined(AF_INET6) */
#if defined(AF_INET6)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2014[0]), Scm_MakeInteger(AF_INET6), SCM_BINDING_CONST);

#endif /* defined(AF_INET6) */
#if defined(PF_INET6)
  scm__rc.d2016[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2017[0])),TRUE); /* PF_INET6 */
#endif /* defined(PF_INET6) */
#if defined(PF_INET6)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2016[0]), Scm_MakeInteger(PF_INET6), SCM_BINDING_CONST);

#endif /* defined(PF_INET6) */
#if defined(IPPROTO_IPV6)
  scm__rc.d2018[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2019[0])),TRUE); /* IPPROTO_IPV6 */
#endif /* defined(IPPROTO_IPV6) */
#if defined(IPPROTO_IPV6)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2018[0]), Scm_MakeInteger(IPPROTO_IPV6), SCM_BINDING_CONST);

#endif /* defined(IPPROTO_IPV6) */
#if defined(IPV6_UNICAST_HOPS)
  scm__rc.d2020[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2021[0])),TRUE); /* IPV6_UNICAST_HOPS */
#endif /* defined(IPV6_UNICAST_HOPS) */
#if defined(IPV6_UNICAST_HOPS)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2020[0]), Scm_MakeInteger(IPV6_UNICAST_HOPS), SCM_BINDING_CONST);

#endif /* defined(IPV6_UNICAST_HOPS) */
#if defined(IPV6_MULTICAST_IF)
  scm__rc.d2022[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2023[0])),TRUE); /* IPV6_MULTICAST_IF */
#endif /* defined(IPV6_MULTICAST_IF) */
#if defined(IPV6_MULTICAST_IF)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2022[0]), Scm_MakeInteger(IPV6_MULTICAST_IF), SCM_BINDING_CONST);

#endif /* defined(IPV6_MULTICAST_IF) */
#if defined(IPV6_MULTICAST_HOPS)
  scm__rc.d2024[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2025[0])),TRUE); /* IPV6_MULTICAST_HOPS */
#endif /* defined(IPV6_MULTICAST_HOPS) */
#if defined(IPV6_MULTICAST_HOPS)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2024[0]), Scm_MakeInteger(IPV6_MULTICAST_HOPS), SCM_BINDING_CONST);

#endif /* defined(IPV6_MULTICAST_HOPS) */
#if defined(IPV6_MULTICAST_LOOP)
  scm__rc.d2026[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2027[0])),TRUE); /* IPV6_MULTICAST_LOOP */
#endif /* defined(IPV6_MULTICAST_LOOP) */
#if defined(IPV6_MULTICAST_LOOP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2026[0]), Scm_MakeInteger(IPV6_MULTICAST_LOOP), SCM_BINDING_CONST);

#endif /* defined(IPV6_MULTICAST_LOOP) */
#if defined(IPV6_JOIN_GROUP)
  scm__rc.d2028[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2029[0])),TRUE); /* IPV6_JOIN_GROUP */
#endif /* defined(IPV6_JOIN_GROUP) */
#if defined(IPV6_JOIN_GROUP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2028[0]), Scm_MakeInteger(IPV6_JOIN_GROUP), SCM_BINDING_CONST);

#endif /* defined(IPV6_JOIN_GROUP) */
#if defined(IPV6_LEAVE_GROUP)
  scm__rc.d2030[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2031[0])),TRUE); /* IPV6_LEAVE_GROUP */
#endif /* defined(IPV6_LEAVE_GROUP) */
#if defined(IPV6_LEAVE_GROUP)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2030[0]), Scm_MakeInteger(IPV6_LEAVE_GROUP), SCM_BINDING_CONST);

#endif /* defined(IPV6_LEAVE_GROUP) */
#if defined(IPV6_V6ONLY)
  scm__rc.d2032[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2033[0])),TRUE); /* IPV6_V6ONLY */
#endif /* defined(IPV6_V6ONLY) */
#if defined(IPV6_V6ONLY)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2032[0]), Scm_MakeInteger(IPV6_V6ONLY), SCM_BINDING_CONST);

#endif /* defined(IPV6_V6ONLY) */
#if defined(AI_PASSIVE)
  scm__rc.d2034[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2035[0])),TRUE); /* AI_PASSIVE */
#endif /* defined(AI_PASSIVE) */
#if defined(AI_PASSIVE)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2034[0]), Scm_MakeInteger(AI_PASSIVE), SCM_BINDING_CONST);

#endif /* defined(AI_PASSIVE) */
#if defined(AI_CANONNAME)
  scm__rc.d2036[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2037[0])),TRUE); /* AI_CANONNAME */
#endif /* defined(AI_CANONNAME) */
#if defined(AI_CANONNAME)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2036[0]), Scm_MakeInteger(AI_CANONNAME), SCM_BINDING_CONST);

#endif /* defined(AI_CANONNAME) */
#if defined(AI_NUMERICHOST)
  scm__rc.d2038[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2039[0])),TRUE); /* AI_NUMERICHOST */
#endif /* defined(AI_NUMERICHOST) */
#if defined(AI_NUMERICHOST)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2038[0]), Scm_MakeInteger(AI_NUMERICHOST), SCM_BINDING_CONST);

#endif /* defined(AI_NUMERICHOST) */
#if defined(AI_NUMERICSERV)
  scm__rc.d2040[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2041[0])),TRUE); /* AI_NUMERICSERV */
#endif /* defined(AI_NUMERICSERV) */
#if defined(AI_NUMERICSERV)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2040[0]), Scm_MakeInteger(AI_NUMERICSERV), SCM_BINDING_CONST);

#endif /* defined(AI_NUMERICSERV) */
#if defined(AI_V4MAPPED)
  scm__rc.d2042[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2043[0])),TRUE); /* AI_V4MAPPED */
#endif /* defined(AI_V4MAPPED) */
#if defined(AI_V4MAPPED)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2042[0]), Scm_MakeInteger(AI_V4MAPPED), SCM_BINDING_CONST);

#endif /* defined(AI_V4MAPPED) */
#if defined(AI_ALL)
  scm__rc.d2044[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2045[0])),TRUE); /* AI_ALL */
#endif /* defined(AI_ALL) */
#if defined(AI_ALL)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2044[0]), Scm_MakeInteger(AI_ALL), SCM_BINDING_CONST);

#endif /* defined(AI_ALL) */
#if defined(AI_ADDRCONFIG)
  scm__rc.d2046[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2047[0])),TRUE); /* AI_ADDRCONFIG */
#endif /* defined(AI_ADDRCONFIG) */
#if defined(AI_ADDRCONFIG)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2046[0]), Scm_MakeInteger(AI_ADDRCONFIG), SCM_BINDING_CONST);

#endif /* defined(AI_ADDRCONFIG) */
#if defined(NI_NOFQDN)
  scm__rc.d2048[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2049[0])),TRUE); /* NI_NOFQDN */
#endif /* defined(NI_NOFQDN) */
#if defined(NI_NOFQDN)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2048[0]), Scm_MakeInteger(NI_NOFQDN), SCM_BINDING_CONST);

#endif /* defined(NI_NOFQDN) */
#if defined(NI_NUMERICHOST)
  scm__rc.d2050[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2051[0])),TRUE); /* NI_NUMERICHOST */
#endif /* defined(NI_NUMERICHOST) */
#if defined(NI_NUMERICHOST)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2050[0]), Scm_MakeInteger(NI_NUMERICHOST), SCM_BINDING_CONST);

#endif /* defined(NI_NUMERICHOST) */
#if defined(NI_NAMEREQD)
  scm__rc.d2052[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2053[0])),TRUE); /* NI_NAMEREQD */
#endif /* defined(NI_NAMEREQD) */
#if defined(NI_NAMEREQD)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2052[0]), Scm_MakeInteger(NI_NAMEREQD), SCM_BINDING_CONST);

#endif /* defined(NI_NAMEREQD) */
#if defined(NI_NUMERICSERV)
  scm__rc.d2054[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2055[0])),TRUE); /* NI_NUMERICSERV */
#endif /* defined(NI_NUMERICSERV) */
#if defined(NI_NUMERICSERV)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2054[0]), Scm_MakeInteger(NI_NUMERICSERV), SCM_BINDING_CONST);

#endif /* defined(NI_NUMERICSERV) */
#if defined(NI_DGRAM)
  scm__rc.d2056[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d2057[0])),TRUE); /* NI_DGRAM */
#endif /* defined(NI_DGRAM) */
#if defined(NI_DGRAM)
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(scm__rc.d2056[0]), Scm_MakeInteger(NI_DGRAM), SCM_BINDING_CONST);

#endif /* defined(NI_DGRAM) */
  scm__rc.d1786[361] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[135])),TRUE); /* IPPROTO_IP */
  scm__rc.d1786[362] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[136])),TRUE); /* IPPROTO_ICMP */
  scm__rc.d1786[363] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[137])),TRUE); /* IPPROTO_TCP */
  scm__rc.d1786[364] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[138])),TRUE); /* IPPROTO_UDP */
  scm__rc.d1786[365] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[139])),TRUE); /* IPPROTO_IPV6 */
  scm__rc.d1786[366] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[140])),TRUE); /* IPPROTO_ICMPV6 */
  scm__rc.d1786[367] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[141])),TRUE); /* SOL_SOCKET */
  scm__rc.d1786[368] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[142])),TRUE); /* SOMAXCONN */
  scm__rc.d1786[369] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[143])),TRUE); /* SO_ACCEPTCONN */
  scm__rc.d1786[370] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[144])),TRUE); /* SO_BINDTODEVICE */
  scm__rc.d1786[371] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[145])),TRUE); /* SO_BROADCAST */
  scm__rc.d1786[372] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[146])),TRUE); /* SO_DEBUG */
  scm__rc.d1786[373] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[147])),TRUE); /* SO_DONTROUTE */
  scm__rc.d1786[374] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[148])),TRUE); /* SO_ERROR */
  scm__rc.d1786[375] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[149])),TRUE); /* SO_KEEPALIVE */
  scm__rc.d1786[376] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[150])),TRUE); /* SO_LINGER */
  scm__rc.d1786[377] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[151])),TRUE); /* SO_OOBINLINE */
  scm__rc.d1786[378] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[152])),TRUE); /* SO_PASSCRED */
  scm__rc.d1786[379] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[153])),TRUE); /* SO_PEERCRED */
  scm__rc.d1786[380] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[154])),TRUE); /* SO_PRIORITY */
  scm__rc.d1786[381] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[155])),TRUE); /* SO_RCVBUF */
  scm__rc.d1786[382] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[156])),TRUE); /* SO_RCVLOWAT */
  scm__rc.d1786[383] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[157])),TRUE); /* SO_RCVTIMEO */
  scm__rc.d1786[384] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[158])),TRUE); /* SO_REUSEADDR */
  scm__rc.d1786[385] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[159])),TRUE); /* SO_REUSEPORT */
  scm__rc.d1786[386] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[160])),TRUE); /* SO_SNDBUF */
  scm__rc.d1786[387] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[161])),TRUE); /* SO_SNDLOWAT */
  scm__rc.d1786[388] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[162])),TRUE); /* SO_SNDTIMEO */
  scm__rc.d1786[389] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[163])),TRUE); /* SO_TIMESTAMP */
  scm__rc.d1786[390] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[164])),TRUE); /* SO_TYPE */
  scm__rc.d1786[391] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[165])),TRUE); /* SOL_TCP */
  scm__rc.d1786[392] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[166])),TRUE); /* TCP_NODELAY */
  scm__rc.d1786[393] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[167])),TRUE); /* TCP_MAXSEG */
  scm__rc.d1786[394] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[168])),TRUE); /* TCP_CORK */
  scm__rc.d1786[395] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[169])),TRUE); /* SOL_IP */
  scm__rc.d1786[396] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[170])),TRUE); /* IP_OPTIONS */
  scm__rc.d1786[397] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[171])),TRUE); /* IP_PKTINFO */
  scm__rc.d1786[398] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[172])),TRUE); /* IP_RECVTOS */
  scm__rc.d1786[399] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[173])),TRUE); /* IP_RECVTTL */
  scm__rc.d1786[400] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[174])),TRUE); /* IP_RECVOPTS */
  scm__rc.d1786[401] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[175])),TRUE); /* IP_TOS */
  scm__rc.d1786[402] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[176])),TRUE); /* IP_TTL */
  scm__rc.d1786[403] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[177])),TRUE); /* IP_HDRINCL */
  scm__rc.d1786[404] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[178])),TRUE); /* IP_RECVERR */
  scm__rc.d1786[405] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[179])),TRUE); /* IP_MTU_DISCOVER */
  scm__rc.d1786[406] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[180])),TRUE); /* IP_MTU */
  scm__rc.d1786[407] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[181])),TRUE); /* IP_ROUTER_ALERT */
  scm__rc.d1786[408] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[182])),TRUE); /* IP_MULTICAST_TTL */
  scm__rc.d1786[409] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[183])),TRUE); /* IP_MULTICAST_LOOP */
  scm__rc.d1786[410] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[184])),TRUE); /* IP_ADD_MEMBERSHIP */
  scm__rc.d1786[411] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[185])),TRUE); /* IP_DROP_MEMBERSHIP */
  scm__rc.d1786[412] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[186])),TRUE); /* IP_MULTICAST_IF */
  scm__rc.d1786[413] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[187])),TRUE); /* MSG_CTRUNC */
  scm__rc.d1786[414] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[188])),TRUE); /* MSG_DONTROUTE */
  scm__rc.d1786[415] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[189])),TRUE); /* MSG_EOR */
  scm__rc.d1786[416] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[190])),TRUE); /* MSG_OOB */
  scm__rc.d1786[417] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[191])),TRUE); /* MSG_PEEK */
  scm__rc.d1786[418] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[192])),TRUE); /* MSG_TRUNC */
  scm__rc.d1786[419] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[193])),TRUE); /* MSG_WAITALL */
  scm__rc.d1786[420] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[194])),TRUE); /* SIOCGIFNAME */
  scm__rc.d1786[421] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[195])),TRUE); /* SIOCSIFNAME */
  scm__rc.d1786[422] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[196])),TRUE); /* SIOCGIFINDEX */
  scm__rc.d1786[423] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[197])),TRUE); /* SIOCGIFFLAGS */
  scm__rc.d1786[424] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[198])),TRUE); /* SIOCSIFFLAGS */
  scm__rc.d1786[425] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[199])),TRUE); /* SIOCGIFMETRIC */
  scm__rc.d1786[426] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[200])),TRUE); /* SIOCSIFMETRIC */
  scm__rc.d1786[427] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[201])),TRUE); /* SIOCGIFMTU */
  scm__rc.d1786[428] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[202])),TRUE); /* SIOCSIFMTU */
  scm__rc.d1786[429] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[203])),TRUE); /* SIOCGIFHWADDR */
  scm__rc.d1786[430] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[204])),TRUE); /* SIOCSIFHWADDR */
  scm__rc.d1786[431] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[205])),TRUE); /* SIOCSIFHWBROADCAST */
  scm__rc.d1786[432] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[206])),TRUE); /* SIOCGIFMAP */
  scm__rc.d1786[433] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[207])),TRUE); /* SIOCSIFMAP */
  scm__rc.d1786[434] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[208])),TRUE); /* SIOCADDMULTI */
  scm__rc.d1786[435] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[209])),TRUE); /* SIOCDELMULTI */
  scm__rc.d1786[436] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[210])),TRUE); /* SIOGIFTXQLEN */
  scm__rc.d1786[437] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[211])),TRUE); /* SIOSIFTXQLEN */
  scm__rc.d1786[438] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[212])),TRUE); /* SIOCGIFCONF */
  scm__rc.d1786[439] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[213])),TRUE); /* SIOCGIFADDR */
  scm__rc.d1786[440] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[214])),TRUE); /* SIOCSIFADDR */
  scm__rc.d1786[441] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[215])),TRUE); /* SIOCGIFDSTADDR */
  scm__rc.d1786[442] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[216])),TRUE); /* SIOCSIFDSTADDR */
  scm__rc.d1786[443] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[217])),TRUE); /* SIOCGIFBRDADDR */
  scm__rc.d1786[444] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[218])),TRUE); /* SIOCSIFBRDADDR */
  scm__rc.d1786[445] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[219])),TRUE); /* SIOCGIFNETMASK */
  scm__rc.d1786[446] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[220])),TRUE); /* SIOCSIFNETMASK */
  scm__rc.d1786[447] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[221])),TRUE); /* IFF_UP */
  scm__rc.d1786[448] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[222])),TRUE); /* IFF_BROADCAST */
  scm__rc.d1786[449] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[223])),TRUE); /* IFF_DEBUG */
  scm__rc.d1786[450] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[224])),TRUE); /* IFF_LOOPBACK */
  scm__rc.d1786[451] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[225])),TRUE); /* IFF_POINTTOPOINT */
  scm__rc.d1786[452] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[226])),TRUE); /* IFF_RUNNING */
  scm__rc.d1786[453] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[227])),TRUE); /* IFF_NOARP */
  scm__rc.d1786[454] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[228])),TRUE); /* IFF_PROMISC */
  scm__rc.d1786[455] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[229])),TRUE); /* IFF_NOTRAILERS */
  scm__rc.d1786[456] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[230])),TRUE); /* IFF_ALLMULTI */
  scm__rc.d1786[457] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[231])),TRUE); /* IFF_MASTER */
  scm__rc.d1786[458] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[232])),TRUE); /* IFF_SLAVE */
  scm__rc.d1786[459] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[233])),TRUE); /* IFF_MULTICAST */
  scm__rc.d1786[460] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[234])),TRUE); /* IFF_PORTSEL */
  scm__rc.d1786[461] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[235])),TRUE); /* IFF_AUTOMEDIA */
  scm__rc.d1786[462] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[236])),TRUE); /* IFF_DYNAMIC */
  scm__rc.d1786[463] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[237])),TRUE); /* PF_INET6 */
  scm__rc.d1786[464] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[238])),TRUE); /* AF_INET6 */
  scm__rc.d1786[465] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[239])),TRUE); /* <sockaddr-in6> */
  scm__rc.d1786[466] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[240])),TRUE); /* <sys-addrinfo> */
  scm__rc.d1786[467] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[241])),TRUE); /* sys-getaddrinfo */
  scm__rc.d1786[468] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[242])),TRUE); /* make-sys-addrinfo */
  scm__rc.d1786[469] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[243])),TRUE); /* AI_PASSIVE */
  scm__rc.d1786[470] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[244])),TRUE); /* AI_CANONNAME */
  scm__rc.d1786[471] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[245])),TRUE); /* AI_NUMERICHOST */
  scm__rc.d1786[472] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[246])),TRUE); /* AI_NUMERICSERV */
  scm__rc.d1786[473] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[247])),TRUE); /* AI_V4MAPPED */
  scm__rc.d1786[474] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[248])),TRUE); /* AI_ALL */
  scm__rc.d1786[475] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[249])),TRUE); /* AI_ADDRCONFIG */
  scm__rc.d1786[476] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[250])),TRUE); /* IPV6_UNICAST_HOPS */
  scm__rc.d1786[477] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[251])),TRUE); /* IPV6_MULTICAST_IF */
  scm__rc.d1786[478] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[252])),TRUE); /* IPV6_MULTICAST_HOPS */
  scm__rc.d1786[479] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[253])),TRUE); /* IPV6_MULTICAST_LOOP */
  scm__rc.d1786[480] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[254])),TRUE); /* IPV6_JOIN_GROUP */
  scm__rc.d1786[481] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[255])),TRUE); /* IPV6_LEAVE_GROUP */
  scm__rc.d1786[482] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[256])),TRUE); /* IPV6_V6ONLY */
  scm__rc.d1786[483] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[257])),TRUE); /* sys-getnameinfo */
  scm__rc.d1786[484] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[258])),TRUE); /* NI_NOFQDN */
  scm__rc.d1786[485] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[259])),TRUE); /* NI_NUMERICHOST */
  scm__rc.d1786[486] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[260])),TRUE); /* NI_NAMEREQD */
  scm__rc.d1786[487] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[261])),TRUE); /* NI_NUMERICSERV */
  scm__rc.d1786[488] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[262])),TRUE); /* NI_DGRAM */
export_bindings();
  scm__rc.d1786[489] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[263])),TRUE); /* size */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[410]), scm__rc.d1786[489]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[411]), scm__rc.d1786[210]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[414]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[415]), scm__rc.d1786[65]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[416]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[417]), scm__rc.d1786[91]);
  scm__rc.d1786[490] = Scm_MakeExtendedPair(scm__rc.d1786[65], SCM_OBJ(&scm__rc.d1788[411]), SCM_OBJ(&scm__rc.d1788[419]));
  scm__rc.d1786[491] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[264])),TRUE); /* <uint> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[492]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[492]))[4] = scm__rc.d1786[212];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[492]))[5] = scm__rc.d1786[241];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[492]))[6] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[492]))[7] = scm__rc.d1786[491];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("inet-checksum")), SCM_OBJ(&libnetinet_checksum__STUB), 0);
  libnetinet_checksum__STUB.common.info = scm__rc.d1786[490];
  libnetinet_checksum__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[492]);
  scm__rc.d1786[500] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[265])),TRUE); /* s */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[420]), scm__rc.d1786[500]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[423]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[424]), scm__rc.d1786[66]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[425]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[426]), scm__rc.d1786[91]);
  scm__rc.d1786[501] = Scm_MakeExtendedPair(scm__rc.d1786[66], SCM_OBJ(&scm__rc.d1788[420]), SCM_OBJ(&scm__rc.d1788[428]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[502]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[502]))[4] = scm__rc.d1786[293];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[502]))[5] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[502]))[6] = scm__rc.d1786[96];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[502]))[7] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("inet-string->address")), SCM_OBJ(&libnetinet_string_TOaddress__STUB), 0);
  libnetinet_string_TOaddress__STUB.common.info = scm__rc.d1786[501];
  libnetinet_string_TOaddress__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[502]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[429]), scm__rc.d1786[500]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[432]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[433]), scm__rc.d1786[67]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[434]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[435]), scm__rc.d1786[91]);
  scm__rc.d1786[510] = Scm_MakeExtendedPair(scm__rc.d1786[67], SCM_OBJ(&scm__rc.d1788[429]), SCM_OBJ(&scm__rc.d1788[437]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[511]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[511]))[4] = scm__rc.d1786[293];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[511]))[5] = scm__rc.d1786[212];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[511]))[6] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[511]))[7] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("inet-string->address!")), SCM_OBJ(&libnetinet_string_TOaddressX__STUB), 0);
  libnetinet_string_TOaddressX__STUB.common.info = scm__rc.d1786[510];
  libnetinet_string_TOaddressX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[511]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[438]), scm__rc.d1786[149]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[441]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[442]), scm__rc.d1786[68]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[443]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[444]), scm__rc.d1786[91]);
  scm__rc.d1786[519] = Scm_MakeExtendedPair(scm__rc.d1786[68], SCM_OBJ(&scm__rc.d1788[438]), SCM_OBJ(&scm__rc.d1788[446]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[520]))[3] = scm__rc.d1786[1];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[520]))[4] = scm__rc.d1786[96];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[520]))[5] = scm__rc.d1786[241];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[520]))[6] = scm__rc.d1786[95];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[520]))[7] = scm__rc.d1786[96];
  Scm_MakeBinding(SCM_MODULE(scm__rc.d1786[0]), SCM_SYMBOL(SCM_INTERN("inet-address->string")), SCM_OBJ(&libnetinet_address_TOstring__STUB), 0);
  libnetinet_address_TOstring__STUB.common.info = scm__rc.d1786[519];
  libnetinet_address_TOstring__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[520]);
  scm__rc.d1786[529] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[266])),TRUE); /* %expression-name-mark-key */
  scm__rc.d1786[528] = Scm_MakeIdentifier(scm__rc.d1786[529], SCM_MODULE(Scm_GaucheInternalModule()), SCM_NIL); /* gauche.internal#%expression-name-mark-key */
  scm__rc.d1786[530] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[267])),TRUE); /* DEFAULT_BACKLOG */
  scm__rc.d1786[531] = Scm_MakeIdentifier(scm__rc.d1786[530], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#DEFAULT_BACKLOG */
  scm__rc.d1786[532] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[1])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[1]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[1]))->debugInfo = scm__rc.d1786[532];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[14]))[3] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[14]))[6] = SCM_WORD(scm__rc.d1786[530]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[14]))[12] = SCM_WORD(scm__rc.d1786[531]);
  scm__rc.d1786[533] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[268])),TRUE); /* ipv6-capable */
  scm__rc.d1786[535] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[269])),TRUE); /* module-binds? */
  scm__rc.d1786[534] = Scm_MakeIdentifier(scm__rc.d1786[535], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#module-binds? */
  scm__rc.d1786[536] = Scm_MakeIdentifier(scm__rc.d1786[533], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#ipv6-capable */
  scm__rc.d1786[537] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[2])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[2]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[2]))->debugInfo = scm__rc.d1786[537];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[28]))[3] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[28]))[6] = SCM_WORD(scm__rc.d1786[533]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[28]))[10] = SCM_WORD(scm__rc.d1786[1]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[28]))[12] = SCM_WORD(scm__rc.d1786[467]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[28]))[14] = SCM_WORD(scm__rc.d1786[534]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[28]))[17] = SCM_WORD(scm__rc.d1786[536]);
  scm__rc.d1786[538] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[270])),TRUE); /* ipv4-preferred */
  scm__rc.d1786[539] = Scm_MakeIdentifier(scm__rc.d1786[538], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#ipv4-preferred */
  scm__rc.d1786[540] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[3])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[3]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[3]))->debugInfo = scm__rc.d1786[540];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[47]))[3] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[47]))[6] = SCM_WORD(scm__rc.d1786[538]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[47]))[11] = SCM_WORD(scm__rc.d1786[539]);
  scm__rc.d1786[542] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[271])),TRUE); /* undefined? */
  scm__rc.d1786[541] = Scm_MakeIdentifier(scm__rc.d1786[542], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#undefined? */
  scm__rc.d1786[543] = Scm_MakeIdentifier(scm__rc.d1786[7], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#AF_UNSPEC */
  scm__rc.d1786[544] = Scm_MakeIdentifier(scm__rc.d1786[466], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#<sys-addrinfo> */
  scm__rc.d1786[545] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[109]))); /* :flags */
  scm__rc.d1786[547] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[272])),TRUE); /* list? */
  scm__rc.d1786[546] = Scm_MakeIdentifier(scm__rc.d1786[547], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#list? */
  scm__rc.d1786[549] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[273])),TRUE); /* logior */
  scm__rc.d1786[548] = Scm_MakeIdentifier(scm__rc.d1786[549], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#logior */
  scm__rc.d1786[550] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[274]))); /* :family */
  scm__rc.d1786[551] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[275]))); /* :socktype */
  scm__rc.d1786[552] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[85]))); /* :protocol */
  scm__rc.d1786[554] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[276])),TRUE); /* make */
  scm__rc.d1786[553] = Scm_MakeIdentifier(scm__rc.d1786[554], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#make */
  scm__rc.d1786[556] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[278])),TRUE); /* error */
  scm__rc.d1786[555] = Scm_MakeIdentifier(scm__rc.d1786[556], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#error */
  scm__rc.d1786[557] = Scm_MakeIdentifier(scm__rc.d1786[556], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#error */
  scm__rc.d1786[559] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[280])),TRUE); /* unwrap-syntax-1 */
  scm__rc.d1786[558] = Scm_MakeIdentifier(scm__rc.d1786[559], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#unwrap-syntax-1 */
  scm__rc.d1786[561] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[282])),TRUE); /* errorf */
  scm__rc.d1786[560] = Scm_MakeIdentifier(scm__rc.d1786[561], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#errorf */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[448]), scm__rc.d1786[175]);
  scm__rc.d1786[562] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[274])),TRUE); /* family */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[449]), scm__rc.d1786[7]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[450]), scm__rc.d1786[562]);
  scm__rc.d1786[563] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[275])),TRUE); /* socktype */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[451]), scm__rc.d1786[563]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[452]), scm__rc.d1786[89]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[457]), scm__rc.d1786[133]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[460]), scm__rc.d1786[90]);
  scm__rc.d1786[564] = Scm_MakeExtendedPair(scm__rc.d1786[468], SCM_OBJ(&scm__rc.d1788[457]), SCM_OBJ(&scm__rc.d1788[461]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[462]), scm__rc.d1786[564]);
  scm__rc.d1786[565] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[4])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[4]))->name = scm__rc.d1786[468];/* make-sys-addrinfo */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[4]))->debugInfo = scm__rc.d1786[565];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[16] = SCM_WORD(scm__rc.d1786[541]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[28] = SCM_WORD(scm__rc.d1786[541]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[32] = SCM_WORD(scm__rc.d1786[543]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[41] = SCM_WORD(scm__rc.d1786[541]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[53] = SCM_WORD(scm__rc.d1786[541]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[62] = SCM_WORD(scm__rc.d1786[536]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[66] = SCM_WORD(scm__rc.d1786[544]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[68] = SCM_WORD(scm__rc.d1786[545]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[73] = SCM_WORD(scm__rc.d1786[546]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[79] = SCM_WORD(scm__rc.d1786[548]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[87] = SCM_WORD(scm__rc.d1786[550]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[90] = SCM_WORD(scm__rc.d1786[551]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[93] = SCM_WORD(scm__rc.d1786[552]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[96] = SCM_WORD(scm__rc.d1786[553]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[101] = SCM_WORD(scm__rc.d1786[555]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[111] = SCM_WORD(scm__rc.d1786[557]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[118] = SCM_WORD(scm__rc.d1786[558]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[122] = SCM_WORD(scm__rc.d1786[545]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[136] = SCM_WORD(scm__rc.d1786[550]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[150] = SCM_WORD(scm__rc.d1786[551]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[164] = SCM_WORD(scm__rc.d1786[552]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[60]))[183] = SCM_WORD(scm__rc.d1786[560]);
  scm__rc.d1786[566] = Scm_MakeIdentifier(scm__rc.d1786[468], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#make-sys-addrinfo */
  scm__rc.d1786[567] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[5])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[5]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[5]))->debugInfo = scm__rc.d1786[567];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[254]))[3] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[254]))[6] = SCM_WORD(scm__rc.d1786[468]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[254]))[13] = SCM_WORD(scm__rc.d1786[566]);
  scm__rc.d1786[568] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[283])),TRUE); /* address->protocol-family */
  scm__rc.d1786[569] = Scm_MakeIdentifier(scm__rc.d1786[45], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#sockaddr-family */
  scm__rc.d1786[570] = Scm_MakeIdentifier(scm__rc.d1786[5], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#PF_UNIX */
  scm__rc.d1786[571] = Scm_MakeIdentifier(scm__rc.d1786[6], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#PF_INET */
  scm__rc.d1786[572] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[284])),TRUE); /* inet6 */
  scm__rc.d1786[573] = Scm_MakeIdentifier(scm__rc.d1786[463], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#PF_INET6 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[465]), scm__rc.d1786[90]);
  scm__rc.d1786[574] = Scm_MakeExtendedPair(scm__rc.d1786[568], SCM_OBJ(&scm__rc.d1788[151]), SCM_OBJ(&scm__rc.d1788[466]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[467]), scm__rc.d1786[574]);
  scm__rc.d1786[575] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[6])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[6]))->name = scm__rc.d1786[568];/* address->protocol-family */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[6]))->debugInfo = scm__rc.d1786[575];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[269]))[4] = SCM_WORD(scm__rc.d1786[569]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[269]))[8] = SCM_WORD(scm__rc.d1786[84]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[269]))[11] = SCM_WORD(scm__rc.d1786[570]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[269]))[15] = SCM_WORD(scm__rc.d1786[85]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[269]))[18] = SCM_WORD(scm__rc.d1786[571]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[269]))[22] = SCM_WORD(scm__rc.d1786[572]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[269]))[25] = SCM_WORD(scm__rc.d1786[573]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[269]))[31] = SCM_WORD(scm__rc.d1786[555]);
  scm__rc.d1786[576] = Scm_MakeIdentifier(scm__rc.d1786[568], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#address->protocol-family */
  scm__rc.d1786[577] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[7])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[7]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[7]))->debugInfo = scm__rc.d1786[577];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[302]))[3] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[302]))[6] = SCM_WORD(scm__rc.d1786[568]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[302]))[13] = SCM_WORD(scm__rc.d1786[576]);
  scm__rc.d1786[579] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[287])),TRUE); /* make-client-socket-unix */
  scm__rc.d1786[578] = Scm_MakeIdentifier(scm__rc.d1786[579], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#make-client-socket-unix */
  scm__rc.d1786[581] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[288])),TRUE); /* integer? */
  scm__rc.d1786[580] = Scm_MakeIdentifier(scm__rc.d1786[581], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#integer? */
  scm__rc.d1786[582] = Scm_MakeIdentifier(scm__rc.d1786[561], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#errorf */
  scm__rc.d1786[584] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[290])),TRUE); /* make-client-socket-inet */
  scm__rc.d1786[583] = Scm_MakeIdentifier(scm__rc.d1786[584], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#make-client-socket-inet */
  scm__rc.d1786[585] = Scm_MakeIdentifier(scm__rc.d1786[40], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#<sockaddr> */
  scm__rc.d1786[587] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[291])),TRUE); /* make-client-socket-from-addr */
  scm__rc.d1786[586] = Scm_MakeIdentifier(scm__rc.d1786[587], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#make-client-socket-from-addr */
  scm__rc.d1786[588] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[293])),TRUE); /* args */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[468]), scm__rc.d1786[320]);
  SCM_SET_CDR(SCM_OBJ(&scm__rc.d1788[468]), scm__rc.d1786[588]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[471]), scm__rc.d1786[90]);
  scm__rc.d1786[589] = Scm_MakeExtendedPair(scm__rc.d1786[48], SCM_OBJ(&scm__rc.d1788[468]), SCM_OBJ(&scm__rc.d1788[472]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[473]), scm__rc.d1786[589]);
  scm__rc.d1786[590] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[8])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[8]))->name = scm__rc.d1786[48];/* make-client-socket */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[8]))->debugInfo = scm__rc.d1786[590];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]))[2] = SCM_WORD(scm__rc.d1786[84]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]))[24] = SCM_WORD(scm__rc.d1786[555]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]))[27] = SCM_WORD(scm__rc.d1786[578]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]))[31] = SCM_WORD(scm__rc.d1786[85]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]))[65] = SCM_WORD(scm__rc.d1786[580]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]))[83] = SCM_WORD(scm__rc.d1786[582]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]))[91] = SCM_WORD(scm__rc.d1786[583]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]))[95] = SCM_WORD(scm__rc.d1786[585]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]))[101] = SCM_WORD(scm__rc.d1786[586]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]))[115] = SCM_WORD(scm__rc.d1786[580]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]))[121] = SCM_WORD(scm__rc.d1786[583]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[317]))[127] = SCM_WORD(scm__rc.d1786[555]);
  scm__rc.d1786[591] = Scm_MakeIdentifier(scm__rc.d1786[48], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#make-client-socket */
  scm__rc.d1786[592] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[9])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[9]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[9]))->debugInfo = scm__rc.d1786[592];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[452]))[3] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[452]))[6] = SCM_WORD(scm__rc.d1786[48]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[452]))[13] = SCM_WORD(scm__rc.d1786[591]);
  scm__rc.d1786[593] = Scm_MakeIdentifier(scm__rc.d1786[10], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#SOCK_STREAM */
  scm__rc.d1786[594] = Scm_MakeIdentifier(scm__rc.d1786[3], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#make-socket */
  scm__rc.d1786[595] = Scm_MakeIdentifier(scm__rc.d1786[23], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#socket-connect */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[476]), scm__rc.d1786[90]);
  scm__rc.d1786[596] = Scm_MakeExtendedPair(scm__rc.d1786[587], SCM_OBJ(&scm__rc.d1788[151]), SCM_OBJ(&scm__rc.d1788[477]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[478]), scm__rc.d1786[596]);
  scm__rc.d1786[597] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[10])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[10]))->name = scm__rc.d1786[587];/* make-client-socket-from-addr */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[10]))->debugInfo = scm__rc.d1786[597];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[467]))[6] = SCM_WORD(scm__rc.d1786[576]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[467]))[8] = SCM_WORD(scm__rc.d1786[593]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[467]))[10] = SCM_WORD(scm__rc.d1786[594]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[467]))[17] = SCM_WORD(scm__rc.d1786[595]);
  scm__rc.d1786[598] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[11])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[11]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[11]))->debugInfo = scm__rc.d1786[598];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[486]))[3] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[486]))[6] = SCM_WORD(scm__rc.d1786[587]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[486]))[13] = SCM_WORD(scm__rc.d1786[586]);
  scm__rc.d1786[599] = Scm_MakeIdentifier(scm__rc.d1786[42], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#<sockaddr-un> */
  scm__rc.d1786[600] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[294]))); /* :path */
  scm__rc.d1786[601] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[294])),TRUE); /* path */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[479]), scm__rc.d1786[601]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[482]), scm__rc.d1786[90]);
  scm__rc.d1786[602] = Scm_MakeExtendedPair(scm__rc.d1786[579], SCM_OBJ(&scm__rc.d1788[479]), SCM_OBJ(&scm__rc.d1788[483]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[484]), scm__rc.d1786[602]);
  scm__rc.d1786[603] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[12])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[12]))->name = scm__rc.d1786[579];/* make-client-socket-unix */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[12]))->debugInfo = scm__rc.d1786[603];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[501]))[3] = SCM_WORD(scm__rc.d1786[570]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[501]))[5] = SCM_WORD(scm__rc.d1786[593]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[501]))[7] = SCM_WORD(scm__rc.d1786[594]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[501]))[15] = SCM_WORD(scm__rc.d1786[599]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[501]))[17] = SCM_WORD(scm__rc.d1786[600]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[501]))[20] = SCM_WORD(scm__rc.d1786[553]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[501]))[22] = SCM_WORD(scm__rc.d1786[595]);
  scm__rc.d1786[604] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[13])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[13]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[13]))->debugInfo = scm__rc.d1786[604];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[525]))[3] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[525]))[6] = SCM_WORD(scm__rc.d1786[579]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[525]))[13] = SCM_WORD(scm__rc.d1786[578]);
  scm__rc.d1786[605] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[295])),TRUE); /* try-connect */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[486]), scm__rc.d1786[605]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[487]), scm__rc.d1786[584]);
  scm__rc.d1786[606] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[296])),TRUE); /* e */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[488]), scm__rc.d1786[606]);
  scm__rc.d1786[607] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[14])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[14]))->debugInfo = scm__rc.d1786[607];
  scm__rc.d1786[608] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[15])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[15]))->debugInfo = scm__rc.d1786[608];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[543]))[6] = SCM_WORD(scm__rc.d1786[576]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[543]))[8] = SCM_WORD(scm__rc.d1786[593]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[543]))[10] = SCM_WORD(scm__rc.d1786[594]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[543]))[17] = SCM_WORD(scm__rc.d1786[595]);
  scm__rc.d1786[609] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[297]))); /* :rewind-before */
  scm__rc.d1786[611] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[298])),TRUE); /* with-error-handler */
  scm__rc.d1786[610] = Scm_MakeIdentifier(scm__rc.d1786[611], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#with-error-handler */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[493]), scm__rc.d1786[605]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[494]), scm__rc.d1786[584]);
  scm__rc.d1786[612] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[299])),TRUE); /* address */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[495]), scm__rc.d1786[612]);
  scm__rc.d1786[613] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[16])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[16]))->name = scm__rc.d1786[605];/* (make-client-socket-inet try-connect) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[16]))->debugInfo = scm__rc.d1786[613];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[562]))[7] = SCM_WORD(scm__rc.d1786[609]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[562]))[11] = SCM_WORD(scm__rc.d1786[610]);
  scm__rc.d1786[614] = Scm_MakeIdentifier(scm__rc.d1786[43], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#make-sockaddrs */
  scm__rc.d1786[616] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[300])),TRUE); /* any */
  scm__rc.d1786[615] = Scm_MakeIdentifier(scm__rc.d1786[616], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#any */
  scm__rc.d1786[618] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[301])),TRUE); /* raise */
  scm__rc.d1786[617] = Scm_MakeIdentifier(scm__rc.d1786[618], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#raise */
  scm__rc.d1786[619] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[302])),TRUE); /* host */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[499]), scm__rc.d1786[330]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[500]), scm__rc.d1786[619]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[503]), scm__rc.d1786[90]);
  scm__rc.d1786[620] = Scm_MakeExtendedPair(scm__rc.d1786[584], SCM_OBJ(&scm__rc.d1788[500]), SCM_OBJ(&scm__rc.d1788[504]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[505]), scm__rc.d1786[620]);
  scm__rc.d1786[621] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[17])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[17]))->name = scm__rc.d1786[584];/* make-client-socket-inet */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[17]))->debugInfo = scm__rc.d1786[621];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[575]))[13] = SCM_WORD(scm__rc.d1786[614]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[575]))[15] = SCM_WORD(scm__rc.d1786[615]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[575]))[26] = SCM_WORD(scm__rc.d1786[617]);
  scm__rc.d1786[622] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[18])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[18]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[18]))->debugInfo = scm__rc.d1786[622];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[603]))[3] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[603]))[6] = SCM_WORD(scm__rc.d1786[584]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[603]))[13] = SCM_WORD(scm__rc.d1786[583]);
  scm__rc.d1786[624] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[303])),TRUE); /* make-server-socket-unix */
  scm__rc.d1786[623] = Scm_MakeIdentifier(scm__rc.d1786[624], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#make-server-socket-unix */
  scm__rc.d1786[626] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[304])),TRUE); /* string? */
  scm__rc.d1786[625] = Scm_MakeIdentifier(scm__rc.d1786[626], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#string? */
  scm__rc.d1786[628] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[305])),TRUE); /* any-pred */
  scm__rc.d1786[627] = Scm_MakeIdentifier(scm__rc.d1786[628], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#any-pred */
  scm__rc.d1786[630] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[306])),TRUE); /* every */
  scm__rc.d1786[629] = Scm_MakeIdentifier(scm__rc.d1786[630], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#every */
  scm__rc.d1786[632] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[308])),TRUE); /* make-server-socket-inet* */
  scm__rc.d1786[631] = Scm_MakeIdentifier(scm__rc.d1786[632], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#make-server-socket-inet* */
  scm__rc.d1786[634] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[309])),TRUE); /* make-server-socket-inet */
  scm__rc.d1786[633] = Scm_MakeIdentifier(scm__rc.d1786[634], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#make-server-socket-inet */
  scm__rc.d1786[636] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[310])),TRUE); /* make-server-socket-from-addr */
  scm__rc.d1786[635] = Scm_MakeIdentifier(scm__rc.d1786[636], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#make-server-socket-from-addr */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[508]), scm__rc.d1786[90]);
  scm__rc.d1786[637] = Scm_MakeExtendedPair(scm__rc.d1786[49], SCM_OBJ(&scm__rc.d1788[468]), SCM_OBJ(&scm__rc.d1788[509]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[510]), scm__rc.d1786[637]);
  scm__rc.d1786[638] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[19])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[19]))->name = scm__rc.d1786[49];/* make-server-socket */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[19]))->debugInfo = scm__rc.d1786[638];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[2] = SCM_WORD(scm__rc.d1786[84]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[24] = SCM_WORD(scm__rc.d1786[555]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[26] = SCM_WORD(scm__rc.d1786[623]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[33] = SCM_WORD(scm__rc.d1786[85]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[47] = SCM_WORD(scm__rc.d1786[546]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[55] = SCM_WORD(scm__rc.d1786[580]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[57] = SCM_WORD(scm__rc.d1786[625]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[59] = SCM_WORD(scm__rc.d1786[627]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[63] = SCM_WORD(scm__rc.d1786[629]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[74] = SCM_WORD(scm__rc.d1786[555]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[76] = SCM_WORD(scm__rc.d1786[631]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[85] = SCM_WORD(scm__rc.d1786[580]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[89] = SCM_WORD(scm__rc.d1786[633]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[102] = SCM_WORD(scm__rc.d1786[555]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[106] = SCM_WORD(scm__rc.d1786[585]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[111] = SCM_WORD(scm__rc.d1786[635]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[120] = SCM_WORD(scm__rc.d1786[580]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[124] = SCM_WORD(scm__rc.d1786[633]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[618]))[133] = SCM_WORD(scm__rc.d1786[555]);
  scm__rc.d1786[639] = Scm_MakeIdentifier(scm__rc.d1786[49], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#make-server-socket */
  scm__rc.d1786[640] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[20])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[20]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[20]))->debugInfo = scm__rc.d1786[640];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[753]))[3] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[753]))[6] = SCM_WORD(scm__rc.d1786[49]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[753]))[13] = SCM_WORD(scm__rc.d1786[639]);
  scm__rc.d1786[642] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[311])),TRUE); /* procedure? */
  scm__rc.d1786[641] = Scm_MakeIdentifier(scm__rc.d1786[642], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#procedure? */
  scm__rc.d1786[643] = Scm_MakeIdentifier(scm__rc.d1786[367], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#SOL_SOCKET */
  scm__rc.d1786[644] = Scm_MakeIdentifier(scm__rc.d1786[384], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#SO_REUSEADDR */
  scm__rc.d1786[645] = Scm_MakeIdentifier(scm__rc.d1786[27], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#socket-setsockopt */
  scm__rc.d1786[646] = Scm_MakeIdentifier(scm__rc.d1786[22], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#socket-bind */
  scm__rc.d1786[647] = Scm_MakeIdentifier(scm__rc.d1786[25], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#socket-listen */
  scm__rc.d1786[648] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[312]))); /* :reuse-addr? */
  scm__rc.d1786[649] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[313]))); /* :sock-init */
  scm__rc.d1786[650] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[107]))); /* :backlog */
  scm__rc.d1786[651] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[312])),TRUE); /* reuse-addr? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[511]), scm__rc.d1786[651]);
  scm__rc.d1786[652] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[313])),TRUE); /* sock-init */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[512]), scm__rc.d1786[652]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[513]), scm__rc.d1786[530]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[514]), scm__rc.d1786[160]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[518]), scm__rc.d1786[133]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[519]), scm__rc.d1786[149]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[522]), scm__rc.d1786[90]);
  scm__rc.d1786[653] = Scm_MakeExtendedPair(scm__rc.d1786[636], SCM_OBJ(&scm__rc.d1788[519]), SCM_OBJ(&scm__rc.d1788[523]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[524]), scm__rc.d1786[653]);
  scm__rc.d1786[654] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[21])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[21]))->name = scm__rc.d1786[636];/* make-server-socket-from-addr */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[21]))->debugInfo = scm__rc.d1786[654];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[14] = SCM_WORD(scm__rc.d1786[541]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[26] = SCM_WORD(scm__rc.d1786[541]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[38] = SCM_WORD(scm__rc.d1786[541]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[52] = SCM_WORD(scm__rc.d1786[576]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[54] = SCM_WORD(scm__rc.d1786[593]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[56] = SCM_WORD(scm__rc.d1786[594]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[62] = SCM_WORD(scm__rc.d1786[641]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[80] = SCM_WORD(scm__rc.d1786[643]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[82] = SCM_WORD(scm__rc.d1786[644]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[85] = SCM_WORD(scm__rc.d1786[645]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[93] = SCM_WORD(scm__rc.d1786[646]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[99] = SCM_WORD(scm__rc.d1786[647]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[108] = SCM_WORD(scm__rc.d1786[557]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[114] = SCM_WORD(scm__rc.d1786[558]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[118] = SCM_WORD(scm__rc.d1786[648]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[131] = SCM_WORD(scm__rc.d1786[649]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[144] = SCM_WORD(scm__rc.d1786[650]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[768]))[162] = SCM_WORD(scm__rc.d1786[560]);
  scm__rc.d1786[655] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[22])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[22]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[22]))->debugInfo = scm__rc.d1786[655];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[940]))[3] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[940]))[6] = SCM_WORD(scm__rc.d1786[636]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[940]))[13] = SCM_WORD(scm__rc.d1786[635]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[525]), scm__rc.d1786[133]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[526]), scm__rc.d1786[601]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[529]), scm__rc.d1786[90]);
  scm__rc.d1786[656] = Scm_MakeExtendedPair(scm__rc.d1786[624], SCM_OBJ(&scm__rc.d1788[526]), SCM_OBJ(&scm__rc.d1788[530]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[531]), scm__rc.d1786[656]);
  scm__rc.d1786[657] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[23])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[23]))->name = scm__rc.d1786[624];/* make-server-socket-unix */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[23]))->debugInfo = scm__rc.d1786[657];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]))[10] = SCM_WORD(scm__rc.d1786[541]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]))[21] = SCM_WORD(scm__rc.d1786[570]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]))[23] = SCM_WORD(scm__rc.d1786[593]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]))[25] = SCM_WORD(scm__rc.d1786[594]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]))[33] = SCM_WORD(scm__rc.d1786[599]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]))[35] = SCM_WORD(scm__rc.d1786[600]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]))[38] = SCM_WORD(scm__rc.d1786[553]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]))[40] = SCM_WORD(scm__rc.d1786[646]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]))[46] = SCM_WORD(scm__rc.d1786[647]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]))[55] = SCM_WORD(scm__rc.d1786[557]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]))[61] = SCM_WORD(scm__rc.d1786[558]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]))[65] = SCM_WORD(scm__rc.d1786[650]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[955]))[80] = SCM_WORD(scm__rc.d1786[560]);
  scm__rc.d1786[658] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[24])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[24]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[24]))->debugInfo = scm__rc.d1786[658];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1043]))[3] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1043]))[6] = SCM_WORD(scm__rc.d1786[624]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1043]))[13] = SCM_WORD(scm__rc.d1786[623]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[532]), scm__rc.d1786[330]);
  SCM_SET_CDR(SCM_OBJ(&scm__rc.d1788[532]), scm__rc.d1786[588]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[535]), scm__rc.d1786[90]);
  scm__rc.d1786[659] = Scm_MakeExtendedPair(scm__rc.d1786[634], SCM_OBJ(&scm__rc.d1788[532]), SCM_OBJ(&scm__rc.d1788[536]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[537]), scm__rc.d1786[659]);
  scm__rc.d1786[660] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[25])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[25]))->name = scm__rc.d1786[634];/* make-server-socket-inet */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[25]))->debugInfo = scm__rc.d1786[660];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1058]))[1] = SCM_WORD(scm__rc.d1786[635]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1058]))[7] = SCM_WORD(scm__rc.d1786[614]);
  scm__rc.d1786[661] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[26])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[26]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[26]))->debugInfo = scm__rc.d1786[661];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1070]))[3] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1070]))[6] = SCM_WORD(scm__rc.d1786[634]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1070]))[13] = SCM_WORD(scm__rc.d1786[633]);
  scm__rc.d1786[662] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[314])),FALSE); /* G2084 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[538]), scm__rc.d1786[662]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[539]), scm__rc.d1786[632]);
  scm__rc.d1786[663] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[315])),FALSE); /* G2083 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[540]), scm__rc.d1786[663]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[543]), scm__rc.d1786[90]);
  scm__rc.d1786[664] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1788[539]), SCM_OBJ(&scm__rc.d1788[540]), SCM_OBJ(&scm__rc.d1788[544]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[545]), scm__rc.d1786[664]);
  scm__rc.d1786[665] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[27])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[27]))->debugInfo = scm__rc.d1786[665];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1085]))[3] = SCM_WORD(scm__rc.d1786[614]);
  scm__rc.d1786[666] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[316])),TRUE); /* try-bind */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[547]), scm__rc.d1786[666]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[548]), scm__rc.d1786[632]);
  scm__rc.d1786[667] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[14])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[28]))->debugInfo = scm__rc.d1786[667];
  scm__rc.d1786[668] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[28])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[29]))->debugInfo = scm__rc.d1786[668];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1093]))[1] = SCM_WORD(scm__rc.d1786[635]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[553]), scm__rc.d1786[666]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[554]), scm__rc.d1786[632]);
  scm__rc.d1786[669] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[29])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[30]))->name = scm__rc.d1786[666];/* (make-server-socket-inet* try-bind) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[30]))->debugInfo = scm__rc.d1786[669];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1099]))[7] = SCM_WORD(scm__rc.d1786[609]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1099]))[11] = SCM_WORD(scm__rc.d1786[610]);
  scm__rc.d1786[671] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[317])),TRUE); /* append-map */
  scm__rc.d1786[670] = Scm_MakeIdentifier(scm__rc.d1786[671], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#append-map */
  scm__rc.d1786[672] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[318])),TRUE); /* ports */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[558]), scm__rc.d1786[672]);
  SCM_SET_CDR(SCM_OBJ(&scm__rc.d1788[558]), scm__rc.d1786[588]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[561]), scm__rc.d1786[90]);
  scm__rc.d1786[673] = Scm_MakeExtendedPair(scm__rc.d1786[632], SCM_OBJ(&scm__rc.d1788[558]), SCM_OBJ(&scm__rc.d1788[562]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[563]), scm__rc.d1786[673]);
  scm__rc.d1786[674] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[30])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[31]))->name = scm__rc.d1786[632];/* make-server-socket-inet* */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[31]))->debugInfo = scm__rc.d1786[674];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1112]))[13] = SCM_WORD(scm__rc.d1786[670]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1112]))[15] = SCM_WORD(scm__rc.d1786[615]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1112]))[26] = SCM_WORD(scm__rc.d1786[617]);
  scm__rc.d1786[675] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[31])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[32]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[32]))->debugInfo = scm__rc.d1786[675];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1140]))[5] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1140]))[8] = SCM_WORD(scm__rc.d1786[632]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1140]))[15] = SCM_WORD(scm__rc.d1786[631]);
  scm__rc.d1786[676] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[319])),FALSE); /* G2091 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[564]), scm__rc.d1786[676]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[565]), scm__rc.d1786[50]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[568]), scm__rc.d1786[90]);
  scm__rc.d1786[677] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1788[565]), SCM_OBJ(&scm__rc.d1788[420]), SCM_OBJ(&scm__rc.d1788[569]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[570]), scm__rc.d1786[677]);
  scm__rc.d1786[678] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[32])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[33]))->debugInfo = scm__rc.d1786[678];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1157]))[4] = SCM_WORD(scm__rc.d1786[569]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1157]))[7] = SCM_WORD(scm__rc.d1786[572]);
  scm__rc.d1786[679] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[320])),FALSE); /* G2090 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[571]), scm__rc.d1786[679]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[572]), scm__rc.d1786[50]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[575]), scm__rc.d1786[90]);
  scm__rc.d1786[680] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1788[572]), SCM_OBJ(&scm__rc.d1788[420]), SCM_OBJ(&scm__rc.d1788[576]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[577]), scm__rc.d1786[680]);
  scm__rc.d1786[681] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[33])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[34]))->debugInfo = scm__rc.d1786[681];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1167]))[4] = SCM_WORD(scm__rc.d1786[569]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1167]))[7] = SCM_WORD(scm__rc.d1786[85]);
  scm__rc.d1786[682] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[321])),FALSE); /* G2089 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[578]), scm__rc.d1786[682]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[579]), scm__rc.d1786[50]);
  scm__rc.d1786[683] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1788[579]), SCM_OBJ(&scm__rc.d1788[420]), SCM_OBJ(&scm__rc.d1788[576]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[580]), scm__rc.d1786[683]);
  scm__rc.d1786[684] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[33])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[35]))->debugInfo = scm__rc.d1786[684];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1177]))[4] = SCM_WORD(scm__rc.d1786[569]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1177]))[7] = SCM_WORD(scm__rc.d1786[85]);
  scm__rc.d1786[685] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[322])),FALSE); /* G2088 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[581]), scm__rc.d1786[685]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[582]), scm__rc.d1786[50]);
  scm__rc.d1786[686] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1788[582]), SCM_OBJ(&scm__rc.d1788[420]), SCM_OBJ(&scm__rc.d1788[569]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[583]), scm__rc.d1786[686]);
  scm__rc.d1786[687] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[32])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[36]))->debugInfo = scm__rc.d1786[687];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1187]))[4] = SCM_WORD(scm__rc.d1786[569]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1187]))[7] = SCM_WORD(scm__rc.d1786[572]);
  scm__rc.d1786[689] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[323])),TRUE); /* <system-error> */
  scm__rc.d1786[688] = Scm_MakeIdentifier(scm__rc.d1786[689], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#<system-error> */
  scm__rc.d1786[690] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[324])),TRUE); /* errno */
  scm__rc.d1786[692] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[325])),TRUE); /* ~ */
  scm__rc.d1786[691] = Scm_MakeIdentifier(scm__rc.d1786[692], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#~ */
  scm__rc.d1786[693] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[326])),TRUE); /* bind-failed? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[588]), scm__rc.d1786[693]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[589]), scm__rc.d1786[50]);
  scm__rc.d1786[694] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[34])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[37]))->name = scm__rc.d1786[693];/* (make-server-sockets bind-failed?) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[37]))->debugInfo = scm__rc.d1786[694];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1197]))[4] = SCM_WORD(scm__rc.d1786[688]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1197]))[10] = SCM_WORD(scm__rc.d1786[690]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1197]))[12] = SCM_WORD(scm__rc.d1786[691]);
  scm__rc.d1786[695] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[327])),TRUE); /* gauche */
  scm__rc.d1786[696] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[328])),TRUE); /* EADDRINUSE */
  scm__rc.d1786[698] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[329])),TRUE); /* global-variable-ref */
  scm__rc.d1786[697] = Scm_MakeIdentifier(scm__rc.d1786[698], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#global-variable-ref */
  scm__rc.d1786[699] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[330])),TRUE); /* EADDRNOTAVAIL */
  scm__rc.d1786[701] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[331])),TRUE); /* filter */
  scm__rc.d1786[700] = Scm_MakeIdentifier(scm__rc.d1786[701], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#filter */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[596]), scm__rc.d1786[50]);
  scm__rc.d1786[702] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[332])),FALSE); /* G2086 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[597]), scm__rc.d1786[702]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[600]), scm__rc.d1786[90]);
  scm__rc.d1786[703] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1788[596]), SCM_OBJ(&scm__rc.d1788[597]), SCM_OBJ(&scm__rc.d1788[601]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[602]), scm__rc.d1786[703]);
  scm__rc.d1786[704] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[35])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[38]))->debugInfo = scm__rc.d1786[704];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1216]))[1] = SCM_WORD(scm__rc.d1786[639]);
  scm__rc.d1786[706] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[333])),TRUE); /* map */
  scm__rc.d1786[705] = Scm_MakeIdentifier(scm__rc.d1786[706], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#map */
  scm__rc.d1786[707] = Scm_MakeIdentifier(scm__rc.d1786[16], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#socket-address */
  scm__rc.d1786[708] = Scm_MakeIdentifier(scm__rc.d1786[47], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#sockaddr-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[604]), scm__rc.d1786[50]);
  scm__rc.d1786[709] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[36])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[39]))->debugInfo = scm__rc.d1786[709];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1222]))[12] = SCM_WORD(scm__rc.d1786[617]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[608]), scm__rc.d1786[50]);
  scm__rc.d1786[710] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[334])),FALSE); /* G2085 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[609]), scm__rc.d1786[710]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[612]), scm__rc.d1786[90]);
  scm__rc.d1786[711] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1788[608]), SCM_OBJ(&scm__rc.d1788[609]), SCM_OBJ(&scm__rc.d1788[613]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[614]), scm__rc.d1786[711]);
  scm__rc.d1786[712] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[37])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[40]))->debugInfo = scm__rc.d1786[712];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1236]))[1] = SCM_WORD(scm__rc.d1786[639]);
  scm__rc.d1786[714] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[335])),TRUE); /* filter-map */
  scm__rc.d1786[713] = Scm_MakeIdentifier(scm__rc.d1786[714], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#filter-map */
  scm__rc.d1786[715] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[38])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[41]))->debugInfo = scm__rc.d1786[715];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1242]))[8] = SCM_WORD(scm__rc.d1786[713]);
  scm__rc.d1786[716] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[336])),FALSE); /* G2087 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[617]), scm__rc.d1786[716]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[620]), scm__rc.d1786[90]);
  scm__rc.d1786[717] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1788[596]), SCM_OBJ(&scm__rc.d1788[617]), SCM_OBJ(&scm__rc.d1788[621]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[622]), scm__rc.d1786[717]);
  scm__rc.d1786[718] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[39])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[42]))->debugInfo = scm__rc.d1786[718];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1253]))[12] = SCM_WORD(scm__rc.d1786[707]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1253]))[14] = SCM_WORD(scm__rc.d1786[708]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1253]))[16] = SCM_WORD(scm__rc.d1786[614]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1253]))[23] = SCM_WORD(scm__rc.d1786[700]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1253]))[32] = SCM_WORD(scm__rc.d1786[700]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1253]))[41] = SCM_WORD(scm__rc.d1786[609]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1253]))[45] = SCM_WORD(scm__rc.d1786[610]);
  scm__rc.d1786[719] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[302]))); /* :host */
  scm__rc.d1786[720] = Scm_MakeIdentifier(scm__rc.d1786[46], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#sockaddr-addr */
  scm__rc.d1786[721] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[131]))); /* :port */
  scm__rc.d1786[723] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[337])),TRUE); /* %reraise */
  scm__rc.d1786[722] = Scm_MakeIdentifier(scm__rc.d1786[723], SCM_MODULE(Scm_GaucheInternalModule()), SCM_NIL); /* gauche.internal#%reraise */
  scm__rc.d1786[724] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[40])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[43]))->debugInfo = scm__rc.d1786[724];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1300]))[11] = SCM_WORD(scm__rc.d1786[722]);
  scm__rc.d1786[725] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[41])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[44]))->debugInfo = scm__rc.d1786[725];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1313]))[3] = SCM_WORD(scm__rc.d1786[639]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1313]))[18] = SCM_WORD(scm__rc.d1786[707]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1313]))[20] = SCM_WORD(scm__rc.d1786[708]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[627]), scm__rc.d1786[619]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[630]), scm__rc.d1786[90]);
  scm__rc.d1786[726] = Scm_MakeExtendedPair(scm__rc.d1786[50], SCM_OBJ(&scm__rc.d1788[627]), SCM_OBJ(&scm__rc.d1788[631]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[632]), scm__rc.d1786[726]);
  scm__rc.d1786[727] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[42])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[45]))->name = scm__rc.d1786[50];/* make-server-sockets */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[45]))->debugInfo = scm__rc.d1786[727];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[7] = SCM_WORD(scm__rc.d1786[695]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[9] = SCM_WORD(scm__rc.d1786[76]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[12] = SCM_WORD(scm__rc.d1786[696]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[15] = SCM_WORD(scm__rc.d1786[697]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[22] = SCM_WORD(scm__rc.d1786[695]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[24] = SCM_WORD(scm__rc.d1786[76]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[27] = SCM_WORD(scm__rc.d1786[699]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[30] = SCM_WORD(scm__rc.d1786[697]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[37] = SCM_WORD(scm__rc.d1786[1]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[39] = SCM_WORD(scm__rc.d1786[76]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[42] = SCM_WORD(scm__rc.d1786[465]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[45] = SCM_WORD(scm__rc.d1786[697]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[52] = SCM_WORD(scm__rc.d1786[614]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[59] = SCM_WORD(scm__rc.d1786[700]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[69] = SCM_WORD(scm__rc.d1786[705]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[87] = SCM_WORD(scm__rc.d1786[700]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[106] = SCM_WORD(scm__rc.d1786[719]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[111] = SCM_WORD(scm__rc.d1786[720]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[114] = SCM_WORD(scm__rc.d1786[721]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[117] = SCM_WORD(scm__rc.d1786[553]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[131] = SCM_WORD(scm__rc.d1786[609]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[135] = SCM_WORD(scm__rc.d1786[610]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1338]))[156] = SCM_WORD(scm__rc.d1786[670]);
  scm__rc.d1786[728] = Scm_MakeIdentifier(scm__rc.d1786[50], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#make-server-sockets */
  scm__rc.d1786[729] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[43])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[46]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[46]))->debugInfo = scm__rc.d1786[729];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1502]))[5] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1502]))[8] = SCM_WORD(scm__rc.d1786[50]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1502]))[15] = SCM_WORD(scm__rc.d1786[728]);
  scm__rc.d1786[730] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[338])),FALSE); /* G2100 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[633]), scm__rc.d1786[730]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[634]), scm__rc.d1786[43]);
  scm__rc.d1786[731] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[339])),FALSE); /* G2095 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[635]), scm__rc.d1786[731]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[638]), scm__rc.d1786[90]);
  scm__rc.d1786[732] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1788[634]), SCM_OBJ(&scm__rc.d1788[635]), SCM_OBJ(&scm__rc.d1788[639]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[640]), scm__rc.d1786[732]);
  scm__rc.d1786[733] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[44])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[47]))->debugInfo = scm__rc.d1786[733];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1519]))[2] = SCM_WORD(scm__rc.d1786[149]);
  scm__rc.d1786[734] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[340])),FALSE); /* G2099 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[641]), scm__rc.d1786[734]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[642]), scm__rc.d1786[43]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[645]), scm__rc.d1786[90]);
  scm__rc.d1786[735] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1788[642]), SCM_OBJ(&scm__rc.d1788[420]), SCM_OBJ(&scm__rc.d1788[646]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[647]), scm__rc.d1786[735]);
  scm__rc.d1786[736] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[33])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[48]))->debugInfo = scm__rc.d1786[736];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1523]))[4] = SCM_WORD(scm__rc.d1786[569]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1523]))[7] = SCM_WORD(scm__rc.d1786[85]);
  scm__rc.d1786[737] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[341])),FALSE); /* G2098 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[648]), scm__rc.d1786[737]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[649]), scm__rc.d1786[43]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[652]), scm__rc.d1786[90]);
  scm__rc.d1786[738] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1788[649]), SCM_OBJ(&scm__rc.d1788[420]), SCM_OBJ(&scm__rc.d1788[653]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[654]), scm__rc.d1786[738]);
  scm__rc.d1786[739] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[33])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[49]))->debugInfo = scm__rc.d1786[739];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1533]))[4] = SCM_WORD(scm__rc.d1786[569]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1533]))[7] = SCM_WORD(scm__rc.d1786[85]);
  scm__rc.d1786[740] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[342])),TRUE); /* tcp */
  scm__rc.d1786[741] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[344])),TRUE); /* lambda */
  scm__rc.d1786[742] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[345])),TRUE); /* quote */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[658]), scm__rc.d1786[740]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[659]), scm__rc.d1786[742]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[661]), scm__rc.d1786[320]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[663]), scm__rc.d1786[88]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[664]), scm__rc.d1786[330]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[665]), scm__rc.d1786[619]);
  scm__rc.d1786[743] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[346])),TRUE); /* if */
  scm__rc.d1786[744] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[347])),TRUE); /* let* */
  scm__rc.d1786[745] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[348])),TRUE); /* case */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[666]), scm__rc.d1786[10]);
  scm__rc.d1786[746] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[349])),TRUE); /* udp */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[668]), scm__rc.d1786[746]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[669]), scm__rc.d1786[11]);
  scm__rc.d1786[747] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[350])),TRUE); /* else */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[672]), scm__rc.d1786[556]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[674]), scm__rc.d1786[747]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[678]), scm__rc.d1786[320]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[679]), scm__rc.d1786[745]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[681]), scm__rc.d1786[563]);
  scm__rc.d1786[748] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[351])),TRUE); /* x->string */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[682]), scm__rc.d1786[748]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[684]), scm__rc.d1786[330]);
  scm__rc.d1786[749] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[352])),TRUE); /* hints */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[685]), scm__rc.d1786[563]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[686]), scm__rc.d1786[551]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[687]), scm__rc.d1786[469]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[688]), scm__rc.d1786[545]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[689]), scm__rc.d1786[468]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[691]), scm__rc.d1786[749]);
  scm__rc.d1786[750] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[353])),TRUE); /* ss */
  scm__rc.d1786[751] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[354])),TRUE); /* cut */
  scm__rc.d1786[752] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[355])),TRUE); /* slot-ref */
  scm__rc.d1786[753] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[356])),TRUE); /* <> */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[692]), scm__rc.d1786[742]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[694]), scm__rc.d1786[753]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[695]), scm__rc.d1786[752]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[696]), scm__rc.d1786[751]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[697]), scm__rc.d1786[749]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[698]), scm__rc.d1786[330]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[699]), scm__rc.d1786[619]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[700]), scm__rc.d1786[467]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[703]), scm__rc.d1786[706]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[705]), scm__rc.d1786[750]);
  scm__rc.d1786[754] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[357])),TRUE); /* append */
  scm__rc.d1786[755] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[358])),TRUE); /* ^s */
  scm__rc.d1786[756] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[359])),TRUE); /* eq? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[710]), scm__rc.d1786[45]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[711]), scm__rc.d1786[85]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[712]), scm__rc.d1786[742]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[715]), scm__rc.d1786[756]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[717]), scm__rc.d1786[755]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[718]), scm__rc.d1786[750]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[720]), scm__rc.d1786[701]);
  scm__rc.d1786[757] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[360])),TRUE); /* remove */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[721]), scm__rc.d1786[757]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[724]), scm__rc.d1786[754]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[726]), scm__rc.d1786[538]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[727]), scm__rc.d1786[743]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[730]), scm__rc.d1786[744]);
  scm__rc.d1786[758] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[361])),TRUE); /* let1 */
  scm__rc.d1786[759] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[362])),TRUE); /* cond */
  scm__rc.d1786[760] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[363])),TRUE); /* number? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[731]), scm__rc.d1786[760]);
  scm__rc.d1786[761] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[364])),TRUE); /* symbol->string */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[733]), scm__rc.d1786[761]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[735]), scm__rc.d1786[330]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[736]), scm__rc.d1786[59]);
  scm__rc.d1786[762] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[365])),TRUE); /* => */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[737]), scm__rc.d1786[742]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[739]), scm__rc.d1786[753]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[740]), scm__rc.d1786[752]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[741]), scm__rc.d1786[751]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[743]), scm__rc.d1786[762]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[746]), scm__rc.d1786[556]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[748]), scm__rc.d1786[747]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[752]), scm__rc.d1786[759]);
  scm__rc.d1786[763] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[367])),TRUE); /* hh */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[753]), scm__rc.d1786[619]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[754]), scm__rc.d1786[53]);
  scm__rc.d1786[764] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[368])),TRUE); /* unless */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[756]), scm__rc.d1786[556]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[758]), scm__rc.d1786[763]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[759]), scm__rc.d1786[764]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[760]), scm__rc.d1786[721]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[761]), scm__rc.d1786[753]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[762]), scm__rc.d1786[719]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[763]), scm__rc.d1786[41]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[764]), scm__rc.d1786[554]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[765]), scm__rc.d1786[751]);
  scm__rc.d1786[765] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[370])),TRUE); /* addresses */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[766]), scm__rc.d1786[765]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[767]), scm__rc.d1786[742]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[769]), scm__rc.d1786[763]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[770]), scm__rc.d1786[752]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[773]), scm__rc.d1786[706]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[777]), scm__rc.d1786[763]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[778]), scm__rc.d1786[758]);
  scm__rc.d1786[766] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[371])),TRUE); /* list */
  scm__rc.d1786[767] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[300]))); /* :any */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[779]), scm__rc.d1786[767]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[780]), scm__rc.d1786[719]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[781]), scm__rc.d1786[41]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[782]), scm__rc.d1786[554]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[784]), scm__rc.d1786[766]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[787]), scm__rc.d1786[619]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[788]), scm__rc.d1786[743]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[791]), scm__rc.d1786[330]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[792]), scm__rc.d1786[758]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[795]), scm__rc.d1786[533]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[796]), scm__rc.d1786[743]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[799]), scm__rc.d1786[741]);
  scm__rc.d1786[768] = Scm_MakeIdentifier(scm__rc.d1786[11], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#SOCK_DGRAM */
  scm__rc.d1786[769] = Scm_MakeIdentifier(scm__rc.d1786[748], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#x->string */
  scm__rc.d1786[770] = Scm_MakeIdentifier(scm__rc.d1786[469], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#AI_PASSIVE */
  scm__rc.d1786[771] = Scm_MakeIdentifier(scm__rc.d1786[467], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#sys-getaddrinfo */
  scm__rc.d1786[772] = Scm_MakeIdentifier(scm__rc.d1786[757], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#remove */
  scm__rc.d1786[773] = Scm_MakeIdentifier(scm__rc.d1786[761], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#symbol->string */
  scm__rc.d1786[774] = Scm_MakeIdentifier(scm__rc.d1786[59], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#sys-getservbyname */
  scm__rc.d1786[775] = Scm_MakeIdentifier(scm__rc.d1786[53], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#sys-gethostbyname */
  scm__rc.d1786[776] = Scm_MakeIdentifier(scm__rc.d1786[41], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#<sockaddr-in> */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[800]), scm__rc.d1786[43]);
  scm__rc.d1786[777] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[372])),FALSE); /* G2097 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[801]), scm__rc.d1786[777]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[804]), scm__rc.d1786[90]);
  scm__rc.d1786[778] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1788[800]), SCM_OBJ(&scm__rc.d1788[801]), SCM_OBJ(&scm__rc.d1788[805]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[806]), scm__rc.d1786[778]);
  scm__rc.d1786[779] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[45])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[50]))->debugInfo = scm__rc.d1786[779];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1543]))[1] = SCM_WORD(scm__rc.d1786[776]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1543]))[3] = SCM_WORD(scm__rc.d1786[719]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1543]))[6] = SCM_WORD(scm__rc.d1786[721]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1543]))[9] = SCM_WORD(scm__rc.d1786[553]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[809]), scm__rc.d1786[90]);
  scm__rc.d1786[780] = Scm_MakeExtendedPair(scm__rc.d1786[43], SCM_OBJ(&scm__rc.d1788[665]), SCM_OBJ(&scm__rc.d1788[810]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[811]), scm__rc.d1786[780]);
  scm__rc.d1786[781] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[46])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[51]))->name = scm__rc.d1786[43];/* make-sockaddrs */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[51]))->debugInfo = scm__rc.d1786[781];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[4] = SCM_WORD(scm__rc.d1786[740]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[29] = SCM_WORD(scm__rc.d1786[557]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[31] = SCM_WORD(scm__rc.d1786[536]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[36] = SCM_WORD(scm__rc.d1786[740]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[39] = SCM_WORD(scm__rc.d1786[593]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[44] = SCM_WORD(scm__rc.d1786[746]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[47] = SCM_WORD(scm__rc.d1786[768]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[56] = SCM_WORD(scm__rc.d1786[555]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[62] = SCM_WORD(scm__rc.d1786[769]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[67] = SCM_WORD(scm__rc.d1786[545]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[69] = SCM_WORD(scm__rc.d1786[770]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[71] = SCM_WORD(scm__rc.d1786[551]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[74] = SCM_WORD(scm__rc.d1786[566]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[85] = SCM_WORD(scm__rc.d1786[771]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[87] = SCM_WORD(scm__rc.d1786[705]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[90] = SCM_WORD(scm__rc.d1786[539]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[98] = SCM_WORD(scm__rc.d1786[700]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[104] = SCM_WORD(scm__rc.d1786[772]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[122] = SCM_WORD(scm__rc.d1786[773]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[124] = SCM_WORD(scm__rc.d1786[774]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[131] = SCM_WORD(scm__rc.d1786[330]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[140] = SCM_WORD(scm__rc.d1786[555]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[150] = SCM_WORD(scm__rc.d1786[775]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[163] = SCM_WORD(scm__rc.d1786[555]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[169] = SCM_WORD(scm__rc.d1786[765]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[171] = SCM_WORD(scm__rc.d1786[705]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[176] = SCM_WORD(scm__rc.d1786[776]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[178] = SCM_WORD(scm__rc.d1786[719]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[180] = SCM_WORD(scm__rc.d1786[767]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[182] = SCM_WORD(scm__rc.d1786[721]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1554]))[185] = SCM_WORD(scm__rc.d1786[553]);
  scm__rc.d1786[782] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[47])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[52]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[52]))->debugInfo = scm__rc.d1786[782];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1742]))[5] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1742]))[8] = SCM_WORD(scm__rc.d1786[43]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1742]))[15] = SCM_WORD(scm__rc.d1786[614]);
  scm__rc.d1786[783] = Scm_MakeIdentifier(scm__rc.d1786[18], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#socket-input-port */
  scm__rc.d1786[784] = Scm_MakeIdentifier(scm__rc.d1786[19], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#socket-output-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[812]), scm__rc.d1786[51]);
  scm__rc.d1786[785] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[48])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[53]))->debugInfo = scm__rc.d1786[785];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1759]))[7] = SCM_WORD(scm__rc.d1786[131]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1759]))[10] = SCM_WORD(scm__rc.d1786[783]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1759]))[17] = SCM_WORD(scm__rc.d1786[783]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1759]))[26] = SCM_WORD(scm__rc.d1786[131]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1759]))[29] = SCM_WORD(scm__rc.d1786[784]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1759]))[36] = SCM_WORD(scm__rc.d1786[784]);
  scm__rc.d1786[786] = Scm_MakeIdentifier(scm__rc.d1786[21], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#socket-close */
  scm__rc.d1786[787] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[49])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[54]))->debugInfo = scm__rc.d1786[787];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1800]))[2] = SCM_WORD(scm__rc.d1786[786]);
  scm__rc.d1786[788] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[86]))); /* :source-info */
  scm__rc.d1786[790] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[373])),TRUE); /* %unwind-protect */
  scm__rc.d1786[789] = Scm_MakeIdentifier(scm__rc.d1786[790], SCM_MODULE(Scm_GaucheInternalModule()), SCM_NIL); /* gauche.internal#%unwind-protect */
  scm__rc.d1786[791] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[374]))); /* :input-buffering */
  scm__rc.d1786[792] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1787[375]))); /* :output-buffering */
  scm__rc.d1786[793] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[376])),TRUE); /* socket */
  scm__rc.d1786[794] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[377])),TRUE); /* proc */
  scm__rc.d1786[795] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[374])),TRUE); /* input-buffering */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[817]), scm__rc.d1786[795]);
  scm__rc.d1786[796] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[375])),TRUE); /* output-buffering */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[818]), scm__rc.d1786[796]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[821]), scm__rc.d1786[133]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[822]), scm__rc.d1786[794]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[823]), scm__rc.d1786[793]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[826]), scm__rc.d1786[90]);
  scm__rc.d1786[797] = Scm_MakeExtendedPair(scm__rc.d1786[51], SCM_OBJ(&scm__rc.d1788[823]), SCM_OBJ(&scm__rc.d1788[827]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1788[828]), scm__rc.d1786[797]);
  scm__rc.d1786[798] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[50])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[55]))->name = scm__rc.d1786[51];/* call-with-client-socket */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[55]))->debugInfo = scm__rc.d1786[798];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]))[12] = SCM_WORD(scm__rc.d1786[541]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]))[24] = SCM_WORD(scm__rc.d1786[541]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]))[39] = SCM_WORD(scm__rc.d1786[788]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]))[43] = SCM_WORD(scm__rc.d1786[789]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]))[52] = SCM_WORD(scm__rc.d1786[557]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]))[58] = SCM_WORD(scm__rc.d1786[558]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]))[62] = SCM_WORD(scm__rc.d1786[791]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]))[74] = SCM_WORD(scm__rc.d1786[792]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1804]))[90] = SCM_WORD(scm__rc.d1786[560]);
  scm__rc.d1786[799] = Scm_MakeIdentifier(scm__rc.d1786[51], SCM_MODULE(scm__rc.d1786[78]), SCM_NIL); /* gauche.net#call-with-client-socket */
  scm__rc.d1786[800] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1791[51])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[56]))->name = scm__rc.d1786[81];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1790[56]))->debugInfo = scm__rc.d1786[800];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1903]))[3] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1903]))[6] = SCM_WORD(scm__rc.d1786[51]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1789[1903]))[13] = SCM_WORD(scm__rc.d1786[799]);
  Scm_VMExecuteToplevels(toplevels);
  scm__rc.d1786[1042] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[378])),TRUE); /* gauche.internal */
  scm__rc.d1786[1043] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[379])),TRUE); /* current-module */
  scm__rc.d1786[1044] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[380])),TRUE); /* define-constant */
  scm__rc.d1786[1045] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[381])),TRUE); /* cond-expand */
  scm__rc.d1786[1046] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[382])),TRUE); /* gauche.os.windows */
  scm__rc.d1786[1047] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[383])),FALSE); /* loop2063 */
  scm__rc.d1786[1048] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[384])),FALSE); /* args2062 */
  scm__rc.d1786[1049] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[385])),FALSE); /* G2064 */
  scm__rc.d1786[1050] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[386])),FALSE); /* G2065 */
  scm__rc.d1786[1051] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[387])),FALSE); /* G2066 */
  scm__rc.d1786[1052] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[388])),FALSE); /* G2067 */
  scm__rc.d1786[1053] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[389])),TRUE); /* tmp */
  scm__rc.d1786[1054] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[390])),FALSE); /* rest2061 */
  scm__rc.d1786[1055] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[391])),TRUE); /* pair? */
  scm__rc.d1786[1056] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[392])),TRUE); /* is-a? */
  scm__rc.d1786[1057] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[393])),FALSE); /* G2070 */
  scm__rc.d1786[1058] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[394])),FALSE); /* G2069 */
  scm__rc.d1786[1059] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[395])),FALSE); /* G2068 */
  scm__rc.d1786[1060] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[396])),TRUE); /* let-optionals* */
  scm__rc.d1786[1061] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[397])),TRUE); /* rlet1 */
  scm__rc.d1786[1062] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[398])),TRUE); /* err */
  scm__rc.d1786[1063] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[399])),TRUE); /* set! */
  scm__rc.d1786[1064] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[400])),TRUE); /* guard */
  scm__rc.d1786[1065] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[401])),FALSE); /* G2072 */
  scm__rc.d1786[1066] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[402])),FALSE); /* G2071 */
  scm__rc.d1786[1067] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[403])),FALSE); /* loop2075 */
  scm__rc.d1786[1068] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[404])),FALSE); /* args2074 */
  scm__rc.d1786[1069] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[405])),FALSE); /* G2076 */
  scm__rc.d1786[1070] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[406])),FALSE); /* G2077 */
  scm__rc.d1786[1071] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[407])),FALSE); /* G2078 */
  scm__rc.d1786[1072] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[408])),FALSE); /* rest2073 */
  scm__rc.d1786[1073] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[409])),FALSE); /* loop2081 */
  scm__rc.d1786[1074] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[410])),FALSE); /* args2080 */
  scm__rc.d1786[1075] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[411])),FALSE); /* G2082 */
  scm__rc.d1786[1076] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[412])),FALSE); /* rest2079 */
  scm__rc.d1786[1077] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[413])),TRUE); /* $ */
  scm__rc.d1786[1078] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[414])),TRUE); /* memv */
  scm__rc.d1786[1079] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[415])),TRUE); /* quasiquote */
  scm__rc.d1786[1080] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[416])),TRUE); /* unquote */
  scm__rc.d1786[1081] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[417])),TRUE); /* s6 */
  scm__rc.d1786[1082] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[418])),TRUE); /* a4s */
  scm__rc.d1786[1083] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[419])),TRUE); /* zero? */
  scm__rc.d1786[1084] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[420])),TRUE); /* v4addrs */
  scm__rc.d1786[1085] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[421])),TRUE); /* try-v4 */
  scm__rc.d1786[1086] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[422])),TRUE); /* values */
  scm__rc.d1786[1087] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[423])),TRUE); /* actual-port */
  scm__rc.d1786[1088] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[424])),TRUE); /* make-v6socks */
  scm__rc.d1786[1089] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[425])),TRUE); /* v6addrs */
  scm__rc.d1786[1090] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[426])),TRUE); /* sockets */
  scm__rc.d1786[1091] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[427])),TRUE); /* receive */
  scm__rc.d1786[1092] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[428])),TRUE); /* try-v6 */
  scm__rc.d1786[1093] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[429])),TRUE); /* reverse */
  scm__rc.d1786[1094] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[430])),TRUE); /* a6s */
  scm__rc.d1786[1095] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[431])),FALSE); /* G2096 */
  scm__rc.d1786[1096] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[432])),FALSE); /* G2093 */
  scm__rc.d1786[1097] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[433])),FALSE); /* G2094 */
  scm__rc.d1786[1098] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[434])),FALSE); /* rest2092 */
  scm__rc.d1786[1099] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[435])),FALSE); /* loop2103 */
  scm__rc.d1786[1100] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[436])),FALSE); /* args2102 */
  scm__rc.d1786[1101] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[437])),FALSE); /* G2104 */
  scm__rc.d1786[1102] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[438])),FALSE); /* G2105 */
  scm__rc.d1786[1103] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[439])),FALSE); /* rest2101 */
  scm__rc.d1786[1104] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1787[440])),TRUE); /* unwind-protect */
}
