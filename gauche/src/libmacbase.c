/* Generated automatically from libmacbase.scm.  DO NOT EDIT */
#define LIBGAUCHE_BODY 
#include <gauche.h>
#include <gauche/code.h>
#include "gauche/priv/configP.h"
#include "gauche/priv/macroP.h"
static unsigned char uvector__00001[] = {
 0u, 3u, 144u, 6u, 8u, 2u, 1u, 128u, 155u, 137u, 28u, 120u, 48u, 36u,
4u, 71u, 29u, 12u, 9u, 0u, 145u, 198u, 195u, 3u, 60u, 6u, 24u, 67u,
24u, 128u, 96u, 65u, 1u, 55u, 18u, 19u, 1u, 194u, 73u, 28u, 84u, 48u,
38u, 22u, 164u, 113u, 16u, 192u, 215u, 2u, 146u, 56u, 128u, 96u, 72u,
20u, 142u, 30u, 24u, 25u, 0u, 128u, 131u, 40u, 107u, 129u, 139u, 112u,
104u, 129u, 137u, 34u, 5u, 12u, 161u, 174u, 6u, 33u, 133u, 72u, 106u,
129u, 137u, 36u, 55u, 132u, 197u, 220u, 38u, 22u, 164u, 50u, 42u, 98u,
30u, 72u, 225u, 193u, 129u, 50u, 120u, 35u, 132u, 134u, 4u, 201u, 56u,
142u, 16u, 24u, 18u, 7u, 35u, 131u, 6u, 4u, 200u, 96u, 142u, 2u, 24u,
19u, 31u, 130u, 56u, 0u, 96u, 72u, 28u, 134u, 32u, 192u, 152u, 106u,
146u,};
static unsigned char uvector__00002[] = {
 0u, 3u, 134u, 6u, 8u, 34u, 9u, 16u, 84u, 3u, 2u, 8u, 9u, 184u, 145u,
0u, 64u, 48u, 19u, 113u, 36u, 112u, 144u, 192u, 198u, 19u, 3u, 212u,
193u, 176u, 142u, 14u, 24u, 19u, 1u, 194u, 56u, 16u, 96u, 76u, 7u, 8u,
224u, 1u, 129u, 48u, 28u, 36u,};
static unsigned char uvector__00003[] = {
 0u, 3u, 144u, 134u, 8u, 2u, 1u, 128u, 159u, 137u, 28u, 120u, 48u,
36u, 4u, 71u, 29u, 12u, 9u, 0u, 145u, 198u, 195u, 3u, 60u, 6u, 24u,
67u, 24u, 128u, 96u, 65u, 1u, 55u, 18u, 19u, 1u, 194u, 73u, 28u, 84u,
48u, 38u, 22u, 164u, 113u, 16u, 192u, 215u, 5u, 146u, 56u, 128u, 96u,
72u, 44u, 142u, 30u, 24u, 25u, 0u, 128u, 131u, 40u, 107u, 131u, 11u,
112u, 104u, 131u, 9u, 34u, 11u, 12u, 161u, 174u, 12u, 33u, 133u, 72u,
106u, 131u, 9u, 36u, 55u, 132u, 197u, 220u, 38u, 22u, 164u, 50u, 42u,
98u, 30u, 72u, 225u, 193u, 129u, 50u, 120u, 35u, 132u, 134u, 4u, 201u,
56u, 142u, 16u, 24u, 18u, 13u, 35u, 131u, 6u, 4u, 200u, 96u, 142u, 2u,
24u, 19u, 31u, 130u, 56u, 0u, 96u, 72u, 52u, 134u, 32u, 192u, 152u,
106u, 146u,};
static unsigned char uvector__00004[] = {
 0u, 3u, 134u, 6u, 8u, 34u, 9u, 16u, 116u, 3u, 2u, 8u, 9u, 184u, 145u,
0u, 64u, 48u, 19u, 241u, 36u, 112u, 144u, 192u, 198u, 19u, 3u, 212u,
193u, 176u, 142u, 14u, 24u, 19u, 1u, 194u, 56u, 16u, 96u, 76u, 7u, 8u,
224u, 1u, 129u, 48u, 28u, 36u,};
static unsigned char uvector__00005[] = {
 0u, 3u, 154u, 6u, 4u, 131u, 200u, 230u, 33u, 130u, 16u, 131u, 201u,
28u, 192u, 48u, 36u, 30u, 71u, 45u, 12u, 15u, 112u, 19u, 113u, 35u,
150u, 6u, 4u, 128u, 136u, 229u, 33u, 130u, 17u, 128u, 68u, 37u, 9u,
152u, 80u, 145u, 202u, 3u, 2u, 66u, 132u, 114u, 80u, 192u, 152u, 142u,
145u, 201u, 3u, 2u, 66u, 100u, 114u, 32u, 192u, 152u, 142u, 145u,
200u, 67u, 2u, 64u, 36u, 113u, 240u, 192u, 152u, 131u, 17u, 199u, 3u,
3u, 60u, 43u, 1u, 18u, 56u, 216u, 96u, 72u, 8u, 142u, 46u, 24u, 19u,
38u, 82u, 56u, 160u, 96u, 72u, 8u, 142u, 36u, 24u, 4u, 45u, 1u, 24u,
8u, 145u, 196u, 3u, 2u, 102u, 74u, 71u, 15u, 12u, 9u, 1u, 17u, 195u,
67u, 2u, 102u, 74u, 71u, 9u, 12u, 16u, 185u, 35u, 131u, 134u, 4u,
207u, 172u, 142u, 10u, 24u, 4u, 49u, 1u, 16u, 153u, 245u, 146u, 56u,
24u, 96u, 77u, 8u, 133u, 35u, 129u, 6u, 4u, 128u, 136u, 224u, 1u,
129u, 52u, 34u, 20u, 134u, 32u, 192u, 198u, 32u, 24u, 10u, 20u, 34u,
25u, 2u, 19u, 13u, 129u, 52u, 34u, 8u, 38u, 100u, 65u, 164u, 38u, 76u,
164u, 145u, 7u, 132u, 196u, 24u, 144u, 202u, 19u, 10u, 48u, 152u, 35u,
65u, 228u, 146u, 64u,};
static unsigned char uvector__00006[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 0u, 64u, 48u, 20u, 40u, 68u, 50u, 4u, 38u,
27u, 0u, 134u, 32u, 34u, 33u, 114u, 64u, 133u, 160u, 35u, 1u, 16u,
210u, 25u, 225u, 88u, 8u, 146u, 68u, 30u, 33u, 24u, 4u, 66u, 80u,
153u, 133u, 9u, 33u, 148u, 61u, 192u, 77u, 196u, 66u, 16u, 121u, 131u,
201u, 36u, 112u, 144u, 192u, 198u, 19u, 3u, 20u, 193u, 72u, 142u, 14u,
24u, 19u, 1u, 194u, 56u, 16u, 96u, 76u, 7u, 8u, 224u, 1u, 129u, 48u,
28u, 36u,};
static ScmObj libmacbaseunwrap_syntax(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libmacbaseunwrap_syntax__STUB, 1, 2,SCM_FALSE,libmacbaseunwrap_syntax, NULL, NULL);

static ScmObj libmacbaseunwrap_syntax_1(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libmacbaseunwrap_syntax_1__STUB, 1, 0,SCM_FALSE,libmacbaseunwrap_syntax_1, NULL, NULL);

static unsigned char uvector__00007[] = {
 0u, 3u, 147u, 6u, 4u, 128u, 72u, 228u, 97u, 130u, 26u, 134u, 224u,
18u, 71u, 34u, 12u, 9u, 0u, 145u, 200u, 67u, 2u, 67u, 100u, 113u,
240u, 192u, 33u, 200u, 4u, 132u, 193u, 24u, 145u, 199u, 131u, 2u, 97u,
180u, 71u, 29u, 12u, 9u, 0u, 145u, 198u, 195u, 3u, 152u, 134u, 195u,
68u, 2u, 72u, 134u, 195u, 84u, 2u, 73u, 35u, 141u, 6u, 4u, 198u, 192u,
142u, 50u, 24u, 18u, 27u, 35u, 139u, 134u, 4u, 198u, 244u, 142u, 42u,
24u, 19u, 27u, 2u, 56u, 160u, 96u, 76u, 98u, 8u, 226u, 97u, 129u, 33u,
178u, 56u, 136u, 96u, 76u, 101u, 72u, 225u, 225u, 129u, 49u, 136u,
35u, 134u, 134u, 1u, 14u, 192u, 36u, 38u, 47u, 164u, 142u, 24u, 24u,
19u, 53u, 242u, 56u, 88u, 96u, 72u, 4u, 142u, 18u, 24u, 33u, 232u, 4u,
145u, 194u, 3u, 2u, 67u, 196u, 112u, 112u, 192u, 144u, 9u, 28u, 20u,
48u, 8u, 126u, 1u, 33u, 52u, 32u, 8u, 145u, 192u, 195u, 2u, 104u, 72u,
97u, 28u, 8u, 48u, 36u, 2u, 71u, 0u, 12u, 9u, 161u, 33u, 132u, 49u,
6u, 6u, 49u, 0u, 144u, 216u, 19u, 66u, 66u, 66u, 102u, 184u, 19u, 13u,
112u, 211u, 0u, 146u, 73u, 0u,};
static unsigned char uvector__00008[] = {
 0u, 3u, 133u, 134u, 4u, 136u, 8u, 225u, 33u, 129u, 210u, 32u, 224u,
36u, 142u, 14u, 24u, 30u, 196u, 67u, 17u, 24u, 140u, 145u, 193u, 131u,
2u, 68u, 100u, 112u, 48u, 192u, 152u, 70u, 145u, 192u, 131u, 2u, 68u,
68u, 112u, 0u, 192u, 152u, 70u, 144u, 196u, 24u, 24u, 196u, 69u, 18u,
68u, 4u, 50u, 132u, 194u, 8u, 38u, 8u, 209u, 1u, 36u, 128u,};
static unsigned char uvector__00009[] = {
 0u, 3u, 161u, 6u, 4u, 137u, 72u, 232u, 1u, 130u, 38u, 137u, 226u,
136u, 148u, 145u, 207u, 195u, 2u, 68u, 164u, 115u, 224u, 192u, 145u,
65u, 28u, 244u, 48u, 36u, 78u, 71u, 59u, 12u, 9u, 130u, 49u, 28u,
232u, 48u, 50u, 1u, 18u, 136u, 166u, 35u, 138u, 162u, 178u, 72u, 76u,
17u, 162u, 82u, 71u, 56u, 12u, 9u, 138u, 161u, 28u, 220u, 48u, 36u,
86u, 71u, 52u, 12u, 9u, 17u, 145u, 204u, 131u, 2u, 98u, 168u, 71u,
49u, 12u, 12u, 128u, 69u, 98u, 44u, 139u, 98u, 112u, 228u, 34u, 40u,
146u, 32u, 33u, 148u, 61u, 136u, 134u, 34u, 49u, 25u, 14u, 145u, 7u,
1u, 49u, 1u, 60u, 0u, 146u, 34u, 232u, 148u, 38u, 42u, 129u, 48u, 70u,
36u, 142u, 94u, 24u, 34u, 216u, 156u, 38u, 85u, 252u, 0u, 145u, 202u,
195u, 3u, 24u, 76u, 174u, 130u, 101u, 188u, 72u, 229u, 65u, 129u, 34u,
114u, 57u, 64u, 96u, 76u, 240u, 136u, 228u, 225u, 129u, 34u, 50u, 57u,
40u, 96u, 137u, 162u, 120u, 162u, 35u, 36u, 114u, 64u, 192u, 145u,
25u, 28u, 140u, 48u, 36u, 80u, 71u, 34u, 12u, 9u, 19u, 145u, 200u, 3u,
2u, 104u, 78u, 65u, 28u, 120u, 48u, 50u, 136u, 190u, 40u, 17u, 132u,
70u, 33u, 114u, 24u, 84u, 146u, 30u, 130u, 104u, 78u, 68u, 70u, 67u,
108u, 86u, 19u, 40u, 224u, 153u, 177u, 18u, 71u, 28u, 12u, 9u, 161u,
121u, 132u, 113u, 160u, 192u, 154u, 24u, 6u, 71u, 22u, 12u, 9u, 161u,
131u, 68u, 113u, 64u, 192u, 154u, 24u, 52u, 71u, 19u, 12u, 9u, 17u,
145u, 196u, 67u, 2u, 104u, 96u, 25u, 28u, 64u, 48u, 36u, 80u, 71u,
14u, 12u, 9u, 161u, 121u, 132u, 112u, 208u, 192u, 200u, 4u, 70u, 34u,
24u, 160u, 146u, 19u, 66u, 240u, 73u, 28u, 44u, 48u, 38u, 136u, 109u,
17u, 194u, 131u, 2u, 69u, 4u, 112u, 128u, 192u, 154u, 33u, 180u, 71u,
5u, 12u, 17u, 140u, 79u, 20u, 55u, 18u, 56u, 24u, 96u, 72u, 160u,
142u, 4u, 24u, 18u, 39u, 35u, 128u, 6u, 4u, 209u, 44u, 50u, 24u, 131u,
3u, 24u, 138u, 8u, 118u, 9u, 162u, 88u, 97u, 182u, 35u, 9u, 162u, 27u,
65u, 52u, 47u, 4u, 146u, 72u,};
static unsigned char uvector__00010[] = {
 0u, 3u, 135u, 134u, 8u, 110u, 1u, 36u, 112u, 224u, 192u, 144u, 217u,
28u, 52u, 48u, 36u, 2u, 71u, 9u, 12u, 12u, 98u, 40u, 33u, 216u, 70u,
49u, 60u, 80u, 220u, 67u, 108u, 70u, 34u, 24u, 160u, 134u, 81u, 23u,
197u, 2u, 48u, 136u, 196u, 46u, 67u, 10u, 146u, 67u, 208u, 137u, 162u,
120u, 162u, 35u, 49u, 25u, 13u, 177u, 88u, 139u, 34u, 216u, 156u, 57u,
8u, 138u, 36u, 136u, 8u, 101u, 15u, 98u, 33u, 136u, 140u, 70u, 67u,
164u, 65u, 192u, 76u, 64u, 79u, 0u, 34u, 46u, 137u, 68u, 83u, 17u,
197u, 81u, 89u, 17u, 52u, 79u, 20u, 68u, 164u, 146u, 73u, 35u, 132u,
6u, 6u, 64u, 34u, 113u, 25u, 6u, 23u, 178u, 73u, 14u, 65u, 48u, 174u,
9u, 133u, 241u, 36u, 112u, 96u, 192u, 153u, 179u, 145u, 192u, 131u,
2u, 102u, 206u, 67u, 16u, 96u, 99u, 16u, 9u, 12u, 208u, 240u, 109u,
137u, 194u, 102u, 206u, 19u, 57u, 50u, 67u, 48u, 134u, 224u, 18u, 27u,
0u, 135u, 224u, 18u, 33u, 232u, 4u, 144u, 33u, 216u, 4u, 135u, 49u,
13u, 134u, 136u, 4u, 145u, 13u, 134u, 168u, 4u, 146u, 64u, 135u, 32u,
18u, 33u, 168u, 110u, 1u, 36u, 52u, 192u, 36u, 144u, 152u, 14u, 18u,
64u,};
static unsigned char uvector__00011[] = {
 0u, 3u, 134u, 6u, 8u, 34u, 9u, 16u, 132u, 2u, 67u, 52u, 60u, 27u,
98u, 113u, 25u, 6u, 23u, 178u, 67u, 144u, 138u, 8u, 118u, 17u, 140u,
79u, 20u, 55u, 16u, 219u, 17u, 136u, 134u, 40u, 33u, 148u, 69u, 241u,
64u, 140u, 34u, 49u, 11u, 144u, 194u, 164u, 144u, 244u, 34u, 104u,
158u, 40u, 136u, 204u, 70u, 67u, 108u, 86u, 34u, 200u, 182u, 39u, 14u,
66u, 34u, 137u, 34u, 2u, 25u, 67u, 216u, 136u, 98u, 35u, 17u, 144u,
233u, 16u, 112u, 19u, 16u, 19u, 192u, 8u, 139u, 162u, 81u, 20u, 196u,
113u, 84u, 86u, 68u, 77u, 19u, 197u, 17u, 41u, 36u, 146u, 73u, 12u,
194u, 27u, 128u, 72u, 108u, 2u, 31u, 128u, 72u, 135u, 160u, 18u, 64u,
135u, 96u, 18u, 28u, 196u, 54u, 26u, 32u, 18u, 68u, 54u, 26u, 160u,
18u, 73u, 2u, 28u, 128u, 72u, 134u, 161u, 184u, 4u, 144u, 211u, 0u,
146u, 68u, 55u, 0u, 146u, 71u, 9u, 12u, 12u, 97u, 48u, 61u, 76u, 18u,
136u, 224u, 225u, 129u, 48u, 28u, 35u, 129u, 6u, 4u, 192u, 112u, 142u,
0u, 24u, 19u, 1u, 194u, 64u,};
static ScmObj libmacbasemacroP(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libmacbasemacroP__STUB, 1, 0,SCM_FALSE,libmacbasemacroP, NULL, NULL);

static ScmObj libmacbasesyntaxP(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libmacbasesyntaxP__STUB, 1, 0,SCM_FALSE,libmacbasesyntaxP, NULL, NULL);

static ScmObj libmacbasecompile_syntax_rules(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libmacbasecompile_syntax_rules__STUB, 7, 0,SCM_FALSE,libmacbasecompile_syntax_rules, NULL, NULL);

static ScmObj libmacbasemacro_transformer(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libmacbasemacro_transformer__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libmacbasemacro_transformer, NULL, NULL);

static ScmObj libmacbasemacro_name(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libmacbasemacro_name__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libmacbasemacro_name, NULL, NULL);

static ScmObj libmacbaseidentifier_macroP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libmacbaseidentifier_macroP__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libmacbaseidentifier_macroP, NULL, NULL);

static unsigned char uvector__00012[] = {
 0u, 3u, 133u, 6u, 6u, 104u, 205u, 184u, 145u, 193u, 195u, 2u, 96u,
56u, 71u, 2u, 12u, 9u, 128u, 225u, 28u, 0u, 48u, 38u, 3u, 132u, 128u,};
static unsigned char uvector__00013[] = {
 0u, 3u, 139u, 6u, 8u, 208u, 70u, 177u, 177u, 36u, 113u, 80u, 192u,
145u, 161u, 28u, 72u, 48u, 38u, 5u, 36u, 113u, 16u, 192u, 145u, 177u,
28u, 60u, 48u, 38u, 5u, 36u, 112u, 208u, 192u, 35u, 120u, 208u, 132u,
192u, 112u, 145u, 194u, 195u, 2u, 98u, 36u, 71u, 10u, 12u, 9u, 26u,
17u, 194u, 3u, 2u, 98u, 36u, 71u, 6u, 12u, 15u, 113u, 164u, 108u, 72u,
224u, 161u, 129u, 35u, 98u, 56u, 32u, 96u, 72u, 208u, 142u, 4u, 24u,
4u, 113u, 26u, 16u, 152u, 241u, 146u, 56u, 8u, 96u, 76u, 157u, 72u,
224u, 1u, 129u, 35u, 66u, 24u, 131u, 3u, 24u, 141u, 8u, 108u, 9u,
147u, 144u, 76u, 67u, 195u, 75u, 113u, 36u, 144u,};
static unsigned char uvector__00014[] = {
 0u, 3u, 139u, 6u, 8u, 228u, 71u, 65u, 176u, 8u, 226u, 52u, 33u, 238u,
52u, 141u, 137u, 2u, 55u, 141u, 8u, 141u, 4u, 107u, 27u, 18u, 67u,
75u, 113u, 38u, 51u, 36u, 113u, 64u, 192u, 145u, 153u, 28u, 72u, 48u,
49u, 136u, 208u, 166u, 6u, 100u, 113u, 0u, 192u, 145u, 177u, 28u, 60u,
48u, 50u, 1u, 27u, 8u, 132u, 71u, 113u, 225u, 36u, 135u, 8u, 216u,
61u, 4u, 192u, 112u, 146u, 71u, 13u, 12u, 9u, 142u, 33u, 28u, 44u,
48u, 38u, 58u, 36u, 112u, 160u, 192u, 145u, 225u, 28u, 32u, 48u, 38u,
58u, 36u, 112u, 96u, 192u, 152u, 226u, 17u, 192u, 131u, 3u, 220u,
102u, 252u, 72u, 224u, 1u, 129u, 35u, 50u, 24u, 131u, 3u, 24u, 143u,
8u, 118u, 9u, 153u, 104u, 143u, 99u, 96u, 152u, 226u, 4u, 192u, 112u,
146u, 72u,};
static unsigned char uvector__00015[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 31u, 71u, 132u, 59u, 7u, 184u, 205u, 248u,
136u, 246u, 54u, 17u, 8u, 142u, 227u, 194u, 68u, 114u, 35u, 160u,
216u, 4u, 113u, 26u, 16u, 247u, 26u, 70u, 196u, 129u, 27u, 198u, 132u,
70u, 130u, 53u, 141u, 137u, 33u, 165u, 184u, 147u, 25u, 146u, 73u,
28u, 36u, 48u, 49u, 132u, 192u, 197u, 48u, 62u, 35u, 131u, 134u, 4u,
192u, 112u, 142u, 4u, 24u, 19u, 1u, 194u, 56u, 0u, 96u, 76u, 7u, 9u,
0u,};
static unsigned char uvector__00016[] = {
 0u, 3u, 194u, 52u, 24u, 18u, 63u, 35u, 194u, 38u, 24u, 36u, 1u, 32u,
146u, 71u, 132u, 68u, 48u, 38u, 11u, 228u, 120u, 67u, 195u, 2u, 96u,
190u, 71u, 132u, 52u, 48u, 38u, 10u, 68u, 120u, 66u, 195u, 4u, 133u,
33u, 137u, 4u, 146u, 60u, 33u, 33u, 129u, 49u, 68u, 35u, 194u, 14u,
24u, 19u, 20u, 66u, 60u, 32u, 97u, 129u, 49u, 45u, 35u, 194u, 2u, 24u,
36u, 64u, 213u, 34u, 153u, 24u, 72u, 38u, 71u, 110u, 146u, 27u, 137u,
29u, 224u, 48u, 38u, 72u, 36u, 119u, 96u, 192u, 153u, 32u, 145u, 220u,
131u, 2u, 100u, 62u, 71u, 112u, 12u, 9u, 144u, 145u, 29u, 184u, 48u,
72u, 82u, 72u, 144u, 73u, 35u, 182u, 6u, 4u, 205u, 128u, 142u, 212u,
24u, 19u, 54u, 2u, 59u, 48u, 96u, 76u, 210u, 72u, 236u, 129u, 130u,
68u, 13u, 18u, 41u, 145u, 132u, 130u, 100u, 118u, 233u, 33u, 184u,
145u, 214u, 195u, 2u, 104u, 68u, 41u, 29u, 100u, 48u, 38u, 132u, 66u,
145u, 213u, 67u, 2u, 104u, 66u, 25u, 29u, 76u, 48u, 38u, 132u, 11u,
17u, 212u, 67u, 4u, 133u, 37u, 9u, 4u, 146u, 58u, 120u, 96u, 77u, 10u,
220u, 35u, 166u, 134u, 4u, 208u, 173u, 194u, 58u, 72u, 96u, 77u, 10u,
197u, 35u, 164u, 6u, 6u, 64u, 36u, 81u, 8u, 7u, 57u, 46u, 63u, 36u,
144u, 154u, 21u, 138u, 19u, 66u, 5u, 130u, 102u, 146u, 19u, 33u, 32u,
152u, 150u, 132u, 193u, 72u, 145u, 209u, 131u, 2u, 104u, 100u, 249u,
29u, 20u, 48u, 38u, 134u, 102u, 17u, 209u, 3u, 2u, 71u, 228u, 116u,
48u, 192u, 146u, 89u, 29u, 4u, 48u, 38u, 134u, 79u, 145u, 207u, 67u,
4u, 125u, 30u, 18u, 57u, 224u, 96u, 72u, 240u, 142u, 116u, 24u, 19u,
68u, 19u, 72u, 231u, 1u, 129u, 224u, 56u, 70u, 97u, 52u, 65u, 52u,
134u, 217u, 20u, 38u, 134u, 79u, 132u, 208u, 172u, 80u, 154u, 16u,
44u, 19u, 52u, 144u, 153u, 9u, 4u, 196u, 180u, 38u, 10u, 68u, 145u,
205u, 131u, 2u, 70u, 100u, 115u, 64u, 192u, 144u, 121u, 28u, 192u,
48u, 36u, 152u, 71u, 46u, 12u, 18u, 108u, 152u, 24u, 100u, 227u, 37u,
146u, 57u, 104u, 96u, 73u, 44u, 142u, 84u, 24u, 18u, 76u, 35u, 148u,
6u, 4u, 209u, 73u, 50u, 57u, 56u, 96u, 100u, 2u, 76u, 12u, 162u, 79u,
131u, 204u, 30u, 37u, 0u, 209u, 7u, 144u, 213u, 7u, 146u, 73u, 9u,
162u, 146u, 114u, 97u, 35u, 146u, 134u, 4u, 209u, 109u, 162u, 57u,
32u, 96u, 77u, 23u, 31u, 35u, 145u, 6u, 4u, 209u, 112u, 114u, 57u, 0u,
96u, 77u, 22u, 218u, 35u, 142u, 134u, 4u, 131u, 200u, 227u, 97u, 129u,
52u, 90u, 44u, 142u, 50u, 24u, 19u, 69u, 165u, 136u, 227u, 1u, 129u,
32u, 242u, 56u, 176u, 96u, 77u, 22u, 150u, 35u, 137u, 134u, 4u, 131u,
200u, 226u, 33u, 129u, 238u, 75u, 131u, 201u, 28u, 64u, 48u, 36u, 30u,
71u, 15u, 12u, 9u, 37u, 145u, 195u, 67u, 3u, 40u, 112u, 16u, 236u,
30u, 67u, 200u, 77u, 27u, 206u, 36u, 69u, 210u, 96u, 77u, 22u, 139u,
9u, 162u, 146u, 102u, 15u, 36u, 112u, 192u, 192u, 154u, 58u, 38u, 71u,
11u, 12u, 9u, 7u, 145u, 194u, 67u, 0u, 148u, 99u, 195u, 37u, 202u,
68u, 142u, 14u, 24u, 19u, 71u, 212u, 8u, 224u, 193u, 129u, 35u, 194u,
56u, 32u, 96u, 77u, 31u, 80u, 35u, 129u, 134u, 4u, 148u, 136u, 224u,
65u, 129u, 36u, 178u, 56u, 0u, 96u, 77u, 31u, 77u, 33u, 136u, 48u,
49u, 136u, 242u, 75u, 148u, 136u, 134u, 64u, 131u, 194u, 104u, 250u,
105u, 17u, 248u, 77u, 28u, 221u, 36u, 38u, 136u, 175u, 71u, 228u,
146u,};
static unsigned char uvector__00017[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 41u, 199u, 146u, 92u, 164u, 68u, 50u, 4u,
30u, 4u, 163u, 30u, 25u, 46u, 82u, 36u, 71u, 225u, 148u, 56u, 8u,
118u, 15u, 33u, 228u, 61u, 201u, 112u, 121u, 36u, 69u, 210u, 96u,
101u, 18u, 124u, 30u, 96u, 241u, 40u, 6u, 136u, 60u, 134u, 168u, 60u,
146u, 36u, 217u, 48u, 48u, 201u, 198u, 75u, 38u, 15u, 36u, 135u, 128u,
225u, 25u, 136u, 250u, 60u, 36u, 54u, 200u, 162u, 16u, 14u, 114u, 92u,
126u, 72u, 144u, 164u, 161u, 32u, 146u, 36u, 64u, 209u, 34u, 153u,
24u, 72u, 38u, 71u, 110u, 146u, 27u, 136u, 144u, 164u, 145u, 32u,
146u, 36u, 64u, 213u, 34u, 153u, 24u, 72u, 38u, 71u, 110u, 146u, 27u,
136u, 144u, 164u, 49u, 32u, 146u, 36u, 1u, 32u, 146u, 76u, 126u, 73u,
28u, 36u, 48u, 49u, 132u, 192u, 197u, 48u, 82u, 35u, 131u, 134u, 4u,
192u, 112u, 142u, 4u, 24u, 19u, 1u, 194u, 56u, 0u, 96u, 76u, 7u, 9u,
0u,};
static unsigned char uvector__00018[] = {
 0u, 3u, 137u, 134u, 9u, 78u, 60u, 143u, 37u, 34u, 71u, 18u, 12u, 9u,
41u, 17u, 196u, 67u, 2u, 71u, 132u, 113u, 0u, 192u, 145u, 225u, 28u,
56u, 48u, 51u, 202u, 145u, 225u, 35u, 134u, 134u, 4u, 143u, 8u, 225u,
33u, 129u, 48u, 225u, 35u, 130u, 134u, 6u, 241u, 42u, 199u, 132u, 38u,
28u, 36u, 142u, 6u, 24u, 19u, 23u, 242u, 56u, 16u, 96u, 72u, 240u,
142u, 0u, 24u, 19u, 23u, 242u, 24u, 131u, 3u, 24u, 143u, 37u, 34u,
19u, 23u, 64u, 152u, 14u, 18u, 64u,};
static unsigned char uvector__00019[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 43u, 71u, 146u, 145u, 13u, 226u, 85u,
143u, 8u, 103u, 149u, 35u, 194u, 68u, 167u, 30u, 71u, 146u, 145u, 36u,
112u, 144u, 192u, 198u, 19u, 3u, 20u, 193u, 32u, 142u, 14u, 24u, 19u,
1u, 194u, 56u, 16u, 96u, 76u, 7u, 8u, 224u, 1u, 129u, 48u, 28u, 36u,};
static ScmObj libmacbasemake_syntax(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libmacbasemake_syntax__STUB, 3, 0,SCM_FALSE,libmacbasemake_syntax, NULL, NULL);

static ScmObj libmacbasecall_syntax_handler(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libmacbasecall_syntax_handler__STUB, 3, 0,SCM_FALSE,libmacbasecall_syntax_handler, NULL, NULL);

static ScmObj libmacbasesyntax_handler(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libmacbasesyntax_handler__STUB, 1, 0,SCM_FALSE,libmacbasesyntax_handler, NULL, NULL);

static unsigned char uvector__00020[] = {
 0u, 3u, 130u, 6u, 8u, 222u, 87u, 36u, 112u, 48u, 192u, 146u, 185u,
28u, 4u, 48u, 71u, 18u, 185u, 35u, 128u, 6u, 4u, 149u, 200u, 98u, 12u,
12u, 98u, 87u, 33u, 216u, 38u, 15u, 33u, 48u, 28u, 36u, 144u,};
static unsigned char uvector__00021[] = {
 0u, 3u, 151u, 134u, 4u, 140u, 200u, 229u, 161u, 130u, 88u, 18u, 204u,
181u, 45u, 146u, 71u, 44u, 12u, 9u, 130u, 153u, 28u, 172u, 48u, 36u,
182u, 71u, 42u, 12u, 9u, 45u, 17u, 202u, 3u, 2u, 96u, 140u, 71u, 38u,
12u, 9u, 45u, 145u, 201u, 67u, 3u, 32u, 18u, 216u, 108u, 1u, 230u,
51u, 33u, 133u, 73u, 2u, 92u, 140u, 204u, 102u, 67u, 75u, 113u, 36u,
135u, 9u, 108u, 61u, 9u, 118u, 51u, 9u, 130u, 49u, 36u, 145u, 199u,
67u, 2u, 70u, 100u, 113u, 176u, 192u, 152u, 225u, 145u, 198u, 67u, 2u,
99u, 140u, 71u, 23u, 12u, 9u, 25u, 145u, 197u, 67u, 2u, 99u, 140u,
71u, 19u, 12u, 9u, 140u, 153u, 28u, 68u, 48u, 36u, 102u, 71u, 15u,
12u, 12u, 242u, 244u, 190u, 72u, 225u, 193u, 129u, 37u, 242u, 56u,
80u, 96u, 76u, 236u, 200u, 224u, 193u, 129u, 188u, 76u, 2u, 97u, 14u,
194u, 56u, 149u, 200u, 141u, 229u, 114u, 76u, 180u, 66u, 103u, 102u,
72u, 224u, 129u, 129u, 52u, 34u, 68u, 142u, 6u, 24u, 18u, 90u, 35u,
129u, 6u, 4u, 152u, 136u, 224u, 1u, 129u, 52u, 34u, 68u, 134u, 32u,
192u, 198u, 37u, 162u, 19u, 66u, 33u, 132u, 123u, 45u, 132u, 198u,
32u, 38u, 69u, 132u, 146u,};
static unsigned char uvector__00022[] = {
 0u, 3u, 188u, 6u, 4u, 140u, 200u, 238u, 161u, 130u, 99u, 151u, 201u,
29u, 208u, 48u, 36u, 190u, 71u, 114u, 12u, 9u, 130u, 49u, 29u, 188u,
48u, 76u, 146u, 249u, 35u, 183u, 6u, 4u, 153u, 8u, 237u, 161u, 129u,
37u, 242u, 59u, 88u, 96u, 76u, 52u, 72u, 237u, 33u, 129u, 148u, 75u,
146u, 249u, 9u, 134u, 136u, 76u, 17u, 137u, 29u, 156u, 48u, 38u, 50u,
36u, 118u, 96u, 192u, 146u, 249u, 29u, 144u, 48u, 38u, 50u, 36u, 117u,
192u, 193u, 49u, 203u, 228u, 142u, 182u, 24u, 18u, 95u, 35u, 172u,
134u, 4u, 201u, 220u, 142u, 172u, 24u, 38u, 73u, 124u, 145u, 213u,
67u, 2u, 76u, 132u, 117u, 64u, 192u, 146u, 249u, 29u, 72u, 48u, 38u,
96u, 68u, 117u, 0u, 192u, 202u, 37u, 201u, 124u, 132u, 204u, 8u, 38u,
78u, 228u, 142u, 156u, 24u, 19u, 60u, 34u, 58u, 104u, 96u, 73u, 124u,
142u, 150u, 24u, 19u, 60u, 34u, 58u, 56u, 96u, 72u, 204u, 142u, 136u,
24u, 26u, 195u, 84u, 190u, 73u, 29u, 12u, 48u, 38u, 132u, 206u, 145u,
208u, 67u, 3u, 40u, 112u, 19u, 40u, 104u, 151u, 203u, 113u, 9u, 161u,
50u, 68u, 76u, 196u, 38u, 118u, 228u, 142u, 126u, 24u, 19u, 66u, 166u,
200u, 231u, 129u, 129u, 52u, 43u, 32u, 142u, 116u, 24u, 19u, 66u,
166u, 200u, 230u, 193u, 130u, 99u, 151u, 201u, 28u, 212u, 48u, 36u,
190u, 71u, 51u, 12u, 9u, 161u, 160u, 196u, 115u, 0u, 193u, 50u, 75u,
228u, 142u, 94u, 24u, 18u, 100u, 35u, 151u, 6u, 4u, 151u, 200u, 229u,
129u, 129u, 52u, 56u, 92u, 142u, 84u, 24u, 25u, 68u, 185u, 47u, 144u,
154u, 28u, 46u, 19u, 67u, 65u, 137u, 28u, 160u, 48u, 38u, 135u, 206u,
145u, 201u, 195u, 2u, 75u, 228u, 114u, 80u, 192u, 154u, 31u, 58u, 71u,
33u, 12u, 9u, 25u, 145u, 199u, 67u, 3u, 88u, 106u, 151u, 201u, 35u,
142u, 6u, 4u, 209u, 33u, 2u, 56u, 208u, 96u, 101u, 19u, 40u, 104u,
151u, 203u, 241u, 12u, 161u, 52u, 72u, 20u, 76u, 228u, 38u, 135u,
201u, 16u, 154u, 21u, 10u, 72u, 227u, 1u, 129u, 52u, 76u, 12u, 142u,
40u, 24u, 19u, 68u, 196u, 8u, 226u, 65u, 129u, 52u, 76u, 12u, 142u,
32u, 24u, 25u, 68u, 59u, 47u, 144u, 154u, 37u, 176u, 19u, 24u, 98u,
71u, 15u, 12u, 9u, 162u, 184u, 164u, 112u, 224u, 192u, 146u, 249u,
28u, 40u, 48u, 53u, 203u, 228u, 142u, 18u, 24u, 18u, 95u, 35u, 132u,
6u, 6u, 64u, 38u, 64u, 198u, 37u, 162u, 27u, 196u, 192u, 38u, 16u,
236u, 35u, 137u, 92u, 136u, 222u, 87u, 36u, 203u, 68u, 51u, 203u,
210u, 249u, 34u, 61u, 150u, 195u, 96u, 15u, 49u, 153u, 12u, 42u, 72u,
18u, 228u, 102u, 99u, 50u, 26u, 91u, 137u, 18u, 236u, 102u, 37u, 129u,
44u, 203u, 82u, 217u, 36u, 146u, 38u, 96u, 198u, 162u, 93u, 140u,
219u, 137u, 34u, 103u, 12u, 106u, 37u, 216u, 205u, 248u, 146u, 38u,
128u, 198u, 173u, 196u, 144u, 202u, 19u, 69u, 210u, 68u, 208u, 66u,
104u, 172u, 209u, 36u, 112u, 96u, 192u, 198u, 154u, 48u, 142u, 71u,
4u, 12u, 19u, 76u, 136u, 72u, 224u, 65u, 129u, 36u, 66u, 56u, 0u, 96u,
77u, 30u, 67u, 33u, 136u, 48u, 49u, 203u, 225u, 52u, 121u, 12u, 77u,
82u, 248u, 84u, 209u, 204u, 48u, 31u, 138u, 104u, 224u, 248u, 13u,
197u, 52u, 110u, 108u, 9u, 106u, 107u, 41u, 163u, 10u, 230u, 51u, 36u,
128u,};
static unsigned char uvector__00023[] = {
 0u, 3u, 135u, 6u, 8u, 34u, 9u, 19u, 98u, 75u, 226u, 105u, 145u, 8u,
154u, 165u, 240u, 173u, 196u, 7u, 226u, 37u, 216u, 205u, 248u, 144u,
27u, 136u, 151u, 99u, 54u, 226u, 64u, 150u, 166u, 178u, 27u, 196u,
192u, 38u, 16u, 236u, 35u, 137u, 92u, 136u, 222u, 87u, 36u, 203u, 68u,
51u, 203u, 210u, 249u, 34u, 61u, 150u, 195u, 96u, 15u, 49u, 153u, 12u,
42u, 72u, 18u, 228u, 102u, 99u, 50u, 26u, 91u, 137u, 18u, 236u, 102u,
37u, 129u, 44u, 203u, 82u, 217u, 36u, 147u, 25u, 146u, 56u, 88u, 96u,
99u, 151u, 211u, 4u, 178u, 56u, 72u, 96u, 76u, 7u, 8u, 224u, 129u,
129u, 48u, 28u, 35u, 129u, 6u, 4u, 192u, 112u, 144u,};
static unsigned char uvector__00024[] = {
 0u, 3u, 129u, 134u, 9u, 182u, 110u, 155u, 201u, 28u, 8u, 48u, 36u,
218u, 71u, 1u, 12u, 9u, 47u, 145u, 192u, 3u, 2u, 77u, 196u, 49u, 6u,
6u, 49u, 55u, 16u, 152u, 14u, 18u, 64u,};
static unsigned char uvector__00025[] = {
 0u, 3u, 156u, 134u, 4u, 140u, 200u, 230u, 225u, 130u, 99u, 151u,
201u, 28u, 216u, 48u, 36u, 190u, 71u, 52u, 12u, 9u, 130u, 49u, 28u,
180u, 48u, 36u, 102u, 71u, 44u, 12u, 9u, 45u, 145u, 201u, 195u, 3u,
92u, 182u, 72u, 228u, 193u, 129u, 37u, 178u, 57u, 40u, 96u, 100u, 2u,
91u, 19u, 128u, 156u, 103u, 41u, 206u, 37u, 49u, 153u, 36u, 75u, 177u,
152u, 101u, 9u, 137u, 43u, 116u, 182u, 73u, 35u, 145u, 134u, 4u, 198u,
108u, 142u, 66u, 24u, 18u, 51u, 35u, 143u, 6u, 6u, 49u, 55u, 17u, 54u,
205u, 211u, 121u, 36u, 113u, 176u, 192u, 147u, 145u, 28u, 100u, 48u,
78u, 128u, 77u, 179u, 145u, 19u, 124u, 74u, 72u, 99u, 76u, 173u, 9u,
28u, 92u, 48u, 38u, 51u, 100u, 113u, 80u, 192u, 240u, 37u, 200u, 204u,
134u, 217u, 108u, 38u, 51u, 97u, 49u, 233u, 36u, 142u, 38u, 24u, 19u,
62u, 162u, 56u, 136u, 96u, 72u, 204u, 142u, 30u, 24u, 19u, 62u, 162u,
56u, 104u, 96u, 101u, 18u, 228u, 190u, 68u, 235u, 47u, 144u, 152u,
35u, 18u, 56u, 88u, 96u, 77u, 9u, 211u, 35u, 133u, 6u, 4u, 151u, 200u,
225u, 1u, 129u, 52u, 39u, 76u, 142u, 8u, 24u, 18u, 51u, 35u, 128u,
134u, 6u, 185u, 124u, 145u, 192u, 3u, 2u, 75u, 228u, 49u, 6u, 6u, 57u,
124u, 77u, 82u, 248u, 81u, 46u, 198u, 109u, 196u, 129u, 18u, 205u,
100u, 38u, 123u, 228u, 198u, 100u, 144u,};
static unsigned char uvector__00026[] = {
 0u, 3u, 134u, 6u, 8u, 34u, 9u, 19u, 178u, 75u, 226u, 106u, 151u,
194u, 137u, 118u, 51u, 110u, 36u, 8u, 150u, 107u, 33u, 224u, 75u,
145u, 153u, 13u, 178u, 216u, 156u, 4u, 227u, 57u, 78u, 113u, 41u,
140u, 200u, 151u, 99u, 48u, 202u, 26u, 229u, 178u, 221u, 45u, 146u,
73u, 49u, 153u, 35u, 132u, 134u, 6u, 57u, 125u, 48u, 75u, 35u, 131u,
134u, 4u, 192u, 112u, 142u, 4u, 24u, 19u, 1u, 194u, 56u, 0u, 96u, 76u,
7u, 9u, 0u,};
static ScmObj SCM_debug_info_const_vector();
#if defined(__CYGWIN__) || defined(GAUCHE_WINDOWS)
#define SCM_CGEN_CONST /*empty*/
#else
#define SCM_CGEN_CONST const
#endif
static SCM_CGEN_CONST struct scm__scRec {
  ScmString d1789[156];
} scm__sc SCM_UNUSED = {
  {   /* ScmString d1789 */
      SCM_STRING_CONST_INITIALIZER("%expression-name-mark-key", 25, 25),
      SCM_STRING_CONST_INITIALIZER("macroexpand", 11, 11),
      SCM_STRING_CONST_INITIALIZER("too many arguments for", 22, 22),
      SCM_STRING_CONST_INITIALIZER("lambda", 6, 6),
      SCM_STRING_CONST_INITIALIZER("form", 4, 4),
      SCM_STRING_CONST_INITIALIZER("optional", 8, 8),
      SCM_STRING_CONST_INITIALIZER("env/flag", 8, 8),
      SCM_STRING_CONST_INITIALIZER("%do-macroexpand", 15, 15),
      SCM_STRING_CONST_INITIALIZER("error", 5, 5),
      SCM_STRING_CONST_INITIALIZER("gauche.internal", 15, 15),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libmacbase.scm", 14, 14),
      SCM_STRING_CONST_INITIALIZER("%toplevel", 9, 9),
      SCM_STRING_CONST_INITIALIZER("macroexpand-1", 13, 13),
      SCM_STRING_CONST_INITIALIZER("boolean\077", 8, 8),
      SCM_STRING_CONST_INITIALIZER("vm-current-module", 17, 17),
      SCM_STRING_CONST_INITIALIZER("module\077", 7, 7),
      SCM_STRING_CONST_INITIALIZER("argument must be a boolean or a module, but got:", 48, 48),
      SCM_STRING_CONST_INITIALIZER("make-cenv", 9, 9),
      SCM_STRING_CONST_INITIALIZER("%internal-macro-expand", 22, 22),
      SCM_STRING_CONST_INITIALIZER("unravel-syntax", 14, 14),
      SCM_STRING_CONST_INITIALIZER("once\077", 5, 5),
      SCM_STRING_CONST_INITIALIZER("unwrap-syntax", 13, 13),
      SCM_STRING_CONST_INITIALIZER("to_immutable", 12, 12),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<top>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("*", 1, 1),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("unwrap-syntax-1", 15, 15),
      SCM_STRING_CONST_INITIALIZER("obj", 3, 3),
      SCM_STRING_CONST_INITIALIZER("wrapped-identifier\077", 19, 19),
      SCM_STRING_CONST_INITIALIZER("vector-map", 10, 10),
      SCM_STRING_CONST_INITIALIZER("rec", 3, 3),
      SCM_STRING_CONST_INITIALIZER("eq\077", 3, 3),
      SCM_STRING_CONST_INITIALIZER("make-hash-table", 15, 15),
      SCM_STRING_CONST_INITIALIZER("hash-table-get", 14, 14),
      SCM_STRING_CONST_INITIALIZER("make-identifier", 15, 15),
      SCM_STRING_CONST_INITIALIZER("global-identifier=\077", 19, 19),
      SCM_STRING_CONST_INITIALIZER("hash-table-put!", 15, 15),
      SCM_STRING_CONST_INITIALIZER("id->sym", 7, 7),
      SCM_STRING_CONST_INITIALIZER("i", 1, 1),
      SCM_STRING_CONST_INITIALIZER("_", 1, 1),
      SCM_STRING_CONST_INITIALIZER("c", 1, 1),
      SCM_STRING_CONST_INITIALIZER("unused-args", 11, 11),
      SCM_STRING_CONST_INITIALIZER("hash-table-fold", 15, 15),
      SCM_STRING_CONST_INITIALIZER(".", 1, 1),
      SCM_STRING_CONST_INITIALIZER("symbol-append", 13, 13),
      SCM_STRING_CONST_INITIALIZER("id", 2, 2),
      SCM_STRING_CONST_INITIALIZER("macro\077", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<boolean>", 9, 9),
      SCM_STRING_CONST_INITIALIZER("syntax\077", 7, 7),
      SCM_STRING_CONST_INITIALIZER("compile-syntax-rules", 20, 20),
      SCM_STRING_CONST_INITIALIZER("name", 4, 4),
      SCM_STRING_CONST_INITIALIZER("src", 3, 3),
      SCM_STRING_CONST_INITIALIZER("ellipsis", 8, 8),
      SCM_STRING_CONST_INITIALIZER("literals", 8, 8),
      SCM_STRING_CONST_INITIALIZER("rules", 5, 5),
      SCM_STRING_CONST_INITIALIZER("mod", 3, 3),
      SCM_STRING_CONST_INITIALIZER("env", 3, 3),
      SCM_STRING_CONST_INITIALIZER("macro-transformer", 17, 17),
      SCM_STRING_CONST_INITIALIZER("mac", 3, 3),
      SCM_STRING_CONST_INITIALIZER("<macro>", 7, 7),
      SCM_STRING_CONST_INITIALIZER("macro-name", 10, 10),
      SCM_STRING_CONST_INITIALIZER("identifier-macro\077", 17, 17),
      SCM_STRING_CONST_INITIALIZER("*trace-macro*", 13, 13),
      SCM_STRING_CONST_INITIALIZER("%macro-traced\077", 14, 14),
      SCM_STRING_CONST_INITIALIZER("regexp\077", 7, 7),
      SCM_STRING_CONST_INITIALIZER("symbol->string", 14, 14),
      SCM_STRING_CONST_INITIALIZER("z", 1, 1),
      SCM_STRING_CONST_INITIALIZER("find", 4, 4),
      SCM_STRING_CONST_INITIALIZER("call-macro-expander", 19, 19),
      SCM_STRING_CONST_INITIALIZER("extended-pair\077", 14, 14),
      SCM_STRING_CONST_INITIALIZER("extended-cons", 13, 13),
      SCM_STRING_CONST_INITIALIZER("original", 8, 8),
      SCM_STRING_CONST_INITIALIZER("pair-attribute-set!", 19, 19),
      SCM_STRING_CONST_INITIALIZER("Macro input>>>\012", 15, 15),
      SCM_STRING_CONST_INITIALIZER("current-trace-port", 18, 18),
      SCM_STRING_CONST_INITIALIZER("display", 7, 7),
      SCM_STRING_CONST_INITIALIZER("port", 4, 4),
      SCM_STRING_CONST_INITIALIZER("level", 5, 5),
      SCM_STRING_CONST_INITIALIZER("length", 6, 6),
      SCM_STRING_CONST_INITIALIZER("pprint", 6, 6),
      SCM_STRING_CONST_INITIALIZER("\012Macro output<<<\012", 17, 17),
      SCM_STRING_CONST_INITIALIZER("\012", 1, 1),
      SCM_STRING_CONST_INITIALIZER("flush", 5, 5),
      SCM_STRING_CONST_INITIALIZER("expr", 4, 4),
      SCM_STRING_CONST_INITIALIZER("cenv", 4, 4),
      SCM_STRING_CONST_INITIALIZER("call-id-macro-expander", 22, 22),
      SCM_STRING_CONST_INITIALIZER("Non-identifier-macro can't appear in this context:", 50, 50),
      SCM_STRING_CONST_INITIALIZER("make-syntax", 11, 11),
      SCM_STRING_CONST_INITIALIZER("module", 6, 6),
      SCM_STRING_CONST_INITIALIZER("proc", 4, 4),
      SCM_STRING_CONST_INITIALIZER("<symbol>", 8, 8),
      SCM_STRING_CONST_INITIALIZER("<module>", 8, 8),
      SCM_STRING_CONST_INITIALIZER("call-syntax-handler", 19, 19),
      SCM_STRING_CONST_INITIALIZER("syn", 3, 3),
      SCM_STRING_CONST_INITIALIZER("program", 7, 7),
      SCM_STRING_CONST_INITIALIZER("syntax-handler", 14, 14),
      SCM_STRING_CONST_INITIALIZER("trace-macro", 11, 11),
      SCM_STRING_CONST_INITIALIZER("G1805", 5, 5),
      SCM_STRING_CONST_INITIALIZER("x", 1, 1),
      SCM_STRING_CONST_INITIALIZER("procedure\077", 10, 10),
      SCM_STRING_CONST_INITIALIZER("every", 5, 5),
      SCM_STRING_CONST_INITIALIZER("Argument(s) of trace-macro should be a boolean, or symbols or regexps, but got:", 79, 79),
      SCM_STRING_CONST_INITIALIZER("list\077", 5, 5),
      SCM_STRING_CONST_INITIALIZER("delete-duplicates", 17, 17),
      SCM_STRING_CONST_INITIALIZER("G1803", 5, 5),
      SCM_STRING_CONST_INITIALIZER("objs", 4, 4),
      SCM_STRING_CONST_INITIALIZER("equal\077", 6, 6),
      SCM_STRING_CONST_INITIALIZER("match:error", 11, 11),
      SCM_STRING_CONST_INITIALIZER("util.match", 10, 10),
      SCM_STRING_CONST_INITIALIZER("args", 4, 4),
      SCM_STRING_CONST_INITIALIZER("untrace-macro", 13, 13),
      SCM_STRING_CONST_INITIALIZER("member", 6, 6),
      SCM_STRING_CONST_INITIALIZER("G1813", 5, 5),
      SCM_STRING_CONST_INITIALIZER("remove", 6, 6),
      SCM_STRING_CONST_INITIALIZER("G1786", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1787", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest1785", 8, 8),
      SCM_STRING_CONST_INITIALIZER("define-in-module", 16, 16),
      SCM_STRING_CONST_INITIALIZER("G1795", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1796", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest1794", 8, 8),
      SCM_STRING_CONST_INITIALIZER("r", 1, 1),
      SCM_STRING_CONST_INITIALIZER("let*", 4, 4),
      SCM_STRING_CONST_INITIALIZER("vector\077", 7, 7),
      SCM_STRING_CONST_INITIALIZER("pair\077", 5, 5),
      SCM_STRING_CONST_INITIALIZER("nam", 3, 3),
      SCM_STRING_CONST_INITIALIZER("sym", 3, 3),
      SCM_STRING_CONST_INITIALIZER("tab", 3, 3),
      SCM_STRING_CONST_INITIALIZER("num", 3, 3),
      SCM_STRING_CONST_INITIALIZER("$", 1, 1),
      SCM_STRING_CONST_INITIALIZER("rlet1", 5, 5),
      SCM_STRING_CONST_INITIALIZER("macname", 7, 7),
      SCM_STRING_CONST_INITIALIZER("symbol\077", 7, 7),
      SCM_STRING_CONST_INITIALIZER("^z", 2, 2),
      SCM_STRING_CONST_INITIALIZER("and-let1", 8, 8),
      SCM_STRING_CONST_INITIALIZER("out", 3, 3),
      SCM_STRING_CONST_INITIALIZER("unraveled", 9, 9),
      SCM_STRING_CONST_INITIALIZER("p", 1, 1),
      SCM_STRING_CONST_INITIALIZER("append", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ms", 2, 2),
      SCM_STRING_CONST_INITIALIZER("set!", 4, 4),
      SCM_STRING_CONST_INITIALIZER("^x", 2, 2),
      SCM_STRING_CONST_INITIALIZER("G1799", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1798", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1797", 5, 5),
      SCM_STRING_CONST_INITIALIZER("match", 5, 5),
      SCM_STRING_CONST_INITIALIZER("...", 3, 3),
      SCM_STRING_CONST_INITIALIZER("G1812", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1814", 5, 5),
      SCM_STRING_CONST_INITIALIZER("cute", 4, 4),
      SCM_STRING_CONST_INITIALIZER("<>", 2, 2),
      SCM_STRING_CONST_INITIALIZER("letrec", 6, 6),
      SCM_STRING_CONST_INITIALIZER("G1810", 5, 5),
  },
};
static struct scm__rcRec {
  ScmUVector d1793[26];
  ScmCompiledCode d1792[26];
  ScmWord d1791[881];
  ScmPair d1790[230] SCM_ALIGN_PAIR;
  ScmObj d1788[444];
} scm__rc SCM_UNUSED = {
  {   /* ScmUVector d1793 */
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 135, uvector__00001, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 48, uvector__00002, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 135, uvector__00003, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 48, uvector__00004, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 202, uvector__00005, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 83, uvector__00006, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 193, uvector__00007, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 64, uvector__00008, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 346, uvector__00009, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 197, uvector__00010, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 176, uvector__00011, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 27, uvector__00012, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 114, uvector__00013, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 132, uvector__00014, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 79, uvector__00015, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 572, uvector__00016, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 156, uvector__00017, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 86, uvector__00018, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 53, uvector__00019, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 39, uvector__00020, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 198, uvector__00021, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 536, uvector__00022, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 113, uvector__00023, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 35, uvector__00024, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 227, uvector__00025, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 79, uvector__00026, 0, NULL),
  },
  {   /* ScmCompiledCode d1792 */
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* macroexpand */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[0])), 35,
            17, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1790[15]),
            SCM_OBJ(&scm__rc.d1792[1]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[35])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* macroexpand-1 */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[50])), 36,
            17, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1790[27]),
            SCM_OBJ(&scm__rc.d1792[3]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[86])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %do-macroexpand */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[101])), 53,
            24, 3, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1790[35]),
            SCM_OBJ(&scm__rc.d1792[5]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[154])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* rec */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[169])), 39,
            12, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1790[60]),
            SCM_OBJ(&scm__rc.d1792[9]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[208])), 12,
            11, 3, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1790[75]),
            SCM_OBJ(&scm__rc.d1792[8]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* id->sym */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[220])), 67,
            30, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1790[83]),
            SCM_OBJ(&scm__rc.d1792[9]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* unravel-syntax */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[287])), 17,
            16, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1790[88]),
            SCM_OBJ(&scm__rc.d1792[10]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[304])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[319])), 13,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[332])), 25,
            11, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1790[151]),
            SCM_OBJ(&scm__rc.d1792[13]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %macro-traced? */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[357])), 25,
            18, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1790[156]),
            SCM_OBJ(&scm__rc.d1792[14]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[382])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* call-macro-expander */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[397])), 155,
            32, 3, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1790[164]),
            SCM_OBJ(&scm__rc.d1792[16]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[552])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* call-id-macro-expander */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[567])), 22,
            12, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1790[170]),
            SCM_OBJ(&scm__rc.d1792[18]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[589])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* (trace-macro G1805) */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[604])), 7,
            4, 1, 0, SCM_OBJ(&scm__rc.d1790[202]), SCM_NIL, SCM_OBJ(&scm__rc.d1790[208]),
            SCM_OBJ(&scm__rc.d1792[22]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* G1803 */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[611])), 50,
            15, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1790[214]),
            SCM_OBJ(&scm__rc.d1792[21]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* trace-macro */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[661])), 123,
            16, 0, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1790[219]),
            SCM_OBJ(&scm__rc.d1792[22]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[784])), 17,
            14, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[801])), 5,
            5, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1790[224]),
            SCM_OBJ(&scm__rc.d1792[24]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* untrace-macro */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[806])), 60,
            12, 0, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1790[229]),
            SCM_OBJ(&scm__rc.d1792[25]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1791[866])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
  },
  {   /* ScmWord d1791 */
    /* macroexpand */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[0]) + 6),
    0x00000004    /*   3 (CONSTF) */,
    0x00000013    /*   4 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[0]) + 7),
    0x0000006a    /*   6 (LREF0-CAR) */,
    0x0000000d    /*   7 (PUSH) */,
    0x0000003d    /*   8 (LREF0) */,
    0x00000022    /*   9 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[0]) + 14),
    0x00000003    /*  11 (CONSTN) */,
    0x00000013    /*  12 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[0]) + 15),
    0x00000076    /*  14 (LREF0-CDR) */,
    0x00002018    /*  15 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  16 (LREF0) */,
    0x00000022    /*  17 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[0]) + 21),
    0x00000013    /*  19 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[0]) + 29),
    0x0000200e    /*  21 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[0]) + 29),
    0x00000006    /*  23 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1789[2])) /* "too many arguments for" */,
    0x00000006    /*  25 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[10])) /* (lambda (form :optional (env/flag #f)) (%do-macroexpand form env/flag #f)) */,
    0x0000205f    /*  27 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.2d3ce860> */,
    0x0000004d    /*  29 (LREF11-PUSH) */,
    0x00000049    /*  30 (LREF1-PUSH) */,
    0x00000009    /*  31 (CONSTF-PUSH) */,
    0x00003060    /*  32 (GREF-TAIL-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%do-macroexpand.2bc72fa0> */,
    0x00000014    /*  34 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[35]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2d3ceba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* macroexpand */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[35]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1792[0])) /* #<compiled-code macroexpand@0x7f972b75e9c0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#macroexpand.2bc6b6c0> */,
    0x00000014    /*  14 (RET) */,
    /* macroexpand-1 */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[50]) + 6),
    0x00000004    /*   3 (CONSTF) */,
    0x00000013    /*   4 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[50]) + 7),
    0x0000006a    /*   6 (LREF0-CAR) */,
    0x0000000d    /*   7 (PUSH) */,
    0x0000003d    /*   8 (LREF0) */,
    0x00000022    /*   9 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[50]) + 14),
    0x00000003    /*  11 (CONSTN) */,
    0x00000013    /*  12 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[50]) + 15),
    0x00000076    /*  14 (LREF0-CDR) */,
    0x00002018    /*  15 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  16 (LREF0) */,
    0x00000022    /*  17 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[50]) + 21),
    0x00000013    /*  19 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[50]) + 29),
    0x0000200e    /*  21 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[50]) + 29),
    0x00000006    /*  23 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1789[2])) /* "too many arguments for" */,
    0x00000006    /*  25 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[22])) /* (lambda (form :optional (env/flag #f)) (%do-macroexpand form env/flag #t)) */,
    0x0000205f    /*  27 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.2d3ce860> */,
    0x0000004d    /*  29 (LREF11-PUSH) */,
    0x00000049    /*  30 (LREF1-PUSH) */,
    0x00000006    /*  31 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x00003060    /*  33 (GREF-TAIL-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%do-macroexpand.2bf40a00> */,
    0x00000014    /*  35 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[86]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2d3ceba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* macroexpand-1 */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[86]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1792[2])) /* #<compiled-code macroexpand-1@0x7f972bd28900> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#macroexpand-1.2bf374a0> */,
    0x00000014    /*  14 (RET) */,
    /* %do-macroexpand */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]) + 5),
    0x00000049    /*   2 (LREF1-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#boolean?.2b87faa0> */,
    0x0000001e    /*   5 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]) + 13),
    0x0000000e    /*   7 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]) + 30),
    0x0000005f    /*   9 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#vm-current-module.2b87fa40> */,
    0x00000013    /*  11 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]) + 30),
    0x0000100e    /*  13 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]) + 18),
    0x00000049    /*  15 (LREF1-PUSH) */,
    0x0000105f    /*  16 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#module?.2b87f9e0> */,
    0x0000001e    /*  18 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]) + 23),
    0x0000003e    /*  20 (LREF1) */,
    0x00000013    /*  21 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]) + 30),
    0x0000200e    /*  23 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]) + 30),
    0x00000006    /*  25 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1789[17])) /* "argument must be a boolean or a module, but got:" */,
    0x00000049    /*  27 (LREF1-PUSH) */,
    0x0000205f    /*  28 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.2b87f8e0> */,
    0x00001018    /*  30 (PUSH-LOCAL-ENV 1) */,
    0x0000300e    /*  31 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]) + 43),
    0x0000004e    /*  33 (LREF12-PUSH) */,
    0x0000100e    /*  34 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]) + 39),
    0x00000048    /*  36 (LREF0-PUSH) */,
    0x0000105f    /*  37 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-cenv.2b87f7e0> */,
    0x0000000d    /*  39 (PUSH) */,
    0x0000004c    /*  40 (LREF10-PUSH) */,
    0x0000305f    /*  41 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%internal-macro-expand.2b87f840> */,
    0x00001018    /*  43 (PUSH-LOCAL-ENV 1) */,
    0x00000045    /*  44 (LREF21) */,
    0x0000002e    /*  45 (BNEQC) */,
    SCM_WORD(SCM_FALSE) /* #f */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]) + 52)  /*     52 */,
    0x00000048    /*  48 (LREF0-PUSH) */,
    0x00001060    /*  49 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#unravel-syntax.2b87f680> */,
    0x00000014    /*  51 (RET) */,
    0x00000053    /*  52 (LREF0-RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[154]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2d3ceba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* %do-macroexpand */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[154]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1792[4])) /* #<compiled-code %do-macroexpand@0x7f972bdba540> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%do-macroexpand.2b87fca0> */,
    0x00000014    /*  14 (RET) */,
    /* (unravel-syntax rec) */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[169]) + 5),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#wrapped-identifier?.2bae90e0> */,
    0x0000001e    /*   5 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[169]) + 11),
    0x00000048    /*   7 (LREF0-PUSH) */,
    0x00000042    /*   8 (LREF11) */,
    0x00001012    /*   9 (TAIL-CALL 1) */,
    0x00000014    /*  10 (RET) */,
    0x0000003d    /*  11 (LREF0) */,
    0x00000098    /*  12 (PAIRP) */,
    0x0000001e    /*  13 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[169]) + 29),
    0x0000100e    /*  15 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[169]) + 21),
    0x0000006a    /*  17 (LREF0-CAR) */,
    0x0000000d    /*  18 (PUSH) */,
    0x00000041    /*  19 (LREF10) */,
    0x0000101c    /*  20 (LOCAL-ENV-CALL 1) */,
    0x0000100f    /*  21 (PUSH-PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[169]) + 27),
    0x00000076    /*  23 (LREF0-CDR) */,
    0x0000000d    /*  24 (PUSH) */,
    0x00000041    /*  25 (LREF10) */,
    0x0000101c    /*  26 (LOCAL-ENV-CALL 1) */,
    0x00000066    /*  27 (CONS) */,
    0x00000014    /*  28 (RET) */,
    0x0000003d    /*  29 (LREF0) */,
    0x0000009d    /*  30 (VECTORP) */,
    0x0000001e    /*  31 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[169]) + 38),
    0x0000004c    /*  33 (LREF10-PUSH) */,
    0x00000048    /*  34 (LREF0-PUSH) */,
    0x00002060    /*  35 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#vector-map.2baecea0> */,
    0x00000014    /*  37 (RET) */,
    0x00000053    /*  38 (LREF0-RET) */,
    /* (unravel-syntax id->sym #f) */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[208]) + 5),
    0x0000004a    /*   2 (LREF2-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#unwrap-syntax.2bae9f20> */,
    0x0000000d    /*   5 (PUSH) */,
    0x00000041    /*   6 (LREF10) */,
    0x00000020    /*   7 (BNEQ) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[208]) + 11),
    0x000010bd    /*   9 (LREF0-NUMADDI 1) */,
    0x00000014    /*  10 (RET) */,
    0x00000053    /*  11 (LREF0-RET) */,
    /* (unravel-syntax id->sym) */
    0x0000300e    /*   0 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]) + 7),
    0x0000004c    /*   2 (LREF10-PUSH) */,
    0x00000048    /*   3 (LREF0-PUSH) */,
    0x00000009    /*   4 (CONSTF-PUSH) */,
    0x0000305f    /*   5 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#hash-table-get.2bae2620> */,
    0x00000031    /*   7 (RT) */,
    0x0000100e    /*   8 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]) + 13),
    0x00000048    /*  10 (LREF0-PUSH) */,
    0x0000105f    /*  11 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#unwrap-syntax.2bae3360> */,
    0x00001018    /*  13 (PUSH-LOCAL-ENV 1) */,
    0x0000200e    /*  14 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]) + 30),
    0x0000004c    /*  16 (LREF10-PUSH) */,
    0x0000300e    /*  17 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]) + 28),
    0x00000048    /*  19 (LREF0-PUSH) */,
    0x0000000e    /*  20 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]) + 24),
    0x0000005f    /*  22 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#vm-current-module.2bae31c0> */,
    0x0000000d    /*  24 (PUSH) */,
    0x00000008    /*  25 (CONSTN-PUSH) */,
    0x0000305f    /*  26 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-identifier.2bae3220> */,
    0x00002062    /*  28 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#global-identifier=?.2bae3280> */,
    0x0000001e    /*  30 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]) + 40),
    0x0000300e    /*  32 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]) + 39),
    0x0000004f    /*  34 (LREF20-PUSH) */,
    0x0000004c    /*  35 (LREF10-PUSH) */,
    0x00000048    /*  36 (LREF0-PUSH) */,
    0x0000305f    /*  37 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#hash-table-put!.2bae30e0> */,
    0x00000053    /*  39 (LREF0-RET) */,
    0x0000300e    /*  40 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]) + 49),
    0x0000004f    /*  42 (LREF20-PUSH) */,
    0x00000016    /*  43 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1792[7])) /* #<compiled-code (unravel-syntax id->sym #f)@0x7f972abae7e0> */,
    0x0000000d    /*  45 (PUSH) */,
    0x00000007    /*  46 (CONSTI-PUSH 0) */,
    0x0000305f    /*  47 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#hash-table-fold.2bae6340> */,
    0x00001018    /*  49 (PUSH-LOCAL-ENV 1) */,
    0x0000300e    /*  50 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]) + 58),
    0x0000004c    /*  52 (LREF10-PUSH) */,
    0x00000006    /*  53 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1789[46])) /* "." */,
    0x00000048    /*  55 (LREF0-PUSH) */,
    0x0000305f    /*  56 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#symbol-append.2bae93a0> */,
    0x00001018    /*  58 (PUSH-LOCAL-ENV 1) */,
    0x0000300e    /*  59 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]) + 66),
    0x00004047    /*  61 (LREF-PUSH 4 0) */,
    0x00000051    /*  62 (LREF30-PUSH) */,
    0x00000048    /*  63 (LREF0-PUSH) */,
    0x0000305f    /*  64 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#hash-table-put!.2bae92c0> */,
    0x00000053    /*  66 (LREF0-RET) */,
    /* unravel-syntax */
    0x00002019    /*   0 (LOCAL-ENV-CLOSURES 2) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[62])) /* (#<undef> #<compiled-code (unravel-syntax rec)@0x7f972abae8a0>) */,
    0x0000100e    /*   2 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[287]) + 8),
    0x00000006    /*   4 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* eq? */,
    0x0000105f    /*   6 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-hash-table.2bae0a80> */,
    0x00001018    /*   8 (PUSH-LOCAL-ENV 1) */,
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1792[8])) /* #<compiled-code (unravel-syntax id->sym)@0x7f972abae840> */,
    0x0000001a    /*  11 (POP-LOCAL-ENV) */,
    0x000010e8    /*  12 (ENV-SET 1) */,
    0x0000004c    /*  13 (LREF10-PUSH) */,
    0x0000003d    /*  14 (LREF0) */,
    0x0000101d    /*  15 (LOCAL-ENV-TAIL-CALL 1) */,
    0x00000014    /*  16 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[304]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2d3ceba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* unravel-syntax */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[304]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1792[9])) /* #<compiled-code unravel-syntax@0x7f972abae900> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#unravel-syntax.2badea40> */,
    0x00000014    /*  14 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[319]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2d3ceba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* *trace-macro* */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[319]) + 10),
    0x0000000b    /*   9 (CONSTF-RET) */,
    0x00000015    /*  10 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#*trace-macro*.2afd7b20> */,
    0x00000014    /*  12 (RET) */,
    /* (%macro-traced? #f) */
    0x0000003d    /*   0 (LREF0) */,
    0x0000009c    /*   1 (SYMBOLP) */,
    0x0000001e    /*   2 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[332]) + 8),
    0x00000048    /*   4 (LREF0-PUSH) */,
    0x00000041    /*   5 (LREF10) */,
    0x0000008f    /*   6 (EQ) */,
    0x00000014    /*   7 (RET) */,
    0x0000100e    /*   8 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[332]) + 13),
    0x00000048    /*  10 (LREF0-PUSH) */,
    0x0000105f    /*  11 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#regexp?.2b00fea0> */,
    0x0000001e    /*  13 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[332]) + 24),
    0x0000100e    /*  15 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[332]) + 20),
    0x0000004c    /*  17 (LREF10-PUSH) */,
    0x0000105f    /*  18 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#symbol->string.2b00fe20> */,
    0x0000000d    /*  20 (PUSH) */,
    0x0000003d    /*  21 (LREF0) */,
    0x00001012    /*  22 (TAIL-CALL 1) */,
    0x00000014    /*  23 (RET) */,
    0x0000000b    /*  24 (CONSTF-RET) */,
    /* %macro-traced? */
    0x0000005d    /*   0 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#*trace-macro*.2b00cb80> */,
    0x0000002e    /*   2 (BNEQC) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[357]) + 6)  /*      6 */,
    0x00000014    /*   5 (RET) */,
    0x0000100e    /*   6 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[357]) + 15),
    0x0000100e    /*   8 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[357]) + 13),
    0x00000048    /*  10 (LREF0-PUSH) */,
    0x0000105f    /*  11 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#macro-name.2b00c4e0> */,
    0x00001062    /*  13 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#unwrap-syntax.2b00c520> */,
    0x00001018    /*  15 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  16 (LREF0) */,
    0x00000030    /*  17 (RF) */,
    0x00000016    /*  18 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1792[12])) /* #<compiled-code (%macro-traced? #f)@0x7f972b00aae0> */,
    0x00000061    /*  20 (PUSH-GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#*trace-macro*.2b00fd60> */,
    0x00002063    /*  22 (PUSH-GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#find.2b00c420> */,
    0x00000014    /*  24 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[382]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2d3ceba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* %macro-traced? */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[382]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1792[13])) /* #<compiled-code %macro-traced?@0x7f972b00ab40> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%macro-traced?.2b00cce0> */,
    0x00000014    /*  14 (RET) */,
    /* call-macro-expander */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 10),
    0x00000049    /*   2 (LREF1-PUSH) */,
    0x00000048    /*   3 (LREF0-PUSH) */,
    0x0000100e    /*   4 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 9),
    0x0000004a    /*   6 (LREF2-PUSH) */,
    0x0000105f    /*   7 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#macro-transformer.2b171460> */,
    0x00002011    /*   9 (CALL 2) */,
    0x00001018    /*  10 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  11 (LREF0) */,
    0x00000098    /*  12 (PAIRP) */,
    0x0000001e    /*  13 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 52),
    0x0000004d    /*  15 (LREF11-PUSH) */,
    0x0000003d    /*  16 (LREF0) */,
    0x00000020    /*  17 (BNEQ) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 22),
    0x0000003d    /*  19 (LREF0) */,
    0x00000013    /*  20 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 53),
    0x0000100e    /*  22 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 27),
    0x00000048    /*  24 (LREF0-PUSH) */,
    0x0000105f    /*  25 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#extended-pair?.2b178e60> */,
    0x0000001e    /*  27 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 32),
    0x0000003d    /*  29 (LREF0) */,
    0x00000013    /*  30 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 39),
    0x0000200e    /*  32 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 39),
    0x0000006a    /*  34 (LREF0-CAR) */,
    0x0000000d    /*  35 (PUSH) */,
    0x00000076    /*  36 (LREF0-CDR) */,
    0x00002062    /*  37 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#extended-cons.2b178de0> */,
    0x00001018    /*  39 (PUSH-LOCAL-ENV 1) */,
    0x0000300e    /*  40 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 48),
    0x00000048    /*  42 (LREF0-PUSH) */,
    0x00000006    /*  43 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* original */,
    0x00000050    /*  45 (LREF21-PUSH) */,
    0x0000305f    /*  46 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#pair-attribute-set!.2b178ce0> */,
    0x0000003d    /*  48 (LREF0) */,
    0x0000001a    /*  49 (POP-LOCAL-ENV) */,
    0x00000013    /*  50 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 53),
    0x0000003d    /*  52 (LREF0) */,
    0x00001018    /*  53 (PUSH-LOCAL-ENV 1) */,
    0x0000005d    /*  54 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#*trace-macro*.2b178ae0> */,
    0x0000001e    /*  56 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 154),
    0x0000100e    /*  58 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 63),
    0x00802047    /*  60 (LREF-PUSH 2 2) */,
    0x0000105f    /*  61 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%macro-traced?.2b178aa0> */,
    0x0000001e    /*  63 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 154),
    0x0000100e    /*  65 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 72),
    0x00000050    /*  67 (LREF21-PUSH) */,
    0x0000003d    /*  68 (LREF0) */,
    0x00000067    /*  69 (CONS-PUSH) */,
    0x0000105f    /*  70 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#unravel-syntax.2b1782a0> */,
    0x00001018    /*  72 (PUSH-LOCAL-ENV 1) */,
    0x0000200e    /*  73 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 83),
    0x00000006    /*  75 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1789[76])) /* "Macro input>>>\n" */,
    0x0000000e    /*  77 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 81),
    0x0000005f    /*  79 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-trace-port.2b178160> */,
    0x00002062    /*  81 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#display.2b1781c0> */,
    0x0000700e    /*  83 (PRE-CALL 7) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 102),
    0x0000006a    /*  85 (LREF0-CAR) */,
    0x0000000d    /*  86 (PUSH) */,
    0x00000006    /*  87 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :port */,
    0x0000000e    /*  89 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 93),
    0x0000005f    /*  91 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-trace-port.2b178060> */,
    0x0000000d    /*  93 (PUSH) */,
    0x00000006    /*  94 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :level */,
    0x00000009    /*  96 (CONSTF-PUSH) */,
    0x00000006    /*  97 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :length */,
    0x00000009    /*  99 (CONSTF-PUSH) */,
    0x0000705f    /* 100 (GREF-CALL 7) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#pprint.2b178120> */,
    0x0000200e    /* 102 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 112),
    0x00000006    /* 104 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1789[83])) /* "\nMacro output<<<\n" */,
    0x0000000e    /* 106 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 110),
    0x0000005f    /* 108 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-trace-port.2b17af00> */,
    0x00002062    /* 110 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#display.2b17af60> */,
    0x0000700e    /* 112 (PRE-CALL 7) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 131),
    0x00000076    /* 114 (LREF0-CDR) */,
    0x0000000d    /* 115 (PUSH) */,
    0x00000006    /* 116 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :port */,
    0x0000000e    /* 118 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 122),
    0x0000005f    /* 120 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-trace-port.2b17ae00> */,
    0x0000000d    /* 122 (PUSH) */,
    0x00000006    /* 123 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :level */,
    0x00000009    /* 125 (CONSTF-PUSH) */,
    0x00000006    /* 126 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* :length */,
    0x00000009    /* 128 (CONSTF-PUSH) */,
    0x0000705f    /* 129 (GREF-CALL 7) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#pprint.2b17aec0> */,
    0x0000200e    /* 131 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 141),
    0x00000006    /* 133 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1789[84])) /* "\n" */,
    0x0000000e    /* 135 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 139),
    0x0000005f    /* 137 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-trace-port.2b17aca0> */,
    0x00002062    /* 139 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#display.2b17ad00> */,
    0x0000100e    /* 141 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 149),
    0x0000000e    /* 143 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 147),
    0x0000005f    /* 145 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#current-trace-port.2b17ac20> */,
    0x00001062    /* 147 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#flush.2b17ac60> */,
    0x0000001a    /* 149 (POP-LOCAL-ENV) */,
    0x00000013    /* 150 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 154),
    0x00000013    /* 152 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]) + 154),
    0x00000053    /* 154 (LREF0-RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[552]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2d3ceba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* call-macro-expander */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[552]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1792[15])) /* #<compiled-code call-macro-expander@0x7f972b176ea0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#call-macro-expander.2b1715a0> */,
    0x00000014    /*  14 (RET) */,
    /* call-id-macro-expander */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[567]) + 5),
    0x00000049    /*   2 (LREF1-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#identifier-macro?.2c8c4800> */,
    0x0000001e    /*   5 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[567]) + 9),
    0x00000013    /*   7 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[567]) + 16),
    0x0000200e    /*   9 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[567]) + 16),
    0x00000006    /*  11 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1789[89])) /* "Non-identifier-macro can't appear in this context:" */,
    0x00000049    /*  13 (LREF1-PUSH) */,
    0x0000205f    /*  14 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.2c8c4760> */,
    0x00000049    /*  16 (LREF1-PUSH) */,
    0x00000049    /*  17 (LREF1-PUSH) */,
    0x00000048    /*  18 (LREF0-PUSH) */,
    0x00003060    /*  19 (GREF-TAIL-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#call-macro-expander.2c8c46c0> */,
    0x00000014    /*  21 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[589]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2d3ceba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* call-id-macro-expander */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[589]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1792[17])) /* #<compiled-code call-id-macro-expander@0x7f972b2ff2a0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#call-id-macro-expander.2c8c02e0> */,
    0x00000014    /*  14 (RET) */,
    /* (trace-macro G1805) */
    0x0000003d    /*   0 (LREF0) */,
    0x0000009c    /*   1 (SYMBOLP) */,
    0x00000031    /*   2 (RT) */,
    0x00000048    /*   3 (LREF0-PUSH) */,
    0x00001060    /*   4 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#regexp?.2c006a20> */,
    0x00000014    /*   6 (RET) */,
    /* (trace-macro G1803) */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]) + 6),
    0x0000004f    /*   2 (LREF20-PUSH) */,
    0x00000048    /*   3 (LREF0-PUSH) */,
    0x0000205f    /*   4 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#every.2c006e60> */,
    0x0000001e    /*   6 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]) + 10),
    0x00000013    /*   8 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]) + 17),
    0x0000200e    /*  10 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]) + 17),
    0x00000006    /*  12 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1789[104])) /* "Argument(s) of trace-macro should be a boolean, or symbols or regexps, but got:" */,
    0x0000004c    /*  14 (LREF10-PUSH) */,
    0x0000205f    /*  15 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.2c006960> */,
    0x0000005d    /*  17 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#*trace-macro*.2c007be0> */,
    0x0000001e    /*  19 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]) + 36),
    0x0000100e    /*  21 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]) + 27),
    0x0000005e    /*  23 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#*trace-macro*.2c0072a0> */,
    0x0000105f    /*  25 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#list?.2c007300> */,
    0x0000001e    /*  27 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]) + 33),
    0x0000005d    /*  29 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#*trace-macro*.2c007260> */,
    0x00000013    /*  31 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]) + 37),
    0x00000004    /*  33 (CONSTF) */,
    0x00000013    /*  34 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]) + 37),
    0x00000003    /*  36 (CONSTN) */,
    0x00001018    /*  37 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  38 (LREF0) */,
    0x00000030    /*  39 (RF) */,
    0x0000100e    /*  40 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]) + 47),
    0x0000004c    /*  42 (LREF10-PUSH) */,
    0x0000003d    /*  43 (LREF0) */,
    0x00002091    /*  44 (APPEND 2) */,
    0x00001062    /*  45 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#delete-duplicates.2c00ab60> */,
    0x0000003b    /*  47 (GSET) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#*trace-macro*.2c00aba0> */,
    0x00000014    /*  49 (RET) */,
    /* trace-macro */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 6),
    0x0000005e    /*   2 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#pprint.2c003ce0> */,
    0x0000105f    /*   4 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#procedure?.2c003d20> */,
    0x00000016    /*   6 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1792[20])) /* #<compiled-code (trace-macro #:G1803)@0x7f972b8f1000> */,
    0x00001018    /*   8 (PUSH-LOCAL-ENV 1) */,
    0x00000041    /*   9 (LREF10) */,
    0x00000022    /*  10 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 14),
    0x00000013    /*  12 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 119),
    0x00000041    /*  14 (LREF10) */,
    0x00000098    /*  15 (PAIRP) */,
    0x0000001e    /*  16 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 100),
    0x0000200e    /*  18 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 26),
    0x0000006e    /*  20 (LREF10-CAR) */,
    0x0000000d    /*  21 (PUSH) */,
    0x00000006    /*  22 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x0000205f    /*  24 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#equal?.2c00a540> */,
    0x0000001e    /*  26 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 58),
    0x0000007a    /*  28 (LREF10-CDR) */,
    0x00000022    /*  29 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 37),
    0x00000001    /*  31 (CONST) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x0000003b    /*  33 (GSET) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#*trace-macro*.2c00a920> */,
    0x00000013    /*  35 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 119),
    0x0000100e    /*  37 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 42),
    0x0000004c    /*  39 (LREF10-PUSH) */,
    0x0000105f    /*  40 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#list?.2c00cae0> */,
    0x0000001e    /*  42 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 51),
    0x0000100e    /*  44 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 119),
    0x0000004c    /*  46 (LREF10-PUSH) */,
    0x0000003d    /*  47 (LREF0) */,
    0x0000101c    /*  48 (LOCAL-ENV-CALL 1) */,
    0x00000013    /*  49 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 119),
    0x0000100e    /*  51 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 119),
    0x0000004c    /*  53 (LREF10-PUSH) */,
    0x0000105f    /*  54 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier util.match#match:error.2ce8f200> */,
    0x00000013    /*  56 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 119),
    0x0000200e    /*  58 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 65),
    0x0000006e    /*  60 (LREF10-CAR) */,
    0x0000000d    /*  61 (PUSH) */,
    0x00000009    /*  62 (CONSTF-PUSH) */,
    0x0000205f    /*  63 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#equal?.2c00c980> */,
    0x0000001e    /*  65 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 75),
    0x0000007a    /*  67 (LREF10-CDR) */,
    0x00000022    /*  68 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 75),
    0x00000004    /*  70 (CONSTF) */,
    0x0000003b    /*  71 (GSET) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#*trace-macro*.2c00aa00> */,
    0x00000013    /*  73 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 119),
    0x0000100e    /*  75 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 80),
    0x0000004c    /*  77 (LREF10-PUSH) */,
    0x0000105f    /*  78 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#list?.2c00c820> */,
    0x0000001e    /*  80 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 89),
    0x0000100e    /*  82 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 119),
    0x0000004c    /*  84 (LREF10-PUSH) */,
    0x0000003d    /*  85 (LREF0) */,
    0x0000101c    /*  86 (LOCAL-ENV-CALL 1) */,
    0x00000013    /*  87 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 119),
    0x0000100e    /*  89 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 119),
    0x0000004c    /*  91 (LREF10-PUSH) */,
    0x0000105f    /*  92 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier util.match#match:error.2ce8f200> */,
    0x00000013    /*  94 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 119),
    0x00000013    /*  96 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 75),
    0x00000013    /*  98 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 119),
    0x0000100e    /* 100 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 105),
    0x0000004c    /* 102 (LREF10-PUSH) */,
    0x0000105f    /* 103 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#list?.2c00c5e0> */,
    0x0000001e    /* 105 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 114),
    0x0000100e    /* 107 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 119),
    0x0000004c    /* 109 (LREF10-PUSH) */,
    0x0000003d    /* 110 (LREF0) */,
    0x0000101c    /* 111 (LOCAL-ENV-CALL 1) */,
    0x00000013    /* 112 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 119),
    0x0000100e    /* 114 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]) + 119),
    0x0000004c    /* 116 (LREF10-PUSH) */,
    0x0000105f    /* 117 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier util.match#match:error.2ce8f200> */,
    0x0000001a    /* 119 (POP-LOCAL-ENV) */,
    0x0000005d    /* 120 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#*trace-macro*.2c00c500> */,
    0x00000014    /* 122 (RET) */,
    /* %toplevel */
    0x00001019    /*   0 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[209])) /* (#<compiled-code (trace-macro #:G1805)@0x7f972b8f1180>) */,
    0x0000000e    /*   2 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[784]) + 6),
    0x0000005f    /*   4 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2d3ceba0> */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000001    /*   7 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* trace-macro */,
    0x000000ee    /*   9 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[784]) + 14),
    0x00000016    /*  11 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1792[21])) /* #<compiled-code trace-macro@0x7f972b8f1060> */,
    0x00000014    /*  13 (RET) */,
    0x00000015    /*  14 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#trace-macro.2c003e00> */,
    0x00000014    /*  16 (RET) */,
    /* (untrace-macro #f) */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x0000004f    /*   1 (LREF20-PUSH) */,
    0x00000041    /*   2 (LREF10) */,
    0x00002012    /*   3 (TAIL-CALL 2) */,
    0x00000014    /*   4 (RET) */,
    /* untrace-macro */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]) + 8),
    0x00000004    /*   3 (CONSTF) */,
    0x0000003b    /*   4 (GSET) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#*trace-macro*.2be9e5a0> */,
    0x00000013    /*   6 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]) + 57),
    0x0000100e    /*   8 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]) + 13),
    0x00000048    /*  10 (LREF0-PUSH) */,
    0x0000105f    /*  11 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#list?.2be9e480> */,
    0x0000001e    /*  13 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]) + 52),
    0x0000100e    /*  15 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]) + 21),
    0x0000005e    /*  17 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#*trace-macro*.2be966c0> */,
    0x0000105f    /*  19 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#list?.2be96800> */,
    0x0000001e    /*  21 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]) + 57),
    0x0000200e    /*  23 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]) + 37),
    0x00001019    /*  25 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1790[220])) /* (#<undef>) */,
    0x0000005d    /*  27 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#member.2be9e8e0> */,
    0x000000e8    /*  29 (ENV-SET 0) */,
    0x00000016    /*  30 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1792[23])) /* #<compiled-code (untrace-macro #f)@0x7f972b518600> */,
    0x0000001a    /*  32 (POP-LOCAL-ENV) */,
    0x00000061    /*  33 (PUSH-GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#*trace-macro*.2be9e780> */,
    0x00002062    /*  35 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#remove.2be96000> */,
    0x00001018    /*  37 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  38 (LREF0) */,
    0x00000022    /*  39 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]) + 44),
    0x00000004    /*  41 (CONSTF) */,
    0x00000013    /*  42 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]) + 45),
    0x0000003d    /*  44 (LREF0) */,
    0x0000003b    /*  45 (GSET) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#*trace-macro*.2be9e6e0> */,
    0x0000001a    /*  47 (POP-LOCAL-ENV) */,
    0x00000013    /*  48 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]) + 57),
    0x00000013    /*  50 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]) + 57),
    0x0000100e    /*  52 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]) + 57),
    0x00000048    /*  54 (LREF0-PUSH) */,
    0x0000105f    /*  55 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier util.match#match:error.2ce8f200> */,
    0x0000005d    /*  57 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#*trace-macro*.2be9e3a0> */,
    0x00000014    /*  59 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[866]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2d3ceba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* untrace-macro */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1791[866]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1792[24])) /* #<compiled-code untrace-macro@0x7f972b518720> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#untrace-macro.2be96ea0> */,
    0x00000014    /*  14 (RET) */,
  },
  {   /* ScmPair d1790 */
       { SCM_NIL, SCM_NIL },
       { SCM_FALSE, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[1])},
       { SCM_OBJ(&scm__rc.d1790[2]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[3])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[4])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[2])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[6])},
       { SCM_OBJ(&scm__rc.d1790[7]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[5]), SCM_OBJ(&scm__rc.d1790[8])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[9])},
       { SCM_MAKE_INT(55U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[11])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[12])},
       { SCM_OBJ(&scm__rc.d1790[13]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_TRUE, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[16])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[17])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[18])},
       { SCM_OBJ(&scm__rc.d1790[19]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[5]), SCM_OBJ(&scm__rc.d1790[20])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[21])},
       { SCM_MAKE_INT(58U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[23])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[24])},
       { SCM_OBJ(&scm__rc.d1790[25]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[28])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[29])},
       { SCM_MAKE_INT(61U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[31])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[32])},
       { SCM_OBJ(&scm__rc.d1790[33]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[36])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[37])},
       { SCM_MAKE_INT(75U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[39])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[40])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[42])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[43])},
       { SCM_OBJ(&scm__rc.d1790[44]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[41]), SCM_OBJ(&scm__rc.d1790[45])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(80U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[48])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[49])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[51])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[52])},
       { SCM_OBJ(&scm__rc.d1790[53]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[50]), SCM_OBJ(&scm__rc.d1790[54])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[56])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[57]), SCM_OBJ(&scm__rc.d1790[58])},
       { SCM_OBJ(&scm__rc.d1790[59]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1792[6]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[61])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[1])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[63])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[65])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[66])},
       { SCM_MAKE_INT(100U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[68])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[69])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[71])},
       { SCM_OBJ(&scm__rc.d1790[72]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[70]), SCM_OBJ(&scm__rc.d1790[73])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[76])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(92U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[79])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[80])},
       { SCM_OBJ(&scm__rc.d1790[81]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(89U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[84])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[85])},
       { SCM_OBJ(&scm__rc.d1790[86]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(119U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[89])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[90])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[92])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[93])},
       { SCM_OBJ(&scm__rc.d1790[94]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[91]), SCM_OBJ(&scm__rc.d1790[95])},
       { SCM_MAKE_INT(120U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[97])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[98])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[100])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[101])},
       { SCM_OBJ(&scm__rc.d1790[102]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[99]), SCM_OBJ(&scm__rc.d1790[103])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[105])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[106])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[107])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[108])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[109])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[110])},
       { SCM_MAKE_INT(145U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[112])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[113])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[115])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[116])},
       { SCM_OBJ(&scm__rc.d1790[117]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[114]), SCM_OBJ(&scm__rc.d1790[118])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(148U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[121])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[122])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[124])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[125])},
       { SCM_OBJ(&scm__rc.d1790[126]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[123]), SCM_OBJ(&scm__rc.d1790[127])},
       { SCM_MAKE_INT(149U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[129])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[130])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[132])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[133])},
       { SCM_OBJ(&scm__rc.d1790[134]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[131]), SCM_OBJ(&scm__rc.d1790[135])},
       { SCM_MAKE_INT(150U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[137])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[138])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[140])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[141])},
       { SCM_OBJ(&scm__rc.d1790[142]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[139]), SCM_OBJ(&scm__rc.d1790[143])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[1])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(161U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[147])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[148])},
       { SCM_OBJ(&scm__rc.d1790[149]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(158U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[152])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[153])},
       { SCM_OBJ(&scm__rc.d1790[154]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[157])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[158])},
       { SCM_MAKE_INT(166U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[160])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[161])},
       { SCM_OBJ(&scm__rc.d1790[162]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[157])},
       { SCM_MAKE_INT(186U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[166])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[167])},
       { SCM_OBJ(&scm__rc.d1790[168]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[171])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[172])},
       { SCM_MAKE_INT(191U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[174])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[175])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[177])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[178])},
       { SCM_OBJ(&scm__rc.d1790[179]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[176]), SCM_OBJ(&scm__rc.d1790[180])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[157])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[182])},
       { SCM_MAKE_INT(194U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[184])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[185])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[187])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[188])},
       { SCM_OBJ(&scm__rc.d1790[189]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[186]), SCM_OBJ(&scm__rc.d1790[190])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(198U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[193])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[194])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[196])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[197])},
       { SCM_OBJ(&scm__rc.d1790[198]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[195]), SCM_OBJ(&scm__rc.d1790[199])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[201])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(211U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[204])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[205])},
       { SCM_OBJ(&scm__rc.d1790[206]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1792[19]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[210])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[211]), SCM_OBJ(&scm__rc.d1790[212])},
       { SCM_OBJ(&scm__rc.d1790[213]), SCM_NIL},
       { SCM_MAKE_INT(202U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[215])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[216])},
       { SCM_OBJ(&scm__rc.d1790[217]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[1])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1790[221]), SCM_OBJ(&scm__rc.d1790[222])},
       { SCM_OBJ(&scm__rc.d1790[223]), SCM_NIL},
       { SCM_MAKE_INT(221U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1789[11]), SCM_OBJ(&scm__rc.d1790[225])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1790[226])},
       { SCM_OBJ(&scm__rc.d1790[227]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
  },
  {   /* ScmObj d1788 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(11, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(119, FALSE),
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
  },
};

static ScmObj libmacbaseunwrap_syntax(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj form_scm;
  ScmObj form;
  ScmObj to_immutable_scm;
  int to_immutable;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("unwrap-syntax");
  if (SCM_ARGCNT >= 3
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 2 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  form_scm = SCM_SUBRARGS[0];
  if (!(form_scm)) Scm_Error("scheme object required, but got %S", form_scm);
  form = (form_scm);
  if (SCM_ARGCNT > 1+1) {
    to_immutable_scm = SCM_SUBRARGS[1];
  } else {
    to_immutable_scm = SCM_FALSE;
  }
  if (!SCM_BOOLP(to_immutable_scm)) Scm_Error("boolean required, but got %S", to_immutable_scm);
  to_immutable = SCM_BOOL_VALUE(to_immutable_scm);
  {
{
ScmObj SCM_RESULT;

#line 76 "libmacbase.scm"
{SCM_RESULT=(Scm_UnwrapSyntax2(form,to_immutable));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libmacbaseunwrap_syntax_1(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("unwrap-syntax-1");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {
{
ScmObj SCM_RESULT;

#line 81 "libmacbase.scm"
if (SCM_IDENTIFIERP(obj)){
{SCM_RESULT=(SCM_OBJ(Scm_UnwrapIdentifier(SCM_IDENTIFIER(obj))));goto SCM_STUB_RETURN;}} else {
{SCM_RESULT=(obj);goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libmacbasemacroP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("macro?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(SCM_MACROP(obj));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libmacbasesyntaxP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("syntax?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(SCM_SYNTAXP(obj));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}

static void Scm_MacroClass_PRINT(ScmObj obj, ScmPort *port, ScmWriteContext *ctx SCM_UNUSED)
{

#line 135 "libmacbase.scm"
Scm_Printf(port,"#<macro %A>",(SCM_MACRO(obj))->name);
}

SCM_DEFINE_BUILTIN_CLASS(Scm_MacroClass, Scm_MacroClass_PRINT, NULL, NULL, NULL, SCM_CLASS_DEFAULT_CPL);

static ScmObj Scm_MacroClass_name_GET(ScmObj OBJARG)
{
  ScmMacro* obj = SCM_MACRO(OBJARG);
  return SCM_OBJ_SAFE(obj->name);
}

static ScmObj Scm_MacroClass_transformer_GET(ScmObj OBJARG)
{
  ScmMacro* obj = SCM_MACRO(OBJARG);
  return SCM_OBJ_SAFE(obj->transformer);
}

static ScmObj Scm_MacroClass_info_alist_GET(ScmObj OBJARG)
{
  ScmMacro* obj = SCM_MACRO(OBJARG);
  return SCM_OBJ_SAFE(obj->info_alist);
}

static ScmObj Scm_MacroClass_flags_GET(ScmObj OBJARG)
{
  ScmMacro* obj = SCM_MACRO(OBJARG);
  return Scm_MakeIntegerU(obj->flags);
}

static ScmClassStaticSlotSpec Scm_MacroClass__SLOTS[] = {
  SCM_CLASS_SLOT_SPEC("name", Scm_MacroClass_name_GET, NULL),
  SCM_CLASS_SLOT_SPEC("transformer", Scm_MacroClass_transformer_GET, NULL),
  SCM_CLASS_SLOT_SPEC("info-alist", Scm_MacroClass_info_alist_GET, NULL),
  SCM_CLASS_SLOT_SPEC("flags", Scm_MacroClass_flags_GET, NULL),
  SCM_CLASS_SLOT_SPEC_END()
};


static ScmObj libmacbasecompile_syntax_rules(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj name_scm;
  ScmObj name;
  ScmObj src_scm;
  ScmObj src;
  ScmObj ellipsis_scm;
  ScmObj ellipsis;
  ScmObj literals_scm;
  ScmObj literals;
  ScmObj rules_scm;
  ScmObj rules;
  ScmObj mod_scm;
  ScmObj mod;
  ScmObj env_scm;
  ScmObj env;
  ScmObj SCM_SUBRARGS[7];
  SCM_ENTER_SUBR("compile-syntax-rules");
  for (int SCM_i=0; SCM_i<7; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  name_scm = SCM_SUBRARGS[0];
  if (!(name_scm)) Scm_Error("scheme object required, but got %S", name_scm);
  name = (name_scm);
  src_scm = SCM_SUBRARGS[1];
  if (!(src_scm)) Scm_Error("scheme object required, but got %S", src_scm);
  src = (src_scm);
  ellipsis_scm = SCM_SUBRARGS[2];
  if (!(ellipsis_scm)) Scm_Error("scheme object required, but got %S", ellipsis_scm);
  ellipsis = (ellipsis_scm);
  literals_scm = SCM_SUBRARGS[3];
  if (!(literals_scm)) Scm_Error("scheme object required, but got %S", literals_scm);
  literals = (literals_scm);
  rules_scm = SCM_SUBRARGS[4];
  if (!(rules_scm)) Scm_Error("scheme object required, but got %S", rules_scm);
  rules = (rules_scm);
  mod_scm = SCM_SUBRARGS[5];
  if (!(mod_scm)) Scm_Error("scheme object required, but got %S", mod_scm);
  mod = (mod_scm);
  env_scm = SCM_SUBRARGS[6];
  if (!(env_scm)) Scm_Error("scheme object required, but got %S", env_scm);
  env = (env_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_CompileSyntaxRules(name,src,ellipsis,literals,rules,mod,env));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libmacbasemacro_transformer(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj mac_scm;
  ScmMacro* mac;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("macro-transformer");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  mac_scm = SCM_SUBRARGS[0];
  if (!SCM_MACROP(mac_scm)) Scm_Error("macro required, but got %S", mac_scm);
  mac = SCM_MACRO(mac_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_MacroTransformer(mac));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libmacbasemacro_name(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj mac_scm;
  ScmMacro* mac;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("macro-name");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  mac_scm = SCM_SUBRARGS[0];
  if (!SCM_MACROP(mac_scm)) Scm_Error("macro required, but got %S", mac_scm);
  mac = SCM_MACRO(mac_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_MacroName(mac));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libmacbaseidentifier_macroP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj mac_scm;
  ScmMacro* mac;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("identifier-macro?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  mac_scm = SCM_SUBRARGS[0];
  if (!SCM_MACROP(mac_scm)) Scm_Error("macro required, but got %S", mac_scm);
  mac = SCM_MACRO(mac_scm);
  {
{
int SCM_RESULT;

#line 151 "libmacbase.scm"
{SCM_RESULT=(((mac)->flags)&(SCM_MACRO_IDENTIFIER));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libmacbasemake_syntax(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj name_scm;
  ScmSymbol* name;
  ScmObj module_scm;
  ScmModule* module;
  ScmObj proc_scm;
  ScmObj proc;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("make-syntax");
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  name_scm = SCM_SUBRARGS[0];
  if (!SCM_SYMBOLP(name_scm)) Scm_Error("<symbol> required, but got %S", name_scm);
  name = SCM_SYMBOL(name_scm);
  module_scm = SCM_SUBRARGS[1];
  if (!SCM_MODULEP(module_scm)) Scm_Error("<module> required, but got %S", module_scm);
  module = SCM_MODULE(module_scm);
  proc_scm = SCM_SUBRARGS[2];
  if (!(proc_scm)) Scm_Error("scheme object required, but got %S", proc_scm);
  proc = (proc_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_MakeSyntax(name,module,proc));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libmacbasecall_syntax_handler(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj syn_scm;
  ScmObj syn;
  ScmObj program_scm;
  ScmObj program;
  ScmObj cenv_scm;
  ScmObj cenv;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("call-syntax-handler");
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  syn_scm = SCM_SUBRARGS[0];
  if (!(syn_scm)) Scm_Error("scheme object required, but got %S", syn_scm);
  syn = (syn_scm);
  program_scm = SCM_SUBRARGS[1];
  if (!(program_scm)) Scm_Error("scheme object required, but got %S", program_scm);
  program = (program_scm);
  cenv_scm = SCM_SUBRARGS[2];
  if (!(cenv_scm)) Scm_Error("scheme object required, but got %S", cenv_scm);
  cenv = (cenv_scm);
  {
{
ScmObj SCM_RESULT;

#line 195 "libmacbase.scm"
SCM_ASSERT(SCM_SYNTAXP(syn));

#line 196 "libmacbase.scm"
{SCM_RESULT=(Scm_VMApply2((SCM_SYNTAX(syn))->handler,program,cenv));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libmacbasesyntax_handler(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj syn_scm;
  ScmObj syn;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("syntax-handler");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  syn_scm = SCM_SUBRARGS[0];
  if (!(syn_scm)) Scm_Error("scheme object required, but got %S", syn_scm);
  syn = (syn_scm);
  {
{
ScmObj SCM_RESULT;

#line 199 "libmacbase.scm"
SCM_ASSERT(SCM_SYNTAXP(syn));

#line 200 "libmacbase.scm"
{SCM_RESULT=((SCM_SYNTAX(syn))->handler);goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

static ScmCompiledCode *toplevels[] = {
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[1])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[3])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[5])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[10])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[11])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[14])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[16])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[18])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[22])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[25])),
 NULL /*termination*/
};
ScmObj SCM_debug_info_const_vector()
{
  static _Bool initialized = FALSE;
  if (!initialized) {
    int i = 0;
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[7];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[4];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[6];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = SCM_OBJ(&scm__sc.d1789[2]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[5];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[405];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[406];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[407];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[408];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[44];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[2];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[409];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[410];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[411];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[19];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[412];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[36];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[34];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[32];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[122];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[37];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = SCM_OBJ(&scm__sc.d1789[17]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[29];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[27];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[25];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[413];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[70];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[71];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[414];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[415];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[85];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[68];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[88];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[41];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[86];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[416];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[87];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[417];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[84];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[418];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[96];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[95];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = SCM_OBJ(&scm__sc.d1789[46]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[419];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[420];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[93];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[421];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[82];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[80];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[77];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[75];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[159];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[168];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[167];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[422];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[165];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[423];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[172];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[424];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[148];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[138];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[425];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[162];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[426];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[196];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[187];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[189];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = SCM_OBJ(&scm__sc.d1789[84]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[194];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[427];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[190];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[191];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[192];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = SCM_OBJ(&scm__sc.d1789[83]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = SCM_OBJ(&scm__sc.d1789[76]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[197];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[428];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[185];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[183];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[180];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[182];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[137];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[198];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[177];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = SCM_OBJ(&scm__sc.d1789[89]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[150];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[203];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[248];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[258];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[429];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[260];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[430];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[256];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[431];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = SCM_OBJ(&scm__sc.d1789[104]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[268];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[254];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[432];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[247];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[265];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[259];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[263];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[433];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[434];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[435];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[252];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[436];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[437];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[246];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[438];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[276];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[439];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[279];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[440];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[275];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[441];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[442];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[443];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1788[284]), i++) = scm__rc.d1788[273];
    initialized = TRUE;
  }
  return SCM_OBJ(&scm__rc.d1788[284]);
}
void Scm_Init_libmacbase() {
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
  scm__rc.d1788[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[0])),TRUE); /* %expression-name-mark-key */
  scm__rc.d1788[0] = Scm_MakeIdentifier(scm__rc.d1788[1], SCM_MODULE(Scm_GaucheInternalModule()), SCM_NIL); /* gauche.internal#%expression-name-mark-key */
  scm__rc.d1788[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[1])),TRUE); /* macroexpand */
  scm__rc.d1788[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[3])),TRUE); /* lambda */
  scm__rc.d1788[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[4])),TRUE); /* form */
  scm__rc.d1788[5] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1789[5]))); /* :optional */
  scm__rc.d1788[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[6])),TRUE); /* env/flag */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[2]), scm__rc.d1788[6]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[4]), scm__rc.d1788[5]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[5]), scm__rc.d1788[4]);
  scm__rc.d1788[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[7])),TRUE); /* %do-macroexpand */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[6]), scm__rc.d1788[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[7]), scm__rc.d1788[7]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[10]), scm__rc.d1788[3]);
  scm__rc.d1788[9] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[8])),TRUE); /* error */
  scm__rc.d1788[8] = Scm_MakeIdentifier(scm__rc.d1788[9], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#error */
  scm__rc.d1788[12] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[9])),TRUE); /* gauche.internal */
  scm__rc.d1788[11] = SCM_OBJ(Scm_FindModule(SCM_SYMBOL(scm__rc.d1788[12]), SCM_FIND_MODULE_CREATE|SCM_FIND_MODULE_PLACEHOLDING)); /* module gauche.internal */
  scm__rc.d1788[10] = Scm_MakeIdentifier(scm__rc.d1788[7], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#%do-macroexpand */
  scm__rc.d1788[13] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[10])),TRUE); /* source-info */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[13]), scm__rc.d1788[13]);
  scm__rc.d1788[14] = Scm_MakeExtendedPair(scm__rc.d1788[2], SCM_OBJ(&scm__rc.d1790[5]), SCM_OBJ(&scm__rc.d1790[14]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[15]), scm__rc.d1788[14]);
  scm__rc.d1788[15] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[0])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[0]))->name = scm__rc.d1788[2];/* macroexpand */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[0]))->debugInfo = scm__rc.d1788[15];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[0]))[28] = SCM_WORD(scm__rc.d1788[8]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[0]))[33] = SCM_WORD(scm__rc.d1788[10]);
  scm__rc.d1788[16] = Scm_MakeIdentifier(scm__rc.d1788[2], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#macroexpand */
  scm__rc.d1788[17] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[12])),TRUE); /* %toplevel */
  scm__rc.d1788[18] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[1])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[1]))->name = scm__rc.d1788[17];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[1]))->debugInfo = scm__rc.d1788[18];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[35]))[3] = SCM_WORD(scm__rc.d1788[0]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[35]))[6] = SCM_WORD(scm__rc.d1788[2]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[35]))[13] = SCM_WORD(scm__rc.d1788[16]);
  scm__rc.d1788[19] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[13])),TRUE); /* macroexpand-1 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[17]), scm__rc.d1788[6]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[18]), scm__rc.d1788[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[19]), scm__rc.d1788[7]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[22]), scm__rc.d1788[3]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[25]), scm__rc.d1788[13]);
  scm__rc.d1788[20] = Scm_MakeExtendedPair(scm__rc.d1788[19], SCM_OBJ(&scm__rc.d1790[5]), SCM_OBJ(&scm__rc.d1790[26]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[27]), scm__rc.d1788[20]);
  scm__rc.d1788[21] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[2])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[2]))->name = scm__rc.d1788[19];/* macroexpand-1 */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[2]))->debugInfo = scm__rc.d1788[21];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[50]))[28] = SCM_WORD(scm__rc.d1788[8]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[50]))[34] = SCM_WORD(scm__rc.d1788[10]);
  scm__rc.d1788[22] = Scm_MakeIdentifier(scm__rc.d1788[19], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#macroexpand-1 */
  scm__rc.d1788[23] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[3])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[3]))->name = scm__rc.d1788[17];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[3]))->debugInfo = scm__rc.d1788[23];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[86]))[3] = SCM_WORD(scm__rc.d1788[0]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[86]))[6] = SCM_WORD(scm__rc.d1788[19]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[86]))[13] = SCM_WORD(scm__rc.d1788[22]);
  scm__rc.d1788[25] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[14])),TRUE); /* boolean? */
  scm__rc.d1788[24] = Scm_MakeIdentifier(scm__rc.d1788[25], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#boolean? */
  scm__rc.d1788[27] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[15])),TRUE); /* vm-current-module */
  scm__rc.d1788[26] = Scm_MakeIdentifier(scm__rc.d1788[27], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#vm-current-module */
  scm__rc.d1788[29] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[16])),TRUE); /* module? */
  scm__rc.d1788[28] = Scm_MakeIdentifier(scm__rc.d1788[29], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#module? */
  scm__rc.d1788[30] = Scm_MakeIdentifier(scm__rc.d1788[9], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#error */
  scm__rc.d1788[32] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[18])),TRUE); /* make-cenv */
  scm__rc.d1788[31] = Scm_MakeIdentifier(scm__rc.d1788[32], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#make-cenv */
  scm__rc.d1788[34] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[19])),TRUE); /* %internal-macro-expand */
  scm__rc.d1788[33] = Scm_MakeIdentifier(scm__rc.d1788[34], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#%internal-macro-expand */
  scm__rc.d1788[36] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[20])),TRUE); /* unravel-syntax */
  scm__rc.d1788[35] = Scm_MakeIdentifier(scm__rc.d1788[36], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#unravel-syntax */
  scm__rc.d1788[37] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[21])),TRUE); /* once? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[28]), scm__rc.d1788[37]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[29]), scm__rc.d1788[6]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[30]), scm__rc.d1788[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[33]), scm__rc.d1788[13]);
  scm__rc.d1788[38] = Scm_MakeExtendedPair(scm__rc.d1788[7], SCM_OBJ(&scm__rc.d1790[30]), SCM_OBJ(&scm__rc.d1790[34]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[35]), scm__rc.d1788[38]);
  scm__rc.d1788[39] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[4])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[4]))->name = scm__rc.d1788[7];/* %do-macroexpand */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[4]))->debugInfo = scm__rc.d1788[39];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]))[4] = SCM_WORD(scm__rc.d1788[24]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]))[10] = SCM_WORD(scm__rc.d1788[26]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]))[17] = SCM_WORD(scm__rc.d1788[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]))[29] = SCM_WORD(scm__rc.d1788[30]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]))[38] = SCM_WORD(scm__rc.d1788[31]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]))[42] = SCM_WORD(scm__rc.d1788[33]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[101]))[50] = SCM_WORD(scm__rc.d1788[35]);
  scm__rc.d1788[40] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[5])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[5]))->name = scm__rc.d1788[17];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[5]))->debugInfo = scm__rc.d1788[40];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[154]))[3] = SCM_WORD(scm__rc.d1788[0]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[154]))[6] = SCM_WORD(scm__rc.d1788[7]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[154]))[13] = SCM_WORD(scm__rc.d1788[10]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1788[41] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[22])),TRUE); /* unwrap-syntax */
  scm__rc.d1788[42] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[23])),TRUE); /* to_immutable */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[36]), scm__rc.d1788[42]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[37]), scm__rc.d1788[5]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[38]), scm__rc.d1788[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[41]), scm__rc.d1788[13]);
  scm__rc.d1788[43] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[24])),TRUE); /* bind-info */
  scm__rc.d1788[44] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[25])),TRUE); /* gauche */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[42]), scm__rc.d1788[41]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[43]), scm__rc.d1788[44]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[44]), scm__rc.d1788[43]);
  scm__rc.d1788[45] = Scm_MakeExtendedPair(scm__rc.d1788[41], SCM_OBJ(&scm__rc.d1790[38]), SCM_OBJ(&scm__rc.d1790[46]));
  scm__rc.d1788[46] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[26])),TRUE); /* <top> */
  scm__rc.d1788[47] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[27])),TRUE); /* * */
  scm__rc.d1788[48] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[28])),TRUE); /* -> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[49]))[3] = scm__rc.d1788[44];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[49]))[4] = scm__rc.d1788[46];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[49]))[5] = scm__rc.d1788[47];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[49]))[6] = scm__rc.d1788[48];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[49]))[7] = scm__rc.d1788[46];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("unwrap-syntax")), SCM_OBJ(&libmacbaseunwrap_syntax__STUB), 0);
  libmacbaseunwrap_syntax__STUB.common.info = scm__rc.d1788[45];
  libmacbaseunwrap_syntax__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1788[49]);
  scm__rc.d1788[57] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[29])),TRUE); /* unwrap-syntax-1 */
  scm__rc.d1788[58] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[30])),TRUE); /* obj */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[47]), scm__rc.d1788[58]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[50]), scm__rc.d1788[13]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[51]), scm__rc.d1788[57]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[52]), scm__rc.d1788[44]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[53]), scm__rc.d1788[43]);
  scm__rc.d1788[59] = Scm_MakeExtendedPair(scm__rc.d1788[57], SCM_OBJ(&scm__rc.d1790[47]), SCM_OBJ(&scm__rc.d1790[55]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[60]))[3] = scm__rc.d1788[44];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[60]))[4] = scm__rc.d1788[46];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[60]))[5] = scm__rc.d1788[48];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[60]))[6] = scm__rc.d1788[46];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("unwrap-syntax-1")), SCM_OBJ(&libmacbaseunwrap_syntax_1__STUB), 0);
  libmacbaseunwrap_syntax_1__STUB.common.info = scm__rc.d1788[59];
  libmacbaseunwrap_syntax_1__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1788[60]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
  scm__rc.d1788[68] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[31])),TRUE); /* wrapped-identifier? */
  scm__rc.d1788[67] = Scm_MakeIdentifier(scm__rc.d1788[68], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#wrapped-identifier? */
  scm__rc.d1788[70] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[32])),TRUE); /* vector-map */
  scm__rc.d1788[69] = Scm_MakeIdentifier(scm__rc.d1788[70], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#vector-map */
  scm__rc.d1788[71] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[33])),TRUE); /* rec */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[56]), scm__rc.d1788[71]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[57]), scm__rc.d1788[36]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[58]), scm__rc.d1788[4]);
  scm__rc.d1788[72] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[6])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[6]))->name = scm__rc.d1788[71];/* (unravel-syntax rec) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[6]))->debugInfo = scm__rc.d1788[72];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[169]))[4] = SCM_WORD(scm__rc.d1788[67]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[169]))[36] = SCM_WORD(scm__rc.d1788[69]);
  scm__rc.d1788[73] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[34])),TRUE); /* eq? */
  scm__rc.d1788[75] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[35])),TRUE); /* make-hash-table */
  scm__rc.d1788[74] = Scm_MakeIdentifier(scm__rc.d1788[75], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#make-hash-table */
  scm__rc.d1788[77] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[36])),TRUE); /* hash-table-get */
  scm__rc.d1788[76] = Scm_MakeIdentifier(scm__rc.d1788[77], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#hash-table-get */
  scm__rc.d1788[78] = Scm_MakeIdentifier(scm__rc.d1788[41], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#unwrap-syntax */
  scm__rc.d1788[80] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[37])),TRUE); /* make-identifier */
  scm__rc.d1788[79] = Scm_MakeIdentifier(scm__rc.d1788[80], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#make-identifier */
  scm__rc.d1788[82] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[38])),TRUE); /* global-identifier=? */
  scm__rc.d1788[81] = Scm_MakeIdentifier(scm__rc.d1788[82], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#global-identifier=? */
  scm__rc.d1788[84] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[39])),TRUE); /* hash-table-put! */
  scm__rc.d1788[83] = Scm_MakeIdentifier(scm__rc.d1788[84], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#hash-table-put! */
  scm__rc.d1788[85] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[40])),TRUE); /* id->sym */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[63]), scm__rc.d1788[85]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[64]), scm__rc.d1788[36]);
  scm__rc.d1788[86] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[41])),TRUE); /* i */
  scm__rc.d1788[87] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[42])),TRUE); /* _ */
  scm__rc.d1788[88] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[43])),TRUE); /* c */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[65]), scm__rc.d1788[88]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[66]), scm__rc.d1788[87]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[67]), scm__rc.d1788[86]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[70]), scm__rc.d1788[13]);
  scm__rc.d1788[89] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[44])),TRUE); /* unused-args */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[71]), scm__rc.d1788[87]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[72]), scm__rc.d1788[89]);
  scm__rc.d1788[90] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1790[64]), SCM_OBJ(&scm__rc.d1790[67]), SCM_OBJ(&scm__rc.d1790[74]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[75]), scm__rc.d1788[90]);
  scm__rc.d1788[91] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[7])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[7]))->debugInfo = scm__rc.d1788[91];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[208]))[4] = SCM_WORD(scm__rc.d1788[78]);
  scm__rc.d1788[93] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[45])),TRUE); /* hash-table-fold */
  scm__rc.d1788[92] = Scm_MakeIdentifier(scm__rc.d1788[93], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#hash-table-fold */
  scm__rc.d1788[95] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[47])),TRUE); /* symbol-append */
  scm__rc.d1788[94] = Scm_MakeIdentifier(scm__rc.d1788[95], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#symbol-append */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[76]), scm__rc.d1788[85]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[77]), scm__rc.d1788[36]);
  scm__rc.d1788[96] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[48])),TRUE); /* id */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[78]), scm__rc.d1788[96]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[81]), scm__rc.d1788[13]);
  scm__rc.d1788[97] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1790[77]), SCM_OBJ(&scm__rc.d1790[78]), SCM_OBJ(&scm__rc.d1790[82]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[83]), scm__rc.d1788[97]);
  scm__rc.d1788[98] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[8])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[8]))->name = scm__rc.d1788[85];/* (unravel-syntax id->sym) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[8]))->debugInfo = scm__rc.d1788[98];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]))[6] = SCM_WORD(scm__rc.d1788[76]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]))[12] = SCM_WORD(scm__rc.d1788[78]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]))[23] = SCM_WORD(scm__rc.d1788[26]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]))[27] = SCM_WORD(scm__rc.d1788[79]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]))[29] = SCM_WORD(scm__rc.d1788[81]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]))[38] = SCM_WORD(scm__rc.d1788[83]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]))[48] = SCM_WORD(scm__rc.d1788[92]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]))[57] = SCM_WORD(scm__rc.d1788[94]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[220]))[65] = SCM_WORD(scm__rc.d1788[83]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[86]), scm__rc.d1788[13]);
  scm__rc.d1788[99] = Scm_MakeExtendedPair(scm__rc.d1788[36], SCM_OBJ(&scm__rc.d1790[58]), SCM_OBJ(&scm__rc.d1790[87]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[88]), scm__rc.d1788[99]);
  scm__rc.d1788[100] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[9])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[9]))->name = scm__rc.d1788[36];/* unravel-syntax */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[9]))->debugInfo = scm__rc.d1788[100];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[287]))[5] = SCM_WORD(scm__rc.d1788[73]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[287]))[7] = SCM_WORD(scm__rc.d1788[74]);
  scm__rc.d1788[101] = Scm_MakeIdentifier(scm__rc.d1788[36], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#unravel-syntax */
  scm__rc.d1788[102] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[10])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[10]))->name = scm__rc.d1788[17];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[10]))->debugInfo = scm__rc.d1788[102];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[304]))[3] = SCM_WORD(scm__rc.d1788[0]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[304]))[6] = SCM_WORD(scm__rc.d1788[36]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[304]))[13] = SCM_WORD(scm__rc.d1788[101]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
  scm__rc.d1788[103] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[49])),TRUE); /* macro? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[91]), scm__rc.d1788[13]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[92]), scm__rc.d1788[103]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[93]), scm__rc.d1788[12]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[94]), scm__rc.d1788[43]);
  scm__rc.d1788[104] = Scm_MakeExtendedPair(scm__rc.d1788[103], SCM_OBJ(&scm__rc.d1790[47]), SCM_OBJ(&scm__rc.d1790[96]));
  scm__rc.d1788[105] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[50])),TRUE); /* <boolean> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[106]))[3] = scm__rc.d1788[12];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[106]))[4] = scm__rc.d1788[46];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[106]))[5] = scm__rc.d1788[48];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[106]))[6] = scm__rc.d1788[105];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("macro?")), SCM_OBJ(&libmacbasemacroP__STUB), 0);
  libmacbasemacroP__STUB.common.info = scm__rc.d1788[104];
  libmacbasemacroP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1788[106]);
  scm__rc.d1788[113] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[51])),TRUE); /* syntax? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[99]), scm__rc.d1788[13]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[100]), scm__rc.d1788[113]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[101]), scm__rc.d1788[12]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[102]), scm__rc.d1788[43]);
  scm__rc.d1788[114] = Scm_MakeExtendedPair(scm__rc.d1788[113], SCM_OBJ(&scm__rc.d1790[47]), SCM_OBJ(&scm__rc.d1790[104]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("syntax?")), SCM_OBJ(&libmacbasesyntaxP__STUB), 0);
  libmacbasesyntaxP__STUB.common.info = scm__rc.d1788[114];
  libmacbasesyntaxP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1788[106]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  Scm_InitStaticClassWithMeta(&Scm_MacroClass, "<macro>", SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), NULL, SCM_FALSE, Scm_MacroClass__SLOTS, 0);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
  scm__rc.d1788[115] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[52])),TRUE); /* compile-syntax-rules */
  scm__rc.d1788[116] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[53])),TRUE); /* name */
  scm__rc.d1788[117] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[54])),TRUE); /* src */
  scm__rc.d1788[118] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[55])),TRUE); /* ellipsis */
  scm__rc.d1788[119] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[56])),TRUE); /* literals */
  scm__rc.d1788[120] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[57])),TRUE); /* rules */
  scm__rc.d1788[121] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[58])),TRUE); /* mod */
  scm__rc.d1788[122] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[59])),TRUE); /* env */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[105]), scm__rc.d1788[122]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[106]), scm__rc.d1788[121]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[107]), scm__rc.d1788[120]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[108]), scm__rc.d1788[119]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[109]), scm__rc.d1788[118]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[110]), scm__rc.d1788[117]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[111]), scm__rc.d1788[116]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[114]), scm__rc.d1788[13]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[115]), scm__rc.d1788[115]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[116]), scm__rc.d1788[12]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[117]), scm__rc.d1788[43]);
  scm__rc.d1788[123] = Scm_MakeExtendedPair(scm__rc.d1788[115], SCM_OBJ(&scm__rc.d1790[111]), SCM_OBJ(&scm__rc.d1790[119]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[124]))[3] = scm__rc.d1788[12];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[124]))[4] = scm__rc.d1788[46];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[124]))[5] = scm__rc.d1788[46];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[124]))[6] = scm__rc.d1788[46];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[124]))[7] = scm__rc.d1788[46];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[124]))[8] = scm__rc.d1788[46];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[124]))[9] = scm__rc.d1788[46];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[124]))[10] = scm__rc.d1788[46];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[124]))[11] = scm__rc.d1788[48];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[124]))[12] = scm__rc.d1788[46];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("compile-syntax-rules")), SCM_OBJ(&libmacbasecompile_syntax_rules__STUB), 0);
  libmacbasecompile_syntax_rules__STUB.common.info = scm__rc.d1788[123];
  libmacbasecompile_syntax_rules__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1788[124]);
  scm__rc.d1788[137] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[60])),TRUE); /* macro-transformer */
  scm__rc.d1788[138] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[61])),TRUE); /* mac */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[120]), scm__rc.d1788[138]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[123]), scm__rc.d1788[13]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[124]), scm__rc.d1788[137]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[125]), scm__rc.d1788[12]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[126]), scm__rc.d1788[43]);
  scm__rc.d1788[139] = Scm_MakeExtendedPair(scm__rc.d1788[137], SCM_OBJ(&scm__rc.d1790[120]), SCM_OBJ(&scm__rc.d1790[128]));
  scm__rc.d1788[140] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[62])),TRUE); /* <macro> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[141]))[3] = scm__rc.d1788[12];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[141]))[4] = scm__rc.d1788[140];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[141]))[5] = scm__rc.d1788[48];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[141]))[6] = scm__rc.d1788[46];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("macro-transformer")), SCM_OBJ(&libmacbasemacro_transformer__STUB), 0);
  libmacbasemacro_transformer__STUB.common.info = scm__rc.d1788[139];
  libmacbasemacro_transformer__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1788[141]);
  scm__rc.d1788[148] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[63])),TRUE); /* macro-name */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[131]), scm__rc.d1788[13]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[132]), scm__rc.d1788[148]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[133]), scm__rc.d1788[12]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[134]), scm__rc.d1788[43]);
  scm__rc.d1788[149] = Scm_MakeExtendedPair(scm__rc.d1788[148], SCM_OBJ(&scm__rc.d1790[120]), SCM_OBJ(&scm__rc.d1790[136]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("macro-name")), SCM_OBJ(&libmacbasemacro_name__STUB), 0);
  libmacbasemacro_name__STUB.common.info = scm__rc.d1788[149];
  libmacbasemacro_name__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1788[141]);
  scm__rc.d1788[150] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[64])),TRUE); /* identifier-macro? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[139]), scm__rc.d1788[13]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[140]), scm__rc.d1788[150]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[141]), scm__rc.d1788[12]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[142]), scm__rc.d1788[43]);
  scm__rc.d1788[151] = Scm_MakeExtendedPair(scm__rc.d1788[150], SCM_OBJ(&scm__rc.d1790[120]), SCM_OBJ(&scm__rc.d1790[144]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[152]))[3] = scm__rc.d1788[12];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[152]))[4] = scm__rc.d1788[140];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[152]))[5] = scm__rc.d1788[48];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[152]))[6] = scm__rc.d1788[105];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("identifier-macro?")), SCM_OBJ(&libmacbaseidentifier_macroP__STUB), 0);
  libmacbaseidentifier_macroP__STUB.common.info = scm__rc.d1788[151];
  libmacbaseidentifier_macroP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1788[152]);
  scm__rc.d1788[159] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[65])),TRUE); /* *trace-macro* */
  scm__rc.d1788[160] = Scm_MakeIdentifier(scm__rc.d1788[159], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#*trace-macro* */
  scm__rc.d1788[161] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[11])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[11]))->name = scm__rc.d1788[17];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[11]))->debugInfo = scm__rc.d1788[161];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[319]))[3] = SCM_WORD(scm__rc.d1788[0]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[319]))[6] = SCM_WORD(scm__rc.d1788[159]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[319]))[11] = SCM_WORD(scm__rc.d1788[160]);
  scm__rc.d1788[162] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[66])),TRUE); /* %macro-traced? */
  scm__rc.d1788[163] = Scm_MakeIdentifier(scm__rc.d1788[148], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#macro-name */
  scm__rc.d1788[165] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[67])),TRUE); /* regexp? */
  scm__rc.d1788[164] = Scm_MakeIdentifier(scm__rc.d1788[165], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#regexp? */
  scm__rc.d1788[167] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[68])),TRUE); /* symbol->string */
  scm__rc.d1788[166] = Scm_MakeIdentifier(scm__rc.d1788[167], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#symbol->string */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[145]), scm__rc.d1788[162]);
  scm__rc.d1788[168] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[69])),TRUE); /* z */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[146]), scm__rc.d1788[168]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[149]), scm__rc.d1788[13]);
  scm__rc.d1788[169] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1790[145]), SCM_OBJ(&scm__rc.d1790[146]), SCM_OBJ(&scm__rc.d1790[150]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[151]), scm__rc.d1788[169]);
  scm__rc.d1788[170] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[12])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[12]))->debugInfo = scm__rc.d1788[170];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[332]))[12] = SCM_WORD(scm__rc.d1788[164]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[332]))[19] = SCM_WORD(scm__rc.d1788[166]);
  scm__rc.d1788[172] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[70])),TRUE); /* find */
  scm__rc.d1788[171] = Scm_MakeIdentifier(scm__rc.d1788[172], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#find */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[154]), scm__rc.d1788[13]);
  scm__rc.d1788[173] = Scm_MakeExtendedPair(scm__rc.d1788[162], SCM_OBJ(&scm__rc.d1790[120]), SCM_OBJ(&scm__rc.d1790[155]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[156]), scm__rc.d1788[173]);
  scm__rc.d1788[174] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[13])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[13]))->name = scm__rc.d1788[162];/* %macro-traced? */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[13]))->debugInfo = scm__rc.d1788[174];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[357]))[1] = SCM_WORD(scm__rc.d1788[160]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[357]))[12] = SCM_WORD(scm__rc.d1788[163]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[357]))[14] = SCM_WORD(scm__rc.d1788[78]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[357]))[21] = SCM_WORD(scm__rc.d1788[160]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[357]))[23] = SCM_WORD(scm__rc.d1788[171]);
  scm__rc.d1788[175] = Scm_MakeIdentifier(scm__rc.d1788[162], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#%macro-traced? */
  scm__rc.d1788[176] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[14])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[14]))->name = scm__rc.d1788[17];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[14]))->debugInfo = scm__rc.d1788[176];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[382]))[3] = SCM_WORD(scm__rc.d1788[0]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[382]))[6] = SCM_WORD(scm__rc.d1788[162]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[382]))[13] = SCM_WORD(scm__rc.d1788[175]);
  scm__rc.d1788[177] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[71])),TRUE); /* call-macro-expander */
  scm__rc.d1788[178] = Scm_MakeIdentifier(scm__rc.d1788[137], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#macro-transformer */
  scm__rc.d1788[180] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[72])),TRUE); /* extended-pair? */
  scm__rc.d1788[179] = Scm_MakeIdentifier(scm__rc.d1788[180], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#extended-pair? */
  scm__rc.d1788[182] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[73])),TRUE); /* extended-cons */
  scm__rc.d1788[181] = Scm_MakeIdentifier(scm__rc.d1788[182], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#extended-cons */
  scm__rc.d1788[183] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[74])),TRUE); /* original */
  scm__rc.d1788[185] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[75])),TRUE); /* pair-attribute-set! */
  scm__rc.d1788[184] = Scm_MakeIdentifier(scm__rc.d1788[185], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#pair-attribute-set! */
  scm__rc.d1788[187] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[77])),TRUE); /* current-trace-port */
  scm__rc.d1788[186] = Scm_MakeIdentifier(scm__rc.d1788[187], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#current-trace-port */
  scm__rc.d1788[189] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[78])),TRUE); /* display */
  scm__rc.d1788[188] = Scm_MakeIdentifier(scm__rc.d1788[189], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#display */
  scm__rc.d1788[190] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1789[79]))); /* :port */
  scm__rc.d1788[191] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1789[80]))); /* :level */
  scm__rc.d1788[192] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1789[81]))); /* :length */
  scm__rc.d1788[194] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[82])),TRUE); /* pprint */
  scm__rc.d1788[193] = Scm_MakeIdentifier(scm__rc.d1788[194], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#pprint */
  scm__rc.d1788[196] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[85])),TRUE); /* flush */
  scm__rc.d1788[195] = Scm_MakeIdentifier(scm__rc.d1788[196], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#flush */
  scm__rc.d1788[197] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[86])),TRUE); /* expr */
  scm__rc.d1788[198] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[87])),TRUE); /* cenv */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[157]), scm__rc.d1788[198]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[158]), scm__rc.d1788[197]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[159]), scm__rc.d1788[138]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[162]), scm__rc.d1788[13]);
  scm__rc.d1788[199] = Scm_MakeExtendedPair(scm__rc.d1788[177], SCM_OBJ(&scm__rc.d1790[159]), SCM_OBJ(&scm__rc.d1790[163]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[164]), scm__rc.d1788[199]);
  scm__rc.d1788[200] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[15])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[15]))->name = scm__rc.d1788[177];/* call-macro-expander */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[15]))->debugInfo = scm__rc.d1788[200];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[8] = SCM_WORD(scm__rc.d1788[178]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[26] = SCM_WORD(scm__rc.d1788[179]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[38] = SCM_WORD(scm__rc.d1788[181]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[44] = SCM_WORD(scm__rc.d1788[183]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[47] = SCM_WORD(scm__rc.d1788[184]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[55] = SCM_WORD(scm__rc.d1788[160]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[62] = SCM_WORD(scm__rc.d1788[175]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[71] = SCM_WORD(scm__rc.d1788[35]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[80] = SCM_WORD(scm__rc.d1788[186]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[82] = SCM_WORD(scm__rc.d1788[188]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[88] = SCM_WORD(scm__rc.d1788[190]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[92] = SCM_WORD(scm__rc.d1788[186]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[95] = SCM_WORD(scm__rc.d1788[191]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[98] = SCM_WORD(scm__rc.d1788[192]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[101] = SCM_WORD(scm__rc.d1788[193]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[109] = SCM_WORD(scm__rc.d1788[186]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[111] = SCM_WORD(scm__rc.d1788[188]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[117] = SCM_WORD(scm__rc.d1788[190]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[121] = SCM_WORD(scm__rc.d1788[186]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[124] = SCM_WORD(scm__rc.d1788[191]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[127] = SCM_WORD(scm__rc.d1788[192]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[130] = SCM_WORD(scm__rc.d1788[193]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[138] = SCM_WORD(scm__rc.d1788[186]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[140] = SCM_WORD(scm__rc.d1788[188]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[146] = SCM_WORD(scm__rc.d1788[186]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[397]))[148] = SCM_WORD(scm__rc.d1788[195]);
  scm__rc.d1788[201] = Scm_MakeIdentifier(scm__rc.d1788[177], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#call-macro-expander */
  scm__rc.d1788[202] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[16])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[16]))->name = scm__rc.d1788[17];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[16]))->debugInfo = scm__rc.d1788[202];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[552]))[3] = SCM_WORD(scm__rc.d1788[0]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[552]))[6] = SCM_WORD(scm__rc.d1788[177]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[552]))[13] = SCM_WORD(scm__rc.d1788[201]);
  scm__rc.d1788[203] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[88])),TRUE); /* call-id-macro-expander */
  scm__rc.d1788[204] = Scm_MakeIdentifier(scm__rc.d1788[150], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#identifier-macro? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[165]), scm__rc.d1788[138]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[168]), scm__rc.d1788[13]);
  scm__rc.d1788[205] = Scm_MakeExtendedPair(scm__rc.d1788[203], SCM_OBJ(&scm__rc.d1790[165]), SCM_OBJ(&scm__rc.d1790[169]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[170]), scm__rc.d1788[205]);
  scm__rc.d1788[206] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[17])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[17]))->name = scm__rc.d1788[203];/* call-id-macro-expander */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[17]))->debugInfo = scm__rc.d1788[206];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[567]))[4] = SCM_WORD(scm__rc.d1788[204]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[567]))[15] = SCM_WORD(scm__rc.d1788[30]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[567]))[20] = SCM_WORD(scm__rc.d1788[201]);
  scm__rc.d1788[207] = Scm_MakeIdentifier(scm__rc.d1788[203], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#call-id-macro-expander */
  scm__rc.d1788[208] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[18])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[18]))->name = scm__rc.d1788[17];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[18]))->debugInfo = scm__rc.d1788[208];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[589]))[3] = SCM_WORD(scm__rc.d1788[0]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[589]))[6] = SCM_WORD(scm__rc.d1788[203]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[589]))[13] = SCM_WORD(scm__rc.d1788[207]);
  scm__rc.d1788[209] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[90])),TRUE); /* make-syntax */
  scm__rc.d1788[210] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[91])),TRUE); /* module */
  scm__rc.d1788[211] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[92])),TRUE); /* proc */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[171]), scm__rc.d1788[211]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[172]), scm__rc.d1788[210]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[173]), scm__rc.d1788[116]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[176]), scm__rc.d1788[13]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[177]), scm__rc.d1788[209]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[178]), scm__rc.d1788[12]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[179]), scm__rc.d1788[43]);
  scm__rc.d1788[212] = Scm_MakeExtendedPair(scm__rc.d1788[209], SCM_OBJ(&scm__rc.d1790[173]), SCM_OBJ(&scm__rc.d1790[181]));
  scm__rc.d1788[213] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[93])),TRUE); /* <symbol> */
  scm__rc.d1788[214] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[94])),TRUE); /* <module> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[215]))[3] = scm__rc.d1788[12];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[215]))[4] = scm__rc.d1788[213];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[215]))[5] = scm__rc.d1788[214];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[215]))[6] = scm__rc.d1788[46];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[215]))[7] = scm__rc.d1788[48];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[215]))[8] = scm__rc.d1788[46];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("make-syntax")), SCM_OBJ(&libmacbasemake_syntax__STUB), 0);
  libmacbasemake_syntax__STUB.common.info = scm__rc.d1788[212];
  libmacbasemake_syntax__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1788[215]);
  scm__rc.d1788[224] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[95])),TRUE); /* call-syntax-handler */
  scm__rc.d1788[225] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[96])),TRUE); /* syn */
  scm__rc.d1788[226] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[97])),TRUE); /* program */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[182]), scm__rc.d1788[226]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[183]), scm__rc.d1788[225]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[186]), scm__rc.d1788[13]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[187]), scm__rc.d1788[224]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[188]), scm__rc.d1788[12]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[189]), scm__rc.d1788[43]);
  scm__rc.d1788[227] = Scm_MakeExtendedPair(scm__rc.d1788[224], SCM_OBJ(&scm__rc.d1790[183]), SCM_OBJ(&scm__rc.d1790[191]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[228]))[3] = scm__rc.d1788[12];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[228]))[4] = scm__rc.d1788[46];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[228]))[5] = scm__rc.d1788[46];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[228]))[6] = scm__rc.d1788[46];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[228]))[7] = scm__rc.d1788[48];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[228]))[8] = scm__rc.d1788[46];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("call-syntax-handler")), SCM_OBJ(&libmacbasecall_syntax_handler__STUB), 0);
  libmacbasecall_syntax_handler__STUB.common.info = scm__rc.d1788[227];
  libmacbasecall_syntax_handler__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1788[228]);
  scm__rc.d1788[237] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[98])),TRUE); /* syntax-handler */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[192]), scm__rc.d1788[225]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[195]), scm__rc.d1788[13]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[196]), scm__rc.d1788[237]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[197]), scm__rc.d1788[12]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[198]), scm__rc.d1788[43]);
  scm__rc.d1788[238] = Scm_MakeExtendedPair(scm__rc.d1788[237], SCM_OBJ(&scm__rc.d1790[192]), SCM_OBJ(&scm__rc.d1790[200]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[239]))[3] = scm__rc.d1788[12];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[239]))[4] = scm__rc.d1788[46];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[239]))[5] = scm__rc.d1788[48];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1788[239]))[6] = scm__rc.d1788[46];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("syntax-handler")), SCM_OBJ(&libmacbasesyntax_handler__STUB), 0);
  libmacbasesyntax_handler__STUB.common.info = scm__rc.d1788[238];
  libmacbasesyntax_handler__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1788[239]);
  scm__rc.d1788[246] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[99])),TRUE); /* trace-macro */
  scm__rc.d1788[247] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[100])),FALSE); /* G1805 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[201]), scm__rc.d1788[247]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[202]), scm__rc.d1788[246]);
  scm__rc.d1788[248] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[101])),TRUE); /* x */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[203]), scm__rc.d1788[248]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[206]), scm__rc.d1788[13]);
  scm__rc.d1788[249] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1790[202]), SCM_OBJ(&scm__rc.d1790[203]), SCM_OBJ(&scm__rc.d1790[207]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[208]), scm__rc.d1788[249]);
  scm__rc.d1788[250] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[19])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[19]))->debugInfo = scm__rc.d1788[250];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[604]))[5] = SCM_WORD(scm__rc.d1788[164]);
  scm__rc.d1788[252] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[102])),TRUE); /* procedure? */
  scm__rc.d1788[251] = Scm_MakeIdentifier(scm__rc.d1788[252], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#procedure? */
  scm__rc.d1788[254] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[103])),TRUE); /* every */
  scm__rc.d1788[253] = Scm_MakeIdentifier(scm__rc.d1788[254], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#every */
  scm__rc.d1788[256] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[105])),TRUE); /* list? */
  scm__rc.d1788[255] = Scm_MakeIdentifier(scm__rc.d1788[256], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#list? */
  scm__rc.d1788[258] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[106])),TRUE); /* delete-duplicates */
  scm__rc.d1788[257] = Scm_MakeIdentifier(scm__rc.d1788[258], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#delete-duplicates */
  scm__rc.d1788[259] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[107])),FALSE); /* G1803 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[210]), scm__rc.d1788[259]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[211]), scm__rc.d1788[246]);
  scm__rc.d1788[260] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[108])),TRUE); /* objs */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[212]), scm__rc.d1788[260]);
  scm__rc.d1788[261] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[20])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[20]))->name = scm__rc.d1788[259];/* (trace-macro G1803) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[20]))->debugInfo = scm__rc.d1788[261];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]))[5] = SCM_WORD(scm__rc.d1788[253]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]))[16] = SCM_WORD(scm__rc.d1788[30]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]))[18] = SCM_WORD(scm__rc.d1788[160]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]))[24] = SCM_WORD(scm__rc.d1788[160]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]))[26] = SCM_WORD(scm__rc.d1788[255]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]))[30] = SCM_WORD(scm__rc.d1788[160]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]))[46] = SCM_WORD(scm__rc.d1788[257]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[611]))[48] = SCM_WORD(scm__rc.d1788[160]);
  scm__rc.d1788[263] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[109])),TRUE); /* equal? */
  scm__rc.d1788[262] = Scm_MakeIdentifier(scm__rc.d1788[263], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#equal? */
  scm__rc.d1788[265] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[110])),TRUE); /* match:error */
  scm__rc.d1788[267] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[111])),TRUE); /* util.match */
  scm__rc.d1788[266] = SCM_OBJ(Scm_FindModule(SCM_SYMBOL(scm__rc.d1788[267]), SCM_FIND_MODULE_CREATE|SCM_FIND_MODULE_PLACEHOLDING)); /* module util.match */
  scm__rc.d1788[264] = Scm_MakeIdentifier(scm__rc.d1788[265], SCM_MODULE(scm__rc.d1788[266]), SCM_NIL); /* util.match#match:error */
  scm__rc.d1788[268] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[112])),TRUE); /* args */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[217]), scm__rc.d1788[13]);
  scm__rc.d1788[269] = Scm_MakeExtendedPair(scm__rc.d1788[246], scm__rc.d1788[268], SCM_OBJ(&scm__rc.d1790[218]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[219]), scm__rc.d1788[269]);
  scm__rc.d1788[270] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[21])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[21]))->name = scm__rc.d1788[246];/* trace-macro */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[21]))->debugInfo = scm__rc.d1788[270];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]))[3] = SCM_WORD(scm__rc.d1788[193]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]))[5] = SCM_WORD(scm__rc.d1788[251]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]))[25] = SCM_WORD(scm__rc.d1788[262]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]))[34] = SCM_WORD(scm__rc.d1788[160]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]))[41] = SCM_WORD(scm__rc.d1788[255]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]))[55] = SCM_WORD(scm__rc.d1788[264]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]))[64] = SCM_WORD(scm__rc.d1788[262]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]))[72] = SCM_WORD(scm__rc.d1788[160]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]))[79] = SCM_WORD(scm__rc.d1788[255]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]))[93] = SCM_WORD(scm__rc.d1788[264]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]))[104] = SCM_WORD(scm__rc.d1788[255]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]))[118] = SCM_WORD(scm__rc.d1788[264]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[661]))[121] = SCM_WORD(scm__rc.d1788[160]);
  scm__rc.d1788[271] = Scm_MakeIdentifier(scm__rc.d1788[246], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#trace-macro */
  scm__rc.d1788[272] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[22])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[22]))->name = scm__rc.d1788[17];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[22]))->debugInfo = scm__rc.d1788[272];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[784]))[5] = SCM_WORD(scm__rc.d1788[0]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[784]))[8] = SCM_WORD(scm__rc.d1788[246]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[784]))[15] = SCM_WORD(scm__rc.d1788[271]);
  scm__rc.d1788[273] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[113])),TRUE); /* untrace-macro */
  scm__rc.d1788[275] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[114])),TRUE); /* member */
  scm__rc.d1788[274] = Scm_MakeIdentifier(scm__rc.d1788[275], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#member */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[221]), scm__rc.d1788[273]);
  scm__rc.d1788[276] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[115])),FALSE); /* G1813 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[222]), scm__rc.d1788[276]);
  scm__rc.d1788[277] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[23])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[23]))->debugInfo = scm__rc.d1788[277];
  scm__rc.d1788[279] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[116])),TRUE); /* remove */
  scm__rc.d1788[278] = Scm_MakeIdentifier(scm__rc.d1788[279], SCM_MODULE(scm__rc.d1788[11]), SCM_NIL); /* gauche.internal#remove */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[227]), scm__rc.d1788[13]);
  scm__rc.d1788[280] = Scm_MakeExtendedPair(scm__rc.d1788[273], scm__rc.d1788[268], SCM_OBJ(&scm__rc.d1790[228]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1790[229]), scm__rc.d1788[280]);
  scm__rc.d1788[281] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[24])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[24]))->name = scm__rc.d1788[273];/* untrace-macro */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[24]))->debugInfo = scm__rc.d1788[281];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]))[5] = SCM_WORD(scm__rc.d1788[160]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]))[12] = SCM_WORD(scm__rc.d1788[255]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]))[18] = SCM_WORD(scm__rc.d1788[160]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]))[20] = SCM_WORD(scm__rc.d1788[255]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]))[28] = SCM_WORD(scm__rc.d1788[274]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]))[34] = SCM_WORD(scm__rc.d1788[160]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]))[36] = SCM_WORD(scm__rc.d1788[278]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]))[46] = SCM_WORD(scm__rc.d1788[160]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]))[56] = SCM_WORD(scm__rc.d1788[264]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[806]))[58] = SCM_WORD(scm__rc.d1788[160]);
  scm__rc.d1788[282] = Scm_MakeIdentifier(scm__rc.d1788[273], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#untrace-macro */
  scm__rc.d1788[283] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1793[25])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[25]))->name = scm__rc.d1788[17];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1792[25]))->debugInfo = scm__rc.d1788[283];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[866]))[3] = SCM_WORD(scm__rc.d1788[0]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[866]))[6] = SCM_WORD(scm__rc.d1788[273]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1791[866]))[13] = SCM_WORD(scm__rc.d1788[282]);
  Scm_VMExecuteToplevels(toplevels);
  scm__rc.d1788[405] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[117])),FALSE); /* G1786 */
  scm__rc.d1788[406] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[118])),FALSE); /* G1787 */
  scm__rc.d1788[407] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[119])),FALSE); /* rest1785 */
  scm__rc.d1788[408] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[120])),TRUE); /* define-in-module */
  scm__rc.d1788[409] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[121])),FALSE); /* G1795 */
  scm__rc.d1788[410] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[122])),FALSE); /* G1796 */
  scm__rc.d1788[411] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[123])),FALSE); /* rest1794 */
  scm__rc.d1788[412] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[124])),TRUE); /* r */
  scm__rc.d1788[413] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[125])),TRUE); /* let* */
  scm__rc.d1788[414] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[126])),TRUE); /* vector? */
  scm__rc.d1788[415] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[127])),TRUE); /* pair? */
  scm__rc.d1788[416] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[128])),TRUE); /* nam */
  scm__rc.d1788[417] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[129])),TRUE); /* sym */
  scm__rc.d1788[418] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[130])),TRUE); /* tab */
  scm__rc.d1788[419] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[131])),TRUE); /* num */
  scm__rc.d1788[420] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[132])),TRUE); /* $ */
  scm__rc.d1788[421] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[133])),TRUE); /* rlet1 */
  scm__rc.d1788[422] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[134])),TRUE); /* macname */
  scm__rc.d1788[423] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[135])),TRUE); /* symbol? */
  scm__rc.d1788[424] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[136])),TRUE); /* ^z */
  scm__rc.d1788[425] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[137])),TRUE); /* and-let1 */
  scm__rc.d1788[426] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[138])),TRUE); /* out */
  scm__rc.d1788[427] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[139])),TRUE); /* unraveled */
  scm__rc.d1788[428] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[140])),TRUE); /* p */
  scm__rc.d1788[429] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[141])),TRUE); /* append */
  scm__rc.d1788[430] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[142])),TRUE); /* ms */
  scm__rc.d1788[431] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[143])),TRUE); /* set! */
  scm__rc.d1788[432] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[144])),TRUE); /* ^x */
  scm__rc.d1788[433] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[145])),FALSE); /* G1799 */
  scm__rc.d1788[434] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[146])),FALSE); /* G1798 */
  scm__rc.d1788[435] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[147])),FALSE); /* G1797 */
  scm__rc.d1788[436] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[148])),TRUE); /* match */
  scm__rc.d1788[437] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[149])),TRUE); /* ... */
  scm__rc.d1788[438] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[150])),FALSE); /* G1812 */
  scm__rc.d1788[439] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[151])),FALSE); /* G1814 */
  scm__rc.d1788[440] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[152])),TRUE); /* cute */
  scm__rc.d1788[441] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[153])),TRUE); /* <> */
  scm__rc.d1788[442] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[154])),TRUE); /* letrec */
  scm__rc.d1788[443] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1789[155])),FALSE); /* G1810 */
}
