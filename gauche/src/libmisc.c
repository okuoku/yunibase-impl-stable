/* Generated automatically from libmisc.scm.  DO NOT EDIT */
#define LIBGAUCHE_BODY 
#include <gauche.h>
#include <gauche/code.h>
#include "gauche/priv/configP.h"
#include "gauche/vminsn.h"
static ScmObj libmischas_setterP(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libmischas_setterP__STUB, 1, 0,SCM_FALSE,libmischas_setterP, NULL, NULL);

static ScmObj libmiscundefined(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libmiscundefined__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libmiscundefined, SCM_MAKE_INT(SCM_VM_CONSTU), NULL);

static ScmObj libmiscundefinedP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libmiscundefinedP__STUB, 1, 0,1, SCM_FALSE,0, libmiscundefinedP, NULL, NULL);

static unsigned char uvector__00001[] = {
 0u, 3u, 140u, 134u, 8u, 0u, 64u, 36u, 145u, 198u, 3u, 2u, 96u, 82u,
71u, 23u, 12u, 14u, 176u, 16u, 128u, 72u, 128u, 224u, 72u, 20u, 192u,
196u, 142u, 44u, 24u, 18u, 6u, 35u, 137u, 134u, 4u, 194u, 160u, 142u,
36u, 24u, 18u, 5u, 35u, 135u, 6u, 4u, 194u, 160u, 142u, 26u, 24u, 19u,
9u, 130u, 56u, 88u, 96u, 72u, 8u, 142u, 18u, 24u, 19u, 8u, 50u, 56u,
48u, 96u, 111u, 16u, 60u, 16u, 66u, 97u, 6u, 19u, 1u, 194u, 71u, 4u,
12u, 9u, 145u, 153u, 28u, 0u, 48u, 38u, 70u, 100u, 49u, 6u, 6u, 49u,
2u, 164u, 12u, 19u, 34u, 130u, 72u,};
static unsigned char uvector__00002[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 4u, 192u, 169u, 3u, 6u, 241u, 3u, 193u,
4u, 58u, 192u, 66u, 1u, 34u, 3u, 129u, 32u, 83u, 3u, 17u, 0u, 8u, 4u,
146u, 72u, 225u, 33u, 129u, 140u, 38u, 6u, 41u, 130u, 73u, 28u, 28u,
48u, 38u, 3u, 132u, 112u, 32u, 192u, 152u, 14u, 17u, 192u, 3u, 2u,
96u, 56u, 72u,};
static ScmObj libmisc_25uninitialized(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libmisc_25uninitialized__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libmisc_25uninitialized, NULL, NULL);

static ScmObj libmiscdebug_label(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libmiscdebug_label__STUB, 1, 0,SCM_FALSE,libmiscdebug_label, NULL, NULL);

static ScmObj libmiscforeign_pointer_invalidP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libmiscforeign_pointer_invalidP__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libmiscforeign_pointer_invalidP, NULL, NULL);

static ScmObj libmiscforeign_pointer_invalidateX(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libmiscforeign_pointer_invalidateX__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libmiscforeign_pointer_invalidateX, NULL, NULL);

static ScmObj libmiscforeign_pointer_attributes(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libmiscforeign_pointer_attributes__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libmiscforeign_pointer_attributes, NULL, NULL);

static ScmObj libmiscforeign_pointer_attribute_get(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libmiscforeign_pointer_attribute_get__STUB, 2, 2,SCM_FALSE,libmiscforeign_pointer_attribute_get, NULL, NULL);

static ScmObj libmiscforeign_pointer_attribute_setX(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libmiscforeign_pointer_attribute_setX__STUB, 3, 0,SCM_FALSE,libmiscforeign_pointer_attribute_setX, NULL, NULL);

static unsigned char uvector__00003[] = {
 0u, 3u, 134u, 6u, 6u, 104u, 42u, 11u, 36u, 112u, 144u, 192u, 144u,
89u, 28u, 28u, 48u, 38u, 3u, 132u, 112u, 32u, 192u, 152u, 14u, 17u,
192u, 3u, 2u, 96u, 56u, 72u,};
static ScmObj libmisccond_features(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libmisccond_features__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libmisccond_features, NULL, NULL);

static ScmObj SCM_debug_info_const_vector();
#if defined(__CYGWIN__) || defined(GAUCHE_WINDOWS)
#define SCM_CGEN_CONST /*empty*/
#else
#define SCM_CGEN_CONST const
#endif
static SCM_CGEN_CONST struct scm__scRec {
  ScmString d1785[43];
} scm__sc SCM_UNUSED = {
  {   /* ScmString d1785 */
      SCM_STRING_CONST_INITIALIZER("has-setter\077", 11, 11),
      SCM_STRING_CONST_INITIALIZER("proc", 4, 4),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libmisc.scm", 11, 11),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<top>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<boolean>", 9, 9),
      SCM_STRING_CONST_INITIALIZER("undefined", 9, 9),
      SCM_STRING_CONST_INITIALIZER("undefined\077", 10, 10),
      SCM_STRING_CONST_INITIALIZER("obj", 3, 3),
      SCM_STRING_CONST_INITIALIZER("%expression-name-mark-key", 25, 25),
      SCM_STRING_CONST_INITIALIZER("warn", 4, 4),
      SCM_STRING_CONST_INITIALIZER("GAUCHE_SUPPRESS_WARNING", 23, 23),
      SCM_STRING_CONST_INITIALIZER("sys-getenv", 10, 10),
      SCM_STRING_CONST_INITIALIZER("format", 6, 6),
      SCM_STRING_CONST_INITIALIZER("WARNING: ", 9, 9),
      SCM_STRING_CONST_INITIALIZER("string-append", 13, 13),
      SCM_STRING_CONST_INITIALIZER("flush", 5, 5),
      SCM_STRING_CONST_INITIALIZER("fmt", 3, 3),
      SCM_STRING_CONST_INITIALIZER("args", 4, 4),
      SCM_STRING_CONST_INITIALIZER("%toplevel", 9, 9),
      SCM_STRING_CONST_INITIALIZER("%uninitialized", 14, 14),
      SCM_STRING_CONST_INITIALIZER("gauche.internal", 15, 15),
      SCM_STRING_CONST_INITIALIZER("debug-label", 11, 11),
      SCM_STRING_CONST_INITIALIZER("foreign-pointer-invalid\077", 24, 24),
      SCM_STRING_CONST_INITIALIZER("fp", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<foreign-pointer>", 17, 17),
      SCM_STRING_CONST_INITIALIZER("foreign-pointer-invalidate!", 27, 27),
      SCM_STRING_CONST_INITIALIZER("<void>", 6, 6),
      SCM_STRING_CONST_INITIALIZER("foreign-pointer-attributes", 26, 26),
      SCM_STRING_CONST_INITIALIZER("foreign-pointer-attribute-get", 29, 29),
      SCM_STRING_CONST_INITIALIZER("key", 3, 3),
      SCM_STRING_CONST_INITIALIZER("optional", 8, 8),
      SCM_STRING_CONST_INITIALIZER("fallback", 8, 8),
      SCM_STRING_CONST_INITIALIZER("*", 1, 1),
      SCM_STRING_CONST_INITIALIZER("foreign-pointer-attribute-set!", 30, 30),
      SCM_STRING_CONST_INITIALIZER("value", 5, 5),
      SCM_STRING_CONST_INITIALIZER("foreign-pointer-attribute-set", 29, 29),
      SCM_STRING_CONST_INITIALIZER("cond-features", 13, 13),
      SCM_STRING_CONST_INITIALIZER("SLIB_DIR", 8, 8),
      SCM_STRING_CONST_INITIALIZER("current-error-port", 18, 18),
  },
};
static struct scm__rcRec {
  ScmUVector d1790[3];
  ScmCompiledCode d1789[3];
  ScmWord d1788[58];
  ScmPair d1787[93] SCM_ALIGN_PAIR;
  ScmObj d1786[143];
} scm__rc SCM_UNUSED = {
  {   /* ScmUVector d1790 */
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 103, uvector__00001, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 58, uvector__00002, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 32, uvector__00003, 0, NULL),
  },
  {   /* ScmCompiledCode d1789 */
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* warn */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[0])), 28,
            22, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[28]),
            SCM_OBJ(&scm__rc.d1789[1]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[28])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[43])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
  },
  {   /* ScmWord d1788 */
    /* warn */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[0]) + 6),
    0x00000006    /*   2 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[14])) /* "GAUCHE_SUPPRESS_WARNING" */,
    0x0000105f    /*   4 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-getenv.adba1680> */,
    0x0000001e    /*   6 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[0]) + 9),
    0x0000000c    /*   8 (CONSTU-RET) */,
    0x0000000e    /*   9 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[0]) + 24),
    0x0000005e    /*  11 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#format.adba1600> */,
    0x000000e0    /*  13 (CURERR) */,
    0x0000200f    /*  14 (PUSH-PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[0]) + 21),
    0x00000006    /*  16 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[17])) /* "WARNING: " */,
    0x00000049    /*  18 (LREF1-PUSH) */,
    0x0000205f    /*  19 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-append.adba15a0> */,
    0x0000000d    /*  21 (PUSH) */,
    0x0000003d    /*  22 (LREF0) */,
    0x00004095    /*  23 (TAIL-APPLY 4) */,
    0x000000e0    /*  24 (CURERR) */,
    0x00001063    /*  25 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#flush.adba1500> */,
    0x00000014    /*  27 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[28]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.aee3eba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* warn */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[28]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[0])) /* #<compiled-code warn@0x7f13ad8c0420> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#warn.adba17a0> */,
    0x00000014    /*  14 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[43]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.aee3eba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* foreign-pointer-attribute-set */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[43]) + 12),
    0x0000005d    /*   9 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#foreign-pointer-attribute-set!.ad6e4540> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#foreign-pointer-attribute-set.ad6e45c0> */,
    0x00000014    /*  14 (RET) */,
  },
  {   /* ScmPair d1787 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(45U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[2])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[3])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[5])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[6])},
       { SCM_OBJ(&scm__rc.d1787[7]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[4]), SCM_OBJ(&scm__rc.d1787[8])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[10])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[11])},
       { SCM_OBJ(&scm__rc.d1787[12]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(48U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[15])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[16])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[18])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[19])},
       { SCM_OBJ(&scm__rc.d1787[20]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[17]), SCM_OBJ(&scm__rc.d1787[21])},
       { SCM_UNDEFINED, SCM_UNDEFINED},
       { SCM_MAKE_INT(50U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[24])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[25])},
       { SCM_OBJ(&scm__rc.d1787[26]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[29])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[30])},
       { SCM_OBJ(&scm__rc.d1787[31]), SCM_NIL},
       { SCM_MAKE_INT(62U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[33])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[34])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[36])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[37])},
       { SCM_OBJ(&scm__rc.d1787[38]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[35]), SCM_OBJ(&scm__rc.d1787[39])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(68U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[42])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[43])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[45])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[46])},
       { SCM_OBJ(&scm__rc.d1787[47]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[44]), SCM_OBJ(&scm__rc.d1787[48])},
       { SCM_MAKE_INT(71U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[50])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[51])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[53])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[54])},
       { SCM_OBJ(&scm__rc.d1787[55]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[52]), SCM_OBJ(&scm__rc.d1787[56])},
       { SCM_MAKE_INT(74U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[58])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[59])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[61])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[62])},
       { SCM_OBJ(&scm__rc.d1787[63]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[60]), SCM_OBJ(&scm__rc.d1787[64])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[66])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[67])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[68])},
       { SCM_MAKE_INT(77U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[70])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[71])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[73])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[74])},
       { SCM_OBJ(&scm__rc.d1787[75]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[72]), SCM_OBJ(&scm__rc.d1787[76])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[78])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[79])},
       { SCM_MAKE_INT(81U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[81])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[82])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[84])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[85])},
       { SCM_OBJ(&scm__rc.d1787[86]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[83]), SCM_OBJ(&scm__rc.d1787[87])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[89])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[90])},
       { SCM_OBJ(&scm__rc.d1787[91]), SCM_NIL},
  },
  {   /* ScmObj d1786 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(4, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(4, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(12, FALSE),
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_UNBOUND,
  },
};

static ScmObj libmischas_setterP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj proc_scm;
  ScmObj proc;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("has-setter?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  proc_scm = SCM_SUBRARGS[0];
  if (!(proc_scm)) Scm_Error("scheme object required, but got %S", proc_scm);
  proc = (proc_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(Scm_HasSetter(proc));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libmiscundefined(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("undefined");
  {
{
ScmObj SCM_RESULT;

#line 47 "libmisc.scm"
{SCM_RESULT=(SCM_UNDEFINED);goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libmiscundefinedP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("undefined?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(SCM_UNDEFINEDP(obj));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libmisc_25uninitialized(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("%uninitialized");
  {
{
ScmObj SCM_RESULT;

#line 58 "libmisc.scm"
{SCM_RESULT=(SCM_UNINITIALIZED);goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libmiscdebug_label(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("debug-label");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {
{
ScmObj SCM_RESULT;

#line 62 "libmisc.scm"
{SCM_RESULT=(Scm_Sprintf("@%lx",obj));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libmiscforeign_pointer_invalidP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj fp_scm;
  ScmForeignPointer* fp;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("foreign-pointer-invalid?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  fp_scm = SCM_SUBRARGS[0];
  if (!SCM_FOREIGN_POINTER_P(fp_scm)) Scm_Error("<foreign-pointer> required, but got %S", fp_scm);
  fp = SCM_FOREIGN_POINTER(fp_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(Scm_ForeignPointerInvalidP(fp));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libmiscforeign_pointer_invalidateX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj fp_scm;
  ScmForeignPointer* fp;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("foreign-pointer-invalidate!");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  fp_scm = SCM_SUBRARGS[0];
  if (!SCM_FOREIGN_POINTER_P(fp_scm)) Scm_Error("<foreign-pointer> required, but got %S", fp_scm);
  fp = SCM_FOREIGN_POINTER(fp_scm);
  {
Scm_ForeignPointerInvalidate(fp);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libmiscforeign_pointer_attributes(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj fp_scm;
  ScmForeignPointer* fp;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("foreign-pointer-attributes");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  fp_scm = SCM_SUBRARGS[0];
  if (!SCM_FOREIGN_POINTER_P(fp_scm)) Scm_Error("<foreign-pointer> required, but got %S", fp_scm);
  fp = SCM_FOREIGN_POINTER(fp_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_ForeignPointerAttr(fp));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libmiscforeign_pointer_attribute_get(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj fp_scm;
  ScmForeignPointer* fp;
  ScmObj key_scm;
  ScmObj key;
  ScmObj fallback_scm;
  ScmObj fallback;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("foreign-pointer-attribute-get");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  fp_scm = SCM_SUBRARGS[0];
  if (!SCM_FOREIGN_POINTER_P(fp_scm)) Scm_Error("<foreign-pointer> required, but got %S", fp_scm);
  fp = SCM_FOREIGN_POINTER(fp_scm);
  key_scm = SCM_SUBRARGS[1];
  if (!(key_scm)) Scm_Error("scheme object required, but got %S", key_scm);
  key = (key_scm);
  if (SCM_ARGCNT > 2+1) {
    fallback_scm = SCM_SUBRARGS[2];
  } else {
    fallback_scm = SCM_UNBOUND;
  }
  if (!(fallback_scm)) Scm_Error("scheme object required, but got %S", fallback_scm);
  fallback = (fallback_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_ForeignPointerAttrGet(fp,key,fallback));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libmiscforeign_pointer_attribute_setX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj fp_scm;
  ScmForeignPointer* fp;
  ScmObj key_scm;
  ScmObj key;
  ScmObj value_scm;
  ScmObj value;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("foreign-pointer-attribute-set!");
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  fp_scm = SCM_SUBRARGS[0];
  if (!SCM_FOREIGN_POINTER_P(fp_scm)) Scm_Error("<foreign-pointer> required, but got %S", fp_scm);
  fp = SCM_FOREIGN_POINTER(fp_scm);
  key_scm = SCM_SUBRARGS[1];
  if (!(key_scm)) Scm_Error("scheme object required, but got %S", key_scm);
  key = (key_scm);
  value_scm = SCM_SUBRARGS[2];
  if (!(value_scm)) Scm_Error("scheme object required, but got %S", value_scm);
  value = (value_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_ForeignPointerAttrSet(fp,key,value));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libmisccond_features(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("cond-features");
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_GetFeatures());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

static ScmCompiledCode *toplevels[] = {
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[1])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[2])),
 NULL /*termination*/
};
ScmObj SCM_debug_info_const_vector()
{
  static _Bool initialized = FALSE;
  if (!initialized) {
    int i = 0;
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[128]), i++) = scm__rc.d1786[38];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[128]), i++) = scm__rc.d1786[142];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[128]), i++) = scm__rc.d1786[34];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[128]), i++) = scm__rc.d1786[36];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[128]), i++) = SCM_OBJ(&scm__sc.d1785[17]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[128]), i++) = scm__rc.d1786[39];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[128]), i++) = scm__rc.d1786[40];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[128]), i++) = scm__rc.d1786[31];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[128]), i++) = SCM_OBJ(&scm__sc.d1785[14]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[128]), i++) = scm__rc.d1786[29];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[128]), i++) = scm__rc.d1786[121];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[128]), i++) = scm__rc.d1786[109];
    initialized = TRUE;
  }
  return SCM_OBJ(&scm__rc.d1786[128]);
}
void Scm_Init_libmisc() {
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[0])),TRUE); /* has-setter? */
  scm__rc.d1786[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[1])),TRUE); /* proc */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1]), scm__rc.d1786[1]);
  scm__rc.d1786[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[2])),TRUE); /* source-info */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[4]), scm__rc.d1786[2]);
  scm__rc.d1786[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[4])),TRUE); /* bind-info */
  scm__rc.d1786[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[5])),TRUE); /* gauche */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[5]), scm__rc.d1786[0]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[6]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[7]), scm__rc.d1786[3]);
  scm__rc.d1786[5] = Scm_MakeExtendedPair(scm__rc.d1786[0], SCM_OBJ(&scm__rc.d1787[1]), SCM_OBJ(&scm__rc.d1787[9]));
  scm__rc.d1786[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[6])),TRUE); /* <top> */
  scm__rc.d1786[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[7])),TRUE); /* -> */
  scm__rc.d1786[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[8])),TRUE); /* <boolean> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[9]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[9]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[9]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[9]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("has-setter?")), SCM_OBJ(&libmischas_setterP__STUB), 0);
  libmischas_setterP__STUB.common.info = scm__rc.d1786[5];
  libmischas_setterP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[9]);
  scm__rc.d1786[16] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[9])),TRUE); /* undefined */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[10]), scm__rc.d1786[16]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[11]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[12]), scm__rc.d1786[3]);
  scm__rc.d1786[17] = Scm_MakeExtendedPair(scm__rc.d1786[16], SCM_NIL, SCM_OBJ(&scm__rc.d1787[13]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[18]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[18]))[4] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[18]))[5] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("undefined")), SCM_OBJ(&libmiscundefined__STUB), SCM_BINDING_INLINABLE);
  libmiscundefined__STUB.common.info = scm__rc.d1786[17];
  libmiscundefined__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[18]);
  scm__rc.d1786[24] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[10])),TRUE); /* undefined? */
  scm__rc.d1786[25] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[11])),TRUE); /* obj */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[14]), scm__rc.d1786[25]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[17]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[18]), scm__rc.d1786[24]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[19]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[20]), scm__rc.d1786[3]);
  scm__rc.d1786[26] = Scm_MakeExtendedPair(scm__rc.d1786[24], SCM_OBJ(&scm__rc.d1787[14]), SCM_OBJ(&scm__rc.d1787[22]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("undefined?")), SCM_OBJ(&libmiscundefinedP__STUB), SCM_BINDING_INLINABLE);
  libmiscundefinedP__STUB.common.info = scm__rc.d1786[26];
  libmiscundefinedP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[9]);
  scm__rc.d1786[28] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[12])),TRUE); /* %expression-name-mark-key */
  scm__rc.d1786[27] = Scm_MakeIdentifier(scm__rc.d1786[28], SCM_MODULE(Scm_GaucheInternalModule()), SCM_NIL); /* gauche.internal#%expression-name-mark-key */
  scm__rc.d1786[29] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[13])),TRUE); /* warn */
  scm__rc.d1786[31] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[15])),TRUE); /* sys-getenv */
  scm__rc.d1786[32] = SCM_OBJ(Scm_FindModule(SCM_SYMBOL(scm__rc.d1786[4]), SCM_FIND_MODULE_CREATE|SCM_FIND_MODULE_PLACEHOLDING)); /* module gauche */
  scm__rc.d1786[30] = Scm_MakeIdentifier(scm__rc.d1786[31], SCM_MODULE(scm__rc.d1786[32]), SCM_NIL); /* gauche#sys-getenv */
  scm__rc.d1786[34] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[16])),TRUE); /* format */
  scm__rc.d1786[33] = Scm_MakeIdentifier(scm__rc.d1786[34], SCM_MODULE(scm__rc.d1786[32]), SCM_NIL); /* gauche#format */
  scm__rc.d1786[36] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[18])),TRUE); /* string-append */
  scm__rc.d1786[35] = Scm_MakeIdentifier(scm__rc.d1786[36], SCM_MODULE(scm__rc.d1786[32]), SCM_NIL); /* gauche#string-append */
  scm__rc.d1786[38] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[19])),TRUE); /* flush */
  scm__rc.d1786[37] = Scm_MakeIdentifier(scm__rc.d1786[38], SCM_MODULE(scm__rc.d1786[32]), SCM_NIL); /* gauche#flush */
  scm__rc.d1786[39] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[20])),TRUE); /* fmt */
  scm__rc.d1786[40] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[21])),TRUE); /* args */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[23]), scm__rc.d1786[39]);
  SCM_SET_CDR(SCM_OBJ(&scm__rc.d1787[23]), scm__rc.d1786[40]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[26]), scm__rc.d1786[2]);
  scm__rc.d1786[41] = Scm_MakeExtendedPair(scm__rc.d1786[29], SCM_OBJ(&scm__rc.d1787[23]), SCM_OBJ(&scm__rc.d1787[27]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[28]), scm__rc.d1786[41]);
  scm__rc.d1786[42] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[0])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[0]))->name = scm__rc.d1786[29];/* warn */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[0]))->debugInfo = scm__rc.d1786[42];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[0]))[5] = SCM_WORD(scm__rc.d1786[30]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[0]))[12] = SCM_WORD(scm__rc.d1786[33]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[0]))[20] = SCM_WORD(scm__rc.d1786[35]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[0]))[26] = SCM_WORD(scm__rc.d1786[37]);
  scm__rc.d1786[43] = Scm_MakeIdentifier(scm__rc.d1786[29], SCM_MODULE(scm__rc.d1786[32]), SCM_NIL); /* gauche#warn */
  scm__rc.d1786[44] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[22])),TRUE); /* %toplevel */
  scm__rc.d1786[45] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[1])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[1]))->name = scm__rc.d1786[44];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[1]))->debugInfo = scm__rc.d1786[45];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[28]))[3] = SCM_WORD(scm__rc.d1786[27]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[28]))[6] = SCM_WORD(scm__rc.d1786[29]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[28]))[13] = SCM_WORD(scm__rc.d1786[43]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
  scm__rc.d1786[46] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[23])),TRUE); /* %uninitialized */
  scm__rc.d1786[47] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[24])),TRUE); /* gauche.internal */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[29]), scm__rc.d1786[46]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[30]), scm__rc.d1786[47]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[31]), scm__rc.d1786[3]);
  scm__rc.d1786[48] = Scm_MakeExtendedPair(scm__rc.d1786[46], SCM_NIL, SCM_OBJ(&scm__rc.d1787[32]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[49]))[3] = scm__rc.d1786[47];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[49]))[4] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[49]))[5] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("%uninitialized")), SCM_OBJ(&libmisc_25uninitialized__STUB), 0);
  libmisc_25uninitialized__STUB.common.info = scm__rc.d1786[48];
  libmisc_25uninitialized__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[49]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[55] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[25])),TRUE); /* debug-label */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[35]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[36]), scm__rc.d1786[55]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[37]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[38]), scm__rc.d1786[3]);
  scm__rc.d1786[56] = Scm_MakeExtendedPair(scm__rc.d1786[55], SCM_OBJ(&scm__rc.d1787[14]), SCM_OBJ(&scm__rc.d1787[40]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[57]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[57]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[57]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[57]))[6] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("debug-label")), SCM_OBJ(&libmiscdebug_label__STUB), 0);
  libmiscdebug_label__STUB.common.info = scm__rc.d1786[56];
  libmiscdebug_label__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[57]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[64] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[26])),TRUE); /* foreign-pointer-invalid? */
  scm__rc.d1786[65] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[27])),TRUE); /* fp */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[41]), scm__rc.d1786[65]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[44]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[45]), scm__rc.d1786[64]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[46]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[47]), scm__rc.d1786[3]);
  scm__rc.d1786[66] = Scm_MakeExtendedPair(scm__rc.d1786[64], SCM_OBJ(&scm__rc.d1787[41]), SCM_OBJ(&scm__rc.d1787[49]));
  scm__rc.d1786[67] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[28])),TRUE); /* <foreign-pointer> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[68]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[68]))[4] = scm__rc.d1786[67];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[68]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[68]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("foreign-pointer-invalid?")), SCM_OBJ(&libmiscforeign_pointer_invalidP__STUB), 0);
  libmiscforeign_pointer_invalidP__STUB.common.info = scm__rc.d1786[66];
  libmiscforeign_pointer_invalidP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[68]);
  scm__rc.d1786[75] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[29])),TRUE); /* foreign-pointer-invalidate! */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[52]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[53]), scm__rc.d1786[75]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[54]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[55]), scm__rc.d1786[3]);
  scm__rc.d1786[76] = Scm_MakeExtendedPair(scm__rc.d1786[75], SCM_OBJ(&scm__rc.d1787[41]), SCM_OBJ(&scm__rc.d1787[57]));
  scm__rc.d1786[77] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[30])),TRUE); /* <void> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[78]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[78]))[4] = scm__rc.d1786[67];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[78]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[78]))[6] = scm__rc.d1786[77];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("foreign-pointer-invalidate!")), SCM_OBJ(&libmiscforeign_pointer_invalidateX__STUB), 0);
  libmiscforeign_pointer_invalidateX__STUB.common.info = scm__rc.d1786[76];
  libmiscforeign_pointer_invalidateX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[78]);
  scm__rc.d1786[85] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[31])),TRUE); /* foreign-pointer-attributes */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[60]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[61]), scm__rc.d1786[85]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[62]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[63]), scm__rc.d1786[3]);
  scm__rc.d1786[86] = Scm_MakeExtendedPair(scm__rc.d1786[85], SCM_OBJ(&scm__rc.d1787[41]), SCM_OBJ(&scm__rc.d1787[65]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[87]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[87]))[4] = scm__rc.d1786[67];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[87]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[87]))[6] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("foreign-pointer-attributes")), SCM_OBJ(&libmiscforeign_pointer_attributes__STUB), 0);
  libmiscforeign_pointer_attributes__STUB.common.info = scm__rc.d1786[86];
  libmiscforeign_pointer_attributes__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[87]);
  scm__rc.d1786[94] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[32])),TRUE); /* foreign-pointer-attribute-get */
  scm__rc.d1786[95] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[33])),TRUE); /* key */
  scm__rc.d1786[96] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[34]))); /* :optional */
  scm__rc.d1786[97] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[35])),TRUE); /* fallback */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[66]), scm__rc.d1786[97]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[67]), scm__rc.d1786[96]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[68]), scm__rc.d1786[95]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[69]), scm__rc.d1786[65]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[72]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[73]), scm__rc.d1786[94]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[74]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[75]), scm__rc.d1786[3]);
  scm__rc.d1786[98] = Scm_MakeExtendedPair(scm__rc.d1786[94], SCM_OBJ(&scm__rc.d1787[69]), SCM_OBJ(&scm__rc.d1787[77]));
  scm__rc.d1786[99] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[36])),TRUE); /* * */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[100]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[100]))[4] = scm__rc.d1786[67];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[100]))[5] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[100]))[6] = scm__rc.d1786[99];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[100]))[7] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[100]))[8] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("foreign-pointer-attribute-get")), SCM_OBJ(&libmiscforeign_pointer_attribute_get__STUB), 0);
  libmiscforeign_pointer_attribute_get__STUB.common.info = scm__rc.d1786[98];
  libmiscforeign_pointer_attribute_get__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[100]);
  scm__rc.d1786[109] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[37])),TRUE); /* foreign-pointer-attribute-set! */
  scm__rc.d1786[110] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[38])),TRUE); /* value */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[78]), scm__rc.d1786[110]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[79]), scm__rc.d1786[95]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[80]), scm__rc.d1786[65]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[83]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[84]), scm__rc.d1786[109]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[85]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[86]), scm__rc.d1786[3]);
  scm__rc.d1786[111] = Scm_MakeExtendedPair(scm__rc.d1786[109], SCM_OBJ(&scm__rc.d1787[80]), SCM_OBJ(&scm__rc.d1787[88]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[112]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[112]))[4] = scm__rc.d1786[67];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[112]))[5] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[112]))[6] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[112]))[7] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[112]))[8] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("foreign-pointer-attribute-set!")), SCM_OBJ(&libmiscforeign_pointer_attribute_setX__STUB), 0);
  libmiscforeign_pointer_attribute_setX__STUB.common.info = scm__rc.d1786[111];
  libmiscforeign_pointer_attribute_setX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[112]);
  scm__rc.d1786[121] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[39])),TRUE); /* foreign-pointer-attribute-set */
  scm__rc.d1786[122] = Scm_MakeIdentifier(scm__rc.d1786[109], SCM_MODULE(scm__rc.d1786[32]), SCM_NIL); /* gauche#foreign-pointer-attribute-set! */
  scm__rc.d1786[123] = Scm_MakeIdentifier(scm__rc.d1786[121], SCM_MODULE(scm__rc.d1786[32]), SCM_NIL); /* gauche#foreign-pointer-attribute-set */
  scm__rc.d1786[124] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[2])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[2]))->name = scm__rc.d1786[44];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[2]))->debugInfo = scm__rc.d1786[124];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[43]))[3] = SCM_WORD(scm__rc.d1786[27]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[43]))[6] = SCM_WORD(scm__rc.d1786[121]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[43]))[10] = SCM_WORD(scm__rc.d1786[122]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[43]))[13] = SCM_WORD(scm__rc.d1786[123]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
  scm__rc.d1786[125] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[40])),TRUE); /* cond-features */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[89]), scm__rc.d1786[125]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[90]), scm__rc.d1786[47]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[91]), scm__rc.d1786[3]);
  scm__rc.d1786[126] = Scm_MakeExtendedPair(scm__rc.d1786[125], SCM_NIL, SCM_OBJ(&scm__rc.d1787[92]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("cond-features")), SCM_OBJ(&libmisccond_features__STUB), 0);
  libmisccond_features__STUB.common.info = scm__rc.d1786[126];
  libmisccond_features__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[49]);
  scm__rc.d1786[127] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[41])),TRUE); /* SLIB_DIR */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(scm__rc.d1786[127]), 
#line 95 "libmisc.scm"
SCM_MAKE_STR_IMMUTABLE(SLIB_DIR), SCM_BINDING_CONST);

  Scm_VMExecuteToplevels(toplevels);
  scm__rc.d1786[142] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[42])),TRUE); /* current-error-port */
}
