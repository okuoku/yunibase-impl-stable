/* Generated from autoloads.scm.  DO NOT EDIT */
#define LIBGAUCHE_BODY
#include <gauche.h>
#include <gauche/priv/configP.h>
#if defined(__CYGWIN__) || defined(GAUCHE_WINDOWS)
#define SCM_CGEN_CONST /*empty*/
#else
#define SCM_CGEN_CONST const
#endif
static SCM_CGEN_CONST struct scm__scRec {
  ScmString d1067[224];
} scm__sc SCM_UNUSED = {
  {   /* ScmString d1067 */
      SCM_STRING_CONST_INITIALIZER("gauche/numutil", 14, 14),
      SCM_STRING_CONST_INITIALIZER("nearly=\077", 8, 8),
      SCM_STRING_CONST_INITIALIZER("approx=\077", 8, 8),
      SCM_STRING_CONST_INITIALIZER("encode-float", 12, 12),
      SCM_STRING_CONST_INITIALIZER("square", 6, 6),
      SCM_STRING_CONST_INITIALIZER("truncate-remainder", 18, 18),
      SCM_STRING_CONST_INITIALIZER("truncate-quotient", 17, 17),
      SCM_STRING_CONST_INITIALIZER("truncate/", 9, 9),
      SCM_STRING_CONST_INITIALIZER("floor-remainder", 15, 15),
      SCM_STRING_CONST_INITIALIZER("floor-quotient", 14, 14),
      SCM_STRING_CONST_INITIALIZER("floor/", 6, 6),
      SCM_STRING_CONST_INITIALIZER("mod0", 4, 4),
      SCM_STRING_CONST_INITIALIZER("div0", 4, 4),
      SCM_STRING_CONST_INITIALIZER("div0-and-mod0", 13, 13),
      SCM_STRING_CONST_INITIALIZER("mod", 3, 3),
      SCM_STRING_CONST_INITIALIZER("div", 3, 3),
      SCM_STRING_CONST_INITIALIZER("div-and-mod", 11, 11),
      SCM_STRING_CONST_INITIALIZER("integer-valued\077", 15, 15),
      SCM_STRING_CONST_INITIALIZER("rational-valued\077", 16, 16),
      SCM_STRING_CONST_INITIALIZER("real-valued\077", 12, 12),
      SCM_STRING_CONST_INITIALIZER("lgamma", 6, 6),
      SCM_STRING_CONST_INITIALIZER("gamma", 5, 5),
      SCM_STRING_CONST_INITIALIZER("expt-mod", 8, 8),
      SCM_STRING_CONST_INITIALIZER("real->rational", 14, 14),
      SCM_STRING_CONST_INITIALIZER("continued-fraction", 18, 18),
      SCM_STRING_CONST_INITIALIZER("exact-integer-sqrt", 18, 18),
      SCM_STRING_CONST_INITIALIZER("gauche/redefutil", 16, 16),
      SCM_STRING_CONST_INITIALIZER("change-object-class", 19, 19),
      SCM_STRING_CONST_INITIALIZER("update-direct-subclass!", 23, 23),
      SCM_STRING_CONST_INITIALIZER("class-redefinition", 18, 18),
      SCM_STRING_CONST_INITIALIZER("redefine-class!", 15, 15),
      SCM_STRING_CONST_INITIALIZER("gauche.charconv", 15, 15),
      SCM_STRING_CONST_INITIALIZER("%open-output-file/conv", 22, 22),
      SCM_STRING_CONST_INITIALIZER("%open-input-file/conv", 21, 21),
      SCM_STRING_CONST_INITIALIZER("gauche/sigutil", 14, 14),
      SCM_STRING_CONST_INITIALIZER("with-signal-handlers", 20, 20),
      SCM_STRING_CONST_INITIALIZER("gauche.modutil", 14, 14),
      SCM_STRING_CONST_INITIALIZER("use-version", 11, 11),
      SCM_STRING_CONST_INITIALIZER("export-if-defined", 17, 17),
      SCM_STRING_CONST_INITIALIZER("gauche.portutil", 15, 15),
      SCM_STRING_CONST_INITIALIZER("copy-port", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche/logutil", 14, 14),
      SCM_STRING_CONST_INITIALIZER("copy-bit-field", 14, 14),
      SCM_STRING_CONST_INITIALIZER("bit-field", 9, 9),
      SCM_STRING_CONST_INITIALIZER("copy-bit", 8, 8),
      SCM_STRING_CONST_INITIALIZER("logbit\077", 7, 7),
      SCM_STRING_CONST_INITIALIZER("logtest", 7, 7),
      SCM_STRING_CONST_INITIALIZER("gauche.common-macros", 20, 20),
      SCM_STRING_CONST_INITIALIZER("until", 5, 5),
      SCM_STRING_CONST_INITIALIZER("while", 5, 5),
      SCM_STRING_CONST_INITIALIZER("fluid-let", 9, 9),
      SCM_STRING_CONST_INITIALIZER("get-keyword*", 12, 12),
      SCM_STRING_CONST_INITIALIZER("get-optional", 12, 12),
      SCM_STRING_CONST_INITIALIZER("gauche.regexp", 13, 13),
      SCM_STRING_CONST_INITIALIZER("<regexp-invalid-ast>", 20, 20),
      SCM_STRING_CONST_INITIALIZER("rxmatch-positions", 17, 17),
      SCM_STRING_CONST_INITIALIZER("rxmatch-substrings", 18, 18),
      SCM_STRING_CONST_INITIALIZER("regexp-unparse", 14, 14),
      SCM_STRING_CONST_INITIALIZER("gauche.regexp.sre", 17, 17),
      SCM_STRING_CONST_INITIALIZER("regexp->sre", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sre->regexp", 11, 11),
      SCM_STRING_CONST_INITIALIZER("regexp-parse-sre", 16, 16),
      SCM_STRING_CONST_INITIALIZER("gauche.procutil", 15, 15),
      SCM_STRING_CONST_INITIALIZER("port-map", 8, 8),
      SCM_STRING_CONST_INITIALIZER("port-for-each", 13, 13),
      SCM_STRING_CONST_INITIALIZER("port-fold-right", 15, 15),
      SCM_STRING_CONST_INITIALIZER("port-fold", 9, 9),
      SCM_STRING_CONST_INITIALIZER("disasm", 6, 6),
      SCM_STRING_CONST_INITIALIZER("source-location", 15, 15),
      SCM_STRING_CONST_INITIALIZER("source-code", 11, 11),
      SCM_STRING_CONST_INITIALIZER("arity-at-least-value", 20, 20),
      SCM_STRING_CONST_INITIALIZER("arity-at-least\077", 15, 15),
      SCM_STRING_CONST_INITIALIZER("<arity-at-least>", 16, 16),
      SCM_STRING_CONST_INITIALIZER("procedure-arity-includes\077", 25, 25),
      SCM_STRING_CONST_INITIALIZER("arity", 5, 5),
      SCM_STRING_CONST_INITIALIZER("every-pred", 10, 10),
      SCM_STRING_CONST_INITIALIZER("any-pred", 8, 8),
      SCM_STRING_CONST_INITIALIZER("assoc$", 6, 6),
      SCM_STRING_CONST_INITIALIZER("member$", 7, 7),
      SCM_STRING_CONST_INITIALIZER("delete$", 7, 7),
      SCM_STRING_CONST_INITIALIZER("every$", 6, 6),
      SCM_STRING_CONST_INITIALIZER("any$", 4, 4),
      SCM_STRING_CONST_INITIALIZER("find-tail$", 10, 10),
      SCM_STRING_CONST_INITIALIZER("find$", 5, 5),
      SCM_STRING_CONST_INITIALIZER("remove$", 7, 7),
      SCM_STRING_CONST_INITIALIZER("partition$", 10, 10),
      SCM_STRING_CONST_INITIALIZER("filter$", 7, 7),
      SCM_STRING_CONST_INITIALIZER("reduce-right$", 13, 13),
      SCM_STRING_CONST_INITIALIZER("reduce$", 7, 7),
      SCM_STRING_CONST_INITIALIZER("fold-right$", 11, 11),
      SCM_STRING_CONST_INITIALIZER("fold$", 5, 5),
      SCM_STRING_CONST_INITIALIZER("count$", 6, 6),
      SCM_STRING_CONST_INITIALIZER("apply$", 6, 6),
      SCM_STRING_CONST_INITIALIZER("for-each$", 9, 9),
      SCM_STRING_CONST_INITIALIZER("map$", 4, 4),
      SCM_STRING_CONST_INITIALIZER("pa$", 3, 3),
      SCM_STRING_CONST_INITIALIZER("swap", 4, 4),
      SCM_STRING_CONST_INITIALIZER("flip", 4, 4),
      SCM_STRING_CONST_INITIALIZER("complement", 10, 10),
      SCM_STRING_CONST_INITIALIZER(".$", 2, 2),
      SCM_STRING_CONST_INITIALIZER("compose", 7, 7),
      SCM_STRING_CONST_INITIALIZER("gauche.time", 11, 11),
      SCM_STRING_CONST_INITIALIZER("time", 4, 4),
      SCM_STRING_CONST_INITIALIZER("gauche.vm.debugger", 18, 18),
      SCM_STRING_CONST_INITIALIZER("debug-thread-post", 17, 17),
      SCM_STRING_CONST_INITIALIZER("debug-thread-pre", 16, 16),
      SCM_STRING_CONST_INITIALIZER("debug-funcall-pre", 17, 17),
      SCM_STRING_CONST_INITIALIZER("debug-print-post", 16, 16),
      SCM_STRING_CONST_INITIALIZER("debug-print-pre", 15, 15),
      SCM_STRING_CONST_INITIALIZER("debug-print-width", 17, 17),
      SCM_STRING_CONST_INITIALIZER("debug-thread-log", 16, 16),
      SCM_STRING_CONST_INITIALIZER("debug-funcall-conditionally", 27, 27),
      SCM_STRING_CONST_INITIALIZER("debug-funcall", 13, 13),
      SCM_STRING_CONST_INITIALIZER("debug-print-conditionally", 25, 25),
      SCM_STRING_CONST_INITIALIZER("debug-print", 11, 11),
      SCM_STRING_CONST_INITIALIZER("gauche.vm.profiler", 18, 18),
      SCM_STRING_CONST_INITIALIZER("with-profiler", 13, 13),
      SCM_STRING_CONST_INITIALIZER("profiler-show-load-stats", 24, 24),
      SCM_STRING_CONST_INITIALIZER("profiler-show", 13, 13),
      SCM_STRING_CONST_INITIALIZER("gauche.vm.debug-info", 20, 20),
      SCM_STRING_CONST_INITIALIZER("decode-debug-info", 17, 17),
      SCM_STRING_CONST_INITIALIZER("srfi.7", 6, 6),
      SCM_STRING_CONST_INITIALIZER("program", 7, 7),
      SCM_STRING_CONST_INITIALIZER("srfi.55", 7, 7),
      SCM_STRING_CONST_INITIALIZER("require-extension", 17, 17),
      SCM_STRING_CONST_INITIALIZER("gauche.interpolate", 18, 18),
      SCM_STRING_CONST_INITIALIZER("string-interpolate*", 19, 19),
      SCM_STRING_CONST_INITIALIZER("string-interpolate", 18, 18),
      SCM_STRING_CONST_INITIALIZER("gauche/sysutil", 14, 14),
      SCM_STRING_CONST_INITIALIZER("sys-fdset->list", 15, 15),
      SCM_STRING_CONST_INITIALIZER("list->sys-fdset", 15, 15),
      SCM_STRING_CONST_INITIALIZER("sys-fdset", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-realpath", 12, 12),
      SCM_STRING_CONST_INITIALIZER("gauche.vecutil", 14, 14),
      SCM_STRING_CONST_INITIALIZER("reverse-list->vector", 20, 20),
      SCM_STRING_CONST_INITIALIZER("vector-for-each-with-index", 26, 26),
      SCM_STRING_CONST_INITIALIZER("vector-map-with-index!", 22, 22),
      SCM_STRING_CONST_INITIALIZER("vector-map-with-index", 21, 21),
      SCM_STRING_CONST_INITIALIZER("vector-for-each", 15, 15),
      SCM_STRING_CONST_INITIALIZER("vector-map!", 11, 11),
      SCM_STRING_CONST_INITIALIZER("vector-map", 10, 10),
      SCM_STRING_CONST_INITIALIZER("vector-tabulate", 15, 15),
      SCM_STRING_CONST_INITIALIZER("gauche.computil", 15, 15),
      SCM_STRING_CONST_INITIALIZER("comparator-if<=>", 16, 16),
      SCM_STRING_CONST_INITIALIZER("make-vector-comparator", 22, 22),
      SCM_STRING_CONST_INITIALIZER("make-list-comparator", 20, 20),
      SCM_STRING_CONST_INITIALIZER("make-pair-comparator", 20, 20),
      SCM_STRING_CONST_INITIALIZER("make-tuple-comparator", 21, 21),
      SCM_STRING_CONST_INITIALIZER("make-key-comparator", 19, 19),
      SCM_STRING_CONST_INITIALIZER("make-reverse-comparator", 23, 23),
      SCM_STRING_CONST_INITIALIZER("make-eqv-comparator", 19, 19),
      SCM_STRING_CONST_INITIALIZER("make-eq-comparator", 18, 18),
      SCM_STRING_CONST_INITIALIZER("uvector-comparator", 18, 18),
      SCM_STRING_CONST_INITIALIZER("bytevector-comparator", 21, 21),
      SCM_STRING_CONST_INITIALIZER("vector-comparator", 17, 17),
      SCM_STRING_CONST_INITIALIZER("list-comparator", 15, 15),
      SCM_STRING_CONST_INITIALIZER("pair-comparator", 15, 15),
      SCM_STRING_CONST_INITIALIZER("number-comparator", 17, 17),
      SCM_STRING_CONST_INITIALIZER("complex-comparator", 18, 18),
      SCM_STRING_CONST_INITIALIZER("real-comparator", 15, 15),
      SCM_STRING_CONST_INITIALIZER("rational-comparator", 19, 19),
      SCM_STRING_CONST_INITIALIZER("integer-comparator", 18, 18),
      SCM_STRING_CONST_INITIALIZER("exact-integer-comparator", 24, 24),
      SCM_STRING_CONST_INITIALIZER("symbol-comparator", 17, 17),
      SCM_STRING_CONST_INITIALIZER("string-ci-comparator", 20, 20),
      SCM_STRING_CONST_INITIALIZER("char-ci-comparator", 18, 18),
      SCM_STRING_CONST_INITIALIZER("char-comparator", 15, 15),
      SCM_STRING_CONST_INITIALIZER("boolean-comparator", 18, 18),
      SCM_STRING_CONST_INITIALIZER("gauche.fileutil", 15, 15),
      SCM_STRING_CONST_INITIALIZER("sys-tm->alist", 13, 13),
      SCM_STRING_CONST_INITIALIZER("sys-stat->type", 14, 14),
      SCM_STRING_CONST_INITIALIZER("sys-stat->ctime", 15, 15),
      SCM_STRING_CONST_INITIALIZER("sys-stat->mtime", 15, 15),
      SCM_STRING_CONST_INITIALIZER("sys-stat->atime", 15, 15),
      SCM_STRING_CONST_INITIALIZER("sys-stat->gid", 13, 13),
      SCM_STRING_CONST_INITIALIZER("sys-stat->uid", 13, 13),
      SCM_STRING_CONST_INITIALIZER("sys-stat->size", 14, 14),
      SCM_STRING_CONST_INITIALIZER("sys-stat->nlink", 15, 15),
      SCM_STRING_CONST_INITIALIZER("sys-stat->rdev", 14, 14),
      SCM_STRING_CONST_INITIALIZER("sys-stat->dev", 13, 13),
      SCM_STRING_CONST_INITIALIZER("sys-stat->ino", 13, 13),
      SCM_STRING_CONST_INITIALIZER("sys-stat->mode", 14, 14),
      SCM_STRING_CONST_INITIALIZER("sys-stat->file-type", 19, 19),
      SCM_STRING_CONST_INITIALIZER("gauche.fmtutil", 14, 14),
      SCM_STRING_CONST_INITIALIZER("format-numeral-R", 16, 16),
      SCM_STRING_CONST_INITIALIZER("gauche.hashutil", 15, 15),
      SCM_STRING_CONST_INITIALIZER("string-ci-hash", 14, 14),
      SCM_STRING_CONST_INITIALIZER("gauche.treeutil", 15, 15),
      SCM_STRING_CONST_INITIALIZER("tree-map-compare-as-sequences", 29, 29),
      SCM_STRING_CONST_INITIALIZER("tree-map-compare-as-sets", 24, 24),
      SCM_STRING_CONST_INITIALIZER("tree-map->generator/key-range", 29, 29),
      SCM_STRING_CONST_INITIALIZER("alist->tree-map", 15, 15),
      SCM_STRING_CONST_INITIALIZER("tree-map->alist", 15, 15),
      SCM_STRING_CONST_INITIALIZER("tree-map-values", 15, 15),
      SCM_STRING_CONST_INITIALIZER("tree-map-keys", 13, 13),
      SCM_STRING_CONST_INITIALIZER("tree-map-for-each", 17, 17),
      SCM_STRING_CONST_INITIALIZER("tree-map-map", 12, 12),
      SCM_STRING_CONST_INITIALIZER("tree-map-fold-right", 19, 19),
      SCM_STRING_CONST_INITIALIZER("tree-map-fold", 13, 13),
      SCM_STRING_CONST_INITIALIZER("tree-map-seek", 13, 13),
      SCM_STRING_CONST_INITIALIZER("tree-map-pop-max!", 17, 17),
      SCM_STRING_CONST_INITIALIZER("tree-map-pop-min!", 17, 17),
      SCM_STRING_CONST_INITIALIZER("tree-map-max", 12, 12),
      SCM_STRING_CONST_INITIALIZER("tree-map-min", 12, 12),
      SCM_STRING_CONST_INITIALIZER("tree-map-empty\077", 15, 15),
      SCM_STRING_CONST_INITIALIZER("make-tree-map", 13, 13),
      SCM_STRING_CONST_INITIALIZER("gauche.libutil", 14, 14),
      SCM_STRING_CONST_INITIALIZER("library-name->module-name", 25, 25),
      SCM_STRING_CONST_INITIALIZER("library-has-module\077", 19, 19),
      SCM_STRING_CONST_INITIALIZER("library-exists\077", 15, 15),
      SCM_STRING_CONST_INITIALIZER("library-for-each", 16, 16),
      SCM_STRING_CONST_INITIALIZER("library-map", 11, 11),
      SCM_STRING_CONST_INITIALIZER("library-fold", 12, 12),
      SCM_STRING_CONST_INITIALIZER("gauche.generic-sortutil", 23, 23),
      SCM_STRING_CONST_INITIALIZER("%generic-sort!", 14, 14),
      SCM_STRING_CONST_INITIALIZER("%generic-sort", 13, 13),
      SCM_STRING_CONST_INITIALIZER("%generic-sorted\077", 16, 16),
      SCM_STRING_CONST_INITIALIZER("gauche.pputil", 13, 13),
      SCM_STRING_CONST_INITIALIZER("pprint", 6, 6),
      SCM_STRING_CONST_INITIALIZER("%pretty-print", 13, 13),
      SCM_STRING_CONST_INITIALIZER("gauche.version-alist", 20, 20),
      SCM_STRING_CONST_INITIALIZER("version-alist", 13, 13),
      SCM_STRING_CONST_INITIALIZER("r7rs-setup", 10, 10),
      SCM_STRING_CONST_INITIALIZER("define-library", 14, 14),
  },
};
static struct scm__rcRec {
  ScmObj d1068[25];
} scm__rc SCM_UNUSED = {
  {   /* ScmObj d1068 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
  },
};
void Scm__InitAutoloads(void)
{
  ScmModule *gauche = Scm_GaucheModule();
  ScmSymbol *sym, *import_from;
  ScmObj al, path;
  path = SCM_OBJ(&scm__sc.d1067[0]);
  import_from = NULL;
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[1])))); /* nearly=? */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[2])))); /* approx=? */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[3])))); /* encode-float */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[4])))); /* square */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[5])))); /* truncate-remainder */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[6])))); /* truncate-quotient */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[7])))); /* truncate/ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[8])))); /* floor-remainder */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[9])))); /* floor-quotient */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[10])))); /* floor/ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[11])))); /* mod0 */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[12])))); /* div0 */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[13])))); /* div0-and-mod0 */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[14])))); /* mod */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[15])))); /* div */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[16])))); /* div-and-mod */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[17])))); /* integer-valued? */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[18])))); /* rational-valued? */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[19])))); /* real-valued? */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[20])))); /* lgamma */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[21])))); /* gamma */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[22])))); /* expt-mod */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[23])))); /* real->rational */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[24])))); /* continued-fraction */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[25])))); /* exact-integer-sqrt */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  path = SCM_OBJ(&scm__sc.d1067[26]);
  import_from = NULL;
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[27])))); /* change-object-class */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[28])))); /* update-direct-subclass! */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[29])))); /* class-redefinition */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[30])))); /* redefine-class! */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[31])),TRUE); /* gauche.charconv */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[0]));
  import_from = SCM_SYMBOL(scm__rc.d1068[0]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[32])))); /* %open-output-file/conv */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[33])))); /* %open-input-file/conv */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  path = SCM_OBJ(&scm__sc.d1067[34]);
  import_from = NULL;
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[35])))); /* with-signal-handlers */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  scm__rc.d1068[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[36])),TRUE); /* gauche.modutil */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[1]));
  import_from = SCM_SYMBOL(scm__rc.d1068[1]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[37])))); /* use-version */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[38])))); /* export-if-defined */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  scm__rc.d1068[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[39])),TRUE); /* gauche.portutil */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[2]));
  import_from = SCM_SYMBOL(scm__rc.d1068[2]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[40])))); /* copy-port */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  path = SCM_OBJ(&scm__sc.d1067[41]);
  import_from = NULL;
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[42])))); /* copy-bit-field */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[43])))); /* bit-field */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[44])))); /* copy-bit */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[45])))); /* logbit? */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[46])))); /* logtest */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[47])),TRUE); /* gauche.common-macros */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[3]));
  import_from = SCM_SYMBOL(scm__rc.d1068[3]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[48])))); /* until */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[49])))); /* while */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[50])))); /* fluid-let */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[51])))); /* get-keyword* */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[52])))); /* get-optional */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  scm__rc.d1068[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[53])),TRUE); /* gauche.regexp */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[4]));
  import_from = SCM_SYMBOL(scm__rc.d1068[4]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[54])))); /* <regexp-invalid-ast> */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[55])))); /* rxmatch-positions */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[56])))); /* rxmatch-substrings */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[57])))); /* regexp-unparse */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[5] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[58])),TRUE); /* gauche.regexp.sre */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[5]));
  import_from = SCM_SYMBOL(scm__rc.d1068[5]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[59])))); /* regexp->sre */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[60])))); /* sre->regexp */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[61])))); /* regexp-parse-sre */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[62])),TRUE); /* gauche.procutil */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[6]));
  import_from = SCM_SYMBOL(scm__rc.d1068[6]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[63])))); /* port-map */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[64])))); /* port-for-each */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[65])))); /* port-fold-right */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[66])))); /* port-fold */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[67])))); /* disasm */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[68])))); /* source-location */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[69])))); /* source-code */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[70])))); /* arity-at-least-value */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[71])))); /* arity-at-least? */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[72])))); /* <arity-at-least> */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[73])))); /* procedure-arity-includes? */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[74])))); /* arity */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[75])))); /* every-pred */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[76])))); /* any-pred */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[77])))); /* assoc$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[78])))); /* member$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[79])))); /* delete$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[80])))); /* every$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[81])))); /* any$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[82])))); /* find-tail$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[83])))); /* find$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[84])))); /* remove$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[85])))); /* partition$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[86])))); /* filter$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[87])))); /* reduce-right$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[88])))); /* reduce$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[89])))); /* fold-right$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[90])))); /* fold$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[91])))); /* count$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[92])))); /* apply$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[93])))); /* for-each$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[94])))); /* map$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[95])))); /* pa$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[96])))); /* swap */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[97])))); /* flip */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[98])))); /* complement */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[99])))); /* .$ */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[100])))); /* compose */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[101])),TRUE); /* gauche.time */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[7]));
  import_from = SCM_SYMBOL(scm__rc.d1068[7]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[102])))); /* time */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  scm__rc.d1068[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[103])),TRUE); /* gauche.vm.debugger */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[8]));
  import_from = SCM_SYMBOL(scm__rc.d1068[8]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[104])))); /* debug-thread-post */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[105])))); /* debug-thread-pre */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[106])))); /* debug-funcall-pre */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[107])))); /* debug-print-post */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[108])))); /* debug-print-pre */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[109])))); /* debug-print-width */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[110])))); /* debug-thread-log */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[111])))); /* debug-funcall-conditionally */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[112])))); /* debug-funcall */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[113])))); /* debug-print-conditionally */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[114])))); /* debug-print */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  scm__rc.d1068[9] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[115])),TRUE); /* gauche.vm.profiler */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[9]));
  import_from = SCM_SYMBOL(scm__rc.d1068[9]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[116])))); /* with-profiler */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[117])))); /* profiler-show-load-stats */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[118])))); /* profiler-show */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[10] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[119])),TRUE); /* gauche.vm.debug-info */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[10]));
  import_from = SCM_SYMBOL(scm__rc.d1068[10]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[120])))); /* decode-debug-info */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[11] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[121])),TRUE); /* srfi.7 */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[11]));
  import_from = SCM_SYMBOL(scm__rc.d1068[11]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[122])))); /* program */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  scm__rc.d1068[12] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[123])),TRUE); /* srfi.55 */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[12]));
  import_from = SCM_SYMBOL(scm__rc.d1068[12]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[124])))); /* require-extension */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  scm__rc.d1068[13] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[125])),TRUE); /* gauche.interpolate */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[13]));
  import_from = SCM_SYMBOL(scm__rc.d1068[13]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[126])))); /* string-interpolate* */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[127])))); /* string-interpolate */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  path = SCM_OBJ(&scm__sc.d1067[128]);
  import_from = NULL;
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[129])))); /* sys-fdset->list */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[130])))); /* list->sys-fdset */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[131])))); /* sys-fdset */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[132])))); /* sys-realpath */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[14] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[133])),TRUE); /* gauche.vecutil */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[14]));
  import_from = SCM_SYMBOL(scm__rc.d1068[14]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[134])))); /* reverse-list->vector */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[135])))); /* vector-for-each-with-index */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[136])))); /* vector-map-with-index! */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[137])))); /* vector-map-with-index */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[138])))); /* vector-for-each */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[139])))); /* vector-map! */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[140])))); /* vector-map */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[141])))); /* vector-tabulate */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[15] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[142])),TRUE); /* gauche.computil */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[15]));
  import_from = SCM_SYMBOL(scm__rc.d1068[15]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[143])))); /* comparator-if<=> */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[144])))); /* make-vector-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[145])))); /* make-list-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[146])))); /* make-pair-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[147])))); /* make-tuple-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[148])))); /* make-key-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[149])))); /* make-reverse-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[150])))); /* make-eqv-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[151])))); /* make-eq-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[152])))); /* uvector-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[153])))); /* bytevector-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[154])))); /* vector-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[155])))); /* list-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[156])))); /* pair-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[157])))); /* number-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[158])))); /* complex-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[159])))); /* real-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[160])))); /* rational-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[161])))); /* integer-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[162])))); /* exact-integer-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[163])))); /* symbol-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[164])))); /* string-ci-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[165])))); /* char-ci-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[166])))); /* char-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[167])))); /* boolean-comparator */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[16] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[168])),TRUE); /* gauche.fileutil */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[16]));
  import_from = SCM_SYMBOL(scm__rc.d1068[16]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[169])))); /* sys-tm->alist */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[170])))); /* sys-stat->type */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[171])))); /* sys-stat->ctime */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[172])))); /* sys-stat->mtime */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[173])))); /* sys-stat->atime */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[174])))); /* sys-stat->gid */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[175])))); /* sys-stat->uid */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[176])))); /* sys-stat->size */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[177])))); /* sys-stat->nlink */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[178])))); /* sys-stat->rdev */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[179])))); /* sys-stat->dev */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[180])))); /* sys-stat->ino */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[181])))); /* sys-stat->mode */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[182])))); /* sys-stat->file-type */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[17] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[183])),TRUE); /* gauche.fmtutil */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[17]));
  import_from = SCM_SYMBOL(scm__rc.d1068[17]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[184])))); /* format-numeral-R */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[18] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[185])),TRUE); /* gauche.hashutil */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[18]));
  import_from = SCM_SYMBOL(scm__rc.d1068[18]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[186])))); /* string-ci-hash */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[19] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[187])),TRUE); /* gauche.treeutil */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[19]));
  import_from = SCM_SYMBOL(scm__rc.d1068[19]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[188])))); /* tree-map-compare-as-sequences */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[189])))); /* tree-map-compare-as-sets */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[190])))); /* tree-map->generator/key-range */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[191])))); /* alist->tree-map */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[192])))); /* tree-map->alist */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[193])))); /* tree-map-values */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[194])))); /* tree-map-keys */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[195])))); /* tree-map-for-each */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[196])))); /* tree-map-map */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[197])))); /* tree-map-fold-right */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[198])))); /* tree-map-fold */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[199])))); /* tree-map-seek */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[200])))); /* tree-map-pop-max! */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[201])))); /* tree-map-pop-min! */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[202])))); /* tree-map-max */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[203])))); /* tree-map-min */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[204])))); /* tree-map-empty? */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[205])))); /* make-tree-map */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[20] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[206])),TRUE); /* gauche.libutil */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[20]));
  import_from = SCM_SYMBOL(scm__rc.d1068[20]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[207])))); /* library-name->module-name */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[208])))); /* library-has-module? */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[209])))); /* library-exists? */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[210])))); /* library-for-each */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[211])))); /* library-map */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[212])))); /* library-fold */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[21] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[213])),TRUE); /* gauche.generic-sortutil */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[21]));
  import_from = SCM_SYMBOL(scm__rc.d1068[21]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[214])))); /* %generic-sort! */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[215])))); /* %generic-sort */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[216])))); /* %generic-sorted? */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[22] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[217])),TRUE); /* gauche.pputil */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[22]));
  import_from = SCM_SYMBOL(scm__rc.d1068[22]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[218])))); /* pprint */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[219])))); /* %pretty-print */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[23] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[220])),TRUE); /* gauche.version-alist */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[23]));
  import_from = SCM_SYMBOL(scm__rc.d1068[23]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[221])))); /* version-alist */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, al);
  scm__rc.d1068[24] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1067[222])),TRUE); /* r7rs-setup */
  path = Scm_ModuleNameToPath(SCM_SYMBOL(scm__rc.d1068[24]));
  import_from = SCM_SYMBOL(scm__rc.d1068[24]);
  sym = SCM_SYMBOL(Scm_Intern(SCM_STRING(SCM_OBJ(&scm__sc.d1067[223])))); /* define-library */
  al = Scm_MakeAutoload(SCM_CURRENT_MODULE(), sym, SCM_STRING(path), import_from);
  Scm_Define(gauche, sym, Scm_MakeMacroAutoload(sym, SCM_AUTOLOAD(al)));
}
