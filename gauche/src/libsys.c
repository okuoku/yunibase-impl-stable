/* Generated automatically from libsys.scm.  DO NOT EDIT */
#define LIBGAUCHE_BODY 
#include <gauche.h>
#include <gauche/code.h>
#include "gauche/priv/configP.h"
#include "gauche/priv/mmapP.h"
#include <stdlib.h>
#include <locale.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#if !(defined(GAUCHE_WINDOWS))
#include <grp.h>
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#include <pwd.h>
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#include <sys/wait.h>
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#include <utime.h>
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#include <sys/times.h>
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#include <sys/utsname.h>
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if HAVE_CRYPT_H
#include <crypt.h>
#endif /* HAVE_CRYPT_H */
#if HAVE_SYS_RESOURCE_H
#include <sys/resource.h>
#endif /* HAVE_SYS_RESOURCE_H */
#if HAVE_SYS_LOADAVG_H
#include <sys/loadavg.h>
#endif /* HAVE_SYS_LOADAVG_H */
#if HAVE_UNISTD_H
#include <unistd.h>
#endif /* HAVE_UNISTD_H */
#if HAVE_SYS_MMAN_H
#include <sys/mman.h>
#endif /* HAVE_SYS_MMAN_H */
#if defined(GAUCHE_WINDOWS)

#endif /* defined(GAUCHE_WINDOWS) */
static ScmObj libsyssys_readdir(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_readdir__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_readdir, NULL, NULL);

static ScmObj libsyssys_tmpdir(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_tmpdir__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_tmpdir, NULL, NULL);

static ScmObj libsyssys_basename(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_basename__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_basename, NULL, NULL);

static ScmObj libsyssys_dirname(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_dirname__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_dirname, NULL, NULL);

static unsigned char uvector__00001[] = {
 0u, 3u, 199u, 144u, 24u, 32u, 0u, 249u, 0u, 152u, 10u, 3u, 129u, 9u,
30u, 60u, 112u, 192u, 144u, 33u, 30u, 60u, 96u, 192u, 144u, 25u, 30u,
60u, 80u, 192u, 144u, 17u, 30u, 60u, 64u, 192u, 152u, 23u, 145u, 227u,
195u, 12u, 9u, 0u, 145u, 227u, 193u, 12u, 16u, 44u, 12u, 26u, 32u,
18u, 72u, 241u, 224u, 6u, 4u, 198u, 72u, 143u, 28u, 248u, 96u, 72u,
4u, 143u, 28u, 216u, 96u, 76u, 94u, 200u, 241u, 204u, 6u, 8u, 0u, 62u,
64u, 38u, 2u, 128u, 195u, 188u, 2u, 73u, 30u, 57u, 112u, 192u, 153u,
78u, 145u, 227u, 150u, 12u, 9u, 0u, 145u, 227u, 149u, 12u, 9u, 1u,
145u, 227u, 148u, 12u, 9u, 1u, 17u, 227u, 147u, 12u, 9u, 147u, 137u,
30u, 57u, 32u, 192u, 144u, 9u, 30u, 56u, 240u, 192u, 32u, 114u, 19u,
38u, 66u, 71u, 142u, 56u, 48u, 36u, 16u, 71u, 142u, 44u, 48u, 64u, 1u,
242u, 1u, 48u, 16u, 119u, 128u, 76u, 8u, 72u, 241u, 197u, 6u, 4u,
129u, 8u, 241u, 196u, 134u, 4u, 208u, 148u, 130u, 60u, 113u, 1u, 129u,
32u, 18u, 60u, 112u, 225u, 129u, 32u, 34u, 60u, 112u, 193u, 129u, 52u,
36u, 24u, 143u, 28u, 40u, 96u, 72u, 4u, 143u, 28u, 16u, 96u, 16u, 73u,
9u, 161u, 27u, 36u, 143u, 28u, 8u, 96u, 72u, 32u, 143u, 26u, 240u,
96u, 128u, 3u, 228u, 2u, 67u, 188u, 2u, 96u, 56u, 16u, 145u, 227u,
93u, 12u, 9u, 2u, 17u, 227u, 92u, 12u, 9u, 1u, 145u, 227u, 91u, 12u,
9u, 161u, 176u, 164u, 120u, 214u, 131u, 2u, 64u, 36u, 120u, 214u, 67u,
2u, 104u, 106u, 105u, 30u, 53u, 128u, 192u, 144u, 9u, 30u, 53u, 80u,
192u, 32u, 162u, 19u, 67u, 80u, 9u, 30u, 53u, 64u, 192u, 144u, 65u,
30u, 53u, 48u, 193u, 5u, 136u, 48u, 52u, 64u, 36u, 132u, 209u, 8u,
112u, 154u, 24u, 128u, 19u, 63u, 240u, 210u, 30u, 130u, 98u, 246u,
19u, 2u, 34u, 73u, 30u, 53u, 16u, 192u, 154u, 35u, 40u, 71u, 141u,
64u, 48u, 38u, 136u, 224u, 145u, 227u, 78u, 12u, 9u, 162u, 50u, 132u,
120u, 210u, 195u, 3u, 60u, 27u, 0u, 146u, 60u, 105u, 65u, 129u, 32u,
18u, 60u, 104u, 193u, 129u, 172u, 53u, 64u, 36u, 145u, 227u, 69u, 12u,
9u, 162u, 179u, 36u, 120u, 209u, 3u, 2u, 65u, 196u, 120u, 208u, 67u,
4u, 30u, 33u, 8u, 58u, 17u, 48u, 145u, 35u, 198u, 56u, 24u, 19u, 69u,
194u, 136u, 241u, 140u, 134u, 4u, 131u, 136u, 241u, 139u, 134u, 4u,
209u, 112u, 162u, 60u, 98u, 161u, 129u, 150u, 19u, 9u, 162u, 219u,
176u, 113u, 35u, 198u, 40u, 24u, 18u, 19u, 35u, 198u, 32u, 24u, 28u,
67u, 84u, 40u, 67u, 152u, 104u, 133u, 12u, 42u, 91u, 137u, 30u, 48u,
224u, 192u, 154u, 52u, 154u, 71u, 140u, 52u, 48u, 36u, 42u, 71u, 140u,
44u, 48u, 38u, 141u, 44u, 17u, 227u, 9u, 12u, 9u, 163u, 66u, 164u,
120u, 192u, 3u, 3u, 136u, 106u, 133u, 8u, 106u, 133u, 75u, 241u, 35u,
197u, 184u, 24u, 19u, 71u, 66u, 8u, 241u, 109u, 6u, 4u, 209u, 205u,
2u, 60u, 90u, 193u, 130u, 22u, 133u, 195u, 68u, 40u, 73u, 30u, 45u,
80u, 192u, 154u, 61u, 42u, 71u, 139u, 68u, 48u, 38u, 143u, 47u, 17u,
226u, 205u, 12u, 16u, 196u, 41u, 192u, 73u, 30u, 44u, 176u, 192u,
144u, 169u, 30u, 44u, 144u, 192u, 154u, 64u, 6u, 71u, 139u, 24u, 48u,
36u, 38u, 71u, 138u, 124u, 48u, 67u, 48u, 160u, 97u, 16u, 209u, 36u,
143u, 20u, 224u, 96u, 72u, 84u, 143u, 20u, 208u, 96u, 77u, 33u, 193u,
35u, 197u, 38u, 24u, 33u, 144u, 209u, 10u, 152u, 108u, 145u, 226u,
143u, 12u, 9u, 164u, 105u, 228u, 120u, 163u, 67u, 2u, 105u, 26u, 17u,
30u, 40u, 160u, 192u, 226u, 26u, 161u, 66u, 28u, 195u, 68u, 40u, 97u,
82u, 220u, 72u, 241u, 68u, 6u, 4u, 210u, 80u, 162u, 60u, 80u, 225u,
129u, 33u, 82u, 60u, 80u, 161u, 129u, 52u, 148u, 84u, 143u, 20u, 24u,
96u, 77u, 36u, 210u, 35u, 197u, 2u, 24u, 26u, 225u, 82u, 71u, 138u,
0u, 48u, 36u, 42u, 71u, 137u, 120u, 48u, 8u, 100u, 52u, 66u, 134u,
27u, 33u, 148u, 59u, 4u, 210u, 120u, 32u, 154u, 70u, 132u, 33u, 200u,
84u, 144u, 154u, 73u, 142u, 19u, 71u, 49u, 73u, 35u, 196u, 184u, 24u,
19u, 74u, 19u, 8u, 241u, 44u, 6u, 4u, 210u, 133u, 146u, 60u, 74u,
193u, 129u, 52u, 161u, 48u, 143u, 18u, 152u, 96u, 113u, 13u, 80u,
161u, 133u, 95u, 137u, 30u, 37u, 0u, 192u, 144u, 169u, 30u, 36u, 224u,
192u, 154u, 87u, 54u, 71u, 137u, 48u, 48u, 8u, 100u, 52u, 66u, 134u,
29u, 33u, 52u, 174u, 64u, 145u, 226u, 74u, 12u, 9u, 165u, 162u, 132u,
120u, 145u, 131u, 2u, 105u, 106u, 9u, 30u, 36u, 64u, 192u, 154u, 90u,
40u, 71u, 137u, 4u, 48u, 65u, 226u, 30u, 133u, 76u, 62u, 72u, 241u,
32u, 6u, 4u, 135u, 200u, 241u, 15u, 6u, 4u, 210u, 245u, 226u, 60u,
67u, 161u, 129u, 33u, 82u, 60u, 67u, 129u, 129u, 144u, 8u, 84u, 50u,
196u, 1u, 206u, 26u, 133u, 76u, 42u, 73u, 9u, 165u, 234u, 36u, 143u,
16u, 216u, 96u, 72u, 84u, 143u, 16u, 192u, 96u, 77u, 49u, 142u, 35u,
196u, 46u, 24u, 18u, 21u, 35u, 196u, 38u, 24u, 19u, 76u, 86u, 72u,
241u, 9u, 6u, 4u, 136u, 8u, 241u, 8u, 6u, 6u, 184u, 80u, 145u, 226u,
15u, 12u, 9u, 10u, 17u, 226u, 9u, 12u, 16u, 132u, 29u, 16u, 146u, 60u,
64u, 193u, 129u, 32u, 226u, 60u, 64u, 129u, 129u, 52u, 213u, 44u,
143u, 16u, 16u, 96u, 101u, 136u, 131u, 35u, 136u, 16u, 160u, 77u, 53u,
75u, 34u, 21u, 12u, 42u, 72u, 136u, 27u, 137u, 13u, 128u, 38u, 154u,
66u, 6u, 216u, 84u, 38u, 152u, 172u, 132u, 210u, 245u, 18u, 66u, 105u,
104u, 136u, 77u, 40u, 73u, 13u, 33u, 52u, 104u, 40u, 146u, 19u, 70u,
55u, 137u, 30u, 32u, 16u, 192u, 145u, 17u, 30u, 32u, 0u, 193u, 17u,
136u, 144u, 68u, 177u, 49u, 36u, 143u, 14u, 240u, 96u, 72u, 56u, 143u,
14u, 208u, 96u, 137u, 196u, 80u, 97u, 248u, 56u, 145u, 225u, 217u,
12u, 9u, 7u, 17u, 225u, 216u, 12u, 9u, 15u, 145u, 225u, 213u, 12u, 9u,
167u, 113u, 164u, 120u, 116u, 195u, 2u, 105u, 220u, 105u, 30u, 29u,
16u, 192u, 154u, 119u, 0u, 71u, 135u, 56u, 48u, 36u, 28u, 71u, 135u,
40u, 48u, 66u, 209u, 76u, 28u, 72u, 240u, 228u, 134u, 4u, 131u, 136u,
240u, 226u, 134u, 4u, 211u, 233u, 18u, 60u, 56u, 33u, 130u, 22u, 138u,
160u, 226u, 71u, 135u, 0u, 48u, 36u, 28u, 71u, 134u, 112u, 48u, 38u,
159u, 239u, 17u, 225u, 154u, 12u, 12u, 176u, 152u, 77u, 63u, 222u, 9u,
167u, 210u, 36u, 143u, 12u, 200u, 96u, 72u, 76u, 143u, 12u, 184u, 96u,
101u, 14u, 17u, 88u, 121u, 17u, 100u, 28u, 73u, 9u, 167u, 112u, 16u,
113u, 35u, 195u, 44u, 24u, 18u, 43u, 35u, 195u, 42u, 24u, 19u, 78u,
161u, 8u, 240u, 201u, 134u, 4u, 137u, 136u, 240u, 198u, 134u, 8u,
158u, 45u, 17u, 112u, 97u, 139u, 201u, 36u, 120u, 99u, 3u, 2u, 106u,
40u, 89u, 30u, 24u, 176u, 192u, 145u, 113u, 30u, 24u, 112u, 192u,
154u, 138u, 22u, 71u, 134u, 24u, 48u, 36u, 90u, 71u, 134u, 16u, 48u,
38u, 162u, 106u, 17u, 225u, 128u, 12u, 12u, 241u, 132u, 76u, 72u,
240u, 175u, 134u, 4u, 137u, 136u, 240u, 173u, 134u, 4u, 212u, 124u,
50u, 60u, 42u, 225u, 130u, 49u, 140u, 131u, 12u, 102u, 73u, 30u, 21u,
64u, 192u, 145u, 145u, 30u, 21u, 32u, 192u, 154u, 146u, 28u, 71u,
133u, 64u, 48u, 50u, 198u, 65u, 53u, 36u, 56u, 38u, 163u, 225u, 146u,
60u, 41u, 225u, 129u, 35u, 34u, 60u, 41u, 193u, 129u, 140u, 70u, 68u,
38u, 165u, 68u, 146u, 60u, 41u, 129u, 130u, 52u, 17u, 119u, 1u, 36u,
143u, 10u, 88u, 96u, 77u, 76u, 139u, 35u, 194u, 148u, 24u, 18u, 46u,
35u, 194u, 142u, 24u, 19u, 83u, 34u, 200u, 240u, 162u, 134u, 4u, 212u,
197u, 226u, 60u, 40u, 33u, 129u, 158u, 53u, 131u, 137u, 30u, 20u, 0u,
192u, 145u, 49u, 30u, 17u, 192u, 192u, 154u, 157u, 140u, 71u, 132u,
104u, 48u, 50u, 194u, 97u, 53u, 59u, 24u, 70u, 193u, 53u, 49u, 120u,
146u, 60u, 35u, 33u, 129u, 33u, 50u, 60u, 34u, 129u, 129u, 53u, 31u,
12u, 143u, 8u, 152u, 96u, 72u, 152u, 143u, 8u, 120u, 96u, 77u, 71u,
195u, 35u, 194u, 22u, 24u, 19u, 82u, 67u, 136u, 240u, 132u, 6u, 4u,
140u, 136u, 240u, 131u, 6u, 4u, 212u, 144u, 226u, 60u, 32u, 129u,
129u, 53u, 42u, 36u, 143u, 8u, 24u, 96u, 72u, 200u, 143u, 8u, 16u,
96u, 77u, 75u, 198u, 35u, 194u, 0u, 24u, 35u, 113u, 28u, 18u, 71u,
126u, 12u, 9u, 170u, 170u, 228u, 119u, 192u, 192u, 154u, 170u, 174u,
71u, 122u, 12u, 9u, 170u, 169u, 68u, 119u, 0u, 193u, 28u, 199u, 68u,
142u, 216u, 24u, 19u, 85u, 183u, 200u, 236u, 193u, 130u, 57u, 142u,
201u, 29u, 136u, 48u, 38u, 171u, 227u, 17u, 216u, 3u, 3u, 44u, 38u,
35u, 194u, 35u, 96u, 154u, 170u, 148u, 73u, 29u, 124u, 48u, 36u, 38u,
71u, 93u, 12u, 12u, 162u, 25u, 143u, 97u, 162u, 19u, 86u, 35u, 66u,
106u, 128u, 137u, 35u, 173u, 134u, 4u, 213u, 156u, 226u, 58u, 192u,
96u, 72u, 244u, 142u, 172u, 24u, 19u, 86u, 115u, 136u, 234u, 129u,
130u, 46u, 224u, 36u, 142u, 166u, 24u, 18u, 46u, 35u, 168u, 6u, 4u,
213u, 189u, 98u, 58u, 112u, 96u, 101u, 139u, 132u, 124u, 4u, 122u,
19u, 86u, 245u, 136u, 139u, 66u, 106u, 206u, 25u, 33u, 53u, 19u, 80u,
193u, 196u, 142u, 154u, 24u, 18u, 46u, 35u, 166u, 6u, 6u, 64u, 34u,
224u, 225u, 31u, 136u, 90u, 64u, 131u, 137u, 36u, 38u, 174u, 132u,
18u, 58u, 80u, 96u, 77u, 95u, 68u, 35u, 164u, 134u, 4u, 137u, 136u,
232u, 161u, 129u, 53u, 125u, 16u, 142u, 132u, 24u, 18u, 63u, 35u,
156u, 134u, 6u, 88u, 78u, 18u, 144u, 73u, 28u, 224u, 48u, 36u, 38u,
71u, 54u, 12u, 18u, 16u, 97u, 144u, 200u, 145u, 9u, 35u, 154u, 6u, 4u,
214u, 41u, 130u, 57u, 144u, 96u, 77u, 98u, 152u, 35u, 150u, 6u, 4u,
129u, 8u, 228u, 225u, 129u, 148u, 72u, 176u, 33u, 110u, 129u, 9u, 28u,
148u, 48u, 38u, 178u, 32u, 145u, 201u, 3u, 2u, 64u, 132u, 114u, 32u,
192u, 154u, 200u, 130u, 71u, 32u, 12u, 9u, 1u, 145u, 198u, 195u, 3u,
40u, 145u, 96u, 50u, 221u, 1u, 146u, 56u, 200u, 96u, 77u, 102u, 80u,
35u, 140u, 6u, 4u, 128u, 200u, 226u, 193u, 129u, 53u, 153u, 64u, 142u,
40u, 24u, 18u, 2u, 35u, 135u, 134u, 6u, 81u, 34u, 192u, 69u, 186u, 2u,
36u, 112u, 208u, 192u, 154u, 208u, 190u, 71u, 12u, 12u, 9u, 1u, 17u,
194u, 131u, 2u, 107u, 66u, 249u, 28u, 32u, 48u, 53u, 192u, 36u, 142u,
14u, 24u, 18u, 1u, 35u, 128u, 6u, 4u, 145u, 136u, 98u, 12u, 12u, 98u,
38u, 145u, 196u, 86u, 220u, 68u, 126u, 220u, 68u, 68u, 220u, 72u,
102u, 132u, 194u, 107u, 18u, 153u, 12u, 208u, 248u, 77u, 97u, 132u,
33u, 152u, 68u, 176u, 113u, 18u, 68u, 92u, 19u, 87u, 195u, 194u, 106u,
232u, 236u, 28u, 72u, 102u, 17u, 225u, 14u, 193u, 53u, 95u, 24u, 38u,
171u, 111u, 195u, 68u, 134u, 97u, 27u, 38u, 165u, 232u, 166u, 165u,
239u, 6u, 97u, 22u, 65u, 196u, 38u, 160u, 165u, 144u, 204u, 34u, 72u,
56u, 132u, 212u, 40u, 130u, 25u, 132u, 57u, 10u, 16u, 236u, 19u, 72u,
112u, 67u, 132u, 38u, 19u, 72u, 0u, 194u, 104u, 242u, 241u, 36u, 51u,
8u, 142u, 14u, 33u, 52u, 218u, 96u, 137u, 38u, 35u, 146u, 98u, 73u,
38u, 37u, 137u, 137u, 36u,};
static unsigned char uvector__00002[] = {
 0u, 3u, 134u, 6u, 9u, 42u, 75u, 18u, 100u, 77u, 35u, 136u, 173u,
184u, 136u, 253u, 184u, 136u, 137u, 184u, 144u, 205u, 9u, 137u, 8u,
48u, 200u, 100u, 72u, 132u, 144u, 205u, 15u, 134u, 88u, 78u, 18u,
144u, 73u, 12u, 194u, 37u, 131u, 136u, 146u, 34u, 224u, 225u, 31u,
136u, 90u, 64u, 131u, 137u, 17u, 240u, 17u, 232u, 139u, 184u, 9u, 34u,
45u, 12u, 162u, 25u, 143u, 97u, 162u, 25u, 97u, 49u, 30u, 17u, 27u,
8u, 220u, 71u, 4u, 146u, 25u, 97u, 48u, 207u, 26u, 193u, 196u, 70u,
194u, 52u, 17u, 119u, 1u, 36u, 146u, 72u, 137u, 226u, 209u, 23u, 6u,
24u, 188u, 146u, 96u, 226u, 67u, 48u, 143u, 8u, 118u, 17u, 204u, 118u,
68u, 115u, 29u, 24u, 104u, 144u, 204u, 35u, 104u, 200u, 134u, 88u,
200u, 70u, 49u, 144u, 97u, 140u, 201u, 12u, 241u, 132u, 76u, 73u, 12u,
194u, 44u, 131u, 136u, 101u, 132u, 196u, 45u, 21u, 65u, 196u, 66u,
209u, 76u, 28u, 73u, 12u, 194u, 36u, 131u, 136u, 101u, 14u, 17u, 88u,
121u, 17u, 100u, 28u, 73u, 17u, 56u, 138u, 12u, 63u, 7u, 24u, 56u,
144u, 204u, 33u, 200u, 80u, 135u, 97u, 12u, 194u, 129u, 132u, 67u,
68u, 144u, 225u, 9u, 136u, 98u, 20u, 224u, 36u, 66u, 208u, 184u, 104u,
133u, 9u, 36u, 134u, 97u, 17u, 193u, 196u, 50u, 196u, 65u, 145u, 196u,
8u, 80u, 66u, 16u, 116u, 66u, 72u, 133u, 67u, 10u, 146u, 34u, 6u,
226u, 67u, 96u, 13u, 112u, 161u, 13u, 176u, 168u, 101u, 136u, 3u,
156u, 53u, 10u, 152u, 84u, 136u, 60u, 67u, 208u, 169u, 135u, 201u,
32u, 67u, 33u, 162u, 20u, 48u, 233u, 14u, 33u, 170u, 20u, 48u, 171u,
241u, 32u, 67u, 33u, 162u, 20u, 48u, 217u, 12u, 161u, 216u, 53u, 194u,
164u, 67u, 33u, 162u, 21u, 48u, 217u, 16u, 228u, 42u, 72u, 113u, 13u,
80u, 161u, 14u, 97u, 162u, 20u, 48u, 169u, 110u, 33u, 196u, 53u, 66u,
132u, 53u, 66u, 165u, 248u, 146u, 26u, 67u, 136u, 106u, 133u, 8u,
115u, 13u, 16u, 161u, 133u, 75u, 113u, 36u, 134u, 88u, 76u, 65u, 226u,
16u, 131u, 161u, 19u, 9u, 24u, 56u, 146u, 36u, 152u, 142u, 73u, 137u,
36u, 152u, 150u, 38u, 36u, 142u, 18u, 24u, 24u, 194u, 96u, 122u, 152u,
78u, 17u, 193u, 195u, 2u, 96u, 56u, 71u, 2u, 12u, 9u, 128u, 225u, 28u,
0u, 48u, 38u, 3u, 132u, 128u,};
static ScmObj libsyssys_errno_TOsymbol(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_errno_TOsymbol__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_errno_TOsymbol, NULL, NULL);

static ScmObj libsyssys_symbol_TOerrno(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_symbol_TOerrno__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_symbol_TOerrno, NULL, NULL);

static ScmObj libsyssys_getgrgid(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getgrgid__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getgrgid, NULL, NULL);

static ScmObj libsyssys_getgrnam(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getgrnam__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getgrnam, NULL, NULL);

static ScmObj libsyssys_gid_TOgroup_name(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_gid_TOgroup_name__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_gid_TOgroup_name, NULL, NULL);

static ScmObj libsyssys_group_name_TOgid(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_group_name_TOgid__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_group_name_TOgid, NULL, NULL);

static ScmObj libsyssys_setlocale(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_setlocale__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_setlocale, NULL, NULL);

static ScmObj libsyssys_localeconv(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_localeconv__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_localeconv, NULL, NULL);

static ScmObj libsyssys_getpwuid(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getpwuid__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getpwuid, NULL, NULL);

static ScmObj libsyssys_getpwnam(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getpwnam__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getpwnam, NULL, NULL);

static ScmObj libsyssys_uid_TOuser_name(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_uid_TOuser_name__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_uid_TOuser_name, NULL, NULL);

static ScmObj libsyssys_user_name_TOuid(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_user_name_TOuid__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_user_name_TOuid, NULL, NULL);

static ScmObj libsyssys_sigset_addX(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_sigset_addX__STUB, 1, 1,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_sigset_addX, NULL, NULL);

static ScmObj libsyssys_sigset_deleteX(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_sigset_deleteX__STUB, 1, 1,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_sigset_deleteX, NULL, NULL);

static ScmObj libsyssys_sigset_fillX(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_sigset_fillX__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_sigset_fillX, NULL, NULL);

static ScmObj libsyssys_sigset_emptyX(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_sigset_emptyX__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_sigset_emptyX, NULL, NULL);

static ScmObj libsyssys_signal_name(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_signal_name__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_signal_name, NULL, NULL);

static ScmObj libsyssys_kill(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_kill__STUB, 2, 0,SCM_FALSE,libsyssys_kill, NULL, NULL);

static ScmObj libsysset_signal_handlerX(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsysset_signal_handlerX__STUB, 2, 2,SCM_FALSE,libsysset_signal_handlerX, NULL, NULL);

static ScmObj libsysget_signal_handler(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsysget_signal_handler__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsysget_signal_handler, NULL, NULL);

static ScmObj libsysget_signal_handler_mask(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsysget_signal_handler_mask__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsysget_signal_handler_mask, NULL, NULL);

static ScmObj libsysget_signal_handlers(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsysget_signal_handlers__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsysget_signal_handlers, NULL, NULL);

static ScmObj libsysset_signal_pending_limit(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsysset_signal_pending_limit__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsysset_signal_pending_limit, NULL, NULL);

static ScmObj libsysget_signal_pending_limit(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsysget_signal_pending_limit__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsysget_signal_pending_limit, NULL, NULL);

static ScmObj libsyssys_sigmask(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_sigmask__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_sigmask, NULL, NULL);

static ScmObj libsyssys_sigsuspend(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_sigsuspend__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_sigsuspend, NULL, NULL);

static ScmObj libsyssys_sigwait(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_sigwait__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_sigwait, NULL, NULL);

static unsigned char uvector__00003[] = {
 0u, 3u, 137u, 6u, 7u, 89u, 52u, 73u, 210u, 121u, 148u, 9u, 28u, 68u,
48u, 36u, 160u, 71u, 14u, 12u, 9u, 129u, 137u, 28u, 48u, 48u, 36u,
158u, 71u, 10u, 12u, 9u, 129u, 137u, 28u, 32u, 48u, 36u, 154u, 71u,
5u, 12u, 18u, 116u, 158u, 72u, 224u, 97u, 129u, 36u, 242u, 56u, 8u,
96u, 107u, 148u, 9u, 28u, 0u, 48u, 36u, 160u, 67u, 16u, 96u, 99u,
148u, 3u, 40u, 76u, 106u, 2u, 98u, 150u, 19u, 1u, 194u, 73u, 0u,};
static unsigned char uvector__00004[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 40u, 164u, 160u, 25u, 67u, 92u, 160u, 68u,
157u, 39u, 144u, 235u, 38u, 137u, 58u, 79u, 50u, 129u, 36u, 142u, 18u,
24u, 24u, 229u, 4u, 192u, 252u, 142u, 14u, 24u, 19u, 1u, 194u, 56u,
16u, 96u, 76u, 7u, 8u, 224u, 1u, 129u, 48u, 28u, 36u,};
static ScmObj libsyssys_remove(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_remove__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_remove, NULL, NULL);

static ScmObj libsyssys_rename(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_rename__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_rename, NULL, NULL);

static ScmObj libsyssys_tmpnam(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_tmpnam__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_tmpnam, NULL, NULL);

static ScmObj libsyssys_mkstemp(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_mkstemp__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_mkstemp, NULL, NULL);

static ScmObj libsyssys_mkdtemp(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_mkdtemp__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_mkdtemp, NULL, NULL);

static ScmObj libsyssys_ctermid(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_ctermid__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_ctermid, NULL, NULL);

static ScmObj libsyssys_exit(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_exit__STUB, 1, 0,SCM_FALSE,libsyssys_exit, NULL, NULL);

static ScmObj libsyssys_getenv(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getenv__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getenv, NULL, NULL);

static ScmObj libsyssys_abort(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_abort__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_abort, NULL, NULL);

static ScmObj libsyssys_system(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_system__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_system, NULL, NULL);

static ScmObj libsyssys_random(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_random__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_random, NULL, NULL);

static ScmObj libsyssys_srandom(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_srandom__STUB, 1, 0,SCM_FALSE,libsyssys_srandom, NULL, NULL);

static ScmObj libsyssys_environ(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_environ__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_environ, NULL, NULL);

static ScmObj libsyssys_setenv(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_setenv__STUB, 2, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_setenv, NULL, NULL);

static ScmObj libsyssys_unsetenv(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_unsetenv__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_unsetenv, NULL, NULL);

static ScmObj libsyssys_clearenv(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_clearenv__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_clearenv, NULL, NULL);

static unsigned char uvector__00005[] = {
 0u, 3u, 138u, 6u, 7u, 57u, 74u, 26u, 36u, 113u, 16u, 192u, 146u,
145u, 28u, 60u, 48u, 57u, 202u, 114u, 161u, 35u, 135u, 6u, 4u, 149u,
8u, 225u, 161u, 129u, 37u, 50u, 56u, 88u, 96u, 101u, 148u, 194u, 97u,
2u, 19u, 1u, 194u, 71u, 10u, 12u, 9u, 41u, 145u, 194u, 67u, 4u, 170u,
37u, 57u, 80u, 137u, 90u, 82u, 149u, 195u, 12u, 176u, 72u, 76u, 68u,
9u, 28u, 28u, 48u, 38u, 55u, 132u, 112u, 32u, 192u, 146u, 145u, 28u,
0u, 48u, 38u, 55u, 132u, 49u, 6u, 6u, 49u, 41u, 16u, 152u, 202u, 146u,
64u,};
static unsigned char uvector__00006[] = {
 0u, 3u, 145u, 6u, 9u, 100u, 57u, 9u, 72u, 137u, 84u, 74u, 114u, 161u,
18u, 180u, 165u, 43u, 134u, 25u, 96u, 144u, 203u, 41u, 135u, 57u, 78u,
84u, 33u, 206u, 82u, 134u, 137u, 38u, 90u, 36u, 114u, 16u, 192u, 146u,
209u, 28u, 128u, 48u, 36u, 182u, 71u, 30u, 12u, 12u, 242u, 224u, 97u,
12u, 98u, 93u, 18u, 208u, 151u, 137u, 33u, 48u, 28u, 36u, 145u, 198u,
3u, 2u, 99u, 6u, 71u, 20u, 12u, 13u, 114u, 249u, 35u, 137u, 134u, 4u,
151u, 200u, 226u, 65u, 129u, 144u, 9u, 104u, 50u, 134u, 185u, 128u,
132u, 199u, 32u, 52u, 76u, 4u, 145u, 47u, 134u, 80u, 215u, 48u, 16u,
194u, 164u, 53u, 76u, 4u, 146u, 27u, 194u, 100u, 134u, 19u, 24u, 50u,
25u, 21u, 49u, 219u, 36u, 113u, 16u, 192u, 153u, 168u, 145u, 195u, 3u,
2u, 102u, 78u, 71u, 11u, 12u, 9u, 48u, 145u, 194u, 67u, 2u, 101u,
226u, 71u, 5u, 12u, 9u, 142u, 65u, 28u, 12u, 48u, 38u, 57u, 4u, 112u,
16u, 192u, 153u, 98u, 17u, 192u, 3u, 2u, 76u, 36u, 49u, 6u, 4u, 198u,
140u, 144u,};
static unsigned char uvector__00007[] = {
 0u, 3u, 135u, 6u, 6u, 97u, 49u, 75u, 162u, 90u, 18u, 241u, 36u, 75u,
33u, 200u, 74u, 68u, 74u, 162u, 83u, 149u, 8u, 149u, 165u, 41u, 92u,
48u, 203u, 4u, 134u, 89u, 76u, 57u, 202u, 114u, 161u, 14u, 114u, 148u,
52u, 73u, 50u, 209u, 36u, 112u, 176u, 192u, 198u, 19u, 3u, 20u, 193u,
120u, 142u, 18u, 24u, 19u, 1u, 194u, 56u, 32u, 96u, 76u, 7u, 8u, 224u,
65u, 129u, 48u, 28u, 36u,};
static unsigned char uvector__00008[] = {
 0u, 3u, 154u, 6u, 9u, 142u, 100u, 13u, 19u, 41u, 126u, 36u, 115u, 0u,
192u, 152u, 25u, 145u, 203u, 195u, 2u, 76u, 132u, 114u, 192u, 193u,
49u, 204u, 211u, 59u, 241u, 35u, 148u, 134u, 4u, 153u, 200u, 229u, 1u,
129u, 38u, 98u, 57u, 48u, 96u, 103u, 154u, 38u, 66u, 71u, 37u, 12u,
9u, 50u, 17u, 200u, 67u, 2u, 98u, 192u, 71u, 29u, 12u, 13u, 243u, 48u,
76u, 88u, 9u, 28u, 112u, 48u, 36u, 204u, 71u, 27u, 12u, 18u, 168u,
153u, 166u, 114u, 37u, 105u, 146u, 87u, 12u, 50u, 193u, 33u, 49u,
243u, 9u, 134u, 57u, 35u, 140u, 134u, 4u, 202u, 40u, 142u, 40u, 24u,
18u, 100u, 35u, 137u, 6u, 4u, 202u, 40u, 142u, 32u, 24u, 32u, 89u,
164u, 48u, 205u, 70u, 8u, 36u, 112u, 240u, 192u, 147u, 33u, 28u, 36u,
48u, 38u, 114u, 68u, 112u, 80u, 192u, 222u, 38u, 184u, 32u, 132u,
206u, 72u, 145u, 193u, 3u, 2u, 104u, 68u, 81u, 28u, 12u, 48u, 36u,
200u, 71u, 1u, 12u, 13u, 115u, 41u, 35u, 128u, 6u, 4u, 153u, 72u, 98u,
12u, 12u, 98u, 100u, 73u, 148u, 54u, 0u, 154u, 19u, 166u, 38u, 201u,
174u, 100u, 33u, 50u, 99u, 33u, 164u, 38u, 3u, 132u, 146u, 64u,};
static unsigned char uvector__00009[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 54u, 204u, 137u, 50u, 134u, 192u, 26u,
230u, 82u, 38u, 201u, 174u, 100u, 34u, 85u, 19u, 52u, 206u, 68u, 173u,
50u, 74u, 225u, 134u, 88u, 36u, 55u, 204u, 193u, 158u, 104u, 153u, 9u,
19u, 28u, 205u, 51u, 191u, 18u, 67u, 72u, 152u, 230u, 64u, 209u, 50u,
151u, 226u, 73u, 35u, 132u, 134u, 6u, 48u, 152u, 24u, 166u, 9u, 36u,
112u, 112u, 192u, 152u, 14u, 17u, 192u, 131u, 2u, 96u, 56u, 71u, 0u,
12u, 9u, 128u, 225u, 32u,};
static ScmObj libsyssys_strerror(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_strerror__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_strerror, NULL, NULL);

#if HAVE_STRSIGNAL
static ScmObj libsyssys_strsignal(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_strsignal__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_strsignal, NULL, NULL);

#endif /* HAVE_STRSIGNAL */
#if !(HAVE_STRSIGNAL)
static ScmObj libsyssys_strsignal(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_strsignal__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_strsignal, NULL, NULL);

#endif /* !(HAVE_STRSIGNAL) */
#if defined(HAVE_GETLOADAVG)
static ScmObj libsyssys_getloadavg(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getloadavg__STUB, 0, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getloadavg, NULL, NULL);

#endif /* defined(HAVE_GETLOADAVG) */
#if !(defined(HAVE_GETLOADAVG))
static ScmObj libsyssys_getloadavg(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getloadavg__STUB, 0, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getloadavg, NULL, NULL);

#endif /* !(defined(HAVE_GETLOADAVG)) */
static ScmObj libsyssys_mmap(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_mmap__STUB, 4, 2,SCM_FALSE,libsyssys_mmap, NULL, NULL);

#if defined(HAVE_SYS_RESOURCE_H)
#if (SIZEOF_RLIM_T)==(4)

#endif /* (SIZEOF_RLIM_T)==(4) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if (SIZEOF_RLIM_T)==(4)

#endif /* (SIZEOF_RLIM_T)==(4) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if !((SIZEOF_RLIM_T)==(4))
#if (SIZEOF_RLIM_T)==(8)

#endif /* (SIZEOF_RLIM_T)==(8) */
#endif /* !((SIZEOF_RLIM_T)==(4)) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if !((SIZEOF_RLIM_T)==(4))
#if (SIZEOF_RLIM_T)==(8)

#endif /* (SIZEOF_RLIM_T)==(8) */
#endif /* !((SIZEOF_RLIM_T)==(4)) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if !((SIZEOF_RLIM_T)==(4))
#if !((SIZEOF_RLIM_T)==(8))

#endif /* !((SIZEOF_RLIM_T)==(8)) */
#endif /* !((SIZEOF_RLIM_T)==(4)) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
static ScmObj libsyssys_getrlimit(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getrlimit__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getrlimit, NULL, NULL);

#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
static ScmObj libsyssys_setrlimit(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_setrlimit__STUB, 2, 2,SCM_FALSE,libsyssys_setrlimit, NULL, NULL);

#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(GAUCHE_WINDOWS)
static const char* check_trailing_separator(const char* path);
#endif /* defined(GAUCHE_WINDOWS) */
#if !(defined(GAUCHE_WINDOWS))
static const char* check_trailing_separator(const char* path);
#endif /* !(defined(GAUCHE_WINDOWS)) */
static ScmObj libsyssys_stat(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_stat__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_stat, NULL, NULL);

#if !(defined(GAUCHE_WINDOWS))
static ScmObj libsyssys_lstat(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_lstat__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_lstat, NULL, NULL);

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(!(defined(GAUCHE_WINDOWS)))
static ScmObj libsyssys_lstat(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_lstat__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_lstat, NULL, NULL);

#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(defined(GAUCHE_WINDOWS))
static ScmObj libsyssys_mkfifo(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_mkfifo__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_mkfifo, NULL, NULL);

#endif /* !(defined(GAUCHE_WINDOWS)) */
static ScmObj libsyssys_fstat(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_fstat__STUB, 1, 0,SCM_FALSE,libsyssys_fstat, NULL, NULL);

static ScmObj libsysfile_existsP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsysfile_existsP__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsysfile_existsP, NULL, NULL);

static ScmObj libsysfile_is_regularP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsysfile_is_regularP__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsysfile_is_regularP, NULL, NULL);

static ScmObj libsysfile_is_directoryP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsysfile_is_directoryP__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsysfile_is_directoryP, NULL, NULL);

static void utime_ts(ScmTimeSpec* ts,ScmObj arg);
static ScmObj libsyssys_utime(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_utime__STUB, 1, 3,SCM_FALSE,libsyssys_utime, NULL, NULL);

static ScmObj libsyssys_times(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_times__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_times, NULL, NULL);

static ScmObj libsyssys_uname(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_uname__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_uname, NULL, NULL);

static ScmObj libsyssys_wait(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_wait__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_wait, NULL, NULL);

static ScmObj libsyssys_waitpid(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_waitpid__STUB, 1, 1,SCM_FALSE,libsyssys_waitpid, NULL, NULL);

static ScmObj libsyssys_wait_exitedP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_wait_exitedP__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_wait_exitedP, NULL, NULL);

static ScmObj libsyssys_wait_exit_status(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_wait_exit_status__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_wait_exit_status, NULL, NULL);

static ScmObj libsyssys_wait_signaledP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_wait_signaledP__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_wait_signaledP, NULL, NULL);

static ScmObj libsyssys_wait_termsig(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_wait_termsig__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_wait_termsig, NULL, NULL);

static ScmObj libsyssys_wait_stoppedP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_wait_stoppedP__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_wait_stoppedP, NULL, NULL);

static ScmObj libsyssys_wait_stopsig(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_wait_stopsig__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_wait_stopsig, NULL, NULL);

static ScmObj libsyssys_time(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_time__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_time, NULL, NULL);

static ScmObj libsyssys_gettimeofday(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_gettimeofday__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_gettimeofday, NULL, NULL);

static ScmObj libsyscurrent_microseconds(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyscurrent_microseconds__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyscurrent_microseconds, NULL, NULL);

static ScmObj libsyssys_clock_gettime_monotonic(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_clock_gettime_monotonic__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_clock_gettime_monotonic, NULL, NULL);

static ScmObj libsyssys_clock_getres_monotonic(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_clock_getres_monotonic__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_clock_getres_monotonic, NULL, NULL);

static ScmObj libsyscurrent_time(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyscurrent_time__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyscurrent_time, NULL, NULL);

static ScmObj libsystimeP(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsystimeP__STUB, 1, 0,SCM_FALSE,libsystimeP, NULL, NULL);

static ScmObj libsysabsolute_time(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsysabsolute_time__STUB, 1, 2,SCM_FALSE,libsysabsolute_time, NULL, NULL);

static unsigned char uvector__00010[] = {
 0u, 3u, 150u, 6u, 9u, 186u, 111u, 156u, 9u, 28u, 172u, 48u, 36u,
224u, 71u, 42u, 12u, 9u, 55u, 145u, 202u, 3u, 4u, 226u, 24u, 102u,
243u, 57u, 78u, 100u, 142u, 78u, 24u, 18u, 111u, 35u, 146u, 134u, 4u,
156u, 136u, 228u, 33u, 129u, 48u, 173u, 35u, 143u, 6u, 4u, 155u, 200u,
227u, 129u, 129u, 148u, 78u, 147u, 156u, 228u, 103u, 48u, 152u, 86u,
146u, 56u, 208u, 96u, 76u, 110u, 72u, 227u, 1u, 129u, 39u, 34u, 56u,
184u, 96u, 73u, 188u, 142u, 42u, 24u, 19u, 27u, 146u, 56u, 152u, 96u,
156u, 67u, 12u, 224u, 103u, 89u, 204u, 145u, 196u, 131u, 2u, 78u, 4u,
113u, 0u, 192u, 147u, 169u, 28u, 48u, 48u, 38u, 89u, 4u, 112u, 144u,
192u, 147u, 129u, 28u, 28u, 48u, 50u, 137u, 210u, 115u, 157u, 76u,
230u, 19u, 44u, 130u, 71u, 5u, 12u, 9u, 158u, 161u, 28u, 12u, 48u,
36u, 234u, 71u, 2u, 12u, 9u, 56u, 17u, 192u, 3u, 2u, 103u, 168u, 67u,
16u, 96u, 99u, 19u, 132u, 222u, 68u, 237u, 56u, 78u, 164u, 78u, 211u,
124u, 228u, 66u, 96u, 56u, 73u, 0u,};
static unsigned char uvector__00011[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 59u, 206u, 19u, 121u, 19u, 180u, 225u,
58u, 145u, 59u, 77u, 243u, 145u, 19u, 116u, 223u, 56u, 18u, 71u, 9u,
12u, 12u, 97u, 48u, 49u, 76u, 18u, 8u, 224u, 225u, 129u, 48u, 28u,
35u, 129u, 6u, 4u, 192u, 112u, 142u, 0u, 24u, 19u, 1u, 194u, 64u,};
static ScmObj libsystime_TOseconds(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsystime_TOseconds__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsystime_TOseconds, NULL, NULL);

static ScmObj libsysseconds_TOtime(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsysseconds_TOtime__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsysseconds_TOtime, NULL, NULL);

static unsigned char uvector__00012[] = {
 0u, 3u, 132u, 6u, 9u, 224u, 79u, 51u, 212u, 246u, 73u, 28u, 16u, 48u,
38u, 5u, 36u, 112u, 48u, 192u, 147u, 217u, 28u, 8u, 48u, 36u, 244u,
71u, 0u, 12u, 9u, 129u, 73u, 12u, 65u, 129u, 140u, 79u, 83u, 217u, 9u,
128u, 225u, 36u,};
static unsigned char uvector__00013[] = {
 0u, 3u, 132u, 6u, 9u, 240u, 79u, 51u, 212u, 246u, 120u, 1u, 35u,
130u, 6u, 4u, 192u, 164u, 142u, 6u, 24u, 18u, 123u, 35u, 129u, 6u, 4u,
158u, 136u, 224u, 1u, 129u, 48u, 41u, 33u, 136u, 48u, 49u, 137u, 234u,
123u, 33u, 48u, 28u, 36u, 128u,};
static unsigned char uvector__00014[] = {
 0u, 3u, 138u, 134u, 6u, 105u, 244u, 79u, 211u, 248u, 114u, 19u, 212u,
246u, 68u, 240u, 39u, 153u, 234u, 123u, 36u, 135u, 33u, 61u, 79u,
100u, 79u, 130u, 121u, 158u, 167u, 179u, 192u, 9u, 161u, 0u, 36u,
142u, 36u, 24u, 19u, 3u, 18u, 56u, 128u, 96u, 74u, 16u, 2u, 56u, 112u,
96u, 99u, 9u, 133u, 120u, 76u, 50u, 73u, 28u, 44u, 48u, 49u, 132u,
193u, 76u, 38u, 13u, 164u, 142u, 18u, 24u, 18u, 127u, 35u, 131u, 134u,
4u, 192u, 112u, 142u, 4u, 24u, 19u, 1u, 194u, 56u, 0u, 96u, 76u, 7u,
9u, 0u,};
static void tm_print(ScmObj obj,ScmPort* port,ScmWriteContext* ctx);
#include <gauche/class.h>
typedef struct {
  SCM_HEADER;
  struct tm data;
} Scm_sys_tm_Rec;
SCM_CLASS_DECL(Scm_sys_tm_Class);
#define SCM_SYS_TM_P(obj) SCM_ISA(obj,&Scm_sys_tm_Class)
#define SCM_SYS_TM(obj) &(((Scm_sys_tm_Rec *)(obj))->data)
SCM_EXTERN ScmObj Scm_Make_sys_tm(const struct tm*);

static ScmObj libsyssys_asctime(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_asctime__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_asctime, NULL, NULL);

static ScmObj libsyssys_ctime(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_ctime__STUB, 1, 0,SCM_FALSE,libsyssys_ctime, NULL, NULL);

static ScmObj libsyssys_difftime(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_difftime__STUB, 2, 0,SCM_FALSE,libsyssys_difftime, NULL, NULL);

static ScmObj libsyssys_strftime(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_strftime__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_strftime, NULL, NULL);

static ScmObj libsyssys_gmtime(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_gmtime__STUB, 1, 0,SCM_FALSE,libsyssys_gmtime, NULL, NULL);

static ScmObj libsyssys_localtime(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_localtime__STUB, 1, 0,SCM_FALSE,libsyssys_localtime, NULL, NULL);

static ScmObj libsyssys_mktime(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_mktime__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_mktime, NULL, NULL);

static ScmObj libsyssys_access(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_access__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_access, NULL, NULL);

static ScmObj libsyssys_chdir(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_chdir__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_chdir, NULL, NULL);

static ScmObj libsyssys_chmod(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_chmod__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_chmod, NULL, NULL);

#if !(defined(GAUCHE_WINDOWS))
static ScmObj libsyssys_fchmod(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_fchmod__STUB, 2, 0,SCM_FALSE,libsyssys_fchmod, NULL, NULL);

#endif /* !(defined(GAUCHE_WINDOWS)) */
static ScmObj libsyssys_chown(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_chown__STUB, 3, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_chown, NULL, NULL);

#if defined(HAVE_LCHOWN)
static ScmObj libsyssys_lchown(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_lchown__STUB, 3, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_lchown, NULL, NULL);

#endif /* defined(HAVE_LCHOWN) */
static ScmObj libsyssys_fork(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_fork__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_fork, NULL, NULL);

static ScmObj libsyssys_exec(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_exec__STUB, 2, 1,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_exec, NULL, NULL);

static ScmObj libsyssys_fork_and_exec(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_fork_and_exec__STUB, 2, 1,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_fork_and_exec, NULL, NULL);

static ScmObj libsyssys_getcwd(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getcwd__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getcwd, NULL, NULL);

static ScmObj libsyssys_getegid(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getegid__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getegid, NULL, NULL);

static ScmObj libsyssys_getgid(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getgid__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getgid, NULL, NULL);

static ScmObj libsyssys_geteuid(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_geteuid__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_geteuid, NULL, NULL);

static ScmObj libsyssys_getuid(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getuid__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getuid, NULL, NULL);

static ScmObj libsyssys_setugidP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_setugidP__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_setugidP, NULL, NULL);

static ScmObj libsyssys_getpid(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getpid__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getpid, NULL, NULL);

static ScmObj libsyssys_getppid(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getppid__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getppid, NULL, NULL);

#if !(defined(GAUCHE_WINDOWS))
static ScmObj libsyssys_setgid(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_setgid__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_setgid, NULL, NULL);

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
static ScmObj libsyssys_setpgid(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_setpgid__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_setpgid, NULL, NULL);

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
static ScmObj libsyssys_getpgid(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getpgid__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getpgid, NULL, NULL);

#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
static ScmObj libsyssys_getpgrp(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getpgrp__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getpgrp, NULL, NULL);

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
static ScmObj libsyssys_setsid(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_setsid__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_setsid, NULL, NULL);

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
static ScmObj libsyssys_setuid(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_setuid__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_setuid, NULL, NULL);

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
static ScmObj libsyssys_nice(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_nice__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_nice, NULL, NULL);

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
static ScmObj libsyssys_getgroups(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getgroups__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getgroups, NULL, NULL);

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
static ScmObj libsyssys_setgroups(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_setgroups__STUB, 1, 0,SCM_FALSE,libsyssys_setgroups, NULL, NULL);

#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
static unsigned char uvector__00015[] = {
 0u, 3u, 129u, 6u, 10u, 16u, 30u, 1u, 192u, 9u, 12u, 65u, 129u, 141u,
66u, 96u, 56u, 73u, 0u,};
static unsigned char uvector__00016[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 66u, 4u, 69u, 8u, 15u, 0u, 224u, 4u, 145u,
194u, 67u, 3u, 26u, 166u, 7u, 68u, 112u, 112u, 192u, 152u, 14u, 17u,
192u, 131u, 2u, 96u, 56u, 71u, 0u, 12u, 9u, 128u, 225u, 32u,};
static ScmObj libsyssys_getlogin(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getlogin__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getlogin, NULL, NULL);

static ScmObj libsyssys_link(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_link__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_link, NULL, NULL);

static ScmObj libsyssys_pause(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_pause__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_pause, NULL, NULL);

static ScmObj libsyssys_alarm(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_alarm__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_alarm, NULL, NULL);

static ScmObj libsyssys_pipe(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_pipe__STUB, 0, 1,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_pipe, NULL, NULL);

static ScmObj libsyssys_close(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_close__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_close, NULL, NULL);

static ScmObj libsyssys_mkdir(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_mkdir__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_mkdir, NULL, NULL);

static ScmObj libsyssys_rmdir(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_rmdir__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_rmdir, NULL, NULL);

static ScmObj libsyssys_umask(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_umask__STUB, 0, 2,SCM_FALSE,libsyssys_umask, NULL, NULL);

static ScmObj libsyssys_sleep(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_sleep__STUB, 1, 2,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_sleep, NULL, NULL);

#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
static ScmObj libsyssys_nanosleep(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_nanosleep__STUB, 1, 2,SCM_FALSE,libsyssys_nanosleep, NULL, NULL);

#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
static ScmObj libsyssys_unlink(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_unlink__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_unlink, NULL, NULL);

static ScmObj libsyssys_isatty(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_isatty__STUB, 1, 0,SCM_FALSE,libsyssys_isatty, NULL, NULL);

static ScmObj libsyssys_ttyname(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_ttyname__STUB, 1, 0,SCM_FALSE,libsyssys_ttyname, NULL, NULL);

static ScmObj libsyssys_truncate(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_truncate__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_truncate, NULL, NULL);

static ScmObj libsyssys_ftruncate(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_ftruncate__STUB, 2, 0,SCM_FALSE,libsyssys_ftruncate, NULL, NULL);

#if defined(HAVE_CRYPT)
static ScmObj libsyssys_crypt(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_crypt__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_crypt, NULL, NULL);

#endif /* defined(HAVE_CRYPT) */
#if !(defined(HOSTNAMELEN))

#endif /* !(defined(HOSTNAMELEN)) */
static ScmObj libsyssys_gethostname(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_gethostname__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_gethostname, NULL, NULL);

static ScmObj libsyssys_getdomainname(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_getdomainname__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_getdomainname, NULL, NULL);

#if defined(HAVE_SYMLINK)
static ScmObj libsyssys_symlink(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_symlink__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_symlink, NULL, NULL);

#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_READLINK)
static ScmObj libsyssys_readlink(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_readlink__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_readlink, NULL, NULL);

#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_SELECT)
static ScmObj libsyssys_fdset_ref(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_fdset_ref__STUB, 2, 0,SCM_FALSE,libsyssys_fdset_ref, NULL, NULL);

#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
static ScmObj libsyssys_fdset_setX(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_fdset_setX__STUB, 3, 0,SCM_FALSE,libsyssys_fdset_setX, NULL, NULL);

#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
static ScmObj libsyssys_fdset_max_fd(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_fdset_max_fd__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_fdset_max_fd, NULL, NULL);

#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
static ScmObj libsyssys_fdset_clearX(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_fdset_clearX__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_fdset_clearX, NULL, NULL);

#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
static ScmObj libsyssys_fdset_copyX(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_fdset_copyX__STUB, 2, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_fdset_copyX, NULL, NULL);

#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
static ScmObj libsyssys_select(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_select__STUB, 3, 2,SCM_FALSE,libsyssys_select, NULL, NULL);

#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
static ScmObj libsyssys_selectX(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_selectX__STUB, 3, 2,SCM_FALSE,libsyssys_selectX, NULL, NULL);

#endif /* defined(HAVE_SELECT) */
static ScmObj libsyssys_available_processors(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(libsyssys_available_processors__STUB, 0, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, libsyssys_available_processors, NULL, NULL);

static unsigned char uvector__00017[] = {
 0u, 80u,};
static unsigned char uvector__00018[] = {
 0u, 3u, 131u, 134u, 7u, 90u, 16u, 58u, 16u, 71u, 48u, 194u, 166u,
132u, 20u, 145u, 193u, 131u, 2u, 80u, 130u, 145u, 192u, 195u, 2u, 57u,
145u, 192u, 131u, 2u, 80u, 130u, 17u, 192u, 3u, 2u, 80u, 129u, 144u,
196u, 24u, 24u, 197u, 8u, 34u, 80u, 130u, 132u, 192u, 112u, 146u,};
static unsigned char uvector__00019[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 66u, 13u, 66u, 8u, 148u, 32u, 161u, 214u,
132u, 14u, 132u, 17u, 204u, 48u, 169u, 161u, 5u, 36u, 142u, 18u, 24u,
24u, 194u, 96u, 110u, 152u, 45u, 145u, 193u, 195u, 2u, 96u, 56u, 71u,
2u, 12u, 9u, 128u, 225u, 28u, 0u, 48u, 38u, 3u, 132u, 128u,};
static unsigned char uvector__00020[] = {
 0u, 3u, 134u, 6u, 6u, 106u, 16u, 122u, 16u, 98u, 71u, 9u, 12u, 9u,
66u, 12u, 71u, 7u, 12u, 9u, 128u, 225u, 28u, 8u, 48u, 38u, 3u, 132u,
112u, 0u, 192u, 152u, 14u, 18u,};
static unsigned char uvector__00021[] = {
 0u, 3u, 131u, 6u, 10u, 16u, 138u, 16u, 154u, 16u, 170u, 16u, 184u,
126u, 132u, 50u, 132u, 52u, 145u, 193u, 67u, 2u, 80u, 134u, 145u,
193u, 3u, 2u, 80u, 134u, 17u, 192u, 195u, 2u, 67u, 228u, 112u, 32u,
192u, 148u, 33u, 100u, 112u, 16u, 192u, 148u, 33u, 68u, 112u, 0u,
192u, 148u, 33u, 36u, 49u, 6u, 6u, 49u, 66u, 19u, 66u, 22u, 66u, 96u,
56u, 73u, 0u,};
static unsigned char uvector__00022[] = {
 0u, 3u, 194u, 180u, 24u, 40u, 67u, 131u, 229u, 8u, 121u, 161u, 16u,
161u, 17u, 161u, 18u, 161u, 19u, 36u, 120u, 86u, 67u, 2u, 80u, 137u,
145u, 225u, 88u, 12u, 9u, 66u, 36u, 71u, 133u, 92u, 48u, 37u, 8u,
137u, 30u, 21u, 96u, 192u, 148u, 34u, 4u, 120u, 85u, 67u, 2u, 96u,
106u, 71u, 133u, 80u, 48u, 37u, 8u, 121u, 30u, 21u, 32u, 193u, 2u,
192u, 193u, 162u, 132u, 60u, 146u, 60u, 42u, 33u, 129u, 50u, 14u, 35u,
194u, 160u, 24u, 18u, 132u, 60u, 143u, 10u, 96u, 96u, 76u, 125u, 200u,
240u, 164u, 134u, 10u, 16u, 224u, 249u, 66u, 30u, 104u, 68u, 40u, 68u,
104u, 68u, 131u, 189u, 8u, 121u, 36u, 120u, 82u, 3u, 2u, 102u, 154u,
71u, 133u, 28u, 48u, 37u, 8u, 121u, 30u, 20u, 96u, 192u, 148u, 34u,
68u, 120u, 81u, 67u, 2u, 80u, 136u, 145u, 225u, 68u, 12u, 9u, 66u,
32u, 71u, 133u, 12u, 48u, 38u, 95u, 228u, 120u, 80u, 131u, 2u, 80u,
135u, 145u, 225u, 31u, 12u, 2u, 132u, 80u, 132u, 203u, 176u, 145u,
225u, 30u, 12u, 9u, 4u, 17u, 225u, 27u, 12u, 20u, 33u, 193u, 242u,
132u, 60u, 208u, 136u, 80u, 136u, 135u, 122u, 16u, 243u, 66u, 38u,
72u, 240u, 141u, 6u, 4u, 161u, 19u, 35u, 194u, 50u, 24u, 19u, 66u,
229u, 8u, 240u, 140u, 6u, 4u, 161u, 15u, 35u, 194u, 46u, 24u, 18u,
132u, 68u, 143u, 8u, 176u, 96u, 74u, 17u, 2u, 60u, 34u, 161u, 129u,
52u, 44u, 88u, 143u, 8u, 160u, 96u, 74u, 16u, 242u, 60u, 34u, 33u,
128u, 80u, 138u, 144u, 154u, 22u, 6u, 72u, 240u, 136u, 6u, 4u, 130u,
8u, 240u, 134u, 134u, 10u, 16u, 224u, 249u, 66u, 30u, 104u, 68u, 3u,
189u, 8u, 121u, 161u, 18u, 161u, 19u, 36u, 120u, 67u, 3u, 2u, 80u,
137u, 145u, 225u, 11u, 12u, 9u, 66u, 36u, 71u, 132u, 40u, 48u, 38u,
136u, 102u, 145u, 225u, 9u, 12u, 9u, 66u, 30u, 71u, 132u, 32u, 48u,
37u, 8u, 129u, 30u, 16u, 112u, 192u, 154u, 32u, 190u, 71u, 132u, 24u,
48u, 37u, 8u, 121u, 30u, 16u, 48u, 192u, 40u, 69u, 136u, 77u, 16u,
76u, 36u, 120u, 64u, 131u, 2u, 65u, 4u, 119u, 240u, 193u, 66u, 28u,
31u, 40u, 67u, 200u, 119u, 161u, 15u, 52u, 34u, 52u, 34u, 84u, 34u,
100u, 142u, 252u, 24u, 18u, 132u, 76u, 142u, 250u, 24u, 18u, 132u,
72u, 142u, 248u, 24u, 18u, 132u, 68u, 142u, 246u, 24u, 19u, 69u, 128u,
8u, 239u, 65u, 129u, 40u, 67u, 200u, 239u, 33u, 129u, 52u, 87u, 8u,
142u, 240u, 24u, 18u, 132u, 60u, 142u, 234u, 24u, 5u, 8u, 185u, 9u,
162u, 177u, 228u, 142u, 232u, 24u, 18u, 8u, 35u, 185u, 134u, 8u, 44u,
65u, 129u, 162u, 132u, 60u, 144u, 154u, 51u, 4u, 19u, 69u, 35u, 194u,
104u, 122u, 48u, 77u, 10u, 3u, 13u, 33u, 232u, 38u, 62u, 225u, 48u,
34u, 36u, 145u, 220u, 67u, 2u, 104u, 212u, 73u, 29u, 192u, 48u, 38u,
141u, 75u, 17u, 219u, 195u, 2u, 80u, 135u, 145u, 219u, 67u, 2u, 104u,
212u, 73u, 29u, 168u, 48u, 51u, 193u, 180u, 33u, 228u, 142u, 210u,
24u, 18u, 132u, 60u, 142u, 202u, 24u, 26u, 195u, 85u, 8u, 121u, 36u,
118u, 64u, 192u, 154u, 62u, 26u, 71u, 99u, 12u, 9u, 66u, 30u, 71u,
98u, 12u, 9u, 10u, 145u, 216u, 3u, 5u, 8u, 196u, 42u, 72u, 235u, 225u,
129u, 40u, 70u, 8u, 235u, 193u, 129u, 33u, 82u, 58u, 224u, 96u, 101u,
161u, 24u, 9u, 164u, 18u, 176u, 169u, 35u, 173u, 134u, 4u, 161u, 24u,
35u, 173u, 6u, 6u, 64u, 33u, 81u, 66u, 50u, 40u, 70u, 168u, 66u, 40u,
70u, 232u, 66u, 168u, 70u, 225u, 250u, 16u, 202u, 16u, 211u, 66u, 56u,
40u, 70u, 104u, 71u, 67u, 10u, 144u, 202u, 40u, 71u, 168u, 65u, 13u,
8u, 32u, 125u, 161u, 4u, 36u, 146u, 66u, 105u, 14u, 65u, 35u, 172u,
6u, 4u, 210u, 48u, 242u, 58u, 176u, 96u, 77u, 36u, 82u, 35u, 170u,
134u, 4u, 210u, 84u, 130u, 58u, 160u, 96u, 74u, 16u, 66u, 58u, 136u,
96u, 74u, 16u, 66u, 58u, 120u, 96u, 77u, 36u, 199u, 35u, 166u, 134u,
4u, 210u, 77u, 34u, 58u, 96u, 96u, 74u, 16u, 66u, 58u, 80u, 96u, 77u,
36u, 210u, 35u, 163u, 134u, 4u, 161u, 29u, 35u, 162u, 134u, 4u, 210u,
69u, 34u, 58u, 32u, 96u, 74u, 17u, 194u, 58u, 8u, 96u, 99u, 20u, 33u,
52u, 33u, 100u, 80u, 132u, 80u, 132u, 208u, 133u, 80u, 133u, 195u,
244u, 33u, 148u, 33u, 164u, 145u, 207u, 195u, 2u, 105u, 24u, 121u,
28u, 244u, 48u, 37u, 8u, 153u, 28u, 224u, 48u, 50u, 137u, 22u, 132u,
76u, 183u, 80u, 137u, 146u, 57u, 176u, 96u, 77u, 45u, 20u, 35u, 154u,
134u, 4u, 161u, 19u, 35u, 153u, 134u, 4u, 210u, 209u, 66u, 57u, 136u,
96u, 74u, 17u, 34u, 57u, 104u, 96u, 74u, 17u, 242u, 57u, 88u, 96u,
101u, 18u, 45u, 8u, 145u, 161u, 31u, 161u, 18u, 36u, 114u, 144u, 192u,
154u, 95u, 150u, 71u, 40u, 12u, 9u, 66u, 36u, 71u, 38u, 12u, 9u, 165u,
249u, 100u, 114u, 64u, 192u, 148u, 34u, 36u, 114u, 0u, 192u, 148u,
40u, 4u, 113u, 224u, 192u, 202u, 36u, 90u, 17u, 19u, 66u, 129u, 66u,
34u, 72u, 227u, 129u, 129u, 52u, 202u, 40u, 142u, 54u, 24u, 18u, 132u,
68u, 142u, 50u, 24u, 19u, 76u, 162u, 136u, 226u, 225u, 129u, 40u, 68u,
8u, 226u, 33u, 129u, 148u, 72u, 180u, 34u, 6u, 133u, 6u, 132u, 64u,
145u, 195u, 195u, 2u, 105u, 166u, 201u, 28u, 56u, 48u, 37u, 8u, 129u,
28u, 48u, 48u, 38u, 154u, 108u, 145u, 194u, 131u, 3u, 93u, 8u, 121u,
35u, 132u, 134u, 4u, 161u, 15u, 35u, 128u, 6u, 4u, 161u, 66u, 33u,
136u, 48u, 49u, 138u, 16u, 74u, 16u, 170u, 17u, 201u, 28u, 67u, 244u,
40u, 36u, 80u, 134u, 80u, 160u, 17u, 66u, 49u, 66u, 62u, 69u, 8u,
107u, 113u, 33u, 182u, 21u, 9u, 164u, 97u, 225u, 52u, 135u, 32u, 146u,
64u,};
static unsigned char uvector__00023[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 66u, 7u, 66u, 9u, 66u, 21u, 66u, 57u, 35u,
136u, 126u, 133u, 4u, 138u, 16u, 202u, 20u, 2u, 40u, 70u, 40u, 71u,
200u, 161u, 13u, 110u, 36u, 54u, 194u, 162u, 132u, 100u, 80u, 141u,
80u, 132u, 80u, 141u, 208u, 133u, 80u, 141u, 195u, 244u, 33u, 148u,
33u, 166u, 132u, 112u, 80u, 140u, 208u, 142u, 134u, 21u, 33u, 148u,
80u, 143u, 80u, 130u, 26u, 16u, 64u, 251u, 66u, 8u, 73u, 33u, 150u,
132u, 96u, 80u, 140u, 66u, 166u, 21u, 36u, 145u, 194u, 67u, 3u, 24u,
76u, 13u, 211u, 15u, 226u, 56u, 56u, 96u, 76u, 7u, 8u, 224u, 65u,
129u, 48u, 28u, 35u, 128u, 6u, 4u, 192u, 112u, 144u,};
static unsigned char uvector__00024[] = {
 0u, 3u, 130u, 134u, 10u, 20u, 58u, 20u, 64u, 213u, 66u, 138u, 104u,
71u, 9u, 28u, 16u, 48u, 37u, 10u, 25u, 28u, 12u, 48u, 37u, 8u, 225u,
28u, 4u, 48u, 38u, 7u, 228u, 112u, 0u, 192u, 148u, 40u, 132u, 49u, 6u,
6u, 49u, 66u, 137u, 66u, 56u, 66u, 96u, 56u, 73u, 0u,};
static unsigned char uvector__00025[] = {
 0u, 3u, 146u, 134u, 10u, 16u, 192u, 228u, 40u, 81u, 40u, 71u, 8u,
161u, 67u, 161u, 68u, 13u, 84u, 40u, 166u, 132u, 112u, 154u, 17u,
202u, 20u, 64u, 209u, 66u, 138u, 95u, 137u, 28u, 144u, 48u, 37u, 8u,
97u, 28u, 128u, 48u, 38u, 26u, 228u, 113u, 240u, 192u, 148u, 40u,
132u, 113u, 224u, 192u, 148u, 35u, 132u, 113u, 176u, 192u, 198u, 19u,
3u, 160u, 152u, 48u, 18u, 56u, 200u, 96u, 161u, 12u, 161u, 10u, 161u,
28u, 161u, 68u, 13u, 20u, 40u, 165u, 184u, 145u, 198u, 3u, 2u, 80u,
134u, 17u, 197u, 67u, 2u, 100u, 214u, 71u, 20u, 12u, 9u, 66u, 136u,
71u, 19u, 12u, 9u, 66u, 56u, 71u, 18u, 12u, 9u, 66u, 20u, 71u, 16u,
12u, 13u, 97u, 170u, 133u, 20u, 146u, 56u, 120u, 96u, 76u, 233u, 136u,
225u, 161u, 130u, 133u, 26u, 133u, 16u, 53u, 80u, 162u, 154u, 17u,
194u, 71u, 12u, 12u, 9u, 66u, 140u, 71u, 11u, 12u, 9u, 66u, 56u, 71u,
9u, 12u, 9u, 161u, 10u, 36u, 112u, 128u, 192u, 148u, 40u, 132u, 112u,
80u, 192u, 246u, 26u, 40u, 81u, 72u, 97u, 161u, 71u, 36u, 142u, 8u,
24u, 19u, 66u, 162u, 136u, 224u, 97u, 129u, 40u, 71u, 8u, 224u, 33u,
129u, 174u, 133u, 20u, 145u, 192u, 3u, 2u, 80u, 162u, 144u, 196u, 24u,
24u, 197u, 10u, 37u, 10u, 45u, 8u, 225u, 13u, 128u, 38u, 134u, 3u,
80u, 142u, 16u, 19u, 66u, 151u, 194u, 104u, 64u, 113u, 1u, 51u, 155u,
9u, 145u, 65u, 13u, 33u, 48u, 28u, 36u, 146u,};
static unsigned char uvector__00026[] = {
 0u, 3u, 130u, 6u, 10u, 20u, 106u, 20u, 138u, 20u, 90u, 20u, 146u,
71u, 3u, 12u, 9u, 66u, 140u, 71u, 2u, 12u, 9u, 66u, 146u, 71u, 1u,
12u, 9u, 66u, 138u, 71u, 0u, 12u, 9u, 66u, 144u, 67u, 16u, 96u, 99u,
20u, 41u, 20u, 41u, 36u, 38u, 3u, 132u, 144u,};
static unsigned char uvector__00027[] = {
 0u, 3u, 138u, 134u, 10u, 17u, 145u, 66u, 53u, 66u, 141u, 66u, 55u,
66u, 139u, 66u, 54u, 69u, 10u, 29u, 10u, 37u, 10u, 45u, 8u, 225u, 20u,
33u, 142u, 97u, 133u, 77u, 10u, 37u, 10u, 83u, 241u, 36u, 113u, 64u,
192u, 152u, 101u, 145u, 196u, 195u, 2u, 80u, 134u, 17u, 195u, 131u,
2u, 80u, 162u, 17u, 194u, 195u, 2u, 57u, 145u, 194u, 67u, 2u, 97u,
150u, 71u, 8u, 12u, 9u, 132u, 41u, 28u, 28u, 48u, 37u, 10u, 25u, 28u,
24u, 48u, 37u, 8u, 225u, 28u, 20u, 48u, 37u, 10u, 41u, 28u, 16u, 48u,
37u, 10u, 33u, 28u, 8u, 48u, 38u, 16u, 164u, 112u, 0u, 192u, 198u,
40u, 82u, 40u, 82u, 72u, 161u, 70u, 161u, 72u, 161u, 69u, 161u, 73u,
36u, 134u, 32u, 192u, 198u, 40u, 81u, 40u, 81u, 104u, 71u, 8u, 76u,
7u, 9u, 32u,};
static unsigned char uvector__00028[] = {
 0u, 3u, 136u, 6u, 10u, 20u, 48u, 209u, 66u, 150u, 67u, 85u, 10u, 89u,
161u, 28u, 36u, 112u, 240u, 192u, 148u, 40u, 100u, 112u, 224u, 192u,
148u, 35u, 132u, 112u, 192u, 192u, 152u, 38u, 145u, 194u, 131u, 2u,
96u, 94u, 71u, 9u, 12u, 12u, 128u, 80u, 165u, 138u, 20u, 202u, 20u,
216u, 126u, 132u, 52u, 146u, 19u, 1u, 194u, 71u, 7u, 12u, 9u, 139u,
161u, 28u, 24u, 48u, 37u, 8u, 105u, 28u, 20u, 48u, 36u, 62u, 71u, 4u,
12u, 9u, 66u, 154u, 71u, 2u, 12u, 9u, 139u, 161u, 12u, 65u, 129u,
140u, 80u, 166u, 208u, 133u, 80u, 142u, 67u, 244u, 33u, 148u, 33u,
164u, 51u, 10u, 20u, 58u, 20u, 74u, 20u, 90u, 17u, 194u, 27u, 0u,
107u, 161u, 69u, 52u, 35u, 132u, 7u, 176u, 209u, 66u, 138u, 67u, 13u,
10u, 57u, 34u, 133u, 26u, 133u, 16u, 53u, 80u, 162u, 154u, 17u, 194u,
64u, 107u, 13u, 84u, 40u, 164u, 138u, 16u, 202u, 16u, 170u, 17u, 202u,
20u, 64u, 209u, 66u, 138u, 91u, 137u, 13u, 34u, 132u, 48u, 57u, 10u,
20u, 74u, 17u, 194u, 40u, 80u, 232u, 81u, 3u, 85u, 10u, 41u, 161u,
28u, 38u, 132u, 114u, 133u, 16u, 52u, 80u, 162u, 151u, 226u, 73u, 12u,
194u, 133u, 26u, 133u, 18u, 133u, 22u, 132u, 112u, 138u, 17u, 145u,
66u, 53u, 66u, 141u, 66u, 55u, 66u, 139u, 66u, 54u, 69u, 10u, 29u,
10u, 37u, 10u, 45u, 8u, 225u, 20u, 33u, 142u, 97u, 133u, 77u, 10u,
37u, 10u, 83u, 241u, 36u, 54u, 208u, 165u, 132u, 197u, 208u, 38u, 3u,
132u, 146u,};
static unsigned char uvector__00029[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 66u, 17u, 66u, 155u, 66u, 21u, 66u, 57u,
15u, 208u, 134u, 80u, 134u, 144u, 204u, 40u, 80u, 232u, 81u, 40u, 81u,
104u, 71u, 8u, 108u, 1u, 174u, 133u, 20u, 208u, 142u, 16u, 30u, 195u,
69u, 10u, 41u, 12u, 52u, 40u, 228u, 138u, 20u, 106u, 20u, 64u, 213u,
66u, 138u, 104u, 71u, 9u, 1u, 172u, 53u, 80u, 162u, 146u, 40u, 67u,
40u, 66u, 168u, 71u, 40u, 81u, 3u, 69u, 10u, 41u, 110u, 36u, 52u,
138u, 16u, 192u, 228u, 40u, 81u, 40u, 71u, 8u, 161u, 67u, 161u, 68u,
13u, 84u, 40u, 166u, 132u, 112u, 154u, 17u, 202u, 20u, 64u, 209u, 66u,
138u, 95u, 137u, 36u, 51u, 10u, 20u, 106u, 20u, 74u, 20u, 90u, 17u,
194u, 40u, 70u, 69u, 8u, 213u, 10u, 53u, 8u, 221u, 10u, 45u, 8u, 217u,
20u, 40u, 116u, 40u, 148u, 40u, 180u, 35u, 132u, 80u, 134u, 57u, 134u,
21u, 52u, 40u, 148u, 41u, 79u, 196u, 144u, 219u, 66u, 150u, 40u, 83u,
40u, 83u, 97u, 250u, 16u, 210u, 40u, 80u, 195u, 69u, 10u, 89u, 13u,
84u, 41u, 102u, 132u, 112u, 146u, 71u, 9u, 12u, 12u, 97u, 48u, 55u,
76u, 37u, 8u, 224u, 225u, 129u, 48u, 28u, 35u, 129u, 6u, 4u, 192u,
112u, 142u, 0u, 24u, 19u, 1u, 194u, 64u,};
static unsigned char uvector__00030[] = {
 0u, 3u, 139u, 134u, 10u, 20u, 234u, 20u, 242u, 71u, 22u, 12u, 9u,
66u, 158u, 71u, 18u, 12u, 2u, 25u, 161u, 79u, 161u, 80u, 33u, 134u,
133u, 28u, 146u, 56u, 128u, 96u, 76u, 35u, 136u, 225u, 161u, 129u,
40u, 83u, 200u, 225u, 97u, 129u, 48u, 142u, 35u, 131u, 134u, 1u, 12u,
208u, 167u, 195u, 68u, 48u, 196u, 4u, 145u, 193u, 67u, 2u, 99u, 14u,
71u, 2u, 12u, 9u, 66u, 158u, 71u, 0u, 12u, 9u, 140u, 57u, 12u, 65u,
129u, 140u, 80u, 167u, 144u, 216u, 19u, 24u, 64u, 152u, 69u, 134u,
144u, 152u, 14u, 18u, 73u, 0u,};
static unsigned char uvector__00031[] = {
 0u, 3u, 147u, 6u, 7u, 58u, 16u, 209u, 44u, 208u, 168u, 194u, 132u,
145u, 201u, 3u, 2u, 96u, 110u, 71u, 35u, 12u, 9u, 10u, 17u, 200u,
131u, 2u, 80u, 169u, 17u, 200u, 3u, 2u, 96u, 110u, 71u, 31u, 12u, 9u,
66u, 26u, 71u, 29u, 12u, 14u, 111u, 194u, 89u, 161u, 81u, 13u, 80u,
161u, 36u, 142u, 54u, 24u, 19u, 23u, 98u, 56u, 208u, 96u, 76u, 100u,
200u, 227u, 33u, 129u, 40u, 84u, 136u, 226u, 225u, 129u, 49u, 118u,
35u, 137u, 134u, 6u, 81u, 12u, 134u, 136u, 80u, 195u, 68u, 38u, 44u,
97u, 48u, 28u, 36u, 113u, 16u, 192u, 153u, 71u, 145u, 195u, 67u, 2u,
101u, 56u, 71u, 11u, 12u, 9u, 148u, 121u, 28u, 40u, 48u, 50u, 1u, 10u,
8u, 66u, 133u, 54u, 31u, 36u, 132u, 202u, 16u, 145u, 194u, 3u, 2u,
103u, 88u, 71u, 7u, 12u, 9u, 15u, 145u, 193u, 131u, 2u, 80u, 166u,
145u, 193u, 3u, 2u, 103u, 88u, 71u, 2u, 12u, 9u, 66u, 164u, 67u, 16u,
96u, 99u, 20u, 41u, 176u, 253u, 8u, 105u, 12u, 194u, 133u, 70u, 133u,
60u, 134u, 192u, 33u, 154u, 20u, 248u, 104u, 134u, 24u, 128u, 144u,
33u, 154u, 20u, 250u, 21u, 2u, 24u, 104u, 81u, 201u, 13u, 34u, 133u,
58u, 133u, 60u, 146u, 67u, 108u, 40u, 19u, 58u, 192u, 153u, 66u, 18u,
72u,};
static unsigned char uvector__00032[] = {
 0u, 3u, 135u, 6u, 6u, 97u, 66u, 153u, 66u, 155u, 15u, 208u, 134u,
144u, 204u, 40u, 84u, 104u, 83u, 200u, 108u, 2u, 25u, 161u, 79u, 134u,
136u, 97u, 136u, 9u, 2u, 25u, 161u, 79u, 161u, 80u, 33u, 134u, 133u,
28u, 144u, 210u, 40u, 83u, 168u, 83u, 201u, 36u, 54u, 194u, 130u, 16u,
161u, 77u, 135u, 200u, 101u, 16u, 200u, 104u, 133u, 12u, 52u, 67u,
155u, 240u, 150u, 104u, 84u, 67u, 84u, 40u, 73u, 14u, 116u, 33u, 162u,
89u, 161u, 81u, 133u, 9u, 36u, 145u, 194u, 195u, 3u, 24u, 76u, 13u,
211u, 6u, 66u, 56u, 72u, 96u, 76u, 7u, 8u, 224u, 129u, 129u, 48u, 28u,
35u, 129u, 6u, 4u, 192u, 112u, 144u,};
static unsigned char uvector__00033[] = {
 0u, 3u, 135u, 6u, 10u, 21u, 58u, 21u, 74u, 17u, 194u, 71u, 13u, 12u,
9u, 66u, 56u, 71u, 12u, 12u, 9u, 66u, 168u, 71u, 11u, 12u, 9u, 66u,
168u, 71u, 9u, 12u, 13u, 116u, 35u, 132u, 142u, 16u, 24u, 18u, 132u,
112u, 142u, 14u, 24u, 25u, 0u, 161u, 84u, 16u, 133u, 10u, 173u, 10u,
177u, 36u, 50u, 132u, 195u, 238u, 133u, 80u, 38u, 3u, 132u, 145u,
193u, 67u, 2u, 99u, 6u, 71u, 2u, 12u, 9u, 66u, 170u, 71u, 0u, 12u, 9u,
140u, 25u, 12u, 65u, 129u, 140u, 80u, 170u, 208u, 142u, 16u, 219u,
66u, 168u, 19u, 24u, 48u, 152u, 220u, 146u, 72u,};
static unsigned char uvector__00034[] = {
 0u, 3u, 132u, 6u, 7u, 49u, 19u, 208u, 171u, 208u, 172u, 26u, 17u,
194u, 71u, 7u, 12u, 9u, 66u, 56u, 71u, 4u, 12u, 9u, 129u, 57u, 28u,
12u, 48u, 37u, 10u, 193u, 28u, 8u, 48u, 37u, 10u, 185u, 28u, 0u, 48u,
38u, 4u, 228u, 49u, 6u, 6u, 49u, 66u, 177u, 66u, 56u, 66u, 96u, 56u,
73u, 0u,};
static unsigned char uvector__00035[] = {
 0u, 3u, 130u, 134u, 10u, 17u, 144u, 228u, 40u, 86u, 40u, 71u, 8u,
115u, 17u, 61u, 10u, 189u, 10u, 193u, 161u, 28u, 38u, 132u, 114u,
133u, 100u, 145u, 193u, 3u, 2u, 80u, 172u, 145u, 192u, 195u, 2u, 80u,
142u, 17u, 192u, 3u, 3u, 24u, 76u, 14u, 130u, 96u, 192u, 72u, 98u,
12u, 12u, 98u, 133u, 94u, 132u, 112u, 132u, 192u, 112u, 146u,};
static unsigned char uvector__00036[] = {
 0u, 3u, 129u, 134u, 10u, 21u, 170u, 21u, 186u, 21u, 194u, 71u, 2u,
12u, 9u, 66u, 180u, 71u, 1u, 12u, 9u, 66u, 184u, 71u, 0u, 12u, 9u,
66u, 182u, 67u, 16u, 96u, 99u, 20u, 43u, 100u, 38u, 3u, 132u, 144u,};
static unsigned char uvector__00037[] = {
 0u, 3u, 129u, 134u, 10u, 21u, 218u, 21u, 234u, 21u, 242u, 71u, 2u,
12u, 9u, 66u, 186u, 71u, 1u, 12u, 9u, 66u, 190u, 71u, 0u, 12u, 9u,
66u, 188u, 67u, 16u, 96u, 99u, 20u, 43u, 196u, 38u, 3u, 132u, 144u,};
static unsigned char uvector__00038[] = {
 0u, 3u, 129u, 134u, 10u, 24u, 10u, 24u, 26u, 24u, 34u, 71u, 2u, 12u,
9u, 67u, 0u, 71u, 1u, 12u, 9u, 67u, 6u, 71u, 0u, 12u, 9u, 67u, 2u,
67u, 16u, 96u, 99u, 20u, 48u, 36u, 38u, 3u, 132u, 144u,};
static unsigned char uvector__00039[] = {
 0u, 3u, 194u, 36u, 24u, 25u, 232u, 97u, 40u, 83u, 73u, 30u, 17u, 16u,
192u, 148u, 41u, 164u, 120u, 67u, 131u, 5u, 12u, 40u, 150u, 69u, 12u,
52u, 79u, 66u, 55u, 67u, 6u, 104u, 85u, 11u, 113u, 35u, 194u, 20u,
24u, 19u, 11u, 130u, 60u, 33u, 33u, 129u, 40u, 85u, 8u, 240u, 130u,
134u, 6u, 49u, 67u, 2u, 69u, 12u, 5u, 12u, 13u, 12u, 17u, 36u, 120u,
64u, 131u, 2u, 68u, 228u, 120u, 64u, 3u, 5u, 12u, 56u, 20u, 48u, 17u,
57u, 20u, 48u, 84u, 48u, 100u, 134u, 52u, 198u, 208u, 145u, 223u,
131u, 2u, 100u, 170u, 71u, 124u, 12u, 9u, 133u, 193u, 29u, 232u, 48u,
80u, 196u, 80u, 196u, 240u, 2u, 71u, 121u, 12u, 9u, 67u, 18u, 71u,
120u, 12u, 20u, 48u, 162u, 132u, 102u, 63u, 12u, 42u, 68u, 178u, 40u,
97u, 162u, 122u, 17u, 177u, 23u, 6u, 26u, 24u, 162u, 77u, 10u, 161u,
34u, 46u, 12u, 49u, 121u, 36u, 142u, 238u, 24u, 19u, 66u, 65u, 136u,
238u, 193u, 129u, 32u, 130u, 59u, 144u, 96u, 77u, 9u, 6u, 35u, 184u,
6u, 4u, 207u, 88u, 142u, 220u, 24u, 19u, 66u, 1u, 72u, 237u, 161u,
129u, 40u, 85u, 8u, 237u, 33u, 129u, 140u, 80u, 175u, 17u, 66u, 187u,
66u, 189u, 66u, 190u, 73u, 29u, 156u, 48u, 38u, 132u, 47u, 145u, 217u,
131u, 2u, 65u, 4u, 118u, 32u, 192u, 154u, 16u, 190u, 71u, 95u, 12u,
9u, 19u, 145u, 215u, 67u, 5u, 12u, 56u, 20u, 43u, 177u, 57u, 20u, 43u,
225u, 52u, 33u, 124u, 144u, 198u, 154u, 23u, 172u, 72u, 235u, 97u,
129u, 52u, 56u, 116u, 142u, 178u, 24u, 19u, 66u, 1u, 72u, 234u, 225u,
129u, 40u, 98u, 200u, 234u, 161u, 129u, 51u, 214u, 35u, 169u, 6u, 6u,
122u, 24u, 202u, 20u, 210u, 71u, 81u, 12u, 9u, 66u, 154u, 71u, 77u,
12u, 20u, 49u, 20u, 49u, 60u, 0u, 145u, 211u, 3u, 2u, 80u, 196u, 145u,
210u, 131u, 3u, 140u, 168u, 40u, 70u, 67u, 144u, 161u, 87u, 161u, 28u,
34u, 132u, 100u, 57u, 10u, 21u, 138u, 17u, 194u, 28u, 196u, 79u, 66u,
175u, 66u, 176u, 104u, 71u, 9u, 161u, 28u, 161u, 89u, 36u, 48u, 169u,
18u, 200u, 161u, 134u, 137u, 232u, 70u, 196u, 92u, 24u, 104u, 98u,
137u, 52u, 42u, 132u, 146u, 58u, 72u, 96u, 71u, 18u, 58u, 48u, 96u,
77u, 19u, 18u, 35u, 162u, 6u, 4u, 209u, 84u, 50u, 58u, 24u, 96u, 74u,
21u, 66u, 57u, 248u, 96u, 99u, 20u, 43u, 100u, 80u, 173u, 80u, 173u,
208u, 174u, 18u, 71u, 61u, 12u, 9u, 162u, 179u, 164u, 115u, 192u,
192u, 144u, 65u, 28u, 224u, 48u, 38u, 138u, 206u, 145u, 205u, 67u, 2u,
68u, 228u, 115u, 48u, 193u, 67u, 14u, 5u, 10u, 212u, 78u, 69u, 10u,
224u, 77u, 21u, 157u, 36u, 49u, 166u, 140u, 4u, 18u, 57u, 136u, 96u,
77u, 26u, 79u, 35u, 151u, 134u, 4u, 209u, 84u, 50u, 57u, 88u, 96u,
99u, 9u, 162u, 106u, 1u, 52u, 78u, 88u, 145u, 202u, 67u, 2u, 104u,
152u, 145u, 28u, 160u, 48u, 36u, 168u, 71u, 39u, 12u, 18u, 168u, 161u,
89u, 149u, 8u, 161u, 141u, 17u, 112u, 97u, 139u, 201u, 12u, 34u, 26u,
36u, 58u, 80u, 196u, 240u, 18u, 66u, 104u, 150u, 233u, 35u, 147u, 6u,
4u, 209u, 228u, 130u, 57u, 40u, 96u, 74u, 24u, 210u, 57u, 32u, 96u,
77u, 31u, 1u, 35u, 144u, 6u, 4u, 209u, 229u, 178u, 56u, 248u, 96u,
72u, 32u, 142u, 54u, 24u, 19u, 71u, 150u, 200u, 227u, 33u, 129u, 52u,
121u, 32u, 142u, 46u, 24u, 4u, 50u, 34u, 238u, 0u, 104u, 99u, 136u,
77u, 29u, 219u, 36u, 113u, 80u, 192u, 154u, 69u, 10u, 71u, 17u, 12u,
9u, 164u, 82u, 68u, 113u, 0u, 192u, 144u, 65u, 28u, 52u, 48u, 38u,
145u, 73u, 17u, 194u, 195u, 2u, 105u, 20u, 41u, 28u, 36u, 48u, 8u,
90u, 134u, 62u, 134u, 12u, 208u, 200u, 10u, 25u, 16u, 216u, 19u, 72u,
160u, 128u, 77u, 18u, 3u, 9u, 162u, 33u, 132u, 52u, 132u, 207u, 12u,
146u, 72u, 225u, 1u, 129u, 32u, 130u, 56u, 56u, 96u, 77u, 37u, 81u,
35u, 130u, 134u, 4u, 210u, 85u, 66u, 56u, 32u, 96u, 74u, 24u, 50u,
56u, 0u, 96u, 77u, 37u, 84u, 33u, 136u, 48u, 50u, 56u, 129u, 67u, 7u,
67u, 6u, 69u, 10u, 165u, 12u, 145u, 33u, 176u, 38u, 146u, 168u, 128u,
153u, 164u, 132u, 194u, 148u, 134u, 144u, 152u, 17u, 18u, 73u, 0u,};
static unsigned char uvector__00040[] = {
 0u, 3u, 130u, 6u, 4u, 113u, 35u, 129u, 134u, 4u, 161u, 146u, 35u,
129u, 6u, 4u, 161u, 131u, 35u, 128u, 6u, 6u, 71u, 16u, 40u, 96u, 232u,
96u, 200u, 161u, 84u, 161u, 146u, 36u, 54u, 1u, 11u, 80u, 199u, 208u,
193u, 154u, 25u, 1u, 67u, 34u, 27u, 0u, 134u, 68u, 93u, 192u, 13u,
12u, 113u, 18u, 168u, 161u, 89u, 149u, 8u, 161u, 141u, 17u, 112u, 97u,
139u, 201u, 12u, 34u, 26u, 36u, 58u, 80u, 196u, 240u, 18u, 67u, 140u,
168u, 40u, 70u, 67u, 144u, 161u, 87u, 161u, 28u, 34u, 132u, 100u, 57u,
10u, 21u, 138u, 17u, 194u, 28u, 196u, 79u, 66u, 175u, 66u, 176u, 104u,
71u, 9u, 161u, 28u, 161u, 89u, 36u, 48u, 169u, 18u, 200u, 161u, 134u,
137u, 232u, 70u, 196u, 92u, 24u, 104u, 98u, 137u, 52u, 42u, 132u,
146u, 64u, 161u, 136u, 161u, 137u, 224u, 4u, 51u, 208u, 198u, 80u,
166u, 146u, 26u, 69u, 12u, 40u, 161u, 25u, 143u, 195u, 10u, 145u, 44u,
138u, 24u, 104u, 158u, 132u, 108u, 69u, 193u, 134u, 134u, 40u, 147u,
66u, 168u, 72u, 139u, 131u, 12u, 94u, 73u, 36u, 144u, 40u, 98u, 40u,
98u, 120u, 1u, 20u, 48u, 162u, 89u, 20u, 48u, 209u, 61u, 8u, 221u,
12u, 25u, 161u, 84u, 45u, 196u, 134u, 144u, 207u, 67u, 9u, 66u, 154u,
73u, 36u, 49u, 6u, 6u, 49u, 67u, 7u, 67u, 37u, 67u, 18u, 66u, 97u,
72u, 73u, 0u,};
static unsigned char uvector__00041[] = {
 0u, 3u, 143u, 6u, 7u, 58u, 20u, 218u, 17u, 194u, 71u, 29u, 12u, 9u,
66u, 56u, 71u, 28u, 12u, 9u, 66u, 154u, 71u, 26u, 12u, 20u, 42u, 98u,
134u, 76u, 80u, 198u, 208u, 166u, 134u, 17u, 13u, 19u, 192u, 15u, 0u,
52u, 35u, 132u, 142u, 50u, 24u, 18u, 132u, 112u, 142u, 46u, 24u, 18u,
134u, 80u, 142u, 42u, 24u, 37u, 81u, 67u, 40u, 148u, 50u, 161u, 48u,
233u, 161u, 148u, 36u, 113u, 64u, 192u, 152u, 116u, 145u, 196u, 195u,
2u, 80u, 198u, 145u, 195u, 195u, 2u, 80u, 166u, 145u, 195u, 67u, 2u,
97u, 210u, 71u, 11u, 12u, 12u, 162u, 86u, 161u, 77u, 161u, 150u, 33u,
48u, 195u, 9u, 128u, 225u, 35u, 132u, 134u, 4u, 204u, 56u, 142u, 12u,
24u, 18u, 133u, 52u, 142u, 8u, 24u, 19u, 48u, 226u, 56u, 16u, 96u,
74u, 24u, 178u, 24u, 131u, 3u, 24u, 161u, 77u, 161u, 28u, 33u, 152u,
80u, 198u, 208u, 193u, 208u, 201u, 80u, 196u, 144u, 200u, 226u, 5u,
12u, 29u, 12u, 25u, 20u, 42u, 148u, 50u, 68u, 134u, 192u, 33u, 106u,
24u, 250u, 24u, 51u, 67u, 32u, 40u, 100u, 67u, 96u, 16u, 200u, 139u,
184u, 1u, 161u, 142u, 34u, 85u, 20u, 43u, 50u, 161u, 20u, 49u, 162u,
46u, 12u, 49u, 121u, 33u, 132u, 67u, 68u, 135u, 74u, 24u, 158u, 2u,
72u, 113u, 149u, 5u, 8u, 200u, 114u, 20u, 42u, 244u, 35u, 132u, 80u,
140u, 135u, 33u, 66u, 177u, 66u, 56u, 67u, 152u, 137u, 232u, 85u,
232u, 86u, 13u, 8u, 225u, 52u, 35u, 148u, 43u, 36u, 134u, 21u, 34u,
89u, 20u, 48u, 209u, 61u, 8u, 216u, 139u, 131u, 13u, 12u, 81u, 38u,
133u, 80u, 146u, 72u, 20u, 49u, 20u, 49u, 60u, 0u, 134u, 122u, 24u,
202u, 20u, 210u, 67u, 72u, 161u, 133u, 20u, 35u, 49u, 248u, 97u, 82u,
37u, 145u, 67u, 13u, 19u, 208u, 141u, 136u, 184u, 48u, 208u, 197u,
18u, 104u, 85u, 9u, 17u, 112u, 97u, 139u, 201u, 36u, 146u, 5u, 12u,
69u, 12u, 79u, 0u, 34u, 134u, 20u, 75u, 34u, 134u, 26u, 39u, 161u,
27u, 161u, 131u, 52u, 42u, 133u, 184u, 144u, 210u, 25u, 232u, 97u,
40u, 83u, 73u, 36u, 134u, 97u, 31u, 208u, 170u, 208u, 142u, 16u, 219u,
66u, 168u, 33u, 10u, 21u, 90u, 21u, 98u, 25u, 67u, 93u, 8u, 225u,
161u, 84u, 20u, 42u, 116u, 42u, 148u, 35u, 132u, 146u, 19u, 48u, 50u,
72u,};
static unsigned char uvector__00042[] = {
 0u, 3u, 135u, 6u, 6u, 97u, 66u, 59u, 66u, 155u, 66u, 56u, 67u, 48u,
161u, 141u, 161u, 131u, 161u, 146u, 161u, 137u, 33u, 145u, 196u, 10u,
24u, 58u, 24u, 50u, 40u, 85u, 40u, 100u, 137u, 13u, 128u, 66u, 212u,
49u, 244u, 48u, 102u, 134u, 64u, 80u, 200u, 134u, 192u, 33u, 145u,
23u, 112u, 3u, 67u, 28u, 68u, 170u, 40u, 86u, 101u, 66u, 40u, 99u,
68u, 92u, 24u, 98u, 242u, 67u, 8u, 134u, 137u, 14u, 148u, 49u, 60u,
4u, 144u, 227u, 42u, 10u, 17u, 144u, 228u, 40u, 85u, 232u, 71u, 8u,
161u, 25u, 14u, 66u, 133u, 98u, 132u, 112u, 135u, 49u, 19u, 208u,
171u, 208u, 172u, 26u, 17u, 194u, 104u, 71u, 40u, 86u, 73u, 12u, 42u,
68u, 178u, 40u, 97u, 162u, 122u, 17u, 177u, 23u, 6u, 26u, 24u, 162u,
77u, 10u, 161u, 36u, 144u, 40u, 98u, 40u, 98u, 120u, 1u, 12u, 244u,
49u, 148u, 41u, 164u, 134u, 145u, 67u, 10u, 40u, 70u, 99u, 240u, 194u,
164u, 75u, 34u, 134u, 26u, 39u, 161u, 27u, 17u, 112u, 97u, 161u, 138u,
36u, 208u, 170u, 18u, 34u, 224u, 195u, 23u, 146u, 73u, 36u, 10u, 24u,
138u, 24u, 158u, 0u, 69u, 12u, 40u, 150u, 69u, 12u, 52u, 79u, 66u,
55u, 67u, 6u, 104u, 85u, 11u, 113u, 33u, 164u, 51u, 208u, 194u, 80u,
166u, 146u, 73u, 12u, 194u, 63u, 161u, 85u, 161u, 28u, 33u, 182u,
133u, 80u, 66u, 20u, 42u, 180u, 42u, 196u, 50u, 134u, 186u, 17u, 195u,
66u, 168u, 40u, 84u, 232u, 85u, 40u, 71u, 9u, 36u, 50u, 137u, 90u,
133u, 54u, 134u, 88u, 138u, 21u, 49u, 67u, 38u, 40u, 99u, 104u, 83u,
67u, 8u, 134u, 137u, 224u, 7u, 128u, 26u, 17u, 194u, 28u, 232u, 83u,
104u, 71u, 9u, 36u, 112u, 176u, 192u, 198u, 19u, 3u, 116u, 193u, 104u,
142u, 18u, 24u, 19u, 1u, 194u, 56u, 32u, 96u, 76u, 7u, 8u, 224u, 65u,
129u, 48u, 28u, 36u,};
static unsigned char uvector__00043[] = {
 0u, 3u, 180u, 134u, 7u, 58u, 25u, 113u, 67u, 48u, 40u, 102u, 77u,
12u, 209u, 36u, 118u, 128u, 192u, 152u, 27u, 145u, 217u, 195u, 2u,
80u, 204u, 17u, 217u, 131u, 2u, 80u, 205u, 17u, 217u, 3u, 2u, 96u,
148u, 71u, 98u, 12u, 9u, 129u, 185u, 29u, 132u, 48u, 37u, 12u, 185u,
29u, 124u, 48u, 57u, 208u, 205u, 138u, 25u, 129u, 67u, 50u, 104u,
102u, 137u, 35u, 175u, 6u, 4u, 199u, 16u, 142u, 186u, 24u, 18u, 134u,
96u, 142u, 184u, 24u, 18u, 134u, 104u, 142u, 180u, 24u, 19u, 29u,
114u, 58u, 192u, 96u, 76u, 113u, 8u, 234u, 225u, 129u, 40u, 102u,
200u, 234u, 193u, 129u, 144u, 10u, 25u, 177u, 67u, 56u, 40u, 103u,
73u, 36u, 38u, 53u, 36u, 142u, 168u, 24u, 19u, 53u, 114u, 58u, 152u,
96u, 76u, 218u, 136u, 234u, 33u, 129u, 51u, 87u, 35u, 167u, 134u, 7u,
49u, 67u, 61u, 67u, 54u, 69u, 12u, 192u, 161u, 153u, 52u, 51u, 68u,
145u, 211u, 131u, 2u, 104u, 70u, 169u, 29u, 52u, 48u, 37u, 12u, 193u,
29u, 48u, 48u, 37u, 12u, 209u, 29u, 40u, 48u, 38u, 132u, 132u, 17u,
210u, 3u, 2u, 104u, 70u, 169u, 29u, 24u, 48u, 38u, 132u, 71u, 145u,
209u, 67u, 2u, 80u, 205u, 145u, 208u, 195u, 2u, 104u, 68u, 121u, 29u,
8u, 48u, 50u, 1u, 67u, 54u, 40u, 103u, 5u, 12u, 233u, 36u, 132u, 208u,
136u, 66u, 71u, 64u, 12u, 9u, 161u, 160u, 68u, 115u, 240u, 192u, 154u,
26u, 42u, 71u, 61u, 12u, 9u, 161u, 160u, 68u, 115u, 192u, 193u, 67u,
50u, 72u, 231u, 33u, 128u, 80u, 207u, 144u, 154u, 30u, 14u, 27u, 104u,
102u, 194u, 104u, 104u, 16u, 77u, 8u, 132u, 36u, 142u, 112u, 24u, 18u,
8u, 35u, 155u, 134u, 8u, 44u, 80u, 224u, 16u, 154u, 30u, 172u, 26u,
67u, 109u, 12u, 216u, 76u, 213u, 194u, 99u, 82u, 73u, 35u, 155u, 6u,
4u, 209u, 17u, 18u, 57u, 152u, 96u, 161u, 193u, 161u, 151u, 161u,
194u, 36u, 115u, 32u, 192u, 148u, 50u, 228u, 115u, 0u, 193u, 67u,
134u, 40u, 113u, 5u, 12u, 192u, 161u, 153u, 52u, 51u, 68u, 146u, 57u,
120u, 96u, 77u, 20u, 13u, 35u, 151u, 6u, 4u, 161u, 152u, 35u, 150u,
134u, 4u, 161u, 154u, 35u, 149u, 134u, 4u, 209u, 68u, 2u, 57u, 72u,
96u, 77u, 20u, 13u, 35u, 146u, 6u, 10u, 28u, 26u, 25u, 122u, 28u, 82u,
71u, 35u, 12u, 9u, 67u, 46u, 71u, 31u, 12u, 20u, 51u, 20u, 50u, 244u,
51u, 68u, 142u, 60u, 24u, 18u, 134u, 96u, 142u, 58u, 24u, 18u, 134u,
104u, 142u, 56u, 24u, 18u, 134u, 92u, 142u, 50u, 24u, 40u, 113u, 133u,
12u, 201u, 161u, 154u, 36u, 113u, 128u, 192u, 148u, 51u, 68u, 113u,
96u, 192u, 154u, 51u, 42u, 71u, 19u, 12u, 2u, 135u, 28u, 132u, 209u,
152u, 34u, 71u, 18u, 12u, 9u, 67u, 46u, 71u, 17u, 12u, 20u, 56u, 194u,
134u, 100u, 208u, 205u, 18u, 56u, 128u, 96u, 74u, 25u, 162u, 56u,
112u, 96u, 77u, 28u, 14u, 35u, 134u, 6u, 4u, 209u, 189u, 178u, 56u,
56u, 96u, 161u, 193u, 161u, 151u, 161u, 199u, 36u, 112u, 96u, 192u,
148u, 50u, 228u, 112u, 32u, 192u, 40u, 114u, 40u, 101u, 200u, 97u,
20u, 57u, 36u, 146u, 56u, 8u, 96u, 77u, 30u, 202u, 35u, 128u, 6u, 4u,
161u, 151u, 33u, 136u, 48u, 49u, 138u, 25u, 122u, 25u, 162u, 27u, 2u,
104u, 246u, 56u, 9u, 163u, 185u, 2u, 135u, 40u, 10u, 28u, 186u, 28u,
194u, 40u, 113u, 2u, 104u, 222u, 217u, 36u, 128u, 154u, 45u, 136u,
40u, 114u, 130u, 104u, 158u, 57u, 32u, 38u, 137u, 100u, 4u, 209u, 16u,
66u, 26u, 66u, 96u, 56u, 73u, 36u,};
static unsigned char uvector__00044[] = {
 0u, 3u, 182u, 134u, 14u, 0u, 221u, 67u, 154u, 40u, 113u, 5u, 14u,
112u, 161u, 153u, 33u, 133u, 73u, 36u, 142u, 216u, 24u, 19u, 2u, 114u,
59u, 80u, 96u, 161u, 152u, 161u, 151u, 161u, 154u, 36u, 118u, 144u,
192u, 148u, 51u, 4u, 118u, 112u, 192u, 148u, 50u, 228u, 118u, 64u,
193u, 67u, 48u, 40u, 102u, 77u, 12u, 209u, 35u, 177u, 134u, 4u, 161u,
152u, 35u, 176u, 6u, 4u, 198u, 136u, 142u, 188u, 24u, 19u, 24u, 242u,
58u, 200u, 96u, 20u, 56u, 164u, 80u, 229u, 1u, 67u, 151u, 67u, 158u,
69u, 14u, 32u, 76u, 99u, 201u, 36u, 142u, 176u, 24u, 18u, 134u, 92u,
142u, 170u, 24u, 5u, 14u, 135u, 0u, 224u, 34u, 135u, 68u, 80u, 229u,
208u, 231u, 145u, 67u, 151u, 67u, 152u, 73u, 20u, 56u, 130u, 134u,
98u, 134u, 94u, 134u, 104u, 146u, 71u, 84u, 12u, 9u, 159u, 49u, 29u,
76u, 48u, 37u, 12u, 193u, 29u, 72u, 48u, 37u, 12u, 209u, 29u, 68u,
48u, 37u, 12u, 185u, 29u, 60u, 48u, 38u, 124u, 196u, 116u, 224u, 192u,
153u, 162u, 145u, 211u, 67u, 2u, 102u, 192u, 71u, 76u, 12u, 9u, 155u,
113u, 28u, 244u, 48u, 80u, 204u, 10u, 25u, 147u, 67u, 52u, 72u, 231u,
129u, 129u, 40u, 102u, 8u, 231u, 97u, 129u, 40u, 102u, 136u, 231u,
33u, 129u, 52u, 46u, 16u, 142u, 110u, 24u, 19u, 66u, 212u, 72u, 229u,
193u, 128u, 80u, 233u, 17u, 67u, 148u, 5u, 14u, 93u, 14u, 121u, 20u,
57u, 116u, 57u, 134u, 135u, 72u, 80u, 226u, 4u, 208u, 181u, 18u, 73u,
35u, 150u, 134u, 4u, 161u, 151u, 35u, 149u, 6u, 10u, 25u, 129u, 67u,
50u, 104u, 102u, 137u, 28u, 164u, 48u, 37u, 12u, 193u, 28u, 160u, 48u,
37u, 12u, 209u, 28u, 152u, 48u, 38u, 135u, 231u, 145u, 201u, 3u, 2u,
104u, 124u, 225u, 28u, 116u, 48u, 10u, 28u, 82u, 40u, 114u, 128u,
161u, 203u, 161u, 207u, 34u, 135u, 46u, 135u, 48u, 138u, 28u, 64u,
154u, 31u, 56u, 73u, 36u, 113u, 192u, 192u, 148u, 50u, 228u, 113u,
144u, 193u, 67u, 166u, 40u, 102u, 77u, 12u, 209u, 35u, 140u, 6u, 4u,
161u, 154u, 35u, 139u, 6u, 4u, 209u, 64u, 162u, 56u, 152u, 96u, 20u,
56u, 228u, 38u, 137u, 235u, 146u, 56u, 144u, 96u, 74u, 25u, 114u, 56u,
136u, 96u, 161u, 211u, 20u, 51u, 38u, 134u, 104u, 145u, 195u, 131u,
2u, 104u, 180u, 25u, 28u, 44u, 48u, 10u, 28u, 114u, 19u, 69u, 148u,
9u, 28u, 40u, 48u, 37u, 12u, 185u, 28u, 36u, 48u, 38u, 10u, 164u,
112u, 128u, 192u, 152u, 52u, 17u, 193u, 131u, 2u, 96u, 170u, 67u, 16u,
96u, 99u, 80u, 204u, 40u, 115u, 168u, 101u, 232u, 102u, 136u, 130u,
232u, 101u, 194u, 104u, 186u, 200u, 76u, 156u, 3u, 72u, 76u, 56u, 9u,
33u, 152u, 80u, 233u, 208u, 203u, 208u, 205u, 17u, 5u, 208u, 203u,
132u, 209u, 85u, 0u, 154u, 35u, 178u, 19u, 67u, 99u, 131u, 72u, 161u,
202u, 9u, 154u, 17u, 36u, 134u, 97u, 67u, 49u, 67u, 47u, 67u, 52u,
67u, 96u, 20u, 57u, 20u, 50u, 228u, 48u, 138u, 28u, 146u, 72u, 20u,
56u, 52u, 50u, 244u, 56u, 228u, 80u, 229u, 1u, 67u, 151u, 67u, 152u,
69u, 14u, 32u, 161u, 198u, 20u, 51u, 38u, 134u, 104u, 146u, 72u, 20u,
56u, 52u, 50u, 244u, 56u, 164u, 80u, 229u, 10u, 28u, 49u, 67u, 136u,
40u, 102u, 5u, 12u, 201u, 161u, 154u, 36u, 146u, 5u, 14u, 13u, 12u,
189u, 14u, 17u, 16u, 88u, 161u, 192u, 32u, 80u, 207u, 145u, 67u, 50u,
67u, 109u, 12u, 216u, 161u, 156u, 20u, 51u, 164u, 135u, 49u, 67u, 61u,
67u, 54u, 69u, 12u, 192u, 161u, 153u, 52u, 51u, 68u, 146u, 26u, 67u,
109u, 12u, 216u, 161u, 156u, 20u, 51u, 164u, 135u, 58u, 25u, 177u,
67u, 48u, 40u, 102u, 77u, 12u, 209u, 36u, 146u, 26u, 67u, 157u, 12u,
184u, 161u, 152u, 20u, 51u, 38u, 134u, 104u, 146u, 72u, 102u, 20u,
56u, 212u, 50u, 244u, 51u, 68u, 65u, 116u, 50u, 224u, 80u, 227u, 145u,
67u, 140u, 40u, 102u, 77u, 12u, 209u, 33u, 164u, 80u, 204u, 80u, 203u,
208u, 205u, 18u, 72u, 161u, 202u, 9u, 128u, 225u, 36u, 128u,};
static unsigned char uvector__00045[] = {
 0u, 3u, 133u, 6u, 10u, 29u, 65u, 67u, 170u, 40u, 117u, 168u, 83u,
67u, 146u, 134u, 97u, 67u, 157u, 67u, 47u, 67u, 52u, 68u, 23u, 67u,
46u, 5u, 14u, 57u, 20u, 58u, 98u, 134u, 100u, 208u, 205u, 18u, 5u,
14u, 41u, 20u, 57u, 64u, 80u, 229u, 208u, 231u, 145u, 67u, 136u, 40u,
102u, 5u, 12u, 201u, 161u, 154u, 36u, 146u, 26u, 69u, 12u, 197u, 12u,
189u, 12u, 209u, 36u, 134u, 97u, 67u, 167u, 67u, 47u, 67u, 52u, 68u,
23u, 67u, 46u, 5u, 14u, 57u, 20u, 58u, 98u, 134u, 100u, 208u, 205u,
18u, 5u, 14u, 41u, 20u, 57u, 64u, 80u, 229u, 208u, 231u, 145u, 67u,
151u, 67u, 152u, 69u, 14u, 32u, 161u, 152u, 20u, 51u, 38u, 134u, 104u,
146u, 72u, 20u, 58u, 68u, 80u, 229u, 1u, 67u, 151u, 67u, 158u, 69u,
14u, 93u, 14u, 97u, 161u, 210u, 20u, 56u, 130u, 134u, 96u, 80u, 204u,
154u, 25u, 162u, 73u, 33u, 164u, 80u, 229u, 1u, 67u, 161u, 192u, 56u,
8u, 161u, 209u, 20u, 57u, 116u, 57u, 228u, 80u, 229u, 208u, 230u, 18u,
69u, 14u, 32u, 161u, 152u, 161u, 151u, 161u, 154u, 36u, 146u, 72u,
102u, 20u, 51u, 20u, 50u, 244u, 51u, 68u, 54u, 1u, 67u, 145u, 67u,
46u, 67u, 8u, 161u, 201u, 36u, 129u, 67u, 131u, 67u, 47u, 67u, 142u,
69u, 14u, 80u, 20u, 57u, 116u, 57u, 132u, 80u, 226u, 10u, 28u, 97u,
67u, 50u, 104u, 102u, 137u, 36u, 129u, 67u, 131u, 67u, 47u, 67u, 138u,
69u, 14u, 80u, 161u, 195u, 20u, 56u, 130u, 134u, 96u, 80u, 204u, 154u,
25u, 162u, 73u, 32u, 80u, 224u, 208u, 203u, 208u, 225u, 17u, 5u, 138u,
28u, 2u, 5u, 12u, 249u, 20u, 51u, 36u, 54u, 208u, 205u, 138u, 25u,
193u, 67u, 58u, 72u, 115u, 20u, 51u, 212u, 51u, 100u, 80u, 204u, 10u,
25u, 147u, 67u, 52u, 73u, 33u, 164u, 54u, 208u, 205u, 138u, 25u, 193u,
67u, 58u, 72u, 115u, 161u, 155u, 20u, 51u, 2u, 134u, 100u, 208u, 205u,
18u, 73u, 33u, 164u, 57u, 208u, 203u, 138u, 25u, 129u, 67u, 50u, 104u,
102u, 137u, 36u, 134u, 97u, 67u, 141u, 67u, 47u, 67u, 52u, 68u, 23u,
67u, 46u, 5u, 14u, 57u, 20u, 56u, 194u, 134u, 100u, 208u, 205u, 18u,
26u, 69u, 12u, 197u, 12u, 189u, 12u, 209u, 36u, 138u, 28u, 161u, 192u,
27u, 168u, 115u, 69u, 14u, 32u, 161u, 206u, 20u, 51u, 36u, 48u, 169u,
36u, 146u, 73u, 35u, 132u, 6u, 4u, 192u, 188u, 142u, 12u, 24u, 19u,
4u, 34u, 56u, 40u, 96u, 74u, 29u, 114u, 56u, 32u, 96u, 74u, 20u, 210u,
56u, 16u, 96u, 76u, 16u, 136u, 224u, 1u, 129u, 48u, 47u, 33u, 136u,
48u, 49u, 138u, 20u, 210u, 25u, 168u, 102u, 104u, 118u, 8u, 102u,
161u, 207u, 12u, 34u, 133u, 61u, 40u, 118u, 73u, 12u, 212u, 57u, 129u,
132u, 80u, 232u, 112u, 6u, 234u, 28u, 50u, 72u, 76u, 7u, 9u, 32u,};
static unsigned char uvector__00046[] = {
 0u, 3u, 135u, 6u, 6u, 97u, 66u, 157u, 66u, 154u, 67u, 53u, 12u, 205u,
14u, 193u, 12u, 212u, 57u, 225u, 132u, 80u, 167u, 165u, 14u, 201u,
33u, 154u, 135u, 48u, 48u, 138u, 29u, 14u, 0u, 221u, 67u, 134u, 73u,
20u, 58u, 130u, 135u, 84u, 80u, 235u, 80u, 166u, 135u, 37u, 12u, 194u,
135u, 58u, 134u, 94u, 134u, 104u, 136u, 46u, 134u, 92u, 10u, 28u,
114u, 40u, 116u, 197u, 12u, 201u, 161u, 154u, 36u, 10u, 28u, 82u, 40u,
114u, 128u, 161u, 203u, 161u, 207u, 34u, 135u, 16u, 80u, 204u, 10u,
25u, 147u, 67u, 52u, 73u, 36u, 52u, 138u, 25u, 138u, 25u, 122u, 25u,
162u, 73u, 12u, 194u, 135u, 78u, 134u, 94u, 134u, 104u, 136u, 46u,
134u, 92u, 10u, 28u, 114u, 40u, 116u, 197u, 12u, 201u, 161u, 154u,
36u, 10u, 28u, 82u, 40u, 114u, 128u, 161u, 203u, 161u, 207u, 34u,
135u, 46u, 135u, 48u, 138u, 28u, 65u, 67u, 48u, 40u, 102u, 77u, 12u,
209u, 36u, 144u, 40u, 116u, 136u, 161u, 202u, 2u, 135u, 46u, 135u,
60u, 138u, 28u, 186u, 28u, 195u, 67u, 164u, 40u, 113u, 5u, 12u, 192u,
161u, 153u, 52u, 51u, 68u, 146u, 67u, 72u, 161u, 202u, 2u, 135u, 67u,
128u, 112u, 17u, 67u, 162u, 40u, 114u, 232u, 115u, 200u, 161u, 203u,
161u, 204u, 36u, 138u, 28u, 65u, 67u, 49u, 67u, 47u, 67u, 52u, 73u,
36u, 144u, 204u, 40u, 102u, 40u, 101u, 232u, 102u, 136u, 108u, 2u,
135u, 34u, 134u, 92u, 134u, 17u, 67u, 146u, 73u, 2u, 135u, 6u, 134u,
94u, 135u, 28u, 138u, 28u, 160u, 40u, 114u, 232u, 115u, 8u, 161u,
196u, 20u, 56u, 194u, 134u, 100u, 208u, 205u, 18u, 73u, 2u, 135u, 6u,
134u, 94u, 135u, 20u, 138u, 28u, 161u, 67u, 134u, 40u, 113u, 5u, 12u,
192u, 161u, 153u, 52u, 51u, 68u, 146u, 64u, 161u, 193u, 161u, 151u,
161u, 194u, 34u, 11u, 20u, 56u, 4u, 10u, 25u, 242u, 40u, 102u, 72u,
109u, 161u, 155u, 20u, 51u, 130u, 134u, 116u, 144u, 230u, 40u, 103u,
168u, 102u, 200u, 161u, 152u, 20u, 51u, 38u, 134u, 104u, 146u, 67u,
72u, 109u, 161u, 155u, 20u, 51u, 130u, 134u, 116u, 144u, 231u, 67u,
54u, 40u, 102u, 5u, 12u, 201u, 161u, 154u, 36u, 146u, 67u, 72u, 115u,
161u, 151u, 20u, 51u, 2u, 134u, 100u, 208u, 205u, 18u, 73u, 12u, 194u,
135u, 26u, 134u, 94u, 134u, 104u, 136u, 46u, 134u, 92u, 10u, 28u,
114u, 40u, 113u, 133u, 12u, 201u, 161u, 154u, 36u, 52u, 138u, 25u,
138u, 25u, 122u, 25u, 162u, 73u, 20u, 57u, 67u, 128u, 55u, 80u, 230u,
138u, 28u, 65u, 67u, 156u, 40u, 102u, 72u, 97u, 82u, 73u, 36u, 146u,
72u, 225u, 97u, 129u, 140u, 38u, 6u, 233u, 130u, 81u, 28u, 36u, 48u,
38u, 3u, 132u, 112u, 64u, 192u, 152u, 14u, 17u, 192u, 131u, 2u, 96u,
56u, 72u,};
static unsigned char uvector__00047[] = {
 0u, 3u, 151u, 6u, 7u, 16u, 213u, 67u, 54u, 67u, 152u, 104u, 161u,
155u, 48u, 169u, 36u, 114u, 208u, 192u, 152u, 34u, 145u, 203u, 3u, 2u,
66u, 164u, 114u, 160u, 192u, 152u, 40u, 17u, 202u, 3u, 2u, 96u, 78u,
71u, 38u, 12u, 2u, 135u, 104u, 52u, 80u, 205u, 146u, 19u, 1u, 194u,
71u, 37u, 12u, 9u, 138u, 113u, 28u, 144u, 48u, 38u, 44u, 36u, 114u,
0u, 193u, 67u, 182u, 33u, 232u, 84u, 146u, 56u, 248u, 96u, 76u, 136u,
136u, 227u, 193u, 129u, 33u, 82u, 56u, 216u, 96u, 123u, 13u, 20u, 51u,
100u, 48u, 208u, 228u, 146u, 71u, 26u, 12u, 9u, 149u, 217u, 28u, 96u,
48u, 53u, 134u, 170u, 25u, 178u, 72u, 226u, 225u, 129u, 51u, 76u, 35u,
138u, 6u, 6u, 160u, 249u, 67u, 184u, 73u, 28u, 76u, 48u, 38u, 119u,
228u, 113u, 32u, 192u, 148u, 59u, 164u, 112u, 240u, 192u, 246u, 40u,
119u, 168u, 119u, 72u, 97u, 161u, 205u, 36u, 142u, 28u, 24u, 26u, 19u,
59u, 194u, 56u, 104u, 96u, 76u, 239u, 200u, 225u, 129u, 129u, 40u,
119u, 72u, 225u, 65u, 130u, 135u, 124u, 81u, 0u, 80u, 238u, 158u, 8u,
72u, 224u, 225u, 129u, 52u, 43u, 36u, 142u, 12u, 24u, 18u, 135u, 116u,
142u, 10u, 24u, 25u, 0u, 161u, 221u, 20u, 64u, 52u, 64u, 68u, 144u,
224u, 19u, 66u, 165u, 130u, 104u, 68u, 184u, 100u, 113u, 2u, 134u,
108u, 81u, 1u, 208u, 238u, 146u, 33u, 80u, 194u, 164u, 144u, 216u, 2u,
102u, 130u, 28u, 2u, 101u, 96u, 19u, 32u, 242u, 66u, 98u, 150u, 26u,
91u, 137u, 36u, 145u, 192u, 195u, 2u, 104u, 98u, 217u, 28u, 8u, 48u,
37u, 16u, 17u, 28u, 0u, 48u, 38u, 134u, 45u, 144u, 196u, 24u, 24u,
197u, 16u, 17u, 13u, 180u, 59u, 161u, 52u, 49u, 108u, 38u, 134u, 99u,
146u, 72u,};
static unsigned char uvector__00048[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 68u, 9u, 68u, 4u, 67u, 109u, 14u, 232u,
162u, 1u, 162u, 2u, 33u, 192u, 80u, 239u, 138u, 32u, 10u, 29u, 211u,
193u, 8u, 123u, 20u, 59u, 212u, 59u, 164u, 48u, 208u, 230u, 146u, 25u,
28u, 64u, 161u, 155u, 20u, 64u, 116u, 59u, 164u, 136u, 84u, 48u, 169u,
36u, 54u, 0u, 214u, 26u, 168u, 102u, 201u, 14u, 1u, 236u, 52u, 80u,
205u, 144u, 195u, 67u, 146u, 72u, 161u, 219u, 16u, 244u, 42u, 73u,
32u, 80u, 237u, 6u, 138u, 25u, 178u, 67u, 136u, 106u, 161u, 155u, 33u,
204u, 52u, 80u, 205u, 152u, 84u, 146u, 26u, 91u, 137u, 36u, 146u, 56u,
72u, 96u, 99u, 9u, 129u, 186u, 96u, 148u, 71u, 7u, 12u, 9u, 128u,
225u, 28u, 8u, 48u, 38u, 3u, 132u, 112u, 0u, 192u, 152u, 14u, 18u,};
static unsigned char uvector__00049[] = {
 0u, 3u, 147u, 6u, 4u, 162u, 5u, 35u, 145u, 6u, 8u, 158u, 136u, 22u,
136u, 24u, 145u, 200u, 67u, 2u, 81u, 3u, 17u, 200u, 3u, 2u, 81u, 2u,
145u, 199u, 131u, 2u, 96u, 152u, 71u, 28u, 12u, 20u, 64u, 224u, 81u,
4u, 10u, 32u, 154u, 32u, 82u, 64u, 161u, 223u, 162u, 8u, 224u, 4u,
128u, 242u, 40u, 112u, 69u, 16u, 85u, 16u, 40u, 162u, 11u, 162u, 8u,
224u, 36u, 138u, 32u, 170u, 32u, 110u, 0u, 73u, 36u, 38u, 9u, 132u,
142u, 54u, 24u, 19u, 26u, 242u, 56u, 208u, 96u, 76u, 110u, 136u, 227u,
1u, 129u, 50u, 36u, 35u, 139u, 6u, 4u, 162u, 6u, 35u, 138u, 6u, 4u,
200u, 144u, 142u, 36u, 24u, 19u, 28u, 210u, 56u, 136u, 96u, 76u, 124u,
8u, 226u, 1u, 129u, 40u, 129u, 72u, 225u, 193u, 129u, 49u, 205u, 35u,
134u, 6u, 4u, 197u, 224u, 142u, 18u, 24u, 19u, 18u, 50u, 56u, 64u,
96u, 74u, 32u, 130u, 56u, 56u, 96u, 76u, 72u, 200u, 224u, 161u, 129u,
49u, 76u, 35u, 130u, 6u, 4u, 162u, 5u, 35u, 129u, 6u, 4u, 197u, 48u,
142u, 0u, 24u, 18u, 136u, 20u, 134u, 32u, 192u, 198u, 40u, 129u, 72u,
112u, 162u, 5u, 14u, 193u, 49u, 35u, 162u, 5u, 36u, 146u,};
static unsigned char uvector__00050[] = {
 0u, 3u, 148u, 6u, 4u, 161u, 28u, 35u, 147u, 134u, 4u, 161u, 28u, 35u,
144u, 6u, 10u, 16u, 170u, 32u, 202u, 17u, 194u, 71u, 31u, 12u, 9u,
66u, 20u, 71u, 30u, 12u, 9u, 66u, 56u, 71u, 29u, 12u, 9u, 68u, 24u,
71u, 27u, 12u, 9u, 131u, 225u, 28u, 92u, 48u, 81u, 6u, 209u, 6u, 18u,
56u, 176u, 96u, 74u, 32u, 194u, 56u, 160u, 96u, 76u, 98u, 72u, 226u,
65u, 130u, 136u, 28u, 1u, 68u, 29u, 68u, 30u, 72u, 162u, 12u, 17u,
61u, 8u, 109u, 16u, 121u, 32u, 59u, 7u, 154u, 33u, 2u, 19u, 24u, 146u,
72u, 76u, 31u, 9u, 28u, 68u, 48u, 37u, 16u, 129u, 28u, 60u, 48u, 38u,
68u, 196u, 112u, 224u, 192u, 148u, 65u, 132u, 112u, 208u, 192u, 153u,
19u, 17u, 194u, 195u, 2u, 100u, 246u, 71u, 10u, 12u, 9u, 68u, 30u,
71u, 9u, 12u, 9u, 66u, 26u, 71u, 7u, 12u, 9u, 147u, 217u, 28u, 16u,
48u, 38u, 71u, 228u, 112u, 48u, 192u, 148u, 65u, 196u, 112u, 32u,
192u, 148u, 65u, 228u, 112u, 0u, 192u, 153u, 31u, 144u, 196u, 24u,
24u, 197u, 16u, 125u, 8u, 225u, 14u, 193u, 50u, 38u, 161u, 28u, 36u,
144u,};
static unsigned char uvector__00051[] = {
 0u, 3u, 187u, 134u, 10u, 17u, 144u, 228u, 40u, 131u, 232u, 71u, 8u,
118u, 20u, 64u, 224u, 10u, 32u, 234u, 32u, 242u, 69u, 16u, 96u, 137u,
232u, 67u, 104u, 131u, 201u, 1u, 216u, 60u, 209u, 8u, 17u, 68u, 27u,
68u, 24u, 73u, 34u, 132u, 42u, 136u, 50u, 132u, 112u, 154u, 17u, 194u,
104u, 71u, 5u, 16u, 136u, 130u, 232u, 81u, 0u, 126u, 33u, 218u, 136u,
74u, 65u, 36u, 6u, 226u, 29u, 168u, 132u, 225u, 210u, 67u, 77u, 10u,
33u, 36u, 145u, 221u, 67u, 2u, 99u, 90u, 71u, 116u, 12u, 9u, 66u,
136u, 71u, 107u, 12u, 9u, 68u, 38u, 71u, 104u, 12u, 9u, 144u, 201u,
29u, 156u, 48u, 37u, 10u, 33u, 29u, 120u, 48u, 37u, 16u, 145u, 29u,
108u, 48u, 38u, 59u, 164u, 117u, 160u, 192u, 148u, 40u, 132u, 117u,
128u, 192u, 152u, 214u, 145u, 213u, 195u, 2u, 80u, 142u, 17u, 213u,
3u, 3u, 24u, 76u, 14u, 130u, 96u, 192u, 72u, 234u, 97u, 129u, 40u,
71u, 8u, 233u, 225u, 129u, 40u, 71u, 8u, 233u, 161u, 130u, 132u, 42u,
136u, 50u, 132u, 112u, 145u, 211u, 3u, 2u, 80u, 133u, 17u, 210u, 195u,
2u, 80u, 142u, 17u, 210u, 131u, 2u, 81u, 6u, 17u, 209u, 131u, 5u, 16u,
109u, 16u, 97u, 35u, 162u, 134u, 4u, 162u, 12u, 35u, 161u, 134u, 4u,
208u, 189u, 210u, 58u, 0u, 96u, 74u, 33u, 2u, 57u, 240u, 96u, 101u,
14u, 2u, 136u, 82u, 136u, 48u, 135u, 96u, 243u, 68u, 32u, 66u, 104u,
94u, 233u, 33u, 52u, 40u, 122u, 132u, 112u, 145u, 207u, 3u, 2u, 104u,
110u, 49u, 28u, 236u, 48u, 37u, 16u, 97u, 28u, 228u, 48u, 38u, 134u,
227u, 17u, 206u, 3u, 3u, 32u, 20u, 65u, 130u, 39u, 161u, 13u, 162u,
5u, 36u, 132u, 208u, 217u, 2u, 71u, 54u, 12u, 9u, 162u, 18u, 196u,
115u, 80u, 192u, 144u, 65u, 28u, 208u, 48u, 37u, 8u, 105u, 28u, 200u,
48u, 38u, 136u, 75u, 17u, 204u, 3u, 0u, 162u, 4u, 162u, 14u, 52u, 50u,
2u, 136u, 84u, 54u, 209u, 6u, 4u, 209u, 9u, 96u, 154u, 27u, 32u, 73u,
35u, 151u, 134u, 4u, 130u, 8u, 229u, 193u, 129u, 52u, 76u, 20u, 142u,
88u, 24u, 19u, 68u, 194u, 8u, 229u, 97u, 129u, 40u, 131u, 136u, 229u,
33u, 129u, 52u, 76u, 32u, 142u, 78u, 24u, 40u, 66u, 168u, 67u, 104u,
71u, 9u, 28u, 152u, 48u, 37u, 8u, 81u, 28u, 148u, 48u, 37u, 8u, 225u,
28u, 144u, 48u, 37u, 8u, 105u, 28u, 132u, 48u, 61u, 209u, 7u, 6u, 24u,
128u, 146u, 57u, 0u, 96u, 74u, 32u, 226u, 56u, 248u, 96u, 100u, 2u,
132u, 52u, 65u, 116u, 40u, 128u, 63u, 16u, 237u, 68u, 37u, 68u, 12u,
72u, 13u, 196u, 59u, 81u, 9u, 195u, 68u, 134u, 145u, 19u, 208u, 162u,
81u, 3u, 18u, 73u, 13u, 128u, 38u, 140u, 12u, 132u, 209u, 101u, 162u,
19u, 68u, 193u, 67u, 72u, 76u, 7u, 9u, 36u, 113u, 208u, 192u, 154u,
53u, 62u, 71u, 28u, 12u, 9u, 68u, 12u, 71u, 27u, 12u, 9u, 66u, 136u,
71u, 25u, 12u, 9u, 163u, 83u, 228u, 113u, 0u, 192u, 148u, 66u, 100u,
112u, 208u, 192u, 154u, 52u, 48u, 71u, 12u, 12u, 9u, 66u, 136u, 71u,
9u, 12u, 9u, 68u, 12u, 71u, 4u, 12u, 9u, 68u, 36u, 71u, 1u, 12u, 9u,
163u, 50u, 196u, 112u, 0u, 192u, 148u, 40u, 132u, 49u, 6u, 6u, 49u,
66u, 21u, 66u, 57u, 66u, 137u, 68u, 29u, 68u, 32u, 67u, 109u, 8u,
104u, 77u, 25u, 89u, 9u, 163u, 99u, 100u, 146u,};
static unsigned char uvector__00052[] = {
 0u, 3u, 187u, 6u, 10u, 33u, 96u, 249u, 68u, 46u, 104u, 134u, 40u,
134u, 73u, 29u, 212u, 48u, 37u, 16u, 201u, 29u, 208u, 48u, 37u, 16u,
193u, 29u, 204u, 48u, 38u, 5u, 228u, 119u, 32u, 192u, 148u, 66u, 228u,
119u, 0u, 193u, 2u, 192u, 193u, 162u, 136u, 92u, 146u, 59u, 120u, 96u,
76u, 87u, 136u, 237u, 97u, 129u, 49u, 71u, 35u, 180u, 6u, 10u, 33u,
96u, 249u, 68u, 46u, 104u, 134u, 3u, 189u, 16u, 185u, 36u, 118u, 112u,
192u, 153u, 26u, 145u, 217u, 131u, 2u, 81u, 11u, 145u, 217u, 67u, 2u,
81u, 12u, 17u, 217u, 3u, 2u, 100u, 14u, 71u, 99u, 12u, 9u, 68u, 46u,
71u, 96u, 12u, 2u, 136u, 104u, 132u, 199u, 208u, 145u, 215u, 195u, 2u,
65u, 4u, 117u, 192u, 193u, 68u, 44u, 31u, 40u, 133u, 200u, 119u, 162u,
23u, 52u, 67u, 36u, 142u, 182u, 24u, 18u, 136u, 100u, 142u, 180u, 24u,
19u, 66u, 0u, 8u, 235u, 33u, 129u, 40u, 133u, 200u, 235u, 1u, 129u,
51u, 226u, 35u, 171u, 134u, 4u, 162u, 23u, 35u, 170u, 6u, 1u, 68u,
54u, 66u, 103u, 158u, 72u, 234u, 97u, 129u, 32u, 130u, 58u, 144u, 96u,
130u, 196u, 24u, 26u, 40u, 133u, 201u, 9u, 161u, 75u, 129u, 51u, 91u,
13u, 33u, 232u, 38u, 40u, 225u, 48u, 28u, 36u, 145u, 212u, 3u, 2u,
104u, 90u, 233u, 29u, 60u, 48u, 38u, 133u, 197u, 17u, 211u, 67u, 2u,
104u, 90u, 233u, 29u, 40u, 48u, 51u, 193u, 180u, 66u, 228u, 142u,
146u, 24u, 18u, 136u, 92u, 142u, 138u, 24u, 26u, 195u, 85u, 16u, 185u,
36u, 116u, 64u, 192u, 154u, 30u, 42u, 71u, 65u, 12u, 12u, 98u, 132u,
42u, 132u, 114u, 133u, 18u, 136u, 58u, 136u, 64u, 134u, 218u, 16u,
209u, 5u, 208u, 162u, 0u, 252u, 67u, 181u, 16u, 149u, 16u, 49u, 32u,
55u, 16u, 237u, 68u, 39u, 13u, 18u, 26u, 68u, 79u, 66u, 137u, 68u,
12u, 73u, 13u, 128u, 61u, 209u, 7u, 6u, 24u, 128u, 145u, 66u, 21u,
66u, 27u, 66u, 56u, 72u, 20u, 64u, 148u, 65u, 198u, 134u, 64u, 81u,
10u, 134u, 218u, 32u, 193u, 19u, 208u, 134u, 209u, 2u, 144u, 202u,
28u, 5u, 16u, 165u, 16u, 97u, 14u, 193u, 230u, 136u, 64u, 138u, 32u,
218u, 32u, 194u, 72u, 161u, 10u, 162u, 12u, 161u, 28u, 52u, 35u, 132u,
146u, 26u, 69u, 8u, 200u, 114u, 20u, 65u, 244u, 35u, 132u, 59u, 10u,
32u, 112u, 5u, 16u, 117u, 16u, 121u, 34u, 136u, 48u, 68u, 244u, 33u,
180u, 65u, 228u, 128u, 236u, 30u, 104u, 132u, 8u, 162u, 13u, 162u,
12u, 36u, 145u, 66u, 21u, 68u, 25u, 66u, 56u, 77u, 8u, 225u, 52u, 35u,
130u, 136u, 68u, 65u, 116u, 40u, 128u, 63u, 16u, 237u, 68u, 37u, 32u,
146u, 3u, 113u, 14u, 212u, 66u, 112u, 233u, 33u, 166u, 133u, 16u,
146u, 73u, 36u, 142u, 126u, 24u, 40u, 135u, 40u, 135u, 73u, 28u, 248u,
48u, 37u, 16u, 225u, 28u, 244u, 48u, 37u, 16u, 233u, 28u, 236u, 48u,
38u, 141u, 225u, 17u, 206u, 67u, 5u, 16u, 229u, 16u, 241u, 35u, 156u,
6u, 4u, 162u, 28u, 35u, 155u, 134u, 4u, 162u, 30u, 35u, 154u, 134u,
4u, 209u, 213u, 82u, 57u, 144u, 96u, 100u, 2u, 136u, 24u, 50u, 138u,
33u, 241u, 68u, 128u, 76u, 37u, 32u, 146u, 67u, 48u, 162u, 28u, 162u,
5u, 33u, 194u, 136u, 20u, 59u, 10u, 32u, 112u, 40u, 130u, 5u, 16u,
77u, 16u, 41u, 32u, 80u, 239u, 209u, 4u, 112u, 2u, 64u, 121u, 20u,
56u, 34u, 136u, 42u, 136u, 20u, 81u, 5u, 209u, 4u, 112u, 18u, 69u,
16u, 85u, 16u, 55u, 0u, 36u, 146u, 34u, 122u, 32u, 90u, 32u, 98u,
104u, 129u, 73u, 33u, 154u, 136u, 72u, 38u, 142u, 170u, 144u, 205u,
68u, 38u, 19u, 70u, 240u, 136u, 114u, 9u, 162u, 1u, 193u, 52u, 67u,
16u, 146u, 57u, 80u, 96u, 77u, 31u, 73u, 35u, 148u, 134u, 4u, 209u,
245u, 66u, 57u, 32u, 96u, 77u, 31u, 135u, 35u, 145u, 6u, 4u, 209u,
248u, 114u, 57u, 0u, 96u, 77u, 31u, 84u, 35u, 143u, 6u, 4u, 162u, 25u,
35u, 140u, 134u, 6u, 81u, 34u, 209u, 12u, 150u, 234u, 33u, 146u, 71u,
23u, 12u, 9u, 164u, 249u, 228u, 113u, 96u, 192u, 148u, 67u, 36u, 113u,
64u, 192u, 154u, 79u, 158u, 71u, 18u, 12u, 9u, 68u, 48u, 71u, 13u,
12u, 12u, 162u, 69u, 162u, 24u, 45u, 212u, 67u, 4u, 142u, 22u, 24u,
19u, 74u, 133u, 136u, 225u, 65u, 129u, 40u, 134u, 8u, 225u, 1u, 129u,
52u, 168u, 88u, 142u, 12u, 24u, 26u, 232u, 133u, 201u, 28u, 20u, 48u,
37u, 16u, 185u, 28u, 0u, 48u, 37u, 18u, 9u, 12u, 65u, 129u, 140u, 72u,
226u, 136u, 121u, 184u, 138u, 33u, 214u, 226u, 67u, 109u, 16u, 48u,
77u, 31u, 73u, 9u, 163u, 251u, 161u, 52u, 142u, 120u, 38u, 146u, 11u,
132u, 210u, 73u, 2u, 73u, 0u,};
static unsigned char uvector__00053[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 68u, 133u, 35u, 138u, 33u, 230u, 226u,
40u, 135u, 91u, 137u, 13u, 180u, 64u, 193u, 148u, 81u, 15u, 138u, 36u,
2u, 97u, 41u, 4u, 134u, 97u, 68u, 57u, 68u, 10u, 67u, 133u, 16u, 40u,
118u, 20u, 64u, 224u, 81u, 4u, 10u, 32u, 154u, 32u, 82u, 64u, 161u,
223u, 162u, 8u, 224u, 4u, 128u, 242u, 40u, 112u, 69u, 16u, 85u, 16u,
40u, 162u, 11u, 162u, 8u, 224u, 36u, 138u, 32u, 170u, 32u, 110u, 0u,
73u, 36u, 68u, 244u, 64u, 180u, 64u, 196u, 209u, 2u, 146u, 67u, 53u,
16u, 144u, 162u, 28u, 162u, 30u, 36u, 51u, 81u, 9u, 138u, 33u, 202u,
33u, 210u, 67u, 144u, 161u, 10u, 161u, 28u, 161u, 68u, 162u, 14u,
162u, 16u, 33u, 182u, 132u, 52u, 65u, 116u, 40u, 128u, 63u, 16u, 237u,
68u, 37u, 68u, 12u, 72u, 13u, 196u, 59u, 81u, 9u, 195u, 68u, 134u,
145u, 19u, 208u, 162u, 81u, 3u, 18u, 67u, 96u, 15u, 116u, 65u, 193u,
134u, 32u, 36u, 80u, 133u, 80u, 134u, 208u, 142u, 18u, 5u, 16u, 37u,
16u, 113u, 161u, 144u, 20u, 66u, 161u, 182u, 136u, 48u, 68u, 244u,
33u, 180u, 64u, 164u, 50u, 135u, 1u, 68u, 41u, 68u, 24u, 67u, 176u,
121u, 162u, 16u, 34u, 136u, 54u, 136u, 48u, 146u, 40u, 66u, 168u,
131u, 40u, 71u, 13u, 8u, 225u, 36u, 134u, 145u, 66u, 50u, 28u, 133u,
16u, 125u, 8u, 225u, 14u, 194u, 136u, 28u, 1u, 68u, 29u, 68u, 30u,
72u, 162u, 12u, 17u, 61u, 8u, 109u, 16u, 121u, 32u, 59u, 7u, 154u,
33u, 2u, 40u, 131u, 104u, 131u, 9u, 36u, 80u, 133u, 81u, 6u, 80u,
142u, 19u, 66u, 56u, 77u, 8u, 224u, 162u, 17u, 16u, 93u, 10u, 32u,
15u, 196u, 59u, 81u, 9u, 72u, 36u, 128u, 220u, 67u, 181u, 16u, 156u,
58u, 72u, 105u, 161u, 68u, 36u, 146u, 73u, 36u, 142u, 18u, 24u, 24u,
194u, 96u, 110u, 152u, 64u, 17u, 193u, 195u, 2u, 96u, 56u, 71u, 2u,
12u, 9u, 128u, 225u, 28u, 0u, 48u, 38u, 3u, 132u, 128u,};
static unsigned char uvector__00054[] = {
 0u, 3u, 134u, 6u, 6u, 106u, 20u, 1u, 68u, 132u, 73u, 28u, 36u, 48u,
38u, 6u, 228u, 112u, 112u, 192u, 152u, 14u, 17u, 192u, 131u, 2u, 96u,
56u, 71u, 0u, 12u, 9u, 128u, 225u, 32u,};
#if defined(GAUCHE_WINDOWS)
static void handle_cleanup(ScmObj h);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
static void handle_print(ScmObj h,ScmPort* p,ScmWriteContext* G1923 SCM_UNUSED);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
static ScmClass *WinHandleClass = NULL;
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)

#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)

#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)

#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)

#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)

#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)

#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
static ScmObj libsyssys_win_processP(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_win_processP__STUB, 1, 0,SCM_FALSE,libsyssys_win_processP, NULL, NULL);

#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
static ScmObj libsyssys_win_process_pid(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_win_process_pid__STUB, 1, 0,SCM_FALSE,libsyssys_win_process_pid, NULL, NULL);

#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
static ScmObj libsyssys_get_osfhandle(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_get_osfhandle__STUB, 1, 0,SCM_FALSE,libsyssys_get_osfhandle, NULL, NULL);

#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
static ScmObj libsyssys_win_pipe_name(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(libsyssys_win_pipe_name__STUB, 1, 0,SCM_FALSE,libsyssys_win_pipe_name, NULL, NULL);

#endif /* defined(GAUCHE_WINDOWS) */
static unsigned char uvector__00055[] = {
 0u, 3u, 139u, 134u, 10u, 36u, 49u, 68u, 137u, 67u, 50u, 73u, 28u,
88u, 48u, 38u, 5u, 228u, 113u, 48u, 192u, 148u, 51u, 36u, 113u, 16u,
192u, 152u, 23u, 145u, 196u, 3u, 5u, 16u, 56u, 2u, 137u, 20u, 48u,
201u, 100u, 48u, 209u, 35u, 18u, 69u, 12u, 200u, 162u, 70u, 162u, 71u,
36u, 132u, 192u, 112u, 145u, 195u, 195u, 2u, 80u, 204u, 145u, 195u,
131u, 2u, 97u, 250u, 71u, 12u, 12u, 9u, 139u, 193u, 28u, 44u, 48u,
37u, 18u, 57u, 28u, 36u, 48u, 38u, 47u, 4u, 112u, 128u, 192u, 152u,
126u, 145u, 193u, 131u, 2u, 98u, 44u, 71u, 0u, 12u, 9u, 136u, 177u,
12u, 65u, 129u, 140u, 81u, 35u, 144u, 152u, 126u, 146u, 64u,};
static unsigned char uvector__00056[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 68u, 145u, 68u, 142u, 69u, 16u, 56u, 2u,
137u, 20u, 48u, 201u, 100u, 48u, 209u, 35u, 18u, 69u, 12u, 200u, 162u,
70u, 162u, 71u, 36u, 138u, 36u, 49u, 68u, 137u, 67u, 50u, 73u, 36u,
112u, 144u, 192u, 198u, 19u, 3u, 116u, 193u, 40u, 142u, 14u, 24u, 19u,
1u, 194u, 56u, 16u, 96u, 76u, 7u, 8u, 224u, 1u, 129u, 48u, 28u, 36u,};
static unsigned char uvector__00057[] = {
 0u, 3u, 139u, 6u, 8u, 156u, 81u, 36u, 135u, 65u, 68u, 149u, 192u,
133u, 16u, 72u, 139u, 184u, 9u, 39u, 128u, 154u, 36u, 179u, 68u, 152u,
72u, 226u, 33u, 129u, 48u, 41u, 35u, 134u, 134u, 4u, 192u, 240u, 142u,
24u, 24u, 19u, 4u, 114u, 56u, 80u, 96u, 76u, 25u, 72u, 225u, 33u,
129u, 48u, 120u, 35u, 132u, 6u, 4u, 139u, 136u, 224u, 161u, 129u, 48u,
120u, 35u, 129u, 134u, 4u, 193u, 148u, 142u, 0u, 24u, 19u, 2u, 146u,
24u, 131u, 3u, 24u, 139u, 136u, 146u, 98u, 112u, 152u, 20u, 209u, 38u,
18u, 72u,};
static unsigned char uvector__00058[] = {
 0u, 3u, 169u, 134u, 10u, 36u, 209u, 68u, 157u, 68u, 10u, 73u, 29u,
68u, 48u, 38u, 5u, 228u, 117u, 0u, 192u, 148u, 64u, 164u, 116u, 224u,
192u, 152u, 23u, 145u, 211u, 67u, 2u, 81u, 2u, 145u, 210u, 131u, 4u,
79u, 68u, 152u, 36u, 154u, 36u, 250u, 37u, 10u, 32u, 81u, 67u, 34u,
36u, 152u, 156u, 81u, 36u, 135u, 65u, 68u, 149u, 192u, 133u, 16u, 72u,
139u, 184u, 9u, 39u, 128u, 154u, 36u, 179u, 68u, 152u, 73u, 162u, 76u,
36u, 116u, 80u, 193u, 68u, 159u, 68u, 161u, 68u, 10u, 19u, 24u, 114u,
71u, 68u, 12u, 9u, 68u, 162u, 71u, 67u, 12u, 9u, 68u, 10u, 71u, 63u,
12u, 9u, 148u, 57u, 28u, 236u, 48u, 10u, 37u, 42u, 32u, 82u, 36u,
152u, 158u, 137u, 48u, 38u, 41u, 84u, 73u, 132u, 145u, 206u, 131u, 2u,
102u, 248u, 71u, 55u, 12u, 9u, 68u, 10u, 71u, 53u, 12u, 9u, 155u,
225u, 28u, 208u, 48u, 37u, 16u, 41u, 28u, 196u, 48u, 68u, 244u, 73u,
148u, 64u, 180u, 73u, 132u, 142u, 92u, 24u, 18u, 136u, 20u, 142u, 84u,
24u, 25u, 68u, 173u, 68u, 11u, 68u, 166u, 66u, 104u, 76u, 77u, 16u,
41u, 35u, 148u, 6u, 4u, 208u, 169u, 210u, 57u, 40u, 96u, 74u, 32u,
82u, 57u, 24u, 96u, 77u, 10u, 157u, 35u, 143u, 134u, 8u, 22u, 137u,
82u, 136u, 20u, 145u, 199u, 131u, 2u, 81u, 2u, 145u, 198u, 131u, 2u,
104u, 102u, 97u, 28u, 96u, 48u, 60u, 10u, 37u, 90u, 32u, 82u, 19u,
67u, 51u, 9u, 28u, 92u, 48u, 38u, 135u, 34u, 17u, 197u, 3u, 2u, 81u,
2u, 145u, 196u, 131u, 2u, 104u, 114u, 33u, 28u, 64u, 48u, 81u, 43u,
4u, 208u, 225u, 144u, 154u, 21u, 36u, 72u, 225u, 225u, 129u, 40u,
149u, 136u, 225u, 97u, 128u, 67u, 52u, 64u, 176u, 209u, 162u, 87u,
36u, 112u, 144u, 192u, 154u, 34u, 162u, 71u, 6u, 12u, 9u, 68u, 10u,
71u, 4u, 12u, 9u, 162u, 42u, 36u, 112u, 32u, 192u, 30u, 68u, 215u,
68u, 10u, 72u, 76u, 7u, 9u, 28u, 4u, 48u, 38u, 137u, 166u, 145u, 192u,
3u, 2u, 81u, 2u, 144u, 196u, 24u, 24u, 197u, 16u, 45u, 18u, 177u, 13u,
129u, 52u, 76u, 124u, 38u, 136u, 167u, 4u, 209u, 1u, 144u, 153u, 188u,
134u, 154u, 32u, 82u, 73u, 32u,};
static unsigned char uvector__00059[] = {
 0u, 3u, 135u, 6u, 6u, 97u, 68u, 155u, 68u, 11u, 68u, 172u, 67u, 96u,
15u, 34u, 107u, 162u, 5u, 36u, 81u, 38u, 138u, 36u, 234u, 32u, 82u,
72u, 16u, 205u, 16u, 44u, 52u, 104u, 149u, 200u, 162u, 86u, 15u, 2u,
137u, 86u, 136u, 20u, 136u, 22u, 137u, 82u, 136u, 20u, 144u, 202u,
37u, 106u, 32u, 90u, 37u, 50u, 34u, 122u, 36u, 202u, 32u, 90u, 36u,
195u, 68u, 10u, 72u, 20u, 74u, 84u, 64u, 164u, 73u, 49u, 61u, 18u,
96u, 146u, 104u, 147u, 232u, 148u, 40u, 129u, 69u, 12u, 136u, 146u,
98u, 113u, 68u, 146u, 29u, 5u, 18u, 87u, 2u, 20u, 65u, 34u, 46u, 224u,
36u, 158u, 2u, 104u, 146u, 205u, 18u, 97u, 38u, 137u, 48u, 144u, 211u,
68u, 10u, 73u, 35u, 133u, 134u, 6u, 48u, 152u, 27u, 166u, 11u, 68u,
112u, 144u, 192u, 152u, 14u, 17u, 193u, 3u, 2u, 96u, 56u, 71u, 2u,
12u, 9u, 128u, 225u, 32u,};
static ScmObj SCM_debug_info_const_vector();
#if defined(__CYGWIN__) || defined(GAUCHE_WINDOWS)
#define SCM_CGEN_CONST /*empty*/
#else
#define SCM_CGEN_CONST const
#endif
static SCM_CGEN_CONST struct scm__scRec {
#if defined(GAUCHE_WINDOWS)
  ScmString d1926[16];
#endif /*defined(GAUCHE_WINDOWS)*/
#if defined(HAVE_SELECT)
  ScmString d1889[28];
#endif /*defined(HAVE_SELECT)*/
#if defined(HAVE_READLINK)
  ScmString d1884[9];
#endif /*defined(HAVE_READLINK)*/
#if defined(HAVE_SYMLINK)
  ScmString d1881[10];
#endif /*defined(HAVE_SYMLINK)*/
#if defined(HAVE_CRYPT)
  ScmString d1878[9];
#endif /*defined(HAVE_CRYPT)*/
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  ScmString d1875[11];
#endif /*(defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))*/
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  ScmString d1871[9];
#endif /*defined(HAVE_SETGROUPS)*/
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  ScmString d1867[8];
#endif /*defined(HAVE_GETPGID)*/
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if defined(HAVE_LCHOWN)
  ScmString d1864[11];
#endif /*defined(HAVE_LCHOWN)*/
#if !(!(defined(GAUCHE_WINDOWS)))
  ScmString d1858[9];
#endif /*!(!(defined(GAUCHE_WINDOWS)))*/
#if !(defined(GAUCHE_WINDOWS))
  ScmString d1855[28];
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_OFILE)
  ScmString d1852[1];
#endif /*defined(RLIMIT_OFILE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_STACK)
  ScmString d1850[1];
#endif /*defined(RLIMIT_STACK)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_SBSIZE)
  ScmString d1848[1];
#endif /*defined(RLIMIT_SBSIZE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_SIGPENDING)
  ScmString d1846[1];
#endif /*defined(RLIMIT_SIGPENDING)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_RTPRIO)
  ScmString d1844[1];
#endif /*defined(RLIMIT_RTPRIO)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_RSS)
  ScmString d1842[1];
#endif /*defined(RLIMIT_RSS)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NPROC)
  ScmString d1840[1];
#endif /*defined(RLIMIT_NPROC)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NOFILE)
  ScmString d1838[1];
#endif /*defined(RLIMIT_NOFILE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NICE)
  ScmString d1836[1];
#endif /*defined(RLIMIT_NICE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_MSGQUEUE)
  ScmString d1834[1];
#endif /*defined(RLIMIT_MSGQUEUE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_MEMLOCK)
  ScmString d1832[1];
#endif /*defined(RLIMIT_MEMLOCK)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_LOCKS)
  ScmString d1830[1];
#endif /*defined(RLIMIT_LOCKS)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_FSIZE)
  ScmString d1828[1];
#endif /*defined(RLIMIT_FSIZE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_DATA)
  ScmString d1826[1];
#endif /*defined(RLIMIT_DATA)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_CPU)
  ScmString d1824[1];
#endif /*defined(RLIMIT_CPU)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_CORE)
  ScmString d1822[1];
#endif /*defined(RLIMIT_CORE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_AS)
  ScmString d1820[1];
#endif /*defined(RLIMIT_AS)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
  ScmString d1816[17];
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if !(defined(HAVE_GETLOADAVG))
  ScmString d1813[10];
#endif /*!(defined(HAVE_GETLOADAVG))*/
#if defined(HAVE_GETLOADAVG)
  ScmString d1809[10];
#endif /*defined(HAVE_GETLOADAVG)*/
#if !(HAVE_STRSIGNAL)
  ScmString d1806[9];
#endif /*!(HAVE_STRSIGNAL)*/
#if HAVE_STRSIGNAL
  ScmString d1803[9];
#endif /*HAVE_STRSIGNAL*/
  ScmString d1785[649];
} scm__sc SCM_UNUSED = {
#if defined(GAUCHE_WINDOWS)
  {   /* ScmString d1926 */
      SCM_STRING_CONST_INITIALIZER("handle-type", 11, 11),
      SCM_STRING_CONST_INITIALIZER("process", 7, 7),
      SCM_STRING_CONST_INITIALIZER("sys-win-process\077", 16, 16),
      SCM_STRING_CONST_INITIALIZER("obj", 3, 3),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<top>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<boolean>", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-win-process-pid", 19, 19),
      SCM_STRING_CONST_INITIALIZER("<int>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("sys-get-osfhandle", 17, 17),
      SCM_STRING_CONST_INITIALIZER("port-or-fd", 10, 10),
      SCM_STRING_CONST_INITIALIZER("sys-win-pipe-name", 17, 17),
  },
#endif /*defined(GAUCHE_WINDOWS)*/
#if defined(HAVE_SELECT)
  {   /* ScmString d1889 */
      SCM_STRING_CONST_INITIALIZER("sys-fdset-ref", 13, 13),
      SCM_STRING_CONST_INITIALIZER("fdset", 5, 5),
      SCM_STRING_CONST_INITIALIZER("pf", 2, 2),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<sys-fdset>", 11, 11),
      SCM_STRING_CONST_INITIALIZER("<top>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<boolean>", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-fdset-set!", 14, 14),
      SCM_STRING_CONST_INITIALIZER("flag", 4, 4),
      SCM_STRING_CONST_INITIALIZER("<void>", 6, 6),
      SCM_STRING_CONST_INITIALIZER("sys-fdset-max-fd", 16, 16),
      SCM_STRING_CONST_INITIALIZER("<int>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("sys-fdset-clear!", 16, 16),
      SCM_STRING_CONST_INITIALIZER("sys-fdset-copy!", 15, 15),
      SCM_STRING_CONST_INITIALIZER("dst", 3, 3),
      SCM_STRING_CONST_INITIALIZER("src", 3, 3),
      SCM_STRING_CONST_INITIALIZER("sys-select", 10, 10),
      SCM_STRING_CONST_INITIALIZER("rfds", 4, 4),
      SCM_STRING_CONST_INITIALIZER("wfds", 4, 4),
      SCM_STRING_CONST_INITIALIZER("efds", 4, 4),
      SCM_STRING_CONST_INITIALIZER("optional", 8, 8),
      SCM_STRING_CONST_INITIALIZER("timeout", 7, 7),
      SCM_STRING_CONST_INITIALIZER("*", 1, 1),
      SCM_STRING_CONST_INITIALIZER("sys-select!", 11, 11),
  },
#endif /*defined(HAVE_SELECT)*/
#if defined(HAVE_READLINK)
  {   /* ScmString d1884 */
      SCM_STRING_CONST_INITIALIZER("sys-readlink", 12, 12),
      SCM_STRING_CONST_INITIALIZER("path", 4, 4),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<const-cstring>", 15, 15),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<top>", 5, 5),
  },
#endif /*defined(HAVE_READLINK)*/
#if defined(HAVE_SYMLINK)
  {   /* ScmString d1881 */
      SCM_STRING_CONST_INITIALIZER("sys-symlink", 11, 11),
      SCM_STRING_CONST_INITIALIZER("existing", 8, 8),
      SCM_STRING_CONST_INITIALIZER("newpath", 7, 7),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<const-cstring>", 15, 15),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<void>", 6, 6),
  },
#endif /*defined(HAVE_SYMLINK)*/
#if defined(HAVE_CRYPT)
  {   /* ScmString d1878 */
      SCM_STRING_CONST_INITIALIZER("sys-crypt", 9, 9),
      SCM_STRING_CONST_INITIALIZER("key", 3, 3),
      SCM_STRING_CONST_INITIALIZER("salt", 4, 4),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<const-cstring>", 15, 15),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
  },
#endif /*defined(HAVE_CRYPT)*/
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  {   /* ScmString d1875 */
      SCM_STRING_CONST_INITIALIZER("sys-nanosleep", 13, 13),
      SCM_STRING_CONST_INITIALIZER("nanoseconds", 11, 11),
      SCM_STRING_CONST_INITIALIZER("optional", 8, 8),
      SCM_STRING_CONST_INITIALIZER("no-retry", 8, 8),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<top>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("*", 1, 1),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
  },
#endif /*(defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))*/
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  {   /* ScmString d1871 */
      SCM_STRING_CONST_INITIALIZER("sys-setgroups", 13, 13),
      SCM_STRING_CONST_INITIALIZER("gids", 4, 4),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<top>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<void>", 6, 6),
  },
#endif /*defined(HAVE_SETGROUPS)*/
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  {   /* ScmString d1867 */
      SCM_STRING_CONST_INITIALIZER("sys-getpgid", 11, 11),
      SCM_STRING_CONST_INITIALIZER("pid", 3, 3),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<int>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
  },
#endif /*defined(HAVE_GETPGID)*/
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if defined(HAVE_LCHOWN)
  {   /* ScmString d1864 */
      SCM_STRING_CONST_INITIALIZER("sys-lchown", 10, 10),
      SCM_STRING_CONST_INITIALIZER("path", 4, 4),
      SCM_STRING_CONST_INITIALIZER("owner", 5, 5),
      SCM_STRING_CONST_INITIALIZER("group", 5, 5),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<const-cstring>", 15, 15),
      SCM_STRING_CONST_INITIALIZER("<int>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
  },
#endif /*defined(HAVE_LCHOWN)*/
#if !(!(defined(GAUCHE_WINDOWS)))
  {   /* ScmString d1858 */
      SCM_STRING_CONST_INITIALIZER("sys-lstat", 9, 9),
      SCM_STRING_CONST_INITIALIZER("path", 4, 4),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<const-cstring>", 15, 15),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<sys-stat>", 10, 10),
  },
#endif /*!(!(defined(GAUCHE_WINDOWS)))*/
#if !(defined(GAUCHE_WINDOWS))
  {   /* ScmString d1855 */
      SCM_STRING_CONST_INITIALIZER("sys-lstat", 9, 9),
      SCM_STRING_CONST_INITIALIZER("path", 4, 4),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<const-cstring>", 15, 15),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<sys-stat>", 10, 10),
      SCM_STRING_CONST_INITIALIZER("sys-mkfifo", 10, 10),
      SCM_STRING_CONST_INITIALIZER("mode", 4, 4),
      SCM_STRING_CONST_INITIALIZER("<int>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("sys-fchmod", 10, 10),
      SCM_STRING_CONST_INITIALIZER("port-or-fd", 10, 10),
      SCM_STRING_CONST_INITIALIZER("<top>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("<void>", 6, 6),
      SCM_STRING_CONST_INITIALIZER("sys-setgid", 10, 10),
      SCM_STRING_CONST_INITIALIZER("gid", 3, 3),
      SCM_STRING_CONST_INITIALIZER("sys-setpgid", 11, 11),
      SCM_STRING_CONST_INITIALIZER("pid", 3, 3),
      SCM_STRING_CONST_INITIALIZER("pgid", 4, 4),
      SCM_STRING_CONST_INITIALIZER("sys-getpgrp", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sys-setsid", 10, 10),
      SCM_STRING_CONST_INITIALIZER("sys-setuid", 10, 10),
      SCM_STRING_CONST_INITIALIZER("uid", 3, 3),
      SCM_STRING_CONST_INITIALIZER("sys-nice", 8, 8),
      SCM_STRING_CONST_INITIALIZER("inc", 3, 3),
      SCM_STRING_CONST_INITIALIZER("sys-getgroups", 13, 13),
  },
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_OFILE)
  {   /* ScmString d1852 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_OFILE", 12, 12),
  },
#endif /*defined(RLIMIT_OFILE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_STACK)
  {   /* ScmString d1850 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_STACK", 12, 12),
  },
#endif /*defined(RLIMIT_STACK)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_SBSIZE)
  {   /* ScmString d1848 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_SBSIZE", 13, 13),
  },
#endif /*defined(RLIMIT_SBSIZE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_SIGPENDING)
  {   /* ScmString d1846 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_SIGPENDING", 17, 17),
  },
#endif /*defined(RLIMIT_SIGPENDING)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_RTPRIO)
  {   /* ScmString d1844 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_RTPRIO", 13, 13),
  },
#endif /*defined(RLIMIT_RTPRIO)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_RSS)
  {   /* ScmString d1842 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_RSS", 10, 10),
  },
#endif /*defined(RLIMIT_RSS)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NPROC)
  {   /* ScmString d1840 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_NPROC", 12, 12),
  },
#endif /*defined(RLIMIT_NPROC)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NOFILE)
  {   /* ScmString d1838 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_NOFILE", 13, 13),
  },
#endif /*defined(RLIMIT_NOFILE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NICE)
  {   /* ScmString d1836 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_NICE", 11, 11),
  },
#endif /*defined(RLIMIT_NICE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_MSGQUEUE)
  {   /* ScmString d1834 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_MSGQUEUE", 15, 15),
  },
#endif /*defined(RLIMIT_MSGQUEUE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_MEMLOCK)
  {   /* ScmString d1832 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_MEMLOCK", 14, 14),
  },
#endif /*defined(RLIMIT_MEMLOCK)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_LOCKS)
  {   /* ScmString d1830 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_LOCKS", 12, 12),
  },
#endif /*defined(RLIMIT_LOCKS)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_FSIZE)
  {   /* ScmString d1828 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_FSIZE", 12, 12),
  },
#endif /*defined(RLIMIT_FSIZE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_DATA)
  {   /* ScmString d1826 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_DATA", 11, 11),
  },
#endif /*defined(RLIMIT_DATA)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_CPU)
  {   /* ScmString d1824 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_CPU", 10, 10),
  },
#endif /*defined(RLIMIT_CPU)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_CORE)
  {   /* ScmString d1822 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_CORE", 11, 11),
  },
#endif /*defined(RLIMIT_CORE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_AS)
  {   /* ScmString d1820 */
      SCM_STRING_CONST_INITIALIZER("RLIMIT_AS", 9, 9),
  },
#endif /*defined(RLIMIT_AS)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
  {   /* ScmString d1816 */
      SCM_STRING_CONST_INITIALIZER("sys-getrlimit", 13, 13),
      SCM_STRING_CONST_INITIALIZER("rsrc", 4, 4),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<int>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<integer>", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-setrlimit", 13, 13),
      SCM_STRING_CONST_INITIALIZER("cur", 3, 3),
      SCM_STRING_CONST_INITIALIZER("optional", 8, 8),
      SCM_STRING_CONST_INITIALIZER("max", 3, 3),
      SCM_STRING_CONST_INITIALIZER("<top>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("*", 1, 1),
      SCM_STRING_CONST_INITIALIZER("<void>", 6, 6),
      SCM_STRING_CONST_INITIALIZER("RLIM_INFINITY", 13, 13),
  },
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if !(defined(HAVE_GETLOADAVG))
  {   /* ScmString d1813 */
      SCM_STRING_CONST_INITIALIZER("sys-getloadavg", 14, 14),
      SCM_STRING_CONST_INITIALIZER("optional", 8, 8),
      SCM_STRING_CONST_INITIALIZER("_", 1, 1),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("*", 1, 1),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<top>", 5, 5),
  },
#endif /*!(defined(HAVE_GETLOADAVG))*/
#if defined(HAVE_GETLOADAVG)
  {   /* ScmString d1809 */
      SCM_STRING_CONST_INITIALIZER("sys-getloadavg", 14, 14),
      SCM_STRING_CONST_INITIALIZER("optional", 8, 8),
      SCM_STRING_CONST_INITIALIZER("nsamples", 8, 8),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("*", 1, 1),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<top>", 5, 5),
  },
#endif /*defined(HAVE_GETLOADAVG)*/
#if !(HAVE_STRSIGNAL)
  {   /* ScmString d1806 */
      SCM_STRING_CONST_INITIALIZER("sys-strsignal", 13, 13),
      SCM_STRING_CONST_INITIALIZER("_", 1, 1),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<int>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<const-cstring>\077", 16, 16),
  },
#endif /*!(HAVE_STRSIGNAL)*/
#if HAVE_STRSIGNAL
  {   /* ScmString d1803 */
      SCM_STRING_CONST_INITIALIZER("sys-strsignal", 13, 13),
      SCM_STRING_CONST_INITIALIZER("signum", 6, 6),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<int>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<const-cstring>\077", 16, 16),
  },
#endif /*HAVE_STRSIGNAL*/
  {   /* ScmString d1785 */
      SCM_STRING_CONST_INITIALIZER("sys-readdir", 11, 11),
      SCM_STRING_CONST_INITIALIZER("pathname", 8, 8),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("libsys.scm", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<string>", 8, 8),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<top>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("sys-tmpdir", 10, 10),
      SCM_STRING_CONST_INITIALIZER("sys-basename", 12, 12),
      SCM_STRING_CONST_INITIALIZER("sys-dirname", 11, 11),
      SCM_STRING_CONST_INITIALIZER("%expression-name-mark-key", 25, 25),
      SCM_STRING_CONST_INITIALIZER("sys-normalize-pathname", 22, 22),
      SCM_STRING_CONST_INITIALIZER("undefined\077", 10, 10),
      SCM_STRING_CONST_INITIALIZER("gauche.os.windows", 17, 17),
      SCM_STRING_CONST_INITIALIZER("cond-features", 13, 13),
      SCM_STRING_CONST_INITIALIZER("gauche.internal", 15, 15),
      SCM_STRING_CONST_INITIALIZER("\134", 1, 1),
      SCM_STRING_CONST_INITIALIZER("/", 1, 1),
      SCM_STRING_CONST_INITIALIZER("^~([^/\134\134]*)", 11, 11),
      SCM_STRING_CONST_INITIALIZER("rxmatch", 7, 7),
      SCM_STRING_CONST_INITIALIZER("", 0, 0),
      SCM_STRING_CONST_INITIALIZER("equal\077", 6, 6),
      SCM_STRING_CONST_INITIALIZER("HOME", 4, 4),
      SCM_STRING_CONST_INITIALIZER("sys-getenv", 10, 10),
      SCM_STRING_CONST_INITIALIZER("USERPROFILE", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sys-getuid", 10, 10),
      SCM_STRING_CONST_INITIALIZER("sys-getpwuid", 12, 12),
      SCM_STRING_CONST_INITIALIZER("dir", 3, 3),
      SCM_STRING_CONST_INITIALIZER("~", 1, 1),
      SCM_STRING_CONST_INITIALIZER("Couldn't obtain username for :", 30, 30),
      SCM_STRING_CONST_INITIALIZER("error", 5, 5),
      SCM_STRING_CONST_INITIALIZER("'~user' expansil isn't supported on Windows:", 44, 44),
      SCM_STRING_CONST_INITIALIZER("sys-getpwnam", 12, 12),
      SCM_STRING_CONST_INITIALIZER("after", 5, 5),
      SCM_STRING_CONST_INITIALIZER("string-append", 13, 13),
      SCM_STRING_CONST_INITIALIZER("^([A-Za-z]:)\077[/\134\134]", 18, 18),
      SCM_STRING_CONST_INITIALIZER("^/", 2, 2),
      SCM_STRING_CONST_INITIALIZER("sys-getcwd", 10, 10),
      SCM_STRING_CONST_INITIALIZER("string-split", 12, 12),
      SCM_STRING_CONST_INITIALIZER("string-join", 11, 11),
      SCM_STRING_CONST_INITIALIZER(".", 1, 1),
      SCM_STRING_CONST_INITIALIZER("..", 2, 2),
      SCM_STRING_CONST_INITIALIZER("length=\077", 8, 8),
      SCM_STRING_CONST_INITIALIZER("^[A-Za-z]:$", 11, 11),
      SCM_STRING_CONST_INITIALIZER("keyword list not even", 21, 21),
      SCM_STRING_CONST_INITIALIZER("unwrap-syntax-1", 15, 15),
      SCM_STRING_CONST_INITIALIZER("absolute", 8, 8),
      SCM_STRING_CONST_INITIALIZER("expand", 6, 6),
      SCM_STRING_CONST_INITIALIZER("canonicalize", 12, 12),
      SCM_STRING_CONST_INITIALIZER("unknown keyword ~S", 18, 18),
      SCM_STRING_CONST_INITIALIZER("errorf", 6, 6),
      SCM_STRING_CONST_INITIALIZER("key", 3, 3),
      SCM_STRING_CONST_INITIALIZER("%toplevel", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-errno->symbol", 17, 17),
      SCM_STRING_CONST_INITIALIZER("num", 3, 3),
      SCM_STRING_CONST_INITIALIZER("<fixnum>", 8, 8),
      SCM_STRING_CONST_INITIALIZER("sys-symbol->errno", 17, 17),
      SCM_STRING_CONST_INITIALIZER("name", 4, 4),
      SCM_STRING_CONST_INITIALIZER("<symbol>", 8, 8),
      SCM_STRING_CONST_INITIALIZER("E2BIG", 5, 5),
      SCM_STRING_CONST_INITIALIZER("EACCES", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EADDRINUSE", 10, 10),
      SCM_STRING_CONST_INITIALIZER("EADDRNOTAVAIL", 13, 13),
      SCM_STRING_CONST_INITIALIZER("EADV", 4, 4),
      SCM_STRING_CONST_INITIALIZER("EAFNOSUPPORT", 12, 12),
      SCM_STRING_CONST_INITIALIZER("EAGAIN", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EALREADY", 8, 8),
      SCM_STRING_CONST_INITIALIZER("EBADE", 5, 5),
      SCM_STRING_CONST_INITIALIZER("EBADF", 5, 5),
      SCM_STRING_CONST_INITIALIZER("EBADFD", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EBADMSG", 7, 7),
      SCM_STRING_CONST_INITIALIZER("EBADR", 5, 5),
      SCM_STRING_CONST_INITIALIZER("EBADRQC", 7, 7),
      SCM_STRING_CONST_INITIALIZER("EBADSLT", 7, 7),
      SCM_STRING_CONST_INITIALIZER("EBFONT", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EBUSY", 5, 5),
      SCM_STRING_CONST_INITIALIZER("ECANCELED", 9, 9),
      SCM_STRING_CONST_INITIALIZER("ECHILD", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ECHRNG", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ECOMM", 5, 5),
      SCM_STRING_CONST_INITIALIZER("ECONNABORTED", 12, 12),
      SCM_STRING_CONST_INITIALIZER("ECONNREFUSED", 12, 12),
      SCM_STRING_CONST_INITIALIZER("ECONNRESET", 10, 10),
      SCM_STRING_CONST_INITIALIZER("EDEADLK", 7, 7),
      SCM_STRING_CONST_INITIALIZER("EDEADLOCK", 9, 9),
      SCM_STRING_CONST_INITIALIZER("EDESTADDRREQ", 12, 12),
      SCM_STRING_CONST_INITIALIZER("EDOM", 4, 4),
      SCM_STRING_CONST_INITIALIZER("EDOTDOT", 7, 7),
      SCM_STRING_CONST_INITIALIZER("EDQUOT", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EEXIST", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EFAULT", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EFBIG", 5, 5),
      SCM_STRING_CONST_INITIALIZER("EHOSTDOWN", 9, 9),
      SCM_STRING_CONST_INITIALIZER("EHOSTUNREACH", 12, 12),
      SCM_STRING_CONST_INITIALIZER("EIDRM", 5, 5),
      SCM_STRING_CONST_INITIALIZER("EILSEQ", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EINPROGRESS", 11, 11),
      SCM_STRING_CONST_INITIALIZER("EINTR", 5, 5),
      SCM_STRING_CONST_INITIALIZER("EINVAL", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EIO", 3, 3),
      SCM_STRING_CONST_INITIALIZER("EISCONN", 7, 7),
      SCM_STRING_CONST_INITIALIZER("EISDIR", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EISNAM", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EKEYEXPIRED", 11, 11),
      SCM_STRING_CONST_INITIALIZER("EKEYREJECTED", 12, 12),
      SCM_STRING_CONST_INITIALIZER("EKEYREVOKED", 11, 11),
      SCM_STRING_CONST_INITIALIZER("EL2HLT", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EL2NSYNC", 8, 8),
      SCM_STRING_CONST_INITIALIZER("EL3HLT", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EL3RST", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ELIBACC", 7, 7),
      SCM_STRING_CONST_INITIALIZER("ELIBBAD", 7, 7),
      SCM_STRING_CONST_INITIALIZER("ELIBEXEC", 8, 8),
      SCM_STRING_CONST_INITIALIZER("ELIBMAX", 7, 7),
      SCM_STRING_CONST_INITIALIZER("ELIBSCN", 7, 7),
      SCM_STRING_CONST_INITIALIZER("ELNRNG", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ELOOP", 5, 5),
      SCM_STRING_CONST_INITIALIZER("EMEDIUMTYPE", 11, 11),
      SCM_STRING_CONST_INITIALIZER("EMFILE", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EMLINK", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EMSGSIZE", 8, 8),
      SCM_STRING_CONST_INITIALIZER("EMULTIHOP", 9, 9),
      SCM_STRING_CONST_INITIALIZER("ENAMETOOLONG", 12, 12),
      SCM_STRING_CONST_INITIALIZER("ENAVAIL", 7, 7),
      SCM_STRING_CONST_INITIALIZER("ENETDOWN", 8, 8),
      SCM_STRING_CONST_INITIALIZER("ENETRESET", 9, 9),
      SCM_STRING_CONST_INITIALIZER("ENETUNREACH", 11, 11),
      SCM_STRING_CONST_INITIALIZER("ENFILE", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ENOANO", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ENOBUFS", 7, 7),
      SCM_STRING_CONST_INITIALIZER("ENOCSI", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ENODATA", 7, 7),
      SCM_STRING_CONST_INITIALIZER("ENODEV", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ENOENT", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ENOEXEC", 7, 7),
      SCM_STRING_CONST_INITIALIZER("ENOKEY", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ENOLCK", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ENOLINK", 7, 7),
      SCM_STRING_CONST_INITIALIZER("ENOMEDIUM", 9, 9),
      SCM_STRING_CONST_INITIALIZER("ENOMEM", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ENOMSG", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ENONET", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ENOPKG", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ENOPROTOOPT", 11, 11),
      SCM_STRING_CONST_INITIALIZER("ENOSPC", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ENOSR", 5, 5),
      SCM_STRING_CONST_INITIALIZER("ENOSTR", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ENOSYS", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ENOTBLK", 7, 7),
      SCM_STRING_CONST_INITIALIZER("ENOTCONN", 8, 8),
      SCM_STRING_CONST_INITIALIZER("ENOTDIR", 7, 7),
      SCM_STRING_CONST_INITIALIZER("ENOTEMPTY", 9, 9),
      SCM_STRING_CONST_INITIALIZER("ENOTNAM", 7, 7),
      SCM_STRING_CONST_INITIALIZER("ENOTSOCK", 8, 8),
      SCM_STRING_CONST_INITIALIZER("ENOTTY", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ENOTUNIQ", 8, 8),
      SCM_STRING_CONST_INITIALIZER("ENXIO", 5, 5),
      SCM_STRING_CONST_INITIALIZER("EOPNOTSUPP", 10, 10),
      SCM_STRING_CONST_INITIALIZER("EOVERFLOW", 9, 9),
      SCM_STRING_CONST_INITIALIZER("EPERM", 5, 5),
      SCM_STRING_CONST_INITIALIZER("EPFNOSUPPORT", 12, 12),
      SCM_STRING_CONST_INITIALIZER("EPIPE", 5, 5),
      SCM_STRING_CONST_INITIALIZER("EPROTO", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EPROTONOSUPPORT", 15, 15),
      SCM_STRING_CONST_INITIALIZER("EPROTOTYPE", 10, 10),
      SCM_STRING_CONST_INITIALIZER("ERANGE", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EREMCHG", 7, 7),
      SCM_STRING_CONST_INITIALIZER("EREMOTE", 7, 7),
      SCM_STRING_CONST_INITIALIZER("EREMOTEIO", 9, 9),
      SCM_STRING_CONST_INITIALIZER("ERESTART", 8, 8),
      SCM_STRING_CONST_INITIALIZER("EROFS", 5, 5),
      SCM_STRING_CONST_INITIALIZER("ESHUTDOWN", 9, 9),
      SCM_STRING_CONST_INITIALIZER("ESOCKTNOSUPPORT", 15, 15),
      SCM_STRING_CONST_INITIALIZER("ESPIPE", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ESRCH", 5, 5),
      SCM_STRING_CONST_INITIALIZER("ESRMNT", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ESTALE", 6, 6),
      SCM_STRING_CONST_INITIALIZER("ESTRPIPE", 8, 8),
      SCM_STRING_CONST_INITIALIZER("ETIME", 5, 5),
      SCM_STRING_CONST_INITIALIZER("ETIMEDOUT", 9, 9),
      SCM_STRING_CONST_INITIALIZER("ETOOMANYREFS", 12, 12),
      SCM_STRING_CONST_INITIALIZER("ETXTBSY", 7, 7),
      SCM_STRING_CONST_INITIALIZER("EUCLEAN", 7, 7),
      SCM_STRING_CONST_INITIALIZER("EUNATCH", 7, 7),
      SCM_STRING_CONST_INITIALIZER("EUSERS", 6, 6),
      SCM_STRING_CONST_INITIALIZER("EWOULDBLOCK", 11, 11),
      SCM_STRING_CONST_INITIALIZER("EXDEV", 5, 5),
      SCM_STRING_CONST_INITIALIZER("EXFULL", 6, 6),
      SCM_STRING_CONST_INITIALIZER("sys-getgrgid", 12, 12),
      SCM_STRING_CONST_INITIALIZER("gid", 3, 3),
      SCM_STRING_CONST_INITIALIZER("<int>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("sys-getgrnam", 12, 12),
      SCM_STRING_CONST_INITIALIZER("sys-gid->group-name", 19, 19),
      SCM_STRING_CONST_INITIALIZER("sys-group-name->gid", 19, 19),
      SCM_STRING_CONST_INITIALIZER("<const-cstring>", 15, 15),
      SCM_STRING_CONST_INITIALIZER("LC_ALL", 6, 6),
      SCM_STRING_CONST_INITIALIZER("LC_COLLATE", 10, 10),
      SCM_STRING_CONST_INITIALIZER("LC_CTYPE", 8, 8),
      SCM_STRING_CONST_INITIALIZER("LC_MONETARY", 11, 11),
      SCM_STRING_CONST_INITIALIZER("LC_NUMERIC", 10, 10),
      SCM_STRING_CONST_INITIALIZER("LC_TIME", 7, 7),
      SCM_STRING_CONST_INITIALIZER("sys-setlocale", 13, 13),
      SCM_STRING_CONST_INITIALIZER("category", 8, 8),
      SCM_STRING_CONST_INITIALIZER("locale", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<const-cstring>\077", 16, 16),
      SCM_STRING_CONST_INITIALIZER("sys-localeconv", 14, 14),
      SCM_STRING_CONST_INITIALIZER("decimal_point", 13, 13),
      SCM_STRING_CONST_INITIALIZER("thousands_sep", 13, 13),
      SCM_STRING_CONST_INITIALIZER("grouping", 8, 8),
      SCM_STRING_CONST_INITIALIZER("int_curr_symbol", 15, 15),
      SCM_STRING_CONST_INITIALIZER("currency_symbol", 15, 15),
      SCM_STRING_CONST_INITIALIZER("mon_decimal_point", 17, 17),
      SCM_STRING_CONST_INITIALIZER("mon_thousands_sep", 17, 17),
      SCM_STRING_CONST_INITIALIZER("mon_grouping", 12, 12),
      SCM_STRING_CONST_INITIALIZER("positive_sign", 13, 13),
      SCM_STRING_CONST_INITIALIZER("negative_sign", 13, 13),
      SCM_STRING_CONST_INITIALIZER("int_frac_digits", 15, 15),
      SCM_STRING_CONST_INITIALIZER("frac_digits", 11, 11),
      SCM_STRING_CONST_INITIALIZER("p_cs_precedes", 13, 13),
      SCM_STRING_CONST_INITIALIZER("p_sep_by_space", 14, 14),
      SCM_STRING_CONST_INITIALIZER("n_cs_precedes", 13, 13),
      SCM_STRING_CONST_INITIALIZER("n_sep_by_space", 14, 14),
      SCM_STRING_CONST_INITIALIZER("p_sign_posn", 11, 11),
      SCM_STRING_CONST_INITIALIZER("n_sign_posn", 11, 11),
      SCM_STRING_CONST_INITIALIZER("uid", 3, 3),
      SCM_STRING_CONST_INITIALIZER("sys-uid->user-name", 18, 18),
      SCM_STRING_CONST_INITIALIZER("sys-user-name->uid", 18, 18),
      SCM_STRING_CONST_INITIALIZER("SIG_SETMASK", 11, 11),
      SCM_STRING_CONST_INITIALIZER("SIG_BLOCK", 9, 9),
      SCM_STRING_CONST_INITIALIZER("SIG_UNBLOCK", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sys-sigset-add!", 15, 15),
      SCM_STRING_CONST_INITIALIZER("set", 3, 3),
      SCM_STRING_CONST_INITIALIZER("rest", 4, 4),
      SCM_STRING_CONST_INITIALIZER("sigs", 4, 4),
      SCM_STRING_CONST_INITIALIZER("<sys-sigset>", 12, 12),
      SCM_STRING_CONST_INITIALIZER("*", 1, 1),
      SCM_STRING_CONST_INITIALIZER("sys-sigset-delete!", 18, 18),
      SCM_STRING_CONST_INITIALIZER("sys-sigset-fill!", 16, 16),
      SCM_STRING_CONST_INITIALIZER("sys-sigset-empty!", 17, 17),
      SCM_STRING_CONST_INITIALIZER("sys-signal-name", 15, 15),
      SCM_STRING_CONST_INITIALIZER("sig", 3, 3),
      SCM_STRING_CONST_INITIALIZER("sys-kill", 8, 8),
      SCM_STRING_CONST_INITIALIZER("process", 7, 7),
      SCM_STRING_CONST_INITIALIZER("<void>", 6, 6),
      SCM_STRING_CONST_INITIALIZER("set-signal-handler!", 19, 19),
      SCM_STRING_CONST_INITIALIZER("proc", 4, 4),
      SCM_STRING_CONST_INITIALIZER("optional", 8, 8),
      SCM_STRING_CONST_INITIALIZER("mask", 4, 4),
      SCM_STRING_CONST_INITIALIZER("get-signal-handler", 18, 18),
      SCM_STRING_CONST_INITIALIZER("get-signal-handler-mask", 23, 23),
      SCM_STRING_CONST_INITIALIZER("get-signal-handlers", 19, 19),
      SCM_STRING_CONST_INITIALIZER("set-signal-pending-limit", 24, 24),
      SCM_STRING_CONST_INITIALIZER("limit", 5, 5),
      SCM_STRING_CONST_INITIALIZER("get-signal-pending-limit", 24, 24),
      SCM_STRING_CONST_INITIALIZER("sys-sigmask", 11, 11),
      SCM_STRING_CONST_INITIALIZER("how", 3, 3),
      SCM_STRING_CONST_INITIALIZER("<sys-sigset>\077", 13, 13),
      SCM_STRING_CONST_INITIALIZER("sys-sigsuspend", 14, 14),
      SCM_STRING_CONST_INITIALIZER("sys-sigwait", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sys-sigset", 10, 10),
      SCM_STRING_CONST_INITIALIZER("make", 4, 4),
      SCM_STRING_CONST_INITIALIZER("signals", 7, 7),
      SCM_STRING_CONST_INITIALIZER("sys-remove", 10, 10),
      SCM_STRING_CONST_INITIALIZER("filename", 8, 8),
      SCM_STRING_CONST_INITIALIZER("sys-rename", 10, 10),
      SCM_STRING_CONST_INITIALIZER("oldname", 7, 7),
      SCM_STRING_CONST_INITIALIZER("newname", 7, 7),
      SCM_STRING_CONST_INITIALIZER("sys-tmpnam", 10, 10),
      SCM_STRING_CONST_INITIALIZER("sys-mkstemp", 11, 11),
      SCM_STRING_CONST_INITIALIZER("template", 8, 8),
      SCM_STRING_CONST_INITIALIZER("sys-mkdtemp", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sys-ctermid", 11, 11),
      SCM_STRING_CONST_INITIALIZER("CON", 3, 3),
      SCM_STRING_CONST_INITIALIZER("sys-exit", 8, 8),
      SCM_STRING_CONST_INITIALIZER("code", 4, 4),
      SCM_STRING_CONST_INITIALIZER("sys-abort", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-system", 10, 10),
      SCM_STRING_CONST_INITIALIZER("command", 7, 7),
      SCM_STRING_CONST_INITIALIZER("sys-random", 10, 10),
      SCM_STRING_CONST_INITIALIZER("<long>", 6, 6),
      SCM_STRING_CONST_INITIALIZER("sys-srandom", 11, 11),
      SCM_STRING_CONST_INITIALIZER("seed", 4, 4),
      SCM_STRING_CONST_INITIALIZER("RAND_MAX", 8, 8),
      SCM_STRING_CONST_INITIALIZER("sys-environ", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sys-setenv", 10, 10),
      SCM_STRING_CONST_INITIALIZER("value", 5, 5),
      SCM_STRING_CONST_INITIALIZER("overwrite", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-unsetenv", 12, 12),
      SCM_STRING_CONST_INITIALIZER("sys-clearenv", 12, 12),
      SCM_STRING_CONST_INITIALIZER("both", 4, 4),
      SCM_STRING_CONST_INITIALIZER("string-scan", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sys-environ->alist", 18, 18),
      SCM_STRING_CONST_INITIALIZER("G1802", 5, 5),
      SCM_STRING_CONST_INITIALIZER("envstr", 6, 6),
      SCM_STRING_CONST_INITIALIZER("too many arguments for", 22, 22),
      SCM_STRING_CONST_INITIALIZER("lambda", 6, 6),
      SCM_STRING_CONST_INITIALIZER("envlist", 7, 7),
      SCM_STRING_CONST_INITIALIZER("map", 3, 3),
      SCM_STRING_CONST_INITIALIZER("^", 1, 1),
      SCM_STRING_CONST_INITIALIZER("receive", 7, 7),
      SCM_STRING_CONST_INITIALIZER("pre", 3, 3),
      SCM_STRING_CONST_INITIALIZER("post", 4, 4),
      SCM_STRING_CONST_INITIALIZER("quote", 5, 5),
      SCM_STRING_CONST_INITIALIZER("if", 2, 2),
      SCM_STRING_CONST_INITIALIZER("cons", 4, 4),
      SCM_STRING_CONST_INITIALIZER("sys-putenv", 10, 10),
      SCM_STRING_CONST_INITIALIZER("bad type of argument for ~s: ~s", 31, 31),
      SCM_STRING_CONST_INITIALIZER("arg", 3, 3),
      SCM_STRING_CONST_INITIALIZER("sys-putenv: argument doesn't contain '=':", 41, 41),
      SCM_STRING_CONST_INITIALIZER("name=value", 10, 10),
      SCM_STRING_CONST_INITIALIZER("other", 5, 5),
      SCM_STRING_CONST_INITIALIZER("sys-strerror", 12, 12),
      SCM_STRING_CONST_INITIALIZER("errno_", 6, 6),
      SCM_STRING_CONST_INITIALIZER("sys-mmap", 8, 8),
      SCM_STRING_CONST_INITIALIZER("maybe-port", 10, 10),
      SCM_STRING_CONST_INITIALIZER("prot", 4, 4),
      SCM_STRING_CONST_INITIALIZER("flags", 5, 5),
      SCM_STRING_CONST_INITIALIZER("size", 4, 4),
      SCM_STRING_CONST_INITIALIZER("off", 3, 3),
      SCM_STRING_CONST_INITIALIZER("<size_t>", 8, 8),
      SCM_STRING_CONST_INITIALIZER("PROT_EXEC", 9, 9),
      SCM_STRING_CONST_INITIALIZER("PROT_READ", 9, 9),
      SCM_STRING_CONST_INITIALIZER("PROT_WRITE", 10, 10),
      SCM_STRING_CONST_INITIALIZER("PROT_NONE", 9, 9),
      SCM_STRING_CONST_INITIALIZER("MAP_SHARED", 10, 10),
      SCM_STRING_CONST_INITIALIZER("MAP_PRIVATE", 11, 11),
      SCM_STRING_CONST_INITIALIZER("MAP_ANONYMOUS", 13, 13),
      SCM_STRING_CONST_INITIALIZER("sys-stat", 8, 8),
      SCM_STRING_CONST_INITIALIZER("path", 4, 4),
      SCM_STRING_CONST_INITIALIZER("<sys-stat>", 10, 10),
      SCM_STRING_CONST_INITIALIZER("sys-fstat", 9, 9),
      SCM_STRING_CONST_INITIALIZER("port-or-fd", 10, 10),
      SCM_STRING_CONST_INITIALIZER("file-exists\077", 12, 12),
      SCM_STRING_CONST_INITIALIZER("<boolean>", 9, 9),
      SCM_STRING_CONST_INITIALIZER("file-is-regular\077", 16, 16),
      SCM_STRING_CONST_INITIALIZER("file-is-directory\077", 18, 18),
      SCM_STRING_CONST_INITIALIZER("sys-utime", 9, 9),
      SCM_STRING_CONST_INITIALIZER("atime", 5, 5),
      SCM_STRING_CONST_INITIALIZER("mtime", 5, 5),
      SCM_STRING_CONST_INITIALIZER("sys-times", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-uname", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-wait", 8, 8),
      SCM_STRING_CONST_INITIALIZER("nohang", 6, 6),
      SCM_STRING_CONST_INITIALIZER("untraced", 8, 8),
      SCM_STRING_CONST_INITIALIZER("sys-waitpid", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sys-wait-exited\077", 16, 16),
      SCM_STRING_CONST_INITIALIZER("status", 6, 6),
      SCM_STRING_CONST_INITIALIZER("sys-wait-exit-status", 20, 20),
      SCM_STRING_CONST_INITIALIZER("sys-wait-signaled\077", 18, 18),
      SCM_STRING_CONST_INITIALIZER("sys-wait-termsig", 16, 16),
      SCM_STRING_CONST_INITIALIZER("sys-wait-stopped\077", 17, 17),
      SCM_STRING_CONST_INITIALIZER("sys-wait-stopsig", 16, 16),
      SCM_STRING_CONST_INITIALIZER("sys-time", 8, 8),
      SCM_STRING_CONST_INITIALIZER("sys-gettimeofday", 16, 16),
      SCM_STRING_CONST_INITIALIZER("<ulong>", 7, 7),
      SCM_STRING_CONST_INITIALIZER("current-microseconds", 20, 20),
      SCM_STRING_CONST_INITIALIZER("sys-clock-gettime-monotonic", 27, 27),
      SCM_STRING_CONST_INITIALIZER("sys-clock-getres-monotonic", 26, 26),
      SCM_STRING_CONST_INITIALIZER("current-time", 12, 12),
      SCM_STRING_CONST_INITIALIZER("time\077", 5, 5),
      SCM_STRING_CONST_INITIALIZER("obj", 3, 3),
      SCM_STRING_CONST_INITIALIZER("absolute-time", 13, 13),
      SCM_STRING_CONST_INITIALIZER("time-utc", 8, 8),
      SCM_STRING_CONST_INITIALIZER("t0", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<time>\077", 7, 7),
      SCM_STRING_CONST_INITIALIZER("seconds+", 8, 8),
      SCM_STRING_CONST_INITIALIZER("<time>", 6, 6),
      SCM_STRING_CONST_INITIALIZER("of-type\077", 8, 8),
      SCM_STRING_CONST_INITIALIZER("t", 1, 1),
      SCM_STRING_CONST_INITIALIZER("type-error", 10, 10),
      SCM_STRING_CONST_INITIALIZER("<real>", 6, 6),
      SCM_STRING_CONST_INITIALIZER("dt", 2, 2),
      SCM_STRING_CONST_INITIALIZER("time->seconds", 13, 13),
      SCM_STRING_CONST_INITIALIZER("seconds->time", 13, 13),
      SCM_STRING_CONST_INITIALIZER("<double>", 8, 8),
      SCM_STRING_CONST_INITIALIZER("time-comparator", 15, 15),
      SCM_STRING_CONST_INITIALIZER("compare", 7, 7),
      SCM_STRING_CONST_INITIALIZER("a", 1, 1),
      SCM_STRING_CONST_INITIALIZER("b", 1, 1),
      SCM_STRING_CONST_INITIALIZER("default-hash", 12, 12),
      SCM_STRING_CONST_INITIALIZER("make-comparator", 15, 15),
      SCM_STRING_CONST_INITIALIZER("sys-asctime", 11, 11),
      SCM_STRING_CONST_INITIALIZER("tm", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<sys-tm>", 8, 8),
      SCM_STRING_CONST_INITIALIZER("sys-ctime", 9, 9),
      SCM_STRING_CONST_INITIALIZER("time", 4, 4),
      SCM_STRING_CONST_INITIALIZER("sys-difftime", 12, 12),
      SCM_STRING_CONST_INITIALIZER("time1", 5, 5),
      SCM_STRING_CONST_INITIALIZER("time0", 5, 5),
      SCM_STRING_CONST_INITIALIZER("sys-strftime", 12, 12),
      SCM_STRING_CONST_INITIALIZER("format", 6, 6),
      SCM_STRING_CONST_INITIALIZER("sys-gmtime", 10, 10),
      SCM_STRING_CONST_INITIALIZER("sys-localtime", 13, 13),
      SCM_STRING_CONST_INITIALIZER("sys-mktime", 10, 10),
      SCM_STRING_CONST_INITIALIZER("R_OK", 4, 4),
      SCM_STRING_CONST_INITIALIZER("W_OK", 4, 4),
      SCM_STRING_CONST_INITIALIZER("X_OK", 4, 4),
      SCM_STRING_CONST_INITIALIZER("F_OK", 4, 4),
      SCM_STRING_CONST_INITIALIZER("sys-access", 10, 10),
      SCM_STRING_CONST_INITIALIZER("amode", 5, 5),
      SCM_STRING_CONST_INITIALIZER("sys-chdir", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-chmod", 9, 9),
      SCM_STRING_CONST_INITIALIZER("mode", 4, 4),
      SCM_STRING_CONST_INITIALIZER("sys-chown", 9, 9),
      SCM_STRING_CONST_INITIALIZER("owner", 5, 5),
      SCM_STRING_CONST_INITIALIZER("group", 5, 5),
      SCM_STRING_CONST_INITIALIZER("sys-fork", 8, 8),
      SCM_STRING_CONST_INITIALIZER("iomap", 5, 5),
      SCM_STRING_CONST_INITIALIZER("sigmask", 7, 7),
      SCM_STRING_CONST_INITIALIZER("directory", 9, 9),
      SCM_STRING_CONST_INITIALIZER("detached", 8, 8),
      SCM_STRING_CONST_INITIALIZER("sys-exec", 8, 8),
      SCM_STRING_CONST_INITIALIZER("args", 4, 4),
      SCM_STRING_CONST_INITIALIZER("<list>", 6, 6),
      SCM_STRING_CONST_INITIALIZER("sys-fork-and-exec", 17, 17),
      SCM_STRING_CONST_INITIALIZER("sys-getegid", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sys-getgid", 10, 10),
      SCM_STRING_CONST_INITIALIZER("sys-geteuid", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sys-setugid\077", 12, 12),
      SCM_STRING_CONST_INITIALIZER("sys-getpid", 10, 10),
      SCM_STRING_CONST_INITIALIZER("sys-getppid", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sys-setpgrp", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sys-setpgid", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sys-getlogin", 12, 12),
      SCM_STRING_CONST_INITIALIZER("sys-link", 8, 8),
      SCM_STRING_CONST_INITIALIZER("existing", 8, 8),
      SCM_STRING_CONST_INITIALIZER("newpath", 7, 7),
      SCM_STRING_CONST_INITIALIZER("sys-pause", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-alarm", 9, 9),
      SCM_STRING_CONST_INITIALIZER("seconds", 7, 7),
      SCM_STRING_CONST_INITIALIZER("(pipe)", 6, 6),
      SCM_STRING_CONST_INITIALIZER("buffering", 9, 9),
      SCM_STRING_CONST_INITIALIZER("buffered\077", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-pipe", 8, 8),
      SCM_STRING_CONST_INITIALIZER("sys-close", 9, 9),
      SCM_STRING_CONST_INITIALIZER("fd", 2, 2),
      SCM_STRING_CONST_INITIALIZER("sys-mkdir", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-rmdir", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-umask", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-sleep", 9, 9),
      SCM_STRING_CONST_INITIALIZER("no-retry", 8, 8),
      SCM_STRING_CONST_INITIALIZER("sys-unlink", 10, 10),
      SCM_STRING_CONST_INITIALIZER("sys-isatty", 10, 10),
      SCM_STRING_CONST_INITIALIZER("port_or_fd", 10, 10),
      SCM_STRING_CONST_INITIALIZER("sys-ttyname", 11, 11),
      SCM_STRING_CONST_INITIALIZER("sys-truncate", 12, 12),
      SCM_STRING_CONST_INITIALIZER("length", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<integer>", 9, 9),
      SCM_STRING_CONST_INITIALIZER("sys-ftruncate", 13, 13),
      SCM_STRING_CONST_INITIALIZER("sys-gethostname", 15, 15),
      SCM_STRING_CONST_INITIALIZER("sys-getdomainname", 17, 17),
      SCM_STRING_CONST_INITIALIZER("sys-available-processors", 24, 24),
      SCM_STRING_CONST_INITIALIZER("glob", 4, 4),
      SCM_STRING_CONST_INITIALIZER("glob-fold", 9, 9),
      SCM_STRING_CONST_INITIALIZER("patterns", 8, 8),
      SCM_STRING_CONST_INITIALIZER("opts", 4, 4),
      SCM_STRING_CONST_INITIALIZER("sys-glob", 8, 8),
      SCM_STRING_CONST_INITIALIZER("glob-fs-folder", 14, 14),
      SCM_STRING_CONST_INITIALIZER("sort", 4, 4),
      SCM_STRING_CONST_INITIALIZER("glob-fold-1", 11, 11),
      SCM_STRING_CONST_INITIALIZER("G1901", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1902", 5, 5),
      SCM_STRING_CONST_INITIALIZER("glob-expand-braces", 18, 18),
      SCM_STRING_CONST_INITIALIZER("list\077", 5, 5),
      SCM_STRING_CONST_INITIALIZER("fold", 4, 4),
      SCM_STRING_CONST_INITIALIZER("separator", 9, 9),
      SCM_STRING_CONST_INITIALIZER("folder", 6, 6),
      SCM_STRING_CONST_INITIALIZER("sorter", 6, 6),
      SCM_STRING_CONST_INITIALIZER("prefix", 6, 6),
      SCM_STRING_CONST_INITIALIZER("**", 2, 2),
      SCM_STRING_CONST_INITIALIZER("rec", 3, 3),
      SCM_STRING_CONST_INITIALIZER("node", 4, 4),
      SCM_STRING_CONST_INITIALIZER("matcher", 7, 7),
      SCM_STRING_CONST_INITIALIZER("rec*", 4, 4),
      SCM_STRING_CONST_INITIALIZER("G1903", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1904", 5, 5),
      SCM_STRING_CONST_INITIALIZER("^[^.].*$", 8, 8),
      SCM_STRING_CONST_INITIALIZER("glob-prepare-pattern", 20, 20),
      SCM_STRING_CONST_INITIALIZER("pattern", 7, 7),
      SCM_STRING_CONST_INITIALIZER("dir\077", 4, 4),
      SCM_STRING_CONST_INITIALIZER("glob-component->regexp", 22, 22),
      SCM_STRING_CONST_INITIALIZER("f", 1, 1),
      SCM_STRING_CONST_INITIALIZER("comp", 4, 4),
      SCM_STRING_CONST_INITIALIZER("pat", 3, 3),
      SCM_STRING_CONST_INITIALIZER("[{}]", 4, 4),
      SCM_STRING_CONST_INITIALIZER("{", 1, 1),
      SCM_STRING_CONST_INITIALIZER("parse", 5, 5),
      SCM_STRING_CONST_INITIALIZER("loop", 4, 4),
      SCM_STRING_CONST_INITIALIZER("in", 2, 2),
      SCM_STRING_CONST_INITIALIZER("seg", 3, 3),
      SCM_STRING_CONST_INITIALIZER("before", 6, 6),
      SCM_STRING_CONST_INITIALIZER("G1907", 5, 5),
      SCM_STRING_CONST_INITIALIZER("extra closing curly-brace in glob pattern:", 42, 42),
      SCM_STRING_CONST_INITIALIZER("G1910", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1913", 5, 5),
      SCM_STRING_CONST_INITIALIZER("unclosed curly-brace in glob pattern:", 37, 37),
      SCM_STRING_CONST_INITIALIZER("_", 1, 1),
      SCM_STRING_CONST_INITIALIZER("str", 3, 3),
      SCM_STRING_CONST_INITIALIZER("pres", 4, 4),
      SCM_STRING_CONST_INITIALIZER("level", 5, 5),
      SCM_STRING_CONST_INITIALIZER("eol", 3, 3),
      SCM_STRING_CONST_INITIALIZER("rep", 3, 3),
      SCM_STRING_CONST_INITIALIZER("any", 3, 3),
      SCM_STRING_CONST_INITIALIZER("read-char-set", 13, 13),
      SCM_STRING_CONST_INITIALIZER("char-set-complement!", 20, 20),
      SCM_STRING_CONST_INITIALIZER("element1", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G1917", 5, 5),
      SCM_STRING_CONST_INITIALIZER("ch", 2, 2),
      SCM_STRING_CONST_INITIALIZER("ct", 2, 2),
      SCM_STRING_CONST_INITIALIZER("bol", 3, 3),
      SCM_STRING_CONST_INITIALIZER("seq", 3, 3),
      SCM_STRING_CONST_INITIALIZER("with-input-from-string", 22, 22),
      SCM_STRING_CONST_INITIALIZER("regexp-optimize", 15, 15),
      SCM_STRING_CONST_INITIALIZER("regexp-compile", 14, 14),
      SCM_STRING_CONST_INITIALIZER("fixed-regexp\077", 13, 13),
      SCM_STRING_CONST_INITIALIZER("regexp-ast", 10, 10),
      SCM_STRING_CONST_INITIALIZER("list->string", 12, 12),
      SCM_STRING_CONST_INITIALIZER("rx", 2, 2),
      SCM_STRING_CONST_INITIALIZER("make-glob-fs-fold", 17, 17),
      SCM_STRING_CONST_INITIALIZER("gauche-architecture", 19, 19),
      SCM_STRING_CONST_INITIALIZER("mingw", 5, 5),
      SCM_STRING_CONST_INITIALIZER("string-length", 13, 13),
      SCM_STRING_CONST_INITIALIZER("string-ref", 10, 10),
      SCM_STRING_CONST_INITIALIZER("ensure-dirname", 14, 14),
      SCM_STRING_CONST_INITIALIZER("s", 1, 1),
      SCM_STRING_CONST_INITIALIZER("loop1920", 8, 8),
      SCM_STRING_CONST_INITIALIZER("child", 5, 5),
      SCM_STRING_CONST_INITIALIZER("regexp", 6, 6),
      SCM_STRING_CONST_INITIALIZER("non-leaf\077", 9, 9),
      SCM_STRING_CONST_INITIALIZER("root-path", 9, 9),
      SCM_STRING_CONST_INITIALIZER("current-path", 12, 12),
      SCM_STRING_CONST_INITIALIZER("%sys-mintty\077", 12, 12),
      SCM_STRING_CONST_INITIALIZER("sys-win-pipe-name", 17, 17),
      SCM_STRING_CONST_INITIALIZER("module-binds\077", 13, 13),
      SCM_STRING_CONST_INITIALIZER("^\134\134msys-[0-9a-f]+-pty[0-9]+-((\077:to|from))-master.*$", 51, 51),
      SCM_STRING_CONST_INITIALIZER("boolean", 7, 7),
      SCM_STRING_CONST_INITIALIZER("make-string", 11, 11),
      SCM_STRING_CONST_INITIALIZER("\042", 1, 1),
      SCM_STRING_CONST_INITIALIZER("%sys-escape-windows-command-line", 32, 32),
      SCM_STRING_CONST_INITIALIZER("G1928", 5, 5),
      SCM_STRING_CONST_INITIALIZER("m", 1, 1),
      SCM_STRING_CONST_INITIALIZER("\042\042", 2, 2),
      SCM_STRING_CONST_INITIALIZER("[!\042%&()<>^|]", 12, 12),
      SCM_STRING_CONST_INITIALIZER("It is unsafe to pass argument ~s to BAT or CMD file.", 52, 52),
      SCM_STRING_CONST_INITIALIZER("[\134u0009-\134u000d !&'+,;->\134[\134]^`{}~]", 33, 33),
      SCM_STRING_CONST_INITIALIZER("(\134\134*)\042", 6, 6),
      SCM_STRING_CONST_INITIALIZER("regexp-replace-all", 18, 18),
      SCM_STRING_CONST_INITIALIZER("write-to-string", 15, 15),
      SCM_STRING_CONST_INITIALIZER("batfilep", 8, 8),
      SCM_STRING_CONST_INITIALIZER("loop1790", 8, 8),
      SCM_STRING_CONST_INITIALIZER("args1789", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G1791", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1792", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1793", 5, 5),
      SCM_STRING_CONST_INITIALIZER("tmp", 3, 3),
      SCM_STRING_CONST_INITIALIZER("case", 4, 4),
      SCM_STRING_CONST_INITIALIZER("windows\077", 8, 8),
      SCM_STRING_CONST_INITIALIZER("comps", 5, 5),
      SCM_STRING_CONST_INITIALIZER("r", 1, 1),
      SCM_STRING_CONST_INITIALIZER("root\077", 5, 5),
      SCM_STRING_CONST_INITIALIZER("reverse", 7, 7),
      SCM_STRING_CONST_INITIALIZER("canon-path", 10, 10),
      SCM_STRING_CONST_INITIALIZER("abs-path", 8, 8),
      SCM_STRING_CONST_INITIALIZER("expand-tilde", 12, 12),
      SCM_STRING_CONST_INITIALIZER("absolute\077", 9, 9),
      SCM_STRING_CONST_INITIALIZER("home", 4, 4),
      SCM_STRING_CONST_INITIALIZER("pw", 2, 2),
      SCM_STRING_CONST_INITIALIZER("get-unix-home", 13, 13),
      SCM_STRING_CONST_INITIALIZER("get-windows-home", 16, 16),
      SCM_STRING_CONST_INITIALIZER("user", 4, 4),
      SCM_STRING_CONST_INITIALIZER("let*", 4, 4),
      SCM_STRING_CONST_INITIALIZER("assq", 4, 4),
      SCM_STRING_CONST_INITIALIZER("rest1788", 8, 8),
      SCM_STRING_CONST_INITIALIZER("if-let1", 7, 7),
      SCM_STRING_CONST_INITIALIZER("$", 1, 1),
      SCM_STRING_CONST_INITIALIZER("define-in-module", 16, 16),
      SCM_STRING_CONST_INITIALIZER("G1800", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1801", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest1799", 8, 8),
      SCM_STRING_CONST_INITIALIZER("string\077", 7, 7),
      SCM_STRING_CONST_INITIALIZER("check-arg", 9, 9),
      SCM_STRING_CONST_INITIALIZER("v", 1, 1),
      SCM_STRING_CONST_INITIALIZER("assume-type", 11, 11),
      SCM_STRING_CONST_INITIALIZER("zero\077", 5, 5),
      SCM_STRING_CONST_INITIALIZER("<", 1, 1),
      SCM_STRING_CONST_INITIALIZER("loop1896", 8, 8),
      SCM_STRING_CONST_INITIALIZER("args1895", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G1897", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1898", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1899", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1900", 5, 5),
      SCM_STRING_CONST_INITIALIZER("cut", 3, 3),
      SCM_STRING_CONST_INITIALIZER("<>", 2, 2),
      SCM_STRING_CONST_INITIALIZER("rest1894", 8, 8),
      SCM_STRING_CONST_INITIALIZER("p", 1, 1),
      SCM_STRING_CONST_INITIALIZER("G1905", 5, 5),
      SCM_STRING_CONST_INITIALIZER("append", 6, 6),
      SCM_STRING_CONST_INITIALIZER("segs", 4, 4),
      SCM_STRING_CONST_INITIALIZER("ins", 3, 3),
      SCM_STRING_CONST_INITIALIZER("G1906", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1908", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1909", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1911", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1912", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1914", 5, 5),
      SCM_STRING_CONST_INITIALIZER("values", 6, 6),
      SCM_STRING_CONST_INITIALIZER("cute", 4, 4),
      SCM_STRING_CONST_INITIALIZER("letrec", 6, 6),
      SCM_STRING_CONST_INITIALIZER("=", 1, 1),
      SCM_STRING_CONST_INITIALIZER("G1916", 5, 5),
      SCM_STRING_CONST_INITIALIZER("=>", 2, 2),
      SCM_STRING_CONST_INITIALIZER("^m", 2, 2),
      SCM_STRING_CONST_INITIALIZER("values-ref", 10, 10),
      SCM_STRING_CONST_INITIALIZER("G1915", 5, 5),
      SCM_STRING_CONST_INITIALIZER("n", 1, 1),
      SCM_STRING_CONST_INITIALIZER("cs", 2, 2),
      SCM_STRING_CONST_INITIALIZER("current-input-port", 18, 18),
      SCM_STRING_CONST_INITIALIZER("peek-char", 9, 9),
      SCM_STRING_CONST_INITIALIZER("eqv\077", 4, 4),
      SCM_STRING_CONST_INITIALIZER("unquote-splicing", 16, 16),
      SCM_STRING_CONST_INITIALIZER("element1*", 9, 9),
      SCM_STRING_CONST_INITIALIZER("eof-object\077", 11, 11),
      SCM_STRING_CONST_INITIALIZER("quasiquote", 10, 10),
      SCM_STRING_CONST_INITIALIZER("unquote", 7, 7),
      SCM_STRING_CONST_INITIALIZER("ra", 2, 2),
      SCM_STRING_CONST_INITIALIZER("element0", 8, 8),
      SCM_STRING_CONST_INITIALIZER("nd", 2, 2),
      SCM_STRING_CONST_INITIALIZER("element0*", 9, 9),
      SCM_STRING_CONST_INITIALIZER("read-char", 9, 9),
      SCM_STRING_CONST_INITIALIZER("char\077", 5, 5),
      SCM_STRING_CONST_INITIALIZER("x", 1, 1),
      SCM_STRING_CONST_INITIALIZER("ast", 3, 3),
      SCM_STRING_CONST_INITIALIZER("caddr", 5, 5),
      SCM_STRING_CONST_INITIALIZER(">", 1, 1),
      SCM_STRING_CONST_INITIALIZER("cdddr", 5, 5),
      SCM_STRING_CONST_INITIALIZER("separ", 5, 5),
      SCM_STRING_CONST_INITIALIZER("and-let*", 8, 8),
      SCM_STRING_CONST_INITIALIZER("len", 3, 3),
      SCM_STRING_CONST_INITIALIZER("-", 1, 1),
      SCM_STRING_CONST_INITIALIZER("full", 4, 4),
      SCM_STRING_CONST_INITIALIZER("root-path/", 10, 10),
      SCM_STRING_CONST_INITIALIZER("current-path/", 13, 13),
      SCM_STRING_CONST_INITIALIZER("^s", 2, 2),
      SCM_STRING_CONST_INITIALIZER("args1919", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G1921", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1922", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest1918", 8, 8),
  },
};
static struct scm__rcRec {
#if defined(GAUCHE_WINDOWS)
  ScmPair d1927[35] SCM_ALIGN_PAIR;
#endif /*defined(GAUCHE_WINDOWS)*/
#if defined(GAUCHE_WINDOWS)
  ScmObj d1925[40];
#endif /*defined(GAUCHE_WINDOWS)*/
#if defined(HAVE_SELECT)
  ScmPair d1892[70] SCM_ALIGN_PAIR;
#endif /*defined(HAVE_SELECT)*/
#if defined(HAVE_SELECT)
  ScmObj d1891[83];
#endif /*defined(HAVE_SELECT)*/
#if defined(HAVE_READLINK)
  ScmPair d1886[10] SCM_ALIGN_PAIR;
#endif /*defined(HAVE_READLINK)*/
#if defined(HAVE_READLINK)
  ScmObj d1885[16];
#endif /*defined(HAVE_READLINK)*/
#if defined(HAVE_SYMLINK)
  ScmPair d1883[11] SCM_ALIGN_PAIR;
#endif /*defined(HAVE_SYMLINK)*/
#if defined(HAVE_SYMLINK)
  ScmObj d1882[18];
#endif /*defined(HAVE_SYMLINK)*/
#if defined(HAVE_CRYPT)
  ScmPair d1880[11] SCM_ALIGN_PAIR;
#endif /*defined(HAVE_CRYPT)*/
#if defined(HAVE_CRYPT)
  ScmObj d1879[17];
#endif /*defined(HAVE_CRYPT)*/
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  ScmPair d1877[12] SCM_ALIGN_PAIR;
#endif /*(defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))*/
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  ScmObj d1876[19];
#endif /*(defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))*/
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  ScmPair d1874[10] SCM_ALIGN_PAIR;
#endif /*defined(HAVE_SETGROUPS)*/
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  ScmObj d1873[16];
#endif /*defined(HAVE_SETGROUPS)*/
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  ScmPair d1869[10] SCM_ALIGN_PAIR;
#endif /*defined(HAVE_GETPGID)*/
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  ScmObj d1868[15];
#endif /*defined(HAVE_GETPGID)*/
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if defined(HAVE_LCHOWN)
  ScmPair d1866[12] SCM_ALIGN_PAIR;
#endif /*defined(HAVE_LCHOWN)*/
#if defined(HAVE_LCHOWN)
  ScmObj d1865[20];
#endif /*defined(HAVE_LCHOWN)*/
#if !(!(defined(GAUCHE_WINDOWS)))
  ScmPair d1860[10] SCM_ALIGN_PAIR;
#endif /*!(!(defined(GAUCHE_WINDOWS)))*/
#if !(!(defined(GAUCHE_WINDOWS)))
  ScmObj d1859[16];
#endif /*!(!(defined(GAUCHE_WINDOWS)))*/
#if !(defined(GAUCHE_WINDOWS))
  ScmPair d1857[90] SCM_ALIGN_PAIR;
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if !(defined(GAUCHE_WINDOWS))
  ScmObj d1856[87];
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_OFILE)
  ScmObj d1851[1];
#endif /*defined(RLIMIT_OFILE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_STACK)
  ScmObj d1849[1];
#endif /*defined(RLIMIT_STACK)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_SBSIZE)
  ScmObj d1847[1];
#endif /*defined(RLIMIT_SBSIZE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_SIGPENDING)
  ScmObj d1845[1];
#endif /*defined(RLIMIT_SIGPENDING)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_RTPRIO)
  ScmObj d1843[1];
#endif /*defined(RLIMIT_RTPRIO)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_RSS)
  ScmObj d1841[1];
#endif /*defined(RLIMIT_RSS)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NPROC)
  ScmObj d1839[1];
#endif /*defined(RLIMIT_NPROC)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NOFILE)
  ScmObj d1837[1];
#endif /*defined(RLIMIT_NOFILE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NICE)
  ScmObj d1835[1];
#endif /*defined(RLIMIT_NICE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_MSGQUEUE)
  ScmObj d1833[1];
#endif /*defined(RLIMIT_MSGQUEUE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_MEMLOCK)
  ScmObj d1831[1];
#endif /*defined(RLIMIT_MEMLOCK)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_LOCKS)
  ScmObj d1829[1];
#endif /*defined(RLIMIT_LOCKS)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_FSIZE)
  ScmObj d1827[1];
#endif /*defined(RLIMIT_FSIZE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_DATA)
  ScmObj d1825[1];
#endif /*defined(RLIMIT_DATA)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_CPU)
  ScmObj d1823[1];
#endif /*defined(RLIMIT_CPU)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_CORE)
  ScmObj d1821[1];
#endif /*defined(RLIMIT_CORE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_AS)
  ScmObj d1819[1];
#endif /*defined(RLIMIT_AS)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
  ScmPair d1818[22] SCM_ALIGN_PAIR;
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
  ScmObj d1817[35];
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if !(defined(HAVE_GETLOADAVG))
  ScmPair d1815[11] SCM_ALIGN_PAIR;
#endif /*!(defined(HAVE_GETLOADAVG))*/
#if !(defined(HAVE_GETLOADAVG))
  ScmObj d1814[17];
#endif /*!(defined(HAVE_GETLOADAVG))*/
#if defined(HAVE_GETLOADAVG)
  ScmPair d1812[11] SCM_ALIGN_PAIR;
#endif /*defined(HAVE_GETLOADAVG)*/
#if defined(HAVE_GETLOADAVG)
  ScmObj d1811[17];
#endif /*defined(HAVE_GETLOADAVG)*/
#if !(HAVE_STRSIGNAL)
  ScmPair d1808[10] SCM_ALIGN_PAIR;
#endif /*!(HAVE_STRSIGNAL)*/
#if !(HAVE_STRSIGNAL)
  ScmObj d1807[16];
#endif /*!(HAVE_STRSIGNAL)*/
#if HAVE_STRSIGNAL
  ScmPair d1805[10] SCM_ALIGN_PAIR;
#endif /*HAVE_STRSIGNAL*/
#if HAVE_STRSIGNAL
  ScmObj d1804[16];
#endif /*HAVE_STRSIGNAL*/
  ScmUVector d1796[59];
  ScmCompiledCode d1795[59];
  ScmWord d1794[2353];
  ScmPair d1787[1213] SCM_ALIGN_PAIR;
  ScmObj d1786[1666];
} scm__rc SCM_UNUSED = {
#if defined(GAUCHE_WINDOWS)
  {   /* ScmPair d1927 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1719U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1926[5]), SCM_OBJ(&scm__rc.d1927[2])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1927[3])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1927[5])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1927[6])},
       { SCM_OBJ(&scm__rc.d1927[7]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1927[4]), SCM_OBJ(&scm__rc.d1927[8])},
       { SCM_MAKE_INT(1720U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1926[5]), SCM_OBJ(&scm__rc.d1927[10])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1927[11])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1927[13])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1927[14])},
       { SCM_OBJ(&scm__rc.d1927[15]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1927[12]), SCM_OBJ(&scm__rc.d1927[16])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1724U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1926[5]), SCM_OBJ(&scm__rc.d1927[19])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1927[20])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1927[22])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1927[23])},
       { SCM_OBJ(&scm__rc.d1927[24]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1927[21]), SCM_OBJ(&scm__rc.d1927[25])},
       { SCM_MAKE_INT(1730U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1926[5]), SCM_OBJ(&scm__rc.d1927[27])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1927[28])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1927[30])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1927[31])},
       { SCM_OBJ(&scm__rc.d1927[32]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1927[29]), SCM_OBJ(&scm__rc.d1927[33])},
  },
#endif /*defined(GAUCHE_WINDOWS)*/
#if defined(GAUCHE_WINDOWS)
  {   /* ScmObj d1925 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
  },
#endif /*defined(GAUCHE_WINDOWS)*/
#if defined(HAVE_SELECT)
  {   /* ScmPair d1892 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[1])},
       { SCM_MAKE_INT(1411U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1889[4]), SCM_OBJ(&scm__rc.d1892[3])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[4])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[6])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[7])},
       { SCM_OBJ(&scm__rc.d1892[8]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1892[5]), SCM_OBJ(&scm__rc.d1892[9])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[11])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[12])},
       { SCM_MAKE_INT(1419U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1889[4]), SCM_OBJ(&scm__rc.d1892[14])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[15])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[17])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[18])},
       { SCM_OBJ(&scm__rc.d1892[19]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1892[16]), SCM_OBJ(&scm__rc.d1892[20])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1432U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1889[4]), SCM_OBJ(&scm__rc.d1892[23])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[24])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[26])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[27])},
       { SCM_OBJ(&scm__rc.d1892[28]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1892[25]), SCM_OBJ(&scm__rc.d1892[29])},
       { SCM_MAKE_INT(1435U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1889[4]), SCM_OBJ(&scm__rc.d1892[31])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[32])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[34])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[35])},
       { SCM_OBJ(&scm__rc.d1892[36]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1892[33]), SCM_OBJ(&scm__rc.d1892[37])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[39])},
       { SCM_MAKE_INT(1440U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1889[4]), SCM_OBJ(&scm__rc.d1892[41])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[42])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[44])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[45])},
       { SCM_OBJ(&scm__rc.d1892[46]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1892[43]), SCM_OBJ(&scm__rc.d1892[47])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[49])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[50])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[51])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[52])},
       { SCM_MAKE_INT(1445U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1889[4]), SCM_OBJ(&scm__rc.d1892[54])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[55])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[57])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[58])},
       { SCM_OBJ(&scm__rc.d1892[59]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1892[56]), SCM_OBJ(&scm__rc.d1892[60])},
       { SCM_MAKE_INT(1448U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1889[4]), SCM_OBJ(&scm__rc.d1892[62])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[63])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[65])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1892[66])},
       { SCM_OBJ(&scm__rc.d1892[67]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1892[64]), SCM_OBJ(&scm__rc.d1892[68])},
  },
#endif /*defined(HAVE_SELECT)*/
#if defined(HAVE_SELECT)
  {   /* ScmObj d1891 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(8, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
  },
#endif /*defined(HAVE_SELECT)*/
#if defined(HAVE_READLINK)
  {   /* ScmPair d1886 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1383U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1884[3]), SCM_OBJ(&scm__rc.d1886[2])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1886[3])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1886[5])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1886[6])},
       { SCM_OBJ(&scm__rc.d1886[7]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1886[4]), SCM_OBJ(&scm__rc.d1886[8])},
  },
#endif /*defined(HAVE_READLINK)*/
#if defined(HAVE_READLINK)
  {   /* ScmObj d1885 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
  },
#endif /*defined(HAVE_READLINK)*/
#if defined(HAVE_SYMLINK)
  {   /* ScmPair d1883 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1883[1])},
       { SCM_MAKE_INT(1373U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1881[4]), SCM_OBJ(&scm__rc.d1883[3])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1883[4])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1883[6])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1883[7])},
       { SCM_OBJ(&scm__rc.d1883[8]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1883[5]), SCM_OBJ(&scm__rc.d1883[9])},
  },
#endif /*defined(HAVE_SYMLINK)*/
#if defined(HAVE_SYMLINK)
  {   /* ScmObj d1882 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
  },
#endif /*defined(HAVE_SYMLINK)*/
#if defined(HAVE_CRYPT)
  {   /* ScmPair d1880 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1880[1])},
       { SCM_MAKE_INT(1338U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1878[4]), SCM_OBJ(&scm__rc.d1880[3])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1880[4])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1880[6])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1880[7])},
       { SCM_OBJ(&scm__rc.d1880[8]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1880[5]), SCM_OBJ(&scm__rc.d1880[9])},
  },
#endif /*defined(HAVE_CRYPT)*/
#if defined(HAVE_CRYPT)
  {   /* ScmObj d1879 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
  },
#endif /*defined(HAVE_CRYPT)*/
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  {   /* ScmPair d1877 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1877[1])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1877[2])},
       { SCM_MAKE_INT(1264U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1875[5]), SCM_OBJ(&scm__rc.d1877[4])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1877[5])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1877[7])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1877[8])},
       { SCM_OBJ(&scm__rc.d1877[9]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1877[6]), SCM_OBJ(&scm__rc.d1877[10])},
  },
#endif /*(defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))*/
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  {   /* ScmObj d1876 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
  },
#endif /*(defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))*/
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  {   /* ScmPair d1874 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1165U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1871[3]), SCM_OBJ(&scm__rc.d1874[2])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1874[3])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1874[5])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1874[6])},
       { SCM_OBJ(&scm__rc.d1874[7]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1874[4]), SCM_OBJ(&scm__rc.d1874[8])},
  },
#endif /*defined(HAVE_SETGROUPS)*/
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  {   /* ScmObj d1873 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
  },
#endif /*defined(HAVE_SETGROUPS)*/
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  {   /* ScmPair d1869 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1117U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1867[3]), SCM_OBJ(&scm__rc.d1869[2])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1869[3])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1869[5])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1869[6])},
       { SCM_OBJ(&scm__rc.d1869[7]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1869[4]), SCM_OBJ(&scm__rc.d1869[8])},
  },
#endif /*defined(HAVE_GETPGID)*/
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  {   /* ScmObj d1868 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
  },
#endif /*defined(HAVE_GETPGID)*/
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if defined(HAVE_LCHOWN)
  {   /* ScmPair d1866 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1866[1])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1866[2])},
       { SCM_MAKE_INT(1030U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1864[5]), SCM_OBJ(&scm__rc.d1866[4])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1866[5])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1866[7])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1866[8])},
       { SCM_OBJ(&scm__rc.d1866[9]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1866[6]), SCM_OBJ(&scm__rc.d1866[10])},
  },
#endif /*defined(HAVE_LCHOWN)*/
#if defined(HAVE_LCHOWN)
  {   /* ScmObj d1865 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
  },
#endif /*defined(HAVE_LCHOWN)*/
#if !(!(defined(GAUCHE_WINDOWS)))
  {   /* ScmPair d1860 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(728U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1858[3]), SCM_OBJ(&scm__rc.d1860[2])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1860[3])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1860[5])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1860[6])},
       { SCM_OBJ(&scm__rc.d1860[7]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1860[4]), SCM_OBJ(&scm__rc.d1860[8])},
  },
#endif /*!(!(defined(GAUCHE_WINDOWS)))*/
#if !(!(defined(GAUCHE_WINDOWS)))
  {   /* ScmObj d1859 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
  },
#endif /*!(!(defined(GAUCHE_WINDOWS)))*/
#if !(defined(GAUCHE_WINDOWS))
  {   /* ScmPair d1857 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(726U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1855[3]), SCM_OBJ(&scm__rc.d1857[2])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[3])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[5])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[6])},
       { SCM_OBJ(&scm__rc.d1857[7]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1857[4]), SCM_OBJ(&scm__rc.d1857[8])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[10])},
       { SCM_MAKE_INT(732U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1855[3]), SCM_OBJ(&scm__rc.d1857[12])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[13])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[15])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[16])},
       { SCM_OBJ(&scm__rc.d1857[17]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1857[14]), SCM_OBJ(&scm__rc.d1857[18])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[10])},
       { SCM_MAKE_INT(1010U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1855[3]), SCM_OBJ(&scm__rc.d1857[21])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[22])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[24])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[25])},
       { SCM_OBJ(&scm__rc.d1857[26]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1857[23]), SCM_OBJ(&scm__rc.d1857[27])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1099U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1855[3]), SCM_OBJ(&scm__rc.d1857[30])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[31])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[33])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[34])},
       { SCM_OBJ(&scm__rc.d1857[35]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1857[32]), SCM_OBJ(&scm__rc.d1857[36])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[38])},
       { SCM_MAKE_INT(1103U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1855[3]), SCM_OBJ(&scm__rc.d1857[40])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[41])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[43])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[44])},
       { SCM_OBJ(&scm__rc.d1857[45]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1857[42]), SCM_OBJ(&scm__rc.d1857[46])},
       { SCM_MAKE_INT(1122U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1855[3]), SCM_OBJ(&scm__rc.d1857[48])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[49])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[51])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[52])},
       { SCM_OBJ(&scm__rc.d1857[53]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1857[50]), SCM_OBJ(&scm__rc.d1857[54])},
       { SCM_MAKE_INT(1126U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1855[3]), SCM_OBJ(&scm__rc.d1857[56])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[57])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[59])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[60])},
       { SCM_OBJ(&scm__rc.d1857[61]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1857[58]), SCM_OBJ(&scm__rc.d1857[62])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1130U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1855[3]), SCM_OBJ(&scm__rc.d1857[65])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[66])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[68])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[69])},
       { SCM_OBJ(&scm__rc.d1857[70]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1857[67]), SCM_OBJ(&scm__rc.d1857[71])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1140U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1855[3]), SCM_OBJ(&scm__rc.d1857[74])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[75])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[77])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[78])},
       { SCM_OBJ(&scm__rc.d1857[79]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1857[76]), SCM_OBJ(&scm__rc.d1857[80])},
       { SCM_MAKE_INT(1148U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1855[3]), SCM_OBJ(&scm__rc.d1857[82])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[83])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[85])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1857[86])},
       { SCM_OBJ(&scm__rc.d1857[87]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1857[84]), SCM_OBJ(&scm__rc.d1857[88])},
  },
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if !(defined(GAUCHE_WINDOWS))
  {   /* ScmObj d1856 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(4, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(4, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
  },
#endif /*!(defined(GAUCHE_WINDOWS))*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_OFILE)
  {   /* ScmObj d1851 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_OFILE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_STACK)
  {   /* ScmObj d1849 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_STACK)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_SBSIZE)
  {   /* ScmObj d1847 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_SBSIZE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_SIGPENDING)
  {   /* ScmObj d1845 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_SIGPENDING)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_RTPRIO)
  {   /* ScmObj d1843 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_RTPRIO)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_RSS)
  {   /* ScmObj d1841 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_RSS)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NPROC)
  {   /* ScmObj d1839 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_NPROC)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NOFILE)
  {   /* ScmObj d1837 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_NOFILE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NICE)
  {   /* ScmObj d1835 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_NICE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_MSGQUEUE)
  {   /* ScmObj d1833 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_MSGQUEUE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_MEMLOCK)
  {   /* ScmObj d1831 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_MEMLOCK)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_LOCKS)
  {   /* ScmObj d1829 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_LOCKS)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_FSIZE)
  {   /* ScmObj d1827 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_FSIZE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_DATA)
  {   /* ScmObj d1825 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_DATA)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_CPU)
  {   /* ScmObj d1823 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_CPU)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_CORE)
  {   /* ScmObj d1821 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_CORE)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_AS)
  {   /* ScmObj d1819 */
    SCM_UNBOUND,
  },
#endif /*defined(RLIMIT_AS)*/
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
  {   /* ScmPair d1818 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(634U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1816[3]), SCM_OBJ(&scm__rc.d1818[2])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1818[3])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1818[5])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1818[6])},
       { SCM_OBJ(&scm__rc.d1818[7]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1818[4]), SCM_OBJ(&scm__rc.d1818[8])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1818[10])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1818[11])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1818[12])},
       { SCM_MAKE_INT(641U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1816[3]), SCM_OBJ(&scm__rc.d1818[14])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1818[15])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1818[17])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1818[18])},
       { SCM_OBJ(&scm__rc.d1818[19]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1818[16]), SCM_OBJ(&scm__rc.d1818[20])},
  },
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if defined(HAVE_SYS_RESOURCE_H)
  {   /* ScmObj d1817 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
  },
#endif /*defined(HAVE_SYS_RESOURCE_H)*/
#if !(defined(HAVE_GETLOADAVG))
  {   /* ScmPair d1815 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1815[1])},
       { SCM_MAKE_INT(574U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1813[4]), SCM_OBJ(&scm__rc.d1815[3])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1815[4])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1815[6])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1815[7])},
       { SCM_OBJ(&scm__rc.d1815[8]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1815[5]), SCM_OBJ(&scm__rc.d1815[9])},
  },
#endif /*!(defined(HAVE_GETLOADAVG))*/
#if !(defined(HAVE_GETLOADAVG))
  {   /* ScmObj d1814 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
  },
#endif /*!(defined(HAVE_GETLOADAVG))*/
#if defined(HAVE_GETLOADAVG)
  {   /* ScmPair d1812 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1812[1])},
       { SCM_MAKE_INT(562U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1809[4]), SCM_OBJ(&scm__rc.d1812[3])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1812[4])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1812[6])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1812[7])},
       { SCM_OBJ(&scm__rc.d1812[8]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1812[5]), SCM_OBJ(&scm__rc.d1812[9])},
  },
#endif /*defined(HAVE_GETLOADAVG)*/
#if defined(HAVE_GETLOADAVG)
  {   /* ScmObj d1811 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
  },
#endif /*defined(HAVE_GETLOADAVG)*/
#if !(HAVE_STRSIGNAL)
  {   /* ScmPair d1808 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(554U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1806[3]), SCM_OBJ(&scm__rc.d1808[2])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1808[3])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1808[5])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1808[6])},
       { SCM_OBJ(&scm__rc.d1808[7]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1808[4]), SCM_OBJ(&scm__rc.d1808[8])},
  },
#endif /*!(HAVE_STRSIGNAL)*/
#if !(HAVE_STRSIGNAL)
  {   /* ScmObj d1807 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
  },
#endif /*!(HAVE_STRSIGNAL)*/
#if HAVE_STRSIGNAL
  {   /* ScmPair d1805 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(552U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1803[3]), SCM_OBJ(&scm__rc.d1805[2])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1805[3])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1805[5])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1805[6])},
       { SCM_OBJ(&scm__rc.d1805[7]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1805[4]), SCM_OBJ(&scm__rc.d1805[8])},
  },
#endif /*HAVE_STRSIGNAL*/
#if HAVE_STRSIGNAL
  {   /* ScmObj d1804 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
  },
#endif /*HAVE_STRSIGNAL*/
  {   /* ScmUVector d1796 */
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 1837, uvector__00001, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 375, uvector__00002, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 79, uvector__00003, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 51, uvector__00004, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 94, uvector__00005, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 174, uvector__00006, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 72, uvector__00007, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 194, uvector__00008, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 83, uvector__00009, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 175, uvector__00010, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 52, uvector__00011, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 44, uvector__00012, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 46, uvector__00013, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 93, uvector__00014, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 19, uvector__00015, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 40, uvector__00016, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 2, uvector__00017, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 51, uvector__00018, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 52, uvector__00019, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 34, uvector__00020, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 67, uvector__00021, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 935, uvector__00022, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 113, uvector__00023, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 52, uvector__00024, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 241, uvector__00025, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 50, uvector__00026, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 135, uvector__00027, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 247, uvector__00028, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 205, uvector__00029, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 96, uvector__00030, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 210, uvector__00031, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 111, uvector__00032, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 102, uvector__00033, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 57, uvector__00034, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 62, uvector__00035, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 40, uvector__00036, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 40, uvector__00037, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 40, uvector__00038, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 691, uvector__00039, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 223, uvector__00040, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 375, uvector__00041, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 301, uvector__00042, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 572, uvector__00043, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 649, uvector__00044, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 454, uvector__00045, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 442, uvector__00046, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 285, uvector__00047, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 130, uvector__00048, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 197, uvector__00049, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 186, uvector__00050, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 548, uvector__00051, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 757, uvector__00052, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 321, uvector__00053, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 35, uvector__00054, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 114, uvector__00055, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 67, uvector__00056, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 93, uvector__00057, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 359, uvector__00058, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 148, uvector__00059, 0, NULL),
  },
  {   /* ScmCompiledCode d1795 */
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* sys-normalize-pathname */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[0])), 492,
            61, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[46]),
            SCM_OBJ(&scm__rc.d1795[1]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[492])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* sys-sigset */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[507])), 20,
            13, 0, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[273]),
            SCM_OBJ(&scm__rc.d1795[3]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[527])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* (sys-environ->alist G1802) */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[542])), 22,
            13, 1, 0, SCM_OBJ(&scm__rc.d1787[390]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[396]),
            SCM_OBJ(&scm__rc.d1795[6]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* sys-environ->alist */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[564])), 37,
            17, 0, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[434]),
            SCM_OBJ(&scm__rc.d1795[6]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[601])), 17,
            14, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* sys-putenv */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[618])), 55,
            17, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[440]),
            SCM_OBJ(&scm__rc.d1795[8]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[673])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* seconds+ */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[688])), 47,
            13, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[641]),
            SCM_OBJ(&scm__rc.d1795[10]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[735])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[750])), 10,
            12, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[665]),
            SCM_OBJ(&scm__rc.d1795[13]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[760])), 10,
            12, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[670]),
            SCM_OBJ(&scm__rc.d1795[13]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[770])), 24,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* sys-setpgrp */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[794])), 5,
            5, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[834]),
            SCM_OBJ(&scm__rc.d1795[15]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[799])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[814])), 1,
            0, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* glob */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[815])), 9,
            5, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[984]),
            SCM_OBJ(&scm__rc.d1795[18]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[824])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[839])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[854])), 9,
            9, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[992]),
            SCM_OBJ(&scm__rc.d1795[21]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* glob-fold */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[863])), 190,
            56, 3, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1012]),
            SCM_OBJ(&scm__rc.d1795[22]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1053])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1068])), 7,
            6, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1020]),
            SCM_OBJ(&scm__rc.d1795[24]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* rec */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1075])), 39,
            8, 3, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1026]),
            SCM_OBJ(&scm__rc.d1795[27]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1114])), 6,
            6, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1035]),
            SCM_OBJ(&scm__rc.d1795[26]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* rec* */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1120])), 24,
            15, 3, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1039]),
            SCM_OBJ(&scm__rc.d1795[27]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* glob-fold-1 */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1144])), 18,
            18, 6, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1052]),
            SCM_OBJ(&scm__rc.d1795[28]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1162])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* (glob-prepare-pattern f) */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1177])), 26,
            12, 1, 0, SCM_OBJ(&scm__rc.d1787[1054]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[1057]),
            SCM_OBJ(&scm__rc.d1795[31]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* glob-prepare-pattern */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1203])), 40,
            21, 3, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1065]),
            SCM_OBJ(&scm__rc.d1795[31]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1243])), 17,
            14, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* (glob-expand-braces expand) */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1260])), 16,
            12, 2, 0, SCM_OBJ(&scm__rc.d1787[1067]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[1070]),
            SCM_OBJ(&scm__rc.d1795[41]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1276])), 10,
            12, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1081]),
            SCM_OBJ(&scm__rc.d1795[34]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1286])), 8,
            6, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1090]),
            SCM_OBJ(&scm__rc.d1795[38]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1294])), 5,
            5, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1093]),
            SCM_OBJ(&scm__rc.d1795[38]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1299])), 5,
            5, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1096]),
            SCM_OBJ(&scm__rc.d1795[38]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1304])), 5,
            5, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1099]),
            SCM_OBJ(&scm__rc.d1795[38]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* loop */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1309])), 149,
            47, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1107]),
            SCM_OBJ(&scm__rc.d1795[39]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* parse */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1458])), 7,
            9, 3, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1115]),
            SCM_OBJ(&scm__rc.d1795[40]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* glob-expand-braces */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1465])), 32,
            18, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1122]),
            SCM_OBJ(&scm__rc.d1795[41]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1497])), 17,
            14, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* element1 */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1514])), 107,
            21, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1135]),
            SCM_OBJ(&scm__rc.d1795[43]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* (glob-component->regexp G1917) */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1621])), 111,
            45, 0, 0, SCM_OBJ(&scm__rc.d1787[1129]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[1142]),
            SCM_OBJ(&scm__rc.d1795[45]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* glob-component->regexp */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1732])), 13,
            19, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1149]),
            SCM_OBJ(&scm__rc.d1795[45]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1745])), 17,
            14, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* fixed-regexp? */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1762])), 53,
            21, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1155]),
            SCM_OBJ(&scm__rc.d1795[47]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1815])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* ensure-dirname */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1830])), 39,
            17, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1160]),
            SCM_OBJ(&scm__rc.d1795[51]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1869])), 41,
            16, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1169]),
            SCM_OBJ(&scm__rc.d1795[50]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* loop1920 */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[1910])), 122,
            23, 5, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1181]),
            SCM_OBJ(&scm__rc.d1795[51]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* make-glob-fs-fold */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2032])), 122,
            42, 0, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1191]),
            SCM_OBJ(&scm__rc.d1795[52]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2154])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2169])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %sys-mintty? */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2184])), 26,
            15, 1, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1196]),
            SCM_OBJ(&scm__rc.d1795[55]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2210])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* (%sys-escape-windows-command-line G1928) */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2225])), 25,
            28, 1, 0, SCM_OBJ(&scm__rc.d1787[1198]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[1204]),
            SCM_OBJ(&scm__rc.d1795[58]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %sys-escape-windows-command-line */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2250])), 86,
            15, 2, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[1212]),
            SCM_OBJ(&scm__rc.d1795[58]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1794[2336])), 17,
            14, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
  },
  {   /* ScmWord d1794 */
    /* sys-normalize-pathname */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00000005    /*   1 (CONSTU) */,
    0x0000000d    /*   2 (PUSH) */,
    0x00000005    /*   3 (CONSTU) */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000005    /*   5 (CONSTU) */,
    0x00004018    /*   6 (PUSH-LOCAL-ENV 4) */,
    0x00000040    /*   7 (LREF3) */,
    0x00000022    /*   8 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 421),
    0x0000100e    /*  10 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 15),
    0x0000004a    /*  12 (LREF2-PUSH) */,
    0x0000105f    /*  13 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.2d8a4360> */,
    0x0000001e    /*  15 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 20),
    0x00000004    /*  17 (CONSTF) */,
    0x00000013    /*  18 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 21),
    0x0000003f    /*  20 (LREF2) */,
    0x00001018    /*  21 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  22 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 27),
    0x0000004d    /*  24 (LREF11-PUSH) */,
    0x0000105f    /*  25 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.2d8a4360> */,
    0x0000001e    /*  27 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 32),
    0x00000004    /*  29 (CONSTF) */,
    0x00000013    /*  30 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 33),
    0x00000042    /*  32 (LREF11) */,
    0x00001018    /*  33 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  34 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 39),
    0x0000004f    /*  36 (LREF20-PUSH) */,
    0x0000105f    /*  37 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.2d8a4360> */,
    0x0000001e    /*  39 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 44),
    0x00000004    /*  41 (CONSTF) */,
    0x00000013    /*  42 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 45),
    0x00000044    /*  44 (LREF20) */,
    0x00001018    /*  45 (PUSH-LOCAL-ENV 1) */,
    0x00002019    /*  46 (LOCAL-ENV-CLOSURES 2) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[31])) /* (#<undef> #<undef>) */,
    0x00000006    /*  48 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* gauche.os.windows */,
    0x0000000e    /*  50 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 54),
    0x0000005f    /*  52 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#cond-features.2d8a8a80> */,
    0x0000008d    /*  54 (ASSQ) */,
    0x000010e8    /*  55 (ENV-SET 1) */,
    0x0000003e    /*  56 (LREF1) */,
    0x0000001e    /*  57 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 63),
    0x00000001    /*  59 (CONST) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[18])) /* "\\" */,
    0x00000013    /*  61 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 65),
    0x00000001    /*  63 (CONST) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[19])) /* "/" */,
    0x000000e8    /*  65 (ENV-SET 0) */,
    0x00000044    /*  66 (LREF20) */,
    0x0000001e    /*  67 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 76),
    0x0000200e    /*  69 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 76),
    0x00000006    /*  71 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #/^~([^\/\\]*)/ */,
    0x00405047    /*  73 (LREF-PUSH 5 1) */,
    0x0000205f    /*  74 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#rxmatch.2d8ab0e0> */,
    0x00001018    /*  76 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  77 (LREF0) */,
    0x0000001e    /*  78 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 211),
    0x0000100e    /*  80 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 85),
    0x00001007    /*  82 (CONSTI-PUSH 1) */,
    0x0000003d    /*  83 (LREF0) */,
    0x00001011    /*  84 (CALL 1) */,
    0x00001018    /*  85 (PUSH-LOCAL-ENV 1) */,
    0x0000200e    /*  86 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 93),
    0x00000048    /*  88 (LREF0-PUSH) */,
    0x00000006    /*  89 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[22])) /* "" */,
    0x0000205f    /*  91 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#equal?.2d8adf00> */,
    0x0000001e    /*  93 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 153),
    0x00000045    /*  95 (LREF21) */,
    0x0000001e    /*  96 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 122),
    0x0000100e    /*  98 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 104),
    0x00000006    /* 100 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[24])) /* "HOME" */,
    0x0000105f    /* 102 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-getenv.2d8ad900> */,
    0x0000001e    /* 104 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 108),
    0x00000013    /* 106 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 195),
    0x0000100e    /* 108 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 114),
    0x00000006    /* 110 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[26])) /* "USERPROFILE" */,
    0x0000105f    /* 112 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-getenv.2d8ad8a0> */,
    0x0000001e    /* 114 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 118),
    0x00000013    /* 116 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 195),
    0x00000001    /* 118 (CONST) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[22])) /* "" */,
    0x00000013    /* 120 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 195),
    0x0000100e    /* 122 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 130),
    0x0000000e    /* 124 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 128),
    0x0000005f    /* 126 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-getuid.2d8adda0> */,
    0x00001062    /* 128 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-getpwuid.2d8adde0> */,
    0x00001018    /* 130 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /* 131 (LREF0) */,
    0x0000001e    /* 132 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 143),
    0x0000200e    /* 134 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 150),
    0x00000048    /* 136 (LREF0-PUSH) */,
    0x00000006    /* 137 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* dir */,
    0x0000205f    /* 139 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#~.2d8ad740> */,
    0x00000013    /* 141 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 150),
    0x0000200e    /* 143 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 150),
    0x00000006    /* 145 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[31])) /* "Couldn't obtain username for :" */,
    0x00408047    /* 147 (LREF-PUSH 8 1) */,
    0x0000205f    /* 148 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.2d8ad660> */,
    0x0000001a    /* 150 (POP-LOCAL-ENV) */,
    0x00000013    /* 151 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 195),
    0x00000045    /* 153 (LREF21) */,
    0x0000001e    /* 154 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 165),
    0x0000200e    /* 156 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 195),
    0x00000006    /* 158 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[33])) /* "'~user' expansil isn't supported on Windows:" */,
    0x00407047    /* 160 (LREF-PUSH 7 1) */,
    0x0000205f    /* 161 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.2d8add00> */,
    0x00000013    /* 163 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 195),
    0x0000100e    /* 165 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 174),
    0x0000100e    /* 167 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 172),
    0x00001007    /* 169 (CONSTI-PUSH 1) */,
    0x00000041    /* 170 (LREF10) */,
    0x00001011    /* 171 (CALL 1) */,
    0x00001062    /* 172 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-getpwnam.2d8adc60> */,
    0x00001018    /* 174 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /* 175 (LREF0) */,
    0x0000001e    /* 176 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 187),
    0x0000200e    /* 178 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 194),
    0x00000048    /* 180 (LREF0-PUSH) */,
    0x00000006    /* 181 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* dir */,
    0x0000205f    /* 183 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#~.2d8ad740> */,
    0x00000013    /* 185 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 194),
    0x0000200e    /* 187 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 194),
    0x00000006    /* 189 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[31])) /* "Couldn't obtain username for :" */,
    0x00408047    /* 191 (LREF-PUSH 8 1) */,
    0x0000205f    /* 192 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.2d8ad660> */,
    0x0000001a    /* 194 (POP-LOCAL-ENV) */,
    0x00001018    /* 195 (PUSH-LOCAL-ENV 1) */,
    0x0000200e    /* 196 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 207),
    0x00000048    /* 198 (LREF0-PUSH) */,
    0x0000100e    /* 199 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 205),
    0x00000006    /* 201 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* after */,
    0x00000044    /* 203 (LREF20) */,
    0x00001011    /* 204 (CALL 1) */,
    0x00002062    /* 205 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-append.2d8adba0> */,
    0x0000001a    /* 207 (POP-LOCAL-ENV) */,
    0x0000001a    /* 208 (POP-LOCAL-ENV) */,
    0x00000013    /* 209 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 212),
    0x0040603c    /* 211 (LREF 6 1) */,
    0x0000001a    /* 212 (POP-LOCAL-ENV) */,
    0x00001018    /* 213 (PUSH-LOCAL-ENV 1) */,
    0x0000403c    /* 214 (LREF 4 0) */,
    0x0000001e    /* 215 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 254),
    0x00000042    /* 217 (LREF11) */,
    0x0000001e    /* 218 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 229),
    0x0000200e    /* 220 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 236),
    0x00000006    /* 222 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #/^([A-Za-z]:)?[\/\\]/ */,
    0x00000048    /* 224 (LREF0-PUSH) */,
    0x0000205f    /* 225 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#rxmatch.2d8ad420> */,
    0x00000013    /* 227 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 236),
    0x0000200e    /* 229 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 236),
    0x00000006    /* 231 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #/^\// */,
    0x00000048    /* 233 (LREF0-PUSH) */,
    0x0000205f    /* 234 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#rxmatch.2d8ad3a0> */,
    0x0000001e    /* 236 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 241),
    0x0000003d    /* 238 (LREF0) */,
    0x00000013    /* 239 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 255),
    0x0000300e    /* 241 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 255),
    0x0000000e    /* 243 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 247),
    0x0000005f    /* 245 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-getcwd.2d8ad040> */,
    0x0000000d    /* 247 (PUSH) */,
    0x0000004c    /* 248 (LREF10-PUSH) */,
    0x00000048    /* 249 (LREF0-PUSH) */,
    0x0000305f    /* 250 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-append.2d8ad080> */,
    0x00000013    /* 252 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 255),
    0x0000003d    /* 254 (LREF0) */,
    0x0000001a    /* 255 (POP-LOCAL-ENV) */,
    0x00001018    /* 256 (PUSH-LOCAL-ENV 1) */,
    0x00000044    /* 257 (LREF20) */,
    0x0000001e    /* 258 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 404),
    0x0000200e    /* 260 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 267),
    0x00000048    /* 262 (LREF0-PUSH) */,
    0x00000006    /* 263 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #[/\\] */,
    0x0000205f    /* 265 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-split.2d8c01c0> */,
    0x0000000d    /* 267 (PUSH) */,
    0x00000008    /* 268 (CONSTN-PUSH) */,
    0x00000009    /* 269 (CONSTF-PUSH) */,
    0x00003017    /* 270 (LOCAL-ENV 3) */,
    0x0000003f    /* 271 (LREF2) */,
    0x00000022    /* 272 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 292),
    0x0000003d    /* 274 (LREF0) */,
    0x0000001e    /* 275 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 283),
    0x00000006    /* 277 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[22])) /* "" */,
    0x0000003e    /* 279 (LREF1) */,
    0x00000066    /* 280 (CONS) */,
    0x00000013    /* 281 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 284),
    0x0000003e    /* 283 (LREF1) */,
    0x00001018    /* 284 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /* 285 (LREF0) */,
    0x00000093    /* 286 (REVERSE) */,
    0x0000000d    /* 287 (PUSH) */,
    0x00000051    /* 288 (LREF30-PUSH) */,
    0x00002060    /* 289 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-join.2d8be3c0> */,
    0x00000014    /* 291 (RET) */,
    0x0000200e    /* 292 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 300),
    0x0000006c    /* 294 (LREF2-CAR) */,
    0x0000000d    /* 295 (PUSH) */,
    0x00000006    /* 296 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[42])) /* "." */,
    0x0000205f    /* 298 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#equal?.2d8be220> */,
    0x0000001e    /* 300 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 310),
    0x00000078    /* 302 (LREF2-CDR) */,
    0x0000000d    /* 303 (PUSH) */,
    0x00000049    /* 304 (LREF1-PUSH) */,
    0x00000006    /* 305 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x0000101b    /* 307 (LOCAL-ENV-JUMP 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 271),
    0x00000014    /* 309 (RET) */,
    0x0000200e    /* 310 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 318),
    0x0000006c    /* 312 (LREF2-CAR) */,
    0x0000000d    /* 313 (PUSH) */,
    0x00000006    /* 314 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[43])) /* ".." */,
    0x0000205f    /* 316 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#equal?.2d8be040> */,
    0x0000001e    /* 318 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 393),
    0x0000003e    /* 320 (LREF1) */,
    0x00000022    /* 321 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 333),
    0x00000078    /* 323 (LREF2-CDR) */,
    0x0000000d    /* 324 (PUSH) */,
    0x0000006c    /* 325 (LREF2-CAR) */,
    0x0000000d    /* 326 (PUSH) */,
    0x0000003e    /* 327 (LREF1) */,
    0x00000067    /* 328 (CONS-PUSH) */,
    0x00000009    /* 329 (CONSTF-PUSH) */,
    0x0000101b    /* 330 (LOCAL-ENV-JUMP 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 271),
    0x00000014    /* 332 (RET) */,
    0x0000200e    /* 333 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 341),
    0x0000006b    /* 335 (LREF1-CAR) */,
    0x0000000d    /* 336 (PUSH) */,
    0x00000006    /* 337 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[43])) /* ".." */,
    0x0000205f    /* 339 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#equal?.2d8c0de0> */,
    0x0000001e    /* 341 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 346),
    0x00000013    /* 343 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 323),
    0x00000014    /* 345 (RET) */,
    0x0000200e    /* 346 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 353),
    0x00000049    /* 348 (LREF1-PUSH) */,
    0x00000006    /* 349 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[32])) /* ("") */,
    0x0000205f    /* 351 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#equal?.2d8b9ec0> */,
    0x0000001e    /* 353 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 358),
    0x00000013    /* 355 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 323),
    0x00000014    /* 357 (RET) */,
    0x00000045    /* 358 (LREF21) */,
    0x0000001e    /* 359 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 378),
    0x0000200e    /* 361 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 367),
    0x00000049    /* 363 (LREF1-PUSH) */,
    0x00001007    /* 364 (CONSTI-PUSH 1) */,
    0x0000205f    /* 365 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#length=?.2d8b9500> */,
    0x0000001e    /* 367 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 378),
    0x0000200e    /* 369 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 376),
    0x00000006    /* 371 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #/^[A-Za-z]:$/ */,
    0x0000006b    /* 373 (LREF1-CAR) */,
    0x00002062    /* 374 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#rxmatch.2d8b93a0> */,
    0x0000001f    /* 376 (BT) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 323),
    0x00000078    /* 378 (LREF2-CDR) */,
    0x0000000d    /* 379 (PUSH) */,
    0x00000077    /* 380 (LREF1-CDR) */,
    0x0000000d    /* 381 (PUSH) */,
    0x00000006    /* 382 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x0000101b    /* 384 (LOCAL-ENV-JUMP 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 271),
    0x00000014    /* 386 (RET) */,
    0x00000013    /* 387 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 378),
    0x00000014    /* 389 (RET) */,
    0x00000013    /* 390 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 378),
    0x00000014    /* 392 (RET) */,
    0x00000078    /* 393 (LREF2-CDR) */,
    0x0000000d    /* 394 (PUSH) */,
    0x0000006c    /* 395 (LREF2-CAR) */,
    0x0000000d    /* 396 (PUSH) */,
    0x0000003e    /* 397 (LREF1) */,
    0x00000067    /* 398 (CONS-PUSH) */,
    0x00000009    /* 399 (CONSTF-PUSH) */,
    0x0000101b    /* 400 (LOCAL-ENV-JUMP 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 271),
    0x00000014    /* 402 (RET) */,
    0x00000014    /* 403 (RET) */,
    0x00000042    /* 404 (LREF11) */,
    0x0000001e    /* 405 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 420),
    0x0000200e    /* 407 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 414),
    0x00000048    /* 409 (LREF0-PUSH) */,
    0x00000006    /* 410 (CONST-PUSH) */,
    SCM_WORD(SCM_MAKE_CHAR(47)) /* #\/ */,
    0x0000205f    /* 412 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-split.2d8c0020> */,
    0x0000000d    /* 414 (PUSH) */,
    0x00000006    /* 415 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[18])) /* "\\" */,
    0x00002060    /* 417 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-join.2d8c0060> */,
    0x00000014    /* 419 (RET) */,
    0x00000053    /* 420 (LREF0-RET) */,
    0x00000079    /* 421 (LREF3-CDR) */,
    0x00000022    /* 422 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 430),
    0x00000006    /* 424 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[46])) /* "keyword list not even" */,
    0x0000004b    /* 426 (LREF3-PUSH) */,
    0x00002060    /* 427 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.2d8a41e0> */,
    0x00000014    /* 429 (RET) */,
    0x0000100e    /* 430 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 435),
    0x0000006d    /* 432 (LREF3-CAR) */,
    0x00001062    /* 433 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#unwrap-syntax-1.2d8a4100> */,
    0x00001018    /* 435 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /* 436 (LREF0) */,
    0x0000002e    /* 437 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :absolute */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 449)  /*    449 */,
    0x00c0103c    /* 440 (LREF 1 3) */,
    0x00000087    /* 441 (CDDR-PUSH) */,
    0x00c0103c    /* 442 (LREF 1 3) */,
    0x00000083    /* 443 (CADR-PUSH) */,
    0x0000004d    /* 444 (LREF11-PUSH) */,
    0x0000004c    /* 445 (LREF10-PUSH) */,
    0x0000201b    /* 446 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 7),
    0x00000014    /* 448 (RET) */,
    0x0000003d    /* 449 (LREF0) */,
    0x0000002e    /* 450 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :expand */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 462)  /*    462 */,
    0x00c0103c    /* 453 (LREF 1 3) */,
    0x00000087    /* 454 (CDDR-PUSH) */,
    0x0000004e    /* 455 (LREF12-PUSH) */,
    0x00c0103c    /* 456 (LREF 1 3) */,
    0x00000083    /* 457 (CADR-PUSH) */,
    0x0000004c    /* 458 (LREF10-PUSH) */,
    0x0000201b    /* 459 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 7),
    0x00000014    /* 461 (RET) */,
    0x0000003d    /* 462 (LREF0) */,
    0x0000002e    /* 463 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :canonicalize */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 475)  /*    475 */,
    0x00c0103c    /* 466 (LREF 1 3) */,
    0x00000087    /* 467 (CDDR-PUSH) */,
    0x0000004e    /* 468 (LREF12-PUSH) */,
    0x0000004d    /* 469 (LREF11-PUSH) */,
    0x00c0103c    /* 470 (LREF 1 3) */,
    0x00000083    /* 471 (CADR-PUSH) */,
    0x0000201b    /* 472 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 7),
    0x00000014    /* 474 (RET) */,
    0x0000200e    /* 475 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 483),
    0x00000006    /* 477 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[51])) /* "unknown keyword ~S" */,
    0x00c0103c    /* 479 (LREF 1 3) */,
    0x00000069    /* 480 (CAR-PUSH) */,
    0x0000205f    /* 481 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#errorf.2d8a6e40> */,
    0x00c0103c    /* 483 (LREF 1 3) */,
    0x00000087    /* 484 (CDDR-PUSH) */,
    0x0000004e    /* 485 (LREF12-PUSH) */,
    0x0000004d    /* 486 (LREF11-PUSH) */,
    0x0000004c    /* 487 (LREF10-PUSH) */,
    0x0000201b    /* 488 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]) + 7),
    0x00000014    /* 490 (RET) */,
    0x00000014    /* 491 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[492]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* sys-normalize-pathname */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[492]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[0])) /* #<compiled-code sys-normalize-pathname@0x7fdc2ddaf420> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#sys-normalize-pathname.2d8974e0> */,
    0x00000014    /*  14 (RET) */,
    /* sys-sigset */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[507]) + 8),
    0x0000005e    /*   3 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<sys-sigset>.2d8ecb20> */,
    0x00001060    /*   5 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make.2d8ecb60> */,
    0x00000014    /*   7 (RET) */,
    0x0000005e    /*   8 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-sigset-add!.2d8ecac0> */,
    0x0000100e    /*  10 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[507]) + 16),
    0x0000005e    /*  12 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<sys-sigset>.2d8eca40> */,
    0x0000105f    /*  14 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make.2d8eca80> */,
    0x0000000d    /*  16 (PUSH) */,
    0x0000003d    /*  17 (LREF0) */,
    0x00003095    /*  18 (TAIL-APPLY 3) */,
    0x00000014    /*  19 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[527]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* sys-sigset */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[527]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[2])) /* #<compiled-code sys-sigset@0x7fdc2db1c780> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-sigset.2d8eccc0> */,
    0x00000014    /*  14 (RET) */,
    /* (sys-environ->alist G1802) */
    0x0000300e    /*   0 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[542]) + 9),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x00000006    /*   3 (CONST-PUSH) */,
    SCM_WORD(SCM_MAKE_CHAR(61)) /* #\= */,
    0x00000006    /*   5 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* both */,
    0x0000305f    /*   7 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-scan.2d6568e0> */,
    0x00002036    /*   9 (TAIL-RECEIVE 2 0) */,
    0x0000003e    /*  10 (LREF1) */,
    0x0000001e    /*  11 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[542]) + 17),
    0x00000049    /*  13 (LREF1-PUSH) */,
    0x0000003d    /*  14 (LREF0) */,
    0x00000066    /*  15 (CONS) */,
    0x00000014    /*  16 (RET) */,
    0x0000004c    /*  17 (LREF10-PUSH) */,
    0x00000001    /*  18 (CONST) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[22])) /* "" */,
    0x00000066    /*  20 (CONS) */,
    0x00000014    /*  21 (RET) */,
    /* sys-environ->alist */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[564]) + 9),
    0x0000000e    /*   3 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[564]) + 10),
    0x0000005f    /*   5 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-environ.2d650d80> */,
    0x00000013    /*   7 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[564]) + 10),
    0x0000006a    /*   9 (LREF0-CAR) */,
    0x0000000d    /*  10 (PUSH) */,
    0x0000003d    /*  11 (LREF0) */,
    0x00000022    /*  12 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[564]) + 17),
    0x00000003    /*  14 (CONSTN) */,
    0x00000013    /*  15 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[564]) + 18),
    0x00000076    /*  17 (LREF0-CDR) */,
    0x00002018    /*  18 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  19 (LREF0) */,
    0x00000022    /*  20 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[564]) + 24),
    0x00000013    /*  22 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[564]) + 32),
    0x0000200e    /*  24 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[564]) + 32),
    0x00000006    /*  26 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[296])) /* "too many arguments for" */,
    0x00000006    /*  28 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[429])) /* (lambda (:optional (envlist (sys-environ))) (map (^ (envstr) (receive (pre post) (string-scan envstr #\= 'both) (if pre (cons pre post) (cons envstr "")))) envlist)) */,
    0x0000205f    /*  30 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.2f222860> */,
    0x0000004f    /*  32 (LREF20-PUSH) */,
    0x00000049    /*  33 (LREF1-PUSH) */,
    0x00002060    /*  34 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#map.2d656e00> */,
    0x00000014    /*  36 (RET) */,
    /* %toplevel */
    0x00001019    /*   0 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[397])) /* (#<compiled-code (sys-environ->alist #:G1802)@0x7fdc2ce37960>) */,
    0x0000000e    /*   2 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[601]) + 6),
    0x0000005f    /*   4 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000001    /*   7 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* sys-environ->alist */,
    0x000000ee    /*   9 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[601]) + 14),
    0x00000016    /*  11 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[5])) /* #<compiled-code sys-environ->alist@0x7fdc2ce37900> */,
    0x00000014    /*  13 (RET) */,
    0x00000015    /*  14 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-environ->alist.2d64cbe0> */,
    0x00000014    /*  16 (RET) */,
    /* sys-putenv */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[618]) + 47),
    0x0000003e    /*   3 (LREF1) */,
    0x0000009b    /*   4 (STRINGP) */,
    0x0000001e    /*   5 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[618]) + 9),
    0x00000013    /*   7 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[618]) + 18),
    0x0000300e    /*   9 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[618]) + 18),
    0x00000006    /*  11 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[308])) /* "bad type of argument for ~s: ~s" */,
    0x00000006    /*  13 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* arg */,
    0x00000049    /*  15 (LREF1-PUSH) */,
    0x0000305f    /*  16 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#errorf.2d7d3c80> */,
    0x0000300e    /*  18 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[618]) + 27),
    0x00000049    /*  20 (LREF1-PUSH) */,
    0x00000006    /*  21 (CONST-PUSH) */,
    SCM_WORD(SCM_MAKE_CHAR(61)) /* #\= */,
    0x00000006    /*  23 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* both */,
    0x0000305f    /*  25 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-scan.2d7d39a0> */,
    0x00002036    /*  27 (TAIL-RECEIVE 2 0) */,
    0x0000003e    /*  28 (LREF1) */,
    0x0000001e    /*  29 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[618]) + 33),
    0x00000013    /*  31 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[618]) + 40),
    0x0000200e    /*  33 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[618]) + 40),
    0x00000006    /*  35 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[310])) /* "sys-putenv: argument doesn't contain '=':" */,
    0x0000004d    /*  37 (LREF11-PUSH) */,
    0x0000205f    /*  38 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.2d7d3820> */,
    0x00000049    /*  40 (LREF1-PUSH) */,
    0x00000048    /*  41 (LREF0-PUSH) */,
    0x00000006    /*  42 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x00003060    /*  44 (GREF-TAIL-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-setenv.2d7d37a0> */,
    0x00000014    /*  46 (RET) */,
    0x00000049    /*  47 (LREF1-PUSH) */,
    0x0000006a    /*  48 (LREF0-CAR) */,
    0x0000000d    /*  49 (PUSH) */,
    0x00000006    /*  50 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x00003060    /*  52 (GREF-TAIL-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-setenv.2d7d3680> */,
    0x00000014    /*  54 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[673]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* sys-putenv */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[673]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[7])) /* #<compiled-code sys-putenv@0x7fdc2cea00c0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-putenv.2d7cf140> */,
    0x00000014    /*  14 (RET) */,
    /* seconds+ */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]) + 7),
    0x00000049    /*   2 (LREF1-PUSH) */,
    0x0000005e    /*   3 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<time>.2d4a6ee0> */,
    0x0000205f    /*   5 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#of-type?.2d4a15e0> */,
    0x0000001e    /*   7 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]) + 12),
    0x0000003e    /*   9 (LREF1) */,
    0x00000013    /*  10 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]) + 21),
    0x0000300e    /*  12 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]) + 21),
    0x00000006    /*  14 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* t */,
    0x0000005e    /*  16 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<time>.2d4a6e00> */,
    0x00000049    /*  18 (LREF1-PUSH) */,
    0x0000305f    /*  19 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#type-error.2d4a15a0> */,
    0x0000200e    /*  21 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]) + 28),
    0x00000048    /*  23 (LREF0-PUSH) */,
    0x0000005e    /*  24 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<real>.2d4a64a0> */,
    0x0000205f    /*  26 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#of-type?.2d4a6c20> */,
    0x0000001e    /*  28 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]) + 33),
    0x0000003d    /*  30 (LREF0) */,
    0x00000013    /*  31 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]) + 42),
    0x0000300e    /*  33 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]) + 42),
    0x00000006    /*  35 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* dt */,
    0x0000005e    /*  37 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#<real>.2d4a6360> */,
    0x00000048    /*  39 (LREF0-PUSH) */,
    0x0000305f    /*  40 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#type-error.2d4a6be0> */,
    0x00000048    /*  42 (LREF0-PUSH) */,
    0x00000049    /*  43 (LREF1-PUSH) */,
    0x00002060    /*  44 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#absolute-time.2d4a6300> */,
    0x00000014    /*  46 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[735]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* seconds+ */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[735]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[9])) /* #<compiled-code seconds+@0x7fdc2d76c540> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#seconds+.2d4a1da0> */,
    0x00000014    /*  14 (RET) */,
    /* #f */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[750]) + 6),
    0x00000049    /*   2 (LREF1-PUSH) */,
    0x00000048    /*   3 (LREF0-PUSH) */,
    0x0000205f    /*   4 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#compare.2d675500> */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000002    /*   7 (CONSTI 0) */,
    0x000000ad    /*   8 (NUMEQ2) */,
    0x00000014    /*   9 (RET) */,
    /* #f */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[760]) + 6),
    0x00000049    /*   2 (LREF1-PUSH) */,
    0x00000048    /*   3 (LREF0-PUSH) */,
    0x0000205f    /*   4 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#compare.2d6750c0> */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000002    /*   7 (CONSTI 0) */,
    0x000000ae    /*   8 (NUMLT2) */,
    0x00000014    /*   9 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[770]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* time-comparator */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[770]) + 21),
    0x0000005e    /*   9 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#time?.2d6757e0> */,
    0x00000016    /*  11 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[11])) /* #<compiled-code #f@0x7fdc2cc7fcc0> */,
    0x0000000d    /*  13 (PUSH) */,
    0x00000016    /*  14 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[12])) /* #<compiled-code #f@0x7fdc2cc7fc60> */,
    0x00000061    /*  16 (PUSH-GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#default-hash.2d675020> */,
    0x00004063    /*  18 (PUSH-GREF-TAIL-CALL 4) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-comparator.2d675840> */,
    0x00000014    /*  20 (RET) */,
    0x00000015    /*  21 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#time-comparator.2d6758c0> */,
    0x00000014    /*  23 (RET) */,
    /* sys-setpgrp */
    0x00000007    /*   0 (CONSTI-PUSH 0) */,
    0x00000007    /*   1 (CONSTI-PUSH 0) */,
    0x00002060    /*   2 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-setpgid.2dda9a60> */,
    0x00000014    /*   4 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[799]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* sys-setpgrp */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[799]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[14])) /* #<compiled-code sys-setpgrp@0x7fdc2dce5540> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-setpgrp.2dda9b80> */,
    0x00000014    /*  14 (RET) */,
    /* %toplevel */
    0x00000014    /*   0 (RET) */,
    /* glob */
    0x0000005e    /*   0 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#glob-fold.2ceac340> */,
    0x00000049    /*   2 (LREF1-PUSH) */,
    0x0000005e    /*   3 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#cons.2ceac280> */,
    0x00000008    /*   5 (CONSTN-PUSH) */,
    0x0000003d    /*   6 (LREF0) */,
    0x00005095    /*   7 (TAIL-APPLY 5) */,
    0x00000014    /*   8 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[824]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* glob */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[824]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[17])) /* #<compiled-code glob@0x7fdc2cbdb240> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#glob.2ceac460> */,
    0x00000014    /*  14 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[839]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* sys-glob */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[839]) + 12),
    0x0000005d    /*   9 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#glob.2cf6a420> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-glob.2cf6a4a0> */,
    0x00000014    /*  14 (RET) */,
    /* (glob-fold #f) */
    0x00000049    /*   0 (LREF1-PUSH) */,
    0x00806047    /*   1 (LREF-PUSH 6 2) */,
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x00004047    /*   3 (LREF-PUSH 4 0) */,
    0x00000051    /*   4 (LREF30-PUSH) */,
    0x0000004c    /*   5 (LREF10-PUSH) */,
    0x00006060    /*   6 (GREF-TAIL-CALL 6) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#glob-fold-1.2cfd28e0> */,
    0x00000014    /*   8 (RET) */,
    /* glob-fold */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00000005    /*   1 (CONSTU) */,
    0x0000000d    /*   2 (PUSH) */,
    0x00000005    /*   3 (CONSTU) */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000005    /*   5 (CONSTU) */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000005    /*   7 (CONSTU) */,
    0x00005018    /*   8 (PUSH-LOCAL-ENV 5) */,
    0x0100003c    /*   9 (LREF 0 4) */,
    0x00000022    /*  10 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 99),
    0x0000100e    /*  12 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 17),
    0x0000004b    /*  14 (LREF3-PUSH) */,
    0x0000105f    /*  15 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.2cfca2c0> */,
    0x0000001e    /*  17 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 23),
    0x00000001    /*  19 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* #[/] */,
    0x00000013    /*  21 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 24),
    0x00000040    /*  23 (LREF3) */,
    0x00001018    /*  24 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  25 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 30),
    0x0000004e    /*  27 (LREF12-PUSH) */,
    0x0000105f    /*  28 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.2cfca2c0> */,
    0x0000001e    /*  30 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 36),
    0x0000005d    /*  32 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#glob-fs-folder.2cfcd980> */,
    0x00000013    /*  34 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 37),
    0x00000043    /*  36 (LREF12) */,
    0x00001018    /*  37 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  38 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 43),
    0x00000050    /*  40 (LREF21-PUSH) */,
    0x0000105f    /*  41 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.2cfca2c0> */,
    0x0000001e    /*  43 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 49),
    0x0000005d    /*  45 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sort.2cfcd880> */,
    0x00000013    /*  47 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 50),
    0x00000045    /*  49 (LREF21) */,
    0x00001018    /*  50 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  51 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 56),
    0x00000051    /*  53 (LREF30-PUSH) */,
    0x0000105f    /*  54 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.2cfca2c0> */,
    0x0000001e    /*  56 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 61),
    0x00000004    /*  58 (CONSTF) */,
    0x00000013    /*  59 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 62),
    0x00000046    /*  61 (LREF30) */,
    0x00001018    /*  62 (PUSH-LOCAL-ENV 1) */,
    0x0000300e    /*  63 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 90),
    0x00000016    /*  65 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[20])) /* #<compiled-code (glob-fold #f)@0x7fdc2cd814e0> */,
    0x0000000d    /*  67 (PUSH) */,
    0x00405047    /*  68 (LREF-PUSH 5 1) */,
    0x0000300e    /*  69 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 88),
    0x0000005e    /*  71 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#glob-expand-braces.2cfd2780> */,
    0x00000008    /*  73 (CONSTN-PUSH) */,
    0x0000100e    /*  74 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 79),
    0x00c05047    /*  76 (LREF-PUSH 5 3) */,
    0x0000105f    /*  77 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#list?.2cfd2680> */,
    0x0000001e    /*  79 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 84),
    0x00c0503c    /*  81 (LREF 5 3) */,
    0x00000013    /*  82 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 86),
    0x00c0503c    /*  84 (LREF 5 3) */,
    0x00001088    /*  85 (LIST 1) */,
    0x00003062    /*  86 (PUSH-GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#fold.2cfd27c0> */,
    0x00003062    /*  88 (PUSH-GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#fold.2cfcd0a0> */,
    0x00001018    /*  90 (PUSH-LOCAL-ENV 1) */,
    0x00000044    /*  91 (LREF20) */,
    0x0000001e    /*  92 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 98),
    0x00000048    /*  94 (LREF0-PUSH) */,
    0x00000044    /*  95 (LREF20) */,
    0x00001012    /*  96 (TAIL-CALL 1) */,
    0x00000014    /*  97 (RET) */,
    0x00000053    /*  98 (LREF0-RET) */,
    0x0100003c    /*  99 (LREF 0 4) */,
    0x00000074    /* 100 (CDR) */,
    0x00000022    /* 101 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 109),
    0x00000006    /* 103 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[46])) /* "keyword list not even" */,
    0x01000047    /* 105 (LREF-PUSH 0 4) */,
    0x00002060    /* 106 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.2cfca0a0> */,
    0x00000014    /* 108 (RET) */,
    0x0000100e    /* 109 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 115),
    0x0100003c    /* 111 (LREF 0 4) */,
    0x00000069    /* 112 (CAR-PUSH) */,
    0x0000105f    /* 113 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#unwrap-syntax-1.2cfca000> */,
    0x00001018    /* 115 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /* 116 (LREF0) */,
    0x0000002e    /* 117 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :separator */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 130)  /*    130 */,
    0x0100103c    /* 120 (LREF 1 4) */,
    0x00000087    /* 121 (CDDR-PUSH) */,
    0x0100103c    /* 122 (LREF 1 4) */,
    0x00000083    /* 123 (CADR-PUSH) */,
    0x0000004e    /* 124 (LREF12-PUSH) */,
    0x0000004d    /* 125 (LREF11-PUSH) */,
    0x0000004c    /* 126 (LREF10-PUSH) */,
    0x0000201b    /* 127 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 9),
    0x00000014    /* 129 (RET) */,
    0x0000003d    /* 130 (LREF0) */,
    0x0000002e    /* 131 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :folder */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 144)  /*    144 */,
    0x0100103c    /* 134 (LREF 1 4) */,
    0x00000087    /* 135 (CDDR-PUSH) */,
    0x00c01047    /* 136 (LREF-PUSH 1 3) */,
    0x0100103c    /* 137 (LREF 1 4) */,
    0x00000083    /* 138 (CADR-PUSH) */,
    0x0000004d    /* 139 (LREF11-PUSH) */,
    0x0000004c    /* 140 (LREF10-PUSH) */,
    0x0000201b    /* 141 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 9),
    0x00000014    /* 143 (RET) */,
    0x0000003d    /* 144 (LREF0) */,
    0x0000002e    /* 145 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :sorter */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 158)  /*    158 */,
    0x0100103c    /* 148 (LREF 1 4) */,
    0x00000087    /* 149 (CDDR-PUSH) */,
    0x00c01047    /* 150 (LREF-PUSH 1 3) */,
    0x0000004e    /* 151 (LREF12-PUSH) */,
    0x0100103c    /* 152 (LREF 1 4) */,
    0x00000083    /* 153 (CADR-PUSH) */,
    0x0000004c    /* 154 (LREF10-PUSH) */,
    0x0000201b    /* 155 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 9),
    0x00000014    /* 157 (RET) */,
    0x0000003d    /* 158 (LREF0) */,
    0x0000002e    /* 159 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :prefix */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 172)  /*    172 */,
    0x0100103c    /* 162 (LREF 1 4) */,
    0x00000087    /* 163 (CDDR-PUSH) */,
    0x00c01047    /* 164 (LREF-PUSH 1 3) */,
    0x0000004e    /* 165 (LREF12-PUSH) */,
    0x0000004d    /* 166 (LREF11-PUSH) */,
    0x0100103c    /* 167 (LREF 1 4) */,
    0x00000083    /* 168 (CADR-PUSH) */,
    0x0000201b    /* 169 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 9),
    0x00000014    /* 171 (RET) */,
    0x0000200e    /* 172 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 180),
    0x00000006    /* 174 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[51])) /* "unknown keyword ~S" */,
    0x0100103c    /* 176 (LREF 1 4) */,
    0x00000069    /* 177 (CAR-PUSH) */,
    0x0000205f    /* 178 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#errorf.2cfcdc60> */,
    0x0100103c    /* 180 (LREF 1 4) */,
    0x00000087    /* 181 (CDDR-PUSH) */,
    0x00c01047    /* 182 (LREF-PUSH 1 3) */,
    0x0000004e    /* 183 (LREF12-PUSH) */,
    0x0000004d    /* 184 (LREF11-PUSH) */,
    0x0000004c    /* 185 (LREF10-PUSH) */,
    0x0000201b    /* 186 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]) + 9),
    0x00000014    /* 188 (RET) */,
    0x00000014    /* 189 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1053]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* glob-fold */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1053]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[21])) /* #<compiled-code glob-fold@0x7fdc2cd81540> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#glob-fold.2cfcaf20> */,
    0x00000014    /*  14 (RET) */,
    /* (glob-fold-1 rec #f) */
    0x00000049    /*   0 (LREF1-PUSH) */,
    0x0000007b    /*   1 (LREF11-CDR) */,
    0x0000000d    /*   2 (PUSH) */,
    0x00000048    /*   3 (LREF0-PUSH) */,
    0x00000045    /*   4 (LREF21) */,
    0x0000301d    /*   5 (LOCAL-ENV-TAIL-CALL 3) */,
    0x00000014    /*   6 (RET) */,
    /* (glob-fold-1 rec) */
    0x0000003e    /*   0 (LREF1) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1075]) + 4),
    0x00000053    /*   3 (LREF0-RET) */,
    0x0000006b    /*   4 (LREF1-CAR) */,
    0x0000002e    /*   5 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* ** */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1075]) + 15)  /*     15 */,
    0x0000004a    /*   8 (LREF2-PUSH) */,
    0x00000077    /*   9 (LREF1-CDR) */,
    0x0000000d    /*  10 (PUSH) */,
    0x00000048    /*  11 (LREF0-PUSH) */,
    0x00000041    /*  12 (LREF10) */,
    0x0000301d    /*  13 (LOCAL-ENV-TAIL-CALL 3) */,
    0x00000014    /*  14 (RET) */,
    0x00000077    /*  15 (LREF1-CDR) */,
    0x00000022    /*  16 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1075]) + 27),
    0x01002047    /*  18 (LREF-PUSH 2 4) */,
    0x00000048    /*  19 (LREF0-PUSH) */,
    0x0000004a    /*  20 (LREF2-PUSH) */,
    0x0000006b    /*  21 (LREF1-CAR) */,
    0x0000000d    /*  22 (PUSH) */,
    0x00000009    /*  23 (CONSTF-PUSH) */,
    0x00000045    /*  24 (LREF21) */,
    0x00005012    /*  25 (TAIL-CALL 5) */,
    0x00000014    /*  26 (RET) */,
    0x00000016    /*  27 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[23])) /* #<compiled-code (glob-fold-1 rec #f)@0x7fdc2dc600c0> */,
    0x0000000d    /*  29 (PUSH) */,
    0x00000048    /*  30 (LREF0-PUSH) */,
    0x0000004a    /*  31 (LREF2-PUSH) */,
    0x0000006b    /*  32 (LREF1-CAR) */,
    0x0000000d    /*  33 (PUSH) */,
    0x00000006    /*  34 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x00000045    /*  36 (LREF21) */,
    0x00005012    /*  37 (TAIL-CALL 5) */,
    0x00000014    /*  38 (RET) */,
    /* (glob-fold-1 rec* #f) */
    0x00000049    /*   0 (LREF1-PUSH) */,
    0x0000004d    /*   1 (LREF11-PUSH) */,
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x00000044    /*   3 (LREF20) */,
    0x0000301d    /*   4 (LOCAL-ENV-TAIL-CALL 3) */,
    0x00000014    /*   5 (RET) */,
    /* (glob-fold-1 rec*) */
    0x00000016    /*   0 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[25])) /* #<compiled-code (glob-fold-1 rec* #f)@0x7fdc2dc60000> */,
    0x0000300f    /*   2 (PUSH-PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1120]) + 9),
    0x0000004a    /*   4 (LREF2-PUSH) */,
    0x00000049    /*   5 (LREF1-PUSH) */,
    0x00000048    /*   6 (LREF0-PUSH) */,
    0x00000042    /*   7 (LREF11) */,
    0x0000301c    /*   8 (LOCAL-ENV-CALL 3) */,
    0x0000500f    /*   9 (PUSH-PRE-CALL 5) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1120]) + 21),
    0x0000005e    /*  11 (GREF-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#cons.2e296960> */,
    0x00000008    /*  13 (CONSTN-PUSH) */,
    0x0000004a    /*  14 (LREF2-PUSH) */,
    0x00000006    /*  15 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #/^[^.].*$/ */,
    0x00000006    /*  17 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x00000045    /*  19 (LREF21) */,
    0x00005011    /*  20 (CALL 5) */,
    0x00003063    /*  21 (PUSH-GREF-TAIL-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#fold.2e28d600> */,
    0x00000014    /*  23 (RET) */,
    /* glob-fold-1 */
    0x00002019    /*   0 (LOCAL-ENV-CLOSURES 2) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1041])) /* (#<compiled-code (glob-fold-1 rec)@0x7fdc2dc60120> #<compiled-code (glob-fold-1 rec*)@0x7fdc2dc60060>) */,
    0x0000300e    /*   2 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1144]) + 9),
    0x01401047    /*   4 (LREF-PUSH 1 5) */,
    0x0000004e    /*   5 (LREF12-PUSH) */,
    0x0000004c    /*   6 (LREF10-PUSH) */,
    0x0000305f    /*   7 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#glob-prepare-pattern.2e297e40> */,
    0x00001018    /*   9 (PUSH-LOCAL-ENV 1) */,
    0x0000006a    /*  10 (LREF0-CAR) */,
    0x0000000d    /*  11 (PUSH) */,
    0x00000076    /*  12 (LREF0-CDR) */,
    0x0000000d    /*  13 (PUSH) */,
    0x00c02047    /*  14 (LREF-PUSH 2 3) */,
    0x00000042    /*  15 (LREF11) */,
    0x0000301d    /*  16 (LOCAL-ENV-TAIL-CALL 3) */,
    0x00000014    /*  17 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1162]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* glob-fold-1 */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1162]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[27])) /* #<compiled-code glob-fold-1@0x7fdc2dc60240> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#glob-fold-1.2e28a940> */,
    0x00000014    /*  14 (RET) */,
    /* (glob-prepare-pattern f) */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1177]) + 7),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x00000006    /*   3 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[22])) /* "" */,
    0x0000205f    /*   5 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#equal?.2df2a6a0> */,
    0x0000001e    /*   7 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1177]) + 11),
    0x0000000a    /*   9 (CONST-RET) */,
    SCM_WORD(SCM_UNDEFINED) /* dir? */,
    0x0000200e    /*  11 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1177]) + 18),
    0x00000048    /*  13 (LREF0-PUSH) */,
    0x00000006    /*  14 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[471])) /* "**" */,
    0x0000205f    /*  16 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#equal?.2df2a4c0> */,
    0x0000001e    /*  18 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1177]) + 22),
    0x0000000a    /*  20 (CONST-RET) */,
    SCM_WORD(SCM_UNDEFINED) /* ** */,
    0x00000048    /*  22 (LREF0-PUSH) */,
    0x00001060    /*  23 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#glob-component->regexp.2df2a200> */,
    0x00000014    /*  25 (RET) */,
    /* glob-prepare-pattern */
    0x00001019    /*   0 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[30])) /* (#<undef>) */,
    0x00000044    /*   2 (LREF20) */,
    0x000000e8    /*   3 (ENV-SET 0) */,
    0x0000200e    /*   4 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1203]) + 10),
    0x0000004e    /*   6 (LREF12-PUSH) */,
    0x0000004d    /*   7 (LREF11-PUSH) */,
    0x0000205f    /*   8 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-split.2df2a100> */,
    0x00001018    /*  10 (PUSH-LOCAL-ENV 1) */,
    0x0000200e    /*  11 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1203]) + 19),
    0x0000006a    /*  13 (LREF0-CAR) */,
    0x0000000d    /*  14 (PUSH) */,
    0x00000006    /*  15 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[22])) /* "" */,
    0x0000205f    /*  17 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#equal?.2df30f20> */,
    0x0000001e    /*  19 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1203]) + 31),
    0x00000006    /*  21 (CONST-PUSH) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    0x0000200e    /*  23 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1203]) + 29),
    0x00000051    /*  25 (LREF30-PUSH) */,
    0x00000076    /*  26 (LREF0-CDR) */,
    0x00002062    /*  27 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#map.2df30d00> */,
    0x00000066    /*  29 (CONS) */,
    0x00000014    /*  30 (RET) */,
    0x0000004f    /*  31 (LREF20-PUSH) */,
    0x0000200e    /*  32 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1203]) + 38),
    0x00000051    /*  34 (LREF30-PUSH) */,
    0x00000048    /*  35 (LREF0-PUSH) */,
    0x0000205f    /*  36 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#map.2df30c20> */,
    0x00000066    /*  38 (CONS) */,
    0x00000014    /*  39 (RET) */,
    /* %toplevel */
    0x00001019    /*   0 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1058])) /* (#<compiled-code (glob-prepare-pattern f)@0x7fdc2da92720>) */,
    0x0000000e    /*   2 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1243]) + 6),
    0x0000005f    /*   4 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000001    /*   7 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* glob-prepare-pattern */,
    0x000000ee    /*   9 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1243]) + 14),
    0x00000016    /*  11 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[30])) /* #<compiled-code glob-prepare-pattern@0x7fdc2da926c0> */,
    0x00000014    /*  13 (RET) */,
    0x00000015    /*  14 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#glob-prepare-pattern.2df265e0> */,
    0x00000014    /*  16 (RET) */,
    /* (glob-expand-braces expand) */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1260]) + 7),
    0x00000049    /*   2 (LREF1-PUSH) */,
    0x00000006    /*   3 (CONST-PUSH) */,
    SCM_WORD(SCM_MAKE_CHAR(44)) /* #\, */,
    0x0000205f    /*   5 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-split.2d9ce1c0> */,
    0x00001018    /*   7 (PUSH-LOCAL-ENV 1) */,
    0x00000041    /*   8 (LREF10) */,
    0x00000022    /*   9 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1260]) + 12),
    0x00000053    /*  11 (LREF0-RET) */,
    0x00000048    /*  12 (LREF0-PUSH) */,
    0x00000041    /*  13 (LREF10) */,
    0x00002091    /*  14 (APPEND 2) */,
    0x00000014    /*  15 (RET) */,
    /* (glob-expand-braces parse loop #f #f) */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1276]) + 6),
    0x0000004d    /*   2 (LREF11-PUSH) */,
    0x00000049    /*   3 (LREF1-PUSH) */,
    0x0000205f    /*   4 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-append.2d99c560> */,
    0x0000000d    /*   6 (PUSH) */,
    0x0000003d    /*   7 (LREF0) */,
    0x00000066    /*   8 (CONS) */,
    0x00000014    /*   9 (RET) */,
    /* (glob-expand-braces parse loop #f) */
    0x00000016    /*   0 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[33])) /* #<compiled-code (glob-expand-braces parse loop #f #f)@0x7fdc2d438a20> */,
    0x0000000d    /*   2 (PUSH) */,
    0x00000048    /*   3 (LREF0-PUSH) */,
    0x0000004d    /*   4 (LREF11-PUSH) */,
    0x00003060    /*   5 (GREF-TAIL-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#fold.2d99cdc0> */,
    0x00000014    /*   7 (RET) */,
    /* (glob-expand-braces parse loop #f) */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x0000004c    /*   1 (LREF10-PUSH) */,
    0x00000042    /*   2 (LREF11) */,
    0x00002012    /*   3 (TAIL-CALL 2) */,
    0x00000014    /*   4 (RET) */,
    /* (glob-expand-braces parse loop #f) */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x0000004c    /*   1 (LREF10-PUSH) */,
    0x00000042    /*   2 (LREF11) */,
    0x00002012    /*   3 (TAIL-CALL 2) */,
    0x00000014    /*   4 (RET) */,
    /* (glob-expand-braces parse loop #f) */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00403047    /*   1 (LREF-PUSH 3 1) */,
    0x00000041    /*   2 (LREF10) */,
    0x00002012    /*   3 (TAIL-CALL 2) */,
    0x00000014    /*   4 (RET) */,
    /* (glob-expand-braces parse loop) */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 7),
    0x00000006    /*   2 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #/[{}]/ */,
    0x00000049    /*   4 (LREF1-PUSH) */,
    0x0000205f    /*   5 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#rxmatch.2d98e9a0> */,
    0x00001018    /*   7 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*   8 (LREF0) */,
    0x0000001e    /*   9 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 121),
    0x0000200e    /*  11 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 23),
    0x0000100e    /*  13 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 18),
    0x00000007    /*  15 (CONSTI-PUSH 0) */,
    0x0000003d    /*  16 (LREF0) */,
    0x00001011    /*  17 (CALL 1) */,
    0x0000000d    /*  18 (PUSH) */,
    0x00000006    /*  19 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[487])) /* "{" */,
    0x0000205f    /*  21 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#equal?.2d996240> */,
    0x0000001e    /*  23 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 76),
    0x0000300e    /*  25 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 39),
    0x0000100e    /*  27 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 33),
    0x00000006    /*  29 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* after */,
    0x0000003d    /*  31 (LREF0) */,
    0x00001011    /*  32 (CALL 1) */,
    0x0000000d    /*  33 (PUSH) */,
    0x00000006    /*  34 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[32])) /* ("") */,
    0x000010d0    /*  36 (LREF30-NUMADDI-PUSH 1) */,
    0x0040403c    /*  37 (LREF 4 1) */,
    0x0000301c    /*  38 (LOCAL-ENV-CALL 3) */,
    0x00002036    /*  39 (TAIL-RECEIVE 2 0) */,
    0x00000048    /*  40 (LREF0-PUSH) */,
    0x0000300e    /*  41 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 72),
    0x00000016    /*  43 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[34])) /* #<compiled-code (glob-expand-braces parse loop #f)@0x7fdc2d438b40> */,
    0x0000000d    /*  45 (PUSH) */,
    0x00000008    /*  46 (CONSTN-PUSH) */,
    0x0000200e    /*  47 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 70),
    0x0000200e    /*  49 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 66),
    0x00002019    /*  51 (LOCAL-ENV-CLOSURES 2) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[31])) /* (#<undef> #<undef>) */,
    0x0000005d    /*  53 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-append.2d9b1fc0> */,
    0x000010e8    /*  55 (ENV-SET 1) */,
    0x0000100e    /*  56 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 62),
    0x00000006    /*  58 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* before */,
    0x00000044    /*  60 (LREF20) */,
    0x00001011    /*  61 (CALL 1) */,
    0x000000e8    /*  62 (ENV-SET 0) */,
    0x00000016    /*  63 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[35])) /* #<compiled-code (glob-expand-braces parse loop #f)@0x7fdc2d4389c0> */,
    0x00000014    /*  65 (RET) */,
    0x0000000d    /*  66 (PUSH) */,
    0x0000004f    /*  67 (LREF20-PUSH) */,
    0x0000205f    /*  68 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#map.2d9a4ec0> */,
    0x00003062    /*  70 (PUSH-GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#fold.2d99abe0> */,
    0x0000000d    /*  72 (PUSH) */,
    0x00000046    /*  73 (LREF30) */,
    0x0000201d    /*  74 (LOCAL-ENV-TAIL-CALL 2) */,
    0x00000014    /*  75 (RET) */,
    0x00000046    /*  76 (LREF30) */,
    0x0000002d    /*  77 (BNUMNEI 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 85),
    0x00000006    /*  79 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[494])) /* "extra closing curly-brace in glob pattern:" */,
    0x00405047    /*  81 (LREF-PUSH 5 1) */,
    0x00002060    /*  82 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.2d9b1600> */,
    0x00000014    /*  84 (RET) */,
    0x0000300e    /*  85 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 114),
    0x00006047    /*  87 (LREF-PUSH 6 0) */,
    0x00000008    /*  88 (CONSTN-PUSH) */,
    0x0000200e    /*  89 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 112),
    0x0000200e    /*  91 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 108),
    0x00002019    /*  93 (LOCAL-ENV-CLOSURES 2) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[31])) /* (#<undef> #<undef>) */,
    0x0000005d    /*  95 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-append.2d9ba1e0> */,
    0x000010e8    /*  97 (ENV-SET 1) */,
    0x0000100e    /*  98 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 104),
    0x00000006    /* 100 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* before */,
    0x00000041    /* 102 (LREF10) */,
    0x00001011    /* 103 (CALL 1) */,
    0x000000e8    /* 104 (ENV-SET 0) */,
    0x00000016    /* 105 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[36])) /* #<compiled-code (glob-expand-braces parse loop #f)@0x7fdc2d438780> */,
    0x00000014    /* 107 (RET) */,
    0x0000000d    /* 108 (PUSH) */,
    0x0000004c    /* 109 (LREF10-PUSH) */,
    0x0000205f    /* 110 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#map.2d9b1000> */,
    0x00003062    /* 112 (PUSH-GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#fold.2d9b1120> */,
    0x0000100f    /* 114 (PUSH-PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 120),
    0x00000006    /* 116 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* after */,
    0x0000003d    /* 118 (LREF0) */,
    0x00001011    /* 119 (CALL 1) */,
    0x000020a3    /* 120 (VALUES-RET 2) */,
    0x00000046    /* 121 (LREF30) */,
    0x0000002d    /* 122 (BNUMNEI 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 143),
    0x0000200e    /* 124 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 140),
    0x0000100e    /* 126 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]) + 136),
    0x00001019    /* 128 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[30])) /* (#<undef>) */,
    0x0000005d    /* 130 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-append.2d9cef40> */,
    0x000000e8    /* 132 (ENV-SET 0) */,
    0x00000016    /* 133 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[37])) /* #<compiled-code (glob-expand-braces parse loop #f)@0x7fdc2d438720> */,
    0x00000014    /* 135 (RET) */,
    0x0000000d    /* 136 (PUSH) */,
    0x0000004c    /* 137 (LREF10-PUSH) */,
    0x0000205f    /* 138 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#map.2d9c47e0> */,
    0x0000000d    /* 140 (PUSH) */,
    0x00000004    /* 141 (CONSTF) */,
    0x000020a3    /* 142 (VALUES-RET 2) */,
    0x00000006    /* 143 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[497])) /* "unclosed curly-brace in glob pattern:" */,
    0x00405047    /* 145 (LREF-PUSH 5 1) */,
    0x00002060    /* 146 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#error.2d9ced40> */,
    0x00000014    /* 148 (RET) */,
    /* (glob-expand-braces parse) */
    0x00001019    /*   0 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1108])) /* (#<compiled-code (glob-expand-braces parse loop)@0x7fdc2d438ba0>) */,
    0x0000004e    /*   2 (LREF12-PUSH) */,
    0x0000004d    /*   3 (LREF11-PUSH) */,
    0x0000003d    /*   4 (LREF0) */,
    0x0000201d    /*   5 (LOCAL-ENV-TAIL-CALL 2) */,
    0x00000014    /*   6 (RET) */,
    /* glob-expand-braces */
    0x00002019    /*   0 (LOCAL-ENV-CLOSURES 2) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1116])) /* (#<compiled-code (glob-expand-braces parse)@0x7fdc2d438c00> #<undef>) */,
    0x00000044    /*   2 (LREF20) */,
    0x000000e8    /*   3 (ENV-SET 0) */,
    0x0000200e    /*   4 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1465]) + 11),
    0x0000004d    /*   6 (LREF11-PUSH) */,
    0x00000006    /*   7 (CONST-PUSH) */,
    SCM_WORD(SCM_MAKE_CHAR(123)) /* #\{ */,
    0x0000205f    /*   9 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-scan.2d9d4fc0> */,
    0x0000001e    /*  11 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1465]) + 28),
    0x0000300e    /*  13 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1465]) + 21),
    0x0000004d    /*  15 (LREF11-PUSH) */,
    0x00000006    /*  16 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[32])) /* ("") */,
    0x00000007    /*  18 (CONSTI-PUSH 0) */,
    0x0000003e    /*  19 (LREF1) */,
    0x0000301c    /*  20 (LOCAL-ENV-CALL 3) */,
    0x00401035    /*  21 (RECEIVE 1 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1465]) + 24),
    0x00000054    /*  23 (LREF1-RET) */,
    0x0000000d    /*  24 (PUSH) */,
    0x00000041    /*  25 (LREF10) */,
    0x00002091    /*  26 (APPEND 2) */,
    0x00000014    /*  27 (RET) */,
    0x0000004d    /*  28 (LREF11-PUSH) */,
    0x00000041    /*  29 (LREF10) */,
    0x00000066    /*  30 (CONS) */,
    0x00000014    /*  31 (RET) */,
    /* %toplevel */
    0x00001019    /*   0 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1071])) /* (#<compiled-code (glob-expand-braces expand)@0x7fdc2d438d20>) */,
    0x0000000e    /*   2 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1497]) + 6),
    0x0000005f    /*   4 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000001    /*   7 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* glob-expand-braces */,
    0x000000ee    /*   9 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1497]) + 14),
    0x00000016    /*  11 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[40])) /* #<compiled-code glob-expand-braces@0x7fdc2d438c60> */,
    0x00000014    /*  13 (RET) */,
    0x00000015    /*  14 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#glob-expand-braces.2d98ee20> */,
    0x00000014    /*  16 (RET) */,
    /* ((glob-component->regexp G1917) element1) */
    0x0000003e    /*   0 (LREF1) */,
    0x0000009a    /*   1 (EOFP) */,
    0x0000001e    /*   2 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]) + 6),
    0x0000000a    /*   4 (CONST-RET) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1123])) /* (eol) */,
    0x0000003e    /*   6 (LREF1) */,
    0x0000002f    /*   7 (BNEQVC) */,
    SCM_WORD(SCM_MAKE_CHAR(42)) /* #\* */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]) + 35)  /*     35 */,
    0x00000006    /*  10 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1127])) /* (rep 0 #f any) */,
    0x0000200e    /*  12 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]) + 33),
    0x000000db    /*  14 (READ-CHAR 0) */,
    0x0000000d    /*  15 (PUSH) */,
    0x00000048    /*  16 (LREF0-PUSH) */,
    0x00002017    /*  17 (LOCAL-ENV 2) */,
    0x0000003e    /*  18 (LREF1) */,
    0x0000002f    /*  19 (BNEQVC) */,
    SCM_WORD(SCM_MAKE_CHAR(42)) /* #\* */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]) + 28)  /*     28 */,
    0x000000db    /*  22 (READ-CHAR 0) */,
    0x0000000d    /*  23 (PUSH) */,
    0x00000048    /*  24 (LREF0-PUSH) */,
    0x0000101b    /*  25 (LOCAL-ENV-JUMP 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]) + 18),
    0x00000014    /*  27 (RET) */,
    0x00000049    /*  28 (LREF1-PUSH) */,
    0x00000048    /*  29 (LREF0-PUSH) */,
    0x00000044    /*  30 (LREF20) */,
    0x0000201d    /*  31 (LOCAL-ENV-TAIL-CALL 2) */,
    0x00000014    /*  32 (RET) */,
    0x00000066    /*  33 (CONS) */,
    0x00000014    /*  34 (RET) */,
    0x0000003e    /*  35 (LREF1) */,
    0x0000002f    /*  36 (BNEQVC) */,
    SCM_WORD(SCM_MAKE_CHAR(63)) /* #\? */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]) + 50)  /*     50 */,
    0x00000006    /*  39 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* any */,
    0x0000200e    /*  41 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]) + 48),
    0x000000db    /*  43 (READ-CHAR 0) */,
    0x0000000d    /*  44 (PUSH) */,
    0x00000048    /*  45 (LREF0-PUSH) */,
    0x00000041    /*  46 (LREF10) */,
    0x0000201c    /*  47 (LOCAL-ENV-CALL 2) */,
    0x00000066    /*  48 (CONS) */,
    0x00000014    /*  49 (RET) */,
    0x0000003e    /*  50 (LREF1) */,
    0x0000002f    /*  51 (BNEQVC) */,
    SCM_WORD(SCM_MAKE_CHAR(91)) /* #\[ */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]) + 97)  /*     97 */,
    0x000000dc    /*  54 (PEEK-CHAR 0) */,
    0x00001018    /*  55 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  56 (LREF0) */,
    0x0000002f    /*  57 (BNEQVC) */,
    SCM_WORD(SCM_MAKE_CHAR(33)) /* #\! */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]) + 81)  /*     81 */,
    0x000000db    /*  60 (READ-CHAR 0) */,
    0x0000100e    /*  61 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]) + 66),
    0x000000de    /*  63 (CURIN) */,
    0x00001062    /*  64 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read-char-set.2d083760> */,
    0x00001018    /*  66 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  67 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]) + 72),
    0x00000048    /*  69 (LREF0-PUSH) */,
    0x0000105f    /*  70 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#char-set-complement!.2d083620> */,
    0x0000200f    /*  72 (PUSH-PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]) + 79),
    0x000000db    /*  74 (READ-CHAR 0) */,
    0x0000000d    /*  75 (PUSH) */,
    0x0000004f    /*  76 (LREF20-PUSH) */,
    0x00000046    /*  77 (LREF30) */,
    0x0000201c    /*  78 (LOCAL-ENV-CALL 2) */,
    0x00000066    /*  79 (CONS) */,
    0x00000014    /*  80 (RET) */,
    0x0000100e    /*  81 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]) + 86),
    0x000000de    /*  83 (CURIN) */,
    0x00001062    /*  84 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read-char-set.2d0831a0> */,
    0x00001018    /*  86 (PUSH-LOCAL-ENV 1) */,
    0x00000048    /*  87 (LREF0-PUSH) */,
    0x0000200e    /*  88 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]) + 95),
    0x000000db    /*  90 (READ-CHAR 0) */,
    0x0000000d    /*  91 (PUSH) */,
    0x0000004f    /*  92 (LREF20-PUSH) */,
    0x00000046    /*  93 (LREF30) */,
    0x0000201c    /*  94 (LOCAL-ENV-CALL 2) */,
    0x00000066    /*  95 (CONS) */,
    0x00000014    /*  96 (RET) */,
    0x00000049    /*  97 (LREF1-PUSH) */,
    0x0000200e    /*  98 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]) + 105),
    0x000000db    /* 100 (READ-CHAR 0) */,
    0x0000000d    /* 101 (PUSH) */,
    0x00000048    /* 102 (LREF0-PUSH) */,
    0x00000041    /* 103 (LREF10) */,
    0x0000201c    /* 104 (LOCAL-ENV-CALL 2) */,
    0x00000066    /* 105 (CONS) */,
    0x00000014    /* 106 (RET) */,
    /* (glob-component->regexp G1917) */
    0x00001019    /*   0 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1136])) /* (#<compiled-code ((glob-component->regexp #:G1917) element1)@0x7fdc2ccb0ea0>) */,
    0x00000007    /*   2 (CONSTI-PUSH 0) */,
    0x00000009    /*   3 (CONSTF-PUSH) */,
    0x00000006    /*   4 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* bol */,
    0x0000100e    /*   6 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1621]) + 108),
    0x000000db    /*   8 (READ-CHAR 0) */,
    0x00001018    /*   9 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  10 (LREF0) */,
    0x0000002f    /*  11 (BNEQVC) */,
    SCM_WORD(SCM_MAKE_CHAR(42)) /* #\* */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1621]) + 88)  /*     88 */,
    0x000000db    /*  14 (READ-CHAR 0) */,
    0x0000000d    /*  15 (PUSH) */,
    0x00000008    /*  16 (CONSTN-PUSH) */,
    0x00002017    /*  17 (LOCAL-ENV 2) */,
    0x0000003e    /*  18 (LREF1) */,
    0x0000002f    /*  19 (BNEQVC) */,
    SCM_WORD(SCM_MAKE_CHAR(42)) /* #\* */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1621]) + 28)  /*     28 */,
    0x000000db    /*  22 (READ-CHAR 0) */,
    0x0000000d    /*  23 (PUSH) */,
    0x00000048    /*  24 (LREF0-PUSH) */,
    0x0000101b    /*  25 (LOCAL-ENV-JUMP 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1621]) + 18),
    0x00000014    /*  27 (RET) */,
    0x0000003e    /*  28 (LREF1) */,
    0x0000002f    /*  29 (BNEQVC) */,
    SCM_WORD(SCM_MAKE_CHAR(63)) /* #\? */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1621]) + 45)  /*     45 */,
    0x00000006    /*  32 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1137])) /* (comp . #[.]) */,
    0x00000006    /*  34 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1127])) /* (rep 0 #f any) */,
    0x0000200e    /*  36 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1621]) + 43),
    0x000000db    /*  38 (READ-CHAR 0) */,
    0x0000000d    /*  39 (PUSH) */,
    0x00000048    /*  40 (LREF0-PUSH) */,
    0x00000044    /*  41 (LREF20) */,
    0x0000201c    /*  42 (LOCAL-ENV-CALL 2) */,
    0x00003089    /*  43 (LIST-STAR 3) */,
    0x00000014    /*  44 (RET) */,
    0x0000003e    /*  45 (LREF1) */,
    0x0000002f    /*  46 (BNEQVC) */,
    SCM_WORD(SCM_MAKE_CHAR(46)) /* #\. */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1621]) + 65)  /*     65 */,
    0x00000006    /*  49 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1137])) /* (comp . #[.]) */,
    0x00000006    /*  51 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1127])) /* (rep 0 #f any) */,
    0x00000006    /*  53 (CONST-PUSH) */,
    SCM_WORD(SCM_MAKE_CHAR(46)) /* #\. */,
    0x0000200e    /*  55 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1621]) + 62),
    0x000000db    /*  57 (READ-CHAR 0) */,
    0x0000000d    /*  58 (PUSH) */,
    0x00000048    /*  59 (LREF0-PUSH) */,
    0x00000044    /*  60 (LREF20) */,
    0x0000201c    /*  61 (LOCAL-ENV-CALL 2) */,
    0x00003089    /*  62 (LIST-STAR 3) */,
    0x00000066    /*  63 (CONS) */,
    0x00000014    /*  64 (RET) */,
    0x00000006    /*  65 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* rep */,
    0x00000007    /*  67 (CONSTI-PUSH 0) */,
    0x00001007    /*  68 (CONSTI-PUSH 1) */,
    0x00000006    /*  69 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* seq */,
    0x00000006    /*  71 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1137])) /* (comp . #[.]) */,
    0x00000001    /*  73 (CONST) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1127])) /* (rep 0 #f any) */,
    0x00002088    /*  75 (LIST 2) */,
    0x00000066    /*  76 (CONS) */,
    0x00002088    /*  77 (LIST 2) */,
    0x00003089    /*  78 (LIST-STAR 3) */,
    0x0000200f    /*  79 (PUSH-PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1621]) + 85),
    0x00000049    /*  81 (LREF1-PUSH) */,
    0x00000048    /*  82 (LREF0-PUSH) */,
    0x00000044    /*  83 (LREF20) */,
    0x0000201c    /*  84 (LOCAL-ENV-CALL 2) */,
    0x00000066    /*  85 (CONS) */,
    0x00000014    /*  86 (RET) */,
    0x00000014    /*  87 (RET) */,
    0x0000003d    /*  88 (LREF0) */,
    0x0000002f    /*  89 (BNEQVC) */,
    SCM_WORD(SCM_MAKE_CHAR(63)) /* #\? */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1621]) + 103)  /*    103 */,
    0x00000006    /*  92 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1137])) /* (comp . #[.]) */,
    0x0000200e    /*  94 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1621]) + 101),
    0x000000db    /*  96 (READ-CHAR 0) */,
    0x0000000d    /*  97 (PUSH) */,
    0x00000008    /*  98 (CONSTN-PUSH) */,
    0x00000041    /*  99 (LREF10) */,
    0x0000201c    /* 100 (LOCAL-ENV-CALL 2) */,
    0x00000066    /* 101 (CONS) */,
    0x00000014    /* 102 (RET) */,
    0x00000048    /* 103 (LREF0-PUSH) */,
    0x00000008    /* 104 (CONSTN-PUSH) */,
    0x00000041    /* 105 (LREF10) */,
    0x0000201d    /* 106 (LOCAL-ENV-TAIL-CALL 2) */,
    0x00000014    /* 107 (RET) */,
    0x00003089    /* 108 (LIST-STAR 3) */,
    0x00000066    /* 109 (CONS) */,
    0x00000014    /* 110 (RET) */,
    /* glob-component->regexp */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1732]) + 10),
    0x0000200e    /*   2 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1732]) + 8),
    0x00000048    /*   4 (LREF0-PUSH) */,
    0x0000004c    /*   5 (LREF10-PUSH) */,
    0x0000205f    /*   6 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#with-input-from-string.2d064a20> */,
    0x00001062    /*   8 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#regexp-optimize.2d064a60> */,
    0x00001063    /*  10 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#regexp-compile.2d064aa0> */,
    0x00000014    /*  12 (RET) */,
    /* %toplevel */
    0x00001019    /*   0 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1143])) /* (#<compiled-code (glob-component->regexp #:G1917)@0x7fdc2ccb0f00>) */,
    0x0000000e    /*   2 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1745]) + 6),
    0x0000005f    /*   4 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000001    /*   7 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* glob-component->regexp */,
    0x000000ee    /*   9 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1745]) + 14),
    0x00000016    /*  11 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[44])) /* #<compiled-code glob-component->regexp@0x7fdc2ccb0e40> */,
    0x00000014    /*  13 (RET) */,
    0x00000015    /*  14 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#glob-component->regexp.2d064f40> */,
    0x00000014    /*  16 (RET) */,
    /* fixed-regexp? */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1762]) + 5),
    0x00000048    /*   2 (LREF0-PUSH) */,
    0x0000105f    /*   3 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#regexp-ast.2cb77220> */,
    0x00001018    /*   5 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*   6 (LREF0) */,
    0x0000008a    /*   7 (LENGTH) */,
    0x0000000d    /*   8 (PUSH) */,
    0x00004002    /*   9 (CONSTI 4) */,
    0x00000026    /*  10 (BNGT) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1762]) + 52),
    0x0000003d    /*  12 (LREF0) */,
    0x00000086    /*  13 (CDDR) */,
    0x00000068    /*  14 (CAR) */,
    0x0000002e    /*  15 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* bol */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1762]) + 51)  /*     51 */,
    0x0000003d    /*  18 (LREF0) */,
    0x00000086    /*  19 (CDDR) */,
    0x00000075    /*  20 (CDR-PUSH) */,
    0x00000008    /*  21 (CONSTN-PUSH) */,
    0x00002017    /*  22 (LOCAL-ENV 2) */,
    0x00000077    /*  23 (LREF1-CDR) */,
    0x00000022    /*  24 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1762]) + 36),
    0x0000006b    /*  26 (LREF1-CAR) */,
    0x0000002e    /*  27 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* eol */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1762]) + 35)  /*     35 */,
    0x0000003d    /*  30 (LREF0) */,
    0x00000093    /*  31 (REVERSE) */,
    0x00001063    /*  32 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#list->string.2cb7bca0> */,
    0x00000014    /*  34 (RET) */,
    0x00000014    /*  35 (RET) */,
    0x0000006b    /*  36 (LREF1-CAR) */,
    0x00000099    /*  37 (CHARP) */,
    0x0000001e    /*  38 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1762]) + 49),
    0x00000077    /*  40 (LREF1-CDR) */,
    0x0000000d    /*  41 (PUSH) */,
    0x0000006b    /*  42 (LREF1-CAR) */,
    0x0000000d    /*  43 (PUSH) */,
    0x0000003d    /*  44 (LREF0) */,
    0x00000067    /*  45 (CONS-PUSH) */,
    0x0000101b    /*  46 (LOCAL-ENV-JUMP 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1762]) + 23),
    0x00000014    /*  48 (RET) */,
    0x0000000b    /*  49 (CONSTF-RET) */,
    0x00000014    /*  50 (RET) */,
    0x00000014    /*  51 (RET) */,
    0x00000014    /*  52 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1815]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* fixed-regexp? */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1815]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[46])) /* #<compiled-code fixed-regexp?@0x7fdc2c780cc0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#fixed-regexp?.2cb77ee0> */,
    0x00000014    /*  14 (RET) */,
    /* (make-glob-fs-fold ensure-dirname) */
    0x0000003d    /*   0 (LREF0) */,
    0x00000030    /*   1 (RF) */,
    0x0000100e    /*   2 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1830]) + 7),
    0x00000048    /*   4 (LREF0-PUSH) */,
    0x0000105f    /*   5 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-length.2e6194c0> */,
    0x00001018    /*   7 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*   8 (LREF0) */,
    0x0000001e    /*   9 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1830]) + 36),
    0x00000002    /*  11 (CONSTI 0) */,
    0x0000002b    /*  12 (LREF-VAL0-BNGT 0 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1830]) + 36),
    0x0000200e    /*  14 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1830]) + 20),
    0x0000004c    /*  16 (LREF10-PUSH) */,
    -0x00000f39   /*  17 (LREF0-NUMADDI-PUSH -1) */,
    0x0000205f    /*  18 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-ref.2e6191a0> */,
    0x0000200f    /*  20 (PUSH-PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1830]) + 26),
    0x00000051    /*  22 (LREF30-PUSH) */,
    0x00000007    /*  23 (CONSTI-PUSH 0) */,
    0x0000205f    /*  24 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-ref.2e619040> */,
    0x00000090    /*  26 (EQV) */,
    0x00000092    /*  27 (NOT) */,
    0x0000001e    /*  28 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1830]) + 36),
    0x0000200e    /*  30 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1830]) + 36),
    0x0000004c    /*  32 (LREF10-PUSH) */,
    0x00000051    /*  33 (LREF30-PUSH) */,
    0x0000205f    /*  34 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-append.2e61ee00> */,
    0x0000001a    /*  36 (POP-LOCAL-ENV) */,
    0x00000031    /*  37 (RT) */,
    0x00000053    /*  38 (LREF0-RET) */,
    /* (make-glob-fs-fold loop1920 #f) */
    0x0000100e    /*   0 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1869]) + 5),
    0x00000049    /*   2 (LREF1-PUSH) */,
    0x0040303c    /*   3 (LREF 3 1) */,
    0x00001011    /*   4 (CALL 1) */,
    0x0000001e    /*   5 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1869]) + 40),
    0x0000200e    /*   7 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1869]) + 13),
    0x0000004f    /*   9 (LREF20-PUSH) */,
    0x00000049    /*  10 (LREF1-PUSH) */,
    0x0000205f    /*  11 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-append.2e677cc0> */,
    0x00001018    /*  13 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  14 (LREF0) */,
    0x0000001e    /*  15 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1869]) + 37),
    0x0000403c    /*  17 (LREF 4 0) */,
    0x0000001e    /*  18 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1869]) + 27),
    0x0000100e    /*  20 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1869]) + 25),
    0x00000048    /*  22 (LREF0-PUSH) */,
    0x0000105f    /*  23 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#file-is-directory?.2e677b80> */,
    0x0000001e    /*  25 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1869]) + 37),
    0x0000200e    /*  27 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1869]) + 37),
    0x00000048    /*  29 (LREF0-PUSH) */,
    0x0000004c    /*  30 (LREF10-PUSH) */,
    0x0100403c    /*  31 (LREF 4 4) */,
    0x00002011    /*  32 (CALL 2) */,
    0x00000013    /*  33 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1869]) + 37),
    0x00000013    /*  35 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1869]) + 27),
    0x0000001a    /*  37 (POP-LOCAL-ENV) */,
    0x00000031    /*  38 (RT) */,
    0x00000053    /*  39 (LREF0-RET) */,
    0x00000053    /*  40 (LREF0-RET) */,
    /* (make-glob-fs-fold loop1920) */
    0x0000003f    /*   0 (LREF2) */,
    0x0000002f    /*   1 (BNEQVC) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 12)  /*     12 */,
    0x00000042    /*   4 (LREF11) */,
    0x0000001e    /*   5 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 9),
    0x00000013    /*   7 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 31),
    0x00000044    /*   9 (LREF20) */,
    0x00000013    /*  10 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 31),
    0x0000003f    /*  12 (LREF2) */,
    0x0000002f    /*  13 (BNEQVC) */,
    SCM_WORD(SCM_FALSE) /* #f */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 25)  /*     25 */,
    0x00000041    /*  16 (LREF10) */,
    0x0000001e    /*  17 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 21),
    0x00000013    /*  19 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 31),
    0x00000001    /*  21 (CONST) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[22])) /* "" */,
    0x00000013    /*  23 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 31),
    0x0000200e    /*  25 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 31),
    0x0000004a    /*  27 (LREF2-PUSH) */,
    0x0000004f    /*  28 (LREF20-PUSH) */,
    0x0000205f    /*  29 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-append.2e659cc0> */,
    0x00001018    /*  31 (PUSH-LOCAL-ENV 1) */,
    0x00000042    /*  32 (LREF11) */,
    0x0000002e    /*  33 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* dir? */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 41)  /*     41 */,
    0x00000048    /*  36 (LREF0-PUSH) */,
    0x00c01047    /*  37 (LREF-PUSH 1 3) */,
    0x0100103c    /*  38 (LREF 1 4) */,
    0x00002012    /*  39 (TAIL-CALL 2) */,
    0x00000014    /*  40 (RET) */,
    0x0000100e    /*  41 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 46),
    0x0000004d    /*  43 (LREF11-PUSH) */,
    0x0000105f    /*  44 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#fixed-regexp?.2e659900> */,
    0x00001018    /*  46 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  47 (LREF0) */,
    0x0000001e    /*  48 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 84),
    0x0000200e    /*  50 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 56),
    0x0000004c    /*  52 (LREF10-PUSH) */,
    0x00000048    /*  53 (LREF0-PUSH) */,
    0x0000205f    /*  54 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-append.2e672460> */,
    0x00001018    /*  56 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  57 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 62),
    0x00000048    /*  59 (LREF0-PUSH) */,
    0x0000105f    /*  60 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#file-exists?.2e672160> */,
    0x0000001e    /*  62 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 83),
    0x00000046    /*  64 (LREF30) */,
    0x0000001e    /*  65 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 74),
    0x0000100e    /*  67 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 72),
    0x00000048    /*  69 (LREF0-PUSH) */,
    0x0000105f    /*  70 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#file-is-directory?.2e676fe0> */,
    0x0000001e    /*  72 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 79),
    0x00000048    /*  74 (LREF0-PUSH) */,
    0x00c03047    /*  75 (LREF-PUSH 3 3) */,
    0x0100303c    /*  76 (LREF 3 4) */,
    0x00002012    /*  77 (TAIL-CALL 2) */,
    0x00000014    /*  78 (RET) */,
    0x00c03052    /*  79 (LREF-RET 3 3) */,
    0x00000013    /*  80 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 74),
    0x00000014    /*  82 (RET) */,
    0x00c03052    /*  83 (LREF-RET 3 3) */,
    0x00000016    /*  84 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[49])) /* #<compiled-code (make-glob-fs-fold #:loop1920 #f)@0x7fdc2c8ab600> */,
    0x0000000d    /*  86 (PUSH) */,
    0x00c02047    /*  87 (LREF-PUSH 2 3) */,
    0x0000100e    /*  88 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 119),
    0x0080203c    /*  90 (LREF 2 2) */,
    0x0000002f    /*  91 (BNEQVC) */,
    SCM_WORD(SCM_TRUE) /* #t */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 103)  /*    103 */,
    0x0040303c    /*  94 (LREF 3 1) */,
    0x0000001e    /*  95 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 99),
    0x00000013    /*  97 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 117),
    0x00000001    /*  99 (CONST) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[19])) /* "/" */,
    0x00000013    /* 101 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 117),
    0x0080203c    /* 103 (LREF 2 2) */,
    0x0000002f    /* 104 (BNEQVC) */,
    SCM_WORD(SCM_FALSE) /* #f */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 116)  /*    116 */,
    0x00000046    /* 107 (LREF30) */,
    0x0000001e    /* 108 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 112),
    0x00000013    /* 110 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 117),
    0x00000001    /* 112 (CONST) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[42])) /* "." */,
    0x00000013    /* 114 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]) + 117),
    0x0080203c    /* 116 (LREF 2 2) */,
    0x00001062    /* 117 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#sys-readdir.2e677a00> */,
    0x00003063    /* 119 (PUSH-GREF-TAIL-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#fold.2e676dc0> */,
    0x00000014    /* 121 (RET) */,
    /* make-glob-fs-fold */
    0x00000048    /*   0 (LREF0-PUSH) */,
    0x00000005    /*   1 (CONSTU) */,
    0x0000000d    /*   2 (PUSH) */,
    0x00000005    /*   3 (CONSTU) */,
    0x00003018    /*   4 (PUSH-LOCAL-ENV 3) */,
    0x0000003f    /*   5 (LREF2) */,
    0x00000022    /*   6 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 68),
    0x0000100e    /*   8 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 13),
    0x00000049    /*  10 (LREF1-PUSH) */,
    0x0000105f    /*  11 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.2e5ea1a0> */,
    0x0000001e    /*  13 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 18),
    0x00000004    /*  15 (CONSTF) */,
    0x00000013    /*  16 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 19),
    0x0000003e    /*  18 (LREF1) */,
    0x00001018    /*  19 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  20 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 25),
    0x0000004c    /*  22 (LREF10-PUSH) */,
    0x0000105f    /*  23 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#undefined?.2e5ea1a0> */,
    0x0000001e    /*  25 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 30),
    0x00000004    /*  27 (CONSTF) */,
    0x00000013    /*  28 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 31),
    0x00000041    /*  30 (LREF10) */,
    0x00001018    /*  31 (PUSH-LOCAL-ENV 1) */,
    0x0000100e    /*  32 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 42),
    0x0000000e    /*  34 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 38),
    0x0000005f    /*  36 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#gauche-architecture.2e6092a0> */,
    0x0000000d    /*  38 (PUSH) */,
    0x00000001    /*  39 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* #/mingw/ */,
    0x00001011    /*  41 (CALL 1) */,
    0x0000001e    /*  42 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 48),
    0x00000001    /*  44 (CONST) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[18])) /* "\\" */,
    0x00000013    /*  46 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 50),
    0x00000001    /*  48 (CONST) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[19])) /* "/" */,
    0x00001018    /*  50 (PUSH-LOCAL-ENV 1) */,
    0x00003019    /*  51 (LOCAL-ENV-CLOSURES 3) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1161])) /* (#<compiled-code (make-glob-fs-fold ensure-dirname)@0x7fdc2c8ab6c0> #<undef> #<undef>) */,
    0x0000100e    /*  53 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 58),
    0x00000051    /*  55 (LREF30-PUSH) */,
    0x0000003f    /*  56 (LREF2) */,
    0x0000101c    /*  57 (LOCAL-ENV-CALL 1) */,
    0x000010e8    /*  58 (ENV-SET 1) */,
    0x0000100e    /*  59 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 64),
    0x0000004f    /*  61 (LREF20-PUSH) */,
    0x0000003f    /*  62 (LREF2) */,
    0x0000101c    /*  63 (LOCAL-ENV-CALL 1) */,
    0x000000e8    /*  64 (ENV-SET 0) */,
    0x00000016    /*  65 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[50])) /* #<compiled-code (make-glob-fs-fold #:loop1920)@0x7fdc2c8ab660> */,
    0x00000014    /*  67 (RET) */,
    0x00000078    /*  68 (LREF2-CDR) */,
    0x00000022    /*  69 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 77),
    0x00000006    /*  71 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[46])) /* "keyword list not even" */,
    0x0000004a    /*  73 (LREF2-PUSH) */,
    0x00002060    /*  74 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.2e5efe00> */,
    0x00000014    /*  76 (RET) */,
    0x0000100e    /*  77 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 82),
    0x0000006c    /*  79 (LREF2-CAR) */,
    0x00001062    /*  80 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#unwrap-syntax-1.2e5ef620> */,
    0x00001018    /*  82 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  83 (LREF0) */,
    0x0000002e    /*  84 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :root-path */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 95)  /*     95 */,
    0x00000043    /*  87 (LREF12) */,
    0x00000087    /*  88 (CDDR-PUSH) */,
    0x00000043    /*  89 (LREF12) */,
    0x00000083    /*  90 (CADR-PUSH) */,
    0x0000004c    /*  91 (LREF10-PUSH) */,
    0x0000201b    /*  92 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 5),
    0x00000014    /*  94 (RET) */,
    0x0000003d    /*  95 (LREF0) */,
    0x0000002e    /*  96 (BNEQC) */,
    SCM_WORD(SCM_UNDEFINED) /* :current-path */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 107)  /*    107 */,
    0x00000043    /*  99 (LREF12) */,
    0x00000087    /* 100 (CDDR-PUSH) */,
    0x0000004d    /* 101 (LREF11-PUSH) */,
    0x00000043    /* 102 (LREF12) */,
    0x00000083    /* 103 (CADR-PUSH) */,
    0x0000201b    /* 104 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 5),
    0x00000014    /* 106 (RET) */,
    0x0000200e    /* 107 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 114),
    0x00000006    /* 109 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[51])) /* "unknown keyword ~S" */,
    0x00000070    /* 111 (LREF12-CAR) */,
    0x00002062    /* 112 (PUSH-GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#errorf.2e5ef240> */,
    0x00000043    /* 114 (LREF12) */,
    0x00000087    /* 115 (CDDR-PUSH) */,
    0x0000004d    /* 116 (LREF11-PUSH) */,
    0x0000004c    /* 117 (LREF10-PUSH) */,
    0x0000201b    /* 118 (LOCAL-ENV-JUMP 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]) + 5),
    0x00000014    /* 120 (RET) */,
    0x00000014    /* 121 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2154]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* make-glob-fs-fold */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2154]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[51])) /* #<compiled-code make-glob-fs-fold@0x7fdc2c8ab720> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-glob-fs-fold.2ef40440> */,
    0x00000014    /*  14 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2169]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* glob-fs-folder */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2169]) + 12),
    0x00000060    /*   9 (GREF-TAIL-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-glob-fs-fold.2da649a0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#glob-fs-folder.2da64a40> */,
    0x00000014    /*  14 (RET) */,
    /* %sys-mintty? */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2184]) + 8),
    0x00000006    /*   2 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* gauche */,
    0x00000006    /*   4 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* sys-win-pipe-name */,
    0x0000205f    /*   6 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#module-binds?.2d629e80> */,
    0x00000030    /*   8 (RF) */,
    0x0000100e    /*   9 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2184]) + 14),
    0x00000048    /*  11 (LREF0-PUSH) */,
    0x0000105f    /*  12 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#sys-win-pipe-name.2d629cc0> */,
    0x00001018    /*  14 (PUSH-LOCAL-ENV 1) */,
    0x0000003d    /*  15 (LREF0) */,
    0x00000030    /*  16 (RF) */,
    0x0000100e    /*  17 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2184]) + 23),
    0x00000048    /*  19 (LREF0-PUSH) */,
    0x00000001    /*  20 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* #/^\\msys-[0-9a-f]+-pty[0-9]+-((?:to|from))-master.*$/ */,
    0x00001011    /*  22 (CALL 1) */,
    0x00001063    /*  23 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#boolean.2d629c00> */,
    0x00000014    /*  25 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2210]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* %sys-mintty? */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2210]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[54])) /* #<compiled-code %sys-mintty?@0x7fdc2cea0600> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%sys-mintty?.2d6251a0> */,
    0x00000014    /*  14 (RET) */,
    /* (%sys-escape-windows-command-line G1928) */
    0x0000200e    /*   0 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2225]) + 19),
    0x00002007    /*   2 (CONSTI-PUSH 2) */,
    0x0000100e    /*   3 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2225]) + 12),
    0x0000100e    /*   5 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2225]) + 10),
    0x00001007    /*   7 (CONSTI-PUSH 1) */,
    0x0000003d    /*   8 (LREF0) */,
    0x00001011    /*   9 (CALL 1) */,
    0x00001062    /*  10 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-length.2d70ca20> */,
    0x000000b4    /*  12 (NUMMUL2) */,
    0x000010bc    /*  13 (NUMADDI 1) */,
    0x0000000d    /*  14 (PUSH) */,
    0x00000006    /*  15 (CONST-PUSH) */,
    SCM_WORD(SCM_MAKE_CHAR(92)) /* #\\ */,
    0x0000205f    /*  17 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-string.2d70cac0> */,
    0x0000000d    /*  19 (PUSH) */,
    0x00000006    /*  20 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[539])) /* "\"" */,
    0x00002060    /*  22 (GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-append.2d70cb00> */,
    0x00000014    /*  24 (RET) */,
    /* %sys-escape-windows-command-line */
    0x0000003e    /*   0 (LREF1) */,
    0x0000009b    /*   1 (STRINGP) */,
    0x0000001e    /*   2 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]) + 78),
    0x0000200e    /*   4 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]) + 11),
    0x00000049    /*   6 (LREF1-PUSH) */,
    0x00000006    /*   7 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[22])) /* "" */,
    0x0000205f    /*   9 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#equal?.2d707c40> */,
    0x0000001e    /*  11 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]) + 15),
    0x0000000a    /*  13 (CONST-RET) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[543])) /* "\"\"" */,
    0x0000003d    /*  15 (LREF0) */,
    0x0000001e    /*  16 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]) + 53),
    0x0000100e    /*  18 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]) + 24),
    0x00000049    /*  20 (LREF1-PUSH) */,
    0x00000001    /*  21 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* #/[!"%&()<>^|]/ */,
    0x00001011    /*  23 (CALL 1) */,
    0x0000001e    /*  24 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]) + 35),
    0x0000200e    /*  26 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]) + 35),
    0x00000006    /*  28 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[545])) /* "It is unsafe to pass argument ~s to BAT or CMD file." */,
    0x00000049    /*  30 (LREF1-PUSH) */,
    0x0000205f    /*  31 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#errorf.2d707b00> */,
    0x00000013    /*  33 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]) + 35),
    0x0000200e    /*  35 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]) + 42),
    0x00000049    /*  37 (LREF1-PUSH) */,
    0x00000006    /*  38 (CONST-PUSH) */,
    SCM_WORD(SCM_MAKE_CHAR(32)) /* #\space */,
    0x0000205f    /*  40 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-scan.2d707a60> */,
    0x0000001e    /*  42 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]) + 52),
    0x00000006    /*  44 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[539])) /* "\"" */,
    0x00000049    /*  46 (LREF1-PUSH) */,
    0x00000006    /*  47 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[539])) /* "\"" */,
    0x00003060    /*  49 (GREF-TAIL-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-append.2d7079e0> */,
    0x00000014    /*  51 (RET) */,
    0x00000054    /*  52 (LREF1-RET) */,
    0x0000100e    /*  53 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]) + 59),
    0x00000049    /*  55 (LREF1-PUSH) */,
    0x00000001    /*  56 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* #/[\u0009-\u000d !&'+,;->\[\]^`{}~]/ */,
    0x00001011    /*  58 (CALL 1) */,
    0x0000001e    /*  59 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]) + 77),
    0x00000006    /*  61 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[539])) /* "\"" */,
    0x0000300e    /*  63 (PRE-CALL 3) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]) + 71),
    0x00000006    /*  65 (CONST-PUSH) */,
    SCM_WORD(SCM_UNDEFINED) /* #/(\\*)"/ */,
    0x00000049    /*  67 (LREF1-PUSH) */,
    0x0000004c    /*  68 (LREF10-PUSH) */,
    0x0000305f    /*  69 (GREF-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#regexp-replace-all.2d70abc0> */,
    0x0000000d    /*  71 (PUSH) */,
    0x00000006    /*  72 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[539])) /* "\"" */,
    0x00003060    /*  74 (GREF-TAIL-CALL 3) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#string-append.2d7072e0> */,
    0x00000014    /*  76 (RET) */,
    0x00000054    /*  77 (LREF1-RET) */,
    0x0000100e    /*  78 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]) + 83),
    0x00000049    /*  80 (LREF1-PUSH) */,
    0x0000105f    /*  81 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#write-to-string.2d707cc0> */,
    0x00001063    /*  83 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%sys-escape-windows-command-line.2d707d00> */,
    0x00000014    /*  85 (RET) */,
    /* %toplevel */
    0x00001019    /*   0 (LOCAL-ENV-CLOSURES 1) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[1205])) /* (#<compiled-code (%sys-escape-windows-command-line #:G1928)@0x7fdc2cb79cc0>) */,
    0x0000000e    /*   2 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2336]) + 6),
    0x0000005f    /*   4 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.2f222ba0> */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000001    /*   7 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* %sys-escape-windows-command-line */,
    0x000000ee    /*   9 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1794[2336]) + 14),
    0x00000016    /*  11 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1795[57])) /* #<compiled-code %sys-escape-windows-command-line@0x7fdc2cb79c60> */,
    0x00000014    /*  13 (RET) */,
    0x00000015    /*  14 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#%sys-escape-windows-command-line.2d707ee0> */,
    0x00000014    /*  16 (RET) */,
  },
  {   /* ScmPair d1787 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(60U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[2])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[3])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[5])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[6])},
       { SCM_OBJ(&scm__rc.d1787[7]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[4]), SCM_OBJ(&scm__rc.d1787[8])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[10])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[11])},
       { SCM_OBJ(&scm__rc.d1787[12]), SCM_NIL},
       { SCM_MAKE_INT(62U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[14])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[15])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[17])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[18])},
       { SCM_OBJ(&scm__rc.d1787[19]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[16]), SCM_OBJ(&scm__rc.d1787[20])},
       { SCM_MAKE_INT(63U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[22])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[23])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[25])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[26])},
       { SCM_OBJ(&scm__rc.d1787[27]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[24]), SCM_OBJ(&scm__rc.d1787[28])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[30])},
       { SCM_OBJ(&scm__sc.d1785[22]), SCM_NIL},
       { SCM_FALSE, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[33])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[33])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[33])},
       { SCM_OBJ(&scm__rc.d1787[36]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[35]), SCM_OBJ(&scm__rc.d1787[37])},
       { SCM_OBJ(&scm__rc.d1787[34]), SCM_OBJ(&scm__rc.d1787[38])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[39])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[40])},
       { SCM_MAKE_INT(68U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[42])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[43])},
       { SCM_OBJ(&scm__rc.d1787[44]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(154U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[48])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[49])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[51])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[52])},
       { SCM_OBJ(&scm__rc.d1787[53]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[50]), SCM_OBJ(&scm__rc.d1787[54])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(156U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[57])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[58])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[60])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[61])},
       { SCM_OBJ(&scm__rc.d1787[62]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[59]), SCM_OBJ(&scm__rc.d1787[63])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(292U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[66])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[67])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[69])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[70])},
       { SCM_OBJ(&scm__rc.d1787[71]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[68]), SCM_OBJ(&scm__rc.d1787[72])},
       { SCM_MAKE_INT(293U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[74])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[75])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[77])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[78])},
       { SCM_OBJ(&scm__rc.d1787[79]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[76]), SCM_OBJ(&scm__rc.d1787[80])},
       { SCM_MAKE_INT(296U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[82])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[83])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[85])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[86])},
       { SCM_OBJ(&scm__rc.d1787[87]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[84]), SCM_OBJ(&scm__rc.d1787[88])},
       { SCM_MAKE_INT(300U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[90])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[91])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[93])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[94])},
       { SCM_OBJ(&scm__rc.d1787[95]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[92]), SCM_OBJ(&scm__rc.d1787[96])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[98])},
       { SCM_MAKE_INT(320U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[100])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[101])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[103])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[104])},
       { SCM_OBJ(&scm__rc.d1787[105]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[102]), SCM_OBJ(&scm__rc.d1787[106])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[108])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[109])},
       { SCM_OBJ(&scm__rc.d1787[110]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(352U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[113])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[114])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[116])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[117])},
       { SCM_OBJ(&scm__rc.d1787[118]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[115]), SCM_OBJ(&scm__rc.d1787[119])},
       { SCM_MAKE_INT(353U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[121])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[122])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[124])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[125])},
       { SCM_OBJ(&scm__rc.d1787[126]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[123]), SCM_OBJ(&scm__rc.d1787[127])},
       { SCM_MAKE_INT(356U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[129])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[130])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[132])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[133])},
       { SCM_OBJ(&scm__rc.d1787[134]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[131]), SCM_OBJ(&scm__rc.d1787[135])},
       { SCM_MAKE_INT(360U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[137])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[138])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[140])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[141])},
       { SCM_OBJ(&scm__rc.d1787[142]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[139]), SCM_OBJ(&scm__rc.d1787[143])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[145])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[146])},
       { SCM_MAKE_INT(374U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[148])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[149])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[151])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[152])},
       { SCM_OBJ(&scm__rc.d1787[153]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[150]), SCM_OBJ(&scm__rc.d1787[154])},
       { SCM_MAKE_INT(377U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[156])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[157])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[159])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[160])},
       { SCM_OBJ(&scm__rc.d1787[161]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[158]), SCM_OBJ(&scm__rc.d1787[162])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(380U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[165])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[166])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[168])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[169])},
       { SCM_OBJ(&scm__rc.d1787[170]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[167]), SCM_OBJ(&scm__rc.d1787[171])},
       { SCM_MAKE_INT(383U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[173])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[174])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[176])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[177])},
       { SCM_OBJ(&scm__rc.d1787[178]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[175]), SCM_OBJ(&scm__rc.d1787[179])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(386U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[182])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[183])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[185])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[186])},
       { SCM_OBJ(&scm__rc.d1787[187]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[184]), SCM_OBJ(&scm__rc.d1787[188])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[181])},
       { SCM_MAKE_INT(388U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[191])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[192])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[194])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[195])},
       { SCM_OBJ(&scm__rc.d1787[196]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[193]), SCM_OBJ(&scm__rc.d1787[197])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[199])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[200])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[201])},
       { SCM_MAKE_INT(390U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[203])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[204])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[206])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[207])},
       { SCM_OBJ(&scm__rc.d1787[208]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[205]), SCM_OBJ(&scm__rc.d1787[209])},
       { SCM_MAKE_INT(392U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[211])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[212])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[214])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[215])},
       { SCM_OBJ(&scm__rc.d1787[216]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[213]), SCM_OBJ(&scm__rc.d1787[217])},
       { SCM_MAKE_INT(393U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[219])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[220])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[222])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[223])},
       { SCM_OBJ(&scm__rc.d1787[224]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[221]), SCM_OBJ(&scm__rc.d1787[225])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[227])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[228])},
       { SCM_OBJ(&scm__rc.d1787[229]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(396U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[232])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[233])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[235])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[236])},
       { SCM_OBJ(&scm__rc.d1787[237]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[234]), SCM_OBJ(&scm__rc.d1787[238])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[240])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[241])},
       { SCM_OBJ(&scm__rc.d1787[242]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[199])},
       { SCM_MAKE_INT(401U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[245])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[246])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[248])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[249])},
       { SCM_OBJ(&scm__rc.d1787[250]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[247]), SCM_OBJ(&scm__rc.d1787[251])},
       { SCM_MAKE_INT(403U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[253])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[254])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[256])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[257])},
       { SCM_OBJ(&scm__rc.d1787[258]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[255]), SCM_OBJ(&scm__rc.d1787[259])},
       { SCM_MAKE_INT(405U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[261])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[262])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[264])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[265])},
       { SCM_OBJ(&scm__rc.d1787[266]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[263]), SCM_OBJ(&scm__rc.d1787[267])},
       { SCM_MAKE_INT(407U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[269])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[270])},
       { SCM_OBJ(&scm__rc.d1787[271]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(415U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[275])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[276])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[278])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[279])},
       { SCM_OBJ(&scm__rc.d1787[280]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[277]), SCM_OBJ(&scm__rc.d1787[281])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[283])},
       { SCM_MAKE_INT(420U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[285])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[286])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[288])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[289])},
       { SCM_OBJ(&scm__rc.d1787[290]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[287]), SCM_OBJ(&scm__rc.d1787[291])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[293])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[294])},
       { SCM_OBJ(&scm__rc.d1787[295]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(447U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[298])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[299])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[301])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[302])},
       { SCM_OBJ(&scm__rc.d1787[303]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[300]), SCM_OBJ(&scm__rc.d1787[304])},
       { SCM_MAKE_INT(448U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[306])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[307])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[309])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[310])},
       { SCM_OBJ(&scm__rc.d1787[311]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[308]), SCM_OBJ(&scm__rc.d1787[312])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[314])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[315])},
       { SCM_OBJ(&scm__rc.d1787[316]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(460U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[319])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[320])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[322])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[323])},
       { SCM_OBJ(&scm__rc.d1787[324]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[321]), SCM_OBJ(&scm__rc.d1787[325])},
       { SCM_MAKE_INT(463U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[327])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[328])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[330])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[331])},
       { SCM_OBJ(&scm__rc.d1787[332]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[329]), SCM_OBJ(&scm__rc.d1787[333])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[335])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[336])},
       { SCM_OBJ(&scm__rc.d1787[337]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(471U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[340])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[341])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[343])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[344])},
       { SCM_OBJ(&scm__rc.d1787[345]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[342]), SCM_OBJ(&scm__rc.d1787[346])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[348])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[349])},
       { SCM_OBJ(&scm__rc.d1787[350]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(486U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[353])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[354])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[356])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[357])},
       { SCM_OBJ(&scm__rc.d1787[358]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[355]), SCM_OBJ(&scm__rc.d1787[359])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[361])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[362])},
       { SCM_OBJ(&scm__rc.d1787[363]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[365])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[366])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[367])},
       { SCM_MAKE_INT(504U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[369])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[370])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[372])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[373])},
       { SCM_OBJ(&scm__rc.d1787[374]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[371]), SCM_OBJ(&scm__rc.d1787[375])},
       { SCM_MAKE_INT(508U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[377])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[378])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[380])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[381])},
       { SCM_OBJ(&scm__rc.d1787[382]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[379]), SCM_OBJ(&scm__rc.d1787[383])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[385])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[386])},
       { SCM_OBJ(&scm__rc.d1787[387]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[389])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(512U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[392])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[393])},
       { SCM_OBJ(&scm__rc.d1787[394]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1795[4]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[361]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[398])},
       { SCM_OBJ(&scm__rc.d1787[399]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[400])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[402])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[404])},
       { SCM_OBJ(&scm__rc.d1787[405]), SCM_NIL},
       { SCM_MAKE_CHAR(61), SCM_OBJ(&scm__rc.d1787[406])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[407])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[408])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[403])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[32])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[411])},
       { SCM_OBJ(&scm__rc.d1787[412]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[410]), SCM_OBJ(&scm__rc.d1787[413])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[414])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[415])},
       { SCM_OBJ(&scm__rc.d1787[416]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[409]), SCM_OBJ(&scm__rc.d1787[417])},
       { SCM_OBJ(&scm__rc.d1787[403]), SCM_OBJ(&scm__rc.d1787[418])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[419])},
       { SCM_OBJ(&scm__rc.d1787[420]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[391]), SCM_OBJ(&scm__rc.d1787[421])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[422])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[423]), SCM_OBJ(&scm__rc.d1787[424])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[425])},
       { SCM_OBJ(&scm__rc.d1787[426]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[401]), SCM_OBJ(&scm__rc.d1787[427])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[428])},
       { SCM_MAKE_INT(511U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[430])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[431])},
       { SCM_OBJ(&scm__rc.d1787[432]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_UNDEFINED},
       { SCM_MAKE_INT(524U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[436])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[437])},
       { SCM_OBJ(&scm__rc.d1787[438]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(540U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[442])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[443])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[445])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[446])},
       { SCM_OBJ(&scm__rc.d1787[447]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[444]), SCM_OBJ(&scm__rc.d1787[448])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[450])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[451])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[452])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[453])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[454])},
       { SCM_MAKE_INT(598U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[456])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[457])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[459])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[460])},
       { SCM_OBJ(&scm__rc.d1787[461]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[458]), SCM_OBJ(&scm__rc.d1787[462])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(717U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[465])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[466])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[468])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[469])},
       { SCM_OBJ(&scm__rc.d1787[470]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[467]), SCM_OBJ(&scm__rc.d1787[471])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(737U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[474])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[475])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[477])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[478])},
       { SCM_OBJ(&scm__rc.d1787[479]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[476]), SCM_OBJ(&scm__rc.d1787[480])},
       { SCM_MAKE_INT(746U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[482])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[483])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[485])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[486])},
       { SCM_OBJ(&scm__rc.d1787[487]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[484]), SCM_OBJ(&scm__rc.d1787[488])},
       { SCM_MAKE_INT(764U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[490])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[491])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[493])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[494])},
       { SCM_OBJ(&scm__rc.d1787[495]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[492]), SCM_OBJ(&scm__rc.d1787[496])},
       { SCM_MAKE_INT(766U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[498])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[499])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[501])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[502])},
       { SCM_OBJ(&scm__rc.d1787[503]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[500]), SCM_OBJ(&scm__rc.d1787[504])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[506])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[507])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[508])},
       { SCM_MAKE_INT(787U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[510])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[511])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[513])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[514])},
       { SCM_OBJ(&scm__rc.d1787[515]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[512]), SCM_OBJ(&scm__rc.d1787[516])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[518])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[519])},
       { SCM_OBJ(&scm__rc.d1787[520]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[522])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[523])},
       { SCM_OBJ(&scm__rc.d1787[524]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[526])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[527])},
       { SCM_OBJ(&scm__rc.d1787[528]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[530])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[531])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[532])},
       { SCM_MAKE_INT(834U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[534])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[535])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[537])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[538])},
       { SCM_OBJ(&scm__rc.d1787[539]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[536]), SCM_OBJ(&scm__rc.d1787[540])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(841U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[543])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[544])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[546])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[547])},
       { SCM_OBJ(&scm__rc.d1787[548]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[545]), SCM_OBJ(&scm__rc.d1787[549])},
       { SCM_MAKE_INT(842U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[551])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[552])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[554])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[555])},
       { SCM_OBJ(&scm__rc.d1787[556]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[553]), SCM_OBJ(&scm__rc.d1787[557])},
       { SCM_MAKE_INT(843U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[559])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[560])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[562])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[563])},
       { SCM_OBJ(&scm__rc.d1787[564]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[561]), SCM_OBJ(&scm__rc.d1787[565])},
       { SCM_MAKE_INT(844U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[567])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[568])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[570])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[571])},
       { SCM_OBJ(&scm__rc.d1787[572]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[569]), SCM_OBJ(&scm__rc.d1787[573])},
       { SCM_MAKE_INT(845U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[575])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[576])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[578])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[579])},
       { SCM_OBJ(&scm__rc.d1787[580]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[577]), SCM_OBJ(&scm__rc.d1787[581])},
       { SCM_MAKE_INT(848U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[583])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[584])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[586])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[587])},
       { SCM_OBJ(&scm__rc.d1787[588]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[585]), SCM_OBJ(&scm__rc.d1787[589])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[591])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[592])},
       { SCM_OBJ(&scm__rc.d1787[593]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[595])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[596])},
       { SCM_OBJ(&scm__rc.d1787[597]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[599])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[600])},
       { SCM_OBJ(&scm__rc.d1787[601]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[603])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[604])},
       { SCM_OBJ(&scm__rc.d1787[605]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[607])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[608])},
       { SCM_OBJ(&scm__rc.d1787[609]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[611])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[612])},
       { SCM_OBJ(&scm__rc.d1787[613]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(884U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[616])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[617])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[619])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[620])},
       { SCM_OBJ(&scm__rc.d1787[621]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[618]), SCM_OBJ(&scm__rc.d1787[622])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[624])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[625])},
       { SCM_MAKE_INT(889U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[627])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[628])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[630])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[631])},
       { SCM_OBJ(&scm__rc.d1787[632]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[629]), SCM_OBJ(&scm__rc.d1787[633])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[635])},
       { SCM_MAKE_INT(898U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[637])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[638])},
       { SCM_OBJ(&scm__rc.d1787[639]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(903U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[643])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[644])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[646])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[647])},
       { SCM_OBJ(&scm__rc.d1787[648]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[645]), SCM_OBJ(&scm__rc.d1787[649])},
       { SCM_MAKE_INT(906U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[651])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[652])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[654])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[655])},
       { SCM_OBJ(&scm__rc.d1787[656]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[653]), SCM_OBJ(&scm__rc.d1787[657])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[659])},
       { SCM_MAKE_INT(911U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[661])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[662])},
       { SCM_OBJ(&scm__rc.d1787[663]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(912U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[666])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[667])},
       { SCM_OBJ(&scm__rc.d1787[668]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(940U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[672])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[673])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[675])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[676])},
       { SCM_OBJ(&scm__rc.d1787[677]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[674]), SCM_OBJ(&scm__rc.d1787[678])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(946U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[681])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[682])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[684])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[685])},
       { SCM_OBJ(&scm__rc.d1787[686]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[683]), SCM_OBJ(&scm__rc.d1787[687])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[689])},
       { SCM_MAKE_INT(950U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[691])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[692])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[694])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[695])},
       { SCM_OBJ(&scm__rc.d1787[696]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[693]), SCM_OBJ(&scm__rc.d1787[697])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[671])},
       { SCM_MAKE_INT(953U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[700])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[701])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[703])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[704])},
       { SCM_OBJ(&scm__rc.d1787[705]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[702]), SCM_OBJ(&scm__rc.d1787[706])},
       { SCM_MAKE_INT(961U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[708])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[709])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[711])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[712])},
       { SCM_OBJ(&scm__rc.d1787[713]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[710]), SCM_OBJ(&scm__rc.d1787[714])},
       { SCM_MAKE_INT(969U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[716])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[717])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[719])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[720])},
       { SCM_OBJ(&scm__rc.d1787[721]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[718]), SCM_OBJ(&scm__rc.d1787[722])},
       { SCM_MAKE_INT(977U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[724])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[725])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[727])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[728])},
       { SCM_OBJ(&scm__rc.d1787[729]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[726]), SCM_OBJ(&scm__rc.d1787[730])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[732])},
       { SCM_MAKE_INT(990U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[734])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[735])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[737])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[738])},
       { SCM_OBJ(&scm__rc.d1787[739]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[736]), SCM_OBJ(&scm__rc.d1787[740])},
       { SCM_MAKE_INT(998U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[742])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[743])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[745])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[746])},
       { SCM_OBJ(&scm__rc.d1787[747]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[744]), SCM_OBJ(&scm__rc.d1787[748])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[750])},
       { SCM_MAKE_INT(1003U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[752])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[753])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[755])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[756])},
       { SCM_OBJ(&scm__rc.d1787[757]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[754]), SCM_OBJ(&scm__rc.d1787[758])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[760])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[761])},
       { SCM_MAKE_INT(1018U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[763])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[764])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[766])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[767])},
       { SCM_OBJ(&scm__rc.d1787[768]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[765]), SCM_OBJ(&scm__rc.d1787[769])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[771])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[772])},
       { SCM_OBJ(&scm__rc.d1787[773]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[775])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[776])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[777])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[778])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[779])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[780])},
       { SCM_MAKE_INT(1067U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[782])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[783])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[785])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[786])},
       { SCM_OBJ(&scm__rc.d1787[787]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[784]), SCM_OBJ(&scm__rc.d1787[788])},
       { SCM_MAKE_INT(1076U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[790])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[791])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[793])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[794])},
       { SCM_OBJ(&scm__rc.d1787[795]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[792]), SCM_OBJ(&scm__rc.d1787[796])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[798])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[799])},
       { SCM_OBJ(&scm__rc.d1787[800]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[802])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[803])},
       { SCM_OBJ(&scm__rc.d1787[804]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[806])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[807])},
       { SCM_OBJ(&scm__rc.d1787[808]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[810])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[811])},
       { SCM_OBJ(&scm__rc.d1787[812]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[814])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[815])},
       { SCM_OBJ(&scm__rc.d1787[816]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[818])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[819])},
       { SCM_OBJ(&scm__rc.d1787[820]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[822])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[823])},
       { SCM_OBJ(&scm__rc.d1787[824]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[826])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[827])},
       { SCM_OBJ(&scm__rc.d1787[828]), SCM_NIL},
       { SCM_MAKE_INT(1184U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[830])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[831])},
       { SCM_OBJ(&scm__rc.d1787[832]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[835])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[836])},
       { SCM_OBJ(&scm__rc.d1787[837]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[839])},
       { SCM_MAKE_INT(1188U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[841])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[842])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[844])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[845])},
       { SCM_OBJ(&scm__rc.d1787[846]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[843]), SCM_OBJ(&scm__rc.d1787[847])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[849])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[850])},
       { SCM_OBJ(&scm__rc.d1787[851]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1201U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[854])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[855])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[857])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[858])},
       { SCM_OBJ(&scm__rc.d1787[859]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[856]), SCM_OBJ(&scm__rc.d1787[860])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[862])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[863])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[864])},
       { SCM_MAKE_INT(1205U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[866])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[867])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[869])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[870])},
       { SCM_OBJ(&scm__rc.d1787[871]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[868]), SCM_OBJ(&scm__rc.d1787[872])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1223U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[875])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[876])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[878])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[879])},
       { SCM_OBJ(&scm__rc.d1787[880]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[877]), SCM_OBJ(&scm__rc.d1787[881])},
       { SCM_MAKE_INT(1227U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[883])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[884])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[886])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[887])},
       { SCM_OBJ(&scm__rc.d1787[888]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[885]), SCM_OBJ(&scm__rc.d1787[889])},
       { SCM_MAKE_INT(1235U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[891])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[892])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[894])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[895])},
       { SCM_OBJ(&scm__rc.d1787[896]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[893]), SCM_OBJ(&scm__rc.d1787[897])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[750])},
       { SCM_MAKE_INT(1240U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[900])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[901])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[903])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[904])},
       { SCM_OBJ(&scm__rc.d1787[905]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[902]), SCM_OBJ(&scm__rc.d1787[906])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[908])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[909])},
       { SCM_MAKE_INT(1248U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[911])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[912])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[914])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[915])},
       { SCM_OBJ(&scm__rc.d1787[916]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[913]), SCM_OBJ(&scm__rc.d1787[917])},
       { SCM_MAKE_INT(1299U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[919])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[920])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[922])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[923])},
       { SCM_OBJ(&scm__rc.d1787[924]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[921]), SCM_OBJ(&scm__rc.d1787[925])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1314U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[928])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[929])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[931])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[932])},
       { SCM_OBJ(&scm__rc.d1787[933]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[930]), SCM_OBJ(&scm__rc.d1787[934])},
       { SCM_MAKE_INT(1318U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[936])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[937])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[939])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[940])},
       { SCM_OBJ(&scm__rc.d1787[941]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[938]), SCM_OBJ(&scm__rc.d1787[942])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[944])},
       { SCM_MAKE_INT(1322U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[946])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[947])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[949])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[950])},
       { SCM_OBJ(&scm__rc.d1787[951]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[948]), SCM_OBJ(&scm__rc.d1787[952])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[944])},
       { SCM_MAKE_INT(1328U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[955])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[956])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[958])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[959])},
       { SCM_OBJ(&scm__rc.d1787[960]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[957]), SCM_OBJ(&scm__rc.d1787[961])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[963])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[964])},
       { SCM_OBJ(&scm__rc.d1787[965]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[967])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[968])},
       { SCM_OBJ(&scm__rc.d1787[969]), SCM_NIL},
       { SCM_MAKE_INT(1458U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[971])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[972])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[974])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[975])},
       { SCM_OBJ(&scm__rc.d1787[976]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[973]), SCM_OBJ(&scm__rc.d1787[977])},
       { SCM_UNDEFINED, SCM_UNDEFINED},
       { SCM_MAKE_INT(1479U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[980])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[981])},
       { SCM_OBJ(&scm__rc.d1787[982]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[33])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[986])},
       { SCM_MAKE_INT(1488U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[988])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[989])},
       { SCM_OBJ(&scm__rc.d1787[990]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[993])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[995])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[997])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[33])},
       { SCM_OBJ(&scm__rc.d1787[999]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[998]), SCM_OBJ(&scm__rc.d1787[1000])},
       { SCM_OBJ(&scm__rc.d1787[996]), SCM_OBJ(&scm__rc.d1787[1001])},
       { SCM_OBJ(&scm__rc.d1787[994]), SCM_OBJ(&scm__rc.d1787[1002])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1003])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1004])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1005])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1006])},
       { SCM_MAKE_INT(1484U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1008])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1009])},
       { SCM_OBJ(&scm__rc.d1787[1010]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[33])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1013])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[352])},
       { SCM_MAKE_INT(1499U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1016])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1017])},
       { SCM_OBJ(&scm__rc.d1787[1018]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1021])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[352])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1023])},
       { SCM_OBJ(&scm__rc.d1787[1022]), SCM_OBJ(&scm__rc.d1787[1024])},
       { SCM_OBJ(&scm__rc.d1787[1025]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[33])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1027])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1029])},
       { SCM_MAKE_INT(1502U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1031])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1032])},
       { SCM_OBJ(&scm__rc.d1787[1033]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1036])},
       { SCM_OBJ(&scm__rc.d1787[1037]), SCM_OBJ(&scm__rc.d1787[1024])},
       { SCM_OBJ(&scm__rc.d1787[1038]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1795[26]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1795[24]), SCM_OBJ(&scm__rc.d1787[1040])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1042])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1043])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1044])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1045])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1046])},
       { SCM_MAKE_INT(1494U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1048])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1049])},
       { SCM_OBJ(&scm__rc.d1787[1050]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1053])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[1054]), SCM_OBJ(&scm__rc.d1787[1055])},
       { SCM_OBJ(&scm__rc.d1787[1056]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1795[29]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1042])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1059])},
       { SCM_MAKE_INT(1508U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1061])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1062])},
       { SCM_OBJ(&scm__rc.d1787[1063]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1066])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[352])},
       { SCM_OBJ(&scm__rc.d1787[1067]), SCM_OBJ(&scm__rc.d1787[1068])},
       { SCM_OBJ(&scm__rc.d1787[1069]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1795[32]), SCM_NIL},
       { SCM_FALSE, SCM_OBJ(&scm__rc.d1787[33])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1072])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1073])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1074])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[352])},
       { SCM_MAKE_INT(1539U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1077])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1078])},
       { SCM_OBJ(&scm__rc.d1787[1079]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[33])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1082])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1083])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[352])},
       { SCM_MAKE_INT(1538U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1086])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1087])},
       { SCM_OBJ(&scm__rc.d1787[1088]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[1084]), SCM_OBJ(&scm__rc.d1787[1091])},
       { SCM_OBJ(&scm__rc.d1787[1092]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[1084]), SCM_OBJ(&scm__rc.d1787[1094])},
       { SCM_OBJ(&scm__rc.d1787[1095]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[1084]), SCM_OBJ(&scm__rc.d1787[1097])},
       { SCM_OBJ(&scm__rc.d1787[1098]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1100])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1101])},
       { SCM_MAKE_INT(1530U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1103])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1104])},
       { SCM_OBJ(&scm__rc.d1787[1105]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1795[38]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1109])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1111])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1112])},
       { SCM_OBJ(&scm__rc.d1787[1110]), SCM_OBJ(&scm__rc.d1787[1113])},
       { SCM_OBJ(&scm__rc.d1787[1114]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1795[39]), SCM_OBJ(&scm__rc.d1787[30])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[352])},
       { SCM_MAKE_INT(1528U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1118])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1119])},
       { SCM_OBJ(&scm__rc.d1787[1120]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_FALSE, SCM_OBJ(&scm__rc.d1787[1124])},
       { SCM_MAKE_INT(0), SCM_OBJ(&scm__rc.d1787[1125])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1126])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1128])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[1129]), SCM_OBJ(&scm__rc.d1787[1130])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1132])},
       { SCM_OBJ(&scm__rc.d1787[1131]), SCM_OBJ(&scm__rc.d1787[1133])},
       { SCM_OBJ(&scm__rc.d1787[1134]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1795[42]), SCM_NIL},
       { SCM_UNDEFINED, SCM_UNDEFINED},
       { SCM_MAKE_INT(1566U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1138])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1139])},
       { SCM_OBJ(&scm__rc.d1787[1140]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1795[43]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1559U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1145])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1146])},
       { SCM_OBJ(&scm__rc.d1787[1147]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1601U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1151])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1152])},
       { SCM_OBJ(&scm__rc.d1787[1153]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1156])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[1157]), SCM_OBJ(&scm__rc.d1787[1158])},
       { SCM_OBJ(&scm__rc.d1787[1159]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1795[48]), SCM_OBJ(&scm__rc.d1787[31])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[33])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1162])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[352])},
       { SCM_MAKE_INT(1646U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1165])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1166])},
       { SCM_OBJ(&scm__rc.d1787[1167]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1170])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1172])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1173])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1174])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1175])},
       { SCM_MAKE_INT(1625U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1177])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1178])},
       { SCM_OBJ(&scm__rc.d1787[1179]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[33])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[33])},
       { SCM_OBJ(&scm__rc.d1787[1183]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[1182]), SCM_OBJ(&scm__rc.d1787[1184])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1185])},
       { SCM_MAKE_INT(1611U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1187])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1188])},
       { SCM_OBJ(&scm__rc.d1787[1189]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1746U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1192])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1193])},
       { SCM_OBJ(&scm__rc.d1787[1194]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1197])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(1816U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1200])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1201])},
       { SCM_OBJ(&scm__rc.d1787[1202]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1795[56]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1206])},
       { SCM_MAKE_INT(1803U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[1208])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[1209])},
       { SCM_OBJ(&scm__rc.d1787[1210]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
  },
  {   /* ScmObj d1786 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(4, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(4, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(4, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(4, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(9, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(4, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(4, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(312, FALSE),
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
  },
};
#if defined(GAUCHE_WINDOWS)
#undef _SC_CLK_TCK

#endif /* defined(GAUCHE_WINDOWS) */

static ScmObj libsyssys_readdir(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj pathname_scm;
  ScmString* pathname;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-readdir");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  pathname_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(pathname_scm)) Scm_Error("<string> required, but got %S", pathname_scm);
  pathname = SCM_STRING(pathname_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_ReadDirectory(pathname));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_tmpdir(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-tmpdir");
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_TmpDir());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_basename(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj pathname_scm;
  ScmString* pathname;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-basename");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  pathname_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(pathname_scm)) Scm_Error("<string> required, but got %S", pathname_scm);
  pathname = SCM_STRING(pathname_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_BaseName(pathname));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_dirname(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj pathname_scm;
  ScmString* pathname;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-dirname");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  pathname_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(pathname_scm)) Scm_Error("<string> required, but got %S", pathname_scm);
  pathname = SCM_STRING(pathname_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_DirName(pathname));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

ScmHashTable *errno_n2y;
ScmHashTable *errno_y2n;

static ScmObj libsyssys_errno_TOsymbol(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj num_scm;
  ScmSmallInt num;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-errno->symbol");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  num_scm = SCM_SUBRARGS[0];
  if (!SCM_INTP(num_scm)) Scm_Error("ScmSmallInt required, but got %S", num_scm);
  num = SCM_INT_VALUE(num_scm);
  {
{
ScmObj SCM_RESULT;

#line 155 "libsys.scm"
{SCM_RESULT=(Scm_HashTableRef(errno_n2y,SCM_MAKE_INT(num),SCM_FALSE));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_symbol_TOerrno(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj name_scm;
  ScmSymbol* name;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-symbol->errno");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  name_scm = SCM_SUBRARGS[0];
  if (!SCM_SYMBOLP(name_scm)) Scm_Error("<symbol> required, but got %S", name_scm);
  name = SCM_SYMBOL(name_scm);
  {
{
ScmObj SCM_RESULT;

#line 157 "libsys.scm"
{SCM_RESULT=(Scm_HashTableRef(errno_y2n,SCM_OBJ(name),SCM_FALSE));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_getgrgid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj gid_scm;
  int gid;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-getgrgid");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  gid_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(gid_scm)) Scm_Error("int required, but got %S", gid_scm);
  gid = Scm_GetInteger(gid_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_GetGroupById(gid));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_getgrnam(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj name_scm;
  ScmString* name;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-getgrnam");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  name_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(name_scm)) Scm_Error("<string> required, but got %S", name_scm);
  name = SCM_STRING(name_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_GetGroupByName(name));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_gid_TOgroup_name(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj gid_scm;
  int gid;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-gid->group-name");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  gid_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(gid_scm)) Scm_Error("int required, but got %S", gid_scm);
  gid = Scm_GetInteger(gid_scm);
  {
{
ScmObj SCM_RESULT;

#line 297 "libsys.scm"
{struct group* g=getgrgid(gid);
if ((g)==(NULL)){Scm_SigCheck(Scm_VM());{SCM_RESULT=(SCM_FALSE);goto SCM_STUB_RETURN;}} else {
{SCM_RESULT=(SCM_MAKE_STR_COPYING((g)->gr_name));goto SCM_STUB_RETURN;}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_group_name_TOgid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj name_scm;
  const char* name;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-group-name->gid");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  name_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(name_scm)) Scm_Error("const char* required, but got %S", name_scm);
  name = SCM_STRING_CONST_CSTRING(name_scm);
  {
{
ScmObj SCM_RESULT;

#line 301 "libsys.scm"
{struct group* g=getgrnam(name);
if ((g)==(NULL)){Scm_SigCheck(Scm_VM());{SCM_RESULT=(SCM_FALSE);goto SCM_STUB_RETURN;}} else {
{SCM_RESULT=(Scm_MakeInteger((g)->gr_gid));goto SCM_STUB_RETURN;}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_setlocale(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj category_scm;
  ScmSmallInt category;
  ScmObj locale_scm;
  const char* locale;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-setlocale");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  category_scm = SCM_SUBRARGS[0];
  if (!SCM_INTP(category_scm)) Scm_Error("ScmSmallInt required, but got %S", category_scm);
  category = SCM_INT_VALUE(category_scm);
  locale_scm = SCM_SUBRARGS[1];
  if (!SCM_MAYBE_P(SCM_STRINGP, locale_scm)) Scm_Error("const char* or #f required, but got %S", locale_scm);
  locale = SCM_MAYBE(SCM_STRING_CONST_CSTRING, locale_scm);
  {
{
const char* SCM_RESULT;
{SCM_RESULT=(setlocale(category,locale));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_MAYBE(SCM_MAKE_STR_COPYING, SCM_RESULT));
}
  }
}


static ScmObj libsyssys_localeconv(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-localeconv");
  {
{
ScmObj SCM_RESULT;

#line 324 "libsys.scm"
{struct lconv* lc=localeconv();
{SCM_RESULT=(Scm_Cons(Scm_Cons(scm__rc.d1786[283],SCM_MAKE_STR_COPYING((lc)->decimal_point)),Scm_Cons(
Scm_Cons(scm__rc.d1786[284],SCM_MAKE_STR_COPYING((lc)->thousands_sep)),Scm_Cons(
Scm_Cons(scm__rc.d1786[285],SCM_MAKE_STR_COPYING((lc)->grouping)),Scm_Cons(
Scm_Cons(scm__rc.d1786[286],SCM_MAKE_STR_COPYING((lc)->int_curr_symbol)),Scm_Cons(
Scm_Cons(scm__rc.d1786[287],SCM_MAKE_STR_COPYING((lc)->currency_symbol)),Scm_Cons(
Scm_Cons(scm__rc.d1786[288],SCM_MAKE_STR_COPYING((lc)->mon_decimal_point)),Scm_Cons(
Scm_Cons(scm__rc.d1786[289],SCM_MAKE_STR_COPYING((lc)->mon_thousands_sep)),Scm_Cons(
Scm_Cons(scm__rc.d1786[290],SCM_MAKE_STR_COPYING((lc)->mon_grouping)),Scm_Cons(
Scm_Cons(scm__rc.d1786[291],SCM_MAKE_STR_COPYING((lc)->positive_sign)),Scm_Cons(
Scm_Cons(scm__rc.d1786[292],SCM_MAKE_STR_COPYING((lc)->negative_sign)),Scm_Cons(
Scm_Cons(scm__rc.d1786[293],SCM_MAKE_INT((lc)->int_frac_digits)),Scm_Cons(
Scm_Cons(scm__rc.d1786[294],SCM_MAKE_INT((lc)->frac_digits)),Scm_Cons(
Scm_Cons(scm__rc.d1786[295],SCM_MAKE_BOOL((lc)->p_cs_precedes)),Scm_Cons(
Scm_Cons(scm__rc.d1786[296],SCM_MAKE_BOOL((lc)->p_sep_by_space)),Scm_Cons(
Scm_Cons(scm__rc.d1786[297],SCM_MAKE_BOOL((lc)->n_cs_precedes)),Scm_Cons(
Scm_Cons(scm__rc.d1786[298],SCM_MAKE_BOOL((lc)->n_sep_by_space)),Scm_Cons(
Scm_Cons(scm__rc.d1786[299],SCM_MAKE_INT((lc)->p_sign_posn)),Scm_Cons(
Scm_Cons(scm__rc.d1786[300],SCM_MAKE_INT((lc)->n_sign_posn)),SCM_NIL)))))))))))))))))));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_getpwuid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj uid_scm;
  int uid;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-getpwuid");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  uid_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(uid_scm)) Scm_Error("int required, but got %S", uid_scm);
  uid = Scm_GetInteger(uid_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_GetPasswdById(uid));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_getpwnam(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj name_scm;
  ScmString* name;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-getpwnam");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  name_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(name_scm)) Scm_Error("<string> required, but got %S", name_scm);
  name = SCM_STRING(name_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_GetPasswdByName(name));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_uid_TOuser_name(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj uid_scm;
  int uid;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-uid->user-name");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  uid_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(uid_scm)) Scm_Error("int required, but got %S", uid_scm);
  uid = Scm_GetInteger(uid_scm);
  {
{
ScmObj SCM_RESULT;

#line 357 "libsys.scm"
{struct passwd* p=getpwuid(uid);
if ((p)==(NULL)){Scm_SigCheck(Scm_VM());{SCM_RESULT=(SCM_FALSE);goto SCM_STUB_RETURN;}} else {
{SCM_RESULT=(SCM_MAKE_STR_COPYING((p)->pw_name));goto SCM_STUB_RETURN;}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_user_name_TOuid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj name_scm;
  const char* name;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-user-name->uid");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  name_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(name_scm)) Scm_Error("const char* required, but got %S", name_scm);
  name = SCM_STRING_CONST_CSTRING(name_scm);
  {
{
ScmObj SCM_RESULT;

#line 361 "libsys.scm"
{struct passwd* p=getpwnam(name);
if ((p)==(NULL)){Scm_SigCheck(Scm_VM());{SCM_RESULT=(SCM_FALSE);goto SCM_STUB_RETURN;}} else {
{SCM_RESULT=(Scm_MakeInteger((p)->pw_uid));goto SCM_STUB_RETURN;}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_sigset_addX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj set_scm;
  ScmSysSigset* set;
  ScmObj sigs_scm;
  ScmObj sigs;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-sigset-add!");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  set_scm = SCM_SUBRARGS[0];
  if (!SCM_SYS_SIGSET_P(set_scm)) Scm_Error("<sys-sigset> required, but got %S", set_scm);
  set = SCM_SYS_SIGSET(set_scm);
  sigs_scm = SCM_SUBRARGS[SCM_ARGCNT-1];
  if (!SCM_LISTP(sigs_scm)) Scm_Error("list required, but got %S", sigs_scm);
  sigs = (sigs_scm);
  {
{
ScmObj SCM_RESULT;

#line 375 "libsys.scm"
{SCM_RESULT=(Scm_SysSigsetOp(set,sigs,FALSE));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_sigset_deleteX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj set_scm;
  ScmSysSigset* set;
  ScmObj sigs_scm;
  ScmObj sigs;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-sigset-delete!");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  set_scm = SCM_SUBRARGS[0];
  if (!SCM_SYS_SIGSET_P(set_scm)) Scm_Error("<sys-sigset> required, but got %S", set_scm);
  set = SCM_SYS_SIGSET(set_scm);
  sigs_scm = SCM_SUBRARGS[SCM_ARGCNT-1];
  if (!SCM_LISTP(sigs_scm)) Scm_Error("list required, but got %S", sigs_scm);
  sigs = (sigs_scm);
  {
{
ScmObj SCM_RESULT;

#line 378 "libsys.scm"
{SCM_RESULT=(Scm_SysSigsetOp(set,sigs,TRUE));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_sigset_fillX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj set_scm;
  ScmSysSigset* set;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-sigset-fill!");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  set_scm = SCM_SUBRARGS[0];
  if (!SCM_SYS_SIGSET_P(set_scm)) Scm_Error("<sys-sigset> required, but got %S", set_scm);
  set = SCM_SYS_SIGSET(set_scm);
  {
{
ScmObj SCM_RESULT;

#line 381 "libsys.scm"
{SCM_RESULT=(Scm_SysSigsetFill(set,FALSE));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_sigset_emptyX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj set_scm;
  ScmSysSigset* set;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-sigset-empty!");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  set_scm = SCM_SUBRARGS[0];
  if (!SCM_SYS_SIGSET_P(set_scm)) Scm_Error("<sys-sigset> required, but got %S", set_scm);
  set = SCM_SYS_SIGSET(set_scm);
  {
{
ScmObj SCM_RESULT;

#line 384 "libsys.scm"
{SCM_RESULT=(Scm_SysSigsetFill(set,TRUE));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_signal_name(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sig_scm;
  ScmSmallInt sig;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-signal-name");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sig_scm = SCM_SUBRARGS[0];
  if (!SCM_INTP(sig_scm)) Scm_Error("ScmSmallInt required, but got %S", sig_scm);
  sig = SCM_INT_VALUE(sig_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SignalName(sig));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_kill(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj process_scm;
  ScmObj process;
  ScmObj sig_scm;
  ScmSmallInt sig;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-kill");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  process_scm = SCM_SUBRARGS[0];
  if (!(process_scm)) Scm_Error("scheme object required, but got %S", process_scm);
  process = (process_scm);
  sig_scm = SCM_SUBRARGS[1];
  if (!SCM_INTP(sig_scm)) Scm_Error("ScmSmallInt required, but got %S", sig_scm);
  sig = SCM_INT_VALUE(sig_scm);
  {
Scm_SysKill(process,sig);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsysset_signal_handlerX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sig_scm;
  ScmObj sig;
  ScmObj proc_scm;
  ScmObj proc;
  ScmObj mask_scm;
  ScmSysSigset* mask;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("set-signal-handler!");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sig_scm = SCM_SUBRARGS[0];
  if (!(sig_scm)) Scm_Error("scheme object required, but got %S", sig_scm);
  sig = (sig_scm);
  proc_scm = SCM_SUBRARGS[1];
  if (!(proc_scm)) Scm_Error("scheme object required, but got %S", proc_scm);
  proc = (proc_scm);
  if (SCM_ARGCNT > 2+1) {
    mask_scm = SCM_SUBRARGS[2];
  } else {
    mask_scm = SCM_FALSE;
  }
  if (!SCM_MAYBE_P(SCM_SYS_SIGSET_P, mask_scm)) Scm_Error("<sys-sigset> or #f required, but got %S", mask_scm);
  mask = SCM_MAYBE(SCM_SYS_SIGSET, mask_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SetSignalHandler(sig,proc,mask));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsysget_signal_handler(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sig_scm;
  ScmSmallInt sig;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("get-signal-handler");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sig_scm = SCM_SUBRARGS[0];
  if (!SCM_INTP(sig_scm)) Scm_Error("ScmSmallInt required, but got %S", sig_scm);
  sig = SCM_INT_VALUE(sig_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_GetSignalHandler(sig));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsysget_signal_handler_mask(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj sig_scm;
  ScmSmallInt sig;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("get-signal-handler-mask");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  sig_scm = SCM_SUBRARGS[0];
  if (!SCM_INTP(sig_scm)) Scm_Error("ScmSmallInt required, but got %S", sig_scm);
  sig = SCM_INT_VALUE(sig_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_GetSignalHandlerMask(sig));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsysget_signal_handlers(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("get-signal-handlers");
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_GetSignalHandlers());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsysset_signal_pending_limit(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj limit_scm;
  ScmSmallInt limit;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("set-signal-pending-limit");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  limit_scm = SCM_SUBRARGS[0];
  if (!SCM_INTP(limit_scm)) Scm_Error("ScmSmallInt required, but got %S", limit_scm);
  limit = SCM_INT_VALUE(limit_scm);
  {
Scm_SetSignalPendingLimit(limit);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsysget_signal_pending_limit(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("get-signal-pending-limit");
  {
{
int SCM_RESULT;
{SCM_RESULT=(Scm_GetSignalPendingLimit());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_sigmask(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj how_scm;
  ScmSmallInt how;
  ScmObj mask_scm;
  ScmSysSigset* mask;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-sigmask");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  how_scm = SCM_SUBRARGS[0];
  if (!SCM_INTP(how_scm)) Scm_Error("ScmSmallInt required, but got %S", how_scm);
  how = SCM_INT_VALUE(how_scm);
  mask_scm = SCM_SUBRARGS[1];
  if (!SCM_MAYBE_P(SCM_SYS_SIGSET_P, mask_scm)) Scm_Error("<sys-sigset> or #f required, but got %S", mask_scm);
  mask = SCM_MAYBE(SCM_SYS_SIGSET, mask_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SysSigmask(how,mask));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_sigsuspend(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj mask_scm;
  ScmSysSigset* mask;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-sigsuspend");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  mask_scm = SCM_SUBRARGS[0];
  if (!SCM_SYS_SIGSET_P(mask_scm)) Scm_Error("<sys-sigset> required, but got %S", mask_scm);
  mask = SCM_SYS_SIGSET(mask_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SigSuspend(mask));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_sigwait(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj mask_scm;
  ScmSysSigset* mask;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-sigwait");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  mask_scm = SCM_SUBRARGS[0];
  if (!SCM_SYS_SIGSET_P(mask_scm)) Scm_Error("<sys-sigset> required, but got %S", mask_scm);
  mask = SCM_SYS_SIGSET(mask_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(Scm_SigWait(mask));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_remove(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj filename_scm;
  const char* filename;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-remove");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  filename_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(filename_scm)) Scm_Error("const char* required, but got %S", filename_scm);
  filename = SCM_STRING_CONST_CSTRING(filename_scm);
  {

#line 416 "libsys.scm"
{int r;
SCM_SYSCALL(r,remove(filename));
if ((r)<(0)){{Scm_SysError("remove failed on %s",filename);}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsyssys_rename(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj oldname_scm;
  const char* oldname;
  ScmObj newname_scm;
  const char* newname;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-rename");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  oldname_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(oldname_scm)) Scm_Error("const char* required, but got %S", oldname_scm);
  oldname = SCM_STRING_CONST_CSTRING(oldname_scm);
  newname_scm = SCM_SUBRARGS[1];
  if (!SCM_STRINGP(newname_scm)) Scm_Error("const char* required, but got %S", newname_scm);
  newname = SCM_STRING_CONST_CSTRING(newname_scm);
  {

#line 423 "libsys.scm"
{int r;

#if defined(GAUCHE_WINDOWS)

#line 429 "libsys.scm"
chmod(newname,438);

unlink(newname);
#endif /* defined(GAUCHE_WINDOWS) */

#line 431 "libsys.scm"
SCM_SYSCALL(r,rename(oldname,newname));
if ((r)<(0)){{Scm_SysError("renaming %s to %s failed",oldname,newname);}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsyssys_tmpnam(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-tmpnam");
  {
{
ScmObj SCM_RESULT;

#line 437 "libsys.scm"

#if HAVE_MKSTEMP

#line 438 "libsys.scm"
{char nam[]="/tmp/fileXXXXXX";int fd;
SCM_SYSCALL(fd,mkstemp(nam));
if ((fd)<(0)){{Scm_SysError("mkstemp failed");}}
close(fd);
unlink(nam);
{SCM_RESULT=(SCM_MAKE_STR_COPYING(nam));goto SCM_STUB_RETURN;}}
#else /* !HAVE_MKSTEMP */

#line 444 "libsys.scm"
{char* s=tmpnam(NULL);
{SCM_RESULT=(SCM_MAKE_STR_COPYING(s));goto SCM_STUB_RETURN;}}
#endif /* HAVE_MKSTEMP */

goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_mkstemp(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj template_scm;
  ScmString* template;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-mkstemp");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  template_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(template_scm)) Scm_Error("<string> required, but got %S", template_scm);
  template = SCM_STRING(template_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SysMkstemp(template));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_mkdtemp(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj template_scm;
  ScmString* template;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-mkdtemp");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  template_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(template_scm)) Scm_Error("<string> required, but got %S", template_scm);
  template = SCM_STRING(template_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SysMkdtemp(template));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_ctermid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-ctermid");
  {
{
ScmObj SCM_RESULT;

#line 452 "libsys.scm"

#if defined(GAUCHE_WINDOWS)

#line 453 "libsys.scm"
{SCM_RESULT=(SCM_OBJ(&scm__sc.d1785[274]));goto SCM_STUB_RETURN;}
#else /* !defined(GAUCHE_WINDOWS) */

#line 454 "libsys.scm"
{char buf[(L_ctermid)+(1)];
{SCM_RESULT=(SCM_MAKE_STR_COPYING(ctermid(buf)));goto SCM_STUB_RETURN;}}
#endif /* defined(GAUCHE_WINDOWS) */

goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_exit(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj code_scm;
  ScmObj code;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-exit");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  code_scm = SCM_SUBRARGS[0];
  if (!(code_scm)) Scm_Error("scheme object required, but got %S", code_scm);
  code = (code_scm);
  {

#line 461 "libsys.scm"
_exit(Scm_ObjToExitCode(code));
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsyssys_getenv(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj name_scm;
  const char* name;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-getenv");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  name_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(name_scm)) Scm_Error("const char* required, but got %S", name_scm);
  name = SCM_STRING_CONST_CSTRING(name_scm);
  {
{
const char* SCM_RESULT;
{SCM_RESULT=(Scm_GetEnv(name));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_MAYBE(SCM_MAKE_STR_COPYING, SCM_RESULT));
}
  }
}


static ScmObj libsyssys_abort(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-abort");
  {
abort();
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsyssys_system(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj command_scm;
  const char* command;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-system");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  command_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(command_scm)) Scm_Error("const char* required, but got %S", command_scm);
  command = SCM_STRING_CONST_CSTRING(command_scm);
  {
{
int SCM_RESULT;

#line 472 "libsys.scm"
if (((command)[0])==(0)){
{SCM_RESULT=(0);goto SCM_STUB_RETURN;}} else {
SCM_SYSCALL(SCM_RESULT,system(command));}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_random(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-random");
  {
{
long SCM_RESULT;

#line 477 "libsys.scm"

#if 0 /*dummy*/
#elif (defined(HAVE_RANDOM))&&(defined(HAVE_SRANDOM))

#line 478 "libsys.scm"
{SCM_RESULT=(random());goto SCM_STUB_RETURN;}
#elif (defined(LRAND48))&&(defined(SRAND48))

#line 480 "libsys.scm"
{SCM_RESULT=(lrand48());goto SCM_STUB_RETURN;}
#else

#line 484 "libsys.scm"
{SCM_RESULT=(rand());goto SCM_STUB_RETURN;}
#endif

goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_srandom(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj seed_scm;
  ScmObj seed;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-srandom");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  seed_scm = SCM_SUBRARGS[0];
  if (!(seed_scm)) Scm_Error("scheme object required, but got %S", seed_scm);
  seed = (seed_scm);
  {

#line 487 "libsys.scm"
if (!(SCM_EXACTP(seed))){{Scm_Error("exact integer required: %S",seed);}}

#line 488 "libsys.scm"

#if 0 /*dummy*/
#elif (defined(HAVE_RANDOM))&&(defined(HAVE_SRANDOM))

#line 489 "libsys.scm"
srandom(Scm_GetUInteger(seed));
#elif (defined(LRAND48))&&(defined(SRAND48))

#line 491 "libsys.scm"
srand48(Scm_GetUInteger(seed));
#else

#line 495 "libsys.scm"
srand(Scm_GetUInteger(seed));
#endif

goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsyssys_environ(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-environ");
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_Environ());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_setenv(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj name_scm;
  const char* name;
  ScmObj value_scm;
  const char* value;
  ScmObj overwrite_scm;
  int overwrite;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("sys-setenv");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  name_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(name_scm)) Scm_Error("const char* required, but got %S", name_scm);
  name = SCM_STRING_CONST_CSTRING(name_scm);
  value_scm = SCM_SUBRARGS[1];
  if (!SCM_STRINGP(value_scm)) Scm_Error("const char* required, but got %S", value_scm);
  value = SCM_STRING_CONST_CSTRING(value_scm);
  if (SCM_ARGCNT > 2+1) {
    overwrite_scm = SCM_SUBRARGS[2];
  } else {
    overwrite_scm = SCM_FALSE;
  }
  if (!SCM_BOOLP(overwrite_scm)) Scm_Error("boolean required, but got %S", overwrite_scm);
  overwrite = SCM_BOOL_VALUE(overwrite_scm);
  {
Scm_SetEnv(name,value,overwrite);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsyssys_unsetenv(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj name_scm;
  const char* name;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-unsetenv");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  name_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(name_scm)) Scm_Error("const char* required, but got %S", name_scm);
  name = SCM_STRING_CONST_CSTRING(name_scm);
  {
Scm_UnsetEnv(name);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsyssys_clearenv(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-clearenv");
  {
Scm_ClearEnv();
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsyssys_strerror(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj errno__scm;
  int errno_;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-strerror");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  errno__scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(errno__scm)) Scm_Error("int required, but got %S", errno__scm);
  errno_ = Scm_GetInteger(errno__scm);
  {
{
const char* SCM_RESULT;
{SCM_RESULT=(strerror(errno_));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_STR_COPYING(SCM_RESULT));
}
  }
}

#if HAVE_STRSIGNAL

static ScmObj libsyssys_strsignal(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj signum_scm;
  int signum;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-strsignal");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  signum_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(signum_scm)) Scm_Error("int required, but got %S", signum_scm);
  signum = Scm_GetInteger(signum_scm);
  {
{
const char* SCM_RESULT;

#line 553 "libsys.scm"
{SCM_RESULT=(strsignal(signum));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_MAYBE(SCM_MAKE_STR_COPYING, SCM_RESULT));
}
  }
}

#endif /* HAVE_STRSIGNAL */
#if !(HAVE_STRSIGNAL)

static ScmObj libsyssys_strsignal(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-strsignal");
  {
{
const char* SCM_RESULT;

#line 555 "libsys.scm"
{SCM_RESULT=(NULL);goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_MAYBE(SCM_MAKE_STR_COPYING, SCM_RESULT));
}
  }
}

#endif /* !(HAVE_STRSIGNAL) */
#if defined(HAVE_GETLOADAVG)

static ScmObj libsyssys_getloadavg(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj nsamples_scm;
  int nsamples;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-getloadavg");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    nsamples_scm = SCM_SUBRARGS[0];
  } else {
    nsamples_scm = SCM_MAKE_INT(3U);
  }
  if (!SCM_INTEGERP(nsamples_scm)) Scm_Error("int required, but got %S", nsamples_scm);
  nsamples = Scm_GetInteger(nsamples_scm);
  {
{
ScmObj SCM_RESULT;

#line 563 "libsys.scm"
{double samples[3];
if (((nsamples)<=(0))||((nsamples)>(3))){{
Scm_Error("sys-getloadavg: argument out of range: %d",nsamples);}};int count=
getloadavg(samples,nsamples);
if ((count)<(0)){
{SCM_RESULT=(SCM_FALSE);goto SCM_STUB_RETURN;}} else {
{ScmObj h=SCM_NIL;ScmObj t=SCM_NIL;
{int i=0;int cise__1810=count;for (; (i)<(cise__1810); (i)++){
{ScmObj n=Scm_MakeFlonum((samples)[i]);
SCM_APPEND1(h,t,n);}}}
{SCM_RESULT=(h);goto SCM_STUB_RETURN;}}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#endif /* defined(HAVE_GETLOADAVG) */
#if !(defined(HAVE_GETLOADAVG))

static ScmObj libsyssys_getloadavg(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-getloadavg");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  {
{
ScmObj SCM_RESULT;

#line 575 "libsys.scm"
{
Scm_Error("sys-getloadavg isn't supported on this platform");
{SCM_RESULT=(SCM_UNDEFINED);goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#endif /* !(defined(HAVE_GETLOADAVG)) */
static void Scm_MemoryRegionClass_PRINT(ScmObj obj, ScmPort *port, ScmWriteContext *ctx SCM_UNUSED)
{

#line 591 "libsys.scm"
{ScmMemoryRegion* m=SCM_MEMORY_REGION(obj);
Scm_Printf(port,"#<memory-region %p[%lx] (%s%s%s)>",
(m)->ptr,(m)->size,
((((m)->prot)&(PROT_READ))?("r"):("")),
((((m)->prot)&(PROT_WRITE))?("w"):("")),
((((m)->prot)&(PROT_EXEC))?("x"):("")));}
}

SCM_DEFINE_BUILTIN_CLASS(Scm_MemoryRegionClass, Scm_MemoryRegionClass_PRINT, NULL, NULL, NULL, SCM_CLASS_DEFAULT_CPL);

static ScmObj Scm_MemoryRegionClass_address_GET(ScmObj OBJARG)
{
  ScmMemoryRegion* obj = SCM_MEMORY_REGION(OBJARG);
  return SCM_OBJ_SAFE(Scm_MakeIntegerU((uintptr_t)obj->ptr));
}

static ScmObj Scm_MemoryRegionClass_size_GET(ScmObj OBJARG)
{
  ScmMemoryRegion* obj = SCM_MEMORY_REGION(OBJARG);
  return Scm_SizeToInteger(obj->size);
}

static ScmObj Scm_MemoryRegionClass_protection_GET(ScmObj OBJARG)
{
  ScmMemoryRegion* obj = SCM_MEMORY_REGION(OBJARG);
  return Scm_MakeInteger(obj->prot);
}

static ScmObj Scm_MemoryRegionClass_flags_GET(ScmObj OBJARG)
{
  ScmMemoryRegion* obj = SCM_MEMORY_REGION(OBJARG);
  return Scm_MakeInteger(obj->flags);
}

static ScmClassStaticSlotSpec Scm_MemoryRegionClass__SLOTS[] = {
  SCM_CLASS_SLOT_SPEC("address", Scm_MemoryRegionClass_address_GET, NULL),
  SCM_CLASS_SLOT_SPEC("size", Scm_MemoryRegionClass_size_GET, NULL),
  SCM_CLASS_SLOT_SPEC("protection", Scm_MemoryRegionClass_protection_GET, NULL),
  SCM_CLASS_SLOT_SPEC("flags", Scm_MemoryRegionClass_flags_GET, NULL),
  SCM_CLASS_SLOT_SPEC_END()
};


static ScmObj libsyssys_mmap(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj maybe_port_scm;
  ScmObj maybe_port;
  ScmObj prot_scm;
  int prot;
  ScmObj flags_scm;
  int flags;
  ScmObj size_scm;
  size_t size;
  ScmObj off_scm;
  off_t off;
  ScmObj SCM_SUBRARGS[5];
  SCM_ENTER_SUBR("sys-mmap");
  if (SCM_ARGCNT >= 6
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 5 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<5; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  maybe_port_scm = SCM_SUBRARGS[0];
  if (!(maybe_port_scm)) Scm_Error("scheme object required, but got %S", maybe_port_scm);
  maybe_port = (maybe_port_scm);
  prot_scm = SCM_SUBRARGS[1];
  if (!SCM_INTEGERP(prot_scm)) Scm_Error("int required, but got %S", prot_scm);
  prot = Scm_GetInteger(prot_scm);
  flags_scm = SCM_SUBRARGS[2];
  if (!SCM_INTEGERP(flags_scm)) Scm_Error("int required, but got %S", flags_scm);
  flags = Scm_GetInteger(flags_scm);
  size_scm = SCM_SUBRARGS[3];
  if (!Scm_IntegerFitsSizeP(size_scm)) Scm_Error("size_t required, but got %S", size_scm);
  size = Scm_IntegerToSize(size_scm);
  if (SCM_ARGCNT > 4+1) {
    off_scm = SCM_SUBRARGS[4];
  } else {
    off_scm = SCM_MAKE_INT(0);
  }
  if (!Scm_IntegerFitsOffsetP(off_scm)) Scm_Error("off_t required, but got %S", off_scm);
  off = Scm_IntegerToOffset(off_scm);
  {
{
ScmObj SCM_RESULT;

#line 600 "libsys.scm"
{int fd=-1;
if (SCM_PORTP(maybe_port)){
fd=(Scm_PortFileNo(SCM_PORT(maybe_port)));
if ((fd)<(0)){{
Scm_Error("non-file-backed port can't be used to mmap: %S",maybe_port);}}}else if(
#line 606 "libsys.scm"
SCM_FALSEP(maybe_port)){} else {
SCM_TYPE_ERROR(maybe_port,"port or #f");}
{SCM_RESULT=(Scm_SysMmap(NULL,fd,size,off,prot,flags));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#if defined(HAVE_SYS_RESOURCE_H)
#if (SIZEOF_RLIM_T)==(4)
#define MAKERLIMIT(val) (Scm_MakeIntegerU(val))

#endif /* (SIZEOF_RLIM_T)==(4) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if (SIZEOF_RLIM_T)==(4)
#define GETRLIMIT(obj) (Scm_GetIntegerU(obj))

#endif /* (SIZEOF_RLIM_T)==(4) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if !((SIZEOF_RLIM_T)==(4))
#if (SIZEOF_RLIM_T)==(8)
#define MAKERLIMIT(val) (Scm_MakeIntegerU64(val))

#endif /* (SIZEOF_RLIM_T)==(8) */
#endif /* !((SIZEOF_RLIM_T)==(4)) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if !((SIZEOF_RLIM_T)==(4))
#if (SIZEOF_RLIM_T)==(8)
#define GETRLIMIT(obj) (Scm_GetIntegerU64(obj))

#endif /* (SIZEOF_RLIM_T)==(8) */
#endif /* !((SIZEOF_RLIM_T)==(4)) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if !((SIZEOF_RLIM_T)==(4))
#if !((SIZEOF_RLIM_T)==(8))
#error "rlim_t must be 32bit or 64bit"
#endif /* !((SIZEOF_RLIM_T)==(8)) */
#endif /* !((SIZEOF_RLIM_T)==(4)) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)

static ScmObj libsyssys_getrlimit(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj rsrc_scm;
  int rsrc;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-getrlimit");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  rsrc_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(rsrc_scm)) Scm_Error("int required, but got %S", rsrc_scm);
  rsrc = Scm_GetInteger(rsrc_scm);
  {
ScmObj SCM_RESULT0;
ScmObj SCM_RESULT1;
{

#line 635 "libsys.scm"
{struct rlimit limit;int ret;
SCM_SYSCALL(ret,getrlimit(rsrc,&(limit)));
if ((ret)<(0)){{Scm_SysError("getrlimit failed");}}
{SCM_RESULT0=(MAKERLIMIT((limit).rlim_cur)),SCM_RESULT1=(
MAKERLIMIT((limit).rlim_max));goto SCM_STUB_RETURN;}}
}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_Values2(SCM_OBJ_SAFE(SCM_RESULT0),SCM_OBJ_SAFE(SCM_RESULT1)));
  }
}

#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)

static ScmObj libsyssys_setrlimit(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj rsrc_scm;
  int rsrc;
  ScmObj cur_scm;
  ScmObj cur;
  ScmObj max_scm;
  ScmObj max;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("sys-setrlimit");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  rsrc_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(rsrc_scm)) Scm_Error("int required, but got %S", rsrc_scm);
  rsrc = Scm_GetInteger(rsrc_scm);
  cur_scm = SCM_SUBRARGS[1];
  if (!(cur_scm)) Scm_Error("scheme object required, but got %S", cur_scm);
  cur = (cur_scm);
  if (SCM_ARGCNT > 2+1) {
    max_scm = SCM_SUBRARGS[2];
  } else {
    max_scm = SCM_FALSE;
  }
  if (!(max_scm)) Scm_Error("scheme object required, but got %S", max_scm);
  max = (max_scm);
  {

#line 642 "libsys.scm"
{struct rlimit limit;int ret;
if ((SCM_FALSEP(cur))||(SCM_FALSEP(max))){{
SCM_SYSCALL(ret,getrlimit(rsrc,&(limit)));
if ((ret)<(0)){{Scm_SysError("getrlimit in sys-setrlimit failed");}}}}
if (SCM_INTEGERP(cur)){(limit).rlim_cur=(GETRLIMIT(cur));}else if(
!(SCM_FALSEP(cur))){
SCM_TYPE_ERROR(cur,"non-negative integer or #f");}
if (SCM_INTEGERP(max)){(limit).rlim_max=(GETRLIMIT(max));}else if(
!(SCM_FALSEP(max))){
SCM_TYPE_ERROR(max,"non-negative integer or #f");}
SCM_SYSCALL(ret,setrlimit(rsrc,&(limit)));
if ((ret)<(0)){{Scm_SysError("setrlimit failed");}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}

#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(GAUCHE_WINDOWS)
static const char* check_trailing_separator(const char* path){{
#line 696 "libsys.scm"
{int size=strlen(path);const char* ends=
(path)+(size);const char* lastchar;
#line 699 "libsys.scm"
if ((size)==(0)){{return (path);}}
SCM_CHAR_BACKWARD(ends,path,lastchar);
if (!(((lastchar)>=(path))&&((lastchar)<(ends)))){{
Scm_SysError("invalid pathname: %s",path);}}
if (((!((lastchar)==(path)))&&(
!(((lastchar)==((path)+(2)))&&(
((path)[1])==('\x3a')))))&&(
((*(lastchar))==('\\'))||(
(*(lastchar))==('\x2f')))){{
{char * pcopy=SCM_NEW_ATOMIC_ARRAY(char ,size);
memcpy(pcopy,path,(size)-(1));
(pcopy)[(size)-(1)]=(0);
path=(((const char * )(pcopy)));}}}
return (path);}}}
#endif /* defined(GAUCHE_WINDOWS) */
#if !(defined(GAUCHE_WINDOWS))
static const char* check_trailing_separator(const char* path){{
#line 715 "libsys.scm"
return (path);}}
#endif /* !(defined(GAUCHE_WINDOWS)) */

static ScmObj libsyssys_stat(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj path_scm;
  const char* path;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-stat");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  path_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(path_scm)) Scm_Error("const char* required, but got %S", path_scm);
  path = SCM_STRING_CONST_CSTRING(path_scm);
  {
{
ScmSysStat* SCM_RESULT;

#line 718 "libsys.scm"
{ScmSysStat* s=SCM_SYS_STAT(Scm_MakeSysStat());int r;const char* p=check_trailing_separator(path);SCM_SYSCALL(r,stat(p,SCM_SYS_STAT_STAT(s)));if ((r)<(0)){{Scm_SysError("%s failed for %s","stat",p);}}{SCM_RESULT=(s);goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#if !(defined(GAUCHE_WINDOWS))

static ScmObj libsyssys_lstat(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj path_scm;
  const char* path;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-lstat");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  path_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(path_scm)) Scm_Error("const char* required, but got %S", path_scm);
  path = SCM_STRING_CONST_CSTRING(path_scm);
  {
{
ScmSysStat* SCM_RESULT;

#line 727 "libsys.scm"
{ScmSysStat* s=SCM_SYS_STAT(Scm_MakeSysStat());int r;const char* p=check_trailing_separator(path);SCM_SYSCALL(r,lstat(p,SCM_SYS_STAT_STAT(s)));if ((r)<(0)){{Scm_SysError("%s failed for %s","lstat",p);}}{SCM_RESULT=(s);goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(!(defined(GAUCHE_WINDOWS)))

static ScmObj libsyssys_lstat(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj path_scm;
  const char* path;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-lstat");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  path_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(path_scm)) Scm_Error("const char* required, but got %S", path_scm);
  path = SCM_STRING_CONST_CSTRING(path_scm);
  {
{
ScmSysStat* SCM_RESULT;

#line 729 "libsys.scm"
{ScmSysStat* s=SCM_SYS_STAT(Scm_MakeSysStat());int r;const char* p=check_trailing_separator(path);SCM_SYSCALL(r,stat(p,SCM_SYS_STAT_STAT(s)));if ((r)<(0)){{Scm_SysError("%s failed for %s","stat",p);}}{SCM_RESULT=(s);goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(defined(GAUCHE_WINDOWS))

static ScmObj libsyssys_mkfifo(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj path_scm;
  const char* path;
  ScmObj mode_scm;
  int mode;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-mkfifo");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  path_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(path_scm)) Scm_Error("const char* required, but got %S", path_scm);
  path = SCM_STRING_CONST_CSTRING(path_scm);
  mode_scm = SCM_SUBRARGS[1];
  if (!SCM_INTEGERP(mode_scm)) Scm_Error("int required, but got %S", mode_scm);
  mode = Scm_GetInteger(mode_scm);
  {
{
int SCM_RESULT;

#line 733 "libsys.scm"
SCM_SYSCALL(SCM_RESULT,mkfifo(path,mode));

#line 734 "libsys.scm"
if ((SCM_RESULT)<(0)){{Scm_SysError("mkfifo failed on %s",path);}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}

#endif /* !(defined(GAUCHE_WINDOWS)) */

static ScmObj libsyssys_fstat(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_or_fd_scm;
  ScmObj port_or_fd;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-fstat");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_or_fd_scm = SCM_SUBRARGS[0];
  if (!(port_or_fd_scm)) Scm_Error("scheme object required, but got %S", port_or_fd_scm);
  port_or_fd = (port_or_fd_scm);
  {
{
ScmObj SCM_RESULT;

#line 738 "libsys.scm"
{ScmSysStat* s=SCM_SYS_STAT(Scm_MakeSysStat());int fd=
Scm_GetPortFd(port_or_fd,FALSE);int r;
#line 741 "libsys.scm"
if ((fd)<(0)){{SCM_RESULT=(SCM_FALSE);goto SCM_STUB_RETURN;}} else {
SCM_SYSCALL(r,fstat(fd,SCM_SYS_STAT_STAT(s)));
if ((r)<(0)){{Scm_SysError("fstat failed for %d",fd);}}
{SCM_RESULT=(SCM_OBJ(s));goto SCM_STUB_RETURN;}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsysfile_existsP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj path_scm;
  const char* path;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("file-exists?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  path_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(path_scm)) Scm_Error("const char* required, but got %S", path_scm);
  path = SCM_STRING_CONST_CSTRING(path_scm);
  {
{
int SCM_RESULT;

#line 747 "libsys.scm"
{int r;
SCM_SYSCALL(r,access(path,F_OK));
{SCM_RESULT=((r)==(0));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libsysfile_is_regularP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj path_scm;
  const char* path;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("file-is-regular?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  path_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(path_scm)) Scm_Error("const char* required, but got %S", path_scm);
  path = SCM_STRING_CONST_CSTRING(path_scm);
  {
{
int SCM_RESULT;

#line 765 "libsys.scm"
{int r;ScmStat s;const char* p=check_trailing_separator(path);SCM_SYSCALL(r,access(p,F_OK));if ((r)==(0)){{SCM_SYSCALL(r,stat(p,&(s)));if ((r)<(0)){{Scm_SysError("stat failed for %s",path);}}{SCM_RESULT=(S_ISREG((s).st_mode));goto SCM_STUB_RETURN;}}} else {{SCM_RESULT=(FALSE);goto SCM_STUB_RETURN;}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libsysfile_is_directoryP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj path_scm;
  const char* path;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("file-is-directory?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  path_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(path_scm)) Scm_Error("const char* required, but got %S", path_scm);
  path = SCM_STRING_CONST_CSTRING(path_scm);
  {
{
int SCM_RESULT;

#line 767 "libsys.scm"
{int r;ScmStat s;const char* p=check_trailing_separator(path);SCM_SYSCALL(r,access(p,F_OK));if ((r)==(0)){{SCM_SYSCALL(r,stat(p,&(s)));if ((r)<(0)){{Scm_SysError("stat failed for %s",path);}}{SCM_RESULT=(S_ISDIR((s).st_mode));goto SCM_STUB_RETURN;}}} else {{SCM_RESULT=(FALSE);goto SCM_STUB_RETURN;}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}

static void utime_ts(ScmTimeSpec* ts,ScmObj arg){{
#line 772 "libsys.scm"
if (SCM_FALSEP(arg)){(ts)->tv_nsec=(UTIME_NOW);}else if(
SCM_TRUEP(arg)){(ts)->tv_nsec=(UTIME_OMIT);}else if(
SCM_REALP(arg)){
{double s;
(ts)->tv_nsec=(
((u_long )((modf(Scm_GetDouble(arg),&(s)))*(1.0e9))));
#line 779 "libsys.scm"
(ts)->tv_sec=(((u_long )(s)));
while(((ts)->tv_nsec)>=(1000000000)){
(ts)->tv_nsec=(((ts)->tv_nsec)-(1000000000));
++((ts)->tv_sec);}}}else if(
SCM_TIMEP(arg)){Scm_GetTimeSpec(arg,ts);} else {
Scm_Error("<time> object, real number or boolean required, but got: %S",arg);}}}

static ScmObj libsyssys_utime(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj path_scm;
  const char* path;
  ScmObj atime_scm;
  ScmObj atime;
  ScmObj mtime_scm;
  ScmObj mtime;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("sys-utime");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  path_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(path_scm)) Scm_Error("const char* required, but got %S", path_scm);
  path = SCM_STRING_CONST_CSTRING(path_scm);
  if (SCM_ARGCNT > 1+1) {
    atime_scm = SCM_SUBRARGS[1];
  } else {
    atime_scm = SCM_FALSE;
  }
  if (!(atime_scm)) Scm_Error("scheme object required, but got %S", atime_scm);
  atime = (atime_scm);
  if (SCM_ARGCNT > 2+1) {
    mtime_scm = SCM_SUBRARGS[2];
  } else {
    mtime_scm = SCM_FALSE;
  }
  if (!(mtime_scm)) Scm_Error("scheme object required, but got %S", mtime_scm);
  mtime = (mtime_scm);
  {

#line 788 "libsys.scm"
{ScmTimeSpec tss[2];int r;
utime_ts(&((tss)[0]),atime);
utime_ts(&((tss)[1]),mtime);
SCM_SYSCALL(r,utimensat(AT_FDCWD,path,tss,0));
if ((r)<(0)){{Scm_SysError("utimensat failed on %s",path);}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsyssys_times(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-times");
  {
{
ScmObj SCM_RESULT;

#line 799 "libsys.scm"
{struct tms info;clock_t r;long tick;
SCM_SYSCALL3(r,times(&(info)),(r)==(((clock_t )(-1))));
if ((r)==(((clock_t )(-1)))){{Scm_SysError("times failed");}}

#if defined(_SC_CLK_TCK)

#line 803 "libsys.scm"
tick=(sysconf(_SC_CLK_TCK));
#else /* !defined(_SC_CLK_TCK) */

#line 804 "libsys.scm"

#if defined(CLK_TCK)

#line 805 "libsys.scm"
tick=(CLK_TCK);
#else /* !defined(CLK_TCK) */

#line 806 "libsys.scm"
tick=(100);
#endif /* defined(CLK_TCK) */

#endif /* defined(_SC_CLK_TCK) */

#line 807 "libsys.scm"
{SCM_RESULT=(SCM_LIST5(Scm_MakeInteger((info).tms_utime),
Scm_MakeInteger((info).tms_stime),
Scm_MakeInteger((info).tms_cutime),
Scm_MakeInteger((info).tms_cstime),
Scm_MakeInteger(tick)));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_uname(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-uname");
  {
{
ScmObj SCM_RESULT;

#line 817 "libsys.scm"

#if !(defined(GAUCHE_WINDOWS))

#line 818 "libsys.scm"
{struct utsname info;
if ((uname(&(info)))<(0)){{Scm_SysError("uname failed");}}
{SCM_RESULT=(SCM_LIST5(SCM_MAKE_STR_COPYING((info).sysname),
SCM_MAKE_STR_COPYING((info).nodename),
SCM_MAKE_STR_COPYING((info).release),
SCM_MAKE_STR_COPYING((info).version),
SCM_MAKE_STR_COPYING((info).machine)));goto SCM_STUB_RETURN;}}
#else /* !!(defined(GAUCHE_WINDOWS)) */

#line 826 "libsys.scm"
{SCM_RESULT=(SCM_LIST5(SCM_FALSE,SCM_FALSE,SCM_FALSE,SCM_FALSE,SCM_FALSE));goto SCM_STUB_RETURN;}
#endif /* !(defined(GAUCHE_WINDOWS)) */

goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_wait(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-wait");
  {
{
ScmObj SCM_RESULT;

#line 832 "libsys.scm"
{SCM_RESULT=(Scm_SysWait(SCM_MAKE_INT(-1),0));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_waitpid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj process_scm;
  ScmObj process;
  ScmObj nohang_scm = SCM_FALSE;
  ScmObj nohang;
  ScmObj untraced_scm = SCM_FALSE;
  ScmObj untraced;
  ScmObj SCM_SUBRARGS[1];
  ScmObj SCM_KEYARGS = SCM_ARGREF(SCM_ARGCNT-1);
  SCM_ENTER_SUBR("sys-waitpid");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  process_scm = SCM_SUBRARGS[0];
  if (!(process_scm)) Scm_Error("scheme object required, but got %S", process_scm);
  process = (process_scm);
  if (Scm_Length(SCM_KEYARGS) % 2)
    Scm_Error("keyword list not even: %S", SCM_KEYARGS);
  while (!SCM_NULLP(SCM_KEYARGS)) {
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[650])) {
      nohang_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[651])) {
      untraced_scm = SCM_CADR(SCM_KEYARGS);
    }
    else Scm_Warn("unknown keyword %S", SCM_CAR(SCM_KEYARGS));
    SCM_KEYARGS = SCM_CDDR(SCM_KEYARGS);
  }
  if (!(nohang_scm)) Scm_Error("scheme object required, but got %S", nohang_scm);
  nohang = (nohang_scm);
  if (!(untraced_scm)) Scm_Error("scheme object required, but got %S", untraced_scm);
  untraced = (untraced_scm);
  {
{
ScmObj SCM_RESULT;

#line 835 "libsys.scm"
{int options=0;
if (!(SCM_FALSEP(nohang))){{(options)|=(WNOHANG);}}
if (!(SCM_FALSEP(untraced))){{(options)|=(WUNTRACED);}}
{SCM_RESULT=(Scm_SysWait(process,options));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_wait_exitedP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj status_scm;
  int status;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-wait-exited?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  status_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(status_scm)) Scm_Error("int required, but got %S", status_scm);
  status = Scm_GetInteger(status_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(WIFEXITED(status));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_wait_exit_status(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj status_scm;
  int status;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-wait-exit-status");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  status_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(status_scm)) Scm_Error("int required, but got %S", status_scm);
  status = Scm_GetInteger(status_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(WEXITSTATUS(status));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_wait_signaledP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj status_scm;
  int status;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-wait-signaled?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  status_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(status_scm)) Scm_Error("int required, but got %S", status_scm);
  status = Scm_GetInteger(status_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(WIFSIGNALED(status));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_wait_termsig(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj status_scm;
  int status;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-wait-termsig");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  status_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(status_scm)) Scm_Error("int required, but got %S", status_scm);
  status = Scm_GetInteger(status_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(WTERMSIG(status));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_wait_stoppedP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj status_scm;
  int status;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-wait-stopped?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  status_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(status_scm)) Scm_Error("int required, but got %S", status_scm);
  status = Scm_GetInteger(status_scm);
  {
{
int SCM_RESULT;

#line 846 "libsys.scm"
((void )(status));

#line 847 "libsys.scm"
{SCM_RESULT=(WIFSTOPPED(status));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_wait_stopsig(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj status_scm;
  int status;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-wait-stopsig");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  status_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(status_scm)) Scm_Error("int required, but got %S", status_scm);
  status = Scm_GetInteger(status_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(WSTOPSIG(status));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_time(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-time");
  {
{
ScmObj SCM_RESULT;

#line 853 "libsys.scm"
{SCM_RESULT=(Scm_MakeSysTime(time(NULL)));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_gettimeofday(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-gettimeofday");
  {
u_long SCM_RESULT0;
u_long SCM_RESULT1;
{

#line 856 "libsys.scm"
Scm_GetTimeOfDay(&(SCM_RESULT0),&(SCM_RESULT1));
}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_Values2(Scm_MakeIntegerU(SCM_RESULT0),Scm_MakeIntegerU(SCM_RESULT1)));
  }
}


static ScmObj libsyscurrent_microseconds(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("current-microseconds");
  {
{
long SCM_RESULT;
{SCM_RESULT=(Scm_CurrentMicroseconds());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_clock_gettime_monotonic(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-clock-gettime-monotonic");
  {
ScmObj SCM_RESULT0;
ScmObj SCM_RESULT1;
{

#line 863 "libsys.scm"
{u_long sec;u_long nsec;int r=
Scm_ClockGetTimeMonotonic(&(sec),&(nsec));
if (r){
{SCM_RESULT0=(Scm_MakeIntegerU(sec));
SCM_RESULT1=(Scm_MakeIntegerU(nsec));}} else {
{SCM_RESULT0=(SCM_FALSE);
SCM_RESULT1=(SCM_FALSE);}}}
}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_Values2(SCM_OBJ_SAFE(SCM_RESULT0),SCM_OBJ_SAFE(SCM_RESULT1)));
  }
}


static ScmObj libsyssys_clock_getres_monotonic(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-clock-getres-monotonic");
  {
ScmObj SCM_RESULT0;
ScmObj SCM_RESULT1;
{

#line 873 "libsys.scm"
{u_long sec;u_long nsec;int r=
Scm_ClockGetResMonotonic(&(sec),&(nsec));
if (r){
{SCM_RESULT0=(Scm_MakeIntegerU(sec));
SCM_RESULT1=(Scm_MakeIntegerU(nsec));}} else {
{SCM_RESULT0=(SCM_FALSE);
SCM_RESULT1=(SCM_FALSE);}}}
}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_Values2(SCM_OBJ_SAFE(SCM_RESULT0),SCM_OBJ_SAFE(SCM_RESULT1)));
  }
}


static ScmObj libsyscurrent_time(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("current-time");
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_CurrentTime());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsystimeP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("time?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(SCM_TIMEP(obj));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libsysabsolute_time(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj t0_scm;
  ScmTime* t0;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("absolute-time");
  if (SCM_ARGCNT >= 3
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 2 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  if (SCM_ARGCNT > 1+1) {
    t0_scm = SCM_SUBRARGS[1];
  } else {
    t0_scm = SCM_FALSE;
  }
  if (!SCM_MAYBE_P(SCM_TIMEP, t0_scm)) Scm_Error("<time> or #f required, but got %S", t0_scm);
  t0 = SCM_MAYBE(SCM_TIME, t0_scm);
  {
{
ScmTime* SCM_RESULT;

#line 890 "libsys.scm"
{ScmTimeSpec ts;ScmTimeSpec* pts=
Scm_ToTimeSpec(obj,t0,&(ts));
if ((pts)==(NULL)){
{SCM_RESULT=(NULL);goto SCM_STUB_RETURN;}} else {
{SCM_RESULT=(SCM_TIME(Scm_MakeTime64(((t0)?((t0)->type):(scm__rc.d1786[728])),
(pts)->tv_sec,
(pts)->tv_nsec)));goto SCM_STUB_RETURN;}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_MAYBE(SCM_OBJ_SAFE, SCM_RESULT));
}
  }
}


static ScmObj libsystime_TOseconds(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj t_scm;
  ScmTime* t;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("time->seconds");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  t_scm = SCM_SUBRARGS[0];
  if (!SCM_TIMEP(t_scm)) Scm_Error("<time> required, but got %S", t_scm);
  t = SCM_TIME(t_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_TimeToSeconds(t));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsysseconds_TOtime(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj t_scm;
  double t;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("seconds->time");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  t_scm = SCM_SUBRARGS[0];
  if (!SCM_REALP(t_scm)) Scm_Error("double required, but got %S", t_scm);
  t = Scm_GetDouble(t_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_RealSecondsToTime(t));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


ScmObj Scm_Make_sys_tm(const struct tm *v)
{
  Scm_sys_tm_Rec *z = SCM_NEW(Scm_sys_tm_Rec);
  SCM_SET_CLASS(z, &Scm_sys_tm_Class);
  z->data = *v;

  return SCM_OBJ(z);
}
SCM_DEFINE_BUILTIN_CLASS(Scm_sys_tm_Class, tm_print, NULL, NULL, NULL, SCM_CLASS_DEFAULT_CPL);

static ScmObj Scm_sys_tm_Class_sec_GET(ScmObj OBJARG)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  return Scm_MakeInteger(obj->tm_sec);
}

static void Scm_sys_tm_Class_sec_SET(ScmObj OBJARG, ScmObj value)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  if (!SCM_INTEGERP(value)) Scm_Error("int required, but got %S", value);
  obj->tm_sec = Scm_GetInteger(value);
}

static ScmObj Scm_sys_tm_Class_min_GET(ScmObj OBJARG)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  return Scm_MakeInteger(obj->tm_min);
}

static void Scm_sys_tm_Class_min_SET(ScmObj OBJARG, ScmObj value)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  if (!SCM_INTEGERP(value)) Scm_Error("int required, but got %S", value);
  obj->tm_min = Scm_GetInteger(value);
}

static ScmObj Scm_sys_tm_Class_hour_GET(ScmObj OBJARG)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  return Scm_MakeInteger(obj->tm_hour);
}

static void Scm_sys_tm_Class_hour_SET(ScmObj OBJARG, ScmObj value)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  if (!SCM_INTEGERP(value)) Scm_Error("int required, but got %S", value);
  obj->tm_hour = Scm_GetInteger(value);
}

static ScmObj Scm_sys_tm_Class_mday_GET(ScmObj OBJARG)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  return Scm_MakeInteger(obj->tm_mday);
}

static void Scm_sys_tm_Class_mday_SET(ScmObj OBJARG, ScmObj value)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  if (!SCM_INTEGERP(value)) Scm_Error("int required, but got %S", value);
  obj->tm_mday = Scm_GetInteger(value);
}

static ScmObj Scm_sys_tm_Class_mon_GET(ScmObj OBJARG)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  return Scm_MakeInteger(obj->tm_mon);
}

static void Scm_sys_tm_Class_mon_SET(ScmObj OBJARG, ScmObj value)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  if (!SCM_INTEGERP(value)) Scm_Error("int required, but got %S", value);
  obj->tm_mon = Scm_GetInteger(value);
}

static ScmObj Scm_sys_tm_Class_year_GET(ScmObj OBJARG)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  return Scm_MakeInteger(obj->tm_year);
}

static void Scm_sys_tm_Class_year_SET(ScmObj OBJARG, ScmObj value)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  if (!SCM_INTEGERP(value)) Scm_Error("int required, but got %S", value);
  obj->tm_year = Scm_GetInteger(value);
}

static ScmObj Scm_sys_tm_Class_wday_GET(ScmObj OBJARG)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  return Scm_MakeInteger(obj->tm_wday);
}

static void Scm_sys_tm_Class_wday_SET(ScmObj OBJARG, ScmObj value)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  if (!SCM_INTEGERP(value)) Scm_Error("int required, but got %S", value);
  obj->tm_wday = Scm_GetInteger(value);
}

static ScmObj Scm_sys_tm_Class_yday_GET(ScmObj OBJARG)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  return Scm_MakeInteger(obj->tm_yday);
}

static void Scm_sys_tm_Class_yday_SET(ScmObj OBJARG, ScmObj value)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  if (!SCM_INTEGERP(value)) Scm_Error("int required, but got %S", value);
  obj->tm_yday = Scm_GetInteger(value);
}

static ScmObj Scm_sys_tm_Class_isdst_GET(ScmObj OBJARG)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  return Scm_MakeInteger(obj->tm_isdst);
}

static void Scm_sys_tm_Class_isdst_SET(ScmObj OBJARG, ScmObj value)
{
  struct tm* obj = SCM_SYS_TM(OBJARG);
  if (!SCM_INTEGERP(value)) Scm_Error("int required, but got %S", value);
  obj->tm_isdst = Scm_GetInteger(value);
}

static ScmClassStaticSlotSpec Scm_sys_tm_Class__SLOTS[] = {
  SCM_CLASS_SLOT_SPEC("sec", Scm_sys_tm_Class_sec_GET, Scm_sys_tm_Class_sec_SET),
  SCM_CLASS_SLOT_SPEC("min", Scm_sys_tm_Class_min_GET, Scm_sys_tm_Class_min_SET),
  SCM_CLASS_SLOT_SPEC("hour", Scm_sys_tm_Class_hour_GET, Scm_sys_tm_Class_hour_SET),
  SCM_CLASS_SLOT_SPEC("mday", Scm_sys_tm_Class_mday_GET, Scm_sys_tm_Class_mday_SET),
  SCM_CLASS_SLOT_SPEC("mon", Scm_sys_tm_Class_mon_GET, Scm_sys_tm_Class_mon_SET),
  SCM_CLASS_SLOT_SPEC("year", Scm_sys_tm_Class_year_GET, Scm_sys_tm_Class_year_SET),
  SCM_CLASS_SLOT_SPEC("wday", Scm_sys_tm_Class_wday_GET, Scm_sys_tm_Class_wday_SET),
  SCM_CLASS_SLOT_SPEC("yday", Scm_sys_tm_Class_yday_GET, Scm_sys_tm_Class_yday_SET),
  SCM_CLASS_SLOT_SPEC("isdst", Scm_sys_tm_Class_isdst_GET, Scm_sys_tm_Class_isdst_SET),
  SCM_CLASS_SLOT_SPEC_END()
};

 void tm_print(ScmObj obj,ScmPort* port,ScmWriteContext* G1863 SCM_UNUSED){{
#line 932 "libsys.scm"
{struct tm* st=SCM_SYS_TM(obj);const char* fmt;
#line 934 "libsys.scm"

#if !(defined(GAUCHE_WINDOWS))

#line 935 "libsys.scm"
fmt=("%a %b %e %T %Y");
#else /* !!(defined(GAUCHE_WINDOWS)) */

#line 936 "libsys.scm"
fmt=("%a %b %d %H:%M:%S %Y");
#endif /* !(defined(GAUCHE_WINDOWS)) */

#line 937 "libsys.scm"
Scm_Printf(port,"#<sys-tm %S>",Scm_StrfTime(fmt,st,SCM_FALSE));}}}

static ScmObj libsyssys_asctime(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj tm_scm;
  struct tm* tm;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-asctime");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  tm_scm = SCM_SUBRARGS[0];
  if (!SCM_SYS_TM_P(tm_scm)) Scm_Error("<sys-tm> required, but got %S", tm_scm);
  tm = SCM_SYS_TM(tm_scm);
  {
{
ScmObj SCM_RESULT;

#line 941 "libsys.scm"
{SCM_RESULT=(SCM_MAKE_STR_COPYING(asctime(tm)));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_ctime(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj time_scm;
  ScmObj time;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-ctime");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  time_scm = SCM_SUBRARGS[0];
  if (!(time_scm)) Scm_Error("scheme object required, but got %S", time_scm);
  time = (time_scm);
  {
{
ScmObj SCM_RESULT;

#line 947 "libsys.scm"
{time_t tim=Scm_GetSysTime(time);
{SCM_RESULT=(SCM_MAKE_STR_COPYING(ctime(&(tim))));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_difftime(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj time1_scm;
  ScmObj time1;
  ScmObj time0_scm;
  ScmObj time0;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-difftime");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  time1_scm = SCM_SUBRARGS[0];
  if (!(time1_scm)) Scm_Error("scheme object required, but got %S", time1_scm);
  time1 = (time1_scm);
  time0_scm = SCM_SUBRARGS[1];
  if (!(time0_scm)) Scm_Error("scheme object required, but got %S", time0_scm);
  time0 = (time0_scm);
  {
{
double SCM_RESULT;

#line 951 "libsys.scm"
{SCM_RESULT=(difftime(Scm_GetSysTime(time1),Scm_GetSysTime(time0)));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_VMReturnFlonum(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_strftime(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj format_scm;
  const char* format;
  ScmObj tm_scm;
  struct tm* tm;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-strftime");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  format_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(format_scm)) Scm_Error("const char* required, but got %S", format_scm);
  format = SCM_STRING_CONST_CSTRING(format_scm);
  tm_scm = SCM_SUBRARGS[1];
  if (!SCM_SYS_TM_P(tm_scm)) Scm_Error("<sys-tm> required, but got %S", tm_scm);
  tm = SCM_SYS_TM(tm_scm);
  {
{
ScmObj SCM_RESULT;

#line 954 "libsys.scm"
{SCM_RESULT=(Scm_StrfTime(format,tm,SCM_FALSE));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_gmtime(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj time_scm;
  ScmObj time;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-gmtime");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  time_scm = SCM_SUBRARGS[0];
  if (!(time_scm)) Scm_Error("scheme object required, but got %S", time_scm);
  time = (time_scm);
  {
{
ScmObj SCM_RESULT;

#line 962 "libsys.scm"

#if defined(GAUCHE_WINDOWS)

#line 963 "libsys.scm"
{time_t tim=Scm_GetSysTime(time);
{SCM_RESULT=(Scm_Make_sys_tm(gmtime(&(tim))));goto SCM_STUB_RETURN;}}
#else /* !defined(GAUCHE_WINDOWS) */

#line 965 "libsys.scm"
{time_t tim=Scm_GetSysTime(time);struct tm buf;
#line 967 "libsys.scm"
{SCM_RESULT=(Scm_Make_sys_tm(gmtime_r(&(tim),&(buf))));goto SCM_STUB_RETURN;}}
#endif /* defined(GAUCHE_WINDOWS) */

goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_localtime(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj time_scm;
  ScmObj time;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-localtime");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  time_scm = SCM_SUBRARGS[0];
  if (!(time_scm)) Scm_Error("scheme object required, but got %S", time_scm);
  time = (time_scm);
  {
{
ScmObj SCM_RESULT;

#line 970 "libsys.scm"

#if defined(GAUCHE_WINDOWS)

#line 971 "libsys.scm"
{time_t tim=Scm_GetSysTime(time);
{SCM_RESULT=(Scm_Make_sys_tm(localtime(&(tim))));goto SCM_STUB_RETURN;}}
#else /* !defined(GAUCHE_WINDOWS) */

#line 973 "libsys.scm"
{time_t tim=Scm_GetSysTime(time);struct tm buf;
#line 975 "libsys.scm"
{SCM_RESULT=(Scm_Make_sys_tm(localtime_r(&(tim),&(buf))));goto SCM_STUB_RETURN;}}
#endif /* defined(GAUCHE_WINDOWS) */

goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_mktime(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj tm_scm;
  struct tm* tm;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-mktime");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  tm_scm = SCM_SUBRARGS[0];
  if (!SCM_SYS_TM_P(tm_scm)) Scm_Error("<sys-tm> required, but got %S", tm_scm);
  tm = SCM_SYS_TM(tm_scm);
  {
{
ScmObj SCM_RESULT;

#line 978 "libsys.scm"
{SCM_RESULT=(Scm_MakeSysTime(mktime(tm)));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_access(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj pathname_scm;
  const char* pathname;
  ScmObj amode_scm;
  int amode;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-access");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  pathname_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(pathname_scm)) Scm_Error("const char* required, but got %S", pathname_scm);
  pathname = SCM_STRING_CONST_CSTRING(pathname_scm);
  amode_scm = SCM_SUBRARGS[1];
  if (!SCM_INTEGERP(amode_scm)) Scm_Error("int required, but got %S", amode_scm);
  amode = Scm_GetInteger(amode_scm);
  {
{
int SCM_RESULT;

#line 992 "libsys.scm"
{int r;
if (Scm_IsSugid()){{
Scm_Error("cannot use sys-access in suid/sgid program.");}}
SCM_SYSCALL(r,access(pathname,amode));
{SCM_RESULT=((r)==(0));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_chdir(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj pathname_scm;
  const char* pathname;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-chdir");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  pathname_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(pathname_scm)) Scm_Error("const char* required, but got %S", pathname_scm);
  pathname = SCM_STRING_CONST_CSTRING(pathname_scm);
  {

#line 999 "libsys.scm"
{int r;
SCM_SYSCALL(r,chdir(pathname));
if ((r)<(0)){{Scm_SysError("chdir failed");}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsyssys_chmod(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj pathname_scm;
  const char* pathname;
  ScmObj mode_scm;
  int mode;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-chmod");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  pathname_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(pathname_scm)) Scm_Error("const char* required, but got %S", pathname_scm);
  pathname = SCM_STRING_CONST_CSTRING(pathname_scm);
  mode_scm = SCM_SUBRARGS[1];
  if (!SCM_INTEGERP(mode_scm)) Scm_Error("int required, but got %S", mode_scm);
  mode = Scm_GetInteger(mode_scm);
  {

#line 1004 "libsys.scm"
{int r;
SCM_SYSCALL(r,chmod(pathname,mode));
if ((r)<(0)){{Scm_SysError("chmod failed");}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}

#if !(defined(GAUCHE_WINDOWS))

static ScmObj libsyssys_fchmod(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_or_fd_scm;
  ScmObj port_or_fd;
  ScmObj mode_scm;
  int mode;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-fchmod");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_or_fd_scm = SCM_SUBRARGS[0];
  if (!(port_or_fd_scm)) Scm_Error("scheme object required, but got %S", port_or_fd_scm);
  port_or_fd = (port_or_fd_scm);
  mode_scm = SCM_SUBRARGS[1];
  if (!SCM_INTEGERP(mode_scm)) Scm_Error("int required, but got %S", mode_scm);
  mode = Scm_GetInteger(mode_scm);
  {

#line 1011 "libsys.scm"
{int r;int fd=Scm_GetPortFd(port_or_fd,TRUE);
SCM_SYSCALL(r,fchmod(fd,mode));
if ((r)<(0)){{Scm_SysError("fchmod failed");}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}

#endif /* !(defined(GAUCHE_WINDOWS)) */

static ScmObj libsyssys_chown(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj path_scm;
  const char* path;
  ScmObj owner_scm;
  int owner;
  ScmObj group_scm;
  int group;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("sys-chown");
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  path_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(path_scm)) Scm_Error("const char* required, but got %S", path_scm);
  path = SCM_STRING_CONST_CSTRING(path_scm);
  owner_scm = SCM_SUBRARGS[1];
  if (!SCM_INTEGERP(owner_scm)) Scm_Error("int required, but got %S", owner_scm);
  owner = Scm_GetInteger(owner_scm);
  group_scm = SCM_SUBRARGS[2];
  if (!SCM_INTEGERP(group_scm)) Scm_Error("int required, but got %S", group_scm);
  group = Scm_GetInteger(group_scm);
  {
{
int SCM_RESULT;

#line 1020 "libsys.scm"
((void )(owner));

#line 1021 "libsys.scm"
((void )(group));

#line 1022 "libsys.scm"

#if !(defined(GAUCHE_WINDOWS))

#line 1023 "libsys.scm"
SCM_SYSCALL(SCM_RESULT,chown(path,owner,group));
#else /* !!(defined(GAUCHE_WINDOWS)) */

#line 1024 "libsys.scm"
SCM_RESULT=(0);
#endif /* !(defined(GAUCHE_WINDOWS)) */


#line 1025 "libsys.scm"
if ((SCM_RESULT)<(0)){{Scm_SysError("chown failed on %s",path);}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}

#if defined(HAVE_LCHOWN)

static ScmObj libsyssys_lchown(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj path_scm;
  const char* path;
  ScmObj owner_scm;
  int owner;
  ScmObj group_scm;
  int group;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("sys-lchown");
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  path_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(path_scm)) Scm_Error("const char* required, but got %S", path_scm);
  path = SCM_STRING_CONST_CSTRING(path_scm);
  owner_scm = SCM_SUBRARGS[1];
  if (!SCM_INTEGERP(owner_scm)) Scm_Error("int required, but got %S", owner_scm);
  owner = Scm_GetInteger(owner_scm);
  group_scm = SCM_SUBRARGS[2];
  if (!SCM_INTEGERP(group_scm)) Scm_Error("int required, but got %S", group_scm);
  group = Scm_GetInteger(group_scm);
  {
{
int SCM_RESULT;

#line 1032 "libsys.scm"
SCM_SYSCALL(SCM_RESULT,lchown(path,owner,group));

#line 1033 "libsys.scm"
if ((SCM_RESULT)<(0)){{Scm_SysError("lchown failed on %S",path);}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}

#endif /* defined(HAVE_LCHOWN) */

static ScmObj libsyssys_fork(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-fork");
  {
{
int SCM_RESULT;

#line 1041 "libsys.scm"
{pid_t pid;
GC_gcollect();
SCM_SYSCALL(pid,fork());
if ((pid)<(0)){{Scm_SysError("fork failed");}}
{SCM_RESULT=(pid);goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_exec(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj command_scm;
  ScmString* command;
  ScmObj args_scm;
  ScmObj args;
  ScmObj iomap_scm = SCM_NIL;
  ScmObj iomap;
  ScmObj sigmask_scm = SCM_FALSE;
  ScmSysSigset* sigmask;
  ScmObj directory_scm = SCM_FALSE;
  ScmString* directory;
  ScmObj detached_scm = SCM_FALSE;
  int detached;
  ScmObj SCM_SUBRARGS[2];
  ScmObj SCM_KEYARGS = SCM_ARGREF(SCM_ARGCNT-1);
  SCM_ENTER_SUBR("sys-exec");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  command_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(command_scm)) Scm_Error("<string> required, but got %S", command_scm);
  command = SCM_STRING(command_scm);
  args_scm = SCM_SUBRARGS[1];
  if (!SCM_LISTP(args_scm)) Scm_Error("list required, but got %S", args_scm);
  args = (args_scm);
  if (Scm_Length(SCM_KEYARGS) % 2)
    Scm_Error("keyword list not even: %S", SCM_KEYARGS);
  while (!SCM_NULLP(SCM_KEYARGS)) {
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[878])) {
      iomap_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[879])) {
      sigmask_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[880])) {
      directory_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[881])) {
      detached_scm = SCM_CADR(SCM_KEYARGS);
    }
    else Scm_Warn("unknown keyword %S", SCM_CAR(SCM_KEYARGS));
    SCM_KEYARGS = SCM_CDDR(SCM_KEYARGS);
  }
  if (!(iomap_scm)) Scm_Error("scheme object required, but got %S", iomap_scm);
  iomap = (iomap_scm);
  if (!SCM_MAYBE_P(SCM_SYS_SIGSET_P, sigmask_scm)) Scm_Error("<sys-sigset> or #f required, but got %S", sigmask_scm);
  sigmask = SCM_MAYBE(SCM_SYS_SIGSET, sigmask_scm);
  if (!SCM_MAYBE_P(SCM_STRINGP, directory_scm)) Scm_Error("<string> or #f required, but got %S", directory_scm);
  directory = SCM_MAYBE(SCM_STRING, directory_scm);
  if (!SCM_BOOLP(detached_scm)) Scm_Error("boolean required, but got %S", detached_scm);
  detached = SCM_BOOL_VALUE(detached_scm);
  {

#line 1073 "libsys.scm"
{u_int flags=((detached)?(SCM_EXEC_DETACHED):(0));
Scm_SysExec(command,args,iomap,sigmask,directory,flags);}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsyssys_fork_and_exec(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj command_scm;
  ScmString* command;
  ScmObj args_scm;
  ScmObj args;
  ScmObj iomap_scm = SCM_NIL;
  ScmObj iomap;
  ScmObj sigmask_scm = SCM_FALSE;
  ScmSysSigset* sigmask;
  ScmObj directory_scm = SCM_FALSE;
  ScmString* directory;
  ScmObj detached_scm = SCM_FALSE;
  int detached;
  ScmObj SCM_SUBRARGS[2];
  ScmObj SCM_KEYARGS = SCM_ARGREF(SCM_ARGCNT-1);
  SCM_ENTER_SUBR("sys-fork-and-exec");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  command_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(command_scm)) Scm_Error("<string> required, but got %S", command_scm);
  command = SCM_STRING(command_scm);
  args_scm = SCM_SUBRARGS[1];
  if (!SCM_LISTP(args_scm)) Scm_Error("list required, but got %S", args_scm);
  args = (args_scm);
  if (Scm_Length(SCM_KEYARGS) % 2)
    Scm_Error("keyword list not even: %S", SCM_KEYARGS);
  while (!SCM_NULLP(SCM_KEYARGS)) {
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[878])) {
      iomap_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[879])) {
      sigmask_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[880])) {
      directory_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[881])) {
      detached_scm = SCM_CADR(SCM_KEYARGS);
    }
    else Scm_Warn("unknown keyword %S", SCM_CAR(SCM_KEYARGS));
    SCM_KEYARGS = SCM_CDDR(SCM_KEYARGS);
  }
  if (!(iomap_scm)) Scm_Error("scheme object required, but got %S", iomap_scm);
  iomap = (iomap_scm);
  if (!SCM_MAYBE_P(SCM_SYS_SIGSET_P, sigmask_scm)) Scm_Error("<sys-sigset> or #f required, but got %S", sigmask_scm);
  sigmask = SCM_MAYBE(SCM_SYS_SIGSET, sigmask_scm);
  if (!SCM_MAYBE_P(SCM_STRINGP, directory_scm)) Scm_Error("<string> or #f required, but got %S", directory_scm);
  directory = SCM_MAYBE(SCM_STRING, directory_scm);
  if (!SCM_BOOLP(detached_scm)) Scm_Error("boolean required, but got %S", detached_scm);
  detached = SCM_BOOL_VALUE(detached_scm);
  {
{
ScmObj SCM_RESULT;

#line 1081 "libsys.scm"
{u_int flags=SCM_EXEC_WITH_FORK;
if (detached){{
flags=((flags)|(SCM_EXEC_DETACHED));}}
{SCM_RESULT=(Scm_SysExec(command,args,iomap,sigmask,directory,flags));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_getcwd(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-getcwd");
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_GetCwd());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_getegid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-getegid");
  {
{
int SCM_RESULT;
{SCM_RESULT=(getegid());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_getgid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-getgid");
  {
{
int SCM_RESULT;
{SCM_RESULT=(getgid());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_geteuid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-geteuid");
  {
{
int SCM_RESULT;
{SCM_RESULT=(geteuid());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_getuid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-getuid");
  {
{
int SCM_RESULT;
{SCM_RESULT=(getuid());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_setugidP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-setugid?");
  {
{
int SCM_RESULT;
{SCM_RESULT=(Scm_IsSugid());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_getpid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-getpid");
  {
{
int SCM_RESULT;
{SCM_RESULT=(getpid());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_getppid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-getppid");
  {
{
int SCM_RESULT;
{SCM_RESULT=(getppid());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}

#if !(defined(GAUCHE_WINDOWS))

static ScmObj libsyssys_setgid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj gid_scm;
  int gid;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-setgid");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  gid_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(gid_scm)) Scm_Error("int required, but got %S", gid_scm);
  gid = Scm_GetInteger(gid_scm);
  {
{
int SCM_RESULT;

#line 1100 "libsys.scm"
SCM_SYSCALL(SCM_RESULT,setgid(gid));

#line 1101 "libsys.scm"
if ((SCM_RESULT)<(0)){{Scm_SysError("setgid failed on %d",gid);}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))

static ScmObj libsyssys_setpgid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj pid_scm;
  int pid;
  ScmObj pgid_scm;
  int pgid;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-setpgid");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  pid_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(pid_scm)) Scm_Error("int required, but got %S", pid_scm);
  pid = Scm_GetInteger(pid_scm);
  pgid_scm = SCM_SUBRARGS[1];
  if (!SCM_INTEGERP(pgid_scm)) Scm_Error("int required, but got %S", pgid_scm);
  pgid = Scm_GetInteger(pgid_scm);
  {
{
int SCM_RESULT;

#line 1104 "libsys.scm"
SCM_SYSCALL(SCM_RESULT,setpgid(pid,pgid));

#line 1105 "libsys.scm"
if ((SCM_RESULT)<(0)){{
Scm_SysError("setpgid failed on process %d for pgid %d",pid,pgid);}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)

static ScmObj libsyssys_getpgid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj pid_scm;
  int pid;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-getpgid");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  pid_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(pid_scm)) Scm_Error("int required, but got %S", pid_scm);
  pid = Scm_GetInteger(pid_scm);
  {
{
int SCM_RESULT;

#line 1118 "libsys.scm"
SCM_SYSCALL(SCM_RESULT,((int )(getpgid(pid))));

#line 1119 "libsys.scm"
if ((SCM_RESULT)<(0)){{Scm_SysError("getpgid failed");}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}

#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))

static ScmObj libsyssys_getpgrp(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-getpgrp");
  {
{
int SCM_RESULT;

#line 1123 "libsys.scm"
SCM_SYSCALL(SCM_RESULT,((int )(getpgrp())));

#line 1124 "libsys.scm"
if ((SCM_RESULT)<(0)){{Scm_SysError("getpgrp failed");}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))

static ScmObj libsyssys_setsid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-setsid");
  {
{
int SCM_RESULT;

#line 1127 "libsys.scm"
SCM_SYSCALL(SCM_RESULT,setsid());

#line 1128 "libsys.scm"
if ((SCM_RESULT)<(0)){{Scm_SysError("setsid failed");}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))

static ScmObj libsyssys_setuid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj uid_scm;
  int uid;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-setuid");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  uid_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(uid_scm)) Scm_Error("int required, but got %S", uid_scm);
  uid = Scm_GetInteger(uid_scm);
  {
{
int SCM_RESULT;

#line 1131 "libsys.scm"
SCM_SYSCALL(SCM_RESULT,setuid(uid));

#line 1132 "libsys.scm"
if ((SCM_RESULT)<(0)){{Scm_SysError("setuid failed");}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
 int call_nice(int inc,int* errno_save){{
#line 1135 "libsys.scm"
errno=(0);
{int r=nice(inc);
*(errno_save)=(errno);
return (r);}}}
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))

static ScmObj libsyssys_nice(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj inc_scm;
  int inc;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-nice");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  inc_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(inc_scm)) Scm_Error("int required, but got %S", inc_scm);
  inc = Scm_GetInteger(inc_scm);
  {
{
int SCM_RESULT;

#line 1141 "libsys.scm"
{int errno_save=0;
SCM_SYSCALL(SCM_RESULT,call_nice(inc,&(errno_save)));
if (((SCM_RESULT)<(0))&&((errno)!=(0))){{
Scm_SysError("nice failed");}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))

static ScmObj libsyssys_getgroups(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-getgroups");
  {
{
ScmObj SCM_RESULT;

#line 1149 "libsys.scm"
{int size=32;gid_t glist[32];gid_t* pglist=glist;
#line 1152 "libsys.scm"
for (;;){{int n=getgroups(size,pglist);
if ((n)>=(0)){{
{ScmObj h=SCM_NIL;ScmObj t=SCM_NIL;
{int i=0;int cise__1870=n;for (; (i)<(cise__1870); (i)++){
SCM_APPEND1(h,t,Scm_MakeInteger((pglist)[i]));}}
SCM_RESULT=(h);
break;}}}
if ((errno)==(EINVAL)){
(size)+=(size);
pglist=(SCM_NEW_ATOMIC_ARRAY(gid_t,size));} else {
Scm_SysError("getgroups failed");}}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)

static ScmObj libsyssys_setgroups(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj gids_scm;
  ScmObj gids;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-setgroups");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  gids_scm = SCM_SUBRARGS[0];
  if (!(gids_scm)) Scm_Error("scheme object required, but got %S", gids_scm);
  gids = (gids_scm);
  {
ScmObj cise__1872;
#line 1166 "libsys.scm"
{int ngid=Scm_Length(gids);gid_t* glist=NULL;int k=0;int r;
#line 1169 "libsys.scm"
if ((ngid)<(0)){{
Scm_Error("List of integer gids required, but got: %S",gids);}}
glist=(SCM_NEW_ATOMIC_ARRAY(gid_t,ngid));
SCM_FOR_EACH(cise__1872,gids) {{ScmObj gid=SCM_CAR(cise__1872);
if (!(SCM_INTP(gid))){{
Scm_Error("gid list contains invalud value: %S",gid);}}
(glist)[k]=(SCM_INT_VALUE(gid));
(k)++;}}
#line 1178 "libsys.scm"
SCM_SYSCALL(r,setgroups(ngid,glist));
if ((r)<(0)){{
Scm_SysError("setgroups failed with %S",gids);}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}

#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */

static ScmObj libsyssys_getlogin(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-getlogin");
  {
{
const char* SCM_RESULT;
{SCM_RESULT=(getlogin());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_MAYBE(SCM_MAKE_STR_COPYING, SCM_RESULT));
}
  }
}


static ScmObj libsyssys_link(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj existing_scm;
  const char* existing;
  ScmObj newpath_scm;
  const char* newpath;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-link");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  existing_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(existing_scm)) Scm_Error("const char* required, but got %S", existing_scm);
  existing = SCM_STRING_CONST_CSTRING(existing_scm);
  newpath_scm = SCM_SUBRARGS[1];
  if (!SCM_STRINGP(newpath_scm)) Scm_Error("const char* required, but got %S", newpath_scm);
  newpath = SCM_STRING_CONST_CSTRING(newpath_scm);
  {

#line 1191 "libsys.scm"
{int r;
SCM_SYSCALL(r,link(existing,newpath));
if ((r)<(0)){{Scm_SysError("link failed");}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsyssys_pause(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-pause");
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_Pause());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_alarm(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj seconds_scm;
  ScmSmallInt seconds;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-alarm");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  seconds_scm = SCM_SUBRARGS[0];
  if (!SCM_INTP(seconds_scm)) Scm_Error("ScmSmallInt required, but got %S", seconds_scm);
  seconds = SCM_INT_VALUE(seconds_scm);
  {
{
int SCM_RESULT;

#line 1202 "libsys.scm"
SCM_SYSCALL(SCM_RESULT,alarm(seconds));
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_pipe(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj name_scm = SCM_OBJ(&scm__sc.d1785[432]);
  ScmObj name;
  ScmObj buffering_scm = SCM_FALSE;
  ScmObj buffering;
  ScmObj bufferedP_scm = SCM_FALSE;
  ScmObj bufferedP;
  ScmObj SCM_KEYARGS = SCM_ARGREF(SCM_ARGCNT-1);
  SCM_ENTER_SUBR("sys-pipe");
  if (Scm_Length(SCM_KEYARGS) % 2)
    Scm_Error("keyword list not even: %S", SCM_KEYARGS);
  while (!SCM_NULLP(SCM_KEYARGS)) {
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[961])) {
      name_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[962])) {
      buffering_scm = SCM_CADR(SCM_KEYARGS);
    } else 
    if (SCM_EQ(SCM_CAR(SCM_KEYARGS), scm__rc.d1786[963])) {
      bufferedP_scm = SCM_CADR(SCM_KEYARGS);
    }
    else Scm_Warn("unknown keyword %S", SCM_CAR(SCM_KEYARGS));
    SCM_KEYARGS = SCM_CDDR(SCM_KEYARGS);
  }
  if (!(name_scm)) Scm_Error("scheme object required, but got %S", name_scm);
  name = (name_scm);
  if (!(buffering_scm)) Scm_Error("scheme object required, but got %S", buffering_scm);
  buffering = (buffering_scm);
  if (!(bufferedP_scm)) Scm_Error("scheme object required, but got %S", bufferedP_scm);
  bufferedP = (bufferedP_scm);
  {
ScmObj SCM_RESULT0;
ScmObj SCM_RESULT1;
{

#line 1207 "libsys.scm"
{int fds[2];int mode;int r;
SCM_SYSCALL(r,pipe(fds));
if ((r)<(0)){{Scm_SysError("pipe failed");}}
if (SCM_TRUEP(bufferedP)){
mode=(SCM_PORT_BUFFER_FULL);} else {
mode=(Scm_BufferingMode(buffering,-1,SCM_PORT_BUFFER_LINE));}
{SCM_RESULT0=(Scm_MakePortWithFd(name,SCM_PORT_INPUT,(fds)[0],mode,TRUE)),SCM_RESULT1=(
Scm_MakePortWithFd(name,SCM_PORT_OUTPUT,(fds)[1],mode,TRUE));goto SCM_STUB_RETURN;}}
}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_Values2(SCM_OBJ_SAFE(SCM_RESULT0),SCM_OBJ_SAFE(SCM_RESULT1)));
  }
}


static ScmObj libsyssys_close(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj fd_scm;
  int fd;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-close");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  fd_scm = SCM_SUBRARGS[0];
  if (!SCM_INTEGERP(fd_scm)) Scm_Error("int required, but got %S", fd_scm);
  fd = Scm_GetInteger(fd_scm);
  {

#line 1224 "libsys.scm"
{int r=close(fd);
if ((r)<(0)){{Scm_SysError("close failed on file descriptor %d",fd);}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsyssys_mkdir(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj pathname_scm;
  const char* pathname;
  ScmObj mode_scm;
  int mode;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-mkdir");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  pathname_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(pathname_scm)) Scm_Error("const char* required, but got %S", pathname_scm);
  pathname = SCM_STRING_CONST_CSTRING(pathname_scm);
  mode_scm = SCM_SUBRARGS[1];
  if (!SCM_INTEGERP(mode_scm)) Scm_Error("int required, but got %S", mode_scm);
  mode = Scm_GetInteger(mode_scm);
  {

#line 1228 "libsys.scm"
{int r;
((void )(mode));

#if !(defined(GAUCHE_WINDOWS))

#line 1231 "libsys.scm"
SCM_SYSCALL(r,mkdir(pathname,mode));
#else /* !!(defined(GAUCHE_WINDOWS)) */

#line 1232 "libsys.scm"
SCM_SYSCALL(r,mkdir(pathname));
#endif /* !(defined(GAUCHE_WINDOWS)) */

#line 1233 "libsys.scm"
if ((r)<(0)){{Scm_SysError("mkdir failed on %s",pathname);}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsyssys_rmdir(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj pathname_scm;
  const char* pathname;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-rmdir");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  pathname_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(pathname_scm)) Scm_Error("const char* required, but got %S", pathname_scm);
  pathname = SCM_STRING_CONST_CSTRING(pathname_scm);
  {

#line 1236 "libsys.scm"
{int r;
SCM_SYSCALL(r,rmdir(pathname));
if ((r)<(0)){{Scm_SysError("rmdir failed for %s",pathname);}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsyssys_umask(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj mode_scm;
  ScmObj mode;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-umask");
  if (SCM_ARGCNT >= 2
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 1 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  if (SCM_ARGCNT > 0+1) {
    mode_scm = SCM_SUBRARGS[0];
  } else {
    mode_scm = SCM_UNBOUND;
  }
  if (!(mode_scm)) Scm_Error("scheme object required, but got %S", mode_scm);
  mode = (mode_scm);
  {
{
int SCM_RESULT;

#line 1241 "libsys.scm"
if ((SCM_UNBOUNDP(mode))||(SCM_FALSEP(mode))){
{int prev=umask(0);
umask(prev);
{SCM_RESULT=(prev);goto SCM_STUB_RETURN;}}}else if(
SCM_INTP(mode)){{SCM_RESULT=(umask(SCM_INT_VALUE(mode)));goto SCM_STUB_RETURN;}} else {
SCM_TYPE_ERROR(mode,"fixnum or #f");{SCM_RESULT=(0);goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_sleep(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj seconds_scm;
  ScmSmallInt seconds;
  ScmObj no_retry_scm;
  int no_retry;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-sleep");
  if (SCM_ARGCNT >= 3
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 2 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  seconds_scm = SCM_SUBRARGS[0];
  if (!SCM_INTP(seconds_scm)) Scm_Error("ScmSmallInt required, but got %S", seconds_scm);
  seconds = SCM_INT_VALUE(seconds_scm);
  if (SCM_ARGCNT > 1+1) {
    no_retry_scm = SCM_SUBRARGS[1];
  } else {
    no_retry_scm = SCM_FALSE;
  }
  if (!SCM_BOOLP(no_retry_scm)) Scm_Error("boolean required, but got %S", no_retry_scm);
  no_retry = SCM_BOOL_VALUE(no_retry_scm);
  {
{
int SCM_RESULT;

#line 1251 "libsys.scm"
((void )(no_retry));

#line 1252 "libsys.scm"

#if defined(GAUCHE_WINDOWS)

#line 1253 "libsys.scm"
{Sleep((seconds)*(1000));{SCM_RESULT=(0);goto SCM_STUB_RETURN;}}
#else /* !defined(GAUCHE_WINDOWS) */

#line 1254 "libsys.scm"
{u_int k=((u_int )(seconds));ScmVM* vm=
Scm_VM();
while((k)>(0)){
k=(sleep(k));
SCM_SIGCHECK(vm);
if (no_retry){{break;}}}
{SCM_RESULT=(k);goto SCM_STUB_RETURN;}}
#endif /* defined(GAUCHE_WINDOWS) */

goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}

#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))

static ScmObj libsyssys_nanosleep(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj nanoseconds_scm;
  ScmObj nanoseconds;
  ScmObj no_retry_scm;
  int no_retry;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-nanosleep");
  if (SCM_ARGCNT >= 3
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 2 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  nanoseconds_scm = SCM_SUBRARGS[0];
  if (!(nanoseconds_scm)) Scm_Error("scheme object required, but got %S", nanoseconds_scm);
  nanoseconds = (nanoseconds_scm);
  if (SCM_ARGCNT > 1+1) {
    no_retry_scm = SCM_SUBRARGS[1];
  } else {
    no_retry_scm = SCM_FALSE;
  }
  if (!SCM_BOOLP(no_retry_scm)) Scm_Error("boolean required, but got %S", no_retry_scm);
  no_retry = SCM_BOOL_VALUE(no_retry_scm);
  {
{
ScmObj SCM_RESULT;

#line 1266 "libsys.scm"
{ScmTimeSpec spec;ScmTimeSpec rem;ScmVM* vm=
Scm_VM();
if (
SCM_TIMEP(nanoseconds)){
(spec).tv_sec=((SCM_TIME(nanoseconds))->sec),
(spec).tv_nsec=((SCM_TIME(nanoseconds))->nsec);}else if(
!(SCM_REALP(nanoseconds))){
Scm_Error("bad timeout spec: <time> object or real number is required, but got %S",nanoseconds);} else {
#line 1276 "libsys.scm"
{double v=Scm_GetDouble(nanoseconds);
if ((v)<(0)){{
Scm_Error("bad timeout spec: positive number required, but got %S",nanoseconds);}}
#line 1280 "libsys.scm"
(spec).tv_sec=(((unsigned long )(floor((v)/(1.0e9))))),
(spec).tv_nsec=(((unsigned long )(fmod(v,1.0e9))));
while(((spec).tv_nsec)>=(1000000000)){
((spec).tv_nsec)-=(1000000000);
((spec).tv_sec)+=(1);}}}
(rem).tv_sec=(0),(rem).tv_nsec=(0);
while((Scm_NanoSleep(&(spec),&(rem)))<(0)){
if (!((errno)==(EINTR))){{
Scm_SysError("nanosleep failed");}}
SCM_SIGCHECK(vm);
if (no_retry){{break;}}
spec=(rem);
(rem).tv_sec=(0),(rem).tv_nsec=(0);}
if ((((rem).tv_sec)==(0))&&(((rem).tv_nsec)==(0))){
{SCM_RESULT=(SCM_FALSE);goto SCM_STUB_RETURN;}} else {
{SCM_RESULT=(Scm_MakeTime(SCM_FALSE,(rem).tv_sec,(rem).tv_nsec));goto SCM_STUB_RETURN;}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */

static ScmObj libsyssys_unlink(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj pathname_scm;
  const char* pathname;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-unlink");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  pathname_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(pathname_scm)) Scm_Error("const char* required, but got %S", pathname_scm);
  pathname = SCM_STRING_CONST_CSTRING(pathname_scm);
  {
{
ScmObj SCM_RESULT;

#line 1300 "libsys.scm"
{int r;

#if defined(GAUCHE_WINDOWS)

#line 1304 "libsys.scm"
if (!(access(pathname,F_OK))){{
chmod(pathname,384);}}
#endif /* defined(GAUCHE_WINDOWS) */

#line 1306 "libsys.scm"
SCM_SYSCALL(r,unlink(pathname));
if ((r)<(0)){
{
if (!((errno)==(ENOENT))){{
Scm_SysError("unlink failed on %s",pathname);}}
{SCM_RESULT=(SCM_FALSE);goto SCM_STUB_RETURN;}}} else {
{SCM_RESULT=(SCM_TRUE);goto SCM_STUB_RETURN;}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_isatty(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_or_fd_scm;
  ScmObj port_or_fd;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-isatty");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_or_fd_scm = SCM_SUBRARGS[0];
  if (!(port_or_fd_scm)) Scm_Error("scheme object required, but got %S", port_or_fd_scm);
  port_or_fd = (port_or_fd_scm);
  {
{
int SCM_RESULT;

#line 1315 "libsys.scm"
{int fd=Scm_GetPortFd(port_or_fd,FALSE);
{SCM_RESULT=(((fd)>=(0))&&(isatty(fd)));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_ttyname(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_or_fd_scm;
  ScmObj port_or_fd;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-ttyname");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_or_fd_scm = SCM_SUBRARGS[0];
  if (!(port_or_fd_scm)) Scm_Error("scheme object required, but got %S", port_or_fd_scm);
  port_or_fd = (port_or_fd_scm);
  {
{
const char* SCM_RESULT;

#line 1319 "libsys.scm"
{int fd=Scm_GetPortFd(port_or_fd,FALSE);
{SCM_RESULT=((((fd)<(0))?(NULL):(ttyname(fd))));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_MAYBE(SCM_MAKE_STR_COPYING, SCM_RESULT));
}
  }
}


static ScmObj libsyssys_truncate(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj path_scm;
  const char* path;
  ScmObj length_scm;
  ScmObj length;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-truncate");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  path_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(path_scm)) Scm_Error("const char* required, but got %S", path_scm);
  path = SCM_STRING_CONST_CSTRING(path_scm);
  length_scm = SCM_SUBRARGS[1];
  if (!SCM_INTEGERP(length_scm)) Scm_Error("exact integer required, but got %S", length_scm);
  length = (length_scm);
  {

#line 1324 "libsys.scm"
{int r;
SCM_SYSCALL(r,truncate(path,Scm_IntegerToOffset(length)));
if ((r)<(0)){{Scm_SysError("truncate failed on %s",path);}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj libsyssys_ftruncate(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_or_fd_scm;
  ScmObj port_or_fd;
  ScmObj length_scm;
  ScmObj length;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-ftruncate");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_or_fd_scm = SCM_SUBRARGS[0];
  if (!(port_or_fd_scm)) Scm_Error("scheme object required, but got %S", port_or_fd_scm);
  port_or_fd = (port_or_fd_scm);
  length_scm = SCM_SUBRARGS[1];
  if (!SCM_INTEGERP(length_scm)) Scm_Error("exact integer required, but got %S", length_scm);
  length = (length_scm);
  {

#line 1329 "libsys.scm"
{int r;int fd=Scm_GetPortFd(port_or_fd,TRUE);
SCM_SYSCALL(r,ftruncate(fd,Scm_IntegerToOffset(length)));
if ((r)<(0)){{Scm_SysError("ftruncate failed on %S",port_or_fd);}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}

#if defined(HAVE_CRYPT)

static ScmObj libsyssys_crypt(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj key_scm;
  const char* key;
  ScmObj salt_scm;
  const char* salt;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-crypt");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  key_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(key_scm)) Scm_Error("const char* required, but got %S", key_scm);
  key = SCM_STRING_CONST_CSTRING(key_scm);
  salt_scm = SCM_SUBRARGS[1];
  if (!SCM_STRINGP(salt_scm)) Scm_Error("const char* required, but got %S", salt_scm);
  salt = SCM_STRING_CONST_CSTRING(salt_scm);
  {
{
const char* SCM_RESULT;

#line 1339 "libsys.scm"
{SCM_RESULT=(((const char * )(crypt(key,salt))));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_STR_COPYING(SCM_RESULT));
}
  }
}

#endif /* defined(HAVE_CRYPT) */
#if !(defined(HOSTNAMELEN))
#define HOSTNAMELEN (1024)

#endif /* !(defined(HOSTNAMELEN)) */

static ScmObj libsyssys_gethostname(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-gethostname");
  {
{
ScmObj SCM_RESULT;

#line 1348 "libsys.scm"

#if defined(HAVE_GETHOSTNAME)

#line 1349 "libsys.scm"
{char buf[HOSTNAMELEN];int r;
SCM_SYSCALL(r,gethostname(buf,HOSTNAMELEN));
if ((r)<(0)){{Scm_SysError("gethostname failed");}}
{SCM_RESULT=(SCM_MAKE_STR_COPYING(buf));goto SCM_STUB_RETURN;}}
#else /* !defined(HAVE_GETHOSTNAME) */

#line 1354 "libsys.scm"
{SCM_RESULT=(SCM_MAKE_STR_IMMUTABLE("localhost"));goto SCM_STUB_RETURN;}
#endif /* defined(HAVE_GETHOSTNAME) */

goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj libsyssys_getdomainname(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-getdomainname");
  {
{
ScmObj SCM_RESULT;

#line 1357 "libsys.scm"

#if defined(HAVE_GETDOMAINNAME)

#line 1358 "libsys.scm"
{char buf[HOSTNAMELEN];int r;
SCM_SYSCALL(r,getdomainname(buf,HOSTNAMELEN));
if ((r)<(0)){{Scm_SysError("getdomainame failed");}}
{SCM_RESULT=(SCM_MAKE_STR_COPYING(buf));goto SCM_STUB_RETURN;}}
#else /* !defined(HAVE_GETDOMAINNAME) */

#line 1363 "libsys.scm"
{SCM_RESULT=(SCM_MAKE_STR_IMMUTABLE("local"));goto SCM_STUB_RETURN;}
#endif /* defined(HAVE_GETDOMAINNAME) */

goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#if defined(HAVE_SYMLINK)

static ScmObj libsyssys_symlink(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj existing_scm;
  const char* existing;
  ScmObj newpath_scm;
  const char* newpath;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-symlink");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  existing_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(existing_scm)) Scm_Error("const char* required, but got %S", existing_scm);
  existing = SCM_STRING_CONST_CSTRING(existing_scm);
  newpath_scm = SCM_SUBRARGS[1];
  if (!SCM_STRINGP(newpath_scm)) Scm_Error("const char* required, but got %S", newpath_scm);
  newpath = SCM_STRING_CONST_CSTRING(newpath_scm);
  {

#line 1376 "libsys.scm"
{int r;
SCM_SYSCALL(r,symlink(existing,newpath));
if ((r)<(0)){{
Scm_SysError("symlink from %s to %s failed",newpath,existing);}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}

#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_READLINK)

static ScmObj libsyssys_readlink(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj path_scm;
  const char* path;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-readlink");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  path_scm = SCM_SUBRARGS[0];
  if (!SCM_STRINGP(path_scm)) Scm_Error("const char* required, but got %S", path_scm);
  path = SCM_STRING_CONST_CSTRING(path_scm);
  {
{
ScmObj SCM_RESULT;

#line 1384 "libsys.scm"
{char buf[1024];int n;
#line 1386 "libsys.scm"
SCM_SYSCALL(n,readlink(path,buf,1024));
if ((n)<(0)){{Scm_SysError("readlink failed on %s",path);}}
if ((n)==(1024)){{Scm_Error("readlink result too long on %s",path);}}
{SCM_RESULT=(Scm_MakeString(buf,n,-1,SCM_STRING_COPYING));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_SELECT)

static ScmObj libsyssys_fdset_ref(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj fdset_scm;
  ScmSysFdset* fdset;
  ScmObj pf_scm;
  ScmObj pf;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-fdset-ref");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  fdset_scm = SCM_SUBRARGS[0];
  if (!SCM_SYS_FDSET_P(fdset_scm)) Scm_Error("<sys-fdset> required, but got %S", fdset_scm);
  fdset = SCM_SYS_FDSET(fdset_scm);
  pf_scm = SCM_SUBRARGS[1];
  if (!(pf_scm)) Scm_Error("scheme object required, but got %S", pf_scm);
  pf = (pf_scm);
  {
{
int SCM_RESULT;

#line 1413 "libsys.scm"
{int fd=Scm_GetPortFd(pf,FALSE);
if ((fd)<(0)){
{SCM_RESULT=(TRUE);goto SCM_STUB_RETURN;}} else {
{
#if !(defined(GAUCHE_WINDOWS))
{int G1890=fd;if (((G1890)<(0))||((G1890)>=(FD_SETSIZE))){{Scm_Error("File descriptor value is out of range: %d (must be between 0 and %d, inclusive)",G1890,(FD_SETSIZE)-(1));}}}
#endif /* ! defined(GAUCHE_WINDOWS) */

#line 1417 "libsys.scm"
{SCM_RESULT=(FD_ISSET(fd,&((fdset)->fdset)));goto SCM_STUB_RETURN;}}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}

#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)

static ScmObj libsyssys_fdset_setX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj fdset_scm;
  ScmSysFdset* fdset;
  ScmObj pf_scm;
  ScmObj pf;
  ScmObj flag_scm;
  int flag;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("sys-fdset-set!");
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  fdset_scm = SCM_SUBRARGS[0];
  if (!SCM_SYS_FDSET_P(fdset_scm)) Scm_Error("<sys-fdset> required, but got %S", fdset_scm);
  fdset = SCM_SYS_FDSET(fdset_scm);
  pf_scm = SCM_SUBRARGS[1];
  if (!(pf_scm)) Scm_Error("scheme object required, but got %S", pf_scm);
  pf = (pf_scm);
  flag_scm = SCM_SUBRARGS[2];
  if (!SCM_BOOLP(flag_scm)) Scm_Error("boolean required, but got %S", flag_scm);
  flag = SCM_BOOL_VALUE(flag_scm);
  {

#line 1420 "libsys.scm"
{int fd=Scm_GetPortFd(pf,FALSE);
if ((fd)>=(0)){{

#if !(defined(GAUCHE_WINDOWS))
{int G1893=fd;if (((G1893)<(0))||((G1893)>=(FD_SETSIZE))){{Scm_Error("File descriptor value is out of range: %d (must be between 0 and %d, inclusive)",G1893,(FD_SETSIZE)-(1));}}}
#endif /* ! defined(GAUCHE_WINDOWS) */

#line 1423 "libsys.scm"
if (flag){FD_SET(fd,&((fdset)->fdset));
if (((fdset)->maxfd)<(fd)){{(fdset)->maxfd=(fd);}}} else {
FD_CLR(fd,&((fdset)->fdset));
if (((fdset)->maxfd)==(fd)){{
{int i=((fdset)->maxfd)-(1);
for (; (i)>=(0); (i)--){
if (FD_ISSET(i,&((fdset)->fdset))){{break;}}}
(fdset)->maxfd=(i);}}}}}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}

#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)

static ScmObj libsyssys_fdset_max_fd(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj fdset_scm;
  ScmSysFdset* fdset;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-fdset-max-fd");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  fdset_scm = SCM_SUBRARGS[0];
  if (!SCM_SYS_FDSET_P(fdset_scm)) Scm_Error("<sys-fdset> required, but got %S", fdset_scm);
  fdset = SCM_SYS_FDSET(fdset_scm);
  {
{
int SCM_RESULT;

#line 1433 "libsys.scm"
{SCM_RESULT=((fdset)->maxfd);goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}

#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)

static ScmObj libsyssys_fdset_clearX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj fdset_scm;
  ScmSysFdset* fdset;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-fdset-clear!");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  fdset_scm = SCM_SUBRARGS[0];
  if (!SCM_SYS_FDSET_P(fdset_scm)) Scm_Error("<sys-fdset> required, but got %S", fdset_scm);
  fdset = SCM_SYS_FDSET(fdset_scm);
  {
{
ScmObj SCM_RESULT;

#line 1436 "libsys.scm"
FD_ZERO(&((fdset)->fdset));

#line 1437 "libsys.scm"
(fdset)->maxfd=(-1);

#line 1438 "libsys.scm"
{SCM_RESULT=(SCM_OBJ(fdset));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)

static ScmObj libsyssys_fdset_copyX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj dst_scm;
  ScmSysFdset* dst;
  ScmObj src_scm;
  ScmSysFdset* src;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("sys-fdset-copy!");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  dst_scm = SCM_SUBRARGS[0];
  if (!SCM_SYS_FDSET_P(dst_scm)) Scm_Error("<sys-fdset> required, but got %S", dst_scm);
  dst = SCM_SYS_FDSET(dst_scm);
  src_scm = SCM_SUBRARGS[1];
  if (!SCM_SYS_FDSET_P(src_scm)) Scm_Error("<sys-fdset> required, but got %S", src_scm);
  src = SCM_SYS_FDSET(src_scm);
  {
{
ScmObj SCM_RESULT;

#line 1441 "libsys.scm"
(dst)->fdset=((src)->fdset),
(dst)->maxfd=((src)->maxfd);

#line 1443 "libsys.scm"
{SCM_RESULT=(SCM_OBJ(dst));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)

static ScmObj libsyssys_select(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj rfds_scm;
  ScmObj rfds;
  ScmObj wfds_scm;
  ScmObj wfds;
  ScmObj efds_scm;
  ScmObj efds;
  ScmObj timeout_scm;
  ScmObj timeout;
  ScmObj SCM_SUBRARGS[4];
  SCM_ENTER_SUBR("sys-select");
  if (SCM_ARGCNT >= 5
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 4 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<4; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  rfds_scm = SCM_SUBRARGS[0];
  if (!(rfds_scm)) Scm_Error("scheme object required, but got %S", rfds_scm);
  rfds = (rfds_scm);
  wfds_scm = SCM_SUBRARGS[1];
  if (!(wfds_scm)) Scm_Error("scheme object required, but got %S", wfds_scm);
  wfds = (wfds_scm);
  efds_scm = SCM_SUBRARGS[2];
  if (!(efds_scm)) Scm_Error("scheme object required, but got %S", efds_scm);
  efds = (efds_scm);
  if (SCM_ARGCNT > 3+1) {
    timeout_scm = SCM_SUBRARGS[3];
  } else {
    timeout_scm = SCM_FALSE;
  }
  if (!(timeout_scm)) Scm_Error("scheme object required, but got %S", timeout_scm);
  timeout = (timeout_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SysSelect(rfds,wfds,efds,timeout));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)

static ScmObj libsyssys_selectX(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj rfds_scm;
  ScmObj rfds;
  ScmObj wfds_scm;
  ScmObj wfds;
  ScmObj efds_scm;
  ScmObj efds;
  ScmObj timeout_scm;
  ScmObj timeout;
  ScmObj SCM_SUBRARGS[4];
  SCM_ENTER_SUBR("sys-select!");
  if (SCM_ARGCNT >= 5
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 4 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<4; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  rfds_scm = SCM_SUBRARGS[0];
  if (!(rfds_scm)) Scm_Error("scheme object required, but got %S", rfds_scm);
  rfds = (rfds_scm);
  wfds_scm = SCM_SUBRARGS[1];
  if (!(wfds_scm)) Scm_Error("scheme object required, but got %S", wfds_scm);
  wfds = (wfds_scm);
  efds_scm = SCM_SUBRARGS[2];
  if (!(efds_scm)) Scm_Error("scheme object required, but got %S", efds_scm);
  efds = (efds_scm);
  if (SCM_ARGCNT > 3+1) {
    timeout_scm = SCM_SUBRARGS[3];
  } else {
    timeout_scm = SCM_FALSE;
  }
  if (!(timeout_scm)) Scm_Error("scheme object required, but got %S", timeout_scm);
  timeout = (timeout_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_SysSelectX(rfds,wfds,efds,timeout));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#endif /* defined(HAVE_SELECT) */

static ScmObj libsyssys_available_processors(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  SCM_ENTER_SUBR("sys-available-processors");
  {
{
int SCM_RESULT;
{SCM_RESULT=(Scm_AvailableProcessors());goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}

#if defined(GAUCHE_WINDOWS)
static void handle_cleanup(ScmObj h){{
#line 1674 "libsys.scm"
CloseHandle(SCM_FOREIGN_POINTER_REF(HANDLE,h));}}
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
static void handle_print(ScmObj h,ScmPort* p,ScmWriteContext* G1924 SCM_UNUSED){{
#line 1677 "libsys.scm"
{ScmObj type=Scm_ForeignPointerAttrGet(SCM_FOREIGN_POINTER(h),
scm__rc.d1925[0],SCM_FALSE);
if (
SCM_EQ(type,scm__rc.d1925[1])){
Scm_Printf(p,"#<win:handle process %d @%p>",Scm_WinProcessPID(h),h);} else {
#line 1683 "libsys.scm"
Scm_Printf(p,"#<win:handle @%p>",h);}}}}
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
 ScmObj Scm_MakeWinHandle(HANDLE wh,ScmObj type){{
#line 1692 "libsys.scm"
{ScmObj h=Scm_MakeForeignPointer(WinHandleClass,wh);
if (!(SCM_FALSEP(type))){{
Scm_ForeignPointerAttrSet(SCM_FOREIGN_POINTER(h),scm__rc.d1925[0],type);}}
return (h);}}}
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
 int Scm_WinHandleP(ScmObj obj,ScmObj type){{
#line 1698 "libsys.scm"
if (!(SCM_XTYPEP(obj,WinHandleClass))){{return (FALSE);}}
return (
(SCM_FALSEP(type))||(
SCM_EQ(type,Scm_ForeignPointerAttrGet(SCM_FOREIGN_POINTER(obj),
scm__rc.d1925[0],SCM_FALSE))));}}
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
 HANDLE Scm_WinHandle(ScmObj h,ScmObj type){{
#line 1705 "libsys.scm"
if (!(Scm_WinHandleP(h,type))){{SCM_TYPE_ERROR(h,"<win:handle>");}}
return (SCM_FOREIGN_POINTER_REF(HANDLE,h));}}
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
 ScmObj Scm_MakeWinProcess(HANDLE h){{
#line 1711 "libsys.scm"
return (Scm_MakeWinHandle(h,scm__rc.d1925[1]));}}
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
 int Scm_WinProcessP(ScmObj obj){{
#line 1714 "libsys.scm"
return (Scm_WinHandleP(obj,scm__rc.d1925[1]));}}
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
 HANDLE Scm_WinProcess(ScmObj obj){{
#line 1717 "libsys.scm"
return (Scm_WinHandle(obj,scm__rc.d1925[1]));}}
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)

static ScmObj libsyssys_win_processP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-win-process?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(Scm_WinProcessP(obj));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}

#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)

static ScmObj libsyssys_win_process_pid(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-win-process-pid");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {
{
int SCM_RESULT;
{SCM_RESULT=(Scm_WinProcessPID(obj));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(Scm_MakeInteger(SCM_RESULT));
}
  }
}

#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)

static ScmObj libsyssys_get_osfhandle(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_or_fd_scm;
  ScmObj port_or_fd;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-get-osfhandle");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_or_fd_scm = SCM_SUBRARGS[0];
  if (!(port_or_fd_scm)) Scm_Error("scheme object required, but got %S", port_or_fd_scm);
  port_or_fd = (port_or_fd_scm);
  {
{
ScmObj SCM_RESULT;

#line 1725 "libsys.scm"
{int fd=Scm_GetPortFd(port_or_fd,TRUE);HANDLE h=
((HANDLE )(_get_osfhandle(fd)));
if ((h)==(INVALID_HANDLE_VALUE)){{Scm_SysError("get_osfhandle failed");}}
{SCM_RESULT=(Scm_MakeWinHandle(h,SCM_FALSE));goto SCM_STUB_RETURN;}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)

static ScmObj libsyssys_win_pipe_name(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj port_or_fd_scm;
  ScmObj port_or_fd;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("sys-win-pipe-name");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  port_or_fd_scm = SCM_SUBRARGS[0];
  if (!(port_or_fd_scm)) Scm_Error("scheme object required, but got %S", port_or_fd_scm);
  port_or_fd = (port_or_fd_scm);
  {
{
ScmObj SCM_RESULT;

#line 1731 "libsys.scm"
{int fd=Scm_GetPortFd(port_or_fd,FALSE);
if ((fd)<(0)){
{SCM_RESULT=(SCM_FALSE);goto SCM_STUB_RETURN;}} else {
{SCM_RESULT=(Scm_WinGetPipeName(((HANDLE )(_get_osfhandle(fd)))));goto SCM_STUB_RETURN;}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

#endif /* defined(GAUCHE_WINDOWS) */
static ScmCompiledCode *toplevels[] = {
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[1])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[3])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[6])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[8])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[10])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[13])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[15])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[16])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[18])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[19])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[22])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[28])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[31])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[41])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[45])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[47])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[52])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[53])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[55])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[58])),
 NULL /*termination*/
};
ScmObj SCM_debug_info_const_vector()
{
  static _Bool initialized = FALSE;
  if (!initialized) {
    int i = 0;
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1568];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1569];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1570];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1571];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1572];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[78];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[51]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[76];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1573];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[75];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[74];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1574];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[73];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[46]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[598];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[67];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[65];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_MAKE_CHAR(47);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[18]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1575];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1576];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1577];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[40];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[70];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[69];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[42];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[22]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[43]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1578];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[42]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1579];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1087];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1115];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[63];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[82];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1580];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1581];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1582];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[58];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[62];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[60];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[59];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[80];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1583];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1584];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1238];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[56];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[31]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[51];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1585];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[49];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[55];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[33]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1586];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[48];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[46];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[44];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[26]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[24]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1587];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1588];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1589];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[81];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[38];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[19]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1590];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[33];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[35];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[32];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1591];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[79];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1592];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1593];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1594];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[4];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[30];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[313];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[421];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[318];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[423];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[417];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[532];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[541];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[542];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[540];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[529];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_MAKE_CHAR(61);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[527];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[538];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[537];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[531];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[296]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[358];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[508];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1595];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1596];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1597];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[530];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[510];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[555];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[556];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[100];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[511];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[310]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[308]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[552];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1598];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1599];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[551];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[729];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[751];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[746];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[748];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[750];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1600];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[745];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[743];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1601];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[741];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1602];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[779];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[780];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[781];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1603];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[776];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[789];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[718];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[787];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[932];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[930];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1055];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1057];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1058];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1053];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1063];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1072];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1073];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[357];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1074];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1088];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1090];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1604];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1605];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1606];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1607];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1608];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1609];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1086];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1085];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1084];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1083];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1089];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1082];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1610];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1611];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[505];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1078];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1080];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1070];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1068];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1066];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1612];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1095];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1096];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1099];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1101];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1094];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1102];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1103];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1106];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1613];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1109];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1110];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1117];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1119];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[471]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1118];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1614];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1615];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1616];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1125];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_MAKE_CHAR(44);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1135];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1132];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1617];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1618];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1139];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1619];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1620];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1141];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1621];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1622];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1143];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1623];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1148];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[497]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1624];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1625];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1626];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1627];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1150];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1138];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1628];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[494]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1130];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[487]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1127];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1629];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1630];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1149];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1631];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1632];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1145];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_MAKE_CHAR(123);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1164];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1162];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1633];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1165];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1634];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1159];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1635];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1161];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_MAKE_CHAR(33);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1636];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1637];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_MAKE_CHAR(91);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1157];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1638];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_MAKE_CHAR(63);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1639];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_MAKE_CHAR(42);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1640];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1155];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1641];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1642];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1643];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1167];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1644];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1645];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1156];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1169];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_MAKE_CHAR(46);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1646];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1177];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1175];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1173];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1163];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1647];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1168];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1648];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1185];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1649];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1650];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1651];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1652];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1025];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1183];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1186];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1653];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1181];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1200];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1654];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1655];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1656];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1196];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1198];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1657];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1658];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[630];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1209];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1205];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1210];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[0];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1659];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1660];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[618];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1661];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1204];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1662];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1663];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1664];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1214];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1213];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1199];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1216];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1215];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1194];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1193];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1665];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1191];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1229];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1227];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1225];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1223];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[609];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1222];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1235];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[319];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_MAKE_CHAR(92);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[539]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1236];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1248];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1246];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1244];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1237];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1243];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_MAKE_CHAR(32);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[545]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1241];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = scm__rc.d1786[1250];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[1254]), i++) = SCM_OBJ(&scm__sc.d1785[543]);
    initialized = TRUE;
  }
  return SCM_OBJ(&scm__rc.d1786[1254]);
}
void Scm_Init_libsys() {
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[0])),TRUE); /* sys-readdir */
  scm__rc.d1786[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[1])),TRUE); /* pathname */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1]), scm__rc.d1786[1]);
  scm__rc.d1786[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[2])),TRUE); /* source-info */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[4]), scm__rc.d1786[2]);
  scm__rc.d1786[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[4])),TRUE); /* bind-info */
  scm__rc.d1786[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[5])),TRUE); /* gauche */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[5]), scm__rc.d1786[0]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[6]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[7]), scm__rc.d1786[3]);
  scm__rc.d1786[5] = Scm_MakeExtendedPair(scm__rc.d1786[0], SCM_OBJ(&scm__rc.d1787[1]), SCM_OBJ(&scm__rc.d1787[9]));
  scm__rc.d1786[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[6])),TRUE); /* <string> */
  scm__rc.d1786[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[7])),TRUE); /* -> */
  scm__rc.d1786[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[8])),TRUE); /* <top> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[9]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[9]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[9]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[9]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-readdir")), SCM_OBJ(&libsyssys_readdir__STUB), 0);
  libsyssys_readdir__STUB.common.info = scm__rc.d1786[5];
  libsyssys_readdir__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[9]);
  scm__rc.d1786[16] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[9])),TRUE); /* sys-tmpdir */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[10]), scm__rc.d1786[16]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[11]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[12]), scm__rc.d1786[3]);
  scm__rc.d1786[17] = Scm_MakeExtendedPair(scm__rc.d1786[16], SCM_NIL, SCM_OBJ(&scm__rc.d1787[13]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[18]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[18]))[4] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[18]))[5] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-tmpdir")), SCM_OBJ(&libsyssys_tmpdir__STUB), 0);
  libsyssys_tmpdir__STUB.common.info = scm__rc.d1786[17];
  libsyssys_tmpdir__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[18]);
  scm__rc.d1786[24] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[10])),TRUE); /* sys-basename */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[16]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[17]), scm__rc.d1786[24]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[18]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[19]), scm__rc.d1786[3]);
  scm__rc.d1786[25] = Scm_MakeExtendedPair(scm__rc.d1786[24], SCM_OBJ(&scm__rc.d1787[1]), SCM_OBJ(&scm__rc.d1787[21]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-basename")), SCM_OBJ(&libsyssys_basename__STUB), 0);
  libsyssys_basename__STUB.common.info = scm__rc.d1786[25];
  libsyssys_basename__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[9]);
  scm__rc.d1786[26] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[11])),TRUE); /* sys-dirname */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[24]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[25]), scm__rc.d1786[26]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[26]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[27]), scm__rc.d1786[3]);
  scm__rc.d1786[27] = Scm_MakeExtendedPair(scm__rc.d1786[26], SCM_OBJ(&scm__rc.d1787[1]), SCM_OBJ(&scm__rc.d1787[29]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-dirname")), SCM_OBJ(&libsyssys_dirname__STUB), 0);
  libsyssys_dirname__STUB.common.info = scm__rc.d1786[27];
  libsyssys_dirname__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[9]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
  scm__rc.d1786[29] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[12])),TRUE); /* %expression-name-mark-key */
  scm__rc.d1786[28] = Scm_MakeIdentifier(scm__rc.d1786[29], SCM_MODULE(Scm_GaucheInternalModule()), SCM_NIL); /* gauche.internal#%expression-name-mark-key */
  scm__rc.d1786[30] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[13])),TRUE); /* sys-normalize-pathname */
  scm__rc.d1786[32] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[14])),TRUE); /* undefined? */
  scm__rc.d1786[31] = Scm_MakeIdentifier(scm__rc.d1786[32], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#undefined? */
  scm__rc.d1786[33] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[15])),TRUE); /* gauche.os.windows */
  scm__rc.d1786[35] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[16])),TRUE); /* cond-features */
  scm__rc.d1786[37] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[17])),TRUE); /* gauche.internal */
  scm__rc.d1786[36] = SCM_OBJ(Scm_FindModule(SCM_SYMBOL(scm__rc.d1786[37]), SCM_FIND_MODULE_CREATE|SCM_FIND_MODULE_PLACEHOLDING)); /* module gauche.internal */
  scm__rc.d1786[34] = Scm_MakeIdentifier(scm__rc.d1786[35], SCM_MODULE(scm__rc.d1786[36]), SCM_NIL); /* gauche.internal#cond-features */
  scm__rc.d1786[38] = Scm_RegComp(SCM_STRING(SCM_OBJ(&scm__sc.d1785[20])), 0);
  scm__rc.d1786[40] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[21])),TRUE); /* rxmatch */
  scm__rc.d1786[39] = Scm_MakeIdentifier(scm__rc.d1786[40], SCM_MODULE(scm__rc.d1786[36]), SCM_NIL); /* gauche.internal#rxmatch */
  scm__rc.d1786[42] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[23])),TRUE); /* equal? */
  scm__rc.d1786[41] = Scm_MakeIdentifier(scm__rc.d1786[42], SCM_MODULE(scm__rc.d1786[36]), SCM_NIL); /* gauche.internal#equal? */
  scm__rc.d1786[44] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[25])),TRUE); /* sys-getenv */
  scm__rc.d1786[43] = Scm_MakeIdentifier(scm__rc.d1786[44], SCM_MODULE(scm__rc.d1786[36]), SCM_NIL); /* gauche.internal#sys-getenv */
  scm__rc.d1786[46] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[27])),TRUE); /* sys-getuid */
  scm__rc.d1786[45] = Scm_MakeIdentifier(scm__rc.d1786[46], SCM_MODULE(scm__rc.d1786[36]), SCM_NIL); /* gauche.internal#sys-getuid */
  scm__rc.d1786[48] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[28])),TRUE); /* sys-getpwuid */
  scm__rc.d1786[47] = Scm_MakeIdentifier(scm__rc.d1786[48], SCM_MODULE(scm__rc.d1786[36]), SCM_NIL); /* gauche.internal#sys-getpwuid */
  scm__rc.d1786[49] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[29])),TRUE); /* dir */
  scm__rc.d1786[51] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[30])),TRUE); /* ~ */
  scm__rc.d1786[50] = Scm_MakeIdentifier(scm__rc.d1786[51], SCM_MODULE(scm__rc.d1786[36]), SCM_NIL); /* gauche.internal#~ */
  scm__rc.d1786[53] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[32])),TRUE); /* error */
  scm__rc.d1786[52] = Scm_MakeIdentifier(scm__rc.d1786[53], SCM_MODULE(scm__rc.d1786[36]), SCM_NIL); /* gauche.internal#error */
  scm__rc.d1786[55] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[34])),TRUE); /* sys-getpwnam */
  scm__rc.d1786[54] = Scm_MakeIdentifier(scm__rc.d1786[55], SCM_MODULE(scm__rc.d1786[36]), SCM_NIL); /* gauche.internal#sys-getpwnam */
  scm__rc.d1786[56] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[35])),TRUE); /* after */
  scm__rc.d1786[58] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[36])),TRUE); /* string-append */
  scm__rc.d1786[57] = Scm_MakeIdentifier(scm__rc.d1786[58], SCM_MODULE(scm__rc.d1786[36]), SCM_NIL); /* gauche.internal#string-append */
  scm__rc.d1786[59] = Scm_RegComp(SCM_STRING(SCM_OBJ(&scm__sc.d1785[37])), 0);
  scm__rc.d1786[60] = Scm_RegComp(SCM_STRING(SCM_OBJ(&scm__sc.d1785[38])), 0);
  scm__rc.d1786[62] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[39])),TRUE); /* sys-getcwd */
  scm__rc.d1786[61] = Scm_MakeIdentifier(scm__rc.d1786[62], SCM_MODULE(scm__rc.d1786[36]), SCM_NIL); /* gauche.internal#sys-getcwd */
  {
     ScmCharSet *cs = SCM_CHARSET(Scm_MakeEmptyCharSet());
     Scm_CharSetAddRange(cs, SCM_CHAR(47), SCM_CHAR(47));
     Scm_CharSetAddRange(cs, SCM_CHAR(92), SCM_CHAR(92));
     scm__rc.d1786[63] = SCM_OBJ(cs);
  }
  scm__rc.d1786[65] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[40])),TRUE); /* string-split */
  scm__rc.d1786[64] = Scm_MakeIdentifier(scm__rc.d1786[65], SCM_MODULE(scm__rc.d1786[36]), SCM_NIL); /* gauche.internal#string-split */
  scm__rc.d1786[67] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[41])),TRUE); /* string-join */
  scm__rc.d1786[66] = Scm_MakeIdentifier(scm__rc.d1786[67], SCM_MODULE(scm__rc.d1786[36]), SCM_NIL); /* gauche.internal#string-join */
  scm__rc.d1786[69] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[44])),TRUE); /* length=? */
  scm__rc.d1786[68] = Scm_MakeIdentifier(scm__rc.d1786[69], SCM_MODULE(scm__rc.d1786[36]), SCM_NIL); /* gauche.internal#length=? */
  scm__rc.d1786[70] = Scm_RegComp(SCM_STRING(SCM_OBJ(&scm__sc.d1785[45])), 0);
  scm__rc.d1786[71] = Scm_MakeIdentifier(scm__rc.d1786[53], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#error */
  scm__rc.d1786[73] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[47])),TRUE); /* unwrap-syntax-1 */
  scm__rc.d1786[72] = Scm_MakeIdentifier(scm__rc.d1786[73], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#unwrap-syntax-1 */
  scm__rc.d1786[74] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[48]))); /* :absolute */
  scm__rc.d1786[75] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[49]))); /* :expand */
  scm__rc.d1786[76] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[50]))); /* :canonicalize */
  scm__rc.d1786[78] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[52])),TRUE); /* errorf */
  scm__rc.d1786[77] = Scm_MakeIdentifier(scm__rc.d1786[78], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#errorf */
  scm__rc.d1786[79] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[53]))); /* :key */
  scm__rc.d1786[80] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[48])),TRUE); /* absolute */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[34]), scm__rc.d1786[80]);
  scm__rc.d1786[81] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[49])),TRUE); /* expand */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[35]), scm__rc.d1786[81]);
  scm__rc.d1786[82] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[50])),TRUE); /* canonicalize */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[36]), scm__rc.d1786[82]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[40]), scm__rc.d1786[79]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[41]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[44]), scm__rc.d1786[2]);
  scm__rc.d1786[83] = Scm_MakeExtendedPair(scm__rc.d1786[30], SCM_OBJ(&scm__rc.d1787[41]), SCM_OBJ(&scm__rc.d1787[45]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[46]), scm__rc.d1786[83]);
  scm__rc.d1786[84] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[0])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[0]))->name = scm__rc.d1786[30];/* sys-normalize-pathname */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[0]))->debugInfo = scm__rc.d1786[84];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[14] = SCM_WORD(scm__rc.d1786[31]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[26] = SCM_WORD(scm__rc.d1786[31]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[38] = SCM_WORD(scm__rc.d1786[31]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[49] = SCM_WORD(scm__rc.d1786[33]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[53] = SCM_WORD(scm__rc.d1786[34]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[72] = SCM_WORD(scm__rc.d1786[38]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[75] = SCM_WORD(scm__rc.d1786[39]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[92] = SCM_WORD(scm__rc.d1786[41]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[103] = SCM_WORD(scm__rc.d1786[43]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[113] = SCM_WORD(scm__rc.d1786[43]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[127] = SCM_WORD(scm__rc.d1786[45]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[129] = SCM_WORD(scm__rc.d1786[47]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[138] = SCM_WORD(scm__rc.d1786[49]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[140] = SCM_WORD(scm__rc.d1786[50]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[149] = SCM_WORD(scm__rc.d1786[52]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[162] = SCM_WORD(scm__rc.d1786[52]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[173] = SCM_WORD(scm__rc.d1786[54]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[182] = SCM_WORD(scm__rc.d1786[49]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[184] = SCM_WORD(scm__rc.d1786[50]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[193] = SCM_WORD(scm__rc.d1786[52]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[202] = SCM_WORD(scm__rc.d1786[56]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[206] = SCM_WORD(scm__rc.d1786[57]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[223] = SCM_WORD(scm__rc.d1786[59]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[226] = SCM_WORD(scm__rc.d1786[39]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[232] = SCM_WORD(scm__rc.d1786[60]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[235] = SCM_WORD(scm__rc.d1786[39]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[246] = SCM_WORD(scm__rc.d1786[61]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[251] = SCM_WORD(scm__rc.d1786[57]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[264] = SCM_WORD(scm__rc.d1786[63]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[266] = SCM_WORD(scm__rc.d1786[64]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[290] = SCM_WORD(scm__rc.d1786[66]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[299] = SCM_WORD(scm__rc.d1786[41]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[317] = SCM_WORD(scm__rc.d1786[41]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[340] = SCM_WORD(scm__rc.d1786[41]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[352] = SCM_WORD(scm__rc.d1786[41]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[366] = SCM_WORD(scm__rc.d1786[68]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[372] = SCM_WORD(scm__rc.d1786[70]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[375] = SCM_WORD(scm__rc.d1786[39]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[413] = SCM_WORD(scm__rc.d1786[64]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[418] = SCM_WORD(scm__rc.d1786[66]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[428] = SCM_WORD(scm__rc.d1786[71]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[434] = SCM_WORD(scm__rc.d1786[72]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[438] = SCM_WORD(scm__rc.d1786[74]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[451] = SCM_WORD(scm__rc.d1786[75]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[464] = SCM_WORD(scm__rc.d1786[76]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[0]))[482] = SCM_WORD(scm__rc.d1786[77]);
  scm__rc.d1786[85] = Scm_MakeIdentifier(scm__rc.d1786[30], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#sys-normalize-pathname */
  scm__rc.d1786[86] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[54])),TRUE); /* %toplevel */
  scm__rc.d1786[87] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[1])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[1]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[1]))->debugInfo = scm__rc.d1786[87];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[492]))[3] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[492]))[6] = SCM_WORD(scm__rc.d1786[30]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[492]))[13] = SCM_WORD(scm__rc.d1786[85]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));

#line 140 "libsys.scm"
errno_n2y=(SCM_HASH_TABLE(Scm_MakeHashTableSimple(SCM_HASH_EQV,0)));

#line 141 "libsys.scm"
errno_y2n=(SCM_HASH_TABLE(Scm_MakeHashTableSimple(SCM_HASH_EQ,0)));
  scm__rc.d1786[88] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[55])),TRUE); /* sys-errno->symbol */
  scm__rc.d1786[89] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[56])),TRUE); /* num */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[47]), scm__rc.d1786[89]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[50]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[51]), scm__rc.d1786[88]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[52]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[53]), scm__rc.d1786[3]);
  scm__rc.d1786[90] = Scm_MakeExtendedPair(scm__rc.d1786[88], SCM_OBJ(&scm__rc.d1787[47]), SCM_OBJ(&scm__rc.d1787[55]));
  scm__rc.d1786[91] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[57])),TRUE); /* <fixnum> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[92]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[92]))[4] = scm__rc.d1786[91];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[92]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[92]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-errno->symbol")), SCM_OBJ(&libsyssys_errno_TOsymbol__STUB), 0);
  libsyssys_errno_TOsymbol__STUB.common.info = scm__rc.d1786[90];
  libsyssys_errno_TOsymbol__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[92]);
  scm__rc.d1786[99] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[58])),TRUE); /* sys-symbol->errno */
  scm__rc.d1786[100] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[59])),TRUE); /* name */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[56]), scm__rc.d1786[100]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[59]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[60]), scm__rc.d1786[99]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[61]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[62]), scm__rc.d1786[3]);
  scm__rc.d1786[101] = Scm_MakeExtendedPair(scm__rc.d1786[99], SCM_OBJ(&scm__rc.d1787[56]), SCM_OBJ(&scm__rc.d1787[64]));
  scm__rc.d1786[102] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[60])),TRUE); /* <symbol> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[103]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[103]))[4] = scm__rc.d1786[102];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[103]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[103]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-symbol->errno")), SCM_OBJ(&libsyssys_symbol_TOerrno__STUB), 0);
  libsyssys_symbol_TOerrno__STUB.common.info = scm__rc.d1786[101];
  libsyssys_symbol_TOerrno__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[103]);
  scm__rc.d1786[110] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[61])),TRUE); /* E2BIG */

#if defined(E2BIG)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[110]),SCM_MAKE_INT(E2BIG));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(E2BIG),scm__rc.d1786[110],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[110],SCM_MAKE_INT(E2BIG),0);}
#endif /* defined(E2BIG) */

  scm__rc.d1786[111] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[62])),TRUE); /* EACCES */

#if defined(EACCES)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[111]),SCM_MAKE_INT(EACCES));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EACCES),scm__rc.d1786[111],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[111],SCM_MAKE_INT(EACCES),0);}
#endif /* defined(EACCES) */

  scm__rc.d1786[112] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[63])),TRUE); /* EADDRINUSE */

#if defined(EADDRINUSE)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[112]),SCM_MAKE_INT(EADDRINUSE));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EADDRINUSE),scm__rc.d1786[112],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[112],SCM_MAKE_INT(EADDRINUSE),0);}
#endif /* defined(EADDRINUSE) */

  scm__rc.d1786[113] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[64])),TRUE); /* EADDRNOTAVAIL */

#if defined(EADDRNOTAVAIL)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[113]),SCM_MAKE_INT(EADDRNOTAVAIL));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EADDRNOTAVAIL),scm__rc.d1786[113],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[113],SCM_MAKE_INT(EADDRNOTAVAIL),0);}
#endif /* defined(EADDRNOTAVAIL) */

  scm__rc.d1786[114] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[65])),TRUE); /* EADV */

#if defined(EADV)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[114]),SCM_MAKE_INT(EADV));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EADV),scm__rc.d1786[114],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[114],SCM_MAKE_INT(EADV),0);}
#endif /* defined(EADV) */

  scm__rc.d1786[115] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[66])),TRUE); /* EAFNOSUPPORT */

#if defined(EAFNOSUPPORT)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[115]),SCM_MAKE_INT(EAFNOSUPPORT));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EAFNOSUPPORT),scm__rc.d1786[115],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[115],SCM_MAKE_INT(EAFNOSUPPORT),0);}
#endif /* defined(EAFNOSUPPORT) */

  scm__rc.d1786[116] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[67])),TRUE); /* EAGAIN */

#if defined(EAGAIN)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[116]),SCM_MAKE_INT(EAGAIN));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EAGAIN),scm__rc.d1786[116],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[116],SCM_MAKE_INT(EAGAIN),0);}
#endif /* defined(EAGAIN) */

  scm__rc.d1786[117] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[68])),TRUE); /* EALREADY */

#if defined(EALREADY)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[117]),SCM_MAKE_INT(EALREADY));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EALREADY),scm__rc.d1786[117],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[117],SCM_MAKE_INT(EALREADY),0);}
#endif /* defined(EALREADY) */

  scm__rc.d1786[118] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[69])),TRUE); /* EBADE */

#if defined(EBADE)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[118]),SCM_MAKE_INT(EBADE));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EBADE),scm__rc.d1786[118],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[118],SCM_MAKE_INT(EBADE),0);}
#endif /* defined(EBADE) */

  scm__rc.d1786[119] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[70])),TRUE); /* EBADF */

#if defined(EBADF)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[119]),SCM_MAKE_INT(EBADF));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EBADF),scm__rc.d1786[119],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[119],SCM_MAKE_INT(EBADF),0);}
#endif /* defined(EBADF) */

  scm__rc.d1786[120] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[71])),TRUE); /* EBADFD */

#if defined(EBADFD)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[120]),SCM_MAKE_INT(EBADFD));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EBADFD),scm__rc.d1786[120],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[120],SCM_MAKE_INT(EBADFD),0);}
#endif /* defined(EBADFD) */

  scm__rc.d1786[121] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[72])),TRUE); /* EBADMSG */

#if defined(EBADMSG)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[121]),SCM_MAKE_INT(EBADMSG));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EBADMSG),scm__rc.d1786[121],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[121],SCM_MAKE_INT(EBADMSG),0);}
#endif /* defined(EBADMSG) */

  scm__rc.d1786[122] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[73])),TRUE); /* EBADR */

#if defined(EBADR)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[122]),SCM_MAKE_INT(EBADR));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EBADR),scm__rc.d1786[122],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[122],SCM_MAKE_INT(EBADR),0);}
#endif /* defined(EBADR) */

  scm__rc.d1786[123] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[74])),TRUE); /* EBADRQC */

#if defined(EBADRQC)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[123]),SCM_MAKE_INT(EBADRQC));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EBADRQC),scm__rc.d1786[123],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[123],SCM_MAKE_INT(EBADRQC),0);}
#endif /* defined(EBADRQC) */

  scm__rc.d1786[124] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[75])),TRUE); /* EBADSLT */

#if defined(EBADSLT)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[124]),SCM_MAKE_INT(EBADSLT));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EBADSLT),scm__rc.d1786[124],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[124],SCM_MAKE_INT(EBADSLT),0);}
#endif /* defined(EBADSLT) */

  scm__rc.d1786[125] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[76])),TRUE); /* EBFONT */

#if defined(EBFONT)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[125]),SCM_MAKE_INT(EBFONT));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EBFONT),scm__rc.d1786[125],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[125],SCM_MAKE_INT(EBFONT),0);}
#endif /* defined(EBFONT) */

  scm__rc.d1786[126] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[77])),TRUE); /* EBUSY */

#if defined(EBUSY)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[126]),SCM_MAKE_INT(EBUSY));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EBUSY),scm__rc.d1786[126],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[126],SCM_MAKE_INT(EBUSY),0);}
#endif /* defined(EBUSY) */

  scm__rc.d1786[127] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[78])),TRUE); /* ECANCELED */

#if defined(ECANCELED)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[127]),SCM_MAKE_INT(ECANCELED));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ECANCELED),scm__rc.d1786[127],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[127],SCM_MAKE_INT(ECANCELED),0);}
#endif /* defined(ECANCELED) */

  scm__rc.d1786[128] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[79])),TRUE); /* ECHILD */

#if defined(ECHILD)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[128]),SCM_MAKE_INT(ECHILD));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ECHILD),scm__rc.d1786[128],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[128],SCM_MAKE_INT(ECHILD),0);}
#endif /* defined(ECHILD) */

  scm__rc.d1786[129] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[80])),TRUE); /* ECHRNG */

#if defined(ECHRNG)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[129]),SCM_MAKE_INT(ECHRNG));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ECHRNG),scm__rc.d1786[129],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[129],SCM_MAKE_INT(ECHRNG),0);}
#endif /* defined(ECHRNG) */

  scm__rc.d1786[130] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[81])),TRUE); /* ECOMM */

#if defined(ECOMM)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[130]),SCM_MAKE_INT(ECOMM));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ECOMM),scm__rc.d1786[130],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[130],SCM_MAKE_INT(ECOMM),0);}
#endif /* defined(ECOMM) */

  scm__rc.d1786[131] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[82])),TRUE); /* ECONNABORTED */

#if defined(ECONNABORTED)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[131]),SCM_MAKE_INT(ECONNABORTED));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ECONNABORTED),scm__rc.d1786[131],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[131],SCM_MAKE_INT(ECONNABORTED),0);}
#endif /* defined(ECONNABORTED) */

  scm__rc.d1786[132] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[83])),TRUE); /* ECONNREFUSED */

#if defined(ECONNREFUSED)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[132]),SCM_MAKE_INT(ECONNREFUSED));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ECONNREFUSED),scm__rc.d1786[132],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[132],SCM_MAKE_INT(ECONNREFUSED),0);}
#endif /* defined(ECONNREFUSED) */

  scm__rc.d1786[133] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[84])),TRUE); /* ECONNRESET */

#if defined(ECONNRESET)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[133]),SCM_MAKE_INT(ECONNRESET));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ECONNRESET),scm__rc.d1786[133],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[133],SCM_MAKE_INT(ECONNRESET),0);}
#endif /* defined(ECONNRESET) */

  scm__rc.d1786[134] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[85])),TRUE); /* EDEADLK */

#if defined(EDEADLK)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[134]),SCM_MAKE_INT(EDEADLK));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EDEADLK),scm__rc.d1786[134],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[134],SCM_MAKE_INT(EDEADLK),0);}
#endif /* defined(EDEADLK) */

  scm__rc.d1786[135] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[86])),TRUE); /* EDEADLOCK */

#if defined(EDEADLOCK)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[135]),SCM_MAKE_INT(EDEADLOCK));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EDEADLOCK),scm__rc.d1786[135],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[135],SCM_MAKE_INT(EDEADLOCK),0);}
#endif /* defined(EDEADLOCK) */

  scm__rc.d1786[136] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[87])),TRUE); /* EDESTADDRREQ */

#if defined(EDESTADDRREQ)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[136]),SCM_MAKE_INT(EDESTADDRREQ));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EDESTADDRREQ),scm__rc.d1786[136],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[136],SCM_MAKE_INT(EDESTADDRREQ),0);}
#endif /* defined(EDESTADDRREQ) */

  scm__rc.d1786[137] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[88])),TRUE); /* EDOM */

#if defined(EDOM)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[137]),SCM_MAKE_INT(EDOM));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EDOM),scm__rc.d1786[137],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[137],SCM_MAKE_INT(EDOM),0);}
#endif /* defined(EDOM) */

  scm__rc.d1786[138] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[89])),TRUE); /* EDOTDOT */

#if defined(EDOTDOT)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[138]),SCM_MAKE_INT(EDOTDOT));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EDOTDOT),scm__rc.d1786[138],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[138],SCM_MAKE_INT(EDOTDOT),0);}
#endif /* defined(EDOTDOT) */

  scm__rc.d1786[139] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[90])),TRUE); /* EDQUOT */

#if defined(EDQUOT)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[139]),SCM_MAKE_INT(EDQUOT));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EDQUOT),scm__rc.d1786[139],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[139],SCM_MAKE_INT(EDQUOT),0);}
#endif /* defined(EDQUOT) */

  scm__rc.d1786[140] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[91])),TRUE); /* EEXIST */

#if defined(EEXIST)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[140]),SCM_MAKE_INT(EEXIST));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EEXIST),scm__rc.d1786[140],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[140],SCM_MAKE_INT(EEXIST),0);}
#endif /* defined(EEXIST) */

  scm__rc.d1786[141] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[92])),TRUE); /* EFAULT */

#if defined(EFAULT)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[141]),SCM_MAKE_INT(EFAULT));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EFAULT),scm__rc.d1786[141],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[141],SCM_MAKE_INT(EFAULT),0);}
#endif /* defined(EFAULT) */

  scm__rc.d1786[142] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[93])),TRUE); /* EFBIG */

#if defined(EFBIG)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[142]),SCM_MAKE_INT(EFBIG));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EFBIG),scm__rc.d1786[142],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[142],SCM_MAKE_INT(EFBIG),0);}
#endif /* defined(EFBIG) */

  scm__rc.d1786[143] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[94])),TRUE); /* EHOSTDOWN */

#if defined(EHOSTDOWN)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[143]),SCM_MAKE_INT(EHOSTDOWN));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EHOSTDOWN),scm__rc.d1786[143],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[143],SCM_MAKE_INT(EHOSTDOWN),0);}
#endif /* defined(EHOSTDOWN) */

  scm__rc.d1786[144] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[95])),TRUE); /* EHOSTUNREACH */

#if defined(EHOSTUNREACH)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[144]),SCM_MAKE_INT(EHOSTUNREACH));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EHOSTUNREACH),scm__rc.d1786[144],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[144],SCM_MAKE_INT(EHOSTUNREACH),0);}
#endif /* defined(EHOSTUNREACH) */

  scm__rc.d1786[145] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[96])),TRUE); /* EIDRM */

#if defined(EIDRM)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[145]),SCM_MAKE_INT(EIDRM));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EIDRM),scm__rc.d1786[145],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[145],SCM_MAKE_INT(EIDRM),0);}
#endif /* defined(EIDRM) */

  scm__rc.d1786[146] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[97])),TRUE); /* EILSEQ */

#if defined(EILSEQ)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[146]),SCM_MAKE_INT(EILSEQ));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EILSEQ),scm__rc.d1786[146],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[146],SCM_MAKE_INT(EILSEQ),0);}
#endif /* defined(EILSEQ) */

  scm__rc.d1786[147] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[98])),TRUE); /* EINPROGRESS */

#if defined(EINPROGRESS)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[147]),SCM_MAKE_INT(EINPROGRESS));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EINPROGRESS),scm__rc.d1786[147],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[147],SCM_MAKE_INT(EINPROGRESS),0);}
#endif /* defined(EINPROGRESS) */

  scm__rc.d1786[148] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[99])),TRUE); /* EINTR */

#if defined(EINTR)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[148]),SCM_MAKE_INT(EINTR));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EINTR),scm__rc.d1786[148],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[148],SCM_MAKE_INT(EINTR),0);}
#endif /* defined(EINTR) */

  scm__rc.d1786[149] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[100])),TRUE); /* EINVAL */

#if defined(EINVAL)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[149]),SCM_MAKE_INT(EINVAL));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EINVAL),scm__rc.d1786[149],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[149],SCM_MAKE_INT(EINVAL),0);}
#endif /* defined(EINVAL) */

  scm__rc.d1786[150] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[101])),TRUE); /* EIO */

#if defined(EIO)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[150]),SCM_MAKE_INT(EIO));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EIO),scm__rc.d1786[150],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[150],SCM_MAKE_INT(EIO),0);}
#endif /* defined(EIO) */

  scm__rc.d1786[151] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[102])),TRUE); /* EISCONN */

#if defined(EISCONN)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[151]),SCM_MAKE_INT(EISCONN));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EISCONN),scm__rc.d1786[151],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[151],SCM_MAKE_INT(EISCONN),0);}
#endif /* defined(EISCONN) */

  scm__rc.d1786[152] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[103])),TRUE); /* EISDIR */

#if defined(EISDIR)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[152]),SCM_MAKE_INT(EISDIR));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EISDIR),scm__rc.d1786[152],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[152],SCM_MAKE_INT(EISDIR),0);}
#endif /* defined(EISDIR) */

  scm__rc.d1786[153] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[104])),TRUE); /* EISNAM */

#if defined(EISNAM)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[153]),SCM_MAKE_INT(EISNAM));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EISNAM),scm__rc.d1786[153],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[153],SCM_MAKE_INT(EISNAM),0);}
#endif /* defined(EISNAM) */

  scm__rc.d1786[154] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[105])),TRUE); /* EKEYEXPIRED */

#if defined(EKEYEXPIRED)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[154]),SCM_MAKE_INT(EKEYEXPIRED));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EKEYEXPIRED),scm__rc.d1786[154],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[154],SCM_MAKE_INT(EKEYEXPIRED),0);}
#endif /* defined(EKEYEXPIRED) */

  scm__rc.d1786[155] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[106])),TRUE); /* EKEYREJECTED */

#if defined(EKEYREJECTED)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[155]),SCM_MAKE_INT(EKEYREJECTED));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EKEYREJECTED),scm__rc.d1786[155],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[155],SCM_MAKE_INT(EKEYREJECTED),0);}
#endif /* defined(EKEYREJECTED) */

  scm__rc.d1786[156] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[107])),TRUE); /* EKEYREVOKED */

#if defined(EKEYREVOKED)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[156]),SCM_MAKE_INT(EKEYREVOKED));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EKEYREVOKED),scm__rc.d1786[156],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[156],SCM_MAKE_INT(EKEYREVOKED),0);}
#endif /* defined(EKEYREVOKED) */

  scm__rc.d1786[157] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[108])),TRUE); /* EL2HLT */

#if defined(EL2HLT)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[157]),SCM_MAKE_INT(EL2HLT));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EL2HLT),scm__rc.d1786[157],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[157],SCM_MAKE_INT(EL2HLT),0);}
#endif /* defined(EL2HLT) */

  scm__rc.d1786[158] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[109])),TRUE); /* EL2NSYNC */

#if defined(EL2NSYNC)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[158]),SCM_MAKE_INT(EL2NSYNC));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EL2NSYNC),scm__rc.d1786[158],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[158],SCM_MAKE_INT(EL2NSYNC),0);}
#endif /* defined(EL2NSYNC) */

  scm__rc.d1786[159] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[110])),TRUE); /* EL3HLT */

#if defined(EL3HLT)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[159]),SCM_MAKE_INT(EL3HLT));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EL3HLT),scm__rc.d1786[159],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[159],SCM_MAKE_INT(EL3HLT),0);}
#endif /* defined(EL3HLT) */

  scm__rc.d1786[160] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[111])),TRUE); /* EL3RST */

#if defined(EL3RST)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[160]),SCM_MAKE_INT(EL3RST));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EL3RST),scm__rc.d1786[160],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[160],SCM_MAKE_INT(EL3RST),0);}
#endif /* defined(EL3RST) */

  scm__rc.d1786[161] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[112])),TRUE); /* ELIBACC */

#if defined(ELIBACC)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[161]),SCM_MAKE_INT(ELIBACC));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ELIBACC),scm__rc.d1786[161],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[161],SCM_MAKE_INT(ELIBACC),0);}
#endif /* defined(ELIBACC) */

  scm__rc.d1786[162] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[113])),TRUE); /* ELIBBAD */

#if defined(ELIBBAD)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[162]),SCM_MAKE_INT(ELIBBAD));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ELIBBAD),scm__rc.d1786[162],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[162],SCM_MAKE_INT(ELIBBAD),0);}
#endif /* defined(ELIBBAD) */

  scm__rc.d1786[163] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[114])),TRUE); /* ELIBEXEC */

#if defined(ELIBEXEC)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[163]),SCM_MAKE_INT(ELIBEXEC));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ELIBEXEC),scm__rc.d1786[163],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[163],SCM_MAKE_INT(ELIBEXEC),0);}
#endif /* defined(ELIBEXEC) */

  scm__rc.d1786[164] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[115])),TRUE); /* ELIBMAX */

#if defined(ELIBMAX)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[164]),SCM_MAKE_INT(ELIBMAX));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ELIBMAX),scm__rc.d1786[164],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[164],SCM_MAKE_INT(ELIBMAX),0);}
#endif /* defined(ELIBMAX) */

  scm__rc.d1786[165] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[116])),TRUE); /* ELIBSCN */

#if defined(ELIBSCN)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[165]),SCM_MAKE_INT(ELIBSCN));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ELIBSCN),scm__rc.d1786[165],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[165],SCM_MAKE_INT(ELIBSCN),0);}
#endif /* defined(ELIBSCN) */

  scm__rc.d1786[166] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[117])),TRUE); /* ELNRNG */

#if defined(ELNRNG)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[166]),SCM_MAKE_INT(ELNRNG));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ELNRNG),scm__rc.d1786[166],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[166],SCM_MAKE_INT(ELNRNG),0);}
#endif /* defined(ELNRNG) */

  scm__rc.d1786[167] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[118])),TRUE); /* ELOOP */

#if defined(ELOOP)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[167]),SCM_MAKE_INT(ELOOP));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ELOOP),scm__rc.d1786[167],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[167],SCM_MAKE_INT(ELOOP),0);}
#endif /* defined(ELOOP) */

  scm__rc.d1786[168] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[119])),TRUE); /* EMEDIUMTYPE */

#if defined(EMEDIUMTYPE)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[168]),SCM_MAKE_INT(EMEDIUMTYPE));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EMEDIUMTYPE),scm__rc.d1786[168],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[168],SCM_MAKE_INT(EMEDIUMTYPE),0);}
#endif /* defined(EMEDIUMTYPE) */

  scm__rc.d1786[169] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[120])),TRUE); /* EMFILE */

#if defined(EMFILE)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[169]),SCM_MAKE_INT(EMFILE));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EMFILE),scm__rc.d1786[169],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[169],SCM_MAKE_INT(EMFILE),0);}
#endif /* defined(EMFILE) */

  scm__rc.d1786[170] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[121])),TRUE); /* EMLINK */

#if defined(EMLINK)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[170]),SCM_MAKE_INT(EMLINK));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EMLINK),scm__rc.d1786[170],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[170],SCM_MAKE_INT(EMLINK),0);}
#endif /* defined(EMLINK) */

  scm__rc.d1786[171] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[122])),TRUE); /* EMSGSIZE */

#if defined(EMSGSIZE)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[171]),SCM_MAKE_INT(EMSGSIZE));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EMSGSIZE),scm__rc.d1786[171],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[171],SCM_MAKE_INT(EMSGSIZE),0);}
#endif /* defined(EMSGSIZE) */

  scm__rc.d1786[172] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[123])),TRUE); /* EMULTIHOP */

#if defined(EMULTIHOP)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[172]),SCM_MAKE_INT(EMULTIHOP));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EMULTIHOP),scm__rc.d1786[172],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[172],SCM_MAKE_INT(EMULTIHOP),0);}
#endif /* defined(EMULTIHOP) */

  scm__rc.d1786[173] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[124])),TRUE); /* ENAMETOOLONG */

#if defined(ENAMETOOLONG)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[173]),SCM_MAKE_INT(ENAMETOOLONG));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENAMETOOLONG),scm__rc.d1786[173],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[173],SCM_MAKE_INT(ENAMETOOLONG),0);}
#endif /* defined(ENAMETOOLONG) */

  scm__rc.d1786[174] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[125])),TRUE); /* ENAVAIL */

#if defined(ENAVAIL)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[174]),SCM_MAKE_INT(ENAVAIL));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENAVAIL),scm__rc.d1786[174],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[174],SCM_MAKE_INT(ENAVAIL),0);}
#endif /* defined(ENAVAIL) */

  scm__rc.d1786[175] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[126])),TRUE); /* ENETDOWN */

#if defined(ENETDOWN)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[175]),SCM_MAKE_INT(ENETDOWN));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENETDOWN),scm__rc.d1786[175],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[175],SCM_MAKE_INT(ENETDOWN),0);}
#endif /* defined(ENETDOWN) */

  scm__rc.d1786[176] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[127])),TRUE); /* ENETRESET */

#if defined(ENETRESET)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[176]),SCM_MAKE_INT(ENETRESET));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENETRESET),scm__rc.d1786[176],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[176],SCM_MAKE_INT(ENETRESET),0);}
#endif /* defined(ENETRESET) */

  scm__rc.d1786[177] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[128])),TRUE); /* ENETUNREACH */

#if defined(ENETUNREACH)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[177]),SCM_MAKE_INT(ENETUNREACH));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENETUNREACH),scm__rc.d1786[177],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[177],SCM_MAKE_INT(ENETUNREACH),0);}
#endif /* defined(ENETUNREACH) */

  scm__rc.d1786[178] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[129])),TRUE); /* ENFILE */

#if defined(ENFILE)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[178]),SCM_MAKE_INT(ENFILE));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENFILE),scm__rc.d1786[178],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[178],SCM_MAKE_INT(ENFILE),0);}
#endif /* defined(ENFILE) */

  scm__rc.d1786[179] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[130])),TRUE); /* ENOANO */

#if defined(ENOANO)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[179]),SCM_MAKE_INT(ENOANO));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOANO),scm__rc.d1786[179],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[179],SCM_MAKE_INT(ENOANO),0);}
#endif /* defined(ENOANO) */

  scm__rc.d1786[180] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[131])),TRUE); /* ENOBUFS */

#if defined(ENOBUFS)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[180]),SCM_MAKE_INT(ENOBUFS));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOBUFS),scm__rc.d1786[180],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[180],SCM_MAKE_INT(ENOBUFS),0);}
#endif /* defined(ENOBUFS) */

  scm__rc.d1786[181] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[132])),TRUE); /* ENOCSI */

#if defined(ENOCSI)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[181]),SCM_MAKE_INT(ENOCSI));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOCSI),scm__rc.d1786[181],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[181],SCM_MAKE_INT(ENOCSI),0);}
#endif /* defined(ENOCSI) */

  scm__rc.d1786[182] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[133])),TRUE); /* ENODATA */

#if defined(ENODATA)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[182]),SCM_MAKE_INT(ENODATA));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENODATA),scm__rc.d1786[182],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[182],SCM_MAKE_INT(ENODATA),0);}
#endif /* defined(ENODATA) */

  scm__rc.d1786[183] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[134])),TRUE); /* ENODEV */

#if defined(ENODEV)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[183]),SCM_MAKE_INT(ENODEV));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENODEV),scm__rc.d1786[183],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[183],SCM_MAKE_INT(ENODEV),0);}
#endif /* defined(ENODEV) */

  scm__rc.d1786[184] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[135])),TRUE); /* ENOENT */

#if defined(ENOENT)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[184]),SCM_MAKE_INT(ENOENT));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOENT),scm__rc.d1786[184],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[184],SCM_MAKE_INT(ENOENT),0);}
#endif /* defined(ENOENT) */

  scm__rc.d1786[185] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[136])),TRUE); /* ENOEXEC */

#if defined(ENOEXEC)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[185]),SCM_MAKE_INT(ENOEXEC));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOEXEC),scm__rc.d1786[185],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[185],SCM_MAKE_INT(ENOEXEC),0);}
#endif /* defined(ENOEXEC) */

  scm__rc.d1786[186] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[137])),TRUE); /* ENOKEY */

#if defined(ENOKEY)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[186]),SCM_MAKE_INT(ENOKEY));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOKEY),scm__rc.d1786[186],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[186],SCM_MAKE_INT(ENOKEY),0);}
#endif /* defined(ENOKEY) */

  scm__rc.d1786[187] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[138])),TRUE); /* ENOLCK */

#if defined(ENOLCK)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[187]),SCM_MAKE_INT(ENOLCK));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOLCK),scm__rc.d1786[187],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[187],SCM_MAKE_INT(ENOLCK),0);}
#endif /* defined(ENOLCK) */

  scm__rc.d1786[188] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[139])),TRUE); /* ENOLINK */

#if defined(ENOLINK)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[188]),SCM_MAKE_INT(ENOLINK));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOLINK),scm__rc.d1786[188],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[188],SCM_MAKE_INT(ENOLINK),0);}
#endif /* defined(ENOLINK) */

  scm__rc.d1786[189] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[140])),TRUE); /* ENOMEDIUM */

#if defined(ENOMEDIUM)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[189]),SCM_MAKE_INT(ENOMEDIUM));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOMEDIUM),scm__rc.d1786[189],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[189],SCM_MAKE_INT(ENOMEDIUM),0);}
#endif /* defined(ENOMEDIUM) */

  scm__rc.d1786[190] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[141])),TRUE); /* ENOMEM */

#if defined(ENOMEM)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[190]),SCM_MAKE_INT(ENOMEM));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOMEM),scm__rc.d1786[190],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[190],SCM_MAKE_INT(ENOMEM),0);}
#endif /* defined(ENOMEM) */

  scm__rc.d1786[191] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[142])),TRUE); /* ENOMSG */

#if defined(ENOMSG)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[191]),SCM_MAKE_INT(ENOMSG));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOMSG),scm__rc.d1786[191],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[191],SCM_MAKE_INT(ENOMSG),0);}
#endif /* defined(ENOMSG) */

  scm__rc.d1786[192] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[143])),TRUE); /* ENONET */

#if defined(ENONET)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[192]),SCM_MAKE_INT(ENONET));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENONET),scm__rc.d1786[192],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[192],SCM_MAKE_INT(ENONET),0);}
#endif /* defined(ENONET) */

  scm__rc.d1786[193] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[144])),TRUE); /* ENOPKG */

#if defined(ENOPKG)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[193]),SCM_MAKE_INT(ENOPKG));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOPKG),scm__rc.d1786[193],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[193],SCM_MAKE_INT(ENOPKG),0);}
#endif /* defined(ENOPKG) */

  scm__rc.d1786[194] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[145])),TRUE); /* ENOPROTOOPT */

#if defined(ENOPROTOOPT)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[194]),SCM_MAKE_INT(ENOPROTOOPT));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOPROTOOPT),scm__rc.d1786[194],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[194],SCM_MAKE_INT(ENOPROTOOPT),0);}
#endif /* defined(ENOPROTOOPT) */

  scm__rc.d1786[195] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[146])),TRUE); /* ENOSPC */

#if defined(ENOSPC)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[195]),SCM_MAKE_INT(ENOSPC));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOSPC),scm__rc.d1786[195],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[195],SCM_MAKE_INT(ENOSPC),0);}
#endif /* defined(ENOSPC) */

  scm__rc.d1786[196] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[147])),TRUE); /* ENOSR */

#if defined(ENOSR)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[196]),SCM_MAKE_INT(ENOSR));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOSR),scm__rc.d1786[196],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[196],SCM_MAKE_INT(ENOSR),0);}
#endif /* defined(ENOSR) */

  scm__rc.d1786[197] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[148])),TRUE); /* ENOSTR */

#if defined(ENOSTR)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[197]),SCM_MAKE_INT(ENOSTR));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOSTR),scm__rc.d1786[197],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[197],SCM_MAKE_INT(ENOSTR),0);}
#endif /* defined(ENOSTR) */

  scm__rc.d1786[198] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[149])),TRUE); /* ENOSYS */

#if defined(ENOSYS)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[198]),SCM_MAKE_INT(ENOSYS));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOSYS),scm__rc.d1786[198],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[198],SCM_MAKE_INT(ENOSYS),0);}
#endif /* defined(ENOSYS) */

  scm__rc.d1786[199] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[150])),TRUE); /* ENOTBLK */

#if defined(ENOTBLK)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[199]),SCM_MAKE_INT(ENOTBLK));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOTBLK),scm__rc.d1786[199],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[199],SCM_MAKE_INT(ENOTBLK),0);}
#endif /* defined(ENOTBLK) */

  scm__rc.d1786[200] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[151])),TRUE); /* ENOTCONN */

#if defined(ENOTCONN)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[200]),SCM_MAKE_INT(ENOTCONN));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOTCONN),scm__rc.d1786[200],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[200],SCM_MAKE_INT(ENOTCONN),0);}
#endif /* defined(ENOTCONN) */

  scm__rc.d1786[201] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[152])),TRUE); /* ENOTDIR */

#if defined(ENOTDIR)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[201]),SCM_MAKE_INT(ENOTDIR));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOTDIR),scm__rc.d1786[201],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[201],SCM_MAKE_INT(ENOTDIR),0);}
#endif /* defined(ENOTDIR) */

  scm__rc.d1786[202] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[153])),TRUE); /* ENOTEMPTY */

#if defined(ENOTEMPTY)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[202]),SCM_MAKE_INT(ENOTEMPTY));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOTEMPTY),scm__rc.d1786[202],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[202],SCM_MAKE_INT(ENOTEMPTY),0);}
#endif /* defined(ENOTEMPTY) */

  scm__rc.d1786[203] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[154])),TRUE); /* ENOTNAM */

#if defined(ENOTNAM)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[203]),SCM_MAKE_INT(ENOTNAM));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOTNAM),scm__rc.d1786[203],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[203],SCM_MAKE_INT(ENOTNAM),0);}
#endif /* defined(ENOTNAM) */

  scm__rc.d1786[204] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[155])),TRUE); /* ENOTSOCK */

#if defined(ENOTSOCK)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[204]),SCM_MAKE_INT(ENOTSOCK));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOTSOCK),scm__rc.d1786[204],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[204],SCM_MAKE_INT(ENOTSOCK),0);}
#endif /* defined(ENOTSOCK) */

  scm__rc.d1786[205] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[156])),TRUE); /* ENOTTY */

#if defined(ENOTTY)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[205]),SCM_MAKE_INT(ENOTTY));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOTTY),scm__rc.d1786[205],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[205],SCM_MAKE_INT(ENOTTY),0);}
#endif /* defined(ENOTTY) */

  scm__rc.d1786[206] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[157])),TRUE); /* ENOTUNIQ */

#if defined(ENOTUNIQ)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[206]),SCM_MAKE_INT(ENOTUNIQ));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENOTUNIQ),scm__rc.d1786[206],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[206],SCM_MAKE_INT(ENOTUNIQ),0);}
#endif /* defined(ENOTUNIQ) */

  scm__rc.d1786[207] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[158])),TRUE); /* ENXIO */

#if defined(ENXIO)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[207]),SCM_MAKE_INT(ENXIO));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ENXIO),scm__rc.d1786[207],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[207],SCM_MAKE_INT(ENXIO),0);}
#endif /* defined(ENXIO) */

  scm__rc.d1786[208] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[159])),TRUE); /* EOPNOTSUPP */

#if defined(EOPNOTSUPP)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[208]),SCM_MAKE_INT(EOPNOTSUPP));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EOPNOTSUPP),scm__rc.d1786[208],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[208],SCM_MAKE_INT(EOPNOTSUPP),0);}
#endif /* defined(EOPNOTSUPP) */

  scm__rc.d1786[209] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[160])),TRUE); /* EOVERFLOW */

#if defined(EOVERFLOW)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[209]),SCM_MAKE_INT(EOVERFLOW));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EOVERFLOW),scm__rc.d1786[209],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[209],SCM_MAKE_INT(EOVERFLOW),0);}
#endif /* defined(EOVERFLOW) */

  scm__rc.d1786[210] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[161])),TRUE); /* EPERM */

#if defined(EPERM)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[210]),SCM_MAKE_INT(EPERM));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EPERM),scm__rc.d1786[210],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[210],SCM_MAKE_INT(EPERM),0);}
#endif /* defined(EPERM) */

  scm__rc.d1786[211] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[162])),TRUE); /* EPFNOSUPPORT */

#if defined(EPFNOSUPPORT)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[211]),SCM_MAKE_INT(EPFNOSUPPORT));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EPFNOSUPPORT),scm__rc.d1786[211],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[211],SCM_MAKE_INT(EPFNOSUPPORT),0);}
#endif /* defined(EPFNOSUPPORT) */

  scm__rc.d1786[212] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[163])),TRUE); /* EPIPE */

#if defined(EPIPE)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[212]),SCM_MAKE_INT(EPIPE));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EPIPE),scm__rc.d1786[212],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[212],SCM_MAKE_INT(EPIPE),0);}
#endif /* defined(EPIPE) */

  scm__rc.d1786[213] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[164])),TRUE); /* EPROTO */

#if defined(EPROTO)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[213]),SCM_MAKE_INT(EPROTO));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EPROTO),scm__rc.d1786[213],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[213],SCM_MAKE_INT(EPROTO),0);}
#endif /* defined(EPROTO) */

  scm__rc.d1786[214] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[165])),TRUE); /* EPROTONOSUPPORT */

#if defined(EPROTONOSUPPORT)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[214]),SCM_MAKE_INT(EPROTONOSUPPORT));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EPROTONOSUPPORT),scm__rc.d1786[214],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[214],SCM_MAKE_INT(EPROTONOSUPPORT),0);}
#endif /* defined(EPROTONOSUPPORT) */

  scm__rc.d1786[215] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[166])),TRUE); /* EPROTOTYPE */

#if defined(EPROTOTYPE)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[215]),SCM_MAKE_INT(EPROTOTYPE));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EPROTOTYPE),scm__rc.d1786[215],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[215],SCM_MAKE_INT(EPROTOTYPE),0);}
#endif /* defined(EPROTOTYPE) */

  scm__rc.d1786[216] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[167])),TRUE); /* ERANGE */

#if defined(ERANGE)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[216]),SCM_MAKE_INT(ERANGE));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ERANGE),scm__rc.d1786[216],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[216],SCM_MAKE_INT(ERANGE),0);}
#endif /* defined(ERANGE) */

  scm__rc.d1786[217] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[168])),TRUE); /* EREMCHG */

#if defined(EREMCHG)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[217]),SCM_MAKE_INT(EREMCHG));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EREMCHG),scm__rc.d1786[217],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[217],SCM_MAKE_INT(EREMCHG),0);}
#endif /* defined(EREMCHG) */

  scm__rc.d1786[218] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[169])),TRUE); /* EREMOTE */

#if defined(EREMOTE)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[218]),SCM_MAKE_INT(EREMOTE));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EREMOTE),scm__rc.d1786[218],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[218],SCM_MAKE_INT(EREMOTE),0);}
#endif /* defined(EREMOTE) */

  scm__rc.d1786[219] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[170])),TRUE); /* EREMOTEIO */

#if defined(EREMOTEIO)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[219]),SCM_MAKE_INT(EREMOTEIO));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EREMOTEIO),scm__rc.d1786[219],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[219],SCM_MAKE_INT(EREMOTEIO),0);}
#endif /* defined(EREMOTEIO) */

  scm__rc.d1786[220] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[171])),TRUE); /* ERESTART */

#if defined(ERESTART)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[220]),SCM_MAKE_INT(ERESTART));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ERESTART),scm__rc.d1786[220],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[220],SCM_MAKE_INT(ERESTART),0);}
#endif /* defined(ERESTART) */

  scm__rc.d1786[221] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[172])),TRUE); /* EROFS */

#if defined(EROFS)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[221]),SCM_MAKE_INT(EROFS));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EROFS),scm__rc.d1786[221],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[221],SCM_MAKE_INT(EROFS),0);}
#endif /* defined(EROFS) */

  scm__rc.d1786[222] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[173])),TRUE); /* ESHUTDOWN */

#if defined(ESHUTDOWN)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[222]),SCM_MAKE_INT(ESHUTDOWN));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ESHUTDOWN),scm__rc.d1786[222],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[222],SCM_MAKE_INT(ESHUTDOWN),0);}
#endif /* defined(ESHUTDOWN) */

  scm__rc.d1786[223] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[174])),TRUE); /* ESOCKTNOSUPPORT */

#if defined(ESOCKTNOSUPPORT)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[223]),SCM_MAKE_INT(ESOCKTNOSUPPORT));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ESOCKTNOSUPPORT),scm__rc.d1786[223],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[223],SCM_MAKE_INT(ESOCKTNOSUPPORT),0);}
#endif /* defined(ESOCKTNOSUPPORT) */

  scm__rc.d1786[224] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[175])),TRUE); /* ESPIPE */

#if defined(ESPIPE)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[224]),SCM_MAKE_INT(ESPIPE));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ESPIPE),scm__rc.d1786[224],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[224],SCM_MAKE_INT(ESPIPE),0);}
#endif /* defined(ESPIPE) */

  scm__rc.d1786[225] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[176])),TRUE); /* ESRCH */

#if defined(ESRCH)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[225]),SCM_MAKE_INT(ESRCH));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ESRCH),scm__rc.d1786[225],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[225],SCM_MAKE_INT(ESRCH),0);}
#endif /* defined(ESRCH) */

  scm__rc.d1786[226] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[177])),TRUE); /* ESRMNT */

#if defined(ESRMNT)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[226]),SCM_MAKE_INT(ESRMNT));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ESRMNT),scm__rc.d1786[226],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[226],SCM_MAKE_INT(ESRMNT),0);}
#endif /* defined(ESRMNT) */

  scm__rc.d1786[227] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[178])),TRUE); /* ESTALE */

#if defined(ESTALE)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[227]),SCM_MAKE_INT(ESTALE));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ESTALE),scm__rc.d1786[227],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[227],SCM_MAKE_INT(ESTALE),0);}
#endif /* defined(ESTALE) */

  scm__rc.d1786[228] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[179])),TRUE); /* ESTRPIPE */

#if defined(ESTRPIPE)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[228]),SCM_MAKE_INT(ESTRPIPE));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ESTRPIPE),scm__rc.d1786[228],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[228],SCM_MAKE_INT(ESTRPIPE),0);}
#endif /* defined(ESTRPIPE) */

  scm__rc.d1786[229] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[180])),TRUE); /* ETIME */

#if defined(ETIME)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[229]),SCM_MAKE_INT(ETIME));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ETIME),scm__rc.d1786[229],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[229],SCM_MAKE_INT(ETIME),0);}
#endif /* defined(ETIME) */

  scm__rc.d1786[230] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[181])),TRUE); /* ETIMEDOUT */

#if defined(ETIMEDOUT)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[230]),SCM_MAKE_INT(ETIMEDOUT));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ETIMEDOUT),scm__rc.d1786[230],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[230],SCM_MAKE_INT(ETIMEDOUT),0);}
#endif /* defined(ETIMEDOUT) */

  scm__rc.d1786[231] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[182])),TRUE); /* ETOOMANYREFS */

#if defined(ETOOMANYREFS)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[231]),SCM_MAKE_INT(ETOOMANYREFS));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ETOOMANYREFS),scm__rc.d1786[231],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[231],SCM_MAKE_INT(ETOOMANYREFS),0);}
#endif /* defined(ETOOMANYREFS) */

  scm__rc.d1786[232] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[183])),TRUE); /* ETXTBSY */

#if defined(ETXTBSY)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[232]),SCM_MAKE_INT(ETXTBSY));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(ETXTBSY),scm__rc.d1786[232],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[232],SCM_MAKE_INT(ETXTBSY),0);}
#endif /* defined(ETXTBSY) */

  scm__rc.d1786[233] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[184])),TRUE); /* EUCLEAN */

#if defined(EUCLEAN)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[233]),SCM_MAKE_INT(EUCLEAN));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EUCLEAN),scm__rc.d1786[233],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[233],SCM_MAKE_INT(EUCLEAN),0);}
#endif /* defined(EUCLEAN) */

  scm__rc.d1786[234] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[185])),TRUE); /* EUNATCH */

#if defined(EUNATCH)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[234]),SCM_MAKE_INT(EUNATCH));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EUNATCH),scm__rc.d1786[234],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[234],SCM_MAKE_INT(EUNATCH),0);}
#endif /* defined(EUNATCH) */

  scm__rc.d1786[235] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[186])),TRUE); /* EUSERS */

#if defined(EUSERS)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[235]),SCM_MAKE_INT(EUSERS));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EUSERS),scm__rc.d1786[235],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[235],SCM_MAKE_INT(EUSERS),0);}
#endif /* defined(EUSERS) */

  scm__rc.d1786[236] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[187])),TRUE); /* EWOULDBLOCK */

#if defined(EWOULDBLOCK)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[236]),SCM_MAKE_INT(EWOULDBLOCK));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EWOULDBLOCK),scm__rc.d1786[236],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[236],SCM_MAKE_INT(EWOULDBLOCK),0);}
#endif /* defined(EWOULDBLOCK) */

  scm__rc.d1786[237] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[188])),TRUE); /* EXDEV */

#if defined(EXDEV)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[237]),SCM_MAKE_INT(EXDEV));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EXDEV),scm__rc.d1786[237],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[237],SCM_MAKE_INT(EXDEV),0);}
#endif /* defined(EXDEV) */

  scm__rc.d1786[238] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[189])),TRUE); /* EXFULL */

#if defined(EXFULL)
{Scm_Define(Scm_GaucheModule(),SCM_SYMBOL(scm__rc.d1786[238]),SCM_MAKE_INT(EXFULL));Scm_HashTableSet(errno_n2y,SCM_MAKE_INT(EXFULL),scm__rc.d1786[238],0);Scm_HashTableSet(errno_y2n,scm__rc.d1786[238],SCM_MAKE_INT(EXFULL),0);}
#endif /* defined(EXFULL) */

  scm__rc.d1786[239] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[190])),TRUE); /* sys-getgrgid */
  scm__rc.d1786[240] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[191])),TRUE); /* gid */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[65]), scm__rc.d1786[240]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[68]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[69]), scm__rc.d1786[239]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[70]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[71]), scm__rc.d1786[3]);
  scm__rc.d1786[241] = Scm_MakeExtendedPair(scm__rc.d1786[239], SCM_OBJ(&scm__rc.d1787[65]), SCM_OBJ(&scm__rc.d1787[73]));
  scm__rc.d1786[242] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[192])),TRUE); /* <int> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[243]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[243]))[4] = scm__rc.d1786[242];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[243]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[243]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getgrgid")), SCM_OBJ(&libsyssys_getgrgid__STUB), 0);
  libsyssys_getgrgid__STUB.common.info = scm__rc.d1786[241];
  libsyssys_getgrgid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[243]);
  scm__rc.d1786[250] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[193])),TRUE); /* sys-getgrnam */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[76]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[77]), scm__rc.d1786[250]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[78]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[79]), scm__rc.d1786[3]);
  scm__rc.d1786[251] = Scm_MakeExtendedPair(scm__rc.d1786[250], SCM_OBJ(&scm__rc.d1787[56]), SCM_OBJ(&scm__rc.d1787[81]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getgrnam")), SCM_OBJ(&libsyssys_getgrnam__STUB), 0);
  libsyssys_getgrnam__STUB.common.info = scm__rc.d1786[251];
  libsyssys_getgrnam__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[9]);
  scm__rc.d1786[252] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[194])),TRUE); /* sys-gid->group-name */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[84]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[85]), scm__rc.d1786[252]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[86]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[87]), scm__rc.d1786[3]);
  scm__rc.d1786[253] = Scm_MakeExtendedPair(scm__rc.d1786[252], SCM_OBJ(&scm__rc.d1787[65]), SCM_OBJ(&scm__rc.d1787[89]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-gid->group-name")), SCM_OBJ(&libsyssys_gid_TOgroup_name__STUB), 0);
  libsyssys_gid_TOgroup_name__STUB.common.info = scm__rc.d1786[253];
  libsyssys_gid_TOgroup_name__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[243]);
  scm__rc.d1786[254] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[195])),TRUE); /* sys-group-name->gid */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[92]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[93]), scm__rc.d1786[254]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[94]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[95]), scm__rc.d1786[3]);
  scm__rc.d1786[255] = Scm_MakeExtendedPair(scm__rc.d1786[254], SCM_OBJ(&scm__rc.d1787[56]), SCM_OBJ(&scm__rc.d1787[97]));
  scm__rc.d1786[256] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[196])),TRUE); /* <const-cstring> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[257]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[257]))[4] = scm__rc.d1786[256];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[257]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[257]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-group-name->gid")), SCM_OBJ(&libsyssys_group_name_TOgid__STUB), 0);
  libsyssys_group_name_TOgid__STUB.common.info = scm__rc.d1786[255];
  libsyssys_group_name_TOgid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[257]);
  scm__rc.d1786[264] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[197])),TRUE); /* LC_ALL */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[264]), Scm_MakeInteger(LC_ALL), SCM_BINDING_CONST);

  scm__rc.d1786[265] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[198])),TRUE); /* LC_COLLATE */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[265]), Scm_MakeInteger(LC_COLLATE), SCM_BINDING_CONST);

  scm__rc.d1786[266] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[199])),TRUE); /* LC_CTYPE */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[266]), Scm_MakeInteger(LC_CTYPE), SCM_BINDING_CONST);

  scm__rc.d1786[267] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[200])),TRUE); /* LC_MONETARY */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[267]), Scm_MakeInteger(LC_MONETARY), SCM_BINDING_CONST);

  scm__rc.d1786[268] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[201])),TRUE); /* LC_NUMERIC */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[268]), Scm_MakeInteger(LC_NUMERIC), SCM_BINDING_CONST);

  scm__rc.d1786[269] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[202])),TRUE); /* LC_TIME */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[269]), Scm_MakeInteger(LC_TIME), SCM_BINDING_CONST);

  scm__rc.d1786[270] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[203])),TRUE); /* sys-setlocale */
  scm__rc.d1786[271] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[204])),TRUE); /* category */
  scm__rc.d1786[272] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[205])),TRUE); /* locale */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[98]), scm__rc.d1786[272]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[99]), scm__rc.d1786[271]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[102]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[103]), scm__rc.d1786[270]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[104]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[105]), scm__rc.d1786[3]);
  scm__rc.d1786[273] = Scm_MakeExtendedPair(scm__rc.d1786[270], SCM_OBJ(&scm__rc.d1787[99]), SCM_OBJ(&scm__rc.d1787[107]));
  scm__rc.d1786[274] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[206])),TRUE); /* <const-cstring>? */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[275]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[275]))[4] = scm__rc.d1786[91];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[275]))[5] = scm__rc.d1786[274];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[275]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[275]))[7] = scm__rc.d1786[274];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-setlocale")), SCM_OBJ(&libsyssys_setlocale__STUB), 0);
  libsyssys_setlocale__STUB.common.info = scm__rc.d1786[273];
  libsyssys_setlocale__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[275]);
  scm__rc.d1786[283] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[208])),TRUE); /* decimal_point */
  scm__rc.d1786[284] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[209])),TRUE); /* thousands_sep */
  scm__rc.d1786[285] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[210])),TRUE); /* grouping */
  scm__rc.d1786[286] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[211])),TRUE); /* int_curr_symbol */
  scm__rc.d1786[287] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[212])),TRUE); /* currency_symbol */
  scm__rc.d1786[288] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[213])),TRUE); /* mon_decimal_point */
  scm__rc.d1786[289] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[214])),TRUE); /* mon_thousands_sep */
  scm__rc.d1786[290] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[215])),TRUE); /* mon_grouping */
  scm__rc.d1786[291] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[216])),TRUE); /* positive_sign */
  scm__rc.d1786[292] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[217])),TRUE); /* negative_sign */
  scm__rc.d1786[293] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[218])),TRUE); /* int_frac_digits */
  scm__rc.d1786[294] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[219])),TRUE); /* frac_digits */
  scm__rc.d1786[295] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[220])),TRUE); /* p_cs_precedes */
  scm__rc.d1786[296] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[221])),TRUE); /* p_sep_by_space */
  scm__rc.d1786[297] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[222])),TRUE); /* n_cs_precedes */
  scm__rc.d1786[298] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[223])),TRUE); /* n_sep_by_space */
  scm__rc.d1786[299] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[224])),TRUE); /* p_sign_posn */
  scm__rc.d1786[300] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[225])),TRUE); /* n_sign_posn */
  scm__rc.d1786[301] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[207])),TRUE); /* sys-localeconv */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[108]), scm__rc.d1786[301]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[109]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[110]), scm__rc.d1786[3]);
  scm__rc.d1786[302] = Scm_MakeExtendedPair(scm__rc.d1786[301], SCM_NIL, SCM_OBJ(&scm__rc.d1787[111]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-localeconv")), SCM_OBJ(&libsyssys_localeconv__STUB), 0);
  libsyssys_localeconv__STUB.common.info = scm__rc.d1786[302];
  libsyssys_localeconv__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[18]);
  scm__rc.d1786[303] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[226])),TRUE); /* uid */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[112]), scm__rc.d1786[303]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[115]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[116]), scm__rc.d1786[48]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[117]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[118]), scm__rc.d1786[3]);
  scm__rc.d1786[304] = Scm_MakeExtendedPair(scm__rc.d1786[48], SCM_OBJ(&scm__rc.d1787[112]), SCM_OBJ(&scm__rc.d1787[120]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getpwuid")), SCM_OBJ(&libsyssys_getpwuid__STUB), 0);
  libsyssys_getpwuid__STUB.common.info = scm__rc.d1786[304];
  libsyssys_getpwuid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[243]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[123]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[124]), scm__rc.d1786[55]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[125]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[126]), scm__rc.d1786[3]);
  scm__rc.d1786[305] = Scm_MakeExtendedPair(scm__rc.d1786[55], SCM_OBJ(&scm__rc.d1787[56]), SCM_OBJ(&scm__rc.d1787[128]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getpwnam")), SCM_OBJ(&libsyssys_getpwnam__STUB), 0);
  libsyssys_getpwnam__STUB.common.info = scm__rc.d1786[305];
  libsyssys_getpwnam__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[9]);
  scm__rc.d1786[306] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[227])),TRUE); /* sys-uid->user-name */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[131]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[132]), scm__rc.d1786[306]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[133]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[134]), scm__rc.d1786[3]);
  scm__rc.d1786[307] = Scm_MakeExtendedPair(scm__rc.d1786[306], SCM_OBJ(&scm__rc.d1787[112]), SCM_OBJ(&scm__rc.d1787[136]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-uid->user-name")), SCM_OBJ(&libsyssys_uid_TOuser_name__STUB), 0);
  libsyssys_uid_TOuser_name__STUB.common.info = scm__rc.d1786[307];
  libsyssys_uid_TOuser_name__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[243]);
  scm__rc.d1786[308] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[228])),TRUE); /* sys-user-name->uid */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[139]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[140]), scm__rc.d1786[308]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[141]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[142]), scm__rc.d1786[3]);
  scm__rc.d1786[309] = Scm_MakeExtendedPair(scm__rc.d1786[308], SCM_OBJ(&scm__rc.d1787[56]), SCM_OBJ(&scm__rc.d1787[144]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-user-name->uid")), SCM_OBJ(&libsyssys_user_name_TOuid__STUB), 0);
  libsyssys_user_name_TOuid__STUB.common.info = scm__rc.d1786[309];
  libsyssys_user_name_TOuid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[257]);
  scm__rc.d1786[310] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[229])),TRUE); /* SIG_SETMASK */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[310]), Scm_MakeInteger(SIG_SETMASK), SCM_BINDING_CONST);

  scm__rc.d1786[311] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[230])),TRUE); /* SIG_BLOCK */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[311]), Scm_MakeInteger(SIG_BLOCK), SCM_BINDING_CONST);

  scm__rc.d1786[312] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[231])),TRUE); /* SIG_UNBLOCK */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[312]), Scm_MakeInteger(SIG_UNBLOCK), SCM_BINDING_CONST);

  scm__rc.d1786[313] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[232])),TRUE); /* sys-sigset-add! */
  scm__rc.d1786[314] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[233])),TRUE); /* set */
  scm__rc.d1786[315] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[234]))); /* :rest */
  scm__rc.d1786[316] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[235])),TRUE); /* sigs */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[145]), scm__rc.d1786[316]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[146]), scm__rc.d1786[315]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[147]), scm__rc.d1786[314]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[150]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[151]), scm__rc.d1786[313]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[152]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[153]), scm__rc.d1786[3]);
  scm__rc.d1786[317] = Scm_MakeExtendedPair(scm__rc.d1786[313], SCM_OBJ(&scm__rc.d1787[147]), SCM_OBJ(&scm__rc.d1787[155]));
  scm__rc.d1786[318] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[236])),TRUE); /* <sys-sigset> */
  scm__rc.d1786[319] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[237])),TRUE); /* * */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[320]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[320]))[4] = scm__rc.d1786[318];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[320]))[5] = scm__rc.d1786[319];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[320]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[320]))[7] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-sigset-add!")), SCM_OBJ(&libsyssys_sigset_addX__STUB), 0);
  libsyssys_sigset_addX__STUB.common.info = scm__rc.d1786[317];
  libsyssys_sigset_addX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[320]);
  scm__rc.d1786[328] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[238])),TRUE); /* sys-sigset-delete! */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[158]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[159]), scm__rc.d1786[328]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[160]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[161]), scm__rc.d1786[3]);
  scm__rc.d1786[329] = Scm_MakeExtendedPair(scm__rc.d1786[328], SCM_OBJ(&scm__rc.d1787[147]), SCM_OBJ(&scm__rc.d1787[163]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-sigset-delete!")), SCM_OBJ(&libsyssys_sigset_deleteX__STUB), 0);
  libsyssys_sigset_deleteX__STUB.common.info = scm__rc.d1786[329];
  libsyssys_sigset_deleteX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[320]);
  scm__rc.d1786[330] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[239])),TRUE); /* sys-sigset-fill! */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[164]), scm__rc.d1786[314]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[167]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[168]), scm__rc.d1786[330]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[169]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[170]), scm__rc.d1786[3]);
  scm__rc.d1786[331] = Scm_MakeExtendedPair(scm__rc.d1786[330], SCM_OBJ(&scm__rc.d1787[164]), SCM_OBJ(&scm__rc.d1787[172]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[332]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[332]))[4] = scm__rc.d1786[318];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[332]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[332]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-sigset-fill!")), SCM_OBJ(&libsyssys_sigset_fillX__STUB), 0);
  libsyssys_sigset_fillX__STUB.common.info = scm__rc.d1786[331];
  libsyssys_sigset_fillX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[332]);
  scm__rc.d1786[339] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[240])),TRUE); /* sys-sigset-empty! */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[175]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[176]), scm__rc.d1786[339]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[177]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[178]), scm__rc.d1786[3]);
  scm__rc.d1786[340] = Scm_MakeExtendedPair(scm__rc.d1786[339], SCM_OBJ(&scm__rc.d1787[164]), SCM_OBJ(&scm__rc.d1787[180]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-sigset-empty!")), SCM_OBJ(&libsyssys_sigset_emptyX__STUB), 0);
  libsyssys_sigset_emptyX__STUB.common.info = scm__rc.d1786[340];
  libsyssys_sigset_emptyX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[332]);
  scm__rc.d1786[341] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[241])),TRUE); /* sys-signal-name */
  scm__rc.d1786[342] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[242])),TRUE); /* sig */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[181]), scm__rc.d1786[342]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[184]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[185]), scm__rc.d1786[341]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[186]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[187]), scm__rc.d1786[3]);
  scm__rc.d1786[343] = Scm_MakeExtendedPair(scm__rc.d1786[341], SCM_OBJ(&scm__rc.d1787[181]), SCM_OBJ(&scm__rc.d1787[189]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-signal-name")), SCM_OBJ(&libsyssys_signal_name__STUB), 0);
  libsyssys_signal_name__STUB.common.info = scm__rc.d1786[343];
  libsyssys_signal_name__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[92]);
  scm__rc.d1786[344] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[243])),TRUE); /* sys-kill */
  scm__rc.d1786[345] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[244])),TRUE); /* process */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[190]), scm__rc.d1786[345]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[193]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[194]), scm__rc.d1786[344]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[195]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[196]), scm__rc.d1786[3]);
  scm__rc.d1786[346] = Scm_MakeExtendedPair(scm__rc.d1786[344], SCM_OBJ(&scm__rc.d1787[190]), SCM_OBJ(&scm__rc.d1787[198]));
  scm__rc.d1786[347] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[245])),TRUE); /* <void> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[348]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[348]))[4] = scm__rc.d1786[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[348]))[5] = scm__rc.d1786[91];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[348]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[348]))[7] = scm__rc.d1786[347];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-kill")), SCM_OBJ(&libsyssys_kill__STUB), 0);
  libsyssys_kill__STUB.common.info = scm__rc.d1786[346];
  libsyssys_kill__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[348]);
  scm__rc.d1786[356] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[246])),TRUE); /* set-signal-handler! */
  scm__rc.d1786[357] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[247])),TRUE); /* proc */
  scm__rc.d1786[358] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[248]))); /* :optional */
  scm__rc.d1786[359] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[249])),TRUE); /* mask */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[199]), scm__rc.d1786[359]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[200]), scm__rc.d1786[358]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[201]), scm__rc.d1786[357]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[202]), scm__rc.d1786[342]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[205]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[206]), scm__rc.d1786[356]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[207]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[208]), scm__rc.d1786[3]);
  scm__rc.d1786[360] = Scm_MakeExtendedPair(scm__rc.d1786[356], SCM_OBJ(&scm__rc.d1787[202]), SCM_OBJ(&scm__rc.d1787[210]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[361]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[361]))[4] = scm__rc.d1786[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[361]))[5] = scm__rc.d1786[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[361]))[6] = scm__rc.d1786[319];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[361]))[7] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[361]))[8] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("set-signal-handler!")), SCM_OBJ(&libsysset_signal_handlerX__STUB), 0);
  libsysset_signal_handlerX__STUB.common.info = scm__rc.d1786[360];
  libsysset_signal_handlerX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[361]);
  scm__rc.d1786[370] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[250])),TRUE); /* get-signal-handler */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[213]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[214]), scm__rc.d1786[370]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[215]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[216]), scm__rc.d1786[3]);
  scm__rc.d1786[371] = Scm_MakeExtendedPair(scm__rc.d1786[370], SCM_OBJ(&scm__rc.d1787[181]), SCM_OBJ(&scm__rc.d1787[218]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("get-signal-handler")), SCM_OBJ(&libsysget_signal_handler__STUB), 0);
  libsysget_signal_handler__STUB.common.info = scm__rc.d1786[371];
  libsysget_signal_handler__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[92]);
  scm__rc.d1786[372] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[251])),TRUE); /* get-signal-handler-mask */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[221]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[222]), scm__rc.d1786[372]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[223]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[224]), scm__rc.d1786[3]);
  scm__rc.d1786[373] = Scm_MakeExtendedPair(scm__rc.d1786[372], SCM_OBJ(&scm__rc.d1787[181]), SCM_OBJ(&scm__rc.d1787[226]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("get-signal-handler-mask")), SCM_OBJ(&libsysget_signal_handler_mask__STUB), 0);
  libsysget_signal_handler_mask__STUB.common.info = scm__rc.d1786[373];
  libsysget_signal_handler_mask__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[92]);
  scm__rc.d1786[374] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[252])),TRUE); /* get-signal-handlers */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[227]), scm__rc.d1786[374]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[228]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[229]), scm__rc.d1786[3]);
  scm__rc.d1786[375] = Scm_MakeExtendedPair(scm__rc.d1786[374], SCM_NIL, SCM_OBJ(&scm__rc.d1787[230]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("get-signal-handlers")), SCM_OBJ(&libsysget_signal_handlers__STUB), 0);
  libsysget_signal_handlers__STUB.common.info = scm__rc.d1786[375];
  libsysget_signal_handlers__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[18]);
  scm__rc.d1786[376] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[253])),TRUE); /* set-signal-pending-limit */
  scm__rc.d1786[377] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[254])),TRUE); /* limit */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[231]), scm__rc.d1786[377]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[234]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[235]), scm__rc.d1786[376]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[236]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[237]), scm__rc.d1786[3]);
  scm__rc.d1786[378] = Scm_MakeExtendedPair(scm__rc.d1786[376], SCM_OBJ(&scm__rc.d1787[231]), SCM_OBJ(&scm__rc.d1787[239]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[379]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[379]))[4] = scm__rc.d1786[91];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[379]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[379]))[6] = scm__rc.d1786[347];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("set-signal-pending-limit")), SCM_OBJ(&libsysset_signal_pending_limit__STUB), 0);
  libsysset_signal_pending_limit__STUB.common.info = scm__rc.d1786[378];
  libsysset_signal_pending_limit__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[379]);
  scm__rc.d1786[386] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[255])),TRUE); /* get-signal-pending-limit */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[240]), scm__rc.d1786[386]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[241]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[242]), scm__rc.d1786[3]);
  scm__rc.d1786[387] = Scm_MakeExtendedPair(scm__rc.d1786[386], SCM_NIL, SCM_OBJ(&scm__rc.d1787[243]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[388]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[388]))[4] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[388]))[5] = scm__rc.d1786[242];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("get-signal-pending-limit")), SCM_OBJ(&libsysget_signal_pending_limit__STUB), 0);
  libsysget_signal_pending_limit__STUB.common.info = scm__rc.d1786[387];
  libsysget_signal_pending_limit__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[388]);
  scm__rc.d1786[394] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[256])),TRUE); /* sys-sigmask */
  scm__rc.d1786[395] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[257])),TRUE); /* how */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[244]), scm__rc.d1786[395]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[247]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[248]), scm__rc.d1786[394]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[249]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[250]), scm__rc.d1786[3]);
  scm__rc.d1786[396] = Scm_MakeExtendedPair(scm__rc.d1786[394], SCM_OBJ(&scm__rc.d1787[244]), SCM_OBJ(&scm__rc.d1787[252]));
  scm__rc.d1786[397] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[258])),TRUE); /* <sys-sigset>? */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[398]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[398]))[4] = scm__rc.d1786[91];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[398]))[5] = scm__rc.d1786[397];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[398]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[398]))[7] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-sigmask")), SCM_OBJ(&libsyssys_sigmask__STUB), 0);
  libsyssys_sigmask__STUB.common.info = scm__rc.d1786[396];
  libsyssys_sigmask__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[398]);
  scm__rc.d1786[406] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[259])),TRUE); /* sys-sigsuspend */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[255]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[256]), scm__rc.d1786[406]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[257]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[258]), scm__rc.d1786[3]);
  scm__rc.d1786[407] = Scm_MakeExtendedPair(scm__rc.d1786[406], SCM_OBJ(&scm__rc.d1787[199]), SCM_OBJ(&scm__rc.d1787[260]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-sigsuspend")), SCM_OBJ(&libsyssys_sigsuspend__STUB), 0);
  libsyssys_sigsuspend__STUB.common.info = scm__rc.d1786[407];
  libsyssys_sigsuspend__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[332]);
  scm__rc.d1786[408] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[260])),TRUE); /* sys-sigwait */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[263]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[264]), scm__rc.d1786[408]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[265]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[266]), scm__rc.d1786[3]);
  scm__rc.d1786[409] = Scm_MakeExtendedPair(scm__rc.d1786[408], SCM_OBJ(&scm__rc.d1787[199]), SCM_OBJ(&scm__rc.d1787[268]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[410]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[410]))[4] = scm__rc.d1786[318];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[410]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[410]))[6] = scm__rc.d1786[242];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-sigwait")), SCM_OBJ(&libsyssys_sigwait__STUB), 0);
  libsyssys_sigwait__STUB.common.info = scm__rc.d1786[409];
  libsyssys_sigwait__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[410]);
  scm__rc.d1786[417] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[261])),TRUE); /* sys-sigset */
  scm__rc.d1786[419] = SCM_OBJ(Scm_FindModule(SCM_SYMBOL(scm__rc.d1786[4]), SCM_FIND_MODULE_CREATE|SCM_FIND_MODULE_PLACEHOLDING)); /* module gauche */
  scm__rc.d1786[418] = Scm_MakeIdentifier(scm__rc.d1786[318], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#<sys-sigset> */
  scm__rc.d1786[421] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[262])),TRUE); /* make */
  scm__rc.d1786[420] = Scm_MakeIdentifier(scm__rc.d1786[421], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#make */
  scm__rc.d1786[422] = Scm_MakeIdentifier(scm__rc.d1786[313], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#sys-sigset-add! */
  scm__rc.d1786[423] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[263])),TRUE); /* signals */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[271]), scm__rc.d1786[2]);
  scm__rc.d1786[424] = Scm_MakeExtendedPair(scm__rc.d1786[417], scm__rc.d1786[423], SCM_OBJ(&scm__rc.d1787[272]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[273]), scm__rc.d1786[424]);
  scm__rc.d1786[425] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[2])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[2]))->name = scm__rc.d1786[417];/* sys-sigset */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[2]))->debugInfo = scm__rc.d1786[425];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[507]))[4] = SCM_WORD(scm__rc.d1786[418]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[507]))[6] = SCM_WORD(scm__rc.d1786[420]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[507]))[9] = SCM_WORD(scm__rc.d1786[422]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[507]))[13] = SCM_WORD(scm__rc.d1786[418]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[507]))[15] = SCM_WORD(scm__rc.d1786[420]);
  scm__rc.d1786[426] = Scm_MakeIdentifier(scm__rc.d1786[417], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#sys-sigset */
  scm__rc.d1786[427] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[3])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[3]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[3]))->debugInfo = scm__rc.d1786[427];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[527]))[3] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[527]))[6] = SCM_WORD(scm__rc.d1786[417]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[527]))[13] = SCM_WORD(scm__rc.d1786[426]);
  scm__rc.d1786[428] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[264])),TRUE); /* sys-remove */
  scm__rc.d1786[429] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[265])),TRUE); /* filename */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[274]), scm__rc.d1786[429]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[277]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[278]), scm__rc.d1786[428]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[279]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[280]), scm__rc.d1786[3]);
  scm__rc.d1786[430] = Scm_MakeExtendedPair(scm__rc.d1786[428], SCM_OBJ(&scm__rc.d1787[274]), SCM_OBJ(&scm__rc.d1787[282]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[431]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[431]))[4] = scm__rc.d1786[256];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[431]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[431]))[6] = scm__rc.d1786[347];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-remove")), SCM_OBJ(&libsyssys_remove__STUB), 0);
  libsyssys_remove__STUB.common.info = scm__rc.d1786[430];
  libsyssys_remove__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[431]);
  scm__rc.d1786[438] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[266])),TRUE); /* sys-rename */
  scm__rc.d1786[439] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[267])),TRUE); /* oldname */
  scm__rc.d1786[440] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[268])),TRUE); /* newname */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[283]), scm__rc.d1786[440]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[284]), scm__rc.d1786[439]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[287]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[288]), scm__rc.d1786[438]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[289]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[290]), scm__rc.d1786[3]);
  scm__rc.d1786[441] = Scm_MakeExtendedPair(scm__rc.d1786[438], SCM_OBJ(&scm__rc.d1787[284]), SCM_OBJ(&scm__rc.d1787[292]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[442]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[442]))[4] = scm__rc.d1786[256];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[442]))[5] = scm__rc.d1786[256];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[442]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[442]))[7] = scm__rc.d1786[347];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-rename")), SCM_OBJ(&libsyssys_rename__STUB), 0);
  libsyssys_rename__STUB.common.info = scm__rc.d1786[441];
  libsyssys_rename__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[442]);
  scm__rc.d1786[450] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[269])),TRUE); /* sys-tmpnam */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[293]), scm__rc.d1786[450]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[294]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[295]), scm__rc.d1786[3]);
  scm__rc.d1786[451] = Scm_MakeExtendedPair(scm__rc.d1786[450], SCM_NIL, SCM_OBJ(&scm__rc.d1787[296]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-tmpnam")), SCM_OBJ(&libsyssys_tmpnam__STUB), 0);
  libsyssys_tmpnam__STUB.common.info = scm__rc.d1786[451];
  libsyssys_tmpnam__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[18]);
  scm__rc.d1786[452] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[270])),TRUE); /* sys-mkstemp */
  scm__rc.d1786[453] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[271])),TRUE); /* template */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[297]), scm__rc.d1786[453]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[300]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[301]), scm__rc.d1786[452]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[302]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[303]), scm__rc.d1786[3]);
  scm__rc.d1786[454] = Scm_MakeExtendedPair(scm__rc.d1786[452], SCM_OBJ(&scm__rc.d1787[297]), SCM_OBJ(&scm__rc.d1787[305]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-mkstemp")), SCM_OBJ(&libsyssys_mkstemp__STUB), 0);
  libsyssys_mkstemp__STUB.common.info = scm__rc.d1786[454];
  libsyssys_mkstemp__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[9]);
  scm__rc.d1786[455] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[272])),TRUE); /* sys-mkdtemp */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[308]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[309]), scm__rc.d1786[455]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[310]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[311]), scm__rc.d1786[3]);
  scm__rc.d1786[456] = Scm_MakeExtendedPair(scm__rc.d1786[455], SCM_OBJ(&scm__rc.d1787[297]), SCM_OBJ(&scm__rc.d1787[313]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-mkdtemp")), SCM_OBJ(&libsyssys_mkdtemp__STUB), 0);
  libsyssys_mkdtemp__STUB.common.info = scm__rc.d1786[456];
  libsyssys_mkdtemp__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[9]);
  scm__rc.d1786[457] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[273])),TRUE); /* sys-ctermid */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[314]), scm__rc.d1786[457]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[315]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[316]), scm__rc.d1786[3]);
  scm__rc.d1786[458] = Scm_MakeExtendedPair(scm__rc.d1786[457], SCM_NIL, SCM_OBJ(&scm__rc.d1787[317]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-ctermid")), SCM_OBJ(&libsyssys_ctermid__STUB), 0);
  libsyssys_ctermid__STUB.common.info = scm__rc.d1786[458];
  libsyssys_ctermid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[18]);
  scm__rc.d1786[459] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[275])),TRUE); /* sys-exit */
  scm__rc.d1786[460] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[276])),TRUE); /* code */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[318]), scm__rc.d1786[460]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[321]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[322]), scm__rc.d1786[459]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[323]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[324]), scm__rc.d1786[3]);
  scm__rc.d1786[461] = Scm_MakeExtendedPair(scm__rc.d1786[459], SCM_OBJ(&scm__rc.d1787[318]), SCM_OBJ(&scm__rc.d1787[326]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[462]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[462]))[4] = scm__rc.d1786[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[462]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[462]))[6] = scm__rc.d1786[347];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-exit")), SCM_OBJ(&libsyssys_exit__STUB), 0);
  libsyssys_exit__STUB.common.info = scm__rc.d1786[461];
  libsyssys_exit__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[462]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[329]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[330]), scm__rc.d1786[44]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[331]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[332]), scm__rc.d1786[3]);
  scm__rc.d1786[469] = Scm_MakeExtendedPair(scm__rc.d1786[44], SCM_OBJ(&scm__rc.d1787[56]), SCM_OBJ(&scm__rc.d1787[334]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[470]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[470]))[4] = scm__rc.d1786[256];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[470]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[470]))[6] = scm__rc.d1786[274];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getenv")), SCM_OBJ(&libsyssys_getenv__STUB), 0);
  libsyssys_getenv__STUB.common.info = scm__rc.d1786[469];
  libsyssys_getenv__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[470]);
  scm__rc.d1786[477] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[277])),TRUE); /* sys-abort */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[335]), scm__rc.d1786[477]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[336]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[337]), scm__rc.d1786[3]);
  scm__rc.d1786[478] = Scm_MakeExtendedPair(scm__rc.d1786[477], SCM_NIL, SCM_OBJ(&scm__rc.d1787[338]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[479]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[479]))[4] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[479]))[5] = scm__rc.d1786[347];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-abort")), SCM_OBJ(&libsyssys_abort__STUB), 0);
  libsyssys_abort__STUB.common.info = scm__rc.d1786[478];
  libsyssys_abort__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[479]);
  scm__rc.d1786[485] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[278])),TRUE); /* sys-system */
  scm__rc.d1786[486] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[279])),TRUE); /* command */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[339]), scm__rc.d1786[486]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[342]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[343]), scm__rc.d1786[485]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[344]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[345]), scm__rc.d1786[3]);
  scm__rc.d1786[487] = Scm_MakeExtendedPair(scm__rc.d1786[485], SCM_OBJ(&scm__rc.d1787[339]), SCM_OBJ(&scm__rc.d1787[347]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[488]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[488]))[4] = scm__rc.d1786[256];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[488]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[488]))[6] = scm__rc.d1786[242];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-system")), SCM_OBJ(&libsyssys_system__STUB), 0);
  libsyssys_system__STUB.common.info = scm__rc.d1786[487];
  libsyssys_system__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[488]);
  scm__rc.d1786[495] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[280])),TRUE); /* sys-random */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[348]), scm__rc.d1786[495]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[349]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[350]), scm__rc.d1786[3]);
  scm__rc.d1786[496] = Scm_MakeExtendedPair(scm__rc.d1786[495], SCM_NIL, SCM_OBJ(&scm__rc.d1787[351]));
  scm__rc.d1786[497] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[281])),TRUE); /* <long> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[498]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[498]))[4] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[498]))[5] = scm__rc.d1786[497];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-random")), SCM_OBJ(&libsyssys_random__STUB), 0);
  libsyssys_random__STUB.common.info = scm__rc.d1786[496];
  libsyssys_random__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[498]);
  scm__rc.d1786[504] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[282])),TRUE); /* sys-srandom */
  scm__rc.d1786[505] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[283])),TRUE); /* seed */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[352]), scm__rc.d1786[505]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[355]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[356]), scm__rc.d1786[504]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[357]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[358]), scm__rc.d1786[3]);
  scm__rc.d1786[506] = Scm_MakeExtendedPair(scm__rc.d1786[504], SCM_OBJ(&scm__rc.d1787[352]), SCM_OBJ(&scm__rc.d1787[360]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-srandom")), SCM_OBJ(&libsyssys_srandom__STUB), 0);
  libsyssys_srandom__STUB.common.info = scm__rc.d1786[506];
  libsyssys_srandom__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[462]);
  scm__rc.d1786[507] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[284])),TRUE); /* RAND_MAX */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[507]), Scm_MakeIntegerFromUI(RAND_MAX), SCM_BINDING_CONST);

  scm__rc.d1786[508] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[285])),TRUE); /* sys-environ */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[361]), scm__rc.d1786[508]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[362]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[363]), scm__rc.d1786[3]);
  scm__rc.d1786[509] = Scm_MakeExtendedPair(scm__rc.d1786[508], SCM_NIL, SCM_OBJ(&scm__rc.d1787[364]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-environ")), SCM_OBJ(&libsyssys_environ__STUB), 0);
  libsyssys_environ__STUB.common.info = scm__rc.d1786[509];
  libsyssys_environ__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[18]);
  scm__rc.d1786[510] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[286])),TRUE); /* sys-setenv */
  scm__rc.d1786[511] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[287])),TRUE); /* value */
  scm__rc.d1786[512] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[288])),TRUE); /* overwrite */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[365]), scm__rc.d1786[512]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[366]), scm__rc.d1786[358]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[367]), scm__rc.d1786[511]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[368]), scm__rc.d1786[100]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[371]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[372]), scm__rc.d1786[510]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[373]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[374]), scm__rc.d1786[3]);
  scm__rc.d1786[513] = Scm_MakeExtendedPair(scm__rc.d1786[510], SCM_OBJ(&scm__rc.d1787[368]), SCM_OBJ(&scm__rc.d1787[376]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[514]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[514]))[4] = scm__rc.d1786[256];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[514]))[5] = scm__rc.d1786[256];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[514]))[6] = scm__rc.d1786[319];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[514]))[7] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[514]))[8] = scm__rc.d1786[347];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-setenv")), SCM_OBJ(&libsyssys_setenv__STUB), 0);
  libsyssys_setenv__STUB.common.info = scm__rc.d1786[513];
  libsyssys_setenv__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[514]);
  scm__rc.d1786[523] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[289])),TRUE); /* sys-unsetenv */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[379]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[380]), scm__rc.d1786[523]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[381]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[382]), scm__rc.d1786[3]);
  scm__rc.d1786[524] = Scm_MakeExtendedPair(scm__rc.d1786[523], SCM_OBJ(&scm__rc.d1787[56]), SCM_OBJ(&scm__rc.d1787[384]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-unsetenv")), SCM_OBJ(&libsyssys_unsetenv__STUB), 0);
  libsyssys_unsetenv__STUB.common.info = scm__rc.d1786[524];
  libsyssys_unsetenv__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[431]);
  scm__rc.d1786[525] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[290])),TRUE); /* sys-clearenv */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[385]), scm__rc.d1786[525]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[386]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[387]), scm__rc.d1786[3]);
  scm__rc.d1786[526] = Scm_MakeExtendedPair(scm__rc.d1786[525], SCM_NIL, SCM_OBJ(&scm__rc.d1787[388]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-clearenv")), SCM_OBJ(&libsyssys_clearenv__STUB), 0);
  libsyssys_clearenv__STUB.common.info = scm__rc.d1786[526];
  libsyssys_clearenv__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[479]);
  scm__rc.d1786[527] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[291])),TRUE); /* both */
  scm__rc.d1786[529] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[292])),TRUE); /* string-scan */
  scm__rc.d1786[528] = Scm_MakeIdentifier(scm__rc.d1786[529], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#string-scan */
  scm__rc.d1786[530] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[293])),TRUE); /* sys-environ->alist */
  scm__rc.d1786[531] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[294])),FALSE); /* G1802 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[389]), scm__rc.d1786[531]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[390]), scm__rc.d1786[530]);
  scm__rc.d1786[532] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[295])),TRUE); /* envstr */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[391]), scm__rc.d1786[532]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[394]), scm__rc.d1786[2]);
  scm__rc.d1786[533] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[390]), SCM_OBJ(&scm__rc.d1787[391]), SCM_OBJ(&scm__rc.d1787[395]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[396]), scm__rc.d1786[533]);
  scm__rc.d1786[534] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[4])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[4]))->debugInfo = scm__rc.d1786[534];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[542]))[6] = SCM_WORD(scm__rc.d1786[527]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[542]))[8] = SCM_WORD(scm__rc.d1786[528]);
  scm__rc.d1786[535] = Scm_MakeIdentifier(scm__rc.d1786[508], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#sys-environ */
  scm__rc.d1786[536] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[297])),TRUE); /* lambda */
  scm__rc.d1786[537] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[298])),TRUE); /* envlist */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[399]), scm__rc.d1786[537]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[401]), scm__rc.d1786[358]);
  scm__rc.d1786[538] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[299])),TRUE); /* map */
  scm__rc.d1786[539] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[300])),TRUE); /* ^ */
  scm__rc.d1786[540] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[301])),TRUE); /* receive */
  scm__rc.d1786[541] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[302])),TRUE); /* pre */
  scm__rc.d1786[542] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[303])),TRUE); /* post */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[402]), scm__rc.d1786[542]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[403]), scm__rc.d1786[541]);
  scm__rc.d1786[543] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[304])),TRUE); /* quote */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[404]), scm__rc.d1786[527]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[405]), scm__rc.d1786[543]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[408]), scm__rc.d1786[532]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[409]), scm__rc.d1786[529]);
  scm__rc.d1786[544] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[305])),TRUE); /* if */
  scm__rc.d1786[545] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[306])),TRUE); /* cons */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[410]), scm__rc.d1786[545]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[411]), scm__rc.d1786[532]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[412]), scm__rc.d1786[545]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[415]), scm__rc.d1786[541]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[416]), scm__rc.d1786[544]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[420]), scm__rc.d1786[540]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[423]), scm__rc.d1786[539]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[424]), scm__rc.d1786[537]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[426]), scm__rc.d1786[538]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[429]), scm__rc.d1786[536]);
  scm__rc.d1786[546] = Scm_MakeIdentifier(scm__rc.d1786[538], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#map */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[432]), scm__rc.d1786[2]);
  scm__rc.d1786[547] = Scm_MakeExtendedPair(scm__rc.d1786[530], SCM_OBJ(&scm__rc.d1787[401]), SCM_OBJ(&scm__rc.d1787[433]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[434]), scm__rc.d1786[547]);
  scm__rc.d1786[548] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[5])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[5]))->name = scm__rc.d1786[530];/* sys-environ->alist */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[5]))->debugInfo = scm__rc.d1786[548];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[564]))[6] = SCM_WORD(scm__rc.d1786[535]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[564]))[31] = SCM_WORD(scm__rc.d1786[71]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[564]))[35] = SCM_WORD(scm__rc.d1786[546]);
  scm__rc.d1786[549] = Scm_MakeIdentifier(scm__rc.d1786[530], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#sys-environ->alist */
  scm__rc.d1786[550] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[6])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[6]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[6]))->debugInfo = scm__rc.d1786[550];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[601]))[5] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[601]))[8] = SCM_WORD(scm__rc.d1786[530]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[601]))[15] = SCM_WORD(scm__rc.d1786[549]);
  scm__rc.d1786[551] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[307])),TRUE); /* sys-putenv */
  scm__rc.d1786[552] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[309])),TRUE); /* arg */
  scm__rc.d1786[553] = Scm_MakeIdentifier(scm__rc.d1786[53], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#error */
  scm__rc.d1786[554] = Scm_MakeIdentifier(scm__rc.d1786[510], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#sys-setenv */
  scm__rc.d1786[555] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[311])),TRUE); /* name=value */
  scm__rc.d1786[556] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[312])),TRUE); /* other */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[435]), scm__rc.d1786[555]);
  SCM_SET_CDR(SCM_OBJ(&scm__rc.d1787[435]), scm__rc.d1786[556]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[438]), scm__rc.d1786[2]);
  scm__rc.d1786[557] = Scm_MakeExtendedPair(scm__rc.d1786[551], SCM_OBJ(&scm__rc.d1787[435]), SCM_OBJ(&scm__rc.d1787[439]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[440]), scm__rc.d1786[557]);
  scm__rc.d1786[558] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[7])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[7]))->name = scm__rc.d1786[551];/* sys-putenv */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[7]))->debugInfo = scm__rc.d1786[558];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[618]))[14] = SCM_WORD(scm__rc.d1786[552]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[618]))[17] = SCM_WORD(scm__rc.d1786[77]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[618]))[24] = SCM_WORD(scm__rc.d1786[527]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[618]))[26] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[618]))[39] = SCM_WORD(scm__rc.d1786[553]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[618]))[45] = SCM_WORD(scm__rc.d1786[554]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[618]))[53] = SCM_WORD(scm__rc.d1786[554]);
  scm__rc.d1786[559] = Scm_MakeIdentifier(scm__rc.d1786[551], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#sys-putenv */
  scm__rc.d1786[560] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[8])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[8]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[8]))->debugInfo = scm__rc.d1786[560];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[673]))[3] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[673]))[6] = SCM_WORD(scm__rc.d1786[551]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[673]))[13] = SCM_WORD(scm__rc.d1786[559]);
  scm__rc.d1786[561] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[313])),TRUE); /* sys-strerror */
  scm__rc.d1786[562] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[314])),TRUE); /* errno_ */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[441]), scm__rc.d1786[562]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[444]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[445]), scm__rc.d1786[561]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[446]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[447]), scm__rc.d1786[3]);
  scm__rc.d1786[563] = Scm_MakeExtendedPair(scm__rc.d1786[561], SCM_OBJ(&scm__rc.d1787[441]), SCM_OBJ(&scm__rc.d1787[449]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[564]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[564]))[4] = scm__rc.d1786[242];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[564]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[564]))[6] = scm__rc.d1786[256];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-strerror")), SCM_OBJ(&libsyssys_strerror__STUB), 0);
  libsyssys_strerror__STUB.common.info = scm__rc.d1786[563];
  libsyssys_strerror__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[564]);
#if HAVE_STRSIGNAL
  scm__rc.d1804[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1803[0])),TRUE); /* sys-strsignal */
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
  scm__rc.d1804[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1803[1])),TRUE); /* signum */
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1805[1]), scm__rc.d1804[1]);
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
  scm__rc.d1804[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1803[2])),TRUE); /* source-info */
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1805[4]), scm__rc.d1804[2]);
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
  scm__rc.d1804[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1803[4])),TRUE); /* bind-info */
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
  scm__rc.d1804[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1803[5])),TRUE); /* gauche */
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1805[5]), scm__rc.d1804[0]);
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1805[6]), scm__rc.d1804[4]);
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1805[7]), scm__rc.d1804[3]);
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
  scm__rc.d1804[5] = Scm_MakeExtendedPair(scm__rc.d1804[0], SCM_OBJ(&scm__rc.d1805[1]), SCM_OBJ(&scm__rc.d1805[9]));
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
  scm__rc.d1804[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1803[6])),TRUE); /* <int> */
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
  scm__rc.d1804[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1803[7])),TRUE); /* -> */
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
  scm__rc.d1804[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1803[8])),TRUE); /* <const-cstring>? */
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
  ((ScmObj*)SCM_OBJ(&scm__rc.d1804[9]))[3] = scm__rc.d1804[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1804[9]))[4] = scm__rc.d1804[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1804[9]))[5] = scm__rc.d1804[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1804[9]))[6] = scm__rc.d1804[8];
#endif /* HAVE_STRSIGNAL */
#if HAVE_STRSIGNAL
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-strsignal")), SCM_OBJ(&libsyssys_strsignal__STUB), 0);
  libsyssys_strsignal__STUB.common.info = scm__rc.d1804[5];
  libsyssys_strsignal__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1804[9]);
#endif /* HAVE_STRSIGNAL */
#if !(HAVE_STRSIGNAL)
  scm__rc.d1807[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1806[0])),TRUE); /* sys-strsignal */
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
  scm__rc.d1807[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1806[1])),TRUE); /* _ */
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1808[1]), scm__rc.d1807[1]);
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
  scm__rc.d1807[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1806[2])),TRUE); /* source-info */
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1808[4]), scm__rc.d1807[2]);
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
  scm__rc.d1807[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1806[4])),TRUE); /* bind-info */
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
  scm__rc.d1807[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1806[5])),TRUE); /* gauche */
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1808[5]), scm__rc.d1807[0]);
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1808[6]), scm__rc.d1807[4]);
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1808[7]), scm__rc.d1807[3]);
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
  scm__rc.d1807[5] = Scm_MakeExtendedPair(scm__rc.d1807[0], SCM_OBJ(&scm__rc.d1808[1]), SCM_OBJ(&scm__rc.d1808[9]));
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
  scm__rc.d1807[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1806[6])),TRUE); /* <int> */
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
  scm__rc.d1807[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1806[7])),TRUE); /* -> */
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
  scm__rc.d1807[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1806[8])),TRUE); /* <const-cstring>? */
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1807[9]))[3] = scm__rc.d1807[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1807[9]))[4] = scm__rc.d1807[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1807[9]))[5] = scm__rc.d1807[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1807[9]))[6] = scm__rc.d1807[8];
#endif /* !(HAVE_STRSIGNAL) */
#if !(HAVE_STRSIGNAL)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-strsignal")), SCM_OBJ(&libsyssys_strsignal__STUB), 0);
  libsyssys_strsignal__STUB.common.info = scm__rc.d1807[5];
  libsyssys_strsignal__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1807[9]);
#endif /* !(HAVE_STRSIGNAL) */
#if defined(HAVE_GETLOADAVG)
  scm__rc.d1811[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1809[0])),TRUE); /* sys-getloadavg */
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  scm__rc.d1811[1] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1809[1]))); /* :optional */
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  scm__rc.d1811[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1809[2])),TRUE); /* nsamples */
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1812[1]), scm__rc.d1811[2]);
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1812[2]), scm__rc.d1811[1]);
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  scm__rc.d1811[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1809[3])),TRUE); /* source-info */
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1812[5]), scm__rc.d1811[3]);
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  scm__rc.d1811[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1809[5])),TRUE); /* bind-info */
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  scm__rc.d1811[5] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1809[6])),TRUE); /* gauche */
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1812[6]), scm__rc.d1811[0]);
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1812[7]), scm__rc.d1811[5]);
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1812[8]), scm__rc.d1811[4]);
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  scm__rc.d1811[6] = Scm_MakeExtendedPair(scm__rc.d1811[0], SCM_OBJ(&scm__rc.d1812[2]), SCM_OBJ(&scm__rc.d1812[10]));
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  scm__rc.d1811[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1809[7])),TRUE); /* * */
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  scm__rc.d1811[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1809[8])),TRUE); /* -> */
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  scm__rc.d1811[9] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1809[9])),TRUE); /* <top> */
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1811[10]))[3] = scm__rc.d1811[5];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1811[10]))[4] = scm__rc.d1811[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1811[10]))[5] = scm__rc.d1811[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1811[10]))[6] = scm__rc.d1811[9];
#endif /* defined(HAVE_GETLOADAVG) */
#if defined(HAVE_GETLOADAVG)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getloadavg")), SCM_OBJ(&libsyssys_getloadavg__STUB), 0);
  libsyssys_getloadavg__STUB.common.info = scm__rc.d1811[6];
  libsyssys_getloadavg__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1811[10]);
#endif /* defined(HAVE_GETLOADAVG) */
#if !(defined(HAVE_GETLOADAVG))
  scm__rc.d1814[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1813[0])),TRUE); /* sys-getloadavg */
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  scm__rc.d1814[1] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1813[1]))); /* :optional */
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  scm__rc.d1814[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1813[2])),TRUE); /* _ */
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1815[1]), scm__rc.d1814[2]);
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1815[2]), scm__rc.d1814[1]);
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  scm__rc.d1814[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1813[3])),TRUE); /* source-info */
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1815[5]), scm__rc.d1814[3]);
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  scm__rc.d1814[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1813[5])),TRUE); /* bind-info */
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  scm__rc.d1814[5] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1813[6])),TRUE); /* gauche */
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1815[6]), scm__rc.d1814[0]);
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1815[7]), scm__rc.d1814[5]);
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1815[8]), scm__rc.d1814[4]);
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  scm__rc.d1814[6] = Scm_MakeExtendedPair(scm__rc.d1814[0], SCM_OBJ(&scm__rc.d1815[2]), SCM_OBJ(&scm__rc.d1815[10]));
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  scm__rc.d1814[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1813[7])),TRUE); /* * */
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  scm__rc.d1814[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1813[8])),TRUE); /* -> */
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  scm__rc.d1814[9] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1813[9])),TRUE); /* <top> */
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  ((ScmObj*)SCM_OBJ(&scm__rc.d1814[10]))[3] = scm__rc.d1814[5];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1814[10]))[4] = scm__rc.d1814[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1814[10]))[5] = scm__rc.d1814[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1814[10]))[6] = scm__rc.d1814[9];
#endif /* !(defined(HAVE_GETLOADAVG)) */
#if !(defined(HAVE_GETLOADAVG))
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getloadavg")), SCM_OBJ(&libsyssys_getloadavg__STUB), 0);
  libsyssys_getloadavg__STUB.common.info = scm__rc.d1814[6];
  libsyssys_getloadavg__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1814[10]);
#endif /* !(defined(HAVE_GETLOADAVG)) */
  Scm_InitStaticClassWithMeta(&Scm_MemoryRegionClass, "<memory-region>", SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), NULL, SCM_FALSE, Scm_MemoryRegionClass__SLOTS, 0);
  scm__rc.d1786[571] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[315])),TRUE); /* sys-mmap */
  scm__rc.d1786[572] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[316])),TRUE); /* maybe-port */
  scm__rc.d1786[573] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[317])),TRUE); /* prot */
  scm__rc.d1786[574] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[318])),TRUE); /* flags */
  scm__rc.d1786[575] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[319])),TRUE); /* size */
  scm__rc.d1786[576] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[320])),TRUE); /* off */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[450]), scm__rc.d1786[576]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[451]), scm__rc.d1786[358]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[452]), scm__rc.d1786[575]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[453]), scm__rc.d1786[574]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[454]), scm__rc.d1786[573]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[455]), scm__rc.d1786[572]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[458]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[459]), scm__rc.d1786[571]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[460]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[461]), scm__rc.d1786[3]);
  scm__rc.d1786[577] = Scm_MakeExtendedPair(scm__rc.d1786[571], SCM_OBJ(&scm__rc.d1787[455]), SCM_OBJ(&scm__rc.d1787[463]));
  scm__rc.d1786[578] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[321])),TRUE); /* <size_t> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[579]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[579]))[4] = scm__rc.d1786[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[579]))[5] = scm__rc.d1786[242];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[579]))[6] = scm__rc.d1786[242];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[579]))[7] = scm__rc.d1786[578];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[579]))[8] = scm__rc.d1786[319];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[579]))[9] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[579]))[10] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-mmap")), SCM_OBJ(&libsyssys_mmap__STUB), 0);
  libsyssys_mmap__STUB.common.info = scm__rc.d1786[577];
  libsyssys_mmap__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[579]);
  scm__rc.d1786[590] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[322])),TRUE); /* PROT_EXEC */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[590]), Scm_MakeInteger(PROT_EXEC), SCM_BINDING_CONST);

  scm__rc.d1786[591] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[323])),TRUE); /* PROT_READ */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[591]), Scm_MakeInteger(PROT_READ), SCM_BINDING_CONST);

  scm__rc.d1786[592] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[324])),TRUE); /* PROT_WRITE */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[592]), Scm_MakeInteger(PROT_WRITE), SCM_BINDING_CONST);

  scm__rc.d1786[593] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[325])),TRUE); /* PROT_NONE */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[593]), Scm_MakeInteger(PROT_NONE), SCM_BINDING_CONST);

  scm__rc.d1786[594] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[326])),TRUE); /* MAP_SHARED */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[594]), Scm_MakeInteger(MAP_SHARED), SCM_BINDING_CONST);

  scm__rc.d1786[595] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[327])),TRUE); /* MAP_PRIVATE */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[595]), Scm_MakeInteger(MAP_PRIVATE), SCM_BINDING_CONST);

  scm__rc.d1786[596] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[328])),TRUE); /* MAP_ANONYMOUS */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[596]), Scm_MakeInteger(MAP_ANONYMOUS), SCM_BINDING_CONST);

#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1816[0])),TRUE); /* sys-getrlimit */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1816[1])),TRUE); /* rsrc */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1818[1]), scm__rc.d1817[1]);
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1816[2])),TRUE); /* source-info */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1818[4]), scm__rc.d1817[2]);
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1816[4])),TRUE); /* bind-info */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1816[5])),TRUE); /* gauche */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1818[5]), scm__rc.d1817[0]);
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1818[6]), scm__rc.d1817[4]);
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1818[7]), scm__rc.d1817[3]);
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[5] = Scm_MakeExtendedPair(scm__rc.d1817[0], SCM_OBJ(&scm__rc.d1818[1]), SCM_OBJ(&scm__rc.d1818[9]));
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1816[6])),TRUE); /* <int> */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1816[7])),TRUE); /* -> */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1816[8])),TRUE); /* <integer> */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1817[9]))[3] = scm__rc.d1817[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1817[9]))[4] = scm__rc.d1817[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1817[9]))[5] = scm__rc.d1817[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1817[9]))[6] = scm__rc.d1817[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1817[9]))[7] = scm__rc.d1817[8];
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getrlimit")), SCM_OBJ(&libsyssys_getrlimit__STUB), 0);
  libsyssys_getrlimit__STUB.common.info = scm__rc.d1817[5];
  libsyssys_getrlimit__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1817[9]);
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[17] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1816[9])),TRUE); /* sys-setrlimit */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[18] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1816[10])),TRUE); /* cur */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[19] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1816[11]))); /* :optional */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[20] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1816[12])),TRUE); /* max */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1818[10]), scm__rc.d1817[20]);
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1818[11]), scm__rc.d1817[19]);
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1818[12]), scm__rc.d1817[18]);
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1818[13]), scm__rc.d1817[1]);
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1818[16]), scm__rc.d1817[2]);
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1818[17]), scm__rc.d1817[17]);
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1818[18]), scm__rc.d1817[4]);
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1818[19]), scm__rc.d1817[3]);
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[21] = Scm_MakeExtendedPair(scm__rc.d1817[17], SCM_OBJ(&scm__rc.d1818[13]), SCM_OBJ(&scm__rc.d1818[21]));
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[22] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1816[13])),TRUE); /* <top> */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[23] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1816[14])),TRUE); /* * */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[24] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1816[15])),TRUE); /* <void> */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1817[25]))[3] = scm__rc.d1817[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1817[25]))[4] = scm__rc.d1817[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1817[25]))[5] = scm__rc.d1817[22];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1817[25]))[6] = scm__rc.d1817[23];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1817[25]))[7] = scm__rc.d1817[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1817[25]))[8] = scm__rc.d1817[24];
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-setrlimit")), SCM_OBJ(&libsyssys_setrlimit__STUB), 0);
  libsyssys_setrlimit__STUB.common.info = scm__rc.d1817[21];
  libsyssys_setrlimit__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1817[25]);
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  scm__rc.d1817[34] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1816[16])),TRUE); /* RLIM_INFINITY */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1817[34]), MAKERLIMIT(RLIM_INFINITY), SCM_BINDING_CONST);

#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_AS)
  scm__rc.d1819[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1820[0])),TRUE); /* RLIMIT_AS */
#endif /* defined(RLIMIT_AS) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_AS)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1819[0]), Scm_MakeInteger(RLIMIT_AS), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_AS) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_CORE)
  scm__rc.d1821[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1822[0])),TRUE); /* RLIMIT_CORE */
#endif /* defined(RLIMIT_CORE) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_CORE)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1821[0]), Scm_MakeInteger(RLIMIT_CORE), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_CORE) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_CPU)
  scm__rc.d1823[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1824[0])),TRUE); /* RLIMIT_CPU */
#endif /* defined(RLIMIT_CPU) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_CPU)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1823[0]), Scm_MakeInteger(RLIMIT_CPU), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_CPU) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_DATA)
  scm__rc.d1825[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1826[0])),TRUE); /* RLIMIT_DATA */
#endif /* defined(RLIMIT_DATA) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_DATA)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1825[0]), Scm_MakeInteger(RLIMIT_DATA), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_DATA) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_FSIZE)
  scm__rc.d1827[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1828[0])),TRUE); /* RLIMIT_FSIZE */
#endif /* defined(RLIMIT_FSIZE) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_FSIZE)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1827[0]), Scm_MakeInteger(RLIMIT_FSIZE), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_FSIZE) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_LOCKS)
  scm__rc.d1829[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1830[0])),TRUE); /* RLIMIT_LOCKS */
#endif /* defined(RLIMIT_LOCKS) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_LOCKS)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1829[0]), Scm_MakeInteger(RLIMIT_LOCKS), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_LOCKS) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_MEMLOCK)
  scm__rc.d1831[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1832[0])),TRUE); /* RLIMIT_MEMLOCK */
#endif /* defined(RLIMIT_MEMLOCK) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_MEMLOCK)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1831[0]), Scm_MakeInteger(RLIMIT_MEMLOCK), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_MEMLOCK) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_MSGQUEUE)
  scm__rc.d1833[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1834[0])),TRUE); /* RLIMIT_MSGQUEUE */
#endif /* defined(RLIMIT_MSGQUEUE) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_MSGQUEUE)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1833[0]), Scm_MakeInteger(RLIMIT_MSGQUEUE), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_MSGQUEUE) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NICE)
  scm__rc.d1835[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1836[0])),TRUE); /* RLIMIT_NICE */
#endif /* defined(RLIMIT_NICE) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NICE)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1835[0]), Scm_MakeInteger(RLIMIT_NICE), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_NICE) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NOFILE)
  scm__rc.d1837[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1838[0])),TRUE); /* RLIMIT_NOFILE */
#endif /* defined(RLIMIT_NOFILE) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NOFILE)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1837[0]), Scm_MakeInteger(RLIMIT_NOFILE), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_NOFILE) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NPROC)
  scm__rc.d1839[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1840[0])),TRUE); /* RLIMIT_NPROC */
#endif /* defined(RLIMIT_NPROC) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_NPROC)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1839[0]), Scm_MakeInteger(RLIMIT_NPROC), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_NPROC) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_RSS)
  scm__rc.d1841[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1842[0])),TRUE); /* RLIMIT_RSS */
#endif /* defined(RLIMIT_RSS) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_RSS)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1841[0]), Scm_MakeInteger(RLIMIT_RSS), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_RSS) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_RTPRIO)
  scm__rc.d1843[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1844[0])),TRUE); /* RLIMIT_RTPRIO */
#endif /* defined(RLIMIT_RTPRIO) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_RTPRIO)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1843[0]), Scm_MakeInteger(RLIMIT_RTPRIO), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_RTPRIO) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_SIGPENDING)
  scm__rc.d1845[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1846[0])),TRUE); /* RLIMIT_SIGPENDING */
#endif /* defined(RLIMIT_SIGPENDING) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_SIGPENDING)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1845[0]), Scm_MakeInteger(RLIMIT_SIGPENDING), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_SIGPENDING) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_SBSIZE)
  scm__rc.d1847[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1848[0])),TRUE); /* RLIMIT_SBSIZE */
#endif /* defined(RLIMIT_SBSIZE) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_SBSIZE)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1847[0]), Scm_MakeInteger(RLIMIT_SBSIZE), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_SBSIZE) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_STACK)
  scm__rc.d1849[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1850[0])),TRUE); /* RLIMIT_STACK */
#endif /* defined(RLIMIT_STACK) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_STACK)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1849[0]), Scm_MakeInteger(RLIMIT_STACK), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_STACK) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_OFILE)
  scm__rc.d1851[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1852[0])),TRUE); /* RLIMIT_OFILE */
#endif /* defined(RLIMIT_OFILE) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
#if defined(HAVE_SYS_RESOURCE_H)
#if defined(RLIMIT_OFILE)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1851[0]), Scm_MakeInteger(RLIMIT_OFILE), SCM_BINDING_CONST);

#endif /* defined(RLIMIT_OFILE) */
#endif /* defined(HAVE_SYS_RESOURCE_H) */
  scm__rc.d1786[597] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[329])),TRUE); /* sys-stat */
  scm__rc.d1786[598] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[330])),TRUE); /* path */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[464]), scm__rc.d1786[598]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[467]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[468]), scm__rc.d1786[597]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[469]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[470]), scm__rc.d1786[3]);
  scm__rc.d1786[599] = Scm_MakeExtendedPair(scm__rc.d1786[597], SCM_OBJ(&scm__rc.d1787[464]), SCM_OBJ(&scm__rc.d1787[472]));
  scm__rc.d1786[600] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[331])),TRUE); /* <sys-stat> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[601]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[601]))[4] = scm__rc.d1786[256];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[601]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[601]))[6] = scm__rc.d1786[600];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-stat")), SCM_OBJ(&libsyssys_stat__STUB), 0);
  libsyssys_stat__STUB.common.info = scm__rc.d1786[599];
  libsyssys_stat__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[601]);
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[0])),TRUE); /* sys-lstat */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[1])),TRUE); /* path */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[1]), scm__rc.d1856[1]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[2])),TRUE); /* source-info */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[4]), scm__rc.d1856[2]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[4])),TRUE); /* bind-info */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[5])),TRUE); /* gauche */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[5]), scm__rc.d1856[0]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[6]), scm__rc.d1856[4]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[7]), scm__rc.d1856[3]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[5] = Scm_MakeExtendedPair(scm__rc.d1856[0], SCM_OBJ(&scm__rc.d1857[1]), SCM_OBJ(&scm__rc.d1857[9]));
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[6])),TRUE); /* <const-cstring> */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[7])),TRUE); /* -> */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[8])),TRUE); /* <sys-stat> */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[9]))[3] = scm__rc.d1856[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[9]))[4] = scm__rc.d1856[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[9]))[5] = scm__rc.d1856[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[9]))[6] = scm__rc.d1856[8];
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-lstat")), SCM_OBJ(&libsyssys_lstat__STUB), 0);
  libsyssys_lstat__STUB.common.info = scm__rc.d1856[5];
  libsyssys_lstat__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1856[9]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(!(defined(GAUCHE_WINDOWS)))
  scm__rc.d1859[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1858[0])),TRUE); /* sys-lstat */
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
  scm__rc.d1859[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1858[1])),TRUE); /* path */
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1860[1]), scm__rc.d1859[1]);
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
  scm__rc.d1859[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1858[2])),TRUE); /* source-info */
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1860[4]), scm__rc.d1859[2]);
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
  scm__rc.d1859[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1858[4])),TRUE); /* bind-info */
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
  scm__rc.d1859[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1858[5])),TRUE); /* gauche */
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1860[5]), scm__rc.d1859[0]);
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1860[6]), scm__rc.d1859[4]);
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1860[7]), scm__rc.d1859[3]);
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
  scm__rc.d1859[5] = Scm_MakeExtendedPair(scm__rc.d1859[0], SCM_OBJ(&scm__rc.d1860[1]), SCM_OBJ(&scm__rc.d1860[9]));
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
  scm__rc.d1859[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1858[6])),TRUE); /* <const-cstring> */
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
  scm__rc.d1859[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1858[7])),TRUE); /* -> */
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
  scm__rc.d1859[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1858[8])),TRUE); /* <sys-stat> */
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
  ((ScmObj*)SCM_OBJ(&scm__rc.d1859[9]))[3] = scm__rc.d1859[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1859[9]))[4] = scm__rc.d1859[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1859[9]))[5] = scm__rc.d1859[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1859[9]))[6] = scm__rc.d1859[8];
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(!(defined(GAUCHE_WINDOWS)))
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-lstat")), SCM_OBJ(&libsyssys_lstat__STUB), 0);
  libsyssys_lstat__STUB.common.info = scm__rc.d1859[5];
  libsyssys_lstat__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1859[9]);
#endif /* !(!(defined(GAUCHE_WINDOWS))) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[16] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[9])),TRUE); /* sys-mkfifo */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[17] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[10])),TRUE); /* mode */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[10]), scm__rc.d1856[17]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[11]), scm__rc.d1856[1]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[14]), scm__rc.d1856[2]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[15]), scm__rc.d1856[16]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[16]), scm__rc.d1856[4]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[17]), scm__rc.d1856[3]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[18] = Scm_MakeExtendedPair(scm__rc.d1856[16], SCM_OBJ(&scm__rc.d1857[11]), SCM_OBJ(&scm__rc.d1857[19]));
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[19] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[11])),TRUE); /* <int> */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[20]))[3] = scm__rc.d1856[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[20]))[4] = scm__rc.d1856[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[20]))[5] = scm__rc.d1856[19];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[20]))[6] = scm__rc.d1856[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[20]))[7] = scm__rc.d1856[19];
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-mkfifo")), SCM_OBJ(&libsyssys_mkfifo__STUB), 0);
  libsyssys_mkfifo__STUB.common.info = scm__rc.d1856[18];
  libsyssys_mkfifo__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1856[20]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
  scm__rc.d1786[608] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[332])),TRUE); /* sys-fstat */
  scm__rc.d1786[609] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[333])),TRUE); /* port-or-fd */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[473]), scm__rc.d1786[609]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[476]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[477]), scm__rc.d1786[608]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[478]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[479]), scm__rc.d1786[3]);
  scm__rc.d1786[610] = Scm_MakeExtendedPair(scm__rc.d1786[608], SCM_OBJ(&scm__rc.d1787[473]), SCM_OBJ(&scm__rc.d1787[481]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[611]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[611]))[4] = scm__rc.d1786[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[611]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[611]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-fstat")), SCM_OBJ(&libsyssys_fstat__STUB), 0);
  libsyssys_fstat__STUB.common.info = scm__rc.d1786[610];
  libsyssys_fstat__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[611]);
  scm__rc.d1786[618] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[334])),TRUE); /* file-exists? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[484]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[485]), scm__rc.d1786[618]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[486]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[487]), scm__rc.d1786[3]);
  scm__rc.d1786[619] = Scm_MakeExtendedPair(scm__rc.d1786[618], SCM_OBJ(&scm__rc.d1787[464]), SCM_OBJ(&scm__rc.d1787[489]));
  scm__rc.d1786[620] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[335])),TRUE); /* <boolean> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[621]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[621]))[4] = scm__rc.d1786[256];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[621]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[621]))[6] = scm__rc.d1786[620];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("file-exists?")), SCM_OBJ(&libsysfile_existsP__STUB), 0);
  libsysfile_existsP__STUB.common.info = scm__rc.d1786[619];
  libsysfile_existsP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[621]);
  scm__rc.d1786[628] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[336])),TRUE); /* file-is-regular? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[492]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[493]), scm__rc.d1786[628]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[494]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[495]), scm__rc.d1786[3]);
  scm__rc.d1786[629] = Scm_MakeExtendedPair(scm__rc.d1786[628], SCM_OBJ(&scm__rc.d1787[464]), SCM_OBJ(&scm__rc.d1787[497]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("file-is-regular?")), SCM_OBJ(&libsysfile_is_regularP__STUB), 0);
  libsysfile_is_regularP__STUB.common.info = scm__rc.d1786[629];
  libsysfile_is_regularP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[621]);
  scm__rc.d1786[630] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[337])),TRUE); /* file-is-directory? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[500]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[501]), scm__rc.d1786[630]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[502]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[503]), scm__rc.d1786[3]);
  scm__rc.d1786[631] = Scm_MakeExtendedPair(scm__rc.d1786[630], SCM_OBJ(&scm__rc.d1787[464]), SCM_OBJ(&scm__rc.d1787[505]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("file-is-directory?")), SCM_OBJ(&libsysfile_is_directoryP__STUB), 0);
  libsysfile_is_directoryP__STUB.common.info = scm__rc.d1786[631];
  libsysfile_is_directoryP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[621]);
  scm__rc.d1786[632] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[338])),TRUE); /* sys-utime */
  scm__rc.d1786[633] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[339])),TRUE); /* atime */
  scm__rc.d1786[634] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[340])),TRUE); /* mtime */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[506]), scm__rc.d1786[634]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[507]), scm__rc.d1786[633]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[508]), scm__rc.d1786[358]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[509]), scm__rc.d1786[598]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[512]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[513]), scm__rc.d1786[632]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[514]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[515]), scm__rc.d1786[3]);
  scm__rc.d1786[635] = Scm_MakeExtendedPair(scm__rc.d1786[632], SCM_OBJ(&scm__rc.d1787[509]), SCM_OBJ(&scm__rc.d1787[517]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[636]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[636]))[4] = scm__rc.d1786[256];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[636]))[5] = scm__rc.d1786[319];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[636]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[636]))[7] = scm__rc.d1786[347];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-utime")), SCM_OBJ(&libsyssys_utime__STUB), 0);
  libsyssys_utime__STUB.common.info = scm__rc.d1786[635];
  libsyssys_utime__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[636]);
  scm__rc.d1786[644] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[341])),TRUE); /* sys-times */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[518]), scm__rc.d1786[644]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[519]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[520]), scm__rc.d1786[3]);
  scm__rc.d1786[645] = Scm_MakeExtendedPair(scm__rc.d1786[644], SCM_NIL, SCM_OBJ(&scm__rc.d1787[521]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-times")), SCM_OBJ(&libsyssys_times__STUB), 0);
  libsyssys_times__STUB.common.info = scm__rc.d1786[645];
  libsyssys_times__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[18]);
  scm__rc.d1786[646] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[342])),TRUE); /* sys-uname */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[522]), scm__rc.d1786[646]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[523]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[524]), scm__rc.d1786[3]);
  scm__rc.d1786[647] = Scm_MakeExtendedPair(scm__rc.d1786[646], SCM_NIL, SCM_OBJ(&scm__rc.d1787[525]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-uname")), SCM_OBJ(&libsyssys_uname__STUB), 0);
  libsyssys_uname__STUB.common.info = scm__rc.d1786[647];
  libsyssys_uname__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[18]);
  scm__rc.d1786[648] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[343])),TRUE); /* sys-wait */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[526]), scm__rc.d1786[648]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[527]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[528]), scm__rc.d1786[3]);
  scm__rc.d1786[649] = Scm_MakeExtendedPair(scm__rc.d1786[648], SCM_NIL, SCM_OBJ(&scm__rc.d1787[529]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-wait")), SCM_OBJ(&libsyssys_wait__STUB), 0);
  libsyssys_wait__STUB.common.info = scm__rc.d1786[649];
  libsyssys_wait__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[18]);
  scm__rc.d1786[650] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[344]))); /* :nohang */
  scm__rc.d1786[651] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[345]))); /* :untraced */
  scm__rc.d1786[652] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[346])),TRUE); /* sys-waitpid */
  scm__rc.d1786[653] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[344])),TRUE); /* nohang */
  scm__rc.d1786[654] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[345])),TRUE); /* untraced */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[530]), scm__rc.d1786[654]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[531]), scm__rc.d1786[653]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[532]), scm__rc.d1786[79]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[533]), scm__rc.d1786[345]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[536]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[537]), scm__rc.d1786[652]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[538]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[539]), scm__rc.d1786[3]);
  scm__rc.d1786[655] = Scm_MakeExtendedPair(scm__rc.d1786[652], SCM_OBJ(&scm__rc.d1787[533]), SCM_OBJ(&scm__rc.d1787[541]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[656]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[656]))[4] = scm__rc.d1786[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[656]))[5] = scm__rc.d1786[319];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[656]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[656]))[7] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-waitpid")), SCM_OBJ(&libsyssys_waitpid__STUB), 0);
  libsyssys_waitpid__STUB.common.info = scm__rc.d1786[655];
  libsyssys_waitpid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[656]);
  scm__rc.d1786[664] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[347])),TRUE); /* sys-wait-exited? */
  scm__rc.d1786[665] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[348])),TRUE); /* status */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[542]), scm__rc.d1786[665]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[545]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[546]), scm__rc.d1786[664]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[547]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[548]), scm__rc.d1786[3]);
  scm__rc.d1786[666] = Scm_MakeExtendedPair(scm__rc.d1786[664], SCM_OBJ(&scm__rc.d1787[542]), SCM_OBJ(&scm__rc.d1787[550]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[667]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[667]))[4] = scm__rc.d1786[242];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[667]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[667]))[6] = scm__rc.d1786[620];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-wait-exited?")), SCM_OBJ(&libsyssys_wait_exitedP__STUB), 0);
  libsyssys_wait_exitedP__STUB.common.info = scm__rc.d1786[666];
  libsyssys_wait_exitedP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[667]);
  scm__rc.d1786[674] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[349])),TRUE); /* sys-wait-exit-status */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[553]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[554]), scm__rc.d1786[674]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[555]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[556]), scm__rc.d1786[3]);
  scm__rc.d1786[675] = Scm_MakeExtendedPair(scm__rc.d1786[674], SCM_OBJ(&scm__rc.d1787[542]), SCM_OBJ(&scm__rc.d1787[558]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[676]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[676]))[4] = scm__rc.d1786[242];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[676]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[676]))[6] = scm__rc.d1786[242];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-wait-exit-status")), SCM_OBJ(&libsyssys_wait_exit_status__STUB), 0);
  libsyssys_wait_exit_status__STUB.common.info = scm__rc.d1786[675];
  libsyssys_wait_exit_status__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[676]);
  scm__rc.d1786[683] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[350])),TRUE); /* sys-wait-signaled? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[561]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[562]), scm__rc.d1786[683]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[563]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[564]), scm__rc.d1786[3]);
  scm__rc.d1786[684] = Scm_MakeExtendedPair(scm__rc.d1786[683], SCM_OBJ(&scm__rc.d1787[542]), SCM_OBJ(&scm__rc.d1787[566]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-wait-signaled?")), SCM_OBJ(&libsyssys_wait_signaledP__STUB), 0);
  libsyssys_wait_signaledP__STUB.common.info = scm__rc.d1786[684];
  libsyssys_wait_signaledP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[667]);
  scm__rc.d1786[685] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[351])),TRUE); /* sys-wait-termsig */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[569]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[570]), scm__rc.d1786[685]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[571]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[572]), scm__rc.d1786[3]);
  scm__rc.d1786[686] = Scm_MakeExtendedPair(scm__rc.d1786[685], SCM_OBJ(&scm__rc.d1787[542]), SCM_OBJ(&scm__rc.d1787[574]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-wait-termsig")), SCM_OBJ(&libsyssys_wait_termsig__STUB), 0);
  libsyssys_wait_termsig__STUB.common.info = scm__rc.d1786[686];
  libsyssys_wait_termsig__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[676]);
  scm__rc.d1786[687] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[352])),TRUE); /* sys-wait-stopped? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[577]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[578]), scm__rc.d1786[687]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[579]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[580]), scm__rc.d1786[3]);
  scm__rc.d1786[688] = Scm_MakeExtendedPair(scm__rc.d1786[687], SCM_OBJ(&scm__rc.d1787[542]), SCM_OBJ(&scm__rc.d1787[582]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-wait-stopped?")), SCM_OBJ(&libsyssys_wait_stoppedP__STUB), 0);
  libsyssys_wait_stoppedP__STUB.common.info = scm__rc.d1786[688];
  libsyssys_wait_stoppedP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[667]);
  scm__rc.d1786[689] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[353])),TRUE); /* sys-wait-stopsig */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[585]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[586]), scm__rc.d1786[689]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[587]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[588]), scm__rc.d1786[3]);
  scm__rc.d1786[690] = Scm_MakeExtendedPair(scm__rc.d1786[689], SCM_OBJ(&scm__rc.d1787[542]), SCM_OBJ(&scm__rc.d1787[590]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-wait-stopsig")), SCM_OBJ(&libsyssys_wait_stopsig__STUB), 0);
  libsyssys_wait_stopsig__STUB.common.info = scm__rc.d1786[690];
  libsyssys_wait_stopsig__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[676]);
  scm__rc.d1786[691] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[354])),TRUE); /* sys-time */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[591]), scm__rc.d1786[691]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[592]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[593]), scm__rc.d1786[3]);
  scm__rc.d1786[692] = Scm_MakeExtendedPair(scm__rc.d1786[691], SCM_NIL, SCM_OBJ(&scm__rc.d1787[594]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-time")), SCM_OBJ(&libsyssys_time__STUB), 0);
  libsyssys_time__STUB.common.info = scm__rc.d1786[692];
  libsyssys_time__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[18]);
  scm__rc.d1786[693] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[355])),TRUE); /* sys-gettimeofday */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[595]), scm__rc.d1786[693]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[596]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[597]), scm__rc.d1786[3]);
  scm__rc.d1786[694] = Scm_MakeExtendedPair(scm__rc.d1786[693], SCM_NIL, SCM_OBJ(&scm__rc.d1787[598]));
  scm__rc.d1786[695] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[356])),TRUE); /* <ulong> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[696]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[696]))[4] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[696]))[5] = scm__rc.d1786[695];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[696]))[6] = scm__rc.d1786[695];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-gettimeofday")), SCM_OBJ(&libsyssys_gettimeofday__STUB), 0);
  libsyssys_gettimeofday__STUB.common.info = scm__rc.d1786[694];
  libsyssys_gettimeofday__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[696]);
  scm__rc.d1786[703] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[357])),TRUE); /* current-microseconds */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[599]), scm__rc.d1786[703]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[600]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[601]), scm__rc.d1786[3]);
  scm__rc.d1786[704] = Scm_MakeExtendedPair(scm__rc.d1786[703], SCM_NIL, SCM_OBJ(&scm__rc.d1787[602]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("current-microseconds")), SCM_OBJ(&libsyscurrent_microseconds__STUB), 0);
  libsyscurrent_microseconds__STUB.common.info = scm__rc.d1786[704];
  libsyscurrent_microseconds__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[498]);
  scm__rc.d1786[705] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[358])),TRUE); /* sys-clock-gettime-monotonic */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[603]), scm__rc.d1786[705]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[604]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[605]), scm__rc.d1786[3]);
  scm__rc.d1786[706] = Scm_MakeExtendedPair(scm__rc.d1786[705], SCM_NIL, SCM_OBJ(&scm__rc.d1787[606]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[707]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[707]))[4] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[707]))[5] = scm__rc.d1786[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[707]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-clock-gettime-monotonic")), SCM_OBJ(&libsyssys_clock_gettime_monotonic__STUB), 0);
  libsyssys_clock_gettime_monotonic__STUB.common.info = scm__rc.d1786[706];
  libsyssys_clock_gettime_monotonic__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[707]);
  scm__rc.d1786[714] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[359])),TRUE); /* sys-clock-getres-monotonic */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[607]), scm__rc.d1786[714]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[608]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[609]), scm__rc.d1786[3]);
  scm__rc.d1786[715] = Scm_MakeExtendedPair(scm__rc.d1786[714], SCM_NIL, SCM_OBJ(&scm__rc.d1787[610]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-clock-getres-monotonic")), SCM_OBJ(&libsyssys_clock_getres_monotonic__STUB), 0);
  libsyssys_clock_getres_monotonic__STUB.common.info = scm__rc.d1786[715];
  libsyssys_clock_getres_monotonic__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[707]);
  scm__rc.d1786[716] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[360])),TRUE); /* current-time */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[611]), scm__rc.d1786[716]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[612]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[613]), scm__rc.d1786[3]);
  scm__rc.d1786[717] = Scm_MakeExtendedPair(scm__rc.d1786[716], SCM_NIL, SCM_OBJ(&scm__rc.d1787[614]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("current-time")), SCM_OBJ(&libsyscurrent_time__STUB), 0);
  libsyscurrent_time__STUB.common.info = scm__rc.d1786[717];
  libsyscurrent_time__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[18]);
  scm__rc.d1786[718] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[361])),TRUE); /* time? */
  scm__rc.d1786[719] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[362])),TRUE); /* obj */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[615]), scm__rc.d1786[719]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[618]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[619]), scm__rc.d1786[718]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[620]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[621]), scm__rc.d1786[3]);
  scm__rc.d1786[720] = Scm_MakeExtendedPair(scm__rc.d1786[718], SCM_OBJ(&scm__rc.d1787[615]), SCM_OBJ(&scm__rc.d1787[623]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[721]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[721]))[4] = scm__rc.d1786[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[721]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[721]))[6] = scm__rc.d1786[620];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("time?")), SCM_OBJ(&libsystimeP__STUB), 0);
  libsystimeP__STUB.common.info = scm__rc.d1786[720];
  libsystimeP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[721]);
  scm__rc.d1786[728] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[364])),TRUE); /* time-utc */
  scm__rc.d1786[729] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[363])),TRUE); /* absolute-time */
  scm__rc.d1786[730] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[365])),TRUE); /* t0 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[624]), scm__rc.d1786[730]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[625]), scm__rc.d1786[358]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[626]), scm__rc.d1786[719]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[629]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[630]), scm__rc.d1786[729]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[631]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[632]), scm__rc.d1786[3]);
  scm__rc.d1786[731] = Scm_MakeExtendedPair(scm__rc.d1786[729], SCM_OBJ(&scm__rc.d1787[626]), SCM_OBJ(&scm__rc.d1787[634]));
  scm__rc.d1786[732] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[366])),TRUE); /* <time>? */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[733]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[733]))[4] = scm__rc.d1786[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[733]))[5] = scm__rc.d1786[319];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[733]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[733]))[7] = scm__rc.d1786[732];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("absolute-time")), SCM_OBJ(&libsysabsolute_time__STUB), 0);
  libsysabsolute_time__STUB.common.info = scm__rc.d1786[731];
  libsysabsolute_time__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[733]);
  scm__rc.d1786[741] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[367])),TRUE); /* seconds+ */
  scm__rc.d1786[743] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[368])),TRUE); /* <time> */
  scm__rc.d1786[742] = Scm_MakeIdentifier(scm__rc.d1786[743], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#<time> */
  scm__rc.d1786[745] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[369])),TRUE); /* of-type? */
  scm__rc.d1786[744] = Scm_MakeIdentifier(scm__rc.d1786[745], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#of-type? */
  scm__rc.d1786[746] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[370])),TRUE); /* t */
  scm__rc.d1786[748] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[371])),TRUE); /* type-error */
  scm__rc.d1786[747] = Scm_MakeIdentifier(scm__rc.d1786[748], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#type-error */
  scm__rc.d1786[750] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[372])),TRUE); /* <real> */
  scm__rc.d1786[749] = Scm_MakeIdentifier(scm__rc.d1786[750], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#<real> */
  scm__rc.d1786[751] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[373])),TRUE); /* dt */
  scm__rc.d1786[752] = Scm_MakeIdentifier(scm__rc.d1786[729], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#absolute-time */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[635]), scm__rc.d1786[751]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[636]), scm__rc.d1786[746]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[639]), scm__rc.d1786[2]);
  scm__rc.d1786[753] = Scm_MakeExtendedPair(scm__rc.d1786[741], SCM_OBJ(&scm__rc.d1787[636]), SCM_OBJ(&scm__rc.d1787[640]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[641]), scm__rc.d1786[753]);
  scm__rc.d1786[754] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[9])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[9]))->name = scm__rc.d1786[741];/* seconds+ */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[9]))->debugInfo = scm__rc.d1786[754];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]))[4] = SCM_WORD(scm__rc.d1786[742]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]))[6] = SCM_WORD(scm__rc.d1786[744]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]))[15] = SCM_WORD(scm__rc.d1786[746]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]))[17] = SCM_WORD(scm__rc.d1786[742]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]))[20] = SCM_WORD(scm__rc.d1786[747]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]))[25] = SCM_WORD(scm__rc.d1786[749]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]))[27] = SCM_WORD(scm__rc.d1786[744]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]))[36] = SCM_WORD(scm__rc.d1786[751]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]))[38] = SCM_WORD(scm__rc.d1786[749]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]))[41] = SCM_WORD(scm__rc.d1786[747]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[688]))[45] = SCM_WORD(scm__rc.d1786[752]);
  scm__rc.d1786[755] = Scm_MakeIdentifier(scm__rc.d1786[741], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#seconds+ */
  scm__rc.d1786[756] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[10])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[10]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[10]))->debugInfo = scm__rc.d1786[756];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[735]))[3] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[735]))[6] = SCM_WORD(scm__rc.d1786[741]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[735]))[13] = SCM_WORD(scm__rc.d1786[755]);
  scm__rc.d1786[757] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[374])),TRUE); /* time->seconds */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[642]), scm__rc.d1786[746]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[645]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[646]), scm__rc.d1786[757]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[647]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[648]), scm__rc.d1786[3]);
  scm__rc.d1786[758] = Scm_MakeExtendedPair(scm__rc.d1786[757], SCM_OBJ(&scm__rc.d1787[642]), SCM_OBJ(&scm__rc.d1787[650]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[759]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[759]))[4] = scm__rc.d1786[743];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[759]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[759]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("time->seconds")), SCM_OBJ(&libsystime_TOseconds__STUB), 0);
  libsystime_TOseconds__STUB.common.info = scm__rc.d1786[758];
  libsystime_TOseconds__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[759]);
  scm__rc.d1786[766] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[375])),TRUE); /* seconds->time */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[653]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[654]), scm__rc.d1786[766]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[655]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[656]), scm__rc.d1786[3]);
  scm__rc.d1786[767] = Scm_MakeExtendedPair(scm__rc.d1786[766], SCM_OBJ(&scm__rc.d1787[642]), SCM_OBJ(&scm__rc.d1787[658]));
  scm__rc.d1786[768] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[376])),TRUE); /* <double> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[769]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[769]))[4] = scm__rc.d1786[768];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[769]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[769]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("seconds->time")), SCM_OBJ(&libsysseconds_TOtime__STUB), 0);
  libsysseconds_TOtime__STUB.common.info = scm__rc.d1786[767];
  libsysseconds_TOtime__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[769]);
  scm__rc.d1786[776] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[377])),TRUE); /* time-comparator */
  scm__rc.d1786[777] = Scm_MakeIdentifier(scm__rc.d1786[718], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#time? */
  scm__rc.d1786[779] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[378])),TRUE); /* compare */
  scm__rc.d1786[778] = Scm_MakeIdentifier(scm__rc.d1786[779], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#compare */
  scm__rc.d1786[780] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[379])),TRUE); /* a */
  scm__rc.d1786[781] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[380])),TRUE); /* b */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[659]), scm__rc.d1786[781]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[660]), scm__rc.d1786[780]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[663]), scm__rc.d1786[2]);
  scm__rc.d1786[782] = Scm_MakeExtendedPair(SCM_FALSE, SCM_OBJ(&scm__rc.d1787[660]), SCM_OBJ(&scm__rc.d1787[664]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[665]), scm__rc.d1786[782]);
  scm__rc.d1786[783] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[11])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[11]))->debugInfo = scm__rc.d1786[783];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[750]))[5] = SCM_WORD(scm__rc.d1786[778]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[668]), scm__rc.d1786[2]);
  scm__rc.d1786[784] = Scm_MakeExtendedPair(SCM_FALSE, SCM_OBJ(&scm__rc.d1787[660]), SCM_OBJ(&scm__rc.d1787[669]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[670]), scm__rc.d1786[784]);
  scm__rc.d1786[785] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[12])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[12]))->debugInfo = scm__rc.d1786[785];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[760]))[5] = SCM_WORD(scm__rc.d1786[778]);
  scm__rc.d1786[787] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[381])),TRUE); /* default-hash */
  scm__rc.d1786[786] = Scm_MakeIdentifier(scm__rc.d1786[787], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#default-hash */
  scm__rc.d1786[789] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[382])),TRUE); /* make-comparator */
  scm__rc.d1786[788] = Scm_MakeIdentifier(scm__rc.d1786[789], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#make-comparator */
  scm__rc.d1786[790] = Scm_MakeIdentifier(scm__rc.d1786[776], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#time-comparator */
  scm__rc.d1786[791] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[13])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[13]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[13]))->debugInfo = scm__rc.d1786[791];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[770]))[3] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[770]))[6] = SCM_WORD(scm__rc.d1786[776]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[770]))[10] = SCM_WORD(scm__rc.d1786[777]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[770]))[17] = SCM_WORD(scm__rc.d1786[786]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[770]))[19] = SCM_WORD(scm__rc.d1786[788]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[770]))[22] = SCM_WORD(scm__rc.d1786[790]);
  Scm_InitStaticClassWithMeta(&Scm_sys_tm_Class, "<sys-tm>", SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), NULL, SCM_FALSE, Scm_sys_tm_Class__SLOTS, 0);
  scm__rc.d1786[792] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[383])),TRUE); /* sys-asctime */
  scm__rc.d1786[793] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[384])),TRUE); /* tm */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[671]), scm__rc.d1786[793]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[674]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[675]), scm__rc.d1786[792]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[676]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[677]), scm__rc.d1786[3]);
  scm__rc.d1786[794] = Scm_MakeExtendedPair(scm__rc.d1786[792], SCM_OBJ(&scm__rc.d1787[671]), SCM_OBJ(&scm__rc.d1787[679]));
  scm__rc.d1786[795] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[385])),TRUE); /* <sys-tm> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[796]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[796]))[4] = scm__rc.d1786[795];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[796]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[796]))[6] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-asctime")), SCM_OBJ(&libsyssys_asctime__STUB), 0);
  libsyssys_asctime__STUB.common.info = scm__rc.d1786[794];
  libsyssys_asctime__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[796]);
  scm__rc.d1786[803] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[386])),TRUE); /* sys-ctime */
  scm__rc.d1786[804] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[387])),TRUE); /* time */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[680]), scm__rc.d1786[804]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[683]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[684]), scm__rc.d1786[803]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[685]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[686]), scm__rc.d1786[3]);
  scm__rc.d1786[805] = Scm_MakeExtendedPair(scm__rc.d1786[803], SCM_OBJ(&scm__rc.d1787[680]), SCM_OBJ(&scm__rc.d1787[688]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-ctime")), SCM_OBJ(&libsyssys_ctime__STUB), 0);
  libsyssys_ctime__STUB.common.info = scm__rc.d1786[805];
  libsyssys_ctime__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[611]);
  scm__rc.d1786[806] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[388])),TRUE); /* sys-difftime */
  scm__rc.d1786[807] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[389])),TRUE); /* time1 */
  scm__rc.d1786[808] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[390])),TRUE); /* time0 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[689]), scm__rc.d1786[808]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[690]), scm__rc.d1786[807]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[693]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[694]), scm__rc.d1786[806]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[695]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[696]), scm__rc.d1786[3]);
  scm__rc.d1786[809] = Scm_MakeExtendedPair(scm__rc.d1786[806], SCM_OBJ(&scm__rc.d1787[690]), SCM_OBJ(&scm__rc.d1787[698]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[810]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[810]))[4] = scm__rc.d1786[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[810]))[5] = scm__rc.d1786[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[810]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[810]))[7] = scm__rc.d1786[768];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-difftime")), SCM_OBJ(&libsyssys_difftime__STUB), 0);
  libsyssys_difftime__STUB.common.info = scm__rc.d1786[809];
  libsyssys_difftime__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[810]);
  scm__rc.d1786[818] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[391])),TRUE); /* sys-strftime */
  scm__rc.d1786[819] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[392])),TRUE); /* format */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[699]), scm__rc.d1786[819]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[702]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[703]), scm__rc.d1786[818]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[704]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[705]), scm__rc.d1786[3]);
  scm__rc.d1786[820] = Scm_MakeExtendedPair(scm__rc.d1786[818], SCM_OBJ(&scm__rc.d1787[699]), SCM_OBJ(&scm__rc.d1787[707]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[821]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[821]))[4] = scm__rc.d1786[256];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[821]))[5] = scm__rc.d1786[795];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[821]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[821]))[7] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-strftime")), SCM_OBJ(&libsyssys_strftime__STUB), 0);
  libsyssys_strftime__STUB.common.info = scm__rc.d1786[820];
  libsyssys_strftime__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[821]);
  scm__rc.d1786[829] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[393])),TRUE); /* sys-gmtime */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[710]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[711]), scm__rc.d1786[829]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[712]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[713]), scm__rc.d1786[3]);
  scm__rc.d1786[830] = Scm_MakeExtendedPair(scm__rc.d1786[829], SCM_OBJ(&scm__rc.d1787[680]), SCM_OBJ(&scm__rc.d1787[715]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-gmtime")), SCM_OBJ(&libsyssys_gmtime__STUB), 0);
  libsyssys_gmtime__STUB.common.info = scm__rc.d1786[830];
  libsyssys_gmtime__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[611]);
  scm__rc.d1786[831] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[394])),TRUE); /* sys-localtime */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[718]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[719]), scm__rc.d1786[831]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[720]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[721]), scm__rc.d1786[3]);
  scm__rc.d1786[832] = Scm_MakeExtendedPair(scm__rc.d1786[831], SCM_OBJ(&scm__rc.d1787[680]), SCM_OBJ(&scm__rc.d1787[723]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-localtime")), SCM_OBJ(&libsyssys_localtime__STUB), 0);
  libsyssys_localtime__STUB.common.info = scm__rc.d1786[832];
  libsyssys_localtime__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[611]);
  scm__rc.d1786[833] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[395])),TRUE); /* sys-mktime */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[726]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[727]), scm__rc.d1786[833]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[728]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[729]), scm__rc.d1786[3]);
  scm__rc.d1786[834] = Scm_MakeExtendedPair(scm__rc.d1786[833], SCM_OBJ(&scm__rc.d1787[671]), SCM_OBJ(&scm__rc.d1787[731]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-mktime")), SCM_OBJ(&libsyssys_mktime__STUB), 0);
  libsyssys_mktime__STUB.common.info = scm__rc.d1786[834];
  libsyssys_mktime__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[796]);
  scm__rc.d1786[835] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[396])),TRUE); /* R_OK */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[835]), Scm_MakeInteger(R_OK), SCM_BINDING_CONST);

  scm__rc.d1786[836] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[397])),TRUE); /* W_OK */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[836]), Scm_MakeInteger(W_OK), SCM_BINDING_CONST);

  scm__rc.d1786[837] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[398])),TRUE); /* X_OK */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[837]), Scm_MakeInteger(X_OK), SCM_BINDING_CONST);

  scm__rc.d1786[838] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[399])),TRUE); /* F_OK */
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(scm__rc.d1786[838]), Scm_MakeInteger(F_OK), SCM_BINDING_CONST);

  scm__rc.d1786[839] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[400])),TRUE); /* sys-access */
  scm__rc.d1786[840] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[401])),TRUE); /* amode */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[732]), scm__rc.d1786[840]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[733]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[736]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[737]), scm__rc.d1786[839]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[738]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[739]), scm__rc.d1786[3]);
  scm__rc.d1786[841] = Scm_MakeExtendedPair(scm__rc.d1786[839], SCM_OBJ(&scm__rc.d1787[733]), SCM_OBJ(&scm__rc.d1787[741]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[842]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[842]))[4] = scm__rc.d1786[256];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[842]))[5] = scm__rc.d1786[242];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[842]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[842]))[7] = scm__rc.d1786[620];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-access")), SCM_OBJ(&libsyssys_access__STUB), 0);
  libsyssys_access__STUB.common.info = scm__rc.d1786[841];
  libsyssys_access__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[842]);
  scm__rc.d1786[850] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[402])),TRUE); /* sys-chdir */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[744]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[745]), scm__rc.d1786[850]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[746]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[747]), scm__rc.d1786[3]);
  scm__rc.d1786[851] = Scm_MakeExtendedPair(scm__rc.d1786[850], SCM_OBJ(&scm__rc.d1787[1]), SCM_OBJ(&scm__rc.d1787[749]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-chdir")), SCM_OBJ(&libsyssys_chdir__STUB), 0);
  libsyssys_chdir__STUB.common.info = scm__rc.d1786[851];
  libsyssys_chdir__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[431]);
  scm__rc.d1786[852] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[403])),TRUE); /* sys-chmod */
  scm__rc.d1786[853] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[404])),TRUE); /* mode */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[750]), scm__rc.d1786[853]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[751]), scm__rc.d1786[1]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[754]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[755]), scm__rc.d1786[852]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[756]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[757]), scm__rc.d1786[3]);
  scm__rc.d1786[854] = Scm_MakeExtendedPair(scm__rc.d1786[852], SCM_OBJ(&scm__rc.d1787[751]), SCM_OBJ(&scm__rc.d1787[759]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[855]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[855]))[4] = scm__rc.d1786[256];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[855]))[5] = scm__rc.d1786[242];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[855]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[855]))[7] = scm__rc.d1786[347];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-chmod")), SCM_OBJ(&libsyssys_chmod__STUB), 0);
  libsyssys_chmod__STUB.common.info = scm__rc.d1786[854];
  libsyssys_chmod__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[855]);
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[28] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[12])),TRUE); /* sys-fchmod */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[29] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[13])),TRUE); /* port-or-fd */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[20]), scm__rc.d1856[29]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[23]), scm__rc.d1856[2]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[24]), scm__rc.d1856[28]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[25]), scm__rc.d1856[4]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[26]), scm__rc.d1856[3]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[30] = Scm_MakeExtendedPair(scm__rc.d1856[28], SCM_OBJ(&scm__rc.d1857[20]), SCM_OBJ(&scm__rc.d1857[28]));
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[31] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[14])),TRUE); /* <top> */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[32] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[15])),TRUE); /* <void> */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[33]))[3] = scm__rc.d1856[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[33]))[4] = scm__rc.d1856[31];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[33]))[5] = scm__rc.d1856[19];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[33]))[6] = scm__rc.d1856[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[33]))[7] = scm__rc.d1856[32];
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-fchmod")), SCM_OBJ(&libsyssys_fchmod__STUB), 0);
  libsyssys_fchmod__STUB.common.info = scm__rc.d1856[30];
  libsyssys_fchmod__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1856[33]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
  scm__rc.d1786[863] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[405])),TRUE); /* sys-chown */
  scm__rc.d1786[864] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[406])),TRUE); /* owner */
  scm__rc.d1786[865] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[407])),TRUE); /* group */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[760]), scm__rc.d1786[865]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[761]), scm__rc.d1786[864]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[762]), scm__rc.d1786[598]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[765]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[766]), scm__rc.d1786[863]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[767]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[768]), scm__rc.d1786[3]);
  scm__rc.d1786[866] = Scm_MakeExtendedPair(scm__rc.d1786[863], SCM_OBJ(&scm__rc.d1787[762]), SCM_OBJ(&scm__rc.d1787[770]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[867]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[867]))[4] = scm__rc.d1786[256];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[867]))[5] = scm__rc.d1786[242];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[867]))[6] = scm__rc.d1786[242];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[867]))[7] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[867]))[8] = scm__rc.d1786[242];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-chown")), SCM_OBJ(&libsyssys_chown__STUB), 0);
  libsyssys_chown__STUB.common.info = scm__rc.d1786[866];
  libsyssys_chown__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[867]);
#if defined(HAVE_LCHOWN)
  scm__rc.d1865[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1864[0])),TRUE); /* sys-lchown */
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  scm__rc.d1865[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1864[1])),TRUE); /* path */
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  scm__rc.d1865[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1864[2])),TRUE); /* owner */
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  scm__rc.d1865[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1864[3])),TRUE); /* group */
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1866[1]), scm__rc.d1865[3]);
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1866[2]), scm__rc.d1865[2]);
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1866[3]), scm__rc.d1865[1]);
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  scm__rc.d1865[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1864[4])),TRUE); /* source-info */
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1866[6]), scm__rc.d1865[4]);
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  scm__rc.d1865[5] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1864[6])),TRUE); /* bind-info */
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  scm__rc.d1865[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1864[7])),TRUE); /* gauche */
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1866[7]), scm__rc.d1865[0]);
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1866[8]), scm__rc.d1865[6]);
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1866[9]), scm__rc.d1865[5]);
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  scm__rc.d1865[7] = Scm_MakeExtendedPair(scm__rc.d1865[0], SCM_OBJ(&scm__rc.d1866[3]), SCM_OBJ(&scm__rc.d1866[11]));
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  scm__rc.d1865[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1864[8])),TRUE); /* <const-cstring> */
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  scm__rc.d1865[9] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1864[9])),TRUE); /* <int> */
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  scm__rc.d1865[10] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1864[10])),TRUE); /* -> */
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1865[11]))[3] = scm__rc.d1865[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1865[11]))[4] = scm__rc.d1865[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1865[11]))[5] = scm__rc.d1865[9];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1865[11]))[6] = scm__rc.d1865[9];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1865[11]))[7] = scm__rc.d1865[10];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1865[11]))[8] = scm__rc.d1865[9];
#endif /* defined(HAVE_LCHOWN) */
#if defined(HAVE_LCHOWN)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-lchown")), SCM_OBJ(&libsyssys_lchown__STUB), 0);
  libsyssys_lchown__STUB.common.info = scm__rc.d1865[7];
  libsyssys_lchown__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1865[11]);
#endif /* defined(HAVE_LCHOWN) */
  scm__rc.d1786[876] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[408])),TRUE); /* sys-fork */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[771]), scm__rc.d1786[876]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[772]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[773]), scm__rc.d1786[3]);
  scm__rc.d1786[877] = Scm_MakeExtendedPair(scm__rc.d1786[876], SCM_NIL, SCM_OBJ(&scm__rc.d1787[774]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-fork")), SCM_OBJ(&libsyssys_fork__STUB), 0);
  libsyssys_fork__STUB.common.info = scm__rc.d1786[877];
  libsyssys_fork__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[388]);
  scm__rc.d1786[878] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[409]))); /* :iomap */
  scm__rc.d1786[879] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[410]))); /* :sigmask */
  scm__rc.d1786[880] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[411]))); /* :directory */
  scm__rc.d1786[881] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[412]))); /* :detached */
  scm__rc.d1786[882] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[413])),TRUE); /* sys-exec */
  scm__rc.d1786[883] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[414])),TRUE); /* args */
  scm__rc.d1786[884] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[409])),TRUE); /* iomap */
  scm__rc.d1786[885] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[410])),TRUE); /* sigmask */
  scm__rc.d1786[886] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[411])),TRUE); /* directory */
  scm__rc.d1786[887] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[412])),TRUE); /* detached */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[775]), scm__rc.d1786[887]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[776]), scm__rc.d1786[886]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[777]), scm__rc.d1786[885]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[778]), scm__rc.d1786[884]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[779]), scm__rc.d1786[79]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[780]), scm__rc.d1786[883]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[781]), scm__rc.d1786[486]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[784]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[785]), scm__rc.d1786[882]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[786]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[787]), scm__rc.d1786[3]);
  scm__rc.d1786[888] = Scm_MakeExtendedPair(scm__rc.d1786[882], SCM_OBJ(&scm__rc.d1787[781]), SCM_OBJ(&scm__rc.d1787[789]));
  scm__rc.d1786[889] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[415])),TRUE); /* <list> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[890]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[890]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[890]))[5] = scm__rc.d1786[889];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[890]))[6] = scm__rc.d1786[319];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[890]))[7] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[890]))[8] = scm__rc.d1786[347];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-exec")), SCM_OBJ(&libsyssys_exec__STUB), 0);
  libsyssys_exec__STUB.common.info = scm__rc.d1786[888];
  libsyssys_exec__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[890]);
  scm__rc.d1786[899] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[416])),TRUE); /* sys-fork-and-exec */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[792]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[793]), scm__rc.d1786[899]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[794]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[795]), scm__rc.d1786[3]);
  scm__rc.d1786[900] = Scm_MakeExtendedPair(scm__rc.d1786[899], SCM_OBJ(&scm__rc.d1787[781]), SCM_OBJ(&scm__rc.d1787[797]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[901]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[901]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[901]))[5] = scm__rc.d1786[889];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[901]))[6] = scm__rc.d1786[319];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[901]))[7] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[901]))[8] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-fork-and-exec")), SCM_OBJ(&libsyssys_fork_and_exec__STUB), 0);
  libsyssys_fork_and_exec__STUB.common.info = scm__rc.d1786[900];
  libsyssys_fork_and_exec__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[901]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[798]), scm__rc.d1786[62]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[799]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[800]), scm__rc.d1786[3]);
  scm__rc.d1786[910] = Scm_MakeExtendedPair(scm__rc.d1786[62], SCM_NIL, SCM_OBJ(&scm__rc.d1787[801]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getcwd")), SCM_OBJ(&libsyssys_getcwd__STUB), 0);
  libsyssys_getcwd__STUB.common.info = scm__rc.d1786[910];
  libsyssys_getcwd__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[18]);
  scm__rc.d1786[911] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[417])),TRUE); /* sys-getegid */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[802]), scm__rc.d1786[911]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[803]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[804]), scm__rc.d1786[3]);
  scm__rc.d1786[912] = Scm_MakeExtendedPair(scm__rc.d1786[911], SCM_NIL, SCM_OBJ(&scm__rc.d1787[805]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getegid")), SCM_OBJ(&libsyssys_getegid__STUB), 0);
  libsyssys_getegid__STUB.common.info = scm__rc.d1786[912];
  libsyssys_getegid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[388]);
  scm__rc.d1786[913] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[418])),TRUE); /* sys-getgid */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[806]), scm__rc.d1786[913]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[807]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[808]), scm__rc.d1786[3]);
  scm__rc.d1786[914] = Scm_MakeExtendedPair(scm__rc.d1786[913], SCM_NIL, SCM_OBJ(&scm__rc.d1787[809]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getgid")), SCM_OBJ(&libsyssys_getgid__STUB), 0);
  libsyssys_getgid__STUB.common.info = scm__rc.d1786[914];
  libsyssys_getgid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[388]);
  scm__rc.d1786[915] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[419])),TRUE); /* sys-geteuid */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[810]), scm__rc.d1786[915]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[811]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[812]), scm__rc.d1786[3]);
  scm__rc.d1786[916] = Scm_MakeExtendedPair(scm__rc.d1786[915], SCM_NIL, SCM_OBJ(&scm__rc.d1787[813]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-geteuid")), SCM_OBJ(&libsyssys_geteuid__STUB), 0);
  libsyssys_geteuid__STUB.common.info = scm__rc.d1786[916];
  libsyssys_geteuid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[388]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[814]), scm__rc.d1786[46]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[815]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[816]), scm__rc.d1786[3]);
  scm__rc.d1786[917] = Scm_MakeExtendedPair(scm__rc.d1786[46], SCM_NIL, SCM_OBJ(&scm__rc.d1787[817]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getuid")), SCM_OBJ(&libsyssys_getuid__STUB), 0);
  libsyssys_getuid__STUB.common.info = scm__rc.d1786[917];
  libsyssys_getuid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[388]);
  scm__rc.d1786[918] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[420])),TRUE); /* sys-setugid? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[818]), scm__rc.d1786[918]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[819]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[820]), scm__rc.d1786[3]);
  scm__rc.d1786[919] = Scm_MakeExtendedPair(scm__rc.d1786[918], SCM_NIL, SCM_OBJ(&scm__rc.d1787[821]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[920]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[920]))[4] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[920]))[5] = scm__rc.d1786[620];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-setugid?")), SCM_OBJ(&libsyssys_setugidP__STUB), 0);
  libsyssys_setugidP__STUB.common.info = scm__rc.d1786[919];
  libsyssys_setugidP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[920]);
  scm__rc.d1786[926] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[421])),TRUE); /* sys-getpid */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[822]), scm__rc.d1786[926]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[823]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[824]), scm__rc.d1786[3]);
  scm__rc.d1786[927] = Scm_MakeExtendedPair(scm__rc.d1786[926], SCM_NIL, SCM_OBJ(&scm__rc.d1787[825]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getpid")), SCM_OBJ(&libsyssys_getpid__STUB), 0);
  libsyssys_getpid__STUB.common.info = scm__rc.d1786[927];
  libsyssys_getpid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[388]);
  scm__rc.d1786[928] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[422])),TRUE); /* sys-getppid */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[826]), scm__rc.d1786[928]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[827]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[828]), scm__rc.d1786[3]);
  scm__rc.d1786[929] = Scm_MakeExtendedPair(scm__rc.d1786[928], SCM_NIL, SCM_OBJ(&scm__rc.d1787[829]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getppid")), SCM_OBJ(&libsyssys_getppid__STUB), 0);
  libsyssys_getppid__STUB.common.info = scm__rc.d1786[929];
  libsyssys_getppid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[388]);
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[41] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[16])),TRUE); /* sys-setgid */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[42] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[17])),TRUE); /* gid */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[29]), scm__rc.d1856[42]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[32]), scm__rc.d1856[2]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[33]), scm__rc.d1856[41]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[34]), scm__rc.d1856[4]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[35]), scm__rc.d1856[3]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[43] = Scm_MakeExtendedPair(scm__rc.d1856[41], SCM_OBJ(&scm__rc.d1857[29]), SCM_OBJ(&scm__rc.d1857[37]));
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[44]))[3] = scm__rc.d1856[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[44]))[4] = scm__rc.d1856[19];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[44]))[5] = scm__rc.d1856[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[44]))[6] = scm__rc.d1856[19];
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-setgid")), SCM_OBJ(&libsyssys_setgid__STUB), 0);
  libsyssys_setgid__STUB.common.info = scm__rc.d1856[43];
  libsyssys_setgid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1856[44]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[51] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[18])),TRUE); /* sys-setpgid */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[52] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[19])),TRUE); /* pid */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[53] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[20])),TRUE); /* pgid */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[38]), scm__rc.d1856[53]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[39]), scm__rc.d1856[52]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[42]), scm__rc.d1856[2]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[43]), scm__rc.d1856[51]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[44]), scm__rc.d1856[4]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[45]), scm__rc.d1856[3]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[54] = Scm_MakeExtendedPair(scm__rc.d1856[51], SCM_OBJ(&scm__rc.d1857[39]), SCM_OBJ(&scm__rc.d1857[47]));
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[55]))[3] = scm__rc.d1856[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[55]))[4] = scm__rc.d1856[19];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[55]))[5] = scm__rc.d1856[19];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[55]))[6] = scm__rc.d1856[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[55]))[7] = scm__rc.d1856[19];
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-setpgid")), SCM_OBJ(&libsyssys_setpgid__STUB), 0);
  libsyssys_setpgid__STUB.common.info = scm__rc.d1856[54];
  libsyssys_setpgid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1856[55]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  scm__rc.d1868[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1867[0])),TRUE); /* sys-getpgid */
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  scm__rc.d1868[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1867[1])),TRUE); /* pid */
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1869[1]), scm__rc.d1868[1]);
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  scm__rc.d1868[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1867[2])),TRUE); /* source-info */
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1869[4]), scm__rc.d1868[2]);
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  scm__rc.d1868[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1867[4])),TRUE); /* bind-info */
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  scm__rc.d1868[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1867[5])),TRUE); /* gauche */
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1869[5]), scm__rc.d1868[0]);
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1869[6]), scm__rc.d1868[4]);
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1869[7]), scm__rc.d1868[3]);
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  scm__rc.d1868[5] = Scm_MakeExtendedPair(scm__rc.d1868[0], SCM_OBJ(&scm__rc.d1869[1]), SCM_OBJ(&scm__rc.d1869[9]));
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  scm__rc.d1868[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1867[6])),TRUE); /* <int> */
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  scm__rc.d1868[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1867[7])),TRUE); /* -> */
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1868[8]))[3] = scm__rc.d1868[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1868[8]))[4] = scm__rc.d1868[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1868[8]))[5] = scm__rc.d1868[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1868[8]))[6] = scm__rc.d1868[6];
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_GETPGID)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getpgid")), SCM_OBJ(&libsyssys_getpgid__STUB), 0);
  libsyssys_getpgid__STUB.common.info = scm__rc.d1868[5];
  libsyssys_getpgid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1868[8]);
#endif /* defined(HAVE_GETPGID) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[63] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[21])),TRUE); /* sys-getpgrp */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[50]), scm__rc.d1856[2]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[51]), scm__rc.d1856[63]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[52]), scm__rc.d1856[4]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[53]), scm__rc.d1856[3]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[64] = Scm_MakeExtendedPair(scm__rc.d1856[63], SCM_NIL, SCM_OBJ(&scm__rc.d1857[55]));
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[65]))[3] = scm__rc.d1856[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[65]))[4] = scm__rc.d1856[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[65]))[5] = scm__rc.d1856[19];
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getpgrp")), SCM_OBJ(&libsyssys_getpgrp__STUB), 0);
  libsyssys_getpgrp__STUB.common.info = scm__rc.d1856[64];
  libsyssys_getpgrp__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1856[65]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[71] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[22])),TRUE); /* sys-setsid */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[58]), scm__rc.d1856[2]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[59]), scm__rc.d1856[71]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[60]), scm__rc.d1856[4]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[61]), scm__rc.d1856[3]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[72] = Scm_MakeExtendedPair(scm__rc.d1856[71], SCM_NIL, SCM_OBJ(&scm__rc.d1857[63]));
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-setsid")), SCM_OBJ(&libsyssys_setsid__STUB), 0);
  libsyssys_setsid__STUB.common.info = scm__rc.d1856[72];
  libsyssys_setsid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1856[65]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[73] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[23])),TRUE); /* sys-setuid */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[74] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[24])),TRUE); /* uid */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[64]), scm__rc.d1856[74]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[67]), scm__rc.d1856[2]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[68]), scm__rc.d1856[73]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[69]), scm__rc.d1856[4]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[70]), scm__rc.d1856[3]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[75] = Scm_MakeExtendedPair(scm__rc.d1856[73], SCM_OBJ(&scm__rc.d1857[64]), SCM_OBJ(&scm__rc.d1857[72]));
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-setuid")), SCM_OBJ(&libsyssys_setuid__STUB), 0);
  libsyssys_setuid__STUB.common.info = scm__rc.d1856[75];
  libsyssys_setuid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1856[44]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[76] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[25])),TRUE); /* sys-nice */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[77] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[26])),TRUE); /* inc */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[73]), scm__rc.d1856[77]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[76]), scm__rc.d1856[2]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[77]), scm__rc.d1856[76]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[78]), scm__rc.d1856[4]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[79]), scm__rc.d1856[3]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[78] = Scm_MakeExtendedPair(scm__rc.d1856[76], SCM_OBJ(&scm__rc.d1857[73]), SCM_OBJ(&scm__rc.d1857[81]));
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-nice")), SCM_OBJ(&libsyssys_nice__STUB), 0);
  libsyssys_nice__STUB.common.info = scm__rc.d1856[78];
  libsyssys_nice__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1856[44]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[79] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1855[27])),TRUE); /* sys-getgroups */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[84]), scm__rc.d1856[2]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[85]), scm__rc.d1856[79]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[86]), scm__rc.d1856[4]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1857[87]), scm__rc.d1856[3]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  scm__rc.d1856[80] = Scm_MakeExtendedPair(scm__rc.d1856[79], SCM_NIL, SCM_OBJ(&scm__rc.d1857[89]));
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[81]))[3] = scm__rc.d1856[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[81]))[4] = scm__rc.d1856[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1856[81]))[5] = scm__rc.d1856[31];
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getgroups")), SCM_OBJ(&libsyssys_getgroups__STUB), 0);
  libsyssys_getgroups__STUB.common.info = scm__rc.d1856[80];
  libsyssys_getgroups__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1856[81]);
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  scm__rc.d1873[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1871[0])),TRUE); /* sys-setgroups */
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  scm__rc.d1873[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1871[1])),TRUE); /* gids */
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1874[1]), scm__rc.d1873[1]);
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  scm__rc.d1873[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1871[2])),TRUE); /* source-info */
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1874[4]), scm__rc.d1873[2]);
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  scm__rc.d1873[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1871[4])),TRUE); /* bind-info */
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  scm__rc.d1873[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1871[5])),TRUE); /* gauche */
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1874[5]), scm__rc.d1873[0]);
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1874[6]), scm__rc.d1873[4]);
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1874[7]), scm__rc.d1873[3]);
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  scm__rc.d1873[5] = Scm_MakeExtendedPair(scm__rc.d1873[0], SCM_OBJ(&scm__rc.d1874[1]), SCM_OBJ(&scm__rc.d1874[9]));
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  scm__rc.d1873[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1871[6])),TRUE); /* <top> */
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  scm__rc.d1873[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1871[7])),TRUE); /* -> */
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  scm__rc.d1873[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1871[8])),TRUE); /* <void> */
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1873[9]))[3] = scm__rc.d1873[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1873[9]))[4] = scm__rc.d1873[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1873[9]))[5] = scm__rc.d1873[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1873[9]))[6] = scm__rc.d1873[8];
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
#if !(defined(GAUCHE_WINDOWS))
#if defined(HAVE_SETGROUPS)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-setgroups")), SCM_OBJ(&libsyssys_setgroups__STUB), 0);
  libsyssys_setgroups__STUB.common.info = scm__rc.d1873[5];
  libsyssys_setgroups__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1873[9]);
#endif /* defined(HAVE_SETGROUPS) */
#endif /* !(defined(GAUCHE_WINDOWS)) */
  scm__rc.d1786[930] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[423])),TRUE); /* sys-setpgrp */
  scm__rc.d1786[932] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[424])),TRUE); /* sys-setpgid */
  scm__rc.d1786[931] = Scm_MakeIdentifier(scm__rc.d1786[932], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#sys-setpgid */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[832]), scm__rc.d1786[2]);
  scm__rc.d1786[933] = Scm_MakeExtendedPair(scm__rc.d1786[930], SCM_NIL, SCM_OBJ(&scm__rc.d1787[833]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[834]), scm__rc.d1786[933]);
  scm__rc.d1786[934] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[14])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[14]))->name = scm__rc.d1786[930];/* sys-setpgrp */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[14]))->debugInfo = scm__rc.d1786[934];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[794]))[3] = SCM_WORD(scm__rc.d1786[931]);
  scm__rc.d1786[935] = Scm_MakeIdentifier(scm__rc.d1786[930], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#sys-setpgrp */
  scm__rc.d1786[936] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[15])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[15]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[15]))->debugInfo = scm__rc.d1786[936];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[799]))[3] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[799]))[6] = SCM_WORD(scm__rc.d1786[930]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[799]))[13] = SCM_WORD(scm__rc.d1786[935]);
  scm__rc.d1786[937] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[425])),TRUE); /* sys-getlogin */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[835]), scm__rc.d1786[937]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[836]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[837]), scm__rc.d1786[3]);
  scm__rc.d1786[938] = Scm_MakeExtendedPair(scm__rc.d1786[937], SCM_NIL, SCM_OBJ(&scm__rc.d1787[838]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[939]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[939]))[4] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[939]))[5] = scm__rc.d1786[274];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getlogin")), SCM_OBJ(&libsyssys_getlogin__STUB), 0);
  libsyssys_getlogin__STUB.common.info = scm__rc.d1786[938];
  libsyssys_getlogin__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[939]);
  scm__rc.d1786[945] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[426])),TRUE); /* sys-link */
  scm__rc.d1786[946] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[427])),TRUE); /* existing */
  scm__rc.d1786[947] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[428])),TRUE); /* newpath */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[839]), scm__rc.d1786[947]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[840]), scm__rc.d1786[946]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[843]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[844]), scm__rc.d1786[945]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[845]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[846]), scm__rc.d1786[3]);
  scm__rc.d1786[948] = Scm_MakeExtendedPair(scm__rc.d1786[945], SCM_OBJ(&scm__rc.d1787[840]), SCM_OBJ(&scm__rc.d1787[848]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-link")), SCM_OBJ(&libsyssys_link__STUB), 0);
  libsyssys_link__STUB.common.info = scm__rc.d1786[948];
  libsyssys_link__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[442]);
  scm__rc.d1786[949] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[429])),TRUE); /* sys-pause */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[849]), scm__rc.d1786[949]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[850]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[851]), scm__rc.d1786[3]);
  scm__rc.d1786[950] = Scm_MakeExtendedPair(scm__rc.d1786[949], SCM_NIL, SCM_OBJ(&scm__rc.d1787[852]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-pause")), SCM_OBJ(&libsyssys_pause__STUB), 0);
  libsyssys_pause__STUB.common.info = scm__rc.d1786[950];
  libsyssys_pause__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[18]);
  scm__rc.d1786[951] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[430])),TRUE); /* sys-alarm */
  scm__rc.d1786[952] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[431])),TRUE); /* seconds */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[853]), scm__rc.d1786[952]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[856]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[857]), scm__rc.d1786[951]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[858]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[859]), scm__rc.d1786[3]);
  scm__rc.d1786[953] = Scm_MakeExtendedPair(scm__rc.d1786[951], SCM_OBJ(&scm__rc.d1787[853]), SCM_OBJ(&scm__rc.d1787[861]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[954]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[954]))[4] = scm__rc.d1786[91];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[954]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[954]))[6] = scm__rc.d1786[242];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-alarm")), SCM_OBJ(&libsyssys_alarm__STUB), 0);
  libsyssys_alarm__STUB.common.info = scm__rc.d1786[953];
  libsyssys_alarm__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[954]);
  scm__rc.d1786[961] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[59]))); /* :name */
  scm__rc.d1786[962] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[433]))); /* :buffering */
  scm__rc.d1786[963] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[434]))); /* :buffered? */
  scm__rc.d1786[964] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[435])),TRUE); /* sys-pipe */
  scm__rc.d1786[965] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[433])),TRUE); /* buffering */
  scm__rc.d1786[966] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[434])),TRUE); /* buffered? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[862]), scm__rc.d1786[966]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[863]), scm__rc.d1786[965]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[864]), scm__rc.d1786[100]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[865]), scm__rc.d1786[79]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[868]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[869]), scm__rc.d1786[964]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[870]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[871]), scm__rc.d1786[3]);
  scm__rc.d1786[967] = Scm_MakeExtendedPair(scm__rc.d1786[964], SCM_OBJ(&scm__rc.d1787[865]), SCM_OBJ(&scm__rc.d1787[873]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[968]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[968]))[4] = scm__rc.d1786[319];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[968]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[968]))[6] = scm__rc.d1786[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[968]))[7] = scm__rc.d1786[8];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-pipe")), SCM_OBJ(&libsyssys_pipe__STUB), 0);
  libsyssys_pipe__STUB.common.info = scm__rc.d1786[967];
  libsyssys_pipe__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[968]);
  scm__rc.d1786[976] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[436])),TRUE); /* sys-close */
  scm__rc.d1786[977] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[437])),TRUE); /* fd */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[874]), scm__rc.d1786[977]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[877]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[878]), scm__rc.d1786[976]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[879]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[880]), scm__rc.d1786[3]);
  scm__rc.d1786[978] = Scm_MakeExtendedPair(scm__rc.d1786[976], SCM_OBJ(&scm__rc.d1787[874]), SCM_OBJ(&scm__rc.d1787[882]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[979]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[979]))[4] = scm__rc.d1786[242];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[979]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[979]))[6] = scm__rc.d1786[347];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-close")), SCM_OBJ(&libsyssys_close__STUB), 0);
  libsyssys_close__STUB.common.info = scm__rc.d1786[978];
  libsyssys_close__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[979]);
  scm__rc.d1786[986] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[438])),TRUE); /* sys-mkdir */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[885]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[886]), scm__rc.d1786[986]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[887]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[888]), scm__rc.d1786[3]);
  scm__rc.d1786[987] = Scm_MakeExtendedPair(scm__rc.d1786[986], SCM_OBJ(&scm__rc.d1787[751]), SCM_OBJ(&scm__rc.d1787[890]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-mkdir")), SCM_OBJ(&libsyssys_mkdir__STUB), 0);
  libsyssys_mkdir__STUB.common.info = scm__rc.d1786[987];
  libsyssys_mkdir__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[855]);
  scm__rc.d1786[988] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[439])),TRUE); /* sys-rmdir */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[893]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[894]), scm__rc.d1786[988]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[895]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[896]), scm__rc.d1786[3]);
  scm__rc.d1786[989] = Scm_MakeExtendedPair(scm__rc.d1786[988], SCM_OBJ(&scm__rc.d1787[1]), SCM_OBJ(&scm__rc.d1787[898]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-rmdir")), SCM_OBJ(&libsyssys_rmdir__STUB), 0);
  libsyssys_rmdir__STUB.common.info = scm__rc.d1786[989];
  libsyssys_rmdir__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[431]);
  scm__rc.d1786[990] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[440])),TRUE); /* sys-umask */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[899]), scm__rc.d1786[358]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[902]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[903]), scm__rc.d1786[990]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[904]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[905]), scm__rc.d1786[3]);
  scm__rc.d1786[991] = Scm_MakeExtendedPair(scm__rc.d1786[990], SCM_OBJ(&scm__rc.d1787[899]), SCM_OBJ(&scm__rc.d1787[907]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[992]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[992]))[4] = scm__rc.d1786[319];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[992]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[992]))[6] = scm__rc.d1786[242];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-umask")), SCM_OBJ(&libsyssys_umask__STUB), 0);
  libsyssys_umask__STUB.common.info = scm__rc.d1786[991];
  libsyssys_umask__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[992]);
  scm__rc.d1786[999] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[441])),TRUE); /* sys-sleep */
  scm__rc.d1786[1000] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[442])),TRUE); /* no-retry */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[908]), scm__rc.d1786[1000]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[909]), scm__rc.d1786[358]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[910]), scm__rc.d1786[952]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[913]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[914]), scm__rc.d1786[999]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[915]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[916]), scm__rc.d1786[3]);
  scm__rc.d1786[1001] = Scm_MakeExtendedPair(scm__rc.d1786[999], SCM_OBJ(&scm__rc.d1787[910]), SCM_OBJ(&scm__rc.d1787[918]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1002]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1002]))[4] = scm__rc.d1786[91];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1002]))[5] = scm__rc.d1786[319];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1002]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1002]))[7] = scm__rc.d1786[242];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-sleep")), SCM_OBJ(&libsyssys_sleep__STUB), 0);
  libsyssys_sleep__STUB.common.info = scm__rc.d1786[1001];
  libsyssys_sleep__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[1002]);
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  scm__rc.d1876[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1875[0])),TRUE); /* sys-nanosleep */
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  scm__rc.d1876[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1875[1])),TRUE); /* nanoseconds */
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  scm__rc.d1876[2] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1875[2]))); /* :optional */
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  scm__rc.d1876[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1875[3])),TRUE); /* no-retry */
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1877[1]), scm__rc.d1876[3]);
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1877[2]), scm__rc.d1876[2]);
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1877[3]), scm__rc.d1876[1]);
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  scm__rc.d1876[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1875[4])),TRUE); /* source-info */
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1877[6]), scm__rc.d1876[4]);
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  scm__rc.d1876[5] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1875[6])),TRUE); /* bind-info */
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  scm__rc.d1876[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1875[7])),TRUE); /* gauche */
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1877[7]), scm__rc.d1876[0]);
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1877[8]), scm__rc.d1876[6]);
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1877[9]), scm__rc.d1876[5]);
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  scm__rc.d1876[7] = Scm_MakeExtendedPair(scm__rc.d1876[0], SCM_OBJ(&scm__rc.d1877[3]), SCM_OBJ(&scm__rc.d1877[11]));
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  scm__rc.d1876[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1875[8])),TRUE); /* <top> */
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  scm__rc.d1876[9] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1875[9])),TRUE); /* * */
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  scm__rc.d1876[10] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1875[10])),TRUE); /* -> */
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  ((ScmObj*)SCM_OBJ(&scm__rc.d1876[11]))[3] = scm__rc.d1876[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1876[11]))[4] = scm__rc.d1876[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1876[11]))[5] = scm__rc.d1876[9];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1876[11]))[6] = scm__rc.d1876[10];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1876[11]))[7] = scm__rc.d1876[8];
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
#if (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS))
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-nanosleep")), SCM_OBJ(&libsyssys_nanosleep__STUB), 0);
  libsyssys_nanosleep__STUB.common.info = scm__rc.d1876[7];
  libsyssys_nanosleep__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1876[11]);
#endif /* (defined(HAVE_NANOSLEEP))||(defined(GAUCHE_WINDOWS)) */
  scm__rc.d1786[1010] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[443])),TRUE); /* sys-unlink */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[921]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[922]), scm__rc.d1786[1010]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[923]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[924]), scm__rc.d1786[3]);
  scm__rc.d1786[1011] = Scm_MakeExtendedPair(scm__rc.d1786[1010], SCM_OBJ(&scm__rc.d1787[1]), SCM_OBJ(&scm__rc.d1787[926]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-unlink")), SCM_OBJ(&libsyssys_unlink__STUB), 0);
  libsyssys_unlink__STUB.common.info = scm__rc.d1786[1011];
  libsyssys_unlink__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[257]);
  scm__rc.d1786[1012] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[444])),TRUE); /* sys-isatty */
  scm__rc.d1786[1013] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[445])),TRUE); /* port_or_fd */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[927]), scm__rc.d1786[1013]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[930]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[931]), scm__rc.d1786[1012]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[932]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[933]), scm__rc.d1786[3]);
  scm__rc.d1786[1014] = Scm_MakeExtendedPair(scm__rc.d1786[1012], SCM_OBJ(&scm__rc.d1787[927]), SCM_OBJ(&scm__rc.d1787[935]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-isatty")), SCM_OBJ(&libsyssys_isatty__STUB), 0);
  libsyssys_isatty__STUB.common.info = scm__rc.d1786[1014];
  libsyssys_isatty__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[721]);
  scm__rc.d1786[1015] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[446])),TRUE); /* sys-ttyname */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[938]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[939]), scm__rc.d1786[1015]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[940]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[941]), scm__rc.d1786[3]);
  scm__rc.d1786[1016] = Scm_MakeExtendedPair(scm__rc.d1786[1015], SCM_OBJ(&scm__rc.d1787[927]), SCM_OBJ(&scm__rc.d1787[943]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1017]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1017]))[4] = scm__rc.d1786[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1017]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1017]))[6] = scm__rc.d1786[274];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-ttyname")), SCM_OBJ(&libsyssys_ttyname__STUB), 0);
  libsyssys_ttyname__STUB.common.info = scm__rc.d1786[1016];
  libsyssys_ttyname__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[1017]);
  scm__rc.d1786[1024] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[447])),TRUE); /* sys-truncate */
  scm__rc.d1786[1025] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[448])),TRUE); /* length */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[944]), scm__rc.d1786[1025]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[945]), scm__rc.d1786[598]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[948]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[949]), scm__rc.d1786[1024]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[950]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[951]), scm__rc.d1786[3]);
  scm__rc.d1786[1026] = Scm_MakeExtendedPair(scm__rc.d1786[1024], SCM_OBJ(&scm__rc.d1787[945]), SCM_OBJ(&scm__rc.d1787[953]));
  scm__rc.d1786[1027] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[449])),TRUE); /* <integer> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1028]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1028]))[4] = scm__rc.d1786[256];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1028]))[5] = scm__rc.d1786[1027];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1028]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1028]))[7] = scm__rc.d1786[347];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-truncate")), SCM_OBJ(&libsyssys_truncate__STUB), 0);
  libsyssys_truncate__STUB.common.info = scm__rc.d1786[1026];
  libsyssys_truncate__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[1028]);
  scm__rc.d1786[1036] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[450])),TRUE); /* sys-ftruncate */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[954]), scm__rc.d1786[1013]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[957]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[958]), scm__rc.d1786[1036]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[959]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[960]), scm__rc.d1786[3]);
  scm__rc.d1786[1037] = Scm_MakeExtendedPair(scm__rc.d1786[1036], SCM_OBJ(&scm__rc.d1787[954]), SCM_OBJ(&scm__rc.d1787[962]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1038]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1038]))[4] = scm__rc.d1786[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1038]))[5] = scm__rc.d1786[1027];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1038]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[1038]))[7] = scm__rc.d1786[347];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-ftruncate")), SCM_OBJ(&libsyssys_ftruncate__STUB), 0);
  libsyssys_ftruncate__STUB.common.info = scm__rc.d1786[1037];
  libsyssys_ftruncate__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[1038]);
#if defined(HAVE_CRYPT)
  scm__rc.d1879[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1878[0])),TRUE); /* sys-crypt */
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
  scm__rc.d1879[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1878[1])),TRUE); /* key */
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
  scm__rc.d1879[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1878[2])),TRUE); /* salt */
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1880[1]), scm__rc.d1879[2]);
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1880[2]), scm__rc.d1879[1]);
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
  scm__rc.d1879[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1878[3])),TRUE); /* source-info */
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1880[5]), scm__rc.d1879[3]);
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
  scm__rc.d1879[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1878[5])),TRUE); /* bind-info */
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
  scm__rc.d1879[5] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1878[6])),TRUE); /* gauche */
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1880[6]), scm__rc.d1879[0]);
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1880[7]), scm__rc.d1879[5]);
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1880[8]), scm__rc.d1879[4]);
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
  scm__rc.d1879[6] = Scm_MakeExtendedPair(scm__rc.d1879[0], SCM_OBJ(&scm__rc.d1880[2]), SCM_OBJ(&scm__rc.d1880[10]));
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
  scm__rc.d1879[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1878[7])),TRUE); /* <const-cstring> */
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
  scm__rc.d1879[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1878[8])),TRUE); /* -> */
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1879[9]))[3] = scm__rc.d1879[5];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1879[9]))[4] = scm__rc.d1879[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1879[9]))[5] = scm__rc.d1879[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1879[9]))[6] = scm__rc.d1879[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1879[9]))[7] = scm__rc.d1879[7];
#endif /* defined(HAVE_CRYPT) */
#if defined(HAVE_CRYPT)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-crypt")), SCM_OBJ(&libsyssys_crypt__STUB), 0);
  libsyssys_crypt__STUB.common.info = scm__rc.d1879[6];
  libsyssys_crypt__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1879[9]);
#endif /* defined(HAVE_CRYPT) */
  scm__rc.d1786[1046] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[451])),TRUE); /* sys-gethostname */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[963]), scm__rc.d1786[1046]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[964]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[965]), scm__rc.d1786[3]);
  scm__rc.d1786[1047] = Scm_MakeExtendedPair(scm__rc.d1786[1046], SCM_NIL, SCM_OBJ(&scm__rc.d1787[966]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-gethostname")), SCM_OBJ(&libsyssys_gethostname__STUB), 0);
  libsyssys_gethostname__STUB.common.info = scm__rc.d1786[1047];
  libsyssys_gethostname__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[18]);
  scm__rc.d1786[1048] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[452])),TRUE); /* sys-getdomainname */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[967]), scm__rc.d1786[1048]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[968]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[969]), scm__rc.d1786[3]);
  scm__rc.d1786[1049] = Scm_MakeExtendedPair(scm__rc.d1786[1048], SCM_NIL, SCM_OBJ(&scm__rc.d1787[970]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-getdomainname")), SCM_OBJ(&libsyssys_getdomainname__STUB), 0);
  libsyssys_getdomainname__STUB.common.info = scm__rc.d1786[1049];
  libsyssys_getdomainname__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[18]);
#if defined(HAVE_SYMLINK)
  scm__rc.d1882[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1881[0])),TRUE); /* sys-symlink */
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  scm__rc.d1882[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1881[1])),TRUE); /* existing */
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  scm__rc.d1882[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1881[2])),TRUE); /* newpath */
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1883[1]), scm__rc.d1882[2]);
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1883[2]), scm__rc.d1882[1]);
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  scm__rc.d1882[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1881[3])),TRUE); /* source-info */
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1883[5]), scm__rc.d1882[3]);
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  scm__rc.d1882[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1881[5])),TRUE); /* bind-info */
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  scm__rc.d1882[5] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1881[6])),TRUE); /* gauche */
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1883[6]), scm__rc.d1882[0]);
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1883[7]), scm__rc.d1882[5]);
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1883[8]), scm__rc.d1882[4]);
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  scm__rc.d1882[6] = Scm_MakeExtendedPair(scm__rc.d1882[0], SCM_OBJ(&scm__rc.d1883[2]), SCM_OBJ(&scm__rc.d1883[10]));
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  scm__rc.d1882[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1881[7])),TRUE); /* <const-cstring> */
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  scm__rc.d1882[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1881[8])),TRUE); /* -> */
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  scm__rc.d1882[9] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1881[9])),TRUE); /* <void> */
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1882[10]))[3] = scm__rc.d1882[5];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1882[10]))[4] = scm__rc.d1882[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1882[10]))[5] = scm__rc.d1882[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1882[10]))[6] = scm__rc.d1882[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1882[10]))[7] = scm__rc.d1882[9];
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_SYMLINK)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-symlink")), SCM_OBJ(&libsyssys_symlink__STUB), 0);
  libsyssys_symlink__STUB.common.info = scm__rc.d1882[6];
  libsyssys_symlink__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1882[10]);
#endif /* defined(HAVE_SYMLINK) */
#if defined(HAVE_READLINK)
  scm__rc.d1885[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1884[0])),TRUE); /* sys-readlink */
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
  scm__rc.d1885[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1884[1])),TRUE); /* path */
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1886[1]), scm__rc.d1885[1]);
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
  scm__rc.d1885[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1884[2])),TRUE); /* source-info */
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1886[4]), scm__rc.d1885[2]);
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
  scm__rc.d1885[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1884[4])),TRUE); /* bind-info */
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
  scm__rc.d1885[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1884[5])),TRUE); /* gauche */
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1886[5]), scm__rc.d1885[0]);
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1886[6]), scm__rc.d1885[4]);
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1886[7]), scm__rc.d1885[3]);
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
  scm__rc.d1885[5] = Scm_MakeExtendedPair(scm__rc.d1885[0], SCM_OBJ(&scm__rc.d1886[1]), SCM_OBJ(&scm__rc.d1886[9]));
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
  scm__rc.d1885[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1884[6])),TRUE); /* <const-cstring> */
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
  scm__rc.d1885[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1884[7])),TRUE); /* -> */
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
  scm__rc.d1885[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1884[8])),TRUE); /* <top> */
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1885[9]))[3] = scm__rc.d1885[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1885[9]))[4] = scm__rc.d1885[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1885[9]))[5] = scm__rc.d1885[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1885[9]))[6] = scm__rc.d1885[8];
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_READLINK)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-readlink")), SCM_OBJ(&libsyssys_readlink__STUB), 0);
  libsyssys_readlink__STUB.common.info = scm__rc.d1885[5];
  libsyssys_readlink__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1885[9]);
#endif /* defined(HAVE_READLINK) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[0])),TRUE); /* sys-fdset-ref */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[1])),TRUE); /* fdset */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[2])),TRUE); /* pf */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[1]), scm__rc.d1891[2]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[2]), scm__rc.d1891[1]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[3])),TRUE); /* source-info */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[5]), scm__rc.d1891[3]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[5])),TRUE); /* bind-info */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[5] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[6])),TRUE); /* gauche */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[6]), scm__rc.d1891[0]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[7]), scm__rc.d1891[5]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[8]), scm__rc.d1891[4]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[6] = Scm_MakeExtendedPair(scm__rc.d1891[0], SCM_OBJ(&scm__rc.d1892[2]), SCM_OBJ(&scm__rc.d1892[10]));
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[7])),TRUE); /* <sys-fdset> */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[8])),TRUE); /* <top> */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[9] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[9])),TRUE); /* -> */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[10] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[10])),TRUE); /* <boolean> */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[11]))[3] = scm__rc.d1891[5];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[11]))[4] = scm__rc.d1891[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[11]))[5] = scm__rc.d1891[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[11]))[6] = scm__rc.d1891[9];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[11]))[7] = scm__rc.d1891[10];
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-fdset-ref")), SCM_OBJ(&libsyssys_fdset_ref__STUB), 0);
  libsyssys_fdset_ref__STUB.common.info = scm__rc.d1891[6];
  libsyssys_fdset_ref__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1891[11]);
  Scm_SetterSet(SCM_PROCEDURE(&libsyssys_fdset_ref__STUB), SCM_PROCEDURE(&libsyssys_fdset_setX__STUB), TRUE);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[19] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[11])),TRUE); /* sys-fdset-set! */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[20] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[12])),TRUE); /* flag */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[11]), scm__rc.d1891[20]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[12]), scm__rc.d1891[2]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[13]), scm__rc.d1891[1]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[16]), scm__rc.d1891[3]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[17]), scm__rc.d1891[19]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[18]), scm__rc.d1891[5]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[19]), scm__rc.d1891[4]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[21] = Scm_MakeExtendedPair(scm__rc.d1891[19], SCM_OBJ(&scm__rc.d1892[13]), SCM_OBJ(&scm__rc.d1892[21]));
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[22] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[13])),TRUE); /* <void> */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[23]))[3] = scm__rc.d1891[5];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[23]))[4] = scm__rc.d1891[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[23]))[5] = scm__rc.d1891[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[23]))[6] = scm__rc.d1891[10];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[23]))[7] = scm__rc.d1891[9];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[23]))[8] = scm__rc.d1891[22];
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-fdset-set!")), SCM_OBJ(&libsyssys_fdset_setX__STUB), 0);
  libsyssys_fdset_setX__STUB.common.info = scm__rc.d1891[21];
  libsyssys_fdset_setX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1891[23]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[32] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[14])),TRUE); /* sys-fdset-max-fd */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[22]), scm__rc.d1891[1]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[25]), scm__rc.d1891[3]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[26]), scm__rc.d1891[32]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[27]), scm__rc.d1891[5]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[28]), scm__rc.d1891[4]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[33] = Scm_MakeExtendedPair(scm__rc.d1891[32], SCM_OBJ(&scm__rc.d1892[22]), SCM_OBJ(&scm__rc.d1892[30]));
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[34] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[15])),TRUE); /* <int> */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[35]))[3] = scm__rc.d1891[5];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[35]))[4] = scm__rc.d1891[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[35]))[5] = scm__rc.d1891[9];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[35]))[6] = scm__rc.d1891[34];
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-fdset-max-fd")), SCM_OBJ(&libsyssys_fdset_max_fd__STUB), 0);
  libsyssys_fdset_max_fd__STUB.common.info = scm__rc.d1891[33];
  libsyssys_fdset_max_fd__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1891[35]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[42] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[16])),TRUE); /* sys-fdset-clear! */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[33]), scm__rc.d1891[3]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[34]), scm__rc.d1891[42]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[35]), scm__rc.d1891[5]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[36]), scm__rc.d1891[4]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[43] = Scm_MakeExtendedPair(scm__rc.d1891[42], SCM_OBJ(&scm__rc.d1892[22]), SCM_OBJ(&scm__rc.d1892[38]));
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[44]))[3] = scm__rc.d1891[5];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[44]))[4] = scm__rc.d1891[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[44]))[5] = scm__rc.d1891[9];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[44]))[6] = scm__rc.d1891[8];
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-fdset-clear!")), SCM_OBJ(&libsyssys_fdset_clearX__STUB), 0);
  libsyssys_fdset_clearX__STUB.common.info = scm__rc.d1891[43];
  libsyssys_fdset_clearX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1891[44]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[51] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[17])),TRUE); /* sys-fdset-copy! */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[52] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[18])),TRUE); /* dst */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[53] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[19])),TRUE); /* src */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[39]), scm__rc.d1891[53]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[40]), scm__rc.d1891[52]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[43]), scm__rc.d1891[3]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[44]), scm__rc.d1891[51]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[45]), scm__rc.d1891[5]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[46]), scm__rc.d1891[4]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[54] = Scm_MakeExtendedPair(scm__rc.d1891[51], SCM_OBJ(&scm__rc.d1892[40]), SCM_OBJ(&scm__rc.d1892[48]));
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[55]))[3] = scm__rc.d1891[5];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[55]))[4] = scm__rc.d1891[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[55]))[5] = scm__rc.d1891[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[55]))[6] = scm__rc.d1891[9];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[55]))[7] = scm__rc.d1891[8];
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-fdset-copy!")), SCM_OBJ(&libsyssys_fdset_copyX__STUB), 0);
  libsyssys_fdset_copyX__STUB.common.info = scm__rc.d1891[54];
  libsyssys_fdset_copyX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1891[55]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[63] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[20])),TRUE); /* sys-select */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[64] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[21])),TRUE); /* rfds */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[65] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[22])),TRUE); /* wfds */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[66] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[23])),TRUE); /* efds */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[67] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1889[24]))); /* :optional */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[68] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[25])),TRUE); /* timeout */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[49]), scm__rc.d1891[68]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[50]), scm__rc.d1891[67]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[51]), scm__rc.d1891[66]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[52]), scm__rc.d1891[65]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[53]), scm__rc.d1891[64]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[56]), scm__rc.d1891[3]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[57]), scm__rc.d1891[63]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[58]), scm__rc.d1891[5]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[59]), scm__rc.d1891[4]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[69] = Scm_MakeExtendedPair(scm__rc.d1891[63], SCM_OBJ(&scm__rc.d1892[53]), SCM_OBJ(&scm__rc.d1892[61]));
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[70] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[26])),TRUE); /* * */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[71]))[3] = scm__rc.d1891[5];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[71]))[4] = scm__rc.d1891[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[71]))[5] = scm__rc.d1891[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[71]))[6] = scm__rc.d1891[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[71]))[7] = scm__rc.d1891[70];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[71]))[8] = scm__rc.d1891[9];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1891[71]))[9] = scm__rc.d1891[8];
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-select")), SCM_OBJ(&libsyssys_select__STUB), 0);
  libsyssys_select__STUB.common.info = scm__rc.d1891[69];
  libsyssys_select__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1891[71]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[81] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1889[27])),TRUE); /* sys-select! */
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[64]), scm__rc.d1891[3]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[65]), scm__rc.d1891[81]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[66]), scm__rc.d1891[5]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1892[67]), scm__rc.d1891[4]);
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  scm__rc.d1891[82] = Scm_MakeExtendedPair(scm__rc.d1891[81], SCM_OBJ(&scm__rc.d1892[53]), SCM_OBJ(&scm__rc.d1892[69]));
#endif /* defined(HAVE_SELECT) */
#if defined(HAVE_SELECT)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-select!")), SCM_OBJ(&libsyssys_selectX__STUB), 0);
  libsyssys_selectX__STUB.common.info = scm__rc.d1891[82];
  libsyssys_selectX__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1891[71]);
#endif /* defined(HAVE_SELECT) */
  scm__rc.d1786[1050] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[453])),TRUE); /* sys-available-processors */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[973]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[974]), scm__rc.d1786[1050]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[975]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[976]), scm__rc.d1786[3]);
  scm__rc.d1786[1051] = Scm_MakeExtendedPair(scm__rc.d1786[1050], SCM_NIL, SCM_OBJ(&scm__rc.d1787[978]));
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-available-processors")), SCM_OBJ(&libsyssys_available_processors__STUB), 0);
  libsyssys_available_processors__STUB.common.info = scm__rc.d1786[1051];
  libsyssys_available_processors__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[388]);
  scm__rc.d1786[1052] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[16])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[16]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[16]))->debugInfo = scm__rc.d1786[1052];
  scm__rc.d1786[1053] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[454])),TRUE); /* glob */
  scm__rc.d1786[1055] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[455])),TRUE); /* glob-fold */
  scm__rc.d1786[1054] = Scm_MakeIdentifier(scm__rc.d1786[1055], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#glob-fold */
  scm__rc.d1786[1056] = Scm_MakeIdentifier(scm__rc.d1786[545], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#cons */
  scm__rc.d1786[1057] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[456])),TRUE); /* patterns */
  scm__rc.d1786[1058] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[457])),TRUE); /* opts */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[979]), scm__rc.d1786[1057]);
  SCM_SET_CDR(SCM_OBJ(&scm__rc.d1787[979]), scm__rc.d1786[1058]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[982]), scm__rc.d1786[2]);
  scm__rc.d1786[1059] = Scm_MakeExtendedPair(scm__rc.d1786[1053], SCM_OBJ(&scm__rc.d1787[979]), SCM_OBJ(&scm__rc.d1787[983]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[984]), scm__rc.d1786[1059]);
  scm__rc.d1786[1060] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[17])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[17]))->name = scm__rc.d1786[1053];/* glob */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[17]))->debugInfo = scm__rc.d1786[1060];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[815]))[1] = SCM_WORD(scm__rc.d1786[1054]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[815]))[4] = SCM_WORD(scm__rc.d1786[1056]);
  scm__rc.d1786[1061] = Scm_MakeIdentifier(scm__rc.d1786[1053], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#glob */
  scm__rc.d1786[1062] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[18])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[18]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[18]))->debugInfo = scm__rc.d1786[1062];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[824]))[3] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[824]))[6] = SCM_WORD(scm__rc.d1786[1053]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[824]))[13] = SCM_WORD(scm__rc.d1786[1061]);
  scm__rc.d1786[1063] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[458])),TRUE); /* sys-glob */
  scm__rc.d1786[1064] = Scm_MakeIdentifier(scm__rc.d1786[1063], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#sys-glob */
  scm__rc.d1786[1065] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[19])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[19]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[19]))->debugInfo = scm__rc.d1786[1065];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[839]))[3] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[839]))[6] = SCM_WORD(scm__rc.d1786[1063]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[839]))[10] = SCM_WORD(scm__rc.d1786[1061]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[839]))[13] = SCM_WORD(scm__rc.d1786[1064]);
  {
     ScmCharSet *cs = SCM_CHARSET(Scm_MakeEmptyCharSet());
     Scm_CharSetAddRange(cs, SCM_CHAR(47), SCM_CHAR(47));
     scm__rc.d1786[1066] = SCM_OBJ(cs);
  }
  scm__rc.d1786[1068] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[459])),TRUE); /* glob-fs-folder */
  scm__rc.d1786[1067] = Scm_MakeIdentifier(scm__rc.d1786[1068], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#glob-fs-folder */
  scm__rc.d1786[1070] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[460])),TRUE); /* sort */
  scm__rc.d1786[1069] = Scm_MakeIdentifier(scm__rc.d1786[1070], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#sort */
  scm__rc.d1786[1072] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[461])),TRUE); /* glob-fold-1 */
  scm__rc.d1786[1071] = Scm_MakeIdentifier(scm__rc.d1786[1072], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#glob-fold-1 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[985]), scm__rc.d1786[1055]);
  scm__rc.d1786[1073] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[462])),FALSE); /* G1901 */
  scm__rc.d1786[1074] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[463])),FALSE); /* G1902 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[986]), scm__rc.d1786[1074]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[987]), scm__rc.d1786[1073]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[990]), scm__rc.d1786[2]);
  scm__rc.d1786[1075] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[985]), SCM_OBJ(&scm__rc.d1787[987]), SCM_OBJ(&scm__rc.d1787[991]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[992]), scm__rc.d1786[1075]);
  scm__rc.d1786[1076] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[20])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[20]))->debugInfo = scm__rc.d1786[1076];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[854]))[7] = SCM_WORD(scm__rc.d1786[1071]);
  scm__rc.d1786[1078] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[464])),TRUE); /* glob-expand-braces */
  scm__rc.d1786[1077] = Scm_MakeIdentifier(scm__rc.d1786[1078], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#glob-expand-braces */
  scm__rc.d1786[1080] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[465])),TRUE); /* list? */
  scm__rc.d1786[1079] = Scm_MakeIdentifier(scm__rc.d1786[1080], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#list? */
  scm__rc.d1786[1082] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[466])),TRUE); /* fold */
  scm__rc.d1786[1081] = Scm_MakeIdentifier(scm__rc.d1786[1082], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#fold */
  scm__rc.d1786[1083] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[467]))); /* :separator */
  scm__rc.d1786[1084] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[468]))); /* :folder */
  scm__rc.d1786[1085] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[469]))); /* :sorter */
  scm__rc.d1786[1086] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[470]))); /* :prefix */
  scm__rc.d1786[1087] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[467])),TRUE); /* separator */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[993]), scm__rc.d1786[1066]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[994]), scm__rc.d1786[1087]);
  scm__rc.d1786[1088] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[468])),TRUE); /* folder */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[995]), scm__rc.d1786[1068]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[996]), scm__rc.d1786[1088]);
  scm__rc.d1786[1089] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[469])),TRUE); /* sorter */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[997]), scm__rc.d1786[1070]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[998]), scm__rc.d1786[1089]);
  scm__rc.d1786[1090] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[470])),TRUE); /* prefix */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[999]), scm__rc.d1786[1090]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1004]), scm__rc.d1786[79]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1005]), scm__rc.d1786[505]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1006]), scm__rc.d1786[357]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1007]), scm__rc.d1786[1057]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1010]), scm__rc.d1786[2]);
  scm__rc.d1786[1091] = Scm_MakeExtendedPair(scm__rc.d1786[1055], SCM_OBJ(&scm__rc.d1787[1007]), SCM_OBJ(&scm__rc.d1787[1011]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1012]), scm__rc.d1786[1091]);
  scm__rc.d1786[1092] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[21])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[21]))->name = scm__rc.d1786[1055];/* glob-fold */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[21]))->debugInfo = scm__rc.d1786[1092];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[16] = SCM_WORD(scm__rc.d1786[31]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[20] = SCM_WORD(scm__rc.d1786[1066]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[29] = SCM_WORD(scm__rc.d1786[31]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[33] = SCM_WORD(scm__rc.d1786[1067]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[42] = SCM_WORD(scm__rc.d1786[31]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[46] = SCM_WORD(scm__rc.d1786[1069]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[55] = SCM_WORD(scm__rc.d1786[31]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[72] = SCM_WORD(scm__rc.d1786[1077]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[78] = SCM_WORD(scm__rc.d1786[1079]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[87] = SCM_WORD(scm__rc.d1786[1081]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[89] = SCM_WORD(scm__rc.d1786[1081]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[107] = SCM_WORD(scm__rc.d1786[71]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[114] = SCM_WORD(scm__rc.d1786[72]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[118] = SCM_WORD(scm__rc.d1786[1083]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[132] = SCM_WORD(scm__rc.d1786[1084]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[146] = SCM_WORD(scm__rc.d1786[1085]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[160] = SCM_WORD(scm__rc.d1786[1086]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[863]))[179] = SCM_WORD(scm__rc.d1786[77]);
  scm__rc.d1786[1093] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[22])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[22]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[22]))->debugInfo = scm__rc.d1786[1093];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1053]))[3] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1053]))[6] = SCM_WORD(scm__rc.d1786[1055]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1053]))[13] = SCM_WORD(scm__rc.d1786[1054]);
  scm__rc.d1786[1094] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[471])),TRUE); /* ** */
  scm__rc.d1786[1095] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[472])),TRUE); /* rec */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1013]), scm__rc.d1786[1095]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1014]), scm__rc.d1786[1072]);
  scm__rc.d1786[1096] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[473])),TRUE); /* node */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1015]), scm__rc.d1786[1096]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1018]), scm__rc.d1786[2]);
  scm__rc.d1786[1097] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1014]), SCM_OBJ(&scm__rc.d1787[1015]), SCM_OBJ(&scm__rc.d1787[1019]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1020]), scm__rc.d1786[1097]);
  scm__rc.d1786[1098] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[23])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[23]))->debugInfo = scm__rc.d1786[1098];
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1021]), scm__rc.d1786[1095]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1022]), scm__rc.d1786[1072]);
  scm__rc.d1786[1099] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[474])),TRUE); /* matcher */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1023]), scm__rc.d1786[1099]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1024]), scm__rc.d1786[1096]);
  scm__rc.d1786[1100] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[24])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[24]))->name = scm__rc.d1786[1095];/* (glob-fold-1 rec) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[24]))->debugInfo = scm__rc.d1786[1100];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1075]))[6] = SCM_WORD(scm__rc.d1786[1094]);
  scm__rc.d1786[1101] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[475])),TRUE); /* rec* */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1027]), scm__rc.d1786[1101]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1028]), scm__rc.d1786[1072]);
  scm__rc.d1786[1102] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[476])),FALSE); /* G1903 */
  scm__rc.d1786[1103] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[477])),FALSE); /* G1904 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1029]), scm__rc.d1786[1103]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1030]), scm__rc.d1786[1102]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1033]), scm__rc.d1786[2]);
  scm__rc.d1786[1104] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1028]), SCM_OBJ(&scm__rc.d1787[1030]), SCM_OBJ(&scm__rc.d1787[1034]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1035]), scm__rc.d1786[1104]);
  scm__rc.d1786[1105] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[25])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[25]))->debugInfo = scm__rc.d1786[1105];
  scm__rc.d1786[1106] = Scm_RegComp(SCM_STRING(SCM_OBJ(&scm__sc.d1785[478])), 0);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1036]), scm__rc.d1786[1101]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1037]), scm__rc.d1786[1072]);
  scm__rc.d1786[1107] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[26])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[26]))->name = scm__rc.d1786[1101];/* (glob-fold-1 rec*) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[26]))->debugInfo = scm__rc.d1786[1107];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1120]))[12] = SCM_WORD(scm__rc.d1786[1056]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1120]))[16] = SCM_WORD(scm__rc.d1786[1106]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1120]))[22] = SCM_WORD(scm__rc.d1786[1081]);
  scm__rc.d1786[1109] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[479])),TRUE); /* glob-prepare-pattern */
  scm__rc.d1786[1108] = Scm_MakeIdentifier(scm__rc.d1786[1109], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#glob-prepare-pattern */
  scm__rc.d1786[1110] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[480])),TRUE); /* pattern */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1042]), scm__rc.d1786[1090]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1043]), scm__rc.d1786[1088]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1044]), scm__rc.d1786[1087]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1045]), scm__rc.d1786[505]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1046]), scm__rc.d1786[357]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1047]), scm__rc.d1786[1110]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1050]), scm__rc.d1786[2]);
  scm__rc.d1786[1111] = Scm_MakeExtendedPair(scm__rc.d1786[1072], SCM_OBJ(&scm__rc.d1787[1047]), SCM_OBJ(&scm__rc.d1787[1051]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1052]), scm__rc.d1786[1111]);
  scm__rc.d1786[1112] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[27])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[27]))->name = scm__rc.d1786[1072];/* glob-fold-1 */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[27]))->debugInfo = scm__rc.d1786[1112];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1144]))[8] = SCM_WORD(scm__rc.d1786[1108]);
  scm__rc.d1786[1113] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[28])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[28]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[28]))->debugInfo = scm__rc.d1786[1113];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1162]))[3] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1162]))[6] = SCM_WORD(scm__rc.d1786[1072]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1162]))[13] = SCM_WORD(scm__rc.d1786[1071]);
  scm__rc.d1786[1114] = Scm_MakeIdentifier(scm__rc.d1786[42], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#equal? */
  scm__rc.d1786[1115] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[481])),TRUE); /* dir? */
  scm__rc.d1786[1117] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[482])),TRUE); /* glob-component->regexp */
  scm__rc.d1786[1116] = Scm_MakeIdentifier(scm__rc.d1786[1117], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#glob-component->regexp */
  scm__rc.d1786[1118] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[483])),TRUE); /* f */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1053]), scm__rc.d1786[1118]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1054]), scm__rc.d1786[1109]);
  scm__rc.d1786[1119] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[484])),TRUE); /* comp */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1055]), scm__rc.d1786[1119]);
  scm__rc.d1786[1120] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[29])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[29]))->debugInfo = scm__rc.d1786[1120];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1177]))[6] = SCM_WORD(scm__rc.d1786[1114]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1177]))[10] = SCM_WORD(scm__rc.d1786[1115]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1177]))[17] = SCM_WORD(scm__rc.d1786[1114]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1177]))[21] = SCM_WORD(scm__rc.d1786[1094]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1177]))[24] = SCM_WORD(scm__rc.d1786[1116]);
  scm__rc.d1786[1121] = Scm_MakeIdentifier(scm__rc.d1786[65], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#string-split */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1059]), scm__rc.d1786[1087]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1060]), scm__rc.d1786[1110]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1063]), scm__rc.d1786[2]);
  scm__rc.d1786[1122] = Scm_MakeExtendedPair(scm__rc.d1786[1109], SCM_OBJ(&scm__rc.d1787[1060]), SCM_OBJ(&scm__rc.d1787[1064]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1065]), scm__rc.d1786[1122]);
  scm__rc.d1786[1123] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[30])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[30]))->name = scm__rc.d1786[1109];/* glob-prepare-pattern */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[30]))->debugInfo = scm__rc.d1786[1123];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1203]))[9] = SCM_WORD(scm__rc.d1786[1121]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1203]))[18] = SCM_WORD(scm__rc.d1786[1114]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1203]))[28] = SCM_WORD(scm__rc.d1786[546]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1203]))[37] = SCM_WORD(scm__rc.d1786[546]);
  scm__rc.d1786[1124] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[31])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[31]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[31]))->debugInfo = scm__rc.d1786[1124];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1243]))[5] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1243]))[8] = SCM_WORD(scm__rc.d1786[1109]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1243]))[15] = SCM_WORD(scm__rc.d1786[1108]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1066]), scm__rc.d1786[81]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1067]), scm__rc.d1786[1078]);
  scm__rc.d1786[1125] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[485])),TRUE); /* pat */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1068]), scm__rc.d1786[1125]);
  scm__rc.d1786[1126] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[32])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[32]))->debugInfo = scm__rc.d1786[1126];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1260]))[6] = SCM_WORD(scm__rc.d1786[1121]);
  scm__rc.d1786[1127] = Scm_RegComp(SCM_STRING(SCM_OBJ(&scm__sc.d1785[486])), 0);
  scm__rc.d1786[1128] = Scm_MakeIdentifier(scm__rc.d1786[40], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#rxmatch */
  scm__rc.d1786[1129] = Scm_MakeIdentifier(scm__rc.d1786[58], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#string-append */
  scm__rc.d1786[1130] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[488])),TRUE); /* parse */
  scm__rc.d1786[1131] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[489])),TRUE); /* loop */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1073]), scm__rc.d1786[1131]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1074]), scm__rc.d1786[1130]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1075]), scm__rc.d1786[1078]);
  scm__rc.d1786[1132] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[490])),TRUE); /* in */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1076]), scm__rc.d1786[1132]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1079]), scm__rc.d1786[2]);
  scm__rc.d1786[1133] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1075]), SCM_OBJ(&scm__rc.d1787[1076]), SCM_OBJ(&scm__rc.d1787[1080]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1081]), scm__rc.d1786[1133]);
  scm__rc.d1786[1134] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[33])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[33]))->debugInfo = scm__rc.d1786[1134];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1276]))[5] = SCM_WORD(scm__rc.d1786[1129]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1082]), scm__rc.d1786[1131]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1083]), scm__rc.d1786[1130]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1084]), scm__rc.d1786[1078]);
  scm__rc.d1786[1135] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[491])),TRUE); /* seg */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1085]), scm__rc.d1786[1135]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1088]), scm__rc.d1786[2]);
  scm__rc.d1786[1136] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1084]), SCM_OBJ(&scm__rc.d1787[1085]), SCM_OBJ(&scm__rc.d1787[1089]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1090]), scm__rc.d1786[1136]);
  scm__rc.d1786[1137] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[34])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[34]))->debugInfo = scm__rc.d1786[1137];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1286]))[6] = SCM_WORD(scm__rc.d1786[1081]);
  scm__rc.d1786[1138] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[492])),TRUE); /* before */
  scm__rc.d1786[1139] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[493])),FALSE); /* G1907 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1091]), scm__rc.d1786[1139]);
  scm__rc.d1786[1140] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[35])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[35]))->debugInfo = scm__rc.d1786[1140];
  scm__rc.d1786[1141] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[495])),FALSE); /* G1910 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1094]), scm__rc.d1786[1141]);
  scm__rc.d1786[1142] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[36])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[36]))->debugInfo = scm__rc.d1786[1142];
  scm__rc.d1786[1143] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[496])),FALSE); /* G1913 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1097]), scm__rc.d1786[1143]);
  scm__rc.d1786[1144] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[37])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[37]))->debugInfo = scm__rc.d1786[1144];
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1100]), scm__rc.d1786[1131]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1101]), scm__rc.d1786[1130]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1102]), scm__rc.d1786[1078]);
  scm__rc.d1786[1145] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[498])),TRUE); /* _ */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1105]), scm__rc.d1786[2]);
  scm__rc.d1786[1146] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1102]), scm__rc.d1786[1145], SCM_OBJ(&scm__rc.d1787[1106]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1107]), scm__rc.d1786[1146]);
  scm__rc.d1786[1147] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[38])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[38]))->name = scm__rc.d1786[1131];/* (glob-expand-braces parse loop) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[38]))->debugInfo = scm__rc.d1786[1147];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[3] = SCM_WORD(scm__rc.d1786[1127]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[6] = SCM_WORD(scm__rc.d1786[1128]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[22] = SCM_WORD(scm__rc.d1786[1114]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[30] = SCM_WORD(scm__rc.d1786[56]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[54] = SCM_WORD(scm__rc.d1786[1129]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[59] = SCM_WORD(scm__rc.d1786[1138]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[69] = SCM_WORD(scm__rc.d1786[546]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[71] = SCM_WORD(scm__rc.d1786[1081]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[83] = SCM_WORD(scm__rc.d1786[553]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[96] = SCM_WORD(scm__rc.d1786[1129]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[101] = SCM_WORD(scm__rc.d1786[1138]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[111] = SCM_WORD(scm__rc.d1786[546]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[113] = SCM_WORD(scm__rc.d1786[1081]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[117] = SCM_WORD(scm__rc.d1786[56]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[131] = SCM_WORD(scm__rc.d1786[1129]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[139] = SCM_WORD(scm__rc.d1786[546]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1309]))[147] = SCM_WORD(scm__rc.d1786[553]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1109]), scm__rc.d1786[1130]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1110]), scm__rc.d1786[1078]);
  scm__rc.d1786[1148] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[499])),TRUE); /* str */
  scm__rc.d1786[1149] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[500])),TRUE); /* pres */
  scm__rc.d1786[1150] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[501])),TRUE); /* level */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1111]), scm__rc.d1786[1150]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1112]), scm__rc.d1786[1149]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1113]), scm__rc.d1786[1148]);
  scm__rc.d1786[1151] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[39])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[39]))->name = scm__rc.d1786[1130];/* (glob-expand-braces parse) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[39]))->debugInfo = scm__rc.d1786[1151];
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1117]), scm__rc.d1786[1110]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1120]), scm__rc.d1786[2]);
  scm__rc.d1786[1152] = Scm_MakeExtendedPair(scm__rc.d1786[1078], SCM_OBJ(&scm__rc.d1787[1117]), SCM_OBJ(&scm__rc.d1787[1121]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1122]), scm__rc.d1786[1152]);
  scm__rc.d1786[1153] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[40])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[40]))->name = scm__rc.d1786[1078];/* glob-expand-braces */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[40]))->debugInfo = scm__rc.d1786[1153];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1465]))[10] = SCM_WORD(scm__rc.d1786[528]);
  scm__rc.d1786[1154] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[41])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[41]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[41]))->debugInfo = scm__rc.d1786[1154];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1497]))[5] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1497]))[8] = SCM_WORD(scm__rc.d1786[1078]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1497]))[15] = SCM_WORD(scm__rc.d1786[1077]);
  scm__rc.d1786[1155] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[502])),TRUE); /* eol */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1123]), scm__rc.d1786[1155]);
  scm__rc.d1786[1156] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[503])),TRUE); /* rep */
  scm__rc.d1786[1157] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[504])),TRUE); /* any */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1124]), scm__rc.d1786[1157]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1127]), scm__rc.d1786[1156]);
  scm__rc.d1786[1159] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[505])),TRUE); /* read-char-set */
  scm__rc.d1786[1158] = Scm_MakeIdentifier(scm__rc.d1786[1159], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#read-char-set */
  scm__rc.d1786[1161] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[506])),TRUE); /* char-set-complement! */
  scm__rc.d1786[1160] = Scm_MakeIdentifier(scm__rc.d1786[1161], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#char-set-complement! */
  scm__rc.d1786[1162] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[507])),TRUE); /* element1 */
  scm__rc.d1786[1163] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[508])),FALSE); /* G1917 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1128]), scm__rc.d1786[1163]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1129]), scm__rc.d1786[1117]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1130]), scm__rc.d1786[1162]);
  scm__rc.d1786[1164] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[509])),TRUE); /* ch */
  scm__rc.d1786[1165] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[510])),TRUE); /* ct */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1132]), scm__rc.d1786[1165]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1133]), scm__rc.d1786[1164]);
  scm__rc.d1786[1166] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[42])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[42]))->name = scm__rc.d1786[1162];/* ((glob-component->regexp G1917) element1) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[42]))->debugInfo = scm__rc.d1786[1166];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]))[40] = SCM_WORD(scm__rc.d1786[1157]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]))[65] = SCM_WORD(scm__rc.d1786[1158]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]))[71] = SCM_WORD(scm__rc.d1786[1160]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1514]))[85] = SCM_WORD(scm__rc.d1786[1158]);
  scm__rc.d1786[1167] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[511])),TRUE); /* bol */
  {
     ScmCharSet *cs = SCM_CHARSET(Scm_MakeEmptyCharSet());
     Scm_CharSetAddRange(cs, SCM_CHAR(46), SCM_CHAR(46));
     scm__rc.d1786[1168] = SCM_OBJ(cs);
  }
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1137]), scm__rc.d1786[1119]);
  SCM_SET_CDR(SCM_OBJ(&scm__rc.d1787[1137]), scm__rc.d1786[1168]);
  scm__rc.d1786[1169] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[512])),TRUE); /* seq */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1140]), scm__rc.d1786[2]);
  scm__rc.d1786[1170] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1129]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[1141]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1142]), scm__rc.d1786[1170]);
  scm__rc.d1786[1171] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[43])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[43]))->debugInfo = scm__rc.d1786[1171];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1621]))[5] = SCM_WORD(scm__rc.d1786[1167]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1621]))[66] = SCM_WORD(scm__rc.d1786[1156]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1621]))[70] = SCM_WORD(scm__rc.d1786[1169]);
  scm__rc.d1786[1173] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[513])),TRUE); /* with-input-from-string */
  scm__rc.d1786[1172] = Scm_MakeIdentifier(scm__rc.d1786[1173], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#with-input-from-string */
  scm__rc.d1786[1175] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[514])),TRUE); /* regexp-optimize */
  scm__rc.d1786[1174] = Scm_MakeIdentifier(scm__rc.d1786[1175], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#regexp-optimize */
  scm__rc.d1786[1177] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[515])),TRUE); /* regexp-compile */
  scm__rc.d1786[1176] = Scm_MakeIdentifier(scm__rc.d1786[1177], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#regexp-compile */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1144]), scm__rc.d1786[1110]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1147]), scm__rc.d1786[2]);
  scm__rc.d1786[1178] = Scm_MakeExtendedPair(scm__rc.d1786[1117], SCM_OBJ(&scm__rc.d1787[1144]), SCM_OBJ(&scm__rc.d1787[1148]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1149]), scm__rc.d1786[1178]);
  scm__rc.d1786[1179] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[44])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[44]))->name = scm__rc.d1786[1117];/* glob-component->regexp */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[44]))->debugInfo = scm__rc.d1786[1179];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1732]))[7] = SCM_WORD(scm__rc.d1786[1172]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1732]))[9] = SCM_WORD(scm__rc.d1786[1174]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1732]))[11] = SCM_WORD(scm__rc.d1786[1176]);
  scm__rc.d1786[1180] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[45])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[45]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[45]))->debugInfo = scm__rc.d1786[1180];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1745]))[5] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1745]))[8] = SCM_WORD(scm__rc.d1786[1117]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1745]))[15] = SCM_WORD(scm__rc.d1786[1116]);
  scm__rc.d1786[1181] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[516])),TRUE); /* fixed-regexp? */
  scm__rc.d1786[1183] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[517])),TRUE); /* regexp-ast */
  scm__rc.d1786[1182] = Scm_MakeIdentifier(scm__rc.d1786[1183], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#regexp-ast */
  scm__rc.d1786[1185] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[518])),TRUE); /* list->string */
  scm__rc.d1786[1184] = Scm_MakeIdentifier(scm__rc.d1786[1185], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#list->string */
  scm__rc.d1786[1186] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[519])),TRUE); /* rx */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1150]), scm__rc.d1786[1186]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1153]), scm__rc.d1786[2]);
  scm__rc.d1786[1187] = Scm_MakeExtendedPair(scm__rc.d1786[1181], SCM_OBJ(&scm__rc.d1787[1150]), SCM_OBJ(&scm__rc.d1787[1154]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1155]), scm__rc.d1786[1187]);
  scm__rc.d1786[1188] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[46])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[46]))->name = scm__rc.d1786[1181];/* fixed-regexp? */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[46]))->debugInfo = scm__rc.d1786[1188];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1762]))[4] = SCM_WORD(scm__rc.d1786[1182]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1762]))[16] = SCM_WORD(scm__rc.d1786[1167]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1762]))[28] = SCM_WORD(scm__rc.d1786[1155]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1762]))[33] = SCM_WORD(scm__rc.d1786[1184]);
  scm__rc.d1786[1189] = Scm_MakeIdentifier(scm__rc.d1786[1181], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#fixed-regexp? */
  scm__rc.d1786[1190] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[47])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[47]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[47]))->debugInfo = scm__rc.d1786[1190];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1815]))[3] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1815]))[6] = SCM_WORD(scm__rc.d1786[1181]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1815]))[13] = SCM_WORD(scm__rc.d1786[1189]);
  scm__rc.d1786[1191] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[520])),TRUE); /* make-glob-fs-fold */
  scm__rc.d1786[1193] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[521])),TRUE); /* gauche-architecture */
  scm__rc.d1786[1192] = Scm_MakeIdentifier(scm__rc.d1786[1193], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#gauche-architecture */
  scm__rc.d1786[1194] = Scm_RegComp(SCM_STRING(SCM_OBJ(&scm__sc.d1785[522])), 0);
  scm__rc.d1786[1196] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[523])),TRUE); /* string-length */
  scm__rc.d1786[1195] = Scm_MakeIdentifier(scm__rc.d1786[1196], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#string-length */
  scm__rc.d1786[1198] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[524])),TRUE); /* string-ref */
  scm__rc.d1786[1197] = Scm_MakeIdentifier(scm__rc.d1786[1198], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#string-ref */
  scm__rc.d1786[1199] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[525])),TRUE); /* ensure-dirname */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1156]), scm__rc.d1786[1199]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1157]), scm__rc.d1786[1191]);
  scm__rc.d1786[1200] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[526])),TRUE); /* s */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1158]), scm__rc.d1786[1200]);
  scm__rc.d1786[1201] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[48])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[48]))->name = scm__rc.d1786[1199];/* (make-glob-fs-fold ensure-dirname) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[48]))->debugInfo = scm__rc.d1786[1201];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1830]))[6] = SCM_WORD(scm__rc.d1786[1195]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1830]))[19] = SCM_WORD(scm__rc.d1786[1197]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1830]))[25] = SCM_WORD(scm__rc.d1786[1197]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1830]))[35] = SCM_WORD(scm__rc.d1786[1129]);
  scm__rc.d1786[1202] = Scm_MakeIdentifier(scm__rc.d1786[618], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#file-exists? */
  scm__rc.d1786[1203] = Scm_MakeIdentifier(scm__rc.d1786[630], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#file-is-directory? */
  scm__rc.d1786[1204] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[527])),FALSE); /* loop1920 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1162]), scm__rc.d1786[1204]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1163]), scm__rc.d1786[1191]);
  scm__rc.d1786[1205] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[528])),TRUE); /* child */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1164]), scm__rc.d1786[1205]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1167]), scm__rc.d1786[2]);
  scm__rc.d1786[1206] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1163]), SCM_OBJ(&scm__rc.d1787[1164]), SCM_OBJ(&scm__rc.d1787[1168]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1169]), scm__rc.d1786[1206]);
  scm__rc.d1786[1207] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[49])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[49]))->debugInfo = scm__rc.d1786[1207];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1869]))[12] = SCM_WORD(scm__rc.d1786[1129]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1869]))[24] = SCM_WORD(scm__rc.d1786[1203]);
  scm__rc.d1786[1208] = Scm_MakeIdentifier(scm__rc.d1786[0], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#sys-readdir */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1170]), scm__rc.d1786[1204]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1171]), scm__rc.d1786[1191]);
  scm__rc.d1786[1209] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[529])),TRUE); /* regexp */
  scm__rc.d1786[1210] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[530])),TRUE); /* non-leaf? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1172]), scm__rc.d1786[1210]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1173]), scm__rc.d1786[1209]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1174]), scm__rc.d1786[1096]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1175]), scm__rc.d1786[505]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1176]), scm__rc.d1786[357]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1179]), scm__rc.d1786[2]);
  scm__rc.d1786[1211] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1171]), SCM_OBJ(&scm__rc.d1787[1176]), SCM_OBJ(&scm__rc.d1787[1180]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1181]), scm__rc.d1786[1211]);
  scm__rc.d1786[1212] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[50])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[50]))->name = scm__rc.d1786[1204];/* (make-glob-fs-fold loop1920) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[50]))->debugInfo = scm__rc.d1786[1212];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]))[30] = SCM_WORD(scm__rc.d1786[1129]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]))[34] = SCM_WORD(scm__rc.d1786[1115]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]))[45] = SCM_WORD(scm__rc.d1786[1189]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]))[55] = SCM_WORD(scm__rc.d1786[1129]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]))[61] = SCM_WORD(scm__rc.d1786[1202]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]))[71] = SCM_WORD(scm__rc.d1786[1203]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]))[118] = SCM_WORD(scm__rc.d1786[1208]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[1910]))[120] = SCM_WORD(scm__rc.d1786[1081]);
  scm__rc.d1786[1213] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[531]))); /* :root-path */
  scm__rc.d1786[1214] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[532]))); /* :current-path */
  scm__rc.d1786[1215] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[531])),TRUE); /* root-path */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1182]), scm__rc.d1786[1215]);
  scm__rc.d1786[1216] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[532])),TRUE); /* current-path */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1183]), scm__rc.d1786[1216]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1186]), scm__rc.d1786[79]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1189]), scm__rc.d1786[2]);
  scm__rc.d1786[1217] = Scm_MakeExtendedPair(scm__rc.d1786[1191], SCM_OBJ(&scm__rc.d1787[1186]), SCM_OBJ(&scm__rc.d1787[1190]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1191]), scm__rc.d1786[1217]);
  scm__rc.d1786[1218] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[51])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[51]))->name = scm__rc.d1786[1191];/* make-glob-fs-fold */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[51]))->debugInfo = scm__rc.d1786[1218];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]))[12] = SCM_WORD(scm__rc.d1786[31]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]))[24] = SCM_WORD(scm__rc.d1786[31]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]))[37] = SCM_WORD(scm__rc.d1786[1192]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]))[40] = SCM_WORD(scm__rc.d1786[1194]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]))[75] = SCM_WORD(scm__rc.d1786[71]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]))[81] = SCM_WORD(scm__rc.d1786[72]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]))[85] = SCM_WORD(scm__rc.d1786[1213]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]))[97] = SCM_WORD(scm__rc.d1786[1214]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2032]))[113] = SCM_WORD(scm__rc.d1786[77]);
  scm__rc.d1786[1219] = Scm_MakeIdentifier(scm__rc.d1786[1191], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#make-glob-fs-fold */
  scm__rc.d1786[1220] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[52])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[52]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[52]))->debugInfo = scm__rc.d1786[1220];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2154]))[3] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2154]))[6] = SCM_WORD(scm__rc.d1786[1191]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2154]))[13] = SCM_WORD(scm__rc.d1786[1219]);
  scm__rc.d1786[1221] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[53])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[53]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[53]))->debugInfo = scm__rc.d1786[1221];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2169]))[3] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2169]))[6] = SCM_WORD(scm__rc.d1786[1068]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2169]))[10] = SCM_WORD(scm__rc.d1786[1219]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2169]))[13] = SCM_WORD(scm__rc.d1786[1067]);
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1926[0])),TRUE); /* handle-type */
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1926[1])),TRUE); /* process */
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)

#line 1686 "libsys.scm"
WinHandleClass=(Scm_MakeForeignPointerClass(
Scm_CurrentModule(),"<win:handle>",handle_print,handle_cleanup,SCM_FOREIGN_POINTER_KEEP_IDENTITY));
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1926[2])),TRUE); /* sys-win-process? */
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1926[3])),TRUE); /* obj */
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[1]), scm__rc.d1925[3]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1926[4])),TRUE); /* source-info */
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[4]), scm__rc.d1925[4]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[5] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1926[6])),TRUE); /* bind-info */
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1926[7])),TRUE); /* gauche */
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[5]), scm__rc.d1925[2]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[6]), scm__rc.d1925[6]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[7]), scm__rc.d1925[5]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[7] = Scm_MakeExtendedPair(scm__rc.d1925[2], SCM_OBJ(&scm__rc.d1927[1]), SCM_OBJ(&scm__rc.d1927[9]));
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[8] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1926[8])),TRUE); /* <top> */
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[9] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1926[9])),TRUE); /* -> */
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[10] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1926[10])),TRUE); /* <boolean> */
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1925[11]))[3] = scm__rc.d1925[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1925[11]))[4] = scm__rc.d1925[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1925[11]))[5] = scm__rc.d1925[9];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1925[11]))[6] = scm__rc.d1925[10];
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-win-process?")), SCM_OBJ(&libsyssys_win_processP__STUB), 0);
  libsyssys_win_processP__STUB.common.info = scm__rc.d1925[7];
  libsyssys_win_processP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1925[11]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[18] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1926[11])),TRUE); /* sys-win-process-pid */
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[12]), scm__rc.d1925[4]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[13]), scm__rc.d1925[18]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[14]), scm__rc.d1925[6]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[15]), scm__rc.d1925[5]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[19] = Scm_MakeExtendedPair(scm__rc.d1925[18], SCM_OBJ(&scm__rc.d1927[1]), SCM_OBJ(&scm__rc.d1927[17]));
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[20] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1926[12])),TRUE); /* <int> */
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1925[21]))[3] = scm__rc.d1925[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1925[21]))[4] = scm__rc.d1925[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1925[21]))[5] = scm__rc.d1925[9];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1925[21]))[6] = scm__rc.d1925[20];
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-win-process-pid")), SCM_OBJ(&libsyssys_win_process_pid__STUB), 0);
  libsyssys_win_process_pid__STUB.common.info = scm__rc.d1925[19];
  libsyssys_win_process_pid__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1925[21]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[28] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1926[13])),TRUE); /* sys-get-osfhandle */
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[29] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1926[14])),TRUE); /* port-or-fd */
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[18]), scm__rc.d1925[29]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[21]), scm__rc.d1925[4]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[22]), scm__rc.d1925[28]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[23]), scm__rc.d1925[6]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[24]), scm__rc.d1925[5]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[30] = Scm_MakeExtendedPair(scm__rc.d1925[28], SCM_OBJ(&scm__rc.d1927[18]), SCM_OBJ(&scm__rc.d1927[26]));
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  ((ScmObj*)SCM_OBJ(&scm__rc.d1925[31]))[3] = scm__rc.d1925[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1925[31]))[4] = scm__rc.d1925[8];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1925[31]))[5] = scm__rc.d1925[9];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1925[31]))[6] = scm__rc.d1925[8];
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-get-osfhandle")), SCM_OBJ(&libsyssys_get_osfhandle__STUB), 0);
  libsyssys_get_osfhandle__STUB.common.info = scm__rc.d1925[30];
  libsyssys_get_osfhandle__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1925[31]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[38] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1926[15])),TRUE); /* sys-win-pipe-name */
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[29]), scm__rc.d1925[4]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[30]), scm__rc.d1925[38]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[31]), scm__rc.d1925[6]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1927[32]), scm__rc.d1925[5]);
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  scm__rc.d1925[39] = Scm_MakeExtendedPair(scm__rc.d1925[38], SCM_OBJ(&scm__rc.d1927[18]), SCM_OBJ(&scm__rc.d1927[34]));
#endif /* defined(GAUCHE_WINDOWS) */
#if defined(GAUCHE_WINDOWS)
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("sys-win-pipe-name")), SCM_OBJ(&libsyssys_win_pipe_name__STUB), 0);
  libsyssys_win_pipe_name__STUB.common.info = scm__rc.d1925[39];
  libsyssys_win_pipe_name__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1925[31]);
#endif /* defined(GAUCHE_WINDOWS) */
  scm__rc.d1786[1222] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[533])),TRUE); /* %sys-mintty? */
  scm__rc.d1786[1223] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[534])),TRUE); /* sys-win-pipe-name */
  scm__rc.d1786[1225] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[535])),TRUE); /* module-binds? */
  scm__rc.d1786[1224] = Scm_MakeIdentifier(scm__rc.d1786[1225], SCM_MODULE(Scm_GaucheInternalModule()), SCM_NIL); /* gauche.internal#module-binds? */
  scm__rc.d1786[1226] = Scm_MakeIdentifier(scm__rc.d1786[1223], SCM_MODULE(Scm_GaucheInternalModule()), SCM_NIL); /* gauche.internal#sys-win-pipe-name */
  scm__rc.d1786[1227] = Scm_RegComp(SCM_STRING(SCM_OBJ(&scm__sc.d1785[536])), 0);
  scm__rc.d1786[1229] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[537])),TRUE); /* boolean */
  scm__rc.d1786[1228] = Scm_MakeIdentifier(scm__rc.d1786[1229], SCM_MODULE(Scm_GaucheInternalModule()), SCM_NIL); /* gauche.internal#boolean */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1194]), scm__rc.d1786[2]);
  scm__rc.d1786[1230] = Scm_MakeExtendedPair(scm__rc.d1786[1222], SCM_OBJ(&scm__rc.d1787[473]), SCM_OBJ(&scm__rc.d1787[1195]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1196]), scm__rc.d1786[1230]);
  scm__rc.d1786[1231] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[54])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[54]))->name = scm__rc.d1786[1222];/* %sys-mintty? */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[54]))->debugInfo = scm__rc.d1786[1231];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2184]))[3] = SCM_WORD(scm__rc.d1786[4]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2184]))[5] = SCM_WORD(scm__rc.d1786[1223]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2184]))[7] = SCM_WORD(scm__rc.d1786[1224]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2184]))[13] = SCM_WORD(scm__rc.d1786[1226]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2184]))[21] = SCM_WORD(scm__rc.d1786[1227]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2184]))[24] = SCM_WORD(scm__rc.d1786[1228]);
  scm__rc.d1786[1232] = Scm_MakeIdentifier(scm__rc.d1786[1222], SCM_MODULE(Scm_GaucheInternalModule()), SCM_NIL); /* gauche.internal#%sys-mintty? */
  scm__rc.d1786[1233] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[55])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[55]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[55]))->debugInfo = scm__rc.d1786[1233];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2210]))[3] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2210]))[6] = SCM_WORD(scm__rc.d1786[1222]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2210]))[13] = SCM_WORD(scm__rc.d1786[1232]);
  scm__rc.d1786[1235] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[538])),TRUE); /* make-string */
  scm__rc.d1786[1234] = Scm_MakeIdentifier(scm__rc.d1786[1235], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#make-string */
  scm__rc.d1786[1236] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[540])),TRUE); /* %sys-escape-windows-command-line */
  scm__rc.d1786[1237] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[541])),FALSE); /* G1928 */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1197]), scm__rc.d1786[1237]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1198]), scm__rc.d1786[1236]);
  scm__rc.d1786[1238] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[542])),TRUE); /* m */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1199]), scm__rc.d1786[1238]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1202]), scm__rc.d1786[2]);
  scm__rc.d1786[1239] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[1198]), SCM_OBJ(&scm__rc.d1787[1199]), SCM_OBJ(&scm__rc.d1787[1203]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1204]), scm__rc.d1786[1239]);
  scm__rc.d1786[1240] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[56])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[56]))->debugInfo = scm__rc.d1786[1240];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2225]))[11] = SCM_WORD(scm__rc.d1786[1195]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2225]))[18] = SCM_WORD(scm__rc.d1786[1234]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2225]))[23] = SCM_WORD(scm__rc.d1786[1129]);
  scm__rc.d1786[1241] = Scm_RegComp(SCM_STRING(SCM_OBJ(&scm__sc.d1785[544])), 0);
  scm__rc.d1786[1242] = Scm_MakeIdentifier(scm__rc.d1786[78], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#errorf */
  scm__rc.d1786[1243] = Scm_RegComp(SCM_STRING(SCM_OBJ(&scm__sc.d1785[546])), 0);
  scm__rc.d1786[1244] = Scm_RegComp(SCM_STRING(SCM_OBJ(&scm__sc.d1785[547])), 0);
  scm__rc.d1786[1246] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[548])),TRUE); /* regexp-replace-all */
  scm__rc.d1786[1245] = Scm_MakeIdentifier(scm__rc.d1786[1246], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#regexp-replace-all */
  scm__rc.d1786[1248] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[549])),TRUE); /* write-to-string */
  scm__rc.d1786[1247] = Scm_MakeIdentifier(scm__rc.d1786[1248], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#write-to-string */
  scm__rc.d1786[1249] = Scm_MakeIdentifier(scm__rc.d1786[1236], SCM_MODULE(scm__rc.d1786[419]), SCM_NIL); /* gauche#%sys-escape-windows-command-line */
  scm__rc.d1786[1250] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[550])),TRUE); /* batfilep */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1206]), scm__rc.d1786[1250]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1207]), scm__rc.d1786[1200]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1210]), scm__rc.d1786[2]);
  scm__rc.d1786[1251] = Scm_MakeExtendedPair(scm__rc.d1786[1236], SCM_OBJ(&scm__rc.d1787[1207]), SCM_OBJ(&scm__rc.d1787[1211]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1212]), scm__rc.d1786[1251]);
  scm__rc.d1786[1252] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[57])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[57]))->name = scm__rc.d1786[1236];/* %sys-escape-windows-command-line */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[57]))->debugInfo = scm__rc.d1786[1252];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]))[10] = SCM_WORD(scm__rc.d1786[1114]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]))[22] = SCM_WORD(scm__rc.d1786[1241]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]))[32] = SCM_WORD(scm__rc.d1786[1242]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]))[41] = SCM_WORD(scm__rc.d1786[528]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]))[50] = SCM_WORD(scm__rc.d1786[1129]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]))[57] = SCM_WORD(scm__rc.d1786[1243]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]))[66] = SCM_WORD(scm__rc.d1786[1244]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]))[70] = SCM_WORD(scm__rc.d1786[1245]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]))[75] = SCM_WORD(scm__rc.d1786[1129]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]))[82] = SCM_WORD(scm__rc.d1786[1247]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2250]))[84] = SCM_WORD(scm__rc.d1786[1249]);
  scm__rc.d1786[1253] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1796[58])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[58]))->name = scm__rc.d1786[86];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1795[58]))->debugInfo = scm__rc.d1786[1253];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2336]))[5] = SCM_WORD(scm__rc.d1786[28]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2336]))[8] = SCM_WORD(scm__rc.d1786[1236]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1794[2336]))[15] = SCM_WORD(scm__rc.d1786[1249]);
  Scm_VMExecuteToplevels(toplevels);
  scm__rc.d1786[1568] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[551])),FALSE); /* loop1790 */
  scm__rc.d1786[1569] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[552])),FALSE); /* args1789 */
  scm__rc.d1786[1570] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[553])),FALSE); /* G1791 */
  scm__rc.d1786[1571] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[554])),FALSE); /* G1792 */
  scm__rc.d1786[1572] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[555])),FALSE); /* G1793 */
  scm__rc.d1786[1573] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[556])),TRUE); /* tmp */
  scm__rc.d1786[1574] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[557])),TRUE); /* case */
  scm__rc.d1786[1575] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[558])),TRUE); /* windows? */
  scm__rc.d1786[1576] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[559])),TRUE); /* comps */
  scm__rc.d1786[1577] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[560])),TRUE); /* r */
  scm__rc.d1786[1578] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[561])),TRUE); /* root? */
  scm__rc.d1786[1579] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[562])),TRUE); /* reverse */
  scm__rc.d1786[1580] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[563])),TRUE); /* canon-path */
  scm__rc.d1786[1581] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[564])),TRUE); /* abs-path */
  scm__rc.d1786[1582] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[565])),TRUE); /* expand-tilde */
  scm__rc.d1786[1583] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[566])),TRUE); /* absolute? */
  scm__rc.d1786[1584] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[567])),TRUE); /* home */
  scm__rc.d1786[1585] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[568])),TRUE); /* pw */
  scm__rc.d1786[1586] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[569])),TRUE); /* get-unix-home */
  scm__rc.d1786[1587] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[570])),TRUE); /* get-windows-home */
  scm__rc.d1786[1588] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[571])),TRUE); /* user */
  scm__rc.d1786[1589] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[572])),TRUE); /* let* */
  scm__rc.d1786[1590] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[573])),TRUE); /* assq */
  scm__rc.d1786[1591] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[574])),FALSE); /* rest1788 */
  scm__rc.d1786[1592] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[575])),TRUE); /* if-let1 */
  scm__rc.d1786[1593] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[576])),TRUE); /* $ */
  scm__rc.d1786[1594] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[577])),TRUE); /* define-in-module */
  scm__rc.d1786[1595] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[578])),FALSE); /* G1800 */
  scm__rc.d1786[1596] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[579])),FALSE); /* G1801 */
  scm__rc.d1786[1597] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[580])),FALSE); /* rest1799 */
  scm__rc.d1786[1598] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[581])),TRUE); /* string? */
  scm__rc.d1786[1599] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[582])),TRUE); /* check-arg */
  scm__rc.d1786[1600] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[583])),TRUE); /* v */
  scm__rc.d1786[1601] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[584])),TRUE); /* assume-type */
  scm__rc.d1786[1602] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[585])),TRUE); /* zero? */
  scm__rc.d1786[1603] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[586])),TRUE); /* < */
  scm__rc.d1786[1604] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[587])),FALSE); /* loop1896 */
  scm__rc.d1786[1605] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[588])),FALSE); /* args1895 */
  scm__rc.d1786[1606] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[589])),FALSE); /* G1897 */
  scm__rc.d1786[1607] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[590])),FALSE); /* G1898 */
  scm__rc.d1786[1608] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[591])),FALSE); /* G1899 */
  scm__rc.d1786[1609] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[592])),FALSE); /* G1900 */
  scm__rc.d1786[1610] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[593])),TRUE); /* cut */
  scm__rc.d1786[1611] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[594])),TRUE); /* <> */
  scm__rc.d1786[1612] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[595])),FALSE); /* rest1894 */
  scm__rc.d1786[1613] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[596])),TRUE); /* p */
  scm__rc.d1786[1614] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[597])),FALSE); /* G1905 */
  scm__rc.d1786[1615] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[598])),TRUE); /* append */
  scm__rc.d1786[1616] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[599])),TRUE); /* segs */
  scm__rc.d1786[1617] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[600])),TRUE); /* ins */
  scm__rc.d1786[1618] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[601])),FALSE); /* G1906 */
  scm__rc.d1786[1619] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[602])),FALSE); /* G1908 */
  scm__rc.d1786[1620] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[603])),FALSE); /* G1909 */
  scm__rc.d1786[1621] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[604])),FALSE); /* G1911 */
  scm__rc.d1786[1622] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[605])),FALSE); /* G1912 */
  scm__rc.d1786[1623] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[606])),FALSE); /* G1914 */
  scm__rc.d1786[1624] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[607])),TRUE); /* values */
  scm__rc.d1786[1625] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[608])),TRUE); /* cute */
  scm__rc.d1786[1626] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[609])),TRUE); /* letrec */
  scm__rc.d1786[1627] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[610])),TRUE); /* = */
  scm__rc.d1786[1628] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[611])),FALSE); /* G1916 */
  scm__rc.d1786[1629] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[612])),TRUE); /* => */
  scm__rc.d1786[1630] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[613])),TRUE); /* ^m */
  scm__rc.d1786[1631] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[614])),TRUE); /* values-ref */
  scm__rc.d1786[1632] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[615])),FALSE); /* G1915 */
  scm__rc.d1786[1633] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[616])),TRUE); /* n */
  scm__rc.d1786[1634] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[617])),TRUE); /* cs */
  scm__rc.d1786[1635] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[618])),TRUE); /* current-input-port */
  scm__rc.d1786[1636] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[619])),TRUE); /* peek-char */
  scm__rc.d1786[1637] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[620])),TRUE); /* eqv? */
  scm__rc.d1786[1638] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[621])),TRUE); /* unquote-splicing */
  scm__rc.d1786[1639] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[622])),TRUE); /* element1* */
  scm__rc.d1786[1640] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[623])),TRUE); /* eof-object? */
  scm__rc.d1786[1641] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[624])),TRUE); /* quasiquote */
  scm__rc.d1786[1642] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[625])),TRUE); /* unquote */
  scm__rc.d1786[1643] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[626])),TRUE); /* ra */
  scm__rc.d1786[1644] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[627])),TRUE); /* element0 */
  scm__rc.d1786[1645] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[628])),TRUE); /* nd */
  scm__rc.d1786[1646] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[629])),TRUE); /* element0* */
  scm__rc.d1786[1647] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[630])),TRUE); /* read-char */
  scm__rc.d1786[1648] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[631])),TRUE); /* char? */
  scm__rc.d1786[1649] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[632])),TRUE); /* x */
  scm__rc.d1786[1650] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[633])),TRUE); /* ast */
  scm__rc.d1786[1651] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[634])),TRUE); /* caddr */
  scm__rc.d1786[1652] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[635])),TRUE); /* > */
  scm__rc.d1786[1653] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[636])),TRUE); /* cdddr */
  scm__rc.d1786[1654] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[637])),TRUE); /* separ */
  scm__rc.d1786[1655] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[638])),TRUE); /* and-let* */
  scm__rc.d1786[1656] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[639])),TRUE); /* len */
  scm__rc.d1786[1657] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[640])),TRUE); /* - */
  scm__rc.d1786[1658] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[641])),TRUE); /* full */
  scm__rc.d1786[1659] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[642])),TRUE); /* root-path/ */
  scm__rc.d1786[1660] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[643])),TRUE); /* current-path/ */
  scm__rc.d1786[1661] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[644])),TRUE); /* ^s */
  scm__rc.d1786[1662] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[645])),FALSE); /* args1919 */
  scm__rc.d1786[1663] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[646])),FALSE); /* G1921 */
  scm__rc.d1786[1664] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[647])),FALSE); /* G1922 */
  scm__rc.d1786[1665] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[648])),FALSE); /* rest1918 */
}
