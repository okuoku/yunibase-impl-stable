/* Generated automatically from liblazy.scm.  DO NOT EDIT */
#define LIBGAUCHE_BODY 
#include <gauche.h>
#include <gauche/code.h>
#include "gauche/priv/configP.h"
#include "gauche/priv/promiseP.h"
static ScmObj liblazyforce(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(liblazyforce__STUB, 1, 0,SCM_FALSE,liblazyforce, NULL, NULL);

static ScmObj liblazypromiseP(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(liblazypromiseP__STUB, 1, 0,1, SCM_FALSE,0, liblazypromiseP, NULL, NULL);

static ScmObj liblazyeager(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(liblazyeager__STUB, 0, 1,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, liblazyeager, NULL, NULL);

static ScmObj liblazypromise_kind_SETTER(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(liblazypromise_kind_SETTER__STUB, 2, 0,SCM_FALSE,liblazypromise_kind_SETTER, NULL, NULL);

static ScmObj liblazypromise_kind(ScmObj*, int, void*);
static SCM_DEFINE_SUBRX(liblazypromise_kind__STUB, 1, 0,0, SCM_FALSE,SCM_SUBR_IMMEDIATE_ARG, liblazypromise_kind, NULL, NULL);

static unsigned char uvector__00001[] = {
 0u, 3u, 134u, 6u, 6u, 104u, 2u, 1u, 36u, 112u, 144u, 192u, 144u, 9u,
28u, 28u, 48u, 38u, 3u, 132u, 112u, 32u, 192u, 152u, 14u, 17u, 192u,
3u, 2u, 96u, 56u, 72u,};
static ScmObj liblazy_25lcons(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(liblazy_25lcons__STUB, 2, 2,SCM_FALSE,liblazy_25lcons, NULL, NULL);

static ScmObj liblazygenerator_TOlseq(ScmObj*, int, void*);
static SCM_DEFINE_SUBR(liblazygenerator_TOlseq__STUB, 1, 1,SCM_FALSE,liblazygenerator_TOlseq, NULL, NULL);

static unsigned char uvector__00002[] = {
 0u, 3u, 128u, 6u, 4u, 128u, 136u, 98u, 12u, 12u, 107u, 1u, 18u, 64u,};
static unsigned char uvector__00003[] = {
 0u, 3u, 131u, 6u, 4u, 128u, 136u, 224u, 129u, 130u, 3u, 128u, 160u,
66u, 71u, 3u, 12u, 9u, 1u, 17u, 192u, 131u, 2u, 64u, 68u, 112u, 16u,
192u, 233u, 1u, 64u, 164u, 142u, 0u, 24u, 18u, 2u, 33u, 136u, 48u,
49u, 168u, 129u, 160u, 40u, 20u, 134u, 80u, 152u, 35u, 64u, 66u, 7u,
36u, 146u,};
static unsigned char uvector__00004[] = {
 0u, 3u, 131u, 6u, 4u, 128u, 136u, 224u, 129u, 130u, 8u, 128u, 160u,
66u, 71u, 3u, 12u, 9u, 1u, 17u, 192u, 131u, 2u, 64u, 68u, 112u, 16u,
192u, 233u, 1u, 64u, 164u, 142u, 0u, 24u, 18u, 2u, 33u, 136u, 48u,
49u, 168u, 129u, 160u, 40u, 20u, 134u, 80u, 152u, 35u, 64u, 66u, 7u,
36u, 146u,};
static unsigned char uvector__00005[] = {
 0u, 3u, 135u, 6u, 4u, 130u, 72u, 225u, 129u, 130u, 3u, 130u, 96u,
66u, 71u, 11u, 12u, 9u, 2u, 17u, 194u, 131u, 3u, 32u, 16u, 72u, 116u,
128u, 132u, 21u, 5u, 192u, 164u, 146u, 25u, 66u, 96u, 141u, 4u, 136u,
28u, 146u, 71u, 9u, 12u, 9u, 134u, 65u, 28u, 32u, 48u, 38u, 27u, 164u,
112u, 112u, 192u, 144u, 41u, 28u, 20u, 48u, 36u, 22u, 71u, 3u, 12u,
9u, 1u, 17u, 192u, 131u, 2u, 65u, 100u, 112u, 16u, 192u, 233u, 5u,
240u, 18u, 71u, 0u, 12u, 9u, 5u, 144u, 196u, 24u, 24u, 212u, 64u,
208u, 89u, 13u, 176u, 72u, 76u, 50u, 2u, 98u, 20u, 73u, 32u,};
static unsigned char uvector__00006[] = {
 0u, 3u, 135u, 6u, 4u, 130u, 72u, 225u, 129u, 130u, 8u, 130u, 96u,
66u, 71u, 11u, 12u, 9u, 2u, 17u, 194u, 131u, 3u, 32u, 16u, 72u, 116u,
128u, 132u, 21u, 5u, 192u, 164u, 146u, 25u, 66u, 96u, 141u, 4u, 136u,
28u, 146u, 71u, 9u, 12u, 9u, 134u, 65u, 28u, 32u, 48u, 38u, 27u, 164u,
112u, 112u, 192u, 144u, 41u, 28u, 20u, 48u, 36u, 22u, 71u, 3u, 12u,
9u, 1u, 17u, 192u, 131u, 2u, 65u, 100u, 112u, 16u, 192u, 233u, 5u,
240u, 18u, 71u, 0u, 12u, 9u, 5u, 144u, 196u, 24u, 24u, 212u, 64u,
208u, 89u, 13u, 176u, 72u, 76u, 50u, 2u, 98u, 20u, 73u, 32u,};
static unsigned char uvector__00007[] = {
 0u, 3u, 190u, 134u, 8u, 48u, 65u, 176u, 17u, 13u, 176u, 95u, 0u, 12u,
162u, 8u, 129u, 120u, 1u, 14u, 74u, 32u, 104u, 44u, 134u, 216u, 36u,
58u, 64u, 66u, 10u, 130u, 224u, 82u, 67u, 40u, 128u, 224u, 152u, 16u,
193u, 34u, 7u, 36u, 144u, 228u, 162u, 6u, 130u, 200u, 109u, 130u, 67u,
164u, 4u, 32u, 168u, 46u, 5u, 36u, 50u, 136u, 34u, 9u, 129u, 12u, 18u,
32u, 114u, 73u, 36u, 145u, 222u, 131u, 3u, 26u, 132u, 197u, 184u, 38u,
49u, 4u, 142u, 238u, 24u, 24u, 212u, 38u, 20u, 65u, 48u, 188u, 36u,
119u, 80u, 192u, 152u, 55u, 17u, 220u, 195u, 2u, 65u, 100u, 119u, 32u,
192u, 200u, 4u, 23u, 192u, 9u, 9u, 131u, 25u, 35u, 183u, 134u, 4u,
205u, 60u, 142u, 218u, 24u, 19u, 2u, 146u, 59u, 96u, 96u, 72u, 8u,
142u, 212u, 24u, 19u, 2u, 146u, 59u, 56u, 96u, 131u, 32u, 32u, 202u,
32u, 136u, 23u, 128u, 16u, 228u, 162u, 6u, 128u, 160u, 82u, 25u, 68u,
7u, 1u, 64u, 134u, 2u, 16u, 57u, 36u, 57u, 40u, 129u, 160u, 40u, 20u,
134u, 81u, 4u, 64u, 80u, 33u, 128u, 132u, 14u, 73u, 36u, 142u, 202u,
24u, 24u, 212u, 38u, 133u, 44u, 4u, 208u, 169u, 194u, 71u, 97u, 12u,
12u, 106u, 19u, 66u, 85u, 130u, 104u, 76u, 209u, 35u, 175u, 134u, 4u,
208u, 144u, 34u, 58u, 224u, 96u, 72u, 8u, 142u, 176u, 24u, 32u, 232u,
20u, 145u, 213u, 195u, 2u, 64u, 164u, 117u, 80u, 192u, 154u, 27u,
148u, 71u, 83u, 12u, 1u, 192u, 65u, 208u, 17u, 9u, 161u, 185u, 68u,
38u, 132u, 96u, 18u, 58u, 136u, 96u, 77u, 15u, 9u, 35u, 168u, 6u, 4u,
128u, 136u, 233u, 193u, 129u, 52u, 60u, 36u, 142u, 150u, 24u, 32u,
192u, 228u, 176u, 17u, 36u, 116u, 144u, 192u, 198u, 176u, 17u, 35u,
163u, 134u, 8u, 62u, 5u, 224u, 4u, 142u, 140u, 24u, 18u, 5u, 35u,
161u, 6u, 0u, 236u, 28u, 4u, 17u, 2u, 240u, 2u, 33u, 8u, 10u, 4u, 36u,
56u, 8u, 14u, 5u, 224u, 4u, 66u, 48u, 20u, 8u, 73u, 12u, 42u, 73u,
28u, 244u, 48u, 38u, 138u, 0u, 145u, 207u, 3u, 2u, 64u, 132u, 115u,
160u, 192u, 144u, 17u, 28u, 224u, 48u, 38u, 137u, 206u, 17u, 204u,
195u, 2u, 104u, 154u, 81u, 28u, 200u, 48u, 36u, 4u, 71u, 48u, 12u, 9u,
162u, 96u, 164u, 114u, 208u, 192u, 207u, 9u, 6u, 16u, 198u, 32u, 40u,
76u, 64u, 144u, 161u, 16u, 47u, 1u, 36u, 54u, 4u, 209u, 44u, 192u,
19u, 68u, 129u, 2u, 104u, 136u, 81u, 9u, 161u, 219u, 97u, 164u, 38u,
3u, 132u, 146u, 72u, 228u, 225u, 129u, 52u, 93u, 28u, 142u, 70u, 24u,
26u, 225u, 82u, 71u, 34u, 12u, 9u, 10u, 145u, 200u, 67u, 3u, 32u, 16u,
40u, 101u, 13u, 112u, 177u, 224u, 33u, 162u, 22u, 36u, 136u, 84u, 50u,
134u, 184u, 88u, 134u, 21u, 33u, 170u, 22u, 36u, 144u, 222u, 19u, 70u,
134u, 130u, 104u, 186u, 57u, 12u, 138u, 154u, 48u, 60u, 72u, 228u, 1u,
129u, 52u, 112u, 120u, 142u, 54u, 24u, 19u, 70u, 245u, 8u, 227u, 65u,
129u, 33u, 98u, 56u, 192u, 96u, 77u, 27u, 94u, 35u, 137u, 134u, 4u,
209u, 177u, 178u, 56u, 144u, 96u, 72u, 88u, 142u, 34u, 24u, 25u, 0u,
129u, 3u, 40u, 107u, 133u, 204u, 40u, 26u, 33u, 114u, 72u, 133u, 131u,
40u, 107u, 133u, 200u, 97u, 82u, 26u, 161u, 114u, 73u, 9u, 163u, 90u,
164u, 142u, 32u, 24u, 19u, 72u, 68u, 136u, 225u, 97u, 129u, 52u, 131u,
32u, 142u, 20u, 24u, 18u, 24u, 35u, 132u, 6u, 4u, 210u, 5u, 34u, 56u,
16u, 96u, 77u, 32u, 16u, 35u, 128u, 134u, 4u, 134u, 8u, 224u, 1u,
129u, 32u, 34u, 24u, 131u, 2u, 104u, 188u, 57u, 32u,};
static unsigned char uvector__00008[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 12u, 192u, 80u, 152u, 129u, 33u, 66u, 32u,
94u, 2u, 72u, 108u, 1u, 216u, 56u, 8u, 34u, 5u, 224u, 4u, 66u, 16u,
20u, 8u, 72u, 112u, 16u, 28u, 11u, 192u, 8u, 132u, 96u, 40u, 16u,
146u, 24u, 84u, 144u, 32u, 248u, 23u, 128u, 17u, 6u, 7u, 37u, 128u,
137u, 32u, 56u, 8u, 58u, 2u, 34u, 14u, 129u, 73u, 16u, 100u, 4u, 25u,
68u, 17u, 2u, 240u, 2u, 28u, 148u, 64u, 208u, 20u, 10u, 67u, 40u,
128u, 224u, 40u, 16u, 192u, 66u, 7u, 36u, 135u, 37u, 16u, 52u, 5u, 2u,
144u, 202u, 32u, 136u, 10u, 4u, 48u, 16u, 129u, 201u, 36u, 144u, 210u,
32u, 193u, 6u, 192u, 68u, 54u, 193u, 124u, 0u, 50u, 136u, 34u, 5u,
224u, 4u, 57u, 40u, 129u, 160u, 178u, 27u, 96u, 144u, 233u, 1u, 8u,
42u, 11u, 129u, 73u, 12u, 162u, 3u, 130u, 96u, 67u, 4u, 136u, 28u,
146u, 67u, 146u, 136u, 26u, 11u, 33u, 182u, 9u, 14u, 144u, 16u, 130u,
160u, 184u, 20u, 144u, 202u, 32u, 136u, 38u, 4u, 48u, 72u, 129u, 201u,
36u, 146u, 73u, 35u, 132u, 134u, 6u, 48u, 152u, 24u, 166u, 15u, 164u,
112u, 112u, 192u, 152u, 14u, 17u, 192u, 131u, 2u, 96u, 56u, 71u, 0u,
12u, 9u, 128u, 225u, 32u,};
static unsigned char uvector__00009[] = {
 0u, 3u, 130u, 134u, 4u, 134u, 136u, 224u, 129u, 129u, 32u, 34u, 56u,
24u, 96u, 116u, 128u, 160u, 82u, 71u, 2u, 12u, 9u, 1u, 17u, 192u, 67u,
3u, 32u, 16u, 212u, 4u, 72u, 129u, 160u, 40u, 20u, 195u, 68u, 142u,
0u, 24u, 18u, 2u, 33u, 136u, 48u, 49u, 168u, 134u, 225u, 168u, 8u,
38u, 32u, 4u, 146u,};
static unsigned char uvector__00010[] = {
 0u, 3u, 135u, 134u, 4u, 134u, 136u, 225u, 193u, 129u, 33u, 194u, 56u,
104u, 96u, 135u, 97u, 206u, 2u, 72u, 225u, 129u, 129u, 33u, 194u, 56u,
88u, 96u, 72u, 8u, 142u, 20u, 24u, 29u, 32u, 40u, 20u, 145u, 194u,
67u, 2u, 64u, 68u, 112u, 128u, 192u, 200u, 4u, 53u, 1u, 18u, 32u,
104u, 10u, 5u, 34u, 30u, 135u, 12u, 52u, 72u, 224u, 225u, 129u, 32u,
34u, 56u, 24u, 96u, 132u, 97u, 206u, 0u, 72u, 224u, 1u, 129u, 33u,
194u, 24u, 131u, 3u, 26u, 134u, 80u, 153u, 16u, 8u, 28u, 136u, 110u,
26u, 128u, 130u, 99u, 36u, 19u, 27u, 98u, 73u, 32u,};
static unsigned char uvector__00011[] = {
 0u, 3u, 133u, 134u, 4u, 134u, 136u, 225u, 65u, 129u, 33u, 242u, 56u,
72u, 96u, 116u, 135u, 248u, 9u, 35u, 132u, 6u, 4u, 135u, 200u, 224u,
225u, 129u, 144u, 8u, 104u, 58u, 64u, 66u, 10u, 135u, 224u, 82u, 73u,
16u, 52u, 62u, 97u, 162u, 71u, 6u, 12u, 9u, 135u, 137u, 28u, 20u, 48u,
38u, 32u, 196u, 112u, 64u, 192u, 144u, 41u, 28u, 8u, 48u, 36u, 62u,
71u, 0u, 12u, 9u, 1u, 16u, 196u, 24u, 24u, 212u, 67u, 112u, 208u, 76u,
60u, 66u, 98u, 102u, 73u, 32u,};
static unsigned char uvector__00012[] = {
 0u, 3u, 138u, 134u, 4u, 134u, 136u, 226u, 129u, 129u, 33u, 194u, 56u,
152u, 96u, 135u, 97u, 206u, 2u, 72u, 226u, 65u, 129u, 33u, 194u, 56u,
136u, 96u, 72u, 124u, 142u, 32u, 24u, 29u, 33u, 254u, 2u, 72u, 225u,
225u, 129u, 33u, 242u, 56u, 112u, 96u, 100u, 2u, 26u, 14u, 144u, 16u,
130u, 161u, 248u, 20u, 146u, 68u, 13u, 15u, 145u, 15u, 67u, 134u, 26u,
36u, 112u, 208u, 192u, 152u, 193u, 145u, 195u, 3u, 2u, 99u, 48u, 71u,
11u, 12u, 9u, 2u, 145u, 194u, 67u, 2u, 67u, 228u, 112u, 112u, 192u,
144u, 17u, 28u, 12u, 48u, 66u, 48u, 231u, 0u, 36u, 112u, 0u, 192u,
144u, 225u, 12u, 65u, 129u, 141u, 67u, 40u, 76u, 190u, 68u, 14u, 68u,
55u, 13u, 4u, 198u, 12u, 38u, 56u, 161u, 49u, 223u, 36u, 146u,};
static unsigned char uvector__00013[] = {
 0u, 3u, 189u, 134u, 8u, 50u, 32u, 36u, 119u, 64u, 192u, 198u, 161u,
148u, 66u, 48u, 231u, 0u, 34u, 7u, 34u, 27u, 134u, 131u, 164u, 4u,
32u, 168u, 126u, 5u, 36u, 64u, 208u, 249u, 16u, 244u, 56u, 73u, 36u,
119u, 0u, 192u, 198u, 162u, 27u, 134u, 131u, 164u, 4u, 32u, 168u,
126u, 5u, 36u, 64u, 208u, 249u, 36u, 142u, 220u, 24u, 25u, 68u, 67u,
14u, 16u, 228u, 161u, 49u, 86u, 33u, 201u, 66u, 96u, 188u, 73u, 29u,
176u, 48u, 38u, 61u, 164u, 118u, 176u, 192u, 144u, 225u, 29u, 164u,
48u, 38u, 61u, 164u, 118u, 128u, 192u, 144u, 249u, 29u, 156u, 48u,
50u, 1u, 15u, 240u, 2u, 66u, 99u, 196u, 72u, 236u, 65u, 129u, 141u,
67u, 40u, 132u, 97u, 206u, 0u, 68u, 14u, 68u, 55u, 13u, 64u, 66u, 6u,
128u, 160u, 82u, 33u, 232u, 112u, 146u, 72u, 235u, 193u, 129u, 141u,
68u, 55u, 13u, 64u, 66u, 6u, 128u, 160u, 82u, 73u, 29u, 112u, 48u,
50u, 136u, 134u, 28u, 33u, 201u, 66u, 104u, 68u, 233u, 14u, 74u, 19u,
56u, 210u, 72u, 235u, 65u, 129u, 52u, 38u, 48u, 142u, 178u, 24u, 18u,
28u, 35u, 171u, 134u, 4u, 208u, 152u, 194u, 58u, 152u, 96u, 131u,
160u, 82u, 71u, 82u, 12u, 9u, 2u, 145u, 212u, 3u, 2u, 104u, 94u, 65u,
29u, 56u, 48u, 50u, 135u, 1u, 7u, 64u, 68u, 38u, 133u, 228u, 16u,
154u, 19u, 2u, 27u, 97u, 254u, 0u, 19u, 30u, 34u, 72u, 233u, 129u,
129u, 52u, 52u, 60u, 142u, 150u, 24u, 18u, 2u, 35u, 164u, 134u, 4u,
208u, 208u, 242u, 58u, 64u, 96u, 72u, 112u, 142u, 142u, 24u, 25u, 0u,
135u, 3u, 40u, 128u, 225u, 206u, 0u, 97u, 72u, 112u, 146u, 25u, 162u,
0u, 154u, 25u, 178u, 66u, 96u, 56u, 72u, 232u, 193u, 129u, 33u, 194u,
58u, 0u, 96u, 77u, 16u, 24u, 35u, 158u, 134u, 6u, 120u, 72u, 48u,
134u, 49u, 9u, 136u, 114u, 20u, 34u, 2u, 224u, 4u, 64u, 188u, 4u,
144u, 219u, 14u, 4u, 209u, 0u, 208u, 154u, 33u, 52u, 19u, 1u, 194u,
73u, 35u, 155u, 134u, 4u, 209u, 33u, 194u, 57u, 152u, 96u, 107u, 136u,
137u, 28u, 200u, 48u, 36u, 68u, 71u, 49u, 12u, 12u, 128u, 64u, 161u,
148u, 53u, 196u, 103u, 128u, 134u, 136u, 140u, 146u, 34u, 32u, 202u,
26u, 226u, 50u, 24u, 84u, 134u, 168u, 140u, 146u, 67u, 120u, 77u, 20u,
192u, 9u, 162u, 67u, 132u, 50u, 42u, 104u, 154u, 33u, 35u, 152u, 6u,
4u, 209u, 108u, 66u, 57u, 88u, 96u, 77u, 22u, 90u, 35u, 149u, 6u, 4u,
136u, 200u, 229u, 1u, 129u, 52u, 88u, 16u, 142u, 70u, 24u, 19u, 69u,
112u, 72u, 228u, 65u, 129u, 34u, 50u, 57u, 8u, 96u, 72u, 8u, 142u,
64u, 24u, 25u, 0u, 128u, 131u, 40u, 107u, 137u, 15u, 0u, 13u, 17u,
33u, 36u, 68u, 97u, 148u, 53u, 196u, 132u, 48u, 169u, 13u, 81u, 33u,
36u, 132u, 209u, 85u, 178u, 71u, 31u, 12u, 9u, 163u, 128u, 100u, 113u,
160u, 192u, 154u, 55u, 50u, 71u, 25u, 12u, 9u, 18u, 17u, 197u, 195u,
2u, 104u, 218u, 25u, 28u, 72u, 48u, 38u, 141u, 128u, 17u, 196u, 67u,
2u, 68u, 132u, 113u, 0u, 192u, 200u, 4u, 56u, 25u, 67u, 92u, 74u, 97u,
64u, 209u, 18u, 146u, 68u, 72u, 25u, 67u, 92u, 74u, 67u, 10u, 144u,
213u, 18u, 146u, 72u, 77u, 26u, 154u, 36u, 112u, 240u, 192u, 154u,
64u, 142u, 71u, 10u, 12u, 9u, 163u, 251u, 164u, 112u, 144u, 192u,
145u, 49u, 28u, 28u, 48u, 38u, 143u, 195u, 145u, 192u, 67u, 2u, 104u,
250u, 41u, 28u, 0u, 48u, 36u, 76u, 67u, 16u, 96u, 77u, 18u, 92u, 36u,};
static unsigned char uvector__00014[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 19u, 194u, 98u, 28u, 133u, 8u, 128u, 184u,
1u, 16u, 47u, 1u, 36u, 54u, 195u, 129u, 148u, 64u, 112u, 231u, 0u,
48u, 164u, 56u, 67u, 52u, 64u, 25u, 67u, 128u, 131u, 160u, 34u, 32u,
232u, 20u, 144u, 202u, 34u, 24u, 112u, 135u, 37u, 16u, 220u, 53u, 1u,
8u, 26u, 2u, 129u, 73u, 33u, 201u, 67u, 40u, 132u, 97u, 206u, 0u, 68u,
14u, 68u, 55u, 13u, 64u, 66u, 6u, 128u, 160u, 82u, 33u, 232u, 112u,
146u, 72u, 109u, 135u, 248u, 0u, 101u, 17u, 12u, 56u, 67u, 146u, 136u,
110u, 26u, 14u, 144u, 16u, 130u, 161u, 248u, 20u, 145u, 3u, 67u, 228u,
144u, 228u, 161u, 148u, 66u, 48u, 231u, 0u, 34u, 7u, 34u, 27u, 134u,
131u, 164u, 4u, 32u, 168u, 126u, 5u, 36u, 64u, 208u, 249u, 16u, 244u,
56u, 73u, 36u, 146u, 32u, 200u, 128u, 146u, 71u, 9u, 12u, 12u, 97u,
48u, 49u, 76u, 35u, 136u, 224u, 225u, 129u, 48u, 28u, 35u, 129u, 6u,
4u, 192u, 112u, 142u, 0u, 24u, 19u, 1u, 194u, 64u,};
static unsigned char uvector__00015[] = {
 0u, 3u, 128u, 134u, 8u, 162u, 41u, 36u, 112u, 0u, 192u, 145u, 73u,
12u, 65u, 129u, 141u, 66u, 96u, 56u, 73u, 0u,};
static unsigned char uvector__00016[] = {
 0u, 3u, 143u, 134u, 8u, 48u, 69u, 81u, 68u, 82u, 73u, 28u, 116u, 48u,
49u, 168u, 138u, 34u, 146u, 72u, 227u, 97u, 129u, 158u, 18u, 12u, 33u,
140u, 66u, 98u, 41u, 17u, 89u, 36u, 38u, 3u, 132u, 146u, 56u, 168u,
96u, 76u, 43u, 136u, 226u, 33u, 129u, 174u, 44u, 36u, 113u, 0u, 192u,
145u, 97u, 28u, 60u, 48u, 50u, 1u, 20u, 134u, 80u, 215u, 22u, 144u,
152u, 121u, 134u, 136u, 180u, 146u, 34u, 192u, 202u, 26u, 226u, 210u,
24u, 84u, 134u, 168u, 180u, 146u, 67u, 120u, 76u, 91u, 130u, 97u, 92u,
67u, 34u, 166u, 32u, 196u, 142u, 28u, 24u, 19u, 39u, 194u, 56u, 72u,
96u, 76u, 148u, 136u, 225u, 1u, 129u, 34u, 226u, 56u, 48u, 96u, 76u,
135u, 8u, 224u, 97u, 129u, 48u, 243u, 35u, 128u, 134u, 4u, 199u, 188u,
142u, 0u, 24u, 18u, 46u, 33u, 136u, 48u, 38u, 25u, 196u, 128u,};
static unsigned char uvector__00017[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 23u, 194u, 98u, 41u, 17u, 89u, 36u, 65u,
130u, 42u, 138u, 34u, 146u, 73u, 28u, 36u, 48u, 49u, 132u, 192u, 197u,
48u, 94u, 35u, 131u, 134u, 4u, 192u, 112u, 142u, 4u, 24u, 19u, 1u,
194u, 56u, 0u, 96u, 76u, 7u, 9u, 0u,};
static unsigned char uvector__00018[] = {
 0u, 3u, 128u, 134u, 8u, 194u, 41u, 36u, 112u, 0u, 192u, 145u, 73u,
12u, 65u, 129u, 141u, 66u, 96u, 56u, 73u, 0u,};
static unsigned char uvector__00019[] = {
 0u, 3u, 143u, 134u, 8u, 48u, 69u, 81u, 132u, 82u, 73u, 28u, 116u,
48u, 49u, 168u, 140u, 34u, 146u, 72u, 227u, 97u, 129u, 158u, 18u, 12u,
33u, 140u, 66u, 98u, 41u, 17u, 89u, 36u, 38u, 3u, 132u, 146u, 56u,
168u, 96u, 76u, 43u, 136u, 226u, 33u, 129u, 174u, 49u, 36u, 113u, 0u,
192u, 145u, 137u, 28u, 60u, 48u, 50u, 1u, 20u, 134u, 80u, 215u, 25u,
16u, 152u, 121u, 134u, 136u, 200u, 146u, 35u, 16u, 202u, 26u, 227u,
34u, 24u, 84u, 134u, 168u, 200u, 146u, 67u, 120u, 76u, 91u, 130u, 97u,
92u, 67u, 34u, 166u, 32u, 196u, 142u, 28u, 24u, 19u, 39u, 194u, 56u,
72u, 96u, 76u, 148u, 136u, 225u, 1u, 129u, 35u, 50u, 56u, 48u, 96u,
76u, 135u, 8u, 224u, 97u, 129u, 48u, 243u, 35u, 128u, 134u, 4u, 199u,
188u, 142u, 0u, 24u, 18u, 51u, 33u, 136u, 48u, 38u, 25u, 196u, 128u,};
static unsigned char uvector__00020[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 26u, 66u, 98u, 41u, 17u, 89u, 36u, 65u,
130u, 42u, 140u, 34u, 146u, 73u, 28u, 36u, 48u, 49u, 132u, 192u, 197u,
48u, 94u, 35u, 131u, 134u, 4u, 192u, 112u, 142u, 4u, 24u, 19u, 1u,
194u, 56u, 0u, 96u, 76u, 7u, 9u, 0u,};
static unsigned char uvector__00021[] = {
 0u, 3u, 128u, 134u, 8u, 214u, 41u, 36u, 112u, 0u, 192u, 145u, 73u,
12u, 65u, 129u, 141u, 66u, 96u, 56u, 73u, 0u,};
static unsigned char uvector__00022[] = {
 0u, 3u, 143u, 134u, 8u, 48u, 69u, 81u, 172u, 82u, 73u, 28u, 116u,
48u, 49u, 168u, 141u, 98u, 146u, 72u, 227u, 97u, 129u, 158u, 18u, 12u,
33u, 140u, 66u, 98u, 41u, 17u, 89u, 36u, 38u, 3u, 132u, 146u, 56u,
168u, 96u, 76u, 43u, 136u, 226u, 33u, 129u, 174u, 54u, 36u, 113u, 0u,
192u, 145u, 177u, 28u, 60u, 48u, 50u, 1u, 20u, 134u, 80u, 215u, 27u,
144u, 152u, 121u, 134u, 136u, 220u, 146u, 35u, 96u, 202u, 26u, 227u,
114u, 24u, 84u, 134u, 168u, 220u, 146u, 67u, 120u, 76u, 91u, 130u,
97u, 92u, 67u, 34u, 166u, 32u, 196u, 142u, 28u, 24u, 19u, 39u, 194u,
56u, 72u, 96u, 76u, 148u, 136u, 225u, 1u, 129u, 35u, 130u, 56u, 48u,
96u, 76u, 135u, 8u, 224u, 97u, 129u, 48u, 243u, 35u, 128u, 134u, 4u,
199u, 188u, 142u, 0u, 24u, 18u, 56u, 33u, 136u, 48u, 38u, 25u, 196u,
128u,};
static unsigned char uvector__00023[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 28u, 194u, 98u, 41u, 17u, 89u, 36u, 65u,
130u, 42u, 141u, 98u, 146u, 73u, 28u, 36u, 48u, 49u, 132u, 192u, 197u,
48u, 94u, 35u, 131u, 134u, 4u, 192u, 112u, 142u, 4u, 24u, 19u, 1u,
194u, 56u, 0u, 96u, 76u, 7u, 9u, 0u,};
static unsigned char uvector__00024[] = {
 0u, 3u, 128u, 134u, 8u, 234u, 41u, 36u, 112u, 0u, 192u, 145u, 73u,
12u, 65u, 129u, 141u, 66u, 96u, 56u, 73u, 0u,};
static unsigned char uvector__00025[] = {
 0u, 3u, 143u, 134u, 8u, 48u, 69u, 81u, 212u, 82u, 73u, 28u, 116u,
48u, 49u, 168u, 142u, 162u, 146u, 72u, 227u, 97u, 129u, 158u, 18u,
12u, 33u, 140u, 66u, 98u, 41u, 17u, 89u, 36u, 38u, 3u, 132u, 146u,
56u, 168u, 96u, 76u, 43u, 136u, 226u, 33u, 129u, 174u, 59u, 36u, 113u,
0u, 192u, 145u, 217u, 28u, 60u, 48u, 50u, 1u, 20u, 134u, 80u, 215u,
30u, 16u, 152u, 121u, 134u, 136u, 240u, 146u, 35u, 176u, 202u, 26u,
227u, 194u, 24u, 84u, 134u, 168u, 240u, 146u, 67u, 120u, 76u, 91u,
130u, 97u, 92u, 67u, 34u, 166u, 32u, 196u, 142u, 28u, 24u, 19u, 39u,
194u, 56u, 72u, 96u, 76u, 148u, 136u, 225u, 1u, 129u, 35u, 210u, 56u,
48u, 96u, 76u, 135u, 8u, 224u, 97u, 129u, 48u, 243u, 35u, 128u, 134u,
4u, 199u, 188u, 142u, 0u, 24u, 18u, 61u, 33u, 136u, 48u, 38u, 25u,
196u, 128u,};
static unsigned char uvector__00026[] = {
 0u, 3u, 134u, 6u, 6u, 97u, 31u, 66u, 98u, 41u, 17u, 89u, 36u, 65u,
130u, 42u, 142u, 162u, 146u, 73u, 28u, 36u, 48u, 49u, 132u, 192u,
197u, 48u, 94u, 35u, 131u, 134u, 4u, 192u, 112u, 142u, 4u, 24u, 19u,
1u, 194u, 56u, 0u, 96u, 76u, 7u, 9u, 0u,};
static ScmObj SCM_debug_info_const_vector();
#if defined(__CYGWIN__) || defined(GAUCHE_WINDOWS)
#define SCM_CGEN_CONST /*empty*/
#else
#define SCM_CGEN_CONST const
#endif
static SCM_CGEN_CONST struct scm__scRec {
  ScmString d1785[99];
} scm__sc SCM_UNUSED = {
  {   /* ScmString d1785 */
      SCM_STRING_CONST_INITIALIZER("force", 5, 5),
      SCM_STRING_CONST_INITIALIZER("p", 1, 1),
      SCM_STRING_CONST_INITIALIZER("source-info", 11, 11),
      SCM_STRING_CONST_INITIALIZER("liblazy.scm", 11, 11),
      SCM_STRING_CONST_INITIALIZER("bind-info", 9, 9),
      SCM_STRING_CONST_INITIALIZER("scheme", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<top>", 5, 5),
      SCM_STRING_CONST_INITIALIZER("->", 2, 2),
      SCM_STRING_CONST_INITIALIZER("promise\077", 8, 8),
      SCM_STRING_CONST_INITIALIZER("obj", 3, 3),
      SCM_STRING_CONST_INITIALIZER("gauche", 6, 6),
      SCM_STRING_CONST_INITIALIZER("<boolean>", 9, 9),
      SCM_STRING_CONST_INITIALIZER("eager", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest", 4, 4),
      SCM_STRING_CONST_INITIALIZER("objs", 4, 4),
      SCM_STRING_CONST_INITIALIZER("*", 1, 1),
      SCM_STRING_CONST_INITIALIZER("promise-kind", 12, 12),
      SCM_STRING_CONST_INITIALIZER("(setter promise-kind)", 21, 21),
      SCM_STRING_CONST_INITIALIZER("<promise>", 9, 9),
      SCM_STRING_CONST_INITIALIZER("%expression-name-mark-key", 25, 25),
      SCM_STRING_CONST_INITIALIZER("make-promise", 12, 12),
      SCM_STRING_CONST_INITIALIZER("%toplevel", 9, 9),
      SCM_STRING_CONST_INITIALIZER("%lcons", 6, 6),
      SCM_STRING_CONST_INITIALIZER("item", 4, 4),
      SCM_STRING_CONST_INITIALIZER("thunk", 5, 5),
      SCM_STRING_CONST_INITIALIZER("optional", 8, 8),
      SCM_STRING_CONST_INITIALIZER("attrs", 5, 5),
      SCM_STRING_CONST_INITIALIZER("gauche.internal", 15, 15),
      SCM_STRING_CONST_INITIALIZER("generator->lseq", 15, 15),
      SCM_STRING_CONST_INITIALIZER("args", 4, 4),
      SCM_STRING_CONST_INITIALIZER("lrange", 6, 6),
      SCM_STRING_CONST_INITIALIZER("too many arguments for", 22, 22),
      SCM_STRING_CONST_INITIALIZER("lambda", 6, 6),
      SCM_STRING_CONST_INITIALIZER("start", 5, 5),
      SCM_STRING_CONST_INITIALIZER("end", 3, 3),
      SCM_STRING_CONST_INITIALIZER("step", 4, 4),
      SCM_STRING_CONST_INITIALIZER("cond", 4, 4),
      SCM_STRING_CONST_INITIALIZER("or", 2, 2),
      SCM_STRING_CONST_INITIALIZER("and", 3, 3),
      SCM_STRING_CONST_INITIALIZER(">", 1, 1),
      SCM_STRING_CONST_INITIALIZER(">=", 2, 2),
      SCM_STRING_CONST_INITIALIZER("<", 1, 1),
      SCM_STRING_CONST_INITIALIZER("<=", 2, 2),
      SCM_STRING_CONST_INITIALIZER("quote", 5, 5),
      SCM_STRING_CONST_INITIALIZER("=", 1, 1),
      SCM_STRING_CONST_INITIALIZER("^", 1, 1),
      SCM_STRING_CONST_INITIALIZER("exact\077", 6, 6),
      SCM_STRING_CONST_INITIALIZER("if", 2, 2),
      SCM_STRING_CONST_INITIALIZER("inc!", 4, 4),
      SCM_STRING_CONST_INITIALIZER("eof-object", 10, 10),
      SCM_STRING_CONST_INITIALIZER("else", 4, 4),
      SCM_STRING_CONST_INITIALIZER("inexact", 7, 7),
      SCM_STRING_CONST_INITIALIZER("let1", 4, 4),
      SCM_STRING_CONST_INITIALIZER("c", 1, 1),
      SCM_STRING_CONST_INITIALIZER("r", 1, 1),
      SCM_STRING_CONST_INITIALIZER("+", 1, 1),
      SCM_STRING_CONST_INITIALIZER("error", 5, 5),
      SCM_STRING_CONST_INITIALIZER("liota", 5, 5),
      SCM_STRING_CONST_INITIALIZER("count", 5, 5),
      SCM_STRING_CONST_INITIALIZER("define", 6, 6),
      SCM_STRING_CONST_INITIALIZER("gen", 3, 3),
      SCM_STRING_CONST_INITIALIZER("infinite\077", 9, 9),
      SCM_STRING_CONST_INITIALIZER("rlet1", 5, 5),
      SCM_STRING_CONST_INITIALIZER("v", 1, 1),
      SCM_STRING_CONST_INITIALIZER("dec!", 4, 4),
      SCM_STRING_CONST_INITIALIZER("k", 1, 1),
      SCM_STRING_CONST_INITIALIZER("port->char-lseq", 15, 15),
      SCM_STRING_CONST_INITIALIZER("port", 4, 4),
      SCM_STRING_CONST_INITIALIZER("current-input-port", 18, 18),
      SCM_STRING_CONST_INITIALIZER("cut", 3, 3),
      SCM_STRING_CONST_INITIALIZER("read-char", 9, 9),
      SCM_STRING_CONST_INITIALIZER("port->byte-lseq", 15, 15),
      SCM_STRING_CONST_INITIALIZER("read-byte", 9, 9),
      SCM_STRING_CONST_INITIALIZER("port->string-lseq", 17, 17),
      SCM_STRING_CONST_INITIALIZER("read-line", 9, 9),
      SCM_STRING_CONST_INITIALIZER("port->sexp-lseq", 15, 15),
      SCM_STRING_CONST_INITIALIZER("read", 4, 4),
      SCM_STRING_CONST_INITIALIZER("G1792", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1794", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1793", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest1791", 8, 8),
      SCM_STRING_CONST_INITIALIZER("-", 1, 1),
      SCM_STRING_CONST_INITIALIZER("G1796", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1799", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1798", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1797", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest1795", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G1801", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1802", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest1800", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G1804", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1805", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest1803", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G1807", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1808", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest1806", 8, 8),
      SCM_STRING_CONST_INITIALIZER("G1810", 5, 5),
      SCM_STRING_CONST_INITIALIZER("G1811", 5, 5),
      SCM_STRING_CONST_INITIALIZER("rest1809", 8, 8),
  },
};
static struct scm__rcRec {
  ScmUVector d1790[26];
  ScmCompiledCode d1789[26];
  ScmWord d1788[625];
  ScmPair d1787[407] SCM_ALIGN_PAIR;
  ScmObj d1786[276];
} scm__rc SCM_UNUSED = {
  {   /* ScmUVector d1790 */
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 32, uvector__00001, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 14, uvector__00002, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 55, uvector__00003, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 55, uvector__00004, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 105, uvector__00005, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 105, uvector__00006, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 588, uvector__00007, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 204, uvector__00008, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 57, uvector__00009, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 101, uvector__00010, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 86, uvector__00011, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 129, uvector__00012, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 569, uvector__00013, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 168, uvector__00014, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 22, uvector__00015, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 142, uvector__00016, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 48, uvector__00017, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 22, uvector__00018, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 142, uvector__00019, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 48, uvector__00020, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 22, uvector__00021, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 142, uvector__00022, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 48, uvector__00023, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 22, uvector__00024, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 142, uvector__00025, 0, NULL),
      SCM_UVECTOR_INITIALIZER(Scm_U8VectorClass, 48, uvector__00026, 0, NULL),
  },
  {   /* ScmCompiledCode d1789 */
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[0])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[15])), 2,
            0, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[190]),
            SCM_OBJ(&scm__rc.d1789[6]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[17])), 10,
            0, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[195]),
            SCM_OBJ(&scm__rc.d1789[6]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[27])), 10,
            0, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[200]),
            SCM_OBJ(&scm__rc.d1789[6]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[37])), 17,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[205]),
            SCM_OBJ(&scm__rc.d1789[6]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[54])), 17,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[210]),
            SCM_OBJ(&scm__rc.d1789[6]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* lrange */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[71])), 131,
            22, 1, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[215]),
            SCM_OBJ(&scm__rc.d1789[7]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[202])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* gen */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[217])), 6,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[308]),
            SCM_OBJ(&scm__rc.d1789[12]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* gen */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[223])), 16,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[313]),
            SCM_OBJ(&scm__rc.d1789[12]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* gen */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[239])), 12,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[318]),
            SCM_OBJ(&scm__rc.d1789[12]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* gen */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[251])), 22,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[323]),
            SCM_OBJ(&scm__rc.d1789[12]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* liota */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[273])), 126,
            34, 0, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[328]),
            SCM_OBJ(&scm__rc.d1789[13]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[399])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[414])), 3,
            0, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[347]),
            SCM_OBJ(&scm__rc.d1789[15]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* port->char-lseq */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[417])), 34,
            17, 0, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[352]),
            SCM_OBJ(&scm__rc.d1789[16]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[451])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[466])), 4,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[365]),
            SCM_OBJ(&scm__rc.d1789[18]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* port->byte-lseq */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[470])), 34,
            17, 0, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[370]),
            SCM_OBJ(&scm__rc.d1789[19]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[504])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[519])), 4,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[383]),
            SCM_OBJ(&scm__rc.d1789[21]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* port->string-lseq */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[523])), 34,
            17, 0, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[388]),
            SCM_OBJ(&scm__rc.d1789[22]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[557])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* #f */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[572])), 4,
            4, 0, 0, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[401]),
            SCM_OBJ(&scm__rc.d1789[24]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* port->sexp-lseq */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[576])), 34,
            17, 0, 1, SCM_FALSE, SCM_NIL, SCM_OBJ(&scm__rc.d1787[406]),
            SCM_OBJ(&scm__rc.d1789[25]), SCM_FALSE),
        SCM_COMPILED_CODE_CONST_INITIALIZER(  /* %toplevel */
            (ScmWord*)(SCM_OBJ(&scm__rc.d1788[610])), 15,
            10, 0, 0, SCM_FALSE, SCM_NIL, SCM_FALSE,
            SCM_FALSE, SCM_FALSE),
  },
  {   /* ScmWord d1788 */
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[0]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.e3753ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* make-promise */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[0]) + 12),
    0x0000005d    /*   9 (GREF) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#eager.e23c1940> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#make-promise.e23c19e0> */,
    0x00000014    /*  14 (RET) */,
    /* (lrange #f) */
    0x004020ea    /*   0 (LREF-UNBOX 2 1) */,
    0x00000014    /*   1 (RET) */,
    /* (lrange #f) */
    0x004020ea    /*   0 (LREF-UNBOX 2 1) */,
    0x004000b6    /*   1 (LREF-VAL0-NUMADD2 0 1) */,
    0x0040203a    /*   2 (LSET 2 1) */,
    0x004020ea    /*   3 (LREF-UNBOX 2 1) */,
    0x0040102b    /*   4 (LREF-VAL0-BNGT 1 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[17]) + 8),
    0x004020ea    /*   6 (LREF-UNBOX 2 1) */,
    0x00000014    /*   7 (RET) */,
    0x0000000a    /*   8 (CONST-RET) */,
    SCM_WORD(SCM_EOF) /* #<eof> */,
    /* (lrange #f) */
    0x004020ea    /*   0 (LREF-UNBOX 2 1) */,
    0x004000b6    /*   1 (LREF-VAL0-NUMADD2 0 1) */,
    0x0040203a    /*   2 (LSET 2 1) */,
    0x004020ea    /*   3 (LREF-UNBOX 2 1) */,
    0x00401029    /*   4 (LREF-VAL0-BNLT 1 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[27]) + 8),
    0x004020ea    /*   6 (LREF-UNBOX 2 1) */,
    0x00000014    /*   7 (RET) */,
    0x0000000a    /*   8 (CONST-RET) */,
    SCM_WORD(SCM_EOF) /* #<eof> */,
    /* (lrange #f) */
    0x000000ea    /*   0 (LREF-UNBOX 0 0) */,
    0x000010bc    /*   1 (NUMADDI 1) */,
    0x0000003a    /*   2 (LSET 0 0) */,
    0x004030ea    /*   3 (LREF-UNBOX 3 1) */,
    0x0000000d    /*   4 (PUSH) */,
    0x000000ea    /*   5 (LREF-UNBOX 0 0) */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000042    /*   7 (LREF11) */,
    0x000000b4    /*   8 (NUMMUL2) */,
    0x000000b2    /*   9 (NUMADD2) */,
    0x00001018    /*  10 (PUSH-LOCAL-ENV 1) */,
    0x0040303c    /*  11 (LREF 3 1) */,
    0x00000029    /*  12 (LREF-VAL0-BNLT 0 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[37]) + 15),
    0x00000053    /*  14 (LREF0-RET) */,
    0x0000000a    /*  15 (CONST-RET) */,
    SCM_WORD(SCM_EOF) /* #<eof> */,
    /* (lrange #f) */
    0x000000ea    /*   0 (LREF-UNBOX 0 0) */,
    0x000010bc    /*   1 (NUMADDI 1) */,
    0x0000003a    /*   2 (LSET 0 0) */,
    0x004030ea    /*   3 (LREF-UNBOX 3 1) */,
    0x0000000d    /*   4 (PUSH) */,
    0x000000ea    /*   5 (LREF-UNBOX 0 0) */,
    0x0000000d    /*   6 (PUSH) */,
    0x00000042    /*   7 (LREF11) */,
    0x000000b4    /*   8 (NUMMUL2) */,
    0x000000b2    /*   9 (NUMADD2) */,
    0x00001018    /*  10 (PUSH-LOCAL-ENV 1) */,
    0x0040303c    /*  11 (LREF 3 1) */,
    0x0000002b    /*  12 (LREF-VAL0-BNGT 0 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[54]) + 15),
    0x00000053    /*  14 (LREF0-RET) */,
    0x0000000a    /*  15 (CONST-RET) */,
    SCM_WORD(SCM_EOF) /* #<eof> */,
    /* lrange */
    0x000020e7    /*   0 (BOX 2) */,
    0x0000003d    /*   1 (LREF0) */,
    0x00000022    /*   2 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 8),
    0x00000001    /*   4 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* +inf.0 */,
    0x00000013    /*   6 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 9),
    0x0000006a    /*   8 (LREF0-CAR) */,
    0x0000000d    /*   9 (PUSH) */,
    0x0000003d    /*  10 (LREF0) */,
    0x00000022    /*  11 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 16),
    0x00000003    /*  13 (CONSTN) */,
    0x00000013    /*  14 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 17),
    0x00000076    /*  16 (LREF0-CDR) */,
    0x00002018    /*  17 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  18 (LREF0) */,
    0x00000022    /*  19 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 24),
    0x00001002    /*  21 (CONSTI 1) */,
    0x00000013    /*  22 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 25),
    0x0000006a    /*  24 (LREF0-CAR) */,
    0x0000000d    /*  25 (PUSH) */,
    0x0000003d    /*  26 (LREF0) */,
    0x00000022    /*  27 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 32),
    0x00000003    /*  29 (CONSTN) */,
    0x00000013    /*  30 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 33),
    0x00000076    /*  32 (LREF0-CDR) */,
    0x00002018    /*  33 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  34 (LREF0) */,
    0x00000022    /*  35 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 39),
    0x00000013    /*  37 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 47),
    0x0000200e    /*  39 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 47),
    0x00000006    /*  41 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[31])) /* "too many arguments for" */,
    0x00000006    /*  43 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[183])) /* (lambda (start :optional (end +inf.0) (step 1)) (cond ((or (and (> step 0) (>= start end)) (and (< step 0) (<= start end))) '()) ((= step 0) (generator->lseq (^ () start))) ((and (exact? start) (exact? step)) (generator->lseq start (if (> step 0) (^ () (inc! start step) (if (< start end) start (eof-object))) (^ () (inc! start step) (if (> start end) start (eof-object)))))) (else (generator->lseq (inexact start) (let1 c 0 (if (> step 0) (^ () (inc! c) (let1 r (+ start (* c step)) (if (< r end) r (eof-object)))) (^ () (inc! c) (let1 r (+ start (* c step)) (if (> r end) r (eof-object)))))))))) */,
    0x0000205f    /*  45 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.e3753860> */,
    0x00000002    /*  47 (CONSTI 0) */,
    0x0040002b    /*  48 (LREF-VAL0-BNGT 0 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 55),
    0x004020ea    /*  50 (LREF-UNBOX 2 1) */,
    0x0040102a    /*  51 (LREF-VAL0-BNLE 1 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 55),
    0x00000013    /*  53 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 66),
    0x00000002    /*  55 (CONSTI 0) */,
    0x00400029    /*  56 (LREF-VAL0-BNLT 0 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 66),
    0x004020ea    /*  58 (LREF-UNBOX 2 1) */,
    0x0000000d    /*  59 (PUSH) */,
    0x00000042    /*  60 (LREF11) */,
    0x000000af    /*  61 (NUMLE2) */,
    0x00000013    /*  62 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 66),
    0x00000013    /*  64 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 55),
    0x0000001e    /*  66 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 70),
    0x00000003    /*  68 (CONSTN) */,
    0x00000014    /*  69 (RET) */,
    0x0000003e    /*  70 (LREF1) */,
    0x0000002d    /*  71 (BNUMNEI 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 78),
    0x00000016    /*  73 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[1])) /* #<compiled-code (lrange #f)@0x7f95e229f420> */,
    0x00001063    /*  75 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#generator->lseq.e1db4a20> */,
    0x00000014    /*  77 (RET) */,
    0x0000100e    /*  78 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 83),
    0x004020ea    /*  80 (LREF-UNBOX 2 1) */,
    0x00001062    /*  81 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#exact?.e1db4120> */,
    0x0000001e    /*  83 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 106),
    0x0000100e    /*  85 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 90),
    0x00000049    /*  87 (LREF1-PUSH) */,
    0x0000105f    /*  88 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#exact?.e1db40c0> */,
    0x0000001e    /*  90 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 106),
    0x004020ea    /*  92 (LREF-UNBOX 2 1) */,
    0x0000000d    /*  93 (PUSH) */,
    0x00000002    /*  94 (CONSTI 0) */,
    0x0040002b    /*  95 (LREF-VAL0-BNGT 0 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 101),
    0x00000016    /*  97 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[2])) /* #<compiled-code (lrange #f)@0x7f95e229f3c0> */,
    0x00000013    /*  99 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 103),
    0x00000016    /* 101 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[3])) /* #<compiled-code (lrange #f)@0x7f95e229f360> */,
    0x00002063    /* 103 (PUSH-GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#generator->lseq.e1db4020> */,
    0x00000014    /* 105 (RET) */,
    0x0000100e    /* 106 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 111),
    0x004020ea    /* 108 (LREF-UNBOX 2 1) */,
    0x00001062    /* 109 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#inexact.e1db95c0> */,
    0x0000100f    /* 111 (PUSH-PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 125),
    0x00000007    /* 113 (CONSTI-PUSH 0) */,
    0x00001017    /* 114 (LOCAL-ENV 1) */,
    0x000010e7    /* 115 (BOX 1) */,
    0x00000002    /* 116 (CONSTI 0) */,
    0x0040102b    /* 117 (LREF-VAL0-BNGT 1 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 122),
    0x00000016    /* 119 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[4])) /* #<compiled-code (lrange #f)@0x7f95e229f300> */,
    0x00000014    /* 121 (RET) */,
    0x00000016    /* 122 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[5])) /* #<compiled-code (lrange #f)@0x7f95e229f2a0> */,
    0x00000014    /* 124 (RET) */,
    0x00002063    /* 125 (PUSH-GREF-TAIL-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#generator->lseq.e1db9600> */,
    0x00000014    /* 127 (RET) */,
    0x00000013    /* 128 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]) + 106),
    0x00000014    /* 130 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[202]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.e3753ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* lrange */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[202]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[6])) /* #<compiled-code lrange@0x7f95e229f480> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#lrange.e1d1f7e0> */,
    0x00000014    /*  14 (RET) */,
    /* (liota gen) */
    0x004020ea    /*   0 (LREF-UNBOX 2 1) */,
    0x00001018    /*   1 (PUSH-LOCAL-ENV 1) */,
    0x004030ea    /*   2 (LREF-UNBOX 3 1) */,
    0x004020b6    /*   3 (LREF-VAL0-NUMADD2 2 1) */,
    0x0040303a    /*   4 (LSET 3 1) */,
    0x00000053    /*   5 (LREF0-RET) */,
    /* (liota gen) */
    0x000000ea    /*   0 (LREF-UNBOX 0 0) */,
    0x0000000d    /*   1 (PUSH) */,
    0x00000002    /*   2 (CONSTI 0) */,
    0x00000025    /*   3 (BNLE) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[223]) + 7),
    0x0000000a    /*   5 (CONST-RET) */,
    SCM_WORD(SCM_EOF) /* #<eof> */,
    0x004020ea    /*   7 (LREF-UNBOX 2 1) */,
    0x00001018    /*   8 (PUSH-LOCAL-ENV 1) */,
    0x004030ea    /*   9 (LREF-UNBOX 3 1) */,
    0x004020b6    /*  10 (LREF-VAL0-NUMADD2 2 1) */,
    0x0040303a    /*  11 (LSET 3 1) */,
    0x000010ea    /*  12 (LREF-UNBOX 1 0) */,
    -0x00000f44   /*  13 (NUMADDI -1) */,
    0x0000103a    /*  14 (LSET 1 0) */,
    0x00000053    /*  15 (LREF0-RET) */,
    /* (liota gen) */
    0x004030ea    /*   0 (LREF-UNBOX 3 1) */,
    0x0000000d    /*   1 (PUSH) */,
    0x000000ea    /*   2 (LREF-UNBOX 0 0) */,
    0x0000000d    /*   3 (PUSH) */,
    0x00000045    /*   4 (LREF21) */,
    0x000000b4    /*   5 (NUMMUL2) */,
    0x000000b2    /*   6 (NUMADD2) */,
    0x00001018    /*   7 (PUSH-LOCAL-ENV 1) */,
    0x000010ea    /*   8 (LREF-UNBOX 1 0) */,
    0x000010bc    /*   9 (NUMADDI 1) */,
    0x0000103a    /*  10 (LSET 1 0) */,
    0x00000053    /*  11 (LREF0-RET) */,
    /* (liota gen) */
    0x000010ea    /*   0 (LREF-UNBOX 1 0) */,
    0x0000000d    /*   1 (PUSH) */,
    0x00000002    /*   2 (CONSTI 0) */,
    0x00000025    /*   3 (BNLE) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[251]) + 7),
    0x0000000a    /*   5 (CONST-RET) */,
    SCM_WORD(SCM_EOF) /* #<eof> */,
    0x004030ea    /*   7 (LREF-UNBOX 3 1) */,
    0x0000000d    /*   8 (PUSH) */,
    0x000000ea    /*   9 (LREF-UNBOX 0 0) */,
    0x0000000d    /*  10 (PUSH) */,
    0x00000045    /*  11 (LREF21) */,
    0x000000b4    /*  12 (NUMMUL2) */,
    0x000000b2    /*  13 (NUMADD2) */,
    0x00001018    /*  14 (PUSH-LOCAL-ENV 1) */,
    0x000010ea    /*  15 (LREF-UNBOX 1 0) */,
    0x000010bc    /*  16 (NUMADDI 1) */,
    0x0000103a    /*  17 (LSET 1 0) */,
    0x000020ea    /*  18 (LREF-UNBOX 2 0) */,
    -0x00000f44   /*  19 (NUMADDI -1) */,
    0x0000203a    /*  20 (LSET 2 0) */,
    0x00000053    /*  21 (LREF0-RET) */,
    /* liota */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 7),
    0x00000001    /*   3 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* +inf.0 */,
    0x00000013    /*   5 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 8),
    0x0000006a    /*   7 (LREF0-CAR) */,
    0x0000000d    /*   8 (PUSH) */,
    0x0000003d    /*   9 (LREF0) */,
    0x00000022    /*  10 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 15),
    0x00000003    /*  12 (CONSTN) */,
    0x00000013    /*  13 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 16),
    0x00000076    /*  15 (LREF0-CDR) */,
    0x00002018    /*  16 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  17 (LREF0) */,
    0x00000022    /*  18 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 23),
    0x00000002    /*  20 (CONSTI 0) */,
    0x00000013    /*  21 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 24),
    0x0000006a    /*  23 (LREF0-CAR) */,
    0x0000000d    /*  24 (PUSH) */,
    0x0000003d    /*  25 (LREF0) */,
    0x00000022    /*  26 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 31),
    0x00000003    /*  28 (CONSTN) */,
    0x00000013    /*  29 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 32),
    0x00000076    /*  31 (LREF0-CDR) */,
    0x00002018    /*  32 (PUSH-LOCAL-ENV 2) */,
    0x000020e7    /*  33 (BOX 2) */,
    0x0000003d    /*  34 (LREF0) */,
    0x00000022    /*  35 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 40),
    0x00001002    /*  37 (CONSTI 1) */,
    0x00000013    /*  38 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 41),
    0x0000006a    /*  40 (LREF0-CAR) */,
    0x0000000d    /*  41 (PUSH) */,
    0x0000003d    /*  42 (LREF0) */,
    0x00000022    /*  43 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 48),
    0x00000003    /*  45 (CONSTN) */,
    0x00000013    /*  46 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 49),
    0x00000076    /*  48 (LREF0-CDR) */,
    0x00002018    /*  49 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  50 (LREF0) */,
    0x00000022    /*  51 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 55),
    0x00000013    /*  53 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 63),
    0x0000200e    /*  55 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 63),
    0x00000006    /*  57 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[31])) /* "too many arguments for" */,
    0x00000006    /*  59 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[302])) /* (lambda (:optional (count +inf.0) (start 0) (step 1)) (let1 count (if (< count 0) +inf.0 count) (define gen (if (and (exact? start) (exact? step)) (if (infinite? count) (^ () (rlet1 v start (inc! start step))) (^ () (if (<= count 0) (eof-object) (rlet1 v start (inc! start step) (dec! count))))) (let1 k 0 (if (infinite? count) (^ () (rlet1 v (+ start (* k step)) (inc! k))) (^ () (if (<= count 0) (eof-object) (rlet1 v (+ start (* k step)) (inc! k) (dec! count)))))))) (generator->lseq gen))) */,
    0x0000205f    /*  61 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.e3753860> */,
    0x00000002    /*  63 (CONSTI 0) */,
    0x00402029    /*  64 (LREF-VAL0-BNLT 2 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 70),
    0x00000001    /*  66 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* +inf.0 */,
    0x00000013    /*  68 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 71),
    0x00000045    /*  70 (LREF21) */,
    0x00001018    /*  71 (PUSH-LOCAL-ENV 1) */,
    0x000010e7    /*  72 (BOX 1) */,
    0x0000100e    /*  73 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 78),
    0x004020ea    /*  75 (LREF-UNBOX 2 1) */,
    0x00001062    /*  76 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#exact?.e1bc48c0> */,
    0x0000001e    /*  78 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 102),
    0x0000100e    /*  80 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 85),
    0x0000004d    /*  82 (LREF11-PUSH) */,
    0x0000105f    /*  83 (GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#exact?.e1bc4860> */,
    0x0000001e    /*  85 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 102),
    0x0000100e    /*  87 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 92),
    0x000000ea    /*  89 (LREF-UNBOX 0 0) */,
    0x00001062    /*  90 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#infinite?.e1bc47e0> */,
    0x0000001e    /*  92 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 98),
    0x00000016    /*  94 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[8])) /* #<compiled-code (liota gen)@0x7f95e11b68a0> */,
    0x00000013    /*  96 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 123),
    0x00000016    /*  98 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[9])) /* #<compiled-code (liota gen)@0x7f95e11b6840> */,
    0x00000013    /* 100 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 123),
    0x00000007    /* 102 (CONSTI-PUSH 0) */,
    0x00001017    /* 103 (LOCAL-ENV 1) */,
    0x000010e7    /* 104 (BOX 1) */,
    0x0000100e    /* 105 (PRE-CALL 1) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 110),
    0x000010ea    /* 107 (LREF-UNBOX 1 0) */,
    0x00001062    /* 108 (PUSH-GREF-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#infinite?.e1bce4a0> */,
    0x0000001e    /* 110 (BF) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 116),
    0x00000016    /* 112 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[10])) /* #<compiled-code (liota gen)@0x7f95e11b67e0> */,
    0x00000013    /* 114 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 118),
    0x00000016    /* 116 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[11])) /* #<compiled-code (liota gen)@0x7f95e11b6780> */,
    0x0000001a    /* 118 (POP-LOCAL-ENV) */,
    0x00000013    /* 119 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 123),
    0x00000013    /* 121 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]) + 102),
    0x00001063    /* 123 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#generator->lseq.e1bd67c0> */,
    0x00000014    /* 125 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[399]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.e3753ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* liota */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[399]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[12])) /* #<compiled-code liota@0x7f95e11b6900> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#liota.e1babb00> */,
    0x00000014    /*  14 (RET) */,
    /* (port->char-lseq #f) */
    0x0000003e    /*   0 (LREF1) */,
    0x000010db    /*   1 (READ-CHAR 1) */,
    0x00000014    /*   2 (RET) */,
    /* port->char-lseq */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[417]) + 6),
    0x000000de    /*   3 (CURIN) */,
    0x00000013    /*   4 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[417]) + 7),
    0x0000006a    /*   6 (LREF0-CAR) */,
    0x0000000d    /*   7 (PUSH) */,
    0x0000003d    /*   8 (LREF0) */,
    0x00000022    /*   9 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[417]) + 14),
    0x00000003    /*  11 (CONSTN) */,
    0x00000013    /*  12 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[417]) + 15),
    0x00000076    /*  14 (LREF0-CDR) */,
    0x00002018    /*  15 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  16 (LREF0) */,
    0x00000022    /*  17 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[417]) + 21),
    0x00000013    /*  19 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[417]) + 29),
    0x0000200e    /*  21 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[417]) + 29),
    0x00000006    /*  23 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[31])) /* "too many arguments for" */,
    0x00000006    /*  25 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[341])) /* (lambda (:optional (port (current-input-port))) (generator->lseq (cut read-char port))) */,
    0x0000205f    /*  27 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.e3753860> */,
    0x00000016    /*  29 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[14])) /* #<compiled-code (port->char-lseq #f)@0x7f95e14ab180> */,
    0x00001063    /*  31 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#generator->lseq.e1647c80> */,
    0x00000014    /*  33 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[451]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.e3753ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* port->char-lseq */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[451]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[15])) /* #<compiled-code port->char-lseq@0x7f95e14ab1e0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port->char-lseq.e1643b40> */,
    0x00000014    /*  14 (RET) */,
    /* (port->byte-lseq #f) */
    0x00000049    /*   0 (LREF1-PUSH) */,
    0x00001060    /*   1 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read-byte.e2814a80> */,
    0x00000014    /*   3 (RET) */,
    /* port->byte-lseq */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[470]) + 6),
    0x000000de    /*   3 (CURIN) */,
    0x00000013    /*   4 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[470]) + 7),
    0x0000006a    /*   6 (LREF0-CAR) */,
    0x0000000d    /*   7 (PUSH) */,
    0x0000003d    /*   8 (LREF0) */,
    0x00000022    /*   9 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[470]) + 14),
    0x00000003    /*  11 (CONSTN) */,
    0x00000013    /*  12 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[470]) + 15),
    0x00000076    /*  14 (LREF0-CDR) */,
    0x00002018    /*  15 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  16 (LREF0) */,
    0x00000022    /*  17 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[470]) + 21),
    0x00000013    /*  19 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[470]) + 29),
    0x0000200e    /*  21 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[470]) + 29),
    0x00000006    /*  23 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[31])) /* "too many arguments for" */,
    0x00000006    /*  25 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[359])) /* (lambda (:optional (port (current-input-port))) (generator->lseq (cut read-byte port))) */,
    0x0000205f    /*  27 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.e3753860> */,
    0x00000016    /*  29 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[17])) /* #<compiled-code (port->byte-lseq #f)@0x7f95e2c05960> */,
    0x00001063    /*  31 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#generator->lseq.e2812400> */,
    0x00000014    /*  33 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[504]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.e3753ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* port->byte-lseq */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[504]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[18])) /* #<compiled-code port->byte-lseq@0x7f95e2c059c0> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port->byte-lseq.e2811c60> */,
    0x00000014    /*  14 (RET) */,
    /* (port->string-lseq #f) */
    0x00000049    /*   0 (LREF1-PUSH) */,
    0x00001060    /*   1 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read-line.e2443ea0> */,
    0x00000014    /*   3 (RET) */,
    /* port->string-lseq */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[523]) + 6),
    0x000000de    /*   3 (CURIN) */,
    0x00000013    /*   4 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[523]) + 7),
    0x0000006a    /*   6 (LREF0-CAR) */,
    0x0000000d    /*   7 (PUSH) */,
    0x0000003d    /*   8 (LREF0) */,
    0x00000022    /*   9 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[523]) + 14),
    0x00000003    /*  11 (CONSTN) */,
    0x00000013    /*  12 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[523]) + 15),
    0x00000076    /*  14 (LREF0-CDR) */,
    0x00002018    /*  15 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  16 (LREF0) */,
    0x00000022    /*  17 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[523]) + 21),
    0x00000013    /*  19 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[523]) + 29),
    0x0000200e    /*  21 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[523]) + 29),
    0x00000006    /*  23 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[31])) /* "too many arguments for" */,
    0x00000006    /*  25 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[377])) /* (lambda (:optional (port (current-input-port))) (generator->lseq (cut read-line port))) */,
    0x0000205f    /*  27 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.e3753860> */,
    0x00000016    /*  29 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[20])) /* #<compiled-code (port->string-lseq #f)@0x7f95e2010780> */,
    0x00001063    /*  31 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#generator->lseq.e2441380> */,
    0x00000014    /*  33 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[557]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.e3753ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* port->string-lseq */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[557]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[21])) /* #<compiled-code port->string-lseq@0x7f95e2010840> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port->string-lseq.e2439680> */,
    0x00000014    /*  14 (RET) */,
    /* (port->sexp-lseq #f) */
    0x00000049    /*   0 (LREF1-PUSH) */,
    0x00001060    /*   1 (GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#read.e26f1a20> */,
    0x00000014    /*   3 (RET) */,
    /* port->sexp-lseq */
    0x0000003d    /*   0 (LREF0) */,
    0x00000022    /*   1 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[576]) + 6),
    0x000000de    /*   3 (CURIN) */,
    0x00000013    /*   4 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[576]) + 7),
    0x0000006a    /*   6 (LREF0-CAR) */,
    0x0000000d    /*   7 (PUSH) */,
    0x0000003d    /*   8 (LREF0) */,
    0x00000022    /*   9 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[576]) + 14),
    0x00000003    /*  11 (CONSTN) */,
    0x00000013    /*  12 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[576]) + 15),
    0x00000076    /*  14 (LREF0-CDR) */,
    0x00002018    /*  15 (PUSH-LOCAL-ENV 2) */,
    0x0000003d    /*  16 (LREF0) */,
    0x00000022    /*  17 (BNNULL) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[576]) + 21),
    0x00000013    /*  19 (JUMP) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[576]) + 29),
    0x0000200e    /*  21 (PRE-CALL 2) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[576]) + 29),
    0x00000006    /*  23 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__sc.d1785[31])) /* "too many arguments for" */,
    0x00000006    /*  25 (CONST-PUSH) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1787[395])) /* (lambda (:optional (port (current-input-port))) (generator->lseq (cut read port))) */,
    0x0000205f    /*  27 (GREF-CALL 2) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche#error.e3753860> */,
    0x00000016    /*  29 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[23])) /* #<compiled-code (port->sexp-lseq #f)@0x7f95e229fcc0> */,
    0x00001063    /*  31 (PUSH-GREF-TAIL-CALL 1) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#generator->lseq.e26f1fc0> */,
    0x00000014    /*  33 (RET) */,
    /* %toplevel */
    0x0000000e    /*   0 (PRE-CALL 0) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[610]) + 4),
    0x0000005f    /*   2 (GREF-CALL 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier gauche.internal#%expression-name-mark-key.e3753ba0> */,
    0x0000000d    /*   4 (PUSH) */,
    0x00000001    /*   5 (CONST) */,
    SCM_WORD(SCM_UNDEFINED) /* port->sexp-lseq */,
    0x000000ee    /*   7 (EXTEND-DENV) */,
    SCM_WORD((ScmWord*)SCM_OBJ(&scm__rc.d1788[610]) + 12),
    0x00000016    /*   9 (CLOSURE) */,
    SCM_WORD(SCM_OBJ(&scm__rc.d1789[24])) /* #<compiled-code port->sexp-lseq@0x7f95e229fd20> */,
    0x00000014    /*  11 (RET) */,
    0x00000015    /*  12 (DEFINE 0) */,
    SCM_WORD(SCM_UNDEFINED) /* #<identifier #f#port->sexp-lseq.e26e2ba0> */,
    0x00000014    /*  14 (RET) */,
  },
  {   /* ScmPair d1787 */
       { SCM_NIL, SCM_NIL },
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(50U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[2])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[3])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[5])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[6])},
       { SCM_OBJ(&scm__rc.d1787[7]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[4]), SCM_OBJ(&scm__rc.d1787[8])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(53U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[11])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[12])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[14])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[15])},
       { SCM_OBJ(&scm__rc.d1787[16]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[13]), SCM_OBJ(&scm__rc.d1787[17])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[19])},
       { SCM_MAKE_INT(55U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[21])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[22])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[24])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[25])},
       { SCM_OBJ(&scm__rc.d1787[26]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[23]), SCM_OBJ(&scm__rc.d1787[27])},
       { SCM_MAKE_INT(57U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[29])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[30])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[32])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[33])},
       { SCM_OBJ(&scm__rc.d1787[34]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[31]), SCM_OBJ(&scm__rc.d1787[35])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[37])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[38])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[39])},
       { SCM_MAKE_INT(70U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[41])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[42])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[44])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[45])},
       { SCM_OBJ(&scm__rc.d1787[46]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[43]), SCM_OBJ(&scm__rc.d1787[47])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[49])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[50])},
       { SCM_MAKE_INT(82U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[52])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[53])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[55])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[56])},
       { SCM_OBJ(&scm__rc.d1787[57]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[54]), SCM_OBJ(&scm__rc.d1787[58])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[60])},
       { SCM_MAKE_INT(1U), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[62])},
       { SCM_OBJ(&scm__rc.d1787[63]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[61]), SCM_OBJ(&scm__rc.d1787[64])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[65])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[66])},
       { SCM_MAKE_INT(0), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[68])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[69])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[71])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[72])},
       { SCM_OBJ(&scm__rc.d1787[73]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[70]), SCM_OBJ(&scm__rc.d1787[74])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[75])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[69])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[72])},
       { SCM_OBJ(&scm__rc.d1787[78]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[77]), SCM_OBJ(&scm__rc.d1787[79])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[80])},
       { SCM_OBJ(&scm__rc.d1787[81]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[76]), SCM_OBJ(&scm__rc.d1787[82])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[83])},
       { SCM_NIL, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[85])},
       { SCM_OBJ(&scm__rc.d1787[86]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[84]), SCM_OBJ(&scm__rc.d1787[87])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[69])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_NIL, SCM_OBJ(&scm__rc.d1787[90])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[91])},
       { SCM_OBJ(&scm__rc.d1787[92]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[93])},
       { SCM_OBJ(&scm__rc.d1787[94]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[89]), SCM_OBJ(&scm__rc.d1787[95])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[90])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[98])},
       { SCM_OBJ(&scm__rc.d1787[99]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[97]), SCM_OBJ(&scm__rc.d1787[100])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[101])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[98])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[103])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[72])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[106]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[107])},
       { SCM_OBJ(&scm__rc.d1787[105]), SCM_OBJ(&scm__rc.d1787[108])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[109])},
       { SCM_OBJ(&scm__rc.d1787[110]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[104]), SCM_OBJ(&scm__rc.d1787[111])},
       { SCM_NIL, SCM_OBJ(&scm__rc.d1787[112])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[113])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[72])},
       { SCM_OBJ(&scm__rc.d1787[115]), SCM_OBJ(&scm__rc.d1787[108])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[116])},
       { SCM_OBJ(&scm__rc.d1787[117]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[104]), SCM_OBJ(&scm__rc.d1787[118])},
       { SCM_NIL, SCM_OBJ(&scm__rc.d1787[119])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[120])},
       { SCM_OBJ(&scm__rc.d1787[121]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[114]), SCM_OBJ(&scm__rc.d1787[122])},
       { SCM_OBJ(&scm__rc.d1787[70]), SCM_OBJ(&scm__rc.d1787[123])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[124])},
       { SCM_OBJ(&scm__rc.d1787[125]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[126])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[127])},
       { SCM_OBJ(&scm__rc.d1787[128]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[102]), SCM_OBJ(&scm__rc.d1787[129])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[90])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[132])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[98])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[134])},
       { SCM_OBJ(&scm__rc.d1787[135]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[136])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[137])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[71])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[139])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[107])},
       { SCM_OBJ(&scm__rc.d1787[140]), SCM_OBJ(&scm__rc.d1787[141])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[142])},
       { SCM_OBJ(&scm__rc.d1787[143]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[138]), SCM_OBJ(&scm__rc.d1787[144])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[145])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[146])},
       { SCM_OBJ(&scm__rc.d1787[147]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[133]), SCM_OBJ(&scm__rc.d1787[148])},
       { SCM_NIL, SCM_OBJ(&scm__rc.d1787[149])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[150])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[139])},
       { SCM_OBJ(&scm__rc.d1787[152]), SCM_OBJ(&scm__rc.d1787[141])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[153])},
       { SCM_OBJ(&scm__rc.d1787[154]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[138]), SCM_OBJ(&scm__rc.d1787[155])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[156])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[157])},
       { SCM_OBJ(&scm__rc.d1787[158]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[133]), SCM_OBJ(&scm__rc.d1787[159])},
       { SCM_NIL, SCM_OBJ(&scm__rc.d1787[160])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[161])},
       { SCM_OBJ(&scm__rc.d1787[162]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[151]), SCM_OBJ(&scm__rc.d1787[163])},
       { SCM_OBJ(&scm__rc.d1787[70]), SCM_OBJ(&scm__rc.d1787[164])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[165])},
       { SCM_OBJ(&scm__rc.d1787[166]), SCM_NIL},
       { SCM_MAKE_INT(0), SCM_OBJ(&scm__rc.d1787[167])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[168])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[169])},
       { SCM_OBJ(&scm__rc.d1787[170]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[131]), SCM_OBJ(&scm__rc.d1787[171])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[172])},
       { SCM_OBJ(&scm__rc.d1787[173]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[174])},
       { SCM_OBJ(&scm__rc.d1787[175]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[130]), SCM_OBJ(&scm__rc.d1787[176])},
       { SCM_OBJ(&scm__rc.d1787[96]), SCM_OBJ(&scm__rc.d1787[177])},
       { SCM_OBJ(&scm__rc.d1787[88]), SCM_OBJ(&scm__rc.d1787[178])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[179])},
       { SCM_OBJ(&scm__rc.d1787[180]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[67]), SCM_OBJ(&scm__rc.d1787[181])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[182])},
       { SCM_FALSE, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[184])},
       { SCM_MAKE_INT(101U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[186])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[187])},
       { SCM_OBJ(&scm__rc.d1787[188]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(105U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[191])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[192])},
       { SCM_OBJ(&scm__rc.d1787[193]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(107U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[196])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[197])},
       { SCM_OBJ(&scm__rc.d1787[198]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(113U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[201])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[202])},
       { SCM_OBJ(&scm__rc.d1787[203]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(116U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[206])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[207])},
       { SCM_OBJ(&scm__rc.d1787[208]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(98U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[211])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[212])},
       { SCM_OBJ(&scm__rc.d1787[213]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[60])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[68])},
       { SCM_OBJ(&scm__rc.d1787[217]), SCM_OBJ(&scm__rc.d1787[64])},
       { SCM_OBJ(&scm__rc.d1787[216]), SCM_OBJ(&scm__rc.d1787[218])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[219])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[68])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[221])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[223])},
       { SCM_OBJ(&scm__rc.d1787[222]), SCM_OBJ(&scm__rc.d1787[224])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[225])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[223])},
       { SCM_OBJ(&scm__rc.d1787[104]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[228])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[229])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[230])},
       { SCM_OBJ(&scm__rc.d1787[231]), SCM_NIL},
       { SCM_NIL, SCM_OBJ(&scm__rc.d1787[232])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[233])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[221])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[223])},
       { SCM_OBJ(&scm__rc.d1787[236]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[104]), SCM_OBJ(&scm__rc.d1787[237])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[238])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[239])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[240])},
       { SCM_OBJ(&scm__rc.d1787[241]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[106]), SCM_OBJ(&scm__rc.d1787[242])},
       { SCM_OBJ(&scm__rc.d1787[235]), SCM_OBJ(&scm__rc.d1787[243])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[244])},
       { SCM_OBJ(&scm__rc.d1787[245]), SCM_NIL},
       { SCM_NIL, SCM_OBJ(&scm__rc.d1787[246])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[247])},
       { SCM_OBJ(&scm__rc.d1787[248]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[234]), SCM_OBJ(&scm__rc.d1787[249])},
       { SCM_OBJ(&scm__rc.d1787[227]), SCM_OBJ(&scm__rc.d1787[250])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[251])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[98])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[253])},
       { SCM_OBJ(&scm__rc.d1787[254]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[255])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[256])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[258])},
       { SCM_OBJ(&scm__rc.d1787[259]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[257]), SCM_OBJ(&scm__rc.d1787[260])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[261])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[262])},
       { SCM_OBJ(&scm__rc.d1787[263]), SCM_NIL},
       { SCM_NIL, SCM_OBJ(&scm__rc.d1787[264])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[265])},
       { SCM_OBJ(&scm__rc.d1787[259]), SCM_OBJ(&scm__rc.d1787[237])},
       { SCM_OBJ(&scm__rc.d1787[257]), SCM_OBJ(&scm__rc.d1787[267])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[268])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[269])},
       { SCM_OBJ(&scm__rc.d1787[270]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[106]), SCM_OBJ(&scm__rc.d1787[271])},
       { SCM_OBJ(&scm__rc.d1787[235]), SCM_OBJ(&scm__rc.d1787[272])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[273])},
       { SCM_OBJ(&scm__rc.d1787[274]), SCM_NIL},
       { SCM_NIL, SCM_OBJ(&scm__rc.d1787[275])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[276])},
       { SCM_OBJ(&scm__rc.d1787[277]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[266]), SCM_OBJ(&scm__rc.d1787[278])},
       { SCM_OBJ(&scm__rc.d1787[227]), SCM_OBJ(&scm__rc.d1787[279])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[280])},
       { SCM_OBJ(&scm__rc.d1787[281]), SCM_NIL},
       { SCM_MAKE_INT(0), SCM_OBJ(&scm__rc.d1787[282])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[283])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[284])},
       { SCM_OBJ(&scm__rc.d1787[285]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[252]), SCM_OBJ(&scm__rc.d1787[286])},
       { SCM_OBJ(&scm__rc.d1787[102]), SCM_OBJ(&scm__rc.d1787[287])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[288])},
       { SCM_OBJ(&scm__rc.d1787[289]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[290])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[291])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[293])},
       { SCM_OBJ(&scm__rc.d1787[294]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[292]), SCM_OBJ(&scm__rc.d1787[295])},
       { SCM_OBJ(&scm__rc.d1787[226]), SCM_OBJ(&scm__rc.d1787[296])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[297])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[298])},
       { SCM_OBJ(&scm__rc.d1787[299]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[220]), SCM_OBJ(&scm__rc.d1787[300])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[301])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[293])},
       { SCM_MAKE_INT(126U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[304])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[305])},
       { SCM_OBJ(&scm__rc.d1787[306]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(127U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[309])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[310])},
       { SCM_OBJ(&scm__rc.d1787[311]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(132U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[314])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[315])},
       { SCM_OBJ(&scm__rc.d1787[316]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(133U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[319])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[320])},
       { SCM_OBJ(&scm__rc.d1787[321]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(121U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[324])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[325])},
       { SCM_OBJ(&scm__rc.d1787[326]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[329]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[330])},
       { SCM_OBJ(&scm__rc.d1787[331]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[332])},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[334])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[335])},
       { SCM_OBJ(&scm__rc.d1787[336]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[337])},
       { SCM_OBJ(&scm__rc.d1787[338]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[333]), SCM_OBJ(&scm__rc.d1787[339])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[340])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[184])},
       { SCM_MAKE_INT(139U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[343])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[344])},
       { SCM_OBJ(&scm__rc.d1787[345]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(138U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[348])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[349])},
       { SCM_OBJ(&scm__rc.d1787[350]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[334])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[353])},
       { SCM_OBJ(&scm__rc.d1787[354]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[355])},
       { SCM_OBJ(&scm__rc.d1787[356]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[333]), SCM_OBJ(&scm__rc.d1787[357])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[358])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[184])},
       { SCM_MAKE_INT(141U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[361])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[362])},
       { SCM_OBJ(&scm__rc.d1787[363]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(140U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[366])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[367])},
       { SCM_OBJ(&scm__rc.d1787[368]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[334])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[371])},
       { SCM_OBJ(&scm__rc.d1787[372]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[373])},
       { SCM_OBJ(&scm__rc.d1787[374]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[333]), SCM_OBJ(&scm__rc.d1787[375])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[376])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[184])},
       { SCM_MAKE_INT(143U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[379])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[380])},
       { SCM_OBJ(&scm__rc.d1787[381]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(142U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[384])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[385])},
       { SCM_OBJ(&scm__rc.d1787[386]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[334])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[389])},
       { SCM_OBJ(&scm__rc.d1787[390]), SCM_NIL},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[391])},
       { SCM_OBJ(&scm__rc.d1787[392]), SCM_NIL},
       { SCM_OBJ(&scm__rc.d1787[333]), SCM_OBJ(&scm__rc.d1787[393])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[394])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[184])},
       { SCM_MAKE_INT(145U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[397])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[398])},
       { SCM_OBJ(&scm__rc.d1787[399]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
       { SCM_MAKE_INT(144U), SCM_NIL},
       { SCM_OBJ(&scm__sc.d1785[3]), SCM_OBJ(&scm__rc.d1787[402])},
       { SCM_UNDEFINED, SCM_OBJ(&scm__rc.d1787[403])},
       { SCM_OBJ(&scm__rc.d1787[404]), SCM_NIL},
       { SCM_UNDEFINED, SCM_NIL},
  },
  {   /* ScmObj d1786 */
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(5, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(7, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(6, FALSE),
    SCM_MAKE_INT(1U),
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNDEFINED,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_OBJ(SCM_CLASS_STATIC_TAG(Scm_VectorClass)) /* <vector> */,
    SCM_VECTOR_SIZE_SLOT_INITIALIZER(63, FALSE),
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_FALSE,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
    SCM_UNBOUND,
  },
};

static ScmObj liblazyforce(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj p_scm;
  ScmObj p;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("force");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  p_scm = SCM_SUBRARGS[0];
  if (!(p_scm)) Scm_Error("scheme object required, but got %S", p_scm);
  p = (p_scm);
  {
{
ScmObj SCM_RESULT;
{SCM_RESULT=(Scm_VMForce(p));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj liblazypromiseP(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("promise?");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  obj_scm = SCM_SUBRARGS[0];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {
{
int SCM_RESULT;

#line 54 "liblazy.scm"
{SCM_RESULT=(SCM_XTYPEP(obj,SCM_CLASS_PROMISE));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_MAKE_BOOL(SCM_RESULT));
}
  }
}


static ScmObj liblazyeager(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj objs_scm;
  ScmObj objs;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("eager");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  objs_scm = SCM_SUBRARGS[SCM_ARGCNT-1];
  if (!SCM_LISTP(objs_scm)) Scm_Error("list required, but got %S", objs_scm);
  objs = (objs_scm);
  {
{
ScmObj SCM_RESULT;

#line 56 "liblazy.scm"
{SCM_RESULT=(Scm_MakePromise(SCM_PROMISE_FORCED,objs));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj liblazypromise_kind_SETTER(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj p_scm;
  ScmPromise* p;
  ScmObj obj_scm;
  ScmObj obj;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("(setter promise-kind)");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  p_scm = SCM_SUBRARGS[0];
  if (!SCM_PROMISEP(p_scm)) Scm_Error("<promise> required, but got %S", p_scm);
  p = SCM_PROMISE(p_scm);
  obj_scm = SCM_SUBRARGS[1];
  if (!(obj_scm)) Scm_Error("scheme object required, but got %S", obj_scm);
  obj = (obj_scm);
  {

#line 58 "liblazy.scm"
(p)->kind=(obj);
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_UNDEFINED);
  }
}


static ScmObj liblazypromise_kind(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj p_scm;
  ScmPromise* p;
  ScmObj SCM_SUBRARGS[1];
  SCM_ENTER_SUBR("promise-kind");
  for (int SCM_i=0; SCM_i<1; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  p_scm = SCM_SUBRARGS[0];
  if (!SCM_PROMISEP(p_scm)) Scm_Error("<promise> required, but got %S", p_scm);
  p = SCM_PROMISE(p_scm);
  {
{
ScmObj SCM_RESULT;

#line 59 "liblazy.scm"
{SCM_RESULT=((p)->kind);goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj liblazy_25lcons(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj item_scm;
  ScmObj item;
  ScmObj thunk_scm;
  ScmObj thunk;
  ScmObj attrs_scm;
  ScmObj attrs;
  ScmObj SCM_SUBRARGS[3];
  SCM_ENTER_SUBR("%lcons");
  if (SCM_ARGCNT >= 4
      && !SCM_NULLP(SCM_ARGREF(SCM_ARGCNT-1)))
    Scm_Error("too many arguments: up to 3 is expected, %d given.", SCM_ARGCNT + Scm_Length(SCM_ARGREF(SCM_ARGCNT-1)) - 1);
  for (int SCM_i=0; SCM_i<3; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  item_scm = SCM_SUBRARGS[0];
  if (!(item_scm)) Scm_Error("scheme object required, but got %S", item_scm);
  item = (item_scm);
  thunk_scm = SCM_SUBRARGS[1];
  if (!(thunk_scm)) Scm_Error("scheme object required, but got %S", thunk_scm);
  thunk = (thunk_scm);
  if (SCM_ARGCNT > 2+1) {
    attrs_scm = SCM_SUBRARGS[2];
  } else {
    attrs_scm = SCM_NIL;
  }
  if (!(attrs_scm)) Scm_Error("scheme object required, but got %S", attrs_scm);
  attrs = (attrs_scm);
  {
{
ScmObj SCM_RESULT;

#line 71 "liblazy.scm"
{SCM_RESULT=(Scm_LazyCons(item,thunk,attrs));goto SCM_STUB_RETURN;}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}


static ScmObj liblazygenerator_TOlseq(ScmObj *SCM_FP SCM_UNUSED, int SCM_ARGCNT SCM_UNUSED, void *data_ SCM_UNUSED)
{
  ScmObj item_scm;
  ScmObj item;
  ScmObj args_scm;
  ScmObj args;
  ScmObj SCM_SUBRARGS[2];
  SCM_ENTER_SUBR("generator->lseq");
  for (int SCM_i=0; SCM_i<2; SCM_i++) {
    SCM_SUBRARGS[SCM_i] = SCM_ARGREF(SCM_i);
  }
  item_scm = SCM_SUBRARGS[0];
  if (!(item_scm)) Scm_Error("scheme object required, but got %S", item_scm);
  item = (item_scm);
  args_scm = SCM_SUBRARGS[SCM_ARGCNT-1];
  if (!SCM_LISTP(args_scm)) Scm_Error("list required, but got %S", args_scm);
  args = (args_scm);
  {
{
ScmObj SCM_RESULT;

#line 83 "liblazy.scm"
if (SCM_NULLP(args)){
{SCM_RESULT=(Scm_GeneratorToLazyPair(item));goto SCM_STUB_RETURN;}} else {
{ScmObj h=SCM_NIL;ScmObj t=SCM_NIL;
for (;;){
if (SCM_NULLP(SCM_CDR(args))){{
if (SCM_NULLP(t)){
{SCM_RESULT=(Scm_MakeLazyPair(item,SCM_CAR(args),SCM_NIL));goto SCM_STUB_RETURN;}} else {
{
SCM_SET_CDR(t,Scm_MakeLazyPair(item,SCM_CAR(args),SCM_NIL));
{SCM_RESULT=(h);goto SCM_STUB_RETURN;}}}}}
SCM_APPEND1(h,t,item);
item=(SCM_CAR(args));
args=(SCM_CDR(args));}}}
goto SCM_STUB_RETURN;
SCM_STUB_RETURN:
SCM_RETURN(SCM_OBJ_SAFE(SCM_RESULT));
}
  }
}

static ScmCompiledCode *toplevels[] = {
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[0])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[7])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[13])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[16])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[19])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[22])),
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[25])),
 NULL /*termination*/
};
ScmObj SCM_debug_info_const_vector()
{
  static _Bool initialized = FALSE;
  if (!initialized) {
    int i = 0;
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[51];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[27];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[87];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[95];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[88];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[89];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[102];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[103];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[93];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[108];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[31];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[107];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[73];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[105];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[100];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[98];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[94];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[96];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = SCM_OBJ(&scm__sc.d1785[31]);
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[60];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[85];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[254];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[255];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[256];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[257];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[84];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[135];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[134];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[130];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[258];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[136];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[137];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[132];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[133];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[259];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[260];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[261];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[262];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[263];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[129];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[155];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[152];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[154];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[153];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[264];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[265];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[266];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[151];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[163];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[267];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[268];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[269];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[162];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[172];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[270];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[271];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[272];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[171];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[181];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[273];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[274];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[275];
    SCM_VECTOR_ELEMENT(SCM_OBJ(&scm__rc.d1786[189]), i++) = scm__rc.d1786[180];
    initialized = TRUE;
  }
  return SCM_OBJ(&scm__rc.d1786[189]);
}
void Scm_Init_liblazy() {
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())));
  scm__rc.d1786[0] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[0])),TRUE); /* force */
  scm__rc.d1786[1] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[1])),TRUE); /* p */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[1]), scm__rc.d1786[1]);
  scm__rc.d1786[2] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[2])),TRUE); /* source-info */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[4]), scm__rc.d1786[2]);
  scm__rc.d1786[3] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[4])),TRUE); /* bind-info */
  scm__rc.d1786[4] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[5])),TRUE); /* scheme */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[5]), scm__rc.d1786[0]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[6]), scm__rc.d1786[4]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[7]), scm__rc.d1786[3]);
  scm__rc.d1786[5] = Scm_MakeExtendedPair(scm__rc.d1786[0], SCM_OBJ(&scm__rc.d1787[1]), SCM_OBJ(&scm__rc.d1787[9]));
  scm__rc.d1786[6] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[6])),TRUE); /* <top> */
  scm__rc.d1786[7] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[7])),TRUE); /* -> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[8]))[3] = scm__rc.d1786[4];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[8]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[8]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[8]))[6] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_SchemeModule())), SCM_SYMBOL(SCM_INTERN("force")), SCM_OBJ(&liblazyforce__STUB), 0);
  liblazyforce__STUB.common.info = scm__rc.d1786[5];
  liblazyforce__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[8]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[15] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[8])),TRUE); /* promise? */
  scm__rc.d1786[16] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[9])),TRUE); /* obj */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[10]), scm__rc.d1786[16]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[13]), scm__rc.d1786[2]);
  scm__rc.d1786[17] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[10])),TRUE); /* gauche */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[14]), scm__rc.d1786[15]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[15]), scm__rc.d1786[17]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[16]), scm__rc.d1786[3]);
  scm__rc.d1786[18] = Scm_MakeExtendedPair(scm__rc.d1786[15], SCM_OBJ(&scm__rc.d1787[10]), SCM_OBJ(&scm__rc.d1787[18]));
  scm__rc.d1786[19] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[11])),TRUE); /* <boolean> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[20]))[3] = scm__rc.d1786[17];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[20]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[20]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[20]))[6] = scm__rc.d1786[19];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("promise?")), SCM_OBJ(&liblazypromiseP__STUB), SCM_BINDING_INLINABLE);
  liblazypromiseP__STUB.common.info = scm__rc.d1786[18];
  liblazypromiseP__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[20]);
  scm__rc.d1786[27] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[12])),TRUE); /* eager */
  scm__rc.d1786[28] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[13]))); /* :rest */
  scm__rc.d1786[29] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[14])),TRUE); /* objs */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[19]), scm__rc.d1786[29]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[20]), scm__rc.d1786[28]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[23]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[24]), scm__rc.d1786[27]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[25]), scm__rc.d1786[17]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[26]), scm__rc.d1786[3]);
  scm__rc.d1786[30] = Scm_MakeExtendedPair(scm__rc.d1786[27], SCM_OBJ(&scm__rc.d1787[20]), SCM_OBJ(&scm__rc.d1787[28]));
  scm__rc.d1786[31] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[15])),TRUE); /* * */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[32]))[3] = scm__rc.d1786[17];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[32]))[4] = scm__rc.d1786[31];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[32]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[32]))[6] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("eager")), SCM_OBJ(&liblazyeager__STUB), 0);
  liblazyeager__STUB.common.info = scm__rc.d1786[30];
  liblazyeager__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[32]);
  scm__rc.d1786[39] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[16])),TRUE); /* promise-kind */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[31]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[32]), scm__rc.d1786[39]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[33]), scm__rc.d1786[17]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[34]), scm__rc.d1786[3]);
  scm__rc.d1786[40] = Scm_MakeExtendedPair(scm__rc.d1786[39], SCM_OBJ(&scm__rc.d1787[1]), SCM_OBJ(&scm__rc.d1787[36]));
  scm__rc.d1786[41] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[18])),TRUE); /* <promise> */
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[42]))[3] = scm__rc.d1786[17];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[42]))[4] = scm__rc.d1786[41];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[42]))[5] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[42]))[6] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("promise-kind")), SCM_OBJ(&liblazypromise_kind__STUB), 0);
  liblazypromise_kind__STUB.common.info = scm__rc.d1786[40];
  liblazypromise_kind__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[42]);
  Scm_SetterSet(SCM_PROCEDURE(&liblazypromise_kind__STUB), SCM_PROCEDURE(&liblazypromise_kind_SETTER__STUB), TRUE);
  scm__rc.d1786[50] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[19])),TRUE); /* %expression-name-mark-key */
  scm__rc.d1786[49] = Scm_MakeIdentifier(scm__rc.d1786[50], SCM_MODULE(Scm_GaucheInternalModule()), SCM_NIL); /* gauche.internal#%expression-name-mark-key */
  scm__rc.d1786[51] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[20])),TRUE); /* make-promise */
  scm__rc.d1786[53] = SCM_OBJ(Scm_FindModule(SCM_SYMBOL(scm__rc.d1786[17]), SCM_FIND_MODULE_CREATE|SCM_FIND_MODULE_PLACEHOLDING)); /* module gauche */
  scm__rc.d1786[52] = Scm_MakeIdentifier(scm__rc.d1786[27], SCM_MODULE(scm__rc.d1786[53]), SCM_NIL); /* gauche#eager */
  scm__rc.d1786[54] = Scm_MakeIdentifier(scm__rc.d1786[51], SCM_MODULE(scm__rc.d1786[53]), SCM_NIL); /* gauche#make-promise */
  scm__rc.d1786[55] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[21])),TRUE); /* %toplevel */
  scm__rc.d1786[56] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[0])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[0]))->name = scm__rc.d1786[55];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[0]))->debugInfo = scm__rc.d1786[56];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[0]))[3] = SCM_WORD(scm__rc.d1786[49]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[0]))[6] = SCM_WORD(scm__rc.d1786[51]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[0]))[10] = SCM_WORD(scm__rc.d1786[52]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[0]))[13] = SCM_WORD(scm__rc.d1786[54]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())));
  scm__rc.d1786[57] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[22])),TRUE); /* %lcons */
  scm__rc.d1786[58] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[23])),TRUE); /* item */
  scm__rc.d1786[59] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[24])),TRUE); /* thunk */
  scm__rc.d1786[60] = Scm_MakeKeyword(SCM_STRING(SCM_OBJ(&scm__sc.d1785[25]))); /* :optional */
  scm__rc.d1786[61] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[26])),TRUE); /* attrs */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[37]), scm__rc.d1786[61]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[38]), scm__rc.d1786[60]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[39]), scm__rc.d1786[59]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[40]), scm__rc.d1786[58]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[43]), scm__rc.d1786[2]);
  scm__rc.d1786[62] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[27])),TRUE); /* gauche.internal */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[44]), scm__rc.d1786[57]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[45]), scm__rc.d1786[62]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[46]), scm__rc.d1786[3]);
  scm__rc.d1786[63] = Scm_MakeExtendedPair(scm__rc.d1786[57], SCM_OBJ(&scm__rc.d1787[40]), SCM_OBJ(&scm__rc.d1787[48]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[64]))[3] = scm__rc.d1786[62];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[64]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[64]))[5] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[64]))[6] = scm__rc.d1786[31];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[64]))[7] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[64]))[8] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheInternalModule())), SCM_SYMBOL(SCM_INTERN("%lcons")), SCM_OBJ(&liblazy_25lcons__STUB), 0);
  liblazy_25lcons__STUB.common.info = scm__rc.d1786[63];
  liblazy_25lcons__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[64]);
   Scm_SelectModule(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())));
  scm__rc.d1786[73] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[28])),TRUE); /* generator->lseq */
  scm__rc.d1786[74] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[29])),TRUE); /* args */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[49]), scm__rc.d1786[74]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[50]), scm__rc.d1786[28]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[51]), scm__rc.d1786[58]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[54]), scm__rc.d1786[2]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[55]), scm__rc.d1786[73]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[56]), scm__rc.d1786[17]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[57]), scm__rc.d1786[3]);
  scm__rc.d1786[75] = Scm_MakeExtendedPair(scm__rc.d1786[73], SCM_OBJ(&scm__rc.d1787[51]), SCM_OBJ(&scm__rc.d1787[59]));
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[76]))[3] = scm__rc.d1786[17];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[76]))[4] = scm__rc.d1786[6];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[76]))[5] = scm__rc.d1786[31];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[76]))[6] = scm__rc.d1786[7];
  ((ScmObj*)SCM_OBJ(&scm__rc.d1786[76]))[7] = scm__rc.d1786[6];
  Scm_MakeBinding(SCM_MODULE(SCM_OBJ(Scm_GaucheModule())), SCM_SYMBOL(SCM_INTERN("generator->lseq")), SCM_OBJ(&liblazygenerator_TOlseq__STUB), 0);
  liblazygenerator_TOlseq__STUB.common.info = scm__rc.d1786[75];
  liblazygenerator_TOlseq__STUB.common.typeHint = SCM_OBJ(&scm__rc.d1786[76]);
  scm__rc.d1786[84] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[30])),TRUE); /* lrange */
  scm__rc.d1786[85] = 
SCM_POSITIVE_INFINITY;
  scm__rc.d1786[86] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[32])),TRUE); /* lambda */
  scm__rc.d1786[87] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[33])),TRUE); /* start */
  scm__rc.d1786[88] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[34])),TRUE); /* end */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[60]), scm__rc.d1786[85]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[61]), scm__rc.d1786[88]);
  scm__rc.d1786[89] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[35])),TRUE); /* step */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[63]), scm__rc.d1786[89]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[66]), scm__rc.d1786[60]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[67]), scm__rc.d1786[87]);
  scm__rc.d1786[90] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[36])),TRUE); /* cond */
  scm__rc.d1786[91] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[37])),TRUE); /* or */
  scm__rc.d1786[92] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[38])),TRUE); /* and */
  scm__rc.d1786[93] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[39])),TRUE); /* > */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[69]), scm__rc.d1786[89]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[70]), scm__rc.d1786[93]);
  scm__rc.d1786[94] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[40])),TRUE); /* >= */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[71]), scm__rc.d1786[88]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[72]), scm__rc.d1786[87]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[73]), scm__rc.d1786[94]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[76]), scm__rc.d1786[92]);
  scm__rc.d1786[95] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[41])),TRUE); /* < */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[77]), scm__rc.d1786[95]);
  scm__rc.d1786[96] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[42])),TRUE); /* <= */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[78]), scm__rc.d1786[96]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[81]), scm__rc.d1786[92]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[84]), scm__rc.d1786[91]);
  scm__rc.d1786[97] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[43])),TRUE); /* quote */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[86]), scm__rc.d1786[97]);
  scm__rc.d1786[98] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[44])),TRUE); /* = */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[89]), scm__rc.d1786[98]);
  scm__rc.d1786[99] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[45])),TRUE); /* ^ */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[90]), scm__rc.d1786[87]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[92]), scm__rc.d1786[99]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[94]), scm__rc.d1786[73]);
  scm__rc.d1786[100] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[46])),TRUE); /* exact? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[97]), scm__rc.d1786[100]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[98]), scm__rc.d1786[89]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[99]), scm__rc.d1786[100]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[102]), scm__rc.d1786[92]);
  scm__rc.d1786[101] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[47])),TRUE); /* if */
  scm__rc.d1786[102] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[48])),TRUE); /* inc! */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[103]), scm__rc.d1786[87]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[104]), scm__rc.d1786[102]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[105]), scm__rc.d1786[95]);
  scm__rc.d1786[103] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[49])),TRUE); /* eof-object */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[106]), scm__rc.d1786[103]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[108]), scm__rc.d1786[87]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[110]), scm__rc.d1786[101]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[114]), scm__rc.d1786[99]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[115]), scm__rc.d1786[93]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[117]), scm__rc.d1786[101]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[121]), scm__rc.d1786[99]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[125]), scm__rc.d1786[101]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[127]), scm__rc.d1786[87]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[128]), scm__rc.d1786[73]);
  scm__rc.d1786[104] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[50])),TRUE); /* else */
  scm__rc.d1786[105] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[51])),TRUE); /* inexact */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[131]), scm__rc.d1786[105]);
  scm__rc.d1786[106] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[52])),TRUE); /* let1 */
  scm__rc.d1786[107] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[53])),TRUE); /* c */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[132]), scm__rc.d1786[107]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[133]), scm__rc.d1786[102]);
  scm__rc.d1786[108] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[54])),TRUE); /* r */
  scm__rc.d1786[109] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[55])),TRUE); /* + */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[134]), scm__rc.d1786[107]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[135]), scm__rc.d1786[31]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[137]), scm__rc.d1786[87]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[138]), scm__rc.d1786[109]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[139]), scm__rc.d1786[108]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[140]), scm__rc.d1786[95]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[141]), scm__rc.d1786[108]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[143]), scm__rc.d1786[101]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[146]), scm__rc.d1786[108]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[147]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[151]), scm__rc.d1786[99]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[152]), scm__rc.d1786[93]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[154]), scm__rc.d1786[101]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[157]), scm__rc.d1786[108]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[158]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[162]), scm__rc.d1786[99]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[166]), scm__rc.d1786[101]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[169]), scm__rc.d1786[107]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[170]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[173]), scm__rc.d1786[73]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[175]), scm__rc.d1786[104]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[180]), scm__rc.d1786[90]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[183]), scm__rc.d1786[86]);
  scm__rc.d1786[111] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[56])),TRUE); /* error */
  scm__rc.d1786[110] = Scm_MakeIdentifier(scm__rc.d1786[111], SCM_MODULE(Scm_GaucheModule()), SCM_NIL); /* gauche#error */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[185]), scm__rc.d1786[84]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[188]), scm__rc.d1786[2]);
  scm__rc.d1786[112] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[185]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[189]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[190]), scm__rc.d1786[112]);
  scm__rc.d1786[113] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[1])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[1]))->debugInfo = scm__rc.d1786[113];
  scm__rc.d1786[114] = Scm_MakeIdentifier(scm__rc.d1786[73], SCM_MODULE(scm__rc.d1786[53]), SCM_NIL); /* gauche#generator->lseq */
  scm__rc.d1786[115] = Scm_MakeIdentifier(scm__rc.d1786[100], SCM_MODULE(scm__rc.d1786[53]), SCM_NIL); /* gauche#exact? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[193]), scm__rc.d1786[2]);
  scm__rc.d1786[116] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[185]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[194]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[195]), scm__rc.d1786[116]);
  scm__rc.d1786[117] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[2])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[2]))->debugInfo = scm__rc.d1786[117];
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[198]), scm__rc.d1786[2]);
  scm__rc.d1786[118] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[185]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[199]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[200]), scm__rc.d1786[118]);
  scm__rc.d1786[119] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[3])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[3]))->debugInfo = scm__rc.d1786[119];
  scm__rc.d1786[120] = Scm_MakeIdentifier(scm__rc.d1786[105], SCM_MODULE(scm__rc.d1786[53]), SCM_NIL); /* gauche#inexact */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[203]), scm__rc.d1786[2]);
  scm__rc.d1786[121] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[185]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[204]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[205]), scm__rc.d1786[121]);
  scm__rc.d1786[122] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[4])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[4]))->debugInfo = scm__rc.d1786[122];
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[208]), scm__rc.d1786[2]);
  scm__rc.d1786[123] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[185]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[209]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[210]), scm__rc.d1786[123]);
  scm__rc.d1786[124] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[5])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[5]))->debugInfo = scm__rc.d1786[124];
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[213]), scm__rc.d1786[2]);
  scm__rc.d1786[125] = Scm_MakeExtendedPair(scm__rc.d1786[84], SCM_OBJ(&scm__rc.d1787[67]), SCM_OBJ(&scm__rc.d1787[214]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[215]), scm__rc.d1786[125]);
  scm__rc.d1786[126] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[6])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[6]))->name = scm__rc.d1786[84];/* lrange */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[6]))->debugInfo = scm__rc.d1786[126];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]))[5] = SCM_WORD(scm__rc.d1786[85]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]))[46] = SCM_WORD(scm__rc.d1786[110]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]))[76] = SCM_WORD(scm__rc.d1786[114]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]))[82] = SCM_WORD(scm__rc.d1786[115]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]))[89] = SCM_WORD(scm__rc.d1786[115]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]))[104] = SCM_WORD(scm__rc.d1786[114]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]))[110] = SCM_WORD(scm__rc.d1786[120]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[71]))[126] = SCM_WORD(scm__rc.d1786[114]);
  scm__rc.d1786[127] = Scm_MakeIdentifier(scm__rc.d1786[84], SCM_MODULE(scm__rc.d1786[53]), SCM_NIL); /* gauche#lrange */
  scm__rc.d1786[128] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[7])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[7]))->name = scm__rc.d1786[55];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[7]))->debugInfo = scm__rc.d1786[128];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[202]))[3] = SCM_WORD(scm__rc.d1786[49]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[202]))[6] = SCM_WORD(scm__rc.d1786[84]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[202]))[13] = SCM_WORD(scm__rc.d1786[127]);
  scm__rc.d1786[129] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[57])),TRUE); /* liota */
  scm__rc.d1786[130] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[58])),TRUE); /* count */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[216]), scm__rc.d1786[130]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[217]), scm__rc.d1786[87]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[220]), scm__rc.d1786[60]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[221]), scm__rc.d1786[130]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[222]), scm__rc.d1786[95]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[223]), scm__rc.d1786[130]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[224]), scm__rc.d1786[85]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[226]), scm__rc.d1786[101]);
  scm__rc.d1786[131] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[59])),TRUE); /* define */
  scm__rc.d1786[132] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[60])),TRUE); /* gen */
  scm__rc.d1786[133] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[61])),TRUE); /* infinite? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[227]), scm__rc.d1786[133]);
  scm__rc.d1786[134] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[62])),TRUE); /* rlet1 */
  scm__rc.d1786[135] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[63])),TRUE); /* v */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[229]), scm__rc.d1786[87]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[230]), scm__rc.d1786[135]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[231]), scm__rc.d1786[134]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[234]), scm__rc.d1786[99]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[235]), scm__rc.d1786[96]);
  scm__rc.d1786[136] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[64])),TRUE); /* dec! */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[236]), scm__rc.d1786[136]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[239]), scm__rc.d1786[87]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[240]), scm__rc.d1786[135]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[241]), scm__rc.d1786[134]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[245]), scm__rc.d1786[101]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[248]), scm__rc.d1786[99]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[252]), scm__rc.d1786[101]);
  scm__rc.d1786[137] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[65])),TRUE); /* k */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[253]), scm__rc.d1786[137]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[254]), scm__rc.d1786[31]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[256]), scm__rc.d1786[87]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[257]), scm__rc.d1786[109]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[258]), scm__rc.d1786[137]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[259]), scm__rc.d1786[102]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[262]), scm__rc.d1786[135]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[263]), scm__rc.d1786[134]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[266]), scm__rc.d1786[99]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[269]), scm__rc.d1786[135]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[270]), scm__rc.d1786[134]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[274]), scm__rc.d1786[101]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[277]), scm__rc.d1786[99]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[281]), scm__rc.d1786[101]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[284]), scm__rc.d1786[137]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[285]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[289]), scm__rc.d1786[101]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[291]), scm__rc.d1786[132]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[292]), scm__rc.d1786[131]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[293]), scm__rc.d1786[132]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[294]), scm__rc.d1786[73]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[298]), scm__rc.d1786[130]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[299]), scm__rc.d1786[106]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[302]), scm__rc.d1786[86]);
  scm__rc.d1786[138] = Scm_MakeIdentifier(scm__rc.d1786[133], SCM_MODULE(scm__rc.d1786[53]), SCM_NIL); /* gauche#infinite? */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[303]), scm__rc.d1786[129]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[306]), scm__rc.d1786[2]);
  scm__rc.d1786[139] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[303]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[307]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[308]), scm__rc.d1786[139]);
  scm__rc.d1786[140] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[8])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[8]))->name = scm__rc.d1786[132];/* (liota gen) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[8]))->debugInfo = scm__rc.d1786[140];
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[311]), scm__rc.d1786[2]);
  scm__rc.d1786[141] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[303]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[312]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[313]), scm__rc.d1786[141]);
  scm__rc.d1786[142] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[9])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[9]))->name = scm__rc.d1786[132];/* (liota gen) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[9]))->debugInfo = scm__rc.d1786[142];
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[316]), scm__rc.d1786[2]);
  scm__rc.d1786[143] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[303]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[317]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[318]), scm__rc.d1786[143]);
  scm__rc.d1786[144] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[10])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[10]))->name = scm__rc.d1786[132];/* (liota gen) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[10]))->debugInfo = scm__rc.d1786[144];
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[321]), scm__rc.d1786[2]);
  scm__rc.d1786[145] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[303]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[322]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[323]), scm__rc.d1786[145]);
  scm__rc.d1786[146] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[11])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[11]))->name = scm__rc.d1786[132];/* (liota gen) */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[11]))->debugInfo = scm__rc.d1786[146];
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[326]), scm__rc.d1786[2]);
  scm__rc.d1786[147] = Scm_MakeExtendedPair(scm__rc.d1786[129], SCM_OBJ(&scm__rc.d1787[220]), SCM_OBJ(&scm__rc.d1787[327]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[328]), scm__rc.d1786[147]);
  scm__rc.d1786[148] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[12])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[12]))->name = scm__rc.d1786[129];/* liota */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[12]))->debugInfo = scm__rc.d1786[148];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]))[4] = SCM_WORD(scm__rc.d1786[85]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]))[62] = SCM_WORD(scm__rc.d1786[110]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]))[67] = SCM_WORD(scm__rc.d1786[85]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]))[77] = SCM_WORD(scm__rc.d1786[115]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]))[84] = SCM_WORD(scm__rc.d1786[115]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]))[91] = SCM_WORD(scm__rc.d1786[138]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]))[109] = SCM_WORD(scm__rc.d1786[138]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[273]))[124] = SCM_WORD(scm__rc.d1786[114]);
  scm__rc.d1786[149] = Scm_MakeIdentifier(scm__rc.d1786[129], SCM_MODULE(scm__rc.d1786[53]), SCM_NIL); /* gauche#liota */
  scm__rc.d1786[150] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[13])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[13]))->name = scm__rc.d1786[55];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[13]))->debugInfo = scm__rc.d1786[150];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[399]))[3] = SCM_WORD(scm__rc.d1786[49]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[399]))[6] = SCM_WORD(scm__rc.d1786[129]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[399]))[13] = SCM_WORD(scm__rc.d1786[149]);
  scm__rc.d1786[151] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[66])),TRUE); /* port->char-lseq */
  scm__rc.d1786[152] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[67])),TRUE); /* port */
  scm__rc.d1786[153] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[68])),TRUE); /* current-input-port */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[329]), scm__rc.d1786[153]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[331]), scm__rc.d1786[152]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[333]), scm__rc.d1786[60]);
  scm__rc.d1786[154] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[69])),TRUE); /* cut */
  scm__rc.d1786[155] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[70])),TRUE); /* read-char */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[334]), scm__rc.d1786[152]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[335]), scm__rc.d1786[155]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[336]), scm__rc.d1786[154]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[338]), scm__rc.d1786[73]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[341]), scm__rc.d1786[86]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[342]), scm__rc.d1786[151]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[345]), scm__rc.d1786[2]);
  scm__rc.d1786[156] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[342]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[346]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[347]), scm__rc.d1786[156]);
  scm__rc.d1786[157] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[14])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[14]))->debugInfo = scm__rc.d1786[157];
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[350]), scm__rc.d1786[2]);
  scm__rc.d1786[158] = Scm_MakeExtendedPair(scm__rc.d1786[151], SCM_OBJ(&scm__rc.d1787[333]), SCM_OBJ(&scm__rc.d1787[351]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[352]), scm__rc.d1786[158]);
  scm__rc.d1786[159] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[15])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[15]))->name = scm__rc.d1786[151];/* port->char-lseq */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[15]))->debugInfo = scm__rc.d1786[159];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[417]))[28] = SCM_WORD(scm__rc.d1786[110]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[417]))[32] = SCM_WORD(scm__rc.d1786[114]);
  scm__rc.d1786[160] = Scm_MakeIdentifier(scm__rc.d1786[151], SCM_MODULE(scm__rc.d1786[53]), SCM_NIL); /* gauche#port->char-lseq */
  scm__rc.d1786[161] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[16])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[16]))->name = scm__rc.d1786[55];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[16]))->debugInfo = scm__rc.d1786[161];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[451]))[3] = SCM_WORD(scm__rc.d1786[49]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[451]))[6] = SCM_WORD(scm__rc.d1786[151]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[451]))[13] = SCM_WORD(scm__rc.d1786[160]);
  scm__rc.d1786[162] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[71])),TRUE); /* port->byte-lseq */
  scm__rc.d1786[163] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[72])),TRUE); /* read-byte */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[353]), scm__rc.d1786[163]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[354]), scm__rc.d1786[154]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[356]), scm__rc.d1786[73]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[359]), scm__rc.d1786[86]);
  scm__rc.d1786[164] = Scm_MakeIdentifier(scm__rc.d1786[163], SCM_MODULE(scm__rc.d1786[53]), SCM_NIL); /* gauche#read-byte */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[360]), scm__rc.d1786[162]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[363]), scm__rc.d1786[2]);
  scm__rc.d1786[165] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[360]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[364]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[365]), scm__rc.d1786[165]);
  scm__rc.d1786[166] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[17])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[17]))->debugInfo = scm__rc.d1786[166];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[466]))[2] = SCM_WORD(scm__rc.d1786[164]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[368]), scm__rc.d1786[2]);
  scm__rc.d1786[167] = Scm_MakeExtendedPair(scm__rc.d1786[162], SCM_OBJ(&scm__rc.d1787[333]), SCM_OBJ(&scm__rc.d1787[369]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[370]), scm__rc.d1786[167]);
  scm__rc.d1786[168] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[18])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[18]))->name = scm__rc.d1786[162];/* port->byte-lseq */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[18]))->debugInfo = scm__rc.d1786[168];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[470]))[28] = SCM_WORD(scm__rc.d1786[110]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[470]))[32] = SCM_WORD(scm__rc.d1786[114]);
  scm__rc.d1786[169] = Scm_MakeIdentifier(scm__rc.d1786[162], SCM_MODULE(scm__rc.d1786[53]), SCM_NIL); /* gauche#port->byte-lseq */
  scm__rc.d1786[170] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[19])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[19]))->name = scm__rc.d1786[55];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[19]))->debugInfo = scm__rc.d1786[170];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[504]))[3] = SCM_WORD(scm__rc.d1786[49]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[504]))[6] = SCM_WORD(scm__rc.d1786[162]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[504]))[13] = SCM_WORD(scm__rc.d1786[169]);
  scm__rc.d1786[171] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[73])),TRUE); /* port->string-lseq */
  scm__rc.d1786[172] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[74])),TRUE); /* read-line */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[371]), scm__rc.d1786[172]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[372]), scm__rc.d1786[154]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[374]), scm__rc.d1786[73]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[377]), scm__rc.d1786[86]);
  scm__rc.d1786[173] = Scm_MakeIdentifier(scm__rc.d1786[172], SCM_MODULE(scm__rc.d1786[53]), SCM_NIL); /* gauche#read-line */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[378]), scm__rc.d1786[171]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[381]), scm__rc.d1786[2]);
  scm__rc.d1786[174] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[378]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[382]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[383]), scm__rc.d1786[174]);
  scm__rc.d1786[175] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[20])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[20]))->debugInfo = scm__rc.d1786[175];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[519]))[2] = SCM_WORD(scm__rc.d1786[173]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[386]), scm__rc.d1786[2]);
  scm__rc.d1786[176] = Scm_MakeExtendedPair(scm__rc.d1786[171], SCM_OBJ(&scm__rc.d1787[333]), SCM_OBJ(&scm__rc.d1787[387]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[388]), scm__rc.d1786[176]);
  scm__rc.d1786[177] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[21])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[21]))->name = scm__rc.d1786[171];/* port->string-lseq */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[21]))->debugInfo = scm__rc.d1786[177];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[523]))[28] = SCM_WORD(scm__rc.d1786[110]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[523]))[32] = SCM_WORD(scm__rc.d1786[114]);
  scm__rc.d1786[178] = Scm_MakeIdentifier(scm__rc.d1786[171], SCM_MODULE(scm__rc.d1786[53]), SCM_NIL); /* gauche#port->string-lseq */
  scm__rc.d1786[179] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[22])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[22]))->name = scm__rc.d1786[55];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[22]))->debugInfo = scm__rc.d1786[179];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[557]))[3] = SCM_WORD(scm__rc.d1786[49]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[557]))[6] = SCM_WORD(scm__rc.d1786[171]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[557]))[13] = SCM_WORD(scm__rc.d1786[178]);
  scm__rc.d1786[180] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[75])),TRUE); /* port->sexp-lseq */
  scm__rc.d1786[181] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[76])),TRUE); /* read */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[389]), scm__rc.d1786[181]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[390]), scm__rc.d1786[154]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[392]), scm__rc.d1786[73]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[395]), scm__rc.d1786[86]);
  scm__rc.d1786[182] = Scm_MakeIdentifier(scm__rc.d1786[181], SCM_MODULE(scm__rc.d1786[53]), SCM_NIL); /* gauche#read */
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[396]), scm__rc.d1786[180]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[399]), scm__rc.d1786[2]);
  scm__rc.d1786[183] = Scm_MakeExtendedPair(SCM_OBJ(&scm__rc.d1787[396]), SCM_NIL, SCM_OBJ(&scm__rc.d1787[400]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[401]), scm__rc.d1786[183]);
  scm__rc.d1786[184] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[23])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[23]))->debugInfo = scm__rc.d1786[184];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[572]))[2] = SCM_WORD(scm__rc.d1786[182]);
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[404]), scm__rc.d1786[2]);
  scm__rc.d1786[185] = Scm_MakeExtendedPair(scm__rc.d1786[180], SCM_OBJ(&scm__rc.d1787[333]), SCM_OBJ(&scm__rc.d1787[405]));
  SCM_SET_CAR(SCM_OBJ(&scm__rc.d1787[406]), scm__rc.d1786[185]);
  scm__rc.d1786[186] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[24])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[24]))->name = scm__rc.d1786[180];/* port->sexp-lseq */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[24]))->debugInfo = scm__rc.d1786[186];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[576]))[28] = SCM_WORD(scm__rc.d1786[110]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[576]))[32] = SCM_WORD(scm__rc.d1786[114]);
  scm__rc.d1786[187] = Scm_MakeIdentifier(scm__rc.d1786[180], SCM_MODULE(scm__rc.d1786[53]), SCM_NIL); /* gauche#port->sexp-lseq */
  scm__rc.d1786[188] = Scm_MakePackedDebugInfo(SCM_U8VECTOR(SCM_OBJ(&scm__rc.d1790[25])),SCM_debug_info_const_vector);
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[25]))->name = scm__rc.d1786[55];/* %toplevel */
  SCM_COMPILED_CODE(SCM_OBJ(&scm__rc.d1789[25]))->debugInfo = scm__rc.d1786[188];
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[610]))[3] = SCM_WORD(scm__rc.d1786[49]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[610]))[6] = SCM_WORD(scm__rc.d1786[180]);
  ((ScmWord*)SCM_OBJ(&scm__rc.d1788[610]))[13] = SCM_WORD(scm__rc.d1786[187]);
  Scm_VMExecuteToplevels(toplevels);
  scm__rc.d1786[254] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[77])),FALSE); /* G1792 */
  scm__rc.d1786[255] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[78])),FALSE); /* G1794 */
  scm__rc.d1786[256] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[79])),FALSE); /* G1793 */
  scm__rc.d1786[257] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[80])),FALSE); /* rest1791 */
  scm__rc.d1786[258] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[81])),TRUE); /* - */
  scm__rc.d1786[259] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[82])),FALSE); /* G1796 */
  scm__rc.d1786[260] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[83])),FALSE); /* G1799 */
  scm__rc.d1786[261] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[84])),FALSE); /* G1798 */
  scm__rc.d1786[262] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[85])),FALSE); /* G1797 */
  scm__rc.d1786[263] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[86])),FALSE); /* rest1795 */
  scm__rc.d1786[264] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[87])),FALSE); /* G1801 */
  scm__rc.d1786[265] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[88])),FALSE); /* G1802 */
  scm__rc.d1786[266] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[89])),FALSE); /* rest1800 */
  scm__rc.d1786[267] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[90])),FALSE); /* G1804 */
  scm__rc.d1786[268] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[91])),FALSE); /* G1805 */
  scm__rc.d1786[269] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[92])),FALSE); /* rest1803 */
  scm__rc.d1786[270] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[93])),FALSE); /* G1807 */
  scm__rc.d1786[271] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[94])),FALSE); /* G1808 */
  scm__rc.d1786[272] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[95])),FALSE); /* rest1806 */
  scm__rc.d1786[273] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[96])),FALSE); /* G1810 */
  scm__rc.d1786[274] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[97])),FALSE); /* G1811 */
  scm__rc.d1786[275] = Scm_MakeSymbol(SCM_STRING(SCM_OBJ(&scm__sc.d1785[98])),FALSE); /* rest1809 */
}
