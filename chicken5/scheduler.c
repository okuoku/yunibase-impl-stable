/* Generated from scheduler.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: scheduler.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -explicit-use -no-trace -output-file scheduler.c
   unit: scheduler
   uses: extras library
*/
#include "chicken.h"

#ifdef HAVE_ERRNO_H
# include <errno.h>
# define C_signal_interrupted_p     C_mk_bool(errno == EINTR)
#else
# define C_signal_interrupted_p     C_SCHEME_FALSE
#endif

#ifdef _WIN32
/* TODO: Winsock select() only works for sockets */
# include <winsock2.h>
/* Beware: winsock2.h must come BEFORE windows.h */
# define C_msleep(n)   (Sleep((DWORD)C_num_to_uint64(n)), C_SCHEME_TRUE)
#else
# include <sys/time.h>
static C_word C_msleep(C_word ms);
C_word C_msleep(C_word ms) {
#ifdef __CYGWIN__
  if(usleep((useconds_t)C_num_to_uint64(ms) * 1000) == -1) return C_SCHEME_FALSE;
#else
  struct timespec ts;
  C_word ab[C_SIZEOF_FIX_BIGNUM], *a = ab,
         sec = C_s_a_u_i_integer_quotient(&a, 2, ms, C_fix(1000)),
         msec = C_s_a_u_i_integer_remainder(&a, 2, ms, C_fix(1000));
  ts.tv_sec = (time_t)C_num_to_uint64(sec);
  ts.tv_nsec = (long)C_unfix(msec) * 1000000;
  
  if(nanosleep(&ts, NULL) == -1) return C_SCHEME_FALSE;
#endif
  return C_SCHEME_TRUE;
}
#endif

#ifdef NO_POSIX_POLL

/* Shouldn't we include <sys/select.h> here? */
static fd_set C_fdset_input, C_fdset_output;

#define C_fd_input_ready(fd,pos)  C_mk_bool(FD_ISSET(C_unfix(fd), &C_fdset_input))
#define C_fd_output_ready(fd,pos)  C_mk_bool(FD_ISSET(C_unfix(fd), &C_fdset_output))

inline static int C_ready_fds_timeout(int to, unsigned int tm) {
  struct timeval timeout;
  timeout.tv_sec = tm / 1000;
  timeout.tv_usec = fmod(tm, 1000) * 1000;
  /* we use FD_SETSIZE, but really should use max fd */
  return select(FD_SETSIZE, &C_fdset_input, &C_fdset_output, NULL, to ? &timeout : NULL);
}

inline static void C_prepare_fdset(int length) {
  FD_ZERO(&C_fdset_input);
  FD_ZERO(&C_fdset_output);
}

inline static void C_fdset_add(int fd, int input, int output) {
  if (input) FD_SET(fd, &C_fdset_input);
  if (output) FD_SET(fd, &C_fdset_output);
}

#else
#  include <poll.h>
#  include <assert.h>

static int C_fdset_nfds;
static struct pollfd *C_fdset_set = NULL;

inline static int C_fd_ready(int fd, int pos, int what) {
  assert(fd == C_fdset_set[pos].fd); /* Must match position in ##sys#fd-list! */
  return(C_fdset_set[pos].revents & what);
}

#define C_fd_input_ready(fd,pos)  C_mk_bool(C_fd_ready(C_unfix(fd), C_unfix(pos),POLLIN|POLLERR|POLLHUP|POLLNVAL))
#define C_fd_output_ready(fd,pos)  C_mk_bool(C_fd_ready(C_unfix(fd), C_unfix(pos),POLLOUT|POLLERR|POLLHUP|POLLNVAL))

inline static int C_ready_fds_timeout(int to, unsigned int tm) {
  return poll(C_fdset_set, C_fdset_nfds, to ? tm : -1);
}

inline static void C_prepare_fdset(int length) {
  /* TODO: Only realloc when needed? */
  C_fdset_set = realloc(C_fdset_set, sizeof(struct pollfd) * length);
  if (C_fdset_set == NULL)
    C_halt(C_SCHEME_FALSE); /* Ugly: no message */
  C_fdset_nfds = 0;
}

/* This *must* be called in order, so position will match ##sys#fd-list */
inline static void C_fdset_add(int fd, int input, int output) {
  C_fdset_set[C_fdset_nfds].events = ((input ? POLLIN : 0) | (output ? POLLOUT : 0));
  C_fdset_set[C_fdset_nfds++].fd = fd;
}
#endif

static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_extras_toplevel)
C_externimport void C_ccall C_extras_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[71];
static double C_possibly_force_alignment;
static C_char C_TLS li0[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,32,108,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li1[] C_aligned={C_lihdr(0,0,12),40,100,101,108,113,32,120,32,108,115,116,41,0,0,0,0};
static C_char C_TLS li2[] C_aligned={C_lihdr(0,0,7),40,108,111,111,112,50,41,0};
static C_char C_TLS li3[] C_aligned={C_lihdr(0,0,20),40,108,111,111,112,50,32,116,104,114,101,97,100,115,32,107,101,101,112,41,0,0,0,0};
static C_char C_TLS li4[] C_aligned={C_lihdr(0,0,16),40,108,111,111,112,32,110,32,112,111,115,32,108,115,116,41};
static C_char C_TLS li5[] C_aligned={C_lihdr(0,0,8),40,103,51,57,51,32,116,41};
static C_char C_TLS li6[] C_aligned={C_lihdr(0,0,23),40,102,111,114,45,101,97,99,104,45,108,111,111,112,51,57,50,32,103,51,57,57,41,0};
static C_char C_TLS li7[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,32,108,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li8[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,32,108,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li9[] C_aligned={C_lihdr(0,0,7),40,108,111,111,112,49,41,0};
static C_char C_TLS li10[] C_aligned={C_lihdr(0,0,16),40,35,35,115,121,115,35,115,99,104,101,100,117,108,101,41};
static C_char C_TLS li11[] C_aligned={C_lihdr(0,0,24),40,35,35,115,121,115,35,102,111,114,99,101,45,112,114,105,109,111,114,100,105,97,108,41};
static C_char C_TLS li12[] C_aligned={C_lihdr(0,0,19),40,35,35,115,121,115,35,114,101,97,100,121,45,113,117,101,117,101,41,0,0,0,0,0};
static C_char C_TLS li13[] C_aligned={C_lihdr(0,0,33),40,35,35,115,121,115,35,97,100,100,45,116,111,45,114,101,97,100,121,45,113,117,101,117,101,32,116,104,114,101,97,100,41,0,0,0,0,0,0,0};
static C_char C_TLS li14[] C_aligned={C_lihdr(0,0,7),40,97,49,51,48,48,41,0};
static C_char C_TLS li15[] C_aligned={C_lihdr(0,0,35),40,35,35,115,121,115,35,105,110,116,101,114,114,117,112,116,45,104,111,111,107,32,114,101,97,115,111,110,32,115,116,97,116,101,41,0,0,0,0,0};
static C_char C_TLS li16[] C_aligned={C_lihdr(0,0,11),40,108,111,111,112,32,112,114,101,118,41,0,0,0,0,0};
static C_char C_TLS li17[] C_aligned={C_lihdr(0,0,34),40,35,35,115,121,115,35,114,101,109,111,118,101,45,102,114,111,109,45,116,105,109,101,111,117,116,45,108,105,115,116,32,116,41,0,0,0,0,0,0};
static C_char C_TLS li18[] C_aligned={C_lihdr(0,0,14),40,108,111,111,112,32,116,108,32,112,114,101,118,41,0,0};
static C_char C_TLS li19[] C_aligned={C_lihdr(0,0,38),40,35,35,115,121,115,35,116,104,114,101,97,100,45,98,108,111,99,107,45,102,111,114,45,116,105,109,101,111,117,116,33,32,116,32,116,109,41,0,0};
static C_char C_TLS li20[] C_aligned={C_lihdr(0,0,42),40,35,35,115,121,115,35,116,104,114,101,97,100,45,98,108,111,99,107,45,102,111,114,45,116,101,114,109,105,110,97,116,105,111,110,33,32,116,32,116,50,41,0,0,0,0,0,0};
static C_char C_TLS li21[] C_aligned={C_lihdr(0,0,9),40,103,51,51,53,32,116,50,41,0,0,0,0,0,0,0};
static C_char C_TLS li22[] C_aligned={C_lihdr(0,0,23),40,102,111,114,45,101,97,99,104,45,108,111,111,112,51,51,52,32,103,51,52,49,41,0};
static C_char C_TLS li23[] C_aligned={C_lihdr(0,0,23),40,102,111,114,45,101,97,99,104,45,108,111,111,112,51,48,48,32,103,51,48,55,41,0};
static C_char C_TLS li24[] C_aligned={C_lihdr(0,0,23),40,102,111,114,45,101,97,99,104,45,108,111,111,112,50,56,54,32,103,50,57,51,41,0};
static C_char C_TLS li25[] C_aligned={C_lihdr(0,0,24),40,35,35,115,121,115,35,116,104,114,101,97,100,45,107,105,108,108,33,32,116,32,115,41};
static C_char C_TLS li26[] C_aligned={C_lihdr(0,0,31),40,35,35,115,121,115,35,116,104,114,101,97,100,45,98,97,115,105,99,45,117,110,98,108,111,99,107,33,32,116,41,0};
static C_char C_TLS li27[] C_aligned={C_lihdr(0,0,7),40,97,49,54,57,55,41,0};
static C_char C_TLS li28[] C_aligned={C_lihdr(0,0,37),40,35,35,115,121,115,35,100,101,102,97,117,108,116,45,101,120,99,101,112,116,105,111,110,45,104,97,110,100,108,101,114,32,97,114,103,41,0,0,0};
static C_char C_TLS li29[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,32,108,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li30[] C_aligned={C_lihdr(0,0,38),40,35,35,115,121,115,35,116,104,114,101,97,100,45,98,108,111,99,107,45,102,111,114,45,105,47,111,33,32,116,32,102,100,32,105,47,111,41,0,0};
static C_char C_TLS li31[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,32,108,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li32[] C_aligned={C_lihdr(0,0,37),40,35,35,115,121,115,35,99,108,101,97,114,45,105,47,111,45,115,116,97,116,101,45,102,111,114,45,116,104,114,101,97,100,33,32,116,41,0,0,0};
static C_char C_TLS li33[] C_aligned={C_lihdr(0,0,27),40,102,95,50,52,55,50,32,113,117,101,117,101,32,97,114,103,32,118,97,108,32,105,110,105,116,41,0,0,0,0,0};
static C_char C_TLS li34[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,108,41};
static C_char C_TLS li35[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,32,108,32,105,41,0,0,0,0,0,0};
static C_char C_TLS li36[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,32,108,32,105,41,0,0,0,0,0,0};
static C_char C_TLS li37[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,32,108,32,105,41,0,0,0,0,0,0};
static C_char C_TLS li38[] C_aligned={C_lihdr(0,0,26),40,35,35,115,121,115,35,97,108,108,45,116,104,114,101,97,100,115,32,46,32,114,101,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li39[] C_aligned={C_lihdr(0,0,31),40,35,35,115,121,115,35,102,101,116,99,104,45,97,110,100,45,99,108,101,97,114,45,116,104,114,101,97,100,115,41,0};
static C_char C_TLS li40[] C_aligned={C_lihdr(0,0,27),40,35,35,115,121,115,35,114,101,115,116,111,114,101,45,116,104,114,101,97,100,115,32,118,101,99,41,0,0,0,0,0};
static C_char C_TLS li41[] C_aligned={C_lihdr(0,0,25),40,35,35,115,121,115,35,116,104,114,101,97,100,45,117,110,98,108,111,99,107,33,32,116,41,0,0,0,0,0,0,0};
static C_char C_TLS li42[] C_aligned={C_lihdr(0,0,7),40,97,50,53,53,57,41,0};
static C_char C_TLS li43[] C_aligned={C_lihdr(0,0,14),40,97,50,53,52,55,32,114,101,116,117,114,110,41,0,0};
static C_char C_TLS li44[] C_aligned={C_lihdr(0,0,24),40,35,35,115,121,115,35,116,104,114,101,97,100,45,115,108,101,101,112,33,32,116,109,41};
static C_char C_TLS li45[] C_aligned={C_lihdr(0,0,27),40,99,104,105,99,107,101,110,46,98,97,115,101,35,115,108,101,101,112,45,104,111,111,107,32,110,41,0,0,0,0,0};
static C_char C_TLS li46[] C_aligned={C_lihdr(0,0,9),40,115,117,115,112,101,110,100,41,0,0,0,0,0,0,0};
static C_char C_TLS li47[] C_aligned={C_lihdr(0,0,6),40,103,54,52,54,41,0,0};
static C_char C_TLS li48[] C_aligned={C_lihdr(0,0,18),40,102,111,114,45,101,97,99,104,45,108,111,111,112,54,52,53,41,0,0,0,0,0,0};
static C_char C_TLS li49[] C_aligned={C_lihdr(0,0,18),40,102,111,114,45,101,97,99,104,45,108,111,111,112,54,55,56,41,0,0,0,0,0,0};
static C_char C_TLS li50[] C_aligned={C_lihdr(0,0,6),40,103,54,53,54,41,0,0};
static C_char C_TLS li51[] C_aligned={C_lihdr(0,0,18),40,102,111,114,45,101,97,99,104,45,108,111,111,112,54,53,53,41,0,0,0,0,0,0};
static C_char C_TLS li52[] C_aligned={C_lihdr(0,0,32),40,35,35,115,121,115,35,107,105,108,108,45,111,116,104,101,114,45,116,104,114,101,97,100,115,32,116,104,117,110,107,41};
static C_char C_TLS li53[] C_aligned={C_lihdr(0,0,10),40,116,111,112,108,101,118,101,108,41,0,0,0,0,0,0};


/* from g505 */
C_regparm static C_word C_fcall stub509(C_word C_buf,C_word C_a0,C_word C_a1){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_truep(C_a0);
unsigned int t1=(unsigned int )C_num_to_unsigned_int(C_a1);
C_r=C_fix((C_word)C_ready_fds_timeout(t0,t1));
return C_r;}

/* from g436 */
C_regparm static C_word C_fcall stub441(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
int t1=(int )C_truep(C_a1);
int t2=(int )C_truep(C_a2);
C_fdset_add(t0,t1,t2);
return C_r;}

/* from g379 */
C_regparm static C_word C_fcall stub382(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
C_prepare_fdset(t0);
return C_r;}

C_noret_decl(f_1000)
static void C_fcall f_1000(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1004)
static void C_fcall f_1004(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1061)
static void C_fcall f_1061(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_1095)
static void C_ccall f_1095(C_word c,C_word *av) C_noret;
C_noret_decl(f_1098)
static void C_ccall f_1098(C_word c,C_word *av) C_noret;
C_noret_decl(f_1110)
static void C_fcall f_1110(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1116)
static void C_ccall f_1116(C_word c,C_word *av) C_noret;
C_noret_decl(f_1128)
static void C_ccall f_1128(C_word c,C_word *av) C_noret;
C_noret_decl(f_1136)
static void C_ccall f_1136(C_word c,C_word *av) C_noret;
C_noret_decl(f_1170)
static void C_ccall f_1170(C_word c,C_word *av) C_noret;
C_noret_decl(f_1181)
static void C_ccall f_1181(C_word c,C_word *av) C_noret;
C_noret_decl(f_1184)
static void C_ccall f_1184(C_word c,C_word *av) C_noret;
C_noret_decl(f_1283)
static void C_ccall f_1283(C_word c,C_word *av) C_noret;
C_noret_decl(f_1287)
static void C_ccall f_1287(C_word c,C_word *av) C_noret;
C_noret_decl(f_1301)
static void C_ccall f_1301(C_word c,C_word *av) C_noret;
C_noret_decl(f_1308)
static void C_ccall f_1308(C_word c,C_word *av) C_noret;
C_noret_decl(f_1314)
static C_word C_fcall f_1314(C_word t0,C_word t1,C_word t2);
C_noret_decl(f_1349)
static void C_ccall f_1349(C_word c,C_word *av) C_noret;
C_noret_decl(f_1359)
static void C_ccall f_1359(C_word c,C_word *av) C_noret;
C_noret_decl(f_1370)
static void C_fcall f_1370(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_1418)
static void C_ccall f_1418(C_word c,C_word *av) C_noret;
C_noret_decl(f_1483)
static void C_ccall f_1483(C_word c,C_word *av) C_noret;
C_noret_decl(f_1499)
static void C_fcall f_1499(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_1509)
static void C_ccall f_1509(C_word c,C_word *av) C_noret;
C_noret_decl(f_1525)
static void C_fcall f_1525(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_1548)
static void C_ccall f_1548(C_word c,C_word *av) C_noret;
C_noret_decl(f_1552)
static void C_ccall f_1552(C_word c,C_word *av) C_noret;
C_noret_decl(f_1558)
static void C_fcall f_1558(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1561)
static void C_ccall f_1561(C_word c,C_word *av) C_noret;
C_noret_decl(f_1564)
static void C_ccall f_1564(C_word c,C_word *av) C_noret;
C_noret_decl(f_1582)
static void C_ccall f_1582(C_word c,C_word *av) C_noret;
C_noret_decl(f_1589)
static void C_fcall f_1589(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_1608)
static void C_fcall f_1608(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_1618)
static void C_ccall f_1618(C_word c,C_word *av) C_noret;
C_noret_decl(f_1639)
static void C_ccall f_1639(C_word c,C_word *av) C_noret;
C_noret_decl(f_1656)
static void C_ccall f_1656(C_word c,C_word *av) C_noret;
C_noret_decl(f_1662)
static void C_ccall f_1662(C_word c,C_word *av) C_noret;
C_noret_decl(f_1674)
static void C_ccall f_1674(C_word c,C_word *av) C_noret;
C_noret_decl(f_1678)
static void C_ccall f_1678(C_word c,C_word *av) C_noret;
C_noret_decl(f_1684)
static void C_ccall f_1684(C_word c,C_word *av) C_noret;
C_noret_decl(f_1698)
static void C_ccall f_1698(C_word c,C_word *av) C_noret;
C_noret_decl(f_1702)
static void C_ccall f_1702(C_word c,C_word *av) C_noret;
C_noret_decl(f_1722)
static void C_fcall f_1722(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_1733)
static void C_fcall f_1733(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_1776)
static void C_ccall f_1776(C_word c,C_word *av) C_noret;
C_noret_decl(f_1782)
static void C_ccall f_1782(C_word c,C_word *av) C_noret;
C_noret_decl(f_1785)
static void C_ccall f_1785(C_word c,C_word *av) C_noret;
C_noret_decl(f_1788)
static void C_ccall f_1788(C_word c,C_word *av) C_noret;
C_noret_decl(f_1791)
static void C_ccall f_1791(C_word c,C_word *av) C_noret;
C_noret_decl(f_1794)
static void C_ccall f_1794(C_word c,C_word *av) C_noret;
C_noret_decl(f_1797)
static void C_ccall f_1797(C_word c,C_word *av) C_noret;
C_noret_decl(f_1818)
static void C_ccall f_1818(C_word c,C_word *av) C_noret;
C_noret_decl(f_1830)
static void C_fcall f_1830(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_1840)
static void C_ccall f_1840(C_word c,C_word *av) C_noret;
C_noret_decl(f_1855)
static void C_ccall f_1855(C_word c,C_word *av) C_noret;
C_noret_decl(f_1889)
static void C_ccall f_1889(C_word c,C_word *av) C_noret;
C_noret_decl(f_1895)
static void C_ccall f_1895(C_word c,C_word *av) C_noret;
C_noret_decl(f_1898)
static void C_ccall f_1898(C_word c,C_word *av) C_noret;
C_noret_decl(f_1901)
static void C_ccall f_1901(C_word c,C_word *av) C_noret;
C_noret_decl(f_1904)
static void C_ccall f_1904(C_word c,C_word *av) C_noret;
C_noret_decl(f_1907)
static void C_ccall f_1907(C_word c,C_word *av) C_noret;
C_noret_decl(f_1910)
static void C_ccall f_1910(C_word c,C_word *av) C_noret;
C_noret_decl(f_1913)
static void C_ccall f_1913(C_word c,C_word *av) C_noret;
C_noret_decl(f_1916)
static void C_ccall f_1916(C_word c,C_word *av) C_noret;
C_noret_decl(f_1934)
static void C_ccall f_1934(C_word c,C_word *av) C_noret;
C_noret_decl(f_1938)
static void C_ccall f_1938(C_word c,C_word *av) C_noret;
C_noret_decl(f_1953)
static void C_fcall f_1953(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2001)
static void C_ccall f_2001(C_word c,C_word *av) C_noret;
C_noret_decl(f_2010)
static void C_ccall f_2010(C_word c,C_word *av) C_noret;
C_noret_decl(f_2034)
static void C_ccall f_2034(C_word c,C_word *av) C_noret;
C_noret_decl(f_2036)
static void C_fcall f_2036(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_2063)
static void C_fcall f_2063(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2098)
static void C_ccall f_2098(C_word c,C_word *av) C_noret;
C_noret_decl(f_2132)
static void C_ccall f_2132(C_word c,C_word *av) C_noret;
C_noret_decl(f_2135)
static void C_ccall f_2135(C_word c,C_word *av) C_noret;
C_noret_decl(f_2156)
static void C_ccall f_2156(C_word c,C_word *av) C_noret;
C_noret_decl(f_2162)
static void C_ccall f_2162(C_word c,C_word *av) C_noret;
C_noret_decl(f_2165)
static void C_ccall f_2165(C_word c,C_word *av) C_noret;
C_noret_decl(f_2168)
static void C_ccall f_2168(C_word c,C_word *av) C_noret;
C_noret_decl(f_2171)
static void C_ccall f_2171(C_word c,C_word *av) C_noret;
C_noret_decl(f_2174)
static void C_ccall f_2174(C_word c,C_word *av) C_noret;
C_noret_decl(f_2177)
static void C_ccall f_2177(C_word c,C_word *av) C_noret;
C_noret_decl(f_2185)
static void C_fcall f_2185(C_word t0,C_word t1) C_noret;
C_noret_decl(f_2188)
static void C_ccall f_2188(C_word c,C_word *av) C_noret;
C_noret_decl(f_2191)
static void C_ccall f_2191(C_word c,C_word *av) C_noret;
C_noret_decl(f_2232)
static void C_ccall f_2232(C_word c,C_word *av) C_noret;
C_noret_decl(f_2252)
static void C_ccall f_2252(C_word c,C_word *av) C_noret;
C_noret_decl(f_2263)
static void C_ccall f_2263(C_word c,C_word *av) C_noret;
C_noret_decl(f_2268)
static void C_fcall f_2268(C_word t0,C_word t1) C_noret;
C_noret_decl(f_2282)
static void C_ccall f_2282(C_word c,C_word *av) C_noret;
C_noret_decl(f_2284)
static void C_fcall f_2284(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2306)
static void C_ccall f_2306(C_word c,C_word *av) C_noret;
C_noret_decl(f_2325)
static void C_ccall f_2325(C_word c,C_word *av) C_noret;
C_noret_decl(f_2337)
static void C_ccall f_2337(C_word c,C_word *av) C_noret;
C_noret_decl(f_2355)
static void C_fcall f_2355(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2371)
static void C_ccall f_2371(C_word c,C_word *av) C_noret;
C_noret_decl(f_2378)
static void C_fcall f_2378(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2397)
static void C_ccall f_2397(C_word c,C_word *av) C_noret;
C_noret_decl(f_2402)
static void C_fcall f_2402(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2420)
static void C_ccall f_2420(C_word c,C_word *av) C_noret;
C_noret_decl(f_2427)
static void C_fcall f_2427(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2443)
static void C_ccall f_2443(C_word c,C_word *av) C_noret;
C_noret_decl(f_2472)
static void C_ccall f_2472(C_word c,C_word *av) C_noret;
C_noret_decl(f_2481)
static void C_ccall f_2481(C_word c,C_word *av) C_noret;
C_noret_decl(f_2491)
static void C_ccall f_2491(C_word c,C_word *av) C_noret;
C_noret_decl(f_2510)
static void C_ccall f_2510(C_word c,C_word *av) C_noret;
C_noret_decl(f_2523)
static void C_ccall f_2523(C_word c,C_word *av) C_noret;
C_noret_decl(f_2526)
static void C_ccall f_2526(C_word c,C_word *av) C_noret;
C_noret_decl(f_2542)
static void C_ccall f_2542(C_word c,C_word *av) C_noret;
C_noret_decl(f_2548)
static void C_ccall f_2548(C_word c,C_word *av) C_noret;
C_noret_decl(f_2555)
static void C_ccall f_2555(C_word c,C_word *av) C_noret;
C_noret_decl(f_2560)
static void C_ccall f_2560(C_word c,C_word *av) C_noret;
C_noret_decl(f_2566)
static void C_ccall f_2566(C_word c,C_word *av) C_noret;
C_noret_decl(f_2580)
static void C_ccall f_2580(C_word c,C_word *av) C_noret;
C_noret_decl(f_2583)
static C_word C_fcall f_2583(C_word t0,C_word t1);
C_noret_decl(f_2609)
static C_word C_fcall f_2609(C_word t0,C_word t1);
C_noret_decl(f_2621)
static C_word C_fcall f_2621(C_word t0,C_word t1);
C_noret_decl(f_2630)
static C_word C_fcall f_2630(C_word t0,C_word t1);
C_noret_decl(f_2659)
static void C_ccall f_2659(C_word c,C_word *av) C_noret;
C_noret_decl(f_2664)
static C_word C_fcall f_2664(C_word t0,C_word t1);
C_noret_decl(f_2687)
static C_word C_fcall f_2687(C_word t0,C_word t1);
C_noret_decl(f_904)
static void C_ccall f_904(C_word c,C_word *av) C_noret;
C_noret_decl(f_907)
static void C_ccall f_907(C_word c,C_word *av) C_noret;
C_noret_decl(f_909)
static void C_fcall f_909(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_915)
static void C_fcall f_915(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_942)
static void C_ccall f_942(C_word c,C_word *av) C_noret;
C_noret_decl(f_952)
static void C_ccall f_952(C_word c,C_word *av) C_noret;
C_noret_decl(f_983)
static void C_ccall f_983(C_word c,C_word *av) C_noret;
C_noret_decl(f_988)
static void C_fcall f_988(C_word t0,C_word t1) C_noret;
C_noret_decl(f_992)
static void C_ccall f_992(C_word c,C_word *av) C_noret;
C_noret_decl(f_995)
static void C_ccall f_995(C_word c,C_word *av) C_noret;
C_noret_decl(C_scheduler_toplevel)
C_externexport void C_ccall C_scheduler_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_1000)
static void C_ccall trf_1000(C_word c,C_word *av) C_noret;
static void C_ccall trf_1000(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1000(t0,t1);}

C_noret_decl(trf_1004)
static void C_ccall trf_1004(C_word c,C_word *av) C_noret;
static void C_ccall trf_1004(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1004(t0,t1);}

C_noret_decl(trf_1061)
static void C_ccall trf_1061(C_word c,C_word *av) C_noret;
static void C_ccall trf_1061(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_1061(t0,t1,t2);}

C_noret_decl(trf_1110)
static void C_ccall trf_1110(C_word c,C_word *av) C_noret;
static void C_ccall trf_1110(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1110(t0,t1);}

C_noret_decl(trf_1370)
static void C_ccall trf_1370(C_word c,C_word *av) C_noret;
static void C_ccall trf_1370(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_1370(t0,t1,t2,t3);}

C_noret_decl(trf_1499)
static void C_ccall trf_1499(C_word c,C_word *av) C_noret;
static void C_ccall trf_1499(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_1499(t0,t1,t2);}

C_noret_decl(trf_1525)
static void C_ccall trf_1525(C_word c,C_word *av) C_noret;
static void C_ccall trf_1525(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_1525(t0,t1,t2);}

C_noret_decl(trf_1558)
static void C_ccall trf_1558(C_word c,C_word *av) C_noret;
static void C_ccall trf_1558(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1558(t0,t1);}

C_noret_decl(trf_1589)
static void C_ccall trf_1589(C_word c,C_word *av) C_noret;
static void C_ccall trf_1589(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_1589(t0,t1,t2);}

C_noret_decl(trf_1608)
static void C_ccall trf_1608(C_word c,C_word *av) C_noret;
static void C_ccall trf_1608(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_1608(t0,t1,t2);}

C_noret_decl(trf_1722)
static void C_ccall trf_1722(C_word c,C_word *av) C_noret;
static void C_ccall trf_1722(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_1722(t0,t1,t2);}

C_noret_decl(trf_1733)
static void C_ccall trf_1733(C_word c,C_word *av) C_noret;
static void C_ccall trf_1733(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_1733(t0,t1,t2);}

C_noret_decl(trf_1830)
static void C_ccall trf_1830(C_word c,C_word *av) C_noret;
static void C_ccall trf_1830(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_1830(t0,t1,t2);}

C_noret_decl(trf_1953)
static void C_ccall trf_1953(C_word c,C_word *av) C_noret;
static void C_ccall trf_1953(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_1953(t0,t1,t2);}

C_noret_decl(trf_2036)
static void C_ccall trf_2036(C_word c,C_word *av) C_noret;
static void C_ccall trf_2036(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_2036(t0,t1,t2,t3,t4);}

C_noret_decl(trf_2063)
static void C_ccall trf_2063(C_word c,C_word *av) C_noret;
static void C_ccall trf_2063(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2063(t0,t1,t2,t3);}

C_noret_decl(trf_2185)
static void C_ccall trf_2185(C_word c,C_word *av) C_noret;
static void C_ccall trf_2185(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_2185(t0,t1);}

C_noret_decl(trf_2268)
static void C_ccall trf_2268(C_word c,C_word *av) C_noret;
static void C_ccall trf_2268(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_2268(t0,t1);}

C_noret_decl(trf_2284)
static void C_ccall trf_2284(C_word c,C_word *av) C_noret;
static void C_ccall trf_2284(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2284(t0,t1,t2);}

C_noret_decl(trf_2355)
static void C_ccall trf_2355(C_word c,C_word *av) C_noret;
static void C_ccall trf_2355(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2355(t0,t1,t2,t3);}

C_noret_decl(trf_2378)
static void C_ccall trf_2378(C_word c,C_word *av) C_noret;
static void C_ccall trf_2378(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2378(t0,t1,t2,t3);}

C_noret_decl(trf_2402)
static void C_ccall trf_2402(C_word c,C_word *av) C_noret;
static void C_ccall trf_2402(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2402(t0,t1,t2);}

C_noret_decl(trf_2427)
static void C_ccall trf_2427(C_word c,C_word *av) C_noret;
static void C_ccall trf_2427(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2427(t0,t1,t2,t3);}

C_noret_decl(trf_909)
static void C_ccall trf_909(C_word c,C_word *av) C_noret;
static void C_ccall trf_909(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_909(t0,t1,t2);}

C_noret_decl(trf_915)
static void C_ccall trf_915(C_word c,C_word *av) C_noret;
static void C_ccall trf_915(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_915(t0,t1,t2);}

C_noret_decl(trf_988)
static void C_ccall trf_988(C_word c,C_word *av) C_noret;
static void C_ccall trf_988(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_988(t0,t1);}

/* loop2 in k993 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_fcall f_1000(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_1000,2,t0,t1);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1004,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t3=lf[16];
t4=C_i_nullp(lf[16]);
if(C_truep(C_i_not(t4))){
t5=C_u_i_cdr(lf[16]);
t6=C_mutate(&lf[16] /* (set! ready-queue-head ...) */,t5);
t7=C_eqp(C_SCHEME_END_OF_LIST,t5);
if(C_truep(t7)){
t8=lf[17] /* ready-queue-tail */ =C_SCHEME_END_OF_LIST;;
t9=t2;
f_1004(t9,C_u_i_car(t3));}
else{
t8=t2;
f_1004(t8,C_u_i_car(t3));}}
else{
t5=t2;
f_1004(t5,C_SCHEME_FALSE);}}

/* k1002 in loop2 in k993 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_fcall f_1004(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_1004,2,t0,t1);}
if(C_truep(C_i_not(t1))){
if(C_truep(C_i_nullp(lf[10]))){
if(C_truep(C_i_nullp(*((C_word*)lf[11]+1)))){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_halt(lf[12]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* scheduler.scm:231: loop1 */
t2=((C_word*)((C_word*)t0)[3])[1];
f_988(t2,((C_word*)t0)[2]);}}
else{
/* scheduler.scm:231: loop1 */
t2=((C_word*)((C_word*)t0)[3])[1];
f_988(t2,((C_word*)t0)[2]);}}
else{
t2=C_slot(t1,C_fix(3));
t3=C_eqp(t2,lf[13]);
if(C_truep(t3)){
t4=C_mutate((C_word*)lf[3]+1 /* (set! ##sys#current-thread ...) */,t1);
t5=C_i_setslot(t1,C_fix(3),lf[14]);
t6=C_slot(t1,C_fix(5));
t7=C_slot(t6,C_fix(0));
t8=C_mutate((C_word*)lf[4]+1 /* (set! ##sys#dynamic-winds ...) */,t7);
t9=C_slot(t6,C_fix(1));
t10=C_mutate((C_word*)lf[5]+1 /* (set! ##sys#standard-input ...) */,t9);
t11=C_slot(t6,C_fix(2));
t12=C_mutate((C_word*)lf[6]+1 /* (set! ##sys#standard-output ...) */,t11);
t13=C_slot(t6,C_fix(3));
t14=C_mutate((C_word*)lf[7]+1 /* (set! ##sys#standard-error ...) */,t13);
t15=C_slot(t6,C_fix(4));
t16=C_mutate((C_word*)lf[8]+1 /* (set! ##sys#current-exception-handler ...) */,t15);
t17=C_slot(t6,C_fix(5));
t18=C_mutate((C_word*)lf[9]+1 /* (set! ##sys#current-parameter-vector ...) */,t17);
t19=C_slot(t1,C_fix(9));
t20=C_set_initial_timer_interrupt_period(t19);
/* scheduler.scm:175: ##sys#call-with-cthulhu */
t21=*((C_word*)lf[15]+1);{
C_word av2[3];
av2[0]=t21;
av2[1]=((C_word*)t0)[2];
av2[2]=C_slot(t1,C_fix(1));
((C_proc)(void*)(*((C_word*)t21+1)))(3,av2);}}
else{
/* scheduler.scm:233: loop2 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_1000(t4,((C_word*)t0)[2]);}}}

/* loop in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_fcall f_1061(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_1061,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_nullp(t2))){
t3=lf[10] /* ##sys#timeout-list */ =C_SCHEME_END_OF_LIST;;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=C_u_i_caar(t2);
t4=C_u_i_car(t2);
t5=C_u_i_cdr(t4);
t6=C_slot(t5,C_fix(4));
if(C_truep(C_i_equalp(t3,t6))){
if(C_truep(C_i_greater_or_equalp(((C_word*)t0)[2],t3))){
t7=C_i_set_i_slot(t5,C_fix(13),C_SCHEME_TRUE);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1095,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=t5,tmp=(C_word)a,a+=6,tmp);
/* scheduler.scm:200: ##sys#clear-i/o-state-for-thread! */
f_2268(t8,t5);}
else{
t7=C_mutate(&lf[10] /* (set! ##sys#timeout-list ...) */,t2);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1110,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_nullp(lf[16]))){
t9=C_i_nullp(*((C_word*)lf[11]+1));
t10=t8;
f_1110(t10,(C_truep(t9)?C_i_pairp(lf[10]):C_SCHEME_FALSE));}
else{
t9=t8;
f_1110(t9,C_SCHEME_FALSE);}}}
else{
/* scheduler.scm:219: loop */
t11=t1;
t12=C_u_i_cdr(t2);
t1=t11;
t2=t12;
goto loop;}}}

/* k1093 in loop in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1095(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_1095,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1098,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* scheduler.scm:201: ##sys#thread-basic-unblock! */
t3=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k1096 in k1093 in loop in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1098(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_1098,c,av);}
/* scheduler.scm:202: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_1061(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]));}

/* k1108 in loop in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_fcall f_1110(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_1110,2,t0,t1);}
a=C_alloc(8);
if(C_truep(t1)){
t2=C_u_i_caar(lf[10]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1116,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1136,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* scheduler.scm:212: scheme#round */
t5=*((C_word*)lf[36]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k1114 in k1108 in loop in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1116(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,3)))){
C_save_and_reclaim((void *)f_1116,c,av);}
a=C_alloc(33);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1128,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_s_a_i_minus(&a,2,t1,((C_word*)t0)[4]);
/* scheduler.scm:216: scheme#max */
t4=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=C_fix(0);
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k1126 in k1114 in k1108 in loop in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1128(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1128,c,av);}
if(C_truep(C_i_not(C_msleep(t1)))){
t2=C_mk_bool(C_signal_interrupted_p);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t2=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_FALSE);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k1134 in k1108 in loop in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1136(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_1136,c,av);}
/* scheduler.scm:212: scheme#inexact->exact */
t2=*((C_word*)lf[35]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* ##sys#force-primordial in k905 in k902 */
static void C_ccall f_1170(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_1170,c,av);}
t2=C_i_set_i_slot(*((C_word*)lf[42]+1),C_fix(13),C_SCHEME_FALSE);
/* scheduler.scm:238: ##sys#thread-unblock! */
t3=*((C_word*)lf[43]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=*((C_word*)lf[42]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* ##sys#ready-queue in k905 in k902 */
static void C_ccall f_1181(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1181,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[16];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* ##sys#add-to-ready-queue in k905 in k902 */
static void C_ccall f_1184(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_1184,c,av);}
a=C_alloc(3);
t3=C_i_setslot(t2,C_fix(3),lf[13]);
t4=C_a_i_cons(&a,2,t2,C_SCHEME_END_OF_LIST);
t5=C_eqp(C_SCHEME_END_OF_LIST,lf[16]);
if(C_truep(t5)){
t6=C_mutate(&lf[16] /* (set! ready-queue-head ...) */,t4);
t7=C_mutate(&lf[17] /* (set! ready-queue-tail ...) */,t4);
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t6=C_i_setslot(lf[17],C_fix(1),t4);
t7=C_mutate(&lf[17] /* (set! ready-queue-tail ...) */,t4);
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* ##sys#interrupt-hook in k905 in k902 */
static void C_ccall f_1283(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_1283,c,av);}
a=C_alloc(12);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1287,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t5=C_eqp(t2,C_fix(255));
if(C_truep(t5)){
t6=*((C_word*)lf[3]+1);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1301,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=t3,a[5]=((C_word)li14),tmp=(C_word)a,a+=6,tmp);
t8=C_i_setslot(*((C_word*)lf[3]+1),C_fix(1),t7);
/* scheduler.scm:285: ##sys#schedule */
t9=*((C_word*)lf[2]+1);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}
else{
/* scheduler.scm:286: oldhook */
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}}

/* k1285 in ##sys#interrupt-hook in k905 in k902 */
static void C_ccall f_1287(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1287,c,av);}
/* scheduler.scm:286: oldhook */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* a1300 in ##sys#interrupt-hook in k905 in k902 */
static void C_ccall f_1301(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1301,c,av);}
/* scheduler.scm:284: oldhook */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* ##sys#remove-from-timeout-list in k905 in k902 */
static void C_ccall f_1308(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_1308,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1314,a[2]=t2,a[3]=((C_word)li16),tmp=(C_word)a,a+=4,tmp);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=(
  f_1314(t3,lf[10],C_SCHEME_FALSE)
);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* loop in ##sys#remove-from-timeout-list in k905 in k902 */
static C_word C_fcall f_1314(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
loop:{}
if(C_truep(C_i_nullp(t1))){
return(t1);}
else{
t3=C_slot(t1,C_fix(0));
t4=C_slot(t1,C_fix(1));
t5=C_slot(t3,C_fix(1));
t6=C_eqp(t5,((C_word*)t0)[2]);
if(C_truep(t6)){
if(C_truep(t2)){
return(C_i_setslot(t2,C_fix(1),t4));}
else{
t7=C_mutate(&lf[10] /* (set! ##sys#timeout-list ...) */,t4);
return(t7);}}
else{
t9=t4;
t10=t1;
t1=t9;
t2=t10;
goto loop;}}}

/* ##sys#thread-block-for-timeout! in k905 in k902 */
static void C_ccall f_1349(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_1349,c,av);}
a=C_alloc(13);
if(C_truep(C_i_greaterp(t3,C_fix(0)))){
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1359,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1370,a[2]=t3,a[3]=t2,a[4]=t6,a[5]=((C_word)li18),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_1370(t8,t4,lf[10],C_SCHEME_FALSE);}
else{
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k1357 in ##sys#thread-block-for-timeout! in k905 in k902 */
static void C_ccall f_1359(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1359,c,av);}
t2=C_i_setslot(((C_word*)t0)[2],C_fix(3),lf[47]);
t3=C_i_set_i_slot(((C_word*)t0)[2],C_fix(13),C_SCHEME_FALSE);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_setslot(((C_word*)t0)[2],C_fix(4),((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* loop in ##sys#thread-block-for-timeout! in k905 in k902 */
static void C_fcall f_1370(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_1370,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=C_i_nullp(t2);
t5=(C_truep(t4)?t4:C_i_lessp(((C_word*)t0)[2],C_u_i_caar(t2)));
if(C_truep(t5)){
if(C_truep(t3)){
t6=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t7=C_a_i_cons(&a,2,t6,t2);
t8=t1;{
C_word av2[2];
av2[0]=t8;
av2[1]=C_i_setslot(t3,C_fix(1),t7);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t6=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t7=C_a_i_cons(&a,2,t6,t2);
t8=C_mutate(&lf[10] /* (set! ##sys#timeout-list ...) */,t7);
t9=t1;{
C_word av2[2];
av2[0]=t9;
av2[1]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}}
else{
/* scheduler.scm:311: loop */
t10=t1;
t11=C_u_i_cdr(t2);
t12=t2;
t1=t10;
t2=t11;
t3=t12;
goto loop;}}

/* ##sys#thread-block-for-termination! in k905 in k902 */
static void C_ccall f_1418(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_1418,c,av);}
a=C_alloc(3);
t4=C_slot(t3,C_fix(3));
t5=C_eqp(t4,lf[49]);
t6=(C_truep(t5)?t5:C_eqp(t4,lf[50]));
if(C_truep(t6)){
t7=C_SCHEME_UNDEFINED;
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t7=C_slot(t3,C_fix(12));
t8=C_a_i_cons(&a,2,t2,t7);
t9=C_i_setslot(t3,C_fix(12),t8);
t10=C_i_setslot(t2,C_fix(3),lf[47]);
t11=C_i_set_i_slot(t2,C_fix(13),C_SCHEME_FALSE);
t12=t1;{
C_word *av2=av;
av2[0]=t12;
av2[1]=C_i_setslot(t2,C_fix(11),t3);
((C_proc)(void*)(*((C_word*)t12+1)))(2,av2);}}}

/* k1481 in for-each-loop286 in ##sys#thread-kill! in k905 in k902 */
static void C_ccall f_1483(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_1483,c,av);}
t2=C_i_set_i_slot(((C_word*)t0)[2],C_fix(3),C_SCHEME_END_OF_LIST);
t3=((C_word*)((C_word*)t0)[3])[1];
f_1525(t3,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* for-each-loop300 in for-each-loop286 in ##sys#thread-kill! in k905 in k902 */
static void C_fcall f_1499(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_1499,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1509,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* scheduler.scm:338: ##sys#thread-unblock! */
t4=*((C_word*)lf[43]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k1507 in for-each-loop300 in for-each-loop286 in ##sys#thread-kill! in k905 in k902 */
static void C_ccall f_1509(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_1509,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_1499(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* for-each-loop286 in ##sys#thread-kill! in k905 in k902 */
static void C_fcall f_1525(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_1525,3,t0,t1,t2);}
a=C_alloc(12);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_set_i_slot(t3,C_fix(2),C_SCHEME_FALSE);
t5=C_i_set_i_slot(t3,C_fix(4),C_SCHEME_TRUE);
t6=C_i_set_i_slot(t3,C_fix(5),C_SCHEME_FALSE);
t7=C_slot(t3,C_fix(3));
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1483,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_nullp(t7))){
t9=C_i_set_i_slot(t3,C_fix(3),C_SCHEME_END_OF_LIST);
t13=t1;
t14=C_slot(t2,C_fix(1));
t1=t13;
t2=t14;
goto loop;}
else{
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1499,a[2]=t10,a[3]=((C_word)li23),tmp=(C_word)a,a+=4,tmp));
t12=((C_word*)t10)[1];
f_1499(t12,t8,t7);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* ##sys#thread-kill! in k905 in k902 */
static void C_ccall f_1548(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_1548,c,av);}
a=C_alloc(11);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1552,a[2]=t2,a[3]=t3,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t5=C_slot(t2,C_fix(8));
if(C_truep(C_i_nullp(t5))){
t6=C_SCHEME_UNDEFINED;
t7=t4;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
f_1552(2,av2);}}
else{
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1525,a[2]=t7,a[3]=((C_word)li24),tmp=(C_word)a,a+=4,tmp));
t9=((C_word*)t7)[1];
f_1525(t9,t4,t5);}}

/* k1550 in ##sys#thread-kill! in k905 in k902 */
static void C_ccall f_1552(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_1552,c,av);}
a=C_alloc(9);
t2=C_slot(((C_word*)t0)[2],C_fix(11));
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1558,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_structurep(t2,lf[52]))){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1639,a[2]=t3,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* scheduler.scm:349: delq */
f_909(t4,((C_word*)t0)[2],C_slot(t2,C_fix(2)));}
else{
if(C_truep(C_i_structurep(t2,lf[53]))){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1656,a[2]=t3,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* scheduler.scm:351: delq */
f_909(t4,((C_word*)t0)[2],C_slot(t2,C_fix(12)));}
else{
t4=C_SCHEME_UNDEFINED;
t5=t3;
f_1558(t5,t4);}}}

/* k1556 in k1550 in ##sys#thread-kill! in k905 in k902 */
static void C_fcall f_1558(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_1558,2,t0,t1);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1561,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* scheduler.scm:352: ##sys#remove-from-timeout-list */
t3=*((C_word*)lf[20]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k1559 in k1556 in k1550 in ##sys#thread-kill! in k905 in k902 */
static void C_ccall f_1561(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_1561,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1564,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* scheduler.scm:353: ##sys#clear-i/o-state-for-thread! */
f_2268(t2,((C_word*)t0)[2]);}

/* k1562 in k1559 in k1556 in k1550 in ##sys#thread-kill! in k905 in k902 */
static void C_ccall f_1564(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_1564,c,av);}
a=C_alloc(15);
t2=C_i_setslot(((C_word*)t0)[2],C_fix(3),((C_word*)t0)[3]);
t3=C_i_set_i_slot(((C_word*)t0)[2],C_fix(4),C_SCHEME_FALSE);
t4=C_i_set_i_slot(((C_word*)t0)[2],C_fix(11),C_SCHEME_FALSE);
t5=C_i_set_i_slot(((C_word*)t0)[2],C_fix(8),C_SCHEME_END_OF_LIST);
t6=C_slot(((C_word*)t0)[2],C_fix(12));
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1582,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_nullp(t6))){
t8=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_i_set_i_slot(((C_word*)t0)[2],C_fix(12),C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1589,a[2]=((C_word*)t0)[2],a[3]=((C_word)li21),tmp=(C_word)a,a+=4,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1608,a[2]=t10,a[3]=t8,a[4]=((C_word)li22),tmp=(C_word)a,a+=5,tmp));
t12=((C_word*)t10)[1];
f_1608(t12,t7,t6);}}

/* k1580 in k1562 in k1559 in k1556 in k1550 in ##sys#thread-kill! in k905 in k902 */
static void C_ccall f_1582(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1582,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_i_set_i_slot(((C_word*)t0)[3],C_fix(12),C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* g335 in k1562 in k1559 in k1556 in k1550 in ##sys#thread-kill! in k905 in k902 */
static void C_fcall f_1589(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_1589,3,t0,t1,t2);}
t3=C_slot(t2,C_fix(11));
t4=C_eqp(t3,((C_word*)t0)[2]);
if(C_truep(t4)){
/* scheduler.scm:364: ##sys#thread-basic-unblock! */
t5=*((C_word*)lf[19]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t5=C_SCHEME_UNDEFINED;
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* for-each-loop334 in k1562 in k1559 in k1556 in k1550 in ##sys#thread-kill! in k905 in k902 */
static void C_fcall f_1608(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_1608,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1618,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* scheduler.scm:360: g335 */
t4=((C_word*)t0)[3];
f_1589(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k1616 in for-each-loop334 in k1562 in k1559 in k1556 in k1550 in ##sys#thread-kill! in k905 in k902 */
static void C_ccall f_1618(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_1618,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_1608(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k1637 in k1550 in ##sys#thread-kill! in k905 in k902 */
static void C_ccall f_1639(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1639,c,av);}
t2=((C_word*)t0)[2];
f_1558(t2,C_i_setslot(((C_word*)t0)[3],C_fix(2),t1));}

/* k1654 in k1550 in ##sys#thread-kill! in k905 in k902 */
static void C_ccall f_1656(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1656,c,av);}
t2=((C_word*)t0)[2];
f_1558(t2,C_i_setslot(((C_word*)t0)[3],C_fix(12),t1));}

/* ##sys#thread-basic-unblock! in k905 in k902 */
static void C_ccall f_1662(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_1662,c,av);}
t3=C_i_set_i_slot(t2,C_fix(11),C_SCHEME_FALSE);
t4=C_i_set_i_slot(t2,C_fix(4),C_SCHEME_FALSE);
/* scheduler.scm:372: ##sys#add-to-ready-queue */
t5=*((C_word*)lf[41]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* ##sys#default-exception-handler in k905 in k902 */
static void C_ccall f_1674(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_1674,c,av);}
a=C_alloc(10);
t3=*((C_word*)lf[3]+1);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1678,a[2]=t3,a[3]=t2,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_mk_bool(C_abort_on_thread_exceptions))){
t5=*((C_word*)lf[42]+1);
t6=C_slot(*((C_word*)lf[42]+1),C_fix(1));
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1698,a[2]=t6,a[3]=t2,a[4]=((C_word)li27),tmp=(C_word)a,a+=5,tmp);
t8=C_i_setslot(*((C_word*)lf[42]+1),C_fix(1),t7);
/* scheduler.scm:386: ##sys#thread-unblock! */
t9=*((C_word*)lf[43]+1);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t4;
av2[2]=*((C_word*)lf[42]+1);
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}
else{
/* scheduler.scm:388: ##sys#show-exception-warning */
t5=*((C_word*)lf[56]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[57];
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}}

/* k1676 in ##sys#default-exception-handler in k905 in k902 */
static void C_ccall f_1678(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_1678,c,av);}
a=C_alloc(3);
t2=C_i_setslot(((C_word*)t0)[2],C_fix(7),((C_word*)t0)[3]);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1684,a[2]=((C_word*)t0)[4],tmp=(C_word)a,a+=3,tmp);
/* scheduler.scm:390: ##sys#thread-kill! */
t4=*((C_word*)lf[51]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[50];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k1682 in k1676 in ##sys#default-exception-handler in k905 in k902 */
static void C_ccall f_1684(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1684,c,av);}
/* scheduler.scm:391: ##sys#schedule */
t2=*((C_word*)lf[2]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a1697 in ##sys#default-exception-handler in k905 in k902 */
static void C_ccall f_1698(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_1698,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1702,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* scheduler.scm:384: chicken.condition#signal */
t3=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k1700 in a1697 in ##sys#default-exception-handler in k905 in k902 */
static void C_ccall f_1702(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1702,c,av);}
/* scheduler.scm:385: ptx */
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* loop in k1853 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_fcall f_1722(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(25,0,3)))){
C_save_and_reclaim_args((void *)trf_1722,3,t0,t1,t2);}
a=C_alloc(25);
if(C_truep(C_i_nullp(t2))){
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=C_u_i_caar(t2);
t4=C_SCHEME_FALSE;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_SCHEME_FALSE;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1733,a[2]=t5,a[3]=t7,a[4]=t3,a[5]=((C_word)li5),tmp=(C_word)a,a+=6,tmp);
t9=C_u_i_car(t2);
t10=C_u_i_cdr(t9);
t11=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_1818,a[2]=t5,a[3]=t7,a[4]=t3,a[5]=((C_word*)t0)[2],a[6]=t1,a[7]=t2,tmp=(C_word)a,a+=8,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1830,a[2]=t13,a[3]=t8,a[4]=((C_word)li6),tmp=(C_word)a,a+=5,tmp));
t15=((C_word*)t13)[1];
f_1830(t15,t11,t10);}}

/* g393 in loop in k1853 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_fcall f_1733(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_1733,3,t0,t1,t2);}
a=C_alloc(5);
t3=C_slot(t2,C_fix(11));
if(C_truep(C_i_pairp(t3))){
t4=C_u_i_cdr(t3);
t5=C_eqp(t4,C_SCHEME_TRUE);
t6=(C_truep(t5)?t5:C_eqp(t4,lf[28]));
if(C_truep(t6)){
t7=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_TRUE);
t8=t1;{
C_word av2[2];
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t7=C_eqp(t4,C_SCHEME_FALSE);
t8=(C_truep(t7)?t7:C_eqp(t4,lf[29]));
if(C_truep(t8)){
t9=C_set_block_item(((C_word*)t0)[3],0,C_SCHEME_TRUE);
t10=t1;{
C_word av2[2];
av2[0]=t10;
av2[1]=t9;
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
t9=C_eqp(t4,lf[30]);
if(C_truep(t9)){
t10=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_TRUE);
t11=C_set_block_item(((C_word*)t0)[3],0,C_SCHEME_TRUE);
t12=t1;{
C_word av2[2];
av2[0]=t12;
av2[1]=t11;
((C_proc)(void*)(*((C_word*)t12+1)))(2,av2);}}
else{
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1776,a[2]=t1,a[3]=((C_word*)t0)[4],a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* scheduler.scm:422: chicken.base#open-output-string */
t11=*((C_word*)lf[27]+1);{
C_word av2[2];
av2[0]=t11;
av2[1]=t10;
((C_proc)(void*)(*((C_word*)t11+1)))(2,av2);}}}}}
else{
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k1774 in g393 in loop in k1853 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1776(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1776,c,av);}
a=C_alloc(6);
t2=C_i_check_port_2(t1,C_fix(2),C_SCHEME_TRUE,lf[21]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1782,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* scheduler.scm:422: ##sys#print */
t4=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[38];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k1780 in k1774 in g393 in loop in k1853 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1782(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_1782,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1785,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* scheduler.scm:422: ##sys#print */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k1783 in k1780 in k1774 in g393 in loop in k1853 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1785(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_1785,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1788,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* scheduler.scm:422: ##sys#print */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[37];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k1786 in k1783 in k1780 in k1774 in g393 in loop in k1853 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1788(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_1788,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1791,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* scheduler.scm:422: ##sys#print */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k1789 in k1786 in k1783 in k1780 in k1774 in g393 in loop in k1853 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1791(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_1791,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1794,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* scheduler.scm:422: ##sys#write-char-0 */
t3=*((C_word*)lf[23]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(41);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k1792 in k1789 in k1786 in k1783 in k1780 in k1774 in g393 in loop in k1853 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1794(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1794,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1797,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* scheduler.scm:422: chicken.base#get-output-string */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k1795 in k1792 in k1789 in k1786 in k1783 in k1780 in k1774 in g393 in loop in k1853 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1797(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1797,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_halt(t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k1816 in loop in k1853 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1818(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_1818,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
t3=((C_word*)((C_word*)t0)[3])[1];
t4=stub441(C_SCHEME_UNDEFINED,((C_word*)t0)[4],t2,t3);
/* scheduler.scm:427: loop */
t5=((C_word*)((C_word*)t0)[5])[1];
f_1722(t5,((C_word*)t0)[6],C_u_i_cdr(((C_word*)t0)[7]));}

/* for-each-loop392 in loop in k1853 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_fcall f_1830(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_1830,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1840,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* scheduler.scm:405: g393 */
t4=((C_word*)t0)[3];
f_1733(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k1838 in for-each-loop392 in loop in k1853 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1840(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_1840,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_1830(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k1853 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1855(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_1855,c,av);}
a=C_alloc(6);
t2=stub382(C_SCHEME_UNDEFINED,t1);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1722,a[2]=t4,a[3]=((C_word)li7),tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_1722(t6,((C_word*)t0)[2],*((C_word*)lf[11]+1));}

/* k1887 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1889(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_1889,c,av);}
a=C_alloc(7);
t2=C_i_check_port_2(t1,C_fix(2),C_SCHEME_TRUE,lf[21]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1895,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* scheduler.scm:435: ##sys#print */
t4=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[33];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k1893 in k1887 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1895(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1895,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1898,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* scheduler.scm:435: ##sys#print */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k1896 in k1893 in k1887 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1898(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1898,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1901,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* scheduler.scm:435: ##sys#print */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[32];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k1899 in k1896 in k1893 in k1887 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1901(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_1901,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1904,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* scheduler.scm:435: ##sys#print */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k1902 in k1899 in k1896 in k1893 in k1887 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1904(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_1904,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1907,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* scheduler.scm:435: ##sys#print */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[31];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k1905 in k1902 in k1899 in k1896 in k1893 in k1887 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1907(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_1907,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1910,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* scheduler.scm:435: ##sys#print */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k1908 in k1905 in k1902 in k1899 in k1896 in k1893 in k1887 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1910(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_1910,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1913,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* scheduler.scm:435: ##sys#write-char-0 */
t3=*((C_word*)lf[23]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(41);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k1911 in k1908 in k1905 in k1902 in k1899 in k1896 in k1893 in k1887 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1913(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1913,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1916,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* scheduler.scm:435: chicken.base#get-output-string */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k1914 in k1911 in k1908 in k1905 in k1902 in k1899 in k1896 in k1893 in k1887 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_1916(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1916,c,av);}
t2=((C_word*)t0)[2];
f_2185(t2,C_halt(t1));}

/* ##sys#thread-block-for-i/o! in k905 in k902 */
static void C_ccall f_1934(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_1934,c,av);}
a=C_alloc(14);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1938,a[2]=t2,a[3]=t3,a[4]=t4,a[5]=t1,tmp=(C_word)a,a+=6,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1953,a[2]=t3,a[3]=t2,a[4]=t7,a[5]=((C_word)li29),tmp=(C_word)a,a+=6,tmp));
t9=((C_word*)t7)[1];
f_1953(t9,t5,*((C_word*)lf[11]+1));}

/* k1936 in ##sys#thread-block-for-i/o! in k905 in k902 */
static void C_ccall f_1938(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_1938,c,av);}
a=C_alloc(3);
t2=C_i_setslot(((C_word*)t0)[2],C_fix(3),lf[47]);
t3=C_i_set_i_slot(((C_word*)t0)[2],C_fix(13),C_SCHEME_FALSE);
t4=C_a_i_cons(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]);
t5=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_i_setslot(((C_word*)t0)[2],C_fix(11),t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* loop in ##sys#thread-block-for-i/o! in k905 in k902 */
static void C_fcall f_1953(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_1953,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_nullp(t2))){
t3=C_a_i_list2(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t4=C_a_i_cons(&a,2,t3,*((C_word*)lf[11]+1));
t5=C_mutate((C_word*)lf[11]+1 /* (set! ##sys#fd-list ...) */,t4);
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t3=C_u_i_car(t2);
t4=C_u_i_car(t3);
t5=C_eqp(((C_word*)t0)[2],t4);
if(C_truep(t5)){
t6=C_u_i_cdr(t3);
t7=C_a_i_cons(&a,2,((C_word*)t0)[3],t6);
t8=t1;{
C_word av2[2];
av2[0]=t8;
av2[1]=C_i_setslot(t3,C_fix(1),t7);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
/* scheduler.scm:448: loop */
t9=t1;
t10=C_u_i_cdr(t2);
t1=t9;
t2=t10;
goto loop;}}}

/* k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2001(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_2001,c,av);}
a=C_alloc(11);
t2=C_i_pairp(lf[10]);
t3=C_i_pairp(lf[16]);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2010,a[2]=t3,a[3]=t2,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
t5=(C_truep(t2)?C_i_not(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_u_i_caar(lf[10]);
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2252,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2263,a[2]=t7,tmp=(C_word)a,a+=3,tmp);
/* scheduler.scm:460: scheme#round */
t9=*((C_word*)lf[36]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t9;
av2[1]=t8;
av2[2]=t6;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}
else{
t6=t4;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_fix(0);
f_2010(2,av2);}}}

/* k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2010(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_2010,c,av);}
a=C_alloc(9);
t2=(C_truep(((C_word*)t0)[2])?stub509(C_SCHEME_UNDEFINED,((C_word*)t0)[2],t1):stub509(C_SCHEME_UNDEFINED,((C_word*)t0)[3],t1));
t3=C_eqp(C_fix(-1),t2);
if(C_truep(t3)){
/* scheduler.scm:470: ##sys#force-primordial */
t4=*((C_word*)lf[18]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
if(C_truep(C_fixnum_greaterp(t2,C_fix(0)))){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2034,a[2]=((C_word*)t0)[4],tmp=(C_word)a,a+=3,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2036,a[2]=t6,a[3]=((C_word)li4),tmp=(C_word)a,a+=4,tmp));
t8=((C_word*)t6)[1];
f_2036(t8,t4,t2,C_fix(0),*((C_word*)lf[11]+1));}
else{
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_995(2,av2);}}}}

/* k2032 in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2034(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2034,c,av);}
t2=C_mutate((C_word*)lf[11]+1 /* (set! ##sys#fd-list ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_995(2,av2);}}

/* loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_fcall f_2036(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(33,0,4)))){
C_save_and_reclaim_args((void *)trf_2036,5,t0,t1,t2,t3,t4);}
a=C_alloc(33);
t5=C_i_zerop(t2);
t6=(C_truep(t5)?t5:C_i_nullp(t4));
if(C_truep(t6)){
t7=t1;{
C_word av2[2];
av2[0]=t7;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
t7=C_u_i_car(t4);
t8=C_u_i_car(t7);
t9=C_fd_input_ready(t8,t3);
t10=C_fd_output_ready(t8,t3);
t11=(C_truep(t9)?t9:t10);
if(C_truep(t11)){
t12=C_u_i_cdr(t7);
t13=C_SCHEME_UNDEFINED;
t14=(*a=C_VECTOR_TYPE|1,a[1]=t13,tmp=(C_word)a,a+=2,tmp);
t15=C_set_block_item(t14,0,(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_2063,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=t4,a[6]=t8,a[7]=t14,a[8]=t9,a[9]=t10,a[10]=((C_word)li3),tmp=(C_word)a,a+=11,tmp));
t16=((C_word*)t14)[1];
f_2063(t16,t1,t12,C_SCHEME_END_OF_LIST);}
else{
t12=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2232,a[2]=t1,a[3]=t7,tmp=(C_word)a,a+=4,tmp);
t13=C_s_a_i_plus(&a,2,t3,C_fix(1));
/* scheduler.scm:511: loop */
t17=t12;
t18=t2;
t19=t13;
t20=C_u_i_cdr(t4);
t1=t17;
t2=t18;
t3=t19;
t4=t20;
goto loop;}}}

/* loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_fcall f_2063(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(65,0,4)))){
C_save_and_reclaim_args((void *)trf_2063,4,t0,t1,t2,t3);}
a=C_alloc(65);
if(C_truep(C_i_nullp(t2))){
if(C_truep(C_i_nullp(t3))){
t4=C_s_a_i_minus(&a,2,((C_word*)t0)[2],C_fix(1));
t5=C_s_a_i_plus(&a,2,((C_word*)t0)[3],C_fix(1));
/* scheduler.scm:487: loop */
t6=((C_word*)((C_word*)t0)[4])[1];
f_2036(t6,t1,t4,t5,C_u_i_cdr(((C_word*)t0)[5]));}
else{
t4=C_a_i_cons(&a,2,((C_word*)t0)[6],t3);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2098,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t6=C_s_a_i_minus(&a,2,((C_word*)t0)[2],C_fix(1));
t7=C_s_a_i_plus(&a,2,((C_word*)t0)[3],C_fix(1));
/* scheduler.scm:489: loop */
t8=((C_word*)((C_word*)t0)[4])[1];
f_2036(t8,t5,t6,t7,C_u_i_cdr(((C_word*)t0)[5]));}}
else{
t4=C_u_i_car(t2);
t5=C_slot(t4,C_fix(11));
if(C_truep(C_slot(t4,C_fix(13)))){
/* scheduler.scm:495: loop2 */
t18=t1;
t19=C_u_i_cdr(t2);
t20=t3;
t1=t18;
t2=t19;
t3=t20;
goto loop;}
else{
t6=C_i_pairp(t5);
if(C_truep(C_i_not(t6))){
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2132,a[2]=((C_word*)t0)[7],a[3]=t1,a[4]=t2,a[5]=t3,a[6]=t4,tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_slot(t4,C_fix(4)))){
/* scheduler.scm:500: ##sys#remove-from-timeout-list */
t8=*((C_word*)lf[20]+1);{
C_word av2[3];
av2[0]=t8;
av2[1]=t7;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}
else{
t8=t7;{
C_word av2[2];
av2[0]=t8;
av2[1]=C_SCHEME_UNDEFINED;
f_2132(2,av2);}}}
else{
t7=C_u_i_car(t5);
t8=C_eqp(((C_word*)t0)[6],t7);
if(C_truep(C_i_not(t8))){
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2156,a[2]=t1,a[3]=((C_word*)t0)[6],a[4]=t5,tmp=(C_word)a,a+=5,tmp);
/* scheduler.scm:504: chicken.base#open-output-string */
t10=*((C_word*)lf[27]+1);{
C_word av2[2];
av2[0]=t10;
av2[1]=t9;
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
t9=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2185,a[2]=((C_word*)t0)[7],a[3]=t1,a[4]=t2,a[5]=t3,a[6]=t4,tmp=(C_word)a,a+=7,tmp);
t10=C_u_i_cdr(t5);
t11=C_eqp(t10,C_SCHEME_TRUE);
t12=(C_truep(t11)?t11:C_eqp(t10,lf[28]));
if(C_truep(t12)){
t13=t9;
f_2185(t13,((C_word*)t0)[8]);}
else{
t13=C_eqp(t10,C_SCHEME_FALSE);
t14=(C_truep(t13)?t13:C_eqp(t10,lf[29]));
if(C_truep(t14)){
t15=t9;
f_2185(t15,((C_word*)t0)[9]);}
else{
t15=C_eqp(t10,lf[30]);
if(C_truep(t15)){
t16=t9;
f_2185(t16,(C_truep(((C_word*)t0)[8])?((C_word*)t0)[8]:((C_word*)t0)[9]));}
else{
t16=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1889,a[2]=t9,a[3]=((C_word*)t0)[9],a[4]=((C_word*)t0)[8],a[5]=t10,tmp=(C_word)a,a+=6,tmp);
/* scheduler.scm:435: chicken.base#open-output-string */
t17=*((C_word*)lf[27]+1);{
C_word av2[2];
av2[0]=t17;
av2[1]=t16;
((C_proc)(void*)(*((C_word*)t17+1)))(2,av2);}}}}}}}}}

/* k2096 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2098(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_2098,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k2130 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2132(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_2132,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2135,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* scheduler.scm:501: ##sys#thread-basic-unblock! */
t3=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k2133 in k2130 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2135(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2135,c,av);}
/* scheduler.scm:502: loop2 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2063(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]),((C_word*)t0)[5]);}

/* k2154 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2156(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_2156,c,av);}
a=C_alloc(6);
t2=C_i_check_port_2(t1,C_fix(2),C_SCHEME_TRUE,lf[21]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2162,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* scheduler.scm:504: ##sys#print */
t4=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[26];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k2160 in k2154 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2162(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_2162,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2165,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* scheduler.scm:504: ##sys#print */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_u_i_car(((C_word*)t0)[5]);
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k2163 in k2160 in k2154 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2165(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_2165,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2168,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* scheduler.scm:504: ##sys#print */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[25];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k2166 in k2163 in k2160 in k2154 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2168(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_2168,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2171,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* scheduler.scm:504: ##sys#print */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k2169 in k2166 in k2163 in k2160 in k2154 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2171(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_2171,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2174,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* scheduler.scm:504: ##sys#write-char-0 */
t3=*((C_word*)lf[23]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(41);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k2172 in k2169 in k2166 in k2163 in k2160 in k2154 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2174(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2174,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2177,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* scheduler.scm:504: chicken.base#get-output-string */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k2175 in k2172 in k2169 in k2166 in k2163 in k2160 in k2154 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2177(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2177,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_halt(t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k2183 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_fcall f_2185(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_2185,2,t0,t1);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2188,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_slot(((C_word*)t0)[6],C_fix(4)))){
/* scheduler.scm:507: ##sys#remove-from-timeout-list */
t3=*((C_word*)lf[20]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=t2;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_2188(2,av2);}}}
else{
t2=C_u_i_cdr(((C_word*)t0)[4]);
t3=C_a_i_cons(&a,2,((C_word*)t0)[6],((C_word*)t0)[5]);
/* scheduler.scm:510: loop2 */
t4=((C_word*)((C_word*)t0)[2])[1];
f_2063(t4,((C_word*)t0)[3],t2,t3);}}

/* k2186 in k2183 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2188(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_2188,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2191,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* scheduler.scm:508: ##sys#thread-basic-unblock! */
t3=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k2189 in k2186 in k2183 in loop2 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2191(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2191,c,av);}
/* scheduler.scm:509: loop2 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2063(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]),((C_word*)t0)[5]);}

/* k2230 in loop in k2008 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2232(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_2232,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k2250 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2252(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,c,3)))){
C_save_and_reclaim((void *)f_2252,c,av);}
a=C_alloc(36);
t2=C_a_i_current_milliseconds(&a,1,C_SCHEME_FALSE);
t3=C_s_a_i_minus(&a,2,t1,t2);
/* scheduler.scm:462: scheme#max */
t4=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(0);
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2261 in k1999 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_2263(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_2263,c,av);}
/* scheduler.scm:460: scheme#inexact->exact */
t2=*((C_word*)lf[35]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* ##sys#clear-i/o-state-for-thread! in k905 in k902 */
static void C_fcall f_2268(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,3)))){
C_save_and_reclaim_args((void *)trf_2268,2,t1,t2);}
a=C_alloc(11);
t3=C_slot(t2,C_fix(11));
if(C_truep(C_i_pairp(t3))){
t4=C_slot(t2,C_fix(11));
t5=C_u_i_car(t4);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2282,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2284,a[2]=t5,a[3]=t2,a[4]=t8,a[5]=((C_word)li31),tmp=(C_word)a,a+=6,tmp));
t10=((C_word*)t8)[1];
f_2284(t10,t6,*((C_word*)lf[11]+1));}
else{
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k2280 in ##sys#clear-i/o-state-for-thread! in k905 in k902 */
static void C_ccall f_2282(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2282,c,av);}
t2=C_mutate((C_word*)lf[11]+1 /* (set! ##sys#fd-list ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* loop in ##sys#clear-i/o-state-for-thread! in k905 in k902 */
static void C_fcall f_2284(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_2284,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_u_i_car(t2);
t4=C_u_i_car(t3);
t5=C_eqp(((C_word*)t0)[2],t4);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2306,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* scheduler.scm:526: delq */
f_909(t6,((C_word*)t0)[3],C_u_i_cdr(t3));}
else{
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2325,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* scheduler.scm:531: loop */
t8=t6;
t9=C_u_i_cdr(t2);
t1=t8;
t2=t9;
goto loop;}}}

/* k2304 in loop in ##sys#clear-i/o-state-for-thread! in k905 in k902 */
static void C_ccall f_2306(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2306,c,av);}
if(C_truep(C_i_nullp(t1))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_u_i_cdr(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_i_setslot(((C_word*)t0)[4],C_fix(1),t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k2323 in loop in ##sys#clear-i/o-state-for-thread! in k905 in k902 */
static void C_ccall f_2325(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_2325,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* ##sys#all-threads in k905 in k902 */
static void C_ccall f_2337(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,6)))){
C_save_and_reclaim((void *)f_2337,c,av);}
a=C_alloc(10);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2472,a[2]=((C_word)li33),tmp=(C_word)a,a+=3,tmp):C_get_rest_arg(c,2,av,2,t0));
t4=C_rest_nullp(c,2);
t5=C_rest_nullp(c,3);
t6=(C_truep(t5)?C_SCHEME_END_OF_LIST:C_get_rest_arg(c,3,av,2,t0));
t7=C_rest_nullp(c,3);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2355,a[2]=t9,a[3]=t3,a[4]=((C_word)li37),tmp=(C_word)a,a+=5,tmp));
t11=((C_word*)t9)[1];
f_2355(t11,t1,lf[16],t6);}

/* loop in ##sys#all-threads in k905 in k902 */
static void C_fcall f_2355(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,5)))){
C_save_and_reclaim_args((void *)trf_2355,4,t0,t1,t2,t3);}
a=C_alloc(7);
if(C_truep(C_i_pairp(t2))){
t4=C_u_i_cdr(t2);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2371,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* scheduler.scm:544: cns */
t6=((C_word*)t0)[3];{
C_word av2[6];
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[13];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_u_i_car(t2);
av2[5]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(6,av2);}}
else{
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2378,a[2]=t5,a[3]=((C_word*)t0)[3],a[4]=((C_word)li36),tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_2378(t7,t1,*((C_word*)lf[11]+1),t3);}}

/* k2369 in loop in ##sys#all-threads in k905 in k902 */
static void C_ccall f_2371(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2371,c,av);}
/* scheduler.scm:544: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2355(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* loop in loop in ##sys#all-threads in k905 in k902 */
static void C_fcall f_2378(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,4)))){
C_save_and_reclaim_args((void *)trf_2378,4,t0,t1,t2,t3);}
a=C_alloc(14);
if(C_truep(C_i_pairp(t2))){
t4=C_u_i_cdr(t2);
t5=C_u_i_caar(t2);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2397,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t7=C_u_i_car(t2);
t8=C_u_i_cdr(t7);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2402,a[2]=t3,a[3]=((C_word*)t0)[3],a[4]=t5,a[5]=t10,a[6]=((C_word)li34),tmp=(C_word)a,a+=7,tmp));
t12=((C_word*)t10)[1];
f_2402(t12,t6,t8);}
else{
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2427,a[2]=t5,a[3]=((C_word*)t0)[3],a[4]=((C_word)li35),tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_2427(t7,t1,lf[10],t3);}}

/* k2395 in loop in loop in ##sys#all-threads in k905 in k902 */
static void C_ccall f_2397(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2397,c,av);}
/* scheduler.scm:547: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2378(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* loop in loop in loop in ##sys#all-threads in k905 in k902 */
static void C_fcall f_2402(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_2402,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_u_i_car(t2);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2420,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* scheduler.scm:551: loop */
t6=t4;
t7=C_u_i_cdr(t2);
t1=t6;
t2=t7;
goto loop;}}

/* k2418 in loop in loop in loop in ##sys#all-threads in k905 in k902 */
static void C_ccall f_2420(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2420,c,av);}
/* scheduler.scm:551: cns */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[60];
av2[3]=((C_word*)t0)[4];
av2[4]=((C_word*)t0)[5];
av2[5]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* loop in loop in loop in ##sys#all-threads in k905 in k902 */
static void C_fcall f_2427(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,5)))){
C_save_and_reclaim_args((void *)trf_2427,4,t0,t1,t2,t3);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t4=C_u_i_cdr(t2);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2443,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t6=C_u_i_caar(t2);
t7=C_u_i_car(t2);
/* scheduler.scm:554: cns */
t8=((C_word*)t0)[3];{
C_word av2[6];
av2[0]=t8;
av2[1]=t5;
av2[2]=lf[61];
av2[3]=t6;
av2[4]=C_u_i_cdr(t7);
av2[5]=t3;
((C_proc)(void*)(*((C_word*)t8+1)))(6,av2);}}
else{
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k2441 in loop in loop in loop in ##sys#all-threads in k905 in k902 */
static void C_ccall f_2443(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2443,c,av);}
/* scheduler.scm:554: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2427(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* f_2472 in ##sys#all-threads in k905 in k902 */
static void C_ccall f_2472(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_2472,c,av);}
a=C_alloc(3);
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_cons(&a,2,t4,t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* ##sys#fetch-and-clear-threads in k905 in k902 */
static void C_ccall f_2481(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_2481,c,av);}
a=C_alloc(5);
t2=C_a_i_vector4(&a,4,lf[16],lf[17],*((C_word*)lf[11]+1),lf[10]);
t3=lf[16] /* ready-queue-head */ =C_SCHEME_END_OF_LIST;;
t4=lf[17] /* ready-queue-tail */ =C_SCHEME_END_OF_LIST;;
t5=C_set_block_item(lf[11] /* ##sys#fd-list */,0,C_SCHEME_END_OF_LIST);
t6=lf[10] /* ##sys#timeout-list */ =C_SCHEME_END_OF_LIST;;
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}

/* ##sys#restore-threads in k905 in k902 */
static void C_ccall f_2491(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2491,c,av);}
t3=C_slot(t2,C_fix(0));
t4=C_mutate(&lf[16] /* (set! ready-queue-head ...) */,t3);
t5=C_slot(t2,C_fix(1));
t6=C_mutate(&lf[17] /* (set! ready-queue-tail ...) */,t5);
t7=C_slot(t2,C_fix(2));
t8=C_mutate((C_word*)lf[11]+1 /* (set! ##sys#fd-list ...) */,t7);
t9=C_slot(t2,C_fix(3));
t10=C_mutate(&lf[10] /* (set! ##sys#timeout-list ...) */,t9);
t11=t1;{
C_word *av2=av;
av2[0]=t11;
av2[1]=t10;
((C_proc)(void*)(*((C_word*)t11+1)))(2,av2);}}

/* ##sys#thread-unblock! in k905 in k902 */
static void C_ccall f_2510(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2510,c,av);}
a=C_alloc(4);
t3=C_slot(t2,C_fix(3));
t4=C_eqp(lf[47],t3);
t5=(C_truep(t4)?t4:C_eqp(lf[64],C_slot(t2,C_fix(3))));
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2523,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* scheduler.scm:583: ##sys#remove-from-timeout-list */
t7=*((C_word*)lf[20]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}
else{
t6=C_SCHEME_UNDEFINED;
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* k2521 in ##sys#thread-unblock! in k905 in k902 */
static void C_ccall f_2523(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2523,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2526,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* scheduler.scm:584: ##sys#clear-i/o-state-for-thread! */
f_2268(t2,((C_word*)t0)[3]);}

/* k2524 in k2521 in ##sys#thread-unblock! in k905 in k902 */
static void C_ccall f_2526(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_2526,c,av);}
/* scheduler.scm:585: ##sys#thread-basic-unblock! */
t2=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* ##sys#thread-sleep! in k905 in k902 */
static void C_ccall f_2542(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_2542,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2548,a[2]=t2,a[3]=((C_word)li43),tmp=(C_word)a,a+=4,tmp);
/* scheduler.scm:591: ##sys#call-with-current-continuation */{
C_word *av2=av;
av2[0]=0;
av2[1]=t1;
av2[2]=t3;
C_call_cc(3,av2);}}

/* a2547 in ##sys#thread-sleep! in k905 in k902 */
static void C_ccall f_2548(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_2548,c,av);}
a=C_alloc(7);
t3=*((C_word*)lf[3]+1);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2560,a[2]=t2,a[3]=((C_word)li42),tmp=(C_word)a,a+=4,tmp);
t5=C_i_setslot(*((C_word*)lf[3]+1),C_fix(1),t4);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2555,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* scheduler.scm:595: ##sys#thread-block-for-timeout! */
t7=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=*((C_word*)lf[3]+1);
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}

/* k2553 in a2547 in ##sys#thread-sleep! in k905 in k902 */
static void C_ccall f_2555(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2555,c,av);}
/* scheduler.scm:596: ##sys#schedule */
t2=*((C_word*)lf[2]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a2559 in a2547 in ##sys#thread-sleep! in k905 in k902 */
static void C_ccall f_2560(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_2560,c,av);}
/* scheduler.scm:594: return */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* chicken.base#sleep-hook in k905 in k902 */
static void C_ccall f_2566(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(69,c,2)))){
C_save_and_reclaim((void *)f_2566,c,av);}
a=C_alloc(69);
t3=C_s_a_i_times(&a,2,lf[67],t2);
t4=C_s_a_i_plus(&a,2,C_a_i_current_milliseconds(&a,1,C_SCHEME_FALSE),t3);
/* scheduler.scm:603: ##sys#thread-sleep! */
t5=*((C_word*)lf[65]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t1;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* ##sys#kill-other-threads in k905 in k902 */
static void C_ccall f_2580(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(31,c,2)))){
C_save_and_reclaim((void *)f_2580,c,av);}
a=C_alloc(31);
t3=*((C_word*)lf[3]+1);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2583,a[2]=t3,a[3]=((C_word)li46),tmp=(C_word)a,a+=4,tmp);
t5=C_mutate((C_word*)lf[42]+1 /* (set! ##sys#primordial-thread ...) */,*((C_word*)lf[3]+1));
t6=C_a_i_list1(&a,1,*((C_word*)lf[3]+1));
t7=C_mutate(&lf[16] /* (set! ready-queue-head ...) */,t6);
t8=C_mutate(&lf[17] /* (set! ready-queue-tail ...) */,lf[16]);
t9=(
/* scheduler.scm:623: suspend */
  f_2583(t4,*((C_word*)lf[3]+1))
);
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2609,a[2]=t4,a[3]=((C_word)li47),tmp=(C_word)a,a+=4,tmp);
t11=lf[10];
t12=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2687,a[2]=t10,a[3]=((C_word)li48),tmp=(C_word)a,a+=4,tmp);
t13=(
  f_2687(t12,lf[10])
);
t14=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2630,a[2]=t4,a[3]=((C_word)li49),tmp=(C_word)a,a+=4,tmp);
t15=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2621,a[2]=t14,a[3]=((C_word)li50),tmp=(C_word)a,a+=4,tmp);
t16=*((C_word*)lf[11]+1);
t17=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2664,a[2]=t15,a[3]=((C_word)li51),tmp=(C_word)a,a+=4,tmp);
t18=(
  f_2664(t17,*((C_word*)lf[11]+1))
);
t19=lf[10] /* ##sys#timeout-list */ =C_SCHEME_END_OF_LIST;;
t20=C_set_block_item(lf[11] /* ##sys#fd-list */,0,C_SCHEME_END_OF_LIST);
t21=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2659,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* scheduler.scm:628: thunk */
t22=t2;{
C_word *av2=av;
av2[0]=t22;
av2[1]=t21;
((C_proc)(void*)(*((C_word*)t22+1)))(2,av2);}}

/* suspend in ##sys#kill-other-threads in k905 in k902 */
static C_word C_fcall f_2583(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;{}
t2=C_eqp(t1,((C_word*)t0)[2]);
t3=(C_truep(t2)?C_SCHEME_UNDEFINED:C_i_setslot(t1,C_fix(3),lf[70]));
t4=C_i_set_i_slot(t1,C_fix(11),C_SCHEME_FALSE);
return(C_i_set_i_slot(t1,C_fix(12),C_SCHEME_END_OF_LIST));}

/* g646 in ##sys#kill-other-threads in k905 in k902 */
static C_word C_fcall f_2609(C_word t0,C_word t1){
C_word tmp;
C_word t2;{}
return((
/* scheduler.scm:624: suspend */
  f_2583(((C_word*)t0)[2],C_u_i_cdr(t1))
));}

/* g656 in ##sys#kill-other-threads in k905 in k902 */
static C_word C_fcall f_2621(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;{}
t2=C_u_i_cdr(t1);
return((
  f_2630(((C_word*)t0)[2],t2)
));}

/* for-each-loop678 in ##sys#kill-other-threads in k905 in k902 */
static C_word C_fcall f_2630(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
loop:{}
if(C_truep(C_i_pairp(t1))){
t2=(
/* scheduler.scm:625: g679 */
  f_2583(((C_word*)t0)[2],C_slot(t1,C_fix(0)))
);
t4=C_slot(t1,C_fix(1));
t1=t4;
goto loop;}
else{
t2=C_SCHEME_UNDEFINED;
return(t2);}}

/* k2657 in ##sys#kill-other-threads in k905 in k902 */
static void C_ccall f_2659(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2659,c,av);}
/* scheduler.scm:629: exit */
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* for-each-loop655 in ##sys#kill-other-threads in k905 in k902 */
static C_word C_fcall f_2664(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
loop:{}
if(C_truep(C_i_pairp(t1))){
t2=(
/* scheduler.scm:625: g656 */
  f_2621(((C_word*)t0)[2],C_slot(t1,C_fix(0)))
);
t4=C_slot(t1,C_fix(1));
t1=t4;
goto loop;}
else{
t2=C_SCHEME_UNDEFINED;
return(t2);}}

/* for-each-loop645 in ##sys#kill-other-threads in k905 in k902 */
static C_word C_fcall f_2687(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
loop:{}
if(C_truep(C_i_pairp(t1))){
t2=(
/* scheduler.scm:624: g646 */
  f_2609(((C_word*)t0)[2],C_slot(t1,C_fix(0)))
);
t4=C_slot(t1,C_fix(1));
t1=t4;
goto loop;}
else{
t2=C_SCHEME_UNDEFINED;
return(t2);}}

/* k902 */
static void C_ccall f_904(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_904,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_907,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

/* k905 in k902 */
static void C_ccall f_907(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(73,c,5)))){
C_save_and_reclaim((void *)f_907,c,av);}
a=C_alloc(73);
t2=C_a_i_provide(&a,1,lf[0]);
t3=C_mutate(&lf[1] /* (set! delq ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_909,a[2]=((C_word)li1),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[2]+1 /* (set! ##sys#schedule ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_952,a[2]=((C_word)li10),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[18]+1 /* (set! ##sys#force-primordial ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1170,a[2]=((C_word)li11),tmp=(C_word)a,a+=3,tmp));
t6=lf[16] /* ready-queue-head */ =C_SCHEME_END_OF_LIST;;
t7=lf[17] /* ready-queue-tail */ =C_SCHEME_END_OF_LIST;;
t8=C_mutate((C_word*)lf[44]+1 /* (set! ##sys#ready-queue ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1181,a[2]=((C_word)li12),tmp=(C_word)a,a+=3,tmp));
t9=C_mutate((C_word*)lf[41]+1 /* (set! ##sys#add-to-ready-queue ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1184,a[2]=((C_word)li13),tmp=(C_word)a,a+=3,tmp));
t10=*((C_word*)lf[45]+1);
t11=C_mutate((C_word*)lf[45]+1 /* (set! ##sys#interrupt-hook ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1283,a[2]=t10,a[3]=((C_word)li15),tmp=(C_word)a,a+=4,tmp));
t12=lf[10] /* ##sys#timeout-list */ =C_SCHEME_END_OF_LIST;;
t13=C_mutate((C_word*)lf[20]+1 /* (set! ##sys#remove-from-timeout-list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1308,a[2]=((C_word)li17),tmp=(C_word)a,a+=3,tmp));
t14=C_mutate((C_word*)lf[46]+1 /* (set! ##sys#thread-block-for-timeout! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1349,a[2]=((C_word)li19),tmp=(C_word)a,a+=3,tmp));
t15=C_mutate((C_word*)lf[48]+1 /* (set! ##sys#thread-block-for-termination! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1418,a[2]=((C_word)li20),tmp=(C_word)a,a+=3,tmp));
t16=C_mutate((C_word*)lf[51]+1 /* (set! ##sys#thread-kill! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1548,a[2]=((C_word)li25),tmp=(C_word)a,a+=3,tmp));
t17=C_mutate((C_word*)lf[19]+1 /* (set! ##sys#thread-basic-unblock! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1662,a[2]=((C_word)li26),tmp=(C_word)a,a+=3,tmp));
t18=C_mutate((C_word*)lf[54]+1 /* (set! ##sys#default-exception-handler ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1674,a[2]=((C_word)li28),tmp=(C_word)a,a+=3,tmp));
t19=C_set_block_item(lf[11] /* ##sys#fd-list */,0,C_SCHEME_END_OF_LIST);
t20=C_mutate((C_word*)lf[58]+1 /* (set! ##sys#thread-block-for-i/o! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1934,a[2]=((C_word)li30),tmp=(C_word)a,a+=3,tmp));
t21=C_mutate(&lf[40] /* (set! ##sys#clear-i/o-state-for-thread! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2268,a[2]=((C_word)li32),tmp=(C_word)a,a+=3,tmp));
t22=C_mutate((C_word*)lf[59]+1 /* (set! ##sys#all-threads ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2337,a[2]=((C_word)li38),tmp=(C_word)a,a+=3,tmp));
t23=C_mutate((C_word*)lf[62]+1 /* (set! ##sys#fetch-and-clear-threads ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2481,a[2]=((C_word)li39),tmp=(C_word)a,a+=3,tmp));
t24=C_mutate((C_word*)lf[63]+1 /* (set! ##sys#restore-threads ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2491,a[2]=((C_word)li40),tmp=(C_word)a,a+=3,tmp));
t25=C_mutate((C_word*)lf[43]+1 /* (set! ##sys#thread-unblock! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2510,a[2]=((C_word)li41),tmp=(C_word)a,a+=3,tmp));
t26=C_mutate((C_word*)lf[65]+1 /* (set! ##sys#thread-sleep! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2542,a[2]=((C_word)li44),tmp=(C_word)a,a+=3,tmp));
t27=C_mutate((C_word*)lf[66]+1 /* (set! chicken.base#sleep-hook ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2566,a[2]=((C_word)li45),tmp=(C_word)a,a+=3,tmp));
t28=*((C_word*)lf[68]+1);
t29=C_mutate((C_word*)lf[69]+1 /* (set! ##sys#kill-other-threads ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2580,a[2]=t28,a[3]=((C_word)li52),tmp=(C_word)a,a+=4,tmp));
t30=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t30;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t30+1)))(2,av2);}}

/* delq in k905 in k902 */
static void C_fcall f_909(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_909,3,t1,t2,t3);}
a=C_alloc(7);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_915,a[2]=t2,a[3]=t5,a[4]=((C_word)li0),tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_915(t7,t1,t3);}

/* loop in delq in k905 in k902 */
static void C_fcall f_915(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_915,3,t0,t1,t2);}
a=C_alloc(4);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_slot(t2,C_fix(0));
t4=C_eqp(((C_word*)t0)[2],t3);
if(C_truep(t4)){
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_slot(t2,C_fix(0));
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_942,a[2]=t1,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
/* scheduler.scm:162: loop */
t8=t6;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}}}

/* k940 in loop in delq in k905 in k902 */
static void C_ccall f_942(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_942,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* ##sys#schedule in k905 in k902 */
static void C_ccall f_952(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_952,c,av);}
a=C_alloc(6);
t2=*((C_word*)lf[3]+1);
t3=C_SCHEME_FALSE;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_slot(*((C_word*)lf[3]+1),C_fix(3));
t6=C_slot(*((C_word*)lf[3]+1),C_fix(5));
t7=C_i_setslot(t6,C_fix(0),*((C_word*)lf[4]+1));
t8=C_i_setslot(t6,C_fix(1),*((C_word*)lf[5]+1));
t9=C_i_setslot(t6,C_fix(2),*((C_word*)lf[6]+1));
t10=C_i_setslot(t6,C_fix(3),*((C_word*)lf[7]+1));
t11=C_i_setslot(t6,C_fix(4),*((C_word*)lf[8]+1));
t12=C_i_setslot(t6,C_fix(5),*((C_word*)lf[9]+1));
t13=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_983,a[2]=t4,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t14=C_eqp(t5,lf[14]);
t15=(C_truep(t14)?t14:C_eqp(t5,lf[13]));
if(C_truep(t15)){
t16=C_i_set_i_slot(*((C_word*)lf[3]+1),C_fix(13),C_SCHEME_FALSE);
/* scheduler.scm:184: ##sys#add-to-ready-queue */
t17=*((C_word*)lf[41]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t17;
av2[1]=t13;
av2[2]=*((C_word*)lf[3]+1);
((C_proc)(void*)(*((C_word*)t17+1)))(3,av2);}}
else{
t16=t13;{
C_word *av2=av;
av2[0]=t16;
av2[1]=C_SCHEME_UNDEFINED;
f_983(2,av2);}}}

/* k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_983(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_983,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_988,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word)li9),tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];
f_988(t5,((C_word*)t0)[3]);}

/* loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_fcall f_988(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,0,3)))){
C_save_and_reclaim_args((void *)trf_988,2,t0,t1);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_992,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_nullp(lf[10]))){
t3=t2;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_992(2,av2);}}
else{
t3=C_a_i_current_milliseconds(&a,1,C_SCHEME_FALSE);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1061,a[2]=t3,a[3]=t5,a[4]=((C_word*)t0)[3],a[5]=((C_word)li8),tmp=(C_word)a,a+=6,tmp));
t7=((C_word*)t5)[1];
f_1061(t7,t2,lf[10]);}}

/* k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_992(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_992,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_995,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(((C_word*)((C_word*)t0)[4])[1])){
/* scheduler.scm:222: ##sys#force-primordial */
t3=*((C_word*)lf[18]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
if(C_truep(C_i_nullp(*((C_word*)lf[11]+1)))){
t3=C_SCHEME_UNDEFINED;
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
f_995(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2001,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1855,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* scheduler.scm:399: ##sys#length */
t5=*((C_word*)lf[39]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}}}

/* k993 in k990 in loop1 in k981 in ##sys#schedule in k905 in k902 */
static void C_ccall f_995(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_995,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1000,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word)li2),tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];
f_1000(t5,((C_word*)t0)[3]);}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_scheduler_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("scheduler"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_scheduler_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(396))){
C_save(t1);
C_rereclaim2(396*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,71);
lf[0]=C_h_intern(&lf[0],9, C_text("scheduler"));
lf[2]=C_h_intern(&lf[2],14, C_text("##sys#schedule"));
lf[3]=C_h_intern(&lf[3],20, C_text("##sys#current-thread"));
lf[4]=C_h_intern(&lf[4],19, C_text("##sys#dynamic-winds"));
lf[5]=C_h_intern(&lf[5],20, C_text("##sys#standard-input"));
lf[6]=C_h_intern(&lf[6],21, C_text("##sys#standard-output"));
lf[7]=C_h_intern(&lf[7],20, C_text("##sys#standard-error"));
lf[8]=C_h_intern(&lf[8],31, C_text("##sys#current-exception-handler"));
lf[9]=C_h_intern(&lf[9],30, C_text("##sys#current-parameter-vector"));
lf[11]=C_h_intern(&lf[11],13, C_text("##sys#fd-list"));
lf[12]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010deadlock"));
lf[13]=C_h_intern(&lf[13],5, C_text("ready"));
lf[14]=C_h_intern(&lf[14],7, C_text("running"));
lf[15]=C_h_intern(&lf[15],23, C_text("##sys#call-with-cthulhu"));
lf[18]=C_h_intern(&lf[18],22, C_text("##sys#force-primordial"));
lf[19]=C_h_intern(&lf[19],27, C_text("##sys#thread-basic-unblock!"));
lf[20]=C_h_intern(&lf[20],30, C_text("##sys#remove-from-timeout-list"));
lf[21]=C_h_intern(&lf[21],6, C_text("format"));
lf[22]=C_h_intern(&lf[22],30, C_text("chicken.base#get-output-string"));
lf[23]=C_h_intern(&lf[23],18, C_text("##sys#write-char-0"));
lf[24]=C_h_intern(&lf[24],11, C_text("##sys#print"));
lf[25]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013 (expected "));
lf[26]=C_decode_literal(C_heaptop,C_text("\376B\000\0009thread is registered for I/O on unknown file-descriptor: "));
lf[27]=C_h_intern(&lf[27],31, C_text("chicken.base#open-output-string"));
lf[28]=C_h_intern_kw(&lf[28],5, C_text("input"));
lf[29]=C_h_intern_kw(&lf[29],6, C_text("output"));
lf[30]=C_h_intern_kw(&lf[30],3, C_text("all"));
lf[31]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006, o = "));
lf[32]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006 (i = "));
lf[33]=C_decode_literal(C_heaptop,C_text("\376B\000\000#fdset-test: invalid i/o direction: "));
lf[34]=C_h_intern(&lf[34],10, C_text("scheme#max"));
lf[35]=C_h_intern(&lf[35],21, C_text("scheme#inexact->exact"));
lf[36]=C_h_intern(&lf[36],12, C_text("scheme#round"));
lf[37]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007 (fd = "));
lf[38]=C_decode_literal(C_heaptop,C_text("\376B\000\000%create-fdset: invalid i/o direction: "));
lf[39]=C_h_intern(&lf[39],12, C_text("##sys#length"));
lf[41]=C_h_intern(&lf[41],24, C_text("##sys#add-to-ready-queue"));
lf[42]=C_h_intern(&lf[42],23, C_text("##sys#primordial-thread"));
lf[43]=C_h_intern(&lf[43],21, C_text("##sys#thread-unblock!"));
lf[44]=C_h_intern(&lf[44],17, C_text("##sys#ready-queue"));
lf[45]=C_h_intern(&lf[45],20, C_text("##sys#interrupt-hook"));
lf[46]=C_h_intern(&lf[46],31, C_text("##sys#thread-block-for-timeout!"));
lf[47]=C_h_intern(&lf[47],7, C_text("blocked"));
lf[48]=C_h_intern(&lf[48],35, C_text("##sys#thread-block-for-termination!"));
lf[49]=C_h_intern(&lf[49],4, C_text("dead"));
lf[50]=C_h_intern(&lf[50],10, C_text("terminated"));
lf[51]=C_h_intern(&lf[51],18, C_text("##sys#thread-kill!"));
lf[52]=C_h_intern(&lf[52],18, C_text("condition-variable"));
lf[53]=C_h_intern(&lf[53],6, C_text("thread"));
lf[54]=C_h_intern(&lf[54],31, C_text("##sys#default-exception-handler"));
lf[55]=C_h_intern(&lf[55],24, C_text("chicken.condition#signal"));
lf[56]=C_h_intern(&lf[56],28, C_text("##sys#show-exception-warning"));
lf[57]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011in thread"));
lf[58]=C_h_intern(&lf[58],27, C_text("##sys#thread-block-for-i/o!"));
lf[59]=C_h_intern(&lf[59],17, C_text("##sys#all-threads"));
lf[60]=C_h_intern(&lf[60],3, C_text("i/o"));
lf[61]=C_h_intern(&lf[61],7, C_text("timeout"));
lf[62]=C_h_intern(&lf[62],29, C_text("##sys#fetch-and-clear-threads"));
lf[63]=C_h_intern(&lf[63],21, C_text("##sys#restore-threads"));
lf[64]=C_h_intern(&lf[64],8, C_text("sleeping"));
lf[65]=C_h_intern(&lf[65],19, C_text("##sys#thread-sleep!"));
lf[66]=C_h_intern(&lf[66],23, C_text("chicken.base#sleep-hook"));
lf[67]=C_decode_literal(C_heaptop,C_text("\376U1000.0\000"));
lf[68]=C_h_intern(&lf[68],17, C_text("chicken.base#exit"));
lf[69]=C_h_intern(&lf[69],24, C_text("##sys#kill-other-threads"));
lf[70]=C_h_intern(&lf[70],9, C_text("suspended"));
C_register_lf2(lf,71,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_904,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_extras_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[133] = {
{C_text("f_1000:scheduler_2escm"),(void*)f_1000},
{C_text("f_1004:scheduler_2escm"),(void*)f_1004},
{C_text("f_1061:scheduler_2escm"),(void*)f_1061},
{C_text("f_1095:scheduler_2escm"),(void*)f_1095},
{C_text("f_1098:scheduler_2escm"),(void*)f_1098},
{C_text("f_1110:scheduler_2escm"),(void*)f_1110},
{C_text("f_1116:scheduler_2escm"),(void*)f_1116},
{C_text("f_1128:scheduler_2escm"),(void*)f_1128},
{C_text("f_1136:scheduler_2escm"),(void*)f_1136},
{C_text("f_1170:scheduler_2escm"),(void*)f_1170},
{C_text("f_1181:scheduler_2escm"),(void*)f_1181},
{C_text("f_1184:scheduler_2escm"),(void*)f_1184},
{C_text("f_1283:scheduler_2escm"),(void*)f_1283},
{C_text("f_1287:scheduler_2escm"),(void*)f_1287},
{C_text("f_1301:scheduler_2escm"),(void*)f_1301},
{C_text("f_1308:scheduler_2escm"),(void*)f_1308},
{C_text("f_1314:scheduler_2escm"),(void*)f_1314},
{C_text("f_1349:scheduler_2escm"),(void*)f_1349},
{C_text("f_1359:scheduler_2escm"),(void*)f_1359},
{C_text("f_1370:scheduler_2escm"),(void*)f_1370},
{C_text("f_1418:scheduler_2escm"),(void*)f_1418},
{C_text("f_1483:scheduler_2escm"),(void*)f_1483},
{C_text("f_1499:scheduler_2escm"),(void*)f_1499},
{C_text("f_1509:scheduler_2escm"),(void*)f_1509},
{C_text("f_1525:scheduler_2escm"),(void*)f_1525},
{C_text("f_1548:scheduler_2escm"),(void*)f_1548},
{C_text("f_1552:scheduler_2escm"),(void*)f_1552},
{C_text("f_1558:scheduler_2escm"),(void*)f_1558},
{C_text("f_1561:scheduler_2escm"),(void*)f_1561},
{C_text("f_1564:scheduler_2escm"),(void*)f_1564},
{C_text("f_1582:scheduler_2escm"),(void*)f_1582},
{C_text("f_1589:scheduler_2escm"),(void*)f_1589},
{C_text("f_1608:scheduler_2escm"),(void*)f_1608},
{C_text("f_1618:scheduler_2escm"),(void*)f_1618},
{C_text("f_1639:scheduler_2escm"),(void*)f_1639},
{C_text("f_1656:scheduler_2escm"),(void*)f_1656},
{C_text("f_1662:scheduler_2escm"),(void*)f_1662},
{C_text("f_1674:scheduler_2escm"),(void*)f_1674},
{C_text("f_1678:scheduler_2escm"),(void*)f_1678},
{C_text("f_1684:scheduler_2escm"),(void*)f_1684},
{C_text("f_1698:scheduler_2escm"),(void*)f_1698},
{C_text("f_1702:scheduler_2escm"),(void*)f_1702},
{C_text("f_1722:scheduler_2escm"),(void*)f_1722},
{C_text("f_1733:scheduler_2escm"),(void*)f_1733},
{C_text("f_1776:scheduler_2escm"),(void*)f_1776},
{C_text("f_1782:scheduler_2escm"),(void*)f_1782},
{C_text("f_1785:scheduler_2escm"),(void*)f_1785},
{C_text("f_1788:scheduler_2escm"),(void*)f_1788},
{C_text("f_1791:scheduler_2escm"),(void*)f_1791},
{C_text("f_1794:scheduler_2escm"),(void*)f_1794},
{C_text("f_1797:scheduler_2escm"),(void*)f_1797},
{C_text("f_1818:scheduler_2escm"),(void*)f_1818},
{C_text("f_1830:scheduler_2escm"),(void*)f_1830},
{C_text("f_1840:scheduler_2escm"),(void*)f_1840},
{C_text("f_1855:scheduler_2escm"),(void*)f_1855},
{C_text("f_1889:scheduler_2escm"),(void*)f_1889},
{C_text("f_1895:scheduler_2escm"),(void*)f_1895},
{C_text("f_1898:scheduler_2escm"),(void*)f_1898},
{C_text("f_1901:scheduler_2escm"),(void*)f_1901},
{C_text("f_1904:scheduler_2escm"),(void*)f_1904},
{C_text("f_1907:scheduler_2escm"),(void*)f_1907},
{C_text("f_1910:scheduler_2escm"),(void*)f_1910},
{C_text("f_1913:scheduler_2escm"),(void*)f_1913},
{C_text("f_1916:scheduler_2escm"),(void*)f_1916},
{C_text("f_1934:scheduler_2escm"),(void*)f_1934},
{C_text("f_1938:scheduler_2escm"),(void*)f_1938},
{C_text("f_1953:scheduler_2escm"),(void*)f_1953},
{C_text("f_2001:scheduler_2escm"),(void*)f_2001},
{C_text("f_2010:scheduler_2escm"),(void*)f_2010},
{C_text("f_2034:scheduler_2escm"),(void*)f_2034},
{C_text("f_2036:scheduler_2escm"),(void*)f_2036},
{C_text("f_2063:scheduler_2escm"),(void*)f_2063},
{C_text("f_2098:scheduler_2escm"),(void*)f_2098},
{C_text("f_2132:scheduler_2escm"),(void*)f_2132},
{C_text("f_2135:scheduler_2escm"),(void*)f_2135},
{C_text("f_2156:scheduler_2escm"),(void*)f_2156},
{C_text("f_2162:scheduler_2escm"),(void*)f_2162},
{C_text("f_2165:scheduler_2escm"),(void*)f_2165},
{C_text("f_2168:scheduler_2escm"),(void*)f_2168},
{C_text("f_2171:scheduler_2escm"),(void*)f_2171},
{C_text("f_2174:scheduler_2escm"),(void*)f_2174},
{C_text("f_2177:scheduler_2escm"),(void*)f_2177},
{C_text("f_2185:scheduler_2escm"),(void*)f_2185},
{C_text("f_2188:scheduler_2escm"),(void*)f_2188},
{C_text("f_2191:scheduler_2escm"),(void*)f_2191},
{C_text("f_2232:scheduler_2escm"),(void*)f_2232},
{C_text("f_2252:scheduler_2escm"),(void*)f_2252},
{C_text("f_2263:scheduler_2escm"),(void*)f_2263},
{C_text("f_2268:scheduler_2escm"),(void*)f_2268},
{C_text("f_2282:scheduler_2escm"),(void*)f_2282},
{C_text("f_2284:scheduler_2escm"),(void*)f_2284},
{C_text("f_2306:scheduler_2escm"),(void*)f_2306},
{C_text("f_2325:scheduler_2escm"),(void*)f_2325},
{C_text("f_2337:scheduler_2escm"),(void*)f_2337},
{C_text("f_2355:scheduler_2escm"),(void*)f_2355},
{C_text("f_2371:scheduler_2escm"),(void*)f_2371},
{C_text("f_2378:scheduler_2escm"),(void*)f_2378},
{C_text("f_2397:scheduler_2escm"),(void*)f_2397},
{C_text("f_2402:scheduler_2escm"),(void*)f_2402},
{C_text("f_2420:scheduler_2escm"),(void*)f_2420},
{C_text("f_2427:scheduler_2escm"),(void*)f_2427},
{C_text("f_2443:scheduler_2escm"),(void*)f_2443},
{C_text("f_2472:scheduler_2escm"),(void*)f_2472},
{C_text("f_2481:scheduler_2escm"),(void*)f_2481},
{C_text("f_2491:scheduler_2escm"),(void*)f_2491},
{C_text("f_2510:scheduler_2escm"),(void*)f_2510},
{C_text("f_2523:scheduler_2escm"),(void*)f_2523},
{C_text("f_2526:scheduler_2escm"),(void*)f_2526},
{C_text("f_2542:scheduler_2escm"),(void*)f_2542},
{C_text("f_2548:scheduler_2escm"),(void*)f_2548},
{C_text("f_2555:scheduler_2escm"),(void*)f_2555},
{C_text("f_2560:scheduler_2escm"),(void*)f_2560},
{C_text("f_2566:scheduler_2escm"),(void*)f_2566},
{C_text("f_2580:scheduler_2escm"),(void*)f_2580},
{C_text("f_2583:scheduler_2escm"),(void*)f_2583},
{C_text("f_2609:scheduler_2escm"),(void*)f_2609},
{C_text("f_2621:scheduler_2escm"),(void*)f_2621},
{C_text("f_2630:scheduler_2escm"),(void*)f_2630},
{C_text("f_2659:scheduler_2escm"),(void*)f_2659},
{C_text("f_2664:scheduler_2escm"),(void*)f_2664},
{C_text("f_2687:scheduler_2escm"),(void*)f_2687},
{C_text("f_904:scheduler_2escm"),(void*)f_904},
{C_text("f_907:scheduler_2escm"),(void*)f_907},
{C_text("f_909:scheduler_2escm"),(void*)f_909},
{C_text("f_915:scheduler_2escm"),(void*)f_915},
{C_text("f_942:scheduler_2escm"),(void*)f_942},
{C_text("f_952:scheduler_2escm"),(void*)f_952},
{C_text("f_983:scheduler_2escm"),(void*)f_983},
{C_text("f_988:scheduler_2escm"),(void*)f_988},
{C_text("f_992:scheduler_2escm"),(void*)f_992},
{C_text("f_995:scheduler_2escm"),(void*)f_995},
{C_text("toplevel:scheduler_2escm"),(void*)C_scheduler_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
S|applied compiler syntax:
S|  chicken.format#sprintf		3
S|  scheme#for-each		6
S|  ##sys#for-each		1
o|eliminated procedure checks: 136 
o|specializations:
o|  3 (chicken.base#add1 *)
o|  2 (chicken.base#sub1 *)
o|  1 (scheme#zero? *)
o|  3 (##sys#check-output-port * * *)
o|  10 (scheme#eqv? * (or eof null fixnum char boolean symbol keyword))
o|  3 (scheme#car pair)
o|  20 (scheme#cdr pair)
o|  4 (scheme#cdar (pair pair *))
(o e)|safe calls: 249 
(o e)|assignments to immediate values: 12 
o|inlining procedure: k917 
o|inlining procedure: k917 
o|inlining procedure: k1005 
o|inlining procedure: k1005 
o|contracted procedure: "(scheduler.scm:232) switch162" 
o|contracted procedure: "(scheduler.scm:170) ##sys#restore-thread-state-buffer" 
o|contracted procedure: "(scheduler.scm:227) remove-from-ready-queue" 
o|inlining procedure: k1206 
o|inlining procedure: k1206 
o|propagated global variable: first-pair222 ready-queue-head 
o|inlining procedure: k1045 
o|inlining procedure: k1045 
o|contracted procedure: "(scheduler.scm:224) ##sys#unblock-threads-for-i/o" 
o|inlining procedure: k2016 
o|inlining procedure: k2016 
o|inlining procedure: k2038 
o|inlining procedure: k2038 
o|inlining procedure: k2065 
o|inlining procedure: k2065 
o|inlining procedure: k2124 
o|inlining procedure: k2124 
o|substituted constant variable: a2158 
o|substituted constant variable: a2159 
o|inlining procedure: k2180 
o|inlining procedure: k2180 
o|contracted procedure: "(scheduler.scm:505) fdset-test" 
o|inlining procedure: k1859 
o|inlining procedure: k1859 
o|inlining procedure: k1877 
o|substituted constant variable: a1891 
o|substituted constant variable: a1892 
o|inlining procedure: k1877 
o|substituted constant variable: a1918 
o|substituted constant variable: a1923 
o|substituted constant variable: a1925 
o|substituted constant variable: a1930 
o|substituted constant variable: a1932 
o|contracted procedure: "(scheduler.scm:465) g505506" 
o|contracted procedure: "(scheduler.scm:455) create-fdset" 
o|inlining procedure: k1724 
o|inlining procedure: k1738 
o|inlining procedure: k1755 
o|inlining procedure: k1755 
o|substituted constant variable: a1778 
o|substituted constant variable: a1779 
o|substituted constant variable: a1799 
o|substituted constant variable: a1804 
o|substituted constant variable: a1806 
o|substituted constant variable: a1811 
o|substituted constant variable: a1813 
o|inlining procedure: k1738 
o|inlining procedure: k1724 
o|contracted procedure: "(scheduler.scm:402) g436437" 
o|inlining procedure: k1832 
o|inlining procedure: k1832 
o|contracted procedure: "(scheduler.scm:398) g379380" 
o|inlining procedure: k1063 
o|inlining procedure: k1063 
o|inlining procedure: k1084 
o|inlining procedure: k1084 
o|inlining procedure: k1118 
o|inlining procedure: k1118 
o|inlining procedure: k1140 
o|inlining procedure: k1140 
o|contracted procedure: "(scheduler.scm:180) ##sys#update-thread-state-buffer" 
o|propagated global variable: ct168 ##sys#current-thread 
o|inlining procedure: k1192 
o|inlining procedure: k1192 
o|inlining procedure: k1285 
o|propagated global variable: ct246 ##sys#current-thread 
o|inlining procedure: k1285 
o|inlining procedure: k1316 
o|inlining procedure: k1316 
o|inlining procedure: k1334 
o|inlining procedure: k1334 
o|inlining procedure: k1351 
o|inlining procedure: k1372 
o|inlining procedure: k1372 
o|inlining procedure: k1351 
o|inlining procedure: k1423 
o|inlining procedure: k1423 
o|inlining procedure: k1580 
o|inlining procedure: k1591 
o|inlining procedure: k1591 
o|inlining procedure: k1580 
o|inlining procedure: k1610 
o|inlining procedure: k1610 
o|inlining procedure: k1644 
o|inlining procedure: k1644 
o|contracted procedure: "(scheduler.scm:345) ##sys#abandon-mutexes" 
o|inlining procedure: k1461 
o|inlining procedure: k1461 
o|inlining procedure: k1527 
o|contracted procedure: "(scheduler.scm:328) g287294" 
o|inlining procedure: k1481 
o|inlining procedure: k1481 
o|inlining procedure: k1501 
o|contracted procedure: "(scheduler.scm:335) g301308" 
o|inlining procedure: k1501 
o|inlining procedure: k1527 
o|propagated global variable: pt370 ##sys#primordial-thread 
o|inlining procedure: k1955 
o|inlining procedure: k1955 
o|inlining procedure: k2270 
o|inlining procedure: k2286 
o|inlining procedure: k2286 
o|inlining procedure: k2307 
o|inlining procedure: k2307 
o|inlining procedure: k2270 
o|inlining procedure: k2357 
o|inlining procedure: k2357 
o|inlining procedure: k2380 
o|inlining procedure: k2404 
o|inlining procedure: k2404 
o|inlining procedure: k2380 
o|inlining procedure: k2429 
o|inlining procedure: k2429 
o|inlining procedure: k2512 
o|inlining procedure: k2512 
o|propagated global variable: ct635 ##sys#current-thread 
o|propagated global variable: primordial641 ##sys#current-thread 
o|inlining procedure: k2632 
o|inlining procedure: k2632 
o|inlining procedure: k2666 
o|inlining procedure: k2666 
o|propagated global variable: g662674 ##sys#fd-list 
o|inlining procedure: k2689 
o|inlining procedure: k2689 
o|propagated global variable: g652667 ##sys#timeout-list 
o|propagated global variable: primordial641 ##sys#current-thread 
o|replaced variables: 442 
o|removed binding forms: 179 
o|inlining procedure: k1216 
o|inlining procedure: k1216 
o|substituted constant variable: r12072714 
o|inlining procedure: k2241 
o|inlining procedure: k2241 
o|substituted constant variable: r11192745 
o|substituted constant variable: r11412748 
o|substituted constant variable: r22872810 
o|converted assignments to bindings: (suspend642) 
o|simplifications: ((let . 1)) 
o|replaced variables: 71 
o|removed binding forms: 372 
o|inlining procedure: k1014 
o|inlining procedure: k1821 
o|replaced variables: 8 
o|removed binding forms: 54 
o|substituted constant variable: r10152928 
o|contracted procedure: k961 
o|contracted procedure: k1715 
o|removed binding forms: 11 
o|removed conditional forms: 1 
o|removed binding forms: 2 
o|simplifications: ((if . 17) (let . 19) (##core#call . 242)) 
o|  call simplifications:
o|    scheme#*
o|    scheme#+
o|    ##sys#call-with-current-continuation
o|    scheme#vector
o|    scheme#list	2
o|    ##sys#structure?	2
o|    scheme#>
o|    scheme#<
o|    chicken.fixnum#fx=	2
o|    scheme#set-cdr!	3
o|    scheme#equal?
o|    scheme#>=
o|    ##sys#setislot	18
o|    scheme#caar	7
o|    scheme#-	2
o|    chicken.fixnum#fx>
o|    scheme#pair?	16
o|    scheme#car	12
o|    scheme#cdr	8
o|    scheme#not	6
o|    ##sys#setslot	30
o|    scheme#null?	26
o|    scheme#eq?	26
o|    scheme#cons	16
o|    ##sys#slot	57
o|contracted procedure: k920 
o|contracted procedure: k948 
o|contracted procedure: k926 
o|contracted procedure: k936 
o|contracted procedure: k944 
o|contracted procedure: k975 
o|contracted procedure: k1231 
o|propagated global variable: ct168 ##sys#current-thread 
o|contracted procedure: k1234 
o|contracted procedure: k1237 
o|contracted procedure: k1240 
o|contracted procedure: k1243 
o|contracted procedure: k1246 
o|contracted procedure: k978 
o|contracted procedure: k1008 
o|contracted procedure: k1020 
o|contracted procedure: k1014 
o|contracted procedure: k1039 
o|contracted procedure: k1029 
o|contracted procedure: k958 
o|contracted procedure: k1255 
o|contracted procedure: k1259 
o|contracted procedure: k1263 
o|contracted procedure: k1267 
o|contracted procedure: k1271 
o|contracted procedure: k1275 
o|contracted procedure: k1279 
o|contracted procedure: k965 
o|contracted procedure: k972 
o|contracted procedure: k1225 
o|contracted procedure: k1209 
o|contracted procedure: k1212 
o|propagated global variable: first-pair222 ready-queue-head 
o|contracted procedure: k1220 
o|contracted procedure: k1048 
o|contracted procedure: k2002 
o|contracted procedure: k2005 
o|contracted procedure: k2013 
o|contracted procedure: k2019 
o|contracted procedure: k2028 
o|contracted procedure: k2042 
o|contracted procedure: k2045 
o|contracted procedure: k2048 
o|contracted procedure: k2054 
o|contracted procedure: k2068 
o|contracted procedure: k2074 
o|contracted procedure: k2085 
o|contracted procedure: k2092 
o|contracted procedure: k2104 
o|contracted procedure: k2107 
o|contracted procedure: k2110 
o|contracted procedure: k2116 
o|contracted procedure: k2223 
o|contracted procedure: k2127 
o|contracted procedure: k2141 
o|contracted procedure: k2219 
o|contracted procedure: k2215 
o|contracted procedure: k2150 
o|contracted procedure: k2197 
o|contracted procedure: k2209 
o|contracted procedure: k1862 
o|contracted procedure: k1865 
o|contracted procedure: k1871 
o|contracted procedure: k1874 
o|contracted procedure: k1880 
o|contracted procedure: k2244 
o|contracted procedure: k2247 
o|contracted procedure: k2257 
o|contracted procedure: k1727 
o|contracted procedure: k1730 
o|contracted procedure: k1735 
o|contracted procedure: k1741 
o|contracted procedure: k1748 
o|contracted procedure: k1751 
o|contracted procedure: k1758 
o|contracted procedure: k1761 
o|contracted procedure: k1768 
o|contracted procedure: k1835 
o|contracted procedure: k1845 
o|contracted procedure: k1849 
o|contracted procedure: k1054 
o|contracted procedure: k1066 
o|contracted procedure: k1070 
o|contracted procedure: k1075 
o|contracted procedure: k1081 
o|contracted procedure: k1087 
o|contracted procedure: k1090 
o|contracted procedure: k1111 
o|contracted procedure: k1121 
o|contracted procedure: k1130 
o|contracted procedure: k1137 
o|contracted procedure: k1143 
o|contracted procedure: k1154 
o|contracted procedure: k1157 
o|contracted procedure: k1160 
o|propagated global variable: ct168 ##sys#current-thread 
o|propagated global variable: ct168 ##sys#current-thread 
o|contracted procedure: k1172 
o|contracted procedure: k1186 
o|contracted procedure: k1189 
o|contracted procedure: k1196 
o|contracted procedure: k1192 
o|contracted procedure: k1291 
o|contracted procedure: k1294 
o|contracted procedure: k1319 
o|contracted procedure: k1322 
o|contracted procedure: k1325 
o|contracted procedure: k1345 
o|contracted procedure: k1331 
o|contracted procedure: k1354 
o|contracted procedure: k1360 
o|contracted procedure: k1363 
o|contracted procedure: k1375 
o|contracted procedure: k1392 
o|contracted procedure: k1388 
o|contracted procedure: k1400 
o|contracted procedure: k1396 
o|contracted procedure: k1407 
o|contracted procedure: k1414 
o|contracted procedure: k1420 
o|contracted procedure: k1426 
o|contracted procedure: k1429 
o|contracted procedure: k1449 
o|contracted procedure: k1445 
o|contracted procedure: k1432 
o|contracted procedure: k1435 
o|contracted procedure: k1438 
o|contracted procedure: k1553 
o|contracted procedure: k1565 
o|contracted procedure: k1568 
o|contracted procedure: k1571 
o|contracted procedure: k1574 
o|contracted procedure: k1577 
o|contracted procedure: k1586 
o|contracted procedure: k1601 
o|contracted procedure: k1594 
o|contracted procedure: k1613 
o|contracted procedure: k1623 
o|contracted procedure: k1627 
o|contracted procedure: k1630 
o|contracted procedure: k1641 
o|contracted procedure: k1647 
o|contracted procedure: k1658 
o|contracted procedure: k1458 
o|contracted procedure: k1464 
o|contracted procedure: k1530 
o|contracted procedure: k1540 
o|contracted procedure: k1544 
o|contracted procedure: k1469 
o|contracted procedure: k1472 
o|contracted procedure: k1475 
o|contracted procedure: k1478 
o|contracted procedure: k1487 
o|contracted procedure: k1504 
o|contracted procedure: k1514 
o|contracted procedure: k1518 
o|contracted procedure: k1664 
o|contracted procedure: k1667 
o|contracted procedure: k1679 
o|contracted procedure: k1688 
o|contracted procedure: k1691 
o|propagated global variable: pt370 ##sys#primordial-thread 
o|propagated global variable: pt370 ##sys#primordial-thread 
o|contracted procedure: k1939 
o|contracted procedure: k1942 
o|contracted procedure: k1949 
o|contracted procedure: k1958 
o|contracted procedure: k1966 
o|contracted procedure: k1962 
o|contracted procedure: k1969 
o|contracted procedure: k1993 
o|contracted procedure: k1975 
o|contracted procedure: k1982 
o|contracted procedure: k2333 
o|contracted procedure: k2273 
o|contracted procedure: k2329 
o|contracted procedure: k2276 
o|contracted procedure: k2289 
o|contracted procedure: k2292 
o|contracted procedure: k2295 
o|contracted procedure: k2301 
o|contracted procedure: k2310 
o|contracted procedure: k2314 
o|contracted procedure: k2469 
o|contracted procedure: k2339 
o|contracted procedure: k2463 
o|contracted procedure: k2342 
o|contracted procedure: k2457 
o|contracted procedure: k2345 
o|contracted procedure: k2451 
o|contracted procedure: k2348 
o|contracted procedure: k2360 
o|contracted procedure: k2383 
o|contracted procedure: k2392 
o|contracted procedure: k2407 
o|contracted procedure: k2414 
o|contracted procedure: k2432 
o|contracted procedure: k2445 
o|contracted procedure: k2483 
o|contracted procedure: k2494 
o|contracted procedure: k2498 
o|contracted procedure: k2502 
o|contracted procedure: k2506 
o|contracted procedure: k2538 
o|contracted procedure: k2515 
o|contracted procedure: k2534 
o|contracted procedure: k2550 
o|propagated global variable: ct635 ##sys#current-thread 
o|contracted procedure: k2576 
o|contracted procedure: k2572 
o|contracted procedure: k2594 
o|contracted procedure: k2585 
o|contracted procedure: k2588 
o|contracted procedure: k2602 
o|contracted procedure: k2615 
o|contracted procedure: k2623 
o|contracted procedure: k2635 
o|contracted procedure: k2645 
o|contracted procedure: k2649 
o|contracted procedure: k2669 
o|contracted procedure: k2679 
o|contracted procedure: k2683 
o|contracted procedure: k2692 
o|contracted procedure: k2702 
o|contracted procedure: k2706 
o|propagated global variable: primordial641 ##sys#current-thread 
o|simplifications: ((if . 2) (let . 67)) 
o|removed binding forms: 218 
o|contracted procedure: k1378 
o|inlining procedure: k1533 
o|inlining procedure: k1533 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest582583 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest582583 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest582583 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest582583 0 
o|contracted procedure: k2518 
o|removed binding forms: 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2343 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r2343 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2343 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r2343 1 
o|removed binding forms: 1 
o|removed binding forms: 2 
o|direct leaf routine/allocation: loop252 0 
o|direct leaf routine/allocation: suspend642 0 
o|direct leaf routine/allocation: g646666 0 
o|converted assignments to bindings: (loop252) 
o|contracted procedure: "(scheduler.scm:623) k2606" 
o|contracted procedure: "(scheduler.scm:625) k2638" 
o|contracted procedure: "(scheduler.scm:624) k2695" 
o|simplifications: ((let . 1)) 
o|removed binding forms: 3 
o|direct leaf routine/allocation: for-each-loop678688 0 
o|direct leaf routine/allocation: for-each-loop645669 0 
o|contracted procedure: k2618 
o|converted assignments to bindings: (for-each-loop678688) 
o|converted assignments to bindings: (for-each-loop645669) 
o|simplifications: ((let . 2)) 
o|removed binding forms: 1 
o|direct leaf routine with hoistable closures/allocation: g656673 (for-each-loop678688) 0 
o|contracted procedure: "(scheduler.scm:625) k2672" 
o|removed binding forms: 2 
o|direct leaf routine/allocation: for-each-loop655692 0 
o|contracted procedure: k2652 
o|converted assignments to bindings: (for-each-loop655692) 
o|simplifications: ((let . 1)) 
o|removed binding forms: 1 
o|customizable procedures: (loop608 loop605 loop601 loop598 loop567 loop489 for-each-loop300311 for-each-loop286319 delq k1556 g335342 for-each-loop334345 loop261 k1108 ##sys#clear-i/o-state-for-thread! loop177 g393400 for-each-loop392432 loop385 k2183 loop2534 loop520 k1002 loop2193 loop1175 loop154) 
o|calls to known targets: 73 
o|identified direct recursive calls: f_915 1 
o|identified direct recursive calls: f_2063 1 
o|identified direct recursive calls: f_2036 1 
o|identified direct recursive calls: f_1061 1 
o|identified direct recursive calls: f_1314 1 
o|identified direct recursive calls: f_1370 1 
o|identified direct recursive calls: f_1525 1 
o|identified direct recursive calls: f_1953 1 
o|identified direct recursive calls: f_2284 1 
o|identified direct recursive calls: f_2402 1 
o|unused rest argument: rest582583 f_2337 
o|identified direct recursive calls: f_2687 1 
o|identified direct recursive calls: f_2630 1 
o|identified direct recursive calls: f_2664 1 
o|fast box initializations: 18 
o|fast global references: 31 
o|fast global assignments: 23 
o|dropping unused closure argument: f_2268 
o|dropping unused closure argument: f_909 
*/
/* end of file */
