/* Generated from support.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: support.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -no-lambda-info -no-trace -emit-import-library chicken.compiler.support -output-file support.c
   unit: support
   uses: library eval expand data-structures extras file internal pathname port
*/
#include "chicken.h"

static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_eval_toplevel)
C_externimport void C_ccall C_eval_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_expand_toplevel)
C_externimport void C_ccall C_expand_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_data_2dstructures_toplevel)
C_externimport void C_ccall C_data_2dstructures_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_extras_toplevel)
C_externimport void C_ccall C_extras_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_file_toplevel)
C_externimport void C_ccall C_file_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_internal_toplevel)
C_externimport void C_ccall C_internal_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_pathname_toplevel)
C_externimport void C_ccall C_pathname_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_port_toplevel)
C_externimport void C_ccall C_port_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[539];
static double C_possibly_force_alignment;


#define return(x) C_cblock C_r = (C_fix((C_word)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub4226(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_word lit=(C_word )(C_a0);
return(C_header_size(lit));
C_ret:
#undef return

return C_r;}

/* from k7269 */
C_regparm static C_word C_fcall stub913(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
C_r=C_fix((C_word)C_wordstobytes(t0));
return C_r;}

/* from k7262 */
C_regparm static C_word C_fcall stub908(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
C_r=C_fix((C_word)C_bytestowords(t0));
return C_r;}

C_noret_decl(f19580)
static void C_ccall f19580(C_word c,C_word *av) C_noret;
C_noret_decl(f_10030)
static void C_ccall f_10030(C_word c,C_word *av) C_noret;
C_noret_decl(f_10034)
static void C_ccall f_10034(C_word c,C_word *av) C_noret;
C_noret_decl(f_10044)
static void C_fcall f_10044(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10068)
static void C_ccall f_10068(C_word c,C_word *av) C_noret;
C_noret_decl(f_10083)
static void C_ccall f_10083(C_word c,C_word *av) C_noret;
C_noret_decl(f_10095)
static void C_ccall f_10095(C_word c,C_word *av) C_noret;
C_noret_decl(f_10120)
static void C_fcall f_10120(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10145)
static void C_ccall f_10145(C_word c,C_word *av) C_noret;
C_noret_decl(f_10175)
static void C_ccall f_10175(C_word c,C_word *av) C_noret;
C_noret_decl(f_10177)
static void C_fcall f_10177(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10202)
static void C_ccall f_10202(C_word c,C_word *av) C_noret;
C_noret_decl(f_10233)
static void C_fcall f_10233(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_10245)
static void C_ccall f_10245(C_word c,C_word *av) C_noret;
C_noret_decl(f_10249)
static void C_ccall f_10249(C_word c,C_word *av) C_noret;
C_noret_decl(f_10272)
static void C_ccall f_10272(C_word c,C_word *av) C_noret;
C_noret_decl(f_10283)
static void C_fcall f_10283(C_word t0,C_word t1) C_noret;
C_noret_decl(f_10290)
static void C_ccall f_10290(C_word c,C_word *av) C_noret;
C_noret_decl(f_10301)
static void C_ccall f_10301(C_word c,C_word *av) C_noret;
C_noret_decl(f_10303)
static void C_fcall f_10303(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10328)
static void C_ccall f_10328(C_word c,C_word *av) C_noret;
C_noret_decl(f_10358)
static void C_ccall f_10358(C_word c,C_word *av) C_noret;
C_noret_decl(f_10360)
static void C_fcall f_10360(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10385)
static void C_ccall f_10385(C_word c,C_word *av) C_noret;
C_noret_decl(f_10399)
static void C_ccall f_10399(C_word c,C_word *av) C_noret;
C_noret_decl(f_10409)
static void C_ccall f_10409(C_word c,C_word *av) C_noret;
C_noret_decl(f_10411)
static void C_fcall f_10411(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10436)
static void C_ccall f_10436(C_word c,C_word *av) C_noret;
C_noret_decl(f_10509)
static void C_ccall f_10509(C_word c,C_word *av) C_noret;
C_noret_decl(f_10515)
static void C_fcall f_10515(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10541)
static void C_ccall f_10541(C_word c,C_word *av) C_noret;
C_noret_decl(f_10545)
static void C_ccall f_10545(C_word c,C_word *av) C_noret;
C_noret_decl(f_10561)
static void C_ccall f_10561(C_word c,C_word *av) C_noret;
C_noret_decl(f_10567)
static void C_ccall f_10567(C_word c,C_word *av) C_noret;
C_noret_decl(f_10573)
static void C_ccall f_10573(C_word c,C_word *av) C_noret;
C_noret_decl(f_10579)
static void C_ccall f_10579(C_word c,C_word *av) C_noret;
C_noret_decl(f_10583)
static void C_ccall f_10583(C_word c,C_word *av) C_noret;
C_noret_decl(f_10586)
static void C_ccall f_10586(C_word c,C_word *av) C_noret;
C_noret_decl(f_10600)
static void C_ccall f_10600(C_word c,C_word *av) C_noret;
C_noret_decl(f_10603)
static void C_ccall f_10603(C_word c,C_word *av) C_noret;
C_noret_decl(f_10606)
static void C_ccall f_10606(C_word c,C_word *av) C_noret;
C_noret_decl(f_10613)
static void C_ccall f_10613(C_word c,C_word *av) C_noret;
C_noret_decl(f_10615)
static void C_fcall f_10615(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10630)
static void C_fcall f_10630(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10663)
static void C_ccall f_10663(C_word c,C_word *av) C_noret;
C_noret_decl(f_10691)
static void C_ccall f_10691(C_word c,C_word *av) C_noret;
C_noret_decl(f_10695)
static void C_ccall f_10695(C_word c,C_word *av) C_noret;
C_noret_decl(f_10719)
static void C_ccall f_10719(C_word c,C_word *av) C_noret;
C_noret_decl(f_10751)
static void C_ccall f_10751(C_word c,C_word *av) C_noret;
C_noret_decl(f_10760)
static void C_fcall f_10760(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10764)
static void C_ccall f_10764(C_word c,C_word *av) C_noret;
C_noret_decl(f_10770)
static void C_ccall f_10770(C_word c,C_word *av) C_noret;
C_noret_decl(f_10776)
static void C_ccall f_10776(C_word c,C_word *av) C_noret;
C_noret_decl(f_10779)
static void C_ccall f_10779(C_word c,C_word *av) C_noret;
C_noret_decl(f_10784)
static void C_fcall f_10784(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10794)
static void C_ccall f_10794(C_word c,C_word *av) C_noret;
C_noret_decl(f_10809)
static void C_ccall f_10809(C_word c,C_word *av) C_noret;
C_noret_decl(f_10811)
static void C_fcall f_10811(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10836)
static void C_ccall f_10836(C_word c,C_word *av) C_noret;
C_noret_decl(f_10857)
static void C_fcall f_10857(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10882)
static void C_ccall f_10882(C_word c,C_word *av) C_noret;
C_noret_decl(f_10904)
static void C_ccall f_10904(C_word c,C_word *av) C_noret;
C_noret_decl(f_10912)
static void C_fcall f_10912(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10963)
static void C_ccall f_10963(C_word c,C_word *av) C_noret;
C_noret_decl(f_10970)
static void C_ccall f_10970(C_word c,C_word *av) C_noret;
C_noret_decl(f_10973)
static void C_ccall f_10973(C_word c,C_word *av) C_noret;
C_noret_decl(f_11002)
static void C_ccall f_11002(C_word c,C_word *av) C_noret;
C_noret_decl(f_11010)
static void C_ccall f_11010(C_word c,C_word *av) C_noret;
C_noret_decl(f_11026)
static void C_ccall f_11026(C_word c,C_word *av) C_noret;
C_noret_decl(f_11029)
static void C_ccall f_11029(C_word c,C_word *av) C_noret;
C_noret_decl(f_11035)
static void C_ccall f_11035(C_word c,C_word *av) C_noret;
C_noret_decl(f_11055)
static void C_ccall f_11055(C_word c,C_word *av) C_noret;
C_noret_decl(f_11078)
static void C_ccall f_11078(C_word c,C_word *av) C_noret;
C_noret_decl(f_11083)
static void C_fcall f_11083(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11087)
static void C_ccall f_11087(C_word c,C_word *av) C_noret;
C_noret_decl(f_11090)
static void C_ccall f_11090(C_word c,C_word *av) C_noret;
C_noret_decl(f_11096)
static void C_ccall f_11096(C_word c,C_word *av) C_noret;
C_noret_decl(f_11099)
static void C_ccall f_11099(C_word c,C_word *av) C_noret;
C_noret_decl(f_11116)
static void C_fcall f_11116(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11126)
static void C_ccall f_11126(C_word c,C_word *av) C_noret;
C_noret_decl(f_11128)
static void C_fcall f_11128(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11153)
static void C_ccall f_11153(C_word c,C_word *av) C_noret;
C_noret_decl(f_11164)
static void C_ccall f_11164(C_word c,C_word *av) C_noret;
C_noret_decl(f_11172)
static void C_ccall f_11172(C_word c,C_word *av) C_noret;
C_noret_decl(f_11180)
static void C_ccall f_11180(C_word c,C_word *av) C_noret;
C_noret_decl(f_11193)
static void C_ccall f_11193(C_word c,C_word *av) C_noret;
C_noret_decl(f_11195)
static void C_fcall f_11195(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_11243)
static void C_fcall f_11243(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11268)
static void C_ccall f_11268(C_word c,C_word *av) C_noret;
C_noret_decl(f_11287)
static void C_ccall f_11287(C_word c,C_word *av) C_noret;
C_noret_decl(f_11292)
static void C_fcall f_11292(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11302)
static void C_ccall f_11302(C_word c,C_word *av) C_noret;
C_noret_decl(f_11304)
static void C_fcall f_11304(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11329)
static void C_ccall f_11329(C_word c,C_word *av) C_noret;
C_noret_decl(f_11351)
static void C_fcall f_11351(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_11402)
static void C_fcall f_11402(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11446)
static void C_ccall f_11446(C_word c,C_word *av) C_noret;
C_noret_decl(f_11493)
static void C_ccall f_11493(C_word c,C_word *av) C_noret;
C_noret_decl(f_11521)
static void C_ccall f_11521(C_word c,C_word *av) C_noret;
C_noret_decl(f_11525)
static void C_ccall f_11525(C_word c,C_word *av) C_noret;
C_noret_decl(f_11529)
static void C_ccall f_11529(C_word c,C_word *av) C_noret;
C_noret_decl(f_11548)
static void C_ccall f_11548(C_word c,C_word *av) C_noret;
C_noret_decl(f_11560)
static void C_ccall f_11560(C_word c,C_word *av) C_noret;
C_noret_decl(f_11562)
static void C_fcall f_11562(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_11604)
static void C_fcall f_11604(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11614)
static void C_ccall f_11614(C_word c,C_word *av) C_noret;
C_noret_decl(f_11636)
static void C_ccall f_11636(C_word c,C_word *av) C_noret;
C_noret_decl(f_11642)
static void C_fcall f_11642(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11656)
static void C_ccall f_11656(C_word c,C_word *av) C_noret;
C_noret_decl(f_11660)
static void C_ccall f_11660(C_word c,C_word *av) C_noret;
C_noret_decl(f_11666)
static void C_ccall f_11666(C_word c,C_word *av) C_noret;
C_noret_decl(f_11704)
static void C_ccall f_11704(C_word c,C_word *av) C_noret;
C_noret_decl(f_11708)
static void C_ccall f_11708(C_word c,C_word *av) C_noret;
C_noret_decl(f_11711)
static void C_ccall f_11711(C_word c,C_word *av) C_noret;
C_noret_decl(f_11714)
static void C_ccall f_11714(C_word c,C_word *av) C_noret;
C_noret_decl(f_11749)
static void C_fcall f_11749(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11793)
static void C_ccall f_11793(C_word c,C_word *av) C_noret;
C_noret_decl(f_11795)
static void C_fcall f_11795(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11820)
static void C_ccall f_11820(C_word c,C_word *av) C_noret;
C_noret_decl(f_11835)
static void C_fcall f_11835(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11864)
static void C_ccall f_11864(C_word c,C_word *av) C_noret;
C_noret_decl(f_11866)
static void C_fcall f_11866(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11891)
static void C_ccall f_11891(C_word c,C_word *av) C_noret;
C_noret_decl(f_11900)
static void C_ccall f_11900(C_word c,C_word *av) C_noret;
C_noret_decl(f_11903)
static void C_fcall f_11903(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11909)
static void C_ccall f_11909(C_word c,C_word *av) C_noret;
C_noret_decl(f_11957)
static void C_ccall f_11957(C_word c,C_word *av) C_noret;
C_noret_decl(f_11960)
static void C_ccall f_11960(C_word c,C_word *av) C_noret;
C_noret_decl(f_11966)
static void C_ccall f_11966(C_word c,C_word *av) C_noret;
C_noret_decl(f_11974)
static void C_ccall f_11974(C_word c,C_word *av) C_noret;
C_noret_decl(f_11982)
static void C_fcall f_11982(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11992)
static void C_ccall f_11992(C_word c,C_word *av) C_noret;
C_noret_decl(f_12020)
static void C_ccall f_12020(C_word c,C_word *av) C_noret;
C_noret_decl(f_12024)
static void C_ccall f_12024(C_word c,C_word *av) C_noret;
C_noret_decl(f_12029)
static void C_ccall f_12029(C_word c,C_word *av) C_noret;
C_noret_decl(f_12035)
static void C_ccall f_12035(C_word c,C_word *av) C_noret;
C_noret_decl(f_12038)
static void C_ccall f_12038(C_word c,C_word *av) C_noret;
C_noret_decl(f_12043)
static void C_fcall f_12043(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_12053)
static void C_ccall f_12053(C_word c,C_word *av) C_noret;
C_noret_decl(f_12068)
static void C_ccall f_12068(C_word c,C_word *av) C_noret;
C_noret_decl(f_12070)
static void C_ccall f_12070(C_word c,C_word *av) C_noret;
C_noret_decl(f_12077)
static void C_ccall f_12077(C_word c,C_word *av) C_noret;
C_noret_decl(f_12098)
static void C_fcall f_12098(C_word t0,C_word t1) C_noret;
C_noret_decl(f_12132)
static void C_ccall f_12132(C_word c,C_word *av) C_noret;
C_noret_decl(f_12135)
static void C_fcall f_12135(C_word t0,C_word t1) C_noret;
C_noret_decl(f_12160)
static void C_ccall f_12160(C_word c,C_word *av) C_noret;
C_noret_decl(f_12166)
static void C_ccall f_12166(C_word c,C_word *av) C_noret;
C_noret_decl(f_12192)
static void C_ccall f_12192(C_word c,C_word *av) C_noret;
C_noret_decl(f_12225)
static void C_ccall f_12225(C_word c,C_word *av) C_noret;
C_noret_decl(f_12227)
static void C_ccall f_12227(C_word c,C_word *av) C_noret;
C_noret_decl(f_12233)
static void C_ccall f_12233(C_word c,C_word *av) C_noret;
C_noret_decl(f_12239)
static void C_fcall f_12239(C_word t0,C_word t1) C_noret;
C_noret_decl(f_12243)
static void C_ccall f_12243(C_word c,C_word *av) C_noret;
C_noret_decl(f_12266)
static void C_ccall f_12266(C_word c,C_word *av) C_noret;
C_noret_decl(f_12277)
static void C_ccall f_12277(C_word c,C_word *av) C_noret;
C_noret_decl(f_12283)
static void C_ccall f_12283(C_word c,C_word *av) C_noret;
C_noret_decl(f_12286)
static void C_fcall f_12286(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_12294)
static C_word C_fcall f_12294(C_word t0,C_word t1);
C_noret_decl(f_12320)
static void C_fcall f_12320(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_12342)
static void C_ccall f_12342(C_word c,C_word *av) C_noret;
C_noret_decl(f_12367)
static void C_fcall f_12367(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_12389)
static void C_ccall f_12389(C_word c,C_word *av) C_noret;
C_noret_decl(f_12407)
static void C_fcall f_12407(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_12438)
static void C_ccall f_12438(C_word c,C_word *av) C_noret;
C_noret_decl(f_12490)
static void C_ccall f_12490(C_word c,C_word *av) C_noret;
C_noret_decl(f_12496)
static void C_ccall f_12496(C_word c,C_word *av) C_noret;
C_noret_decl(f_12516)
static void C_ccall f_12516(C_word c,C_word *av) C_noret;
C_noret_decl(f_12522)
static void C_ccall f_12522(C_word c,C_word *av) C_noret;
C_noret_decl(f_12548)
static void C_fcall f_12548(C_word t0,C_word t1) C_noret;
C_noret_decl(f_12562)
static void C_fcall f_12562(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_12570)
static void C_ccall f_12570(C_word c,C_word *av) C_noret;
C_noret_decl(f_12625)
static void C_ccall f_12625(C_word c,C_word *av) C_noret;
C_noret_decl(f_12654)
static void C_ccall f_12654(C_word c,C_word *av) C_noret;
C_noret_decl(f_12751)
static void C_ccall f_12751(C_word c,C_word *av) C_noret;
C_noret_decl(f_12757)
static void C_ccall f_12757(C_word c,C_word *av) C_noret;
C_noret_decl(f_12764)
static void C_fcall f_12764(C_word t0,C_word t1) C_noret;
C_noret_decl(f_12767)
static void C_ccall f_12767(C_word c,C_word *av) C_noret;
C_noret_decl(f_12790)
static void C_ccall f_12790(C_word c,C_word *av) C_noret;
C_noret_decl(f_12792)
static void C_ccall f_12792(C_word c,C_word *av) C_noret;
C_noret_decl(f_12798)
static void C_ccall f_12798(C_word c,C_word *av) C_noret;
C_noret_decl(f_12805)
static void C_fcall f_12805(C_word t0,C_word t1) C_noret;
C_noret_decl(f_12808)
static void C_ccall f_12808(C_word c,C_word *av) C_noret;
C_noret_decl(f_12827)
static void C_ccall f_12827(C_word c,C_word *av) C_noret;
C_noret_decl(f_12829)
static void C_ccall f_12829(C_word c,C_word *av) C_noret;
C_noret_decl(f_12835)
static void C_ccall f_12835(C_word c,C_word *av) C_noret;
C_noret_decl(f_12848)
static void C_ccall f_12848(C_word c,C_word *av) C_noret;
C_noret_decl(f_12876)
static void C_ccall f_12876(C_word c,C_word *av) C_noret;
C_noret_decl(f_12878)
static void C_ccall f_12878(C_word c,C_word *av) C_noret;
C_noret_decl(f_12899)
static void C_ccall f_12899(C_word c,C_word *av) C_noret;
C_noret_decl(f_12906)
static void C_ccall f_12906(C_word c,C_word *av) C_noret;
C_noret_decl(f_12912)
static void C_ccall f_12912(C_word c,C_word *av) C_noret;
C_noret_decl(f_12918)
static void C_ccall f_12918(C_word c,C_word *av) C_noret;
C_noret_decl(f_12927)
static void C_ccall f_12927(C_word c,C_word *av) C_noret;
C_noret_decl(f_12936)
static void C_ccall f_12936(C_word c,C_word *av) C_noret;
C_noret_decl(f_12945)
static void C_ccall f_12945(C_word c,C_word *av) C_noret;
C_noret_decl(f_12954)
static void C_ccall f_12954(C_word c,C_word *av) C_noret;
C_noret_decl(f_12963)
static void C_ccall f_12963(C_word c,C_word *av) C_noret;
C_noret_decl(f_12989)
static void C_ccall f_12989(C_word c,C_word *av) C_noret;
C_noret_decl(f_12992)
static void C_ccall f_12992(C_word c,C_word *av) C_noret;
C_noret_decl(f_13003)
static void C_ccall f_13003(C_word c,C_word *av) C_noret;
C_noret_decl(f_13005)
static void C_ccall f_13005(C_word c,C_word *av) C_noret;
C_noret_decl(f_13059)
static void C_ccall f_13059(C_word c,C_word *av) C_noret;
C_noret_decl(f_13065)
static void C_ccall f_13065(C_word c,C_word *av) C_noret;
C_noret_decl(f_13071)
static void C_ccall f_13071(C_word c,C_word *av) C_noret;
C_noret_decl(f_13077)
static void C_fcall f_13077(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_13102)
static void C_fcall f_13102(C_word t0,C_word t1) C_noret;
C_noret_decl(f_13117)
static void C_fcall f_13117(C_word t0,C_word t1) C_noret;
C_noret_decl(f_13135)
static void C_ccall f_13135(C_word c,C_word *av) C_noret;
C_noret_decl(f_13185)
static void C_ccall f_13185(C_word c,C_word *av) C_noret;
C_noret_decl(f_13200)
static void C_fcall f_13200(C_word t0,C_word t1) C_noret;
C_noret_decl(f_13240)
static void C_fcall f_13240(C_word t0,C_word t1) C_noret;
C_noret_decl(f_13243)
static void C_ccall f_13243(C_word c,C_word *av) C_noret;
C_noret_decl(f_13258)
static void C_fcall f_13258(C_word t0,C_word t1) C_noret;
C_noret_decl(f_13282)
static void C_fcall f_13282(C_word t0,C_word t1) C_noret;
C_noret_decl(f_13308)
static void C_fcall f_13308(C_word t0,C_word t1) C_noret;
C_noret_decl(f_13314)
static void C_ccall f_13314(C_word c,C_word *av) C_noret;
C_noret_decl(f_13320)
static void C_ccall f_13320(C_word c,C_word *av) C_noret;
C_noret_decl(f_13323)
static void C_ccall f_13323(C_word c,C_word *av) C_noret;
C_noret_decl(f_13326)
static void C_ccall f_13326(C_word c,C_word *av) C_noret;
C_noret_decl(f_13329)
static void C_ccall f_13329(C_word c,C_word *av) C_noret;
C_noret_decl(f_13351)
static void C_fcall f_13351(C_word t0,C_word t1) C_noret;
C_noret_decl(f_13357)
static void C_ccall f_13357(C_word c,C_word *av) C_noret;
C_noret_decl(f_13363)
static void C_ccall f_13363(C_word c,C_word *av) C_noret;
C_noret_decl(f_13366)
static void C_ccall f_13366(C_word c,C_word *av) C_noret;
C_noret_decl(f_13369)
static void C_ccall f_13369(C_word c,C_word *av) C_noret;
C_noret_decl(f_13372)
static void C_ccall f_13372(C_word c,C_word *av) C_noret;
C_noret_decl(f_13395)
static void C_fcall f_13395(C_word t0,C_word t1) C_noret;
C_noret_decl(f_13398)
static void C_ccall f_13398(C_word c,C_word *av) C_noret;
C_noret_decl(f_13439)
static void C_fcall f_13439(C_word t0,C_word t1) C_noret;
C_noret_decl(f_13442)
static void C_ccall f_13442(C_word c,C_word *av) C_noret;
C_noret_decl(f_13457)
static void C_fcall f_13457(C_word t0,C_word t1) C_noret;
C_noret_decl(f_13484)
static void C_fcall f_13484(C_word t0,C_word t1) C_noret;
C_noret_decl(f_13527)
static void C_ccall f_13527(C_word c,C_word *av) C_noret;
C_noret_decl(f_13531)
static void C_fcall f_13531(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_13558)
static void C_fcall f_13558(C_word t0,C_word t1) C_noret;
C_noret_decl(f_13561)
static void C_ccall f_13561(C_word c,C_word *av) C_noret;
C_noret_decl(f_13596)
static void C_ccall f_13596(C_word c,C_word *av) C_noret;
C_noret_decl(f_13632)
static void C_ccall f_13632(C_word c,C_word *av) C_noret;
C_noret_decl(f_14135)
static void C_ccall f_14135(C_word c,C_word *av) C_noret;
C_noret_decl(f_14141)
static void C_fcall f_14141(C_word t0,C_word t1) C_noret;
C_noret_decl(f_14151)
static void C_ccall f_14151(C_word c,C_word *av) C_noret;
C_noret_decl(f_14162)
static void C_fcall f_14162(C_word t0,C_word t1) C_noret;
C_noret_decl(f_14172)
static void C_ccall f_14172(C_word c,C_word *av) C_noret;
C_noret_decl(f_14183)
static void C_ccall f_14183(C_word c,C_word *av) C_noret;
C_noret_decl(f_14187)
static void C_ccall f_14187(C_word c,C_word *av) C_noret;
C_noret_decl(f_14198)
static void C_ccall f_14198(C_word c,C_word *av) C_noret;
C_noret_decl(f_14202)
static void C_ccall f_14202(C_word c,C_word *av) C_noret;
C_noret_decl(f_14213)
static void C_ccall f_14213(C_word c,C_word *av) C_noret;
C_noret_decl(f_14219)
static void C_ccall f_14219(C_word c,C_word *av) C_noret;
C_noret_decl(f_14223)
static void C_ccall f_14223(C_word c,C_word *av) C_noret;
C_noret_decl(f_14227)
static void C_fcall f_14227(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_14246)
static void C_ccall f_14246(C_word c,C_word *av) C_noret;
C_noret_decl(f_14252)
static void C_ccall f_14252(C_word c,C_word *av) C_noret;
C_noret_decl(f_14255)
static void C_fcall f_14255(C_word t0,C_word t1) C_noret;
C_noret_decl(f_14264)
static void C_ccall f_14264(C_word c,C_word *av) C_noret;
C_noret_decl(f_14274)
static void C_fcall f_14274(C_word t0,C_word t1) C_noret;
C_noret_decl(f_14283)
static void C_fcall f_14283(C_word t0,C_word t1) C_noret;
C_noret_decl(f_14295)
static void C_fcall f_14295(C_word t0,C_word t1) C_noret;
C_noret_decl(f_14307)
static void C_fcall f_14307(C_word t0,C_word t1) C_noret;
C_noret_decl(f_14319)
static void C_fcall f_14319(C_word t0,C_word t1) C_noret;
C_noret_decl(f_14325)
static void C_ccall f_14325(C_word c,C_word *av) C_noret;
C_noret_decl(f_14329)
static void C_fcall f_14329(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_14356)
static void C_fcall f_14356(C_word t0,C_word t1) C_noret;
C_noret_decl(f_14721)
static void C_ccall f_14721(C_word c,C_word *av) C_noret;
C_noret_decl(f_14727)
static void C_ccall f_14727(C_word c,C_word *av) C_noret;
C_noret_decl(f_14739)
static void C_ccall f_14739(C_word c,C_word *av) C_noret;
C_noret_decl(f_14749)
static void C_fcall f_14749(C_word t0,C_word t1) C_noret;
C_noret_decl(f_14761)
static void C_fcall f_14761(C_word t0,C_word t1) C_noret;
C_noret_decl(f_14767)
static void C_ccall f_14767(C_word c,C_word *av) C_noret;
C_noret_decl(f_14771)
static void C_fcall f_14771(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_14798)
static void C_fcall f_14798(C_word t0,C_word t1) C_noret;
C_noret_decl(f_15171)
static void C_ccall f_15171(C_word c,C_word *av) C_noret;
C_noret_decl(f_15177)
static void C_ccall f_15177(C_word c,C_word *av) C_noret;
C_noret_decl(f_15181)
static void C_ccall f_15181(C_word c,C_word *av) C_noret;
C_noret_decl(f_15297)
static void C_fcall f_15297(C_word t0,C_word t1) C_noret;
C_noret_decl(f_15325)
static void C_ccall f_15325(C_word c,C_word *av) C_noret;
C_noret_decl(f_15445)
static void C_ccall f_15445(C_word c,C_word *av) C_noret;
C_noret_decl(f_15452)
static void C_ccall f_15452(C_word c,C_word *av) C_noret;
C_noret_decl(f_15455)
static void C_ccall f_15455(C_word c,C_word *av) C_noret;
C_noret_decl(f_15458)
static void C_ccall f_15458(C_word c,C_word *av) C_noret;
C_noret_decl(f_15482)
static void C_fcall f_15482(C_word t0,C_word t1) C_noret;
C_noret_decl(f_15557)
static void C_fcall f_15557(C_word t0,C_word t1) C_noret;
C_noret_decl(f_15644)
static void C_fcall f_15644(C_word t0,C_word t1) C_noret;
C_noret_decl(f_15665)
static void C_fcall f_15665(C_word t0,C_word t1) C_noret;
C_noret_decl(f_15683)
static void C_fcall f_15683(C_word t0,C_word t1) C_noret;
C_noret_decl(f_15705)
static void C_fcall f_15705(C_word t0,C_word t1) C_noret;
C_noret_decl(f_16075)
static void C_ccall f_16075(C_word c,C_word *av) C_noret;
C_noret_decl(f_16079)
static void C_ccall f_16079(C_word c,C_word *av) C_noret;
C_noret_decl(f_16081)
static void C_fcall f_16081(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_16113)
static void C_fcall f_16113(C_word t0,C_word t1) C_noret;
C_noret_decl(f_16121)
static void C_fcall f_16121(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_16131)
static void C_ccall f_16131(C_word c,C_word *av) C_noret;
C_noret_decl(f_16177)
static void C_fcall f_16177(C_word t0,C_word t1) C_noret;
C_noret_decl(f_16185)
static void C_fcall f_16185(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_16195)
static void C_ccall f_16195(C_word c,C_word *av) C_noret;
C_noret_decl(f_16230)
static void C_ccall f_16230(C_word c,C_word *av) C_noret;
C_noret_decl(f_16233)
static void C_fcall f_16233(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_16267)
static void C_fcall f_16267(C_word t0,C_word t1) C_noret;
C_noret_decl(f_16286)
static void C_ccall f_16286(C_word c,C_word *av) C_noret;
C_noret_decl(f_16292)
static void C_ccall f_16292(C_word c,C_word *av) C_noret;
C_noret_decl(f_16296)
static void C_ccall f_16296(C_word c,C_word *av) C_noret;
C_noret_decl(f_16322)
static void C_ccall f_16322(C_word c,C_word *av) C_noret;
C_noret_decl(f_16331)
static void C_ccall f_16331(C_word c,C_word *av) C_noret;
C_noret_decl(f_16342)
static void C_ccall f_16342(C_word c,C_word *av) C_noret;
C_noret_decl(f_16361)
static void C_ccall f_16361(C_word c,C_word *av) C_noret;
C_noret_decl(f_16373)
static void C_ccall f_16373(C_word c,C_word *av) C_noret;
C_noret_decl(f_16417)
static void C_fcall f_16417(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_16419)
static void C_fcall f_16419(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_16431)
static void C_fcall f_16431(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_16441)
static void C_ccall f_16441(C_word c,C_word *av) C_noret;
C_noret_decl(f_16455)
static void C_ccall f_16455(C_word c,C_word *av) C_noret;
C_noret_decl(f_16460)
static void C_ccall f_16460(C_word c,C_word *av) C_noret;
C_noret_decl(f_16484)
static void C_ccall f_16484(C_word c,C_word *av) C_noret;
C_noret_decl(f_16490)
static void C_ccall f_16490(C_word c,C_word *av) C_noret;
C_noret_decl(f_16496)
static void C_ccall f_16496(C_word c,C_word *av) C_noret;
C_noret_decl(f_16505)
static void C_ccall f_16505(C_word c,C_word *av) C_noret;
C_noret_decl(f_16513)
static void C_ccall f_16513(C_word c,C_word *av) C_noret;
C_noret_decl(f_16519)
static void C_ccall f_16519(C_word c,C_word *av) C_noret;
C_noret_decl(f_16522)
static void C_ccall f_16522(C_word c,C_word *av) C_noret;
C_noret_decl(f_16525)
static void C_ccall f_16525(C_word c,C_word *av) C_noret;
C_noret_decl(f_16528)
static void C_ccall f_16528(C_word c,C_word *av) C_noret;
C_noret_decl(f_16531)
static void C_ccall f_16531(C_word c,C_word *av) C_noret;
C_noret_decl(f_16536)
static void C_ccall f_16536(C_word c,C_word *av) C_noret;
C_noret_decl(f_16540)
static void C_ccall f_16540(C_word c,C_word *av) C_noret;
C_noret_decl(f_16552)
static void C_ccall f_16552(C_word c,C_word *av) C_noret;
C_noret_decl(f_16557)
static void C_ccall f_16557(C_word c,C_word *av) C_noret;
C_noret_decl(f_16559)
static void C_ccall f_16559(C_word c,C_word *av) C_noret;
C_noret_decl(f_16565)
static void C_ccall f_16565(C_word c,C_word *av) C_noret;
C_noret_decl(f_16572)
static void C_ccall f_16572(C_word c,C_word *av) C_noret;
C_noret_decl(f_16575)
static void C_fcall f_16575(C_word t0,C_word t1) C_noret;
C_noret_decl(f_16579)
static void C_ccall f_16579(C_word c,C_word *av) C_noret;
C_noret_decl(f_16585)
static void C_ccall f_16585(C_word c,C_word *av) C_noret;
C_noret_decl(f_16591)
static void C_ccall f_16591(C_word c,C_word *av) C_noret;
C_noret_decl(f_16618)
static void C_ccall f_16618(C_word c,C_word *av) C_noret;
C_noret_decl(f_16620)
static void C_fcall f_16620(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_16634)
static void C_ccall f_16634(C_word c,C_word *av) C_noret;
C_noret_decl(f_16644)
static void C_ccall f_16644(C_word c,C_word *av) C_noret;
C_noret_decl(f_16657)
static void C_ccall f_16657(C_word c,C_word *av) C_noret;
C_noret_decl(f_16672)
static void C_ccall f_16672(C_word c,C_word *av) C_noret;
C_noret_decl(f_16676)
static void C_ccall f_16676(C_word c,C_word *av) C_noret;
C_noret_decl(f_16683)
static void C_ccall f_16683(C_word c,C_word *av) C_noret;
C_noret_decl(f_16687)
static void C_ccall f_16687(C_word c,C_word *av) C_noret;
C_noret_decl(f_16692)
static void C_ccall f_16692(C_word c,C_word *av) C_noret;
C_noret_decl(f_16696)
static void C_ccall f_16696(C_word c,C_word *av) C_noret;
C_noret_decl(f_16704)
static void C_ccall f_16704(C_word c,C_word *av) C_noret;
C_noret_decl(f_16710)
static void C_ccall f_16710(C_word c,C_word *av) C_noret;
C_noret_decl(f_16717)
static void C_ccall f_16717(C_word c,C_word *av) C_noret;
C_noret_decl(f_16720)
static void C_ccall f_16720(C_word c,C_word *av) C_noret;
C_noret_decl(f_16723)
static void C_ccall f_16723(C_word c,C_word *av) C_noret;
C_noret_decl(f_16728)
static void C_ccall f_16728(C_word c,C_word *av) C_noret;
C_noret_decl(f_16748)
static void C_ccall f_16748(C_word c,C_word *av) C_noret;
C_noret_decl(f_16752)
static void C_ccall f_16752(C_word c,C_word *av) C_noret;
C_noret_decl(f_16763)
static void C_ccall f_16763(C_word c,C_word *av) C_noret;
C_noret_decl(f_16778)
static void C_ccall f_16778(C_word c,C_word *av) C_noret;
C_noret_decl(f_16790)
static void C_ccall f_16790(C_word c,C_word *av) C_noret;
C_noret_decl(f_16797)
static void C_ccall f_16797(C_word c,C_word *av) C_noret;
C_noret_decl(f_16827)
static void C_ccall f_16827(C_word c,C_word *av) C_noret;
C_noret_decl(f_16851)
static void C_ccall f_16851(C_word c,C_word *av) C_noret;
C_noret_decl(f_16866)
static void C_ccall f_16866(C_word c,C_word *av) C_noret;
C_noret_decl(f_16869)
static void C_ccall f_16869(C_word c,C_word *av) C_noret;
C_noret_decl(f_16875)
static void C_ccall f_16875(C_word c,C_word *av) C_noret;
C_noret_decl(f_16884)
static void C_ccall f_16884(C_word c,C_word *av) C_noret;
C_noret_decl(f_16887)
static void C_ccall f_16887(C_word c,C_word *av) C_noret;
C_noret_decl(f_16926)
static void C_ccall f_16926(C_word c,C_word *av) C_noret;
C_noret_decl(f_16932)
static void C_ccall f_16932(C_word c,C_word *av) C_noret;
C_noret_decl(f_16938)
static void C_ccall f_16938(C_word c,C_word *av) C_noret;
C_noret_decl(f_16941)
static void C_ccall f_16941(C_word c,C_word *av) C_noret;
C_noret_decl(f_16947)
static void C_ccall f_16947(C_word c,C_word *av) C_noret;
C_noret_decl(f_16953)
static void C_ccall f_16953(C_word c,C_word *av) C_noret;
C_noret_decl(f_16959)
static void C_ccall f_16959(C_word c,C_word *av) C_noret;
C_noret_decl(f_16965)
static void C_ccall f_16965(C_word c,C_word *av) C_noret;
C_noret_decl(f_16987)
static void C_ccall f_16987(C_word c,C_word *av) C_noret;
C_noret_decl(f_16989)
static void C_fcall f_16989(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_17023)
static void C_fcall f_17023(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_17057)
static void C_ccall f_17057(C_word c,C_word *av) C_noret;
C_noret_decl(f_17060)
static void C_ccall f_17060(C_word c,C_word *av) C_noret;
C_noret_decl(f_17088)
static void C_ccall f_17088(C_word c,C_word *av) C_noret;
C_noret_decl(f_17095)
static void C_ccall f_17095(C_word c,C_word *av) C_noret;
C_noret_decl(f_17110)
static void C_ccall f_17110(C_word c,C_word *av) C_noret;
C_noret_decl(f_17116)
static void C_ccall f_17116(C_word c,C_word *av) C_noret;
C_noret_decl(f_17119)
static void C_ccall f_17119(C_word c,C_word *av) C_noret;
C_noret_decl(f_17156)
static void C_ccall f_17156(C_word c,C_word *av) C_noret;
C_noret_decl(f_17171)
static void C_ccall f_17171(C_word c,C_word *av) C_noret;
C_noret_decl(f_17181)
static void C_ccall f_17181(C_word c,C_word *av) C_noret;
C_noret_decl(f_17184)
static void C_ccall f_17184(C_word c,C_word *av) C_noret;
C_noret_decl(f_17196)
static void C_ccall f_17196(C_word c,C_word *av) C_noret;
C_noret_decl(f_17202)
static void C_ccall f_17202(C_word c,C_word *av) C_noret;
C_noret_decl(f_17208)
static void C_ccall f_17208(C_word c,C_word *av) C_noret;
C_noret_decl(f_17211)
static void C_ccall f_17211(C_word c,C_word *av) C_noret;
C_noret_decl(f_17213)
static void C_fcall f_17213(C_word t0,C_word t1) C_noret;
C_noret_decl(f_17220)
static void C_fcall f_17220(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_17226)
static void C_ccall f_17226(C_word c,C_word *av) C_noret;
C_noret_decl(f_17237)
static void C_ccall f_17237(C_word c,C_word *av) C_noret;
C_noret_decl(f_17285)
static void C_ccall f_17285(C_word c,C_word *av) C_noret;
C_noret_decl(f_17287)
static C_word C_fcall f_17287(C_word t0,C_word t1);
C_noret_decl(f_17293)
static void C_ccall f_17293(C_word c,C_word *av) C_noret;
C_noret_decl(f_17297)
static void C_ccall f_17297(C_word c,C_word *av) C_noret;
C_noret_decl(f_17302)
static void C_fcall f_17302(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_17330)
static void C_ccall f_17330(C_word c,C_word *av) C_noret;
C_noret_decl(f_17338)
static void C_ccall f_17338(C_word c,C_word *av) C_noret;
C_noret_decl(f_17341)
static void C_ccall f_17341(C_word c,C_word *av) C_noret;
C_noret_decl(f_17344)
static void C_ccall f_17344(C_word c,C_word *av) C_noret;
C_noret_decl(f_17347)
static void C_ccall f_17347(C_word c,C_word *av) C_noret;
C_noret_decl(f_17350)
static void C_ccall f_17350(C_word c,C_word *av) C_noret;
C_noret_decl(f_17353)
static void C_ccall f_17353(C_word c,C_word *av) C_noret;
C_noret_decl(f_17354)
static void C_fcall f_17354(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_17364)
static void C_ccall f_17364(C_word c,C_word *av) C_noret;
C_noret_decl(f_17370)
static void C_ccall f_17370(C_word c,C_word *av) C_noret;
C_noret_decl(f_17382)
static void C_ccall f_17382(C_word c,C_word *av) C_noret;
C_noret_decl(f_17385)
static void C_ccall f_17385(C_word c,C_word *av) C_noret;
C_noret_decl(f_17388)
static void C_ccall f_17388(C_word c,C_word *av) C_noret;
C_noret_decl(f_17393)
static void C_fcall f_17393(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_17406)
static void C_ccall f_17406(C_word c,C_word *av) C_noret;
C_noret_decl(f_17409)
static void C_ccall f_17409(C_word c,C_word *av) C_noret;
C_noret_decl(f_17426)
static void C_fcall f_17426(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_17436)
static void C_ccall f_17436(C_word c,C_word *av) C_noret;
C_noret_decl(f_17449)
static void C_ccall f_17449(C_word c,C_word *av) C_noret;
C_noret_decl(f_17453)
static void C_ccall f_17453(C_word c,C_word *av) C_noret;
C_noret_decl(f_17471)
static void C_ccall f_17471(C_word c,C_word *av) C_noret;
C_noret_decl(f_17475)
static void C_ccall f_17475(C_word c,C_word *av) C_noret;
C_noret_decl(f_17492)
static void C_ccall f_17492(C_word c,C_word *av) C_noret;
C_noret_decl(f_17498)
static void C_ccall f_17498(C_word c,C_word *av) C_noret;
C_noret_decl(f_17508)
static void C_ccall f_17508(C_word c,C_word *av) C_noret;
C_noret_decl(f_17511)
static void C_ccall f_17511(C_word c,C_word *av) C_noret;
C_noret_decl(f_17527)
static void C_ccall f_17527(C_word c,C_word *av) C_noret;
C_noret_decl(f_17532)
static void C_fcall f_17532(C_word t0,C_word t1) C_noret;
C_noret_decl(f_17536)
static void C_ccall f_17536(C_word c,C_word *av) C_noret;
C_noret_decl(f_17553)
static void C_ccall f_17553(C_word c,C_word *av) C_noret;
C_noret_decl(f_17564)
static void C_ccall f_17564(C_word c,C_word *av) C_noret;
C_noret_decl(f_17576)
static void C_ccall f_17576(C_word c,C_word *av) C_noret;
C_noret_decl(f_17579)
static void C_ccall f_17579(C_word c,C_word *av) C_noret;
C_noret_decl(f_17587)
static void C_ccall f_17587(C_word c,C_word *av) C_noret;
C_noret_decl(f_17592)
static void C_ccall f_17592(C_word c,C_word *av) C_noret;
C_noret_decl(f_17605)
static void C_ccall f_17605(C_word c,C_word *av) C_noret;
C_noret_decl(f_17616)
static void C_ccall f_17616(C_word c,C_word *av) C_noret;
C_noret_decl(f_17638)
static void C_ccall f_17638(C_word c,C_word *av) C_noret;
C_noret_decl(f_17640)
static void C_ccall f_17640(C_word c,C_word *av) C_noret;
C_noret_decl(f_17660)
static void C_ccall f_17660(C_word c,C_word *av) C_noret;
C_noret_decl(f_17680)
static void C_ccall f_17680(C_word c,C_word *av) C_noret;
C_noret_decl(f_17688)
static void C_ccall f_17688(C_word c,C_word *av) C_noret;
C_noret_decl(f_17697)
static void C_ccall f_17697(C_word c,C_word *av) C_noret;
C_noret_decl(f_17702)
static void C_ccall f_17702(C_word c,C_word *av) C_noret;
C_noret_decl(f_17706)
static void C_ccall f_17706(C_word c,C_word *av) C_noret;
C_noret_decl(f_17727)
static void C_ccall f_17727(C_word c,C_word *av) C_noret;
C_noret_decl(f_17742)
static void C_ccall f_17742(C_word c,C_word *av) C_noret;
C_noret_decl(f_17748)
static void C_ccall f_17748(C_word c,C_word *av) C_noret;
C_noret_decl(f_17759)
static void C_ccall f_17759(C_word c,C_word *av) C_noret;
C_noret_decl(f_17770)
static void C_ccall f_17770(C_word c,C_word *av) C_noret;
C_noret_decl(f_17781)
static void C_ccall f_17781(C_word c,C_word *av) C_noret;
C_noret_decl(f_17785)
static void C_ccall f_17785(C_word c,C_word *av) C_noret;
C_noret_decl(f_17791)
static void C_ccall f_17791(C_word c,C_word *av) C_noret;
C_noret_decl(f_17803)
static void C_ccall f_17803(C_word c,C_word *av) C_noret;
C_noret_decl(f_17807)
static void C_ccall f_17807(C_word c,C_word *av) C_noret;
C_noret_decl(f_17819)
static void C_ccall f_17819(C_word c,C_word *av) C_noret;
C_noret_decl(f_17827)
static void C_fcall f_17827(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_17837)
static void C_ccall f_17837(C_word c,C_word *av) C_noret;
C_noret_decl(f_17852)
static void C_ccall f_17852(C_word c,C_word *av) C_noret;
C_noret_decl(f_17858)
static void C_ccall f_17858(C_word c,C_word *av) C_noret;
C_noret_decl(f_17861)
static void C_ccall f_17861(C_word c,C_word *av) C_noret;
C_noret_decl(f_17864)
static void C_ccall f_17864(C_word c,C_word *av) C_noret;
C_noret_decl(f_17867)
static void C_ccall f_17867(C_word c,C_word *av) C_noret;
C_noret_decl(f_17870)
static void C_ccall f_17870(C_word c,C_word *av) C_noret;
C_noret_decl(f_17874)
static void C_ccall f_17874(C_word c,C_word *av) C_noret;
C_noret_decl(f_17876)
static void C_ccall f_17876(C_word c,C_word *av) C_noret;
C_noret_decl(f_17883)
static void C_ccall f_17883(C_word c,C_word *av) C_noret;
C_noret_decl(f_17890)
static void C_ccall f_17890(C_word c,C_word *av) C_noret;
C_noret_decl(f_17901)
static void C_ccall f_17901(C_word c,C_word *av) C_noret;
C_noret_decl(f_17905)
static void C_ccall f_17905(C_word c,C_word *av) C_noret;
C_noret_decl(f_17908)
static void C_ccall f_17908(C_word c,C_word *av) C_noret;
C_noret_decl(f_17913)
static void C_ccall f_17913(C_word c,C_word *av) C_noret;
C_noret_decl(f_17919)
static void C_ccall f_17919(C_word c,C_word *av) C_noret;
C_noret_decl(f_17926)
static void C_ccall f_17926(C_word c,C_word *av) C_noret;
C_noret_decl(f_17929)
static void C_ccall f_17929(C_word c,C_word *av) C_noret;
C_noret_decl(f_17932)
static void C_ccall f_17932(C_word c,C_word *av) C_noret;
C_noret_decl(f_17935)
static void C_ccall f_17935(C_word c,C_word *av) C_noret;
C_noret_decl(f_5288)
static void C_ccall f_5288(C_word c,C_word *av) C_noret;
C_noret_decl(f_5291)
static void C_ccall f_5291(C_word c,C_word *av) C_noret;
C_noret_decl(f_5294)
static void C_ccall f_5294(C_word c,C_word *av) C_noret;
C_noret_decl(f_5297)
static void C_ccall f_5297(C_word c,C_word *av) C_noret;
C_noret_decl(f_5300)
static void C_ccall f_5300(C_word c,C_word *av) C_noret;
C_noret_decl(f_5303)
static void C_ccall f_5303(C_word c,C_word *av) C_noret;
C_noret_decl(f_5306)
static void C_ccall f_5306(C_word c,C_word *av) C_noret;
C_noret_decl(f_5309)
static void C_ccall f_5309(C_word c,C_word *av) C_noret;
C_noret_decl(f_5312)
static void C_ccall f_5312(C_word c,C_word *av) C_noret;
C_noret_decl(f_5422)
static void C_fcall f_5422(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5440)
static void C_ccall f_5440(C_word c,C_word *av) C_noret;
C_noret_decl(f_5480)
static void C_fcall f_5480(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_5494)
static void C_ccall f_5494(C_word c,C_word *av) C_noret;
C_noret_decl(f_5683)
static void C_fcall f_5683(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5689)
static void C_fcall f_5689(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5711)
static void C_ccall f_5711(C_word c,C_word *av) C_noret;
C_noret_decl(f_5717)
static void C_fcall f_5717(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5723)
static void C_fcall f_5723(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5733)
static void C_ccall f_5733(C_word c,C_word *av) C_noret;
C_noret_decl(f_5747)
static void C_fcall f_5747(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5753)
static void C_fcall f_5753(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_5767)
static void C_ccall f_5767(C_word c,C_word *av) C_noret;
C_noret_decl(f_5976)
static void C_fcall f_5976(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5984)
static void C_fcall f_5984(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_5992)
static C_word C_fcall f_5992(C_word *a,C_word t0,C_word t1);
C_noret_decl(f_6010)
static void C_ccall f_6010(C_word c,C_word *av) C_noret;
C_noret_decl(f_6049)
static void C_fcall f_6049(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6083)
static void C_fcall f_6083(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6089)
static C_word C_fcall f_6089(C_word t0);
C_noret_decl(f_6142)
static void C_fcall f_6142(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6148)
static void C_fcall f_6148(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6331)
static void C_fcall f_6331(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6349)
static void C_ccall f_6349(C_word c,C_word *av) C_noret;
C_noret_decl(f_6444)
static void C_fcall f_6444(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6457)
static void C_ccall f_6457(C_word c,C_word *av) C_noret;
C_noret_decl(f_6595)
static void C_ccall f_6595(C_word c,C_word *av) C_noret;
C_noret_decl(f_6599)
static void C_ccall f_6599(C_word c,C_word *av) C_noret;
C_noret_decl(f_6613)
static void C_ccall f_6613(C_word c,C_word *av) C_noret;
C_noret_decl(f_6624)
static void C_ccall f_6624(C_word c,C_word *av) C_noret;
C_noret_decl(f_6627)
static void C_fcall f_6627(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6642)
static void C_ccall f_6642(C_word c,C_word *av) C_noret;
C_noret_decl(f_6648)
static void C_ccall f_6648(C_word c,C_word *av) C_noret;
C_noret_decl(f_6651)
static void C_fcall f_6651(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6657)
static void C_ccall f_6657(C_word c,C_word *av) C_noret;
C_noret_decl(f_6661)
static void C_ccall f_6661(C_word c,C_word *av) C_noret;
C_noret_decl(f_6664)
static void C_ccall f_6664(C_word c,C_word *av) C_noret;
C_noret_decl(f_6673)
static void C_ccall f_6673(C_word c,C_word *av) C_noret;
C_noret_decl(f_6681)
static void C_ccall f_6681(C_word c,C_word *av) C_noret;
C_noret_decl(f_6688)
static void C_ccall f_6688(C_word c,C_word *av) C_noret;
C_noret_decl(f_6693)
static void C_fcall f_6693(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6703)
static void C_ccall f_6703(C_word c,C_word *av) C_noret;
C_noret_decl(f_6716)
static void C_fcall f_6716(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6723)
static void C_ccall f_6723(C_word c,C_word *av) C_noret;
C_noret_decl(f_6726)
static void C_ccall f_6726(C_word c,C_word *av) C_noret;
C_noret_decl(f_6735)
static void C_ccall f_6735(C_word c,C_word *av) C_noret;
C_noret_decl(f_6738)
static void C_ccall f_6738(C_word c,C_word *av) C_noret;
C_noret_decl(f_6741)
static void C_ccall f_6741(C_word c,C_word *av) C_noret;
C_noret_decl(f_6744)
static void C_ccall f_6744(C_word c,C_word *av) C_noret;
C_noret_decl(f_6747)
static void C_ccall f_6747(C_word c,C_word *av) C_noret;
C_noret_decl(f_6750)
static void C_ccall f_6750(C_word c,C_word *av) C_noret;
C_noret_decl(f_6756)
static void C_ccall f_6756(C_word c,C_word *av) C_noret;
C_noret_decl(f_6759)
static void C_ccall f_6759(C_word c,C_word *av) C_noret;
C_noret_decl(f_6766)
static void C_ccall f_6766(C_word c,C_word *av) C_noret;
C_noret_decl(f_6768)
static void C_ccall f_6768(C_word c,C_word *av) C_noret;
C_noret_decl(f_6771)
static void C_fcall f_6771(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6773)
static void C_fcall f_6773(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6780)
static void C_ccall f_6780(C_word c,C_word *av) C_noret;
C_noret_decl(f_6783)
static void C_ccall f_6783(C_word c,C_word *av) C_noret;
C_noret_decl(f_6786)
static void C_ccall f_6786(C_word c,C_word *av) C_noret;
C_noret_decl(f_6800)
static void C_ccall f_6800(C_word c,C_word *av) C_noret;
C_noret_decl(f_6805)
static void C_fcall f_6805(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6815)
static void C_ccall f_6815(C_word c,C_word *av) C_noret;
C_noret_decl(f_6832)
static void C_ccall f_6832(C_word c,C_word *av) C_noret;
C_noret_decl(f_6835)
static void C_ccall f_6835(C_word c,C_word *av) C_noret;
C_noret_decl(f_6838)
static void C_ccall f_6838(C_word c,C_word *av) C_noret;
C_noret_decl(f_6841)
static void C_ccall f_6841(C_word c,C_word *av) C_noret;
C_noret_decl(f_6847)
static void C_ccall f_6847(C_word c,C_word *av) C_noret;
C_noret_decl(f_6856)
static void C_ccall f_6856(C_word c,C_word *av) C_noret;
C_noret_decl(f_6863)
static void C_ccall f_6863(C_word c,C_word *av) C_noret;
C_noret_decl(f_6865)
static void C_ccall f_6865(C_word c,C_word *av) C_noret;
C_noret_decl(f_6869)
static void C_ccall f_6869(C_word c,C_word *av) C_noret;
C_noret_decl(f_6872)
static void C_ccall f_6872(C_word c,C_word *av) C_noret;
C_noret_decl(f_6879)
static void C_ccall f_6879(C_word c,C_word *av) C_noret;
C_noret_decl(f_6881)
static void C_ccall f_6881(C_word c,C_word *av) C_noret;
C_noret_decl(f_6885)
static void C_fcall f_6885(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6888)
static void C_ccall f_6888(C_word c,C_word *av) C_noret;
C_noret_decl(f_6889)
static void C_fcall f_6889(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6899)
static void C_ccall f_6899(C_word c,C_word *av) C_noret;
C_noret_decl(f_6902)
static void C_ccall f_6902(C_word c,C_word *av) C_noret;
C_noret_decl(f_6907)
static void C_fcall f_6907(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6917)
static void C_ccall f_6917(C_word c,C_word *av) C_noret;
C_noret_decl(f_6934)
static void C_ccall f_6934(C_word c,C_word *av) C_noret;
C_noret_decl(f_6937)
static void C_ccall f_6937(C_word c,C_word *av) C_noret;
C_noret_decl(f_6940)
static void C_ccall f_6940(C_word c,C_word *av) C_noret;
C_noret_decl(f_6943)
static void C_ccall f_6943(C_word c,C_word *av) C_noret;
C_noret_decl(f_6946)
static void C_ccall f_6946(C_word c,C_word *av) C_noret;
C_noret_decl(f_6955)
static void C_ccall f_6955(C_word c,C_word *av) C_noret;
C_noret_decl(f_6958)
static void C_ccall f_6958(C_word c,C_word *av) C_noret;
C_noret_decl(f_6961)
static void C_ccall f_6961(C_word c,C_word *av) C_noret;
C_noret_decl(f_6978)
static void C_ccall f_6978(C_word c,C_word *av) C_noret;
C_noret_decl(f_7032)
static void C_ccall f_7032(C_word c,C_word *av) C_noret;
C_noret_decl(f_7038)
static C_word C_fcall f_7038(C_word t0,C_word t1);
C_noret_decl(f_7073)
static void C_ccall f_7073(C_word c,C_word *av) C_noret;
C_noret_decl(f_7079)
static void C_fcall f_7079(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_7101)
static void C_ccall f_7101(C_word c,C_word *av) C_noret;
C_noret_decl(f_7110)
static void C_ccall f_7110(C_word c,C_word *av) C_noret;
C_noret_decl(f_7122)
static void C_ccall f_7122(C_word c,C_word *av) C_noret;
C_noret_decl(f_7126)
static void C_ccall f_7126(C_word c,C_word *av) C_noret;
C_noret_decl(f_7128)
static void C_fcall f_7128(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7150)
static void C_fcall f_7150(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7157)
static void C_fcall f_7157(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7161)
static void C_ccall f_7161(C_word c,C_word *av) C_noret;
C_noret_decl(f_7165)
static void C_ccall f_7165(C_word c,C_word *av) C_noret;
C_noret_decl(f_7171)
static void C_ccall f_7171(C_word c,C_word *av) C_noret;
C_noret_decl(f_7193)
static void C_ccall f_7193(C_word c,C_word *av) C_noret;
C_noret_decl(f_7209)
static void C_ccall f_7209(C_word c,C_word *av) C_noret;
C_noret_decl(f_7213)
static void C_ccall f_7213(C_word c,C_word *av) C_noret;
C_noret_decl(f_7234)
static void C_ccall f_7234(C_word c,C_word *av) C_noret;
C_noret_decl(f_7257)
static void C_ccall f_7257(C_word c,C_word *av) C_noret;
C_noret_decl(f_7259)
static void C_ccall f_7259(C_word c,C_word *av) C_noret;
C_noret_decl(f_7266)
static void C_ccall f_7266(C_word c,C_word *av) C_noret;
C_noret_decl(f_7273)
static void C_ccall f_7273(C_word c,C_word *av) C_noret;
C_noret_decl(f_7286)
static void C_ccall f_7286(C_word c,C_word *av) C_noret;
C_noret_decl(f_7317)
static void C_ccall f_7317(C_word c,C_word *av) C_noret;
C_noret_decl(f_7329)
static void C_ccall f_7329(C_word c,C_word *av) C_noret;
C_noret_decl(f_7343)
static void C_ccall f_7343(C_word c,C_word *av) C_noret;
C_noret_decl(f_7345)
static void C_fcall f_7345(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7371)
static void C_ccall f_7371(C_word c,C_word *av) C_noret;
C_noret_decl(f_7385)
static void C_fcall f_7385(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_7391)
static void C_fcall f_7391(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_7406)
static void C_ccall f_7406(C_word c,C_word *av) C_noret;
C_noret_decl(f_7422)
static void C_ccall f_7422(C_word c,C_word *av) C_noret;
C_noret_decl(f_7430)
static void C_ccall f_7430(C_word c,C_word *av) C_noret;
C_noret_decl(f_7434)
static void C_ccall f_7434(C_word c,C_word *av) C_noret;
C_noret_decl(f_7436)
static void C_ccall f_7436(C_word c,C_word *av) C_noret;
C_noret_decl(f_7447)
static void C_ccall f_7447(C_word c,C_word *av) C_noret;
C_noret_decl(f_7449)
static void C_fcall f_7449(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_7466)
static void C_ccall f_7466(C_word c,C_word *av) C_noret;
C_noret_decl(f_7480)
static void C_ccall f_7480(C_word c,C_word *av) C_noret;
C_noret_decl(f_7514)
static void C_ccall f_7514(C_word c,C_word *av) C_noret;
C_noret_decl(f_7526)
static void C_ccall f_7526(C_word c,C_word *av) C_noret;
C_noret_decl(f_7542)
static void C_ccall f_7542(C_word c,C_word *av) C_noret;
C_noret_decl(f_7572)
static void C_ccall f_7572(C_word c,C_word *av) C_noret;
C_noret_decl(f_7576)
static void C_fcall f_7576(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7616)
static void C_ccall f_7616(C_word c,C_word *av) C_noret;
C_noret_decl(f_7618)
static void C_ccall f_7618(C_word c,C_word *av) C_noret;
C_noret_decl(f_7634)
static void C_ccall f_7634(C_word c,C_word *av) C_noret;
C_noret_decl(f_7640)
static void C_ccall f_7640(C_word c,C_word *av) C_noret;
C_noret_decl(f_7655)
static void C_ccall f_7655(C_word c,C_word *av) C_noret;
C_noret_decl(f_7672)
static void C_ccall f_7672(C_word c,C_word *av) C_noret;
C_noret_decl(f_7674)
static void C_ccall f_7674(C_word c,C_word *av) C_noret;
C_noret_decl(f_7680)
static void C_fcall f_7680(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7704)
static void C_fcall f_7704(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7720)
static void C_ccall f_7720(C_word c,C_word *av) C_noret;
C_noret_decl(f_7730)
static void C_ccall f_7730(C_word c,C_word *av) C_noret;
C_noret_decl(f_7735)
static void C_ccall f_7735(C_word c,C_word *av) C_noret;
C_noret_decl(f_7749)
static void C_ccall f_7749(C_word c,C_word *av) C_noret;
C_noret_decl(f_7752)
static void C_ccall f_7752(C_word c,C_word *av) C_noret;
C_noret_decl(f_7753)
static void C_ccall f_7753(C_word c,C_word *av) C_noret;
C_noret_decl(f_7757)
static void C_ccall f_7757(C_word c,C_word *av) C_noret;
C_noret_decl(f_7762)
static void C_ccall f_7762(C_word c,C_word *av) C_noret;
C_noret_decl(f_7768)
static void C_ccall f_7768(C_word c,C_word *av) C_noret;
C_noret_decl(f_7774)
static void C_ccall f_7774(C_word c,C_word *av) C_noret;
C_noret_decl(f_7782)
static void C_ccall f_7782(C_word c,C_word *av) C_noret;
C_noret_decl(f_7785)
static void C_ccall f_7785(C_word c,C_word *av) C_noret;
C_noret_decl(f_7793)
static void C_ccall f_7793(C_word c,C_word *av) C_noret;
C_noret_decl(f_7795)
static void C_fcall f_7795(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7799)
static void C_ccall f_7799(C_word c,C_word *av) C_noret;
C_noret_decl(f_7821)
static void C_ccall f_7821(C_word c,C_word *av) C_noret;
C_noret_decl(f_7827)
static void C_fcall f_7827(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7831)
static void C_ccall f_7831(C_word c,C_word *av) C_noret;
C_noret_decl(f_7844)
static void C_ccall f_7844(C_word c,C_word *av) C_noret;
C_noret_decl(f_7852)
static void C_fcall f_7852(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7858)
static void C_ccall f_7858(C_word c,C_word *av) C_noret;
C_noret_decl(f_7869)
static void C_ccall f_7869(C_word c,C_word *av) C_noret;
C_noret_decl(f_7871)
static void C_ccall f_7871(C_word c,C_word *av) C_noret;
C_noret_decl(f_7874)
static void C_ccall f_7874(C_word c,C_word *av) C_noret;
C_noret_decl(f_7880)
static C_word C_fcall f_7880(C_word t0,C_word t1);
C_noret_decl(f_7919)
static void C_ccall f_7919(C_word c,C_word *av) C_noret;
C_noret_decl(f_7924)
static void C_ccall f_7924(C_word c,C_word *av) C_noret;
C_noret_decl(f_7928)
static void C_ccall f_7928(C_word c,C_word *av) C_noret;
C_noret_decl(f_7932)
static void C_ccall f_7932(C_word c,C_word *av) C_noret;
C_noret_decl(f_7983)
static void C_ccall f_7983(C_word c,C_word *av) C_noret;
C_noret_decl(f_8020)
static void C_ccall f_8020(C_word c,C_word *av) C_noret;
C_noret_decl(f_8022)
static void C_fcall f_8022(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8072)
static void C_ccall f_8072(C_word c,C_word *av) C_noret;
C_noret_decl(f_8076)
static void C_ccall f_8076(C_word c,C_word *av) C_noret;
C_noret_decl(f_8090)
static void C_ccall f_8090(C_word c,C_word *av) C_noret;
C_noret_decl(f_8094)
static void C_ccall f_8094(C_word c,C_word *av) C_noret;
C_noret_decl(f_8102)
static C_word C_fcall f_8102(C_word t0,C_word t1);
C_noret_decl(f_8108)
static void C_ccall f_8108(C_word c,C_word *av) C_noret;
C_noret_decl(f_8112)
static void C_ccall f_8112(C_word c,C_word *av) C_noret;
C_noret_decl(f_8154)
static void C_ccall f_8154(C_word c,C_word *av) C_noret;
C_noret_decl(f_8158)
static void C_ccall f_8158(C_word c,C_word *av) C_noret;
C_noret_decl(f_8206)
static void C_ccall f_8206(C_word c,C_word *av) C_noret;
C_noret_decl(f_8210)
static void C_ccall f_8210(C_word c,C_word *av) C_noret;
C_noret_decl(f_8215)
static void C_ccall f_8215(C_word c,C_word *av) C_noret;
C_noret_decl(f_8225)
static void C_ccall f_8225(C_word c,C_word *av) C_noret;
C_noret_decl(f_8232)
static void C_ccall f_8232(C_word c,C_word *av) C_noret;
C_noret_decl(f_8239)
static void C_fcall f_8239(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8266)
static void C_ccall f_8266(C_word c,C_word *av) C_noret;
C_noret_decl(f_8272)
static void C_ccall f_8272(C_word c,C_word *av) C_noret;
C_noret_decl(f_8282)
static void C_ccall f_8282(C_word c,C_word *av) C_noret;
C_noret_decl(f_8285)
static void C_ccall f_8285(C_word c,C_word *av) C_noret;
C_noret_decl(f_8288)
static void C_ccall f_8288(C_word c,C_word *av) C_noret;
C_noret_decl(f_8301)
static void C_ccall f_8301(C_word c,C_word *av) C_noret;
C_noret_decl(f_8303)
static void C_fcall f_8303(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8338)
static void C_ccall f_8338(C_word c,C_word *av) C_noret;
C_noret_decl(f_8344)
static void C_ccall f_8344(C_word c,C_word *av) C_noret;
C_noret_decl(f_8350)
static void C_ccall f_8350(C_word c,C_word *av) C_noret;
C_noret_decl(f_8359)
static void C_ccall f_8359(C_word c,C_word *av) C_noret;
C_noret_decl(f_8368)
static void C_ccall f_8368(C_word c,C_word *av) C_noret;
C_noret_decl(f_8377)
static void C_ccall f_8377(C_word c,C_word *av) C_noret;
C_noret_decl(f_8386)
static void C_ccall f_8386(C_word c,C_word *av) C_noret;
C_noret_decl(f_8395)
static void C_ccall f_8395(C_word c,C_word *av) C_noret;
C_noret_decl(f_8405)
static void C_ccall f_8405(C_word c,C_word *av) C_noret;
C_noret_decl(f_8407)
static void C_ccall f_8407(C_word c,C_word *av) C_noret;
C_noret_decl(f_8413)
static void C_ccall f_8413(C_word c,C_word *av) C_noret;
C_noret_decl(f_8428)
static void C_ccall f_8428(C_word c,C_word *av) C_noret;
C_noret_decl(f_8443)
static void C_ccall f_8443(C_word c,C_word *av) C_noret;
C_noret_decl(f_8446)
static void C_fcall f_8446(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8513)
static void C_ccall f_8513(C_word c,C_word *av) C_noret;
C_noret_decl(f_8515)
static void C_fcall f_8515(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8540)
static void C_ccall f_8540(C_word c,C_word *av) C_noret;
C_noret_decl(f_8563)
static void C_ccall f_8563(C_word c,C_word *av) C_noret;
C_noret_decl(f_8566)
static void C_fcall f_8566(C_word t0,C_word t1) C_noret;
C_noret_decl(f_8569)
static void C_ccall f_8569(C_word c,C_word *av) C_noret;
C_noret_decl(f_8576)
static void C_ccall f_8576(C_word c,C_word *av) C_noret;
C_noret_decl(f_8623)
static void C_ccall f_8623(C_word c,C_word *av) C_noret;
C_noret_decl(f_8627)
static void C_ccall f_8627(C_word c,C_word *av) C_noret;
C_noret_decl(f_8632)
static void C_fcall f_8632(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8649)
static void C_ccall f_8649(C_word c,C_word *av) C_noret;
C_noret_decl(f_8657)
static void C_ccall f_8657(C_word c,C_word *av) C_noret;
C_noret_decl(f_8659)
static void C_fcall f_8659(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8684)
static void C_ccall f_8684(C_word c,C_word *av) C_noret;
C_noret_decl(f_8720)
static void C_ccall f_8720(C_word c,C_word *av) C_noret;
C_noret_decl(f_8754)
static void C_ccall f_8754(C_word c,C_word *av) C_noret;
C_noret_decl(f_8785)
static void C_fcall f_8785(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_8808)
static void C_ccall f_8808(C_word c,C_word *av) C_noret;
C_noret_decl(f_8829)
static void C_ccall f_8829(C_word c,C_word *av) C_noret;
C_noret_decl(f_8851)
static void C_ccall f_8851(C_word c,C_word *av) C_noret;
C_noret_decl(f_8859)
static void C_ccall f_8859(C_word c,C_word *av) C_noret;
C_noret_decl(f_8863)
static void C_ccall f_8863(C_word c,C_word *av) C_noret;
C_noret_decl(f_8871)
static void C_ccall f_8871(C_word c,C_word *av) C_noret;
C_noret_decl(f_8892)
static void C_ccall f_8892(C_word c,C_word *av) C_noret;
C_noret_decl(f_8896)
static void C_ccall f_8896(C_word c,C_word *av) C_noret;
C_noret_decl(f_8908)
static void C_ccall f_8908(C_word c,C_word *av) C_noret;
C_noret_decl(f_8935)
static void C_fcall f_8935(C_word t0,C_word t1) C_noret;
C_noret_decl(f_8947)
static void C_ccall f_8947(C_word c,C_word *av) C_noret;
C_noret_decl(f_8949)
static void C_fcall f_8949(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8974)
static void C_ccall f_8974(C_word c,C_word *av) C_noret;
C_noret_decl(f_9008)
static void C_fcall f_9008(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9034)
static void C_ccall f_9034(C_word c,C_word *av) C_noret;
C_noret_decl(f_9036)
static void C_fcall f_9036(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9061)
static void C_ccall f_9061(C_word c,C_word *av) C_noret;
C_noret_decl(f_9145)
static void C_ccall f_9145(C_word c,C_word *av) C_noret;
C_noret_decl(f_9147)
static void C_fcall f_9147(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9172)
static void C_ccall f_9172(C_word c,C_word *av) C_noret;
C_noret_decl(f_9212)
static void C_ccall f_9212(C_word c,C_word *av) C_noret;
C_noret_decl(f_9253)
static void C_fcall f_9253(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9282)
static void C_ccall f_9282(C_word c,C_word *av) C_noret;
C_noret_decl(f_9284)
static void C_fcall f_9284(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9309)
static void C_ccall f_9309(C_word c,C_word *av) C_noret;
C_noret_decl(f_9345)
static void C_ccall f_9345(C_word c,C_word *av) C_noret;
C_noret_decl(f_9347)
static void C_fcall f_9347(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9372)
static void C_ccall f_9372(C_word c,C_word *av) C_noret;
C_noret_decl(f_9384)
static void C_ccall f_9384(C_word c,C_word *av) C_noret;
C_noret_decl(f_9390)
static void C_ccall f_9390(C_word c,C_word *av) C_noret;
C_noret_decl(f_9413)
static void C_ccall f_9413(C_word c,C_word *av) C_noret;
C_noret_decl(f_9415)
static void C_fcall f_9415(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9440)
static void C_ccall f_9440(C_word c,C_word *av) C_noret;
C_noret_decl(f_9451)
static void C_fcall f_9451(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9455)
static void C_ccall f_9455(C_word c,C_word *av) C_noret;
C_noret_decl(f_9458)
static void C_ccall f_9458(C_word c,C_word *av) C_noret;
C_noret_decl(f_9465)
static void C_ccall f_9465(C_word c,C_word *av) C_noret;
C_noret_decl(f_9479)
static void C_ccall f_9479(C_word c,C_word *av) C_noret;
C_noret_decl(f_9585)
static void C_ccall f_9585(C_word c,C_word *av) C_noret;
C_noret_decl(f_9587)
static void C_fcall f_9587(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9612)
static void C_ccall f_9612(C_word c,C_word *av) C_noret;
C_noret_decl(f_9630)
static void C_ccall f_9630(C_word c,C_word *av) C_noret;
C_noret_decl(f_9633)
static void C_ccall f_9633(C_word c,C_word *av) C_noret;
C_noret_decl(f_9639)
static void C_ccall f_9639(C_word c,C_word *av) C_noret;
C_noret_decl(f_9645)
static void C_fcall f_9645(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9679)
static void C_fcall f_9679(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9692)
static void C_ccall f_9692(C_word c,C_word *av) C_noret;
C_noret_decl(f_9694)
static void C_fcall f_9694(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9719)
static void C_ccall f_9719(C_word c,C_word *av) C_noret;
C_noret_decl(f_9749)
static void C_ccall f_9749(C_word c,C_word *av) C_noret;
C_noret_decl(f_9751)
static void C_fcall f_9751(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9776)
static void C_ccall f_9776(C_word c,C_word *av) C_noret;
C_noret_decl(f_9849)
static void C_ccall f_9849(C_word c,C_word *av) C_noret;
C_noret_decl(f_9852)
static void C_ccall f_9852(C_word c,C_word *av) C_noret;
C_noret_decl(f_9861)
static void C_ccall f_9861(C_word c,C_word *av) C_noret;
C_noret_decl(f_9865)
static void C_ccall f_9865(C_word c,C_word *av) C_noret;
C_noret_decl(f_9869)
static void C_ccall f_9869(C_word c,C_word *av) C_noret;
C_noret_decl(f_9871)
static void C_fcall f_9871(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_9919)
static void C_fcall f_9919(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9944)
static void C_ccall f_9944(C_word c,C_word *av) C_noret;
C_noret_decl(f_9972)
static void C_ccall f_9972(C_word c,C_word *av) C_noret;
C_noret_decl(f_9996)
static void C_ccall f_9996(C_word c,C_word *av) C_noret;
C_noret_decl(C_support_toplevel)
C_externexport void C_ccall C_support_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_10044)
static void C_ccall trf_10044(C_word c,C_word *av) C_noret;
static void C_ccall trf_10044(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10044(t0,t1,t2,t3);}

C_noret_decl(trf_10120)
static void C_ccall trf_10120(C_word c,C_word *av) C_noret;
static void C_ccall trf_10120(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10120(t0,t1,t2);}

C_noret_decl(trf_10177)
static void C_ccall trf_10177(C_word c,C_word *av) C_noret;
static void C_ccall trf_10177(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10177(t0,t1,t2);}

C_noret_decl(trf_10233)
static void C_ccall trf_10233(C_word c,C_word *av) C_noret;
static void C_ccall trf_10233(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_10233(t0,t1,t2,t3,t4);}

C_noret_decl(trf_10283)
static void C_ccall trf_10283(C_word c,C_word *av) C_noret;
static void C_ccall trf_10283(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_10283(t0,t1);}

C_noret_decl(trf_10303)
static void C_ccall trf_10303(C_word c,C_word *av) C_noret;
static void C_ccall trf_10303(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10303(t0,t1,t2);}

C_noret_decl(trf_10360)
static void C_ccall trf_10360(C_word c,C_word *av) C_noret;
static void C_ccall trf_10360(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10360(t0,t1,t2);}

C_noret_decl(trf_10411)
static void C_ccall trf_10411(C_word c,C_word *av) C_noret;
static void C_ccall trf_10411(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10411(t0,t1,t2);}

C_noret_decl(trf_10515)
static void C_ccall trf_10515(C_word c,C_word *av) C_noret;
static void C_ccall trf_10515(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10515(t0,t1,t2);}

C_noret_decl(trf_10615)
static void C_ccall trf_10615(C_word c,C_word *av) C_noret;
static void C_ccall trf_10615(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10615(t0,t1,t2,t3);}

C_noret_decl(trf_10630)
static void C_ccall trf_10630(C_word c,C_word *av) C_noret;
static void C_ccall trf_10630(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10630(t0,t1,t2,t3);}

C_noret_decl(trf_10760)
static void C_ccall trf_10760(C_word c,C_word *av) C_noret;
static void C_ccall trf_10760(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10760(t0,t1,t2);}

C_noret_decl(trf_10784)
static void C_ccall trf_10784(C_word c,C_word *av) C_noret;
static void C_ccall trf_10784(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10784(t0,t1,t2);}

C_noret_decl(trf_10811)
static void C_ccall trf_10811(C_word c,C_word *av) C_noret;
static void C_ccall trf_10811(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10811(t0,t1,t2);}

C_noret_decl(trf_10857)
static void C_ccall trf_10857(C_word c,C_word *av) C_noret;
static void C_ccall trf_10857(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10857(t0,t1,t2);}

C_noret_decl(trf_10912)
static void C_ccall trf_10912(C_word c,C_word *av) C_noret;
static void C_ccall trf_10912(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10912(t0,t1,t2,t3);}

C_noret_decl(trf_11083)
static void C_ccall trf_11083(C_word c,C_word *av) C_noret;
static void C_ccall trf_11083(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11083(t0,t1,t2);}

C_noret_decl(trf_11116)
static void C_ccall trf_11116(C_word c,C_word *av) C_noret;
static void C_ccall trf_11116(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11116(t0,t1,t2);}

C_noret_decl(trf_11128)
static void C_ccall trf_11128(C_word c,C_word *av) C_noret;
static void C_ccall trf_11128(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11128(t0,t1,t2);}

C_noret_decl(trf_11195)
static void C_ccall trf_11195(C_word c,C_word *av) C_noret;
static void C_ccall trf_11195(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_11195(t0,t1,t2,t3);}

C_noret_decl(trf_11243)
static void C_ccall trf_11243(C_word c,C_word *av) C_noret;
static void C_ccall trf_11243(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11243(t0,t1,t2);}

C_noret_decl(trf_11292)
static void C_ccall trf_11292(C_word c,C_word *av) C_noret;
static void C_ccall trf_11292(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11292(t0,t1,t2);}

C_noret_decl(trf_11304)
static void C_ccall trf_11304(C_word c,C_word *av) C_noret;
static void C_ccall trf_11304(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11304(t0,t1,t2);}

C_noret_decl(trf_11351)
static void C_ccall trf_11351(C_word c,C_word *av) C_noret;
static void C_ccall trf_11351(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_11351(t0,t1,t2,t3);}

C_noret_decl(trf_11402)
static void C_ccall trf_11402(C_word c,C_word *av) C_noret;
static void C_ccall trf_11402(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11402(t0,t1,t2);}

C_noret_decl(trf_11562)
static void C_ccall trf_11562(C_word c,C_word *av) C_noret;
static void C_ccall trf_11562(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_11562(t0,t1,t2,t3);}

C_noret_decl(trf_11604)
static void C_ccall trf_11604(C_word c,C_word *av) C_noret;
static void C_ccall trf_11604(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11604(t0,t1,t2);}

C_noret_decl(trf_11642)
static void C_ccall trf_11642(C_word c,C_word *av) C_noret;
static void C_ccall trf_11642(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11642(t0,t1,t2);}

C_noret_decl(trf_11749)
static void C_ccall trf_11749(C_word c,C_word *av) C_noret;
static void C_ccall trf_11749(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11749(t0,t1,t2);}

C_noret_decl(trf_11795)
static void C_ccall trf_11795(C_word c,C_word *av) C_noret;
static void C_ccall trf_11795(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11795(t0,t1,t2);}

C_noret_decl(trf_11835)
static void C_ccall trf_11835(C_word c,C_word *av) C_noret;
static void C_ccall trf_11835(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11835(t0,t1,t2);}

C_noret_decl(trf_11866)
static void C_ccall trf_11866(C_word c,C_word *av) C_noret;
static void C_ccall trf_11866(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11866(t0,t1,t2);}

C_noret_decl(trf_11903)
static void C_ccall trf_11903(C_word c,C_word *av) C_noret;
static void C_ccall trf_11903(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11903(t0,t1,t2);}

C_noret_decl(trf_11982)
static void C_ccall trf_11982(C_word c,C_word *av) C_noret;
static void C_ccall trf_11982(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11982(t0,t1,t2);}

C_noret_decl(trf_12043)
static void C_ccall trf_12043(C_word c,C_word *av) C_noret;
static void C_ccall trf_12043(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_12043(t0,t1,t2);}

C_noret_decl(trf_12098)
static void C_ccall trf_12098(C_word c,C_word *av) C_noret;
static void C_ccall trf_12098(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_12098(t0,t1);}

C_noret_decl(trf_12135)
static void C_ccall trf_12135(C_word c,C_word *av) C_noret;
static void C_ccall trf_12135(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_12135(t0,t1);}

C_noret_decl(trf_12239)
static void C_ccall trf_12239(C_word c,C_word *av) C_noret;
static void C_ccall trf_12239(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_12239(t0,t1);}

C_noret_decl(trf_12286)
static void C_ccall trf_12286(C_word c,C_word *av) C_noret;
static void C_ccall trf_12286(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_12286(t0,t1,t2,t3);}

C_noret_decl(trf_12320)
static void C_ccall trf_12320(C_word c,C_word *av) C_noret;
static void C_ccall trf_12320(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_12320(t0,t1,t2,t3);}

C_noret_decl(trf_12367)
static void C_ccall trf_12367(C_word c,C_word *av) C_noret;
static void C_ccall trf_12367(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_12367(t0,t1,t2,t3);}

C_noret_decl(trf_12407)
static void C_ccall trf_12407(C_word c,C_word *av) C_noret;
static void C_ccall trf_12407(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_12407(t0,t1,t2,t3);}

C_noret_decl(trf_12548)
static void C_ccall trf_12548(C_word c,C_word *av) C_noret;
static void C_ccall trf_12548(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_12548(t0,t1);}

C_noret_decl(trf_12562)
static void C_ccall trf_12562(C_word c,C_word *av) C_noret;
static void C_ccall trf_12562(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_12562(t0,t1,t2);}

C_noret_decl(trf_12764)
static void C_ccall trf_12764(C_word c,C_word *av) C_noret;
static void C_ccall trf_12764(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_12764(t0,t1);}

C_noret_decl(trf_12805)
static void C_ccall trf_12805(C_word c,C_word *av) C_noret;
static void C_ccall trf_12805(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_12805(t0,t1);}

C_noret_decl(trf_13077)
static void C_ccall trf_13077(C_word c,C_word *av) C_noret;
static void C_ccall trf_13077(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_13077(t0,t1,t2);}

C_noret_decl(trf_13102)
static void C_ccall trf_13102(C_word c,C_word *av) C_noret;
static void C_ccall trf_13102(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_13102(t0,t1);}

C_noret_decl(trf_13117)
static void C_ccall trf_13117(C_word c,C_word *av) C_noret;
static void C_ccall trf_13117(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_13117(t0,t1);}

C_noret_decl(trf_13200)
static void C_ccall trf_13200(C_word c,C_word *av) C_noret;
static void C_ccall trf_13200(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_13200(t0,t1);}

C_noret_decl(trf_13240)
static void C_ccall trf_13240(C_word c,C_word *av) C_noret;
static void C_ccall trf_13240(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_13240(t0,t1);}

C_noret_decl(trf_13258)
static void C_ccall trf_13258(C_word c,C_word *av) C_noret;
static void C_ccall trf_13258(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_13258(t0,t1);}

C_noret_decl(trf_13282)
static void C_ccall trf_13282(C_word c,C_word *av) C_noret;
static void C_ccall trf_13282(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_13282(t0,t1);}

C_noret_decl(trf_13308)
static void C_ccall trf_13308(C_word c,C_word *av) C_noret;
static void C_ccall trf_13308(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_13308(t0,t1);}

C_noret_decl(trf_13351)
static void C_ccall trf_13351(C_word c,C_word *av) C_noret;
static void C_ccall trf_13351(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_13351(t0,t1);}

C_noret_decl(trf_13395)
static void C_ccall trf_13395(C_word c,C_word *av) C_noret;
static void C_ccall trf_13395(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_13395(t0,t1);}

C_noret_decl(trf_13439)
static void C_ccall trf_13439(C_word c,C_word *av) C_noret;
static void C_ccall trf_13439(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_13439(t0,t1);}

C_noret_decl(trf_13457)
static void C_ccall trf_13457(C_word c,C_word *av) C_noret;
static void C_ccall trf_13457(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_13457(t0,t1);}

C_noret_decl(trf_13484)
static void C_ccall trf_13484(C_word c,C_word *av) C_noret;
static void C_ccall trf_13484(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_13484(t0,t1);}

C_noret_decl(trf_13531)
static void C_ccall trf_13531(C_word c,C_word *av) C_noret;
static void C_ccall trf_13531(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_13531(t0,t1,t2);}

C_noret_decl(trf_13558)
static void C_ccall trf_13558(C_word c,C_word *av) C_noret;
static void C_ccall trf_13558(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_13558(t0,t1);}

C_noret_decl(trf_14141)
static void C_ccall trf_14141(C_word c,C_word *av) C_noret;
static void C_ccall trf_14141(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_14141(t0,t1);}

C_noret_decl(trf_14162)
static void C_ccall trf_14162(C_word c,C_word *av) C_noret;
static void C_ccall trf_14162(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_14162(t0,t1);}

C_noret_decl(trf_14227)
static void C_ccall trf_14227(C_word c,C_word *av) C_noret;
static void C_ccall trf_14227(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_14227(t0,t1,t2);}

C_noret_decl(trf_14255)
static void C_ccall trf_14255(C_word c,C_word *av) C_noret;
static void C_ccall trf_14255(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_14255(t0,t1);}

C_noret_decl(trf_14274)
static void C_ccall trf_14274(C_word c,C_word *av) C_noret;
static void C_ccall trf_14274(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_14274(t0,t1);}

C_noret_decl(trf_14283)
static void C_ccall trf_14283(C_word c,C_word *av) C_noret;
static void C_ccall trf_14283(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_14283(t0,t1);}

C_noret_decl(trf_14295)
static void C_ccall trf_14295(C_word c,C_word *av) C_noret;
static void C_ccall trf_14295(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_14295(t0,t1);}

C_noret_decl(trf_14307)
static void C_ccall trf_14307(C_word c,C_word *av) C_noret;
static void C_ccall trf_14307(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_14307(t0,t1);}

C_noret_decl(trf_14319)
static void C_ccall trf_14319(C_word c,C_word *av) C_noret;
static void C_ccall trf_14319(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_14319(t0,t1);}

C_noret_decl(trf_14329)
static void C_ccall trf_14329(C_word c,C_word *av) C_noret;
static void C_ccall trf_14329(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_14329(t0,t1,t2);}

C_noret_decl(trf_14356)
static void C_ccall trf_14356(C_word c,C_word *av) C_noret;
static void C_ccall trf_14356(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_14356(t0,t1);}

C_noret_decl(trf_14749)
static void C_ccall trf_14749(C_word c,C_word *av) C_noret;
static void C_ccall trf_14749(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_14749(t0,t1);}

C_noret_decl(trf_14761)
static void C_ccall trf_14761(C_word c,C_word *av) C_noret;
static void C_ccall trf_14761(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_14761(t0,t1);}

C_noret_decl(trf_14771)
static void C_ccall trf_14771(C_word c,C_word *av) C_noret;
static void C_ccall trf_14771(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_14771(t0,t1,t2);}

C_noret_decl(trf_14798)
static void C_ccall trf_14798(C_word c,C_word *av) C_noret;
static void C_ccall trf_14798(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_14798(t0,t1);}

C_noret_decl(trf_15297)
static void C_ccall trf_15297(C_word c,C_word *av) C_noret;
static void C_ccall trf_15297(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_15297(t0,t1);}

C_noret_decl(trf_15482)
static void C_ccall trf_15482(C_word c,C_word *av) C_noret;
static void C_ccall trf_15482(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_15482(t0,t1);}

C_noret_decl(trf_15557)
static void C_ccall trf_15557(C_word c,C_word *av) C_noret;
static void C_ccall trf_15557(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_15557(t0,t1);}

C_noret_decl(trf_15644)
static void C_ccall trf_15644(C_word c,C_word *av) C_noret;
static void C_ccall trf_15644(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_15644(t0,t1);}

C_noret_decl(trf_15665)
static void C_ccall trf_15665(C_word c,C_word *av) C_noret;
static void C_ccall trf_15665(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_15665(t0,t1);}

C_noret_decl(trf_15683)
static void C_ccall trf_15683(C_word c,C_word *av) C_noret;
static void C_ccall trf_15683(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_15683(t0,t1);}

C_noret_decl(trf_15705)
static void C_ccall trf_15705(C_word c,C_word *av) C_noret;
static void C_ccall trf_15705(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_15705(t0,t1);}

C_noret_decl(trf_16081)
static void C_ccall trf_16081(C_word c,C_word *av) C_noret;
static void C_ccall trf_16081(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_16081(t0,t1,t2);}

C_noret_decl(trf_16113)
static void C_ccall trf_16113(C_word c,C_word *av) C_noret;
static void C_ccall trf_16113(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_16113(t0,t1);}

C_noret_decl(trf_16121)
static void C_ccall trf_16121(C_word c,C_word *av) C_noret;
static void C_ccall trf_16121(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_16121(t0,t1,t2);}

C_noret_decl(trf_16177)
static void C_ccall trf_16177(C_word c,C_word *av) C_noret;
static void C_ccall trf_16177(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_16177(t0,t1);}

C_noret_decl(trf_16185)
static void C_ccall trf_16185(C_word c,C_word *av) C_noret;
static void C_ccall trf_16185(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_16185(t0,t1,t2);}

C_noret_decl(trf_16233)
static void C_ccall trf_16233(C_word c,C_word *av) C_noret;
static void C_ccall trf_16233(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_16233(t0,t1,t2,t3);}

C_noret_decl(trf_16267)
static void C_ccall trf_16267(C_word c,C_word *av) C_noret;
static void C_ccall trf_16267(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_16267(t0,t1);}

C_noret_decl(trf_16417)
static void C_ccall trf_16417(C_word c,C_word *av) C_noret;
static void C_ccall trf_16417(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_16417(t0,t1,t2,t3);}

C_noret_decl(trf_16419)
static void C_ccall trf_16419(C_word c,C_word *av) C_noret;
static void C_ccall trf_16419(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_16419(t0,t1,t2);}

C_noret_decl(trf_16431)
static void C_ccall trf_16431(C_word c,C_word *av) C_noret;
static void C_ccall trf_16431(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_16431(t0,t1,t2);}

C_noret_decl(trf_16575)
static void C_ccall trf_16575(C_word c,C_word *av) C_noret;
static void C_ccall trf_16575(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_16575(t0,t1);}

C_noret_decl(trf_16620)
static void C_ccall trf_16620(C_word c,C_word *av) C_noret;
static void C_ccall trf_16620(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_16620(t0,t1,t2,t3,t4);}

C_noret_decl(trf_16989)
static void C_ccall trf_16989(C_word c,C_word *av) C_noret;
static void C_ccall trf_16989(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_16989(t0,t1,t2);}

C_noret_decl(trf_17023)
static void C_ccall trf_17023(C_word c,C_word *av) C_noret;
static void C_ccall trf_17023(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_17023(t0,t1,t2);}

C_noret_decl(trf_17213)
static void C_ccall trf_17213(C_word c,C_word *av) C_noret;
static void C_ccall trf_17213(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_17213(t0,t1);}

C_noret_decl(trf_17220)
static void C_ccall trf_17220(C_word c,C_word *av) C_noret;
static void C_ccall trf_17220(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_17220(t0,t1,t2);}

C_noret_decl(trf_17302)
static void C_ccall trf_17302(C_word c,C_word *av) C_noret;
static void C_ccall trf_17302(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_17302(t0,t1,t2,t3);}

C_noret_decl(trf_17354)
static void C_ccall trf_17354(C_word c,C_word *av) C_noret;
static void C_ccall trf_17354(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_17354(t0,t1,t2);}

C_noret_decl(trf_17393)
static void C_ccall trf_17393(C_word c,C_word *av) C_noret;
static void C_ccall trf_17393(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_17393(t0,t1,t2);}

C_noret_decl(trf_17426)
static void C_ccall trf_17426(C_word c,C_word *av) C_noret;
static void C_ccall trf_17426(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_17426(t0,t1,t2);}

C_noret_decl(trf_17532)
static void C_ccall trf_17532(C_word c,C_word *av) C_noret;
static void C_ccall trf_17532(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_17532(t0,t1);}

C_noret_decl(trf_17827)
static void C_ccall trf_17827(C_word c,C_word *av) C_noret;
static void C_ccall trf_17827(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_17827(t0,t1,t2);}

C_noret_decl(trf_5422)
static void C_ccall trf_5422(C_word c,C_word *av) C_noret;
static void C_ccall trf_5422(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5422(t0,t1,t2);}

C_noret_decl(trf_5480)
static void C_ccall trf_5480(C_word c,C_word *av) C_noret;
static void C_ccall trf_5480(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_5480(t0,t1,t2,t3,t4);}

C_noret_decl(trf_5683)
static void C_ccall trf_5683(C_word c,C_word *av) C_noret;
static void C_ccall trf_5683(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5683(t0,t1,t2);}

C_noret_decl(trf_5689)
static void C_ccall trf_5689(C_word c,C_word *av) C_noret;
static void C_ccall trf_5689(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5689(t0,t1,t2);}

C_noret_decl(trf_5717)
static void C_ccall trf_5717(C_word c,C_word *av) C_noret;
static void C_ccall trf_5717(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5717(t0,t1,t2);}

C_noret_decl(trf_5723)
static void C_ccall trf_5723(C_word c,C_word *av) C_noret;
static void C_ccall trf_5723(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5723(t0,t1,t2);}

C_noret_decl(trf_5747)
static void C_ccall trf_5747(C_word c,C_word *av) C_noret;
static void C_ccall trf_5747(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5747(t0,t1,t2);}

C_noret_decl(trf_5753)
static void C_ccall trf_5753(C_word c,C_word *av) C_noret;
static void C_ccall trf_5753(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_5753(t0,t1,t2,t3);}

C_noret_decl(trf_5976)
static void C_ccall trf_5976(C_word c,C_word *av) C_noret;
static void C_ccall trf_5976(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5976(t0,t1,t2);}

C_noret_decl(trf_5984)
static void C_ccall trf_5984(C_word c,C_word *av) C_noret;
static void C_ccall trf_5984(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_5984(t0,t1,t2,t3);}

C_noret_decl(trf_6049)
static void C_ccall trf_6049(C_word c,C_word *av) C_noret;
static void C_ccall trf_6049(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6049(t0,t1,t2);}

C_noret_decl(trf_6083)
static void C_ccall trf_6083(C_word c,C_word *av) C_noret;
static void C_ccall trf_6083(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6083(t0,t1);}

C_noret_decl(trf_6142)
static void C_ccall trf_6142(C_word c,C_word *av) C_noret;
static void C_ccall trf_6142(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6142(t0,t1,t2);}

C_noret_decl(trf_6148)
static void C_ccall trf_6148(C_word c,C_word *av) C_noret;
static void C_ccall trf_6148(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6148(t0,t1,t2,t3);}

C_noret_decl(trf_6331)
static void C_ccall trf_6331(C_word c,C_word *av) C_noret;
static void C_ccall trf_6331(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6331(t0,t1,t2);}

C_noret_decl(trf_6444)
static void C_ccall trf_6444(C_word c,C_word *av) C_noret;
static void C_ccall trf_6444(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6444(t0,t1,t2);}

C_noret_decl(trf_6627)
static void C_ccall trf_6627(C_word c,C_word *av) C_noret;
static void C_ccall trf_6627(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6627(t0,t1,t2);}

C_noret_decl(trf_6651)
static void C_ccall trf_6651(C_word c,C_word *av) C_noret;
static void C_ccall trf_6651(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6651(t0,t1);}

C_noret_decl(trf_6693)
static void C_ccall trf_6693(C_word c,C_word *av) C_noret;
static void C_ccall trf_6693(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6693(t0,t1,t2);}

C_noret_decl(trf_6716)
static void C_ccall trf_6716(C_word c,C_word *av) C_noret;
static void C_ccall trf_6716(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6716(t0,t1,t2);}

C_noret_decl(trf_6771)
static void C_ccall trf_6771(C_word c,C_word *av) C_noret;
static void C_ccall trf_6771(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6771(t0,t1,t2);}

C_noret_decl(trf_6773)
static void C_ccall trf_6773(C_word c,C_word *av) C_noret;
static void C_ccall trf_6773(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6773(t0,t1,t2);}

C_noret_decl(trf_6805)
static void C_ccall trf_6805(C_word c,C_word *av) C_noret;
static void C_ccall trf_6805(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6805(t0,t1,t2);}

C_noret_decl(trf_6885)
static void C_ccall trf_6885(C_word c,C_word *av) C_noret;
static void C_ccall trf_6885(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6885(t0,t1);}

C_noret_decl(trf_6889)
static void C_ccall trf_6889(C_word c,C_word *av) C_noret;
static void C_ccall trf_6889(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6889(t0,t1,t2);}

C_noret_decl(trf_6907)
static void C_ccall trf_6907(C_word c,C_word *av) C_noret;
static void C_ccall trf_6907(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6907(t0,t1,t2);}

C_noret_decl(trf_7079)
static void C_ccall trf_7079(C_word c,C_word *av) C_noret;
static void C_ccall trf_7079(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7079(t0,t1,t2,t3);}

C_noret_decl(trf_7128)
static void C_ccall trf_7128(C_word c,C_word *av) C_noret;
static void C_ccall trf_7128(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7128(t0,t1,t2);}

C_noret_decl(trf_7150)
static void C_ccall trf_7150(C_word c,C_word *av) C_noret;
static void C_ccall trf_7150(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7150(t0,t1);}

C_noret_decl(trf_7157)
static void C_ccall trf_7157(C_word c,C_word *av) C_noret;
static void C_ccall trf_7157(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7157(t0,t1);}

C_noret_decl(trf_7345)
static void C_ccall trf_7345(C_word c,C_word *av) C_noret;
static void C_ccall trf_7345(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7345(t0,t1,t2);}

C_noret_decl(trf_7385)
static void C_ccall trf_7385(C_word c,C_word *av) C_noret;
static void C_ccall trf_7385(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7385(t0,t1,t2,t3);}

C_noret_decl(trf_7391)
static void C_ccall trf_7391(C_word c,C_word *av) C_noret;
static void C_ccall trf_7391(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7391(t0,t1,t2,t3);}

C_noret_decl(trf_7449)
static void C_ccall trf_7449(C_word c,C_word *av) C_noret;
static void C_ccall trf_7449(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_7449(t0,t1,t2,t3,t4);}

C_noret_decl(trf_7576)
static void C_ccall trf_7576(C_word c,C_word *av) C_noret;
static void C_ccall trf_7576(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7576(t0,t1);}

C_noret_decl(trf_7680)
static void C_ccall trf_7680(C_word c,C_word *av) C_noret;
static void C_ccall trf_7680(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7680(t0,t1,t2);}

C_noret_decl(trf_7704)
static void C_ccall trf_7704(C_word c,C_word *av) C_noret;
static void C_ccall trf_7704(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7704(t0,t1);}

C_noret_decl(trf_7795)
static void C_ccall trf_7795(C_word c,C_word *av) C_noret;
static void C_ccall trf_7795(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7795(t0,t1);}

C_noret_decl(trf_7827)
static void C_ccall trf_7827(C_word c,C_word *av) C_noret;
static void C_ccall trf_7827(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7827(t0,t1,t2);}

C_noret_decl(trf_7852)
static void C_ccall trf_7852(C_word c,C_word *av) C_noret;
static void C_ccall trf_7852(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7852(t0,t1,t2);}

C_noret_decl(trf_8022)
static void C_ccall trf_8022(C_word c,C_word *av) C_noret;
static void C_ccall trf_8022(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8022(t0,t1,t2);}

C_noret_decl(trf_8239)
static void C_ccall trf_8239(C_word c,C_word *av) C_noret;
static void C_ccall trf_8239(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8239(t0,t1,t2);}

C_noret_decl(trf_8303)
static void C_ccall trf_8303(C_word c,C_word *av) C_noret;
static void C_ccall trf_8303(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8303(t0,t1,t2);}

C_noret_decl(trf_8446)
static void C_ccall trf_8446(C_word c,C_word *av) C_noret;
static void C_ccall trf_8446(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8446(t0,t1,t2);}

C_noret_decl(trf_8515)
static void C_ccall trf_8515(C_word c,C_word *av) C_noret;
static void C_ccall trf_8515(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8515(t0,t1,t2);}

C_noret_decl(trf_8566)
static void C_ccall trf_8566(C_word c,C_word *av) C_noret;
static void C_ccall trf_8566(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_8566(t0,t1);}

C_noret_decl(trf_8632)
static void C_ccall trf_8632(C_word c,C_word *av) C_noret;
static void C_ccall trf_8632(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8632(t0,t1,t2);}

C_noret_decl(trf_8659)
static void C_ccall trf_8659(C_word c,C_word *av) C_noret;
static void C_ccall trf_8659(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8659(t0,t1,t2);}

C_noret_decl(trf_8785)
static void C_ccall trf_8785(C_word c,C_word *av) C_noret;
static void C_ccall trf_8785(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_8785(t0,t1,t2,t3,t4);}

C_noret_decl(trf_8935)
static void C_ccall trf_8935(C_word c,C_word *av) C_noret;
static void C_ccall trf_8935(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_8935(t0,t1);}

C_noret_decl(trf_8949)
static void C_ccall trf_8949(C_word c,C_word *av) C_noret;
static void C_ccall trf_8949(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8949(t0,t1,t2);}

C_noret_decl(trf_9008)
static void C_ccall trf_9008(C_word c,C_word *av) C_noret;
static void C_ccall trf_9008(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9008(t0,t1);}

C_noret_decl(trf_9036)
static void C_ccall trf_9036(C_word c,C_word *av) C_noret;
static void C_ccall trf_9036(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9036(t0,t1,t2);}

C_noret_decl(trf_9147)
static void C_ccall trf_9147(C_word c,C_word *av) C_noret;
static void C_ccall trf_9147(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9147(t0,t1,t2);}

C_noret_decl(trf_9253)
static void C_ccall trf_9253(C_word c,C_word *av) C_noret;
static void C_ccall trf_9253(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9253(t0,t1);}

C_noret_decl(trf_9284)
static void C_ccall trf_9284(C_word c,C_word *av) C_noret;
static void C_ccall trf_9284(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9284(t0,t1,t2);}

C_noret_decl(trf_9347)
static void C_ccall trf_9347(C_word c,C_word *av) C_noret;
static void C_ccall trf_9347(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9347(t0,t1,t2);}

C_noret_decl(trf_9415)
static void C_ccall trf_9415(C_word c,C_word *av) C_noret;
static void C_ccall trf_9415(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9415(t0,t1,t2);}

C_noret_decl(trf_9451)
static void C_ccall trf_9451(C_word c,C_word *av) C_noret;
static void C_ccall trf_9451(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9451(t0,t1);}

C_noret_decl(trf_9587)
static void C_ccall trf_9587(C_word c,C_word *av) C_noret;
static void C_ccall trf_9587(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9587(t0,t1,t2);}

C_noret_decl(trf_9645)
static void C_ccall trf_9645(C_word c,C_word *av) C_noret;
static void C_ccall trf_9645(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9645(t0,t1,t2);}

C_noret_decl(trf_9679)
static void C_ccall trf_9679(C_word c,C_word *av) C_noret;
static void C_ccall trf_9679(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9679(t0,t1);}

C_noret_decl(trf_9694)
static void C_ccall trf_9694(C_word c,C_word *av) C_noret;
static void C_ccall trf_9694(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9694(t0,t1,t2);}

C_noret_decl(trf_9751)
static void C_ccall trf_9751(C_word c,C_word *av) C_noret;
static void C_ccall trf_9751(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9751(t0,t1,t2);}

C_noret_decl(trf_9871)
static void C_ccall trf_9871(C_word c,C_word *av) C_noret;
static void C_ccall trf_9871(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_9871(t0,t1,t2,t3);}

C_noret_decl(trf_9919)
static void C_ccall trf_9919(C_word c,C_word *av) C_noret;
static void C_ccall trf_9919(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9919(t0,t1,t2);}

/* f19580 in chicken.compiler.support#print-version in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f19580(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f19580,c,av);}
/* support.scm:1758: chicken.base#print */
t2=*((C_word*)lf[229]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k10028 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10030(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_10030,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10034,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t3=C_i_cdr(((C_word*)t0)[3]);
t4=C_i_cdr(((C_word*)t0)[4]);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10044,a[2]=((C_word*)t0)[5],a[3]=t6,tmp=(C_word)a,a+=4,tmp));
t8=((C_word*)t6)[1];
f_10044(t8,t2,t3,t4);}

/* k10032 in k10028 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10034(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_10034,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[197],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* loop in k10028 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_10044(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_10044,4,t0,t1,t2,t3);}
a=C_alloc(7);
if(C_truep(C_i_nullp(t2))){
if(C_truep(C_i_nullp(t3))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10068,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* support.scm:617: walk */
t5=((C_word*)((C_word*)t0)[2])[1];
f_9645(t5,t4,C_i_car(t3));}}
else{
t4=C_i_car(t2);
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10095,a[2]=t4,a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=t2,a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* support.scm:618: walk */
t6=((C_word*)((C_word*)t0)[2])[1];
f_9645(t6,t5,C_i_car(t3));}}

/* k10066 in loop in k10028 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10068(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_10068,c,av);}
a=C_alloc(9);
t2=C_a_i_list(&a,2,lf[168],t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,1,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k10081 in k10093 in loop in k10028 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10083(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_10083,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k10093 in loop in k10028 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10095(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_10095,c,av);}
a=C_alloc(10);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10083,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* support.scm:619: loop */
t4=((C_word*)((C_word*)t0)[4])[1];
f_10044(t4,t3,C_u_i_cdr(((C_word*)t0)[5]),C_u_i_cdr(((C_word*)t0)[6]));}

/* map-loop1891 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_10120(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_10120,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10145,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:621: g1897 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_9645(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10143 in map-loop1891 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10145(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_10145,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_10120(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k10173 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10175(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_10175,c,av);}
a=C_alloc(6);
/* support.scm:622: cons* */
f_5747(((C_word*)t0)[2],lf[189],C_a_i_list(&a,2,((C_word*)t0)[3],t1));}

/* map-loop1917 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_10177(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_10177,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10202,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:622: g1923 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_9645(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10200 in map-loop1917 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10202(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_10202,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_10177(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* loop in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_10233(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_10233,5,t0,t1,t2,t3,t4);}
a=C_alloc(36);
if(C_truep(C_i_zerop(t2))){
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10245,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* support.scm:627: scheme#reverse */
t6=*((C_word*)lf[80]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t5=C_s_a_i_minus(&a,2,t2,C_fix(1));
t6=C_i_cdr(t3);
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10272,a[2]=t4,a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t5,a[6]=t6,tmp=(C_word)a,a+=7,tmp);
/* support.scm:628: walk */
t8=((C_word*)((C_word*)t0)[2])[1];
f_9645(t8,t7,C_u_i_car(t3));}}

/* k10243 in loop in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10245(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_10245,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10249,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* support.scm:627: walk */
t3=((C_word*)((C_word*)t0)[3])[1];
f_9645(t3,t2,C_i_car(((C_word*)t0)[4]));}

/* k10247 in k10243 in loop in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10249(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_10249,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[198],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k10270 in loop in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10272(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_10272,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
/* support.scm:628: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_10233(t3,((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],t2);}

/* k10281 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_10283(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,0,3)))){
C_save_and_reclaim_args((void *)trf_10283,2,t0,t1);}
a=C_alloc(21);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10290,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* support.scm:630: walk */
t3=((C_word*)((C_word*)t0)[6])[1];
f_9645(t3,t2,C_i_car(((C_word*)t0)[2]));}
else{
t2=C_eqp(((C_word*)t0)[4],lf[177]);
if(C_truep(t2)){
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_i_check_list_2(((C_word*)t0)[2],lf[125]);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10358,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10360,a[2]=t5,a[3]=t10,a[4]=((C_word*)t0)[6],a[5]=t6,tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_10360(t12,t8,((C_word*)t0)[2]);}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10399,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=C_i_check_list_2(((C_word*)t0)[2],lf[125]);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10409,a[2]=t3,a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10411,a[2]=t6,a[3]=t11,a[4]=((C_word*)t0)[6],a[5]=t7,tmp=(C_word)a,a+=6,tmp));
t13=((C_word*)t11)[1];
f_10411(t13,t9,((C_word*)t0)[2]);}}}

/* k10288 in k10281 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10290(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,3)))){
C_save_and_reclaim((void *)f_10290,c,av);}
a=C_alloc(19);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_u_i_cdr(((C_word*)t0)[2]);
t7=C_i_check_list_2(t6,lf[125]);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10301,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t1,a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10303,a[2]=t4,a[3]=t10,a[4]=((C_word*)t0)[6],a[5]=t5,tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_10303(t12,t8,t6);}

/* k10299 in k10288 in k10281 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10301(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_10301,c,av);}
a=C_alloc(9);
/* support.scm:630: cons* */
f_5747(((C_word*)t0)[2],((C_word*)t0)[3],C_a_i_list(&a,3,((C_word*)t0)[4],((C_word*)t0)[5],t1));}

/* map-loop1960 in k10288 in k10281 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_10303(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_10303,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10328,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:630: g1966 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_9645(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10326 in map-loop1960 in k10288 in k10281 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10328(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_10328,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_10303(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k10356 in k10281 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10358(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_10358,c,av);}
a=C_alloc(6);
/* support.scm:632: cons* */
f_5747(((C_word*)t0)[2],((C_word*)t0)[3],C_a_i_list(&a,2,((C_word*)t0)[4],t1));}

/* map-loop1986 in k10281 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_10360(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_10360,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10385,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:632: g1992 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_9645(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10383 in map-loop1986 in k10281 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10385(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_10385,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_10360(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k10397 in k10281 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10399(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_10399,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k10407 in k10281 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10409(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_10409,c,av);}
/* support.scm:633: scheme#append */
t2=*((C_word*)lf[58]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop2012 in k10281 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_10411(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_10411,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10436,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:633: g2018 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_9645(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10434 in map-loop2012 in k10281 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10436(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_10436,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_10411(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* chicken.compiler.support#fold-boolean in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10509(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_10509,c,av);}
a=C_alloc(6);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10515,a[2]=t2,a[3]=t5,tmp=(C_word)a,a+=4,tmp));
t7=((C_word*)t5)[1];
f_10515(t7,t1,t3);}

/* fold in chicken.compiler.support#fold-boolean in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_10515(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_10515,3,t0,t1,t2);}
a=C_alloc(5);
t3=C_i_cddr(t2);
if(C_truep(C_i_nullp(t3))){{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=t2;
C_apply(4,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10541,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:641: proc */
t5=((C_word*)t0)[2];{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=C_i_car(t2);
av2[3]=C_i_cadr(t2);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}

/* k10539 in fold in chicken.compiler.support#fold-boolean in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10541(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_10541,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10545,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* support.scm:642: fold */
t3=((C_word*)((C_word*)t0)[3])[1];
f_10515(t3,t2,C_u_i_cdr(((C_word*)t0)[4]));}

/* k10543 in k10539 in fold in chicken.compiler.support#fold-boolean in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10545(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,1)))){
C_save_and_reclaim((void *)f_10545,c,av);}
a=C_alloc(11);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[172],lf[206],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10561(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6=av[6];
C_word t7=av[7];
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,5)))){
C_save_and_reclaim((void *)f_10561,c,av);}
a=C_alloc(7);
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10567,a[2]=t3,a[3]=t6,a[4]=t5,a[5]=t7,a[6]=t4,tmp=(C_word)a,a+=7,tmp);
/* support.scm:646: ##sys#decompose-lambda-list */
t9=*((C_word*)lf[225]+1);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t1;
av2[2]=t2;
av2[3]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(4,av2);}}

/* a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10567(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_10567,c,av);}
a=C_alloc(13);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10573,a[2]=t3,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_10579,a[2]=t4,a[3]=((C_word*)t0)[3],a[4]=t3,a[5]=((C_word*)t0)[4],a[6]=t2,a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[6],tmp=(C_word)a,a+=9,tmp);
/* support.scm:649: ##sys#call-with-values */{
C_word *av2=av;
av2[0]=0;
av2[1]=t1;
av2[2]=t5;
av2[3]=t6;
C_call_with_values(4,av2);}}

/* a10572 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10573(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_10573,c,av);}
a=C_alloc(5);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5480,a[2]=t3,tmp=(C_word)a,a+=3,tmp));
t5=((C_word*)t3)[1];
f_5480(t5,t1,((C_word*)t0)[2],C_SCHEME_END_OF_LIST,((C_word*)t0)[3]);}

/* a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10579(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_10579,c,av);}
a=C_alloc(24);
t4=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_10583,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=t1,a[6]=t2,a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[6],a[10]=((C_word*)t0)[7],a[11]=((C_word*)t0)[8],tmp=(C_word)a,a+=12,tmp);
if(C_truep(((C_word*)t0)[5])){
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=C_i_check_list_2(((C_word*)t0)[6],lf[125]);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10857,a[2]=t7,a[3]=t11,a[4]=t8,tmp=(C_word)a,a+=5,tmp));
t13=((C_word*)t11)[1];
f_10857(t13,t4,((C_word*)t0)[6]);}
else{
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=((C_word*)t0)[6];
f_10583(2,av2);}}}

/* k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10583(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,4)))){
C_save_and_reclaim((void *)f_10583,c,av);}
a=C_alloc(27);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_10586,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
if(C_truep(((C_word*)t0)[8])){
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_i_check_list_2(((C_word*)t0)[9],lf[125]);
t8=C_i_check_list_2(t1,lf[125]);
t9=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10904,a[2]=((C_word*)t0)[10],a[3]=((C_word*)t0)[4],a[4]=t2,a[5]=((C_word*)t0)[11],tmp=(C_word)a,a+=6,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11351,a[2]=t5,a[3]=t11,a[4]=t6,tmp=(C_word)a,a+=5,tmp));
t13=((C_word*)t11)[1];
f_11351(t13,t9,((C_word*)t0)[9],t1);}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[11];
f_10586(2,av2);}}}

/* k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10586(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,3)))){
C_save_and_reclaim((void *)f_10586,c,av);}
a=C_alloc(22);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(((C_word*)t0)[2],lf[125]);
t7=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_10600,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10811,a[2]=t4,a[3]=t9,a[4]=t5,tmp=(C_word)a,a+=5,tmp));
t11=((C_word*)t9)[1];
f_10811(t11,t7,((C_word*)t0)[2]);}

/* k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10600(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,2)))){
C_save_and_reclaim((void *)f_10600,c,av);}
a=C_alloc(18);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_10603,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10809,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[6],a[5]=t2,a[6]=((C_word*)t0)[3],tmp=(C_word)a,a+=7,tmp);
/* support.scm:655: last */
f_6083(t3,((C_word*)t0)[5]);}

/* k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10603(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,4)))){
C_save_and_reclaim((void *)f_10603,c,av);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_10606,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
if(C_truep(((C_word*)t0)[2])){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10760,a[2]=((C_word*)t0)[6],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10770,a[2]=t2,a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[2],a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* support.scm:663: db-get-list */
t5=*((C_word*)lf[134]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[6];
av2[3]=((C_word*)t0)[2];
av2[4]=lf[212];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_10606(2,av2);}}}

/* k10604 in k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10606(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_10606,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_10613,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
/* support.scm:667: take */
f_5422(t2,((C_word*)t0)[5],((C_word*)t0)[10]);}

/* k10611 in k10604 in k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_10613(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_10613,c,av);}
a=C_alloc(11);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_10615,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t3,tmp=(C_word)a,a+=9,tmp));
t5=((C_word*)t3)[1];
f_10615(t5,((C_word*)t0)[8],t1,((C_word*)t0)[9]);}

/* loop in k10611 in k10604 in k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_fcall f_10615(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(10,0,4)))){
C_save_and_reclaim_args((void *)trf_10615,4,t0,t1,t2,t3);}
a=C_alloc(10);
if(C_truep(C_i_nullp(t2))){
if(C_truep(((C_word*)t0)[2])){
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_10630,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[2],a[7]=t5,tmp=(C_word)a,a+=8,tmp));
t7=((C_word*)t5)[1];
f_10630(t7,t1,((C_word*)t0)[4],((C_word*)t0)[7]);}
else{
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}
else{
t4=C_i_car(t2);
t5=C_a_i_list1(&a,1,t4);
t6=C_i_car(t3);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10751,a[2]=t6,a[3]=t1,a[4]=t5,tmp=(C_word)a,a+=5,tmp);
/* support.scm:693: loop */
t9=t7;
t10=C_u_i_cdr(t2);
t11=C_u_i_cdr(t3);
t1=t9;
t2=t10;
t3=t11;
goto loop;}}

/* loop2 in loop in k10611 in k10604 in k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in ... */
static void C_fcall f_10630(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(8,0,4)))){
C_save_and_reclaim_args((void *)trf_10630,4,t0,t1,t2,t3);}
a=C_alloc(8);
if(C_truep(C_i_nullp(t3))){
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10695,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* support.scm:676: db-get-list */
t5=*((C_word*)lf[134]+1);{
C_word av2[5];
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)t0)[6];
av2[4]=lf[209];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
t4=C_i_car(t3);
t5=C_a_i_list1(&a,1,t4);
t6=C_i_car(t2);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10719,a[2]=t6,a[3]=t1,a[4]=t5,tmp=(C_word)a,a+=5,tmp);
/* support.scm:689: loop2 */
t9=t7;
t10=C_u_i_cdr(t2);
t11=C_u_i_cdr(t3);
t1=t9;
t2=t10;
t3=t11;
goto loop;}}

/* k10661 in k10689 in k10693 in loop2 in loop in k10611 in k10604 in k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in ... */
static void C_ccall f_10663(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,1)))){
C_save_and_reclaim((void *)f_10663,c,av);}
a=C_alloc(11);
t2=C_a_i_list2(&a,2,t1,((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[96],((C_word*)t0)[4],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k10689 in k10693 in loop2 in loop in k10611 in k10604 in k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in ... */
static void C_ccall f_10691(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(35,c,2)))){
C_save_and_reclaim((void *)f_10691,c,av);}
a=C_alloc(35);
t2=C_a_i_list1(&a,1,t1);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10663,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_nullp(((C_word*)t0)[4]))){
/* support.scm:681: qnode */
t4=*((C_word*)lf[155]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t4=C_i_length(((C_word*)t0)[4]);
t5=C_a_i_fixnum_times(&a,2,C_fix(3),t4);
t6=C_a_i_list2(&a,2,lf[208],t5);
t7=C_a_i_record4(&a,4,lf[141],lf[177],t6,((C_word*)t0)[4]);
t8=C_a_i_list2(&a,2,t7,((C_word*)t0)[2]);
t9=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t9;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[96],t2,t8);
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}}

/* k10693 in loop2 in loop in k10611 in k10604 in k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in ... */
static void C_ccall f_10695(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_10695,c,av);}
a=C_alloc(5);
if(C_truep(C_i_nullp(t1))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10691,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:679: last */
f_6083(t2,((C_word*)t0)[5]);}}

/* k10717 in loop2 in loop in k10611 in k10604 in k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in ... */
static void C_ccall f_10719(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,1)))){
C_save_and_reclaim((void *)f_10719,c,av);}
a=C_alloc(11);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[96],((C_word*)t0)[4],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k10749 in loop in k10611 in k10604 in k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in ... */
static void C_ccall f_10751(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,1)))){
C_save_and_reclaim((void *)f_10751,c,av);}
a=C_alloc(11);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[96],((C_word*)t0)[4],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* g2119 in k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_10760(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,5)))){
C_save_and_reclaim_args((void *)trf_10760,3,t0,t1,t2);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10764,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:661: db-put! */
t4=*((C_word*)lf[131]+1);{
C_word av2[6];
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
av2[3]=t2;
av2[4]=lf[211];
av2[5]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(6,av2);}}

/* k10762 in g2119 in k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_10764(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_10764,c,av);}
/* support.scm:662: db-put! */
t2=*((C_word*)lf[131]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=lf[210];
av2[5]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k10768 in k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10770(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_10770,c,av);}
a=C_alloc(11);
t2=C_i_check_list_2(t1,lf[44]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10776,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10784,a[2]=t5,a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp));
t7=((C_word*)t5)[1];
f_10784(t7,t3,t1);}

/* k10774 in k10768 in k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_10776(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_10776,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10779,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:664: db-put! */
t3=*((C_word*)lf[131]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=lf[211];
av2[5]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k10777 in k10774 in k10768 in k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_ccall f_10779(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_10779,c,av);}
/* support.scm:665: db-put! */
t2=*((C_word*)lf[131]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=lf[212];
av2[5]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* for-each-loop2118 in k10768 in k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_fcall f_10784(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_10784,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10794,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:660: g2119 */
t4=((C_word*)t0)[3];
f_10760(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k10792 in for-each-loop2118 in k10768 in k10601 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_ccall f_10794(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10794,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_10784(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k10807 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10809(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_10809,c,av);}
a=C_alloc(9);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11402,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=t3,tmp=(C_word)a,a+=7,tmp));
/* support.scm:779: walk */
t5=((C_word*)t3)[1];
f_11402(t5,((C_word*)t0)[5],((C_word*)t0)[6]);}

/* map-loop2091 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_10811(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_10811,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10836,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:654: chicken.base#gensym */
t4=*((C_word*)lf[97]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[219];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10834 in map-loop2091 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10836(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_10836,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_10811(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* map-loop2063 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_10857(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_10857,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10882,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:650: g2069 */
t4=*((C_word*)lf[97]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10880 in map-loop2063 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10882(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_10882,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_10857(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10904(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_10904,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10912,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t3,tmp=(C_word)a,a+=5,tmp));
/* support.scm:742: walk */
t5=((C_word*)t3)[1];
f_10912(t5,((C_word*)t0)[4],((C_word*)t0)[5],t1);}

/* walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_10912(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(10,0,5)))){
C_save_and_reclaim_args((void *)trf_10912,4,t0,t1,t2,t3);}
a=C_alloc(10);
t4=C_slot(t2,C_fix(3));
t5=C_slot(t2,C_fix(2));
t6=C_slot(t2,C_fix(1));
t7=C_eqp(t6,lf[85]);
if(C_truep(t7)){
t8=t1;{
C_word av2[2];
av2[0]=t8;
av2[1]=C_a_i_record4(&a,4,lf[141],t6,t5,C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t8=C_eqp(t6,lf[154]);
if(C_truep(t8)){
t9=C_i_car(t5);
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10963,a[2]=t1,a[3]=t9,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
t11=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10973,a[2]=((C_word*)t0)[2],a[3]=t10,a[4]=t9,tmp=(C_word)a,a+=5,tmp);
/* support.scm:708: db-get */
t12=*((C_word*)lf[127]+1);{
C_word av2[5];
av2[0]=t12;
av2[1]=t11;
av2[2]=((C_word*)t0)[3];
av2[3]=t9;
av2[4]=lf[222];
((C_proc)(void*)(*((C_word*)t12+1)))(5,av2);}}
else{
t9=C_eqp(t6,lf[124]);
if(C_truep(t9)){
t10=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11010,a[2]=t1,a[3]=((C_word*)t0)[4],a[4]=t4,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t11=C_i_car(t5);
/* support.scm:698: chicken.base#alist-ref */
t12=*((C_word*)lf[220]+1);{
C_word av2[6];
av2[0]=t12;
av2[1]=t10;
av2[2]=t11;
av2[3]=t3;
av2[4]=*((C_word*)lf[221]+1);
av2[5]=t11;
((C_proc)(void*)(*((C_word*)t12+1)))(6,av2);}}
else{
t10=C_eqp(t6,lf[96]);
if(C_truep(t10)){
t11=C_i_car(t5);
t12=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11026,a[2]=t11,a[3]=t3,a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=t4,a[7]=((C_word*)t0)[3],tmp=(C_word)a,a+=8,tmp);
/* support.scm:717: walk */
t15=t12;
t16=C_i_car(t4);
t17=t3;
t1=t15;
t2=t16;
t3=t17;
goto loop;}
else{
t11=C_eqp(t6,lf[118]);
if(C_truep(t11)){
t12=C_i_caddr(t5);
t13=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11078,a[2]=((C_word*)t0)[3],a[3]=t5,a[4]=((C_word*)t0)[4],a[5]=t4,a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* support.scm:725: ##sys#decompose-lambda-list */
t14=*((C_word*)lf[225]+1);{
C_word av2[4];
av2[0]=t14;
av2[1]=t1;
av2[2]=t12;
av2[3]=t13;
((C_proc)(void*)(*((C_word*)t14+1)))(4,av2);}}
else{
t12=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11287,a[2]=((C_word*)t0)[4],a[3]=t3,a[4]=t4,a[5]=t1,a[6]=t6,tmp=(C_word)a,a+=7,tmp);
/* support.scm:740: tree-copy */
t13=*((C_word*)lf[226]+1);{
C_word av2[3];
av2[0]=t13;
av2[1]=t12;
av2[2]=t5;
((C_proc)(void*)(*((C_word*)t13+1)))(3,av2);}}}}}}}

/* k10961 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10963(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_10963,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10970,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:698: chicken.base#alist-ref */
t3=*((C_word*)lf[220]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=*((C_word*)lf[221]+1);
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k10968 in k10961 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10970(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10970,c,av);}
/* support.scm:710: varnode */
t2=*((C_word*)lf[153]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k10971 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_10973(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10973,c,av);}
if(C_truep(t1)){
/* support.scm:709: cfk */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_10963(2,av2);}}}

/* k11000 in k11008 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11002(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,1)))){
C_save_and_reclaim((void *)f_11002,c,av);}
a=C_alloc(8);
t2=C_a_i_list1(&a,1,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[124],((C_word*)t0)[3],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k11008 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11010(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_11010,c,av);}
a=C_alloc(7);
t2=C_a_i_list1(&a,1,t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11002,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* support.scm:714: walk */
t4=((C_word*)((C_word*)t0)[3])[1];
f_10912(t4,t3,C_i_car(((C_word*)t0)[4]),((C_word*)t0)[5]);}

/* k11024 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11026(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_11026,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_11029,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* support.scm:718: chicken.base#gensym */
t3=*((C_word*)lf[97]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11027 in k11024 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11029(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,5)))){
C_save_and_reclaim((void *)f_11029,c,av);}
a=C_alloc(14);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,t2,((C_word*)t0)[3]);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11035,a[2]=t1,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=t3,tmp=(C_word)a,a+=8,tmp);
/* support.scm:720: db-put! */
t5=*((C_word*)lf[131]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[8];
av2[3]=t1;
av2[4]=lf[223];
av2[5]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t5+1)))(6,av2);}}

/* k11033 in k11027 in k11024 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_11035(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_11035,c,av);}
a=C_alloc(8);
t2=C_a_i_list1(&a,1,((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11055,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:723: walk */
t4=((C_word*)((C_word*)t0)[5])[1];
f_10912(t4,t3,C_i_cadr(((C_word*)t0)[6]),((C_word*)t0)[7]);}

/* k11053 in k11033 in k11027 in k11024 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_ccall f_11055(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,1)))){
C_save_and_reclaim((void *)f_11055,c,av);}
a=C_alloc(11);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[96],((C_word*)t0)[4],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11078(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,3)))){
C_save_and_reclaim((void *)f_11078,c,av);}
a=C_alloc(26);
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11083,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t10=C_i_check_list_2(t2,lf[125]);
t11=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_11096,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=t1,a[6]=t3,a[7]=t4,a[8]=((C_word*)t0)[6],a[9]=t2,tmp=(C_word)a,a+=10,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11243,a[2]=t7,a[3]=t13,a[4]=t9,a[5]=t8,tmp=(C_word)a,a+=6,tmp));
t15=((C_word*)t13)[1];
f_11243(t15,t11,t2);}

/* g2263 in a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_11083(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_11083,3,t0,t1,t2);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11087,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:729: chicken.base#gensym */
t4=*((C_word*)lf[97]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k11085 in g2263 in a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_11087(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_11087,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11090,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* support.scm:730: db-put! */
t3=*((C_word*)lf[131]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=lf[223];
av2[5]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k11088 in k11085 in g2263 in a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_ccall f_11090(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11090,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k11094 in a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11096(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(25,c,4)))){
C_save_and_reclaim((void *)f_11096,c,av);}
a=C_alloc(25);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_11099,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_i_check_list_2(t1,lf[125]);
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11193,a[2]=t2,a[3]=((C_word*)t0)[8],tmp=(C_word)a,a+=4,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11195,a[2]=t5,a[3]=t10,a[4]=t6,tmp=(C_word)a,a+=5,tmp));
t12=((C_word*)t10)[1];
f_11195(t12,t8,((C_word*)t0)[9],t1);}

/* k11097 in k11094 in a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_11099(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_11099,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_11164,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* support.scm:736: chicken.base#gensym */
t3=*((C_word*)lf[97]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[224];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* g2330 in k11170 in k11162 in k11097 in k11094 in a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in ... */
static void C_fcall f_11116(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_11116,3,t0,t1,t2);}
/* support.scm:739: g2347 */
t3=((C_word*)((C_word*)t0)[2])[1];
f_10912(t3,t1,t2,((C_word*)t0)[3]);}

/* k11124 in k11170 in k11162 in k11097 in k11094 in a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in ... */
static void C_ccall f_11126(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_11126,c,av);}
a=C_alloc(5);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[118],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop2324 in k11170 in k11162 in k11097 in k11094 in a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in ... */
static void C_fcall f_11128(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_11128,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11153,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:739: g2330 */
t4=((C_word*)t0)[4];
f_11116(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11151 in map-loop2324 in k11170 in k11162 in k11097 in k11094 in a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in ... */
static void C_ccall f_11153(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11153,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_11128(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k11162 in k11097 in k11094 in a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_ccall f_11164(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,5)))){
C_save_and_reclaim((void *)f_11164,c,av);}
a=C_alloc(14);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_11172,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[6],tmp=(C_word)a,a+=9,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11180,a[2]=t3,a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[8],tmp=(C_word)a,a+=5,tmp);
if(C_truep(((C_word*)t0)[9])){
/* support.scm:698: chicken.base#alist-ref */
t5=*((C_word*)lf[220]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[9];
av2[3]=((C_word*)t0)[4];
av2[4]=*((C_word*)lf[221]+1);
av2[5]=((C_word*)t0)[9];
((C_proc)(void*)(*((C_word*)t5+1)))(6,av2);}}
else{
/* support.scm:737: build-lambda-list */
t5=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=((C_word*)t0)[7];
av2[3]=((C_word*)t0)[8];
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}}

/* k11170 in k11162 in k11097 in k11094 in a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in ... */
static void C_ccall f_11172(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,3)))){
C_save_and_reclaim((void *)f_11172,c,av);}
a=C_alloc(33);
t2=C_i_cadddr(((C_word*)t0)[2]);
t3=C_a_i_list4(&a,4,((C_word*)t0)[3],((C_word*)t0)[4],t1,t2);
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11116,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
t9=C_i_check_list_2(((C_word*)t0)[7],lf[125]);
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11126,a[2]=((C_word*)t0)[8],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11128,a[2]=t6,a[3]=t12,a[4]=t8,a[5]=t7,tmp=(C_word)a,a+=6,tmp));
t14=((C_word*)t12)[1];
f_11128(t14,t10,((C_word*)t0)[7]);}

/* k11178 in k11162 in k11097 in k11094 in a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in ... */
static void C_ccall f_11180(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11180,c,av);}
/* support.scm:737: build-lambda-list */
t2=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k11191 in k11094 in a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_11193(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11193,c,av);}
/* support.scm:733: scheme#append */
t2=*((C_word*)lf[58]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop2287 in k11094 in a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_fcall f_11195(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_11195,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_cons(&a,2,t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop2257 in a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_11243(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_11243,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11268,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:728: g2263 */
t4=((C_word*)t0)[4];
f_11083(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11266 in map-loop2257 in a11077 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_11268(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11268,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_11243(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k11285 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11287(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,3)))){
C_save_and_reclaim((void *)f_11287,c,av);}
a=C_alloc(22);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11292,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t7=C_i_check_list_2(((C_word*)t0)[4],lf[125]);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11302,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11304,a[2]=t4,a[3]=t10,a[4]=t6,a[5]=t5,tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_11304(t12,t8,((C_word*)t0)[4]);}

/* g2369 in k11285 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_11292(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_11292,3,t0,t1,t2);}
/* support.scm:741: g2386 */
t3=((C_word*)((C_word*)t0)[2])[1];
f_10912(t3,t1,t2,((C_word*)t0)[3]);}

/* k11300 in k11285 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11302(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_11302,c,av);}
a=C_alloc(5);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_record4(&a,4,lf[141],((C_word*)t0)[3],((C_word*)t0)[4],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop2363 in k11285 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_11304(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_11304,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11329,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:741: g2369 */
t4=((C_word*)t0)[4];
f_11292(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11327 in map-loop2363 in k11285 in walk in k10902 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_11329(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11329,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_11304(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* map-loop2176 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_11351(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_11351,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_cons(&a,2,t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* walk in k10807 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_11402(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,5)))){
C_save_and_reclaim_args((void *)trf_11402,3,t0,t1,t2);}
a=C_alloc(9);
t3=C_slot(t2,C_fix(3));
t4=C_slot(t2,C_fix(2));
t5=C_slot(t2,C_fix(1));
t6=C_eqp(t5,lf[213]);
if(C_truep(t6)){
t7=C_i_car(t4);
t8=C_eqp(((C_word*)t0)[2],t7);
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11446,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t10=C_i_length(((C_word*)t0)[3]);
t11=C_i_cadr(t4);
/* support.scm:754: qnode */
t12=*((C_word*)lf[155]+1);{
C_word av2[3];
av2[0]=t12;
av2[1]=t9;
av2[2]=C_i_less_or_equalp(t10,t11);
((C_proc)(void*)(*((C_word*)t12+1)))(3,av2);}}
else{
t9=t1;{
C_word av2[2];
av2[0]=t9;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}}
else{
t7=C_eqp(t5,lf[215]);
if(C_truep(t7)){
t8=C_i_car(t4);
t9=C_eqp(((C_word*)t0)[2],t8);
if(C_truep(t9)){
t10=C_i_cadr(t4);
t11=C_i_length(((C_word*)t0)[3]);
if(C_truep(C_i_greaterp(t11,t10))){
t12=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11493,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* support.scm:761: varnode */
t13=*((C_word*)lf[153]+1);{
C_word av2[3];
av2[0]=t13;
av2[1]=t12;
av2[2]=C_i_list_ref(((C_word*)t0)[3],t10);
((C_proc)(void*)(*((C_word*)t13+1)))(3,av2);}}
else{
t12=C_a_i_list1(&a,1,lf[216]);
t13=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11521,a[2]=t12,a[3]=t1,a[4]=t2,a[5]=t10,tmp=(C_word)a,a+=6,tmp);
/* support.scm:764: qnode */
t14=*((C_word*)lf[155]+1);{
C_word av2[3];
av2[0]=t14;
av2[1]=t13;
av2[2]=t11;
((C_proc)(void*)(*((C_word*)t14+1)))(3,av2);}}}
else{
t10=t1;{
C_word av2[2];
av2[0]=t10;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}}
else{
t8=C_eqp(t5,lf[217]);
if(C_truep(t8)){
t9=C_i_car(t4);
t10=C_eqp(((C_word*)t0)[2],t9);
if(C_truep(t10)){
t11=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11548,a[2]=t4,a[3]=t2,a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* support.scm:769: collect! */
t12=*((C_word*)lf[133]+1);{
C_word av2[6];
av2[0]=t12;
av2[1]=t11;
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)t0)[2];
av2[4]=lf[209];
av2[5]=t2;
((C_proc)(void*)(*((C_word*)t12+1)))(6,av2);}}
else{
t11=t1;{
C_word av2[2];
av2[0]=t11;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t11+1)))(2,av2);}}}
else{
t9=C_i_check_list_2(t3,lf[44]);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11604,a[2]=t11,a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp));
t13=((C_word*)t11)[1];
f_11604(t13,t1,t3);}}}}

/* k11444 in walk in k10807 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_11446(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11446,c,av);}
/* support.scm:754: copy-node! */
t2=*((C_word*)lf[214]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k11491 in walk in k10807 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_11493(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11493,c,av);}
/* support.scm:761: copy-node! */
t2=*((C_word*)lf[214]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k11519 in walk in k10807 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_11521(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_11521,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11525,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* support.scm:764: qnode */
t3=*((C_word*)lf[155]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11523 in k11519 in walk in k10807 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_ccall f_11525(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_11525,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11529,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* support.scm:764: qnode */
t3=*((C_word*)lf[155]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11527 in k11523 in k11519 in walk in k10807 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in ... */
static void C_ccall f_11529(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_11529,c,av);}
a=C_alloc(14);
t2=C_a_i_list3(&a,3,((C_word*)t0)[2],((C_word*)t0)[3],t1);
t3=C_a_i_record4(&a,4,lf[141],lf[172],((C_word*)t0)[4],t2);
/* support.scm:762: copy-node! */
t4=*((C_word*)lf[214]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[5];
av2[2]=t3;
av2[3]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k11546 in walk in k10807 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_11548(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(34,c,2)))){
C_save_and_reclaim((void *)f_11548,c,av);}
a=C_alloc(34);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_s_a_i_plus(&a,2,t2,C_fix(1));
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11560,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* support.scm:771: varnode */
t5=*((C_word*)lf[153]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k11558 in k11546 in walk in k10807 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_ccall f_11560(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_11560,c,av);}
a=C_alloc(6);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11562,a[2]=((C_word*)t0)[2],a[3]=t3,tmp=(C_word)a,a+=4,tmp));
t5=((C_word*)t3)[1];
f_11562(t5,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* lp in k11558 in k11546 in walk in k10807 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in ... */
static void C_fcall f_11562(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(40,0,3)))){
C_save_and_reclaim_args((void *)trf_11562,4,t0,t1,t2,t3);}
a=C_alloc(40);
if(C_truep(C_i_zerop(t2))){
/* support.scm:773: copy-node! */
t4=*((C_word*)lf[214]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=C_s_a_i_minus(&a,2,t2,C_fix(1));
t5=C_a_i_list1(&a,1,lf[218]);
t6=C_a_i_list1(&a,1,t3);
t7=C_a_i_record4(&a,4,lf[141],lf[172],t5,t6);
/* support.scm:774: lp */
t9=t1;
t10=t4;
t11=t7;
t1=t9;
t2=t10;
t3=t11;
goto loop;}}

/* for-each-loop2446 in walk in k10807 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_fcall f_11604(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_11604,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11614,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:777: g2447 */
t4=((C_word*)((C_word*)t0)[3])[1];
f_11402(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k11612 in for-each-loop2446 in walk in k10807 in k10598 in k10584 in k10581 in a10578 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_ccall f_11614(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11614,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_11604(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* chicken.compiler.support#tree-copy in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11636(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_11636,c,av);}
a=C_alloc(5);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11642,a[2]=t4,tmp=(C_word)a,a+=3,tmp));
t6=((C_word*)t4)[1];
f_11642(t6,t1,t2);}

/* rec in chicken.compiler.support#tree-copy in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_11642(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_11642,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11656,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:785: rec */
t5=t3;
t6=C_u_i_car(t2);
t1=t5;
t2=t6;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11654 in rec in chicken.compiler.support#tree-copy in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11656(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_11656,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11660,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* support.scm:785: rec */
t3=((C_word*)((C_word*)t0)[3])[1];
f_11642(t3,t2,C_u_i_cdr(((C_word*)t0)[4]));}

/* k11658 in k11654 in rec in chicken.compiler.support#tree-copy in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11660(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_11660,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.compiler.support#copy-node in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11666(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_11666,c,av);}
a=C_alloc(5);
t3=C_slot(t2,C_fix(1));
t4=C_slot(t2,C_fix(2));
t5=C_slot(t2,C_fix(3));
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_record4(&a,4,lf[141],t3,t4,t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* chicken.compiler.support#copy-node! in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11704(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_11704,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11708,a[2]=t1,a[3]=t3,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:794: node-class-set! */
t5=*((C_word*)lf[145]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
av2[3]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k11706 in chicken.compiler.support#copy-node! in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11708(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_11708,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11711,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:795: node-parameters-set! */
t3=*((C_word*)lf[149]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=C_slot(((C_word*)t0)[4],C_fix(2));
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k11709 in k11706 in chicken.compiler.support#copy-node! in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11711(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_11711,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11714,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:796: node-subexpressions-set! */
t3=*((C_word*)lf[152]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=C_slot(((C_word*)t0)[4],C_fix(3));
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k11712 in k11709 in k11706 in chicken.compiler.support#copy-node! in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11714(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11714,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* walk in k12164 in k12133 in k12130 in k12190 in k12096 in k12223 in k12075 in a12069 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_fcall f_11749(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,0,3)))){
C_save_and_reclaim_args((void *)trf_11749,3,t0,t1,t2);}
a=C_alloc(18);
t3=C_slot(t2,C_fix(1));
t4=C_slot(t2,C_fix(2));
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=C_slot(t2,C_fix(3));
t10=C_i_check_list_2(t9,lf[125]);
t11=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11793,a[2]=t4,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11795,a[2]=t7,a[3]=t13,a[4]=((C_word*)t0)[2],a[5]=t8,tmp=(C_word)a,a+=6,tmp));
t15=((C_word*)t13)[1];
f_11795(t15,t11,t9);}

/* k11791 in walk in k12164 in k12133 in k12130 in k12190 in k12096 in k12223 in k12075 in a12069 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in ... */
static void C_ccall f_11793(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_11793,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[4],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* map-loop2513 in walk in k12164 in k12133 in k12130 in k12190 in k12096 in k12223 in k12075 in a12069 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in ... */
static void C_fcall f_11795(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_11795,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11820,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:803: g2519 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_11749(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11818 in map-loop2513 in walk in k12164 in k12133 in k12130 in k12190 in k12096 in k12223 in k12075 in a12069 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in ... */
static void C_ccall f_11820(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11820,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_11795(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* walk in k12241 in loop in a12232 in chicken.compiler.support#load-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_11835(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,0,3)))){
C_save_and_reclaim_args((void *)trf_11835,3,t0,t1,t2);}
a=C_alloc(18);
t3=C_i_car(t2);
t4=C_i_cadr(t2);
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=C_u_i_cdr(t2);
t10=C_u_i_cdr(t9);
t11=C_i_check_list_2(t10,lf[125]);
t12=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11864,a[2]=t1,a[3]=t3,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t13=C_SCHEME_UNDEFINED;
t14=(*a=C_VECTOR_TYPE|1,a[1]=t13,tmp=(C_word)a,a+=2,tmp);
t15=C_set_block_item(t14,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11866,a[2]=t7,a[3]=t14,a[4]=((C_word*)t0)[2],a[5]=t8,tmp=(C_word)a,a+=6,tmp));
t16=((C_word*)t14)[1];
f_11866(t16,t12,t10);}

/* k11862 in walk in k12241 in loop in a12232 in chicken.compiler.support#load-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11864(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_11864,c,av);}
a=C_alloc(5);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_record4(&a,4,lf[141],((C_word*)t0)[3],((C_word*)t0)[4],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop2552 in walk in k12241 in loop in a12232 in chicken.compiler.support#load-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_11866(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_11866,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11891,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:807: g2558 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_11835(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11889 in map-loop2552 in walk in k12241 in loop in a12232 in chicken.compiler.support#load-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11891(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11891,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_11866(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11900(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6=av[6];
C_word t7=av[7];
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,4)))){
C_save_and_reclaim((void *)f_11900,c,av);}
a=C_alloc(22);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11903,a[2]=t7,tmp=(C_word)a,a+=3,tmp);
t9=C_SCHEME_END_OF_LIST;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_SCHEME_END_OF_LIST;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11957,a[2]=t1,a[3]=t10,a[4]=t12,a[5]=t3,a[6]=t2,tmp=(C_word)a,a+=7,tmp);
t14=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_12070,a[2]=t10,a[3]=t12,a[4]=t8,a[5]=t6,a[6]=t4,a[7]=t5,tmp=(C_word)a,a+=8,tmp);
/* support.scm:822: chicken.internal#hash-table-for-each */
t15=*((C_word*)lf[139]+1);{
C_word *av2=av;
av2[0]=t15;
av2[1]=t13;
av2[2]=t14;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t15+1)))(4,av2);}}

/* uses-foreign-stubs? in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_11903(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_11903,3,t0,t1,t2);}
a=C_alloc(6);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11909,a[2]=((C_word*)t0)[2],a[3]=t4,tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];{
C_word av2[3];
av2[0]=t6;
av2[1]=t1;
av2[2]=t2;
f_11909(3,av2);}}

/* walk in uses-foreign-stubs? in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11909(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11909,c,av);}
t3=C_slot(t2,C_fix(1));
t4=C_eqp(t3,lf[172]);
if(C_truep(t4)){
t5=C_slot(t2,C_fix(2));
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_i_memq(C_i_car(t5),((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
/* support.scm:819: any */
f_5717(t1,((C_word*)((C_word*)t0)[3])[1],C_slot(t2,C_fix(3)));}}

/* k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11957(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_11957,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11960,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_nullp(((C_word*)((C_word*)t0)[4])[1]))){
/* support.scm:844: chicken.file#delete-file* */
t3=*((C_word*)lf[236]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12020,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
/* support.scm:845: scheme#with-output-to-file */
t4=*((C_word*)lf[243]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* k11958 in k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11960(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_11960,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11966,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_pairp(((C_word*)((C_word*)t0)[3])[1]))){
/* support.scm:856: debugging */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[234];
av2[3]=lf[235];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_11966(2,av2);}}}

/* k11964 in k11958 in k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11966(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_11966,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11974,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=((C_word*)((C_word*)t0)[3])[1];
t4=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7422,tmp=(C_word)a,a+=2,tmp);
/* support.scm:281: chicken.sort#sort */
t5=*((C_word*)lf[233]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=t3;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11972 in k11964 in k11958 in k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11974(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_11974,c,av);}
a=C_alloc(5);
t2=C_i_check_list_2(t1,lf[44]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11982,a[2]=t4,tmp=(C_word)a,a+=3,tmp));
t6=((C_word*)t4)[1];
f_11982(t6,((C_word*)t0)[2],t1);}

/* for-each-loop2678 in k11972 in k11964 in k11958 in k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_11982(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_11982,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11992,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:857: g2694 */
t4=*((C_word*)lf[229]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[230];
av2[3]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k11990 in for-each-loop2678 in k11972 in k11964 in k11958 in k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_11992(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11992,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_11982(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* a12019 in k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12020(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_12020,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12024,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12068,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:847: chicken.platform#chicken-version */
t4=*((C_word*)lf[242]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k12022 in a12019 in k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12024(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_12024,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12035,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:853: scheme#reverse */
t3=*((C_word*)lf[80]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[3])[1];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k12027 in for-each-loop2656 in k12033 in k12022 in a12019 in k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12029(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_12029,c,av);}
/* support.scm:852: scheme#newline */
t2=*((C_word*)lf[23]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k12033 in k12022 in a12019 in k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12035(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_12035,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12038,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12043,a[2]=t4,tmp=(C_word)a,a+=3,tmp));
t6=((C_word*)t4)[1];
f_12043(t6,t2,t1);}

/* k12036 in k12033 in k12022 in a12019 in k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12038(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_12038,c,av);}
/* support.scm:854: chicken.base#print */
t2=*((C_word*)lf[229]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[237];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* for-each-loop2656 in k12033 in k12022 in a12019 in k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_12043(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_12043,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12053,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12029,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* support.scm:851: chicken.pretty-print#pp */
t6=*((C_word*)lf[238]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k12051 in for-each-loop2656 in k12033 in k12022 in a12019 in k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12053(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_12053,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_12043(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k12066 in a12019 in k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12068(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_12068,c,av);}
/* support.scm:847: chicken.base#print */
t2=*((C_word*)lf[229]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[239];
av2[3]=t1;
av2[4]=lf[240];
av2[5]=((C_word*)t0)[3];
av2[6]=lf[241];
((C_proc)(void*)(*((C_word*)t2+1)))(7,av2);}}

/* a12069 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12070(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_12070,c,av);}
a=C_alloc(10);
t4=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_12077,a[2]=t3,a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],a[6]=t1,a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[6],tmp=(C_word)a,a+=10,tmp);
/* support.scm:824: variable-visible? */
t5=*((C_word*)lf[253]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
av2[3]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k12075 in a12069 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12077(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_12077,c,av);}
a=C_alloc(11);
if(C_truep(t1)){
t2=C_i_assq(lf[244],((C_word*)t0)[2]);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_12225,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* tweaks.scm:60: ##sys#get */
t4=*((C_word*)lf[182]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[252];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t3=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k12096 in k12223 in k12075 in a12069 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_12098(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,4)))){
C_save_and_reclaim_args((void *)trf_12098,2,t0,t1);}
a=C_alloc(10);
if(C_truep(t1)){
if(C_truep(C_i_assq(lf[246],((C_word*)t0)[2]))){
t2=C_i_cdr(((C_word*)t0)[3]);
t3=C_slot(t2,C_fix(2));
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_12192,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[8],a[8]=t3,a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
/* support.scm:832: db-get */
t5=*((C_word*)lf[127]+1);{
C_word av2[5];
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[10];
av2[3]=((C_word*)t0)[4];
av2[4]=lf[250];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
t4=((C_word*)t0)[7];{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}
else{
t2=((C_word*)t0)[7];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}
else{
t2=((C_word*)t0)[7];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k12130 in k12190 in k12096 in k12223 in k12075 in a12069 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12132(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_12132,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_12135,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
t3=C_eqp(t1,lf[247]);
if(C_truep(t3)){
t4=t2;
f_12135(t4,C_SCHEME_TRUE);}
else{
t4=C_eqp(t1,lf[248]);
t5=t2;
f_12135(t5,(C_truep(t4)?C_SCHEME_FALSE:C_i_lessp(C_i_cadddr(((C_word*)t0)[8]),((C_word*)t0)[9])));}}

/* k12133 in k12130 in k12190 in k12096 in k12223 in k12075 in a12069 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_12135(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_12135,2,t0,t1);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_12166,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* support.scm:839: uses-foreign-stubs? */
t3=((C_word*)t0)[7];
f_11903(t3,t2,C_u_i_cdr(((C_word*)t0)[6]));}
else{
t2=((C_word*)t0)[5];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k12158 in k12164 in k12133 in k12130 in k12190 in k12096 in k12223 in k12075 in a12069 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_ccall f_12160(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_12160,c,av);}
a=C_alloc(9);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,t2,((C_word*)((C_word*)t0)[3])[1]);
t4=C_mutate(((C_word *)((C_word*)t0)[3])+1,t3);
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k12164 in k12133 in k12130 in k12190 in k12096 in k12223 in k12075 in a12069 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_12166(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_12166,c,av);}
a=C_alloc(13);
if(C_truep(C_i_not(t1))){
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)((C_word*)t0)[3])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[3])+1,t2);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12160,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
t5=C_u_i_cdr(((C_word*)t0)[6]);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11749,a[2]=t7,tmp=(C_word)a,a+=3,tmp));
t9=((C_word*)t7)[1];
f_11749(t9,t4,t5);}
else{
t2=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k12190 in k12096 in k12223 in k12075 in a12069 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12192(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_12192,c,av);}
a=C_alloc(10);
if(C_truep(C_i_not(t1))){
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_12132,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
/* tweaks.scm:60: ##sys#get */
t3=*((C_word*)lf[182]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[249];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t2=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k12223 in k12075 in a12069 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12225(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_12225,c,av);}
a=C_alloc(11);
t2=C_i_structurep(t1,lf[141]);
if(C_truep(C_i_not(t2))){
t3=C_i_assq(lf[245],((C_word*)t0)[2]);
t4=C_i_not(t3);
t5=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_12098,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
if(C_truep(t4)){
t6=t5;
f_12098(t6,t4);}
else{
t6=C_i_cdr(t3);
t7=t5;
f_12098(t7,C_i_not(C_eqp(lf[251],t6)));}}
else{
t3=((C_word*)t0)[7];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.compiler.support#load-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12227(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,3)))){
C_save_and_reclaim((void *)f_12227,c,av);}
a=C_alloc(2);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12233,tmp=(C_word)a,a+=2,tmp);
/* support.scm:861: scheme#with-input-from-file */
t4=*((C_word*)lf[256]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* a12232 in chicken.compiler.support#load-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12233(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_12233,c,av);}
a=C_alloc(5);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12239,a[2]=t3,tmp=(C_word)a,a+=3,tmp));
t5=((C_word*)t3)[1];
f_12239(t5,t1);}

/* loop in a12232 in chicken.compiler.support#load-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_12239(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_12239,2,t0,t1);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12243,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* support.scm:864: scheme#read */
t3=*((C_word*)lf[83]+1);{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k12241 in loop in a12232 in chicken.compiler.support#load-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12243(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_12243,c,av);}
a=C_alloc(13);
if(C_truep(C_eofp(t1))){
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12266,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t3=C_i_car(t1);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12277,a[2]=t2,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t5=C_i_cadr(t1);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11835,a[2]=t7,tmp=(C_word)a,a+=3,tmp));
t9=((C_word*)t7)[1];
f_11835(t9,t4,t5);}}

/* k12264 in k12241 in loop in a12232 in chicken.compiler.support#load-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12266(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_12266,c,av);}
/* support.scm:870: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_12239(t2,((C_word*)t0)[3]);}

/* k12275 in k12241 in loop in a12232 in chicken.compiler.support#load-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12277(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_12277,c,av);}
a=C_alloc(3);
t2=C_a_i_list(&a,1,t1);
if(C_truep(C_i_nullp(t2))){
/* tweaks.scm:57: ##sys#put! */
t3=*((C_word*)lf[255]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[252];
av2[4]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
/* tweaks.scm:57: ##sys#put! */
t3=*((C_word*)lf[255]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[252];
av2[4]=C_i_car(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}

/* chicken.compiler.support#match-node in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12283(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,4)))){
C_save_and_reclaim((void *)f_12283,c,av);}
a=C_alloc(27);
t5=C_SCHEME_END_OF_LIST;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12286,a[2]=t6,a[3]=t4,tmp=(C_word)a,a+=4,tmp));
t14=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12320,a[2]=t8,a[3]=t10,tmp=(C_word)a,a+=4,tmp));
t15=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12367,a[2]=t8,a[3]=t12,a[4]=t10,tmp=(C_word)a,a+=5,tmp));
t16=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12490,a[2]=t6,a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* support.scm:904: matchn */
t17=((C_word*)t12)[1];
f_12367(t17,t16,t2,t3);}

/* resolve in chicken.compiler.support#match-node in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_12286(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_12286,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=C_i_assq(t2,((C_word*)((C_word*)t0)[2])[1]);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12294,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* support.scm:879: g2740 */
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=(
/* support.scm:879: g2740 */
  f_12294(t5,t4)
);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
if(C_truep(C_i_memq(t2,((C_word*)t0)[3]))){
t5=((C_word*)((C_word*)t0)[2])[1];
t6=C_a_i_cons(&a,2,t2,t3);
t7=C_a_i_cons(&a,2,t6,t5);
t8=C_mutate(((C_word *)((C_word*)t0)[2])+1,t7);
t9=t1;{
C_word av2[2];
av2[0]=t9;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}
else{
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_eqp(t2,t3);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}}

/* g2740 in resolve in chicken.compiler.support#match-node in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static C_word C_fcall f_12294(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_equalp(((C_word*)t0)[2],C_i_cdr(t1)));}

/* match1 in chicken.compiler.support#match-node in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_12320(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_12320,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=C_i_pairp(t3);
if(C_truep(C_i_not(t4))){
/* support.scm:886: resolve */
t5=((C_word*)((C_word*)t0)[2])[1];
f_12286(t5,t1,t3,t2);}
else{
t5=C_i_pairp(t2);
if(C_truep(C_i_not(t5))){
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12342,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* support.scm:888: match1 */
t8=t6;
t9=C_i_car(t2);
t10=C_i_car(t3);
t1=t8;
t2=t9;
t3=t10;
goto loop;}}}

/* k12340 in match1 in chicken.compiler.support#match-node in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12342(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_12342,c,av);}
if(C_truep(t1)){
/* support.scm:888: match1 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_12320(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]),C_u_i_cdr(((C_word*)t0)[5]));}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* matchn in chicken.compiler.support#match-node in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_12367(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_12367,4,t0,t1,t2,t3);}
a=C_alloc(7);
t4=C_i_pairp(t3);
if(C_truep(C_i_not(t4))){
/* support.scm:893: resolve */
t5=((C_word*)((C_word*)t0)[2])[1];
f_12286(t5,t1,t3,t2);}
else{
t5=C_slot(t2,C_fix(1));
t6=C_i_car(t3);
t7=C_eqp(t5,t6);
if(C_truep(t7)){
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_12389,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* support.scm:895: match1 */
t9=((C_word*)((C_word*)t0)[4])[1];
f_12320(t9,t8,C_slot(t2,C_fix(2)),C_i_cadr(t3));}
else{
t8=t1;{
C_word av2[2];
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}}

/* k12387 in matchn in chicken.compiler.support#match-node in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12389(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_12389,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=C_slot(((C_word*)t0)[2],C_fix(3));
t3=C_i_cddr(((C_word*)t0)[3]);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12407,a[2]=((C_word*)t0)[4],a[3]=t5,a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_12407(t7,((C_word*)t0)[6],t2,t3);}
else{
t2=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* loop in k12387 in matchn in chicken.compiler.support#match-node in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_12407(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_12407,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_i_nullp(t3))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_i_nullp(t2);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_i_pairp(t3);
if(C_truep(C_i_not(t4))){
/* support.scm:899: resolve */
t5=((C_word*)((C_word*)t0)[2])[1];
f_12286(t5,t1,t3,t2);}
else{
if(C_truep(C_i_nullp(t2))){
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12438,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* support.scm:901: matchn */
t6=((C_word*)((C_word*)t0)[4])[1];
f_12367(t6,t5,C_i_car(t2),C_i_car(t3));}}}}

/* k12436 in loop in k12387 in matchn in chicken.compiler.support#match-node in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12438(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_12438,c,av);}
if(C_truep(t1)){
/* support.scm:902: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_12407(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]),C_u_i_cdr(((C_word*)t0)[5]));}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k12488 in chicken.compiler.support#match-node in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12490(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,6)))){
C_save_and_reclaim((void *)f_12490,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12496,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:907: debugging */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[258];
av2[3]=lf[259];
av2[4]=C_slot(((C_word*)t0)[4],C_fix(1));
av2[5]=C_slot(((C_word*)t0)[4],C_fix(2));
av2[6]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k12494 in k12488 in chicken.compiler.support#match-node in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12496(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_12496,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.compiler.support#expression-has-side-effects? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12516(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_12516,c,av);}
a=C_alloc(5);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12522,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t7=((C_word*)t5)[1];{
C_word *av2=av;
av2[0]=t7;
av2[1]=t1;
av2[2]=t2;
f_12522(3,av2);}}

/* walk in chicken.compiler.support#expression-has-side-effects? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12522(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_12522,c,av);}
a=C_alloc(7);
t3=C_slot(t2,C_fix(3));
t4=C_slot(t2,C_fix(1));
t5=C_eqp(t4,lf[154]);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_12548,a[2]=t1,a[3]=t4,a[4]=t2,a[5]=((C_word*)t0)[2],a[6]=t3,tmp=(C_word)a,a+=7,tmp);
if(C_truep(t5)){
t7=t6;
f_12548(t7,t5);}
else{
t7=C_eqp(t4,lf[85]);
if(C_truep(t7)){
t8=t6;
f_12548(t8,t7);}
else{
t8=C_eqp(t4,lf[159]);
t9=t6;
f_12548(t9,(C_truep(t8)?t8:C_eqp(t4,lf[174])));}}}

/* k12546 in walk in chicken.compiler.support#expression-has-side-effects? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_12548(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_12548,2,t0,t1);}
a=C_alloc(9);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[3],lf[118]);
if(C_truep(t2)){
t3=C_slot(((C_word*)t0)[4],C_fix(2));
t4=C_i_car(t3);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12562,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
t6=*((C_word*)lf[262]+1);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6444,a[2]=t8,a[3]=t5,tmp=(C_word)a,a+=4,tmp));
t10=((C_word*)t8)[1];
f_6444(t10,((C_word*)t0)[2],*((C_word*)lf[262]+1));}
else{
t3=C_eqp(((C_word*)t0)[3],lf[158]);
if(C_truep(t3)){
if(C_truep(t3)){
/* support.scm:923: any */
f_5717(((C_word*)t0)[2],((C_word*)((C_word*)t0)[5])[1],((C_word*)t0)[6]);}
else{
t4=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}
else{
t4=C_eqp(((C_word*)t0)[3],lf[96]);
if(C_truep(t4)){
/* support.scm:923: any */
f_5717(((C_word*)t0)[2],((C_word*)((C_word*)t0)[5])[1],((C_word*)t0)[6]);}
else{
t5=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t5;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}}}}

/* a12561 in k12546 in walk in chicken.compiler.support#expression-has-side-effects? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_12562(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_12562,3,t0,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12570,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* support.scm:921: foreign-callback-stub-id */
t4=*((C_word*)lf[261]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k12568 in a12561 in k12546 in walk in chicken.compiler.support#expression-has-side-effects? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12570(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_12570,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.compiler.support#simple-lambda-node? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12625(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_12625,c,av);}
a=C_alloc(6);
t3=C_slot(t2,C_fix(2));
t4=C_i_caddr(t3);
t5=C_i_pairp(t4);
t6=(C_truep(t5)?C_i_car(t4):C_SCHEME_FALSE);
if(C_truep(t6)){
if(C_truep(C_i_cadr(t3))){
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12654,a[2]=t6,a[3]=t8,tmp=(C_word)a,a+=4,tmp));
t10=((C_word*)t8)[1];{
C_word *av2=av;
av2[0]=t10;
av2[1]=t1;
av2[2]=t2;
f_12654(3,av2);}}
else{
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}
else{
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* rec in chicken.compiler.support#simple-lambda-node? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12654(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_12654,c,av);}
t3=C_slot(t2,C_fix(1));
t4=C_eqp(t3,lf[179]);
if(C_truep(t4)){
t5=C_slot(t2,C_fix(3));
t6=C_i_car(t5);
t7=C_slot(t6,C_fix(1));
t8=C_eqp(lf[154],t7);
if(C_truep(t8)){
t9=C_slot(t6,C_fix(2));
t10=C_i_car(t9);
t11=C_eqp(((C_word*)t0)[2],t10);
if(C_truep(t11)){
/* support.scm:939: every */
f_5683(t1,((C_word*)((C_word*)t0)[3])[1],C_i_cdr(t5));}
else{
t12=t1;{
C_word *av2=av;
av2[0]=t12;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t12+1)))(2,av2);}}}
else{
t9=t1;{
C_word *av2=av;
av2[0]=t9;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}}
else{
t5=C_eqp(t3,lf[189]);
if(C_truep(t5)){
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
/* support.scm:941: every */
f_5683(t1,((C_word*)((C_word*)t0)[3])[1],C_slot(t2,C_fix(3)));}}}

/* chicken.compiler.support#dump-undefined-globals in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12751(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,4)))){
C_save_and_reclaim((void *)f_12751,c,av);}
a=C_alloc(2);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12757,tmp=(C_word)a,a+=2,tmp);
/* support.scm:947: chicken.internal#hash-table-for-each */
t4=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* a12756 in chicken.compiler.support#dump-undefined-globals in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12757(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_12757,c,av);}
a=C_alloc(8);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12764,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12790,a[2]=t3,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* support.scm:949: chicken.keyword#keyword? */
t6=*((C_word*)lf[268]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k12762 in a12756 in chicken.compiler.support#dump-undefined-globals in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_12764(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_12764,2,t0,t1);}
a=C_alloc(3);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12767,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:952: scheme#write */
t3=*((C_word*)lf[265]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k12765 in k12762 in a12756 in chicken.compiler.support#dump-undefined-globals in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12767(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_12767,c,av);}
/* support.scm:953: scheme#newline */
t2=*((C_word*)lf[23]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k12788 in a12756 in chicken.compiler.support#dump-undefined-globals in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12790(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_12790,c,av);}
if(C_truep(C_i_not(t1))){
t2=C_i_assq(lf[266],((C_word*)t0)[2]);
t3=((C_word*)t0)[3];
f_12764(t3,(C_truep(t2)?C_i_not(C_i_assq(lf[267],((C_word*)t0)[2])):C_SCHEME_FALSE));}
else{
t2=((C_word*)t0)[3];
f_12764(t2,C_SCHEME_FALSE);}}

/* chicken.compiler.support#dump-defined-globals in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12792(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,4)))){
C_save_and_reclaim((void *)f_12792,c,av);}
a=C_alloc(2);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12798,tmp=(C_word)a,a+=2,tmp);
/* support.scm:957: chicken.internal#hash-table-for-each */
t4=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* a12797 in chicken.compiler.support#dump-defined-globals in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12798(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_12798,c,av);}
a=C_alloc(8);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12805,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12827,a[2]=t3,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* support.scm:959: chicken.keyword#keyword? */
t6=*((C_word*)lf[268]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k12803 in a12797 in chicken.compiler.support#dump-defined-globals in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_12805(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_12805,2,t0,t1);}
a=C_alloc(3);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12808,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:962: scheme#write */
t3=*((C_word*)lf[265]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k12806 in k12803 in a12797 in chicken.compiler.support#dump-defined-globals in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12808(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_12808,c,av);}
/* support.scm:963: scheme#newline */
t2=*((C_word*)lf[23]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k12825 in a12797 in chicken.compiler.support#dump-defined-globals in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12827(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_12827,c,av);}
if(C_truep(C_i_not(t1))){
t2=C_i_assq(lf[266],((C_word*)t0)[2]);
t3=((C_word*)t0)[3];
f_12805(t3,(C_truep(t2)?C_i_assq(lf[267],((C_word*)t0)[2]):C_SCHEME_FALSE));}
else{
t2=((C_word*)t0)[3];
f_12805(t2,C_SCHEME_FALSE);}}

/* chicken.compiler.support#dump-global-refs in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12829(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,4)))){
C_save_and_reclaim((void *)f_12829,c,av);}
a=C_alloc(2);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12835,tmp=(C_word)a,a+=2,tmp);
/* support.scm:967: chicken.internal#hash-table-for-each */
t4=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* a12834 in chicken.compiler.support#dump-global-refs in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12835(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_12835,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12876,a[2]=t3,a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:969: chicken.keyword#keyword? */
t5=*((C_word*)lf[268]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k12846 in k12874 in a12834 in chicken.compiler.support#dump-global-refs in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12848(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_12848,c,av);}
/* support.scm:972: scheme#newline */
t2=*((C_word*)lf[23]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k12874 in a12834 in chicken.compiler.support#dump-global-refs in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12876(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_12876,c,av);}
a=C_alloc(9);
t2=C_i_not(t1);
t3=(C_truep(t2)?C_i_assq(lf[266],((C_word*)t0)[2]):C_SCHEME_FALSE);
if(C_truep(t3)){
t4=C_i_assq(lf[209],((C_word*)t0)[2]);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12848,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
if(C_truep(t4)){
t6=C_i_cdr(t4);
t7=C_i_length(t6);
t8=C_a_i_list2(&a,2,((C_word*)t0)[4],t7);
/* support.scm:971: scheme#write */
t9=*((C_word*)lf[265]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t9;
av2[1]=t5;
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}
else{
t6=C_a_i_list2(&a,2,((C_word*)t0)[4],C_fix(0));
/* support.scm:971: scheme#write */
t7=*((C_word*)lf[265]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}}
else{
t4=C_SCHEME_UNDEFINED;
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* ##sys#toplevel-definition-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12878(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_12878,c,av);}
a=C_alloc(4);
if(C_truep(C_u_i_namespaced_symbolp(t2))){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17697,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1716: variable-hidden? */
t6=*((C_word*)lf[274]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
if(C_truep(C_i_not(t4))){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12899,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* support.scm:983: debugging */
t6=*((C_word*)lf[22]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[190];
av2[3]=lf[276];
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}}

/* k12897 in ##sys#toplevel-definition-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12899(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_12899,c,av);}
/* support.scm:984: hide-variable */
t2=*((C_word*)lf[275]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* chicken.compiler.support#make-foreign-callback-stub in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12906(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6=av[6];
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,1)))){
C_save_and_reclaim((void *)f_12906,c,av);}
a=C_alloc(7);
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_a_i_record6(&a,6,lf[278],t2,t3,t4,t5,t6);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}

/* chicken.compiler.support#foreign-callback-stub? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12912(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_12912,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_structurep(t2,lf[278]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.compiler.support#foreign-callback-stub-id in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12918(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_12918,c,av);}
t3=C_i_check_structure_2(t2,lf[278],lf[280]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_block_ref(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.compiler.support#foreign-callback-stub-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12927(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_12927,c,av);}
t3=C_i_check_structure_2(t2,lf[278],lf[282]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_block_ref(t2,C_fix(2));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.compiler.support#foreign-callback-stub-qualifiers in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12936(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_12936,c,av);}
t3=C_i_check_structure_2(t2,lf[278],lf[284]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_block_ref(t2,C_fix(3));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.compiler.support#foreign-callback-stub-return-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12945(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_12945,c,av);}
t3=C_i_check_structure_2(t2,lf[278],lf[286]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_block_ref(t2,C_fix(4));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.compiler.support#foreign-callback-stub-argument-types in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12954(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_12954,c,av);}
t3=C_i_check_structure_2(t2,lf[278],lf[288]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_block_ref(t2,C_fix(5));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.compiler.support#register-foreign-callback-stub! in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12963(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_12963,c,av);}
a=C_alloc(4);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12989,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[277]+1);
av2[3]=t2;
av2[4]=t3;
C_apply(5,av2);}}

/* k12987 in chicken.compiler.support#register-foreign-callback-stub! in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12989(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_12989,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,*((C_word*)lf[262]+1));
t3=C_mutate((C_word*)lf[262]+1 /* (set! chicken.compiler.support#foreign-callback-stubs ...) */,t2);
t4=C_SCHEME_END_OF_LIST;
if(C_truep(C_i_nullp(t4))){
/* tweaks.scm:57: ##sys#put! */
t5=*((C_word*)lf[255]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[290];
av2[4]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
/* tweaks.scm:57: ##sys#put! */
t5=*((C_word*)lf[255]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[290];
av2[4]=C_i_car(t4);
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}}

/* chicken.compiler.support#clear-foreign-type-table! in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_12992(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_12992,c,av);}
a=C_alloc(3);
if(C_truep(lf[291])){
/* support.scm:1012: scheme#vector-fill! */
t2=*((C_word*)lf[293]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[291];
av2[3]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_13003,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* support.scm:1013: scheme#make-vector */
t3=*((C_word*)lf[294]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_fix(301);
av2[3]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}

/* k13001 in chicken.compiler.support#clear-foreign-type-table! in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_13003(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_13003,c,av);}
t2=C_mutate(&lf[291] /* (set! chicken.compiler.support#foreign-type-table ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.compiler.support#register-foreign-type! in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_13005(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_13005,c,av);}
a=C_alloc(4);
t4=C_rest_nullp(c,4);
t5=(C_truep(t4)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,4,t0));
t6=C_rest_nullp(c,4);
t7=C_rest_nullp(c,5);
t8=(C_truep(t7)?C_SCHEME_FALSE:C_get_rest_arg(c,5,av,4,t0));
t9=C_rest_nullp(c,5);
t10=(C_truep(t8)?t5:C_SCHEME_FALSE);
t11=(C_truep(t5)?C_a_i_vector3(&a,3,t3,t10,t8):C_a_i_vector3(&a,3,t3,t10,C_SCHEME_FALSE));
/* support.scm:1021: chicken.internal#hash-table-set! */
t12=*((C_word*)lf[132]+1);{
C_word av2[5];
av2[0]=t12;
av2[1]=t1;
av2[2]=lf[291];
av2[3]=t2;
av2[4]=t11;
((C_proc)(void*)(*((C_word*)t12+1)))(5,av2);}}

/* chicken.compiler.support#lookup-foreign-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_13059(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_13059,c,av);}
/* support.scm:1029: chicken.internal#hash-table-ref */
t3=*((C_word*)lf[128]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=lf[291];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_13065(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_13065,c,av);}
a=C_alloc(7);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_13071,a[2]=t2,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_14135,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* support.scm:1046: follow-without-loop */
f_7385(t1,t3,t4,t5);}

/* a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_13071(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_13071,c,av);}
a=C_alloc(8);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_13077,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t3,a[5]=t5,tmp=(C_word)a,a+=6,tmp));
t7=((C_word*)t5)[1];
f_13077(t7,t1,t2);}

/* repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_13077(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_13077,3,t0,t1,t2);}
a=C_alloc(8);
t3=C_eqp(t2,lf[299]);
t4=(C_truep(t3)?t3:C_eqp(t2,lf[300]));
if(C_truep(t4)){
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=(C_truep(*((C_word*)lf[10]+1))?((C_word*)t0)[2]:C_a_i_list(&a,2,lf[301],((C_word*)t0)[2]));
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_eqp(t2,lf[302]);
t6=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_13102,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],tmp=(C_word)a,a+=8,tmp);
if(C_truep(t5)){
t7=t6;
f_13102(t7,t5);}
else{
t7=C_eqp(t2,lf[389]);
if(C_truep(t7)){
t8=t6;
f_13102(t8,t7);}
else{
t8=C_eqp(t2,lf[390]);
if(C_truep(t8)){
t9=t6;
f_13102(t9,t8);}
else{
t9=C_eqp(t2,lf[391]);
if(C_truep(t9)){
t10=t6;
f_13102(t10,t9);}
else{
t10=C_eqp(t2,lf[392]);
t11=t6;
f_13102(t11,(C_truep(t10)?t10:C_eqp(t2,lf[393])));}}}}}}

/* k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_13102(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_13102,2,t0,t1);}
a=C_alloc(8);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=(C_truep(*((C_word*)lf[10]+1))?((C_word*)t0)[3]:C_a_i_list(&a,2,lf[303],((C_word*)t0)[3]));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[4],lf[304]);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_13117,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
if(C_truep(t2)){
t4=t3;
f_13117(t4,t2);}
else{
t4=C_eqp(((C_word*)t0)[4],lf[387]);
t5=t3;
f_13117(t5,(C_truep(t4)?t4:C_eqp(((C_word*)t0)[4],lf[388])));}}}

/* k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_13117(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,2)))){
C_save_and_reclaim_args((void *)trf_13117,2,t0,t1);}
a=C_alloc(15);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=(C_truep(*((C_word*)lf[10]+1))?((C_word*)t0)[3]:C_a_i_list(&a,2,lf[305],((C_word*)t0)[3]));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[4],lf[306]);
t3=(C_truep(t2)?t2:C_eqp(((C_word*)t0)[4],lf[307]));
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_13135,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1057: chicken.base#gensym */
t5=*((C_word*)lf[97]+1);{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=C_eqp(((C_word*)t0)[4],lf[312]);
t5=(C_truep(t4)?t4:C_eqp(((C_word*)t0)[4],lf[313]));
if(C_truep(t5)){
t6=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t6;
av2[1]=(C_truep(*((C_word*)lf[10]+1))?((C_word*)t0)[3]:C_a_i_list(&a,2,lf[308],((C_word*)t0)[3]));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_eqp(((C_word*)t0)[4],lf[314]);
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_13185,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1069: chicken.base#gensym */
t8=*((C_word*)lf[97]+1);{
C_word av2[2];
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t7=C_eqp(((C_word*)t0)[4],lf[316]);
if(C_truep(t7)){
if(C_truep(*((C_word*)lf[10]+1))){
t8=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t8;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t8=C_a_i_list(&a,2,lf[309],lf[314]);
t9=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t9;
av2[1]=C_a_i_list(&a,3,lf[315],t8,((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}}
else{
t8=C_eqp(((C_word*)t0)[4],lf[317]);
t9=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_13240,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
if(C_truep(t8)){
t10=t9;
f_13240(t10,t8);}
else{
t10=C_eqp(((C_word*)t0)[4],lf[378]);
if(C_truep(t10)){
t11=t9;
f_13240(t11,t10);}
else{
t11=C_eqp(((C_word*)t0)[4],lf[379]);
if(C_truep(t11)){
t12=t9;
f_13240(t12,t11);}
else{
t12=C_eqp(((C_word*)t0)[4],lf[380]);
if(C_truep(t12)){
t13=t9;
f_13240(t13,t12);}
else{
t13=C_eqp(((C_word*)t0)[4],lf[381]);
if(C_truep(t13)){
t14=t9;
f_13240(t14,t13);}
else{
t14=C_eqp(((C_word*)t0)[4],lf[382]);
if(C_truep(t14)){
t15=t9;
f_13240(t15,t14);}
else{
t15=C_eqp(((C_word*)t0)[4],lf[383]);
if(C_truep(t15)){
t16=t9;
f_13240(t16,t15);}
else{
t16=C_eqp(((C_word*)t0)[4],lf[384]);
if(C_truep(t16)){
t17=t9;
f_13240(t17,t16);}
else{
t17=C_eqp(((C_word*)t0)[4],lf[385]);
t18=t9;
f_13240(t18,(C_truep(t17)?t17:C_eqp(((C_word*)t0)[4],lf[386])));}}}}}}}}}}}}}}

/* k13133 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_13135(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(42,c,1)))){
C_save_and_reclaim((void *)f_13135,c,av);}
a=C_alloc(42);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,1,t2);
t4=(C_truep(*((C_word*)lf[10]+1))?t1:C_a_i_list(&a,2,lf[308],t1));
t5=C_a_i_list(&a,2,lf[309],C_SCHEME_FALSE);
t6=C_a_i_list(&a,4,lf[310],t1,t4,t5);
t7=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_a_i_list(&a,3,lf[311],t3,t6);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}

/* k13183 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_13185(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,2)))){
C_save_and_reclaim((void *)f_13185,c,av);}
a=C_alloc(29);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,1,t2);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_13200,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t3,tmp=(C_word)a,a+=5,tmp);
if(C_truep(*((C_word*)lf[10]+1))){
t5=t4;
f_13200(t5,t1);}
else{
t5=C_a_i_list(&a,2,lf[309],lf[314]);
t6=t4;
f_13200(t6,C_a_i_list(&a,3,lf[315],t5,t1));}}

/* k13198 in k13183 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_13200(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,0,1)))){
C_save_and_reclaim_args((void *)trf_13200,2,t0,t1);}
a=C_alloc(27);
t2=C_a_i_list(&a,2,lf[309],C_SCHEME_FALSE);
t3=C_a_i_list(&a,4,lf[310],((C_word*)t0)[2],t1,t2);
t4=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t4;
av2[1]=C_a_i_list(&a,3,lf[311],((C_word*)t0)[4],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_13240(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_13240,2,t0,t1);}
a=C_alloc(8);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_13243,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:1082: chicken.base#gensym */
t3=*((C_word*)lf[97]+1);{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[4],lf[318]);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_13282,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
if(C_truep(t2)){
t4=t3;
f_13282(t4,t2);}
else{
t4=C_eqp(((C_word*)t0)[4],lf[369]);
if(C_truep(t4)){
t5=t3;
f_13282(t5,t4);}
else{
t5=C_eqp(((C_word*)t0)[4],lf[370]);
if(C_truep(t5)){
t6=t3;
f_13282(t6,t5);}
else{
t6=C_eqp(((C_word*)t0)[4],lf[371]);
if(C_truep(t6)){
t7=t3;
f_13282(t7,t6);}
else{
t7=C_eqp(((C_word*)t0)[4],lf[372]);
if(C_truep(t7)){
t8=t3;
f_13282(t8,t7);}
else{
t8=C_eqp(((C_word*)t0)[4],lf[373]);
if(C_truep(t8)){
t9=t3;
f_13282(t9,t8);}
else{
t9=C_eqp(((C_word*)t0)[4],lf[374]);
if(C_truep(t9)){
t10=t3;
f_13282(t10,t9);}
else{
t10=C_eqp(((C_word*)t0)[4],lf[375]);
if(C_truep(t10)){
t11=t3;
f_13282(t11,t10);}
else{
t11=C_eqp(((C_word*)t0)[4],lf[376]);
t12=t3;
f_13282(t12,(C_truep(t11)?t11:C_eqp(((C_word*)t0)[4],lf[377])));}}}}}}}}}}

/* k13241 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_13243(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,2)))){
C_save_and_reclaim((void *)f_13243,c,av);}
a=C_alloc(29);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,1,t2);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_13258,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t3,tmp=(C_word)a,a+=5,tmp);
if(C_truep(*((C_word*)lf[10]+1))){
t5=t4;
f_13258(t5,t1);}
else{
t5=C_a_i_list(&a,2,lf[309],((C_word*)t0)[4]);
t6=t4;
f_13258(t6,C_a_i_list(&a,3,lf[315],t5,t1));}}

/* k13256 in k13241 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_13258(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,0,1)))){
C_save_and_reclaim_args((void *)trf_13258,2,t0,t1);}
a=C_alloc(27);
t2=C_a_i_list(&a,2,lf[309],C_SCHEME_FALSE);
t3=C_a_i_list(&a,4,lf[310],((C_word*)t0)[2],t1,t2);
t4=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t4;
av2[1]=C_a_i_list(&a,3,lf[311],((C_word*)t0)[4],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_13282(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,2)))){
C_save_and_reclaim_args((void *)trf_13282,2,t0,t1);}
a=C_alloc(15);
if(C_truep(t1)){
if(C_truep(*((C_word*)lf[10]+1))){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_slot(C_u_i_assq(((C_word*)t0)[4],lf[319]),C_fix(1));
t3=C_a_i_list(&a,2,lf[309],t2);
t4=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t4;
av2[1]=C_a_i_list(&a,3,lf[315],t3,((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}
else{
t2=C_eqp(((C_word*)t0)[4],lf[320]);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_13308,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
if(C_truep(t2)){
t4=t3;
f_13308(t4,t2);}
else{
t4=C_eqp(((C_word*)t0)[4],lf[364]);
if(C_truep(t4)){
t5=t3;
f_13308(t5,t4);}
else{
t5=C_eqp(((C_word*)t0)[4],lf[365]);
if(C_truep(t5)){
t6=t3;
f_13308(t6,t5);}
else{
t6=C_eqp(((C_word*)t0)[4],lf[366]);
if(C_truep(t6)){
t7=t3;
f_13308(t7,t6);}
else{
t7=C_eqp(((C_word*)t0)[4],lf[367]);
t8=t3;
f_13308(t8,(C_truep(t7)?t7:C_eqp(((C_word*)t0)[4],lf[368])));}}}}}}

/* k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_13308(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_13308,2,t0,t1);}
a=C_alloc(8);
if(C_truep(t1)){
t2=C_slot(C_u_i_assq(((C_word*)t0)[2],((C_word*)t0)[3]),C_fix(1));
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_13314,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:1101: chicken.base#open-output-string */
t4=*((C_word*)lf[327]+1);{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[2],lf[328]);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_13351,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
if(C_truep(t2)){
t4=t3;
f_13351(t4,t2);}
else{
t4=C_eqp(((C_word*)t0)[2],lf[359]);
if(C_truep(t4)){
t5=t3;
f_13351(t5,t4);}
else{
t5=C_eqp(((C_word*)t0)[2],lf[360]);
if(C_truep(t5)){
t6=t3;
f_13351(t6,t5);}
else{
t6=C_eqp(((C_word*)t0)[2],lf[361]);
if(C_truep(t6)){
t7=t3;
f_13351(t7,t6);}
else{
t7=C_eqp(((C_word*)t0)[2],lf[362]);
t8=t3;
f_13351(t8,(C_truep(t7)?t7:C_eqp(((C_word*)t0)[2],lf[363])));}}}}}}

/* k13312 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_13314(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_13314,c,av);}
a=C_alloc(6);
t2=C_i_check_port_2(t1,C_fix(2),C_SCHEME_TRUE,lf[321]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_13320,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* support.scm:1101: ##sys#print */
t4=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[326];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k13318 in k13312 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_ccall f_13320(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_13320,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_13323,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:1101: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k13321 in k13318 in k13312 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in ... */
static void C_ccall f_13323(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_13323,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_13326,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:1101: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[325];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k13324 in k13321 in k13318 in k13312 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in ... */
static void C_ccall f_13326(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_13326,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_13329,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1101: chicken.base#get-output-string */
t3=*((C_word*)lf[324]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k13327 in k13324 in k13321 in k13318 in k13312 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in ... */
static void C_ccall f_13329(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,1)))){
C_save_and_reclaim((void *)f_13329,c,av);}
a=C_alloc(18);
if(C_truep(*((C_word*)lf[10]+1))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_a_i_list(&a,3,lf[322],t1,lf[302]);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,3,lf[323],((C_word*)t0)[3],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_fcall f_13351(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_13351,2,t0,t1);}
a=C_alloc(7);
if(C_truep(t1)){
t2=C_i_assq(((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_slot(t2,C_fix(1));
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_13357,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* support.scm:1109: chicken.base#open-output-string */
t5=*((C_word*)lf[327]+1);{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[2],lf[332]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_13395,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],tmp=(C_word)a,a+=7,tmp);
if(C_truep(t2)){
t4=t3;
f_13395(t4,t2);}
else{
t4=C_eqp(((C_word*)t0)[2],lf[357]);
t5=t3;
f_13395(t5,(C_truep(t4)?t4:C_eqp(((C_word*)t0)[2],lf[358])));}}}

/* k13355 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_ccall f_13357(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_13357,c,av);}
a=C_alloc(6);
t2=C_i_check_port_2(t1,C_fix(2),C_SCHEME_TRUE,lf[321]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_13363,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* support.scm:1109: ##sys#print */
t4=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[331];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k13361 in k13355 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in ... */
static void C_ccall f_13363(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_13363,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_13366,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:1109: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k13364 in k13361 in k13355 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in ... */
static void C_ccall f_13366(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_13366,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_13369,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:1109: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[330];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k13367 in k13364 in k13361 in k13355 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in ... */
static void C_ccall f_13369(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_13369,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_13372,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1109: chicken.base#get-output-string */
t3=*((C_word*)lf[324]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k13370 in k13367 in k13364 in k13361 in k13355 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in ... */
static void C_ccall f_13372(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,1)))){
C_save_and_reclaim((void *)f_13372,c,av);}
a=C_alloc(18);
if(C_truep(*((C_word*)lf[10]+1))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_a_i_list(&a,3,lf[322],t1,lf[302]);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,3,lf[329],((C_word*)t0)[3],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k13393 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_fcall f_13395(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_13395,2,t0,t1);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_13398,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1115: chicken.base#gensym */
t3=*((C_word*)lf[97]+1);{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[4],lf[334]);
if(C_truep(t2)){
t3=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t3;
av2[1]=C_a_i_list(&a,2,lf[333],((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_eqp(((C_word*)t0)[4],lf[335]);
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_13439,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(t3)){
t5=t4;
f_13439(t5,t3);}
else{
t5=C_eqp(((C_word*)t0)[4],lf[354]);
if(C_truep(t5)){
t6=t4;
f_13439(t6,t5);}
else{
t6=C_eqp(((C_word*)t0)[4],lf[355]);
t7=t4;
f_13439(t7,(C_truep(t6)?t6:C_eqp(((C_word*)t0)[4],lf[356])));}}}}}

/* k13396 in k13393 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in ... */
static void C_ccall f_13398(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(42,c,1)))){
C_save_and_reclaim((void *)f_13398,c,av);}
a=C_alloc(42);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,1,t2);
t4=C_a_i_list(&a,2,lf[333],t1);
t5=C_a_i_list(&a,2,lf[309],C_SCHEME_FALSE);
t6=C_a_i_list(&a,4,lf[310],t1,t4,t5);
t7=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_a_i_list(&a,3,lf[311],t3,t6);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}

/* k13437 in k13393 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in ... */
static void C_fcall f_13439(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_13439,2,t0,t1);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_13442,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1123: chicken.base#gensym */
t3=*((C_word*)lf[97]+1);{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[4],lf[338]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_13484,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(t2)){
t4=t3;
f_13484(t4,t2);}
else{
t4=C_eqp(((C_word*)t0)[4],lf[352]);
t5=t3;
f_13484(t5,(C_truep(t4)?t4:C_eqp(((C_word*)t0)[4],lf[353])));}}}

/* k13440 in k13437 in k13393 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in ... */
static void C_ccall f_13442(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,2)))){
C_save_and_reclaim((void *)f_13442,c,av);}
a=C_alloc(26);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,1,t2);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_13457,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t3,tmp=(C_word)a,a+=5,tmp);
if(C_truep(*((C_word*)lf[10]+1))){
t5=t4;
f_13457(t5,C_a_i_list(&a,2,lf[336],t1));}
else{
t5=C_a_i_list(&a,2,lf[337],t1);
t6=t4;
f_13457(t6,C_a_i_list(&a,2,lf[336],t5));}}

/* k13455 in k13440 in k13437 in k13393 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in ... */
static void C_fcall f_13457(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,0,1)))){
C_save_and_reclaim_args((void *)trf_13457,2,t0,t1);}
a=C_alloc(27);
t2=C_a_i_list(&a,2,lf[309],C_SCHEME_FALSE);
t3=C_a_i_list(&a,4,lf[310],((C_word*)t0)[2],t1,t2);
t4=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t4;
av2[1]=C_a_i_list(&a,3,lf[311],((C_word*)t0)[4],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k13482 in k13437 in k13393 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in ... */
static void C_fcall f_13484(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,0,2)))){
C_save_and_reclaim_args((void *)trf_13484,2,t0,t1);}
a=C_alloc(18);
if(C_truep(t1)){
if(C_truep(*((C_word*)lf[10]+1))){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_a_i_list(&a,2,lf[336],((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_a_i_list(&a,2,lf[337],((C_word*)t0)[3]);
t3=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t3;
av2[1]=C_a_i_list(&a,2,lf[336],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}
else{
t2=C_eqp(((C_word*)t0)[4],lf[339]);
if(C_truep(t2)){
if(C_truep(*((C_word*)lf[10]+1))){
t3=C_a_i_list(&a,2,lf[180],((C_word*)t0)[3]);
t4=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t4;
av2[1]=C_a_i_list(&a,2,lf[336],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=C_a_i_list(&a,2,lf[180],((C_word*)t0)[3]);
t4=C_a_i_list(&a,2,lf[337],t3);
t5=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t5;
av2[1]=C_a_i_list(&a,2,lf[336],t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}
else{
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_13527,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_i_symbolp(((C_word*)t0)[4]))){
/* support.scm:1139: lookup-foreign-type */
t4=*((C_word*)lf[296]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t4=t3;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
f_13527(2,av2);}}}}}

/* k13525 in k13482 in k13437 in k13393 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in ... */
static void C_ccall f_13527(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_13527,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_13531,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:1139: g3227 */
t3=t2;
f_13531(t3,((C_word*)t0)[3],t1);}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[4]))){
t2=C_u_i_car(((C_word*)t0)[4]);
t3=C_eqp(t2,lf[340]);
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_13558,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[4],tmp=(C_word)a,a+=7,tmp);
if(C_truep(t3)){
t5=t4;
f_13558(t5,t3);}
else{
t5=C_eqp(t2,lf[350]);
if(C_truep(t5)){
t6=t4;
f_13558(t6,t5);}
else{
t6=C_eqp(t2,lf[351]);
t7=t4;
f_13558(t7,(C_truep(t6)?t6:C_eqp(t2,lf[332])));}}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}}

/* g3227 in k13525 in k13482 in k13437 in k13393 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in ... */
static void C_fcall f_13531(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_13531,3,t0,t1,t2);}
/* support.scm:1140: next */
t3=((C_word*)t0)[2];{
C_word av2[3];
av2[0]=t3;
av2[1]=t1;
av2[2]=C_i_vector_ref(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k13556 in k13525 in k13482 in k13437 in k13393 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in ... */
static void C_fcall f_13558(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,0,2)))){
C_save_and_reclaim_args((void *)trf_13558,2,t0,t1);}
a=C_alloc(18);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_13561,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1144: chicken.base#gensym */
t3=*((C_word*)lf[97]+1);{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[4],lf[341]);
t3=(C_truep(t2)?t2:C_eqp(((C_word*)t0)[4],lf[342]));
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_13596,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1150: chicken.base#gensym */
t5=*((C_word*)lf[97]+1);{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=C_eqp(((C_word*)t0)[4],lf[307]);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_13632,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1156: chicken.base#gensym */
t6=*((C_word*)lf[97]+1);{
C_word av2[2];
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t5=C_eqp(((C_word*)t0)[4],lf[312]);
if(C_truep(t5)){
t6=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t6;
av2[1]=(C_truep(*((C_word*)lf[10]+1))?((C_word*)t0)[2]:C_a_i_list(&a,2,lf[308],((C_word*)t0)[2]));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_eqp(((C_word*)t0)[4],lf[345]);
if(C_truep(t6)){
t7=C_a_i_list(&a,2,lf[309],lf[343]);
t8=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t8;
av2[1]=C_a_i_list(&a,3,lf[344],((C_word*)t0)[2],t7);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t7=C_eqp(((C_word*)t0)[4],lf[346]);
if(C_truep(t7)){
/* support.scm:1169: repeat */
t8=((C_word*)((C_word*)t0)[5])[1];
f_13077(t8,((C_word*)t0)[3],C_i_cadr(((C_word*)t0)[6]));}
else{
t8=C_eqp(((C_word*)t0)[4],lf[347]);
if(C_truep(t8)){
if(C_truep(*((C_word*)lf[10]+1))){
t9=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t9;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}
else{
t9=C_a_i_list(&a,3,lf[322],lf[348],lf[302]);
t10=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t10;
av2[1]=C_a_i_list(&a,3,lf[323],((C_word*)t0)[2],t9);
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}}
else{
t9=C_eqp(((C_word*)t0)[4],lf[349]);
if(C_truep(t9)){
t10=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t10;
av2[1]=(C_truep(t9)?C_a_i_list(&a,2,lf[333],((C_word*)t0)[2]):((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
t10=C_eqp(((C_word*)t0)[4],lf[334]);
t11=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t11;
av2[1]=(C_truep(t10)?C_a_i_list(&a,2,lf[333],((C_word*)t0)[2]):((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t11+1)))(2,av2);}}}}}}}}}}

/* k13559 in k13556 in k13525 in k13482 in k13437 in k13393 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in ... */
static void C_ccall f_13561(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(42,c,1)))){
C_save_and_reclaim((void *)f_13561,c,av);}
a=C_alloc(42);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,1,t2);
t4=C_a_i_list(&a,2,lf[333],t1);
t5=C_a_i_list(&a,2,lf[309],C_SCHEME_FALSE);
t6=C_a_i_list(&a,4,lf[310],t1,t4,t5);
t7=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_a_i_list(&a,3,lf[311],t3,t6);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}

/* k13594 in k13556 in k13525 in k13482 in k13437 in k13393 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in ... */
static void C_ccall f_13596(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(51,c,1)))){
C_save_and_reclaim((void *)f_13596,c,av);}
a=C_alloc(51);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,1,t2);
t4=C_a_i_list(&a,2,lf[309],lf[343]);
t5=C_a_i_list(&a,3,lf[344],((C_word*)t0)[2],t4);
t6=C_a_i_list(&a,2,lf[309],C_SCHEME_FALSE);
t7=C_a_i_list(&a,4,lf[310],t1,t5,t6);
t8=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_a_i_list(&a,3,lf[311],t3,t7);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}

/* k13630 in k13556 in k13525 in k13482 in k13437 in k13393 in k13349 in k13306 in k13280 in k13238 in k13115 in k13100 in repeat in a13070 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in ... */
static void C_ccall f_13632(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(42,c,1)))){
C_save_and_reclaim((void *)f_13632,c,av);}
a=C_alloc(42);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,1,t2);
t4=(C_truep(*((C_word*)lf[10]+1))?t1:C_a_i_list(&a,2,lf[308],t1));
t5=C_a_i_list(&a,2,lf[309],C_SCHEME_FALSE);
t6=C_a_i_list(&a,4,lf[310],t1,t4,t5);
t7=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_a_i_list(&a,3,lf[311],t3,t6);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}

/* a14134 in chicken.compiler.support#foreign-type-check in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14135(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_14135,c,av);}
/* support.scm:1181: quit-compiling */
t2=*((C_word*)lf[37]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[394];
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.compiler.support#foreign-type-result-converter in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_14141(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_14141,2,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_symbolp(t2))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_14151,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* support.scm:1188: lookup-foreign-type */
t4=*((C_word*)lf[296]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k14149 in chicken.compiler.support#foreign-type-result-converter in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14151(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_14151,c,av);}
if(C_truep(t1)){
t2=C_i_vector_ref(t1,C_fix(2));
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(t2)?t2:C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* chicken.compiler.support#foreign-type-argument-converter in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_14162(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_14162,2,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_symbolp(t2))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_14172,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* support.scm:1194: lookup-foreign-type */
t4=*((C_word*)lf[296]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k14170 in chicken.compiler.support#foreign-type-argument-converter in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14172(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_14172,c,av);}
if(C_truep(t1)){
t2=C_i_vector_ref(t1,C_fix(1));
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(t2)?t2:C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* chicken.compiler.support#foreign-type-convert-result in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14183(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_14183,c,av);}
a=C_alloc(4);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_14187,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1199: foreign-type-result-converter */
f_14141(t4,t3);}

/* k14185 in chicken.compiler.support#foreign-type-convert-result in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14187(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_14187,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=C_a_i_list2(&a,2,t1,((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(t2)?t2:((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* chicken.compiler.support#foreign-type-convert-argument in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14198(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_14198,c,av);}
a=C_alloc(4);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_14202,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1204: foreign-type-argument-converter */
f_14162(t4,t3);}

/* k14200 in chicken.compiler.support#foreign-type-convert-argument in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14202(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_14202,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=C_a_i_list2(&a,2,t1,((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(t2)?t2:((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* chicken.compiler.support#final-foreign-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14213(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_14213,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_14219,tmp=(C_word)a,a+=2,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_14246,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* support.scm:1209: follow-without-loop */
f_7385(t1,t2,t3,t4);}

/* a14218 in chicken.compiler.support#final-foreign-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14219(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_14219,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_14223,a[2]=t3,a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_symbolp(t2))){
/* support.scm:1212: lookup-foreign-type */
t5=*((C_word*)lf[296]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
f_14223(2,av2);}}}

/* k14221 in a14218 in chicken.compiler.support#final-foreign-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14223(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_14223,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_14227,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:1212: g3322 */
t3=t2;
f_14227(t3,((C_word*)t0)[3],t1);}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* g3322 in k14221 in a14218 in chicken.compiler.support#final-foreign-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_14227(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_14227,3,t0,t1,t2);}
/* support.scm:1213: next */
t3=((C_word*)t0)[2];{
C_word av2[3];
av2[0]=t3;
av2[1]=t1;
av2[2]=C_i_vector_ref(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* a14245 in chicken.compiler.support#final-foreign-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14246(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_14246,c,av);}
/* support.scm:1215: quit-compiling */
t2=*((C_word*)lf[37]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[400];
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.compiler.support#estimate-foreign-result-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14252(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_14252,c,av);}
a=C_alloc(9);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_14255,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_14264,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_14721,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* support.scm:1223: follow-without-loop */
f_7385(t1,t2,t4,t5);}

/* err in chicken.compiler.support#estimate-foreign-result-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_14255(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_14255,2,t0,t1);}
/* support.scm:1222: quit-compiling */
t2=*((C_word*)lf[37]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[402];
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* a14263 in chicken.compiler.support#estimate-foreign-result-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14264(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_14264,c,av);}
a=C_alloc(6);
t4=C_eqp(t2,lf[299]);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_14274,a[2]=t1,a[3]=t2,a[4]=t3,a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t4)){
t6=t5;
f_14274(t6,t4);}
else{
t6=C_eqp(t2,lf[302]);
if(C_truep(t6)){
t7=t5;
f_14274(t7,t6);}
else{
t7=C_eqp(t2,lf[366]);
if(C_truep(t7)){
t8=t5;
f_14274(t8,t7);}
else{
t8=C_eqp(t2,lf[403]);
if(C_truep(t8)){
t9=t5;
f_14274(t9,t8);}
else{
t9=C_eqp(t2,lf[404]);
if(C_truep(t9)){
t10=t5;
f_14274(t10,t9);}
else{
t10=C_eqp(t2,lf[328]);
if(C_truep(t10)){
t11=t5;
f_14274(t11,t10);}
else{
t11=C_eqp(t2,lf[405]);
if(C_truep(t11)){
t12=t5;
f_14274(t12,t11);}
else{
t12=C_eqp(t2,lf[300]);
if(C_truep(t12)){
t13=t5;
f_14274(t13,t12);}
else{
t13=C_eqp(t2,lf[389]);
if(C_truep(t13)){
t14=t5;
f_14274(t14,t13);}
else{
t14=C_eqp(t2,lf[390]);
if(C_truep(t14)){
t15=t5;
f_14274(t15,t14);}
else{
t15=C_eqp(t2,lf[391]);
if(C_truep(t15)){
t16=t5;
f_14274(t16,t15);}
else{
t16=C_eqp(t2,lf[392]);
t17=t5;
f_14274(t17,(C_truep(t16)?t16:C_eqp(t2,lf[393])));}}}}}}}}}}}}

/* k14272 in a14263 in chicken.compiler.support#estimate-foreign-result-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_14274(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_14274,2,t0,t1);}
a=C_alloc(6);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_fix(0);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[3],lf[335]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_14283,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t2)){
t4=t3;
f_14283(t4,t2);}
else{
t4=C_eqp(((C_word*)t0)[3],lf[338]);
if(C_truep(t4)){
t5=t3;
f_14283(t5,t4);}
else{
t5=C_eqp(((C_word*)t0)[3],lf[332]);
if(C_truep(t5)){
t6=t3;
f_14283(t6,t5);}
else{
t6=C_eqp(((C_word*)t0)[3],lf[334]);
if(C_truep(t6)){
t7=t3;
f_14283(t7,t6);}
else{
t7=C_eqp(((C_word*)t0)[3],lf[339]);
if(C_truep(t7)){
t8=t3;
f_14283(t8,t7);}
else{
t8=C_eqp(((C_word*)t0)[3],lf[354]);
if(C_truep(t8)){
t9=t3;
f_14283(t9,t8);}
else{
t9=C_eqp(((C_word*)t0)[3],lf[352]);
if(C_truep(t9)){
t10=t3;
f_14283(t10,t9);}
else{
t10=C_eqp(((C_word*)t0)[3],lf[355]);
if(C_truep(t10)){
t11=t3;
f_14283(t11,t10);}
else{
t11=C_eqp(((C_word*)t0)[3],lf[356]);
if(C_truep(t11)){
t12=t3;
f_14283(t12,t11);}
else{
t12=C_eqp(((C_word*)t0)[3],lf[353]);
if(C_truep(t12)){
t13=t3;
f_14283(t13,t12);}
else{
t13=C_eqp(((C_word*)t0)[3],lf[357]);
t14=t3;
f_14283(t14,(C_truep(t13)?t13:C_eqp(((C_word*)t0)[3],lf[358])));}}}}}}}}}}}}

/* k14281 in k14272 in a14263 in chicken.compiler.support#estimate-foreign-result-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_14283(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_14283,2,t0,t1);}
a=C_alloc(6);
if(C_truep(t1)){
/* support.scm:1233: words->bytes */
t2=*((C_word*)lf[68]+1);{
C_word av2[3];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(3);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=C_eqp(((C_word*)t0)[3],lf[360]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_14295,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t2)){
t4=t3;
f_14295(t4,t2);}
else{
t4=C_eqp(((C_word*)t0)[3],lf[367]);
if(C_truep(t4)){
t5=t3;
f_14295(t5,t4);}
else{
t5=C_eqp(((C_word*)t0)[3],lf[365]);
if(C_truep(t5)){
t6=t3;
f_14295(t6,t5);}
else{
t6=C_eqp(((C_word*)t0)[3],lf[359]);
if(C_truep(t6)){
t7=t3;
f_14295(t7,t6);}
else{
t7=C_eqp(((C_word*)t0)[3],lf[320]);
t8=t3;
f_14295(t8,(C_truep(t7)?t7:C_eqp(((C_word*)t0)[3],lf[362])));}}}}}}

/* k14293 in k14281 in k14272 in a14263 in chicken.compiler.support#estimate-foreign-result-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_14295(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_14295,2,t0,t1);}
a=C_alloc(6);
if(C_truep(t1)){
/* support.scm:1235: words->bytes */
t2=*((C_word*)lf[68]+1);{
C_word av2[3];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(6);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=C_eqp(((C_word*)t0)[3],lf[304]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_14307,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t2)){
t4=t3;
f_14307(t4,t2);}
else{
t4=C_eqp(((C_word*)t0)[3],lf[387]);
t5=t3;
f_14307(t5,(C_truep(t4)?t4:C_eqp(((C_word*)t0)[3],lf[388])));}}}

/* k14305 in k14293 in k14281 in k14272 in a14263 in chicken.compiler.support#estimate-foreign-result-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_14307(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_14307,2,t0,t1);}
a=C_alloc(6);
if(C_truep(t1)){
/* support.scm:1237: words->bytes */
t2=*((C_word*)lf[68]+1);{
C_word av2[3];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(4);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=C_eqp(((C_word*)t0)[3],lf[364]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_14319,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t2)){
t4=t3;
f_14319(t4,t2);}
else{
t4=C_eqp(((C_word*)t0)[3],lf[363]);
if(C_truep(t4)){
t5=t3;
f_14319(t5,t4);}
else{
t5=C_eqp(((C_word*)t0)[3],lf[361]);
t6=t3;
f_14319(t6,(C_truep(t5)?t5:C_eqp(((C_word*)t0)[3],lf[368])));}}}}

/* k14317 in k14305 in k14293 in k14281 in k14272 in a14263 in chicken.compiler.support#estimate-foreign-result-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_14319(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_14319,2,t0,t1);}
a=C_alloc(6);
if(C_truep(t1)){
/* support.scm:1239: words->bytes */
t2=*((C_word*)lf[68]+1);{
C_word av2[3];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(7);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_14325,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_symbolp(((C_word*)t0)[4]))){
/* support.scm:1241: lookup-foreign-type */
t3=*((C_word*)lf[296]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=t2;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_14325(2,av2);}}}}

/* k14323 in k14317 in k14305 in k14293 in k14281 in k14272 in a14263 in chicken.compiler.support#estimate-foreign-result-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14325(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_14325,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_14329,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:1241: g3443 */
t3=t2;
f_14329(t3,((C_word*)t0)[3],t1);}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[4]))){
t2=C_u_i_car(((C_word*)t0)[4]);
t3=C_eqp(t2,lf[340]);
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_14356,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
if(C_truep(t3)){
t5=t4;
f_14356(t5,t3);}
else{
t5=C_eqp(t2,lf[349]);
if(C_truep(t5)){
t6=t4;
f_14356(t6,t5);}
else{
t6=C_eqp(t2,lf[350]);
if(C_truep(t6)){
t7=t4;
f_14356(t7,t6);}
else{
t7=C_eqp(t2,lf[332]);
if(C_truep(t7)){
t8=t4;
f_14356(t8,t7);}
else{
t8=C_eqp(t2,lf[334]);
if(C_truep(t8)){
t9=t4;
f_14356(t9,t8);}
else{
t9=C_eqp(t2,lf[351]);
if(C_truep(t9)){
t10=t4;
f_14356(t10,t9);}
else{
t10=C_eqp(t2,lf[341]);
if(C_truep(t10)){
t11=t4;
f_14356(t11,t10);}
else{
t11=C_eqp(t2,lf[342]);
t12=t4;
f_14356(t12,(C_truep(t11)?t11:C_eqp(t2,lf[345])));}}}}}}}}
else{
/* support.scm:1250: err */
t2=((C_word*)t0)[5];
f_14255(t2,((C_word*)t0)[3]);}}}

/* g3443 in k14323 in k14317 in k14305 in k14293 in k14281 in k14272 in a14263 in chicken.compiler.support#estimate-foreign-result-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_fcall f_14329(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_14329,3,t0,t1,t2);}
/* support.scm:1242: next */
t3=((C_word*)t0)[2];{
C_word av2[3];
av2[0]=t3;
av2[1]=t1;
av2[2]=C_i_vector_ref(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k14354 in k14323 in k14317 in k14305 in k14293 in k14281 in k14272 in a14263 in chicken.compiler.support#estimate-foreign-result-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_fcall f_14356(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_14356,2,t0,t1);}
if(C_truep(t1)){
/* support.scm:1246: words->bytes */
t2=*((C_word*)lf[68]+1);{
C_word av2[3];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(3);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=C_eqp(((C_word*)t0)[3],lf[346]);
if(C_truep(t2)){
/* support.scm:1247: next */
t3=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=C_i_cadr(((C_word*)t0)[5]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=C_eqp(((C_word*)t0)[3],lf[347]);
if(C_truep(t3)){
/* support.scm:1248: words->bytes */
t4=*((C_word*)lf[68]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(6);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
/* support.scm:1249: err */
t4=((C_word*)t0)[6];
f_14255(t4,((C_word*)t0)[2]);}}}}

/* a14720 in chicken.compiler.support#estimate-foreign-result-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14721(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_14721,c,av);}
/* support.scm:1251: quit-compiling */
t2=*((C_word*)lf[37]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[406];
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.compiler.support#estimate-foreign-result-location-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14727(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_14727,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_14739,tmp=(C_word)a,a+=2,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_15171,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* support.scm:1256: follow-without-loop */
f_7385(t1,t2,t3,t4);}

/* a14738 in chicken.compiler.support#estimate-foreign-result-location-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14739(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_14739,c,av);}
a=C_alloc(5);
t4=C_eqp(t2,lf[299]);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_14749,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
if(C_truep(t4)){
t6=t5;
f_14749(t6,t4);}
else{
t6=C_eqp(t2,lf[302]);
if(C_truep(t6)){
t7=t5;
f_14749(t7,t6);}
else{
t7=C_eqp(t2,lf[366]);
if(C_truep(t7)){
t8=t5;
f_14749(t8,t7);}
else{
t8=C_eqp(t2,lf[403]);
if(C_truep(t8)){
t9=t5;
f_14749(t9,t8);}
else{
t9=C_eqp(t2,lf[328]);
if(C_truep(t9)){
t10=t5;
f_14749(t10,t9);}
else{
t10=C_eqp(t2,lf[300]);
if(C_truep(t10)){
t11=t5;
f_14749(t11,t10);}
else{
t11=C_eqp(t2,lf[389]);
if(C_truep(t11)){
t12=t5;
f_14749(t12,t11);}
else{
t12=C_eqp(t2,lf[367]);
if(C_truep(t12)){
t13=t5;
f_14749(t13,t12);}
else{
t13=C_eqp(t2,lf[359]);
if(C_truep(t13)){
t14=t5;
f_14749(t14,t13);}
else{
t14=C_eqp(t2,lf[390]);
if(C_truep(t14)){
t15=t5;
f_14749(t15,t14);}
else{
t15=C_eqp(t2,lf[391]);
if(C_truep(t15)){
t16=t5;
f_14749(t16,t15);}
else{
t16=C_eqp(t2,lf[332]);
if(C_truep(t16)){
t17=t5;
f_14749(t17,t16);}
else{
t17=C_eqp(t2,lf[334]);
if(C_truep(t17)){
t18=t5;
f_14749(t18,t17);}
else{
t18=C_eqp(t2,lf[360]);
if(C_truep(t18)){
t19=t5;
f_14749(t19,t18);}
else{
t19=C_eqp(t2,lf[365]);
if(C_truep(t19)){
t20=t5;
f_14749(t20,t19);}
else{
t20=C_eqp(t2,lf[304]);
if(C_truep(t20)){
t21=t5;
f_14749(t21,t20);}
else{
t21=C_eqp(t2,lf[335]);
if(C_truep(t21)){
t22=t5;
f_14749(t22,t21);}
else{
t22=C_eqp(t2,lf[339]);
if(C_truep(t22)){
t23=t5;
f_14749(t23,t22);}
else{
t23=C_eqp(t2,lf[307]);
if(C_truep(t23)){
t24=t5;
f_14749(t24,t23);}
else{
t24=C_eqp(t2,lf[312]);
if(C_truep(t24)){
t25=t5;
f_14749(t25,t24);}
else{
t25=C_eqp(t2,lf[392]);
if(C_truep(t25)){
t26=t5;
f_14749(t26,t25);}
else{
t26=C_eqp(t2,lf[393]);
if(C_truep(t26)){
t27=t5;
f_14749(t27,t26);}
else{
t27=C_eqp(t2,lf[320]);
if(C_truep(t27)){
t28=t5;
f_14749(t28,t27);}
else{
t28=C_eqp(t2,lf[362]);
if(C_truep(t28)){
t29=t5;
f_14749(t29,t28);}
else{
t29=C_eqp(t2,lf[355]);
if(C_truep(t29)){
t30=t5;
f_14749(t30,t29);}
else{
t30=C_eqp(t2,lf[356]);
if(C_truep(t30)){
t31=t5;
f_14749(t31,t30);}
else{
t31=C_eqp(t2,lf[353]);
if(C_truep(t31)){
t32=t5;
f_14749(t32,t31);}
else{
t32=C_eqp(t2,lf[338]);
if(C_truep(t32)){
t33=t5;
f_14749(t33,t32);}
else{
t33=C_eqp(t2,lf[354]);
if(C_truep(t33)){
t34=t5;
f_14749(t34,t33);}
else{
t34=C_eqp(t2,lf[352]);
if(C_truep(t34)){
t35=t5;
f_14749(t35,t34);}
else{
t35=C_eqp(t2,lf[357]);
t36=t5;
f_14749(t36,(C_truep(t35)?t35:C_eqp(t2,lf[358])));}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}

/* k14747 in a14738 in chicken.compiler.support#estimate-foreign-result-location-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_14749(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_14749,2,t0,t1);}
a=C_alloc(5);
if(C_truep(t1)){
/* support.scm:1265: words->bytes */
t2=*((C_word*)lf[68]+1);{
C_word av2[3];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(1);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=C_eqp(((C_word*)t0)[3],lf[387]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_14761,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t2)){
t4=t3;
f_14761(t4,t2);}
else{
t4=C_eqp(((C_word*)t0)[3],lf[364]);
if(C_truep(t4)){
t5=t3;
f_14761(t5,t4);}
else{
t5=C_eqp(((C_word*)t0)[3],lf[363]);
if(C_truep(t5)){
t6=t3;
f_14761(t6,t5);}
else{
t6=C_eqp(((C_word*)t0)[3],lf[361]);
t7=t3;
f_14761(t7,(C_truep(t6)?t6:C_eqp(((C_word*)t0)[3],lf[368])));}}}}}

/* k14759 in k14747 in a14738 in chicken.compiler.support#estimate-foreign-result-location-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_14761(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_14761,2,t0,t1);}
a=C_alloc(5);
if(C_truep(t1)){
/* support.scm:1267: words->bytes */
t2=*((C_word*)lf[68]+1);{
C_word av2[3];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(2);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_14767,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_symbolp(((C_word*)t0)[4]))){
/* support.scm:1269: lookup-foreign-type */
t3=*((C_word*)lf[296]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=t2;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_14767(2,av2);}}}}

/* k14765 in k14759 in k14747 in a14738 in chicken.compiler.support#estimate-foreign-result-location-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_14767(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_14767,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_14771,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:1269: g3601 */
t3=t2;
f_14771(t3,((C_word*)t0)[3],t1);}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[4]))){
t2=C_u_i_car(((C_word*)t0)[4]);
t3=C_eqp(t2,lf[340]);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_14798,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t3)){
t5=t4;
f_14798(t5,t3);}
else{
t5=C_eqp(t2,lf[349]);
if(C_truep(t5)){
t6=t4;
f_14798(t6,t5);}
else{
t6=C_eqp(t2,lf[350]);
if(C_truep(t6)){
t7=t4;
f_14798(t7,t6);}
else{
t7=C_eqp(t2,lf[332]);
if(C_truep(t7)){
t8=t4;
f_14798(t8,t7);}
else{
t8=C_eqp(t2,lf[334]);
if(C_truep(t8)){
t9=t4;
f_14798(t9,t8);}
else{
t9=C_eqp(t2,lf[351]);
if(C_truep(t9)){
t10=t4;
f_14798(t10,t9);}
else{
t10=C_eqp(t2,lf[307]);
if(C_truep(t10)){
t11=t4;
f_14798(t11,t10);}
else{
t11=C_eqp(t2,lf[312]);
t12=t4;
f_14798(t12,(C_truep(t11)?t11:C_eqp(t2,lf[347])));}}}}}}}}
else{
/* support.scm:1255: quit-compiling */
t2=*((C_word*)lf[37]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[408];
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}}}

/* g3601 in k14765 in k14759 in k14747 in a14738 in chicken.compiler.support#estimate-foreign-result-location-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_14771(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_14771,3,t0,t1,t2);}
/* support.scm:1270: next */
t3=((C_word*)t0)[2];{
C_word av2[3];
av2[0]=t3;
av2[1]=t1;
av2[2]=C_i_vector_ref(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k14796 in k14765 in k14759 in k14747 in a14738 in chicken.compiler.support#estimate-foreign-result-location-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_14798(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_14798,2,t0,t1);}
if(C_truep(t1)){
/* support.scm:1275: words->bytes */
t2=*((C_word*)lf[68]+1);{
C_word av2[3];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(1);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=C_eqp(((C_word*)t0)[3],lf[346]);
if(C_truep(t2)){
/* support.scm:1276: next */
t3=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=C_i_cadr(((C_word*)t0)[5]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
/* support.scm:1255: quit-compiling */
t3=*((C_word*)lf[37]+1);{
C_word av2[4];
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[408];
av2[3]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}}

/* a15170 in chicken.compiler.support#estimate-foreign-result-location-size in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_15171(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_15171,c,av);}
/* support.scm:1279: quit-compiling */
t2=*((C_word*)lf[37]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[409];
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.compiler.support#finish-foreign-result in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_15177(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_15177,c,av);}
a=C_alloc(4);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_15181,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1285: chicken.syntax#strip-syntax */
t5=*((C_word*)lf[423]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k15179 in chicken.compiler.support#finish-foreign-result in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_15181(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,2)))){
C_save_and_reclaim((void *)f_15181,c,av);}
a=C_alloc(21);
t2=C_eqp(t1,lf[335]);
t3=(C_truep(t2)?t2:C_eqp(t1,lf[355]));
if(C_truep(t3)){
t4=C_a_i_list(&a,2,lf[309],C_fix(0));
t5=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_list(&a,3,lf[411],((C_word*)t0)[3],t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=C_eqp(t1,lf[338]);
if(C_truep(t4)){
t5=C_a_i_list(&a,2,lf[309],C_fix(0));
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_list(&a,3,lf[412],((C_word*)t0)[3],t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t5=C_eqp(t1,lf[354]);
t6=(C_truep(t5)?t5:C_eqp(t1,lf[356]));
if(C_truep(t6)){
t7=C_a_i_list(&a,2,lf[309],C_fix(0));
t8=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_a_i_list(&a,3,lf[413],((C_word*)t0)[3],t7);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t7=C_eqp(t1,lf[352]);
t8=(C_truep(t7)?t7:C_eqp(t1,lf[353]));
if(C_truep(t8)){
t9=C_a_i_list(&a,2,lf[309],C_fix(0));
t10=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t10;
av2[1]=C_a_i_list(&a,3,lf[414],((C_word*)t0)[3],t9);
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
t9=C_eqp(t1,lf[339]);
if(C_truep(t9)){
t10=C_a_i_list(&a,2,lf[309],C_fix(0));
t11=C_a_i_list(&a,3,lf[411],((C_word*)t0)[3],t10);
t12=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t12;
av2[1]=C_a_i_list(&a,2,lf[415],t11);
((C_proc)(void*)(*((C_word*)t12+1)))(2,av2);}}
else{
t10=C_eqp(t1,lf[357]);
if(C_truep(t10)){
t11=C_a_i_list(&a,2,lf[309],C_SCHEME_FALSE);
t12=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t12;
av2[1]=C_a_i_list(&a,3,lf[416],((C_word*)t0)[3],t11);
((C_proc)(void*)(*((C_word*)t12+1)))(2,av2);}}
else{
t11=C_eqp(t1,lf[358]);
if(C_truep(t11)){
t12=C_a_i_list(&a,2,lf[309],C_SCHEME_FALSE);
t13=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t13;
av2[1]=C_a_i_list(&a,3,lf[417],((C_word*)t0)[3],t12);
((C_proc)(void*)(*((C_word*)t13+1)))(2,av2);}}
else{
if(C_truep(C_i_listp(t1))){
t12=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_15297,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t13=C_i_car(t1);
t14=C_eqp(t13,lf[346]);
if(C_truep(t14)){
t15=C_i_length(t1);
t16=C_eqp(C_fix(2),t15);
t17=t12;
f_15297(t17,(C_truep(t16)?C_u_i_memq(C_i_cadr(t1),lf[422]):C_SCHEME_FALSE));}
else{
t15=t12;
f_15297(t15,C_SCHEME_FALSE);}}
else{
t12=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t12;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t12+1)))(2,av2);}}}}}}}}}}

/* k15295 in k15179 in chicken.compiler.support#finish-foreign-result in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_15297(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,0,3)))){
C_save_and_reclaim_args((void *)trf_15297,2,t0,t1);}
a=C_alloc(18);
if(C_truep(t1)){
/* support.scm:1302: finish-foreign-result */
t2=*((C_word*)lf[410]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_i_cadr(((C_word*)t0)[3]);
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
t2=C_i_length(((C_word*)t0)[3]);
t3=C_eqp(C_fix(3),t2);
if(C_truep(t3)){
t4=C_i_car(((C_word*)t0)[3]);
t5=C_eqp(t4,lf[341]);
t6=(C_truep(t5)?t5:C_eqp(t4,lf[342]));
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_15325,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* support.scm:1306: chicken.base#gensym */
t8=*((C_word*)lf[97]+1);{
C_word av2[2];
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t7=C_eqp(t4,lf[345]);
if(C_truep(t7)){
t8=C_i_caddr(((C_word*)t0)[3]);
t9=C_a_i_list(&a,2,lf[309],lf[343]);
t10=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t10;
av2[1]=C_a_i_list(&a,4,lf[420],t8,t9,((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
t8=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t8;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}}
else{
t4=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t4;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}}

/* k15323 in k15295 in k15179 in chicken.compiler.support#finish-foreign-result in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_15325(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(60,c,1)))){
C_save_and_reclaim((void *)f_15325,c,av);}
a=C_alloc(60);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,1,t2);
t4=C_a_i_list(&a,2,lf[418],t1);
t5=C_a_i_list(&a,2,lf[419],t4);
t6=C_i_caddr(((C_word*)t0)[3]);
t7=C_a_i_list(&a,2,lf[309],lf[343]);
t8=C_a_i_list(&a,4,lf[420],t6,t7,t1);
t9=C_a_i_list(&a,4,lf[421],t1,t5,t8);
t10=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t10;
av2[1]=C_a_i_list(&a,3,lf[96],t3,t9);
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}

/* chicken.compiler.support#foreign-type->scrutiny-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_15445(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_15445,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_15452,a[2]=t1,a[3]=t3,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t5=C_eqp(t3,lf[426]);
if(C_truep(t5)){
/* support.scm:1325: foreign-type-argument-converter */
f_14162(t4,t2);}
else{
t6=t4;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_FALSE;
f_15452(2,av2);}}}

/* k15450 in chicken.compiler.support#foreign-type->scrutiny-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_15452(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_15452,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_15455,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t1)){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
f_15455(2,av2);}}
else{
t3=C_eqp(((C_word*)t0)[3],lf[447]);
if(C_truep(t3)){
/* support.scm:1326: foreign-type-result-converter */
f_14141(t2,((C_word*)t0)[4]);}
else{
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
f_15455(2,av2);}}}}

/* k15453 in k15450 in chicken.compiler.support#foreign-type->scrutiny-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_15455(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_15455,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[170];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_15458,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:1330: final-foreign-type */
t3=*((C_word*)lf[399]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k15456 in k15453 in k15450 in chicken.compiler.support#foreign-type->scrutiny-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_15458(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_15458,c,av);}
a=C_alloc(6);
t2=C_eqp(t1,lf[404]);
if(C_truep(t2)){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=lf[425];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_eqp(t1,lf[299]);
t4=(C_truep(t3)?t3:C_eqp(t1,lf[300]));
if(C_truep(t4)){
t5=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t5;
av2[1]=lf[299];
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_eqp(t1,lf[302]);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_15482,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t5)){
t7=t6;
f_15482(t7,t5);}
else{
t7=C_eqp(t1,lf[389]);
if(C_truep(t7)){
t8=t6;
f_15482(t8,t7);}
else{
t8=C_eqp(t1,lf[366]);
if(C_truep(t8)){
t9=t6;
f_15482(t9,t8);}
else{
t9=C_eqp(t1,lf[328]);
if(C_truep(t9)){
t10=t6;
f_15482(t10,t9);}
else{
t10=C_eqp(t1,lf[390]);
if(C_truep(t10)){
t11=t6;
f_15482(t11,t10);}
else{
t11=C_eqp(t1,lf[391]);
if(C_truep(t11)){
t12=t6;
f_15482(t12,t11);}
else{
t12=C_eqp(t1,lf[392]);
t13=t6;
f_15482(t13,(C_truep(t12)?t12:C_eqp(t1,lf[393])));}}}}}}}}}

/* k15480 in k15456 in k15453 in k15450 in chicken.compiler.support#foreign-type->scrutiny-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_15482(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_15482,2,t0,t1);}
a=C_alloc(6);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=lf[164];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[3],lf[304]);
t3=(C_truep(t2)?t2:C_eqp(((C_word*)t0)[3],lf[387]));
if(C_truep(t3)){
t4=C_eqp(((C_word*)t0)[4],lf[426]);
t5=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t5;
av2[1]=(C_truep(t4)?lf[388]:lf[304]);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=C_eqp(((C_word*)t0)[3],lf[307]);
t5=(C_truep(t4)?t4:C_eqp(((C_word*)t0)[3],lf[312]));
if(C_truep(t5)){
t6=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t6;
av2[1]=lf[170];
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_eqp(((C_word*)t0)[3],lf[306]);
if(C_truep(t6)){
t7=C_eqp(((C_word*)t0)[4],lf[426]);
t8=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t8;
av2[1]=(C_truep(t7)?lf[427]:lf[306]);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t7=C_eqp(((C_word*)t0)[3],lf[313]);
if(C_truep(t7)){
t8=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t8;
av2[1]=lf[306];
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t8=C_eqp(((C_word*)t0)[3],lf[314]);
if(C_truep(t8)){
t9=C_eqp(((C_word*)t0)[4],lf[426]);
t10=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t10;
av2[1]=(C_truep(t9)?lf[428]:lf[314]);
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
t9=C_eqp(((C_word*)t0)[3],lf[316]);
if(C_truep(t9)){
t10=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t10;
av2[1]=lf[314];
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
t10=C_eqp(((C_word*)t0)[3],lf[317]);
t11=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_15557,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t10)){
t12=t11;
f_15557(t12,t10);}
else{
t12=C_eqp(((C_word*)t0)[3],lf[378]);
if(C_truep(t12)){
t13=t11;
f_15557(t13,t12);}
else{
t13=C_eqp(((C_word*)t0)[3],lf[379]);
if(C_truep(t13)){
t14=t11;
f_15557(t14,t13);}
else{
t14=C_eqp(((C_word*)t0)[3],lf[380]);
if(C_truep(t14)){
t15=t11;
f_15557(t15,t14);}
else{
t15=C_eqp(((C_word*)t0)[3],lf[381]);
if(C_truep(t15)){
t16=t11;
f_15557(t16,t15);}
else{
t16=C_eqp(((C_word*)t0)[3],lf[382]);
if(C_truep(t16)){
t17=t11;
f_15557(t17,t16);}
else{
t17=C_eqp(((C_word*)t0)[3],lf[383]);
if(C_truep(t17)){
t18=t11;
f_15557(t18,t17);}
else{
t18=C_eqp(((C_word*)t0)[3],lf[384]);
if(C_truep(t18)){
t19=t11;
f_15557(t19,t18);}
else{
t19=C_eqp(((C_word*)t0)[3],lf[385]);
t20=t11;
f_15557(t20,(C_truep(t19)?t19:C_eqp(((C_word*)t0)[3],lf[386])));}}}}}}}}}}}}}}}}

/* k15555 in k15480 in k15456 in k15453 in k15450 in chicken.compiler.support#foreign-type->scrutiny-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_15557(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,2)))){
C_save_and_reclaim_args((void *)trf_15557,2,t0,t1);}
a=C_alloc(15);
if(C_truep(t1)){
t2=C_eqp(((C_word*)t0)[2],lf[426]);
if(C_truep(t2)){
t3=C_a_i_list(&a,2,lf[429],((C_word*)t0)[3]);
t4=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t4;
av2[1]=C_a_i_list(&a,3,lf[430],lf[431],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t3;
av2[1]=C_a_i_list(&a,2,lf[429],((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}
else{
t2=C_eqp(((C_word*)t0)[3],lf[318]);
if(C_truep(t2)){
t3=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t3;
av2[1]=lf[432];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_eqp(((C_word*)t0)[3],lf[370]);
if(C_truep(t3)){
t4=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t4;
av2[1]=lf[433];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_eqp(((C_word*)t0)[3],lf[369]);
if(C_truep(t4)){
t5=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t5;
av2[1]=lf[434];
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_eqp(((C_word*)t0)[3],lf[371]);
if(C_truep(t5)){
t6=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t6;
av2[1]=lf[435];
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_eqp(((C_word*)t0)[3],lf[372]);
if(C_truep(t6)){
t7=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t7;
av2[1]=lf[436];
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
t7=C_eqp(((C_word*)t0)[3],lf[373]);
if(C_truep(t7)){
t8=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t8;
av2[1]=lf[437];
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t8=C_eqp(((C_word*)t0)[3],lf[374]);
if(C_truep(t8)){
t9=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t9;
av2[1]=lf[438];
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}
else{
t9=C_eqp(((C_word*)t0)[3],lf[375]);
if(C_truep(t9)){
t10=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t10;
av2[1]=lf[439];
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
t10=C_eqp(((C_word*)t0)[3],lf[376]);
if(C_truep(t10)){
t11=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t11;
av2[1]=lf[440];
((C_proc)(void*)(*((C_word*)t11+1)))(2,av2);}}
else{
t11=C_eqp(((C_word*)t0)[3],lf[377]);
if(C_truep(t11)){
t12=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t12;
av2[1]=lf[441];
((C_proc)(void*)(*((C_word*)t12+1)))(2,av2);}}
else{
t12=C_eqp(((C_word*)t0)[3],lf[365]);
t13=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_15644,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t12)){
t14=t13;
f_15644(t14,t12);}
else{
t14=C_eqp(((C_word*)t0)[3],lf[367]);
if(C_truep(t14)){
t15=t13;
f_15644(t15,t14);}
else{
t15=C_eqp(((C_word*)t0)[3],lf[361]);
if(C_truep(t15)){
t16=t13;
f_15644(t16,t15);}
else{
t16=C_eqp(((C_word*)t0)[3],lf[368]);
if(C_truep(t16)){
t17=t13;
f_15644(t17,t16);}
else{
t17=C_eqp(((C_word*)t0)[3],lf[320]);
if(C_truep(t17)){
t18=t13;
f_15644(t18,t17);}
else{
t18=C_eqp(((C_word*)t0)[3],lf[362]);
if(C_truep(t18)){
t19=t13;
f_15644(t19,t18);}
else{
t19=C_eqp(((C_word*)t0)[3],lf[364]);
if(C_truep(t19)){
t20=t13;
f_15644(t20,t19);}
else{
t20=C_eqp(((C_word*)t0)[3],lf[363]);
t21=t13;
f_15644(t21,(C_truep(t20)?t20:C_eqp(((C_word*)t0)[3],lf[359])));}}}}}}}}}}}}}}}}}}}

/* k15642 in k15555 in k15480 in k15456 in k15453 in k15450 in chicken.compiler.support#foreign-type->scrutiny-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_15644(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_15644,2,t0,t1);}
a=C_alloc(6);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=lf[365];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[3],lf[332]);
if(C_truep(t2)){
t3=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t3;
av2[1]=lf[442];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_eqp(((C_word*)t0)[3],lf[334]);
if(C_truep(t3)){
t4=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t4;
av2[1]=lf[350];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_eqp(((C_word*)t0)[3],lf[335]);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_15665,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t4)){
t6=t5;
f_15665(t6,t4);}
else{
t6=C_eqp(((C_word*)t0)[3],lf[354]);
if(C_truep(t6)){
t7=t5;
f_15665(t7,t6);}
else{
t7=C_eqp(((C_word*)t0)[3],lf[355]);
t8=t5;
f_15665(t8,(C_truep(t7)?t7:C_eqp(((C_word*)t0)[3],lf[356])));}}}}}}

/* k15663 in k15642 in k15555 in k15480 in k15456 in k15453 in k15450 in chicken.compiler.support#foreign-type->scrutiny-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_15665(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_15665,2,t0,t1);}
a=C_alloc(6);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=lf[443];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[3],lf[357]);
t3=(C_truep(t2)?t2:C_eqp(((C_word*)t0)[3],lf[358]));
if(C_truep(t3)){
t4=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t4;
av2[1]=lf[444];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_eqp(((C_word*)t0)[3],lf[338]);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_15683,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t4)){
t6=t5;
f_15683(t6,t4);}
else{
t6=C_eqp(((C_word*)t0)[3],lf[352]);
t7=t5;
f_15683(t7,(C_truep(t6)?t6:C_eqp(((C_word*)t0)[3],lf[353])));}}}}

/* k15681 in k15663 in k15642 in k15555 in k15480 in k15456 in k15453 in k15450 in chicken.compiler.support#foreign-type->scrutiny-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_fcall f_15683(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_15683,2,t0,t1);}
a=C_alloc(6);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=lf[445];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[3],lf[339]);
if(C_truep(t2)){
t3=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t3;
av2[1]=lf[339];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[4]))){
t3=C_u_i_car(((C_word*)t0)[4]);
t4=C_eqp(t3,lf[340]);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_15705,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t4)){
t6=t5;
f_15705(t6,t4);}
else{
t6=C_eqp(t3,lf[350]);
if(C_truep(t6)){
t7=t5;
f_15705(t7,t6);}
else{
t7=C_eqp(t3,lf[351]);
t8=t5;
f_15705(t8,(C_truep(t7)?t7:C_eqp(t3,lf[332])));}}}
else{
t3=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t3;
av2[1]=lf[170];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}}}

/* k15703 in k15681 in k15663 in k15642 in k15555 in k15480 in k15456 in k15453 in k15450 in chicken.compiler.support#foreign-type->scrutiny-type in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_fcall f_15705(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_15705,2,t0,t1);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=lf[446];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[3],lf[346]);
if(C_truep(t2)){
/* support.scm:1382: foreign-type->scrutiny-type */
t3=*((C_word*)lf[424]+1);{
C_word av2[4];
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=C_i_cadr(((C_word*)t0)[4]);
av2[3]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t3=C_eqp(((C_word*)t0)[3],lf[347]);
if(C_truep(t3)){
t4=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t4;
av2[1]=lf[365];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_eqp(((C_word*)t0)[3],lf[349]);
if(C_truep(t4)){
t5=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t5;
av2[1]=(C_truep(t4)?lf[350]:lf[170]);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_eqp(((C_word*)t0)[3],lf[334]);
t6=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t6;
av2[1]=(C_truep(t5)?lf[350]:lf[170]);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}}}}

/* chicken.compiler.support#scan-used-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16075(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_16075,c,av);}
a=C_alloc(13);
t4=C_SCHEME_END_OF_LIST;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16079,a[2]=t1,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16081,a[2]=t8,a[3]=t3,a[4]=t5,tmp=(C_word)a,a+=5,tmp));
t10=((C_word*)t8)[1];
f_16081(t10,t6,t2);}

/* k16077 in chicken.compiler.support#scan-used-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16079(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_16079,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)((C_word*)t0)[3])[1];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* walk in chicken.compiler.support#scan-used-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_16081(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_16081,3,t0,t1,t2);}
a=C_alloc(8);
t3=C_slot(t2,C_fix(3));
t4=C_slot(t2,C_fix(1));
t5=C_eqp(t4,lf[154]);
t6=(C_truep(t5)?t5:C_eqp(t4,lf[124]));
if(C_truep(t6)){
t7=C_slot(t2,C_fix(2));
t8=C_i_car(t7);
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16113,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t10=C_i_memq(t8,((C_word*)t0)[3]);
t11=(C_truep(t10)?C_i_not(C_i_memq(t8,((C_word*)((C_word*)t0)[4])[1])):C_SCHEME_FALSE);
if(C_truep(t11)){
t12=C_a_i_cons(&a,2,t8,((C_word*)((C_word*)t0)[4])[1]);
t13=C_mutate(((C_word *)((C_word*)t0)[4])+1,t12);
t14=t9;
f_16113(t14,t13);}
else{
t12=t9;
f_16113(t12,C_SCHEME_UNDEFINED);}}
else{
t7=C_eqp(t4,lf[85]);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16177,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t7)){
t9=t8;
f_16177(t9,t7);}
else{
t9=C_eqp(t4,lf[159]);
t10=t8;
f_16177(t10,(C_truep(t9)?t9:C_eqp(t4,lf[171])));}}}

/* k16111 in walk in chicken.compiler.support#scan-used-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_16113(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_16113,2,t0,t1);}
a=C_alloc(6);
t2=C_i_check_list_2(((C_word*)t0)[2],lf[44]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16121,a[2]=t4,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_16121(t6,((C_word*)t0)[4],((C_word*)t0)[2]);}

/* for-each-loop3887 in k16111 in walk in chicken.compiler.support#scan-used-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_16121(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_16121,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16131,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:1400: g3888 */
t4=((C_word*)((C_word*)t0)[3])[1];
f_16081(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k16129 in for-each-loop3887 in k16111 in walk in chicken.compiler.support#scan-used-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16131(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_16131,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_16121(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k16175 in walk in chicken.compiler.support#scan-used-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_16177(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_16177,2,t0,t1);}
a=C_alloc(6);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_i_check_list_2(((C_word*)t0)[3],lf[44]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16185,a[2]=t4,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_16185(t6,((C_word*)t0)[2],((C_word*)t0)[3]);}}

/* for-each-loop3912 in k16175 in walk in chicken.compiler.support#scan-used-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_16185(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_16185,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16195,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:1402: g3913 */
t4=((C_word*)((C_word*)t0)[3])[1];
f_16081(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k16193 in for-each-loop3912 in k16175 in walk in chicken.compiler.support#scan-used-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16195(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_16195,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_16185(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* chicken.compiler.support#scan-free-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16230(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,4)))){
C_save_and_reclaim((void *)f_16230,c,av);}
a=C_alloc(23);
t4=C_SCHEME_END_OF_LIST;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_SCHEME_END_OF_LIST;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_16233,a[2]=t5,a[3]=t7,a[4]=t3,a[5]=t9,a[6]=t11,tmp=(C_word)a,a+=7,tmp));
t13=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_16417,a[2]=t9,tmp=(C_word)a,a+=3,tmp));
t14=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16455,a[2]=t1,a[3]=t5,a[4]=t7,tmp=(C_word)a,a+=5,tmp);
/* support.scm:1440: walk */
t15=((C_word*)t9)[1];
f_16233(t15,t14,t2,C_SCHEME_END_OF_LIST);}

/* walk in chicken.compiler.support#scan-free-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_16233(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_16233,4,t0,t1,t2,t3);}
a=C_alloc(12);
t4=C_slot(t2,C_fix(3));
t5=C_slot(t2,C_fix(2));
t6=C_slot(t2,C_fix(1));
t7=C_eqp(t6,lf[85]);
t8=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_16267,a[2]=t1,a[3]=t6,a[4]=t5,a[5]=t3,a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[4],a[9]=((C_word*)t0)[5],a[10]=t4,a[11]=((C_word*)t0)[6],tmp=(C_word)a,a+=12,tmp);
if(C_truep(t7)){
t9=t8;
f_16267(t9,t7);}
else{
t9=C_eqp(t6,lf[159]);
if(C_truep(t9)){
t10=t8;
f_16267(t10,t9);}
else{
t10=C_eqp(t6,lf[171]);
if(C_truep(t10)){
t11=t8;
f_16267(t11,t10);}
else{
t11=C_eqp(t6,lf[174]);
t12=t8;
f_16267(t12,(C_truep(t11)?t11:C_eqp(t6,lf[184])));}}}}

/* k16265 in walk in chicken.compiler.support#scan-free-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_16267(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,5)))){
C_save_and_reclaim_args((void *)trf_16267,2,t0,t1);}
a=C_alloc(10);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[3],lf[154]);
if(C_truep(t2)){
t3=C_i_car(((C_word*)t0)[4]);
if(C_truep(C_i_memq(t3,((C_word*)t0)[5]))){
t4=C_SCHEME_UNDEFINED;
t5=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_16286,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[7],a[5]=t3,a[6]=((C_word*)t0)[8],tmp=(C_word)a,a+=7,tmp);
/* support.scm:1420: lset-adjoin/eq? */
f_6142(t4,((C_word*)((C_word*)t0)[6])[1],C_a_i_list(&a,1,t3));}}
else{
t3=C_eqp(((C_word*)t0)[3],lf[124]);
if(C_truep(t3)){
t4=C_i_car(((C_word*)t0)[4]);
if(C_truep(C_i_memq(t4,((C_word*)t0)[5]))){
/* support.scm:1426: walk */
t5=((C_word*)((C_word*)t0)[9])[1];
f_16233(t5,((C_word*)t0)[2],C_i_car(((C_word*)t0)[10]),((C_word*)t0)[5]);}
else{
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_16322,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[9],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[10],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* support.scm:1425: lset-adjoin/eq? */
f_6142(t5,((C_word*)((C_word*)t0)[6])[1],C_a_i_list(&a,1,t4));}}
else{
t4=C_eqp(((C_word*)t0)[3],lf[96]);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_16331,a[2]=((C_word*)t0)[10],a[3]=((C_word*)t0)[9],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* support.scm:1428: walk */
t6=((C_word*)((C_word*)t0)[9])[1];
f_16233(t6,t5,C_i_car(((C_word*)t0)[10]),((C_word*)t0)[5]);}
else{
t5=C_eqp(((C_word*)t0)[3],lf[118]);
if(C_truep(t5)){
t6=C_i_caddr(((C_word*)t0)[4]);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16361,a[2]=((C_word*)t0)[10],a[3]=((C_word*)t0)[9],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* support.scm:1431: ##sys#decompose-lambda-list */
t8=*((C_word*)lf[225]+1);{
C_word av2[4];
av2[0]=t8;
av2[1]=((C_word*)t0)[2];
av2[2]=t6;
av2[3]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}
else{
/* support.scm:1435: walkeach */
t6=((C_word*)((C_word*)t0)[11])[1];
f_16417(t6,((C_word*)t0)[2],((C_word*)t0)[10],((C_word*)t0)[5]);}}}}}}

/* k16284 in k16265 in walk in chicken.compiler.support#scan-free-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16286(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_16286,c,av);}
a=C_alloc(5);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16292,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* support.scm:1421: variable-visible? */
t4=*((C_word*)lf[253]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k16290 in k16284 in k16265 in walk in chicken.compiler.support#scan-free-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16292(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_16292,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16296,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1422: lset-adjoin/eq? */
f_6142(t2,((C_word*)((C_word*)t0)[3])[1],C_a_i_list(&a,1,((C_word*)t0)[4]));}}

/* k16294 in k16290 in k16284 in k16265 in walk in chicken.compiler.support#scan-free-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16296(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_16296,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k16320 in k16265 in walk in chicken.compiler.support#scan-free-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16322(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_16322,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
/* support.scm:1426: walk */
t3=((C_word*)((C_word*)t0)[3])[1];
f_16233(t3,((C_word*)t0)[4],C_i_car(((C_word*)t0)[5]),((C_word*)t0)[6]);}

/* k16329 in k16265 in walk in chicken.compiler.support#scan-free-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16331(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_16331,c,av);}
a=C_alloc(5);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16342,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:1429: scheme#append */
t4=*((C_word*)lf[58]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k16340 in k16329 in k16265 in walk in chicken.compiler.support#scan-free-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16342(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_16342,c,av);}
/* support.scm:1429: walk */
t2=((C_word*)((C_word*)t0)[2])[1];
f_16233(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* a16360 in k16265 in walk in chicken.compiler.support#scan-free-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16361(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_16361,c,av);}
a=C_alloc(5);
t5=C_i_car(((C_word*)t0)[2]);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16373,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t5,tmp=(C_word)a,a+=5,tmp);
/* support.scm:1434: scheme#append */
t7=*((C_word*)lf[58]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=t2;
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}

/* k16371 in a16360 in k16265 in walk in chicken.compiler.support#scan-free-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16373(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_16373,c,av);}
/* support.scm:1434: walk */
t2=((C_word*)((C_word*)t0)[2])[1];
f_16233(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* walkeach in chicken.compiler.support#scan-free-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_16417(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,3)))){
C_save_and_reclaim_args((void *)trf_16417,4,t0,t1,t2,t3);}
a=C_alloc(10);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16419,a[2]=((C_word*)t0)[2],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t5=C_i_check_list_2(t2,lf[44]);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16431,a[2]=t7,a[3]=t4,tmp=(C_word)a,a+=4,tmp));
t9=((C_word*)t7)[1];
f_16431(t9,t1,t2);}

/* g3980 in walkeach in chicken.compiler.support#scan-free-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_16419(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_16419,3,t0,t1,t2);}
/* support.scm:1438: walk */
t3=((C_word*)((C_word*)t0)[2])[1];
f_16233(t3,t1,t2,((C_word*)t0)[3]);}

/* for-each-loop3979 in walkeach in chicken.compiler.support#scan-free-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_16431(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_16431,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16441,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:1438: g3980 */
t4=((C_word*)t0)[3];
f_16419(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k16439 in for-each-loop3979 in walkeach in chicken.compiler.support#scan-free-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16441(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_16441,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_16431(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k16453 in chicken.compiler.support#scan-free-variables in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16455(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_16455,c,av);}
/* support.scm:1441: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=((C_word*)((C_word*)t0)[4])[1];
C_values(4,av2);}}

/* chicken.compiler.support#chop-separator in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16460(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_16460,c,av);}
a=C_alloc(5);
t3=C_i_string_length(t2);
t4=C_a_i_fixnum_difference(&a,2,t3,C_fix(1));
if(C_truep(C_i_integer_greaterp(t4,C_fix(0)))){
t5=C_i_string_ref(t2,t4);
if(C_truep((C_truep(C_eqp(t5,C_make_character(92)))?C_SCHEME_TRUE:(C_truep(C_eqp(t5,C_make_character(47)))?C_SCHEME_TRUE:C_SCHEME_FALSE)))){
/* support.scm:1450: scheme#substring */
t6=*((C_word*)lf[451]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t1;
av2[2]=t2;
av2[3]=C_fix(0);
av2[4]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}
else{
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* chicken.compiler.support#make-block-variable-literal in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16484(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_16484,c,av);}
a=C_alloc(3);
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_record2(&a,2,lf[453],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.compiler.support#block-variable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16490(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_16490,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_structurep(t2,lf[453]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.compiler.support#block-variable-literal-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16496(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_16496,c,av);}
t3=C_i_check_structure_2(t2,lf[453],lf[456]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_block_ref(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.compiler.support#make-random-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16505(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +4,c,2)))){
C_save_and_reclaim((void*)f_16505,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+4);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16513,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1466: chicken.base#open-output-string */
t4=*((C_word*)lf[327]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k16511 in chicken.compiler.support#make-random-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16513(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_16513,c,av);}
a=C_alloc(8);
t2=C_i_check_port_2(t1,C_fix(2),C_SCHEME_TRUE,lf[321]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16519,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16540,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_nullp(((C_word*)t0)[3]))){
/* support.scm:1467: chicken.base#gensym */
t5=*((C_word*)lf[97]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
/* support.scm:1466: ##sys#print */
t5=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=C_i_car(((C_word*)t0)[3]);
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}}

/* k16517 in k16511 in chicken.compiler.support#make-random-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16519(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_16519,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16522,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1466: ##sys#write-char-0 */
t3=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(45);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k16520 in k16517 in k16511 in chicken.compiler.support#make-random-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16522(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_16522,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16525,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16536,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1468: chicken.time#current-seconds */
t4=*((C_word*)lf[458]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k16523 in k16520 in k16517 in k16511 in chicken.compiler.support#make-random-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16525(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_16525,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16528,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1466: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_random_fixnum(C_fix(1000));
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k16526 in k16523 in k16520 in k16517 in k16511 in chicken.compiler.support#make-random-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16528(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_16528,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_16531,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:1466: chicken.base#get-output-string */
t3=*((C_word*)lf[324]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k16529 in k16526 in k16523 in k16520 in k16517 in k16511 in chicken.compiler.support#make-random-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16531(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_16531,c,av);}
/* support.scm:1465: scheme#string->symbol */
t2=*((C_word*)lf[457]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k16534 in k16520 in k16517 in k16511 in chicken.compiler.support#make-random-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16536(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_16536,c,av);}
/* support.scm:1466: ##sys#print */
t2=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k16538 in k16511 in chicken.compiler.support#make-random-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16540(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_16540,c,av);}
/* support.scm:1466: ##sys#print */
t2=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* chicken.compiler.support#clear-real-name-table! in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16552(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_16552,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_16557,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* support.scm:1484: scheme#make-vector */
t3=*((C_word*)lf[294]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_fix(997);
av2[3]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k16555 in chicken.compiler.support#clear-real-name-table! in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16557(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_16557,c,av);}
t2=C_mutate(&lf[459] /* (set! chicken.compiler.support#real-name-table ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.compiler.support#set-real-name! in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16559(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_16559,c,av);}
/* support.scm:1487: chicken.internal#hash-table-set! */
t4=*((C_word*)lf[132]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=lf[459];
av2[3]=t2;
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* chicken.compiler.support#get-real-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16565(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_16565,c,av);}
/* support.scm:1492: chicken.internal#hash-table-ref */
t3=*((C_word*)lf[128]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=lf[459];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* chicken.compiler.support#real-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16572(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +8,c,3)))){
C_save_and_reclaim((void*)f_16572,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+8);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
t4=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16575,tmp=(C_word)a,a+=2,tmp);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_16591,a[2]=t1,a[3]=t2,a[4]=t3,a[5]=t4,tmp=(C_word)a,a+=6,tmp);
/* support.scm:1504: resolve */
f_16575(t5,t2);}

/* resolve in chicken.compiler.support#real-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_16575(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_16575,2,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16579,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1499: chicken.internal#hash-table-ref */
t4=*((C_word*)lf[128]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[459];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k16577 in resolve in chicken.compiler.support#real-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16579(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_16579,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16585,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1501: chicken.internal#hash-table-ref */
t3=*((C_word*)lf[128]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[459];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k16583 in k16577 in resolve in chicken.compiler.support#real-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16585(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_16585,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(t1)?t1:((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k16589 in chicken.compiler.support#real-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16591(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_16591,c,av);}
a=C_alloc(6);
if(C_truep(C_i_not(t1))){
/* support.scm:1505: ##sys#symbol->string */
t2=*((C_word*)lf[180]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[4]))){
t2=C_u_i_car(((C_word*)t0)[4]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_16687,a[2]=t2,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* support.scm:1508: ##sys#symbol->string */
t4=*((C_word*)lf[180]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
/* support.scm:1522: ##sys#symbol->string */
t2=*((C_word*)lf[180]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}}}

/* k16616 in k16685 in k16589 in chicken.compiler.support#real-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16618(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,5)))){
C_save_and_reclaim((void *)f_16618,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16620,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];
f_16620(t5,((C_word*)t0)[4],((C_word*)t0)[5],C_fix(0),t1);}

/* loop in k16616 in k16685 in k16589 in chicken.compiler.support#real-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_16620(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_16620,5,t0,t1,t2,t3,t4);}
a=C_alloc(8);
if(C_truep(C_i_greaterp(t3,C_fix(20)))){
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_16634,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t6=C_a_i_cons(&a,2,lf[465],t2);
/* support.scm:1513: scheme#reverse */
t7=*((C_word*)lf[80]+1);{
C_word av2[3];
av2[0]=t7;
av2[1]=t5;
av2[2]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}
else{
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_16644,a[2]=t4,a[3]=t1,a[4]=t2,a[5]=t3,a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[3],tmp=(C_word)a,a+=8,tmp);
/* support.scm:1515: resolve */
f_16575(t5,t4);}
else{
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_16683,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* support.scm:1521: scheme#reverse */
t6=*((C_word*)lf[80]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}}}

/* k16632 in loop in k16616 in k16685 in k16589 in chicken.compiler.support#real-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16634(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_16634,c,av);}
/* support.scm:1513: chicken.string#string-intersperse */
t2=*((C_word*)lf[463]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[464];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k16642 in loop in k16616 in k16685 in k16589 in chicken.compiler.support#real-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16644(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_16644,c,av);}
a=C_alloc(8);
t2=C_eqp(t1,((C_word*)t0)[2]);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_16657,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
/* support.scm:1517: scheme#reverse */
t4=*((C_word*)lf[80]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_16676,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[2],tmp=(C_word)a,a+=8,tmp);
/* support.scm:1518: scheme#symbol->string */
t4=*((C_word*)lf[232]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k16655 in k16642 in loop in k16616 in k16685 in k16589 in chicken.compiler.support#real-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_16657,c,av);}
/* support.scm:1517: chicken.string#string-intersperse */
t2=*((C_word*)lf[463]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[466];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k16670 in k16674 in k16642 in loop in k16616 in k16685 in k16589 in chicken.compiler.support#real-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16672(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_16672,c,av);}
/* support.scm:1518: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_16620(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],t1);}

/* k16674 in k16642 in loop in k16616 in k16685 in k16589 in chicken.compiler.support#real-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16676(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_16676,c,av);}
a=C_alloc(9);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
t3=C_fixnum_plus(((C_word*)t0)[3],C_fix(1));
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_16672,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* support.scm:1520: db-get */
t5=*((C_word*)lf[127]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[6];
av2[3]=((C_word*)t0)[7];
av2[4]=lf[467];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k16681 in loop in k16616 in k16685 in k16589 in chicken.compiler.support#real-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16683(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_16683,c,av);}
/* support.scm:1521: chicken.string#string-intersperse */
t2=*((C_word*)lf[463]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[468];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k16685 in k16589 in chicken.compiler.support#real-name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16687(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_16687,c,av);}
a=C_alloc(9);
t2=C_a_i_list1(&a,1,t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_16618,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:1510: db-get */
t4=*((C_word*)lf[127]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
av2[3]=((C_word*)t0)[5];
av2[4]=lf[467];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* chicken.compiler.support#real-name2 in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16692(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_16692,c,av);}
a=C_alloc(4);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16696,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1525: chicken.internal#hash-table-ref */
t5=*((C_word*)lf[128]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[459];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k16694 in chicken.compiler.support#real-name2 in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16696(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_16696,c,av);}
if(C_truep(t1)){
/* support.scm:1526: real-name */
t2=*((C_word*)lf[181]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* chicken.compiler.support#display-real-name-table in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16704(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,4)))){
C_save_and_reclaim((void *)f_16704,c,av);}
a=C_alloc(2);
t2=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16710,tmp=(C_word)a,a+=2,tmp);
/* support.scm:1529: chicken.internal#hash-table-for-each */
t3=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
av2[3]=lf[459];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* a16709 in chicken.compiler.support#display-real-name-table in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16710(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_16710,c,av);}
a=C_alloc(5);
t4=*((C_word*)lf[24]+1);
t5=*((C_word*)lf[24]+1);
t6=C_i_check_port_2(*((C_word*)lf[24]+1),C_fix(2),C_SCHEME_TRUE,lf[25]);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16717,a[2]=t1,a[3]=t4,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* support.scm:1531: ##sys#print */
t8=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=t2;
av2[3]=C_SCHEME_TRUE;
av2[4]=*((C_word*)lf[24]+1);
((C_proc)(void*)(*((C_word*)t8+1)))(5,av2);}}

/* k16715 in a16709 in chicken.compiler.support#display-real-name-table in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16717(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_16717,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16720,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:1531: ##sys#write-char-0 */
t3=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(9);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k16718 in k16715 in a16709 in chicken.compiler.support#display-real-name-table in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16720(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_16720,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16723,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1531: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k16721 in k16718 in k16715 in a16709 in chicken.compiler.support#display-real-name-table in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16723(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_16723,c,av);}
/* support.scm:1531: ##sys#write-char-0 */
t2=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.compiler.support#source-info->string in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16728(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_16728,c,av);}
a=C_alloc(13);
if(C_truep(C_i_listp(t2))){
t3=C_i_car(t2);
t4=C_i_cadr(t2);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16748,a[2]=t1,a[3]=t3,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_16752,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
t7=C_i_string_length(t3);
t8=C_a_i_fixnum_difference(&a,2,C_fix(4),t7);
/* support.scm:1538: scheme#max */
t9=*((C_word*)lf[476]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t9;
av2[1]=t6;
av2[2]=C_fix(0);
av2[3]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(4,av2);}}
else{
/* support.scm:1539: chicken.string#->string */
t3=*((C_word*)lf[66]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k16746 in chicken.compiler.support#source-info->string in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16748(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_16748,c,av);}
/* support.scm:1538: chicken.string#conc */
t2=*((C_word*)lf[472]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[473];
av2[4]=t1;
av2[5]=lf[474];
av2[6]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(7,av2);}}

/* k16750 in chicken.compiler.support#source-info->string in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16752(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_16752,c,av);}
/* support.scm:1538: scheme#make-string */
t2=*((C_word*)lf[475]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_make_character(32);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.compiler.support#source-info->name in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16763(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_16763,c,av);}
if(C_truep(C_i_listp(t2))){
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_cadr(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
/* support.scm:1542: chicken.string#->string */
t3=*((C_word*)lf[66]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* chicken.compiler.support#source-info->line in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16778(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_16778,c,av);}
t3=C_i_listp(t2);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=(C_truep(t3)?C_i_car(t2):C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.compiler.support#call-info in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16790(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_16790,c,av);}
a=C_alloc(4);
t4=C_i_cdr(t2);
t5=C_i_pairp(t4);
t6=(C_truep(t5)?C_i_cadr(t2):C_SCHEME_FALSE);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16797,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
if(C_truep(t6)){
if(C_truep(C_i_listp(t6))){
t8=C_i_car(t6);
t9=C_i_cadr(t6);
/* support.scm:1552: chicken.string#conc */
t10=*((C_word*)lf[472]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t10;
av2[1]=t7;
av2[2]=lf[480];
av2[3]=t8;
av2[4]=lf[481];
av2[5]=t3;
((C_proc)(void*)(*((C_word*)t10+1)))(6,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k16795 in chicken.compiler.support#call-info in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16797(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_16797,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(t1)?t1:((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16827(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,3)))){
C_save_and_reclaim((void *)f_16827,c,av);}
a=C_alloc(17);
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=C_i_check_list_2(t3,lf[125]);
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16851,a[2]=t2,a[3]=t4,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_17023,a[2]=t7,a[3]=t12,a[4]=t8,tmp=(C_word)a,a+=5,tmp));
t14=((C_word*)t12)[1];
f_17023(t14,t10,t3);}

/* k16849 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16851(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_16851,c,av);}
a=C_alloc(18);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(t1,lf[125]);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_16987,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16989,a[2]=t4,a[3]=t9,a[4]=t5,tmp=(C_word)a,a+=5,tmp));
t11=((C_word*)t9)[1];
f_16989(t11,t7,t1);}

/* k16864 in k16985 in k16849 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16866(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_16866,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16869,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:1564: g4177 */
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k16867 in k16864 in k16985 in k16849 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16869(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_16869,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_16875,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* support.scm:1565: chicken.condition#condition? */
t3=*((C_word*)lf[486]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k16873 in k16867 in k16864 in k16985 in k16849 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16875(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_16875,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
/* support.scm:1565: k */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=C_SCHEME_FALSE;
av2[3]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_16884,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=C_i_length(((C_word*)t0)[4]);
t4=C_eqp(C_fix(1),t3);
if(C_truep(t4)){
/* support.scm:1567: encodeable-literal? */
t5=lf[485];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=C_i_car(((C_word*)t0)[4]);
f_17156(3,av2);}}
else{
t5=t2;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
f_16884(2,av2);}}}}

/* k16882 in k16873 in k16867 in k16864 in k16985 in k16849 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16884(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_16884,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16887,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:1568: debugging */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[190];
av2[3]=lf[483];
av2[4]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
t2=C_eqp(C_fix(1),C_u_i_length(((C_word*)t0)[4]));
if(C_truep(t2)){
/* support.scm:1571: k */
t3=((C_word*)t0)[2];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=C_SCHEME_FALSE;
av2[3]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
/* support.scm:1573: bomb */
t3=*((C_word*)lf[13]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[484];
av2[3]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}}

/* k16885 in k16882 in k16873 in k16867 in k16864 in k16985 in k16849 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16887(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_16887,c,av);}
/* support.scm:1569: k */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=C_SCHEME_TRUE;
av2[3]=C_i_car(((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* a16925 in k16985 in k16849 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16926(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_16926,c,av);}
a=C_alloc(8);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_16932,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16941,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:1564: chicken.condition#with-exception-handler */
t5=*((C_word*)lf[106]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=t3;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* a16931 in a16925 in k16985 in k16849 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16932(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_16932,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_16938,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* support.scm:1564: k4174 */
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* a16937 in a16931 in a16925 in k16985 in k16849 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16938(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_16938,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a16940 in a16925 in k16985 in k16849 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16941(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_16941,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16947,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_16959,a[2]=((C_word*)t0)[4],tmp=(C_word)a,a+=3,tmp);
/* support.scm:1564: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a16946 in a16940 in a16925 in k16985 in k16849 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16947(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_16947,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16953,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1564: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=t2;
av2[3]=*((C_word*)lf[487]+1);
C_call_with_values(4,av2);}}

/* a16952 in a16946 in a16940 in a16925 in k16985 in k16849 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16953(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_16953,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=((C_word*)t0)[3];
C_apply(4,av2);}}

/* a16958 in a16940 in a16925 in k16985 in k16849 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16959(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +3,c,2)))){
C_save_and_reclaim((void*)f_16959,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+3);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_16965,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* support.scm:1564: k4174 */
t4=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* a16964 in a16958 in a16940 in a16925 in k16985 in k16849 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16965(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_16965,c,av);}{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
C_apply_values(3,av2);}}

/* k16985 in k16849 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_16987(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_16987,c,av);}
a=C_alloc(12);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_slot(((C_word*)t0)[2],C_fix(0));
if(C_truep(C_i_closurep(t3))){
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_16866,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_16926,a[2]=t3,a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1564: scheme#call-with-current-continuation */
t6=*((C_word*)lf[107]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t4;
av2[2]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
/* support.scm:1574: bomb */
t4=*((C_word*)lf[13]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[4];
av2[2]=lf[488];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* map-loop4144 in k16849 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_16989(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_16989,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_a_i_list(&a,2,lf[85],t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* map-loop4113 in chicken.compiler.support#constant-form-eval in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_17023(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_17023,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_slot(t3,C_fix(2));
t5=C_i_car(t4);
t6=C_a_i_cons(&a,2,t5,C_SCHEME_END_OF_LIST);
t7=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t6);
t8=C_mutate(((C_word *)((C_word*)t0)[2])+1,t6);
t10=t1;
t11=C_slot(t2,C_fix(1));
t1=t10;
t2=t11;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.compiler.support#maybe-constant-fold-call in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17057(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,4)))){
C_save_and_reclaim((void *)f_17057,c,av);}
a=C_alloc(14);
t5=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17060,tmp=(C_word)a,a+=2,tmp);
t6=C_i_car(t3);
t7=C_slot(t6,C_fix(1));
t8=C_eqp(lf[154],t7);
if(C_truep(t8)){
t9=C_slot(C_u_i_car(t3),C_fix(2));
t10=C_i_car(t9);
t11=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_17088,a[2]=t3,a[3]=t4,a[4]=t1,a[5]=t10,tmp=(C_word)a,a+=6,tmp);
t12=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_17110,a[2]=t11,a[3]=t5,a[4]=t3,a[5]=t10,tmp=(C_word)a,a+=6,tmp);
/* tweaks.scm:51: ##sys#get */
t13=*((C_word*)lf[182]+1);{
C_word *av2=av;
av2[0]=t13;
av2[1]=t12;
av2[2]=t10;
av2[3]=lf[492];
((C_proc)(void*)(*((C_word*)t13+1)))(4,av2);}}
else{
/* support.scm:1586: k */
t9=t4;{
C_word *av2=av;
av2[0]=t9;
av2[1]=t1;
av2[2]=C_SCHEME_FALSE;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}}

/* constant-node? in chicken.compiler.support#maybe-constant-fold-call in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17060(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_17060,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_eqp(lf[85],C_slot(t2,C_fix(1)));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k17086 in chicken.compiler.support#maybe-constant-fold-call in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17088(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_17088,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17095,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
/* support.scm:1584: constant-form-eval */
t4=*((C_word*)lf[482]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[4];
av2[2]=((C_word*)t0)[5];
av2[3]=t2;
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
/* support.scm:1585: k */
t2=((C_word*)t0)[3];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
av2[2]=C_SCHEME_FALSE;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}

/* a17094 in k17086 in chicken.compiler.support#maybe-constant-fold-call in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17095(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_17095,c,av);}
/* support.scm:1584: k */
t4=((C_word*)t0)[2];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
av2[4]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k17108 in chicken.compiler.support#maybe-constant-fold-call in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17110(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_17110,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_17116,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* support.scm:1581: foldable? */
t3=*((C_word*)lf[491]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_17088(2,av2);}}}

/* k17114 in k17108 in chicken.compiler.support#maybe-constant-fold-call in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17116(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_17116,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_17119,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t1)){
if(C_truep(t1)){
/* support.scm:1583: every */
f_5683(((C_word*)t0)[2],((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]));}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_17088(2,av2);}}}
else{
/* support.scm:1582: predicate? */
t3=*((C_word*)lf[490]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k17117 in k17114 in k17108 in chicken.compiler.support#maybe-constant-fold-call in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17119(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_17119,c,av);}
if(C_truep(t1)){
/* support.scm:1583: every */
f_5683(((C_word*)t0)[2],((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]));}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_17088(2,av2);}}}

/* chicken.compiler.support#encodeable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17156(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_17156,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17171,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1596: immediate? */
t4=*((C_word*)lf[89]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k17169 in chicken.compiler.support#encodeable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17171(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_17171,c,av);}
a=C_alloc(13);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_i_exact_integerp(((C_word*)t0)[3]))){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17181,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17196,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
/* support.scm:1601: scheme#call-with-current-continuation */
t4=*((C_word*)lf[107]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t2=C_i_flonump(((C_word*)t0)[3]);
if(C_truep(t2)){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
if(C_truep(C_i_symbolp(((C_word*)t0)[3]))){
t3=C_slot(((C_word*)t0)[3],C_fix(1));
t4=C_i_string_length(t3);
t5=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_fixnum_less_or_equal_p(C_i_integer_length(t4),C_fix(24));
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
if(C_truep(C_byteblockp(((C_word*)t0)[3]))){
t3=stub4226(C_SCHEME_UNDEFINED,((C_word*)t0)[3]);
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_fixnum_less_or_equal_p(C_i_integer_length(t3),C_fix(24));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=stub4226(C_SCHEME_UNDEFINED,((C_word*)t0)[3]);
if(C_truep(C_fixnum_less_or_equal_p(C_i_integer_length(t3),C_fix(24)))){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17285,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17287,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6331,a[2]=t3,a[3]=t5,a[4]=t7,tmp=(C_word)a,a+=5,tmp));
t9=((C_word*)t7)[1];
f_6331(t9,t4,C_fix(0));}
else{
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}}}}}}

/* k17179 in k17169 in chicken.compiler.support#encodeable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17181(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_17181,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17184,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:1601: g4235 */
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k17182 in k17179 in k17169 in chicken.compiler.support#encodeable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17184(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_17184,c,av);}
if(C_truep(t1)){
t2=C_i_string_length(t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_fixnum_less_or_equal_p(C_i_integer_length(t2),C_fix(24));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* a17195 in k17169 in chicken.compiler.support#encodeable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17196(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_17196,c,av);}
a=C_alloc(7);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17202,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17211,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1601: chicken.condition#with-exception-handler */
t5=*((C_word*)lf[106]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=t3;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* a17201 in a17195 in k17169 in chicken.compiler.support#encodeable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17202(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,2)))){
C_save_and_reclaim((void *)f_17202,c,av);}
a=C_alloc(2);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17208,tmp=(C_word)a,a+=2,tmp);
/* support.scm:1601: k4232 */
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* a17207 in a17201 in a17195 in k17169 in chicken.compiler.support#encodeable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17208(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_17208,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a17210 in a17195 in k17169 in chicken.compiler.support#encodeable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17211(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_17211,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17213,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17220,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17237,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* tmp15275 */
t5=t2;
f_17213(t5,t4);}

/* tmp15275 in a17210 in a17195 in k17169 in chicken.compiler.support#encodeable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_17213(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_17213,2,t0,t1);}
/* ##sys#number->string */
t2=*((C_word*)lf[493]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(16);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* tmp25276 in a17210 in a17195 in k17169 in chicken.compiler.support#encodeable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_17220(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_17220,3,t0,t1,t2);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17226,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* support.scm:1601: k4232 */
t4=((C_word*)t0)[2];{
C_word av2[3];
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* a17225 in tmp25276 in a17210 in a17195 in k17169 in chicken.compiler.support#encodeable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17226(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_17226,c,av);}{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
C_apply_values(3,av2);}}

/* k17235 in a17210 in a17195 in k17169 in chicken.compiler.support#encodeable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17237(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_17237,c,av);}
a=C_alloc(3);
/* tmp25276 */
t2=((C_word*)t0)[2];
f_17220(t2,((C_word*)t0)[3],C_a_i_list(&a,1,t1));}

/* k17283 in k17169 in chicken.compiler.support#encodeable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17285(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_17285,c,av);}
/* support.scm:1612: every */
f_5683(((C_word*)t0)[2],lf[485],t1);}

/* a17286 in k17169 in chicken.compiler.support#encodeable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static C_word C_fcall f_17287(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_slot(((C_word*)t0)[2],t1));}

/* chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17293(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_17293,c,av);}
a=C_alloc(8);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17297,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17302,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t7=((C_word*)t5)[1];
f_17302(t7,t3,C_fix(0),t2);}

/* k17295 in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17297(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_17297,c,av);}
/* support.scm:1637: scheme#newline */
t2=*((C_word*)lf[23]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_17302(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_17302,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=C_slot(t3,C_fix(1));
t5=C_slot(t3,C_fix(2));
t6=C_slot(t3,C_fix(3));
t7=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_17330,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t6,a[5]=t3,a[6]=t1,a[7]=t5,a[8]=t4,tmp=(C_word)a,a+=9,tmp);
/* support.scm:1625: scheme#make-string */
t8=*((C_word*)lf[475]+1);{
C_word av2[4];
av2[0]=t8;
av2[1]=t7;
av2[2]=t2;
av2[3]=C_make_character(32);
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}

/* k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17330(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_17330,c,av);}
a=C_alloc(16);
t2=C_a_i_fixnum_plus(&a,2,((C_word*)t0)[2],C_fix(2));
t3=*((C_word*)lf[24]+1);
t4=*((C_word*)lf[24]+1);
t5=C_i_check_port_2(*((C_word*)lf[24]+1),C_fix(2),C_SCHEME_TRUE,lf[25]);
t6=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_17338,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t3,a[9]=((C_word*)t0)[8],a[10]=t1,tmp=(C_word)a,a+=11,tmp);
/* support.scm:1627: ##sys#write-char-0 */
t7=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=C_make_character(10);
av2[3]=*((C_word*)lf[24]+1);
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}

/* k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17338(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_17338,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_17341,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
/* support.scm:1627: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[10];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k17339 in k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17341(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_17341,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_17344,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
/* support.scm:1627: ##sys#write-char-0 */
t3=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(60);
av2[3]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k17342 in k17339 in k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17344(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_17344,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_17347,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
/* support.scm:1627: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[9];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k17345 in k17342 in k17339 in k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17347(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_17347,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_17350,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
/* support.scm:1627: ##sys#write-char-0 */
t3=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(32);
av2[3]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k17348 in k17345 in k17342 in k17339 in k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17350(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_17350,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_17353,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* support.scm:1627: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k17351 in k17348 in k17345 in k17342 in k17339 in k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_17353(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_17353,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17354,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_i_check_list_2(((C_word*)t0)[4],lf[44]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17364,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17426,a[2]=t6,a[3]=t2,tmp=(C_word)a,a+=4,tmp));
t8=((C_word*)t6)[1];
f_17426(t8,t4,((C_word*)t0)[4]);}

/* g4275 in k17351 in k17348 in k17345 in k17342 in k17339 in k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_fcall f_17354(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_17354,3,t0,t1,t2);}
/* support.scm:1628: g4297 */
t3=((C_word*)((C_word*)t0)[2])[1];
f_17302(t3,t1,((C_word*)t0)[3],t2);}

/* k17362 in k17351 in k17348 in k17345 in k17342 in k17339 in k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_ccall f_17364(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_17364,c,av);}
a=C_alloc(9);
t2=C_block_size(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17370,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_fixnum_greaterp(t2,C_fix(4)))){
t4=*((C_word*)lf[24]+1);
t5=*((C_word*)lf[24]+1);
t6=C_i_check_port_2(*((C_word*)lf[24]+1),C_fix(2),C_SCHEME_TRUE,lf[25]);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_17382,a[2]=t3,a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=t4,tmp=(C_word)a,a+=6,tmp);
/* support.scm:1631: ##sys#write-char-0 */
t8=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=C_make_character(91);
av2[3]=*((C_word*)lf[24]+1);
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}
else{
/* ##sys#write-char/port */
t4=*((C_word*)lf[495]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=C_make_character(62);
av2[3]=*((C_word*)lf[24]+1);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* k17368 in k17362 in k17351 in k17348 in k17345 in k17342 in k17339 in k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in ... */
static void C_ccall f_17370(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_17370,c,av);}
/* ##sys#write-char/port */
t2=*((C_word*)lf[495]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(62);
av2[3]=*((C_word*)lf[24]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k17380 in k17362 in k17351 in k17348 in k17345 in k17342 in k17339 in k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in ... */
static void C_ccall f_17382(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_17382,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_17385,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:1631: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_slot(((C_word*)t0)[4],C_fix(4));
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k17383 in k17380 in k17362 in k17351 in k17348 in k17345 in k17342 in k17339 in k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in ... */
static void C_ccall f_17385(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_17385,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17388,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_17393,a[2]=((C_word*)t0)[3],a[3]=t4,a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp));
t6=((C_word*)t4)[1];
f_17393(t6,t2,C_fix(5));}

/* k17386 in k17383 in k17380 in k17362 in k17351 in k17348 in k17345 in k17342 in k17339 in k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in ... */
static void C_ccall f_17388(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_17388,c,av);}
/* ##sys#write-char/port */
t2=*((C_word*)lf[495]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(93);
av2[3]=*((C_word*)lf[24]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* doloop4311 in k17383 in k17380 in k17362 in k17351 in k17348 in k17345 in k17342 in k17339 in k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in ... */
static void C_fcall f_17393(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_17393,3,t0,t1,t2);}
a=C_alloc(7);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=*((C_word*)lf[24]+1);
t4=*((C_word*)lf[24]+1);
t5=C_i_check_port_2(*((C_word*)lf[24]+1),C_fix(2),C_SCHEME_TRUE,lf[25]);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_17406,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=((C_word*)t0)[4],a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* support.scm:1634: ##sys#write-char-0 */
t7=*((C_word*)lf[26]+1);{
C_word av2[4];
av2[0]=t7;
av2[1]=t6;
av2[2]=C_make_character(32);
av2[3]=*((C_word*)lf[24]+1);
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}}

/* k17404 in doloop4311 in k17383 in k17380 in k17362 in k17351 in k17348 in k17345 in k17342 in k17339 in k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in ... */
static void C_ccall f_17406(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_17406,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_17409,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:1634: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_slot(((C_word*)t0)[5],((C_word*)t0)[4]);
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k17407 in k17404 in doloop4311 in k17383 in k17380 in k17362 in k17351 in k17348 in k17345 in k17342 in k17339 in k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in ... */
static void C_ccall f_17409(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_17409,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_17393(t2,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)));}

/* for-each-loop4274 in k17351 in k17348 in k17345 in k17342 in k17339 in k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in ... */
static void C_fcall f_17426(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_17426,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_17436,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:1628: g4275 */
t4=((C_word*)t0)[3];
f_17354(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k17434 in for-each-loop4274 in k17351 in k17348 in k17345 in k17342 in k17339 in k17336 in k17328 in loop in chicken.compiler.support#dump-nodes in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in ... */
static void C_ccall f_17436(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_17436,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_17426(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* chicken.compiler.support#read-info-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17449(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_17449,c,av);}
a=C_alloc(9);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17453,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t6=C_eqp(lf[497],t2);
t7=(C_truep(t6)?C_i_symbolp(C_i_car(t3)):C_SCHEME_FALSE);
if(C_truep(t7)){
t8=C_i_car(t3);
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_17471,a[2]=t3,a[3]=t5,a[4]=t8,tmp=(C_word)a,a+=5,tmp);
/* support.scm:1648: chicken.string#conc */
t10=*((C_word*)lf[472]+1);{
C_word *av2=av;
av2[0]=t10;
av2[1]=t9;
av2[2]=*((C_word*)lf[498]+1);
av2[3]=lf[499];
av2[4]=t4;
((C_proc)(void*)(*((C_word*)t10+1)))(5,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k17451 in chicken.compiler.support#read-info-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17453(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_17453,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k17469 in chicken.compiler.support#read-info-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17471(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_17471,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_17475,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* support.scm:1649: chicken.internal#hash-table-ref */
t3=*((C_word*)lf[128]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=*((C_word*)lf[136]+1);
av2[3]=C_u_i_car(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k17473 in k17469 in chicken.compiler.support#read-info-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17475(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_17475,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_cons(&a,2,t2,t1);
/* support.scm:1644: chicken.internal#hash-table-set! */
t4=*((C_word*)lf[132]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[4];
av2[2]=*((C_word*)lf[136]+1);
av2[3]=((C_word*)t0)[5];
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_cons(&a,2,t2,C_SCHEME_END_OF_LIST);
/* support.scm:1644: chicken.internal#hash-table-set! */
t4=*((C_word*)lf[132]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[4];
av2[2]=*((C_word*)lf[136]+1);
av2[3]=((C_word*)t0)[5];
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}

/* chicken.compiler.support#read/source-info in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17492(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_17492,c,av);}
/* support.scm:1654: ##sys#read */
t3=*((C_word*)lf[501]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
av2[3]=*((C_word*)lf[496]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* ##sys#user-read-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17498(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_17498,c,av);}
a=C_alloc(4);
if(C_truep(C_i_char_equalp(C_make_character(62),t2))){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17508,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* ##sys#read-char/port */
t5=*((C_word*)lf[506]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
/* support.scm:1666: old-hook */
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* k17506 in ##sys#user-read-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17508(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_17508,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17511,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17527,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1669: chicken.base#open-output-string */
t4=*((C_word*)lf[327]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k17509 in k17506 in ##sys#user-read-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17511(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,1)))){
C_save_and_reclaim((void *)f_17511,c,av);}
a=C_alloc(12);
t2=C_a_i_list(&a,2,lf[503],t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,2,lf[504],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k17525 in k17506 in ##sys#user-read-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17527(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_17527,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_17532,a[2]=t3,a[3]=t1,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];
f_17532(t5,((C_word*)t0)[3]);}

/* loop in k17525 in k17506 in ##sys#user-read-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_17532(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_17532,2,t0,t1);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_17536,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* ##sys#read-char/port */
t3=*((C_word*)lf[506]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k17534 in loop in k17525 in k17506 in ##sys#user-read-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17536(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_17536,c,av);}
a=C_alloc(5);
if(C_truep(C_eofp(t1))){
/* support.scm:1673: quit-compiling */
t2=*((C_word*)lf[37]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[505];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
if(C_truep(C_u_i_char_equalp(t1,C_make_character(10)))){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17553,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1675: scheme#newline */
t3=*((C_word*)lf[23]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
if(C_truep(C_u_i_char_equalp(t1,C_make_character(60)))){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_17564,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* ##sys#read-char/port */
t3=*((C_word*)lf[506]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17587,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* ##sys#write-char/port */
t3=*((C_word*)lf[495]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}}}

/* k17551 in k17534 in loop in k17525 in k17506 in ##sys#user-read-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17553(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_17553,c,av);}
/* support.scm:1676: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_17532(t2,((C_word*)t0)[3]);}

/* k17562 in k17534 in loop in k17525 in k17506 in ##sys#user-read-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17564(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_17564,c,av);}
a=C_alloc(6);
t2=C_eqp(C_make_character(35),t1);
if(C_truep(t2)){
/* support.scm:1680: chicken.base#get-output-string */
t3=*((C_word*)lf[324]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_17576,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* ##sys#write-char/port */
t4=*((C_word*)lf[495]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_make_character(60);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* k17574 in k17562 in k17534 in loop in k17525 in k17506 in ##sys#user-read-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17576(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_17576,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17579,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* ##sys#write-char/port */
t3=*((C_word*)lf[495]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k17577 in k17574 in k17562 in k17534 in loop in k17525 in k17506 in ##sys#user-read-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17579(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_17579,c,av);}
/* support.scm:1684: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_17532(t2,((C_word*)t0)[3]);}

/* k17585 in k17534 in loop in k17525 in k17506 in ##sys#user-read-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17587(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_17587,c,av);}
/* support.scm:1687: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_17532(t2,((C_word*)t0)[3]);}

/* chicken.compiler.support#big-fixnum? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17592(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_17592,c,av);}
a=C_alloc(4);
if(C_truep(C_fixnump(t2))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17605,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1694: chicken.platform#feature? */
t4=*((C_word*)lf[507]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[508];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k17603 in chicken.compiler.support#big-fixnum? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17605(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_17605,c,av);}
if(C_truep(t1)){
t2=C_fixnum_greaterp(((C_word*)t0)[2],C_fix(1073741823));
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(t2)?t2:C_fixnum_lessp(((C_word*)t0)[2],C_fix(-1073741824)));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* chicken.compiler.support#small-bignum? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17616(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_17616,c,av);}
a=C_alloc(4);
if(C_truep(C_i_bignump(t2))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17638,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1700: chicken.platform#feature? */
t4=*((C_word*)lf[507]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[508];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k17636 in chicken.compiler.support#small-bignum? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17638(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_17638,c,av);}
t2=C_i_not(t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(t2)?C_fixnum_less_or_equal_p(C_i_integer_length(((C_word*)t0)[3]),C_fix(62)):C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.compiler.support#hide-variable in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17640(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_17640,c,av);}
a=C_alloc(3);
t3=C_a_i_list(&a,1,lf[510]);
if(C_truep(C_i_nullp(t3))){
/* tweaks.scm:57: ##sys#put! */
t4=*((C_word*)lf[255]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=lf[273];
av2[4]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
/* tweaks.scm:57: ##sys#put! */
t4=*((C_word*)lf[255]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=lf[273];
av2[4]=C_i_car(t3);
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}

/* chicken.compiler.support#export-variable in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17660(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_17660,c,av);}
a=C_alloc(3);
t3=C_a_i_list(&a,1,lf[512]);
if(C_truep(C_i_nullp(t3))){
/* tweaks.scm:57: ##sys#put! */
t4=*((C_word*)lf[255]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=lf[273];
av2[4]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
/* tweaks.scm:57: ##sys#put! */
t4=*((C_word*)lf[255]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=lf[273];
av2[4]=C_i_car(t3);
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}

/* chicken.compiler.support#variable-hidden? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17680(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_17680,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17688,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* support.scm:1713: ##sys#get */
t4=*((C_word*)lf[182]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[273];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k17686 in chicken.compiler.support#variable-hidden? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17688(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_17688,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(t1,lf[510]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k17695 in ##sys#toplevel-definition-hook in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17697(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_17697,c,av);}
if(C_truep(t1)){
/* support.scm:1716: chicken.plist#remprop! */
t2=*((C_word*)lf[272]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[273];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.compiler.support#variable-visible? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17702(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_17702,c,av);}
a=C_alloc(4);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17706,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1719: ##sys#get */
t5=*((C_word*)lf[182]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[273];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k17704 in chicken.compiler.support#variable-visible? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17706(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_17706,c,av);}
t2=C_eqp(t1,lf[510]);
if(C_truep(t2)){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_eqp(t1,lf[512]);
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=(C_truep(t3)?C_SCHEME_TRUE:C_i_not(((C_word*)t0)[3]));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* chicken.compiler.support#mark-variable in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17727(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_17727,c,av);}
if(C_truep(C_rest_nullp(c,4))){
/* support.scm:1729: ##sys#put! */
t4=*((C_word*)lf[255]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
av2[4]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
/* support.scm:1729: ##sys#put! */
t4=*((C_word*)lf[255]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
av2[4]=C_get_rest_arg(c,4,av,4,t0);
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}

/* chicken.compiler.support#variable-mark in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17742(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_17742,c,av);}
/* support.scm:1732: ##sys#get */
t4=*((C_word*)lf[182]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* chicken.compiler.support#intrinsic? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17748(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_17748,c,av);}
/* tweaks.scm:60: ##sys#get */
t3=*((C_word*)lf[182]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
av2[3]=lf[492];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* chicken.compiler.support#foldable? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17759(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_17759,c,av);}
/* tweaks.scm:60: ##sys#get */
t3=*((C_word*)lf[182]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
av2[3]=lf[516];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* chicken.compiler.support#predicate? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17770(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_17770,c,av);}
/* tweaks.scm:60: ##sys#get */
t3=*((C_word*)lf[182]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
av2[3]=lf[517];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* chicken.compiler.support#load-identifier-database in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17781(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_17781,c,av);}
a=C_alloc(7);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17785,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17874,a[2]=t3,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1743: chicken.platform#repository-path */
t5=*((C_word*)lf[525]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k17783 in chicken.compiler.support#load-identifier-database in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17785(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_17785,c,av);}
a=C_alloc(8);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17791,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17852,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1744: chicken.base#open-output-string */
t4=*((C_word*)lf[327]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k17789 in k17783 in chicken.compiler.support#load-identifier-database in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17791(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_17791,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17819,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:1751: scheme#call-with-input-file */
t3=*((C_word*)lf[520]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=*((C_word*)lf[82]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k17801 in for-each-loop4487 in k17817 in k17789 in k17783 in chicken.compiler.support#load-identifier-database in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17803(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_17803,c,av);}
/* support.scm:1748: ##sys#put! */
t2=*((C_word*)lf[255]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[519];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k17805 in for-each-loop4487 in k17817 in k17789 in k17783 in chicken.compiler.support#load-identifier-database in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17807(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_17807,c,av);}
a=C_alloc(3);
t2=(C_truep(t1)?t1:C_SCHEME_END_OF_LIST);
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_a_i_list1(&a,1,t3);
/* support.scm:1750: scheme#append */
t5=*((C_word*)lf[58]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k17817 in k17789 in k17783 in chicken.compiler.support#load-identifier-database in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17819(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_17819,c,av);}
a=C_alloc(5);
t2=C_i_check_list_2(t1,lf[44]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17827,a[2]=t4,tmp=(C_word)a,a+=3,tmp));
t6=((C_word*)t4)[1];
f_17827(t6,((C_word*)t0)[2],t1);}

/* for-each-loop4487 in k17817 in k17789 in k17783 in chicken.compiler.support#load-identifier-database in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_17827(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,0,3)))){
C_save_and_reclaim_args((void *)trf_17827,3,t0,t1,t2);}
a=C_alloc(13);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_17837,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=C_i_car(t4);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17803,a[2]=t3,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17807,a[2]=t4,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
/* support.scm:1750: ##sys#get */
t8=*((C_word*)lf[182]+1);{
C_word av2[4];
av2[0]=t8;
av2[1]=t7;
av2[2]=t5;
av2[3]=lf[519];
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k17835 in for-each-loop4487 in k17817 in k17789 in k17783 in chicken.compiler.support#load-identifier-database in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17837(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_17837,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_17827(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k17850 in k17783 in chicken.compiler.support#load-identifier-database in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17852(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_17852,c,av);}
a=C_alloc(5);
t2=C_i_check_port_2(t1,C_fix(2),C_SCHEME_TRUE,lf[321]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_17858,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* support.scm:1744: ##sys#print */
t4=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[523];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k17856 in k17850 in k17783 in chicken.compiler.support#load-identifier-database in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17858(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_17858,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17861,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1744: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k17859 in k17856 in k17850 in k17783 in chicken.compiler.support#load-identifier-database in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17861(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_17861,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17864,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1744: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[522];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k17862 in k17859 in k17856 in k17850 in k17783 in chicken.compiler.support#load-identifier-database in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17864(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_17864,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17867,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:1744: ##sys#write-char-0 */
t3=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k17865 in k17862 in k17859 in k17856 in k17850 in k17783 in chicken.compiler.support#load-identifier-database in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17867(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_17867,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17870,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:1744: chicken.base#get-output-string */
t3=*((C_word*)lf[324]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k17868 in k17865 in k17862 in k17859 in k17856 in k17850 in k17783 in chicken.compiler.support#load-identifier-database in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17870(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_17870,c,av);}
/* support.scm:1744: debugging */
t2=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[521];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k17872 in chicken.compiler.support#load-identifier-database in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17874(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_17874,c,av);}
/* support.scm:1743: chicken.load#find-file */
t2=*((C_word*)lf[524]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.compiler.support#print-version in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17876(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_17876,c,av);}
a=C_alloc(6);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?C_SCHEME_FALSE:C_get_rest_arg(c,2,av,2,t0));
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17883,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
if(C_truep(t3)){
/* support.scm:1757: chicken.base#print* */
t5=*((C_word*)lf[527]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[528];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f19580,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* support.scm:1758: chicken.platform#chicken-version */
t6=*((C_word*)lf[242]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}}

/* k17881 in chicken.compiler.support#print-version in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17883(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_17883,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17890,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:1758: chicken.platform#chicken-version */
t3=*((C_word*)lf[242]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k17888 in k17881 in chicken.compiler.support#print-version in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17890(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_17890,c,av);}
/* support.scm:1758: chicken.base#print */
t2=*((C_word*)lf[229]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* chicken.compiler.support#print-usage in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17901(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_17901,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17905,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* support.scm:1763: print-version */
t3=*((C_word*)lf[526]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k17903 in chicken.compiler.support#print-usage in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17905(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_17905,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17908,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:1764: scheme#newline */
t3=*((C_word*)lf[23]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k17906 in k17903 in chicken.compiler.support#print-usage in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17908(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_17908,c,av);}
/* support.scm:1765: scheme#display */
t2=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[530];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* chicken.compiler.support#print-debug-options in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17913(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_17913,c,av);}
/* support.scm:1897: scheme#display */
t2=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[532];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* a17918 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17919(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_17919,c,av);}
a=C_alloc(5);
t4=C_i_check_port_2(t3,C_fix(2),C_SCHEME_TRUE,lf[32]);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_17926,a[2]=t1,a[3]=t3,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:476: ##sys#print */
t6=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[533];
av2[3]=C_SCHEME_FALSE;
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k17924 in a17918 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17926(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_17926,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_17929,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:476: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_slot(((C_word*)t0)[4],C_fix(1));
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k17927 in k17924 in a17918 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17929(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_17929,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_17932,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:476: ##sys#write-char-0 */
t3=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(32);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k17930 in k17927 in k17924 in a17918 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17932(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_17932,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_17935,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:476: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_slot(((C_word*)t0)[4],C_fix(2));
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k17933 in k17930 in k17927 in k17924 in a17918 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_17935(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_17935,c,av);}
/* support.scm:476: ##sys#write-char-0 */
t2=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(62);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k5286 */
static void C_ccall f_5288(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5288,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5291,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_eval_toplevel(2,av2);}}

/* k5289 in k5286 */
static void C_ccall f_5291(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5291,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5294,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_expand_toplevel(2,av2);}}

/* k5292 in k5289 in k5286 */
static void C_ccall f_5294(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5294,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5297,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_data_2dstructures_toplevel(2,av2);}}

/* k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_5297(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5297,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5300,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_extras_toplevel(2,av2);}}

/* k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_5300(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5300,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5303,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_file_toplevel(2,av2);}}

/* k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_5303(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5303,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5306,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_internal_toplevel(2,av2);}}

/* k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_5306(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5306,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5309,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_pathname_toplevel(2,av2);}}

/* k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_5309(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5309,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5312,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_port_toplevel(2,av2);}}

/* k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_5312(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(35,c,4)))){
C_save_and_reclaim((void *)f_5312,c,av);}
a=C_alloc(35);
t2=C_a_i_provide(&a,1,lf[0]);
t3=C_a_i_provide(&a,1,lf[1]);
t4=C_mutate(&lf[2] /* (set! chicken.compiler.support#take ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5422,tmp=(C_word)a,a+=2,tmp));
t5=C_mutate(&lf[3] /* (set! chicken.compiler.support#every ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5683,tmp=(C_word)a,a+=2,tmp));
t6=C_mutate(&lf[4] /* (set! chicken.compiler.support#any ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5717,tmp=(C_word)a,a+=2,tmp));
t7=C_mutate(&lf[5] /* (set! chicken.compiler.support#cons* ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5747,tmp=(C_word)a,a+=2,tmp));
t8=C_mutate(&lf[6] /* (set! chicken.compiler.support#last ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6083,tmp=(C_word)a,a+=2,tmp));
t9=C_mutate(&lf[7] /* (set! chicken.compiler.support#lset-adjoin/eq? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6142,tmp=(C_word)a,a+=2,tmp));
t10=C_mutate((C_word*)lf[8]+1 /* (set! chicken.compiler.support#number-type ...) */,lf[9]);
t11=C_set_block_item(lf[10] /* chicken.compiler.support#unsafe */,0,C_SCHEME_FALSE);
t12=C_mutate((C_word*)lf[11]+1 /* (set! chicken.compiler.support#compiler-cleanup-hook ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6595,tmp=(C_word)a,a+=2,tmp));
t13=C_set_block_item(lf[12] /* chicken.compiler.support#debugging-chicken */,0,C_SCHEME_END_OF_LIST);
t14=C_mutate((C_word*)lf[13]+1 /* (set! chicken.compiler.support#bomb ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6599,tmp=(C_word)a,a+=2,tmp));
t15=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6624,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:121: chicken.base#open-output-string */
t16=*((C_word*)lf[327]+1);{
C_word *av2=av;
av2[0]=t16;
av2[1]=t15;
((C_proc)(void*)(*((C_word*)t16+1)))(2,av2);}}

/* chicken.compiler.support#take in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_5422(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_5422,3,t1,t2,t3);}
a=C_alloc(4);
if(C_truep(C_fixnum_less_or_equal_p(t3,C_fix(0)))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_i_car(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5440,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* mini-srfi-1.scm:56: take */
t7=t5;
t8=C_u_i_cdr(t2);
t9=C_fixnum_difference(t3,C_fix(1));
t1=t7;
t2=t8;
t3=t9;
goto loop;}}

/* k5438 in chicken.compiler.support#take in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_5440(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_5440,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* loop in a10572 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_5480(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,4)))){
C_save_and_reclaim_args((void *)trf_5480,5,t0,t1,t2,t3,t4);}
a=C_alloc(4);
if(C_truep(C_fixnum_less_or_equal_p(t2,C_fix(0)))){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5494,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* mini-srfi-1.scm:67: scheme#reverse */
t6=*((C_word*)lf[80]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t5=C_fixnum_difference(t2,C_fix(1));
t6=C_i_car(t4);
t7=C_a_i_cons(&a,2,t6,t3);
/* mini-srfi-1.scm:68: loop */
t9=t1;
t10=t5;
t11=t7;
t12=C_u_i_cdr(t4);
t1=t9;
t2=t10;
t3=t11;
t4=t12;
goto loop;}}

/* k5492 in loop in a10572 in a10566 in chicken.compiler.support#inline-lambda-bindings in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_5494(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5494,c,av);}
/* mini-srfi-1.scm:67: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
C_values(4,av2);}}

/* chicken.compiler.support#every in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_5683(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_5683,3,t1,t2,t3);}
a=C_alloc(6);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5689,a[2]=t5,a[3]=t2,tmp=(C_word)a,a+=4,tmp));
t7=((C_word*)t5)[1];
f_5689(t7,t1,t3);}

/* loop in chicken.compiler.support#every in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_5689(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_5689,3,t0,t1,t2);}
a=C_alloc(5);
t3=C_i_nullp(t2);
if(C_truep(t3)){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5711,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* mini-srfi-1.scm:82: pred */
t5=((C_word*)t0)[3];{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=C_i_car(t2);
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}}

/* k5709 in loop in chicken.compiler.support#every in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_5711(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5711,c,av);}
if(C_truep(C_i_not(t1))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* mini-srfi-1.scm:83: loop */
t2=((C_word*)((C_word*)t0)[3])[1];
f_5689(t2,((C_word*)t0)[2],C_u_i_cdr(((C_word*)t0)[4]));}}

/* chicken.compiler.support#any in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_5717(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_5717,3,t1,t2,t3);}
a=C_alloc(6);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5723,a[2]=t5,a[3]=t2,tmp=(C_word)a,a+=4,tmp));
t7=((C_word*)t5)[1];
f_5723(t7,t1,t3);}

/* loop in chicken.compiler.support#any in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_5723(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_5723,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5733,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* mini-srfi-1.scm:88: pred */
t4=((C_word*)t0)[3];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_car(t2);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k5731 in loop in chicken.compiler.support#any in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_5733(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5733,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* mini-srfi-1.scm:89: loop */
t2=((C_word*)((C_word*)t0)[3])[1];
f_5723(t2,((C_word*)t0)[2],C_u_i_cdr(((C_word*)t0)[4]));}}

/* chicken.compiler.support#cons* in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_5747(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_5747,3,t1,t2,t3);}
a=C_alloc(5);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5753,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t7=((C_word*)t5)[1];
f_5753(t7,t1,t2,t3);}

/* loop in chicken.compiler.support#cons* in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_5753(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_5753,4,t0,t1,t2,t3);}
a=C_alloc(4);
if(C_truep(C_i_nullp(t3))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5767,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* mini-srfi-1.scm:95: loop */
t6=t4;
t7=C_i_car(t3);
t8=C_u_i_cdr(t3);
t1=t6;
t2=t7;
t3=t8;
goto loop;}}

/* k5765 in loop in chicken.compiler.support#cons* in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_5767(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_5767,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* foldr389 in k8092 in chicken.compiler.support#db-get-all in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_5976(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(8,0,4)))){
C_save_and_reclaim_args((void *)trf_5976,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5984,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6010,a[2]=t3,a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t7=t5;
t8=C_slot(t2,C_fix(1));
t1=t7;
t2=t8;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* g394 in foldr389 in k8092 in chicken.compiler.support#db-get-all in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_5984(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_5984,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=(
/* mini-srfi-1.scm:135: pred */
  f_8102(((C_word*)t0)[2],t2)
);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5992,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* mini-srfi-1.scm:135: g404 */
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=(
/* mini-srfi-1.scm:135: g404 */
  f_5992(C_a_i(&a,3),t5,t4)
);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* g404 in g394 in foldr389 in k8092 in chicken.compiler.support#db-get-all in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static C_word C_fcall f_5992(C_word *a,C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_a_i_cons(&a,2,t1,((C_word*)t0)[2]));}

/* k6008 in foldr389 in k8092 in chicken.compiler.support#db-get-all in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6010(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6010,c,av);}
/* mini-srfi-1.scm:134: g394 */
t2=((C_word*)t0)[2];
f_5984(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* map-loop417 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_6049(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_6049,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.compiler.support#last in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_6083(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,0,2)))){
C_save_and_reclaim_args((void *)trf_6083,2,t1,t2);}
a=C_alloc(2);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6089,tmp=(C_word)a,a+=2,tmp);
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=(
  f_6089(t2)
);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* loop in chicken.compiler.support#last in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static C_word C_fcall f_6089(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
t2=C_i_cdr(t1);
if(C_truep(C_i_nullp(t2))){
return(C_u_i_car(t1));}
else{
t4=C_u_i_cdr(t1);
t1=t4;
goto loop;}}

/* chicken.compiler.support#lset-adjoin/eq? in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_6142(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_6142,3,t1,t2,t3);}
a=C_alloc(5);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6148,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t7=((C_word*)t5)[1];
f_6148(t7,t1,t3,t2);}

/* loop in chicken.compiler.support#lset-adjoin/eq? in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_6148(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,3)))){
C_save_and_reclaim_args((void *)trf_6148,4,t0,t1,t2,t3);}
a=C_alloc(3);
if(C_truep(C_i_nullp(t2))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_i_car(t2);
if(C_truep(C_i_memq(t4,t3))){
/* mini-srfi-1.scm:160: loop */
t9=t1;
t10=C_u_i_cdr(t2);
t11=t3;
t1=t9;
t2=t10;
t3=t11;
goto loop;}
else{
t5=C_u_i_cdr(t2);
t6=C_u_i_car(t2);
t7=C_a_i_cons(&a,2,t6,t3);
/* mini-srfi-1.scm:161: loop */
t9=t1;
t10=t5;
t11=t7;
t1=t9;
t2=t10;
t3=t11;
goto loop;}}}

/* loop in k17169 in chicken.compiler.support#encodeable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_6331(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_6331,3,t0,t1,t2);}
a=C_alloc(4);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(
/* mini-srfi-1.scm:190: proc */
  f_17287(((C_word*)t0)[3],t2)
);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6349,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* mini-srfi-1.scm:190: loop */
t6=t4;
t7=C_fixnum_plus(t2,C_fix(1));
t1=t6;
t2=t7;
goto loop;}}

/* k6347 in loop in k17169 in chicken.compiler.support#encodeable-literal? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6349(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_6349,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* loop in k12546 in walk in chicken.compiler.support#expression-has-side-effects? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_6444(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_6444,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6457,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* mini-srfi-1.scm:216: pred */
t4=((C_word*)t0)[3];
f_12562(t4,t3,C_i_car(t2));}}

/* k6455 in loop in k12546 in walk in chicken.compiler.support#expression-has-side-effects? in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6457(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6457,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_u_i_car(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* mini-srfi-1.scm:217: loop */
t2=((C_word*)((C_word*)t0)[4])[1];
f_6444(t2,((C_word*)t0)[2],C_u_i_cdr(((C_word*)t0)[3]));}}

/* chicken.compiler.support#compiler-cleanup-hook in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6595(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6595,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.compiler.support#bomb in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6599(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +4,c,3)))){
C_save_and_reclaim((void*)f_6599,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+4);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6613,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* support.scm:117: scheme#string-append */
t4=*((C_word*)lf[15]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[16];
av2[3]=C_u_i_car(t2);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
/* support.scm:118: chicken.base#error */
t3=*((C_word*)lf[14]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=lf[17];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k6611 in chicken.compiler.support#bomb in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6613(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6613,c,av);}{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[14]+1);
av2[3]=t1;
av2[4]=C_u_i_cdr(((C_word*)t0)[3]);
C_apply(5,av2);}}

/* k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6624(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(47,c,5)))){
C_save_and_reclaim((void *)f_6624,c,av);}
a=C_alloc(47);
t2=C_mutate((C_word*)lf[18]+1 /* (set! chicken.compiler.support#collected-debugging-output ...) */,t1);
t3=C_mutate(&lf[19] /* (set! chicken.compiler.support#+logged-debugging-modes+ ...) */,lf[20]);
t4=C_mutate(&lf[21] /* (set! chicken.compiler.support#test-debugging-mode ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6627,tmp=(C_word)a,a+=2,tmp));
t5=C_mutate((C_word*)lf[22]+1 /* (set! chicken.compiler.support#debugging ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6648,tmp=(C_word)a,a+=2,tmp));
t6=C_mutate((C_word*)lf[34]+1 /* (set! chicken.compiler.support#with-debugging-output ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6768,tmp=(C_word)a,a+=2,tmp));
t7=C_mutate((C_word*)lf[37]+1 /* (set! chicken.compiler.support#quit-compiling ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6865,tmp=(C_word)a,a+=2,tmp));
t8=C_mutate((C_word*)lf[42]+1 /* (set! ##sys#syntax-error-hook ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6881,tmp=(C_word)a,a+=2,tmp));
t9=C_mutate((C_word*)lf[51]+1 /* (set! chicken.syntax#syntax-error ...) */,*((C_word*)lf[42]+1));
t10=C_mutate((C_word*)lf[52]+1 /* (set! chicken.compiler.support#emit-syntax-trace-info ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6978,tmp=(C_word)a,a+=2,tmp));
t11=C_mutate((C_word*)lf[53]+1 /* (set! chicken.compiler.support#check-signature ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7032,tmp=(C_word)a,a+=2,tmp));
t12=C_mutate((C_word*)lf[54]+1 /* (set! chicken.compiler.support#build-lambda-list ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7073,tmp=(C_word)a,a+=2,tmp));
t13=C_mutate((C_word*)lf[55]+1 /* (set! chicken.compiler.support#c-ify-string ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7110,tmp=(C_word)a,a+=2,tmp));
t14=C_mutate((C_word*)lf[65]+1 /* (set! chicken.compiler.support#valid-c-identifier? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7209,tmp=(C_word)a,a+=2,tmp));
t15=C_mutate((C_word*)lf[67]+1 /* (set! chicken.compiler.support#bytes->words ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7259,tmp=(C_word)a,a+=2,tmp));
t16=C_mutate((C_word*)lf[68]+1 /* (set! chicken.compiler.support#words->bytes ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7266,tmp=(C_word)a,a+=2,tmp));
t17=C_mutate((C_word*)lf[69]+1 /* (set! chicken.compiler.support#check-and-open-input-file ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7273,tmp=(C_word)a,a+=2,tmp));
t18=C_mutate((C_word*)lf[76]+1 /* (set! chicken.compiler.support#close-checked-input-file ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7317,tmp=(C_word)a,a+=2,tmp));
t19=C_mutate((C_word*)lf[79]+1 /* (set! chicken.compiler.support#fold-inner ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7329,tmp=(C_word)a,a+=2,tmp));
t20=C_mutate(&lf[81] /* (set! chicken.compiler.support#follow-without-loop ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7385,tmp=(C_word)a,a+=2,tmp));
t21=C_mutate((C_word*)lf[82]+1 /* (set! chicken.compiler.support#read-expressions ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7436,tmp=(C_word)a,a+=2,tmp));
t22=C_mutate((C_word*)lf[84]+1 /* (set! chicken.compiler.support#constant? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7480,tmp=(C_word)a,a+=2,tmp));
t23=C_mutate((C_word*)lf[88]+1 /* (set! chicken.compiler.support#collapsable-literal? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7542,tmp=(C_word)a,a+=2,tmp));
t24=C_mutate((C_word*)lf[89]+1 /* (set! chicken.compiler.support#immediate? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7572,tmp=(C_word)a,a+=2,tmp));
t25=C_mutate((C_word*)lf[91]+1 /* (set! chicken.compiler.support#basic-literal? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7618,tmp=(C_word)a,a+=2,tmp));
t26=C_mutate((C_word*)lf[93]+1 /* (set! chicken.compiler.support#canonicalize-begin-body ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7674,tmp=(C_word)a,a+=2,tmp));
t27=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7749,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:344: chicken.condition#condition-predicate */
t28=*((C_word*)lf[538]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t28;
av2[1]=t27;
av2[2]=lf[536];
((C_proc)(void*)(*((C_word*)t28+1)))(3,av2);}}

/* chicken.compiler.support#test-debugging-mode in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_6627(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,3)))){
C_save_and_reclaim_args((void *)trf_6627,3,t1,t2,t3);}
a=C_alloc(3);
if(C_truep(C_i_symbolp(t2))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_i_memq(t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6642,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* support.scm:128: any */
f_5717(t1,t4,t2);}}

/* a6641 in chicken.compiler.support#test-debugging-mode in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6642(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6642,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_memq(t2,((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6648(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +17,c,3)))){
C_save_and_reclaim((void*)f_6648,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+17);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6651,a[2]=t4,a[3]=t3,tmp=(C_word)a,a+=4,tmp));
t10=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6716,a[2]=t2,tmp=(C_word)a,a+=3,tmp));
t11=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6735,a[2]=t1,a[3]=t8,a[4]=t2,a[5]=t6,tmp=(C_word)a,a+=6,tmp);
/* support.scm:143: test-debugging-mode */
f_6627(t11,t2,*((C_word*)lf[12]+1));}

/* text in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_6651(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_6651,2,t0,t1);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6657,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:132: chicken.port#with-output-to-string */
t3=*((C_word*)lf[31]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* a6656 in text in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6657,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6661,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* support.scm:134: scheme#display */
t3=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6659 in a6656 in text in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6661(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_6661,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6664,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_i_pairp(((C_word*)t0)[3]))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6673,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:136: scheme#display */
t4=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[30];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
/* support.scm:140: scheme#newline */
t3=*((C_word*)lf[23]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6662 in k6659 in a6656 in text in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6664(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6664,c,av);}
/* support.scm:140: scheme#newline */
t2=*((C_word*)lf[23]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k6671 in k6659 in a6656 in text in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6673(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6673,c,av);}
a=C_alloc(5);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6693,a[2]=t3,tmp=(C_word)a,a+=3,tmp));
t5=((C_word*)t3)[1];
f_6693(t5,((C_word*)t0)[2],((C_word*)t0)[3]);}

/* k6679 in for-each-loop685 in k6671 in k6659 in a6656 in text in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6681(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6681,c,av);}
/* support.scm:138: ##sys#write-char-0 */
t2=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(32);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k6686 in for-each-loop685 in k6671 in k6659 in a6656 in text in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6688(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6688,c,av);}
/* support.scm:138: ##sys#print */
t2=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* for-each-loop685 in k6671 in k6659 in a6656 in text in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_6693(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,0,2)))){
C_save_and_reclaim_args((void *)trf_6693,3,t0,t1,t2);}
a=C_alloc(13);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6703,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=*((C_word*)lf[24]+1);
t6=*((C_word*)lf[24]+1);
t7=C_i_check_port_2(*((C_word*)lf[24]+1),C_fix(2),C_SCHEME_TRUE,lf[25]);
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6681,a[2]=t3,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6688,a[2]=t8,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
/* support.scm:138: scheme#force */
t10=*((C_word*)lf[28]+1);{
C_word av2[3];
av2[0]=t10;
av2[1]=t9;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6701 in for-each-loop685 in k6671 in k6659 in a6656 in text in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6703(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6703,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6693(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* dump in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_6716(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_6716,3,t0,t1,t2);}
a=C_alloc(5);
t3=*((C_word*)lf[18]+1);
t4=*((C_word*)lf[18]+1);
t5=C_i_check_port_2(*((C_word*)lf[18]+1),C_fix(2),C_SCHEME_TRUE,lf[32]);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6723,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* support.scm:142: ##sys#print */
t7=*((C_word*)lf[27]+1);{
C_word av2[5];
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)t0)[2];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[18]+1);
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}

/* k6721 in dump in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6723(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6723,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6726,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:142: ##sys#write-char-0 */
t3=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(124);
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6724 in k6721 in dump in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6726(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6726,c,av);}
/* support.scm:142: ##sys#print */
t2=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k6733 in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6735(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_6735,c,av);}
a=C_alloc(9);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6738,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:144: text */
t3=((C_word*)((C_word*)t0)[5])[1];
f_6651(t3,t2);}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6756,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6759,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
/* support.scm:151: test-debugging-mode */
f_6627(t3,((C_word*)t0)[4],lf[19]);}}

/* k6736 in k6733 in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6738(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6738,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6741,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* support.scm:145: scheme#display */
t3=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6739 in k6736 in k6733 in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6741(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6741,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6744,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* support.scm:146: chicken.base#flush-output */
t3=*((C_word*)lf[33]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k6742 in k6739 in k6736 in k6733 in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6744(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_6744,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6747,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6750,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
/* support.scm:147: test-debugging-mode */
f_6627(t3,((C_word*)t0)[5],lf[19]);}

/* k6745 in k6742 in k6739 in k6736 in k6733 in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6747(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6747,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k6748 in k6742 in k6739 in k6736 in k6733 in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6750(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6750,c,av);}
if(C_truep(t1)){
/* support.scm:148: dump */
t2=((C_word*)((C_word*)t0)[2])[1];
f_6716(t2,((C_word*)t0)[3],((C_word*)t0)[4]);}
else{
t2=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k6754 in k6733 in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6756(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6756,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k6757 in k6733 in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6759(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6759,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6766,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:152: text */
t3=((C_word*)((C_word*)t0)[4])[1];
f_6651(t3,t2);}
else{
t2=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k6764 in k6757 in k6733 in chicken.compiler.support#debugging in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6766(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6766,c,av);}
/* support.scm:152: dump */
t2=((C_word*)((C_word*)t0)[2])[1];
f_6716(t2,((C_word*)t0)[3],t1);}

/* chicken.compiler.support#with-debugging-output in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6768(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_6768,c,av);}
a=C_alloc(9);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6771,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6832,a[2]=t4,a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* support.scm:163: test-debugging-mode */
f_6627(t5,t2,*((C_word*)lf[12]+1));}

/* collect in chicken.compiler.support#with-debugging-output in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_6771(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_6771,3,t0,t1,t2);}
a=C_alloc(7);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6773,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6800,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* support.scm:162: chicken.string#string-split */
t5=*((C_word*)lf[35]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[36];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* g737 in collect in chicken.compiler.support#with-debugging-output in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_6773(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_6773,3,t0,t1,t2);}
a=C_alloc(5);
t3=*((C_word*)lf[18]+1);
t4=*((C_word*)lf[18]+1);
t5=C_i_check_port_2(*((C_word*)lf[18]+1),C_fix(2),C_SCHEME_TRUE,lf[32]);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6780,a[2]=t1,a[3]=t3,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_pairp(((C_word*)t0)[2]))){
/* support.scm:159: ##sys#print */
t7=*((C_word*)lf[27]+1);{
C_word av2[5];
av2[0]=t7;
av2[1]=t6;
av2[2]=C_u_i_car(((C_word*)t0)[2]);
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[18]+1);
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}
else{
/* support.scm:159: ##sys#print */
t7=*((C_word*)lf[27]+1);{
C_word av2[5];
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)t0)[2];
av2[3]=C_SCHEME_FALSE;
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}}

/* k6778 in g737 in collect in chicken.compiler.support#with-debugging-output in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6780(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6780,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6783,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:159: ##sys#write-char-0 */
t3=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(124);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6781 in k6778 in g737 in collect in chicken.compiler.support#with-debugging-output in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6783(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_6783,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6786,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:159: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6784 in k6781 in k6778 in g737 in collect in chicken.compiler.support#with-debugging-output in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6786(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6786,c,av);}
/* support.scm:159: ##sys#write-char-0 */
t2=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k6798 in collect in chicken.compiler.support#with-debugging-output in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6800(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_6800,c,av);}
a=C_alloc(6);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6805,a[2]=t3,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp));
t5=((C_word*)t3)[1];
f_6805(t5,((C_word*)t0)[3],t1);}

/* for-each-loop736 in k6798 in collect in chicken.compiler.support#with-debugging-output in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_6805(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_6805,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6815,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:157: g737 */
t4=((C_word*)t0)[3];
f_6773(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6813 in for-each-loop736 in k6798 in collect in chicken.compiler.support#with-debugging-output in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6815(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6815,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6805(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k6830 in chicken.compiler.support#with-debugging-output in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6832(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6832,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6835,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:164: chicken.port#with-output-to-string */
t3=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6856,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* support.scm:169: test-debugging-mode */
f_6627(t2,((C_word*)t0)[4],lf[19]);}}

/* k6833 in k6830 in chicken.compiler.support#with-debugging-output in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6835(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6835,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6838,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* support.scm:165: scheme#display */
t3=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6836 in k6833 in k6830 in chicken.compiler.support#with-debugging-output in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6838(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6838,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6841,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* support.scm:166: chicken.base#flush-output */
t3=*((C_word*)lf[33]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k6839 in k6836 in k6833 in k6830 in chicken.compiler.support#with-debugging-output in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6841(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6841,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6847,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:167: test-debugging-mode */
f_6627(t2,((C_word*)t0)[5],lf[19]);}

/* k6845 in k6839 in k6836 in k6833 in k6830 in chicken.compiler.support#with-debugging-output in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6847(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6847,c,av);}
if(C_truep(t1)){
/* support.scm:168: collect */
t2=((C_word*)t0)[2];
f_6771(t2,((C_word*)t0)[3],((C_word*)t0)[4]);}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k6854 in k6830 in chicken.compiler.support#with-debugging-output in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6856(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6856,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6863,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:170: chicken.port#with-output-to-string */
t3=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k6861 in k6854 in k6830 in chicken.compiler.support#with-debugging-output in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6863(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6863,c,av);}
/* support.scm:170: collect */
t2=((C_word*)t0)[2];
f_6771(t2,((C_word*)t0)[3],t1);}

/* chicken.compiler.support#quit-compiling in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6865(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +9,c,3)))){
C_save_and_reclaim((void*)f_6865,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+9);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
C_word t7;
t4=*((C_word*)lf[38]+1);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6869,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6879,a[2]=t5,a[3]=t4,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* support.scm:174: scheme#string-append */
t7=*((C_word*)lf[15]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[41];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}

/* k6867 in chicken.compiler.support#quit-compiling in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6869(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6869,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6872,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:175: scheme#newline */
t3=*((C_word*)lf[23]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6870 in k6867 in chicken.compiler.support#quit-compiling in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6872(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6872,c,av);}
/* support.scm:176: chicken.base#exit */
t2=*((C_word*)lf[39]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(1);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6877 in chicken.compiler.support#quit-compiling in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6879(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6879,c,av);}{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[40]+1);
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
av2[5]=((C_word*)t0)[4];
C_apply(6,av2);}}

/* ##sys#syntax-error-hook in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6881(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +10,c,2)))){
C_save_and_reclaim((void*)f_6881,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+10);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t5=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t6=*((C_word*)lf[38]+1);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6885,a[2]=t6,a[3]=t5,a[4]=t1,a[5]=t4,tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_symbolp(((C_word*)t4)[1]))){
t8=((C_word*)t4)[1];
t9=C_i_car(((C_word*)t5)[1]);
t10=C_set_block_item(t4,0,t9);
t11=C_i_cdr(((C_word*)t5)[1]);
t12=C_set_block_item(t5,0,t11);
t13=t7;
f_6885(t13,t8);}
else{
t8=t7;
f_6885(t8,C_SCHEME_FALSE);}}

/* k6883 in ##sys#syntax-error-hook in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_6885(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,4)))){
C_save_and_reclaim_args((void *)trf_6885,2,t0,t1);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6888,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t1)){
t3=C_i_check_port_2(((C_word*)t0)[2],C_fix(2),C_SCHEME_TRUE,lf[32]);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6934,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[5],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* support.scm:187: ##sys#print */
t5=*((C_word*)lf[27]+1);{
C_word av2[5];
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[49];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
t3=C_i_check_port_2(((C_word*)t0)[2],C_fix(2),C_SCHEME_TRUE,lf[32]);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6955,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* support.scm:188: ##sys#print */
t5=*((C_word*)lf[27]+1);{
C_word av2[5];
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[50];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}}

/* k6886 in k6883 in ##sys#syntax-error-hook in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6888(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_6888,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6889,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=((C_word*)((C_word*)t0)[3])[1];
t4=C_i_check_list_2(t3,lf[44]);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6899,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6907,a[2]=t7,a[3]=t2,tmp=(C_word)a,a+=4,tmp));
t9=((C_word*)t7)[1];
f_6907(t9,t5,t3);}

/* g781 in k6886 in k6883 in ##sys#syntax-error-hook in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_6889(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_6889,3,t0,t1,t2);}
/* support.scm:189: g814 */
t3=*((C_word*)lf[40]+1);{
C_word av2[5];
av2[0]=t3;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[43];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6897 in k6886 in k6883 in ##sys#syntax-error-hook in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6899(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_6899,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6902,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:190: chicken.base#print-call-chain */
t3=*((C_word*)lf[45]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=C_fix(0);
av2[4]=*((C_word*)lf[46]+1);
av2[5]=lf[47];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k6900 in k6897 in k6886 in k6883 in ##sys#syntax-error-hook in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6902(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6902,c,av);}
/* support.scm:191: chicken.base#exit */
t2=*((C_word*)lf[39]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(70);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* for-each-loop780 in k6886 in k6883 in ##sys#syntax-error-hook in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_6907(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_6907,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6917,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* support.scm:189: g781 */
t4=((C_word*)t0)[3];
f_6889(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6915 in for-each-loop780 in k6886 in k6883 in ##sys#syntax-error-hook in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6917(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6917,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6907(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k6932 in k6883 in ##sys#syntax-error-hook in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6934(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_6934,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6937,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:187: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6935 in k6932 in k6883 in ##sys#syntax-error-hook in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6937(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_6937,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6940,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:187: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[48];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6938 in k6935 in k6932 in k6883 in ##sys#syntax-error-hook in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6940(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_6940,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6943,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:187: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6941 in k6938 in k6935 in k6932 in k6883 in ##sys#syntax-error-hook in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6943(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_6943,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6946,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:187: ##sys#write-char-0 */
t3=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6944 in k6941 in k6938 in k6935 in k6932 in k6883 in ##sys#syntax-error-hook in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6946(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6946,c,av);}
/* support.scm:187: ##sys#write-char-0 */
t2=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k6953 in k6883 in ##sys#syntax-error-hook in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6955(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_6955,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6958,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:188: ##sys#print */
t3=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6956 in k6953 in k6883 in ##sys#syntax-error-hook in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6958(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_6958,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6961,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:188: ##sys#write-char-0 */
t3=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6959 in k6956 in k6953 in k6883 in ##sys#syntax-error-hook in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6961(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6961,c,av);}
/* support.scm:188: ##sys#write-char-0 */
t2=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.compiler.support#emit-syntax-trace-info in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_6978(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6978,c,av);}
t4=*((C_word*)lf[46]+1);
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_emit_syntax_trace_info(t2,t3,C_slot(*((C_word*)lf[46]+1),C_fix(14)));
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* chicken.compiler.support#check-signature in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7032(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,3)))){
C_save_and_reclaim((void *)f_7032,c,av);}
a=C_alloc(2);
t5=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7038,tmp=(C_word)a,a+=2,tmp);
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=(
  f_7038(t3,t4)
);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* loop in chicken.compiler.support#check-signature in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static C_word C_fcall f_7038(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_nullp(t2))){
return(C_i_nullp(t1));}
else{
t3=C_i_symbolp(t2);
if(C_truep(t3)){
return(t3);}
else{
if(C_truep(C_i_nullp(t1))){
return(C_SCHEME_FALSE);}
else{
t5=C_i_cdr(t1);
t6=C_i_cdr(t2);
t1=t5;
t2=t6;
goto loop;}}}}

/* chicken.compiler.support#build-lambda-list in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7073(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_7073,c,av);}
a=C_alloc(6);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7079,a[2]=t4,a[3]=t6,tmp=(C_word)a,a+=4,tmp));
t8=((C_word*)t6)[1];
f_7079(t8,t1,t2,t3);}

/* loop in chicken.compiler.support#build-lambda-list in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_7079(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(33,0,3)))){
C_save_and_reclaim_args((void *)trf_7079,4,t0,t1,t2,t3);}
a=C_alloc(33);
t4=C_i_zerop(t3);
t5=(C_truep(t4)?t4:C_i_nullp(t2));
if(C_truep(t5)){
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=(C_truep(((C_word*)t0)[2])?((C_word*)t0)[2]:C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_i_car(t2);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7101,a[2]=t1,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
t8=C_u_i_cdr(t2);
t9=C_s_a_i_minus(&a,2,t3,C_fix(1));
/* support.scm:219: loop */
t11=t7;
t12=t8;
t13=t9;
t1=t11;
t2=t12;
t3=t13;
goto loop;}}

/* k7099 in loop in chicken.compiler.support#build-lambda-list in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7101(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_7101,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.compiler.support#c-ify-string in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7110(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_7110,c,av);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7122,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7126,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string->list */
t5=*((C_word*)lf[60]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k7120 in chicken.compiler.support#c-ify-string in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7122(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7122,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,C_make_character(34),t1);
/* ##sys#list->string */
t3=*((C_word*)lf[56]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7124 in chicken.compiler.support#c-ify-string in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7126(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_7126,c,av);}
a=C_alloc(5);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7128,a[2]=t3,tmp=(C_word)a,a+=3,tmp));
t5=((C_word*)t3)[1];
f_7128(t5,((C_word*)t0)[2],t1);}

/* loop in k7124 in chicken.compiler.support#c-ify-string in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_7128(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_7128,3,t0,t1,t2);}
a=C_alloc(7);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=lf[57];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_car(t2);
t4=C_fix(C_character_code(t3));
t5=C_fixnum_lessp(t4,C_fix(32));
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7150,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,a[5]=t4,a[6]=t3,tmp=(C_word)a,a+=7,tmp);
if(C_truep(t5)){
t7=t6;
f_7150(t7,t5);}
else{
t7=C_fixnum_greater_or_equal_p(t4,C_fix(127));
t8=t6;
f_7150(t8,(C_truep(t7)?t7:C_u_i_memq(t3,lf[64])));}}}

/* k7148 in loop in k7124 in chicken.compiler.support#c-ify-string in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_7150(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7150,2,t0,t1);}
a=C_alloc(6);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7157,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_fixnum_lessp(((C_word*)t0)[5],C_fix(8)))){
t3=t2;
f_7157(t3,lf[62]);}
else{
t3=C_fixnum_lessp(((C_word*)t0)[5],C_fix(64));
t4=t2;
f_7157(t4,(C_truep(t3)?lf[63]:C_SCHEME_END_OF_LIST));}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7193,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
/* support.scm:238: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_7128(t3,t2,C_u_i_cdr(((C_word*)t0)[4]));}}

/* k7155 in k7148 in loop in k7124 in chicken.compiler.support#c-ify-string in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_7157(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_7157,2,t0,t1);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7161,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7171,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* ##sys#fixnum->string */
t4=*((C_word*)lf[61]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
av2[3]=C_fix(8);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k7159 in k7155 in k7148 in loop in k7124 in chicken.compiler.support#c-ify-string in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7161(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_7161,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7165,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* support.scm:237: loop */
t3=((C_word*)((C_word*)t0)[4])[1];
f_7128(t3,t2,C_u_i_cdr(((C_word*)t0)[5]));}

/* k7163 in k7159 in k7155 in k7148 in loop in k7124 in chicken.compiler.support#c-ify-string in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7165(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_7165,c,av);}
/* support.scm:232: scheme#append */
t2=*((C_word*)lf[58]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[59];
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[4];
av2[5]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k7169 in k7155 in k7148 in loop in k7124 in chicken.compiler.support#c-ify-string in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7171(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7171,c,av);}
/* ##sys#string->list */
t2=*((C_word*)lf[60]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7191 in k7148 in loop in k7124 in chicken.compiler.support#c-ify-string in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7193(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_7193,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.compiler.support#valid-c-identifier? in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7209(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_7209,c,av);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7213,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7257,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* support.scm:242: chicken.string#->string */
t5=*((C_word*)lf[66]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k7211 in chicken.compiler.support#valid-c-identifier? in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7213(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,3)))){
C_save_and_reclaim((void *)f_7213,c,av);}
a=C_alloc(2);
if(C_truep(C_i_pairp(t1))){
t2=C_u_i_car(t1);
t3=C_u_i_char_alphabeticp(t2);
t4=(C_truep(t3)?t3:C_u_i_char_equalp(C_make_character(95),t2));
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7234,tmp=(C_word)a,a+=2,tmp);
/* support.scm:246: every */
f_5683(((C_word*)t0)[2],t5,C_u_i_cdr(t1));}
else{
t5=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* a7233 in k7211 in chicken.compiler.support#valid-c-identifier? in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7234(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7234,c,av);}
t3=C_u_i_char_alphabeticp(t2);
if(C_truep(t3)){
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_u_i_char_numericp(t2);
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=(C_truep(t4)?t4:C_u_i_char_equalp(C_make_character(95),t2));
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k7255 in chicken.compiler.support#valid-c-identifier? in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7257(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7257,c,av);}
/* ##sys#string->list */
t2=*((C_word*)lf[60]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* chicken.compiler.support#bytes->words in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7259(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7259,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=stub908(C_SCHEME_UNDEFINED,C_i_foreign_fixnum_argumentp(t2));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.compiler.support#words->bytes in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7266(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7266,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=stub913(C_SCHEME_UNDEFINED,C_i_foreign_fixnum_argumentp(t2));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.compiler.support#check-and-open-input-file in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7273(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +5,c,2)))){
C_save_and_reclaim((void*)f_7273,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+5);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
if(C_truep(C_i_string_equal_p(t2,lf[70]))){
t4=*((C_word*)lf[71]+1);
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=*((C_word*)lf[71]+1);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7286,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* support.scm:256: chicken.file#file-exists? */
t5=*((C_word*)lf[75]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}}

/* k7284 in chicken.compiler.support#check-and-open-input-file in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7286(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7286,c,av);}
if(C_truep(t1)){
/* support.scm:256: scheme#open-input-file */
t2=*((C_word*)lf[72]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=C_i_nullp(((C_word*)t0)[4]);
t3=(C_truep(t2)?t2:C_i_not(C_i_car(((C_word*)t0)[4])));
if(C_truep(t3)){
/* support.scm:258: quit-compiling */
t4=*((C_word*)lf[37]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[73];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
/* support.scm:259: quit-compiling */
t4=*((C_word*)lf[37]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[74];
av2[3]=C_i_car(((C_word*)t0)[4]);
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}}

/* chicken.compiler.support#close-checked-input-file in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7317(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7317,c,av);}
if(C_truep(C_i_string_equal_p(t3,lf[77]))){
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
/* support.scm:262: scheme#close-input-port */
t4=*((C_word*)lf[78]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* chicken.compiler.support#fold-inner in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7329(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7329,c,av);}
a=C_alloc(4);
t4=C_i_cdr(t3);
if(C_truep(C_i_nullp(t4))){
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7343,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* support.scm:267: scheme#reverse */
t6=*((C_word*)lf[80]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}}

/* k7341 in chicken.compiler.support#fold-inner in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7343(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_7343,c,av);}
a=C_alloc(6);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7345,a[2]=((C_word*)t0)[2],a[3]=t3,tmp=(C_word)a,a+=4,tmp));
t5=((C_word*)t3)[1];
f_7345(t5,((C_word*)t0)[3],t1);}

/* fold in k7341 in chicken.compiler.support#fold-inner in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_7345(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_7345,3,t0,t1,t2);}
a=C_alloc(6);
t3=C_i_cddr(t2);
if(C_truep(C_i_nullp(t3))){
t4=C_u_i_cdr(t2);
t5=C_u_i_car(t4);
t6=C_u_i_car(t2);
t7=C_a_i_list2(&a,2,t5,t6);{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=t7;
C_apply(4,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7371,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* support.scm:272: fold */
t9=t4;
t10=C_u_i_cdr(t2);
t1=t9;
t2=t10;
goto loop;}}

/* k7369 in fold in k7341 in chicken.compiler.support#fold-inner in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7371(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_7371,c,av);}
a=C_alloc(6);
t2=C_u_i_car(((C_word*)t0)[2]);
t3=C_a_i_list2(&a,2,t1,t2);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=t3;
C_apply(4,av2);}}

/* chicken.compiler.support#follow-without-loop in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_7385(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,4)))){
C_save_and_reclaim_args((void *)trf_7385,4,t1,t2,t3,t4);}
a=C_alloc(7);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7391,a[2]=t4,a[3]=t6,a[4]=t3,tmp=(C_word)a,a+=5,tmp));
t8=((C_word*)t6)[1];
f_7391(t8,t1,t2,C_SCHEME_END_OF_LIST);}

/* loop in chicken.compiler.support#follow-without-loop in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_7391(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_7391,4,t0,t1,t2,t3);}
a=C_alloc(5);
if(C_truep(C_i_member(t2,t3))){
/* support.scm:277: abort */
t4=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t4;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7406,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* support.scm:278: proc */
t5=((C_word*)t0)[4];{
C_word av2[4];
av2[0]=t5;
av2[1]=t1;
av2[2]=t2;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}

/* a7405 in loop in chicken.compiler.support#follow-without-loop in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7406(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_7406,c,av);}
a=C_alloc(3);
t3=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
/* support.scm:278: loop */
t4=((C_word*)((C_word*)t0)[4])[1];
f_7391(t4,t1,t2,t3);}

/* a7421 in k11964 in k11958 in k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7422(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7422,c,av);}
a=C_alloc(4);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7430,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* support.scm:281: scheme#symbol->string */
t5=*((C_word*)lf[232]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k7428 in a7421 in k11964 in k11958 in k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7430(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7430,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7434,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* support.scm:281: scheme#symbol->string */
t3=*((C_word*)lf[232]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7432 in k7428 in a7421 in k11964 in k11958 in k11955 in chicken.compiler.support#emit-global-inline-file in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7434(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7434,c,av);}
/* support.scm:281: scheme#string<? */
t2=*((C_word*)lf[231]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.compiler.support#read-expressions in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7436(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7436,c,av);}
a=C_alloc(4);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?*((C_word*)lf[71]+1):C_get_rest_arg(c,2,av,2,t0));
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7447,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* support.scm:284: scheme#read */
t5=*((C_word*)lf[83]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k7445 in chicken.compiler.support#read-expressions in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7447(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_7447,c,av);}
a=C_alloc(6);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7449,a[2]=t3,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp));
t5=((C_word*)t3)[1];
f_7449(t5,((C_word*)t0)[3],t1,C_fix(0),C_SCHEME_END_OF_LIST);}

/* doloop959 in k7445 in chicken.compiler.support#read-expressions in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_7449(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_7449,5,t0,t1,t2,t3,t4);}
a=C_alloc(7);
if(C_truep(C_eofp(t2))){
/* support.scm:287: scheme#reverse */
t5=*((C_word*)lf[80]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t1;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7466,a[2]=t3,a[3]=t2,a[4]=t4,a[5]=((C_word*)t0)[2],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* support.scm:284: scheme#read */
t6=*((C_word*)lf[83]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}}

/* k7464 in doloop959 in k7445 in chicken.compiler.support#read-expressions in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7466(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(32,c,4)))){
C_save_and_reclaim((void *)f_7466,c,av);}
a=C_alloc(32);
t2=C_s_a_i_plus(&a,2,((C_word*)t0)[2],C_fix(1));
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]);
t4=((C_word*)((C_word*)t0)[5])[1];
f_7449(t4,((C_word*)t0)[6],t1,t2,t3);}

/* chicken.compiler.support#constant? in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7480(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7480,c,av);}
a=C_alloc(4);
t3=C_i_numberp(t2);
if(C_truep(t3)){
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_charp(t2);
if(C_truep(t4)){
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_i_stringp(t2);
if(C_truep(t5)){
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_booleanp(t2);
if(C_truep(t6)){
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
t7=C_eofp(t2);
if(C_truep(t7)){
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7514,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* support.scm:298: chicken.blob#blob? */
t9=*((C_word*)lf[87]+1);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}}}}}}

/* k7512 in chicken.compiler.support#constant? in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7514(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7514,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_i_vectorp(((C_word*)t0)[3]);
if(C_truep(t2)){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7526,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:300: ##sys#srfi-4-vector? */
t4=*((C_word*)lf[86]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}}

/* k7524 in k7512 in chicken.compiler.support#constant? in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7526(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7526,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_i_pairp(((C_word*)t0)[3]);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(t2)?C_eqp(lf[85],C_u_i_car(((C_word*)t0)[3])):C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.compiler.support#collapsable-literal? in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7542(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7542,c,av);}
t3=C_booleanp(t2);
if(C_truep(t3)){
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_charp(t2);
if(C_truep(t4)){
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_eofp(t2);
if(C_truep(t5)){
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_i_numberp(t2);
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=(C_truep(t6)?t6:C_i_symbolp(t2));
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}}}

/* chicken.compiler.support#immediate? in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7572(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_7572,c,av);}
a=C_alloc(7);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7576,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_fixnump(t2))){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7616,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* support.scm:311: big-fixnum? */
t5=*((C_word*)lf[90]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t4=t3;
f_7576(t4,C_SCHEME_FALSE);}}

/* k7574 in chicken.compiler.support#immediate? in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_7576(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,1)))){
C_save_and_reclaim_args((void *)trf_7576,2,t0,t1);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_eqp(C_SCHEME_UNDEFINED,((C_word*)t0)[3]);
if(C_truep(t2)){
t3=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_nullp(((C_word*)t0)[3]);
if(C_truep(t3)){
t4=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_eofp(((C_word*)t0)[3]);
if(C_truep(t4)){
t5=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_charp(((C_word*)t0)[3]);
t6=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t6;
av2[1]=(C_truep(t5)?t5:C_booleanp(((C_word*)t0)[3]));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}}}}

/* k7614 in chicken.compiler.support#immediate? in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7616(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7616,c,av);}
t2=((C_word*)t0)[2];
f_7576(t2,C_i_not(t1));}

/* chicken.compiler.support#basic-literal? in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7618(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7618,c,av);}
a=C_alloc(4);
t3=C_i_nullp(t2);
if(C_truep(t3)){
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_i_symbolp(t2);
if(C_truep(t4)){
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7634,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* support.scm:321: constant? */
t6=*((C_word*)lf[84]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}}}

/* k7632 in chicken.compiler.support#basic-literal? in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7634(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_7634,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7640,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_vectorp(((C_word*)t0)[3]))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7672,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* support.scm:322: scheme#vector->list */
t4=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_7640(2,av2);}}}}

/* k7638 in k7632 in chicken.compiler.support#basic-literal? in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7640(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7640,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[3]))){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7655,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:324: basic-literal? */
t3=*((C_word*)lf[91]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_u_i_car(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}}

/* k7653 in k7638 in k7632 in chicken.compiler.support#basic-literal? in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7655(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7655,c,av);}
if(C_truep(t1)){
/* support.scm:325: basic-literal? */
t2=*((C_word*)lf[91]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_u_i_cdr(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k7670 in k7632 in chicken.compiler.support#basic-literal? in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7672(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7672,c,av);}
/* support.scm:322: every */
f_5683(((C_word*)t0)[2],*((C_word*)lf[91]+1),t1);}

/* chicken.compiler.support#canonicalize-begin-body in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7674(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_7674,c,av);}
a=C_alloc(5);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7680,a[2]=t4,tmp=(C_word)a,a+=3,tmp));
t6=((C_word*)t4)[1];
f_7680(t6,t1,t2);}

/* loop in chicken.compiler.support#canonicalize-begin-body in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_7680(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_7680,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=lf[94];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_cdr(t2);
if(C_truep(C_i_nullp(t3))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_u_i_car(t2);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_u_i_car(t2);
t5=C_i_equalp(t4,lf[95]);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7704,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(t5)){
t7=t6;
f_7704(t7,t5);}
else{
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7735,a[2]=t6,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* support.scm:336: constant? */
t8=*((C_word*)lf[84]+1);{
C_word av2[3];
av2[0]=t8;
av2[1]=t7;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}}}}

/* k7702 in loop in chicken.compiler.support#canonicalize-begin-body in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_7704(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_7704,2,t0,t1);}
a=C_alloc(5);
if(C_truep(t1)){
/* support.scm:338: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_7680(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]));}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7730,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* support.scm:339: chicken.base#gensym */
t3=*((C_word*)lf[97]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[98];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k7718 in k7728 in k7702 in loop in chicken.compiler.support#canonicalize-begin-body in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7720(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_7720,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[96],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7728 in k7702 in loop in chicken.compiler.support#canonicalize-begin-body in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7730(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_7730,c,av);}
a=C_alloc(13);
t2=C_u_i_car(((C_word*)t0)[2]);
t3=C_a_i_list(&a,2,t1,t2);
t4=C_a_i_list(&a,1,t3);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7720,a[2]=((C_word*)t0)[3],a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* support.scm:340: loop */
t6=((C_word*)((C_word*)t0)[4])[1];
f_7680(t6,t5,C_u_i_cdr(((C_word*)t0)[2]));}

/* k7733 in loop in chicken.compiler.support#canonicalize-begin-body in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7735(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7735,c,av);}
t2=((C_word*)t0)[2];
f_7704(t2,(C_truep(t1)?t1:C_i_equalp(((C_word*)t0)[3],lf[99])));}

/* k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7749(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_7749,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7752,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* support.scm:345: chicken.condition#condition-property-accessor */
t3=*((C_word*)lf[535]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[536];
av2[3]=lf[537];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7752(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(51,c,6)))){
C_save_and_reclaim((void *)f_7752,c,av);}
a=C_alloc(51);
t2=C_mutate((C_word*)lf[100]+1 /* (set! chicken.compiler.support#string->expr ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7753,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp));
t3=C_mutate((C_word*)lf[108]+1 /* (set! chicken.compiler.support#llist-length ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7871,tmp=(C_word)a,a+=2,tmp));
t4=C_mutate((C_word*)lf[109]+1 /* (set! chicken.compiler.support#llist-match? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7874,tmp=(C_word)a,a+=2,tmp));
t5=lf[110] /* chicken.compiler.support#profile-info-vector-name */ =C_SCHEME_FALSE;;
t6=C_mutate((C_word*)lf[111]+1 /* (set! chicken.compiler.support#reset-profile-info-vector-name! ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7919,tmp=(C_word)a,a+=2,tmp));
t7=lf[114] /* chicken.compiler.support#profile-lambda-list */ =C_SCHEME_END_OF_LIST;;
t8=lf[115] /* chicken.compiler.support#profile-lambda-index */ =C_fix(0);;
t9=C_mutate((C_word*)lf[116]+1 /* (set! chicken.compiler.support#expand-profile-lambda ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7928,tmp=(C_word)a,a+=2,tmp));
t10=C_mutate((C_word*)lf[122]+1 /* (set! chicken.compiler.support#profiling-prelude-exps ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7983,tmp=(C_word)a,a+=2,tmp));
t11=C_mutate((C_word*)lf[127]+1 /* (set! chicken.compiler.support#db-get ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8072,tmp=(C_word)a,a+=2,tmp));
t12=C_mutate((C_word*)lf[129]+1 /* (set! chicken.compiler.support#db-get-all ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8090,tmp=(C_word)a,a+=2,tmp));
t13=C_mutate((C_word*)lf[131]+1 /* (set! chicken.compiler.support#db-put! ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8108,tmp=(C_word)a,a+=2,tmp));
t14=C_mutate((C_word*)lf[133]+1 /* (set! chicken.compiler.support#collect! ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8154,tmp=(C_word)a,a+=2,tmp));
t15=C_mutate((C_word*)lf[134]+1 /* (set! chicken.compiler.support#db-get-list ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8206,tmp=(C_word)a,a+=2,tmp));
t16=C_mutate((C_word*)lf[135]+1 /* (set! chicken.compiler.support#get-line ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8215,tmp=(C_word)a,a+=2,tmp));
t17=C_mutate((C_word*)lf[137]+1 /* (set! chicken.compiler.support#get-line-2 ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8225,tmp=(C_word)a,a+=2,tmp));
t18=C_mutate((C_word*)lf[138]+1 /* (set! chicken.compiler.support#display-line-number-database ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8266,tmp=(C_word)a,a+=2,tmp));
t19=C_mutate((C_word*)lf[140]+1 /* (set! chicken.compiler.support#make-node ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8338,tmp=(C_word)a,a+=2,tmp));
t20=C_mutate((C_word*)lf[142]+1 /* (set! chicken.compiler.support#node? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8344,tmp=(C_word)a,a+=2,tmp));
t21=C_mutate((C_word*)lf[143]+1 /* (set! chicken.compiler.support#node-class ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8350,tmp=(C_word)a,a+=2,tmp));
t22=C_mutate((C_word*)lf[145]+1 /* (set! chicken.compiler.support#node-class-set! ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8359,tmp=(C_word)a,a+=2,tmp));
t23=C_mutate((C_word*)lf[147]+1 /* (set! chicken.compiler.support#node-parameters ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8368,tmp=(C_word)a,a+=2,tmp));
t24=C_mutate((C_word*)lf[149]+1 /* (set! chicken.compiler.support#node-parameters-set! ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8377,tmp=(C_word)a,a+=2,tmp));
t25=C_mutate((C_word*)lf[150]+1 /* (set! chicken.compiler.support#node-subexpressions ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8386,tmp=(C_word)a,a+=2,tmp));
t26=C_mutate((C_word*)lf[152]+1 /* (set! chicken.compiler.support#node-subexpressions-set! ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8395,tmp=(C_word)a,a+=2,tmp));
t27=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8405,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
t28=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17919,tmp=(C_word)a,a+=2,tmp);
/* support.scm:475: ##sys#register-record-printer */
t29=*((C_word*)lf[534]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t29;
av2[1]=t27;
av2[2]=lf[141];
av2[3]=t28;
((C_proc)(void*)(*((C_word*)t29+1)))(4,av2);}}

/* chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7753(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_7753,c,av);}
a=C_alloc(8);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7757,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7762,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* support.scm:347: scheme#call-with-current-continuation */
t5=*((C_word*)lf[107]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k7755 in chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7757(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7757,c,av);}
/* support.scm:346: g1067 */
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a7761 in chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7762(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_7762,c,av);}
a=C_alloc(10);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7768,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7793,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* support.scm:347: chicken.condition#with-exception-handler */
t5=*((C_word*)lf[106]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=t3;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* a7767 in a7761 in chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7768(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_7768,c,av);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7774,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* support.scm:347: k1064 */
t4=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* a7773 in a7767 in a7761 in chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7774(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_7774,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7782,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7785,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:350: exn? */
t4=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k7780 in a7773 in a7767 in a7761 in chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7782(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7782,c,av);}
/* support.scm:348: quit-compiling */
t2=*((C_word*)lf[37]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[101];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k7783 in a7773 in a7767 in a7761 in chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7785(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7785,c,av);}
if(C_truep(t1)){
/* support.scm:351: exn-msg */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
/* support.scm:352: chicken.string#->string */
t2=*((C_word*)lf[66]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}}

/* a7792 in a7761 in chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7793(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_7793,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7795,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7852,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7869,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* tmp14905 */
t5=t2;
f_7795(t5,t4);}

/* tmp14905 in a7792 in a7761 in chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_7795(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_7795,2,t0,t1);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7799,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7821,tmp=(C_word)a,a+=2,tmp);
/* support.scm:353: chicken.port#with-input-from-string */
t4=*((C_word*)lf[105]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k7797 in tmp14905 in a7792 in a7761 in chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7799(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_7799,c,av);}
a=C_alloc(3);
if(C_truep(C_i_nullp(t1))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[102];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_i_cdr(t1);
t3=C_i_nullp(t2);
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=(C_truep(t3)?C_u_i_car(t1):C_a_i_cons(&a,2,lf[103],t1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* a7820 in tmp14905 in a7792 in a7761 in chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7821(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_7821,c,av);}
a=C_alloc(5);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7827,a[2]=t3,tmp=(C_word)a,a+=3,tmp));
t5=((C_word*)t3)[1];
f_7827(t5,t1,C_SCHEME_END_OF_LIST);}

/* loop in a7820 in tmp14905 in a7792 in a7761 in chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_7827(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_7827,3,t0,t1,t2);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7831,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* support.scm:357: scheme#read */
t4=*((C_word*)lf[83]+1);{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k7829 in loop in a7820 in tmp14905 in a7792 in a7761 in chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7831(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7831,c,av);}
a=C_alloc(3);
if(C_truep(C_eofp(t1))){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7844,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:359: scheme#reverse */
t3=*((C_word*)lf[80]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[3]);
/* support.scm:360: loop */
t3=((C_word*)((C_word*)t0)[4])[1];
f_7827(t3,((C_word*)t0)[2],t2);}}

/* k7842 in k7829 in loop in a7820 in tmp14905 in a7792 in a7761 in chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7844(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7844,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[104]+1);
av2[3]=t1;
C_apply(4,av2);}}

/* tmp24906 in a7792 in a7761 in chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_7852(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_7852,3,t0,t1,t2);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7858,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* support.scm:347: k1064 */
t4=((C_word*)t0)[2];{
C_word av2[3];
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* a7857 in tmp24906 in a7792 in a7761 in chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7858(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7858,c,av);}{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
C_apply_values(3,av2);}}

/* k7867 in a7792 in a7761 in chicken.compiler.support#string->expr in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7869(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7869,c,av);}
a=C_alloc(3);
/* tmp24906 */
t2=((C_word*)t0)[2];
f_7852(t2,((C_word*)t0)[3],C_a_i_list(&a,1,t1));}

/* chicken.compiler.support#llist-length in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7871(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7871,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_u_i_length(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.compiler.support#llist-match? in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7874(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,3)))){
C_save_and_reclaim((void *)f_7874,c,av);}
a=C_alloc(2);
t4=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7880,tmp=(C_word)a,a+=2,tmp);
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=(
  f_7880(t2,t3)
);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* loop in chicken.compiler.support#llist-match? in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static C_word C_fcall f_7880(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_nullp(t1))){
return(C_i_nullp(t2));}
else{
t3=C_i_symbolp(t1);
if(C_truep(t3)){
return(t3);}
else{
if(C_truep(C_i_nullp(t2))){
return(C_i_not_pair_p(t1));}
else{
t5=C_i_cdr(t1);
t6=C_i_cdr(t2);
t1=t5;
t2=t6;
goto loop;}}}}

/* chicken.compiler.support#reset-profile-info-vector-name! in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7919(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7919,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7924,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* support.scm:381: make-random-name */
t3=*((C_word*)lf[112]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[113];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7922 in chicken.compiler.support#reset-profile-info-vector-name! in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7924(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7924,c,av);}
t2=C_mutate(&lf[110] /* (set! chicken.compiler.support#profile-info-vector-name ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.compiler.support#expand-profile-lambda in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7928(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_7928,c,av);}
a=C_alloc(7);
t5=lf[115];
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7932,a[2]=t5,a[3]=t2,a[4]=t3,a[5]=t4,a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* support.scm:388: chicken.base#gensym */
t7=*((C_word*)lf[97]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}

/* k7930 in chicken.compiler.support#expand-profile-lambda in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7932(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(131,c,1)))){
C_save_and_reclaim((void *)f_7932,c,av);}
a=C_alloc(131);
t2=lf[114];
t3=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t4=C_a_i_cons(&a,2,t3,lf[114]);
t5=C_mutate(&lf[114] /* (set! chicken.compiler.support#profile-lambda-list ...) */,t4);
t6=C_mutate(&lf[115] /* (set! chicken.compiler.support#profile-lambda-index ...) */,C_s_a_i_plus(&a,2,((C_word*)t0)[2],C_fix(1)));
t7=C_a_i_list(&a,2,lf[85],((C_word*)t0)[2]);
t8=C_a_i_list(&a,3,lf[117],t7,lf[110]);
t9=C_a_i_list(&a,3,lf[118],C_SCHEME_END_OF_LIST,t8);
t10=C_a_i_list(&a,3,lf[118],((C_word*)t0)[4],((C_word*)t0)[5]);
t11=C_a_i_list(&a,3,lf[119],t10,t1);
t12=C_a_i_list(&a,3,lf[118],C_SCHEME_END_OF_LIST,t11);
t13=C_a_i_list(&a,2,lf[85],((C_word*)t0)[2]);
t14=C_a_i_list(&a,3,lf[120],t13,lf[110]);
t15=C_a_i_list(&a,3,lf[118],C_SCHEME_END_OF_LIST,t14);
t16=C_a_i_list(&a,4,lf[121],t9,t12,t15);
t17=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t17;
av2[1]=C_a_i_list(&a,3,lf[118],t1,t16);
((C_proc)(void*)(*((C_word*)t17+1)))(2,av2);}}

/* chicken.compiler.support#profiling-prelude-exps in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_7983(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(46,c,3)))){
C_save_and_reclaim((void *)f_7983,c,av);}
a=C_alloc(46);
t3=C_i_length(lf[114]);
t4=C_a_i_list(&a,2,lf[85],t3);
t5=C_a_i_list(&a,2,lf[85],t2);
t6=C_a_i_list(&a,3,lf[123],t4,t5);
t7=C_a_i_list(&a,3,lf[124],lf[110],t6);
t8=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t9=t8;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=((C_word*)t10)[1];
t12=lf[114];
t13=C_i_check_list_2(lf[114],lf[125]);
t14=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8020,a[2]=t1,a[3]=t7,tmp=(C_word)a,a+=4,tmp);
t15=C_SCHEME_UNDEFINED;
t16=(*a=C_VECTOR_TYPE|1,a[1]=t15,tmp=(C_word)a,a+=2,tmp);
t17=C_set_block_item(t16,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8022,a[2]=t10,a[3]=t16,a[4]=t11,tmp=(C_word)a,a+=5,tmp));
t18=((C_word*)t16)[1];
f_8022(t18,t14,lf[114]);}

/* k8018 in chicken.compiler.support#profiling-prelude-exps in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8020(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_8020,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop1122 in chicken.compiler.support#profiling-prelude-exps in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_8022(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(27,0,2)))){
C_save_and_reclaim_args((void *)trf_8022,3,t0,t1,t2);}
a=C_alloc(27);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_list(&a,2,lf[85],t4);
t6=C_u_i_cdr(t3);
t7=C_a_i_list(&a,2,lf[85],t6);
t8=C_a_i_list(&a,4,lf[126],lf[110],t5,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t1=t13;
t2=t14;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.compiler.support#db-get in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8072(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_8072,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8076,a[2]=t4,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* support.scm:413: chicken.internal#hash-table-ref */
t6=*((C_word*)lf[128]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k8074 in chicken.compiler.support#db-get in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8076(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8076,c,av);}
if(C_truep(t1)){
t2=C_i_assq(((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(t2)?C_slot(t2,C_fix(1)):C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* chicken.compiler.support#db-get-all in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8090(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +4,c,3)))){
C_save_and_reclaim((void*)f_8090,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+4);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8094,a[2]=t4,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* support.scm:419: chicken.internal#hash-table-ref */
t6=*((C_word*)lf[128]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k8092 in chicken.compiler.support#db-get-all in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8094(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_8094,c,av);}
a=C_alloc(9);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8102,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t3=C_i_check_list_2(((C_word*)t0)[2],lf[130]);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5976,a[2]=t2,a[3]=t5,tmp=(C_word)a,a+=4,tmp));
t7=((C_word*)t5)[1];
f_5976(t7,((C_word*)t0)[3],((C_word*)t0)[2]);}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* a8101 in k8092 in chicken.compiler.support#db-get-all in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static C_word C_fcall f_8102(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_assq(t1,((C_word*)t0)[2]));}

/* chicken.compiler.support#db-put! in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8108(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_8108,c,av);}
a=C_alloc(7);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8112,a[2]=t4,a[3]=t1,a[4]=t5,a[5]=t2,a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* support.scm:425: chicken.internal#hash-table-ref */
t7=*((C_word*)lf[128]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=t2;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}

/* k8110 in chicken.compiler.support#db-put! in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8112(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_8112,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=C_i_assq(((C_word*)t0)[2],t1);
if(C_truep(t2)){
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_setslot(t2,C_fix(1),((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
if(C_truep(((C_word*)t0)[4])){
t3=C_slot(t1,C_fix(1));
t4=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[4]);
t5=C_a_i_cons(&a,2,t4,t3);
t6=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_i_setslot(t1,C_fix(1),t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}}
else{
if(C_truep(((C_word*)t0)[4])){
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[4]);
t3=C_a_i_list1(&a,1,t2);
/* support.scm:430: chicken.internal#hash-table-set! */
t4=*((C_word*)lf[132]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)t0)[6];
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}}

/* chicken.compiler.support#collect! in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8154(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_8154,c,av);}
a=C_alloc(7);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8158,a[2]=t4,a[3]=t5,a[4]=t1,a[5]=t2,a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* support.scm:433: chicken.internal#hash-table-ref */
t7=*((C_word*)lf[128]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=t2;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}

/* k8156 in chicken.compiler.support#collect! in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8158(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_8158,c,av);}
a=C_alloc(9);
if(C_truep(t1)){
t2=C_i_assq(((C_word*)t0)[2],t1);
if(C_truep(t2)){
t3=C_slot(t2,C_fix(1));
t4=C_a_i_cons(&a,2,((C_word*)t0)[3],t3);
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_i_setslot(t2,C_fix(1),t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t3=C_a_i_list1(&a,1,((C_word*)t0)[3]);
t4=C_slot(t1,C_fix(1));
t5=C_a_i_cons(&a,2,((C_word*)t0)[2],t3);
t6=C_a_i_cons(&a,2,t5,t4);
t7=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_i_setslot(t1,C_fix(1),t6);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}
else{
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_list1(&a,1,t2);
/* support.scm:438: chicken.internal#hash-table-set! */
t4=*((C_word*)lf[132]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[4];
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)t0)[6];
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}

/* chicken.compiler.support#db-get-list in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8206(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_8206,c,av);}
a=C_alloc(3);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8210,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* support.scm:441: db-get */
t6=*((C_word*)lf[127]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
av2[3]=t3;
av2[4]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k8208 in chicken.compiler.support#db-get-list in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8210(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8210,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(t1)?t1:C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.compiler.support#get-line in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8215(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8215,c,av);}
/* support.scm:448: db-get */
t3=*((C_word*)lf[127]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=*((C_word*)lf[136]+1);
av2[3]=C_i_car(t2);
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* chicken.compiler.support#get-line-2 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8225(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_8225,c,av);}
a=C_alloc(5);
t3=C_i_car(t2);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8232,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* support.scm:452: chicken.internal#hash-table-ref */
t5=*((C_word*)lf[128]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=*((C_word*)lf[136]+1);
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k8230 in chicken.compiler.support#get-line-2 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8232(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_8232,c,av);}
a=C_alloc(3);
t2=(C_truep(t1)?C_i_assq(((C_word*)t0)[2],C_i_cdr(t1)):C_SCHEME_FALSE);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8239,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* support.scm:453: g1205 */
t4=t3;
f_8239(t4,((C_word*)t0)[3],t2);}
else{
/* support.scm:455: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=C_SCHEME_FALSE;
C_values(4,av2);}}}

/* g1205 in k8230 in chicken.compiler.support#get-line-2 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_8239(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_8239,3,t0,t1,t2);}
/* support.scm:454: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=C_i_car(((C_word*)t0)[2]);
av2[3]=C_i_cdr(t2);
C_values(4,av2);}}

/* chicken.compiler.support#display-line-number-database in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8266(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,4)))){
C_save_and_reclaim((void *)f_8266,c,av);}
a=C_alloc(2);
t2=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8272,tmp=(C_word)a,a+=2,tmp);
/* support.scm:458: chicken.internal#hash-table-for-each */
t3=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
av2[3]=*((C_word*)lf[136]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* a8271 in chicken.compiler.support#display-line-number-database in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8272(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_8272,c,av);}
a=C_alloc(5);
if(C_truep(t3)){
t4=*((C_word*)lf[24]+1);
t5=*((C_word*)lf[24]+1);
t6=C_i_check_port_2(*((C_word*)lf[24]+1),C_fix(2),C_SCHEME_TRUE,lf[25]);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8282,a[2]=t1,a[3]=t4,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* support.scm:460: ##sys#print */
t8=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=t2;
av2[3]=C_SCHEME_TRUE;
av2[4]=*((C_word*)lf[24]+1);
((C_proc)(void*)(*((C_word*)t8+1)))(5,av2);}}
else{
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k8280 in a8271 in chicken.compiler.support#display-line-number-database in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8282(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_8282,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8285,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* support.scm:460: ##sys#write-char-0 */
t3=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(32);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k8283 in k8280 in a8271 in chicken.compiler.support#display-line-number-database in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8285(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,3)))){
C_save_and_reclaim((void *)f_8285,c,av);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8288,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_i_check_list_2(((C_word*)t0)[4],lf[125]);
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8301,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8303,a[2]=t5,a[3]=t10,a[4]=t6,tmp=(C_word)a,a+=5,tmp));
t12=((C_word*)t10)[1];
f_8303(t12,t8,((C_word*)t0)[4]);}

/* k8286 in k8283 in k8280 in a8271 in chicken.compiler.support#display-line-number-database in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8288(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8288,c,av);}
/* support.scm:460: ##sys#write-char-0 */
t2=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k8299 in k8283 in k8280 in a8271 in chicken.compiler.support#display-line-number-database in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8301(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8301,c,av);}
/* support.scm:460: ##sys#print */
t2=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* map-loop1217 in k8283 in k8280 in a8271 in chicken.compiler.support#display-line-number-database in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_8303(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_8303,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_cdr(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.compiler.support#make-node in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8338(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_8338,c,av);}
a=C_alloc(5);
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_record4(&a,4,lf[141],t2,t3,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* chicken.compiler.support#node? in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8344(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8344,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_structurep(t2,lf[141]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.compiler.support#node-class in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8350(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8350,c,av);}
t3=C_i_check_structure_2(t2,lf[141],lf[144]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_block_ref(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.compiler.support#node-class-set! in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8359(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8359,c,av);}
t4=C_i_check_structure_2(t2,lf[141],C_SCHEME_FALSE);
/* support.scm:468: ##sys#block-set! */
t5=*((C_word*)lf[146]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=t2;
av2[3]=C_fix(1);
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* chicken.compiler.support#node-parameters in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8368(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8368,c,av);}
t3=C_i_check_structure_2(t2,lf[141],lf[148]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_block_ref(t2,C_fix(2));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.compiler.support#node-parameters-set! in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8377(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8377,c,av);}
t4=C_i_check_structure_2(t2,lf[141],C_SCHEME_FALSE);
/* support.scm:468: ##sys#block-set! */
t5=*((C_word*)lf[146]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=t2;
av2[3]=C_fix(2);
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* chicken.compiler.support#node-subexpressions in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8386(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8386,c,av);}
t3=C_i_check_structure_2(t2,lf[141],lf[151]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_block_ref(t2,C_fix(3));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.compiler.support#node-subexpressions-set! in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8395(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8395,c,av);}
t4=C_i_check_structure_2(t2,lf[141],C_SCHEME_FALSE);
/* support.scm:468: ##sys#block-set! */
t5=*((C_word*)lf[146]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=t2;
av2[3]=C_fix(3);
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8405(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word t44;
C_word t45;
C_word t46;
C_word t47;
C_word t48;
C_word t49;
C_word t50;
C_word t51;
C_word t52;
C_word t53;
C_word t54;
C_word t55;
C_word t56;
C_word t57;
C_word t58;
C_word t59;
C_word t60;
C_word t61;
C_word t62;
C_word t63;
C_word t64;
C_word t65;
C_word t66;
C_word t67;
C_word t68;
C_word t69;
C_word t70;
C_word t71;
C_word t72;
C_word t73;
C_word t74;
C_word t75;
C_word t76;
C_word t77;
C_word t78;
C_word t79;
C_word t80;
C_word t81;
C_word t82;
C_word t83;
C_word t84;
C_word t85;
C_word t86;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(160,c,8)))){
C_save_and_reclaim((void *)f_8405,c,av);}
a=C_alloc(160);
t2=C_mutate((C_word*)lf[140]+1 /* (set! chicken.compiler.support#make-node ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8407,tmp=(C_word)a,a+=2,tmp));
t3=C_mutate((C_word*)lf[153]+1 /* (set! chicken.compiler.support#varnode ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8413,tmp=(C_word)a,a+=2,tmp));
t4=C_mutate((C_word*)lf[155]+1 /* (set! chicken.compiler.support#qnode ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8428,tmp=(C_word)a,a+=2,tmp));
t5=C_mutate((C_word*)lf[156]+1 /* (set! chicken.compiler.support#build-node-graph ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8443,tmp=(C_word)a,a+=2,tmp));
t6=C_mutate((C_word*)lf[192]+1 /* (set! chicken.compiler.support#build-expression-tree ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_9639,tmp=(C_word)a,a+=2,tmp));
t7=C_mutate((C_word*)lf[205]+1 /* (set! chicken.compiler.support#fold-boolean ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_10509,tmp=(C_word)a,a+=2,tmp));
t8=C_mutate((C_word*)lf[207]+1 /* (set! chicken.compiler.support#inline-lambda-bindings ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_10561,tmp=(C_word)a,a+=2,tmp));
t9=C_mutate((C_word*)lf[226]+1 /* (set! chicken.compiler.support#tree-copy ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_11636,tmp=(C_word)a,a+=2,tmp));
t10=C_mutate((C_word*)lf[227]+1 /* (set! chicken.compiler.support#copy-node ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_11666,tmp=(C_word)a,a+=2,tmp));
t11=C_mutate((C_word*)lf[214]+1 /* (set! chicken.compiler.support#copy-node! ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_11704,tmp=(C_word)a,a+=2,tmp));
t12=C_mutate((C_word*)lf[228]+1 /* (set! chicken.compiler.support#emit-global-inline-file ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_11900,tmp=(C_word)a,a+=2,tmp));
t13=C_mutate((C_word*)lf[254]+1 /* (set! chicken.compiler.support#load-inline-file ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12227,tmp=(C_word)a,a+=2,tmp));
t14=C_mutate((C_word*)lf[257]+1 /* (set! chicken.compiler.support#match-node ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12283,tmp=(C_word)a,a+=2,tmp));
t15=C_mutate((C_word*)lf[260]+1 /* (set! chicken.compiler.support#expression-has-side-effects? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12516,tmp=(C_word)a,a+=2,tmp));
t16=C_mutate((C_word*)lf[263]+1 /* (set! chicken.compiler.support#simple-lambda-node? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12625,tmp=(C_word)a,a+=2,tmp));
t17=C_mutate((C_word*)lf[264]+1 /* (set! chicken.compiler.support#dump-undefined-globals ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12751,tmp=(C_word)a,a+=2,tmp));
t18=C_mutate((C_word*)lf[269]+1 /* (set! chicken.compiler.support#dump-defined-globals ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12792,tmp=(C_word)a,a+=2,tmp));
t19=C_mutate((C_word*)lf[270]+1 /* (set! chicken.compiler.support#dump-global-refs ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12829,tmp=(C_word)a,a+=2,tmp));
t20=C_mutate((C_word*)lf[271]+1 /* (set! ##sys#toplevel-definition-hook ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12878,tmp=(C_word)a,a+=2,tmp));
t21=C_set_block_item(lf[262] /* chicken.compiler.support#foreign-callback-stubs */,0,C_SCHEME_END_OF_LIST);
t22=C_mutate((C_word*)lf[277]+1 /* (set! chicken.compiler.support#make-foreign-callback-stub ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12906,tmp=(C_word)a,a+=2,tmp));
t23=C_mutate((C_word*)lf[279]+1 /* (set! chicken.compiler.support#foreign-callback-stub? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12912,tmp=(C_word)a,a+=2,tmp));
t24=C_mutate((C_word*)lf[261]+1 /* (set! chicken.compiler.support#foreign-callback-stub-id ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12918,tmp=(C_word)a,a+=2,tmp));
t25=C_mutate((C_word*)lf[281]+1 /* (set! chicken.compiler.support#foreign-callback-stub-name ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12927,tmp=(C_word)a,a+=2,tmp));
t26=C_mutate((C_word*)lf[283]+1 /* (set! chicken.compiler.support#foreign-callback-stub-qualifiers ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12936,tmp=(C_word)a,a+=2,tmp));
t27=C_mutate((C_word*)lf[285]+1 /* (set! chicken.compiler.support#foreign-callback-stub-return-type ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12945,tmp=(C_word)a,a+=2,tmp));
t28=C_mutate((C_word*)lf[287]+1 /* (set! chicken.compiler.support#foreign-callback-stub-argument-types ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12954,tmp=(C_word)a,a+=2,tmp));
t29=C_mutate((C_word*)lf[289]+1 /* (set! chicken.compiler.support#register-foreign-callback-stub! ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12963,tmp=(C_word)a,a+=2,tmp));
t30=lf[291] /* chicken.compiler.support#foreign-type-table */ =C_SCHEME_FALSE;;
t31=C_mutate((C_word*)lf[292]+1 /* (set! chicken.compiler.support#clear-foreign-type-table! ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_12992,tmp=(C_word)a,a+=2,tmp));
t32=C_mutate((C_word*)lf[295]+1 /* (set! chicken.compiler.support#register-foreign-type! ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_13005,tmp=(C_word)a,a+=2,tmp));
t33=C_mutate((C_word*)lf[296]+1 /* (set! chicken.compiler.support#lookup-foreign-type ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_13059,tmp=(C_word)a,a+=2,tmp));
t34=lf[297];
t35=C_mutate((C_word*)lf[298]+1 /* (set! chicken.compiler.support#foreign-type-check ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_13065,a[2]=t34,tmp=(C_word)a,a+=3,tmp));
t36=C_mutate(&lf[395] /* (set! chicken.compiler.support#foreign-type-result-converter ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_14141,tmp=(C_word)a,a+=2,tmp));
t37=C_mutate(&lf[396] /* (set! chicken.compiler.support#foreign-type-argument-converter ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_14162,tmp=(C_word)a,a+=2,tmp));
t38=C_mutate((C_word*)lf[397]+1 /* (set! chicken.compiler.support#foreign-type-convert-result ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_14183,tmp=(C_word)a,a+=2,tmp));
t39=C_mutate((C_word*)lf[398]+1 /* (set! chicken.compiler.support#foreign-type-convert-argument ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_14198,tmp=(C_word)a,a+=2,tmp));
t40=C_mutate((C_word*)lf[399]+1 /* (set! chicken.compiler.support#final-foreign-type ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_14213,tmp=(C_word)a,a+=2,tmp));
t41=C_mutate((C_word*)lf[401]+1 /* (set! chicken.compiler.support#estimate-foreign-result-size ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_14252,tmp=(C_word)a,a+=2,tmp));
t42=C_mutate((C_word*)lf[407]+1 /* (set! chicken.compiler.support#estimate-foreign-result-location-size ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_14727,tmp=(C_word)a,a+=2,tmp));
t43=C_mutate((C_word*)lf[410]+1 /* (set! chicken.compiler.support#finish-foreign-result ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_15177,tmp=(C_word)a,a+=2,tmp));
t44=C_mutate((C_word*)lf[424]+1 /* (set! chicken.compiler.support#foreign-type->scrutiny-type ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_15445,tmp=(C_word)a,a+=2,tmp));
t45=C_mutate((C_word*)lf[448]+1 /* (set! chicken.compiler.support#scan-used-variables ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16075,tmp=(C_word)a,a+=2,tmp));
t46=C_mutate((C_word*)lf[449]+1 /* (set! chicken.compiler.support#scan-free-variables ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16230,tmp=(C_word)a,a+=2,tmp));
t47=C_mutate((C_word*)lf[450]+1 /* (set! chicken.compiler.support#chop-separator ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16460,tmp=(C_word)a,a+=2,tmp));
t48=C_mutate((C_word*)lf[452]+1 /* (set! chicken.compiler.support#make-block-variable-literal ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16484,tmp=(C_word)a,a+=2,tmp));
t49=C_mutate((C_word*)lf[454]+1 /* (set! chicken.compiler.support#block-variable-literal? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16490,tmp=(C_word)a,a+=2,tmp));
t50=C_mutate((C_word*)lf[455]+1 /* (set! chicken.compiler.support#block-variable-literal-name ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16496,tmp=(C_word)a,a+=2,tmp));
t51=C_mutate((C_word*)lf[112]+1 /* (set! chicken.compiler.support#make-random-name ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16505,tmp=(C_word)a,a+=2,tmp));
t52=lf[459] /* chicken.compiler.support#real-name-table */ =C_SCHEME_FALSE;;
t53=C_mutate((C_word*)lf[460]+1 /* (set! chicken.compiler.support#clear-real-name-table! ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16552,tmp=(C_word)a,a+=2,tmp));
t54=C_mutate((C_word*)lf[461]+1 /* (set! chicken.compiler.support#set-real-name! ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16559,tmp=(C_word)a,a+=2,tmp));
t55=C_mutate((C_word*)lf[462]+1 /* (set! chicken.compiler.support#get-real-name ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16565,tmp=(C_word)a,a+=2,tmp));
t56=C_mutate((C_word*)lf[181]+1 /* (set! chicken.compiler.support#real-name ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16572,tmp=(C_word)a,a+=2,tmp));
t57=C_mutate((C_word*)lf[469]+1 /* (set! chicken.compiler.support#real-name2 ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16692,tmp=(C_word)a,a+=2,tmp));
t58=C_mutate((C_word*)lf[470]+1 /* (set! chicken.compiler.support#display-real-name-table ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16704,tmp=(C_word)a,a+=2,tmp));
t59=C_mutate((C_word*)lf[471]+1 /* (set! chicken.compiler.support#source-info->string ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16728,tmp=(C_word)a,a+=2,tmp));
t60=C_mutate((C_word*)lf[477]+1 /* (set! chicken.compiler.support#source-info->name ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16763,tmp=(C_word)a,a+=2,tmp));
t61=C_mutate((C_word*)lf[478]+1 /* (set! chicken.compiler.support#source-info->line ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16778,tmp=(C_word)a,a+=2,tmp));
t62=C_mutate((C_word*)lf[479]+1 /* (set! chicken.compiler.support#call-info ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16790,tmp=(C_word)a,a+=2,tmp));
t63=C_mutate((C_word*)lf[482]+1 /* (set! chicken.compiler.support#constant-form-eval ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_16827,tmp=(C_word)a,a+=2,tmp));
t64=C_mutate((C_word*)lf[489]+1 /* (set! chicken.compiler.support#maybe-constant-fold-call ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17057,tmp=(C_word)a,a+=2,tmp));
t65=C_mutate(&lf[485] /* (set! chicken.compiler.support#encodeable-literal? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17156,tmp=(C_word)a,a+=2,tmp));
t66=C_mutate((C_word*)lf[494]+1 /* (set! chicken.compiler.support#dump-nodes ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17293,tmp=(C_word)a,a+=2,tmp));
t67=C_mutate((C_word*)lf[496]+1 /* (set! chicken.compiler.support#read-info-hook ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17449,tmp=(C_word)a,a+=2,tmp));
t68=C_mutate((C_word*)lf[500]+1 /* (set! chicken.compiler.support#read/source-info ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17492,tmp=(C_word)a,a+=2,tmp));
t69=*((C_word*)lf[502]+1);
t70=C_mutate((C_word*)lf[502]+1 /* (set! ##sys#user-read-hook ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_17498,a[2]=t69,tmp=(C_word)a,a+=3,tmp));
t71=C_mutate((C_word*)lf[90]+1 /* (set! chicken.compiler.support#big-fixnum? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17592,tmp=(C_word)a,a+=2,tmp));
t72=C_mutate((C_word*)lf[509]+1 /* (set! chicken.compiler.support#small-bignum? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17616,tmp=(C_word)a,a+=2,tmp));
t73=C_mutate((C_word*)lf[275]+1 /* (set! chicken.compiler.support#hide-variable ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17640,tmp=(C_word)a,a+=2,tmp));
t74=C_mutate((C_word*)lf[511]+1 /* (set! chicken.compiler.support#export-variable ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17660,tmp=(C_word)a,a+=2,tmp));
t75=C_mutate((C_word*)lf[274]+1 /* (set! chicken.compiler.support#variable-hidden? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17680,tmp=(C_word)a,a+=2,tmp));
t76=C_mutate((C_word*)lf[253]+1 /* (set! chicken.compiler.support#variable-visible? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17702,tmp=(C_word)a,a+=2,tmp));
t77=C_mutate((C_word*)lf[513]+1 /* (set! chicken.compiler.support#mark-variable ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17727,tmp=(C_word)a,a+=2,tmp));
t78=C_mutate((C_word*)lf[514]+1 /* (set! chicken.compiler.support#variable-mark ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17742,tmp=(C_word)a,a+=2,tmp));
t79=C_mutate((C_word*)lf[515]+1 /* (set! chicken.compiler.support#intrinsic? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17748,tmp=(C_word)a,a+=2,tmp));
t80=C_mutate((C_word*)lf[491]+1 /* (set! chicken.compiler.support#foldable? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17759,tmp=(C_word)a,a+=2,tmp));
t81=C_mutate((C_word*)lf[490]+1 /* (set! chicken.compiler.support#predicate? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17770,tmp=(C_word)a,a+=2,tmp));
t82=C_mutate((C_word*)lf[518]+1 /* (set! chicken.compiler.support#load-identifier-database ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17781,tmp=(C_word)a,a+=2,tmp));
t83=C_mutate((C_word*)lf[526]+1 /* (set! chicken.compiler.support#print-version ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17876,tmp=(C_word)a,a+=2,tmp));
t84=C_mutate((C_word*)lf[529]+1 /* (set! chicken.compiler.support#print-usage ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17901,tmp=(C_word)a,a+=2,tmp));
t85=C_mutate((C_word*)lf[531]+1 /* (set! chicken.compiler.support#print-debug-options ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_17913,tmp=(C_word)a,a+=2,tmp));
t86=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t86;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t86+1)))(2,av2);}}

/* chicken.compiler.support#make-node in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8407(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_8407,c,av);}
a=C_alloc(5);
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_record4(&a,4,lf[141],t2,t3,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* chicken.compiler.support#varnode in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8413(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,1)))){
C_save_and_reclaim((void *)f_8413,c,av);}
a=C_alloc(8);
t3=C_a_i_list1(&a,1,t2);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[154],t3,C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.compiler.support#qnode in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8428(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,1)))){
C_save_and_reclaim((void *)f_8428,c,av);}
a=C_alloc(8);
t3=C_a_i_list1(&a,1,t2);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[85],t3,C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8443(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_8443,c,av);}
a=C_alloc(12);
t3=C_fix(0);
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8446,a[2]=t6,a[3]=t4,tmp=(C_word)a,a+=4,tmp));
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9630,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* support.scm:577: walk */
t9=((C_word*)t6)[1];
f_8446(t9,t8,t2);}

/* walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_8446(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(20,0,3)))){
C_save_and_reclaim_args((void *)trf_8446,3,t0,t1,t2);}
a=C_alloc(20);
if(C_truep(C_i_symbolp(t2))){
/* support.scm:487: varnode */
t3=*((C_word*)lf[153]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
if(C_truep(C_i_structurep(t2,lf[141]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_pairp(t2);
if(C_truep(C_i_not(t3))){
/* support.scm:489: bomb */
t4=*((C_word*)lf[13]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t1;
av2[2]=lf[157];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=C_i_car(t2);
if(C_truep(C_i_symbolp(t4))){
t5=C_u_i_car(t2);
t6=C_eqp(t5,lf[158]);
t7=(C_truep(t6)?t6:C_eqp(t5,lf[159]));
if(C_truep(t7)){
t8=C_u_i_car(t2);
t9=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t10=t9;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=((C_word*)t11)[1];
t13=C_u_i_cdr(t2);
t14=C_i_check_list_2(t13,lf[125]);
t15=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8513,a[2]=t1,a[3]=t8,tmp=(C_word)a,a+=4,tmp);
t16=C_SCHEME_UNDEFINED;
t17=(*a=C_VECTOR_TYPE|1,a[1]=t16,tmp=(C_word)a,a+=2,tmp);
t18=C_set_block_item(t17,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8515,a[2]=t11,a[3]=t17,a[4]=((C_word*)t0)[2],a[5]=t12,tmp=(C_word)a,a+=6,tmp));
t19=((C_word*)t17)[1];
f_8515(t19,t15,t13);}
else{
t8=C_eqp(t5,lf[85]);
if(C_truep(t8)){
t9=C_i_cadr(t2);
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8563,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t11=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8566,a[2]=t10,a[3]=t9,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_numberp(t9))){
t12=C_eqp(lf[164],*((C_word*)lf[8]+1));
t13=t11;
f_8566(t13,(C_truep(t12)?C_i_not(C_i_integerp(t9)):C_SCHEME_FALSE));}
else{
t12=t11;
f_8566(t12,C_SCHEME_FALSE);}}
else{
t9=C_eqp(t5,lf[96]);
if(C_truep(t9)){
t10=C_i_cadr(t2);
t11=C_i_caddr(t2);
if(C_truep(C_i_nullp(t10))){
/* support.scm:507: walk */
t22=t1;
t23=t11;
t1=t22;
t2=t23;
goto loop;}
else{
t12=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8623,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,a[5]=t11,tmp=(C_word)a,a+=6,tmp);
t13=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t14=t13;
t15=(*a=C_VECTOR_TYPE|1,a[1]=t14,tmp=(C_word)a,a+=2,tmp);
t16=((C_word*)t15)[1];
t17=C_i_check_list_2(t10,lf[125]);
t18=C_SCHEME_UNDEFINED;
t19=(*a=C_VECTOR_TYPE|1,a[1]=t18,tmp=(C_word)a,a+=2,tmp);
t20=C_set_block_item(t19,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6049,a[2]=t15,a[3]=t19,a[4]=t16,tmp=(C_word)a,a+=5,tmp));
t21=((C_word*)t19)[1];
f_6049(t21,t12,t10);}}
else{
t10=C_eqp(t5,lf[165]);
t11=(C_truep(t10)?t10:C_eqp(t5,lf[118]));
if(C_truep(t11)){
t12=C_i_cadr(t2);
t13=C_a_i_list1(&a,1,t12);
t14=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8720,a[2]=t1,a[3]=t13,tmp=(C_word)a,a+=4,tmp);
/* support.scm:513: walk */
t22=t14;
t23=C_i_caddr(t2);
t1=t22;
t2=t23;
goto loop;}
else{
t12=C_eqp(t5,lf[166]);
if(C_truep(t12)){
t13=C_i_cadr(t2);
t14=C_i_caddr(t2);
t15=C_a_i_list2(&a,2,t13,t14);
t16=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8754,a[2]=t1,a[3]=t15,tmp=(C_word)a,a+=4,tmp);
/* support.scm:517: walk */
t22=t16;
t23=C_i_cadddr(t2);
t1=t22;
t2=t23;
goto loop;}
else{
t13=C_eqp(t5,lf[167]);
if(C_truep(t13)){
t14=C_i_cdddr(t2);
t15=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8908,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=t14,tmp=(C_word)a,a+=6,tmp);
t16=C_u_i_cdr(t2);
t17=C_u_i_cdr(t16);
/* support.scm:520: walk */
t22=t15;
t23=C_u_i_car(t17);
t1=t22;
t2=t23;
goto loop;}
else{
t14=C_eqp(t5,lf[171]);
if(C_truep(t14)){
t15=C_i_cadr(t2);
t16=C_u_i_car(t2);
t17=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8935,a[2]=t2,a[3]=t1,a[4]=t16,a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_pairp(t15))){
t18=C_eqp(lf[85],C_u_i_car(t15));
if(C_truep(t18)){
t19=C_i_cadr(t15);
t20=t17;
f_8935(t20,C_a_i_list1(&a,1,t19));}
else{
t19=t17;
f_8935(t19,C_a_i_list1(&a,1,t15));}}
else{
t18=t17;
f_8935(t18,C_a_i_list1(&a,1,t15));}}
else{
t15=C_eqp(t5,lf[172]);
t16=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9008,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=t5,a[6]=((C_word*)t0)[3],tmp=(C_word)a,a+=7,tmp);
if(C_truep(t15)){
t17=t16;
f_9008(t17,t15);}
else{
t17=C_eqp(t5,lf[188]);
t18=t16;
f_9008(t18,(C_truep(t17)?t17:C_eqp(t5,lf[189])));}}}}}}}}}
else{
t5=C_a_i_list1(&a,1,C_SCHEME_FALSE);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9585,a[2]=t1,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9587,a[2]=t8,a[3]=t12,a[4]=((C_word*)t0)[2],a[5]=t9,tmp=(C_word)a,a+=6,tmp));
t14=((C_word*)t12)[1];
f_9587(t14,t10,t2);}}}}}

/* k8511 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8513(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_8513,c,av);}
a=C_alloc(5);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_record4(&a,4,lf[141],((C_word*)t0)[3],C_SCHEME_END_OF_LIST,t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop1347 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_8515(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_8515,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8540,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:492: g1353 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_8446(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k8538 in map-loop1347 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8540(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8540,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_8515(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k8561 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8563(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8563,c,av);}
/* support.scm:495: qnode */
t2=*((C_word*)lf[155]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k8564 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_8566(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_8566,2,t0,t1);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8569,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* support.scm:499: chicken.base#warning */
t3=*((C_word*)lf[162]+1);{
C_word av2[4];
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[163];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
/* support.scm:495: qnode */
t2=*((C_word*)lf[155]+1);{
C_word av2[3];
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}}

/* k8567 in k8564 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8569(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8569,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8576,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* support.scm:501: scheme#truncate */
t3=*((C_word*)lf[161]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k8574 in k8567 in k8564 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8576(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8576,c,av);}
/* support.scm:501: scheme#inexact->exact */
t2=*((C_word*)lf[160]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k8621 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8623(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(25,c,3)))){
C_save_and_reclaim((void *)f_8623,c,av);}
a=C_alloc(25);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8627,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8632,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
t8=C_i_cadr(((C_word*)t0)[4]);
t9=C_i_check_list_2(t8,lf[125]);
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8649,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8659,a[2]=t5,a[3]=t12,a[4]=t7,a[5]=t6,tmp=(C_word)a,a+=6,tmp));
t14=((C_word*)t12)[1];
f_8659(t14,t10,t8);}

/* k8625 in k8621 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8627(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_8627,c,av);}
a=C_alloc(5);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[96],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* g1390 in k8621 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_8632(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_8632,3,t0,t1,t2);}
/* support.scm:510: walk */
t3=((C_word*)((C_word*)t0)[2])[1];
f_8446(t3,t1,C_i_cadr(t2));}

/* k8647 in k8621 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8649(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_8649,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8657,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* support.scm:511: walk */
t3=((C_word*)((C_word*)t0)[3])[1];
f_8446(t3,t2,((C_word*)t0)[4]);}

/* k8655 in k8647 in k8621 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_8657,c,av);}
a=C_alloc(3);
t2=C_a_i_list1(&a,1,t1);
/* support.scm:510: scheme#append */
t3=*((C_word*)lf[58]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* map-loop1384 in k8621 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_8659(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_8659,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8684,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:510: g1390 */
t4=((C_word*)t0)[4];
f_8632(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k8682 in map-loop1384 in k8621 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8684(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8684,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_8659(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k8718 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8720(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,1)))){
C_save_and_reclaim((void *)f_8720,c,av);}
a=C_alloc(8);
t2=C_a_i_list1(&a,1,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[165],((C_word*)t0)[3],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k8752 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8754(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,1)))){
C_save_and_reclaim((void *)f_8754,c,av);}
a=C_alloc(8);
t2=C_a_i_list1(&a,1,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[166],((C_word*)t0)[3],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* loop in k8906 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_8785(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,2)))){
C_save_and_reclaim_args((void *)trf_8785,5,t0,t1,t2,t3,t4);}
a=C_alloc(14);
if(C_truep(C_i_nullp(t2))){
t5=C_i_cadr(((C_word*)t0)[2]);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8829,a[2]=t5,a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* support.scm:524: scheme#reverse */
t7=*((C_word*)lf[80]+1);{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}
else{
t5=C_i_caar(t2);
t6=C_eqp(lf[168],t5);
if(C_truep(t6)){
t7=C_i_cadr(((C_word*)t0)[2]);
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8871,a[2]=t7,a[3]=t1,a[4]=t4,a[5]=((C_word*)t0)[3],a[6]=t2,tmp=(C_word)a,a+=7,tmp);
t9=C_a_i_cons(&a,2,lf[170],t3);
/* support.scm:530: scheme#reverse */
t10=*((C_word*)lf[80]+1);{
C_word av2[3];
av2[0]=t10;
av2[1]=t8;
av2[2]=t9;
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}
else{
t7=C_u_i_cdr(t2);
t8=C_i_caar(t2);
t9=C_a_i_cons(&a,2,t8,t3);
t10=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8892,a[2]=t4,a[3]=((C_word*)t0)[4],a[4]=t1,a[5]=t7,a[6]=t9,tmp=(C_word)a,a+=7,tmp);
t11=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8896,a[2]=((C_word*)t0)[3],a[3]=t10,tmp=(C_word)a,a+=4,tmp);
/* support.scm:534: scheme#cadar */
t12=*((C_word*)lf[169]+1);{
C_word av2[3];
av2[0]=t12;
av2[1]=t11;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t12+1)))(3,av2);}}}}

/* k8806 in k8827 in loop in k8906 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8808(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_8808,c,av);}
a=C_alloc(5);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[167],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k8827 in loop in k8906 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8829(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_8829,c,av);}
a=C_alloc(15);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8808,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=C_a_i_record4(&a,4,lf[141],lf[159],C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST);
t5=C_a_i_cons(&a,2,t4,((C_word*)t0)[4]);
/* support.scm:525: scheme#reverse */
t6=*((C_word*)lf[80]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k8849 in k8869 in loop in k8906 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8851(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_8851,c,av);}
a=C_alloc(5);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[167],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k8857 in k8869 in loop in k8906 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8859(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8859,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
/* support.scm:531: scheme#reverse */
t3=*((C_word*)lf[80]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k8861 in k8869 in loop in k8906 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8863(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8863,c,av);}
/* support.scm:531: walk */
t2=((C_word*)((C_word*)t0)[2])[1];
f_8446(t2,((C_word*)t0)[3],t1);}

/* k8869 in loop in k8906 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8871(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_8871,c,av);}
a=C_alloc(15);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8851,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8859,a[2]=((C_word*)t0)[4],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8863,a[2]=((C_word*)t0)[5],a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* support.scm:531: scheme#cadar */
t6=*((C_word*)lf[169]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k8890 in loop in k8906 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8892(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_8892,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
/* support.scm:532: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_8785(t3,((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],t2);}

/* k8894 in loop in k8906 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8896(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8896,c,av);}
/* support.scm:534: walk */
t2=((C_word*)((C_word*)t0)[2])[1];
f_8446(t2,((C_word*)t0)[3],t1);}

/* k8906 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8908(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8908,c,av);}
a=C_alloc(10);
t2=C_a_i_list1(&a,1,t1);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8785,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t4,tmp=(C_word)a,a+=5,tmp));
t6=((C_word*)t4)[1];
f_8785(t6,((C_word*)t0)[4],((C_word*)t0)[5],C_SCHEME_END_OF_LIST,t2);}

/* k8933 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_8935(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,0,3)))){
C_save_and_reclaim_args((void *)trf_8935,2,t0,t1);}
a=C_alloc(18);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_u_i_cdr(((C_word*)t0)[2]);
t7=C_u_i_cdr(t6);
t8=C_i_check_list_2(t7,lf[125]);
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8947,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8949,a[2]=t4,a[3]=t11,a[4]=((C_word*)t0)[5],a[5]=t5,tmp=(C_word)a,a+=6,tmp));
t13=((C_word*)t11)[1];
f_8949(t13,t9,t7);}

/* k8945 in k8933 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8947(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_8947,c,av);}
a=C_alloc(5);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_record4(&a,4,lf[141],((C_word*)t0)[3],((C_word*)t0)[4],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop1455 in k8933 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_8949(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_8949,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8974,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:540: g1461 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_8446(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k8972 in map-loop1455 in k8933 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_8974(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8974,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_8949(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_9008(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,0,3)))){
C_save_and_reclaim_args((void *)trf_9008,2,t0,t1);}
a=C_alloc(21);
if(C_truep(t1)){
t2=C_u_i_car(((C_word*)t0)[2]);
t3=C_i_cadr(((C_word*)t0)[2]);
t4=C_a_i_list1(&a,1,t3);
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=C_u_i_cdr(((C_word*)t0)[2]);
t10=C_u_i_cdr(t9);
t11=C_i_check_list_2(t10,lf[125]);
t12=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9034,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t13=C_SCHEME_UNDEFINED;
t14=(*a=C_VECTOR_TYPE|1,a[1]=t13,tmp=(C_word)a,a+=2,tmp);
t15=C_set_block_item(t14,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9036,a[2]=t7,a[3]=t14,a[4]=((C_word*)t0)[4],a[5]=t8,tmp=(C_word)a,a+=6,tmp));
t16=((C_word*)t14)[1];
f_9036(t16,t12,t10);}
else{
t2=C_eqp(((C_word*)t0)[5],lf[173]);
if(C_truep(t2)){
t3=C_u_i_car(((C_word*)t0)[2]);
t4=C_u_i_cdr(((C_word*)t0)[2]);
t5=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t5;
av2[1]=C_a_i_record4(&a,4,lf[141],t3,t4,C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t3=C_eqp(((C_word*)t0)[5],lf[174]);
if(C_truep(t3)){
t4=C_i_cadr(((C_word*)t0)[2]);
t5=C_a_i_list2(&a,2,t4,C_SCHEME_TRUE);
t6=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t6;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[174],t5,C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t4=C_eqp(((C_word*)t0)[5],lf[124]);
t5=(C_truep(t4)?t4:C_eqp(((C_word*)t0)[5],lf[175]));
if(C_truep(t5)){
t6=C_i_cadr(((C_word*)t0)[2]);
t7=C_a_i_list1(&a,1,t6);
t8=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t9=t8;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=((C_word*)t10)[1];
t12=C_u_i_cdr(((C_word*)t0)[2]);
t13=C_u_i_cdr(t12);
t14=C_i_check_list_2(t13,lf[125]);
t15=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9145,a[2]=((C_word*)t0)[3],a[3]=t7,tmp=(C_word)a,a+=4,tmp);
t16=C_SCHEME_UNDEFINED;
t17=(*a=C_VECTOR_TYPE|1,a[1]=t16,tmp=(C_word)a,a+=2,tmp);
t18=C_set_block_item(t17,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9147,a[2]=t10,a[3]=t17,a[4]=((C_word*)t0)[4],a[5]=t11,tmp=(C_word)a,a+=6,tmp));
t19=((C_word*)t17)[1];
f_9147(t19,t15,t13);}
else{
t6=C_eqp(((C_word*)t0)[5],lf[176]);
if(C_truep(t6)){
t7=C_i_cadr(((C_word*)t0)[2]);
t8=C_i_cadr(t7);
t9=C_i_caddr(((C_word*)t0)[2]);
t10=C_i_cadr(t9);
t11=C_i_cadddr(((C_word*)t0)[2]);
t12=C_i_cadr(t11);
t13=C_i_cddddr(((C_word*)t0)[2]);
t14=C_i_car(t13);
t15=C_i_cadr(t14);
t16=C_a_i_list4(&a,4,t8,t10,t12,t15);
t17=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9212,a[2]=((C_word*)t0)[3],a[3]=t16,tmp=(C_word)a,a+=4,tmp);
/* support.scm:556: walk */
t18=((C_word*)((C_word*)t0)[4])[1];
f_8446(t18,t17,C_i_list_ref(((C_word*)t0)[2],C_fix(5)));}
else{
t7=C_eqp(((C_word*)t0)[5],lf[177]);
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9253,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(t7)){
t9=t8;
f_9253(t9,t7);}
else{
t9=C_eqp(((C_word*)t0)[5],lf[184]);
if(C_truep(t9)){
t10=t8;
f_9253(t10,t9);}
else{
t10=C_eqp(((C_word*)t0)[5],lf[185]);
if(C_truep(t10)){
t11=t8;
f_9253(t11,t10);}
else{
t11=C_eqp(((C_word*)t0)[5],lf[186]);
t12=t8;
f_9253(t12,(C_truep(t11)?t11:C_eqp(((C_word*)t0)[5],lf[187])));}}}}}}}}}

/* k9032 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9034(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_9034,c,av);}
a=C_alloc(5);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_record4(&a,4,lf[141],((C_word*)t0)[3],((C_word*)t0)[4],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop1492 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_9036(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_9036,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9061,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:542: g1498 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_8446(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9059 in map-loop1492 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9061(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_9061,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_9036(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k9143 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9145(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_9145,c,av);}
a=C_alloc(5);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[124],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop1536 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_9147(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_9147,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9172,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:550: g1542 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_8446(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9170 in map-loop1536 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9172(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_9172,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_9147(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k9210 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9212(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,1)))){
C_save_and_reclaim((void *)f_9212,c,av);}
a=C_alloc(8);
t2=C_a_i_list1(&a,1,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[176],((C_word*)t0)[3],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_9253(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,0,4)))){
C_save_and_reclaim_args((void *)trf_9253,2,t0,t1);}
a=C_alloc(20);
if(C_truep(t1)){
t2=C_i_car(((C_word*)t0)[2]);
t3=C_i_cadr(((C_word*)t0)[2]);
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=C_i_cddr(((C_word*)t0)[2]);
t9=C_i_check_list_2(t8,lf[125]);
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9282,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9284,a[2]=t6,a[3]=t12,a[4]=((C_word*)t0)[4],a[5]=t7,tmp=(C_word)a,a+=6,tmp));
t14=((C_word*)t12)[1];
f_9284(t14,t10,t8);}
else{
t2=C_eqp(((C_word*)t0)[5],lf[178]);
if(C_truep(t2)){
t3=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=C_u_i_cdr(((C_word*)t0)[2]);
t9=C_i_check_list_2(t8,lf[125]);
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9345,a[2]=((C_word*)t0)[3],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9347,a[2]=t6,a[3]=t12,a[4]=((C_word*)t0)[4],a[5]=t7,tmp=(C_word)a,a+=6,tmp));
t14=((C_word*)t12)[1];
f_9347(t14,t10,t8);}
else{
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9384,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9390,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
/* support.scm:563: ##sys#call-with-values */{
C_word av2[4];
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=t3;
av2[3]=t4;
C_call_with_values(4,av2);}}}}

/* k9280 in k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9282(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_9282,c,av);}
a=C_alloc(5);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_record4(&a,4,lf[141],((C_word*)t0)[3],((C_word*)t0)[4],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop1585 in k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_9284(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_9284,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9309,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:559: g1591 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_8446(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9307 in map-loop1585 in k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9309(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_9309,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_9284(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k9343 in k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9345(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_9345,c,av);}
a=C_alloc(5);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[179],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop1616 in k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_9347(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_9347,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9372,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:561: g1622 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_8446(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9370 in map-loop1616 in k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9372(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_9372,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_9347(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* a9383 in k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9384(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9384,c,av);}
/* support.scm:563: get-line-2 */
t2=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* a9389 in k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9390(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_9390,c,av);}
a=C_alloc(11);
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9451,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=t3,a[6]=t2,tmp=(C_word)a,a+=7,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9479,a[2]=((C_word*)t0)[4],a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* tweaks.scm:60: ##sys#get */
t6=*((C_word*)lf[182]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
av2[3]=lf[183];
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k9411 in k9453 in k9449 in a9389 in k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9413(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_9413,c,av);}
a=C_alloc(5);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[179],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop1662 in k9453 in k9449 in a9389 in k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_9415(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_9415,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9440,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:575: g1668 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_8446(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9438 in map-loop1662 in k9453 in k9449 in a9389 in k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 in ... */
static void C_ccall f_9440(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_9440,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_9415(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k9449 in a9389 in k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_9451(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,2)))){
C_save_and_reclaim_args((void *)trf_9451,2,t0,t1);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9455,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[5])){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9458,a[2]=t2,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
/* support.scm:571: real-name */
t4=*((C_word*)lf[181]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
/* support.scm:574: ##sys#symbol->string */
t3=*((C_word*)lf[180]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k9453 in k9449 in a9389 in k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9455(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,3)))){
C_save_and_reclaim((void *)f_9455,c,av);}
a=C_alloc(23);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_i_check_list_2(((C_word*)t0)[3],lf[125]);
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9413,a[2]=((C_word*)t0)[4],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9415,a[2]=t5,a[3]=t10,a[4]=((C_word*)t0)[5],a[5]=t6,tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_9415(t12,t8,((C_word*)t0)[3]);}

/* k9456 in k9449 in a9389 in k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9458(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_9458,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9465,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(t1)){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list2(&a,2,((C_word*)t0)[3],t1);
f_9455(2,av2);}}
else{
/* support.scm:573: ##sys#symbol->string */
t3=*((C_word*)lf[180]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k9463 in k9456 in k9449 in a9389 in k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9465(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_9465,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list2(&a,2,((C_word*)t0)[3],t1);
f_9455(2,av2);}}

/* k9477 in a9389 in k9251 in k9006 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9479(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,1)))){
C_save_and_reclaim((void *)f_9479,c,av);}
a=C_alloc(29);
if(C_truep(t1)){
t2=((C_word*)((C_word*)t0)[2])[1];
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,C_s_a_i_plus(&a,2,t2,C_fix(1)));
t4=((C_word*)t0)[3];
f_9451(t4,C_SCHEME_TRUE);}
else{
t2=((C_word*)t0)[3];
f_9451(t2,C_SCHEME_FALSE);}}

/* k9583 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9585(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_9585,c,av);}
a=C_alloc(5);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_record4(&a,4,lf[141],lf[179],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop1693 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_9587(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_9587,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9612,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:576: g1699 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_8446(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9610 in map-loop1693 in walk in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9612(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_9612,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_9587(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k9628 in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9630(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_9630,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9633,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=((C_word*)((C_word*)t0)[3])[1];
if(C_truep(C_i_positivep(t3))){
/* support.scm:579: debugging */
t4=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[190];
av2[3]=lf[191];
av2[4]=((C_word*)((C_word*)t0)[3])[1];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k9631 in k9628 in chicken.compiler.support#build-node-graph in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9633(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_9633,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9639(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_9639,c,av);}
a=C_alloc(5);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9645,a[2]=t4,tmp=(C_word)a,a+=3,tmp));
t6=((C_word*)t4)[1];
f_9645(t6,t1,t2);}

/* walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_9645(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_9645,3,t0,t1,t2);}
a=C_alloc(7);
t3=C_slot(t2,C_fix(3));
t4=C_slot(t2,C_fix(2));
t5=C_slot(t2,C_fix(1));
t6=C_eqp(t5,lf[158]);
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9679,a[2]=t3,a[3]=t1,a[4]=t5,a[5]=((C_word*)t0)[2],a[6]=t4,tmp=(C_word)a,a+=7,tmp);
if(C_truep(t6)){
t8=t7;
f_9679(t8,t6);}
else{
t8=C_eqp(t5,lf[203]);
t9=t7;
f_9679(t9,(C_truep(t8)?t8:C_eqp(t5,lf[204])));}}

/* k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_9679(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,0,5)))){
C_save_and_reclaim_args((void *)trf_9679,2,t0,t1);}
a=C_alloc(20);
if(C_truep(t1)){
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(((C_word*)t0)[2],lf[125]);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9692,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9694,a[2]=t4,a[3]=t9,a[4]=((C_word*)t0)[5],a[5]=t5,tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_9694(t11,t7,((C_word*)t0)[2]);}
else{
t2=C_eqp(((C_word*)t0)[4],lf[193]);
if(C_truep(t2)){
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_i_check_list_2(((C_word*)t0)[2],lf[125]);
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9749,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9751,a[2]=t5,a[3]=t10,a[4]=((C_word*)t0)[5],a[5]=t6,tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_9751(t12,t8,((C_word*)t0)[2]);}
else{
t3=C_eqp(((C_word*)t0)[4],lf[154]);
if(C_truep(t3)){
t4=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t4;
av2[1]=C_i_car(((C_word*)t0)[6]);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_eqp(((C_word*)t0)[4],lf[85]);
if(C_truep(t4)){
t5=C_i_car(((C_word*)t0)[6]);
t6=C_booleanp(t5);
if(C_truep(t6)){
if(C_truep(t6)){
t7=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t7;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
t7=C_u_i_car(((C_word*)t0)[6]);
t8=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t8;
av2[1]=C_a_i_list(&a,2,lf[85],t7);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}
else{
t7=C_i_stringp(t5);
if(C_truep(t7)){
if(C_truep(t7)){
t8=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t8;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t8=C_u_i_car(((C_word*)t0)[6]);
t9=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t9;
av2[1]=C_a_i_list(&a,2,lf[85],t8);
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}}
else{
t8=C_i_numberp(t5);
t9=(C_truep(t8)?t8:C_charp(t5));
if(C_truep(t9)){
t10=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t10;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
t10=C_u_i_car(((C_word*)t0)[6]);
t11=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t11;
av2[1]=C_a_i_list(&a,2,lf[85],t10);
((C_proc)(void*)(*((C_word*)t11+1)))(2,av2);}}}}}
else{
t5=C_eqp(((C_word*)t0)[4],lf[96]);
if(C_truep(t5)){
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t11=t10;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=((C_word*)t12)[1];
t14=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_9849,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],a[6]=t8,a[7]=t9,a[8]=t12,a[9]=t13,tmp=(C_word)a,a+=10,tmp);
/* support.scm:598: chicken.base#butlast */
t15=*((C_word*)lf[194]+1);{
C_word av2[3];
av2[0]=t15;
av2[1]=t14;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t15+1)))(3,av2);}}
else{
t6=C_eqp(((C_word*)t0)[4],lf[118]);
if(C_truep(t6)){
t7=C_i_cadr(((C_word*)t0)[6]);
t8=(C_truep(t7)?lf[165]:lf[118]);
t9=C_i_caddr(((C_word*)t0)[6]);
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9972,a[2]=((C_word*)t0)[3],a[3]=t8,a[4]=t9,tmp=(C_word)a,a+=5,tmp);
/* support.scm:605: walk */
t11=((C_word*)((C_word*)t0)[5])[1];
f_9645(t11,t10,C_i_car(((C_word*)t0)[2]));}
else{
t7=C_eqp(((C_word*)t0)[4],lf[166]);
if(C_truep(t7)){
t8=C_i_car(((C_word*)t0)[6]);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9996,a[2]=((C_word*)t0)[3],a[3]=t8,tmp=(C_word)a,a+=4,tmp);
/* support.scm:607: walk */
t10=((C_word*)((C_word*)t0)[5])[1];
f_9645(t10,t9,C_i_car(((C_word*)t0)[2]));}
else{
t8=C_eqp(((C_word*)t0)[4],lf[196]);
if(C_truep(t8)){
/* support.scm:609: walk */
t9=((C_word*)((C_word*)t0)[5])[1];
f_9645(t9,((C_word*)t0)[3],C_i_car(((C_word*)t0)[2]));}
else{
t9=C_eqp(((C_word*)t0)[4],lf[167]);
if(C_truep(t9)){
t10=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10030,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* support.scm:612: walk */
t11=((C_word*)((C_word*)t0)[5])[1];
f_9645(t11,t10,C_i_car(((C_word*)t0)[2]));}
else{
t10=C_eqp(((C_word*)t0)[4],lf[179]);
if(C_truep(t10)){
t11=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t12=t11;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=((C_word*)t13)[1];
t15=C_i_check_list_2(((C_word*)t0)[2],lf[125]);
t16=C_SCHEME_UNDEFINED;
t17=(*a=C_VECTOR_TYPE|1,a[1]=t16,tmp=(C_word)a,a+=2,tmp);
t18=C_set_block_item(t17,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10120,a[2]=t13,a[3]=t17,a[4]=((C_word*)t0)[5],a[5]=t14,tmp=(C_word)a,a+=6,tmp));
t19=((C_word*)t17)[1];
f_10120(t19,((C_word*)t0)[3],((C_word*)t0)[2]);}
else{
t11=C_eqp(((C_word*)t0)[4],lf[189]);
if(C_truep(t11)){
t12=C_i_car(((C_word*)t0)[6]);
t13=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t14=t13;
t15=(*a=C_VECTOR_TYPE|1,a[1]=t14,tmp=(C_word)a,a+=2,tmp);
t16=((C_word*)t15)[1];
t17=C_i_check_list_2(((C_word*)t0)[2],lf[125]);
t18=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10175,a[2]=((C_word*)t0)[3],a[3]=t12,tmp=(C_word)a,a+=4,tmp);
t19=C_SCHEME_UNDEFINED;
t20=(*a=C_VECTOR_TYPE|1,a[1]=t19,tmp=(C_word)a,a+=2,tmp);
t21=C_set_block_item(t20,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10177,a[2]=t15,a[3]=t20,a[4]=((C_word*)t0)[5],a[5]=t16,tmp=(C_word)a,a+=6,tmp));
t22=((C_word*)t20)[1];
f_10177(t22,t18,((C_word*)t0)[2]);}
else{
t12=C_eqp(((C_word*)t0)[4],lf[159]);
if(C_truep(t12)){
t13=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t13;
av2[1]=C_a_i_list1(&a,1,((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t13+1)))(2,av2);}}
else{
t13=C_eqp(((C_word*)t0)[4],lf[198]);
if(C_truep(t13)){
t14=C_i_car(((C_word*)t0)[6]);
t15=C_SCHEME_UNDEFINED;
t16=(*a=C_VECTOR_TYPE|1,a[1]=t15,tmp=(C_word)a,a+=2,tmp);
t17=C_set_block_item(t16,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10233,a[2]=((C_word*)t0)[5],a[3]=t16,tmp=(C_word)a,a+=4,tmp));
t18=((C_word*)t16)[1];
f_10233(t18,((C_word*)t0)[3],t14,((C_word*)t0)[2],C_SCHEME_END_OF_LIST);}
else{
t14=C_eqp(((C_word*)t0)[4],lf[199]);
t15=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10283,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
if(C_truep(t14)){
t16=t15;
f_10283(t16,t14);}
else{
t16=C_eqp(((C_word*)t0)[4],lf[200]);
if(C_truep(t16)){
t17=t15;
f_10283(t17,t16);}
else{
t17=C_eqp(((C_word*)t0)[4],lf[201]);
t18=t15;
f_10283(t18,(C_truep(t17)?t17:C_eqp(((C_word*)t0)[4],lf[202])));}}}}}}}}}}}}}}}}

/* k9690 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9692(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_9692,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop1750 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_9694(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_9694,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9719,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:588: g1756 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_9645(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9717 in map-loop1750 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9719(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_9719,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_9694(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k9747 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9749(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_9749,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[193],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* map-loop1779 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_9751(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_9751,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9776,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:590: g1785 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_9645(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9774 in map-loop1779 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9776(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_9776,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_9751(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k9847 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9849(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_9849,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_9852,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9919,a[2]=((C_word*)t0)[8],a[3]=t4,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[9],tmp=(C_word)a,a+=6,tmp));
t6=((C_word*)t4)[1];
f_9919(t6,t2,t1);}

/* k9850 in k9847 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9852(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_9852,c,av);}
a=C_alloc(12);
t2=C_i_check_list_2(((C_word*)t0)[2],lf[125]);
t3=C_i_check_list_2(t1,lf[125]);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9861,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9871,a[2]=((C_word*)t0)[6],a[3]=t6,a[4]=((C_word*)t0)[7],tmp=(C_word)a,a+=5,tmp));
t8=((C_word*)t6)[1];
f_9871(t8,t4,((C_word*)t0)[2],t1);}

/* k9859 in k9850 in k9847 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9861(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_9861,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9865,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9869,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* support.scm:599: last */
f_6083(t3,((C_word*)t0)[4]);}

/* k9863 in k9859 in k9850 in k9847 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9865(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_9865,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[96],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k9867 in k9859 in k9850 in k9847 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9869(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9869,c,av);}
/* support.scm:599: walk */
t2=((C_word*)((C_word*)t0)[2])[1];
f_9645(t2,((C_word*)t0)[3],t1);}

/* map-loop1821 in k9850 in k9847 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_9871(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_9871,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list2(&a,2,t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop1842 in k9847 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_fcall f_9919(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_9919,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9944,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* support.scm:598: g1848 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_9645(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9942 in map-loop1842 in k9847 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9944(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_9944,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_9919(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k9970 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9972(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_9972,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list3(&a,3,((C_word*)t0)[3],((C_word*)t0)[4],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k9994 in k9677 in walk in chicken.compiler.support#build-expression-tree in k8403 in k7750 in k7747 in k6622 in k5310 in k5307 in k5304 in k5301 in k5298 in k5295 in k5292 in k5289 in k5286 */
static void C_ccall f_9996(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_9996,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[195],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_support_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("support"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_support_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(3950))){
C_save(t1);
C_rereclaim2(3950*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,539);
lf[0]=C_h_intern(&lf[0],7, C_text("support"));
lf[1]=C_h_intern(&lf[1],25, C_text("chicken.compiler.support#"));
lf[8]=C_h_intern(&lf[8],36, C_text("chicken.compiler.support#number-type"));
lf[9]=C_h_intern(&lf[9],7, C_text("generic"));
lf[10]=C_h_intern(&lf[10],31, C_text("chicken.compiler.support#unsafe"));
lf[11]=C_h_intern(&lf[11],46, C_text("chicken.compiler.support#compiler-cleanup-hook"));
lf[12]=C_h_intern(&lf[12],42, C_text("chicken.compiler.support#debugging-chicken"));
lf[13]=C_h_intern(&lf[13],29, C_text("chicken.compiler.support#bomb"));
lf[14]=C_h_intern(&lf[14],18, C_text("chicken.base#error"));
lf[15]=C_h_intern(&lf[15],20, C_text("scheme#string-append"));
lf[16]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032[internal compiler error] "));
lf[17]=C_decode_literal(C_heaptop,C_text("\376B\000\000\031[internal compiler error]"));
lf[18]=C_h_intern(&lf[18],51, C_text("chicken.compiler.support#collected-debugging-output"));
lf[20]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001o\376\003\000\000\002\376\001\000\000\001\001x\376\003\000\000\002\376\001\000\000\001\001S\376\377\016"));
lf[22]=C_h_intern(&lf[22],34, C_text("chicken.compiler.support#debugging"));
lf[23]=C_h_intern(&lf[23],14, C_text("scheme#newline"));
lf[24]=C_h_intern(&lf[24],21, C_text("##sys#standard-output"));
lf[25]=C_h_intern(&lf[25],6, C_text("printf"));
lf[26]=C_h_intern(&lf[26],18, C_text("##sys#write-char-0"));
lf[27]=C_h_intern(&lf[27],11, C_text("##sys#print"));
lf[28]=C_h_intern(&lf[28],12, C_text("scheme#force"));
lf[29]=C_h_intern(&lf[29],14, C_text("scheme#display"));
lf[30]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002: "));
lf[31]=C_h_intern(&lf[31],34, C_text("chicken.port#with-output-to-string"));
lf[32]=C_h_intern(&lf[32],7, C_text("fprintf"));
lf[33]=C_h_intern(&lf[33],25, C_text("chicken.base#flush-output"));
lf[34]=C_h_intern(&lf[34],46, C_text("chicken.compiler.support#with-debugging-output"));
lf[35]=C_h_intern(&lf[35],27, C_text("chicken.string#string-split"));
lf[36]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\012"));
lf[37]=C_h_intern(&lf[37],39, C_text("chicken.compiler.support#quit-compiling"));
lf[38]=C_h_intern(&lf[38],20, C_text("##sys#standard-error"));
lf[39]=C_h_intern(&lf[39],17, C_text("chicken.base#exit"));
lf[40]=C_h_intern(&lf[40],22, C_text("chicken.format#fprintf"));
lf[41]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010\012Error: "));
lf[42]=C_h_intern(&lf[42],23, C_text("##sys#syntax-error-hook"));
lf[43]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005\011~s~%"));
lf[44]=C_h_intern(&lf[44],8, C_text("for-each"));
lf[45]=C_h_intern(&lf[45],29, C_text("chicken.base#print-call-chain"));
lf[46]=C_h_intern(&lf[46],20, C_text("##sys#current-thread"));
lf[47]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025\012\011Expansion history:\012"));
lf[48]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003): "));
lf[49]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017\012Syntax error ("));
lf[50]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017\012Syntax error: "));
lf[51]=C_h_intern(&lf[51],27, C_text("chicken.syntax#syntax-error"));
lf[52]=C_h_intern(&lf[52],47, C_text("chicken.compiler.support#emit-syntax-trace-info"));
lf[53]=C_h_intern(&lf[53],40, C_text("chicken.compiler.support#check-signature"));
lf[54]=C_h_intern(&lf[54],42, C_text("chicken.compiler.support#build-lambda-list"));
lf[55]=C_h_intern(&lf[55],37, C_text("chicken.compiler.support#c-ify-string"));
lf[56]=C_h_intern(&lf[56],18, C_text("##sys#list->string"));
lf[57]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\377\012\000\000\042\376\377\016"));
lf[58]=C_h_intern(&lf[58],13, C_text("scheme#append"));
lf[59]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\377\012\000\000\134\376\377\016"));
lf[60]=C_h_intern(&lf[60],18, C_text("##sys#string->list"));
lf[61]=C_h_intern(&lf[61],20, C_text("##sys#fixnum->string"));
lf[62]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\377\012\000\0000\376\003\000\000\002\376\377\012\000\0000\376\377\016"));
lf[63]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\377\012\000\0000\376\377\016"));
lf[64]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\377\012\000\000\042\376\003\000\000\002\376\377\012\000\000\047\376\003\000\000\002\376\377\012\000\000\134\376\003\000\000\002\376\377\012\000\000\077\376\003\000\000\002\376\377\012\000\000\052\376\377\016"));
lf[65]=C_h_intern(&lf[65],44, C_text("chicken.compiler.support#valid-c-identifier\077"));
lf[66]=C_h_intern(&lf[66],23, C_text("chicken.string#->string"));
lf[67]=C_h_intern(&lf[67],37, C_text("chicken.compiler.support#bytes->words"));
lf[68]=C_h_intern(&lf[68],37, C_text("chicken.compiler.support#words->bytes"));
lf[69]=C_h_intern(&lf[69],50, C_text("chicken.compiler.support#check-and-open-input-file"));
lf[70]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001-"));
lf[71]=C_h_intern(&lf[71],20, C_text("##sys#standard-input"));
lf[72]=C_h_intern(&lf[72],22, C_text("scheme#open-input-file"));
lf[73]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024Can not open file ~s"));
lf[74]=C_decode_literal(C_heaptop,C_text("\376B\000\000\031(~a) can not open file ~s"));
lf[75]=C_h_intern(&lf[75],25, C_text("chicken.file#file-exists\077"));
lf[76]=C_h_intern(&lf[76],49, C_text("chicken.compiler.support#close-checked-input-file"));
lf[77]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001-"));
lf[78]=C_h_intern(&lf[78],23, C_text("scheme#close-input-port"));
lf[79]=C_h_intern(&lf[79],35, C_text("chicken.compiler.support#fold-inner"));
lf[80]=C_h_intern(&lf[80],14, C_text("scheme#reverse"));
lf[82]=C_h_intern(&lf[82],41, C_text("chicken.compiler.support#read-expressions"));
lf[83]=C_h_intern(&lf[83],11, C_text("scheme#read"));
lf[84]=C_h_intern(&lf[84],34, C_text("chicken.compiler.support#constant\077"));
lf[85]=C_h_intern(&lf[85],5, C_text("quote"));
lf[86]=C_h_intern(&lf[86],20, C_text("##sys#srfi-4-vector\077"));
lf[87]=C_h_intern(&lf[87],18, C_text("chicken.blob#blob\077"));
lf[88]=C_h_intern(&lf[88],45, C_text("chicken.compiler.support#collapsable-literal\077"));
lf[89]=C_h_intern(&lf[89],35, C_text("chicken.compiler.support#immediate\077"));
lf[90]=C_h_intern(&lf[90],36, C_text("chicken.compiler.support#big-fixnum\077"));
lf[91]=C_h_intern(&lf[91],39, C_text("chicken.compiler.support#basic-literal\077"));
lf[92]=C_h_intern(&lf[92],19, C_text("scheme#vector->list"));
lf[93]=C_h_intern(&lf[93],48, C_text("chicken.compiler.support#canonicalize-begin-body"));
lf[94]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[95]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[96]=C_h_intern(&lf[96],3, C_text("let"));
lf[97]=C_h_intern(&lf[97],19, C_text("chicken.base#gensym"));
lf[98]=C_h_intern(&lf[98],1, C_text("t"));
lf[99]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\012\001##sys#void\376\377\016"));
lf[100]=C_h_intern(&lf[100],37, C_text("chicken.compiler.support#string->expr"));
lf[101]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042cannot parse expression: ~s [~a]~%"));
lf[102]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[103]=C_h_intern(&lf[103],5, C_text("begin"));
lf[104]=C_h_intern(&lf[104],13, C_text("scheme#values"));
lf[105]=C_h_intern(&lf[105],35, C_text("chicken.port#with-input-from-string"));
lf[106]=C_h_intern(&lf[106],40, C_text("chicken.condition#with-exception-handler"));
lf[107]=C_h_intern(&lf[107],37, C_text("scheme#call-with-current-continuation"));
lf[108]=C_h_intern(&lf[108],37, C_text("chicken.compiler.support#llist-length"));
lf[109]=C_h_intern(&lf[109],37, C_text("chicken.compiler.support#llist-match\077"));
lf[111]=C_h_intern(&lf[111],56, C_text("chicken.compiler.support#reset-profile-info-vector-name!"));
lf[112]=C_h_intern(&lf[112],41, C_text("chicken.compiler.support#make-random-name"));
lf[113]=C_h_intern(&lf[113],12, C_text("profile-info"));
lf[116]=C_h_intern(&lf[116],46, C_text("chicken.compiler.support#expand-profile-lambda"));
lf[117]=C_h_intern(&lf[117],19, C_text("##sys#profile-entry"));
lf[118]=C_h_intern(&lf[118],13, C_text("##core#lambda"));
lf[119]=C_h_intern(&lf[119],11, C_text("##sys#apply"));
lf[120]=C_h_intern(&lf[120],18, C_text("##sys#profile-exit"));
lf[121]=C_h_intern(&lf[121],18, C_text("##sys#dynamic-wind"));
lf[122]=C_h_intern(&lf[122],47, C_text("chicken.compiler.support#profiling-prelude-exps"));
lf[123]=C_h_intern(&lf[123],27, C_text("##sys#register-profile-info"));
lf[124]=C_h_intern(&lf[124],4, C_text("set!"));
lf[125]=C_h_intern(&lf[125],3, C_text("map"));
lf[126]=C_h_intern(&lf[126],30, C_text("##sys#set-profile-info-vector!"));
lf[127]=C_h_intern(&lf[127],31, C_text("chicken.compiler.support#db-get"));
lf[128]=C_h_intern(&lf[128],31, C_text("chicken.internal#hash-table-ref"));
lf[129]=C_h_intern(&lf[129],35, C_text("chicken.compiler.support#db-get-all"));
lf[130]=C_h_intern(&lf[130],5, C_text("foldr"));
lf[131]=C_h_intern(&lf[131],32, C_text("chicken.compiler.support#db-put!"));
lf[132]=C_h_intern(&lf[132],32, C_text("chicken.internal#hash-table-set!"));
lf[133]=C_h_intern(&lf[133],33, C_text("chicken.compiler.support#collect!"));
lf[134]=C_h_intern(&lf[134],36, C_text("chicken.compiler.support#db-get-list"));
lf[135]=C_h_intern(&lf[135],33, C_text("chicken.compiler.support#get-line"));
lf[136]=C_h_intern(&lf[136],26, C_text("##sys#line-number-database"));
lf[137]=C_h_intern(&lf[137],35, C_text("chicken.compiler.support#get-line-2"));
lf[138]=C_h_intern(&lf[138],53, C_text("chicken.compiler.support#display-line-number-database"));
lf[139]=C_h_intern(&lf[139],36, C_text("chicken.internal#hash-table-for-each"));
lf[140]=C_h_intern(&lf[140],34, C_text("chicken.compiler.support#make-node"));
lf[141]=C_h_intern(&lf[141],29, C_text("chicken.compiler.support#node"));
lf[142]=C_h_intern(&lf[142],30, C_text("chicken.compiler.support#node\077"));
lf[143]=C_h_intern(&lf[143],35, C_text("chicken.compiler.support#node-class"));
lf[144]=C_h_intern(&lf[144],10, C_text("node-class"));
lf[145]=C_h_intern(&lf[145],40, C_text("chicken.compiler.support#node-class-set!"));
lf[146]=C_h_intern(&lf[146],16, C_text("##sys#block-set!"));
lf[147]=C_h_intern(&lf[147],40, C_text("chicken.compiler.support#node-parameters"));
lf[148]=C_h_intern(&lf[148],15, C_text("node-parameters"));
lf[149]=C_h_intern(&lf[149],45, C_text("chicken.compiler.support#node-parameters-set!"));
lf[150]=C_h_intern(&lf[150],44, C_text("chicken.compiler.support#node-subexpressions"));
lf[151]=C_h_intern(&lf[151],19, C_text("node-subexpressions"));
lf[152]=C_h_intern(&lf[152],49, C_text("chicken.compiler.support#node-subexpressions-set!"));
lf[153]=C_h_intern(&lf[153],32, C_text("chicken.compiler.support#varnode"));
lf[154]=C_h_intern(&lf[154],15, C_text("##core#variable"));
lf[155]=C_h_intern(&lf[155],30, C_text("chicken.compiler.support#qnode"));
lf[156]=C_h_intern(&lf[156],41, C_text("chicken.compiler.support#build-node-graph"));
lf[157]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016bad expression"));
lf[158]=C_h_intern(&lf[158],2, C_text("if"));
lf[159]=C_h_intern(&lf[159],16, C_text("##core#undefined"));
lf[160]=C_h_intern(&lf[160],21, C_text("scheme#inexact->exact"));
lf[161]=C_h_intern(&lf[161],15, C_text("scheme#truncate"));
lf[162]=C_h_intern(&lf[162],20, C_text("chicken.base#warning"));
lf[163]=C_decode_literal(C_heaptop,C_text("\376B\000\0006literal is out of range - will be truncated to integer"));
lf[164]=C_h_intern(&lf[164],6, C_text("fixnum"));
lf[165]=C_h_intern(&lf[165],6, C_text("lambda"));
lf[166]=C_h_intern(&lf[166],10, C_text("##core#the"));
lf[167]=C_h_intern(&lf[167],15, C_text("##core#typecase"));
lf[168]=C_h_intern(&lf[168],4, C_text("else"));
lf[169]=C_h_intern(&lf[169],12, C_text("scheme#cadar"));
lf[170]=C_h_intern(&lf[170],1, C_text("\052"));
lf[171]=C_h_intern(&lf[171],16, C_text("##core#primitive"));
lf[172]=C_h_intern(&lf[172],13, C_text("##core#inline"));
lf[173]=C_h_intern(&lf[173],18, C_text("##core#debug-event"));
lf[174]=C_h_intern(&lf[174],11, C_text("##core#proc"));
lf[175]=C_h_intern(&lf[175],11, C_text("##core#set!"));
lf[176]=C_h_intern(&lf[176],31, C_text("##core#foreign-callback-wrapper"));
lf[177]=C_h_intern(&lf[177],22, C_text("##core#inline_allocate"));
lf[178]=C_h_intern(&lf[178],10, C_text("##core#app"));
lf[179]=C_h_intern(&lf[179],11, C_text("##core#call"));
lf[180]=C_h_intern(&lf[180],20, C_text("##sys#symbol->string"));
lf[181]=C_h_intern(&lf[181],34, C_text("chicken.compiler.support#real-name"));
lf[182]=C_h_intern(&lf[182],9, C_text("##sys#get"));
lf[183]=C_h_intern(&lf[183],36, C_text("##compiler#always-bound-to-procedure"));
lf[184]=C_h_intern(&lf[184],17, C_text("##core#inline_ref"));
lf[185]=C_h_intern(&lf[185],20, C_text("##core#inline_update"));
lf[186]=C_h_intern(&lf[186],21, C_text("##core#inline_loc_ref"));
lf[187]=C_h_intern(&lf[187],24, C_text("##core#inline_loc_update"));
lf[188]=C_h_intern(&lf[188],14, C_text("##core#provide"));
lf[189]=C_h_intern(&lf[189],15, C_text("##core#callunit"));
lf[190]=C_h_intern(&lf[190],1, C_text("o"));
lf[191]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033eliminated procedure checks"));
lf[192]=C_h_intern(&lf[192],46, C_text("chicken.compiler.support#build-expression-tree"));
lf[193]=C_h_intern(&lf[193],14, C_text("##core#closure"));
lf[194]=C_h_intern(&lf[194],20, C_text("chicken.base#butlast"));
lf[195]=C_h_intern(&lf[195],3, C_text("the"));
lf[196]=C_h_intern(&lf[196],17, C_text("##core#the/result"));
lf[197]=C_h_intern(&lf[197],17, C_text("compiler-typecase"));
lf[198]=C_h_intern(&lf[198],11, C_text("##core#bind"));
lf[199]=C_h_intern(&lf[199],12, C_text("##core#unbox"));
lf[200]=C_h_intern(&lf[200],10, C_text("##core#ref"));
lf[201]=C_h_intern(&lf[201],13, C_text("##core#update"));
lf[202]=C_h_intern(&lf[202],15, C_text("##core#update_i"));
lf[203]=C_h_intern(&lf[203],10, C_text("##core#box"));
lf[204]=C_h_intern(&lf[204],11, C_text("##core#cond"));
lf[205]=C_h_intern(&lf[205],37, C_text("chicken.compiler.support#fold-boolean"));
lf[206]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\005C_and\376\377\016"));
lf[207]=C_h_intern(&lf[207],47, C_text("chicken.compiler.support#inline-lambda-bindings"));
lf[208]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_a_i_list"));
lf[209]=C_h_intern(&lf[209],10, C_text("references"));
lf[210]=C_h_intern(&lf[210],10, C_text("rest-null\077"));
lf[211]=C_h_intern(&lf[211],8, C_text("rest-cdr"));
lf[212]=C_h_intern(&lf[212],17, C_text("derived-rest-vars"));
lf[213]=C_h_intern(&lf[213],17, C_text("##core#rest-null\077"));
lf[214]=C_h_intern(&lf[214],35, C_text("chicken.compiler.support#copy-node!"));
lf[215]=C_h_intern(&lf[215],15, C_text("##core#rest-car"));
lf[216]=C_decode_literal(C_heaptop,C_text("\376B\000\000$C_rest_arg_out_of_bounds_error_value"));
lf[217]=C_h_intern(&lf[217],15, C_text("##core#rest-cdr"));
lf[218]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007C_i_cdr"));
lf[219]=C_h_intern(&lf[219],4, C_text("rarg"));
lf[220]=C_h_intern(&lf[220],22, C_text("chicken.base#alist-ref"));
lf[221]=C_h_intern(&lf[221],10, C_text("scheme#eq\077"));
lf[222]=C_h_intern(&lf[222],12, C_text("contractable"));
lf[223]=C_h_intern(&lf[223],16, C_text("inline-transient"));
lf[224]=C_h_intern(&lf[224],1, C_text("f"));
lf[225]=C_h_intern(&lf[225],27, C_text("##sys#decompose-lambda-list"));
lf[226]=C_h_intern(&lf[226],34, C_text("chicken.compiler.support#tree-copy"));
lf[227]=C_h_intern(&lf[227],34, C_text("chicken.compiler.support#copy-node"));
lf[228]=C_h_intern(&lf[228],48, C_text("chicken.compiler.support#emit-global-inline-file"));
lf[229]=C_h_intern(&lf[229],18, C_text("chicken.base#print"));
lf[230]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002  "));
lf[231]=C_h_intern(&lf[231],15, C_text("scheme#string<\077"));
lf[232]=C_h_intern(&lf[232],21, C_text("scheme#symbol->string"));
lf[233]=C_h_intern(&lf[233],17, C_text("chicken.sort#sort"));
lf[234]=C_h_intern(&lf[234],1, C_text("i"));
lf[235]=C_decode_literal(C_heaptop,C_text("\376B\000\0001the following procedures can be globally inlined:"));
lf[236]=C_h_intern(&lf[236],25, C_text("chicken.file#delete-file\052"));
lf[237]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015; END OF FILE"));
lf[238]=C_h_intern(&lf[238],23, C_text("chicken.pretty-print#pp"));
lf[239]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027; GENERATED BY CHICKEN "));
lf[240]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006 FROM "));
lf[241]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\012"));
lf[242]=C_h_intern(&lf[242],32, C_text("chicken.platform#chicken-version"));
lf[243]=C_h_intern(&lf[243],26, C_text("scheme#with-output-to-file"));
lf[244]=C_h_intern(&lf[244],11, C_text("local-value"));
lf[245]=C_h_intern(&lf[245],5, C_text("value"));
lf[246]=C_h_intern(&lf[246],9, C_text("inlinable"));
lf[247]=C_h_intern(&lf[247],3, C_text("yes"));
lf[248]=C_h_intern(&lf[248],2, C_text("no"));
lf[249]=C_h_intern(&lf[249],17, C_text("##compiler#inline"));
lf[250]=C_h_intern(&lf[250],11, C_text("hidden-refs"));
lf[251]=C_h_intern(&lf[251],7, C_text("unknown"));
lf[252]=C_h_intern(&lf[252],24, C_text("##compiler#inline-global"));
lf[253]=C_h_intern(&lf[253],42, C_text("chicken.compiler.support#variable-visible\077"));
lf[254]=C_h_intern(&lf[254],41, C_text("chicken.compiler.support#load-inline-file"));
lf[255]=C_h_intern(&lf[255],10, C_text("##sys#put!"));
lf[256]=C_h_intern(&lf[256],27, C_text("scheme#with-input-from-file"));
lf[257]=C_h_intern(&lf[257],35, C_text("chicken.compiler.support#match-node"));
lf[258]=C_h_intern(&lf[258],1, C_text("a"));
lf[259]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007matched"));
lf[260]=C_h_intern(&lf[260],53, C_text("chicken.compiler.support#expression-has-side-effects\077"));
lf[261]=C_h_intern(&lf[261],49, C_text("chicken.compiler.support#foreign-callback-stub-id"));
lf[262]=C_h_intern(&lf[262],47, C_text("chicken.compiler.support#foreign-callback-stubs"));
lf[263]=C_h_intern(&lf[263],44, C_text("chicken.compiler.support#simple-lambda-node\077"));
lf[264]=C_h_intern(&lf[264],47, C_text("chicken.compiler.support#dump-undefined-globals"));
lf[265]=C_h_intern(&lf[265],12, C_text("scheme#write"));
lf[266]=C_h_intern(&lf[266],6, C_text("global"));
lf[267]=C_h_intern(&lf[267],8, C_text("assigned"));
lf[268]=C_h_intern(&lf[268],24, C_text("chicken.keyword#keyword\077"));
lf[269]=C_h_intern(&lf[269],45, C_text("chicken.compiler.support#dump-defined-globals"));
lf[270]=C_h_intern(&lf[270],41, C_text("chicken.compiler.support#dump-global-refs"));
lf[271]=C_h_intern(&lf[271],30, C_text("##sys#toplevel-definition-hook"));
lf[272]=C_h_intern(&lf[272],22, C_text("chicken.plist#remprop!"));
lf[273]=C_h_intern(&lf[273],21, C_text("##compiler#visibility"));
lf[274]=C_h_intern(&lf[274],41, C_text("chicken.compiler.support#variable-hidden\077"));
lf[275]=C_h_intern(&lf[275],38, C_text("chicken.compiler.support#hide-variable"));
lf[276]=C_decode_literal(C_heaptop,C_text("\376B\000\000 hiding unexported module binding"));
lf[277]=C_h_intern(&lf[277],51, C_text("chicken.compiler.support#make-foreign-callback-stub"));
lf[278]=C_h_intern(&lf[278],46, C_text("chicken.compiler.support#foreign-callback-stub"));
lf[279]=C_h_intern(&lf[279],47, C_text("chicken.compiler.support#foreign-callback-stub\077"));
lf[280]=C_h_intern(&lf[280],24, C_text("foreign-callback-stub-id"));
lf[281]=C_h_intern(&lf[281],51, C_text("chicken.compiler.support#foreign-callback-stub-name"));
lf[282]=C_h_intern(&lf[282],26, C_text("foreign-callback-stub-name"));
lf[283]=C_h_intern(&lf[283],57, C_text("chicken.compiler.support#foreign-callback-stub-qualifiers"));
lf[284]=C_h_intern(&lf[284],32, C_text("foreign-callback-stub-qualifiers"));
lf[285]=C_h_intern(&lf[285],58, C_text("chicken.compiler.support#foreign-callback-stub-return-type"));
lf[286]=C_h_intern(&lf[286],33, C_text("foreign-callback-stub-return-type"));
lf[287]=C_h_intern(&lf[287],61, C_text("chicken.compiler.support#foreign-callback-stub-argument-types"));
lf[288]=C_h_intern(&lf[288],36, C_text("foreign-callback-stub-argument-types"));
lf[289]=C_h_intern(&lf[289],56, C_text("chicken.compiler.support#register-foreign-callback-stub!"));
lf[290]=C_h_intern(&lf[290],26, C_text("##compiler#callback-lambda"));
lf[292]=C_h_intern(&lf[292],50, C_text("chicken.compiler.support#clear-foreign-type-table!"));
lf[293]=C_h_intern(&lf[293],19, C_text("scheme#vector-fill!"));
lf[294]=C_h_intern(&lf[294],18, C_text("scheme#make-vector"));
lf[295]=C_h_intern(&lf[295],47, C_text("chicken.compiler.support#register-foreign-type!"));
lf[296]=C_h_intern(&lf[296],44, C_text("chicken.compiler.support#lookup-foreign-type"));
lf[297]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001integer\376B\000\000\003int\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001unsigned-integer\376B\000\000\014unsigned int"
"\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001integer32\376B\000\000\005C_s32\376\003\000\000\002\376\003\000\000\002\376\001\000\000\022\001unsigned-integer32\376B\000\000\005C_u32\376"
"\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001integer64\376B\000\000\005C_s64\376\003\000\000\002\376\003\000\000\002\376\001\000\000\022\001unsigned-integer64\376B\000\000\005C_u64\376\003"
"\000\000\002\376\003\000\000\002\376\001\000\000\005\001short\376B\000\000\005short\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001unsigned-short\376B\000\000\016unsigned short\376\003"
"\000\000\002\376\003\000\000\002\376\001\000\000\004\001long\376B\000\000\004long\376\003\000\000\002\376\003\000\000\002\376\001\000\000\015\001unsigned-long\376B\000\000\015unsigned long\376\003\000\000\002\376"
"\003\000\000\002\376\001\000\000\007\001ssize_t\376B\000\000\007ssize_t\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001size_t\376B\000\000\006size_t\376\377\016"));
lf[298]=C_h_intern(&lf[298],43, C_text("chicken.compiler.support#foreign-type-check"));
lf[299]=C_h_intern(&lf[299],4, C_text("char"));
lf[300]=C_h_intern(&lf[300],13, C_text("unsigned-char"));
lf[301]=C_h_intern(&lf[301],27, C_text("##sys#foreign-char-argument"));
lf[302]=C_h_intern(&lf[302],3, C_text("int"));
lf[303]=C_h_intern(&lf[303],29, C_text("##sys#foreign-fixnum-argument"));
lf[304]=C_h_intern(&lf[304],5, C_text("float"));
lf[305]=C_h_intern(&lf[305],29, C_text("##sys#foreign-flonum-argument"));
lf[306]=C_h_intern(&lf[306],4, C_text("blob"));
lf[307]=C_h_intern(&lf[307],14, C_text("scheme-pointer"));
lf[308]=C_h_intern(&lf[308],28, C_text("##sys#foreign-block-argument"));
lf[309]=C_h_intern(&lf[309],12, C_text("##core#quote"));
lf[310]=C_h_intern(&lf[310],9, C_text("##core#if"));
lf[311]=C_h_intern(&lf[311],10, C_text("##core#let"));
lf[312]=C_h_intern(&lf[312],22, C_text("nonnull-scheme-pointer"));
lf[313]=C_h_intern(&lf[313],12, C_text("nonnull-blob"));
lf[314]=C_h_intern(&lf[314],14, C_text("pointer-vector"));
lf[315]=C_h_intern(&lf[315],37, C_text("##sys#foreign-struct-wrapper-argument"));
lf[316]=C_h_intern(&lf[316],22, C_text("nonnull-pointer-vector"));
lf[317]=C_h_intern(&lf[317],8, C_text("u8vector"));
lf[318]=C_h_intern(&lf[318],16, C_text("nonnull-u8vector"));
lf[319]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001nonnull-u8vector\376\001\000\000\010\001u8vector\376\003\000\000\002\376\003\000\000\002\376\001\000\000\021\001nonnull-u16vector\376"
"\001\000\000\011\001u16vector\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001nonnull-s8vector\376\001\000\000\010\001s8vector\376\003\000\000\002\376\003\000\000\002\376\001\000\000\021\001nonn"
"ull-s16vector\376\001\000\000\011\001s16vector\376\003\000\000\002\376\003\000\000\002\376\001\000\000\021\001nonnull-u32vector\376\001\000\000\011\001u32vector\376\003\000\000"
"\002\376\003\000\000\002\376\001\000\000\021\001nonnull-s32vector\376\001\000\000\011\001s32vector\376\003\000\000\002\376\003\000\000\002\376\001\000\000\021\001nonnull-u64vector\376\001\000"
"\000\011\001u64vector\376\003\000\000\002\376\003\000\000\002\376\001\000\000\021\001nonnull-s64vector\376\001\000\000\011\001s64vector\376\003\000\000\002\376\003\000\000\002\376\001\000\000\021\001nonn"
"ull-f32vector\376\001\000\000\011\001f32vector\376\003\000\000\002\376\003\000\000\002\376\001\000\000\021\001nonnull-f64vector\376\001\000\000\011\001f64vector\376\377\016"));
lf[320]=C_h_intern(&lf[320],9, C_text("integer32"));
lf[321]=C_h_intern(&lf[321],6, C_text("format"));
lf[322]=C_h_intern(&lf[322],13, C_text("foreign-value"));
lf[323]=C_h_intern(&lf[323],37, C_text("##sys#foreign-ranged-integer-argument"));
lf[324]=C_h_intern(&lf[324],30, C_text("chicken.base#get-output-string"));
lf[325]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014) \052 CHAR_BIT"));
lf[326]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007sizeof("));
lf[327]=C_h_intern(&lf[327],31, C_text("chicken.base#open-output-string"));
lf[328]=C_h_intern(&lf[328],14, C_text("unsigned-short"));
lf[329]=C_h_intern(&lf[329],46, C_text("##sys#foreign-unsigned-ranged-integer-argument"));
lf[330]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014) \052 CHAR_BIT"));
lf[331]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007sizeof("));
lf[332]=C_h_intern(&lf[332],9, C_text("c-pointer"));
lf[333]=C_h_intern(&lf[333],30, C_text("##sys#foreign-pointer-argument"));
lf[334]=C_h_intern(&lf[334],17, C_text("nonnull-c-pointer"));
lf[335]=C_h_intern(&lf[335],8, C_text("c-string"));
lf[336]=C_h_intern(&lf[336],19, C_text("##sys#make-c-string"));
lf[337]=C_h_intern(&lf[337],29, C_text("##sys#foreign-string-argument"));
lf[338]=C_h_intern(&lf[338],16, C_text("nonnull-c-string"));
lf[339]=C_h_intern(&lf[339],6, C_text("symbol"));
lf[340]=C_h_intern(&lf[340],3, C_text("ref"));
lf[341]=C_h_intern(&lf[341],8, C_text("instance"));
lf[342]=C_h_intern(&lf[342],12, C_text("instance-ref"));
lf[343]=C_h_intern(&lf[343],4, C_text("this"));
lf[344]=C_h_intern(&lf[344],8, C_text("slot-ref"));
lf[345]=C_h_intern(&lf[345],16, C_text("nonnull-instance"));
lf[346]=C_h_intern(&lf[346],5, C_text("const"));
lf[347]=C_h_intern(&lf[347],4, C_text("enum"));
lf[348]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026sizeof(int) \052 CHAR_BIT"));
lf[349]=C_h_intern(&lf[349],15, C_text("nonnull-pointer"));
lf[350]=C_h_intern(&lf[350],7, C_text("pointer"));
lf[351]=C_h_intern(&lf[351],8, C_text("function"));
lf[352]=C_h_intern(&lf[352],17, C_text("nonnull-c-string\052"));
lf[353]=C_h_intern(&lf[353],26, C_text("nonnull-unsigned-c-string\052"));
lf[354]=C_h_intern(&lf[354],9, C_text("c-string\052"));
lf[355]=C_h_intern(&lf[355],17, C_text("unsigned-c-string"));
lf[356]=C_h_intern(&lf[356],18, C_text("unsigned-c-string\052"));
lf[357]=C_h_intern(&lf[357],13, C_text("c-string-list"));
lf[358]=C_h_intern(&lf[358],14, C_text("c-string-list\052"));
lf[359]=C_h_intern(&lf[359],13, C_text("unsigned-long"));
lf[360]=C_h_intern(&lf[360],16, C_text("unsigned-integer"));
lf[361]=C_h_intern(&lf[361],6, C_text("size_t"));
lf[362]=C_h_intern(&lf[362],18, C_text("unsigned-integer32"));
lf[363]=C_h_intern(&lf[363],18, C_text("unsigned-integer64"));
lf[364]=C_h_intern(&lf[364],9, C_text("integer64"));
lf[365]=C_h_intern(&lf[365],7, C_text("integer"));
lf[366]=C_h_intern(&lf[366],5, C_text("short"));
lf[367]=C_h_intern(&lf[367],4, C_text("long"));
lf[368]=C_h_intern(&lf[368],7, C_text("ssize_t"));
lf[369]=C_h_intern(&lf[369],17, C_text("nonnull-u16vector"));
lf[370]=C_h_intern(&lf[370],16, C_text("nonnull-s8vector"));
lf[371]=C_h_intern(&lf[371],17, C_text("nonnull-s16vector"));
lf[372]=C_h_intern(&lf[372],17, C_text("nonnull-u32vector"));
lf[373]=C_h_intern(&lf[373],17, C_text("nonnull-s32vector"));
lf[374]=C_h_intern(&lf[374],17, C_text("nonnull-u64vector"));
lf[375]=C_h_intern(&lf[375],17, C_text("nonnull-s64vector"));
lf[376]=C_h_intern(&lf[376],17, C_text("nonnull-f32vector"));
lf[377]=C_h_intern(&lf[377],17, C_text("nonnull-f64vector"));
lf[378]=C_h_intern(&lf[378],9, C_text("u16vector"));
lf[379]=C_h_intern(&lf[379],8, C_text("s8vector"));
lf[380]=C_h_intern(&lf[380],9, C_text("s16vector"));
lf[381]=C_h_intern(&lf[381],9, C_text("u32vector"));
lf[382]=C_h_intern(&lf[382],9, C_text("s32vector"));
lf[383]=C_h_intern(&lf[383],9, C_text("u64vector"));
lf[384]=C_h_intern(&lf[384],9, C_text("s64vector"));
lf[385]=C_h_intern(&lf[385],9, C_text("f32vector"));
lf[386]=C_h_intern(&lf[386],9, C_text("f64vector"));
lf[387]=C_h_intern(&lf[387],6, C_text("double"));
lf[388]=C_h_intern(&lf[388],6, C_text("number"));
lf[389]=C_h_intern(&lf[389],12, C_text("unsigned-int"));
lf[390]=C_h_intern(&lf[390],4, C_text("byte"));
lf[391]=C_h_intern(&lf[391],13, C_text("unsigned-byte"));
lf[392]=C_h_intern(&lf[392],5, C_text("int32"));
lf[393]=C_h_intern(&lf[393],14, C_text("unsigned-int32"));
lf[394]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042foreign type `~S\047 refers to itself"));
lf[397]=C_h_intern(&lf[397],52, C_text("chicken.compiler.support#foreign-type-convert-result"));
lf[398]=C_h_intern(&lf[398],54, C_text("chicken.compiler.support#foreign-type-convert-argument"));
lf[399]=C_h_intern(&lf[399],43, C_text("chicken.compiler.support#final-foreign-type"));
lf[400]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042foreign type `~S\047 refers to itself"));
lf[401]=C_h_intern(&lf[401],53, C_text("chicken.compiler.support#estimate-foreign-result-size"));
lf[402]=C_decode_literal(C_heaptop,C_text("\376B\000\0008cannot compute size for unknown foreign type `~S\047 result"));
lf[403]=C_h_intern(&lf[403],4, C_text("bool"));
lf[404]=C_h_intern(&lf[404],4, C_text("void"));
lf[405]=C_h_intern(&lf[405],13, C_text("scheme-object"));
lf[406]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042foreign type `~S\047 refers to itself"));
lf[407]=C_h_intern(&lf[407],62, C_text("chicken.compiler.support#estimate-foreign-result-location-size"));
lf[408]=C_decode_literal(C_heaptop,C_text("\376B\000\0005cannot compute size of location for foreign type `~S\047"));
lf[409]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042foreign type `~S\047 refers to itself"));
lf[410]=C_h_intern(&lf[410],46, C_text("chicken.compiler.support#finish-foreign-result"));
lf[411]=C_h_intern(&lf[411],19, C_text("##sys#peek-c-string"));
lf[412]=C_h_intern(&lf[412],27, C_text("##sys#peek-nonnull-c-string"));
lf[413]=C_h_intern(&lf[413],28, C_text("##sys#peek-and-free-c-string"));
lf[414]=C_h_intern(&lf[414],36, C_text("##sys#peek-and-free-nonnull-c-string"));
lf[415]=C_h_intern(&lf[415],19, C_text("##sys#intern-symbol"));
lf[416]=C_h_intern(&lf[416],24, C_text("##sys#peek-c-string-list"));
lf[417]=C_h_intern(&lf[417],33, C_text("##sys#peek-and-free-c-string-list"));
lf[418]=C_h_intern(&lf[418],19, C_text("##sys#null-pointer\077"));
lf[419]=C_h_intern(&lf[419],3, C_text("not"));
lf[420]=C_h_intern(&lf[420],4, C_text("make"));
lf[421]=C_h_intern(&lf[421],3, C_text("and"));
lf[422]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\010\001c-string\376\003\000\000\002\376\001\000\000\011\001c-string\052\376\003\000\000\002\376\001\000\000\021\001unsigned-c-string\376\003\000\000\002\376\001\000\000\022\001un"
"signed-c-string\052\376\003\000\000\002\376\001\000\000\020\001nonnull-c-string\376\003\000\000\002\376\001\000\000\021\001nonnull-c-string\052\376\003\000\000\002\376\001\000\000"
"\030\001nonnull-unsigned-string\052\376\377\016"));
lf[423]=C_h_intern(&lf[423],27, C_text("chicken.syntax#strip-syntax"));
lf[424]=C_h_intern(&lf[424],52, C_text("chicken.compiler.support#foreign-type->scrutiny-type"));
lf[425]=C_h_intern(&lf[425],9, C_text("undefined"));
lf[426]=C_h_intern(&lf[426],3, C_text("arg"));
lf[427]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\002\001or\376\003\000\000\002\376\001\000\000\007\001boolean\376\003\000\000\002\376\001\000\000\004\001blob\376\377\016"));
lf[428]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\002\001or\376\003\000\000\002\376\001\000\000\007\001boolean\376\003\000\000\002\376\001\000\000\016\001pointer-vector\376\377\016"));
lf[429]=C_h_intern(&lf[429],6, C_text("struct"));
lf[430]=C_h_intern(&lf[430],2, C_text("or"));
lf[431]=C_h_intern(&lf[431],7, C_text("boolean"));
lf[432]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\006\001struct\376\003\000\000\002\376\001\000\000\010\001u8vector\376\377\016"));
lf[433]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\006\001struct\376\003\000\000\002\376\001\000\000\010\001s8vector\376\377\016"));
lf[434]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\006\001struct\376\003\000\000\002\376\001\000\000\011\001u16vector\376\377\016"));
lf[435]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\006\001struct\376\003\000\000\002\376\001\000\000\011\001s16vector\376\377\016"));
lf[436]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\006\001struct\376\003\000\000\002\376\001\000\000\011\001u32vector\376\377\016"));
lf[437]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\006\001struct\376\003\000\000\002\376\001\000\000\011\001s32vector\376\377\016"));
lf[438]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\006\001struct\376\003\000\000\002\376\001\000\000\011\001u64vector\376\377\016"));
lf[439]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\006\001struct\376\003\000\000\002\376\001\000\000\011\001s64vector\376\377\016"));
lf[440]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\006\001struct\376\003\000\000\002\376\001\000\000\011\001f32vector\376\377\016"));
lf[441]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\006\001struct\376\003\000\000\002\376\001\000\000\011\001f64vector\376\377\016"));
lf[442]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\002\001or\376\003\000\000\002\376\001\000\000\007\001boolean\376\003\000\000\002\376\001\000\000\007\001pointer\376\003\000\000\002\376\001\000\000\010\001locative\376\377\016"));
lf[443]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\002\001or\376\003\000\000\002\376\001\000\000\007\001boolean\376\003\000\000\002\376\001\000\000\006\001string\376\377\016"));
lf[444]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\007\001list-of\376\003\000\000\002\376\001\000\000\006\001string\376\377\016"));
lf[445]=C_h_intern(&lf[445],6, C_text("string"));
lf[446]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\002\001or\376\003\000\000\002\376\001\000\000\007\001boolean\376\003\000\000\002\376\001\000\000\007\001pointer\376\003\000\000\002\376\001\000\000\010\001locative\376\377\016"));
lf[447]=C_h_intern(&lf[447],6, C_text("result"));
lf[448]=C_h_intern(&lf[448],44, C_text("chicken.compiler.support#scan-used-variables"));
lf[449]=C_h_intern(&lf[449],44, C_text("chicken.compiler.support#scan-free-variables"));
lf[450]=C_h_intern(&lf[450],39, C_text("chicken.compiler.support#chop-separator"));
lf[451]=C_h_intern(&lf[451],16, C_text("scheme#substring"));
lf[452]=C_h_intern(&lf[452],52, C_text("chicken.compiler.support#make-block-variable-literal"));
lf[453]=C_h_intern(&lf[453],47, C_text("chicken.compiler.support#block-variable-literal"));
lf[454]=C_h_intern(&lf[454],48, C_text("chicken.compiler.support#block-variable-literal\077"));
lf[455]=C_h_intern(&lf[455],52, C_text("chicken.compiler.support#block-variable-literal-name"));
lf[456]=C_h_intern(&lf[456],27, C_text("block-variable-literal-name"));
lf[457]=C_h_intern(&lf[457],21, C_text("scheme#string->symbol"));
lf[458]=C_h_intern(&lf[458],28, C_text("chicken.time#current-seconds"));
lf[460]=C_h_intern(&lf[460],47, C_text("chicken.compiler.support#clear-real-name-table!"));
lf[461]=C_h_intern(&lf[461],39, C_text("chicken.compiler.support#set-real-name!"));
lf[462]=C_h_intern(&lf[462],38, C_text("chicken.compiler.support#get-real-name"));
lf[463]=C_h_intern(&lf[463],33, C_text("chicken.string#string-intersperse"));
lf[464]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004 in "));
lf[465]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003..."));
lf[466]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004 in "));
lf[467]=C_h_intern(&lf[467],12, C_text("contained-in"));
lf[468]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004 in "));
lf[469]=C_h_intern(&lf[469],35, C_text("chicken.compiler.support#real-name2"));
lf[470]=C_h_intern(&lf[470],48, C_text("chicken.compiler.support#display-real-name-table"));
lf[471]=C_h_intern(&lf[471],44, C_text("chicken.compiler.support#source-info->string"));
lf[472]=C_h_intern(&lf[472],19, C_text("chicken.string#conc"));
lf[473]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001:"));
lf[474]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001 "));
lf[475]=C_h_intern(&lf[475],18, C_text("scheme#make-string"));
lf[476]=C_h_intern(&lf[476],10, C_text("scheme#max"));
lf[477]=C_h_intern(&lf[477],42, C_text("chicken.compiler.support#source-info->name"));
lf[478]=C_h_intern(&lf[478],42, C_text("chicken.compiler.support#source-info->line"));
lf[479]=C_h_intern(&lf[479],34, C_text("chicken.compiler.support#call-info"));
lf[480]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001("));
lf[481]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002) "));
lf[482]=C_h_intern(&lf[482],43, C_text("chicken.compiler.support#constant-form-eval"));
lf[483]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032folded constant expression"));
lf[484]=C_decode_literal(C_heaptop,C_text("\376B\000\000Dattempt to constant-fold call to procedure that has multiple results"));
lf[486]=C_h_intern(&lf[486],28, C_text("chicken.condition#condition\077"));
lf[487]=C_h_intern(&lf[487],10, C_text("##sys#list"));
lf[488]=C_decode_literal(C_heaptop,C_text("\376B\000\000.attempt to constant-fold call to non-procedure"));
lf[489]=C_h_intern(&lf[489],49, C_text("chicken.compiler.support#maybe-constant-fold-call"));
lf[490]=C_h_intern(&lf[490],35, C_text("chicken.compiler.support#predicate\077"));
lf[491]=C_h_intern(&lf[491],34, C_text("chicken.compiler.support#foldable\077"));
lf[492]=C_h_intern(&lf[492],20, C_text("##compiler#intrinsic"));
lf[493]=C_h_intern(&lf[493],20, C_text("##sys#number->string"));
lf[494]=C_h_intern(&lf[494],35, C_text("chicken.compiler.support#dump-nodes"));
lf[495]=C_h_intern(&lf[495],21, C_text("##sys#write-char/port"));
lf[496]=C_h_intern(&lf[496],39, C_text("chicken.compiler.support#read-info-hook"));
lf[497]=C_h_intern(&lf[497],9, C_text("list-info"));
lf[498]=C_h_intern(&lf[498],29, C_text("##sys#current-source-filename"));
lf[499]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001:"));
lf[500]=C_h_intern(&lf[500],41, C_text("chicken.compiler.support#read/source-info"));
lf[501]=C_h_intern(&lf[501],10, C_text("##sys#read"));
lf[502]=C_h_intern(&lf[502],20, C_text("##sys#user-read-hook"));
lf[503]=C_h_intern(&lf[503],15, C_text("foreign-declare"));
lf[504]=C_h_intern(&lf[504],7, C_text("declare"));
lf[505]=C_decode_literal(C_heaptop,C_text("\376B\000\000&unexpected end of `#> ... <#\047 sequence"));
lf[506]=C_h_intern(&lf[506],20, C_text("##sys#read-char/port"));
lf[507]=C_h_intern(&lf[507],25, C_text("chicken.platform#feature\077"));
lf[508]=C_h_intern_kw(&lf[508],5, C_text("64bit"));
lf[509]=C_h_intern(&lf[509],38, C_text("chicken.compiler.support#small-bignum\077"));
lf[510]=C_h_intern(&lf[510],6, C_text("hidden"));
lf[511]=C_h_intern(&lf[511],40, C_text("chicken.compiler.support#export-variable"));
lf[512]=C_h_intern(&lf[512],8, C_text("exported"));
lf[513]=C_h_intern(&lf[513],38, C_text("chicken.compiler.support#mark-variable"));
lf[514]=C_h_intern(&lf[514],38, C_text("chicken.compiler.support#variable-mark"));
lf[515]=C_h_intern(&lf[515],35, C_text("chicken.compiler.support#intrinsic\077"));
lf[516]=C_h_intern(&lf[516],19, C_text("##compiler#foldable"));
lf[517]=C_h_intern(&lf[517],20, C_text("##compiler#predicate"));
lf[518]=C_h_intern(&lf[518],49, C_text("chicken.compiler.support#load-identifier-database"));
lf[519]=C_h_intern(&lf[519],9, C_text("##core#db"));
lf[520]=C_h_intern(&lf[520],27, C_text("scheme#call-with-input-file"));
lf[521]=C_h_intern(&lf[521],1, C_text("p"));
lf[522]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004 ..."));
lf[523]=C_decode_literal(C_heaptop,C_text("\376B\000\000\034loading identifier database "));
lf[524]=C_h_intern(&lf[524],22, C_text("chicken.load#find-file"));
lf[525]=C_h_intern(&lf[525],32, C_text("chicken.platform#repository-path"));
lf[526]=C_h_intern(&lf[526],38, C_text("chicken.compiler.support#print-version"));
lf[527]=C_h_intern(&lf[527],19, C_text("chicken.base#print\052"));
lf[528]=C_decode_literal(C_heaptop,C_text("\376B\000\000KCHICKEN\012(c) 2008-2020, The CHICKEN Team\012(c) 2000-2007, Felix L. Winkelmann\012"
));
lf[529]=C_h_intern(&lf[529],36, C_text("chicken.compiler.support#print-usage"));
lf[530]=C_decode_literal(C_heaptop,C_text("\376B\000\031\317Usage: chicken FILENAME [OPTION ...]\012\012  `chicken\047 is the CHICKEN compiler.\012"
"  \012  FILENAME should be a complete source file name with extension, or \042-\042 for\012 "
" standard input. OPTION may be one of the following:\012\012  General options:\012\012    -h"
"elp                        display this text and exit\012    -version              "
"       display compiler version and exit\012    -release                     print "
"release number and exit\012    -verbose                     display information on "
"compilation progress\012\012  File and pathname options:\012\012    -output-file FILENAME   "
"     specifies output-filename, default is \047out.c\047\012    -include-path PATHNAME   "
"    specifies alternative path for included files\012    -to-stdout                "
"   write compiled file to stdout instead of file\012\012  Language options:\012\012    -feat"
"ure SYMBOL              register feature identifier\012    -no-feature SYMBOL      "
"     disable built-in feature identifier\012\012  Syntax related options:\012\012    -case-i"
"nsensitive            don\047t preserve case of read symbols\012    -keyword-style STY"
"LE         allow alternative keyword syntax\012                                  (p"
"refix, suffix or none)\012    -no-parentheses-synonyms     disables list delimiter "
"synonyms\012    -no-symbol-escape            disables support for escaped symbols\012 "
"   -r5rs-syntax                 disables the CHICKEN extensions to\012             "
"                     R5RS syntax\012    -compile-syntax              macros are mad"
"e available at run-time\012    -emit-import-library MODULE  write compile-time modu"
"le information into\012                                  separate file\012    -emit-al"
"l-import-libraries   emit import-libraries for all defined modules\012    -no-compi"
"ler-syntax          disable expansion of compiler-macros\012    -module NAME       "
"          wrap compiled code in a module\012    -module-registration         always"
" generate module registration code\012    -no-module-registration      never genera"
"te module registration code\012                                  (overrides `-modul"
"e-registration\047)\012\012  Translation options:\012\012    -explicit-use                do no"
"t use units \047library\047 and \047eval\047 by\012                                  default\012  "
"  -check-syntax                stop compilation after macro-expansion\012    -analy"
"ze-only                stop compilation after first analysis pass\012\012  Debugging o"
"ptions:\012\012    -no-warnings                 disable warnings\012    -debug-level NUMB"
"ER          set level of available debugging information\012    -no-trace          "
"          disable tracing information\012    -debug-info                  enable de"
"bug-information in compiled code for use\012                                  with "
"an external debugger\012    -profile                     executable emits profiling"
" information \012    -profile-name FILENAME       name of the generated profile inf"
"ormation file\012    -accumulate-profile          executable emits profiling inform"
"ation in\012                                  append mode\012    -no-lambda-info      "
"        omit additional procedure-information\012    -emit-types-file FILENAME    w"
"rite type-declaration information into file\012    -consult-types-file FILENAME loa"
"d additional type database\012\012  Optimization options:\012\012    -optimize-level NUMBER "
"      enable certain sets of optimization options\012    -optimize-leaf-routines   "
"   enable leaf routine optimization\012    -no-usual-integrations       standard pr"
"ocedures may be redefined\012    -unsafe                      disable all safety ch"
"ecks\012    -local                       assume globals are only modified in curren"
"t\012                                  file\012    -block                       enable"
" block-compilation\012    -disable-interrupts          disable interrupts in compil"
"ed code\012    -fixnum-arithmetic           assume all numbers are fixnums\012    -dis"
"able-stack-overflow-checks  disables detection of stack-overflows\012    -inline   "
"                   enable inlining\012    -inline-limit LIMIT          set inlining"
" threshold\012    -inline-global               enable cross-module inlining\012    -sp"
"ecialize                  perform type-based specialization of primitive calls\012 "
"   -emit-inline-file FILENAME   generate file with globally inlinable\012          "
"                        procedures (implies -inline -local)\012    -consult-inline-"
"file FILENAME  explicitly load inline file\012    -no-argc-checks              disa"
"ble argument count checks\012    -no-bound-checks             disable bound variabl"
"e checks\012    -no-procedure-checks         disable procedure call checks\012    -no-"
"procedure-checks-for-usual-bindings\012                                   disable p"
"rocedure call checks only for usual\012                                   bindings\012"
"    -no-procedure-checks-for-toplevel-bindings\012                                 "
"  disable procedure call checks for toplevel\012                                   "
"bindings\012    -strict-types                assume variable do not change their ty"
"pe\012    -clustering                  combine groups of local procedures into disp"
"atch\012                                   loop\012    -lfa2                        pe"
"rform additional lightweight flow-analysis pass\012    -unroll-limit LIMIT         "
" specifies inlining limit for self-recursive calls\012\012  Configuration options:\012\012  "
"  -unit NAME                   compile file as a library unit\012    -uses NAME    "
"               declare library unit as used.\012    -heap-size NUMBER            sp"
"ecifies heap-size of compiled executable\012    -nursery NUMBER  -stack-size NUMBER"
"\012                                 specifies nursery size of compiled executable\012"
"    -extend FILENAME             load file before compilation commences\012    -pre"
"lude EXPRESSION          add expression to front of source file\012    -postlude EX"
"PRESSION         add expression to end of source file\012    -prologue FILENAME    "
"       include file before main source file\012    -epilogue FILENAME           inc"
"lude file after main source file\012    -dynamic                     compile as dyn"
"amically loadable code\012    -require-extension NAME      require and import exten"
"sion NAME\012\012  Obscure options:\012\012    -debug MODES                 display debuggin"
"g output for the given modes\012    -raw                         do not generate im"
"plicit init- and exit code                           \012    -emit-external-prototy"
"pes-first\012                                 emit prototypes for callbacks before "
"foreign\012                                  declarations\012    -regenerate-import-li"
"braries emit import libraries even when unchanged\012    -ignore-repository        "
"   do not refer to repository for extensions\012    -setup-mode                  pr"
"efer the current directory when locating extensions\012"));
lf[531]=C_h_intern(&lf[531],44, C_text("chicken.compiler.support#print-debug-options"));
lf[532]=C_decode_literal(C_heaptop,C_text("\376B\000\007\026\012Available debugging options:\012\012     a          show node-matching during si"
"mplification\012     b          show breakdown of time needed for each compiler pas"
"s\012     c          print every expression before macro-expansion\012     d          "
"lists all assigned global variables\012     e          show information about speci"
"alizations\012     h          you already figured that out\012     i          show inf"
"ormation about inlining\012     m          show GC statistics during compilation\012  "
"   n          print the line-number database \012     o          show performed opt"
"imizations\012     p          display information about what the compiler is curren"
"tly doing\012     r          show invocation parameters\012     s          show progra"
"m-size information and other statistics\012     t          show time needed for com"
"pilation\012     u          lists all unassigned global variable references\012     x "
"         display information about experimental features\012     D          when pr"
"inting nodes, use node-tree output\012     I          show inferred type informatio"
"n for unexported globals\012     M          show syntax-/runtime-requirements\012     "
"N          show the real-name mapping table\012     P          show expressions aft"
"er specialization\012     S          show applications of compiler syntax\012     T   "
"       show expressions after converting to node tree\012     1          show sourc"
"e expressions\012     2          show canonicalized expressions\012     3          sho"
"w expressions converted into CPS\012     4          show database after each analys"
"is pass\012     5          show expressions after each optimization pass\012     6    "
"      show expressions after each inlining pass\012     7          show expressions"
" after complete optimization\012     8          show database after final analysis\012"
"     9          show expressions after closure conversion\012\012"));
lf[533]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007#<node "));
lf[534]=C_h_intern(&lf[534],29, C_text("##sys#register-record-printer"));
lf[535]=C_h_intern(&lf[535],45, C_text("chicken.condition#condition-property-accessor"));
lf[536]=C_h_intern(&lf[536],3, C_text("exn"));
lf[537]=C_h_intern(&lf[537],7, C_text("message"));
lf[538]=C_h_intern(&lf[538],37, C_text("chicken.condition#condition-predicate"));
C_register_lf2(lf,539,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5288,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[777] = {
{C_text("f19580:support_2escm"),(void*)f19580},
{C_text("f_10030:support_2escm"),(void*)f_10030},
{C_text("f_10034:support_2escm"),(void*)f_10034},
{C_text("f_10044:support_2escm"),(void*)f_10044},
{C_text("f_10068:support_2escm"),(void*)f_10068},
{C_text("f_10083:support_2escm"),(void*)f_10083},
{C_text("f_10095:support_2escm"),(void*)f_10095},
{C_text("f_10120:support_2escm"),(void*)f_10120},
{C_text("f_10145:support_2escm"),(void*)f_10145},
{C_text("f_10175:support_2escm"),(void*)f_10175},
{C_text("f_10177:support_2escm"),(void*)f_10177},
{C_text("f_10202:support_2escm"),(void*)f_10202},
{C_text("f_10233:support_2escm"),(void*)f_10233},
{C_text("f_10245:support_2escm"),(void*)f_10245},
{C_text("f_10249:support_2escm"),(void*)f_10249},
{C_text("f_10272:support_2escm"),(void*)f_10272},
{C_text("f_10283:support_2escm"),(void*)f_10283},
{C_text("f_10290:support_2escm"),(void*)f_10290},
{C_text("f_10301:support_2escm"),(void*)f_10301},
{C_text("f_10303:support_2escm"),(void*)f_10303},
{C_text("f_10328:support_2escm"),(void*)f_10328},
{C_text("f_10358:support_2escm"),(void*)f_10358},
{C_text("f_10360:support_2escm"),(void*)f_10360},
{C_text("f_10385:support_2escm"),(void*)f_10385},
{C_text("f_10399:support_2escm"),(void*)f_10399},
{C_text("f_10409:support_2escm"),(void*)f_10409},
{C_text("f_10411:support_2escm"),(void*)f_10411},
{C_text("f_10436:support_2escm"),(void*)f_10436},
{C_text("f_10509:support_2escm"),(void*)f_10509},
{C_text("f_10515:support_2escm"),(void*)f_10515},
{C_text("f_10541:support_2escm"),(void*)f_10541},
{C_text("f_10545:support_2escm"),(void*)f_10545},
{C_text("f_10561:support_2escm"),(void*)f_10561},
{C_text("f_10567:support_2escm"),(void*)f_10567},
{C_text("f_10573:support_2escm"),(void*)f_10573},
{C_text("f_10579:support_2escm"),(void*)f_10579},
{C_text("f_10583:support_2escm"),(void*)f_10583},
{C_text("f_10586:support_2escm"),(void*)f_10586},
{C_text("f_10600:support_2escm"),(void*)f_10600},
{C_text("f_10603:support_2escm"),(void*)f_10603},
{C_text("f_10606:support_2escm"),(void*)f_10606},
{C_text("f_10613:support_2escm"),(void*)f_10613},
{C_text("f_10615:support_2escm"),(void*)f_10615},
{C_text("f_10630:support_2escm"),(void*)f_10630},
{C_text("f_10663:support_2escm"),(void*)f_10663},
{C_text("f_10691:support_2escm"),(void*)f_10691},
{C_text("f_10695:support_2escm"),(void*)f_10695},
{C_text("f_10719:support_2escm"),(void*)f_10719},
{C_text("f_10751:support_2escm"),(void*)f_10751},
{C_text("f_10760:support_2escm"),(void*)f_10760},
{C_text("f_10764:support_2escm"),(void*)f_10764},
{C_text("f_10770:support_2escm"),(void*)f_10770},
{C_text("f_10776:support_2escm"),(void*)f_10776},
{C_text("f_10779:support_2escm"),(void*)f_10779},
{C_text("f_10784:support_2escm"),(void*)f_10784},
{C_text("f_10794:support_2escm"),(void*)f_10794},
{C_text("f_10809:support_2escm"),(void*)f_10809},
{C_text("f_10811:support_2escm"),(void*)f_10811},
{C_text("f_10836:support_2escm"),(void*)f_10836},
{C_text("f_10857:support_2escm"),(void*)f_10857},
{C_text("f_10882:support_2escm"),(void*)f_10882},
{C_text("f_10904:support_2escm"),(void*)f_10904},
{C_text("f_10912:support_2escm"),(void*)f_10912},
{C_text("f_10963:support_2escm"),(void*)f_10963},
{C_text("f_10970:support_2escm"),(void*)f_10970},
{C_text("f_10973:support_2escm"),(void*)f_10973},
{C_text("f_11002:support_2escm"),(void*)f_11002},
{C_text("f_11010:support_2escm"),(void*)f_11010},
{C_text("f_11026:support_2escm"),(void*)f_11026},
{C_text("f_11029:support_2escm"),(void*)f_11029},
{C_text("f_11035:support_2escm"),(void*)f_11035},
{C_text("f_11055:support_2escm"),(void*)f_11055},
{C_text("f_11078:support_2escm"),(void*)f_11078},
{C_text("f_11083:support_2escm"),(void*)f_11083},
{C_text("f_11087:support_2escm"),(void*)f_11087},
{C_text("f_11090:support_2escm"),(void*)f_11090},
{C_text("f_11096:support_2escm"),(void*)f_11096},
{C_text("f_11099:support_2escm"),(void*)f_11099},
{C_text("f_11116:support_2escm"),(void*)f_11116},
{C_text("f_11126:support_2escm"),(void*)f_11126},
{C_text("f_11128:support_2escm"),(void*)f_11128},
{C_text("f_11153:support_2escm"),(void*)f_11153},
{C_text("f_11164:support_2escm"),(void*)f_11164},
{C_text("f_11172:support_2escm"),(void*)f_11172},
{C_text("f_11180:support_2escm"),(void*)f_11180},
{C_text("f_11193:support_2escm"),(void*)f_11193},
{C_text("f_11195:support_2escm"),(void*)f_11195},
{C_text("f_11243:support_2escm"),(void*)f_11243},
{C_text("f_11268:support_2escm"),(void*)f_11268},
{C_text("f_11287:support_2escm"),(void*)f_11287},
{C_text("f_11292:support_2escm"),(void*)f_11292},
{C_text("f_11302:support_2escm"),(void*)f_11302},
{C_text("f_11304:support_2escm"),(void*)f_11304},
{C_text("f_11329:support_2escm"),(void*)f_11329},
{C_text("f_11351:support_2escm"),(void*)f_11351},
{C_text("f_11402:support_2escm"),(void*)f_11402},
{C_text("f_11446:support_2escm"),(void*)f_11446},
{C_text("f_11493:support_2escm"),(void*)f_11493},
{C_text("f_11521:support_2escm"),(void*)f_11521},
{C_text("f_11525:support_2escm"),(void*)f_11525},
{C_text("f_11529:support_2escm"),(void*)f_11529},
{C_text("f_11548:support_2escm"),(void*)f_11548},
{C_text("f_11560:support_2escm"),(void*)f_11560},
{C_text("f_11562:support_2escm"),(void*)f_11562},
{C_text("f_11604:support_2escm"),(void*)f_11604},
{C_text("f_11614:support_2escm"),(void*)f_11614},
{C_text("f_11636:support_2escm"),(void*)f_11636},
{C_text("f_11642:support_2escm"),(void*)f_11642},
{C_text("f_11656:support_2escm"),(void*)f_11656},
{C_text("f_11660:support_2escm"),(void*)f_11660},
{C_text("f_11666:support_2escm"),(void*)f_11666},
{C_text("f_11704:support_2escm"),(void*)f_11704},
{C_text("f_11708:support_2escm"),(void*)f_11708},
{C_text("f_11711:support_2escm"),(void*)f_11711},
{C_text("f_11714:support_2escm"),(void*)f_11714},
{C_text("f_11749:support_2escm"),(void*)f_11749},
{C_text("f_11793:support_2escm"),(void*)f_11793},
{C_text("f_11795:support_2escm"),(void*)f_11795},
{C_text("f_11820:support_2escm"),(void*)f_11820},
{C_text("f_11835:support_2escm"),(void*)f_11835},
{C_text("f_11864:support_2escm"),(void*)f_11864},
{C_text("f_11866:support_2escm"),(void*)f_11866},
{C_text("f_11891:support_2escm"),(void*)f_11891},
{C_text("f_11900:support_2escm"),(void*)f_11900},
{C_text("f_11903:support_2escm"),(void*)f_11903},
{C_text("f_11909:support_2escm"),(void*)f_11909},
{C_text("f_11957:support_2escm"),(void*)f_11957},
{C_text("f_11960:support_2escm"),(void*)f_11960},
{C_text("f_11966:support_2escm"),(void*)f_11966},
{C_text("f_11974:support_2escm"),(void*)f_11974},
{C_text("f_11982:support_2escm"),(void*)f_11982},
{C_text("f_11992:support_2escm"),(void*)f_11992},
{C_text("f_12020:support_2escm"),(void*)f_12020},
{C_text("f_12024:support_2escm"),(void*)f_12024},
{C_text("f_12029:support_2escm"),(void*)f_12029},
{C_text("f_12035:support_2escm"),(void*)f_12035},
{C_text("f_12038:support_2escm"),(void*)f_12038},
{C_text("f_12043:support_2escm"),(void*)f_12043},
{C_text("f_12053:support_2escm"),(void*)f_12053},
{C_text("f_12068:support_2escm"),(void*)f_12068},
{C_text("f_12070:support_2escm"),(void*)f_12070},
{C_text("f_12077:support_2escm"),(void*)f_12077},
{C_text("f_12098:support_2escm"),(void*)f_12098},
{C_text("f_12132:support_2escm"),(void*)f_12132},
{C_text("f_12135:support_2escm"),(void*)f_12135},
{C_text("f_12160:support_2escm"),(void*)f_12160},
{C_text("f_12166:support_2escm"),(void*)f_12166},
{C_text("f_12192:support_2escm"),(void*)f_12192},
{C_text("f_12225:support_2escm"),(void*)f_12225},
{C_text("f_12227:support_2escm"),(void*)f_12227},
{C_text("f_12233:support_2escm"),(void*)f_12233},
{C_text("f_12239:support_2escm"),(void*)f_12239},
{C_text("f_12243:support_2escm"),(void*)f_12243},
{C_text("f_12266:support_2escm"),(void*)f_12266},
{C_text("f_12277:support_2escm"),(void*)f_12277},
{C_text("f_12283:support_2escm"),(void*)f_12283},
{C_text("f_12286:support_2escm"),(void*)f_12286},
{C_text("f_12294:support_2escm"),(void*)f_12294},
{C_text("f_12320:support_2escm"),(void*)f_12320},
{C_text("f_12342:support_2escm"),(void*)f_12342},
{C_text("f_12367:support_2escm"),(void*)f_12367},
{C_text("f_12389:support_2escm"),(void*)f_12389},
{C_text("f_12407:support_2escm"),(void*)f_12407},
{C_text("f_12438:support_2escm"),(void*)f_12438},
{C_text("f_12490:support_2escm"),(void*)f_12490},
{C_text("f_12496:support_2escm"),(void*)f_12496},
{C_text("f_12516:support_2escm"),(void*)f_12516},
{C_text("f_12522:support_2escm"),(void*)f_12522},
{C_text("f_12548:support_2escm"),(void*)f_12548},
{C_text("f_12562:support_2escm"),(void*)f_12562},
{C_text("f_12570:support_2escm"),(void*)f_12570},
{C_text("f_12625:support_2escm"),(void*)f_12625},
{C_text("f_12654:support_2escm"),(void*)f_12654},
{C_text("f_12751:support_2escm"),(void*)f_12751},
{C_text("f_12757:support_2escm"),(void*)f_12757},
{C_text("f_12764:support_2escm"),(void*)f_12764},
{C_text("f_12767:support_2escm"),(void*)f_12767},
{C_text("f_12790:support_2escm"),(void*)f_12790},
{C_text("f_12792:support_2escm"),(void*)f_12792},
{C_text("f_12798:support_2escm"),(void*)f_12798},
{C_text("f_12805:support_2escm"),(void*)f_12805},
{C_text("f_12808:support_2escm"),(void*)f_12808},
{C_text("f_12827:support_2escm"),(void*)f_12827},
{C_text("f_12829:support_2escm"),(void*)f_12829},
{C_text("f_12835:support_2escm"),(void*)f_12835},
{C_text("f_12848:support_2escm"),(void*)f_12848},
{C_text("f_12876:support_2escm"),(void*)f_12876},
{C_text("f_12878:support_2escm"),(void*)f_12878},
{C_text("f_12899:support_2escm"),(void*)f_12899},
{C_text("f_12906:support_2escm"),(void*)f_12906},
{C_text("f_12912:support_2escm"),(void*)f_12912},
{C_text("f_12918:support_2escm"),(void*)f_12918},
{C_text("f_12927:support_2escm"),(void*)f_12927},
{C_text("f_12936:support_2escm"),(void*)f_12936},
{C_text("f_12945:support_2escm"),(void*)f_12945},
{C_text("f_12954:support_2escm"),(void*)f_12954},
{C_text("f_12963:support_2escm"),(void*)f_12963},
{C_text("f_12989:support_2escm"),(void*)f_12989},
{C_text("f_12992:support_2escm"),(void*)f_12992},
{C_text("f_13003:support_2escm"),(void*)f_13003},
{C_text("f_13005:support_2escm"),(void*)f_13005},
{C_text("f_13059:support_2escm"),(void*)f_13059},
{C_text("f_13065:support_2escm"),(void*)f_13065},
{C_text("f_13071:support_2escm"),(void*)f_13071},
{C_text("f_13077:support_2escm"),(void*)f_13077},
{C_text("f_13102:support_2escm"),(void*)f_13102},
{C_text("f_13117:support_2escm"),(void*)f_13117},
{C_text("f_13135:support_2escm"),(void*)f_13135},
{C_text("f_13185:support_2escm"),(void*)f_13185},
{C_text("f_13200:support_2escm"),(void*)f_13200},
{C_text("f_13240:support_2escm"),(void*)f_13240},
{C_text("f_13243:support_2escm"),(void*)f_13243},
{C_text("f_13258:support_2escm"),(void*)f_13258},
{C_text("f_13282:support_2escm"),(void*)f_13282},
{C_text("f_13308:support_2escm"),(void*)f_13308},
{C_text("f_13314:support_2escm"),(void*)f_13314},
{C_text("f_13320:support_2escm"),(void*)f_13320},
{C_text("f_13323:support_2escm"),(void*)f_13323},
{C_text("f_13326:support_2escm"),(void*)f_13326},
{C_text("f_13329:support_2escm"),(void*)f_13329},
{C_text("f_13351:support_2escm"),(void*)f_13351},
{C_text("f_13357:support_2escm"),(void*)f_13357},
{C_text("f_13363:support_2escm"),(void*)f_13363},
{C_text("f_13366:support_2escm"),(void*)f_13366},
{C_text("f_13369:support_2escm"),(void*)f_13369},
{C_text("f_13372:support_2escm"),(void*)f_13372},
{C_text("f_13395:support_2escm"),(void*)f_13395},
{C_text("f_13398:support_2escm"),(void*)f_13398},
{C_text("f_13439:support_2escm"),(void*)f_13439},
{C_text("f_13442:support_2escm"),(void*)f_13442},
{C_text("f_13457:support_2escm"),(void*)f_13457},
{C_text("f_13484:support_2escm"),(void*)f_13484},
{C_text("f_13527:support_2escm"),(void*)f_13527},
{C_text("f_13531:support_2escm"),(void*)f_13531},
{C_text("f_13558:support_2escm"),(void*)f_13558},
{C_text("f_13561:support_2escm"),(void*)f_13561},
{C_text("f_13596:support_2escm"),(void*)f_13596},
{C_text("f_13632:support_2escm"),(void*)f_13632},
{C_text("f_14135:support_2escm"),(void*)f_14135},
{C_text("f_14141:support_2escm"),(void*)f_14141},
{C_text("f_14151:support_2escm"),(void*)f_14151},
{C_text("f_14162:support_2escm"),(void*)f_14162},
{C_text("f_14172:support_2escm"),(void*)f_14172},
{C_text("f_14183:support_2escm"),(void*)f_14183},
{C_text("f_14187:support_2escm"),(void*)f_14187},
{C_text("f_14198:support_2escm"),(void*)f_14198},
{C_text("f_14202:support_2escm"),(void*)f_14202},
{C_text("f_14213:support_2escm"),(void*)f_14213},
{C_text("f_14219:support_2escm"),(void*)f_14219},
{C_text("f_14223:support_2escm"),(void*)f_14223},
{C_text("f_14227:support_2escm"),(void*)f_14227},
{C_text("f_14246:support_2escm"),(void*)f_14246},
{C_text("f_14252:support_2escm"),(void*)f_14252},
{C_text("f_14255:support_2escm"),(void*)f_14255},
{C_text("f_14264:support_2escm"),(void*)f_14264},
{C_text("f_14274:support_2escm"),(void*)f_14274},
{C_text("f_14283:support_2escm"),(void*)f_14283},
{C_text("f_14295:support_2escm"),(void*)f_14295},
{C_text("f_14307:support_2escm"),(void*)f_14307},
{C_text("f_14319:support_2escm"),(void*)f_14319},
{C_text("f_14325:support_2escm"),(void*)f_14325},
{C_text("f_14329:support_2escm"),(void*)f_14329},
{C_text("f_14356:support_2escm"),(void*)f_14356},
{C_text("f_14721:support_2escm"),(void*)f_14721},
{C_text("f_14727:support_2escm"),(void*)f_14727},
{C_text("f_14739:support_2escm"),(void*)f_14739},
{C_text("f_14749:support_2escm"),(void*)f_14749},
{C_text("f_14761:support_2escm"),(void*)f_14761},
{C_text("f_14767:support_2escm"),(void*)f_14767},
{C_text("f_14771:support_2escm"),(void*)f_14771},
{C_text("f_14798:support_2escm"),(void*)f_14798},
{C_text("f_15171:support_2escm"),(void*)f_15171},
{C_text("f_15177:support_2escm"),(void*)f_15177},
{C_text("f_15181:support_2escm"),(void*)f_15181},
{C_text("f_15297:support_2escm"),(void*)f_15297},
{C_text("f_15325:support_2escm"),(void*)f_15325},
{C_text("f_15445:support_2escm"),(void*)f_15445},
{C_text("f_15452:support_2escm"),(void*)f_15452},
{C_text("f_15455:support_2escm"),(void*)f_15455},
{C_text("f_15458:support_2escm"),(void*)f_15458},
{C_text("f_15482:support_2escm"),(void*)f_15482},
{C_text("f_15557:support_2escm"),(void*)f_15557},
{C_text("f_15644:support_2escm"),(void*)f_15644},
{C_text("f_15665:support_2escm"),(void*)f_15665},
{C_text("f_15683:support_2escm"),(void*)f_15683},
{C_text("f_15705:support_2escm"),(void*)f_15705},
{C_text("f_16075:support_2escm"),(void*)f_16075},
{C_text("f_16079:support_2escm"),(void*)f_16079},
{C_text("f_16081:support_2escm"),(void*)f_16081},
{C_text("f_16113:support_2escm"),(void*)f_16113},
{C_text("f_16121:support_2escm"),(void*)f_16121},
{C_text("f_16131:support_2escm"),(void*)f_16131},
{C_text("f_16177:support_2escm"),(void*)f_16177},
{C_text("f_16185:support_2escm"),(void*)f_16185},
{C_text("f_16195:support_2escm"),(void*)f_16195},
{C_text("f_16230:support_2escm"),(void*)f_16230},
{C_text("f_16233:support_2escm"),(void*)f_16233},
{C_text("f_16267:support_2escm"),(void*)f_16267},
{C_text("f_16286:support_2escm"),(void*)f_16286},
{C_text("f_16292:support_2escm"),(void*)f_16292},
{C_text("f_16296:support_2escm"),(void*)f_16296},
{C_text("f_16322:support_2escm"),(void*)f_16322},
{C_text("f_16331:support_2escm"),(void*)f_16331},
{C_text("f_16342:support_2escm"),(void*)f_16342},
{C_text("f_16361:support_2escm"),(void*)f_16361},
{C_text("f_16373:support_2escm"),(void*)f_16373},
{C_text("f_16417:support_2escm"),(void*)f_16417},
{C_text("f_16419:support_2escm"),(void*)f_16419},
{C_text("f_16431:support_2escm"),(void*)f_16431},
{C_text("f_16441:support_2escm"),(void*)f_16441},
{C_text("f_16455:support_2escm"),(void*)f_16455},
{C_text("f_16460:support_2escm"),(void*)f_16460},
{C_text("f_16484:support_2escm"),(void*)f_16484},
{C_text("f_16490:support_2escm"),(void*)f_16490},
{C_text("f_16496:support_2escm"),(void*)f_16496},
{C_text("f_16505:support_2escm"),(void*)f_16505},
{C_text("f_16513:support_2escm"),(void*)f_16513},
{C_text("f_16519:support_2escm"),(void*)f_16519},
{C_text("f_16522:support_2escm"),(void*)f_16522},
{C_text("f_16525:support_2escm"),(void*)f_16525},
{C_text("f_16528:support_2escm"),(void*)f_16528},
{C_text("f_16531:support_2escm"),(void*)f_16531},
{C_text("f_16536:support_2escm"),(void*)f_16536},
{C_text("f_16540:support_2escm"),(void*)f_16540},
{C_text("f_16552:support_2escm"),(void*)f_16552},
{C_text("f_16557:support_2escm"),(void*)f_16557},
{C_text("f_16559:support_2escm"),(void*)f_16559},
{C_text("f_16565:support_2escm"),(void*)f_16565},
{C_text("f_16572:support_2escm"),(void*)f_16572},
{C_text("f_16575:support_2escm"),(void*)f_16575},
{C_text("f_16579:support_2escm"),(void*)f_16579},
{C_text("f_16585:support_2escm"),(void*)f_16585},
{C_text("f_16591:support_2escm"),(void*)f_16591},
{C_text("f_16618:support_2escm"),(void*)f_16618},
{C_text("f_16620:support_2escm"),(void*)f_16620},
{C_text("f_16634:support_2escm"),(void*)f_16634},
{C_text("f_16644:support_2escm"),(void*)f_16644},
{C_text("f_16657:support_2escm"),(void*)f_16657},
{C_text("f_16672:support_2escm"),(void*)f_16672},
{C_text("f_16676:support_2escm"),(void*)f_16676},
{C_text("f_16683:support_2escm"),(void*)f_16683},
{C_text("f_16687:support_2escm"),(void*)f_16687},
{C_text("f_16692:support_2escm"),(void*)f_16692},
{C_text("f_16696:support_2escm"),(void*)f_16696},
{C_text("f_16704:support_2escm"),(void*)f_16704},
{C_text("f_16710:support_2escm"),(void*)f_16710},
{C_text("f_16717:support_2escm"),(void*)f_16717},
{C_text("f_16720:support_2escm"),(void*)f_16720},
{C_text("f_16723:support_2escm"),(void*)f_16723},
{C_text("f_16728:support_2escm"),(void*)f_16728},
{C_text("f_16748:support_2escm"),(void*)f_16748},
{C_text("f_16752:support_2escm"),(void*)f_16752},
{C_text("f_16763:support_2escm"),(void*)f_16763},
{C_text("f_16778:support_2escm"),(void*)f_16778},
{C_text("f_16790:support_2escm"),(void*)f_16790},
{C_text("f_16797:support_2escm"),(void*)f_16797},
{C_text("f_16827:support_2escm"),(void*)f_16827},
{C_text("f_16851:support_2escm"),(void*)f_16851},
{C_text("f_16866:support_2escm"),(void*)f_16866},
{C_text("f_16869:support_2escm"),(void*)f_16869},
{C_text("f_16875:support_2escm"),(void*)f_16875},
{C_text("f_16884:support_2escm"),(void*)f_16884},
{C_text("f_16887:support_2escm"),(void*)f_16887},
{C_text("f_16926:support_2escm"),(void*)f_16926},
{C_text("f_16932:support_2escm"),(void*)f_16932},
{C_text("f_16938:support_2escm"),(void*)f_16938},
{C_text("f_16941:support_2escm"),(void*)f_16941},
{C_text("f_16947:support_2escm"),(void*)f_16947},
{C_text("f_16953:support_2escm"),(void*)f_16953},
{C_text("f_16959:support_2escm"),(void*)f_16959},
{C_text("f_16965:support_2escm"),(void*)f_16965},
{C_text("f_16987:support_2escm"),(void*)f_16987},
{C_text("f_16989:support_2escm"),(void*)f_16989},
{C_text("f_17023:support_2escm"),(void*)f_17023},
{C_text("f_17057:support_2escm"),(void*)f_17057},
{C_text("f_17060:support_2escm"),(void*)f_17060},
{C_text("f_17088:support_2escm"),(void*)f_17088},
{C_text("f_17095:support_2escm"),(void*)f_17095},
{C_text("f_17110:support_2escm"),(void*)f_17110},
{C_text("f_17116:support_2escm"),(void*)f_17116},
{C_text("f_17119:support_2escm"),(void*)f_17119},
{C_text("f_17156:support_2escm"),(void*)f_17156},
{C_text("f_17171:support_2escm"),(void*)f_17171},
{C_text("f_17181:support_2escm"),(void*)f_17181},
{C_text("f_17184:support_2escm"),(void*)f_17184},
{C_text("f_17196:support_2escm"),(void*)f_17196},
{C_text("f_17202:support_2escm"),(void*)f_17202},
{C_text("f_17208:support_2escm"),(void*)f_17208},
{C_text("f_17211:support_2escm"),(void*)f_17211},
{C_text("f_17213:support_2escm"),(void*)f_17213},
{C_text("f_17220:support_2escm"),(void*)f_17220},
{C_text("f_17226:support_2escm"),(void*)f_17226},
{C_text("f_17237:support_2escm"),(void*)f_17237},
{C_text("f_17285:support_2escm"),(void*)f_17285},
{C_text("f_17287:support_2escm"),(void*)f_17287},
{C_text("f_17293:support_2escm"),(void*)f_17293},
{C_text("f_17297:support_2escm"),(void*)f_17297},
{C_text("f_17302:support_2escm"),(void*)f_17302},
{C_text("f_17330:support_2escm"),(void*)f_17330},
{C_text("f_17338:support_2escm"),(void*)f_17338},
{C_text("f_17341:support_2escm"),(void*)f_17341},
{C_text("f_17344:support_2escm"),(void*)f_17344},
{C_text("f_17347:support_2escm"),(void*)f_17347},
{C_text("f_17350:support_2escm"),(void*)f_17350},
{C_text("f_17353:support_2escm"),(void*)f_17353},
{C_text("f_17354:support_2escm"),(void*)f_17354},
{C_text("f_17364:support_2escm"),(void*)f_17364},
{C_text("f_17370:support_2escm"),(void*)f_17370},
{C_text("f_17382:support_2escm"),(void*)f_17382},
{C_text("f_17385:support_2escm"),(void*)f_17385},
{C_text("f_17388:support_2escm"),(void*)f_17388},
{C_text("f_17393:support_2escm"),(void*)f_17393},
{C_text("f_17406:support_2escm"),(void*)f_17406},
{C_text("f_17409:support_2escm"),(void*)f_17409},
{C_text("f_17426:support_2escm"),(void*)f_17426},
{C_text("f_17436:support_2escm"),(void*)f_17436},
{C_text("f_17449:support_2escm"),(void*)f_17449},
{C_text("f_17453:support_2escm"),(void*)f_17453},
{C_text("f_17471:support_2escm"),(void*)f_17471},
{C_text("f_17475:support_2escm"),(void*)f_17475},
{C_text("f_17492:support_2escm"),(void*)f_17492},
{C_text("f_17498:support_2escm"),(void*)f_17498},
{C_text("f_17508:support_2escm"),(void*)f_17508},
{C_text("f_17511:support_2escm"),(void*)f_17511},
{C_text("f_17527:support_2escm"),(void*)f_17527},
{C_text("f_17532:support_2escm"),(void*)f_17532},
{C_text("f_17536:support_2escm"),(void*)f_17536},
{C_text("f_17553:support_2escm"),(void*)f_17553},
{C_text("f_17564:support_2escm"),(void*)f_17564},
{C_text("f_17576:support_2escm"),(void*)f_17576},
{C_text("f_17579:support_2escm"),(void*)f_17579},
{C_text("f_17587:support_2escm"),(void*)f_17587},
{C_text("f_17592:support_2escm"),(void*)f_17592},
{C_text("f_17605:support_2escm"),(void*)f_17605},
{C_text("f_17616:support_2escm"),(void*)f_17616},
{C_text("f_17638:support_2escm"),(void*)f_17638},
{C_text("f_17640:support_2escm"),(void*)f_17640},
{C_text("f_17660:support_2escm"),(void*)f_17660},
{C_text("f_17680:support_2escm"),(void*)f_17680},
{C_text("f_17688:support_2escm"),(void*)f_17688},
{C_text("f_17697:support_2escm"),(void*)f_17697},
{C_text("f_17702:support_2escm"),(void*)f_17702},
{C_text("f_17706:support_2escm"),(void*)f_17706},
{C_text("f_17727:support_2escm"),(void*)f_17727},
{C_text("f_17742:support_2escm"),(void*)f_17742},
{C_text("f_17748:support_2escm"),(void*)f_17748},
{C_text("f_17759:support_2escm"),(void*)f_17759},
{C_text("f_17770:support_2escm"),(void*)f_17770},
{C_text("f_17781:support_2escm"),(void*)f_17781},
{C_text("f_17785:support_2escm"),(void*)f_17785},
{C_text("f_17791:support_2escm"),(void*)f_17791},
{C_text("f_17803:support_2escm"),(void*)f_17803},
{C_text("f_17807:support_2escm"),(void*)f_17807},
{C_text("f_17819:support_2escm"),(void*)f_17819},
{C_text("f_17827:support_2escm"),(void*)f_17827},
{C_text("f_17837:support_2escm"),(void*)f_17837},
{C_text("f_17852:support_2escm"),(void*)f_17852},
{C_text("f_17858:support_2escm"),(void*)f_17858},
{C_text("f_17861:support_2escm"),(void*)f_17861},
{C_text("f_17864:support_2escm"),(void*)f_17864},
{C_text("f_17867:support_2escm"),(void*)f_17867},
{C_text("f_17870:support_2escm"),(void*)f_17870},
{C_text("f_17874:support_2escm"),(void*)f_17874},
{C_text("f_17876:support_2escm"),(void*)f_17876},
{C_text("f_17883:support_2escm"),(void*)f_17883},
{C_text("f_17890:support_2escm"),(void*)f_17890},
{C_text("f_17901:support_2escm"),(void*)f_17901},
{C_text("f_17905:support_2escm"),(void*)f_17905},
{C_text("f_17908:support_2escm"),(void*)f_17908},
{C_text("f_17913:support_2escm"),(void*)f_17913},
{C_text("f_17919:support_2escm"),(void*)f_17919},
{C_text("f_17926:support_2escm"),(void*)f_17926},
{C_text("f_17929:support_2escm"),(void*)f_17929},
{C_text("f_17932:support_2escm"),(void*)f_17932},
{C_text("f_17935:support_2escm"),(void*)f_17935},
{C_text("f_5288:support_2escm"),(void*)f_5288},
{C_text("f_5291:support_2escm"),(void*)f_5291},
{C_text("f_5294:support_2escm"),(void*)f_5294},
{C_text("f_5297:support_2escm"),(void*)f_5297},
{C_text("f_5300:support_2escm"),(void*)f_5300},
{C_text("f_5303:support_2escm"),(void*)f_5303},
{C_text("f_5306:support_2escm"),(void*)f_5306},
{C_text("f_5309:support_2escm"),(void*)f_5309},
{C_text("f_5312:support_2escm"),(void*)f_5312},
{C_text("f_5422:support_2escm"),(void*)f_5422},
{C_text("f_5440:support_2escm"),(void*)f_5440},
{C_text("f_5480:support_2escm"),(void*)f_5480},
{C_text("f_5494:support_2escm"),(void*)f_5494},
{C_text("f_5683:support_2escm"),(void*)f_5683},
{C_text("f_5689:support_2escm"),(void*)f_5689},
{C_text("f_5711:support_2escm"),(void*)f_5711},
{C_text("f_5717:support_2escm"),(void*)f_5717},
{C_text("f_5723:support_2escm"),(void*)f_5723},
{C_text("f_5733:support_2escm"),(void*)f_5733},
{C_text("f_5747:support_2escm"),(void*)f_5747},
{C_text("f_5753:support_2escm"),(void*)f_5753},
{C_text("f_5767:support_2escm"),(void*)f_5767},
{C_text("f_5976:support_2escm"),(void*)f_5976},
{C_text("f_5984:support_2escm"),(void*)f_5984},
{C_text("f_5992:support_2escm"),(void*)f_5992},
{C_text("f_6010:support_2escm"),(void*)f_6010},
{C_text("f_6049:support_2escm"),(void*)f_6049},
{C_text("f_6083:support_2escm"),(void*)f_6083},
{C_text("f_6089:support_2escm"),(void*)f_6089},
{C_text("f_6142:support_2escm"),(void*)f_6142},
{C_text("f_6148:support_2escm"),(void*)f_6148},
{C_text("f_6331:support_2escm"),(void*)f_6331},
{C_text("f_6349:support_2escm"),(void*)f_6349},
{C_text("f_6444:support_2escm"),(void*)f_6444},
{C_text("f_6457:support_2escm"),(void*)f_6457},
{C_text("f_6595:support_2escm"),(void*)f_6595},
{C_text("f_6599:support_2escm"),(void*)f_6599},
{C_text("f_6613:support_2escm"),(void*)f_6613},
{C_text("f_6624:support_2escm"),(void*)f_6624},
{C_text("f_6627:support_2escm"),(void*)f_6627},
{C_text("f_6642:support_2escm"),(void*)f_6642},
{C_text("f_6648:support_2escm"),(void*)f_6648},
{C_text("f_6651:support_2escm"),(void*)f_6651},
{C_text("f_6657:support_2escm"),(void*)f_6657},
{C_text("f_6661:support_2escm"),(void*)f_6661},
{C_text("f_6664:support_2escm"),(void*)f_6664},
{C_text("f_6673:support_2escm"),(void*)f_6673},
{C_text("f_6681:support_2escm"),(void*)f_6681},
{C_text("f_6688:support_2escm"),(void*)f_6688},
{C_text("f_6693:support_2escm"),(void*)f_6693},
{C_text("f_6703:support_2escm"),(void*)f_6703},
{C_text("f_6716:support_2escm"),(void*)f_6716},
{C_text("f_6723:support_2escm"),(void*)f_6723},
{C_text("f_6726:support_2escm"),(void*)f_6726},
{C_text("f_6735:support_2escm"),(void*)f_6735},
{C_text("f_6738:support_2escm"),(void*)f_6738},
{C_text("f_6741:support_2escm"),(void*)f_6741},
{C_text("f_6744:support_2escm"),(void*)f_6744},
{C_text("f_6747:support_2escm"),(void*)f_6747},
{C_text("f_6750:support_2escm"),(void*)f_6750},
{C_text("f_6756:support_2escm"),(void*)f_6756},
{C_text("f_6759:support_2escm"),(void*)f_6759},
{C_text("f_6766:support_2escm"),(void*)f_6766},
{C_text("f_6768:support_2escm"),(void*)f_6768},
{C_text("f_6771:support_2escm"),(void*)f_6771},
{C_text("f_6773:support_2escm"),(void*)f_6773},
{C_text("f_6780:support_2escm"),(void*)f_6780},
{C_text("f_6783:support_2escm"),(void*)f_6783},
{C_text("f_6786:support_2escm"),(void*)f_6786},
{C_text("f_6800:support_2escm"),(void*)f_6800},
{C_text("f_6805:support_2escm"),(void*)f_6805},
{C_text("f_6815:support_2escm"),(void*)f_6815},
{C_text("f_6832:support_2escm"),(void*)f_6832},
{C_text("f_6835:support_2escm"),(void*)f_6835},
{C_text("f_6838:support_2escm"),(void*)f_6838},
{C_text("f_6841:support_2escm"),(void*)f_6841},
{C_text("f_6847:support_2escm"),(void*)f_6847},
{C_text("f_6856:support_2escm"),(void*)f_6856},
{C_text("f_6863:support_2escm"),(void*)f_6863},
{C_text("f_6865:support_2escm"),(void*)f_6865},
{C_text("f_6869:support_2escm"),(void*)f_6869},
{C_text("f_6872:support_2escm"),(void*)f_6872},
{C_text("f_6879:support_2escm"),(void*)f_6879},
{C_text("f_6881:support_2escm"),(void*)f_6881},
{C_text("f_6885:support_2escm"),(void*)f_6885},
{C_text("f_6888:support_2escm"),(void*)f_6888},
{C_text("f_6889:support_2escm"),(void*)f_6889},
{C_text("f_6899:support_2escm"),(void*)f_6899},
{C_text("f_6902:support_2escm"),(void*)f_6902},
{C_text("f_6907:support_2escm"),(void*)f_6907},
{C_text("f_6917:support_2escm"),(void*)f_6917},
{C_text("f_6934:support_2escm"),(void*)f_6934},
{C_text("f_6937:support_2escm"),(void*)f_6937},
{C_text("f_6940:support_2escm"),(void*)f_6940},
{C_text("f_6943:support_2escm"),(void*)f_6943},
{C_text("f_6946:support_2escm"),(void*)f_6946},
{C_text("f_6955:support_2escm"),(void*)f_6955},
{C_text("f_6958:support_2escm"),(void*)f_6958},
{C_text("f_6961:support_2escm"),(void*)f_6961},
{C_text("f_6978:support_2escm"),(void*)f_6978},
{C_text("f_7032:support_2escm"),(void*)f_7032},
{C_text("f_7038:support_2escm"),(void*)f_7038},
{C_text("f_7073:support_2escm"),(void*)f_7073},
{C_text("f_7079:support_2escm"),(void*)f_7079},
{C_text("f_7101:support_2escm"),(void*)f_7101},
{C_text("f_7110:support_2escm"),(void*)f_7110},
{C_text("f_7122:support_2escm"),(void*)f_7122},
{C_text("f_7126:support_2escm"),(void*)f_7126},
{C_text("f_7128:support_2escm"),(void*)f_7128},
{C_text("f_7150:support_2escm"),(void*)f_7150},
{C_text("f_7157:support_2escm"),(void*)f_7157},
{C_text("f_7161:support_2escm"),(void*)f_7161},
{C_text("f_7165:support_2escm"),(void*)f_7165},
{C_text("f_7171:support_2escm"),(void*)f_7171},
{C_text("f_7193:support_2escm"),(void*)f_7193},
{C_text("f_7209:support_2escm"),(void*)f_7209},
{C_text("f_7213:support_2escm"),(void*)f_7213},
{C_text("f_7234:support_2escm"),(void*)f_7234},
{C_text("f_7257:support_2escm"),(void*)f_7257},
{C_text("f_7259:support_2escm"),(void*)f_7259},
{C_text("f_7266:support_2escm"),(void*)f_7266},
{C_text("f_7273:support_2escm"),(void*)f_7273},
{C_text("f_7286:support_2escm"),(void*)f_7286},
{C_text("f_7317:support_2escm"),(void*)f_7317},
{C_text("f_7329:support_2escm"),(void*)f_7329},
{C_text("f_7343:support_2escm"),(void*)f_7343},
{C_text("f_7345:support_2escm"),(void*)f_7345},
{C_text("f_7371:support_2escm"),(void*)f_7371},
{C_text("f_7385:support_2escm"),(void*)f_7385},
{C_text("f_7391:support_2escm"),(void*)f_7391},
{C_text("f_7406:support_2escm"),(void*)f_7406},
{C_text("f_7422:support_2escm"),(void*)f_7422},
{C_text("f_7430:support_2escm"),(void*)f_7430},
{C_text("f_7434:support_2escm"),(void*)f_7434},
{C_text("f_7436:support_2escm"),(void*)f_7436},
{C_text("f_7447:support_2escm"),(void*)f_7447},
{C_text("f_7449:support_2escm"),(void*)f_7449},
{C_text("f_7466:support_2escm"),(void*)f_7466},
{C_text("f_7480:support_2escm"),(void*)f_7480},
{C_text("f_7514:support_2escm"),(void*)f_7514},
{C_text("f_7526:support_2escm"),(void*)f_7526},
{C_text("f_7542:support_2escm"),(void*)f_7542},
{C_text("f_7572:support_2escm"),(void*)f_7572},
{C_text("f_7576:support_2escm"),(void*)f_7576},
{C_text("f_7616:support_2escm"),(void*)f_7616},
{C_text("f_7618:support_2escm"),(void*)f_7618},
{C_text("f_7634:support_2escm"),(void*)f_7634},
{C_text("f_7640:support_2escm"),(void*)f_7640},
{C_text("f_7655:support_2escm"),(void*)f_7655},
{C_text("f_7672:support_2escm"),(void*)f_7672},
{C_text("f_7674:support_2escm"),(void*)f_7674},
{C_text("f_7680:support_2escm"),(void*)f_7680},
{C_text("f_7704:support_2escm"),(void*)f_7704},
{C_text("f_7720:support_2escm"),(void*)f_7720},
{C_text("f_7730:support_2escm"),(void*)f_7730},
{C_text("f_7735:support_2escm"),(void*)f_7735},
{C_text("f_7749:support_2escm"),(void*)f_7749},
{C_text("f_7752:support_2escm"),(void*)f_7752},
{C_text("f_7753:support_2escm"),(void*)f_7753},
{C_text("f_7757:support_2escm"),(void*)f_7757},
{C_text("f_7762:support_2escm"),(void*)f_7762},
{C_text("f_7768:support_2escm"),(void*)f_7768},
{C_text("f_7774:support_2escm"),(void*)f_7774},
{C_text("f_7782:support_2escm"),(void*)f_7782},
{C_text("f_7785:support_2escm"),(void*)f_7785},
{C_text("f_7793:support_2escm"),(void*)f_7793},
{C_text("f_7795:support_2escm"),(void*)f_7795},
{C_text("f_7799:support_2escm"),(void*)f_7799},
{C_text("f_7821:support_2escm"),(void*)f_7821},
{C_text("f_7827:support_2escm"),(void*)f_7827},
{C_text("f_7831:support_2escm"),(void*)f_7831},
{C_text("f_7844:support_2escm"),(void*)f_7844},
{C_text("f_7852:support_2escm"),(void*)f_7852},
{C_text("f_7858:support_2escm"),(void*)f_7858},
{C_text("f_7869:support_2escm"),(void*)f_7869},
{C_text("f_7871:support_2escm"),(void*)f_7871},
{C_text("f_7874:support_2escm"),(void*)f_7874},
{C_text("f_7880:support_2escm"),(void*)f_7880},
{C_text("f_7919:support_2escm"),(void*)f_7919},
{C_text("f_7924:support_2escm"),(void*)f_7924},
{C_text("f_7928:support_2escm"),(void*)f_7928},
{C_text("f_7932:support_2escm"),(void*)f_7932},
{C_text("f_7983:support_2escm"),(void*)f_7983},
{C_text("f_8020:support_2escm"),(void*)f_8020},
{C_text("f_8022:support_2escm"),(void*)f_8022},
{C_text("f_8072:support_2escm"),(void*)f_8072},
{C_text("f_8076:support_2escm"),(void*)f_8076},
{C_text("f_8090:support_2escm"),(void*)f_8090},
{C_text("f_8094:support_2escm"),(void*)f_8094},
{C_text("f_8102:support_2escm"),(void*)f_8102},
{C_text("f_8108:support_2escm"),(void*)f_8108},
{C_text("f_8112:support_2escm"),(void*)f_8112},
{C_text("f_8154:support_2escm"),(void*)f_8154},
{C_text("f_8158:support_2escm"),(void*)f_8158},
{C_text("f_8206:support_2escm"),(void*)f_8206},
{C_text("f_8210:support_2escm"),(void*)f_8210},
{C_text("f_8215:support_2escm"),(void*)f_8215},
{C_text("f_8225:support_2escm"),(void*)f_8225},
{C_text("f_8232:support_2escm"),(void*)f_8232},
{C_text("f_8239:support_2escm"),(void*)f_8239},
{C_text("f_8266:support_2escm"),(void*)f_8266},
{C_text("f_8272:support_2escm"),(void*)f_8272},
{C_text("f_8282:support_2escm"),(void*)f_8282},
{C_text("f_8285:support_2escm"),(void*)f_8285},
{C_text("f_8288:support_2escm"),(void*)f_8288},
{C_text("f_8301:support_2escm"),(void*)f_8301},
{C_text("f_8303:support_2escm"),(void*)f_8303},
{C_text("f_8338:support_2escm"),(void*)f_8338},
{C_text("f_8344:support_2escm"),(void*)f_8344},
{C_text("f_8350:support_2escm"),(void*)f_8350},
{C_text("f_8359:support_2escm"),(void*)f_8359},
{C_text("f_8368:support_2escm"),(void*)f_8368},
{C_text("f_8377:support_2escm"),(void*)f_8377},
{C_text("f_8386:support_2escm"),(void*)f_8386},
{C_text("f_8395:support_2escm"),(void*)f_8395},
{C_text("f_8405:support_2escm"),(void*)f_8405},
{C_text("f_8407:support_2escm"),(void*)f_8407},
{C_text("f_8413:support_2escm"),(void*)f_8413},
{C_text("f_8428:support_2escm"),(void*)f_8428},
{C_text("f_8443:support_2escm"),(void*)f_8443},
{C_text("f_8446:support_2escm"),(void*)f_8446},
{C_text("f_8513:support_2escm"),(void*)f_8513},
{C_text("f_8515:support_2escm"),(void*)f_8515},
{C_text("f_8540:support_2escm"),(void*)f_8540},
{C_text("f_8563:support_2escm"),(void*)f_8563},
{C_text("f_8566:support_2escm"),(void*)f_8566},
{C_text("f_8569:support_2escm"),(void*)f_8569},
{C_text("f_8576:support_2escm"),(void*)f_8576},
{C_text("f_8623:support_2escm"),(void*)f_8623},
{C_text("f_8627:support_2escm"),(void*)f_8627},
{C_text("f_8632:support_2escm"),(void*)f_8632},
{C_text("f_8649:support_2escm"),(void*)f_8649},
{C_text("f_8657:support_2escm"),(void*)f_8657},
{C_text("f_8659:support_2escm"),(void*)f_8659},
{C_text("f_8684:support_2escm"),(void*)f_8684},
{C_text("f_8720:support_2escm"),(void*)f_8720},
{C_text("f_8754:support_2escm"),(void*)f_8754},
{C_text("f_8785:support_2escm"),(void*)f_8785},
{C_text("f_8808:support_2escm"),(void*)f_8808},
{C_text("f_8829:support_2escm"),(void*)f_8829},
{C_text("f_8851:support_2escm"),(void*)f_8851},
{C_text("f_8859:support_2escm"),(void*)f_8859},
{C_text("f_8863:support_2escm"),(void*)f_8863},
{C_text("f_8871:support_2escm"),(void*)f_8871},
{C_text("f_8892:support_2escm"),(void*)f_8892},
{C_text("f_8896:support_2escm"),(void*)f_8896},
{C_text("f_8908:support_2escm"),(void*)f_8908},
{C_text("f_8935:support_2escm"),(void*)f_8935},
{C_text("f_8947:support_2escm"),(void*)f_8947},
{C_text("f_8949:support_2escm"),(void*)f_8949},
{C_text("f_8974:support_2escm"),(void*)f_8974},
{C_text("f_9008:support_2escm"),(void*)f_9008},
{C_text("f_9034:support_2escm"),(void*)f_9034},
{C_text("f_9036:support_2escm"),(void*)f_9036},
{C_text("f_9061:support_2escm"),(void*)f_9061},
{C_text("f_9145:support_2escm"),(void*)f_9145},
{C_text("f_9147:support_2escm"),(void*)f_9147},
{C_text("f_9172:support_2escm"),(void*)f_9172},
{C_text("f_9212:support_2escm"),(void*)f_9212},
{C_text("f_9253:support_2escm"),(void*)f_9253},
{C_text("f_9282:support_2escm"),(void*)f_9282},
{C_text("f_9284:support_2escm"),(void*)f_9284},
{C_text("f_9309:support_2escm"),(void*)f_9309},
{C_text("f_9345:support_2escm"),(void*)f_9345},
{C_text("f_9347:support_2escm"),(void*)f_9347},
{C_text("f_9372:support_2escm"),(void*)f_9372},
{C_text("f_9384:support_2escm"),(void*)f_9384},
{C_text("f_9390:support_2escm"),(void*)f_9390},
{C_text("f_9413:support_2escm"),(void*)f_9413},
{C_text("f_9415:support_2escm"),(void*)f_9415},
{C_text("f_9440:support_2escm"),(void*)f_9440},
{C_text("f_9451:support_2escm"),(void*)f_9451},
{C_text("f_9455:support_2escm"),(void*)f_9455},
{C_text("f_9458:support_2escm"),(void*)f_9458},
{C_text("f_9465:support_2escm"),(void*)f_9465},
{C_text("f_9479:support_2escm"),(void*)f_9479},
{C_text("f_9585:support_2escm"),(void*)f_9585},
{C_text("f_9587:support_2escm"),(void*)f_9587},
{C_text("f_9612:support_2escm"),(void*)f_9612},
{C_text("f_9630:support_2escm"),(void*)f_9630},
{C_text("f_9633:support_2escm"),(void*)f_9633},
{C_text("f_9639:support_2escm"),(void*)f_9639},
{C_text("f_9645:support_2escm"),(void*)f_9645},
{C_text("f_9679:support_2escm"),(void*)f_9679},
{C_text("f_9692:support_2escm"),(void*)f_9692},
{C_text("f_9694:support_2escm"),(void*)f_9694},
{C_text("f_9719:support_2escm"),(void*)f_9719},
{C_text("f_9749:support_2escm"),(void*)f_9749},
{C_text("f_9751:support_2escm"),(void*)f_9751},
{C_text("f_9776:support_2escm"),(void*)f_9776},
{C_text("f_9849:support_2escm"),(void*)f_9849},
{C_text("f_9852:support_2escm"),(void*)f_9852},
{C_text("f_9861:support_2escm"),(void*)f_9861},
{C_text("f_9865:support_2escm"),(void*)f_9865},
{C_text("f_9869:support_2escm"),(void*)f_9869},
{C_text("f_9871:support_2escm"),(void*)f_9871},
{C_text("f_9919:support_2escm"),(void*)f_9919},
{C_text("f_9944:support_2escm"),(void*)f_9944},
{C_text("f_9972:support_2escm"),(void*)f_9972},
{C_text("f_9996:support_2escm"),(void*)f_9996},
{C_text("toplevel:support_2escm"),(void*)C_support_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
o|hiding unexported module binding: chicken.compiler.support#partition 
o|hiding unexported module binding: chicken.compiler.support#span 
o|hiding unexported module binding: chicken.compiler.support#take 
o|hiding unexported module binding: chicken.compiler.support#drop 
o|hiding unexported module binding: chicken.compiler.support#split-at 
o|hiding unexported module binding: chicken.compiler.support#append-map 
o|hiding unexported module binding: chicken.compiler.support#every 
o|hiding unexported module binding: chicken.compiler.support#any 
o|hiding unexported module binding: chicken.compiler.support#cons* 
o|hiding unexported module binding: chicken.compiler.support#concatenate 
o|hiding unexported module binding: chicken.compiler.support#delete 
o|hiding unexported module binding: chicken.compiler.support#first 
o|hiding unexported module binding: chicken.compiler.support#second 
o|hiding unexported module binding: chicken.compiler.support#third 
o|hiding unexported module binding: chicken.compiler.support#fourth 
o|hiding unexported module binding: chicken.compiler.support#fifth 
o|hiding unexported module binding: chicken.compiler.support#delete-duplicates 
o|hiding unexported module binding: chicken.compiler.support#alist-cons 
o|hiding unexported module binding: chicken.compiler.support#filter 
o|hiding unexported module binding: chicken.compiler.support#filter-map 
o|hiding unexported module binding: chicken.compiler.support#remove 
o|hiding unexported module binding: chicken.compiler.support#unzip1 
o|hiding unexported module binding: chicken.compiler.support#last 
o|hiding unexported module binding: chicken.compiler.support#list-index 
o|hiding unexported module binding: chicken.compiler.support#lset-adjoin/eq? 
o|hiding unexported module binding: chicken.compiler.support#lset-difference/eq? 
o|hiding unexported module binding: chicken.compiler.support#lset-union/eq? 
o|hiding unexported module binding: chicken.compiler.support#lset-intersection/eq? 
o|hiding unexported module binding: chicken.compiler.support#list-tabulate 
o|hiding unexported module binding: chicken.compiler.support#lset<=/eq? 
o|hiding unexported module binding: chicken.compiler.support#lset=/eq? 
o|hiding unexported module binding: chicken.compiler.support#length+ 
o|hiding unexported module binding: chicken.compiler.support#find 
o|hiding unexported module binding: chicken.compiler.support#find-tail 
o|hiding unexported module binding: chicken.compiler.support#iota 
o|hiding unexported module binding: chicken.compiler.support#make-list 
o|hiding unexported module binding: chicken.compiler.support#posq 
o|hiding unexported module binding: chicken.compiler.support#posv 
o|hiding unexported module binding: chicken.compiler.support#constant659 
o|hiding unexported module binding: chicken.compiler.support#+logged-debugging-modes+ 
o|hiding unexported module binding: chicken.compiler.support#test-debugging-mode 
o|hiding unexported module binding: chicken.compiler.support#map-llist 
o|hiding unexported module binding: chicken.compiler.support#follow-without-loop 
o|hiding unexported module binding: chicken.compiler.support#sort-symbols 
o|hiding unexported module binding: chicken.compiler.support#profile-info-vector-name 
o|hiding unexported module binding: chicken.compiler.support#profile-lambda-list 
o|hiding unexported module binding: chicken.compiler.support#profile-lambda-index 
o|hiding unexported module binding: chicken.compiler.support#node 
o|hiding unexported module binding: chicken.compiler.support#copy-node-tree-and-rename 
o|hiding unexported module binding: chicken.compiler.support#replace-rest-ops-in-known-call! 
o|hiding unexported module binding: chicken.compiler.support#node->sexpr 
o|hiding unexported module binding: chicken.compiler.support#sexpr->node 
o|hiding unexported module binding: chicken.compiler.support#foreign-callback-stub 
o|hiding unexported module binding: chicken.compiler.support#foreign-type-table 
o|hiding unexported module binding: chicken.compiler.support#foreign-type-result-converter 
o|hiding unexported module binding: chicken.compiler.support#foreign-type-argument-converter 
o|hiding unexported module binding: chicken.compiler.support#block-variable-literal 
o|hiding unexported module binding: chicken.compiler.support#real-name-table 
o|hiding unexported module binding: chicken.compiler.support#real-name-max-depth 
o|hiding unexported module binding: chicken.compiler.support#encodeable-literal? 
o|hiding unexported module binding: chicken.compiler.support#scan-sharp-greater-string 
o|hiding unexported module binding: chicken.compiler.support#unhide-variable 
S|applied compiler syntax:
S|  chicken.format#sprintf		4
S|  chicken.format#fprintf		5
S|  chicken.format#printf		6
S|  scheme#for-each		12
S|  chicken.base#foldl		3
S|  scheme#map		34
S|  chicken.base#foldr		3
o|eliminated procedure checks: 452 
o|specializations:
o|  1 (scheme#eqv? (or eof null fixnum char boolean symbol keyword) *)
o|  1 (scheme#+ fixnum fixnum)
o|  1 (scheme#number->string * *)
o|  1 (chicken.base#exact-integer? *)
o|  2 (chicken.bitwise#integer-length *)
o|  1 (scheme#length list)
o|  1 (scheme#- fixnum fixnum)
o|  1 (scheme#> integer integer)
o|  1 (chicken.base#sub1 fixnum)
o|  4 (scheme#= fixnum fixnum)
o|  2 (scheme#assq * (list-of pair))
o|  1 (scheme#* fixnum fixnum)
o|  1 (scheme#positive? *)
o|  4 (scheme#cddr (pair * pair))
o|  1 (scheme#caddr (pair * (pair * pair)))
o|  1 (scheme#integer? *)
o|  334 (scheme#eqv? * (or eof null fixnum char boolean symbol keyword))
o|  2 (##sys#call-with-values (procedure () *) *)
o|  4 (chicken.base#add1 *)
o|  1 (scheme#cadr (pair * pair))
o|  2 (scheme#current-input-port)
o|  4 (scheme#char=? char char)
o|  1 (scheme#number->string fixnum fixnum)
o|  3 (scheme#memq * list)
o|  1 (scheme#>= fixnum fixnum)
o|  3 (scheme#< fixnum fixnum)
o|  2 (chicken.base#sub1 *)
o|  3 (scheme#zero? *)
o|  2 (chicken.base#current-error-port)
o|  15 (##sys#check-output-port * * *)
o|  1 (scheme#eqv? * *)
o|  9 (##sys#check-list (or pair list) *)
o|  55 (scheme#cdr pair)
o|  34 (scheme#car pair)
(o e)|safe calls: 1876 
(o e)|assignments to immediate values: 5 
o|safe globals: (chicken.compiler.support#bomb chicken.compiler.support#debugging-chicken chicken.compiler.support#compiler-cleanup-hook chicken.compiler.support#unsafe chicken.compiler.support#number-type chicken.compiler.support#constant659 chicken.compiler.support#posv chicken.compiler.support#posq chicken.compiler.support#make-list chicken.compiler.support#iota chicken.compiler.support#find-tail chicken.compiler.support#find chicken.compiler.support#length+ chicken.compiler.support#lset=/eq? chicken.compiler.support#lset<=/eq? chicken.compiler.support#list-tabulate chicken.compiler.support#lset-intersection/eq? chicken.compiler.support#lset-union/eq? chicken.compiler.support#lset-difference/eq? chicken.compiler.support#lset-adjoin/eq? chicken.compiler.support#list-index chicken.compiler.support#last chicken.compiler.support#unzip1 chicken.compiler.support#remove chicken.compiler.support#filter-map chicken.compiler.support#filter chicken.compiler.support#alist-cons chicken.compiler.support#delete-duplicates chicken.compiler.support#fifth chicken.compiler.support#fourth chicken.compiler.support#third chicken.compiler.support#second chicken.compiler.support#first chicken.compiler.support#delete chicken.compiler.support#concatenate chicken.compiler.support#cons* chicken.compiler.support#any chicken.compiler.support#every chicken.compiler.support#append-map chicken.compiler.support#split-at chicken.compiler.support#drop chicken.compiler.support#take chicken.compiler.support#span chicken.compiler.support#partition) 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#partition 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#span 
o|inlining procedure: k5424 
o|inlining procedure: k5424 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#drop 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#append-map 
o|inlining procedure: k5694 
o|inlining procedure: k5694 
o|inlining procedure: k5725 
o|inlining procedure: k5725 
o|merged explicitly consed rest parameter: xs320 
o|inlining procedure: k5755 
o|inlining procedure: k5755 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#concatenate 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#delete-duplicates 
o|inlining procedure: k5942 
o|inlining procedure: k5942 
o|inlining procedure: k5934 
o|inlining procedure: k5934 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#remove 
o|inlining procedure: k6091 
o|inlining procedure: k6091 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#list-index 
o|merged explicitly consed rest parameter: vals460 
o|inlining procedure: k6150 
o|inlining procedure: k6150 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#lset-difference/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#lset-union/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#lset-intersection/eq? 
o|inlining procedure: k6333 
o|inlining procedure: k6333 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#lset<=/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#lset=/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#length+ 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#find-tail 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#iota 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#make-list 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#posq 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#posv 
o|inlining procedure: k6601 
o|inlining procedure: k6601 
o|inlining procedure: k6629 
o|inlining procedure: k6629 
o|inlining procedure: k6662 
o|inlining procedure: k6695 
o|contracted procedure: "(support.scm:137) g686693" 
o|propagated global variable: out696699 ##sys#standard-output 
o|substituted constant variable: a6677 
o|substituted constant variable: a6678 
o|inlining procedure: k6695 
o|inlining procedure: k6662 
o|propagated global variable: out711714 chicken.compiler.support#collected-debugging-output 
o|substituted constant variable: a6719 
o|substituted constant variable: a6720 
o|propagated global variable: out711714 chicken.compiler.support#collected-debugging-output 
o|inlining procedure: k6730 
o|inlining procedure: k6730 
o|propagated global variable: out747750 chicken.compiler.support#collected-debugging-output 
o|substituted constant variable: a6776 
o|substituted constant variable: a6777 
o|inlining procedure: k6791 
o|inlining procedure: k6791 
o|inlining procedure: k6807 
o|inlining procedure: k6807 
o|inlining procedure: k6827 
o|inlining procedure: k6827 
o|inlining procedure: k6909 
o|inlining procedure: k6909 
o|substituted constant variable: a6930 
o|substituted constant variable: a6931 
o|substituted constant variable: a6951 
o|substituted constant variable: a6952 
o|contracted procedure: "(support.scm:198) thread-id827" 
o|propagated global variable: t828 ##sys#current-thread 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#map-llist 
o|inlining procedure: k7040 
o|inlining procedure: k7040 
o|inlining procedure: k7055 
o|inlining procedure: k7055 
o|inlining procedure: k7081 
o|inlining procedure: k7081 
o|inlining procedure: k7130 
o|inlining procedure: k7130 
o|substituted constant variable: a7173 
o|inlining procedure: k7177 
o|inlining procedure: k7177 
o|substituted constant variable: a7184 
o|substituted constant variable: a7186 
o|inlining procedure: k7199 
o|inlining procedure: k7199 
o|substituted constant variable: a7203 
o|substituted constant variable: a7205 
o|substituted constant variable: a7207 
o|inlining procedure: k7214 
o|inlining procedure: k7239 
o|inlining procedure: k7239 
o|substituted constant variable: a7248 
o|substituted constant variable: a7252 
o|inlining procedure: k7214 
o|inlining procedure: k7275 
o|propagated global variable: r727618019 ##sys#standard-input 
o|inlining procedure: k7275 
o|inlining procedure: k7290 
o|inlining procedure: k7290 
o|inlining procedure: k7319 
o|inlining procedure: k7319 
o|inlining procedure: k7331 
o|inlining procedure: k7331 
o|inlining procedure: k7351 
o|inlining procedure: k7351 
o|inlining procedure: k7393 
o|inlining procedure: k7393 
o|inlining procedure: k7451 
o|inlining procedure: k7451 
o|inlining procedure: k7485 
o|inlining procedure: k7485 
o|inlining procedure: k7497 
o|inlining procedure: k7497 
o|inlining procedure: k7509 
o|inlining procedure: k7509 
o|inlining procedure: k7521 
o|inlining procedure: k7521 
o|inlining procedure: k7530 
o|inlining procedure: k7530 
o|inlining procedure: k7547 
o|inlining procedure: k7547 
o|inlining procedure: k7559 
o|inlining procedure: k7559 
o|inlining procedure: k7577 
o|inlining procedure: k7577 
o|inlining procedure: k7589 
o|inlining procedure: k7589 
o|inlining procedure: k7601 
o|inlining procedure: k7601 
o|inlining procedure: k7623 
o|inlining procedure: k7623 
o|inlining procedure: k7635 
o|inlining procedure: k7635 
o|inlining procedure: k7644 
o|inlining procedure: k7644 
o|inlining procedure: k7682 
o|inlining procedure: k7682 
o|inlining procedure: k7695 
o|inlining procedure: k7695 
o|inlining procedure: k7736 
o|inlining procedure: k7736 
o|inlining procedure: k7780 
o|inlining procedure: k7780 
o|inlining procedure: k7800 
o|inlining procedure: k7800 
o|inlining procedure: k7832 
o|inlining procedure: k7832 
o|merged explicitly consed rest parameter: args10651083 
o|consed rest parameter at call site: tmp24906 1 
o|inlining procedure: k7882 
o|inlining procedure: k7882 
o|inlining procedure: k7897 
o|inlining procedure: k7897 
o|inlining procedure: k8024 
o|contracted procedure: "(support.scm:403) g11281137" 
o|inlining procedure: k8024 
o|propagated global variable: g11341138 chicken.compiler.support#profile-lambda-list 
o|inlining procedure: k8077 
o|inlining procedure: k8077 
o|inlining procedure: k8095 
o|contracted procedure: "(support.scm:421) chicken.compiler.support#filter-map" 
o|inlining procedure: k5989 
o|inlining procedure: k5989 
o|inlining procedure: k5978 
o|inlining procedure: k5978 
o|inlining procedure: k8095 
o|inlining procedure: k8113 
o|inlining procedure: k8125 
o|inlining procedure: k8125 
o|inlining procedure: k8113 
o|inlining procedure: k8159 
o|inlining procedure: k8159 
o|inlining procedure: k8211 
o|inlining procedure: k8211 
o|inlining procedure: k8236 
o|inlining procedure: k8236 
o|propagated global variable: out12111214 ##sys#standard-output 
o|substituted constant variable: a8278 
o|substituted constant variable: a8279 
o|inlining procedure: k8274 
o|inlining procedure: k8305 
o|inlining procedure: k8305 
o|propagated global variable: out12111214 ##sys#standard-output 
o|inlining procedure: k8274 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#node 
o|contracted procedure: "(support.scm:481) g13071308" 
o|contracted procedure: "(support.scm:482) g13141315" 
o|inlining procedure: k8448 
o|inlining procedure: k8448 
o|inlining procedure: k8468 
o|inlining procedure: k8468 
o|inlining procedure: k8484 
o|contracted procedure: "(support.scm:492) g13401341" 
o|inlining procedure: k8517 
o|inlining procedure: k8517 
o|inlining procedure: k8484 
o|inlining procedure: k8561 
o|inlining procedure: k8561 
o|inlining procedure: k8580 
o|inlining procedure: k8580 
o|inlining procedure: k8591 
o|contracted procedure: "(support.scm:508) g13771378" 
o|inlining procedure: k8661 
o|inlining procedure: k8661 
o|contracted procedure: "(support.scm:509) chicken.compiler.support#unzip1" 
o|inlining procedure: k6051 
o|contracted procedure: "(mini-srfi-1.scm:143) g423432" 
o|inlining procedure: k6051 
o|inlining procedure: k8591 
o|contracted procedure: "(support.scm:513) g14121413" 
o|inlining procedure: k8729 
o|contracted procedure: "(support.scm:515) g14171418" 
o|inlining procedure: "(support.scm:517) chicken.compiler.support#fourth" 
o|inlining procedure: "(support.scm:516) chicken.compiler.support#third" 
o|inlining procedure: "(support.scm:516) chicken.compiler.support#second" 
o|inlining procedure: k8729 
o|inlining procedure: k8787 
o|contracted procedure: "(support.scm:522) g14301431" 
o|contracted procedure: "(support.scm:526) g14351436" 
o|inlining procedure: k8787 
o|contracted procedure: "(support.scm:528) g14401441" 
o|inlining procedure: k8913 
o|contracted procedure: "(support.scm:537) g14471448" 
o|inlining procedure: k8951 
o|inlining procedure: k8951 
o|inlining procedure: k8983 
o|inlining procedure: k8983 
o|inlining procedure: k8913 
o|contracted procedure: "(support.scm:542) g14851486" 
o|inlining procedure: k9038 
o|inlining procedure: k9038 
o|inlining procedure: k9073 
o|contracted procedure: "(support.scm:544) g15161517" 
o|inlining procedure: k9073 
o|contracted procedure: "(support.scm:546) g15211522" 
o|inlining procedure: k9113 
o|contracted procedure: "(support.scm:548) g15291530" 
o|inlining procedure: k9149 
o|inlining procedure: k9149 
o|inlining procedure: k9113 
o|contracted procedure: "(support.scm:553) g15611562" 
o|contracted procedure: "(support.scm:555) chicken.compiler.support#fifth" 
o|inlining procedure: "(support.scm:555) chicken.compiler.support#fourth" 
o|inlining procedure: "(support.scm:555) chicken.compiler.support#third" 
o|inlining procedure: "(support.scm:552) chicken.compiler.support#second" 
o|inlining procedure: k9245 
o|contracted procedure: "(support.scm:559) g15781579" 
o|inlining procedure: k9286 
o|inlining procedure: k9286 
o|inlining procedure: "(support.scm:559) chicken.compiler.support#second" 
o|inlining procedure: "(support.scm:559) chicken.compiler.support#first" 
o|inlining procedure: k9245 
o|contracted procedure: "(support.scm:561) g16091610" 
o|inlining procedure: k9349 
o|inlining procedure: k9349 
o|contracted procedure: "(support.scm:564) g16421643" 
o|inlining procedure: k9417 
o|inlining procedure: k9417 
o|inlining procedure: k9453 
o|inlining procedure: k9463 
o|inlining procedure: k9463 
o|inlining procedure: k9453 
o|contracted procedure: "(support.scm:566) g16511652" 
o|substituted constant variable: a9483 
o|inlining procedure: k9487 
o|inlining procedure: k9487 
o|inlining procedure: k9499 
o|inlining procedure: k9499 
o|substituted constant variable: a9506 
o|substituted constant variable: a9508 
o|substituted constant variable: a9510 
o|substituted constant variable: a9512 
o|substituted constant variable: a9514 
o|substituted constant variable: a9516 
o|substituted constant variable: a9521 
o|substituted constant variable: a9523 
o|substituted constant variable: a9525 
o|substituted constant variable: a9527 
o|inlining procedure: k9531 
o|inlining procedure: k9531 
o|substituted constant variable: a9538 
o|substituted constant variable: a9540 
o|substituted constant variable: a9542 
o|substituted constant variable: a9544 
o|substituted constant variable: a9546 
o|substituted constant variable: a9548 
o|substituted constant variable: a9553 
o|substituted constant variable: a9555 
o|substituted constant variable: a9557 
o|substituted constant variable: a9559 
o|substituted constant variable: a9564 
o|substituted constant variable: a9566 
o|contracted procedure: "(support.scm:576) g16861687" 
o|inlining procedure: k9589 
o|inlining procedure: k9589 
o|contracted procedure: "(support.scm:488) g13281329" 
o|inlining procedure: k9631 
o|inlining procedure: k9631 
o|inlining procedure: k9671 
o|inlining procedure: k9696 
o|inlining procedure: k9696 
o|inlining procedure: k9671 
o|inlining procedure: k9753 
o|inlining procedure: k9753 
o|inlining procedure: k9784 
o|inlining procedure: k9784 
o|inlining procedure: k9802 
o|inlining procedure: k9802 
o|inlining procedure: k9819 
o|inlining procedure: k9819 
o|inlining procedure: k9831 
o|inlining procedure: k9873 
o|inlining procedure: k9873 
o|inlining procedure: k9921 
o|inlining procedure: k9921 
o|inlining procedure: k9831 
o|inlining procedure: "(support.scm:604) chicken.compiler.support#third" 
o|inlining procedure: "(support.scm:601) chicken.compiler.support#second" 
o|inlining procedure: k9980 
o|inlining procedure: "(support.scm:607) chicken.compiler.support#first" 
o|inlining procedure: "(support.scm:607) chicken.compiler.support#first" 
o|inlining procedure: k9980 
o|inlining procedure: "(support.scm:609) chicken.compiler.support#first" 
o|inlining procedure: k10014 
o|inlining procedure: k10046 
o|inlining procedure: k10046 
o|inlining procedure: "(support.scm:612) chicken.compiler.support#first" 
o|inlining procedure: k10014 
o|inlining procedure: k10122 
o|inlining procedure: k10122 
o|inlining procedure: k10153 
o|consed rest parameter at call site: "(support.scm:622) chicken.compiler.support#cons*" 2 
o|inlining procedure: k10179 
o|inlining procedure: k10179 
o|inlining procedure: k10153 
o|inlining procedure: k10219 
o|inlining procedure: k10235 
o|inlining procedure: k10235 
o|inlining procedure: k10219 
o|consed rest parameter at call site: "(support.scm:630) chicken.compiler.support#cons*" 2 
o|inlining procedure: k10305 
o|inlining procedure: k10305 
o|inlining procedure: k10340 
o|consed rest parameter at call site: "(support.scm:632) chicken.compiler.support#cons*" 2 
o|inlining procedure: k10362 
o|inlining procedure: k10362 
o|inlining procedure: k10340 
o|inlining procedure: k10413 
o|inlining procedure: k10413 
o|substituted constant variable: a10445 
o|inlining procedure: k10449 
o|inlining procedure: k10449 
o|substituted constant variable: a10462 
o|substituted constant variable: a10464 
o|substituted constant variable: a10466 
o|substituted constant variable: a10468 
o|substituted constant variable: a10470 
o|substituted constant variable: a10472 
o|substituted constant variable: a10474 
o|substituted constant variable: a10476 
o|substituted constant variable: a10478 
o|substituted constant variable: a10480 
o|substituted constant variable: a10482 
o|substituted constant variable: a10484 
o|substituted constant variable: a10486 
o|substituted constant variable: a10488 
o|substituted constant variable: a10490 
o|substituted constant variable: a10492 
o|inlining procedure: k10496 
o|inlining procedure: k10496 
o|substituted constant variable: a10503 
o|substituted constant variable: a10505 
o|substituted constant variable: a10507 
o|contracted procedure: "(support.scm:586) g17331734" 
o|contracted procedure: "(support.scm:585) g17301731" 
o|contracted procedure: "(support.scm:584) g17271728" 
o|inlining procedure: k10517 
o|inlining procedure: k10517 
o|contracted procedure: "(support.scm:639) g20422043" 
o|inlining procedure: "(support.scm:641) chicken.compiler.support#second" 
o|inlining procedure: "(support.scm:641) chicken.compiler.support#first" 
o|contracted procedure: "(support.scm:649) chicken.compiler.support#split-at" 
o|inlining procedure: k5482 
o|inlining procedure: k5482 
o|inlining procedure: k10617 
o|inlining procedure: k10632 
o|contracted procedure: "(support.scm:678) g21432144" 
o|inlining procedure: k10661 
o|inlining procedure: k10661 
o|contracted procedure: "(support.scm:682) g21482149" 
o|substituted constant variable: a10683 
o|inlining procedure: k10632 
o|contracted procedure: "(support.scm:687) g21532154" 
o|inlining procedure: k10617 
o|contracted procedure: "(support.scm:691) g21592160" 
o|inlining procedure: k10786 
o|inlining procedure: k10786 
o|contracted procedure: "(support.scm:655) chicken.compiler.support#replace-rest-ops-in-known-call!" 
o|inlining procedure: k11428 
o|inlining procedure: "(support.scm:754) chicken.compiler.support#second" 
o|inlining procedure: "(support.scm:753) chicken.compiler.support#first" 
o|inlining procedure: k11428 
o|inlining procedure: k11469 
o|contracted procedure: "(support.scm:762) g24252426" 
o|inlining procedure: "(support.scm:758) chicken.compiler.support#second" 
o|inlining procedure: k11469 
o|inlining procedure: "(support.scm:757) chicken.compiler.support#first" 
o|inlining procedure: k11534 
o|inlining procedure: k11564 
o|inlining procedure: k11564 
o|contracted procedure: "(support.scm:775) g24372438" 
o|inlining procedure: "(support.scm:770) chicken.compiler.support#second" 
o|inlining procedure: "(support.scm:768) chicken.compiler.support#first" 
o|inlining procedure: k11534 
o|inlining procedure: k11606 
o|inlining procedure: k11606 
o|substituted constant variable: a11627 
o|substituted constant variable: a11629 
o|substituted constant variable: a11631 
o|contracted procedure: "(support.scm:750) g24142415" 
o|contracted procedure: "(support.scm:749) g24112412" 
o|contracted procedure: "(support.scm:748) g24082409" 
o|inlining procedure: k10813 
o|contracted procedure: "(support.scm:654) g20972106" 
o|inlining procedure: k10813 
o|contracted procedure: "(support.scm:652) chicken.compiler.support#copy-node-tree-and-rename" 
o|inlining procedure: k10938 
o|contracted procedure: "(support.scm:705) g22292230" 
o|inlining procedure: k10938 
o|inlining procedure: "(support.scm:710) rename2205" 
o|inlining procedure: "(support.scm:707) chicken.compiler.support#first" 
o|inlining procedure: k10977 
o|contracted procedure: "(support.scm:712) g22362237" 
o|inlining procedure: "(support.scm:714) chicken.compiler.support#first" 
o|inlining procedure: "(support.scm:713) rename2205" 
o|inlining procedure: "(support.scm:713) chicken.compiler.support#first" 
o|inlining procedure: k10977 
o|contracted procedure: "(support.scm:721) g22452246" 
o|inlining procedure: "(support.scm:723) chicken.compiler.support#second" 
o|inlining procedure: "(support.scm:717) chicken.compiler.support#first" 
o|inlining procedure: "(support.scm:716) chicken.compiler.support#first" 
o|inlining procedure: k11064 
o|contracted procedure: "(support.scm:734) g23162317" 
o|inlining procedure: k11130 
o|inlining procedure: k11130 
o|inlining procedure: "(support.scm:738) chicken.compiler.support#fourth" 
o|inlining procedure: k11178 
o|inlining procedure: "(support.scm:737) rename2205" 
o|inlining procedure: k11178 
o|inlining procedure: "(support.scm:736) chicken.compiler.support#second" 
o|inlining procedure: k11197 
o|inlining procedure: k11197 
o|inlining procedure: k11245 
o|inlining procedure: k11245 
o|inlining procedure: "(support.scm:726) chicken.compiler.support#third" 
o|inlining procedure: k11064 
o|contracted procedure: "(support.scm:740) g23562357" 
o|inlining procedure: k11306 
o|inlining procedure: k11306 
o|substituted constant variable: a11338 
o|substituted constant variable: a11340 
o|substituted constant variable: a11342 
o|substituted constant variable: a11344 
o|substituted constant variable: a11346 
o|contracted procedure: "(support.scm:702) g22202221" 
o|contracted procedure: "(support.scm:701) g22172218" 
o|contracted procedure: "(support.scm:700) g22142215" 
o|inlining procedure: k11353 
o|inlining procedure: k11353 
o|inlining procedure: k10859 
o|inlining procedure: k10859 
o|inlining procedure: k11644 
o|inlining procedure: k11644 
o|contracted procedure: "(support.scm:789) g24692470" 
o|contracted procedure: "(support.scm:791) g24802481" 
o|contracted procedure: "(support.scm:790) g24772478" 
o|contracted procedure: "(support.scm:789) g24742475" 
o|contracted procedure: "(support.scm:796) g24922493" 
o|contracted procedure: "(support.scm:795) g24892490" 
o|contracted procedure: "(support.scm:794) g24862487" 
o|inlining procedure: k11919 
o|contracted procedure: "(support.scm:817) g25972598" 
o|inlining procedure: k11919 
o|contracted procedure: "(support.scm:819) g26002601" 
o|substituted constant variable: a11954 
o|contracted procedure: "(support.scm:815) g25942595" 
o|inlining procedure: k11961 
o|inlining procedure: k11984 
o|contracted procedure: "(support.scm:857) g26792686" 
o|inlining procedure: k11984 
o|contracted procedure: "(support.scm:857) chicken.compiler.support#sort-symbols" 
o|inlining procedure: k11961 
o|inlining procedure: k12045 
o|contracted procedure: "(support.scm:849) g26572664" 
o|inlining procedure: k12045 
o|inlining procedure: k12072 
o|inlining procedure: k12087 
o|inlining procedure: k12105 
o|inlining procedure: k12122 
o|inlining procedure: k12142 
o|contracted procedure: "(support.scm:841) chicken.compiler.support#node->sexpr" 
o|inlining procedure: k11797 
o|inlining procedure: k11797 
o|contracted procedure: "(support.scm:803) g25302531" 
o|contracted procedure: "(support.scm:802) g25082509" 
o|contracted procedure: "(support.scm:801) g25052506" 
o|inlining procedure: k12142 
o|inlining procedure: k12172 
o|inlining procedure: k12172 
o|inlining procedure: "(support.scm:837) chicken.compiler.support#fourth" 
o|substituted constant variable: a12186 
o|substituted constant variable: a12188 
o|contracted procedure: "(support.scm:833) g26472648" 
o|inlining procedure: k12122 
o|contracted procedure: "(support.scm:831) g26362637" 
o|inlining procedure: k12105 
o|inlining procedure: k12087 
o|contracted procedure: "(support.scm:826) g26222623" 
o|contracted procedure: "(support.scm:826) g26252626" 
o|inlining procedure: k12072 
o|inlining procedure: k12244 
o|inlining procedure: k12244 
o|contracted procedure: "(support.scm:866) g27082709" 
o|contracted procedure: "(support.scm:869) chicken.compiler.support#sexpr->node" 
o|contracted procedure: "(support.scm:807) g25452546" 
o|inlining procedure: k11868 
o|inlining procedure: k11868 
o|inlining procedure: k12291 
o|inlining procedure: k12291 
o|inlining procedure: k12322 
o|inlining procedure: k12322 
o|inlining procedure: k12337 
o|inlining procedure: k12337 
o|inlining procedure: k12369 
o|inlining procedure: k12369 
o|inlining procedure: k12384 
o|inlining procedure: k12409 
o|inlining procedure: k12409 
o|inlining procedure: k12427 
o|inlining procedure: k12427 
o|contracted procedure: "(support.scm:896) g27692770" 
o|inlining procedure: k12384 
o|inlining procedure: "(support.scm:895) chicken.compiler.support#second" 
o|contracted procedure: "(support.scm:895) g27572758" 
o|inlining procedure: "(support.scm:894) chicken.compiler.support#first" 
o|contracted procedure: "(support.scm:894) g27532754" 
o|inlining procedure: k12491 
o|contracted procedure: "(support.scm:907) g27772778" 
o|contracted procedure: "(support.scm:907) g27742775" 
o|inlining procedure: k12491 
o|inlining procedure: k12540 
o|inlining procedure: k12540 
o|contracted procedure: "(support.scm:920) chicken.compiler.support#find" 
o|inlining procedure: k6446 
o|inlining procedure: k6446 
o|propagated global variable: lst566 chicken.compiler.support#foreign-callback-stubs 
o|inlining procedure: "(support.scm:919) chicken.compiler.support#first" 
o|contracted procedure: "(support.scm:919) g28122813" 
o|inlining procedure: k12580 
o|inlining procedure: k12580 
o|substituted constant variable: a12596 
o|substituted constant variable: a12598 
o|substituted constant variable: a12600 
o|inlining procedure: k12604 
o|inlining procedure: k12604 
o|substituted constant variable: a12617 
o|substituted constant variable: a12619 
o|substituted constant variable: a12621 
o|substituted constant variable: a12623 
o|contracted procedure: "(support.scm:916) g27992800" 
o|contracted procedure: "(support.scm:915) g27902791" 
o|inlining procedure: k12641 
o|inlining procedure: k12664 
o|inlining procedure: k12687 
o|inlining procedure: k12687 
o|inlining procedure: "(support.scm:938) chicken.compiler.support#first" 
o|contracted procedure: "(support.scm:938) g28522853" 
o|contracted procedure: "(support.scm:937) g28482849" 
o|inlining procedure: "(support.scm:936) chicken.compiler.support#first" 
o|contracted procedure: "(support.scm:935) g28432844" 
o|inlining procedure: k12664 
o|contracted procedure: "(support.scm:941) g28552856" 
o|substituted constant variable: a12741 
o|substituted constant variable: a12743 
o|contracted procedure: "(support.scm:933) g28392840" 
o|inlining procedure: "(support.scm:931) chicken.compiler.support#second" 
o|inlining procedure: k12641 
o|inlining procedure: "(support.scm:929) chicken.compiler.support#first" 
o|inlining procedure: "(support.scm:928) chicken.compiler.support#third" 
o|contracted procedure: "(support.scm:927) g28232824" 
o|inlining procedure: k12759 
o|inlining procedure: k12759 
o|inlining procedure: k12774 
o|inlining procedure: k12774 
o|inlining procedure: k12800 
o|inlining procedure: k12800 
o|inlining procedure: k12815 
o|inlining procedure: k12815 
o|inlining procedure: k12837 
o|inlining procedure: k12857 
o|inlining procedure: k12857 
o|inlining procedure: k12837 
o|inlining procedure: k12880 
o|contracted procedure: "(support.scm:981) chicken.compiler.support#unhide-variable" 
o|inlining procedure: k17692 
o|inlining procedure: k17692 
o|inlining procedure: k12880 
o|contracted procedure: "(support.scm:980) g28872888" 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#foreign-callback-stub 
o|contracted procedure: "(support.scm:1004) g29292930" 
o|inlining procedure: k12994 
o|inlining procedure: k12994 
o|inlining procedure: k13031 
o|inlining procedure: k13031 
o|inlining procedure: k13079 
o|inlining procedure: k13079 
o|inlining procedure: k13103 
o|inlining procedure: k13103 
o|inlining procedure: k13109 
o|inlining procedure: k13109 
o|inlining procedure: k13162 
o|inlining procedure: k13162 
o|inlining procedure: k13216 
o|inlining procedure: k13216 
o|inlining procedure: k13274 
o|substituted constant variable: tmap2969 
o|substituted constant variable: tmap2969 
o|inlining procedure: k13274 
o|substituted constant variable: a13316 
o|substituted constant variable: a13317 
o|inlining procedure: k13330 
o|inlining procedure: k13330 
o|substituted constant variable: a13359 
o|substituted constant variable: a13360 
o|inlining procedure: k13343 
o|inlining procedure: k13343 
o|inlining procedure: k13422 
o|inlining procedure: k13422 
o|inlining procedure: k13476 
o|inlining procedure: k13476 
o|inlining procedure: k13504 
o|inlining procedure: k13504 
o|inlining procedure: k13528 
o|inlining procedure: k13528 
o|inlining procedure: k13550 
o|inlining procedure: k13550 
o|inlining procedure: k13624 
o|inlining procedure: k13624 
o|inlining procedure: k13665 
o|inlining procedure: k13665 
o|inlining procedure: k13671 
o|inlining procedure: k13671 
o|inlining procedure: k13697 
o|inlining procedure: k13697 
o|substituted constant variable: a13729 
o|substituted constant variable: a13731 
o|substituted constant variable: a13733 
o|substituted constant variable: a13735 
o|substituted constant variable: a13737 
o|substituted constant variable: a13739 
o|substituted constant variable: a13741 
o|substituted constant variable: a13746 
o|substituted constant variable: a13748 
o|inlining procedure: k13752 
o|inlining procedure: k13752 
o|substituted constant variable: a13765 
o|substituted constant variable: a13767 
o|substituted constant variable: a13769 
o|substituted constant variable: a13771 
o|substituted constant variable: a13779 
o|inlining procedure: k13783 
o|inlining procedure: k13783 
o|substituted constant variable: a13790 
o|substituted constant variable: a13792 
o|substituted constant variable: a13794 
o|inlining procedure: k13798 
o|inlining procedure: k13798 
o|substituted constant variable: a13811 
o|substituted constant variable: a13813 
o|substituted constant variable: a13815 
o|substituted constant variable: a13817 
o|substituted constant variable: a13819 
o|inlining procedure: k13823 
o|inlining procedure: k13823 
o|substituted constant variable: a13830 
o|substituted constant variable: a13832 
o|substituted constant variable: a13834 
o|inlining procedure: k13838 
o|inlining procedure: k13838 
o|inlining procedure: k13850 
o|inlining procedure: k13850 
o|substituted constant variable: a13863 
o|substituted constant variable: a13865 
o|substituted constant variable: a13867 
o|substituted constant variable: a13869 
o|substituted constant variable: a13871 
o|substituted constant variable: a13873 
o|inlining procedure: k13877 
o|inlining procedure: k13877 
o|inlining procedure: k13889 
o|inlining procedure: k13889 
o|substituted constant variable: a13902 
o|substituted constant variable: a13904 
o|substituted constant variable: a13906 
o|substituted constant variable: a13908 
o|substituted constant variable: a13910 
o|substituted constant variable: a13912 
o|inlining procedure: k13916 
o|inlining procedure: k13916 
o|inlining procedure: k13928 
o|inlining procedure: k13928 
o|inlining procedure: k13940 
o|inlining procedure: k13940 
o|inlining procedure: k13952 
o|inlining procedure: k13952 
o|substituted constant variable: a13965 
o|substituted constant variable: a13967 
o|substituted constant variable: a13969 
o|substituted constant variable: a13971 
o|substituted constant variable: a13973 
o|substituted constant variable: a13975 
o|substituted constant variable: a13977 
o|substituted constant variable: a13979 
o|substituted constant variable: a13981 
o|substituted constant variable: a13983 
o|inlining procedure: k13987 
o|inlining procedure: k13987 
o|inlining procedure: k13999 
o|inlining procedure: k13999 
o|inlining procedure: k14011 
o|inlining procedure: k14011 
o|inlining procedure: k14023 
o|inlining procedure: k14023 
o|substituted constant variable: a14036 
o|substituted constant variable: a14038 
o|substituted constant variable: a14040 
o|substituted constant variable: a14042 
o|substituted constant variable: a14044 
o|substituted constant variable: a14046 
o|substituted constant variable: a14048 
o|substituted constant variable: a14050 
o|substituted constant variable: a14052 
o|substituted constant variable: a14054 
o|substituted constant variable: a14056 
o|substituted constant variable: a14058 
o|substituted constant variable: a14063 
o|substituted constant variable: a14065 
o|substituted constant variable: a14070 
o|substituted constant variable: a14072 
o|inlining procedure: k14076 
o|inlining procedure: k14076 
o|substituted constant variable: a14083 
o|substituted constant variable: a14085 
o|substituted constant variable: a14087 
o|inlining procedure: k14091 
o|inlining procedure: k14091 
o|inlining procedure: k14103 
o|inlining procedure: k14103 
o|substituted constant variable: a14116 
o|substituted constant variable: a14118 
o|substituted constant variable: a14120 
o|substituted constant variable: a14122 
o|substituted constant variable: a14124 
o|substituted constant variable: a14126 
o|substituted constant variable: a14131 
o|substituted constant variable: a14133 
o|inlining procedure: k14146 
o|inlining procedure: k14158 
o|inlining procedure: k14158 
o|inlining procedure: k14146 
o|inlining procedure: k14167 
o|inlining procedure: k14179 
o|inlining procedure: k14179 
o|inlining procedure: k14167 
o|inlining procedure: k14191 
o|inlining procedure: k14191 
o|inlining procedure: k14206 
o|inlining procedure: k14206 
o|inlining procedure: k14224 
o|inlining procedure: k14224 
o|removed unused formal parameters: (t3328) 
o|inlining procedure: k14266 
o|inlining procedure: k14266 
o|inlining procedure: k14287 
o|inlining procedure: k14287 
o|inlining procedure: k14311 
o|inlining procedure: k14311 
o|inlining procedure: k14341 
o|inlining procedure: k14360 
o|inlining procedure: k14360 
o|removed unused parameter to known procedure: t3328 "(support.scm:1249) err3327" 
o|substituted constant variable: a14386 
o|substituted constant variable: a14388 
o|inlining procedure: k14392 
o|inlining procedure: k14392 
o|inlining procedure: k14404 
o|inlining procedure: k14404 
o|inlining procedure: k14416 
o|inlining procedure: k14416 
o|inlining procedure: k14428 
o|inlining procedure: k14428 
o|substituted constant variable: a14435 
o|substituted constant variable: a14437 
o|substituted constant variable: a14439 
o|substituted constant variable: a14441 
o|substituted constant variable: a14443 
o|substituted constant variable: a14445 
o|substituted constant variable: a14447 
o|substituted constant variable: a14449 
o|substituted constant variable: a14451 
o|inlining procedure: k14341 
o|removed unused parameter to known procedure: t3328 "(support.scm:1250) err3327" 
o|inlining procedure: k14464 
o|inlining procedure: k14464 
o|substituted constant variable: a14477 
o|substituted constant variable: a14479 
o|substituted constant variable: a14481 
o|substituted constant variable: a14483 
o|inlining procedure: k14487 
o|inlining procedure: k14487 
o|substituted constant variable: a14494 
o|substituted constant variable: a14496 
o|substituted constant variable: a14498 
o|inlining procedure: k14502 
o|inlining procedure: k14502 
o|inlining procedure: k14514 
o|inlining procedure: k14514 
o|substituted constant variable: a14527 
o|substituted constant variable: a14529 
o|substituted constant variable: a14531 
o|substituted constant variable: a14533 
o|substituted constant variable: a14535 
o|substituted constant variable: a14537 
o|inlining procedure: k14541 
o|inlining procedure: k14541 
o|inlining procedure: k14553 
o|inlining procedure: k14553 
o|inlining procedure: k14565 
o|inlining procedure: k14565 
o|inlining procedure: k14577 
o|inlining procedure: k14577 
o|inlining procedure: k14589 
o|inlining procedure: k14589 
o|substituted constant variable: a14602 
o|substituted constant variable: a14604 
o|substituted constant variable: a14606 
o|substituted constant variable: a14608 
o|substituted constant variable: a14610 
o|substituted constant variable: a14612 
o|substituted constant variable: a14614 
o|substituted constant variable: a14616 
o|substituted constant variable: a14618 
o|substituted constant variable: a14620 
o|substituted constant variable: a14622 
o|substituted constant variable: a14624 
o|inlining procedure: k14628 
o|inlining procedure: k14628 
o|inlining procedure: k14640 
o|inlining procedure: k14640 
o|inlining procedure: k14652 
o|inlining procedure: k14652 
o|inlining procedure: k14664 
o|inlining procedure: k14664 
o|inlining procedure: k14676 
o|inlining procedure: k14676 
o|inlining procedure: k14688 
o|inlining procedure: k14688 
o|substituted constant variable: a14695 
o|substituted constant variable: a14697 
o|substituted constant variable: a14699 
o|substituted constant variable: a14701 
o|substituted constant variable: a14703 
o|substituted constant variable: a14705 
o|substituted constant variable: a14707 
o|substituted constant variable: a14709 
o|substituted constant variable: a14711 
o|substituted constant variable: a14713 
o|substituted constant variable: a14715 
o|substituted constant variable: a14717 
o|substituted constant variable: a14719 
o|inlining procedure: k14741 
o|inlining procedure: k14741 
o|inlining procedure: k14768 
o|inlining procedure: k14768 
o|inlining procedure: k14790 
o|inlining procedure: k14790 
o|inlining procedure: "(support.scm:1277) err3479" 
o|substituted constant variable: a14819 
o|inlining procedure: k14823 
o|inlining procedure: k14823 
o|inlining procedure: k14835 
o|inlining procedure: k14835 
o|inlining procedure: k14847 
o|inlining procedure: k14847 
o|inlining procedure: k14859 
o|inlining procedure: k14859 
o|substituted constant variable: a14866 
o|substituted constant variable: a14868 
o|substituted constant variable: a14870 
o|substituted constant variable: a14872 
o|substituted constant variable: a14874 
o|substituted constant variable: a14876 
o|substituted constant variable: a14878 
o|substituted constant variable: a14880 
o|substituted constant variable: a14882 
o|inlining procedure: "(support.scm:1278) err3479" 
o|inlining procedure: k14895 
o|inlining procedure: k14895 
o|inlining procedure: k14907 
o|inlining procedure: k14907 
o|substituted constant variable: a14914 
o|substituted constant variable: a14916 
o|substituted constant variable: a14918 
o|substituted constant variable: a14920 
o|substituted constant variable: a14922 
o|inlining procedure: k14926 
o|inlining procedure: k14926 
o|inlining procedure: k14938 
o|inlining procedure: k14938 
o|inlining procedure: k14950 
o|inlining procedure: k14950 
o|inlining procedure: k14962 
o|inlining procedure: k14962 
o|inlining procedure: k14974 
o|inlining procedure: k14974 
o|inlining procedure: k14986 
o|inlining procedure: k14986 
o|inlining procedure: k14998 
o|inlining procedure: k14998 
o|inlining procedure: k15010 
o|inlining procedure: k15010 
o|inlining procedure: k15022 
o|inlining procedure: k15022 
o|inlining procedure: k15034 
o|inlining procedure: k15034 
o|inlining procedure: k15046 
o|inlining procedure: k15046 
o|inlining procedure: k15058 
o|inlining procedure: k15058 
o|inlining procedure: k15070 
o|inlining procedure: k15070 
o|inlining procedure: k15082 
o|inlining procedure: k15082 
o|inlining procedure: k15094 
o|inlining procedure: k15094 
o|substituted constant variable: a15107 
o|substituted constant variable: a15109 
o|substituted constant variable: a15111 
o|substituted constant variable: a15113 
o|substituted constant variable: a15115 
o|substituted constant variable: a15117 
o|substituted constant variable: a15119 
o|substituted constant variable: a15121 
o|substituted constant variable: a15123 
o|substituted constant variable: a15125 
o|substituted constant variable: a15127 
o|substituted constant variable: a15129 
o|substituted constant variable: a15131 
o|substituted constant variable: a15133 
o|substituted constant variable: a15135 
o|substituted constant variable: a15137 
o|substituted constant variable: a15139 
o|substituted constant variable: a15141 
o|substituted constant variable: a15143 
o|substituted constant variable: a15145 
o|substituted constant variable: a15147 
o|substituted constant variable: a15149 
o|substituted constant variable: a15151 
o|substituted constant variable: a15153 
o|substituted constant variable: a15155 
o|substituted constant variable: a15157 
o|substituted constant variable: a15159 
o|substituted constant variable: a15161 
o|substituted constant variable: a15163 
o|substituted constant variable: a15165 
o|substituted constant variable: a15167 
o|substituted constant variable: a15169 
o|inlining procedure: k15182 
o|inlining procedure: k15182 
o|inlining procedure: k15211 
o|inlining procedure: k15211 
o|inlining procedure: k15243 
o|inlining procedure: k15243 
o|inlining procedure: k15273 
o|inlining procedure: k15273 
o|inlining procedure: k15292 
o|inlining procedure: k15292 
o|inlining procedure: k15314 
o|inlining procedure: k15314 
o|substituted constant variable: a15379 
o|substituted constant variable: a15384 
o|substituted constant variable: a15386 
o|substituted constant variable: a15387 
o|inlining procedure: k15395 
o|substituted constant variable: a15405 
o|inlining procedure: k15395 
o|substituted constant variable: a15406 
o|substituted constant variable: a15416 
o|substituted constant variable: a15418 
o|substituted constant variable: a15420 
o|substituted constant variable: a15425 
o|substituted constant variable: a15427 
o|substituted constant variable: a15432 
o|substituted constant variable: a15434 
o|substituted constant variable: a15436 
o|substituted constant variable: a15441 
o|substituted constant variable: a15443 
o|inlining procedure: k15447 
o|inlining procedure: k15447 
o|inlining procedure: k15465 
o|inlining procedure: k15465 
o|inlining procedure: k15483 
o|substituted constant variable: a15499 
o|inlining procedure: k15483 
o|inlining procedure: k15509 
o|substituted constant variable: a15522 
o|inlining procedure: k15509 
o|inlining procedure: k15529 
o|substituted constant variable: a15542 
o|inlining procedure: k15529 
o|inlining procedure: k15549 
o|substituted constant variable: a15575 
o|inlining procedure: k15549 
o|inlining procedure: k15582 
o|inlining procedure: k15582 
o|inlining procedure: k15594 
o|inlining procedure: k15594 
o|inlining procedure: k15606 
o|inlining procedure: k15606 
o|inlining procedure: k15618 
o|inlining procedure: k15618 
o|inlining procedure: k15630 
o|inlining procedure: k15630 
o|inlining procedure: k15645 
o|inlining procedure: k15645 
o|inlining procedure: k15657 
o|inlining procedure: k15657 
o|inlining procedure: k15675 
o|inlining procedure: k15675 
o|inlining procedure: k15690 
o|inlining procedure: k15706 
o|inlining procedure: k15706 
o|inlining procedure: k15725 
o|inlining procedure: k15725 
o|substituted constant variable: a15738 
o|substituted constant variable: a15740 
o|substituted constant variable: a15742 
o|substituted constant variable: a15744 
o|inlining procedure: k15748 
o|inlining procedure: k15748 
o|substituted constant variable: a15761 
o|substituted constant variable: a15763 
o|substituted constant variable: a15765 
o|substituted constant variable: a15767 
o|inlining procedure: k15690 
o|substituted constant variable: a15769 
o|inlining procedure: k15773 
o|inlining procedure: k15773 
o|substituted constant variable: a15780 
o|substituted constant variable: a15782 
o|substituted constant variable: a15784 
o|substituted constant variable: a15789 
o|substituted constant variable: a15791 
o|inlining procedure: k15795 
o|inlining procedure: k15795 
o|substituted constant variable: a15808 
o|substituted constant variable: a15810 
o|substituted constant variable: a15812 
o|substituted constant variable: a15814 
o|substituted constant variable: a15816 
o|substituted constant variable: a15818 
o|inlining procedure: k15822 
o|inlining procedure: k15822 
o|inlining procedure: k15834 
o|inlining procedure: k15834 
o|inlining procedure: k15846 
o|inlining procedure: k15846 
o|inlining procedure: k15858 
o|inlining procedure: k15858 
o|substituted constant variable: a15865 
o|substituted constant variable: a15867 
o|substituted constant variable: a15869 
o|substituted constant variable: a15871 
o|substituted constant variable: a15873 
o|substituted constant variable: a15875 
o|substituted constant variable: a15877 
o|substituted constant variable: a15879 
o|substituted constant variable: a15881 
o|substituted constant variable: a15883 
o|substituted constant variable: a15885 
o|substituted constant variable: a15887 
o|substituted constant variable: a15889 
o|substituted constant variable: a15891 
o|substituted constant variable: a15893 
o|substituted constant variable: a15895 
o|substituted constant variable: a15897 
o|substituted constant variable: a15899 
o|substituted constant variable: a15901 
o|inlining procedure: k15905 
o|inlining procedure: k15905 
o|inlining procedure: k15917 
o|inlining procedure: k15917 
o|inlining procedure: k15929 
o|inlining procedure: k15929 
o|inlining procedure: k15941 
o|inlining procedure: k15941 
o|substituted constant variable: a15954 
o|substituted constant variable: a15956 
o|substituted constant variable: a15958 
o|substituted constant variable: a15960 
o|substituted constant variable: a15962 
o|substituted constant variable: a15964 
o|substituted constant variable: a15966 
o|substituted constant variable: a15968 
o|substituted constant variable: a15970 
o|substituted constant variable: a15972 
o|substituted constant variable: a15974 
o|substituted constant variable: a15976 
o|substituted constant variable: a15978 
o|substituted constant variable: a15980 
o|substituted constant variable: a15985 
o|substituted constant variable: a15987 
o|substituted constant variable: a15992 
o|substituted constant variable: a15994 
o|inlining procedure: k15998 
o|inlining procedure: k15998 
o|inlining procedure: k16010 
o|inlining procedure: k16010 
o|inlining procedure: k16022 
o|inlining procedure: k16022 
o|substituted constant variable: a16035 
o|substituted constant variable: a16037 
o|substituted constant variable: a16039 
o|substituted constant variable: a16041 
o|substituted constant variable: a16043 
o|substituted constant variable: a16045 
o|substituted constant variable: a16047 
o|substituted constant variable: a16049 
o|substituted constant variable: a16054 
o|substituted constant variable: a16056 
o|substituted constant variable: a16058 
o|inlining procedure: k16059 
o|inlining procedure: k16059 
o|inlining procedure: k16099 
o|inlining procedure: k16123 
o|inlining procedure: k16123 
o|inlining procedure: "(support.scm:1397) chicken.compiler.support#first" 
o|contracted procedure: "(support.scm:1397) g38823883" 
o|inlining procedure: k16099 
o|inlining procedure: k16187 
o|inlining procedure: k16187 
o|inlining procedure: k16210 
o|inlining procedure: k16210 
o|substituted constant variable: a16217 
o|substituted constant variable: a16219 
o|substituted constant variable: a16221 
o|substituted constant variable: a16226 
o|substituted constant variable: a16228 
o|contracted procedure: "(support.scm:1395) g38753876" 
o|contracted procedure: "(support.scm:1394) g38663867" 
o|inlining procedure: k16259 
o|inlining procedure: k16259 
o|inlining procedure: k16277 
o|inlining procedure: k16277 
o|consed rest parameter at call site: "(support.scm:1422) chicken.compiler.support#lset-adjoin/eq?" 2 
o|consed rest parameter at call site: "(support.scm:1420) chicken.compiler.support#lset-adjoin/eq?" 2 
o|inlining procedure: "(support.scm:1418) chicken.compiler.support#first" 
o|inlining procedure: k16297 
o|consed rest parameter at call site: "(support.scm:1425) chicken.compiler.support#lset-adjoin/eq?" 2 
o|inlining procedure: "(support.scm:1424) chicken.compiler.support#first" 
o|inlining procedure: k16297 
o|inlining procedure: "(support.scm:1429) chicken.compiler.support#second" 
o|inlining procedure: "(support.scm:1428) chicken.compiler.support#first" 
o|inlining procedure: k16347 
o|inlining procedure: "(support.scm:1434) chicken.compiler.support#first" 
o|inlining procedure: "(support.scm:1432) chicken.compiler.support#third" 
o|inlining procedure: k16347 
o|substituted constant variable: a16378 
o|substituted constant variable: a16380 
o|substituted constant variable: a16382 
o|substituted constant variable: a16384 
o|inlining procedure: k16388 
o|inlining procedure: k16388 
o|inlining procedure: k16400 
o|inlining procedure: k16400 
o|substituted constant variable: a16407 
o|substituted constant variable: a16409 
o|substituted constant variable: a16411 
o|substituted constant variable: a16413 
o|substituted constant variable: a16415 
o|contracted procedure: "(support.scm:1415) g39523953" 
o|contracted procedure: "(support.scm:1414) g39433944" 
o|contracted procedure: "(support.scm:1413) g39403941" 
o|inlining procedure: k16433 
o|inlining procedure: k16433 
o|inlining procedure: k16466 
o|inlining procedure: k16466 
o|substituted constant variable: a16476 
o|substituted constant variable: a16481 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#block-variable-literal 
o|substituted constant variable: a16515 
o|substituted constant variable: a16516 
o|inlining procedure: k16538 
o|inlining procedure: k16538 
o|inlining procedure: k16580 
o|inlining procedure: k16580 
o|inlining procedure: k16592 
o|inlining procedure: k16592 
o|inlining procedure: k16622 
o|inlining procedure: k16622 
o|inlining procedure: k16645 
o|inlining procedure: k16645 
o|substituted constant variable: chicken.compiler.support#real-name-max-depth 
o|inlining procedure: k16697 
o|inlining procedure: k16697 
o|propagated global variable: out40774080 ##sys#standard-output 
o|substituted constant variable: a16713 
o|substituted constant variable: a16714 
o|propagated global variable: out40774080 ##sys#standard-output 
o|inlining procedure: k16730 
o|substituted constant variable: a16754 
o|inlining procedure: k16730 
o|inlining procedure: k16765 
o|inlining procedure: k16765 
o|inlining procedure: k16780 
o|inlining procedure: k16780 
o|inlining procedure: k16798 
o|inlining procedure: k16798 
o|inlining procedure: k16801 
o|inlining procedure: k16801 
o|inlining procedure: "(support.scm:1548) chicken.compiler.support#second" 
o|inlining procedure: k16858 
o|inlining procedure: k16879 
o|inlining procedure: k16879 
o|substituted constant variable: a16907 
o|substituted constant variable: a16920 
o|inlining procedure: k16858 
o|inlining procedure: k16991 
o|contracted procedure: "(support.scm:1560) g41504159" 
o|inlining procedure: k16991 
o|inlining procedure: k17025 
o|contracted procedure: "(support.scm:1559) g41194128" 
o|inlining procedure: "(support.scm:1559) chicken.compiler.support#first" 
o|contracted procedure: "(support.scm:1559) g41314132" 
o|inlining procedure: k17025 
o|contracted procedure: "(support.scm:1577) g41944195" 
o|inlining procedure: k17074 
o|inlining procedure: k17111 
o|inlining procedure: k17111 
o|contracted procedure: "(support.scm:1580) g42054206" 
o|inlining procedure: "(support.scm:1579) chicken.compiler.support#first" 
o|contracted procedure: "(support.scm:1579) g42014202" 
o|inlining procedure: k17074 
o|contracted procedure: "(support.scm:1578) g41974198" 
o|inlining procedure: k17172 
o|inlining procedure: k17172 
o|inlining procedure: k17185 
o|inlining procedure: k17185 
o|substituted constant variable: a17219 
o|merged explicitly consed rest parameter: args42334239 
o|consed rest parameter at call site: tmp25276 1 
o|inlining procedure: k17241 
o|inlining procedure: k17241 
o|inlining procedure: k17260 
o|inlining procedure: "(support.scm:1608) getsize4223" 
o|inlining procedure: k17260 
o|inlining procedure: "(support.scm:1610) getsize4223" 
o|substituted constant variable: a17332 
o|propagated global variable: out42694282 ##sys#standard-output 
o|substituted constant variable: a17334 
o|substituted constant variable: a17335 
o|propagated global variable: out43054308 ##sys#standard-output 
o|substituted constant variable: a17378 
o|substituted constant variable: a17379 
o|inlining procedure: k17368 
o|inlining procedure: k17395 
o|propagated global variable: out43144317 ##sys#standard-output 
o|substituted constant variable: a17402 
o|substituted constant variable: a17403 
o|inlining procedure: k17395 
o|propagated global variable: out43144317 ##sys#standard-output 
o|propagated global variable: out43054308 ##sys#standard-output 
o|inlining procedure: k17368 
o|inlining procedure: k17428 
o|inlining procedure: k17428 
o|propagated global variable: out42694282 ##sys#standard-output 
o|contracted procedure: "(support.scm:1624) g42664267" 
o|contracted procedure: "(support.scm:1623) g42634264" 
o|contracted procedure: "(support.scm:1622) g42604261" 
o|inlining procedure: k17451 
o|inlining procedure: k17476 
o|inlining procedure: k17476 
o|inlining procedure: k17451 
o|inlining procedure: k17500 
o|contracted procedure: "(support.scm:1664) chicken.compiler.support#scan-sharp-greater-string" 
o|inlining procedure: k17537 
o|inlining procedure: k17537 
o|substituted constant variable: a17550 
o|substituted constant variable: a17561 
o|inlining procedure: k17557 
o|substituted constant variable: a17583 
o|inlining procedure: k17557 
o|inlining procedure: k17500 
o|inlining procedure: k17594 
o|inlining procedure: k17609 
o|inlining procedure: k17609 
o|inlining procedure: k17594 
o|inlining procedure: k17618 
o|inlining procedure: k17618 
o|contracted procedure: "(support.scm:1707) g43764377" 
o|contracted procedure: "(support.scm:1710) g43924393" 
o|inlining procedure: k17707 
o|inlining procedure: k17707 
o|substituted constant variable: a17723 
o|substituted constant variable: a17725 
o|contracted procedure: "(support.scm:1734) g44434444" 
o|contracted procedure: "(support.scm:1736) g44544455" 
o|contracted procedure: "(support.scm:1737) g44654466" 
o|inlining procedure: k17786 
o|inlining procedure: k17829 
o|contracted procedure: "(support.scm:1745) g44884495" 
o|inlining procedure: k17829 
o|substituted constant variable: a17854 
o|substituted constant variable: a17855 
o|inlining procedure: k17786 
o|substituted constant variable: chicken.compiler.support#constant659 
o|substituted constant variable: a17922 
o|substituted constant variable: a17923 
o|contracted procedure: "(support.scm:476) g12931294" 
o|contracted procedure: "(support.scm:476) g12901291" 
o|replaced variables: 3950 
o|removed binding forms: 680 
o|substituted constant variable: r542517957 
o|substituted constant variable: r572617961 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#first 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#second 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#third 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#fourth 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#filter 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#constant659 
o|propagated global variable: out696699 ##sys#standard-output 
o|propagated global variable: out711714 chicken.compiler.support#collected-debugging-output 
o|substituted constant variable: r673117991 
o|inlining procedure: k6745 
o|substituted constant variable: r673117992 
o|inlining procedure: k6754 
o|propagated global variable: out747750 chicken.compiler.support#collected-debugging-output 
o|converted assignments to bindings: (collect732) 
o|substituted constant variable: r705618005 
o|substituted constant variable: r713118009 
o|substituted constant variable: r717818011 
o|substituted constant variable: r717818012 
o|substituted constant variable: r721518018 
o|substituted constant variable: r753118044 
o|substituted constant variable: r764518060 
o|substituted constant variable: r768318061 
o|substituted constant variable: r780118071 
o|substituted constant variable: r807818082 
o|substituted constant variable: r597918087 
o|substituted constant variable: r809618088 
o|substituted constant variable: r821218096 
o|propagated global variable: out12111214 ##sys#standard-output 
o|substituted constant variable: c1309 
o|substituted constant variable: s1311 
o|substituted constant variable: c1316 
o|substituted constant variable: s1318 
o|substituted constant variable: p1343 
o|substituted constant variable: r858118116 
o|substituted constant variable: c1379 
o|substituted constant variable: c1414 
o|substituted constant variable: c1419 
o|substituted constant variable: c1432 
o|substituted constant variable: c1437 
o|substituted constant variable: p1438 
o|substituted constant variable: s1439 
o|substituted constant variable: c1442 
o|substituted constant variable: s1520 
o|substituted constant variable: c1523 
o|substituted constant variable: s1525 
o|substituted constant variable: c1531 
o|substituted constant variable: c1563 
o|substituted constant variable: c1611 
o|substituted constant variable: c1644 
o|substituted constant variable: mark1654 
o|substituted constant variable: c1688 
o|substituted constant variable: c2044 
o|substituted constant variable: p2045 
o|substituted constant variable: c2145 
o|substituted constant variable: c2150 
o|substituted constant variable: c2155 
o|substituted constant variable: c2161 
o|substituted constant variable: c2427 
o|substituted constant variable: c2439 
o|removed side-effect free assignment to unused variable: rename2205 
o|substituted constant variable: s2233 
o|substituted constant variable: c2238 
o|substituted constant variable: c2247 
o|substituted constant variable: c2318 
o|substituted constant variable: r1117918415 
o|substituted constant variable: r1117918415 
o|substituted constant variable: r1214318455 
o|substituted constant variable: r1217318456 
o|substituted constant variable: mark2650 
o|substituted constant variable: r1212318463 
o|substituted constant variable: r1210618464 
o|substituted constant variable: r1208818465 
o|substituted constant variable: mark2628 
o|converted assignments to bindings: (uses-foreign-stubs?2584) 
o|substituted constant variable: mark2715 
o|substituted constant variable: r1233818477 
o|substituted constant variable: r1242818483 
o|substituted constant variable: r1238518485 
o|substituted constant variable: r1249218497 
o|substituted constant variable: r1254118498 
o|substituted constant variable: r644718500 
o|substituted constant variable: r1258118508 
o|substituted constant variable: r1268818514 
o|substituted constant variable: r1264218531 
o|substituted constant variable: r1277518545 
o|substituted constant variable: r1281618549 
o|substituted constant variable: r1285818553 
o|substituted constant variable: r1285818553 
o|substituted constant variable: mark2936 
o|substituted constant variable: r1303218564 
o|substituted constant variable: r1303218564 
o|substituted constant variable: r1415918640 
o|substituted constant variable: r1414718641 
o|substituted constant variable: r1418018644 
o|substituted constant variable: r1416818645 
o|substituted constant variable: r1426718652 
o|converted assignments to bindings: (err3327) 
o|removed side-effect free assignment to unused variable: err3479 
o|substituted constant variable: r1539618771 
o|substituted constant variable: r1544818772 
o|substituted constant variable: r1546618774 
o|substituted constant variable: r1558318784 
o|substituted constant variable: r1559518786 
o|substituted constant variable: r1560718788 
o|substituted constant variable: r1561918790 
o|substituted constant variable: r1563118792 
o|substituted constant variable: r1564618794 
o|substituted constant variable: r1565818796 
o|substituted constant variable: r1567618798 
o|substituted constant variable: r1572618803 
o|substituted constant variable: r1572618804 
o|inlining procedure: k15731 
o|substituted constant variable: r1569118807 
o|substituted constant variable: r1606018835 
o|substituted constant variable: r1626018849 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#real-name-max-depth 
o|converted assignments to bindings: (resolve4046) 
o|substituted constant variable: r1669818908 
o|propagated global variable: out40774080 ##sys#standard-output 
o|substituted constant variable: r1678118914 
o|substituted constant variable: r1680218918 
o|substituted constant variable: r1711218939 
o|converted assignments to bindings: (constant-node?4192) 
o|removed side-effect free assignment to unused variable: getsize4223 
o|substituted constant variable: r1718618949 
o|contracted procedure: "(support.scm:1614) chicken.compiler.support#list-tabulate" 
o|substituted constant variable: r633417973 
o|propagated global variable: out42694282 ##sys#standard-output 
o|propagated global variable: out43054308 ##sys#standard-output 
o|propagated global variable: out43144317 ##sys#standard-output 
o|substituted constant variable: r1747718976 
o|substituted constant variable: r1747718976 
o|substituted constant variable: r1759518989 
o|substituted constant variable: r1761918991 
o|substituted constant variable: mark4383 
o|substituted constant variable: mark4399 
o|substituted constant variable: r1770818994 
o|substituted constant variable: mark4446 
o|substituted constant variable: mark4457 
o|substituted constant variable: mark4468 
o|substituted constant variable: r1778718999 
o|simplifications: ((let . 5)) 
o|replaced variables: 611 
o|removed binding forms: 3336 
o|substituted constant variable: r67311799119027 
o|substituted constant variable: r67311799219029 
o|inlining procedure: "(support.scm:389) chicken.compiler.support#alist-cons" 
o|inlining procedure: "(support.scm:429) chicken.compiler.support#alist-cons" 
o|inlining procedure: "(support.scm:437) chicken.compiler.support#alist-cons" 
o|removed call to pure procedure with unused result: "(support.scm:654) ##sys#slot" 
o|inlining procedure: "(support.scm:719) chicken.compiler.support#alist-cons" 
o|inlining procedure: k12252 
o|inlining procedure: "(support.scm:881) chicken.compiler.support#alist-cons" 
o|inlining procedure: k12586 
o|inlining procedure: k12971 
o|inlining procedure: k13719 
o|inlining procedure: k14188 
o|inlining procedure: k14203 
o|substituted constant variable: r157261880319240 
o|substituted constant variable: r157261880419241 
o|inlining procedure: k16306 
o|inlining procedure: k16306 
o|inlining procedure: k16795 
o|inlining procedure: k16795 
o|inlining procedure: "(support.scm:1602) fits?4224" 
o|inlining procedure: "(support.scm:1606) fits?4224" 
o|inlining procedure: "(support.scm:1608) fits?4224" 
o|inlining procedure: "(support.scm:1611) fits?4224" 
o|converted assignments to bindings: (fits?4224) 
o|inlining procedure: "(support.scm:1647) chicken.compiler.support#alist-cons" 
o|inlining procedure: "(support.scm:1647) chicken.compiler.support#alist-cons" 
o|inlining procedure: k17644 
o|inlining procedure: k17664 
o|inlining procedure: k17729 
o|inlining procedure: k17881 
o|simplifications: ((let . 1)) 
o|replaced variables: 4 
o|removed binding forms: 728 
o|removed side-effect free assignment to unused variable: chicken.compiler.support#alist-cons 
o|inlining procedure: k8986 
o|contracted procedure: k10838 
o|substituted constant variable: r1225319470 
o|contracted procedure: k12885 
o|substituted constant variable: r1297219498 
o|substituted constant variable: r1418919502 
o|substituted constant variable: r1418919502 
o|substituted constant variable: r1420419505 
o|substituted constant variable: r1420419505 
o|inlining procedure: k16469 
o|inlining procedure: k16469 
o|substituted constant variable: r1679619526 
o|substituted constant variable: r1679619526 
o|substituted constant variable: r1679619529 
o|substituted constant variable: r1679619529 
o|contracted procedure: k17267 
o|contracted procedure: k17270 
o|substituted constant variable: z36319567 
o|substituted constant variable: r1764519573 
o|substituted constant variable: r1766519574 
o|substituted constant variable: r1773019575 
o|simplifications: ((let . 1)) 
o|replaced variables: 35 
o|removed binding forms: 26 
o|removed conditional forms: 4 
o|substituted constant variable: r898719632 
o|substituted constant variable: r1647019740 
o|simplifications: ((if . 1) (let . 1)) 
o|removed binding forms: 48 
o|removed conditional forms: 2 
o|replaced variables: 2 
o|removed binding forms: 2 
o|removed binding forms: 1 
o|simplifications: ((if . 104) (let . 52) (##core#call . 1492)) 
o|  call simplifications:
o|    chicken.base#bignum?
o|    scheme#char=?
o|    scheme#read-char	3
o|    ##sys#size
o|    chicken.fixnum#fx>	2
o|    scheme#write-char	6
o|    chicken.base#flonum?
o|    scheme#procedure?
o|    chicken.fixnum#fx+	3
o|    scheme#string-length	4
o|    scheme#string-ref
o|    scheme#list?	5
o|    scheme#vector-ref	6
o|    scheme#vector	2
o|    scheme#<
o|    scheme#>	2
o|    scheme#<=
o|    scheme#-
o|    ##sys#call-with-values	4
o|    scheme#cddddr
o|    scheme#list-ref	2
o|    scheme#cdddr
o|    scheme#caar	2
o|    scheme#cadddr	4
o|    scheme#caddr	10
o|    scheme#cadr	41
o|    ##sys#check-structure	12
o|    ##sys#block-ref	9
o|    ##sys#structure?	5
o|    ##sys#make-structure	37
o|    scheme#values	4
o|    scheme#assq	16
o|    scheme#length	8
o|    ##sys#setslot	36
o|    chicken.base#atom?
o|    ##sys#apply	3
o|    ##sys#cons	8
o|    scheme#equal?	3
o|    ##sys#list	139
o|    chicken.base#fixnum?	2
o|    scheme#number?	4
o|    scheme#char?	4
o|    scheme#string?	2
o|    scheme#boolean?	4
o|    scheme#vector?	2
o|    scheme#eq?	361
o|    scheme#eof-object?	7
o|    scheme#member
o|    scheme#cddr	4
o|    scheme#list	55
o|    scheme#string=?	2
o|    ##sys#foreign-fixnum-argument	2
o|    scheme#char-alphabetic?	2
o|    scheme#char-numeric?
o|    scheme#char->integer
o|    chicken.fixnum#fx>=	3
o|    chicken.fixnum#fx<	4
o|    scheme#string->list	3
o|    scheme#list->string
o|    ##sys#check-list	42
o|    ##sys#slot	191
o|    scheme#symbol?	16
o|    scheme#pair?	69
o|    scheme#apply	7
o|    scheme#memq	9
o|    scheme#cdr	23
o|    scheme#null?	48
o|    scheme#not	23
o|    chicken.fixnum#fx<=	7
o|    scheme#car	89
o|    chicken.fixnum#fx-	2
o|    scheme#cons	113
o|contracted procedure: k5427 
o|contracted procedure: k5434 
o|contracted procedure: k5444 
o|contracted procedure: k5691 
o|contracted procedure: k5700 
o|contracted procedure: k5713 
o|contracted procedure: k5728 
o|contracted procedure: k5743 
o|contracted procedure: k5758 
o|contracted procedure: k5769 
o|contracted procedure: k6104 
o|contracted procedure: k6094 
o|contracted procedure: k6153 
o|contracted procedure: k6179 
o|contracted procedure: k6159 
o|contracted procedure: k6173 
o|contracted procedure: k6604 
o|contracted procedure: k6632 
o|contracted procedure: k6668 
o|contracted procedure: k6698 
o|contracted procedure: k6708 
o|contracted procedure: k6712 
o|contracted procedure: k6794 
o|propagated global variable: out747750 chicken.compiler.support#collected-debugging-output 
o|contracted procedure: k6810 
o|contracted procedure: k6820 
o|contracted procedure: k6824 
o|contracted procedure: k6894 
o|contracted procedure: k6912 
o|contracted procedure: k6922 
o|contracted procedure: k6926 
o|contracted procedure: k6965 
o|contracted procedure: k6969 
o|contracted procedure: k6973 
o|contracted procedure: k6987 
o|contracted procedure: k7043 
o|contracted procedure: k7049 
o|contracted procedure: k7058 
o|contracted procedure: k7065 
o|contracted procedure: k7069 
o|contracted procedure: k7085 
o|contracted procedure: k7095 
o|contracted procedure: k7116 
o|contracted procedure: k7133 
o|contracted procedure: k7136 
o|contracted procedure: k7139 
o|contracted procedure: k7145 
o|contracted procedure: k7174 
o|contracted procedure: k7180 
o|contracted procedure: k7196 
o|contracted procedure: k7217 
o|contracted procedure: k7224 
o|contracted procedure: k7227 
o|contracted procedure: k7236 
o|contracted procedure: k7242 
o|contracted procedure: k7262 
o|contracted procedure: k7269 
o|contracted procedure: k7278 
o|contracted procedure: k7293 
o|contracted procedure: k7306 
o|contracted procedure: k7313 
o|contracted procedure: k7322 
o|contracted procedure: k7381 
o|contracted procedure: k7334 
o|contracted procedure: k7377 
o|contracted procedure: k7354 
o|inlining procedure: k7351 
o|inlining procedure: k7351 
o|contracted procedure: k7396 
o|contracted procedure: k7412 
o|contracted procedure: k7473 
o|contracted procedure: k7438 
o|contracted procedure: k7454 
o|contracted procedure: k7470 
o|contracted procedure: k7482 
o|contracted procedure: k7488 
o|contracted procedure: k7494 
o|contracted procedure: k7500 
o|contracted procedure: k7506 
o|contracted procedure: k7518 
o|contracted procedure: k7533 
o|contracted procedure: k7544 
o|contracted procedure: k7550 
o|contracted procedure: k7556 
o|contracted procedure: k7562 
o|contracted procedure: k7580 
o|contracted procedure: k7586 
o|contracted procedure: k7592 
o|contracted procedure: k7598 
o|contracted procedure: k7607 
o|contracted procedure: k7620 
o|contracted procedure: k7626 
o|contracted procedure: k7647 
o|contracted procedure: k7663 
o|contracted procedure: k7685 
o|contracted procedure: k7743 
o|contracted procedure: k7691 
o|contracted procedure: k7699 
o|contracted procedure: k7724 
o|contracted procedure: k7714 
o|contracted procedure: k7803 
o|contracted procedure: k7817 
o|contracted procedure: k7809 
o|contracted procedure: k7835 
o|contracted procedure: k7849 
o|contracted procedure: k7885 
o|contracted procedure: k7891 
o|contracted procedure: k7900 
o|contracted procedure: k7910 
o|contracted procedure: k7914 
o|contracted procedure: k591919326 
o|contracted procedure: k7934 
o|propagated global variable: z36319323 chicken.compiler.support#profile-lambda-list 
o|contracted procedure: k7979 
o|contracted procedure: k7975 
o|contracted procedure: k7947 
o|contracted procedure: k7971 
o|contracted procedure: k7967 
o|contracted procedure: k7951 
o|contracted procedure: k7963 
o|contracted procedure: k7959 
o|contracted procedure: k7955 
o|contracted procedure: k7943 
o|contracted procedure: k8068 
o|contracted procedure: k8060 
o|contracted procedure: k8064 
o|contracted procedure: k8056 
o|contracted procedure: k7989 
o|contracted procedure: k7993 
o|contracted procedure: k8015 
o|contracted procedure: k8027 
o|contracted procedure: k8049 
o|contracted procedure: k8012 
o|contracted procedure: k8002 
o|contracted procedure: k8006 
o|contracted procedure: k8045 
o|contracted procedure: k8030 
o|contracted procedure: k8033 
o|contracted procedure: k8041 
o|propagated global variable: g11341138 chicken.compiler.support#profile-lambda-list 
o|contracted procedure: k8080 
o|contracted procedure: k5969 
o|contracted procedure: k5981 
o|contracted procedure: k6004 
o|contracted procedure: k6012 
o|contracted procedure: k8116 
o|contracted procedure: k8136 
o|contracted procedure: k591919337 
o|contracted procedure: k8132 
o|contracted procedure: k8150 
o|contracted procedure: k8146 
o|contracted procedure: k8162 
o|contracted procedure: k8176 
o|contracted procedure: k8172 
o|contracted procedure: k8187 
o|contracted procedure: k8191 
o|contracted procedure: k591919344 
o|contracted procedure: k8183 
o|contracted procedure: k8202 
o|contracted procedure: k8198 
o|contracted procedure: k8221 
o|contracted procedure: k8227 
o|contracted procedure: k8245 
o|contracted procedure: k8249 
o|contracted procedure: k8262 
o|contracted procedure: k8293 
o|contracted procedure: k8296 
o|contracted procedure: k8308 
o|contracted procedure: k8330 
o|contracted procedure: k8326 
o|contracted procedure: k8311 
o|contracted procedure: k8314 
o|contracted procedure: k8322 
o|contracted procedure: k8352 
o|contracted procedure: k8361 
o|contracted procedure: k8370 
o|contracted procedure: k8379 
o|contracted procedure: k8388 
o|contracted procedure: k8397 
o|contracted procedure: k8424 
o|contracted procedure: k8439 
o|contracted procedure: k8451 
o|contracted procedure: k8465 
o|contracted procedure: k9625 
o|contracted procedure: k8471 
o|contracted procedure: k9621 
o|contracted procedure: k8480 
o|contracted procedure: k8487 
o|contracted procedure: k8490 
o|contracted procedure: k8504 
o|contracted procedure: k8508 
o|contracted procedure: k8520 
o|contracted procedure: k8523 
o|contracted procedure: k8526 
o|contracted procedure: k8534 
o|contracted procedure: k8542 
o|contracted procedure: k8551 
o|contracted procedure: k8554 
o|contracted procedure: k8577 
o|contracted procedure: k8583 
o|contracted procedure: k8594 
o|contracted procedure: k8597 
o|contracted procedure: k8600 
o|contracted procedure: k8606 
o|contracted procedure: k8629 
o|contracted procedure: k8638 
o|contracted procedure: k8641 
o|contracted procedure: k8644 
o|contracted procedure: k8651 
o|contracted procedure: k8664 
o|contracted procedure: k8667 
o|contracted procedure: k8670 
o|contracted procedure: k8678 
o|contracted procedure: k8686 
o|contracted procedure: k6034 
o|contracted procedure: k6042 
o|contracted procedure: k6054 
o|contracted procedure: k6076 
o|contracted procedure: k6072 
o|contracted procedure: k6057 
o|contracted procedure: k6060 
o|contracted procedure: k6068 
o|contracted procedure: k8695 
o|contracted procedure: k8698 
o|contracted procedure: k8726 
o|contracted procedure: k8710 
o|contracted procedure: k8714 
o|contracted procedure: k8722 
o|contracted procedure: k8732 
o|contracted procedure: k8760 
o|contracted procedure: k8764 
o|contracted procedure: k8744 
o|contracted procedure: k8748 
o|contracted procedure: k8756 
o|contracted procedure: k8770 
o|contracted procedure: k8777 
o|contracted procedure: k8781 
o|contracted procedure: k8790 
o|contracted procedure: k8823 
o|contracted procedure: k8802 
o|contracted procedure: k8819 
o|contracted procedure: k8810 
o|contracted procedure: k8902 
o|contracted procedure: k8833 
o|contracted procedure: k8865 
o|contracted procedure: k8845 
o|contracted procedure: k8853 
o|contracted procedure: k8873 
o|contracted procedure: k8898 
o|contracted procedure: k8882 
o|contracted procedure: k8886 
o|contracted procedure: k8916 
o|contracted procedure: k8919 
o|contracted procedure: k8937 
o|contracted procedure: k8942 
o|contracted procedure: k8954 
o|contracted procedure: k8957 
o|contracted procedure: k8960 
o|contracted procedure: k8968 
o|contracted procedure: k8976 
o|contracted procedure: k8992 
o|contracted procedure: k8986 
o|contracted procedure: k8983 
o|contracted procedure: k9003 
o|contracted procedure: k9070 
o|contracted procedure: k9020 
o|contracted procedure: k9024 
o|contracted procedure: k9029 
o|contracted procedure: k9041 
o|contracted procedure: k9044 
o|contracted procedure: k9047 
o|contracted procedure: k9055 
o|contracted procedure: k9063 
o|contracted procedure: k9076 
o|contracted procedure: k9094 
o|contracted procedure: k9110 
o|contracted procedure: k9106 
o|contracted procedure: k9116 
o|contracted procedure: k9119 
o|contracted procedure: k9181 
o|contracted procedure: k9131 
o|contracted procedure: k9135 
o|contracted procedure: k9140 
o|contracted procedure: k9152 
o|contracted procedure: k9155 
o|contracted procedure: k9158 
o|contracted procedure: k9166 
o|contracted procedure: k9174 
o|contracted procedure: k9187 
o|contracted procedure: k9242 
o|contracted procedure: k9190 
o|contracted procedure: k9238 
o|contracted procedure: k9218 
o|contracted procedure: k9234 
o|contracted procedure: k9222 
o|contracted procedure: k5874 
o|contracted procedure: k9230 
o|contracted procedure: k9226 
o|contracted procedure: k9202 
o|contracted procedure: k9206 
o|contracted procedure: k9214 
o|contracted procedure: k9248 
o|contracted procedure: k9263 
o|contracted procedure: k9267 
o|contracted procedure: k9271 
o|contracted procedure: k9274 
o|contracted procedure: k9277 
o|contracted procedure: k9289 
o|contracted procedure: k9292 
o|contracted procedure: k9295 
o|contracted procedure: k9303 
o|contracted procedure: k9311 
o|contracted procedure: k9320 
o|contracted procedure: k9332 
o|contracted procedure: k9336 
o|contracted procedure: k9340 
o|contracted procedure: k9352 
o|contracted procedure: k9355 
o|contracted procedure: k9358 
o|contracted procedure: k9366 
o|contracted procedure: k9374 
o|contracted procedure: k9401 
o|contracted procedure: k9405 
o|contracted procedure: k9408 
o|contracted procedure: k9420 
o|contracted procedure: k9423 
o|contracted procedure: k9426 
o|contracted procedure: k9434 
o|contracted procedure: k9442 
o|contracted procedure: k9484 
o|contracted procedure: k9490 
o|contracted procedure: k9496 
o|contracted procedure: k9528 
o|contracted procedure: k9576 
o|contracted procedure: k9580 
o|contracted procedure: k9592 
o|contracted procedure: k9595 
o|contracted procedure: k9598 
o|contracted procedure: k9606 
o|contracted procedure: k9614 
o|contracted procedure: k9652 
o|contracted procedure: k9660 
o|contracted procedure: k9668 
o|contracted procedure: k9674 
o|contracted procedure: k9684 
o|contracted procedure: k9687 
o|contracted procedure: k9699 
o|contracted procedure: k9702 
o|contracted procedure: k9705 
o|contracted procedure: k9713 
o|contracted procedure: k9721 
o|contracted procedure: k9730 
o|contracted procedure: k9741 
o|contracted procedure: k9744 
o|contracted procedure: k9737 
o|contracted procedure: k9756 
o|contracted procedure: k9759 
o|contracted procedure: k9762 
o|contracted procedure: k9770 
o|contracted procedure: k9778 
o|contracted procedure: k9787 
o|contracted procedure: k9796 
o|contracted procedure: k9799 
o|contracted procedure: k9805 
o|inlining procedure: k9808 
o|contracted procedure: k9816 
o|inlining procedure: k9808 
o|contracted procedure: k9822 
o|contracted procedure: k9834 
o|contracted procedure: k9841 
o|contracted procedure: k9844 
o|contracted procedure: k9853 
o|contracted procedure: k9856 
o|contracted procedure: k9912 
o|contracted procedure: k9876 
o|contracted procedure: k9902 
o|contracted procedure: k9906 
o|contracted procedure: k9898 
o|contracted procedure: k9879 
o|contracted procedure: k9882 
o|contracted procedure: k9890 
o|contracted procedure: k9894 
o|contracted procedure: k9924 
o|contracted procedure: k9927 
o|contracted procedure: k9930 
o|contracted procedure: k9938 
o|contracted procedure: k9946 
o|contracted procedure: k9955 
o|contracted procedure: k9977 
o|contracted procedure: k9962 
o|contracted procedure: k9966 
o|contracted procedure: k9974 
o|contracted procedure: k9983 
o|contracted procedure: k9990 
o|contracted procedure: k9998 
o|contracted procedure: k10004 
o|contracted procedure: k10011 
o|contracted procedure: k10017 
o|contracted procedure: k10024 
o|contracted procedure: k10036 
o|contracted procedure: k10040 
o|contracted procedure: k10049 
o|contracted procedure: k10055 
o|contracted procedure: k10062 
o|contracted procedure: k10070 
o|contracted procedure: k10089 
o|contracted procedure: k10077 
o|contracted procedure: k10097 
o|contracted procedure: k10101 
o|contracted procedure: k10107 
o|contracted procedure: k10110 
o|contracted procedure: k10113 
o|contracted procedure: k10125 
o|contracted procedure: k10128 
o|contracted procedure: k10131 
o|contracted procedure: k10139 
o|contracted procedure: k10147 
o|contracted procedure: k10156 
o|contracted procedure: k10163 
o|contracted procedure: k10167 
o|contracted procedure: k10170 
o|contracted procedure: k10182 
o|contracted procedure: k10185 
o|contracted procedure: k10188 
o|contracted procedure: k10196 
o|contracted procedure: k10204 
o|contracted procedure: k10213 
o|contracted procedure: k10222 
o|contracted procedure: k10229 
o|contracted procedure: k10251 
o|contracted procedure: k10258 
o|contracted procedure: k10262 
o|contracted procedure: k10266 
o|contracted procedure: k10278 
o|contracted procedure: k10292 
o|contracted procedure: k10296 
o|contracted procedure: k10308 
o|contracted procedure: k10311 
o|contracted procedure: k10314 
o|contracted procedure: k10322 
o|contracted procedure: k10330 
o|contracted procedure: k10337 
o|contracted procedure: k10343 
o|contracted procedure: k10350 
o|contracted procedure: k10353 
o|contracted procedure: k10365 
o|contracted procedure: k10368 
o|contracted procedure: k10371 
o|contracted procedure: k10379 
o|contracted procedure: k10387 
o|contracted procedure: k10401 
o|contracted procedure: k10404 
o|contracted procedure: k10416 
o|contracted procedure: k10419 
o|contracted procedure: k10422 
o|contracted procedure: k10430 
o|contracted procedure: k10438 
o|contracted procedure: k10446 
o|contracted procedure: k10452 
o|contracted procedure: k10493 
o|contracted procedure: k10557 
o|contracted procedure: k10520 
o|contracted procedure: k10535 
o|contracted procedure: k10549 
o|contracted procedure: k10553 
o|contracted procedure: k5485 
o|contracted procedure: k5499 
o|contracted procedure: k5509 
o|contracted procedure: k5503 
o|contracted procedure: k10587 
o|contracted procedure: k10595 
o|contracted procedure: k10620 
o|contracted procedure: k10635 
o|contracted procedure: k10641 
o|contracted procedure: k10653 
o|contracted procedure: k10657 
o|contracted procedure: k10664 
o|contracted procedure: k10685 
o|contracted procedure: k10679 
o|contracted procedure: k10725 
o|contracted procedure: k10705 
o|contracted procedure: k10713 
o|contracted procedure: k10709 
o|contracted procedure: k10757 
o|contracted procedure: k10737 
o|contracted procedure: k10745 
o|contracted procedure: k10741 
o|contracted procedure: k10771 
o|contracted procedure: k10789 
o|contracted procedure: k10799 
o|contracted procedure: k10803 
o|contracted procedure: k11409 
o|contracted procedure: k11417 
o|contracted procedure: k11425 
o|contracted procedure: k11431 
o|contracted procedure: k11460 
o|contracted procedure: k11437 
o|contracted procedure: k11452 
o|contracted procedure: k11456 
o|contracted procedure: k11448 
o|contracted procedure: k11466 
o|contracted procedure: k11531 
o|contracted procedure: k11472 
o|contracted procedure: k11475 
o|contracted procedure: k11478 
o|contracted procedure: k11484 
o|contracted procedure: k11495 
o|contracted procedure: k11511 
o|contracted procedure: k11515 
o|contracted procedure: k11507 
o|contracted procedure: k11537 
o|contracted procedure: k11594 
o|contracted procedure: k11543 
o|contracted procedure: k11554 
o|contracted procedure: k11586 
o|contracted procedure: k11590 
o|contracted procedure: k11582 
o|contracted procedure: k11597 
o|contracted procedure: k11609 
o|contracted procedure: k11619 
o|contracted procedure: k11623 
o|contracted procedure: k10816 
o|contracted procedure: k10819 
o|contracted procedure: k10822 
o|contracted procedure: k10830 
o|contracted procedure: k10893 
o|contracted procedure: k10896 
o|contracted procedure: k10899 
o|contracted procedure: k10919 
o|contracted procedure: k10927 
o|contracted procedure: k10935 
o|contracted procedure: k10941 
o|contracted procedure: k10955 
o|contracted procedure: k10958 
o|contracted procedure: k10980 
o|contracted procedure: k10992 
o|contracted procedure: k10996 
o|contracted procedure: k11004 
o|contracted procedure: k11012 
o|contracted procedure: k11018 
o|contracted procedure: k11021 
o|contracted procedure: k591919437 
o|contracted procedure: k11030 
o|contracted procedure: k11045 
o|contracted procedure: k11049 
o|contracted procedure: k11057 
o|contracted procedure: k11061 
o|contracted procedure: k11067 
o|contracted procedure: k11074 
o|contracted procedure: k11080 
o|contracted procedure: k11091 
o|contracted procedure: k11166 
o|contracted procedure: k11174 
o|contracted procedure: k11109 
o|contracted procedure: k11113 
o|contracted procedure: k11121 
o|contracted procedure: k11133 
o|contracted procedure: k11136 
o|contracted procedure: k11139 
o|contracted procedure: k11147 
o|contracted procedure: k11155 
o|contracted procedure: k11185 
o|contracted procedure: k11188 
o|contracted procedure: k11236 
o|contracted procedure: k11200 
o|contracted procedure: k11226 
o|contracted procedure: k11230 
o|contracted procedure: k11222 
o|contracted procedure: k11203 
o|contracted procedure: k11206 
o|contracted procedure: k11214 
o|contracted procedure: k11218 
o|contracted procedure: k11248 
o|contracted procedure: k11251 
o|contracted procedure: k11254 
o|contracted procedure: k11262 
o|contracted procedure: k11270 
o|contracted procedure: k11289 
o|contracted procedure: k11297 
o|contracted procedure: k11309 
o|contracted procedure: k11312 
o|contracted procedure: k11315 
o|contracted procedure: k11323 
o|contracted procedure: k11331 
o|contracted procedure: k11392 
o|contracted procedure: k11356 
o|contracted procedure: k11382 
o|contracted procedure: k11386 
o|contracted procedure: k11378 
o|contracted procedure: k11359 
o|contracted procedure: k11362 
o|contracted procedure: k11370 
o|contracted procedure: k11374 
o|contracted procedure: k10847 
o|contracted procedure: k10850 
o|contracted procedure: k10862 
o|contracted procedure: k10865 
o|contracted procedure: k10868 
o|contracted procedure: k10876 
o|contracted procedure: k10884 
o|contracted procedure: k11647 
o|contracted procedure: k11682 
o|contracted procedure: k11691 
o|contracted procedure: k11700 
o|contracted procedure: k11721 
o|contracted procedure: k11730 
o|contracted procedure: k11739 
o|contracted procedure: k11916 
o|contracted procedure: k11922 
o|contracted procedure: k11938 
o|contracted procedure: k11929 
o|contracted procedure: k11950 
o|contracted procedure: k11975 
o|contracted procedure: k11987 
o|contracted procedure: k11997 
o|contracted procedure: k12001 
o|contracted procedure: k12004 
o|contracted procedure: k12010 
o|contracted procedure: k12048 
o|contracted procedure: k12058 
o|contracted procedure: k12062 
o|contracted procedure: k12078 
o|contracted procedure: k12214 
o|contracted procedure: k12084 
o|contracted procedure: k12090 
o|contracted procedure: k12093 
o|contracted procedure: k12102 
o|contracted procedure: k12194 
o|contracted procedure: k12113 
o|contracted procedure: k12119 
o|contracted procedure: k12139 
o|contracted procedure: k12146 
o|contracted procedure: k12154 
o|contracted procedure: k12150 
o|contracted procedure: k11760 
o|contracted procedure: k11773 
o|contracted procedure: k11777 
o|contracted procedure: k11785 
o|contracted procedure: k11788 
o|contracted procedure: k11764 
o|contracted procedure: k11800 
o|contracted procedure: k11803 
o|contracted procedure: k11806 
o|contracted procedure: k11814 
o|contracted procedure: k11822 
o|contracted procedure: k12169 
o|contracted procedure: k12175 
o|contracted procedure: k12182 
o|contracted procedure: k12205 
o|contracted procedure: k12201 
o|contracted procedure: k12247 
o|contracted procedure: k12271 
o|contracted procedure: k12258 
o|contracted procedure: k12252 
o|contracted procedure: k12279 
o|contracted procedure: k11846 
o|contracted procedure: k11850 
o|contracted procedure: k11854 
o|contracted procedure: k11859 
o|contracted procedure: k11871 
o|contracted procedure: k11874 
o|contracted procedure: k11877 
o|contracted procedure: k11885 
o|contracted procedure: k11893 
o|contracted procedure: k12288 
o|contracted procedure: k12300 
o|contracted procedure: k12309 
o|contracted procedure: k591919487 
o|contracted procedure: k12313 
o|contracted procedure: k12363 
o|contracted procedure: k12325 
o|contracted procedure: k12359 
o|contracted procedure: k12334 
o|contracted procedure: k12351 
o|contracted procedure: k12355 
o|contracted procedure: k12485 
o|contracted procedure: k12372 
o|contracted procedure: k12477 
o|contracted procedure: k12481 
o|contracted procedure: k12381 
o|contracted procedure: k12399 
o|contracted procedure: k12403 
o|contracted procedure: k12412 
o|contracted procedure: k12455 
o|contracted procedure: k12421 
o|contracted procedure: k12430 
o|contracted procedure: k12447 
o|contracted procedure: k12451 
o|contracted procedure: k12464 
o|contracted procedure: k12468 
o|contracted procedure: k12503 
o|contracted procedure: k12512 
o|contracted procedure: k12529 
o|contracted procedure: k12537 
o|contracted procedure: k12543 
o|contracted procedure: k12552 
o|contracted procedure: k12577 
o|contracted procedure: k12555 
o|contracted procedure: k6449 
o|contracted procedure: k6465 
o|contracted procedure: k12583 
o|contracted procedure: k12586 
o|contracted procedure: k12601 
o|contracted procedure: k12607 
o|contracted procedure: k12632 
o|contracted procedure: k12635 
o|contracted procedure: k12744 
o|contracted procedure: k12638 
o|contracted procedure: k12647 
o|contracted procedure: k12661 
o|contracted procedure: k12667 
o|contracted procedure: k12675 
o|contracted procedure: k12678 
o|contracted procedure: k12719 
o|contracted procedure: k12684 
o|contracted procedure: k12710 
o|contracted procedure: k12701 
o|contracted procedure: k12690 
o|contracted procedure: k12697 
o|contracted procedure: k12725 
o|contracted procedure: k12737 
o|contracted procedure: k12771 
o|contracted procedure: k12777 
o|contracted procedure: k12784 
o|contracted procedure: k12812 
o|contracted procedure: k12818 
o|contracted procedure: k12867 
o|contracted procedure: k12840 
o|contracted procedure: k12843 
o|contracted procedure: k12864 
o|contracted procedure: k12857 
o|inlining procedure: k12853 
o|inlining procedure: k12853 
o|contracted procedure: k12894 
o|contracted procedure: k12920 
o|contracted procedure: k12929 
o|contracted procedure: k12938 
o|contracted procedure: k12947 
o|contracted procedure: k12956 
o|contracted procedure: k12966 
o|contracted procedure: k12977 
o|contracted procedure: k12971 
o|contracted procedure: k13052 
o|contracted procedure: k13007 
o|contracted procedure: k13046 
o|contracted procedure: k13010 
o|contracted procedure: k13040 
o|contracted procedure: k13013 
o|contracted procedure: k13034 
o|contracted procedure: k13016 
o|contracted procedure: k13027 
o|contracted procedure: k13023 
o|contracted procedure: k13082 
o|contracted procedure: k13085 
o|contracted procedure: k13097 
o|contracted procedure: k13112 
o|contracted procedure: k13127 
o|contracted procedure: k13130 
o|contracted procedure: k13159 
o|contracted procedure: k13140 
o|contracted procedure: k13148 
o|contracted procedure: k13152 
o|contracted procedure: k13144 
o|contracted procedure: k13165 
o|contracted procedure: k13168 
o|contracted procedure: k13180 
o|contracted procedure: k13213 
o|contracted procedure: k13190 
o|contracted procedure: k13202 
o|contracted procedure: k13194 
o|contracted procedure: k13209 
o|contracted procedure: k13219 
o|contracted procedure: k13229 
o|contracted procedure: k13235 
o|contracted procedure: k13271 
o|contracted procedure: k13248 
o|contracted procedure: k13260 
o|contracted procedure: k13252 
o|contracted procedure: k13267 
o|contracted procedure: k13277 
o|contracted procedure: k13294 
o|contracted procedure: k13290 
o|contracted procedure: k13303 
o|contracted procedure: k13309 
o|contracted procedure: k13337 
o|contracted procedure: k13346 
o|contracted procedure: k13384 
o|contracted procedure: k13352 
o|contracted procedure: k13380 
o|contracted procedure: k13390 
o|contracted procedure: k13419 
o|contracted procedure: k13403 
o|contracted procedure: k13411 
o|contracted procedure: k13415 
o|contracted procedure: k13407 
o|contracted procedure: k13425 
o|contracted procedure: k13434 
o|contracted procedure: k13473 
o|contracted procedure: k13447 
o|contracted procedure: k13459 
o|contracted procedure: k13451 
o|contracted procedure: k13469 
o|contracted procedure: k13479 
o|contracted procedure: k13495 
o|contracted procedure: k13501 
o|contracted procedure: k13511 
o|contracted procedure: k13522 
o|contracted procedure: k13518 
o|contracted procedure: k13537 
o|contracted procedure: k13546 
o|contracted procedure: k13553 
o|contracted procedure: k13582 
o|contracted procedure: k13566 
o|contracted procedure: k13574 
o|contracted procedure: k13578 
o|contracted procedure: k13570 
o|contracted procedure: k13588 
o|contracted procedure: k13591 
o|contracted procedure: k13621 
o|contracted procedure: k13601 
o|contracted procedure: k13617 
o|contracted procedure: k13609 
o|contracted procedure: k13613 
o|contracted procedure: k13605 
o|contracted procedure: k13627 
o|contracted procedure: k13656 
o|contracted procedure: k13637 
o|contracted procedure: k13645 
o|contracted procedure: k13649 
o|contracted procedure: k13641 
o|contracted procedure: k13662 
o|contracted procedure: k13674 
o|contracted procedure: k13681 
o|contracted procedure: k13687 
o|contracted procedure: k13694 
o|contracted procedure: k13700 
o|contracted procedure: k13710 
o|contracted procedure: k13716 
o|contracted procedure: k13719 
o|contracted procedure: k13749 
o|contracted procedure: k13755 
o|contracted procedure: k13772 
o|contracted procedure: k13780 
o|contracted procedure: k13795 
o|contracted procedure: k13801 
o|contracted procedure: k13820 
o|contracted procedure: k13835 
o|contracted procedure: k13841 
o|contracted procedure: k13847 
o|contracted procedure: k13853 
o|contracted procedure: k13874 
o|contracted procedure: k13880 
o|contracted procedure: k13886 
o|contracted procedure: k13892 
o|contracted procedure: k13913 
o|contracted procedure: k13919 
o|contracted procedure: k13925 
o|contracted procedure: k13931 
o|contracted procedure: k13937 
o|contracted procedure: k13943 
o|contracted procedure: k13949 
o|contracted procedure: k13955 
o|contracted procedure: k13984 
o|contracted procedure: k13990 
o|contracted procedure: k13996 
o|contracted procedure: k14002 
o|contracted procedure: k14008 
o|contracted procedure: k14014 
o|contracted procedure: k14020 
o|contracted procedure: k14026 
o|contracted procedure: k14073 
o|contracted procedure: k14088 
o|contracted procedure: k14094 
o|contracted procedure: k14100 
o|contracted procedure: k14106 
o|contracted procedure: k14143 
o|contracted procedure: k14155 
o|contracted procedure: k14164 
o|contracted procedure: k14176 
o|contracted procedure: k14188 
o|contracted procedure: k14203 
o|contracted procedure: k14233 
o|contracted procedure: k14239 
o|contracted procedure: k14269 
o|contracted procedure: k14278 
o|contracted procedure: k14290 
o|contracted procedure: k14302 
o|contracted procedure: k14314 
o|contracted procedure: k14335 
o|contracted procedure: k14344 
o|contracted procedure: k14351 
o|contracted procedure: k14363 
o|contracted procedure: k14370 
o|contracted procedure: k14376 
o|contracted procedure: k14389 
o|contracted procedure: k14395 
o|contracted procedure: k14401 
o|contracted procedure: k14407 
o|contracted procedure: k14413 
o|contracted procedure: k14419 
o|contracted procedure: k14425 
o|contracted procedure: k14455 
o|contracted procedure: k14461 
o|contracted procedure: k14467 
o|contracted procedure: k14484 
o|contracted procedure: k14499 
o|contracted procedure: k14505 
o|contracted procedure: k14511 
o|contracted procedure: k14517 
o|contracted procedure: k14538 
o|contracted procedure: k14544 
o|contracted procedure: k14550 
o|contracted procedure: k14556 
o|contracted procedure: k14562 
o|contracted procedure: k14568 
o|contracted procedure: k14574 
o|contracted procedure: k14580 
o|contracted procedure: k14586 
o|contracted procedure: k14592 
o|contracted procedure: k14625 
o|contracted procedure: k14631 
o|contracted procedure: k14637 
o|contracted procedure: k14643 
o|contracted procedure: k14649 
o|contracted procedure: k14655 
o|contracted procedure: k14661 
o|contracted procedure: k14667 
o|contracted procedure: k14673 
o|contracted procedure: k14679 
o|contracted procedure: k14685 
o|contracted procedure: k14744 
o|contracted procedure: k14756 
o|contracted procedure: k14777 
o|contracted procedure: k14786 
o|contracted procedure: k14793 
o|contracted procedure: k14805 
o|contracted procedure: k14812 
o|contracted procedure: k14820 
o|contracted procedure: k14826 
o|contracted procedure: k14832 
o|contracted procedure: k14838 
o|contracted procedure: k14844 
o|contracted procedure: k14850 
o|contracted procedure: k14856 
o|contracted procedure: k14886 
o|contracted procedure: k14892 
o|contracted procedure: k14898 
o|contracted procedure: k14904 
o|contracted procedure: k14923 
o|contracted procedure: k14929 
o|contracted procedure: k14935 
o|contracted procedure: k14941 
o|contracted procedure: k14947 
o|contracted procedure: k14953 
o|contracted procedure: k14959 
o|contracted procedure: k14965 
o|contracted procedure: k14971 
o|contracted procedure: k14977 
o|contracted procedure: k14983 
o|contracted procedure: k14989 
o|contracted procedure: k14995 
o|contracted procedure: k15001 
o|contracted procedure: k15007 
o|contracted procedure: k15013 
o|contracted procedure: k15019 
o|contracted procedure: k15025 
o|contracted procedure: k15031 
o|contracted procedure: k15037 
o|contracted procedure: k15043 
o|contracted procedure: k15049 
o|contracted procedure: k15055 
o|contracted procedure: k15061 
o|contracted procedure: k15067 
o|contracted procedure: k15073 
o|contracted procedure: k15079 
o|contracted procedure: k15085 
o|contracted procedure: k15091 
o|contracted procedure: k15097 
o|contracted procedure: k15185 
o|contracted procedure: k15188 
o|contracted procedure: k15195 
o|contracted procedure: k15201 
o|contracted procedure: k15208 
o|contracted procedure: k15214 
o|contracted procedure: k15217 
o|contracted procedure: k15224 
o|contracted procedure: k15230 
o|contracted procedure: k15233 
o|contracted procedure: k15240 
o|contracted procedure: k15246 
o|contracted procedure: k15257 
o|contracted procedure: k15253 
o|contracted procedure: k15263 
o|contracted procedure: k15270 
o|contracted procedure: k15276 
o|contracted procedure: k15283 
o|contracted procedure: k15289 
o|contracted procedure: k15302 
o|contracted procedure: k15389 
o|contracted procedure: k15308 
o|contracted procedure: k15311 
o|contracted procedure: k15317 
o|contracted procedure: k15320 
o|contracted procedure: k15358 
o|contracted procedure: k15330 
o|contracted procedure: k15354 
o|contracted procedure: k15338 
o|contracted procedure: k15346 
o|contracted procedure: k15350 
o|contracted procedure: k15342 
o|contracted procedure: k15334 
o|contracted procedure: k15364 
o|contracted procedure: k15371 
o|contracted procedure: k15375 
o|contracted procedure: k15412 
o|contracted procedure: k15392 
o|contracted procedure: k15408 
o|contracted procedure: k15398 
o|contracted procedure: k15402 
o|contracted procedure: k15462 
o|contracted procedure: k15468 
o|contracted procedure: k15471 
o|contracted procedure: k15477 
o|contracted procedure: k15486 
o|contracted procedure: k15489 
o|contracted procedure: k15495 
o|contracted procedure: k15503 
o|contracted procedure: k15506 
o|contracted procedure: k15512 
o|contracted procedure: k15518 
o|contracted procedure: k15526 
o|contracted procedure: k15532 
o|contracted procedure: k15538 
o|contracted procedure: k15546 
o|contracted procedure: k15552 
o|contracted procedure: k15561 
o|contracted procedure: k15568 
o|contracted procedure: k15579 
o|contracted procedure: k15585 
o|contracted procedure: k15591 
o|contracted procedure: k15597 
o|contracted procedure: k15603 
o|contracted procedure: k15609 
o|contracted procedure: k15615 
o|contracted procedure: k15621 
o|contracted procedure: k15627 
o|contracted procedure: k15633 
o|contracted procedure: k15639 
o|contracted procedure: k15648 
o|contracted procedure: k15654 
o|contracted procedure: k15660 
o|contracted procedure: k15669 
o|contracted procedure: k15672 
o|contracted procedure: k15678 
o|contracted procedure: k15687 
o|contracted procedure: k15693 
o|contracted procedure: k15700 
o|contracted procedure: k15709 
o|contracted procedure: k15716 
o|contracted procedure: k15722 
o|contracted procedure: k15728 
o|contracted procedure: k15731 
o|contracted procedure: k15745 
o|contracted procedure: k15751 
o|contracted procedure: k15770 
o|contracted procedure: k15792 
o|contracted procedure: k15798 
o|contracted procedure: k15819 
o|contracted procedure: k15825 
o|contracted procedure: k15831 
o|contracted procedure: k15837 
o|contracted procedure: k15843 
o|contracted procedure: k15849 
o|contracted procedure: k15855 
o|contracted procedure: k15902 
o|contracted procedure: k15908 
o|contracted procedure: k15914 
o|contracted procedure: k15920 
o|contracted procedure: k15926 
o|contracted procedure: k15932 
o|contracted procedure: k15938 
o|contracted procedure: k15944 
o|contracted procedure: k15995 
o|contracted procedure: k16001 
o|contracted procedure: k16007 
o|contracted procedure: k16013 
o|contracted procedure: k16019 
o|contracted procedure: k16025 
o|contracted procedure: k16062 
o|contracted procedure: k16068 
o|contracted procedure: k16088 
o|contracted procedure: k16096 
o|contracted procedure: k16102 
o|contracted procedure: k16105 
o|contracted procedure: k16166 
o|contracted procedure: k16108 
o|contracted procedure: k16114 
o|contracted procedure: k16126 
o|contracted procedure: k16136 
o|contracted procedure: k16140 
o|contracted procedure: k16147 
o|contracted procedure: k16150 
o|contracted procedure: k16157 
o|contracted procedure: k16172 
o|contracted procedure: k16178 
o|contracted procedure: k16190 
o|contracted procedure: k16200 
o|contracted procedure: k16204 
o|contracted procedure: k16207 
o|contracted procedure: k16240 
o|contracted procedure: k16248 
o|contracted procedure: k16256 
o|contracted procedure: k16262 
o|contracted procedure: k16271 
o|contracted procedure: k16274 
o|contracted procedure: k16280 
o|contracted procedure: k16300 
o|contracted procedure: k16303 
o|contracted procedure: k16316 
o|contracted procedure: k1631319517 
o|contracted procedure: k1631319521 
o|contracted procedure: k16326 
o|contracted procedure: k16336 
o|contracted procedure: k16344 
o|contracted procedure: k16350 
o|contracted procedure: k16357 
o|contracted procedure: k16367 
o|contracted procedure: k16385 
o|contracted procedure: k16391 
o|contracted procedure: k16397 
o|contracted procedure: k16424 
o|contracted procedure: k16436 
o|contracted procedure: k16446 
o|contracted procedure: k16450 
o|contracted procedure: k16463 
o|contracted procedure: k16478 
o|contracted procedure: k16498 
o|contracted procedure: k16541 
o|inlining procedure: k16538 
o|contracted procedure: k16595 
o|contracted procedure: k16604 
o|contracted procedure: k16612 
o|contracted procedure: k16625 
o|contracted procedure: k16636 
o|contracted procedure: k16648 
o|contracted procedure: k16662 
o|contracted procedure: k16666 
o|contracted procedure: k16733 
o|contracted procedure: k16736 
o|contracted procedure: k16739 
o|contracted procedure: k16756 
o|contracted procedure: k16768 
o|contracted procedure: k16783 
o|contracted procedure: k16823 
o|contracted procedure: k16816 
o|contracted procedure: k16792 
o|contracted procedure: k16804 
o|contracted procedure: k16807 
o|contracted procedure: k16810 
o|contracted procedure: k16829 
o|contracted procedure: k16846 
o|contracted procedure: k16974 
o|contracted procedure: k16982 
o|contracted procedure: k16852 
o|contracted procedure: k16855 
o|contracted procedure: k16861 
o|contracted procedure: k16892 
o|contracted procedure: k16898 
o|contracted procedure: k16922 
o|contracted procedure: k16910 
o|contracted procedure: k16917 
o|contracted procedure: k16994 
o|contracted procedure: k17016 
o|contracted procedure: k17012 
o|contracted procedure: k16997 
o|contracted procedure: k17000 
o|contracted procedure: k17008 
o|contracted procedure: k17028 
o|contracted procedure: k17050 
o|contracted procedure: k16843 
o|contracted procedure: k17046 
o|contracted procedure: k17031 
o|contracted procedure: k17034 
o|contracted procedure: k17042 
o|contracted procedure: k17071 
o|contracted procedure: k17152 
o|contracted procedure: k17148 
o|contracted procedure: k17077 
o|contracted procedure: k17134 
o|contracted procedure: k17080 
o|inlining procedure: k17117 
o|contracted procedure: k17192 
o|contracted procedure: k17238 
o|contracted procedure: k17247 
o|contracted procedure: k17250 
o|contracted procedure: k17257 
o|contracted procedure: k17276 
o|contracted procedure: k6336 
o|contracted procedure: k6351 
o|contracted procedure: k17309 
o|contracted procedure: k17317 
o|contracted procedure: k17325 
o|contracted procedure: k17359 
o|contracted procedure: k17365 
o|contracted procedure: k17374 
o|contracted procedure: k17398 
o|contracted procedure: k17414 
o|contracted procedure: k17418 
o|contracted procedure: k17422 
o|contracted procedure: k17431 
o|contracted procedure: k17441 
o|contracted procedure: k17445 
o|contracted procedure: k17461 
o|contracted procedure: k591919563 
o|inlining procedure: k17465 
o|contracted procedure: k591919570 
o|inlining procedure: k17465 
o|contracted procedure: k17481 
o|contracted procedure: k17488 
o|contracted procedure: k17503 
o|contracted procedure: k17516 
o|contracted procedure: k17540 
o|contracted procedure: k17568 
o|contracted procedure: k17597 
o|contracted procedure: k17606 
o|contracted procedure: k17621 
o|contracted procedure: k17627 
o|contracted procedure: k17650 
o|contracted procedure: k17644 
o|contracted procedure: k17670 
o|contracted procedure: k17664 
o|contracted procedure: k17710 
o|contracted procedure: k17716 
o|contracted procedure: k17735 
o|contracted procedure: k17729 
o|contracted procedure: k17820 
o|contracted procedure: k17832 
o|contracted procedure: k17842 
o|contracted procedure: k17846 
o|contracted procedure: k17794 
o|contracted procedure: k17808 
o|contracted procedure: k17812 
o|contracted procedure: k17894 
o|contracted procedure: k17878 
o|contracted procedure: k17945 
o|contracted procedure: k17954 
o|simplifications: ((if . 7) (let . 312)) 
o|removed binding forms: 1233 
o|contracted procedure: k7296 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest952953 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest952953 0 
o|contracted procedure: k8233 
o|contracted procedure: k9808 
o|inlining procedure: k10661 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest29502953 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest29502953 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest29502953 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest29502953 0 
o|contracted procedure: k16143 
o|contracted procedure: k17454 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest44244427 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest44244427 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest45134514 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest45134514 0 
o|simplifications: ((let . 1)) 
o|replaced variables: 3 
o|removed binding forms: 8 
(o x)|known list op on rest arg sublist: ##core#rest-null? r13011 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r13011 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r13011 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r13011 1 
o|removed binding forms: 3 
o|removed binding forms: 2 
o|direct leaf routine/allocation: loop444 0 
o|direct leaf routine/allocation: loop844 0 
o|direct leaf routine/allocation: loop1089 0 
o|direct leaf routine/allocation: a8101 0 
o|direct leaf routine/allocation: g404405 3 
o|direct leaf routine/allocation: g27402741 0 
o|direct leaf routine/allocation: a17286 0 
o|converted assignments to bindings: (loop444) 
o|converted assignments to bindings: (loop844) 
o|converted assignments to bindings: (loop1089) 
o|contracted procedure: "(mini-srfi-1.scm:135) k5986" 
o|contracted procedure: "(mini-srfi-1.scm:190) k6343" 
o|simplifications: ((let . 3)) 
o|removed binding forms: 2 
o|customizable procedures: (for-each-loop44874502 loop4351 g42754289 for-each-loop42744299 doloop43114312 loop4252 loop539 tmp15275 tmp25276 map-loop41134134 map-loop41444165 resolve4046 loop4058 g39803987 for-each-loop39793990 k16265 walkeach3935 walk3934 chicken.compiler.support#lset-adjoin/eq? k16175 for-each-loop39123922 k16111 walk3863 for-each-loop38873898 k15480 k15555 k15642 k15663 k15681 k15703 k15295 k14747 k14759 k14796 g36013602 k14272 k14281 k14293 k14305 k14317 k14354 err3327 g34433444 g33223323 chicken.compiler.support#foreign-type-argument-converter chicken.compiler.support#foreign-type-result-converter chicken.compiler.support#follow-without-loop k13100 k13115 k13238 k13280 k13306 k13349 k13393 k13437 k13482 k13556 repeat2975 g32273228 k13455 k13256 k13198 k12803 k12762 k12546 a12561 loop567 matchn2731 loop2760 match12730 resolve2729 walk2543 map-loop25522569 loop2706 k12096 k12133 uses-foreign-stubs?2584 walk2500 map-loop25132533 for-each-loop26562668 for-each-loop26782696 rec2464 map-loop20632080 map-loop21762195 g23692378 map-loop23632388 g22632272 map-loop22572277 map-loop22872306 g23302339 map-loop23242349 walk2206 map-loop20912109 walk2403 for-each-loop24462456 lp2434 g21192126 for-each-loop21182130 loop2137 loop22140 loop211 fold2040 k9677 k10281 map-loop20122029 map-loop19862003 map-loop19601977 loop1941 map-loop19171934 chicken.compiler.support#cons* map-loop18911908 loop1882 map-loop18421859 map-loop18211866 chicken.compiler.support#last map-loop17791796 walk1722 map-loop17501767 map-loop16931710 k9006 k9251 k9449 map-loop16621679 map-loop16161633 map-loop15851602 map-loop15361553 map-loop14921509 k8933 map-loop14551472 loop1422 map-loop417435 g13901399 map-loop13841402 k8564 walk1322 map-loop13471364 map-loop12171234 g12051206 foldr389392 g394395 map-loop11221143 tmp14905 tmp24906 loop1072 k7702 loop1043 k7574 doloop959960 loop939 fold932 chicken.compiler.support#every k7148 k7155 loop875 loop859 k6883 g781806 for-each-loop780816 collect732 g737744 for-each-loop736755 text681 chicken.compiler.support#test-debugging-mode dump682 for-each-loop685702 chicken.compiler.support#any loop461 loop321 loop308 loop295 chicken.compiler.support#take) 
o|calls to known targets: 528 
o|identified direct recursive calls: f_5422 1 
o|identified direct recursive calls: f_5753 1 
o|identified direct recursive calls: f_6089 1 
o|identified direct recursive calls: f_6148 2 
o|identified direct recursive calls: f_7038 1 
o|identified direct recursive calls: f_7079 1 
o|identified direct recursive calls: f_7345 1 
o|unused rest argument: rest952953 f_7436 
o|identified direct recursive calls: f_7880 1 
o|identified direct recursive calls: f_8022 1 
o|identified direct recursive calls: f_5976 1 
o|identified direct recursive calls: f_8303 1 
o|identified direct recursive calls: f_6049 1 
o|identified direct recursive calls: f_8446 4 
o|identified direct recursive calls: f_9871 1 
o|identified direct recursive calls: f_5480 1 
o|identified direct recursive calls: f_10630 1 
o|identified direct recursive calls: f_10615 1 
o|identified direct recursive calls: f_11562 1 
o|identified direct recursive calls: f_11195 1 
o|identified direct recursive calls: f_10912 1 
o|identified direct recursive calls: f_11351 1 
o|identified direct recursive calls: f_11642 1 
o|identified direct recursive calls: f_12320 1 
o|unused rest argument: rest29502953 f_13005 
o|identified direct recursive calls: f_16989 1 
o|identified direct recursive calls: f_17023 1 
o|identified direct recursive calls: f_6331 1 
o|unused rest argument: rest44244427 f_17727 
o|unused rest argument: rest45134514 f_17876 
o|fast box initializations: 93 
o|fast global references: 63 
o|fast global assignments: 22 
o|dropping unused closure argument: f_14141 
o|dropping unused closure argument: f_14162 
o|dropping unused closure argument: f_16575 
o|dropping unused closure argument: f_5422 
o|dropping unused closure argument: f_5683 
o|dropping unused closure argument: f_5717 
o|dropping unused closure argument: f_5747 
o|dropping unused closure argument: f_6083 
o|dropping unused closure argument: f_6089 
o|dropping unused closure argument: f_6142 
o|dropping unused closure argument: f_6627 
o|dropping unused closure argument: f_7038 
o|dropping unused closure argument: f_7385 
o|dropping unused closure argument: f_7880 
*/
/* end of file */
