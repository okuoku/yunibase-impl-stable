/* Generated from expand.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: expand.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -explicit-use -no-trace -output-file expand.c -no-module-registration
   unit: expand
   uses: internal library
*/
#include "chicken.h"

static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_internal_toplevel)
C_externimport void C_ccall C_internal_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[375];
static double C_possibly_force_alignment;
static C_char C_TLS li0[] C_aligned={C_lihdr(0,0,26),40,99,104,105,99,107,101,110,46,115,121,110,116,97,120,35,108,111,111,107,117,112,32,115,101,41,0,0,0,0,0,0};
static C_char C_TLS li1[] C_aligned={C_lihdr(0,0,35),40,99,104,105,99,107,101,110,46,115,121,110,116,97,120,35,109,97,99,114,111,45,97,108,105,97,115,32,118,97,114,32,115,101,41,0,0,0,0,0};
static C_char C_TLS li2[] C_aligned={C_lihdr(0,0,13),40,100,111,108,111,111,112,51,56,52,32,105,41,0,0,0};
static C_char C_TLS li3[] C_aligned={C_lihdr(0,0,8),40,119,97,108,107,32,120,41};
static C_char C_TLS li4[] C_aligned={C_lihdr(0,0,33),40,99,104,105,99,107,101,110,46,115,121,110,116,97,120,35,115,116,114,105,112,45,115,121,110,116,97,120,32,101,120,112,41,0,0,0,0,0,0,0};
static C_char C_TLS li5[] C_aligned={C_lihdr(0,0,23),40,109,97,112,45,108,111,111,112,52,54,57,32,103,52,56,49,32,103,52,56,50,41,0};
static C_char C_TLS li6[] C_aligned={C_lihdr(0,0,28),40,102,111,114,45,101,97,99,104,45,108,111,111,112,52,51,50,32,103,52,51,57,32,103,52,52,48,41,0,0,0,0};
static C_char C_TLS li7[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,52,48,54,32,103,52,49,56,41,0,0,0,0,0,0};
static C_char C_TLS li8[] C_aligned={C_lihdr(0,0,32),40,35,35,115,121,115,35,101,120,116,101,110,100,45,115,101,32,115,101,32,118,97,114,115,32,46,32,114,101,115,116,41};
static C_char C_TLS li9[] C_aligned={C_lihdr(0,0,8),40,103,53,49,54,32,97,41};
static C_char C_TLS li10[] C_aligned={C_lihdr(0,0,9),40,108,111,111,112,32,115,101,41,0,0,0,0,0,0,0};
static C_char C_TLS li11[] C_aligned={C_lihdr(0,0,11),40,108,111,111,112,49,32,115,121,109,41,0,0,0,0,0};
static C_char C_TLS li12[] C_aligned={C_lihdr(0,0,24),40,35,35,115,121,115,35,103,108,111,98,97,108,105,122,101,32,115,121,109,32,115,101,41};
static C_char C_TLS li13[] C_aligned={C_lihdr(0,0,35),40,35,35,115,121,115,35,101,110,115,117,114,101,45,116,114,97,110,115,102,111,114,109,101,114,32,116,32,46,32,114,101,115,116,41,0,0,0,0,0};
static C_char C_TLS li14[] C_aligned={C_lihdr(0,0,6),40,103,53,54,49,41,0,0};
static C_char C_TLS li15[] C_aligned={C_lihdr(0,0,52),40,35,35,115,121,115,35,101,120,116,101,110,100,45,109,97,99,114,111,45,101,110,118,105,114,111,110,109,101,110,116,32,110,97,109,101,32,115,101,32,116,114,97,110,115,102,111,114,109,101,114,41,0,0,0,0};
static C_char C_TLS li16[] C_aligned={C_lihdr(0,0,25),40,35,35,115,121,115,35,109,97,99,114,111,63,32,115,121,109,32,46,32,114,101,115,116,41,0,0,0,0,0,0,0};
static C_char C_TLS li17[] C_aligned={C_lihdr(0,0,9),40,108,111,111,112,32,109,101,41,0,0,0,0,0,0,0};
static C_char C_TLS li18[] C_aligned={C_lihdr(0,0,28),40,35,35,115,121,115,35,117,110,100,101,102,105,110,101,45,109,97,99,114,111,33,32,110,97,109,101,41,0,0,0,0};
static C_char C_TLS li19[] C_aligned={C_lihdr(0,0,9),40,99,111,112,121,32,112,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li20[] C_aligned={C_lihdr(0,0,7),40,97,52,51,55,53,41,0};
static C_char C_TLS li21[] C_aligned={C_lihdr(0,0,10),40,97,52,51,54,57,32,101,120,41,0,0,0,0,0,0};
static C_char C_TLS li22[] C_aligned={C_lihdr(0,0,14),40,102,95,52,53,49,49,32,105,110,112,117,116,41,0,0};
static C_char C_TLS li23[] C_aligned={C_lihdr(0,0,7),40,97,52,53,49,54,41,0};
static C_char C_TLS li24[] C_aligned={C_lihdr(0,0,7),40,97,52,53,50,49,41,0};
static C_char C_TLS li25[] C_aligned={C_lihdr(0,0,7),40,97,52,53,50,55,41,0};
static C_char C_TLS li26[] C_aligned={C_lihdr(0,0,7),40,97,52,52,55,54,41,0};
static C_char C_TLS li27[] C_aligned={C_lihdr(0,0,7),40,97,52,53,52,49,41,0};
static C_char C_TLS li28[] C_aligned={C_lihdr(0,0,14),40,97,52,53,51,53,32,46,32,97,114,103,115,41,0,0};
static C_char C_TLS li29[] C_aligned={C_lihdr(0,0,7),40,97,52,52,55,48,41,0};
static C_char C_TLS li30[] C_aligned={C_lihdr(0,0,9),40,97,52,51,54,51,32,107,41,0,0,0,0,0,0,0};
static C_char C_TLS li31[] C_aligned={C_lihdr(0,0,37),40,99,97,108,108,45,104,97,110,100,108,101,114,32,110,97,109,101,32,104,97,110,100,108,101,114,32,101,120,112,32,115,101,32,99,115,41,0,0,0};
static C_char C_TLS li32[] C_aligned={C_lihdr(0,0,22),40,101,120,112,97,110,100,32,104,101,97,100,32,101,120,112,32,109,100,101,102,41,0,0};
static C_char C_TLS li33[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,55,48,57,32,103,55,50,49,41,0,0,0,0,0,0};
static C_char C_TLS li34[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,54,56,50,32,103,54,57,52,41,0,0,0,0,0,0};
static C_char C_TLS li35[] C_aligned={C_lihdr(0,0,9),40,103,55,52,50,32,99,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li36[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,32,101,120,112,41,0,0,0,0,0,0};
static C_char C_TLS li37[] C_aligned={C_lihdr(0,0,28),40,35,35,115,121,115,35,101,120,112,97,110,100,45,48,32,101,120,112,32,100,115,101,32,99,115,63,41,0,0,0,0};
static C_char C_TLS li38[] C_aligned={C_lihdr(0,0,51),40,99,104,105,99,107,101,110,46,115,121,110,116,97,120,35,101,120,112,97,110,115,105,111,110,45,114,101,115,117,108,116,45,104,111,111,107,32,105,110,112,117,116,32,111,117,116,112,117,116,41,0,0,0,0,0};
static C_char C_TLS li39[] C_aligned={C_lihdr(0,0,7),40,97,52,56,55,48,41,0};
static C_char C_TLS li40[] C_aligned={C_lihdr(0,0,20),40,97,52,56,55,54,32,101,120,112,50,55,55,57,32,109,55,56,49,41,0,0,0,0};
static C_char C_TLS li41[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,32,101,120,112,41,0,0,0,0,0,0};
static C_char C_TLS li42[] C_aligned={C_lihdr(0,0,34),40,99,104,105,99,107,101,110,46,115,121,110,116,97,120,35,101,120,112,97,110,100,32,101,120,112,32,46,32,114,101,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li43[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li44[] C_aligned={C_lihdr(0,0,35),40,35,35,115,121,115,35,101,120,116,101,110,100,101,100,45,108,97,109,98,100,97,45,108,105,115,116,63,32,108,108,105,115,116,41,0,0,0,0,0};
static C_char C_TLS li45[] C_aligned={C_lihdr(0,0,9),40,101,114,114,32,109,115,103,41,0,0,0,0,0,0,0};
static C_char C_TLS li46[] C_aligned={C_lihdr(0,0,8),40,103,56,52,53,32,107,41};
static C_char C_TLS li47[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,56,51,57,32,103,56,53,49,41,0,0,0,0,0,0};
static C_char C_TLS li48[] C_aligned={C_lihdr(0,0,29),40,108,111,111,112,32,109,111,100,101,32,114,101,113,32,111,112,116,32,107,101,121,32,108,108,105,115,116,41,0,0,0};
static C_char C_TLS li49[] C_aligned={C_lihdr(0,0,55),40,35,35,115,121,115,35,101,120,112,97,110,100,45,101,120,116,101,110,100,101,100,45,108,97,109,98,100,97,45,108,105,115,116,32,108,108,105,115,116,48,32,98,111,100,121,32,101,114,114,104,32,115,101,41,0};
static C_char C_TLS li50[] C_aligned={C_lihdr(0,0,23),40,109,97,112,45,108,111,111,112,57,55,54,32,103,57,56,56,32,103,57,56,57,41,0};
static C_char C_TLS li51[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,57,52,55,32,103,57,53,57,41,0,0,0,0,0,0};
static C_char C_TLS li52[] C_aligned={C_lihdr(0,0,22),40,97,53,53,55,52,32,118,97,114,115,32,97,114,103,99,32,114,101,115,116,41,0,0};
static C_char C_TLS li53[] C_aligned={C_lihdr(0,0,54),40,35,35,115,121,115,35,101,120,112,97,110,100,45,109,117,108,116,105,112,108,101,45,118,97,108,117,101,115,45,97,115,115,105,103,110,109,101,110,116,32,102,111,114,109,97,108,115,32,101,120,112,114,41,0,0};
static C_char C_TLS li54[] C_aligned={C_lihdr(0,0,9),40,99,111,109,112,32,105,100,41,0,0,0,0,0,0,0};
static C_char C_TLS li55[] C_aligned={C_lihdr(0,0,12),40,108,111,111,112,50,32,98,111,100,121,41,0,0,0,0};
static C_char C_TLS li56[] C_aligned={C_lihdr(0,0,16),40,108,111,111,112,32,98,111,100,121,32,101,120,112,115,41};
static C_char C_TLS li57[] C_aligned={C_lihdr(0,0,32),40,109,97,112,45,108,111,111,112,49,49,53,53,32,103,49,49,54,55,32,103,49,49,54,56,32,103,49,49,54,57,41};
static C_char C_TLS li58[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,49,48,55,32,103,49,49,49,57,41,0,0,0,0};
static C_char C_TLS li59[] C_aligned={C_lihdr(0,0,13),40,97,54,50,54,55,32,97,32,95,32,95,41,0,0,0};
static C_char C_TLS li60[] C_aligned={C_lihdr(0,0,23),40,102,111,108,100,108,49,49,51,48,32,103,49,49,51,49,32,103,49,49,50,57,41,0};
static C_char C_TLS li61[] C_aligned={C_lihdr(0,0,27),40,102,105,110,105,32,118,97,114,115,32,118,97,108,115,32,109,118,97,114,115,32,98,111,100,121,41,0,0,0,0,0};
static C_char C_TLS li62[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,50,49,53,32,103,49,50,50,55,41,0,0,0,0};
static C_char C_TLS li63[] C_aligned={C_lihdr(0,0,21),40,108,111,111,112,32,98,111,100,121,32,100,101,102,115,32,100,111,110,101,41,0,0,0};
static C_char C_TLS li64[] C_aligned={C_lihdr(0,0,34),40,102,105,110,105,47,115,121,110,116,97,120,32,118,97,114,115,32,118,97,108,115,32,109,118,97,114,115,32,98,111,100,121,41,0,0,0,0,0,0};
static C_char C_TLS li65[] C_aligned={C_lihdr(0,0,9),40,108,111,111,112,50,32,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li66[] C_aligned={C_lihdr(0,0,27),40,108,111,111,112,32,98,111,100,121,32,118,97,114,115,32,118,97,108,115,32,109,118,97,114,115,41,0,0,0,0,0};
static C_char C_TLS li67[] C_aligned={C_lihdr(0,0,13),40,101,120,112,97,110,100,32,98,111,100,121,41,0,0,0};
static C_char C_TLS li68[] C_aligned={C_lihdr(0,0,37),40,35,35,115,121,115,35,99,97,110,111,110,105,99,97,108,105,122,101,45,98,111,100,121,32,98,111,100,121,32,46,32,114,101,115,116,41,0,0,0};
static C_char C_TLS li69[] C_aligned={C_lihdr(0,0,7),40,103,49,51,48,53,41,0};
static C_char C_TLS li70[] C_aligned={C_lihdr(0,0,11),40,109,119,97,108,107,32,120,32,112,41,0,0,0,0,0};
static C_char C_TLS li71[] C_aligned={C_lihdr(0,0,46),40,99,104,105,99,107,101,110,46,115,121,110,116,97,120,35,109,97,116,99,104,45,101,120,112,114,101,115,115,105,111,110,32,101,120,112,32,112,97,116,32,118,97,114,115,41,0,0};
static C_char C_TLS li72[] C_aligned={C_lihdr(0,0,16),40,108,111,111,112,32,104,101,97,100,32,98,111,100,121,41};
static C_char C_TLS li73[] C_aligned={C_lihdr(0,0,51),40,99,104,105,99,107,101,110,46,115,121,110,116,97,120,35,101,120,112,97,110,100,45,99,117,114,114,105,101,100,45,100,101,102,105,110,101,32,104,101,97,100,32,98,111,100,121,32,115,101,41,0,0,0,0,0};
static C_char C_TLS li74[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,115,121,110,116,97,120,35,115,121,110,116,97,120,45,101,114,114,111,114,32,46,32,97,114,103,115,41,0,0,0,0};
static C_char C_TLS li75[] C_aligned={C_lihdr(0,0,12),40,111,117,116,115,116,114,32,115,116,114,41,0,0,0,0};
static C_char C_TLS li76[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,32,108,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li77[] C_aligned={C_lihdr(0,0,11),40,108,111,111,112,32,100,101,102,115,41,0,0,0,0,0};
static C_char C_TLS li78[] C_aligned={C_lihdr(0,0,9),40,108,111,111,112,32,99,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li79[] C_aligned={C_lihdr(0,0,36),40,35,35,115,121,115,35,115,121,110,116,97,120,45,101,114,114,111,114,47,99,111,110,116,101,120,116,32,109,115,103,32,97,114,103,41,0,0,0,0};
static C_char C_TLS li80[] C_aligned={C_lihdr(0,0,7),40,103,49,51,57,52,41,0};
static C_char C_TLS li81[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,115,121,110,116,97,120,35,103,101,116,45,108,105,110,101,45,110,117,109,98,101,114,32,115,101,120,112,41,0,0,0};
static C_char C_TLS li82[] C_aligned={C_lihdr(0,0,17),40,116,101,115,116,32,120,32,112,114,101,100,32,109,115,103,41,0,0,0,0,0,0,0};
static C_char C_TLS li83[] C_aligned={C_lihdr(0,0,9),40,101,114,114,32,109,115,103,41,0,0,0,0,0,0,0};
static C_char C_TLS li84[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li85[] C_aligned={C_lihdr(0,0,16),40,108,97,109,98,100,97,45,108,105,115,116,63,32,120,41};
static C_char C_TLS li86[] C_aligned={C_lihdr(0,0,13),40,118,97,114,105,97,98,108,101,63,32,118,41,0,0,0};
static C_char C_TLS li87[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li88[] C_aligned={C_lihdr(0,0,16),40,112,114,111,112,101,114,45,108,105,115,116,63,32,120,41};
static C_char C_TLS li89[] C_aligned={C_lihdr(0,0,16),40,100,111,108,111,111,112,49,52,55,51,32,120,32,110,41};
static C_char C_TLS li90[] C_aligned={C_lihdr(0,0,9),40,97,55,52,57,54,32,121,41,0,0,0,0,0,0,0};
static C_char C_TLS li91[] C_aligned={C_lihdr(0,0,10),40,119,97,108,107,32,120,32,112,41,0,0,0,0,0,0};
static C_char C_TLS li92[] C_aligned={C_lihdr(0,0,38),40,35,35,115,121,115,35,99,104,101,99,107,45,115,121,110,116,97,120,32,105,100,32,101,120,112,32,112,97,116,32,46,32,114,101,115,116,41,0,0};
static C_char C_TLS li93[] C_aligned={C_lihdr(0,0,12),40,114,101,110,97,109,101,32,115,121,109,41,0,0,0,0};
static C_char C_TLS li94[] C_aligned={C_lihdr(0,0,16),40,100,111,108,111,111,112,49,53,52,55,32,105,32,102,41};
static C_char C_TLS li95[] C_aligned={C_lihdr(0,0,7),40,103,49,53,57,48,41,0};
static C_char C_TLS li96[] C_aligned={C_lihdr(0,0,7),40,103,49,53,57,57,41,0};
static C_char C_TLS li97[] C_aligned={C_lihdr(0,0,15),40,99,111,109,112,97,114,101,32,115,49,32,115,50,41,0};
static C_char C_TLS li98[] C_aligned={C_lihdr(0,0,16),40,97,115,115,113,45,114,101,118,101,114,115,101,32,108,41};
static C_char C_TLS li99[] C_aligned={C_lihdr(0,0,19),40,109,105,114,114,111,114,45,114,101,110,97,109,101,32,115,121,109,41,0,0,0,0,0};
static C_char C_TLS li100[] C_aligned={C_lihdr(0,0,19),40,97,55,54,49,50,32,102,111,114,109,32,115,101,32,100,115,101,41,0,0,0,0,0};
static C_char C_TLS li101[] C_aligned={C_lihdr(0,0,66),40,99,104,105,99,107,101,110,46,115,121,110,116,97,120,35,109,97,107,101,45,101,114,47,105,114,45,116,114,97,110,115,102,111,114,109,101,114,32,104,97,110,100,108,101,114,32,101,120,112,108,105,99,105,116,45,114,101,110,97,109,105,110,103,63,41,0,0,0,0,0,0};
static C_char C_TLS li102[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,115,121,110,116,97,120,35,101,114,45,109,97,99,114,111,45,116,114,97,110,115,102,111,114,109,101,114,32,104,97,110,100,108,101,114,41,0,0,0};
static C_char C_TLS li103[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,115,121,110,116,97,120,35,105,114,45,109,97,99,114,111,45,116,114,97,110,115,102,111,114,109,101,114,32,104,97,110,100,108,101,114,41,0,0,0};
static C_char C_TLS li104[] C_aligned={C_lihdr(0,0,59),40,99,104,105,99,107,101,110,46,105,110,116,101,114,110,97,108,46,115,121,110,116,97,120,45,114,117,108,101,115,35,115,121,110,116,97,120,45,114,117,108,101,115,45,109,105,115,109,97,116,99,104,32,105,110,112,117,116,41,0,0,0,0,0};
static C_char C_TLS li105[] C_aligned={C_lihdr(0,0,16),40,108,111,111,112,32,108,101,110,32,105,110,112,117,116,41};
static C_char C_TLS li106[] C_aligned={C_lihdr(0,0,53),40,99,104,105,99,107,101,110,46,105,110,116,101,114,110,97,108,46,115,121,110,116,97,120,45,114,117,108,101,115,35,100,114,111,112,45,114,105,103,104,116,32,105,110,112,117,116,32,116,101,109,112,41,0,0,0};
static C_char C_TLS li107[] C_aligned={C_lihdr(0,0,12),40,108,111,111,112,32,105,110,112,117,116,41,0,0,0,0};
static C_char C_TLS li108[] C_aligned={C_lihdr(0,0,53),40,99,104,105,99,107,101,110,46,105,110,116,101,114,110,97,108,46,115,121,110,116,97,120,45,114,117,108,101,115,35,116,97,107,101,45,114,105,103,104,116,32,105,110,112,117,116,32,116,101,109,112,41,0,0,0};
static C_char C_TLS li109[] C_aligned={C_lihdr(0,0,10),40,102,95,56,52,48,51,32,120,41,0,0,0,0,0,0};
static C_char C_TLS li110[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,54,56,50,32,103,50,54,57,52,41,0,0,0,0};
static C_char C_TLS li111[] C_aligned={C_lihdr(0,0,14),40,102,95,56,52,48,57,32,114,117,108,101,115,41,0,0};
static C_char C_TLS li112[] C_aligned={C_lihdr(0,0,9),40,97,56,53,52,48,32,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li113[] C_aligned={C_lihdr(0,0,13),40,102,95,56,53,48,51,32,114,117,108,101,41,0,0,0};
static C_char C_TLS li114[] C_aligned={C_lihdr(0,0,36),40,102,95,56,53,54,57,32,105,110,112,117,116,32,112,97,116,116,101,114,110,32,115,101,101,110,45,115,101,103,109,101,110,116,63,41,0,0,0,0};
static C_char C_TLS li115[] C_aligned={C_lihdr(0,0,22),40,102,95,56,55,53,48,32,105,110,112,117,116,32,112,97,116,116,101,114,110,41,0,0};
static C_char C_TLS li116[] C_aligned={C_lihdr(0,0,9),40,97,56,57,51,53,32,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li117[] C_aligned={C_lihdr(0,0,41),40,102,95,56,56,55,55,32,112,97,116,116,101,114,110,32,112,97,116,104,32,109,97,112,105,116,32,115,101,101,110,45,115,101,103,109,101,110,116,63,41,0,0,0,0,0,0,0};
static C_char C_TLS li118[] C_aligned={C_lihdr(0,0,18),40,100,111,108,111,111,112,50,56,49,54,32,100,32,103,101,110,41,0,0,0,0,0,0};
static C_char C_TLS li119[] C_aligned={C_lihdr(0,0,25),40,102,95,57,48,49,55,32,116,101,109,112,108,97,116,101,32,100,105,109,32,101,110,118,41,0,0,0,0,0,0,0};
static C_char C_TLS li120[] C_aligned={C_lihdr(0,0,39),40,102,95,57,50,48,57,32,112,97,116,116,101,114,110,32,100,105,109,32,118,97,114,115,32,115,101,101,110,45,115,101,103,109,101,110,116,63,41,0};
static C_char C_TLS li121[] C_aligned={C_lihdr(0,0,30),40,102,95,57,50,56,54,32,116,101,109,112,108,97,116,101,32,100,105,109,32,101,110,118,32,102,114,101,101,41,0,0};
static C_char C_TLS li122[] C_aligned={C_lihdr(0,0,24),40,102,95,57,51,55,53,32,112,32,115,101,101,110,45,115,101,103,109,101,110,116,63,41};
static C_char C_TLS li123[] C_aligned={C_lihdr(0,0,16),40,102,95,57,52,48,51,32,112,97,116,116,101,114,110,41};
static C_char C_TLS li124[] C_aligned={C_lihdr(0,0,16),40,102,95,57,52,50,55,32,112,97,116,116,101,114,110,41};
static C_char C_TLS li125[] C_aligned={C_lihdr(0,0,14),40,108,111,111,112,32,112,97,116,116,101,114,110,41,0,0};
static C_char C_TLS li126[] C_aligned={C_lihdr(0,0,16),40,102,95,57,52,52,55,32,112,97,116,116,101,114,110,41};
static C_char C_TLS li127[] C_aligned={C_lihdr(0,0,15),40,97,57,53,48,54,32,101,120,112,32,114,32,99,41,0};
static C_char C_TLS li128[] C_aligned={C_lihdr(0,0,16),40,97,57,53,52,50,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li129[] C_aligned={C_lihdr(0,0,10),40,119,97,108,107,32,120,32,110,41,0,0,0,0,0,0};
static C_char C_TLS li130[] C_aligned={C_lihdr(0,0,11),40,119,97,108,107,49,32,120,32,110,41,0,0,0,0,0};
static C_char C_TLS li131[] C_aligned={C_lihdr(0,0,11),40,103,50,53,52,54,32,101,110,118,41,0,0,0,0,0};
static C_char C_TLS li132[] C_aligned={C_lihdr(0,0,11),40,103,50,53,53,51,32,101,110,118,41,0,0,0,0,0};
static C_char C_TLS li133[] C_aligned={C_lihdr(0,0,12),40,115,105,109,112,108,105,102,121,32,120,41,0,0,0,0};
static C_char C_TLS li134[] C_aligned={C_lihdr(0,0,16),40,97,57,53,55,53,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li135[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,52,53,56,32,103,50,52,55,48,41,0,0,0,0};
static C_char C_TLS li136[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,52,50,52,32,103,50,52,51,54,41,0,0,0,0};
static C_char C_TLS li137[] C_aligned={C_lihdr(0,0,16),40,97,57,56,54,56,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li138[] C_aligned={C_lihdr(0,0,11),40,101,120,112,97,110,100,32,98,115,41,0,0,0,0,0};
static C_char C_TLS li139[] C_aligned={C_lihdr(0,0,17),40,97,49,48,48,54,52,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li140[] C_aligned={C_lihdr(0,0,7),40,103,50,51,54,53,41,0};
static C_char C_TLS li141[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,51,53,57,32,103,50,51,55,49,41,0,0,0,0};
static C_char C_TLS li142[] C_aligned={C_lihdr(0,0,22),40,101,120,112,97,110,100,32,99,108,97,117,115,101,115,32,101,108,115,101,63,41,0,0};
static C_char C_TLS li143[] C_aligned={C_lihdr(0,0,17),40,97,49,48,49,49,53,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li144[] C_aligned={C_lihdr(0,0,22),40,101,120,112,97,110,100,32,99,108,97,117,115,101,115,32,101,108,115,101,63,41,0,0};
static C_char C_TLS li145[] C_aligned={C_lihdr(0,0,17),40,97,49,48,51,53,49,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li146[] C_aligned={C_lihdr(0,0,17),40,97,49,48,55,51,52,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li147[] C_aligned={C_lihdr(0,0,17),40,97,49,48,55,56,54,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li148[] C_aligned={C_lihdr(0,0,14),40,97,49,48,56,50,51,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li149[] C_aligned={C_lihdr(0,0,14),40,97,49,48,56,54,55,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li150[] C_aligned={C_lihdr(0,0,14),40,97,49,48,56,56,57,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li151[] C_aligned={C_lihdr(0,0,14),40,97,49,48,57,49,49,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li152[] C_aligned={C_lihdr(0,0,14),40,97,49,48,57,51,51,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li153[] C_aligned={C_lihdr(0,0,17),40,97,49,48,57,56,53,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li154[] C_aligned={C_lihdr(0,0,11),40,108,111,111,112,32,102,111,114,109,41,0,0,0,0,0};
static C_char C_TLS li155[] C_aligned={C_lihdr(0,0,14),40,97,49,49,48,51,50,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li156[] C_aligned={C_lihdr(0,0,14),40,97,49,49,49,53,57,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li157[] C_aligned={C_lihdr(0,0,14),40,97,49,49,49,55,54,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li158[] C_aligned={C_lihdr(0,0,14),40,97,49,49,49,57,51,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li159[] C_aligned={C_lihdr(0,0,14),40,97,49,49,50,49,48,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li160[] C_aligned={C_lihdr(0,0,14),40,97,49,49,50,50,55,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li161[] C_aligned={C_lihdr(0,0,14),40,97,49,49,50,53,48,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li162[] C_aligned={C_lihdr(0,0,11),40,103,50,48,49,50,32,97,114,103,41,0,0,0,0,0};
static C_char C_TLS li163[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,48,48,54,32,103,50,48,49,56,41,0,0,0,0};
static C_char C_TLS li164[] C_aligned={C_lihdr(0,0,14),40,97,49,49,51,51,52,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li165[] C_aligned={C_lihdr(0,0,26),40,97,49,49,53,48,53,32,103,49,57,56,48,32,103,49,57,56,50,32,103,49,57,56,52,41,0,0,0,0,0,0};
static C_char C_TLS li166[] C_aligned={C_lihdr(0,0,14),40,97,49,49,53,49,53,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li167[] C_aligned={C_lihdr(0,0,14),40,97,49,49,53,52,50,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li168[] C_aligned={C_lihdr(0,0,7),40,101,114,114,32,120,41,0};
static C_char C_TLS li169[] C_aligned={C_lihdr(0,0,9),40,116,101,115,116,32,102,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li170[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,56,57,53,32,103,49,57,48,55,41,0,0,0,0};
static C_char C_TLS li171[] C_aligned={C_lihdr(0,0,12),40,101,120,112,97,110,100,32,99,108,115,41,0,0,0,0};
static C_char C_TLS li172[] C_aligned={C_lihdr(0,0,17),40,97,49,49,55,49,55,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li173[] C_aligned={C_lihdr(0,0,14),40,97,49,50,48,49,48,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li174[] C_aligned={C_lihdr(0,0,8),40,97,49,50,48,54,48,41};
static C_char C_TLS li175[] C_aligned={C_lihdr(0,0,52),40,97,49,50,48,54,54,32,110,97,109,101,49,56,48,52,32,108,105,98,49,56,48,54,32,115,112,101,99,49,56,48,56,32,118,49,56,49,48,32,115,49,56,49,50,32,105,49,56,49,52,41,0,0,0,0};
static C_char C_TLS li176[] C_aligned={C_lihdr(0,0,9),40,103,49,55,57,48,32,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li177[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,55,56,52,32,103,49,55,57,54,41,0,0,0,0};
static C_char C_TLS li178[] C_aligned={C_lihdr(0,0,14),40,97,49,50,48,52,53,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li179[] C_aligned={C_lihdr(0,0,26),40,97,49,50,49,54,48,32,103,49,55,54,53,32,103,49,55,54,55,32,103,49,55,54,57,41,0,0,0,0,0,0};
static C_char C_TLS li180[] C_aligned={C_lihdr(0,0,26),40,97,49,50,49,55,48,32,103,49,55,53,49,32,103,49,55,53,51,32,103,49,55,53,53,41,0,0,0,0,0,0};
static C_char C_TLS li181[] C_aligned={C_lihdr(0,0,10),40,116,111,112,108,101,118,101,108,41,0,0,0,0,0,0};


C_noret_decl(f_10027)
static void C_fcall f_10027(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10063)
static void C_ccall f_10063(C_word c,C_word *av) C_noret;
C_noret_decl(f_10065)
static void C_ccall f_10065(C_word c,C_word *av) C_noret;
C_noret_decl(f_10069)
static void C_ccall f_10069(C_word c,C_word *av) C_noret;
C_noret_decl(f_10079)
static void C_fcall f_10079(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10104)
static void C_ccall f_10104(C_word c,C_word *av) C_noret;
C_noret_decl(f_10114)
static void C_ccall f_10114(C_word c,C_word *av) C_noret;
C_noret_decl(f_10116)
static void C_ccall f_10116(C_word c,C_word *av) C_noret;
C_noret_decl(f_10120)
static void C_ccall f_10120(C_word c,C_word *av) C_noret;
C_noret_decl(f_10128)
static void C_ccall f_10128(C_word c,C_word *av) C_noret;
C_noret_decl(f_10131)
static void C_ccall f_10131(C_word c,C_word *av) C_noret;
C_noret_decl(f_10134)
static void C_ccall f_10134(C_word c,C_word *av) C_noret;
C_noret_decl(f_10137)
static void C_ccall f_10137(C_word c,C_word *av) C_noret;
C_noret_decl(f_10140)
static void C_ccall f_10140(C_word c,C_word *av) C_noret;
C_noret_decl(f_10151)
static void C_ccall f_10151(C_word c,C_word *av) C_noret;
C_noret_decl(f_10153)
static void C_fcall f_10153(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10167)
static void C_ccall f_10167(C_word c,C_word *av) C_noret;
C_noret_decl(f_10173)
static void C_ccall f_10173(C_word c,C_word *av) C_noret;
C_noret_decl(f_10176)
static void C_ccall f_10176(C_word c,C_word *av) C_noret;
C_noret_decl(f_10180)
static void C_ccall f_10180(C_word c,C_word *av) C_noret;
C_noret_decl(f_10186)
static void C_ccall f_10186(C_word c,C_word *av) C_noret;
C_noret_decl(f_10189)
static void C_ccall f_10189(C_word c,C_word *av) C_noret;
C_noret_decl(f_10204)
static void C_ccall f_10204(C_word c,C_word *av) C_noret;
C_noret_decl(f_10245)
static void C_fcall f_10245(C_word t0,C_word t1) C_noret;
C_noret_decl(f_10249)
static void C_ccall f_10249(C_word c,C_word *av) C_noret;
C_noret_decl(f_10252)
static void C_ccall f_10252(C_word c,C_word *av) C_noret;
C_noret_decl(f_10285)
static C_word C_fcall f_10285(C_word *a,C_word t0,C_word t1);
C_noret_decl(f_10300)
static void C_ccall f_10300(C_word c,C_word *av) C_noret;
C_noret_decl(f_10302)
static void C_fcall f_10302(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10350)
static void C_ccall f_10350(C_word c,C_word *av) C_noret;
C_noret_decl(f_10352)
static void C_ccall f_10352(C_word c,C_word *av) C_noret;
C_noret_decl(f_10359)
static void C_ccall f_10359(C_word c,C_word *av) C_noret;
C_noret_decl(f_10362)
static void C_ccall f_10362(C_word c,C_word *av) C_noret;
C_noret_decl(f_10365)
static void C_ccall f_10365(C_word c,C_word *av) C_noret;
C_noret_decl(f_10370)
static void C_fcall f_10370(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10384)
static void C_ccall f_10384(C_word c,C_word *av) C_noret;
C_noret_decl(f_10390)
static void C_ccall f_10390(C_word c,C_word *av) C_noret;
C_noret_decl(f_10393)
static void C_ccall f_10393(C_word c,C_word *av) C_noret;
C_noret_decl(f_10397)
static void C_ccall f_10397(C_word c,C_word *av) C_noret;
C_noret_decl(f_10403)
static void C_ccall f_10403(C_word c,C_word *av) C_noret;
C_noret_decl(f_10406)
static void C_ccall f_10406(C_word c,C_word *av) C_noret;
C_noret_decl(f_10409)
static void C_ccall f_10409(C_word c,C_word *av) C_noret;
C_noret_decl(f_10412)
static void C_ccall f_10412(C_word c,C_word *av) C_noret;
C_noret_decl(f_10416)
static void C_ccall f_10416(C_word c,C_word *av) C_noret;
C_noret_decl(f_10422)
static void C_ccall f_10422(C_word c,C_word *av) C_noret;
C_noret_decl(f_10425)
static void C_ccall f_10425(C_word c,C_word *av) C_noret;
C_noret_decl(f_10428)
static void C_ccall f_10428(C_word c,C_word *av) C_noret;
C_noret_decl(f_10434)
static void C_ccall f_10434(C_word c,C_word *av) C_noret;
C_noret_decl(f_10460)
static void C_ccall f_10460(C_word c,C_word *av) C_noret;
C_noret_decl(f_10488)
static void C_ccall f_10488(C_word c,C_word *av) C_noret;
C_noret_decl(f_10505)
static void C_ccall f_10505(C_word c,C_word *av) C_noret;
C_noret_decl(f_10511)
static void C_ccall f_10511(C_word c,C_word *av) C_noret;
C_noret_decl(f_10514)
static void C_ccall f_10514(C_word c,C_word *av) C_noret;
C_noret_decl(f_10533)
static void C_ccall f_10533(C_word c,C_word *av) C_noret;
C_noret_decl(f_10551)
static void C_ccall f_10551(C_word c,C_word *av) C_noret;
C_noret_decl(f_10554)
static void C_ccall f_10554(C_word c,C_word *av) C_noret;
C_noret_decl(f_10581)
static void C_ccall f_10581(C_word c,C_word *av) C_noret;
C_noret_decl(f_10608)
static void C_ccall f_10608(C_word c,C_word *av) C_noret;
C_noret_decl(f_10671)
static void C_ccall f_10671(C_word c,C_word *av) C_noret;
C_noret_decl(f_10683)
static void C_ccall f_10683(C_word c,C_word *av) C_noret;
C_noret_decl(f_10699)
static void C_ccall f_10699(C_word c,C_word *av) C_noret;
C_noret_decl(f_10733)
static void C_ccall f_10733(C_word c,C_word *av) C_noret;
C_noret_decl(f_10735)
static void C_ccall f_10735(C_word c,C_word *av) C_noret;
C_noret_decl(f_10758)
static void C_ccall f_10758(C_word c,C_word *av) C_noret;
C_noret_decl(f_10777)
static void C_ccall f_10777(C_word c,C_word *av) C_noret;
C_noret_decl(f_10785)
static void C_ccall f_10785(C_word c,C_word *av) C_noret;
C_noret_decl(f_10787)
static void C_ccall f_10787(C_word c,C_word *av) C_noret;
C_noret_decl(f_10818)
static void C_ccall f_10818(C_word c,C_word *av) C_noret;
C_noret_decl(f_10822)
static void C_ccall f_10822(C_word c,C_word *av) C_noret;
C_noret_decl(f_10824)
static void C_ccall f_10824(C_word c,C_word *av) C_noret;
C_noret_decl(f_10828)
static void C_ccall f_10828(C_word c,C_word *av) C_noret;
C_noret_decl(f_10851)
static void C_ccall f_10851(C_word c,C_word *av) C_noret;
C_noret_decl(f_10866)
static void C_ccall f_10866(C_word c,C_word *av) C_noret;
C_noret_decl(f_10868)
static void C_ccall f_10868(C_word c,C_word *av) C_noret;
C_noret_decl(f_10872)
static void C_ccall f_10872(C_word c,C_word *av) C_noret;
C_noret_decl(f_10875)
static void C_ccall f_10875(C_word c,C_word *av) C_noret;
C_noret_decl(f_10888)
static void C_ccall f_10888(C_word c,C_word *av) C_noret;
C_noret_decl(f_10890)
static void C_ccall f_10890(C_word c,C_word *av) C_noret;
C_noret_decl(f_10894)
static void C_ccall f_10894(C_word c,C_word *av) C_noret;
C_noret_decl(f_10897)
static void C_ccall f_10897(C_word c,C_word *av) C_noret;
C_noret_decl(f_10910)
static void C_ccall f_10910(C_word c,C_word *av) C_noret;
C_noret_decl(f_10912)
static void C_ccall f_10912(C_word c,C_word *av) C_noret;
C_noret_decl(f_10916)
static void C_ccall f_10916(C_word c,C_word *av) C_noret;
C_noret_decl(f_10919)
static void C_ccall f_10919(C_word c,C_word *av) C_noret;
C_noret_decl(f_10932)
static void C_ccall f_10932(C_word c,C_word *av) C_noret;
C_noret_decl(f_10934)
static void C_ccall f_10934(C_word c,C_word *av) C_noret;
C_noret_decl(f_10938)
static void C_ccall f_10938(C_word c,C_word *av) C_noret;
C_noret_decl(f_10949)
static void C_ccall f_10949(C_word c,C_word *av) C_noret;
C_noret_decl(f_10959)
static void C_ccall f_10959(C_word c,C_word *av) C_noret;
C_noret_decl(f_10984)
static void C_ccall f_10984(C_word c,C_word *av) C_noret;
C_noret_decl(f_10986)
static void C_ccall f_10986(C_word c,C_word *av) C_noret;
C_noret_decl(f_10990)
static void C_ccall f_10990(C_word c,C_word *av) C_noret;
C_noret_decl(f_11007)
static void C_ccall f_11007(C_word c,C_word *av) C_noret;
C_noret_decl(f_11010)
static void C_ccall f_11010(C_word c,C_word *av) C_noret;
C_noret_decl(f_11016)
static void C_ccall f_11016(C_word c,C_word *av) C_noret;
C_noret_decl(f_11023)
static void C_ccall f_11023(C_word c,C_word *av) C_noret;
C_noret_decl(f_11027)
static void C_ccall f_11027(C_word c,C_word *av) C_noret;
C_noret_decl(f_11031)
static void C_ccall f_11031(C_word c,C_word *av) C_noret;
C_noret_decl(f_11033)
static void C_ccall f_11033(C_word c,C_word *av) C_noret;
C_noret_decl(f_11037)
static void C_ccall f_11037(C_word c,C_word *av) C_noret;
C_noret_decl(f_11042)
static void C_fcall f_11042(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11057)
static void C_ccall f_11057(C_word c,C_word *av) C_noret;
C_noret_decl(f_11068)
static void C_ccall f_11068(C_word c,C_word *av) C_noret;
C_noret_decl(f_11071)
static void C_ccall f_11071(C_word c,C_word *av) C_noret;
C_noret_decl(f_11093)
static void C_ccall f_11093(C_word c,C_word *av) C_noret;
C_noret_decl(f_11100)
static void C_ccall f_11100(C_word c,C_word *av) C_noret;
C_noret_decl(f_11104)
static void C_ccall f_11104(C_word c,C_word *av) C_noret;
C_noret_decl(f_11113)
static void C_ccall f_11113(C_word c,C_word *av) C_noret;
C_noret_decl(f_11120)
static void C_ccall f_11120(C_word c,C_word *av) C_noret;
C_noret_decl(f_11123)
static void C_ccall f_11123(C_word c,C_word *av) C_noret;
C_noret_decl(f_11158)
static void C_ccall f_11158(C_word c,C_word *av) C_noret;
C_noret_decl(f_11160)
static void C_ccall f_11160(C_word c,C_word *av) C_noret;
C_noret_decl(f_11164)
static void C_ccall f_11164(C_word c,C_word *av) C_noret;
C_noret_decl(f_11175)
static void C_ccall f_11175(C_word c,C_word *av) C_noret;
C_noret_decl(f_11177)
static void C_ccall f_11177(C_word c,C_word *av) C_noret;
C_noret_decl(f_11181)
static void C_ccall f_11181(C_word c,C_word *av) C_noret;
C_noret_decl(f_11192)
static void C_ccall f_11192(C_word c,C_word *av) C_noret;
C_noret_decl(f_11194)
static void C_ccall f_11194(C_word c,C_word *av) C_noret;
C_noret_decl(f_11198)
static void C_ccall f_11198(C_word c,C_word *av) C_noret;
C_noret_decl(f_11209)
static void C_ccall f_11209(C_word c,C_word *av) C_noret;
C_noret_decl(f_11211)
static void C_ccall f_11211(C_word c,C_word *av) C_noret;
C_noret_decl(f_11215)
static void C_ccall f_11215(C_word c,C_word *av) C_noret;
C_noret_decl(f_11226)
static void C_ccall f_11226(C_word c,C_word *av) C_noret;
C_noret_decl(f_11228)
static void C_ccall f_11228(C_word c,C_word *av) C_noret;
C_noret_decl(f_11232)
static void C_ccall f_11232(C_word c,C_word *av) C_noret;
C_noret_decl(f_11235)
static void C_ccall f_11235(C_word c,C_word *av) C_noret;
C_noret_decl(f_11245)
static void C_ccall f_11245(C_word c,C_word *av) C_noret;
C_noret_decl(f_11249)
static void C_ccall f_11249(C_word c,C_word *av) C_noret;
C_noret_decl(f_11251)
static void C_ccall f_11251(C_word c,C_word *av) C_noret;
C_noret_decl(f_11255)
static void C_ccall f_11255(C_word c,C_word *av) C_noret;
C_noret_decl(f_11258)
static void C_ccall f_11258(C_word c,C_word *av) C_noret;
C_noret_decl(f_11261)
static void C_ccall f_11261(C_word c,C_word *av) C_noret;
C_noret_decl(f_11284)
static void C_ccall f_11284(C_word c,C_word *av) C_noret;
C_noret_decl(f_11287)
static void C_ccall f_11287(C_word c,C_word *av) C_noret;
C_noret_decl(f_11333)
static void C_ccall f_11333(C_word c,C_word *av) C_noret;
C_noret_decl(f_11335)
static void C_ccall f_11335(C_word c,C_word *av) C_noret;
C_noret_decl(f_11339)
static void C_ccall f_11339(C_word c,C_word *av) C_noret;
C_noret_decl(f_11342)
static void C_ccall f_11342(C_word c,C_word *av) C_noret;
C_noret_decl(f_11365)
static void C_ccall f_11365(C_word c,C_word *av) C_noret;
C_noret_decl(f_11393)
static void C_ccall f_11393(C_word c,C_word *av) C_noret;
C_noret_decl(f_11398)
static void C_fcall f_11398(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11405)
static void C_ccall f_11405(C_word c,C_word *av) C_noret;
C_noret_decl(f_11408)
static void C_ccall f_11408(C_word c,C_word *av) C_noret;
C_noret_decl(f_11417)
static void C_ccall f_11417(C_word c,C_word *av) C_noret;
C_noret_decl(f_11462)
static void C_ccall f_11462(C_word c,C_word *av) C_noret;
C_noret_decl(f_11464)
static void C_fcall f_11464(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11489)
static void C_ccall f_11489(C_word c,C_word *av) C_noret;
C_noret_decl(f_11500)
static void C_ccall f_11500(C_word c,C_word *av) C_noret;
C_noret_decl(f_11504)
static void C_ccall f_11504(C_word c,C_word *av) C_noret;
C_noret_decl(f_11506)
static void C_ccall f_11506(C_word c,C_word *av) C_noret;
C_noret_decl(f_11514)
static void C_ccall f_11514(C_word c,C_word *av) C_noret;
C_noret_decl(f_11516)
static void C_ccall f_11516(C_word c,C_word *av) C_noret;
C_noret_decl(f_11520)
static void C_ccall f_11520(C_word c,C_word *av) C_noret;
C_noret_decl(f_11523)
static void C_ccall f_11523(C_word c,C_word *av) C_noret;
C_noret_decl(f_11526)
static void C_ccall f_11526(C_word c,C_word *av) C_noret;
C_noret_decl(f_11533)
static void C_ccall f_11533(C_word c,C_word *av) C_noret;
C_noret_decl(f_11541)
static void C_ccall f_11541(C_word c,C_word *av) C_noret;
C_noret_decl(f_11543)
static void C_ccall f_11543(C_word c,C_word *av) C_noret;
C_noret_decl(f_11547)
static void C_ccall f_11547(C_word c,C_word *av) C_noret;
C_noret_decl(f_11553)
static void C_ccall f_11553(C_word c,C_word *av) C_noret;
C_noret_decl(f_11559)
static void C_fcall f_11559(C_word t0,C_word t1) C_noret;
C_noret_decl(f_11562)
static void C_ccall f_11562(C_word c,C_word *av) C_noret;
C_noret_decl(f_11574)
static void C_ccall f_11574(C_word c,C_word *av) C_noret;
C_noret_decl(f_11577)
static void C_ccall f_11577(C_word c,C_word *av) C_noret;
C_noret_decl(f_11608)
static void C_ccall f_11608(C_word c,C_word *av) C_noret;
C_noret_decl(f_11612)
static void C_ccall f_11612(C_word c,C_word *av) C_noret;
C_noret_decl(f_11615)
static void C_ccall f_11615(C_word c,C_word *av) C_noret;
C_noret_decl(f_11622)
static void C_ccall f_11622(C_word c,C_word *av) C_noret;
C_noret_decl(f_11631)
static void C_ccall f_11631(C_word c,C_word *av) C_noret;
C_noret_decl(f_11656)
static void C_fcall f_11656(C_word t0,C_word t1) C_noret;
C_noret_decl(f_11690)
static void C_ccall f_11690(C_word c,C_word *av) C_noret;
C_noret_decl(f_11704)
static void C_ccall f_11704(C_word c,C_word *av) C_noret;
C_noret_decl(f_11716)
static void C_ccall f_11716(C_word c,C_word *av) C_noret;
C_noret_decl(f_11718)
static void C_ccall f_11718(C_word c,C_word *av) C_noret;
C_noret_decl(f_11724)
static void C_fcall f_11724(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11734)
static void C_fcall f_11734(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11748)
static void C_ccall f_11748(C_word c,C_word *av) C_noret;
C_noret_decl(f_11764)
static void C_ccall f_11764(C_word c,C_word *av) C_noret;
C_noret_decl(f_11788)
static void C_ccall f_11788(C_word c,C_word *av) C_noret;
C_noret_decl(f_11823)
static void C_ccall f_11823(C_word c,C_word *av) C_noret;
C_noret_decl(f_11857)
static void C_ccall f_11857(C_word c,C_word *av) C_noret;
C_noret_decl(f_11879)
static void C_fcall f_11879(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11904)
static void C_ccall f_11904(C_word c,C_word *av) C_noret;
C_noret_decl(f_11906)
static void C_fcall f_11906(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11985)
static void C_ccall f_11985(C_word c,C_word *av) C_noret;
C_noret_decl(f_11997)
static void C_ccall f_11997(C_word c,C_word *av) C_noret;
C_noret_decl(f_12009)
static void C_ccall f_12009(C_word c,C_word *av) C_noret;
C_noret_decl(f_12011)
static void C_ccall f_12011(C_word c,C_word *av) C_noret;
C_noret_decl(f_12015)
static void C_ccall f_12015(C_word c,C_word *av) C_noret;
C_noret_decl(f_12026)
static void C_ccall f_12026(C_word c,C_word *av) C_noret;
C_noret_decl(f_12036)
static void C_ccall f_12036(C_word c,C_word *av) C_noret;
C_noret_decl(f_12044)
static void C_ccall f_12044(C_word c,C_word *av) C_noret;
C_noret_decl(f_12046)
static void C_ccall f_12046(C_word c,C_word *av) C_noret;
C_noret_decl(f_12055)
static void C_fcall f_12055(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_12061)
static void C_ccall f_12061(C_word c,C_word *av) C_noret;
C_noret_decl(f_12067)
static void C_ccall f_12067(C_word c,C_word *av) C_noret;
C_noret_decl(f_12071)
static void C_ccall f_12071(C_word c,C_word *av) C_noret;
C_noret_decl(f_12074)
static void C_ccall f_12074(C_word c,C_word *av) C_noret;
C_noret_decl(f_12077)
static void C_ccall f_12077(C_word c,C_word *av) C_noret;
C_noret_decl(f_12090)
static void C_ccall f_12090(C_word c,C_word *av) C_noret;
C_noret_decl(f_12112)
static void C_ccall f_12112(C_word c,C_word *av) C_noret;
C_noret_decl(f_12121)
static void C_ccall f_12121(C_word c,C_word *av) C_noret;
C_noret_decl(f_12123)
static void C_fcall f_12123(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_12148)
static void C_ccall f_12148(C_word c,C_word *av) C_noret;
C_noret_decl(f_12159)
static void C_ccall f_12159(C_word c,C_word *av) C_noret;
C_noret_decl(f_12161)
static void C_ccall f_12161(C_word c,C_word *av) C_noret;
C_noret_decl(f_12169)
static void C_ccall f_12169(C_word c,C_word *av) C_noret;
C_noret_decl(f_12171)
static void C_ccall f_12171(C_word c,C_word *av) C_noret;
C_noret_decl(f_3700)
static void C_ccall f_3700(C_word c,C_word *av) C_noret;
C_noret_decl(f_3703)
static void C_ccall f_3703(C_word c,C_word *av) C_noret;
C_noret_decl(f_3707)
static void C_ccall f_3707(C_word c,C_word *av) C_noret;
C_noret_decl(f_3712)
static void C_ccall f_3712(C_word c,C_word *av) C_noret;
C_noret_decl(f_3716)
static void C_ccall f_3716(C_word c,C_word *av) C_noret;
C_noret_decl(f_3718)
static C_word C_fcall f_3718(C_word t0,C_word t1);
C_noret_decl(f_3735)
static void C_fcall f_3735(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3742)
static void C_ccall f_3742(C_word c,C_word *av) C_noret;
C_noret_decl(f_3748)
static void C_ccall f_3748(C_word c,C_word *av) C_noret;
C_noret_decl(f_3782)
static void C_ccall f_3782(C_word c,C_word *av) C_noret;
C_noret_decl(f_3788)
static void C_fcall f_3788(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3804)
static void C_ccall f_3804(C_word c,C_word *av) C_noret;
C_noret_decl(f_3859)
static void C_ccall f_3859(C_word c,C_word *av) C_noret;
C_noret_decl(f_3866)
static void C_ccall f_3866(C_word c,C_word *av) C_noret;
C_noret_decl(f_3884)
static void C_ccall f_3884(C_word c,C_word *av) C_noret;
C_noret_decl(f_3893)
static void C_fcall f_3893(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3914)
static void C_ccall f_3914(C_word c,C_word *av) C_noret;
C_noret_decl(f_3924)
static void C_ccall f_3924(C_word c,C_word *av) C_noret;
C_noret_decl(f_3928)
static void C_ccall f_3928(C_word c,C_word *av) C_noret;
C_noret_decl(f_3953)
static void C_ccall f_3953(C_word c,C_word *av) C_noret;
C_noret_decl(f_3968)
static void C_ccall f_3968(C_word c,C_word *av) C_noret;
C_noret_decl(f_3970)
static void C_fcall f_3970(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_4018)
static void C_fcall f_4018(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_4067)
static void C_fcall f_4067(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4092)
static void C_ccall f_4092(C_word c,C_word *av) C_noret;
C_noret_decl(f_4104)
static void C_ccall f_4104(C_word c,C_word *av) C_noret;
C_noret_decl(f_4110)
static void C_fcall f_4110(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4126)
static void C_fcall f_4126(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4144)
static void C_fcall f_4144(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4188)
static void C_ccall f_4188(C_word c,C_word *av) C_noret;
C_noret_decl(f_4198)
static void C_ccall f_4198(C_word c,C_word *av) C_noret;
C_noret_decl(f_4222)
static void C_ccall f_4222(C_word c,C_word *av) C_noret;
C_noret_decl(f_4226)
static void C_ccall f_4226(C_word c,C_word *av) C_noret;
C_noret_decl(f_4229)
static void C_ccall f_4229(C_word c,C_word *av) C_noret;
C_noret_decl(f_4236)
static C_word C_fcall f_4236(C_word t0,C_word t1);
C_noret_decl(f_4254)
static void C_ccall f_4254(C_word c,C_word *av) C_noret;
C_noret_decl(f_4264)
static void C_ccall f_4264(C_word c,C_word *av) C_noret;
C_noret_decl(f_4268)
static void C_ccall f_4268(C_word c,C_word *av) C_noret;
C_noret_decl(f_4290)
static void C_ccall f_4290(C_word c,C_word *av) C_noret;
C_noret_decl(f_4301)
static void C_ccall f_4301(C_word c,C_word *av) C_noret;
C_noret_decl(f_4309)
static void C_ccall f_4309(C_word c,C_word *av) C_noret;
C_noret_decl(f_4313)
static void C_ccall f_4313(C_word c,C_word *av) C_noret;
C_noret_decl(f_4315)
static void C_fcall f_4315(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4338)
static void C_ccall f_4338(C_word c,C_word *av) C_noret;
C_noret_decl(f_4346)
static void C_ccall f_4346(C_word c,C_word *av) C_noret;
C_noret_decl(f_4349)
static void C_fcall f_4349(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6) C_noret;
C_noret_decl(f_4359)
static void C_ccall f_4359(C_word c,C_word *av) C_noret;
C_noret_decl(f_4364)
static void C_ccall f_4364(C_word c,C_word *av) C_noret;
C_noret_decl(f_4370)
static void C_ccall f_4370(C_word c,C_word *av) C_noret;
C_noret_decl(f_4376)
static void C_ccall f_4376(C_word c,C_word *av) C_noret;
C_noret_decl(f_4398)
static void C_ccall f_4398(C_word c,C_word *av) C_noret;
C_noret_decl(f_4404)
static void C_fcall f_4404(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4421)
static void C_fcall f_4421(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4432)
static void C_ccall f_4432(C_word c,C_word *av) C_noret;
C_noret_decl(f_4471)
static void C_ccall f_4471(C_word c,C_word *av) C_noret;
C_noret_decl(f_4477)
static void C_ccall f_4477(C_word c,C_word *av) C_noret;
C_noret_decl(f_4481)
static void C_ccall f_4481(C_word c,C_word *av) C_noret;
C_noret_decl(f_4484)
static void C_ccall f_4484(C_word c,C_word *av) C_noret;
C_noret_decl(f_4500)
static void C_ccall f_4500(C_word c,C_word *av) C_noret;
C_noret_decl(f_4504)
static void C_ccall f_4504(C_word c,C_word *av) C_noret;
C_noret_decl(f_4511)
static void C_ccall f_4511(C_word c,C_word *av) C_noret;
C_noret_decl(f_4517)
static void C_ccall f_4517(C_word c,C_word *av) C_noret;
C_noret_decl(f_4522)
static void C_ccall f_4522(C_word c,C_word *av) C_noret;
C_noret_decl(f_4528)
static void C_ccall f_4528(C_word c,C_word *av) C_noret;
C_noret_decl(f_4536)
static void C_ccall f_4536(C_word c,C_word *av) C_noret;
C_noret_decl(f_4542)
static void C_ccall f_4542(C_word c,C_word *av) C_noret;
C_noret_decl(f_4548)
static void C_fcall f_4548(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_4574)
static void C_ccall f_4574(C_word c,C_word *av) C_noret;
C_noret_decl(f_4592)
static void C_fcall f_4592(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4616)
static void C_fcall f_4616(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4625)
static void C_ccall f_4625(C_word c,C_word *av) C_noret;
C_noret_decl(f_4637)
static void C_ccall f_4637(C_word c,C_word *av) C_noret;
C_noret_decl(f_4662)
static void C_ccall f_4662(C_word c,C_word *av) C_noret;
C_noret_decl(f_4664)
static void C_fcall f_4664(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4727)
static void C_ccall f_4727(C_word c,C_word *av) C_noret;
C_noret_decl(f_4733)
static void C_fcall f_4733(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4771)
static void C_fcall f_4771(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4775)
static void C_fcall f_4775(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4779)
static void C_ccall f_4779(C_word c,C_word *av) C_noret;
C_noret_decl(f_4791)
static void C_ccall f_4791(C_word c,C_word *av) C_noret;
C_noret_decl(f_4834)
static void C_ccall f_4834(C_word c,C_word *av) C_noret;
C_noret_decl(f_4844)
static void C_ccall f_4844(C_word c,C_word *av) C_noret;
C_noret_decl(f_4847)
static void C_ccall f_4847(C_word c,C_word *av) C_noret;
C_noret_decl(f_4851)
static void C_ccall f_4851(C_word c,C_word *av) C_noret;
C_noret_decl(f_4865)
static void C_fcall f_4865(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4871)
static void C_ccall f_4871(C_word c,C_word *av) C_noret;
C_noret_decl(f_4877)
static void C_ccall f_4877(C_word c,C_word *av) C_noret;
C_noret_decl(f_4913)
static void C_ccall f_4913(C_word c,C_word *av) C_noret;
C_noret_decl(f_4919)
static C_word C_fcall f_4919(C_word t0);
C_noret_decl(f_4960)
static void C_ccall f_4960(C_word c,C_word *av) C_noret;
C_noret_decl(f_4963)
static void C_fcall f_4963(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4980)
static void C_ccall f_4980(C_word c,C_word *av) C_noret;
C_noret_decl(f_4983)
static void C_ccall f_4983(C_word c,C_word *av) C_noret;
C_noret_decl(f_4986)
static void C_ccall f_4986(C_word c,C_word *av) C_noret;
C_noret_decl(f_4991)
static void C_fcall f_4991(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6) C_noret;
C_noret_decl(f_5005)
static void C_ccall f_5005(C_word c,C_word *av) C_noret;
C_noret_decl(f_5009)
static void C_fcall f_5009(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5021)
static void C_fcall f_5021(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5052)
static void C_ccall f_5052(C_word c,C_word *av) C_noret;
C_noret_decl(f_5077)
static void C_ccall f_5077(C_word c,C_word *av) C_noret;
C_noret_decl(f_5096)
static void C_ccall f_5096(C_word c,C_word *av) C_noret;
C_noret_decl(f_5100)
static void C_ccall f_5100(C_word c,C_word *av) C_noret;
C_noret_decl(f_5149)
static void C_fcall f_5149(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5202)
static void C_ccall f_5202(C_word c,C_word *av) C_noret;
C_noret_decl(f_5206)
static void C_ccall f_5206(C_word c,C_word *av) C_noret;
C_noret_decl(f_5209)
static void C_ccall f_5209(C_word c,C_word *av) C_noret;
C_noret_decl(f_5212)
static void C_ccall f_5212(C_word c,C_word *av) C_noret;
C_noret_decl(f_5214)
static void C_fcall f_5214(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5239)
static void C_ccall f_5239(C_word c,C_word *av) C_noret;
C_noret_decl(f_5253)
static void C_ccall f_5253(C_word c,C_word *av) C_noret;
C_noret_decl(f_5294)
static void C_fcall f_5294(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5307)
static void C_fcall f_5307(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5323)
static void C_ccall f_5323(C_word c,C_word *av) C_noret;
C_noret_decl(f_5344)
static void C_fcall f_5344(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5383)
static void C_fcall f_5383(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5402)
static void C_ccall f_5402(C_word c,C_word *av) C_noret;
C_noret_decl(f_5470)
static void C_fcall f_5470(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5569)
static void C_ccall f_5569(C_word c,C_word *av) C_noret;
C_noret_decl(f_5575)
static void C_ccall f_5575(C_word c,C_word *av) C_noret;
C_noret_decl(f_5579)
static void C_ccall f_5579(C_word c,C_word *av) C_noret;
C_noret_decl(f_5582)
static void C_ccall f_5582(C_word c,C_word *av) C_noret;
C_noret_decl(f_5601)
static void C_ccall f_5601(C_word c,C_word *av) C_noret;
C_noret_decl(f_5605)
static void C_ccall f_5605(C_word c,C_word *av) C_noret;
C_noret_decl(f_5623)
static void C_ccall f_5623(C_word c,C_word *av) C_noret;
C_noret_decl(f_5645)
static void C_fcall f_5645(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_5711)
static void C_fcall f_5711(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5736)
static void C_ccall f_5736(C_word c,C_word *av) C_noret;
C_noret_decl(f_5749)
static void C_ccall f_5749(C_word c,C_word *av) C_noret;
C_noret_decl(f_5753)
static void C_ccall f_5753(C_word c,C_word *av) C_noret;
C_noret_decl(f_5764)
static C_word C_fcall f_5764(C_word t0,C_word t1,C_word t2);
C_noret_decl(f_5847)
static void C_fcall f_5847(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5) C_noret;
C_noret_decl(f_5859)
static void C_fcall f_5859(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_5873)
static void C_ccall f_5873(C_word c,C_word *av) C_noret;
C_noret_decl(f_5878)
static void C_fcall f_5878(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5889)
static void C_fcall f_5889(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5909)
static void C_ccall f_5909(C_word c,C_word *av) C_noret;
C_noret_decl(f_5913)
static void C_ccall f_5913(C_word c,C_word *av) C_noret;
C_noret_decl(f_5921)
static void C_ccall f_5921(C_word c,C_word *av) C_noret;
C_noret_decl(f_5928)
static void C_ccall f_5928(C_word c,C_word *av) C_noret;
C_noret_decl(f_5940)
static void C_fcall f_5940(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5947)
static void C_ccall f_5947(C_word c,C_word *av) C_noret;
C_noret_decl(f_5951)
static void C_ccall f_5951(C_word c,C_word *av) C_noret;
C_noret_decl(f_5989)
static void C_ccall f_5989(C_word c,C_word *av) C_noret;
C_noret_decl(f_6100)
static void C_ccall f_6100(C_word c,C_word *av) C_noret;
C_noret_decl(f_6103)
static void C_ccall f_6103(C_word c,C_word *av) C_noret;
C_noret_decl(f_6109)
static void C_ccall f_6109(C_word c,C_word *av) C_noret;
C_noret_decl(f_6113)
static void C_ccall f_6113(C_word c,C_word *av) C_noret;
C_noret_decl(f_6135)
static void C_ccall f_6135(C_word c,C_word *av) C_noret;
C_noret_decl(f_6138)
static void C_ccall f_6138(C_word c,C_word *av) C_noret;
C_noret_decl(f_6141)
static void C_ccall f_6141(C_word c,C_word *av) C_noret;
C_noret_decl(f_6144)
static void C_ccall f_6144(C_word c,C_word *av) C_noret;
C_noret_decl(f_6146)
static void C_fcall f_6146(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_6153)
static void C_fcall f_6153(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6179)
static void C_ccall f_6179(C_word c,C_word *av) C_noret;
C_noret_decl(f_6208)
static void C_fcall f_6208(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6242)
static void C_fcall f_6242(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6266)
static void C_ccall f_6266(C_word c,C_word *av) C_noret;
C_noret_decl(f_6268)
static void C_ccall f_6268(C_word c,C_word *av) C_noret;
C_noret_decl(f_6272)
static void C_ccall f_6272(C_word c,C_word *av) C_noret;
C_noret_decl(f_6284)
static void C_fcall f_6284(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5) C_noret;
C_noret_decl(f_6292)
static void C_ccall f_6292(C_word c,C_word *av) C_noret;
C_noret_decl(f_6294)
static void C_fcall f_6294(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_6316)
static void C_ccall f_6316(C_word c,C_word *av) C_noret;
C_noret_decl(f_6319)
static void C_ccall f_6319(C_word c,C_word *av) C_noret;
C_noret_decl(f_6321)
static void C_fcall f_6321(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6368)
static void C_fcall f_6368(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6372)
static void C_ccall f_6372(C_word c,C_word *av) C_noret;
C_noret_decl(f_6440)
static void C_fcall f_6440(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6446)
static void C_fcall f_6446(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5) C_noret;
C_noret_decl(f_6466)
static void C_fcall f_6466(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6484)
static void C_ccall f_6484(C_word c,C_word *av) C_noret;
C_noret_decl(f_6489)
static void C_fcall f_6489(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6502)
static void C_ccall f_6502(C_word c,C_word *av) C_noret;
C_noret_decl(f_6505)
static void C_ccall f_6505(C_word c,C_word *av) C_noret;
C_noret_decl(f_6555)
static void C_ccall f_6555(C_word c,C_word *av) C_noret;
C_noret_decl(f_6562)
static void C_ccall f_6562(C_word c,C_word *av) C_noret;
C_noret_decl(f_6569)
static void C_ccall f_6569(C_word c,C_word *av) C_noret;
C_noret_decl(f_6621)
static void C_ccall f_6621(C_word c,C_word *av) C_noret;
C_noret_decl(f_6633)
static void C_ccall f_6633(C_word c,C_word *av) C_noret;
C_noret_decl(f_6669)
static void C_ccall f_6669(C_word c,C_word *av) C_noret;
C_noret_decl(f_6685)
static void C_ccall f_6685(C_word c,C_word *av) C_noret;
C_noret_decl(f_6755)
static void C_ccall f_6755(C_word c,C_word *av) C_noret;
C_noret_decl(f_6758)
static void C_fcall f_6758(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6772)
static C_word C_fcall f_6772(C_word t0,C_word t1);
C_noret_decl(f_6812)
static void C_ccall f_6812(C_word c,C_word *av) C_noret;
C_noret_decl(f_6835)
static void C_ccall f_6835(C_word c,C_word *av) C_noret;
C_noret_decl(f_6837)
static void C_ccall f_6837(C_word c,C_word *av) C_noret;
C_noret_decl(f_6840)
static void C_fcall f_6840(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6884)
static void C_ccall f_6884(C_word c,C_word *av) C_noret;
C_noret_decl(f_6892)
static void C_ccall f_6892(C_word c,C_word *av) C_noret;
C_noret_decl(f_6900)
static void C_ccall f_6900(C_word c,C_word *av) C_noret;
C_noret_decl(f_6903)
static void C_ccall f_6903(C_word c,C_word *av) C_noret;
C_noret_decl(f_6914)
static void C_ccall f_6914(C_word c,C_word *av) C_noret;
C_noret_decl(f_6919)
static void C_fcall f_6919(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6939)
static void C_ccall f_6939(C_word c,C_word *av) C_noret;
C_noret_decl(f_6943)
static void C_ccall f_6943(C_word c,C_word *av) C_noret;
C_noret_decl(f_6958)
static void C_ccall f_6958(C_word c,C_word *av) C_noret;
C_noret_decl(f_6970)
static void C_ccall f_6970(C_word c,C_word *av) C_noret;
C_noret_decl(f_6972)
static void C_fcall f_6972(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6979)
static void C_ccall f_6979(C_word c,C_word *av) C_noret;
C_noret_decl(f_6986)
static void C_ccall f_6986(C_word c,C_word *av) C_noret;
C_noret_decl(f_6988)
static void C_fcall f_6988(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6998)
static void C_ccall f_6998(C_word c,C_word *av) C_noret;
C_noret_decl(f_7001)
static void C_ccall f_7001(C_word c,C_word *av) C_noret;
C_noret_decl(f_7004)
static void C_ccall f_7004(C_word c,C_word *av) C_noret;
C_noret_decl(f_7007)
static void C_ccall f_7007(C_word c,C_word *av) C_noret;
C_noret_decl(f_7010)
static void C_ccall f_7010(C_word c,C_word *av) C_noret;
C_noret_decl(f_7017)
static void C_ccall f_7017(C_word c,C_word *av) C_noret;
C_noret_decl(f_7024)
static void C_ccall f_7024(C_word c,C_word *av) C_noret;
C_noret_decl(f_7027)
static void C_ccall f_7027(C_word c,C_word *av) C_noret;
C_noret_decl(f_7036)
static void C_ccall f_7036(C_word c,C_word *av) C_noret;
C_noret_decl(f_7039)
static void C_ccall f_7039(C_word c,C_word *av) C_noret;
C_noret_decl(f_7042)
static void C_ccall f_7042(C_word c,C_word *av) C_noret;
C_noret_decl(f_7045)
static void C_ccall f_7045(C_word c,C_word *av) C_noret;
C_noret_decl(f_7048)
static void C_ccall f_7048(C_word c,C_word *av) C_noret;
C_noret_decl(f_7051)
static void C_ccall f_7051(C_word c,C_word *av) C_noret;
C_noret_decl(f_7064)
static void C_ccall f_7064(C_word c,C_word *av) C_noret;
C_noret_decl(f_7068)
static void C_ccall f_7068(C_word c,C_word *av) C_noret;
C_noret_decl(f_7079)
static void C_ccall f_7079(C_word c,C_word *av) C_noret;
C_noret_decl(f_7083)
static void C_ccall f_7083(C_word c,C_word *av) C_noret;
C_noret_decl(f_7085)
static void C_fcall f_7085(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7099)
static void C_ccall f_7099(C_word c,C_word *av) C_noret;
C_noret_decl(f_7103)
static void C_ccall f_7103(C_word c,C_word *av) C_noret;
C_noret_decl(f_7124)
static void C_ccall f_7124(C_word c,C_word *av) C_noret;
C_noret_decl(f_7144)
static void C_ccall f_7144(C_word c,C_word *av) C_noret;
C_noret_decl(f_7148)
static C_word C_fcall f_7148(C_word t0,C_word t1);
C_noret_decl(f_7163)
static void C_ccall f_7163(C_word c,C_word *av) C_noret;
C_noret_decl(f_7173)
static void C_ccall f_7173(C_word c,C_word *av) C_noret;
C_noret_decl(f_7178)
static void C_fcall f_7178(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_7185)
static void C_ccall f_7185(C_word c,C_word *av) C_noret;
C_noret_decl(f_7190)
static void C_fcall f_7190(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7194)
static void C_ccall f_7194(C_word c,C_word *av) C_noret;
C_noret_decl(f_7201)
static void C_ccall f_7201(C_word c,C_word *av) C_noret;
C_noret_decl(f_7208)
static void C_ccall f_7208(C_word c,C_word *av) C_noret;
C_noret_decl(f_7215)
static void C_ccall f_7215(C_word c,C_word *av) C_noret;
C_noret_decl(f_7217)
static void C_ccall f_7217(C_word c,C_word *av) C_noret;
C_noret_decl(f_7221)
static void C_ccall f_7221(C_word c,C_word *av) C_noret;
C_noret_decl(f_7229)
static C_word C_fcall f_7229(C_word t0);
C_noret_decl(f_7262)
static void C_ccall f_7262(C_word c,C_word *av) C_noret;
C_noret_decl(f_7268)
static void C_ccall f_7268(C_word c,C_word *av) C_noret;
C_noret_decl(f_7274)
static C_word C_fcall f_7274(C_word t0);
C_noret_decl(f_7295)
static void C_fcall f_7295(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7300)
static void C_fcall f_7300(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_7319)
static void C_fcall f_7319(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7324)
static void C_fcall f_7324(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_7343)
static void C_ccall f_7343(C_word c,C_word *av) C_noret;
C_noret_decl(f_7497)
static void C_ccall f_7497(C_word c,C_word *av) C_noret;
C_noret_decl(f_7554)
static void C_ccall f_7554(C_word c,C_word *av) C_noret;
C_noret_decl(f_7607)
static void C_fcall f_7607(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7613)
static void C_ccall f_7613(C_word c,C_word *av) C_noret;
C_noret_decl(f_7620)
static void C_ccall f_7620(C_word c,C_word *av) C_noret;
C_noret_decl(f_7622)
static void C_ccall f_7622(C_word c,C_word *av) C_noret;
C_noret_decl(f_7636)
static void C_ccall f_7636(C_word c,C_word *av) C_noret;
C_noret_decl(f_7640)
static void C_ccall f_7640(C_word c,C_word *av) C_noret;
C_noret_decl(f_7657)
static void C_ccall f_7657(C_word c,C_word *av) C_noret;
C_noret_decl(f_7661)
static void C_ccall f_7661(C_word c,C_word *av) C_noret;
C_noret_decl(f_7670)
static void C_ccall f_7670(C_word c,C_word *av) C_noret;
C_noret_decl(f_7690)
static void C_ccall f_7690(C_word c,C_word *av) C_noret;
C_noret_decl(f_7710)
static void C_ccall f_7710(C_word c,C_word *av) C_noret;
C_noret_decl(f_7732)
static void C_ccall f_7732(C_word c,C_word *av) C_noret;
C_noret_decl(f_7769)
static void C_fcall f_7769(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_7790)
static void C_ccall f_7790(C_word c,C_word *av) C_noret;
C_noret_decl(f_7813)
static void C_fcall f_7813(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7821)
static void C_fcall f_7821(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7829)
static void C_fcall f_7829(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7851)
static C_word C_fcall f_7851(C_word t0,C_word t1);
C_noret_decl(f_7866)
static void C_ccall f_7866(C_word c,C_word *av) C_noret;
C_noret_decl(f_7879)
static C_word C_fcall f_7879(C_word t0,C_word t1);
C_noret_decl(f_7894)
static void C_ccall f_7894(C_word c,C_word *av) C_noret;
C_noret_decl(f_7932)
static C_word C_fcall f_7932(C_word t0,C_word t1);
C_noret_decl(f_7957)
static void C_fcall f_7957(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7971)
static void C_ccall f_7971(C_word c,C_word *av) C_noret;
C_noret_decl(f_7975)
static void C_ccall f_7975(C_word c,C_word *av) C_noret;
C_noret_decl(f_7992)
static void C_ccall f_7992(C_word c,C_word *av) C_noret;
C_noret_decl(f_7996)
static void C_ccall f_7996(C_word c,C_word *av) C_noret;
C_noret_decl(f_8005)
static void C_ccall f_8005(C_word c,C_word *av) C_noret;
C_noret_decl(f_8091)
static void C_ccall f_8091(C_word c,C_word *av) C_noret;
C_noret_decl(f_8095)
static void C_ccall f_8095(C_word c,C_word *av) C_noret;
C_noret_decl(f_8100)
static void C_ccall f_8100(C_word c,C_word *av) C_noret;
C_noret_decl(f_8106)
static void C_ccall f_8106(C_word c,C_word *av) C_noret;
C_noret_decl(f_8119)
static void C_ccall f_8119(C_word c,C_word *av) C_noret;
C_noret_decl(f_8122)
static void C_ccall f_8122(C_word c,C_word *av) C_noret;
C_noret_decl(f_8126)
static void C_ccall f_8126(C_word c,C_word *av) C_noret;
C_noret_decl(f_8129)
static void C_ccall f_8129(C_word c,C_word *av) C_noret;
C_noret_decl(f_8132)
static void C_ccall f_8132(C_word c,C_word *av) C_noret;
C_noret_decl(f_8136)
static void C_ccall f_8136(C_word c,C_word *av) C_noret;
C_noret_decl(f_8139)
static void C_ccall f_8139(C_word c,C_word *av) C_noret;
C_noret_decl(f_8142)
static void C_ccall f_8142(C_word c,C_word *av) C_noret;
C_noret_decl(f_8145)
static void C_ccall f_8145(C_word c,C_word *av) C_noret;
C_noret_decl(f_8148)
static void C_ccall f_8148(C_word c,C_word *av) C_noret;
C_noret_decl(f_8151)
static void C_ccall f_8151(C_word c,C_word *av) C_noret;
C_noret_decl(f_8154)
static void C_ccall f_8154(C_word c,C_word *av) C_noret;
C_noret_decl(f_8158)
static void C_ccall f_8158(C_word c,C_word *av) C_noret;
C_noret_decl(f_8162)
static void C_ccall f_8162(C_word c,C_word *av) C_noret;
C_noret_decl(f_8165)
static void C_ccall f_8165(C_word c,C_word *av) C_noret;
C_noret_decl(f_8168)
static void C_ccall f_8168(C_word c,C_word *av) C_noret;
C_noret_decl(f_8171)
static void C_ccall f_8171(C_word c,C_word *av) C_noret;
C_noret_decl(f_8174)
static void C_ccall f_8174(C_word c,C_word *av) C_noret;
C_noret_decl(f_8178)
static void C_ccall f_8178(C_word c,C_word *av) C_noret;
C_noret_decl(f_8182)
static void C_ccall f_8182(C_word c,C_word *av) C_noret;
C_noret_decl(f_8185)
static void C_ccall f_8185(C_word c,C_word *av) C_noret;
C_noret_decl(f_8188)
static void C_ccall f_8188(C_word c,C_word *av) C_noret;
C_noret_decl(f_8191)
static void C_ccall f_8191(C_word c,C_word *av) C_noret;
C_noret_decl(f_8194)
static void C_ccall f_8194(C_word c,C_word *av) C_noret;
C_noret_decl(f_8197)
static void C_ccall f_8197(C_word c,C_word *av) C_noret;
C_noret_decl(f_8200)
static void C_ccall f_8200(C_word c,C_word *av) C_noret;
C_noret_decl(f_8203)
static void C_ccall f_8203(C_word c,C_word *av) C_noret;
C_noret_decl(f_8206)
static void C_ccall f_8206(C_word c,C_word *av) C_noret;
C_noret_decl(f_8209)
static void C_ccall f_8209(C_word c,C_word *av) C_noret;
C_noret_decl(f_8212)
static void C_ccall f_8212(C_word c,C_word *av) C_noret;
C_noret_decl(f_8215)
static void C_ccall f_8215(C_word c,C_word *av) C_noret;
C_noret_decl(f_8218)
static void C_ccall f_8218(C_word c,C_word *av) C_noret;
C_noret_decl(f_8221)
static void C_ccall f_8221(C_word c,C_word *av) C_noret;
C_noret_decl(f_8224)
static void C_ccall f_8224(C_word c,C_word *av) C_noret;
C_noret_decl(f_8226)
static void C_ccall f_8226(C_word c,C_word *av) C_noret;
C_noret_decl(f_8232)
static void C_ccall f_8232(C_word c,C_word *av) C_noret;
C_noret_decl(f_8242)
static void C_fcall f_8242(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_8260)
static void C_ccall f_8260(C_word c,C_word *av) C_noret;
C_noret_decl(f_8268)
static void C_ccall f_8268(C_word c,C_word *av) C_noret;
C_noret_decl(f_8278)
static C_word C_fcall f_8278(C_word t0,C_word t1,C_word t2);
C_noret_decl(f_8305)
static void C_ccall f_8305(C_word c,C_word *av) C_noret;
C_noret_decl(f_8320)
static void C_ccall f_8320(C_word c,C_word *av) C_noret;
C_noret_decl(f_8324)
static void C_ccall f_8324(C_word c,C_word *av) C_noret;
C_noret_decl(f_8329)
static void C_ccall f_8329(C_word c,C_word *av) C_noret;
C_noret_decl(f_8335)
static void C_ccall f_8335(C_word c,C_word *av) C_noret;
C_noret_decl(f_8339)
static void C_ccall f_8339(C_word c,C_word *av) C_noret;
C_noret_decl(f_8343)
static void C_ccall f_8343(C_word c,C_word *av) C_noret;
C_noret_decl(f_8347)
static void C_ccall f_8347(C_word c,C_word *av) C_noret;
C_noret_decl(f_8351)
static void C_ccall f_8351(C_word c,C_word *av) C_noret;
C_noret_decl(f_8355)
static void C_ccall f_8355(C_word c,C_word *av) C_noret;
C_noret_decl(f_8360)
static void C_ccall f_8360(C_word c,C_word *av) C_noret;
C_noret_decl(f_8367)
static void C_ccall f_8367(C_word c,C_word *av) C_noret;
C_noret_decl(f_8372)
static void C_ccall f_8372(C_word c,C_word *av) C_noret;
C_noret_decl(f_8376)
static void C_ccall f_8376(C_word c,C_word *av) C_noret;
C_noret_decl(f_8380)
static void C_ccall f_8380(C_word c,C_word *av) C_noret;
C_noret_decl(f_8384)
static void C_ccall f_8384(C_word c,C_word *av) C_noret;
C_noret_decl(f_8389)
static void C_ccall f_8389(C_word c,C_word *av) C_noret;
C_noret_decl(f_8393)
static void C_ccall f_8393(C_word c,C_word *av) C_noret;
C_noret_decl(f_8397)
static void C_ccall f_8397(C_word c,C_word *av) C_noret;
C_noret_decl(f_8401)
static void C_ccall f_8401(C_word c,C_word *av) C_noret;
C_noret_decl(f_8403)
static void C_ccall f_8403(C_word c,C_word *av) C_noret;
C_noret_decl(f_8409)
static void C_ccall f_8409(C_word c,C_word *av) C_noret;
C_noret_decl(f_8437)
static void C_ccall f_8437(C_word c,C_word *av) C_noret;
C_noret_decl(f_8447)
static void C_ccall f_8447(C_word c,C_word *av) C_noret;
C_noret_decl(f_8461)
static void C_fcall f_8461(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8486)
static void C_ccall f_8486(C_word c,C_word *av) C_noret;
C_noret_decl(f_8503)
static void C_ccall f_8503(C_word c,C_word *av) C_noret;
C_noret_decl(f_8510)
static void C_fcall f_8510(C_word t0,C_word t1) C_noret;
C_noret_decl(f_8531)
static void C_ccall f_8531(C_word c,C_word *av) C_noret;
C_noret_decl(f_8535)
static void C_ccall f_8535(C_word c,C_word *av) C_noret;
C_noret_decl(f_8539)
static void C_ccall f_8539(C_word c,C_word *av) C_noret;
C_noret_decl(f_8541)
static void C_ccall f_8541(C_word c,C_word *av) C_noret;
C_noret_decl(f_8546)
static void C_ccall f_8546(C_word c,C_word *av) C_noret;
C_noret_decl(f_8569)
static void C_ccall f_8569(C_word c,C_word *av) C_noret;
C_noret_decl(f_8603)
static void C_ccall f_8603(C_word c,C_word *av) C_noret;
C_noret_decl(f_8639)
static void C_ccall f_8639(C_word c,C_word *av) C_noret;
C_noret_decl(f_8643)
static void C_ccall f_8643(C_word c,C_word *av) C_noret;
C_noret_decl(f_8647)
static void C_ccall f_8647(C_word c,C_word *av) C_noret;
C_noret_decl(f_8696)
static void C_ccall f_8696(C_word c,C_word *av) C_noret;
C_noret_decl(f_8704)
static void C_ccall f_8704(C_word c,C_word *av) C_noret;
C_noret_decl(f_8717)
static void C_fcall f_8717(C_word t0,C_word t1) C_noret;
C_noret_decl(f_8750)
static void C_ccall f_8750(C_word c,C_word *av) C_noret;
C_noret_decl(f_8754)
static void C_ccall f_8754(C_word c,C_word *av) C_noret;
C_noret_decl(f_8809)
static void C_ccall f_8809(C_word c,C_word *av) C_noret;
C_noret_decl(f_8833)
static void C_ccall f_8833(C_word c,C_word *av) C_noret;
C_noret_decl(f_8877)
static void C_ccall f_8877(C_word c,C_word *av) C_noret;
C_noret_decl(f_8901)
static void C_ccall f_8901(C_word c,C_word *av) C_noret;
C_noret_decl(f_8907)
static void C_ccall f_8907(C_word c,C_word *av) C_noret;
C_noret_decl(f_8920)
static void C_ccall f_8920(C_word c,C_word *av) C_noret;
C_noret_decl(f_8924)
static void C_ccall f_8924(C_word c,C_word *av) C_noret;
C_noret_decl(f_8936)
static void C_ccall f_8936(C_word c,C_word *av) C_noret;
C_noret_decl(f_8982)
static void C_ccall f_8982(C_word c,C_word *av) C_noret;
C_noret_decl(f_8986)
static void C_ccall f_8986(C_word c,C_word *av) C_noret;
C_noret_decl(f_9011)
static void C_ccall f_9011(C_word c,C_word *av) C_noret;
C_noret_decl(f_9017)
static void C_ccall f_9017(C_word c,C_word *av) C_noret;
C_noret_decl(f_9056)
static void C_ccall f_9056(C_word c,C_word *av) C_noret;
C_noret_decl(f_9059)
static void C_ccall f_9059(C_word c,C_word *av) C_noret;
C_noret_decl(f_9065)
static void C_ccall f_9065(C_word c,C_word *av) C_noret;
C_noret_decl(f_9077)
static void C_ccall f_9077(C_word c,C_word *av) C_noret;
C_noret_decl(f_9080)
static void C_fcall f_9080(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9083)
static void C_ccall f_9083(C_word c,C_word *av) C_noret;
C_noret_decl(f_9096)
static void C_ccall f_9096(C_word c,C_word *av) C_noret;
C_noret_decl(f_9100)
static void C_ccall f_9100(C_word c,C_word *av) C_noret;
C_noret_decl(f_9104)
static void C_ccall f_9104(C_word c,C_word *av) C_noret;
C_noret_decl(f_9106)
static void C_fcall f_9106(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_9127)
static void C_fcall f_9127(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9179)
static void C_ccall f_9179(C_word c,C_word *av) C_noret;
C_noret_decl(f_9183)
static void C_ccall f_9183(C_word c,C_word *av) C_noret;
C_noret_decl(f_9200)
static void C_ccall f_9200(C_word c,C_word *av) C_noret;
C_noret_decl(f_9204)
static void C_ccall f_9204(C_word c,C_word *av) C_noret;
C_noret_decl(f_9209)
static void C_ccall f_9209(C_word c,C_word *av) C_noret;
C_noret_decl(f_9235)
static void C_ccall f_9235(C_word c,C_word *av) C_noret;
C_noret_decl(f_9250)
static void C_ccall f_9250(C_word c,C_word *av) C_noret;
C_noret_decl(f_9269)
static void C_ccall f_9269(C_word c,C_word *av) C_noret;
C_noret_decl(f_9284)
static void C_ccall f_9284(C_word c,C_word *av) C_noret;
C_noret_decl(f_9286)
static void C_ccall f_9286(C_word c,C_word *av) C_noret;
C_noret_decl(f_9328)
static void C_ccall f_9328(C_word c,C_word *av) C_noret;
C_noret_decl(f_9339)
static void C_ccall f_9339(C_word c,C_word *av) C_noret;
C_noret_decl(f_9358)
static void C_ccall f_9358(C_word c,C_word *av) C_noret;
C_noret_decl(f_9373)
static void C_ccall f_9373(C_word c,C_word *av) C_noret;
C_noret_decl(f_9375)
static void C_ccall f_9375(C_word c,C_word *av) C_noret;
C_noret_decl(f_9382)
static void C_ccall f_9382(C_word c,C_word *av) C_noret;
C_noret_decl(f_9403)
static void C_ccall f_9403(C_word c,C_word *av) C_noret;
C_noret_decl(f_9427)
static void C_ccall f_9427(C_word c,C_word *av) C_noret;
C_noret_decl(f_9434)
static void C_ccall f_9434(C_word c,C_word *av) C_noret;
C_noret_decl(f_9441)
static void C_ccall f_9441(C_word c,C_word *av) C_noret;
C_noret_decl(f_9447)
static void C_ccall f_9447(C_word c,C_word *av) C_noret;
C_noret_decl(f_9457)
static void C_fcall f_9457(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9464)
static void C_ccall f_9464(C_word c,C_word *av) C_noret;
C_noret_decl(f_9485)
static void C_ccall f_9485(C_word c,C_word *av) C_noret;
C_noret_decl(f_9489)
static void C_ccall f_9489(C_word c,C_word *av) C_noret;
C_noret_decl(f_9493)
static void C_ccall f_9493(C_word c,C_word *av) C_noret;
C_noret_decl(f_9497)
static void C_ccall f_9497(C_word c,C_word *av) C_noret;
C_noret_decl(f_9501)
static void C_ccall f_9501(C_word c,C_word *av) C_noret;
C_noret_decl(f_9505)
static void C_ccall f_9505(C_word c,C_word *av) C_noret;
C_noret_decl(f_9507)
static void C_ccall f_9507(C_word c,C_word *av) C_noret;
C_noret_decl(f_9511)
static void C_ccall f_9511(C_word c,C_word *av) C_noret;
C_noret_decl(f_9519)
static void C_fcall f_9519(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9528)
static void C_ccall f_9528(C_word c,C_word *av) C_noret;
C_noret_decl(f_9541)
static void C_ccall f_9541(C_word c,C_word *av) C_noret;
C_noret_decl(f_9543)
static void C_ccall f_9543(C_word c,C_word *av) C_noret;
C_noret_decl(f_9547)
static void C_ccall f_9547(C_word c,C_word *av) C_noret;
C_noret_decl(f_9554)
static void C_ccall f_9554(C_word c,C_word *av) C_noret;
C_noret_decl(f_9574)
static void C_ccall f_9574(C_word c,C_word *av) C_noret;
C_noret_decl(f_9576)
static void C_ccall f_9576(C_word c,C_word *av) C_noret;
C_noret_decl(f_9580)
static void C_ccall f_9580(C_word c,C_word *av) C_noret;
C_noret_decl(f_9583)
static void C_ccall f_9583(C_word c,C_word *av) C_noret;
C_noret_decl(f_9586)
static void C_ccall f_9586(C_word c,C_word *av) C_noret;
C_noret_decl(f_9588)
static void C_fcall f_9588(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_9596)
static void C_ccall f_9596(C_word c,C_word *av) C_noret;
C_noret_decl(f_9598)
static void C_fcall f_9598(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_9612)
static void C_ccall f_9612(C_word c,C_word *av) C_noret;
C_noret_decl(f_9616)
static void C_ccall f_9616(C_word c,C_word *av) C_noret;
C_noret_decl(f_9635)
static void C_ccall f_9635(C_word c,C_word *av) C_noret;
C_noret_decl(f_9644)
static void C_ccall f_9644(C_word c,C_word *av) C_noret;
C_noret_decl(f_9658)
static void C_ccall f_9658(C_word c,C_word *av) C_noret;
C_noret_decl(f_9668)
static void C_ccall f_9668(C_word c,C_word *av) C_noret;
C_noret_decl(f_9679)
static void C_ccall f_9679(C_word c,C_word *av) C_noret;
C_noret_decl(f_9689)
static void C_ccall f_9689(C_word c,C_word *av) C_noret;
C_noret_decl(f_9698)
static void C_ccall f_9698(C_word c,C_word *av) C_noret;
C_noret_decl(f_9709)
static void C_ccall f_9709(C_word c,C_word *av) C_noret;
C_noret_decl(f_9720)
static void C_ccall f_9720(C_word c,C_word *av) C_noret;
C_noret_decl(f_9728)
static void C_ccall f_9728(C_word c,C_word *av) C_noret;
C_noret_decl(f_9743)
static void C_ccall f_9743(C_word c,C_word *av) C_noret;
C_noret_decl(f_9747)
static void C_ccall f_9747(C_word c,C_word *av) C_noret;
C_noret_decl(f_9761)
static void C_fcall f_9761(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9765)
static void C_ccall f_9765(C_word c,C_word *av) C_noret;
C_noret_decl(f_9769)
static void C_fcall f_9769(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9791)
static void C_ccall f_9791(C_word c,C_word *av) C_noret;
C_noret_decl(f_9795)
static void C_fcall f_9795(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9838)
static void C_ccall f_9838(C_word c,C_word *av) C_noret;
C_noret_decl(f_9856)
static void C_ccall f_9856(C_word c,C_word *av) C_noret;
C_noret_decl(f_9867)
static void C_ccall f_9867(C_word c,C_word *av) C_noret;
C_noret_decl(f_9869)
static void C_ccall f_9869(C_word c,C_word *av) C_noret;
C_noret_decl(f_9873)
static void C_ccall f_9873(C_word c,C_word *av) C_noret;
C_noret_decl(f_9885)
static void C_ccall f_9885(C_word c,C_word *av) C_noret;
C_noret_decl(f_9913)
static void C_ccall f_9913(C_word c,C_word *av) C_noret;
C_noret_decl(f_9934)
static void C_fcall f_9934(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9975)
static void C_ccall f_9975(C_word c,C_word *av) C_noret;
C_noret_decl(f_9977)
static void C_fcall f_9977(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9987)
static void C_fcall f_9987(C_word t0,C_word t1) C_noret;
C_noret_decl(C_expand_toplevel)
C_externexport void C_ccall C_expand_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_10027)
static void C_ccall trf_10027(C_word c,C_word *av) C_noret;
static void C_ccall trf_10027(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10027(t0,t1,t2);}

C_noret_decl(trf_10079)
static void C_ccall trf_10079(C_word c,C_word *av) C_noret;
static void C_ccall trf_10079(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10079(t0,t1,t2);}

C_noret_decl(trf_10153)
static void C_ccall trf_10153(C_word c,C_word *av) C_noret;
static void C_ccall trf_10153(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10153(t0,t1,t2,t3);}

C_noret_decl(trf_10245)
static void C_ccall trf_10245(C_word c,C_word *av) C_noret;
static void C_ccall trf_10245(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_10245(t0,t1);}

C_noret_decl(trf_10302)
static void C_ccall trf_10302(C_word c,C_word *av) C_noret;
static void C_ccall trf_10302(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10302(t0,t1,t2);}

C_noret_decl(trf_10370)
static void C_ccall trf_10370(C_word c,C_word *av) C_noret;
static void C_ccall trf_10370(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10370(t0,t1,t2,t3);}

C_noret_decl(trf_11042)
static void C_ccall trf_11042(C_word c,C_word *av) C_noret;
static void C_ccall trf_11042(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11042(t0,t1,t2);}

C_noret_decl(trf_11398)
static void C_ccall trf_11398(C_word c,C_word *av) C_noret;
static void C_ccall trf_11398(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11398(t0,t1,t2);}

C_noret_decl(trf_11464)
static void C_ccall trf_11464(C_word c,C_word *av) C_noret;
static void C_ccall trf_11464(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11464(t0,t1,t2);}

C_noret_decl(trf_11559)
static void C_ccall trf_11559(C_word c,C_word *av) C_noret;
static void C_ccall trf_11559(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_11559(t0,t1);}

C_noret_decl(trf_11656)
static void C_ccall trf_11656(C_word c,C_word *av) C_noret;
static void C_ccall trf_11656(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_11656(t0,t1);}

C_noret_decl(trf_11724)
static void C_ccall trf_11724(C_word c,C_word *av) C_noret;
static void C_ccall trf_11724(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11724(t0,t1,t2);}

C_noret_decl(trf_11734)
static void C_ccall trf_11734(C_word c,C_word *av) C_noret;
static void C_ccall trf_11734(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11734(t0,t1,t2);}

C_noret_decl(trf_11879)
static void C_ccall trf_11879(C_word c,C_word *av) C_noret;
static void C_ccall trf_11879(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11879(t0,t1,t2);}

C_noret_decl(trf_11906)
static void C_ccall trf_11906(C_word c,C_word *av) C_noret;
static void C_ccall trf_11906(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11906(t0,t1,t2);}

C_noret_decl(trf_12055)
static void C_ccall trf_12055(C_word c,C_word *av) C_noret;
static void C_ccall trf_12055(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_12055(t0,t1,t2);}

C_noret_decl(trf_12123)
static void C_ccall trf_12123(C_word c,C_word *av) C_noret;
static void C_ccall trf_12123(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_12123(t0,t1,t2);}

C_noret_decl(trf_3735)
static void C_ccall trf_3735(C_word c,C_word *av) C_noret;
static void C_ccall trf_3735(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3735(t0,t1,t2);}

C_noret_decl(trf_3788)
static void C_ccall trf_3788(C_word c,C_word *av) C_noret;
static void C_ccall trf_3788(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3788(t0,t1,t2);}

C_noret_decl(trf_3893)
static void C_ccall trf_3893(C_word c,C_word *av) C_noret;
static void C_ccall trf_3893(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3893(t0,t1,t2);}

C_noret_decl(trf_3970)
static void C_ccall trf_3970(C_word c,C_word *av) C_noret;
static void C_ccall trf_3970(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3970(t0,t1,t2,t3);}

C_noret_decl(trf_4018)
static void C_ccall trf_4018(C_word c,C_word *av) C_noret;
static void C_ccall trf_4018(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_4018(t0,t1,t2,t3);}

C_noret_decl(trf_4067)
static void C_ccall trf_4067(C_word c,C_word *av) C_noret;
static void C_ccall trf_4067(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4067(t0,t1,t2);}

C_noret_decl(trf_4110)
static void C_ccall trf_4110(C_word c,C_word *av) C_noret;
static void C_ccall trf_4110(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4110(t0,t1,t2);}

C_noret_decl(trf_4126)
static void C_ccall trf_4126(C_word c,C_word *av) C_noret;
static void C_ccall trf_4126(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4126(t0,t1,t2);}

C_noret_decl(trf_4144)
static void C_ccall trf_4144(C_word c,C_word *av) C_noret;
static void C_ccall trf_4144(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4144(t0,t1,t2);}

C_noret_decl(trf_4315)
static void C_ccall trf_4315(C_word c,C_word *av) C_noret;
static void C_ccall trf_4315(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4315(t0,t1,t2);}

C_noret_decl(trf_4349)
static void C_ccall trf_4349(C_word c,C_word *av) C_noret;
static void C_ccall trf_4349(C_word c,C_word *av){
C_word t0=av[6];
C_word t1=av[5];
C_word t2=av[4];
C_word t3=av[3];
C_word t4=av[2];
C_word t5=av[1];
C_word t6=av[0];
f_4349(t0,t1,t2,t3,t4,t5,t6);}

C_noret_decl(trf_4404)
static void C_ccall trf_4404(C_word c,C_word *av) C_noret;
static void C_ccall trf_4404(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4404(t0,t1,t2);}

C_noret_decl(trf_4421)
static void C_ccall trf_4421(C_word c,C_word *av) C_noret;
static void C_ccall trf_4421(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4421(t0,t1);}

C_noret_decl(trf_4548)
static void C_ccall trf_4548(C_word c,C_word *av) C_noret;
static void C_ccall trf_4548(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_4548(t0,t1,t2,t3,t4);}

C_noret_decl(trf_4592)
static void C_ccall trf_4592(C_word c,C_word *av) C_noret;
static void C_ccall trf_4592(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4592(t0,t1,t2);}

C_noret_decl(trf_4616)
static void C_ccall trf_4616(C_word c,C_word *av) C_noret;
static void C_ccall trf_4616(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4616(t0,t1);}

C_noret_decl(trf_4664)
static void C_ccall trf_4664(C_word c,C_word *av) C_noret;
static void C_ccall trf_4664(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4664(t0,t1,t2);}

C_noret_decl(trf_4733)
static void C_ccall trf_4733(C_word c,C_word *av) C_noret;
static void C_ccall trf_4733(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4733(t0,t1,t2);}

C_noret_decl(trf_4771)
static void C_ccall trf_4771(C_word c,C_word *av) C_noret;
static void C_ccall trf_4771(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4771(t0,t1);}

C_noret_decl(trf_4775)
static void C_ccall trf_4775(C_word c,C_word *av) C_noret;
static void C_ccall trf_4775(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4775(t0,t1,t2);}

C_noret_decl(trf_4865)
static void C_ccall trf_4865(C_word c,C_word *av) C_noret;
static void C_ccall trf_4865(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4865(t0,t1,t2);}

C_noret_decl(trf_4963)
static void C_ccall trf_4963(C_word c,C_word *av) C_noret;
static void C_ccall trf_4963(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4963(t0,t1,t2);}

C_noret_decl(trf_4991)
static void C_ccall trf_4991(C_word c,C_word *av) C_noret;
static void C_ccall trf_4991(C_word c,C_word *av){
C_word t0=av[6];
C_word t1=av[5];
C_word t2=av[4];
C_word t3=av[3];
C_word t4=av[2];
C_word t5=av[1];
C_word t6=av[0];
f_4991(t0,t1,t2,t3,t4,t5,t6);}

C_noret_decl(trf_5009)
static void C_ccall trf_5009(C_word c,C_word *av) C_noret;
static void C_ccall trf_5009(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5009(t0,t1);}

C_noret_decl(trf_5021)
static void C_ccall trf_5021(C_word c,C_word *av) C_noret;
static void C_ccall trf_5021(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5021(t0,t1);}

C_noret_decl(trf_5149)
static void C_ccall trf_5149(C_word c,C_word *av) C_noret;
static void C_ccall trf_5149(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5149(t0,t1,t2);}

C_noret_decl(trf_5214)
static void C_ccall trf_5214(C_word c,C_word *av) C_noret;
static void C_ccall trf_5214(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5214(t0,t1,t2);}

C_noret_decl(trf_5294)
static void C_ccall trf_5294(C_word c,C_word *av) C_noret;
static void C_ccall trf_5294(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5294(t0,t1);}

C_noret_decl(trf_5307)
static void C_ccall trf_5307(C_word c,C_word *av) C_noret;
static void C_ccall trf_5307(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5307(t0,t1);}

C_noret_decl(trf_5344)
static void C_ccall trf_5344(C_word c,C_word *av) C_noret;
static void C_ccall trf_5344(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5344(t0,t1);}

C_noret_decl(trf_5383)
static void C_ccall trf_5383(C_word c,C_word *av) C_noret;
static void C_ccall trf_5383(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5383(t0,t1);}

C_noret_decl(trf_5470)
static void C_ccall trf_5470(C_word c,C_word *av) C_noret;
static void C_ccall trf_5470(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5470(t0,t1);}

C_noret_decl(trf_5645)
static void C_ccall trf_5645(C_word c,C_word *av) C_noret;
static void C_ccall trf_5645(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_5645(t0,t1,t2,t3);}

C_noret_decl(trf_5711)
static void C_ccall trf_5711(C_word c,C_word *av) C_noret;
static void C_ccall trf_5711(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5711(t0,t1,t2);}

C_noret_decl(trf_5847)
static void C_ccall trf_5847(C_word c,C_word *av) C_noret;
static void C_ccall trf_5847(C_word c,C_word *av){
C_word t0=av[5];
C_word t1=av[4];
C_word t2=av[3];
C_word t3=av[2];
C_word t4=av[1];
C_word t5=av[0];
f_5847(t0,t1,t2,t3,t4,t5);}

C_noret_decl(trf_5859)
static void C_ccall trf_5859(C_word c,C_word *av) C_noret;
static void C_ccall trf_5859(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_5859(t0,t1,t2,t3);}

C_noret_decl(trf_5878)
static void C_ccall trf_5878(C_word c,C_word *av) C_noret;
static void C_ccall trf_5878(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5878(t0,t1,t2);}

C_noret_decl(trf_5889)
static void C_ccall trf_5889(C_word c,C_word *av) C_noret;
static void C_ccall trf_5889(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5889(t0,t1);}

C_noret_decl(trf_5940)
static void C_ccall trf_5940(C_word c,C_word *av) C_noret;
static void C_ccall trf_5940(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5940(t0,t1);}

C_noret_decl(trf_6146)
static void C_ccall trf_6146(C_word c,C_word *av) C_noret;
static void C_ccall trf_6146(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_6146(t0,t1,t2,t3,t4);}

C_noret_decl(trf_6153)
static void C_ccall trf_6153(C_word c,C_word *av) C_noret;
static void C_ccall trf_6153(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6153(t0,t1);}

C_noret_decl(trf_6208)
static void C_ccall trf_6208(C_word c,C_word *av) C_noret;
static void C_ccall trf_6208(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6208(t0,t1,t2);}

C_noret_decl(trf_6242)
static void C_ccall trf_6242(C_word c,C_word *av) C_noret;
static void C_ccall trf_6242(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6242(t0,t1,t2,t3);}

C_noret_decl(trf_6284)
static void C_ccall trf_6284(C_word c,C_word *av) C_noret;
static void C_ccall trf_6284(C_word c,C_word *av){
C_word t0=av[5];
C_word t1=av[4];
C_word t2=av[3];
C_word t3=av[2];
C_word t4=av[1];
C_word t5=av[0];
f_6284(t0,t1,t2,t3,t4,t5);}

C_noret_decl(trf_6294)
static void C_ccall trf_6294(C_word c,C_word *av) C_noret;
static void C_ccall trf_6294(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_6294(t0,t1,t2,t3,t4);}

C_noret_decl(trf_6321)
static void C_ccall trf_6321(C_word c,C_word *av) C_noret;
static void C_ccall trf_6321(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6321(t0,t1,t2);}

C_noret_decl(trf_6368)
static void C_ccall trf_6368(C_word c,C_word *av) C_noret;
static void C_ccall trf_6368(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6368(t0,t1);}

C_noret_decl(trf_6440)
static void C_ccall trf_6440(C_word c,C_word *av) C_noret;
static void C_ccall trf_6440(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6440(t0,t1,t2);}

C_noret_decl(trf_6446)
static void C_ccall trf_6446(C_word c,C_word *av) C_noret;
static void C_ccall trf_6446(C_word c,C_word *av){
C_word t0=av[5];
C_word t1=av[4];
C_word t2=av[3];
C_word t3=av[2];
C_word t4=av[1];
C_word t5=av[0];
f_6446(t0,t1,t2,t3,t4,t5);}

C_noret_decl(trf_6466)
static void C_ccall trf_6466(C_word c,C_word *av) C_noret;
static void C_ccall trf_6466(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6466(t0,t1);}

C_noret_decl(trf_6489)
static void C_ccall trf_6489(C_word c,C_word *av) C_noret;
static void C_ccall trf_6489(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6489(t0,t1,t2);}

C_noret_decl(trf_6758)
static void C_ccall trf_6758(C_word c,C_word *av) C_noret;
static void C_ccall trf_6758(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6758(t0,t1,t2,t3);}

C_noret_decl(trf_6840)
static void C_ccall trf_6840(C_word c,C_word *av) C_noret;
static void C_ccall trf_6840(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6840(t0,t1,t2,t3);}

C_noret_decl(trf_6919)
static void C_ccall trf_6919(C_word c,C_word *av) C_noret;
static void C_ccall trf_6919(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6919(t0,t1,t2);}

C_noret_decl(trf_6972)
static void C_ccall trf_6972(C_word c,C_word *av) C_noret;
static void C_ccall trf_6972(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6972(t0,t1,t2);}

C_noret_decl(trf_6988)
static void C_ccall trf_6988(C_word c,C_word *av) C_noret;
static void C_ccall trf_6988(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6988(t0,t1,t2);}

C_noret_decl(trf_7085)
static void C_ccall trf_7085(C_word c,C_word *av) C_noret;
static void C_ccall trf_7085(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7085(t0,t1,t2);}

C_noret_decl(trf_7178)
static void C_ccall trf_7178(C_word c,C_word *av) C_noret;
static void C_ccall trf_7178(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_7178(t0,t1,t2,t3,t4);}

C_noret_decl(trf_7190)
static void C_ccall trf_7190(C_word c,C_word *av) C_noret;
static void C_ccall trf_7190(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7190(t0,t1,t2);}

C_noret_decl(trf_7295)
static void C_ccall trf_7295(C_word c,C_word *av) C_noret;
static void C_ccall trf_7295(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7295(t0,t1);}

C_noret_decl(trf_7300)
static void C_ccall trf_7300(C_word c,C_word *av) C_noret;
static void C_ccall trf_7300(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7300(t0,t1,t2,t3);}

C_noret_decl(trf_7319)
static void C_ccall trf_7319(C_word c,C_word *av) C_noret;
static void C_ccall trf_7319(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7319(t0,t1);}

C_noret_decl(trf_7324)
static void C_ccall trf_7324(C_word c,C_word *av) C_noret;
static void C_ccall trf_7324(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7324(t0,t1,t2,t3);}

C_noret_decl(trf_7607)
static void C_ccall trf_7607(C_word c,C_word *av) C_noret;
static void C_ccall trf_7607(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7607(t0,t1,t2);}

C_noret_decl(trf_7769)
static void C_ccall trf_7769(C_word c,C_word *av) C_noret;
static void C_ccall trf_7769(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7769(t0,t1,t2,t3);}

C_noret_decl(trf_7813)
static void C_ccall trf_7813(C_word c,C_word *av) C_noret;
static void C_ccall trf_7813(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7813(t0,t1);}

C_noret_decl(trf_7821)
static void C_ccall trf_7821(C_word c,C_word *av) C_noret;
static void C_ccall trf_7821(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7821(t0,t1);}

C_noret_decl(trf_7829)
static void C_ccall trf_7829(C_word c,C_word *av) C_noret;
static void C_ccall trf_7829(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7829(t0,t1);}

C_noret_decl(trf_7957)
static void C_ccall trf_7957(C_word c,C_word *av) C_noret;
static void C_ccall trf_7957(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7957(t0,t1,t2);}

C_noret_decl(trf_8242)
static void C_ccall trf_8242(C_word c,C_word *av) C_noret;
static void C_ccall trf_8242(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_8242(t0,t1,t2,t3);}

C_noret_decl(trf_8461)
static void C_ccall trf_8461(C_word c,C_word *av) C_noret;
static void C_ccall trf_8461(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8461(t0,t1,t2);}

C_noret_decl(trf_8510)
static void C_ccall trf_8510(C_word c,C_word *av) C_noret;
static void C_ccall trf_8510(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_8510(t0,t1);}

C_noret_decl(trf_8717)
static void C_ccall trf_8717(C_word c,C_word *av) C_noret;
static void C_ccall trf_8717(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_8717(t0,t1);}

C_noret_decl(trf_9080)
static void C_ccall trf_9080(C_word c,C_word *av) C_noret;
static void C_ccall trf_9080(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9080(t0,t1);}

C_noret_decl(trf_9106)
static void C_ccall trf_9106(C_word c,C_word *av) C_noret;
static void C_ccall trf_9106(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_9106(t0,t1,t2,t3);}

C_noret_decl(trf_9127)
static void C_ccall trf_9127(C_word c,C_word *av) C_noret;
static void C_ccall trf_9127(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9127(t0,t1);}

C_noret_decl(trf_9457)
static void C_ccall trf_9457(C_word c,C_word *av) C_noret;
static void C_ccall trf_9457(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9457(t0,t1,t2);}

C_noret_decl(trf_9519)
static void C_ccall trf_9519(C_word c,C_word *av) C_noret;
static void C_ccall trf_9519(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9519(t0,t1);}

C_noret_decl(trf_9588)
static void C_ccall trf_9588(C_word c,C_word *av) C_noret;
static void C_ccall trf_9588(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_9588(t0,t1,t2,t3);}

C_noret_decl(trf_9598)
static void C_ccall trf_9598(C_word c,C_word *av) C_noret;
static void C_ccall trf_9598(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_9598(t0,t1,t2,t3);}

C_noret_decl(trf_9761)
static void C_ccall trf_9761(C_word c,C_word *av) C_noret;
static void C_ccall trf_9761(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9761(t0,t1,t2);}

C_noret_decl(trf_9769)
static void C_ccall trf_9769(C_word c,C_word *av) C_noret;
static void C_ccall trf_9769(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9769(t0,t1,t2);}

C_noret_decl(trf_9795)
static void C_ccall trf_9795(C_word c,C_word *av) C_noret;
static void C_ccall trf_9795(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9795(t0,t1,t2);}

C_noret_decl(trf_9934)
static void C_ccall trf_9934(C_word c,C_word *av) C_noret;
static void C_ccall trf_9934(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9934(t0,t1);}

C_noret_decl(trf_9977)
static void C_ccall trf_9977(C_word c,C_word *av) C_noret;
static void C_ccall trf_9977(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9977(t0,t1,t2);}

C_noret_decl(trf_9987)
static void C_ccall trf_9987(C_word c,C_word *av) C_noret;
static void C_ccall trf_9987(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9987(t0,t1);}

/* map-loop2424 in k9883 in k9871 in a9868 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in ... */
static void C_fcall f_10027(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_10027,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_i_car(C_u_i_cdr(t3));
t6=C_a_i_list2(&a,2,t4,t5);
t7=C_a_i_cons(&a,2,t6,C_SCHEME_END_OF_LIST);
t8=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t7);
t9=C_mutate(((C_word *)((C_word*)t0)[2])+1,t7);
t11=t1;
t12=C_slot(t2,C_fix(1));
t1=t11;
t2=t12;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10061 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in ... */
static void C_ccall f_10063(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10063,c,av);}
/* expand.scm:1476: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[92];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a10064 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in ... */
static void C_ccall f_10065(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_10065,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10069,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1481: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[92];
av2[3]=t2;
av2[4]=lf[265];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k10067 in a10064 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in ... */
static void C_ccall f_10069(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_10069,c,av);}
a=C_alloc(7);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_u_i_cdr(t3);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10079,a[2]=t4,a[3]=t6,a[4]=((C_word)li138),tmp=(C_word)a,a+=5,tmp));
t8=((C_word*)t6)[1];
f_10079(t8,((C_word*)t0)[3],t2);}

/* expand in k10067 in a10064 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in ... */
static void C_fcall f_10079(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_10079,3,t0,t1,t2);}
a=C_alloc(7);
t3=C_eqp(t2,C_SCHEME_END_OF_LIST);
if(C_truep(t3)){
t4=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,((C_word*)t0)[2]);
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_a_i_cons(&a,2,lf[55],t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=C_i_car(t2);
t5=C_a_i_list(&a,1,t4);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10104,a[2]=t1,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1487: expand */
t8=t6;
t9=C_u_i_cdr(t2);
t1=t8;
t2=t9;
goto loop;}}

/* k10102 in expand in k10067 in a10064 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in ... */
static void C_ccall f_10104(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_10104,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[55],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k10112 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in ... */
static void C_ccall f_10114(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10114,c,av);}
/* expand.scm:1431: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[266];
av2[3]=lf[267];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in ... */
static void C_ccall f_10116(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_10116,c,av);}
a=C_alloc(6);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10120,a[2]=t2,a[3]=t1,a[4]=t4,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1436: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[266];
av2[3]=t2;
av2[4]=lf[277];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in ... */
static void C_ccall f_10120(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_10120,c,av);}
a=C_alloc(7);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_u_i_cdr(t3);
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10128,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t4,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* expand.scm:1439: r */
t6=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[276];
((C_proc)C_fast_retrieve_proc(t6))(3,av2);}}

/* k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in ... */
static void C_ccall f_10128(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_10128,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_10131,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* expand.scm:1440: r */
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[228];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in ... */
static void C_ccall f_10131(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_10131,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_10134,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* expand.scm:1441: r */
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[275];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in ... */
static void C_ccall f_10134(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_10134,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_10137,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* expand.scm:1442: r */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[274];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in ... */
static void C_ccall f_10137(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_10137,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_10140,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* expand.scm:1443: r */
t3=((C_word*)t0)[9];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[234];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* k10138 in k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in ... */
static void C_ccall f_10140(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(25,c,4)))){
C_save_and_reclaim((void *)f_10140,c,av);}
a=C_alloc(25);
t2=C_a_i_list(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_list(&a,1,t2);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10151,a[2]=((C_word*)t0)[4],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_10153,a[2]=t6,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=t1,a[9]=((C_word)li142),tmp=(C_word)a,a+=10,tmp));
t8=((C_word*)t6)[1];
f_10153(t8,t4,((C_word*)t0)[9],C_SCHEME_FALSE);}

/* k10149 in k10138 in k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_ccall f_10151(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_10151,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[60],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* expand in k10138 in k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_fcall f_10153(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,0,4)))){
C_save_and_reclaim_args((void *)trf_10153,4,t0,t1,t2,t3);}
a=C_alloc(13);
t4=C_i_pairp(t2);
if(C_truep(C_i_not(t4))){
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=lf[268];
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_i_car(t2);
t6=C_u_i_cdr(t2);
t7=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_10167,a[2]=t3,a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=t6,a[6]=t5,a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[4],a[9]=((C_word*)t0)[5],a[10]=((C_word*)t0)[6],a[11]=((C_word*)t0)[7],a[12]=((C_word*)t0)[8],tmp=(C_word)a,a+=13,tmp);
/* expand.scm:1450: ##sys#check-syntax */
t8=*((C_word*)lf[59]+1);{
C_word av2[5];
av2[0]=t8;
av2[1]=t7;
av2[2]=lf[266];
av2[3]=t5;
av2[4]=lf[273];
((C_proc)(void*)(*((C_word*)t8+1)))(5,av2);}}}

/* k10165 in expand in k10138 in k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in ... */
static void C_ccall f_10167(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_10167,c,av);}
a=C_alloc(11);
if(C_truep(((C_word*)t0)[2])){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10173,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10180,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1454: chicken.syntax#strip-syntax */
t4=*((C_word*)lf[12]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_10186,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],tmp=(C_word)a,a+=11,tmp);
/* expand.scm:1457: c */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[12];
av2[3]=C_i_car(((C_word*)t0)[6]);
((C_proc)C_fast_retrieve_proc(t3))(4,av2);}}}

/* k10171 in k10165 in expand in k10138 in k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in ... */
static void C_ccall f_10173(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_10173,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10176,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1455: expand */
t3=((C_word*)((C_word*)t0)[3])[1];
f_10153(t3,t2,((C_word*)t0)[4],C_SCHEME_TRUE);}

/* k10174 in k10171 in k10165 in expand in k10138 in k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in ... */
static void C_ccall f_10176(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_10176,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[269];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k10178 in k10165 in expand in k10138 in k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in ... */
static void C_ccall f_10180(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_10180,c,av);}
/* expand.scm:1452: ##sys#warn */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[270]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[270]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=lf[271];
av2[3]=t1;
tp(4,av2);}}

/* k10184 in k10165 in expand in k10138 in k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in ... */
static void C_ccall f_10186(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,3)))){
C_save_and_reclaim((void *)f_10186,c,av);}
a=C_alloc(29);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10189,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* expand.scm:1458: expand */
t3=((C_word*)((C_word*)t0)[7])[1];
f_10153(t3,t2,((C_word*)t0)[8],C_SCHEME_TRUE);}
else{
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10285,a[2]=((C_word*)t0)[9],a[3]=((C_word*)t0)[4],a[4]=((C_word)li140),tmp=(C_word)a,a+=5,tmp);
t7=C_u_i_car(((C_word*)t0)[2]);
t8=C_i_check_list_2(t7,lf[17]);
t9=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_10300,a[2]=((C_word*)t0)[10],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[6],tmp=(C_word)a,a+=10,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10302,a[2]=t6,a[3]=t4,a[4]=t11,a[5]=t5,a[6]=((C_word)li141),tmp=(C_word)a,a+=7,tmp));
t13=((C_word*)t11)[1];
f_10302(t13,t9,t7);}}

/* k10187 in k10184 in k10165 in expand in k10138 in k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in ... */
static void C_ccall f_10189(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_10189,c,av);}
a=C_alloc(5);
if(C_truep(C_i_nullp(C_u_i_cdr(((C_word*)t0)[2])))){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,1,lf[272]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10204,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_i_length(((C_word*)t0)[2]);
t4=C_eqp(t3,C_fix(3));
if(C_truep(t4)){
/* expand.scm:1462: c */
t5=((C_word*)t0)[5];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=C_i_cadr(((C_word*)t0)[2]);
((C_proc)C_fast_retrieve_proc(t5))(4,av2);}}
else{
t5=t2;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
f_10204(2,av2);}}}}

/* k10202 in k10187 in k10184 in k10165 in expand in k10138 in k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in ... */
static void C_ccall f_10204(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_10204,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=C_i_caddr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,2,t2,((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=C_i_cdr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[108],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10243 in k10298 in k10184 in k10165 in expand in k10138 in k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in ... */
static void C_fcall f_10245(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_10245,2,t0,t1);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10249,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1474: expand */
t3=((C_word*)((C_word*)t0)[4])[1];
f_10153(t3,t2,((C_word*)t0)[5],C_SCHEME_FALSE);}

/* k10247 in k10243 in k10298 in k10184 in k10165 in expand in k10138 in k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in ... */
static void C_ccall f_10249(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,1)))){
C_save_and_reclaim((void *)f_10249,c,av);}
a=C_alloc(12);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,4,lf[261],((C_word*)t0)[3],((C_word*)t0)[4],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k10250 in k10298 in k10184 in k10165 in expand in k10138 in k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in ... */
static void C_ccall f_10252(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_10252,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=C_i_caddr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];
f_10245(t3,C_a_i_list(&a,2,t2,((C_word*)t0)[4]));}
else{
t2=C_i_cdr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];
f_10245(t3,C_a_i_cons(&a,2,lf[108],t2));}}

/* g2365 in k10184 in k10165 in expand in k10138 in k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in ... */
static C_word C_fcall f_10285(C_word *a,C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_stack_overflow_check;{}
t2=C_a_i_list(&a,2,lf[219],t1);
return(C_a_i_list(&a,3,((C_word*)t0)[2],((C_word*)t0)[3],t2));}

/* k10298 in k10184 in k10165 in expand in k10138 in k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in ... */
static void C_ccall f_10300(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_10300,c,av);}
a=C_alloc(14);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10245,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10252,a[2]=((C_word*)t0)[6],a[3]=t3,a[4]=((C_word*)t0)[7],tmp=(C_word)a,a+=5,tmp);
t5=C_i_length(((C_word*)t0)[6]);
t6=C_eqp(t5,C_fix(3));
if(C_truep(t6)){
/* expand.scm:1471: c */
t7=((C_word*)t0)[8];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t4;
av2[2]=((C_word*)t0)[9];
av2[3]=C_i_cadr(((C_word*)t0)[6]);
((C_proc)C_fast_retrieve_proc(t7))(4,av2);}}
else{
t7=t4;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_SCHEME_FALSE;
f_10252(2,av2);}}}

/* map-loop2359 in k10184 in k10165 in expand in k10138 in k10135 in k10132 in k10129 in k10126 in k10118 in a10115 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in ... */
static void C_fcall f_10302(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(18,0,2)))){
C_save_and_reclaim_args((void *)trf_10302,3,t0,t1,t2);}
a=C_alloc(18);
if(C_truep(C_i_pairp(t2))){
t3=(
/* expand.scm:1467: g2365 */
  f_10285(C_a_i(&a,15),((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10348 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in ... */
static void C_ccall f_10350(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10350,c,av);}
/* expand.scm:1366: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[235];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in ... */
static void C_ccall f_10352(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_10352,c,av);}
a=C_alloc(6);
t5=C_i_cdr(t2);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10359,a[2]=t4,a[3]=t3,a[4]=t1,a[5]=t5,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1372: r */
t7=t3;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[275];
((C_proc)C_fast_retrieve_proc(t7))(3,av2);}}

/* k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in ... */
static void C_ccall f_10359(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_10359,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10362,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* expand.scm:1373: r */
t3=((C_word*)t0)[3];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[228];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in ... */
static void C_ccall f_10362(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_10362,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_10365,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* expand.scm:1374: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[234];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in ... */
static void C_ccall f_10365(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_10365,c,av);}
a=C_alloc(11);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_10370,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=((C_word)li144),tmp=(C_word)a,a+=9,tmp));
t5=((C_word*)t3)[1];
f_10370(t5,((C_word*)t0)[6],((C_word*)t0)[7],C_SCHEME_FALSE);}

/* expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in ... */
static void C_fcall f_10370(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,4)))){
C_save_and_reclaim_args((void *)trf_10370,4,t0,t1,t2,t3);}
a=C_alloc(12);
t4=C_i_pairp(t2);
if(C_truep(C_i_not(t4))){
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=lf[278];
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_i_car(t2);
t6=C_u_i_cdr(t2);
t7=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_10384,a[2]=t3,a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=t6,a[6]=t5,a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[4],a[9]=((C_word*)t0)[5],a[10]=((C_word*)t0)[6],a[11]=((C_word*)t0)[7],tmp=(C_word)a,a+=12,tmp);
/* expand.scm:1380: ##sys#check-syntax */
t8=*((C_word*)lf[59]+1);{
C_word av2[5];
av2[0]=t8;
av2[1]=t7;
av2[2]=lf[235];
av2[3]=t5;
av2[4]=lf[286];
((C_proc)(void*)(*((C_word*)t8+1)))(5,av2);}}}

/* k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in ... */
static void C_ccall f_10384(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_10384,c,av);}
a=C_alloc(11);
if(C_truep(((C_word*)t0)[2])){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10390,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10397,a[2]=t2,a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1383: chicken.base#open-output-string */
t4=*((C_word*)lf[147]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_10422,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],tmp=(C_word)a,a+=11,tmp);
/* expand.scm:1387: c */
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[8];
av2[3]=C_i_car(((C_word*)t0)[6]);
((C_proc)C_fast_retrieve_proc(t3))(4,av2);}}}

/* k10388 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in ... */
static void C_ccall f_10390(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_10390,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10393,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1385: expand */
t3=((C_word*)((C_word*)t0)[3])[1];
f_10370(t3,t2,((C_word*)t0)[4],((C_word*)t0)[5]);}

/* k10391 in k10388 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in ... */
static void C_ccall f_10393(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_10393,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[279];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k10395 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in ... */
static void C_ccall f_10397(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_10397,c,av);}
a=C_alloc(6);
t2=C_i_check_port_2(t1,C_fix(2),C_SCHEME_TRUE,lf[280]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10403,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1383: ##sys#print */
t4=*((C_word*)lf[130]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[282];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k10401 in k10395 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in ... */
static void C_ccall f_10403(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_10403,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10406,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1383: ##sys#print */
t3=*((C_word*)lf[130]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k10404 in k10401 in k10395 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_ccall f_10406(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_10406,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10409,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1383: ##sys#print */
t3=*((C_word*)lf[130]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[281];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k10407 in k10404 in k10401 in k10395 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in ... */
static void C_ccall f_10409(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_10409,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10412,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1383: chicken.base#get-output-string */
t3=*((C_word*)lf[131]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k10410 in k10407 in k10404 in k10401 in k10395 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in ... */
static void C_ccall f_10412(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_10412,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10416,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1384: chicken.syntax#strip-syntax */
t3=*((C_word*)lf[12]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k10414 in k10410 in k10407 in k10404 in k10401 in k10395 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in ... */
static void C_ccall f_10416(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_10416,c,av);}
/* expand.scm:1382: ##sys#warn */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[270]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[270]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
tp(4,av2);}}

/* k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in ... */
static void C_ccall f_10422(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,2)))){
C_save_and_reclaim((void *)f_10422,c,av);}
a=C_alloc(17);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_10425,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
if(C_truep(t1)){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
f_10425(2,av2);}}
else{
t3=C_eqp(C_SCHEME_TRUE,C_u_i_car(((C_word*)t0)[2]));
if(C_truep(t3)){
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
f_10425(2,av2);}}
else{
t4=C_i_numberp(C_u_i_car(((C_word*)t0)[2]));
if(C_truep(t4)){
t5=t2;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
f_10425(2,av2);}}
else{
t5=C_charp(C_u_i_car(((C_word*)t0)[2]));
if(C_truep(t5)){
t6=t2;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
f_10425(2,av2);}}
else{
t6=C_i_stringp(C_u_i_car(((C_word*)t0)[2]));
if(C_truep(t6)){
t7=t2;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
f_10425(2,av2);}}
else{
t7=C_eofp(C_u_i_car(((C_word*)t0)[2]));
if(C_truep(t7)){
t8=t2;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t7;
f_10425(2,av2);}}
else{
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10671,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[10],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1394: chicken.blob#blob? */
t9=*((C_word*)lf[285]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t9;
av2[1]=t8;
av2[2]=C_u_i_car(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}}}}}}}

/* k10423 in k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in ... */
static void C_ccall f_10425(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_10425,c,av);}
a=C_alloc(12);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10428,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10488,a[2]=((C_word*)t0)[7],a[3]=t2,a[4]=((C_word*)t0)[8],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1399: chicken.syntax#strip-syntax */
t4=*((C_word*)lf[12]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_u_i_car(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
if(C_truep(C_i_nullp(C_u_i_cdr(((C_word*)t0)[2])))){
t2=C_u_i_car(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10505,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[9],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1409: expand */
t4=((C_word*)((C_word*)t0)[7])[1];
f_10370(t4,t3,((C_word*)t0)[8],C_SCHEME_FALSE);}
else{
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_10511,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[10],a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[6],tmp=(C_word)a,a+=9,tmp);
t3=C_i_length(((C_word*)t0)[2]);
t4=C_eqp(t3,C_fix(3));
if(C_truep(t4)){
/* expand.scm:1411: c */
t5=((C_word*)t0)[4];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=C_i_cadr(((C_word*)t0)[2]);
((C_proc)C_fast_retrieve_proc(t5))(4,av2);}}
else{
t5=t2;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
f_10511(2,av2);}}}}}

/* k10426 in k10423 in k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_ccall f_10428(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_10428,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10434,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=C_i_length(((C_word*)t0)[2]);
t4=C_eqp(t3,C_fix(3));
if(C_truep(t4)){
/* expand.scm:1401: c */
t5=((C_word*)t0)[4];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=C_i_cadr(((C_word*)t0)[2]);
((C_proc)C_fast_retrieve_proc(t5))(4,av2);}}
else{
t5=t2;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
f_10434(2,av2);}}}

/* k10432 in k10426 in k10423 in k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in ... */
static void C_ccall f_10434(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_10434,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=C_i_caddr(((C_word*)t0)[2]);
t3=C_u_i_car(((C_word*)t0)[2]);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,2,t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t2=C_i_cdr(((C_word*)t0)[2]);
if(C_truep(C_i_pairp(t2))){
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_cons(&a,2,lf[108],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10460,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1405: c */
t4=((C_word*)t0)[4];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
av2[3]=C_u_i_car(((C_word*)t0)[2]);
((C_proc)C_fast_retrieve_proc(t4))(4,av2);}}}}

/* k10458 in k10432 in k10426 in k10423 in k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in ... */
static void C_ccall f_10460(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_10460,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(t1)?C_a_i_list(&a,1,lf[272]):C_u_i_car(((C_word*)t0)[3]));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k10486 in k10423 in k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_ccall f_10488(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_10488,c,av);}
/* expand.scm:1399: expand */
t2=((C_word*)((C_word*)t0)[2])[1];
f_10370(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k10503 in k10423 in k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_ccall f_10505(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_10505,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,((C_word*)t0)[3],((C_word*)t0)[4],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k10509 in k10423 in k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_ccall f_10511(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_10511,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10514,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1412: r */
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[276];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10551,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t3=C_eqp(C_u_i_length(((C_word*)t0)[2]),C_fix(4));
if(C_truep(t3)){
/* expand.scm:1418: c */
t4=((C_word*)t0)[7];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=((C_word*)t0)[8];
av2[3]=C_i_caddr(((C_word*)t0)[2]);
((C_proc)C_fast_retrieve_proc(t4))(4,av2);}}
else{
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
f_10551(2,av2);}}}}

/* k10512 in k10509 in k10423 in k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in ... */
static void C_ccall f_10514(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,3)))){
C_save_and_reclaim((void *)f_10514,c,av);}
a=C_alloc(21);
t2=C_i_car(((C_word*)t0)[2]);
t3=C_a_i_list(&a,2,t1,t2);
t4=C_a_i_list(&a,1,t3);
t5=C_i_caddr(((C_word*)t0)[2]);
t6=C_a_i_list(&a,2,t5,t1);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10533,a[2]=t1,a[3]=t6,a[4]=((C_word*)t0)[3],a[5]=t4,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1416: expand */
t8=((C_word*)((C_word*)t0)[4])[1];
f_10370(t8,t7,((C_word*)t0)[5],C_SCHEME_FALSE);}

/* k10531 in k10512 in k10509 in k10423 in k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in ... */
static void C_ccall f_10533(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,1)))){
C_save_and_reclaim((void *)f_10533,c,av);}
a=C_alloc(21);
t2=C_a_i_list(&a,4,lf[261],((C_word*)t0)[2],((C_word*)t0)[3],t1);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,3,lf[55],((C_word*)t0)[5],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k10549 in k10509 in k10423 in k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in ... */
static void C_ccall f_10551(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_10551,c,av);}
a=C_alloc(8);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10554,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1419: r */
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[276];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}
else{
t2=C_i_car(((C_word*)t0)[2]);
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_a_i_cons(&a,2,lf[108],t3);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10608,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1429: expand */
t6=((C_word*)((C_word*)t0)[4])[1];
f_10370(t6,t5,((C_word*)t0)[5],C_SCHEME_FALSE);}}

/* k10552 in k10549 in k10509 in k10423 in k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in ... */
static void C_ccall f_10554(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(34,c,3)))){
C_save_and_reclaim((void *)f_10554,c,av);}
a=C_alloc(34);
t2=C_i_car(((C_word*)t0)[2]);
t3=C_a_i_list(&a,3,lf[76],C_SCHEME_END_OF_LIST,t2);
t4=C_i_cadr(((C_word*)t0)[2]);
t5=C_a_i_list(&a,3,lf[222],t4,t1);
t6=C_i_cadddr(((C_word*)t0)[2]);
t7=C_a_i_list(&a,3,lf[222],t6,t1);
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10581,a[2]=t5,a[3]=t7,a[4]=t1,a[5]=((C_word*)t0)[3],a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* expand.scm:1426: expand */
t9=((C_word*)((C_word*)t0)[4])[1];
f_10370(t9,t8,((C_word*)t0)[5],C_SCHEME_FALSE);}

/* k10579 in k10552 in k10549 in k10509 in k10423 in k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in ... */
static void C_ccall f_10581(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(30,c,1)))){
C_save_and_reclaim((void *)f_10581,c,av);}
a=C_alloc(30);
t2=C_a_i_list(&a,4,lf[283],((C_word*)t0)[2],((C_word*)t0)[3],t1);
t3=C_a_i_list(&a,3,lf[76],((C_word*)t0)[4],t2);
t4=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,3,lf[95],((C_word*)t0)[6],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k10606 in k10549 in k10509 in k10423 in k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in ... */
static void C_ccall f_10608(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,1)))){
C_save_and_reclaim((void *)f_10608,c,av);}
a=C_alloc(12);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,4,lf[261],((C_word*)t0)[3],((C_word*)t0)[4],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k10669 in k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in ... */
static void C_ccall f_10671(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_10671,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
f_10425(2,av2);}}
else{
t2=C_i_vectorp(C_u_i_car(((C_word*)t0)[3]));
if(C_truep(t2)){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_10425(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10683,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1396: ##sys#srfi-4-vector? */
t4=*((C_word*)lf[284]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_u_i_car(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}}

/* k10681 in k10669 in k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_ccall f_10683(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_10683,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
f_10425(2,av2);}}
else{
if(C_truep(C_i_pairp(C_u_i_car(((C_word*)t0)[3])))){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10699,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1398: r */
t3=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[219];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_10425(2,av2);}}}}

/* k10697 in k10681 in k10669 in k10420 in k10382 in expand in k10363 in k10360 in k10357 in a10351 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in ... */
static void C_ccall f_10699(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_10699,c,av);}
/* expand.scm:1398: c */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=C_i_caar(((C_word*)t0)[4]);
((C_proc)C_fast_retrieve_proc(t2))(4,av2);}}

/* k10731 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in ... */
static void C_ccall f_10733(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10733,c,av);}
/* expand.scm:1350: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[228];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a10734 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in ... */
static void C_ccall f_10735(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_10735,c,av);}
a=C_alloc(6);
t5=C_i_cdr(t2);
if(C_truep(C_i_nullp(t5))){
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_i_cdr(t5);
t7=C_u_i_car(t5);
if(C_truep(C_i_nullp(t6))){
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10758,a[2]=t7,a[3]=t6,a[4]=t1,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1362: r */
t9=t3;{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
av2[2]=lf[276];
((C_proc)C_fast_retrieve_proc(t9))(3,av2);}}}}

/* k10756 in a10734 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in ... */
static void C_ccall f_10758(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_10758,c,av);}
a=C_alloc(15);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,1,t2);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10777,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1364: r */
t5=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[228];
((C_proc)C_fast_retrieve_proc(t5))(3,av2);}}

/* k10775 in k10756 in a10734 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in ... */
static void C_ccall f_10777(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,1)))){
C_save_and_reclaim((void *)f_10777,c,av);}
a=C_alloc(24);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,4,lf[261],((C_word*)t0)[3],((C_word*)t0)[3],t2);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,3,lf[55],((C_word*)t0)[5],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k10783 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in ... */
static void C_ccall f_10785(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10785,c,av);}
/* expand.scm:1336: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[237];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a10786 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in ... */
static void C_ccall f_10787(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_10787,c,av);}
a=C_alloc(5);
t5=C_i_cdr(t2);
if(C_truep(C_i_nullp(t5))){
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_i_cdr(t5);
t7=C_u_i_car(t5);
if(C_truep(C_i_nullp(t6))){
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10818,a[2]=t6,a[3]=t1,a[4]=t7,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1348: r */
t9=t3;{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
av2[2]=lf[237];
((C_proc)C_fast_retrieve_proc(t9))(3,av2);}}}}

/* k10816 in a10786 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in ... */
static void C_ccall f_10818(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,1)))){
C_save_and_reclaim((void *)f_10818,c,av);}
a=C_alloc(15);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,4,lf[261],((C_word*)t0)[4],t2,C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k10820 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in ... */
static void C_ccall f_10822(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10822,c,av);}
/* expand.scm:1324: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[287];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a10823 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in ... */
static void C_ccall f_10824(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_10824,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10828,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1329: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[287];
av2[3]=t2;
av2[4]=lf[289];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k10826 in a10823 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in ... */
static void C_ccall f_10828(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_10828,c,av);}
a=C_alloc(13);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_i_caddr(((C_word*)t0)[2]);
if(C_truep(C_i_pairp(t2))){
t4=C_u_i_car(t2);
t5=C_a_i_list(&a,2,lf[288],t4);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10851,a[2]=((C_word*)t0)[3],a[3]=t5,tmp=(C_word)a,a+=4,tmp);
t7=C_u_i_cdr(t2);
t8=C_a_i_list(&a,1,t3);
/* expand.scm:1333: ##sys#append */
t9=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t9;
av2[1]=t6;
av2[2]=t7;
av2[3]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(4,av2);}}
else{
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,3,lf[97],t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k10849 in k10826 in a10823 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in ... */
static void C_ccall f_10851(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_10851,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k10864 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in ... */
static void C_ccall f_10866(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10866,c,av);}
/* expand.scm:1315: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[290];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a10867 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in ... */
static void C_ccall f_10868(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_10868,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10872,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1320: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[290];
av2[3]=t2;
av2[4]=lf[293];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k10870 in a10867 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in ... */
static void C_ccall f_10872(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_10872,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10875,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1321: chicken.internal#check-for-multiple-bindings */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[291]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[291]+1);
av2[1]=t2;
av2[2]=C_i_cadr(((C_word*)t0)[2]);
av2[3]=((C_word*)t0)[2];
av2[4]=lf[292];
tp(5,av2);}}

/* k10873 in k10870 in a10867 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in ... */
static void C_ccall f_10875(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_10875,c,av);}
a=C_alloc(3);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[112],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k10886 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in ... */
static void C_ccall f_10888(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10888,c,av);}
/* expand.scm:1306: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[294];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a10889 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in ... */
static void C_ccall f_10890(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_10890,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10894,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1311: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[294];
av2[3]=t2;
av2[4]=lf[297];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k10892 in a10889 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in ... */
static void C_ccall f_10894(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_10894,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10897,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1312: chicken.internal#check-for-multiple-bindings */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[291]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[291]+1);
av2[1]=t2;
av2[2]=C_i_cadr(((C_word*)t0)[2]);
av2[3]=((C_word*)t0)[2];
av2[4]=lf[296];
tp(5,av2);}}

/* k10895 in k10892 in a10889 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in ... */
static void C_ccall f_10897(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_10897,c,av);}
a=C_alloc(3);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[295],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k10908 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in ... */
static void C_ccall f_10910(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10910,c,av);}
/* expand.scm:1297: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[298];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a10911 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in ... */
static void C_ccall f_10912(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_10912,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10916,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1302: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[298];
av2[3]=t2;
av2[4]=lf[301];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k10914 in a10911 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in ... */
static void C_ccall f_10916(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_10916,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10919,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1303: chicken.internal#check-for-multiple-bindings */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[291]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[291]+1);
av2[1]=t2;
av2[2]=C_i_cadr(((C_word*)t0)[2]);
av2[3]=((C_word*)t0)[2];
av2[4]=lf[300];
tp(5,av2);}}

/* k10917 in k10914 in a10911 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in ... */
static void C_ccall f_10919(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_10919,c,av);}
a=C_alloc(3);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[299],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k10930 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in ... */
static void C_ccall f_10932(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10932,c,av);}
/* expand.scm:1284: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[60];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a10933 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in ... */
static void C_ccall f_10934(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_10934,c,av);}
a=C_alloc(8);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10938,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t6=C_i_cdr(t2);
t7=C_i_pairp(t6);
t8=(C_truep(t7)?C_i_symbolp(C_i_cadr(t2)):C_SCHEME_FALSE);
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10949,a[2]=t5,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1290: ##sys#check-syntax */
t10=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t10;
av2[1]=t9;
av2[2]=lf[60];
av2[3]=t2;
av2[4]=lf[303];
((C_proc)(void*)(*((C_word*)t10+1)))(5,av2);}}
else{
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10959,a[2]=t5,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1293: ##sys#check-syntax */
t10=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t10;
av2[1]=t9;
av2[2]=lf[60];
av2[3]=t2;
av2[4]=lf[305];
((C_proc)(void*)(*((C_word*)t10+1)))(5,av2);}}}

/* k10936 in a10933 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in ... */
static void C_ccall f_10938(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_10938,c,av);}
a=C_alloc(3);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[55],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k10947 in a10933 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in ... */
static void C_ccall f_10949(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10949,c,av);}
/* expand.scm:1291: chicken.internal#check-for-multiple-bindings */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[291]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[291]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=C_i_caddr(((C_word*)t0)[3]);
av2[3]=((C_word*)t0)[3];
av2[4]=lf[302];
tp(5,av2);}}

/* k10957 in a10933 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in ... */
static void C_ccall f_10959(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10959,c,av);}
/* expand.scm:1294: chicken.internal#check-for-multiple-bindings */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[291]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[291]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=C_i_cadr(((C_word*)t0)[3]);
av2[3]=((C_word*)t0)[3];
av2[4]=lf[304];
tp(5,av2);}}

/* k10982 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in ... */
static void C_ccall f_10984(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10984,c,av);}
/* expand.scm:1270: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[105];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a10985 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in ... */
static void C_ccall f_10986(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_10986,c,av);}
a=C_alloc(6);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10990,a[2]=t2,a[3]=t1,a[4]=t4,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1275: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[105];
av2[3]=t2;
av2[4]=lf[309];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k10988 in a10985 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in ... */
static void C_ccall f_10990(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_10990,c,av);}
a=C_alloc(12);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_i_caddr(((C_word*)t0)[2]);
t4=C_i_getprop(t2,lf[7],C_SCHEME_FALSE);
t5=(C_truep(t4)?t4:t2);
t6=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11007,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=t3,a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],tmp=(C_word)a,a+=8,tmp);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11027,a[2]=t6,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1279: ##sys#current-module */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[308]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[308]+1);
av2[1]=t7;
tp(2,av2);}}

/* k11005 in k10988 in a10985 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in ... */
static void C_ccall f_11007(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,2)))){
C_save_and_reclaim((void *)f_11007,c,av);}
a=C_alloc(17);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11010,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11016,a[2]=t2,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],tmp=(C_word)a,a+=7,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11023,a[2]=((C_word*)t0)[6],a[3]=t3,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1280: r */
t5=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[105];
((C_proc)C_fast_retrieve_proc(t5))(3,av2);}}

/* k11008 in k11005 in k10988 in a10985 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in ... */
static void C_ccall f_11010(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_11010,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[306],((C_word*)t0)[3],((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k11014 in k11005 in k10988 in a10985 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in ... */
static void C_ccall f_11016(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_11016,c,av);}
a=C_alloc(9);
if(C_truep(t1)){
/* expand.scm:443: ##sys#syntax-error-hook */
t2=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[113];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[306],((C_word*)t0)[5],((C_word*)t0)[6]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k11021 in k11005 in k10988 in a10985 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in ... */
static void C_ccall f_11023(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11023,c,av);}
/* expand.scm:1280: c */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=((C_word*)t0)[4];
((C_proc)C_fast_retrieve_proc(t2))(4,av2);}}

/* k11025 in k10988 in a10985 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in ... */
static void C_ccall f_11027(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11027,c,av);}
/* expand.scm:1279: ##sys#register-export */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[307]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[307]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
tp(4,av2);}}

/* k11029 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in ... */
static void C_ccall f_11031(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11031,c,av);}
/* expand.scm:1242: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[104];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11032 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in ... */
static void C_ccall f_11033(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_11033,c,av);}
a=C_alloc(6);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11037,a[2]=t2,a[3]=t4,a[4]=t3,a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1247: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[104];
av2[3]=t2;
av2[4]=lf[315];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11035 in a11032 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in ... */
static void C_ccall f_11037(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_11037,c,av);}
a=C_alloc(9);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11042,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t3,a[6]=((C_word)li154),tmp=(C_word)a,a+=7,tmp));
t5=((C_word*)t3)[1];
f_11042(t5,((C_word*)t0)[5],((C_word*)t0)[2]);}

/* loop in k11035 in a11032 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in ... */
static void C_fcall f_11042(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,4)))){
C_save_and_reclaim_args((void *)trf_11042,3,t0,t1,t2);}
a=C_alloc(8);
t3=C_i_cadr(t2);
t4=C_u_i_cdr(t2);
t5=C_u_i_cdr(t4);
t6=C_i_pairp(t3);
if(C_truep(C_i_not(t6))){
t7=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11057,a[2]=t3,a[3]=t5,a[4]=t1,a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[4],tmp=(C_word)a,a+=8,tmp);
/* expand.scm:1252: ##sys#check-syntax */
t8=*((C_word*)lf[59]+1);{
C_word av2[5];
av2[0]=t8;
av2[1]=t7;
av2[2]=lf[104];
av2[3]=t2;
av2[4]=lf[312];
((C_proc)(void*)(*((C_word*)t8+1)))(5,av2);}}
else{
t7=C_i_car(t3);
if(C_truep(C_i_pairp(t7))){
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11113,a[2]=((C_word*)t0)[5],a[3]=t1,a[4]=t3,a[5]=t5,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1263: ##sys#check-syntax */
t9=*((C_word*)lf[59]+1);{
C_word av2[5];
av2[0]=t9;
av2[1]=t8;
av2[2]=lf[104];
av2[3]=t2;
av2[4]=lf[313];
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}
else{
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11123,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=t5,a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* expand.scm:1266: ##sys#check-syntax */
t9=*((C_word*)lf[59]+1);{
C_word av2[5];
av2[0]=t9;
av2[1]=t8;
av2[2]=lf[104];
av2[3]=t2;
av2[4]=lf[314];
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}}}

/* k11055 in loop in k11035 in a11032 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in ... */
static void C_ccall f_11057(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_11057,c,av);}
a=C_alloc(12);
t2=C_i_getprop(((C_word*)t0)[2],lf[7],C_SCHEME_FALSE);
t3=(C_truep(t2)?t2:((C_word*)t0)[2]);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11068,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11104,a[2]=t4,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1254: ##sys#current-module */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[308]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[308]+1);
av2[1]=t5;
tp(2,av2);}}

/* k11066 in k11055 in loop in k11035 in a11032 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in ... */
static void C_ccall f_11068(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_11068,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11071,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11093,a[2]=t2,a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11100,a[2]=((C_word*)t0)[6],a[3]=t3,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1255: r */
t5=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[104];
((C_proc)C_fast_retrieve_proc(t5))(3,av2);}}

/* k11069 in k11066 in k11055 in loop in k11035 in a11032 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in ... */
static void C_ccall f_11071(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,1)))){
C_save_and_reclaim((void *)f_11071,c,av);}
a=C_alloc(24);
t2=C_a_i_list(&a,2,lf[310],((C_word*)t0)[2]);
if(C_truep(C_i_pairp(((C_word*)t0)[3]))){
t3=C_u_i_car(((C_word*)t0)[3]);
t4=C_a_i_list(&a,3,lf[97],((C_word*)t0)[2],t3);
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_list(&a,3,lf[108],t2,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t3=C_a_i_list(&a,3,lf[97],((C_word*)t0)[2],lf[311]);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,3,lf[108],t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k11091 in k11066 in k11055 in loop in k11035 in a11032 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in ... */
static void C_ccall f_11093(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11093,c,av);}
if(C_truep(t1)){
/* expand.scm:443: ##sys#syntax-error-hook */
t2=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[113];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_11071(2,av2);}}}

/* k11098 in k11066 in k11055 in loop in k11035 in a11032 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in ... */
static void C_ccall f_11100(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11100,c,av);}
/* expand.scm:1255: c */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=((C_word*)t0)[4];
((C_proc)C_fast_retrieve_proc(t2))(4,av2);}}

/* k11102 in k11055 in loop in k11035 in a11032 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in ... */
static void C_ccall f_11104(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11104,c,av);}
/* expand.scm:1254: ##sys#register-export */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[307]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[307]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
tp(4,av2);}}

/* k11111 in loop in k11035 in a11032 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in ... */
static void C_ccall f_11113(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_11113,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11120,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1264: chicken.syntax#expand-curried-define */
t3=*((C_word*)lf[116]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
av2[4]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k11118 in k11111 in loop in k11035 in a11032 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in ... */
static void C_ccall f_11120(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11120,c,av);}
/* expand.scm:1264: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_11042(t2,((C_word*)t0)[3],t1);}

/* k11121 in loop in k11035 in a11032 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in ... */
static void C_ccall f_11123(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_11123,c,av);}
a=C_alloc(15);
t2=C_i_car(((C_word*)t0)[2]);
t3=C_u_i_car(((C_word*)t0)[3]);
t4=C_u_i_cdr(((C_word*)t0)[3]);
t5=C_a_i_cons(&a,2,t4,((C_word*)t0)[4]);
t6=C_a_i_cons(&a,2,lf[76],t5);
t7=C_a_i_list3(&a,3,t2,t3,t6);
/* expand.scm:1267: loop */
t8=((C_word*)((C_word*)t0)[5])[1];
f_11042(t8,((C_word*)t0)[6],t7);}

/* k11156 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in ... */
static void C_ccall f_11158(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11158,c,av);}
/* expand.scm:1233: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[316];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11159 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in ... */
static void C_ccall f_11160(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_11160,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11164,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1238: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[316];
av2[3]=t2;
av2[4]=lf[317];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11162 in a11159 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in ... */
static void C_ccall f_11164(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_11164,c,av);}
a=C_alloc(3);
t2=C_i_cdr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[108],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k11173 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in ... */
static void C_ccall f_11175(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11175,c,av);}
/* expand.scm:1225: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[283];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11176 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in ... */
static void C_ccall f_11177(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_11177,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11181,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1230: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[283];
av2[3]=t2;
av2[4]=lf[318];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11179 in a11176 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in ... */
static void C_ccall f_11181(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_11181,c,av);}
a=C_alloc(3);
t2=C_i_cdr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[261],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k11190 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 in ... */
static void C_ccall f_11192(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11192,c,av);}
/* expand.scm:1217: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[219];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11193 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 in ... */
static void C_ccall f_11194(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_11194,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11198,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1222: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[219];
av2[3]=t2;
av2[4]=lf[319];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11196 in a11193 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in ... */
static void C_ccall f_11198(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_11198,c,av);}
a=C_alloc(6);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,2,lf[75],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k11207 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11209(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11209,c,av);}
/* expand.scm:1209: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[230];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11210 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11211(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_11211,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11215,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1214: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[230];
av2[3]=t2;
av2[4]=lf[320];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11213 in a11210 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 in ... */
static void C_ccall f_11215(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_11215,c,av);}
a=C_alloc(3);
t2=C_i_cdr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[76],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k11224 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11226(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11226,c,av);}
/* expand.scm:1195: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[321];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11227 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11228(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_11228,c,av);}
a=C_alloc(3);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11232,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1199: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[321];
av2[3]=t2;
av2[4]=lf[323];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11230 in a11227 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11232(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11232,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11235,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1200: ##sys#current-module */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[308]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[308]+1);
av2[1]=t2;
tp(2,av2);}}

/* k11233 in k11230 in a11227 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11235(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11235,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11245,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1201: ##sys#module-name */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[322]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[322]+1);
av2[1]=t2;
av2[2]=t1;
tp(3,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k11243 in k11233 in k11230 in a11227 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11245(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_11245,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,2,lf[75],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k11247 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11249(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11249,c,av);}
/* expand.scm:1172: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[324];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11250 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11251(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_11251,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11255,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1176: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[324];
av2[3]=t2;
av2[4]=lf[334];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11253 in a11250 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11255(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_11255,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11258,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1177: chicken.syntax#strip-syntax */
t3=*((C_word*)lf[12]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_cadr(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11256 in k11253 in a11250 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11258(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_11258,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11261,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t3=C_eqp(lf[328],t1);
if(C_truep(t3)){
/* expand.scm:1179: syntax-error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[331]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[331]+1);
av2[1]=t2;
av2[2]=lf[324];
av2[3]=lf[333];
tp(4,av2);}}
else{
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_11261(2,av2);}}}

/* k11259 in k11256 in k11253 in a11250 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11261(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,2)))){
C_save_and_reclaim((void *)f_11261,c,av);}
a=C_alloc(18);
t2=C_a_i_list(&a,2,lf[75],((C_word*)t0)[2]);
t3=C_a_i_list(&a,2,lf[75],lf[325]);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11284,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1186: chicken.syntax#strip-syntax */
t5=*((C_word*)lf[12]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=C_i_caddr(((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k11282 in k11259 in k11256 in k11253 in a11250 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11284(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(35,c,4)))){
C_save_and_reclaim((void *)f_11284,c,av);}
a=C_alloc(35);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11287,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_eqp(lf[328],t1);
if(C_truep(t3)){
t4=C_a_i_list(&a,2,lf[75],lf[328]);
t5=C_a_i_list(&a,4,lf[326],((C_word*)t0)[2],((C_word*)t0)[3],t4);
t6=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_list(&a,2,lf[327],t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
if(C_truep(C_i_symbolp(t1))){
t4=C_a_i_list(&a,2,lf[329],t1);
t5=C_a_i_list(&a,2,lf[75],t4);
t6=C_a_i_list(&a,4,lf[326],((C_word*)t0)[2],((C_word*)t0)[3],t5);
t7=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_a_i_list(&a,2,lf[327],t6);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
if(C_truep(C_i_listp(t1))){
/* expand.scm:1190: ##sys#validate-exports */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[330]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[330]+1);
av2[1]=t2;
av2[2]=t1;
av2[3]=lf[324];
tp(4,av2);}}
else{
/* expand.scm:1192: syntax-error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[331]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[331]+1);
av2[1]=t2;
av2[2]=lf[324];
av2[3]=lf[332];
av2[4]=C_i_caddr(((C_word*)t0)[5]);
tp(5,av2);}}}}}

/* k11285 in k11282 in k11259 in k11256 in k11253 in a11250 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 in ... */
static void C_ccall f_11287(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,1)))){
C_save_and_reclaim((void *)f_11287,c,av);}
a=C_alloc(24);
t2=C_a_i_list(&a,2,lf[75],t1);
t3=C_a_i_list(&a,4,lf[326],((C_word*)t0)[2],((C_word*)t0)[3],t2);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,2,lf[327],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k11331 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11333(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11333,c,av);}
/* expand.scm:1137: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[335];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11334 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11335(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_11335,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11339,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1141: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[335];
av2[3]=t2;
av2[4]=lf[343];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11337 in a11334 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11339(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11339,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11342,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1142: chicken.syntax#strip-syntax */
t3=*((C_word*)lf[12]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11340 in k11337 in a11334 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11342(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_11342,c,av);}
a=C_alloc(7);
t2=C_i_cadr(t1);
t3=C_i_car(t2);
t4=C_u_i_cdr(t2);
t5=C_i_caddr(t1);
t6=C_u_i_cdr(t1);
t7=C_u_i_cdr(t6);
t8=C_u_i_cdr(t7);
t9=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11500,a[2]=t3,a[3]=t4,a[4]=t8,a[5]=((C_word*)t0)[2],a[6]=t5,tmp=(C_word)a,a+=7,tmp);
/* expand.scm:1150: chicken.internal#library-id */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[342]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[342]+1);
av2[1]=t9;
av2[2]=t3;
tp(3,av2);}}

/* k11363 in k11391 in k11460 in k11498 in k11340 in k11337 in a11334 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 in ... */
static void C_ccall f_11365(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(30,c,1)))){
C_save_and_reclaim((void *)f_11365,c,av);}
a=C_alloc(30);
t2=C_a_i_list(&a,3,lf[107],lf[339],lf[340]);
t3=C_a_i_list(&a,2,lf[341],((C_word*)t0)[2]);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,5,lf[109],t1,C_SCHEME_TRUE,t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k11391 in k11460 in k11498 in k11340 in k11337 in a11334 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11393(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(31,c,2)))){
C_save_and_reclaim((void *)f_11393,c,av);}
a=C_alloc(31);
t2=C_a_i_list(&a,2,lf[75],t1);
t3=C_a_i_list(&a,2,lf[75],((C_word*)t0)[2]);
t4=C_a_i_list(&a,5,lf[338],((C_word*)t0)[3],((C_word*)t0)[4],t2,t3);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11365,a[2]=t4,a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1165: chicken.internal#library-id */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[342]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[342]+1);
av2[1]=t5;
av2[2]=((C_word*)t0)[6];
tp(3,av2);}}

/* g2012 in k11498 in k11340 in k11337 in a11334 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_11398(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_11398,3,t0,t1,t2);}
a=C_alloc(6);
t3=C_i_car(t2);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11405,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1154: ##sys#validate-exports */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[330]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[330]+1);
av2[1]=t4;
av2[2]=C_i_cadr(t2);
av2[3]=lf[335];
tp(4,av2);}}

/* k11403 in g2012 in k11498 in k11340 in k11337 in a11334 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11405(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_11405,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11408,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t3=C_i_symbolp(((C_word*)t0)[3]);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11417,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],tmp=(C_word)a,a+=8,tmp);
if(C_truep(t3)){
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t3;
f_11417(2,av2);}}
else{
if(C_truep(C_i_listp(((C_word*)t0)[3]))){
t5=C_eqp(C_fix(2),C_u_i_length(((C_word*)t0)[3]));
if(C_truep(t5)){
t6=C_i_car(((C_word*)t0)[3]);
if(C_truep(C_i_symbolp(t6))){
/* expand.scm:1159: chicken.internal#valid-library-specifier? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[337]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[337]+1);
av2[1]=t4;
av2[2]=C_i_cadr(((C_word*)t0)[3]);
tp(3,av2);}}
else{
t7=t4;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_SCHEME_FALSE;
f_11417(2,av2);}}}
else{
t6=t4;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_FALSE;
f_11417(2,av2);}}}
else{
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
f_11417(2,av2);}}}}

/* k11406 in k11403 in g2012 in k11498 in k11340 in k11337 in a11334 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 in ... */
static void C_ccall f_11408(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_11408,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k11415 in k11403 in g2012 in k11498 in k11340 in k11337 in a11334 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 in ... */
static void C_ccall f_11417(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_11417,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* expand.scm:1160: ##sys#syntax-error-hook */
t2=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[5];
av2[2]=lf[336];
av2[3]=((C_word*)t0)[6];
av2[4]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}

/* k11460 in k11498 in k11340 in k11337 in a11334 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11462(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_11462,c,av);}
a=C_alloc(13);
t2=C_a_i_list(&a,2,lf[75],t1);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11393,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* expand.scm:1163: ##sys#validate-exports */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[330]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[330]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
av2[3]=lf[335];
tp(4,av2);}}

/* map-loop2006 in k11498 in k11340 in k11337 in a11334 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_11464(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_11464,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11489,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1152: g2012 */
t4=((C_word*)t0)[4];
f_11398(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11487 in map-loop2006 in k11498 in k11340 in k11337 in a11334 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11489(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11489,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_11464(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k11498 in k11340 in k11337 in a11334 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11500(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(31,c,3)))){
C_save_and_reclaim((void *)f_11500,c,av);}
a=C_alloc(31);
t2=C_a_i_list(&a,2,lf[75],t1);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11398,a[2]=((C_word*)t0)[2],a[3]=((C_word)li162),tmp=(C_word)a,a+=4,tmp);
t8=C_i_check_list_2(((C_word*)t0)[3],lf[17]);
t9=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11462,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11464,a[2]=t5,a[3]=t11,a[4]=t7,a[5]=t6,a[6]=((C_word)li163),tmp=(C_word)a,a+=7,tmp));
t13=((C_word*)t11)[1];
f_11464(t13,t9,((C_word*)t0)[3]);}

/* k11502 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11504(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11504,c,av);}
/* expand.scm:1128: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[344];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11505 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11506(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,9)))){
C_save_and_reclaim((void *)f_11506,c,av);}
t5=*((C_word*)lf[345]+1);
/* expand.scm:1131: g1989 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[345]+1));
C_word *av2;
if(c >= 10) {
  av2=av;
} else {
  av2=C_alloc(10);
}
av2[0]=*((C_word*)lf[345]+1);
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
av2[4]=t4;
av2[5]=*((C_word*)lf[4]+1);
av2[6]=*((C_word*)lf[20]+1);
av2[7]=C_SCHEME_FALSE;
av2[8]=C_SCHEME_TRUE;
av2[9]=lf[344];
tp(10,av2);}}

/* k11512 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11514(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11514,c,av);}
/* expand.scm:1118: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[346];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11515 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11516(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_11516,c,av);}
a=C_alloc(6);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11520,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11533,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1122: chicken.syntax#strip-syntax */
t7=*((C_word*)lf[12]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=C_i_cdr(t2);
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* k11518 in a11515 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11520(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_11520,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11523,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1123: ##sys#current-module */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[308]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[308]+1);
av2[1]=t2;
tp(2,av2);}}

/* k11521 in k11518 in a11515 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11523(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_11523,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11526,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(t1)){
/* expand.scm:1125: ##sys#add-to-export-list */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[348]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[348]+1);
av2[1]=t2;
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=lf[347];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11524 in k11521 in k11518 in a11515 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11526(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11526,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[347];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k11531 in a11515 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11533(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11533,c,av);}
/* expand.scm:1122: ##sys#validate-exports */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[330]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[330]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[346];
tp(4,av2);}}

/* k11539 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11541(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11541,c,av);}
/* expand.scm:1058: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[349];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11542 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11543(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_11543,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11547,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1062: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[349];
av2[3]=t2;
av2[4]=lf[356];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11545 in a11542 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11547(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_11547,c,av);}
a=C_alloc(6);
t2=C_i_length(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11553,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1064: chicken.internal#library-id */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[342]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[342]+1);
av2[1]=t3;
av2[2]=C_i_cadr(((C_word*)t0)[2]);
tp(3,av2);}}

/* k11551 in k11545 in a11542 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11553(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_11553,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11559,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_fixnum_greater_or_equal_p(((C_word*)t0)[2],C_fix(4)))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11704,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1071: chicken.syntax#strip-syntax */
t4=*((C_word*)lf[12]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_caddr(((C_word*)t0)[5]);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t2;
f_11559(t3,C_SCHEME_FALSE);}}

/* k11557 in k11551 in k11545 in a11542 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_11559(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_11559,2,t0,t1);}
a=C_alloc(8);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11562,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1072: chicken.syntax#strip-syntax */
t3=*((C_word*)lf[12]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11631,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11690,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1105: chicken.syntax#strip-syntax */
t4=*((C_word*)lf[12]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_caddr(((C_word*)t0)[6]);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k11560 in k11557 in k11551 in k11545 in a11542 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11562(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_11562,c,av);}
a=C_alloc(13);
t2=C_i_cadddr(t1);
if(C_truep(C_fixnum_greaterp(((C_word*)t0)[2],C_fix(4)))){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11574,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11608,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11612,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1091: scheme#symbol->string */
t6=*((C_word*)lf[49]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11615,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1097: ##sys#check-syntax */
t4=*((C_word*)lf[59]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[349];
av2[3]=t1;
av2[4]=lf[355];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}

/* k11572 in k11560 in k11557 in k11551 in k11545 in a11542 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11574(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_11574,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11577,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* expand.scm:1092: r */
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[349];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* k11575 in k11572 in k11560 in k11557 in k11551 in k11545 in a11542 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11577(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,c,1)))){
C_save_and_reclaim((void *)f_11577,c,av);}
a=C_alloc(36);
t2=C_i_cddddr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,lf[328],t2);
t4=C_a_i_cons(&a,2,((C_word*)t0)[3],t3);
t5=C_a_i_cons(&a,2,t1,t4);
t6=C_a_i_list(&a,2,((C_word*)t0)[4],((C_word*)t0)[3]);
t7=C_a_i_list(&a,4,t1,((C_word*)t0)[5],lf[350],t6);
t8=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_a_i_list(&a,3,lf[108],t5,t7);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}

/* k11606 in k11560 in k11557 in k11551 in k11545 in a11542 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11608(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11608,c,av);}
/* expand.scm:1088: scheme#string->symbol */
t2=*((C_word*)lf[351]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k11610 in k11560 in k11557 in k11551 in k11545 in a11542 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11612(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11612,c,av);}
/* expand.scm:1089: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[352]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[352]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=lf[353];
av2[3]=t1;
tp(4,av2);}}

/* k11613 in k11560 in k11557 in k11551 in k11545 in a11542 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11615(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_11615,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11622,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1101: chicken.internal#library-id */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[342]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[342]+1);
av2[1]=t2;
av2[2]=C_i_car(((C_word*)t0)[4]);
tp(3,av2);}}

/* k11620 in k11613 in k11560 in k11557 in k11551 in k11545 in a11542 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11622(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11622,c,av);}
/* expand.scm:1099: ##sys#instantiate-functor */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[354]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[354]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
av2[4]=C_u_i_cdr(((C_word*)t0)[4]);
tp(5,av2);}}

/* k11629 in k11557 in k11551 in k11545 in a11542 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11631(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_11631,c,av);}
a=C_alloc(6);
t2=C_eqp(lf[328],t1);
t3=(C_truep(t2)?C_SCHEME_TRUE:t1);
t4=C_i_cdddr(((C_word*)t0)[2]);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11656,a[2]=t4,a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_pairp(t4))){
t6=C_i_nullp(C_u_i_cdr(t4));
t7=t5;
f_11656(t7,(C_truep(t6)?C_i_stringp(C_u_i_car(t4)):C_SCHEME_FALSE));}
else{
t6=t5;
f_11656(t6,C_SCHEME_FALSE);}}

/* k11654 in k11629 in k11557 in k11551 in k11545 in a11542 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_11656(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,0,1)))){
C_save_and_reclaim_args((void *)trf_11656,2,t0,t1);}
a=C_alloc(21);
if(C_truep(t1)){
t2=C_i_car(((C_word*)t0)[2]);
t3=C_a_i_list(&a,3,lf[110],t2,*((C_word*)lf[3]+1));
t4=C_a_i_list(&a,1,t3);
t5=C_a_i_cons(&a,2,((C_word*)t0)[3],t4);
t6=C_a_i_cons(&a,2,((C_word*)t0)[4],t5);
t7=((C_word*)t0)[5];{
C_word av2[2];
av2[0]=t7;
av2[1]=C_a_i_cons(&a,2,lf[109],t6);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
t2=C_a_i_cons(&a,2,((C_word*)t0)[3],((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,((C_word*)t0)[4],t2);
t4=((C_word*)t0)[5];{
C_word av2[2];
av2[0]=t4;
av2[1]=C_a_i_cons(&a,2,lf[109],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k11688 in k11557 in k11551 in k11545 in a11542 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11690(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11690,c,av);}
/* expand.scm:1105: ##sys#validate-exports */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[330]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[330]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[349];
tp(4,av2);}}

/* k11702 in k11551 in k11545 in a11542 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11704(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11704,c,av);}
t2=((C_word*)t0)[2];
f_11559(t2,C_eqp(lf[350],t1));}

/* k11714 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11716(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11716,c,av);}
/* expand.scm:1001: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[357];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11717 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11718(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(22,c,3)))){
C_save_and_reclaim((void *)f_11718,c,av);}
a=C_alloc(22);
t5=C_i_cdr(t2);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11724,a[2]=t5,a[3]=((C_word)li168),tmp=(C_word)a,a+=4,tmp));
t11=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11734,a[2]=t7,a[3]=t9,a[4]=((C_word)li169),tmp=(C_word)a,a+=5,tmp));
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11879,a[2]=t5,a[3]=t7,a[4]=t13,a[5]=t9,a[6]=((C_word)li171),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_11879(t15,t1,t5);}

/* err in a11717 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_11724(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,4)))){
C_save_and_reclaim_args((void *)trf_11724,3,t0,t1,t2);}
a=C_alloc(3);
t3=C_a_i_cons(&a,2,lf[357],((C_word*)t0)[2]);
/* expand.scm:1008: ##sys#error */
t4=*((C_word*)lf[31]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t1;
av2[2]=lf[358];
av2[3]=t2;
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* test in a11717 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_11734(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_11734,3,t0,t1,t2);}
a=C_alloc(7);
if(C_truep(C_i_symbolp(t2))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11748,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1012: chicken.syntax#strip-syntax */
t4=*((C_word*)lf[12]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=C_i_pairp(t2);
if(C_truep(C_i_not(t3))){
/* expand.scm:1013: err */
t4=((C_word*)((C_word*)t0)[2])[1];
f_11724(t4,t1,t2);}
else{
t4=C_i_car(t2);
t5=C_u_i_cdr(t2);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11764,a[2]=t5,a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[2],a[6]=t2,tmp=(C_word)a,a+=7,tmp);
/* expand.scm:1017: chicken.syntax#strip-syntax */
t7=*((C_word*)lf[12]+1);{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}}}

/* k11746 in test in a11717 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11748(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11748,c,av);}
/* expand.scm:1012: chicken.platform#feature? */
t2=*((C_word*)lf[359]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k11762 in test in a11717 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11764(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_11764,c,av);}
a=C_alloc(5);
t2=C_eqp(t1,lf[237]);
if(C_truep(t2)){
t3=C_eqp(((C_word*)t0)[2],C_SCHEME_END_OF_LIST);
if(C_truep(t3)){
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[2]))){
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11788,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1021: test */
t5=((C_word*)((C_word*)t0)[4])[1];
f_11734(t5,t4,C_u_i_car(((C_word*)t0)[2]));}
else{
/* expand.scm:1023: err */
t4=((C_word*)((C_word*)t0)[5])[1];
f_11724(t4,((C_word*)t0)[3],((C_word*)t0)[6]);}}}
else{
t3=C_eqp(t1,lf[228]);
if(C_truep(t3)){
t4=C_eqp(((C_word*)t0)[2],C_SCHEME_END_OF_LIST);
if(C_truep(C_i_not(t4))){
if(C_truep(C_i_pairp(((C_word*)t0)[2]))){
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11823,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1027: test */
t6=((C_word*)((C_word*)t0)[4])[1];
f_11734(t6,t5,C_u_i_car(((C_word*)t0)[2]));}
else{
/* expand.scm:1029: err */
t5=((C_word*)((C_word*)t0)[5])[1];
f_11724(t5,((C_word*)t0)[3],((C_word*)t0)[6]);}}
else{
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}
else{
t4=C_eqp(t1,lf[360]);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11857,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1030: test */
t6=((C_word*)((C_word*)t0)[4])[1];
f_11734(t6,t5,C_i_cadr(((C_word*)t0)[6]));}
else{
/* expand.scm:1031: err */
t5=((C_word*)((C_word*)t0)[5])[1];
f_11724(t5,((C_word*)t0)[3],((C_word*)t0)[6]);}}}}

/* k11786 in k11762 in test in a11717 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11788(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11788,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,lf[237],t2);
/* expand.scm:1022: test */
t4=((C_word*)((C_word*)t0)[3])[1];
f_11734(t4,((C_word*)t0)[4],t3);}
else{
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k11821 in k11762 in test in a11717 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11823(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11823,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_u_i_cdr(((C_word*)t0)[3]);
t3=C_a_i_cons(&a,2,lf[228],t2);
/* expand.scm:1028: test */
t4=((C_word*)((C_word*)t0)[4])[1];
f_11734(t4,((C_word*)t0)[2],t3);}}

/* k11855 in k11762 in test in a11717 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11857(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11857,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_i_not(t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* expand in a11717 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_11879(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,0,3)))){
C_save_and_reclaim_args((void *)trf_11879,3,t0,t1,t2);}
a=C_alloc(16);
t3=C_eqp(t2,C_SCHEME_END_OF_LIST);
if(C_truep(t3)){
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=C_i_check_list_2(((C_word*)t0)[2],lf[17]);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11904,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11906,a[2]=t6,a[3]=t11,a[4]=t7,a[5]=((C_word)li170),tmp=(C_word)a,a+=6,tmp));
t13=((C_word*)t11)[1];
f_11906(t13,t9,((C_word*)t0)[2]);}
else{
t4=C_i_pairp(t2);
if(C_truep(C_i_not(t4))){
/* expand.scm:1037: err */
t5=((C_word*)((C_word*)t0)[3])[1];
f_11724(t5,t1,t2);}
else{
t5=C_i_car(t2);
t6=C_u_i_cdr(t2);
t7=C_i_pairp(t5);
if(C_truep(C_i_not(t7))){
/* expand.scm:1042: err */
t8=((C_word*)((C_word*)t0)[3])[1];
f_11724(t8,t1,t5);}
else{
t8=C_i_car(t5);
t9=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11997,a[2]=t5,a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=t6,a[6]=((C_word*)t0)[5],a[7]=t8,tmp=(C_word)a,a+=8,tmp);
/* expand.scm:1044: chicken.syntax#strip-syntax */
t10=*((C_word*)lf[12]+1);{
C_word av2[3];
av2[0]=t10;
av2[1]=t9;
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}}}}

/* k11902 in expand in a11717 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11904(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11904,c,av);}{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[31]+1);
av2[3]=lf[361];
av2[4]=t1;
C_apply(5,av2);}}

/* map-loop1895 in expand in a11717 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_11906(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_11906,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11983 in k11995 in expand in a11717 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11985(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11985,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[108],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
/* expand.scm:1050: expand */
t2=((C_word*)((C_word*)t0)[4])[1];
f_11879(t2,((C_word*)t0)[3],((C_word*)t0)[5]);}}

/* k11995 in expand in a11717 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_11997(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_11997,c,av);}
a=C_alloc(6);
t2=C_eqp(t1,lf[234]);
if(C_truep(t2)){
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_eqp(t3,C_SCHEME_END_OF_LIST);
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=(C_truep(t4)?lf[362]:C_a_i_cons(&a,2,lf[108],t3));
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11985,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1049: test */
t4=((C_word*)((C_word*)t0)[6])[1];
f_11734(t4,t3,((C_word*)t0)[7]);}}

/* k12007 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12009(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_12009,c,av);}
/* expand.scm:993: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[363];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a12010 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12011(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_12011,c,av);}
a=C_alloc(9);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12015,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12036,a[2]=t2,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:997: r */
t7=t3;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[107];
((C_proc)C_fast_retrieve_proc(t7))(3,av2);}}

/* k12013 in a12010 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12015(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_12015,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12026,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:998: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[107];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* k12024 in k12013 in a12010 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12026(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_12026,c,av);}
a=C_alloc(9);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,t1,t2);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,2,lf[327],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k12034 in a12010 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12036(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_12036,c,av);}
a=C_alloc(3);
t2=C_i_cdr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,t1,t2);
/* expand.scm:997: ##sys#register-meta-expression */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[364]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[364]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=t3;
tp(3,av2);}}

/* k12042 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12044(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_12044,c,av);}
/* expand.scm:971: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[107];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a12045 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12046(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(22,c,3)))){
C_save_and_reclaim((void *)f_12046,c,av);}
a=C_alloc(22);
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12055,a[2]=t3,a[3]=t4,a[4]=((C_word)li176),tmp=(C_word)a,a+=5,tmp);
t10=C_i_cdr(t2);
t11=C_i_check_list_2(t10,lf[17]);
t12=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12121,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t13=C_SCHEME_UNDEFINED;
t14=(*a=C_VECTOR_TYPE|1,a[1]=t13,tmp=(C_word)a,a+=2,tmp);
t15=C_set_block_item(t14,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_12123,a[2]=t7,a[3]=t14,a[4]=t9,a[5]=t8,a[6]=((C_word)li177),tmp=(C_word)a,a+=7,tmp));
t16=((C_word*)t14)[1];
f_12123(t16,t12,t10);}

/* g1790 in a12045 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_12055(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,8)))){
C_save_and_reclaim_args((void *)trf_12055,3,t0,t1,t2);}
a=C_alloc(9);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12061,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word)li174),tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12067,a[2]=((C_word)li175),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:977: ##sys#call-with-values */{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=t3;
av2[3]=t4;
C_call_with_values(4,av2);}}

/* a12060 in g1790 in a12045 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12061(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_12061,c,av);}
/* expand.scm:977: ##sys#decompose-import */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[365]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[365]+1);
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[4];
av2[5]=lf[107];
tp(6,av2);}}

/* a12066 in g1790 in a12045 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12067(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6=av[6];
C_word t7=av[7];
C_word t8;
C_word t9;
C_word *a;
if(c!=8) C_bad_argc_2(c,8,t0);
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_12067,c,av);}
a=C_alloc(9);
t8=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_12071,a[2]=t3,a[3]=t1,a[4]=t2,a[5]=t4,a[6]=t5,a[7]=t6,a[8]=t7,tmp=(C_word)a,a+=9,tmp);
/* expand.scm:978: ##sys#current-module */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[308]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[308]+1);
av2[1]=t8;
tp(2,av2);}}

/* k12069 in a12066 in g1790 in a12045 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12071(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_12071,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_12074,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
if(C_truep(t1)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12112,a[2]=((C_word*)t0)[4],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:979: ##sys#module-name */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[322]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[322]+1);
av2[1]=t3;
av2[2]=t1;
tp(3,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_12074(2,av2);}}}

/* k12072 in k12069 in a12066 in g1790 in a12045 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12074(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,10)))){
C_save_and_reclaim((void *)f_12074,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12077,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_not(((C_word*)t0)[5]))){
/* expand.scm:983: ##sys#syntax-error-hook */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[107];
av2[3]=lf[369];
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
/* expand.scm:985: ##sys#import */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[370]+1));
C_word *av2;
if(c >= 11) {
  av2=av;
} else {
  av2=C_alloc(11);
}
av2[0]=*((C_word*)lf[370]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)t0)[6];
av2[4]=((C_word*)t0)[7];
av2[5]=((C_word*)t0)[8];
av2[6]=*((C_word*)lf[4]+1);
av2[7]=*((C_word*)lf[20]+1);
av2[8]=C_SCHEME_FALSE;
av2[9]=C_SCHEME_FALSE;
av2[10]=lf[107];
tp(11,av2);}}}

/* k12075 in k12072 in k12069 in a12066 in g1790 in a12045 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12077(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_12077,c,av);}
a=C_alloc(4);
if(C_truep(C_i_not(((C_word*)t0)[2]))){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[366];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12090,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:990: chicken.internal#module-requirement */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[368]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[368]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}}

/* k12088 in k12075 in k12072 in k12069 in a12066 in g1790 in a12045 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12090(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_12090,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[367],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k12110 in k12069 in a12066 in g1790 in a12045 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12112(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_12112,c,av);}
t2=C_eqp(((C_word*)t0)[2],t1);
if(C_truep(t2)){
/* expand.scm:980: ##sys#syntax-error-hook */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[107];
av2[3]=lf[371];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_12074(2,av2);}}}

/* k12119 in a12045 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12121(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_12121,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,lf[108],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop1784 in a12045 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_12123(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_12123,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12148,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:976: g1790 */
t4=((C_word*)t0)[4];
f_12055(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k12146 in map-loop1784 in a12045 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12148(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_12148,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_12123(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k12157 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12159(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_12159,c,av);}
/* expand.scm:963: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[372];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a12160 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12161(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,9)))){
C_save_and_reclaim((void *)f_12161,c,av);}
t5=*((C_word*)lf[345]+1);
/* expand.scm:966: g1774 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[345]+1));
C_word *av2;
if(c >= 10) {
  av2=av;
} else {
  av2=C_alloc(10);
}
av2[0]=*((C_word*)lf[345]+1);
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
av2[4]=t4;
av2[5]=*((C_word*)lf[5]+1);
av2[6]=*((C_word*)lf[195]+1);
av2[7]=C_SCHEME_TRUE;
av2[8]=C_SCHEME_FALSE;
av2[9]=lf[372];
tp(10,av2);}}

/* k12167 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12169(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_12169,c,av);}
/* expand.scm:956: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[373];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a12170 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_12171(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,9)))){
C_save_and_reclaim((void *)f_12171,c,av);}
t5=*((C_word*)lf[345]+1);
/* expand.scm:959: g1760 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[345]+1));
C_word *av2;
if(c >= 10) {
  av2=av;
} else {
  av2=C_alloc(10);
}
av2[0]=*((C_word*)lf[345]+1);
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
av2[4]=t4;
av2[5]=*((C_word*)lf[4]+1);
av2[6]=*((C_word*)lf[20]+1);
av2[7]=C_SCHEME_FALSE;
av2[8]=C_SCHEME_FALSE;
av2[9]=lf[373];
tp(10,av2);}}

/* k3698 */
static void C_ccall f_3700(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3700,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3703,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

/* k3701 in k3698 */
static void C_ccall f_3703(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,3)))){
C_save_and_reclaim((void *)f_3703,c,av);}
a=C_alloc(19);
t2=C_a_i_provide(&a,1,lf[0]);
t3=C_a_i_provide(&a,1,lf[1]);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3707,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* expand.scm:66: scheme#append */
t5=*((C_word*)lf[16]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[374];
av2[3]=*((C_word*)lf[2]+1);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k3705 in k3701 in k3698 */
static void C_ccall f_3707(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3707,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[2]+1 /* (set! ##sys#features ...) */,t1);
t3=C_set_block_item(lf[3] /* ##sys#current-source-filename */,0,C_SCHEME_FALSE);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3712,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* expand.scm:88: chicken.base#make-parameter */
t5=*((C_word*)lf[196]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_3712(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3712,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[4]+1 /* (set! ##sys#current-environment ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3716,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* expand.scm:89: chicken.base#make-parameter */
t4=*((C_word*)lf[196]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_3716(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,4)))){
C_save_and_reclaim((void *)f_3716,c,av);}
a=C_alloc(18);
t2=C_mutate((C_word*)lf[5]+1 /* (set! ##sys#current-meta-environment ...) */,t1);
t3=C_mutate(&lf[6] /* (set! chicken.syntax#lookup ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3718,a[2]=((C_word)li0),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate(&lf[8] /* (set! chicken.syntax#macro-alias ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3735,a[2]=((C_word)li1),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[12]+1 /* (set! chicken.syntax#strip-syntax ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3782,a[2]=((C_word)li4),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate((C_word*)lf[14]+1 /* (set! ##sys#extend-se ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3924,a[2]=((C_word)li8),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate((C_word*)lf[18]+1 /* (set! ##sys#globalize ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4104,a[2]=((C_word)li12),tmp=(C_word)a,a+=3,tmp));
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4188,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* expand.scm:162: chicken.base#make-parameter */
t9=*((C_word*)lf[196]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t9;
av2[1]=t8;
av2[2]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* chicken.syntax#lookup in k3714 in k3710 in k3705 in k3701 in k3698 */
static C_word C_fcall f_3718(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;{}
t3=C_u_i_assq(t1,t2);
if(C_truep(t3)){
return(C_i_cdr(t3));}
else{
t4=C_i_getprop(t1,lf[7],C_SCHEME_FALSE);
return((C_truep(t4)?t4:C_SCHEME_FALSE));}}

/* chicken.syntax#macro-alias in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_3735(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_3735,3,t1,t2,t3);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3742,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:97: chicken.keyword#keyword? */
t5=*((C_word*)lf[11]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k3740 in chicken.syntax#macro-alias in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_3742(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_3742,c,av);}
a=C_alloc(5);
t2=(C_truep(t1)?t1:C_u_i_namespaced_symbolp(((C_word*)t0)[2]));
if(C_truep(t2)){
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3748,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:99: chicken.base#gensym */
t4=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k3746 in k3740 in chicken.syntax#macro-alias in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_3748(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,1)))){
C_save_and_reclaim((void *)f_3748,c,av);}
a=C_alloc(16);
t2=(
/* expand.scm:100: lookup */
  f_3718(((C_word*)t0)[2],((C_word*)t0)[3])
);
t3=(C_truep(t2)?t2:((C_word*)t0)[2]);
t4=C_i_getprop(((C_word*)t0)[2],lf[9],C_SCHEME_FALSE);
if(C_truep(t4)){
t5=C_a_i_putprop(&a,3,t1,lf[7],t3);
t6=C_a_i_putprop(&a,3,t1,lf[9],t4);
t7=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t7;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
t5=C_a_i_putprop(&a,3,t1,lf[7],t3);
t6=C_a_i_putprop(&a,3,t1,lf[9],((C_word*)t0)[2]);
t7=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t7;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* chicken.syntax#strip-syntax in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_3782(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_3782,c,av);}
a=C_alloc(9);
t3=C_SCHEME_END_OF_LIST;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3788,a[2]=t4,a[3]=t6,a[4]=((C_word)li3),tmp=(C_word)a,a+=5,tmp));
t8=((C_word*)t6)[1];
f_3788(t8,t1,t2);}

/* walk in chicken.syntax#strip-syntax in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_3788(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_3788,3,t0,t1,t2);}
a=C_alloc(6);
t3=C_i_assq(t2,((C_word*)((C_word*)t0)[2])[1]);
if(C_truep(t3)){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_i_cdr(t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3804,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:114: chicken.keyword#keyword? */
t5=*((C_word*)lf[11]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}}

/* k3802 in walk in chicken.syntax#strip-syntax in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_3804(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_3804,c,av);}
a=C_alloc(15);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_i_symbolp(((C_word*)t0)[3]))){
t2=C_i_getprop(((C_word*)t0)[3],lf[7],C_SCHEME_FALSE);
t3=C_i_getprop(((C_word*)t0)[3],lf[9],C_SCHEME_FALSE);
if(C_truep(t3)){
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
if(C_truep(C_i_not(t2))){
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_i_pairp(t2);
t5=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t5;
av2[1]=(C_truep(t4)?((C_word*)t0)[3]:t2);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[3]))){
t2=C_a_i_cons(&a,2,C_SCHEME_FALSE,C_SCHEME_FALSE);
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],t2);
t4=C_a_i_cons(&a,2,t3,((C_word*)((C_word*)t0)[4])[1]);
t5=C_mutate(((C_word *)((C_word*)t0)[4])+1,t4);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3866,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:124: walk */
t7=((C_word*)((C_word*)t0)[5])[1];
f_3788(t7,t6,C_u_i_car(((C_word*)t0)[3]));}
else{
if(C_truep(C_i_vectorp(((C_word*)t0)[3]))){
t2=C_block_size(((C_word*)t0)[3]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3884,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[2],tmp=(C_word)a,a+=7,tmp);
/* expand.scm:129: scheme#make-vector */
t4=*((C_word*)lf[13]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}}}}

/* k3857 in k3864 in k3802 in walk in chicken.syntax#strip-syntax in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_3859(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3859,c,av);}
t2=C_i_setslot(((C_word*)t0)[2],C_fix(1),t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k3864 in k3802 in walk in chicken.syntax#strip-syntax in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_3866(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3866,c,av);}
a=C_alloc(4);
t2=C_i_setslot(((C_word*)t0)[2],C_fix(0),t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3859,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:125: walk */
t4=((C_word*)((C_word*)t0)[4])[1];
f_3788(t4,t3,C_u_i_cdr(((C_word*)t0)[5]));}

/* k3882 in k3802 in walk in chicken.syntax#strip-syntax in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_3884(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_3884,c,av);}
a=C_alloc(16);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,t2,((C_word*)((C_word*)t0)[3])[1]);
t4=C_mutate(((C_word *)((C_word*)t0)[3])+1,t3);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3893,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t6,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[2],a[7]=((C_word)li2),tmp=(C_word)a,a+=8,tmp));
t8=((C_word*)t6)[1];
f_3893(t8,((C_word*)t0)[6],C_fix(0));}

/* doloop384 in k3882 in k3802 in walk in chicken.syntax#strip-syntax in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_3893(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_3893,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3914,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:133: walk */
t4=((C_word*)((C_word*)t0)[5])[1];
f_3788(t4,t3,C_slot(((C_word*)t0)[6],t2));}}

/* k3912 in doloop384 in k3882 in k3802 in walk in chicken.syntax#strip-syntax in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_3914(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3914,c,av);}
t2=C_i_setslot(((C_word*)t0)[2],((C_word*)t0)[3],t1);
t3=((C_word*)((C_word*)t0)[4])[1];
f_3893(t3,((C_word*)t0)[5],C_fixnum_plus(((C_word*)t0)[3],C_fix(1)));}

/* ##sys#extend-se in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_3924(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_3924,c,av);}
a=C_alloc(18);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3928,a[2]=t3,a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_rest_nullp(c,4))){
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=C_i_check_list_2(t3,lf[17]);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4067,a[2]=t7,a[3]=t11,a[4]=t8,a[5]=((C_word)li7),tmp=(C_word)a,a+=6,tmp));
t13=((C_word*)t11)[1];
f_4067(t13,t4,t3);}
else{
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_get_rest_arg(c,4,av,4,t0);
f_3928(2,av2);}}}

/* k3926 in ##sys#extend-se in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_3928(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_3928,c,av);}
a=C_alloc(12);
t2=C_i_check_list_2(t1,lf[15]);
t3=C_i_check_list_2(((C_word*)t0)[2],lf[15]);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3953,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4018,a[2]=t6,a[3]=((C_word)li6),tmp=(C_word)a,a+=4,tmp));
t8=((C_word*)t6)[1];
f_4018(t8,t4,t1,((C_word*)t0)[2]);}

/* k3951 in k3926 in ##sys#extend-se in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_3953(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,4)))){
C_save_and_reclaim((void *)f_3953,c,av);}
a=C_alloc(17);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3968,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3970,a[2]=t4,a[3]=t8,a[4]=t5,a[5]=((C_word)li5),tmp=(C_word)a,a+=6,tmp));
t10=((C_word*)t8)[1];
f_3970(t10,t6,((C_word*)t0)[4],((C_word*)t0)[5]);}

/* k3966 in k3951 in k3926 in ##sys#extend-se in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_3968(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3968,c,av);}
/* expand.scm:142: scheme#append */
t2=*((C_word*)lf[16]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop469 in k3951 in k3926 in ##sys#extend-se in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_3970(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_3970,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_cons(&a,2,t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* for-each-loop432 in k3926 in ##sys#extend-se in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4018(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_4018,4,t0,t1,t2,t3);}
a=C_alloc(8);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_i_getprop(t7,lf[9],C_SCHEME_FALSE);
t9=(C_truep(t8)?C_a_i_putprop(&a,3,t6,lf[9],t8):C_a_i_putprop(&a,3,t6,lf[9],t7));
t11=t1;
t12=C_slot(t2,C_fix(1));
t13=C_slot(t3,C_fix(1));
t1=t11;
t2=t12;
t3=t13;
goto loop;}
else{
t6=C_SCHEME_UNDEFINED;
t7=t1;{
C_word av2[2];
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* map-loop406 in ##sys#extend-se in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4067(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_4067,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4092,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:136: g412 */
t4=*((C_word*)lf[10]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4090 in map-loop406 in ##sys#extend-se in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4092(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4092,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_4067(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* ##sys#globalize in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4104(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_4104,c,av);}
a=C_alloc(7);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4110,a[2]=t5,a[3]=t3,a[4]=((C_word)li11),tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_4110(t7,t1,t2);}

/* loop1 in ##sys#globalize in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4110(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_4110,3,t0,t1,t2);}
a=C_alloc(7);
t3=C_i_symbolp(t2);
if(C_truep(C_i_not(t3))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_i_getprop(t2,lf[7],C_SCHEME_FALSE);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4126,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word)li9),tmp=(C_word)a,a+=5,tmp);
/* expand.scm:149: g516 */
t6=t5;
f_4126(t6,t1,t4);}
else{
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4144,a[2]=t2,a[3]=t6,a[4]=((C_word)li10),tmp=(C_word)a,a+=5,tmp));
t8=((C_word*)t6)[1];
f_4144(t8,t1,((C_word*)t0)[3]);}}}

/* g516 in loop1 in ##sys#globalize in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4126(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_4126,3,t0,t1,t2);}
if(C_truep(C_i_symbolp(t2))){
/* expand.scm:151: loop1 */
t3=((C_word*)((C_word*)t0)[2])[1];
f_4110(t3,t1,t2);}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* loop in loop1 in ##sys#globalize in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4144(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_4144,3,t0,t1,t2);}
if(C_truep(C_i_nullp(t2))){
/* expand.scm:155: ##sys#alias-global-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[19]+1));
C_word av2[5];
av2[0]=*((C_word*)lf[19]+1);
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
tp(5,av2);}}
else{
t3=C_i_caar(t2);
t4=C_eqp(((C_word*)t0)[2],t3);
if(C_truep(t4)){
t5=C_u_i_car(t2);
if(C_truep(C_i_symbolp(C_u_i_cdr(t5)))){
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_u_i_cdr(C_u_i_car(t2));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
/* expand.scm:157: loop */
t7=t1;
t8=C_u_i_cdr(t2);
t1=t7;
t2=t8;
goto loop;}}
else{
/* expand.scm:157: loop */
t7=t1;
t8=C_u_i_cdr(t2);
t1=t7;
t2=t8;
goto loop;}}}

/* k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4188(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word t44;
C_word t45;
C_word t46;
C_word t47;
C_word t48;
C_word t49;
C_word t50;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(69,c,6)))){
C_save_and_reclaim((void *)f_4188,c,av);}
a=C_alloc(69);
t2=C_mutate((C_word*)lf[20]+1 /* (set! ##sys#macro-environment ...) */,t1);
t3=C_set_block_item(lf[21] /* ##sys#scheme-macro-environment */,0,C_SCHEME_END_OF_LIST);
t4=C_set_block_item(lf[22] /* ##sys#chicken-macro-environment */,0,C_SCHEME_END_OF_LIST);
t5=C_set_block_item(lf[23] /* ##sys#chicken-ffi-macro-environment */,0,C_SCHEME_END_OF_LIST);
t6=C_set_block_item(lf[24] /* ##sys#chicken.condition-macro-environment */,0,C_SCHEME_END_OF_LIST);
t7=C_set_block_item(lf[25] /* ##sys#chicken.time-macro-environment */,0,C_SCHEME_END_OF_LIST);
t8=C_set_block_item(lf[26] /* ##sys#chicken.type-macro-environment */,0,C_SCHEME_END_OF_LIST);
t9=C_set_block_item(lf[27] /* ##sys#chicken.syntax-macro-environment */,0,C_SCHEME_END_OF_LIST);
t10=C_set_block_item(lf[28] /* ##sys#chicken.base-macro-environment */,0,C_SCHEME_END_OF_LIST);
t11=C_mutate((C_word*)lf[29]+1 /* (set! ##sys#ensure-transformer ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4198,a[2]=((C_word)li13),tmp=(C_word)a,a+=3,tmp));
t12=C_mutate((C_word*)lf[33]+1 /* (set! ##sys#extend-macro-environment ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4222,a[2]=((C_word)li15),tmp=(C_word)a,a+=3,tmp));
t13=C_mutate((C_word*)lf[34]+1 /* (set! ##sys#macro? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4264,a[2]=((C_word)li16),tmp=(C_word)a,a+=3,tmp));
t14=C_mutate((C_word*)lf[35]+1 /* (set! ##sys#undefine-macro! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4301,a[2]=((C_word)li18),tmp=(C_word)a,a+=3,tmp));
t15=C_mutate((C_word*)lf[36]+1 /* (set! ##sys#expand-0 ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4346,a[2]=((C_word)li37),tmp=(C_word)a,a+=3,tmp));
t16=C_set_block_item(lf[63] /* ##sys#compiler-syntax-hook */,0,C_SCHEME_FALSE);
t17=C_set_block_item(lf[65] /* ##sys#enable-runtime-macros */,0,C_SCHEME_FALSE);
t18=C_mutate((C_word*)lf[45]+1 /* (set! chicken.syntax#expansion-result-hook ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4844,a[2]=((C_word)li38),tmp=(C_word)a,a+=3,tmp));
t19=C_mutate((C_word*)lf[66]+1 /* (set! chicken.syntax#expand ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4847,a[2]=((C_word)li42),tmp=(C_word)a,a+=3,tmp));
t20=C_mutate((C_word*)lf[67]+1 /* (set! ##sys#extended-lambda-list? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4913,a[2]=((C_word)li44),tmp=(C_word)a,a+=3,tmp));
t21=C_mutate((C_word*)lf[71]+1 /* (set! ##sys#expand-extended-lambda-list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4960,a[2]=((C_word)li49),tmp=(C_word)a,a+=3,tmp));
t22=C_mutate((C_word*)lf[94]+1 /* (set! ##sys#expand-multiple-values-assignment ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5569,a[2]=((C_word)li53),tmp=(C_word)a,a+=3,tmp));
t23=C_set_block_item(lf[99] /* chicken.syntax#define-definition */,0,C_SCHEME_UNDEFINED);
t24=C_set_block_item(lf[100] /* chicken.syntax#define-syntax-definition */,0,C_SCHEME_UNDEFINED);
t25=C_set_block_item(lf[101] /* chicken.syntax#define-values-definition */,0,C_SCHEME_UNDEFINED);
t26=lf[102] /* chicken.syntax#import-definition */ =C_SCHEME_UNDEFINED;;
t27=C_mutate((C_word*)lf[103]+1 /* (set! ##sys#canonicalize-body ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5749,a[2]=((C_word)li68),tmp=(C_word)a,a+=3,tmp));
t28=C_mutate((C_word*)lf[122]+1 /* (set! chicken.syntax#match-expression ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6755,a[2]=((C_word)li71),tmp=(C_word)a,a+=3,tmp));
t29=C_mutate((C_word*)lf[116]+1 /* (set! chicken.syntax#expand-curried-define ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6837,a[2]=((C_word)li73),tmp=(C_word)a,a+=3,tmp));
t30=C_set_block_item(lf[123] /* ##sys#line-number-database */,0,C_SCHEME_FALSE);
t31=C_set_block_item(lf[124] /* ##sys#syntax-error-culprit */,0,C_SCHEME_FALSE);
t32=C_set_block_item(lf[125] /* ##sys#syntax-context */,0,C_SCHEME_END_OF_LIST);
t33=C_mutate((C_word*)lf[126]+1 /* (set! chicken.syntax#syntax-error ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6892,a[2]=((C_word)li74),tmp=(C_word)a,a+=3,tmp));
t34=C_mutate((C_word*)lf[46]+1 /* (set! ##sys#syntax-error-hook ...) */,*((C_word*)lf[126]+1));
t35=C_mutate((C_word*)lf[129]+1 /* (set! ##sys#syntax-error/context ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6903,a[2]=((C_word)li79),tmp=(C_word)a,a+=3,tmp));
t36=C_mutate((C_word*)lf[148]+1 /* (set! chicken.syntax#get-line-number ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7124,a[2]=((C_word)li81),tmp=(C_word)a,a+=3,tmp));
t37=C_mutate((C_word*)lf[59]+1 /* (set! ##sys#check-syntax ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7163,a[2]=((C_word)li92),tmp=(C_word)a,a+=3,tmp));
t38=C_mutate(&lf[181] /* (set! chicken.syntax#make-er/ir-transformer ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7607,a[2]=((C_word)li101),tmp=(C_word)a,a+=3,tmp));
t39=C_mutate((C_word*)lf[185]+1 /* (set! chicken.syntax#er-macro-transformer ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8100,a[2]=((C_word)li102),tmp=(C_word)a,a+=3,tmp));
t40=C_mutate((C_word*)lf[186]+1 /* (set! chicken.syntax#ir-macro-transformer ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8106,a[2]=((C_word)li103),tmp=(C_word)a,a+=3,tmp));
t41=C_mutate((C_word*)lf[187]+1 /* (set! ##sys#er-transformer ...) */,*((C_word*)lf[185]+1));
t42=C_mutate((C_word*)lf[188]+1 /* (set! ##sys#ir-transformer ...) */,*((C_word*)lf[186]+1));
t43=C_mutate((C_word*)lf[99]+1 /* (set! chicken.syntax#define-definition ...) */,*((C_word*)lf[99]+1));
t44=C_mutate((C_word*)lf[100]+1 /* (set! chicken.syntax#define-syntax-definition ...) */,*((C_word*)lf[100]+1));
t45=C_mutate((C_word*)lf[101]+1 /* (set! chicken.syntax#define-values-definition ...) */,*((C_word*)lf[101]+1));
t46=C_mutate((C_word*)lf[45]+1 /* (set! chicken.syntax#expansion-result-hook ...) */,*((C_word*)lf[45]+1));
t47=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8119,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t48=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12169,a[2]=t47,tmp=(C_word)a,a+=3,tmp);
t49=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12171,a[2]=((C_word)li180),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:958: ##sys#er-transformer */
t50=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t50;
av2[1]=t48;
av2[2]=t49;
((C_proc)(void*)(*((C_word*)t50+1)))(3,av2);}}

/* ##sys#ensure-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4198(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4198,c,av);}
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
if(C_truep(C_i_structurep(t2,lf[30]))){
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
/* expand.scm:177: ##sys#error */
t5=*((C_word*)lf[31]+1);{
C_word av2[5];
av2[0]=t5;
av2[1]=t1;
av2[2]=t4;
av2[3]=lf[32];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}}

/* ##sys#extend-macro-environment in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4222(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_4222,c,av);}
a=C_alloc(6);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4226,a[2]=t2,a[3]=t3,a[4]=t1,a[5]=t4,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:180: ##sys#macro-environment */
t6=*((C_word*)lf[20]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* k4224 in ##sys#extend-macro-environment in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4226(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4226,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4229,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:181: ##sys#ensure-transformer */
t3=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4227 in k4224 in ##sys#extend-macro-environment in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4229(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_4229,c,av);}
a=C_alloc(16);
t2=(
/* expand.scm:182: lookup */
  f_3718(((C_word*)t0)[2],((C_word*)t0)[3])
);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4236,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=((C_word)li14),tmp=(C_word)a,a+=5,tmp);
/* expand.scm:182: g561 */
t4=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t4;
av2[1]=(
/* expand.scm:182: g561 */
  f_4236(t3,t2)
);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=C_a_i_list2(&a,2,((C_word*)t0)[4],t1);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4254,a[2]=((C_word*)t0)[5],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t5=C_a_i_cons(&a,2,((C_word*)t0)[2],t3);
t6=C_a_i_cons(&a,2,t5,((C_word*)t0)[3]);
/* expand.scm:189: ##sys#macro-environment */
t7=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t4;
av2[2]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}}

/* g561 in k4227 in k4224 in ##sys#extend-macro-environment in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static C_word C_fcall f_4236(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;{}
t2=C_i_set_car(t1,((C_word*)t0)[2]);
t3=C_i_set_car(C_u_i_cdr(t1),((C_word*)t0)[3]);
return(t1);}

/* k4252 in k4227 in k4224 in ##sys#extend-macro-environment in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4254(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4254,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* ##sys#macro? in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4264(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4264,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4268,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_rest_nullp(c,3))){
/* expand.scm:193: ##sys#current-environment */
t4=*((C_word*)lf[4]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_get_rest_arg(c,3,av,3,t0);
f_4268(2,av2);}}}

/* k4266 in ##sys#macro? in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4268(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4268,c,av);}
a=C_alloc(4);
t2=(
/* expand.scm:194: lookup */
  f_3718(((C_word*)t0)[2],t1)
);
t3=C_i_pairp(t2);
if(C_truep(t3)){
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4290,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:196: ##sys#macro-environment */
t5=*((C_word*)lf[20]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k4288 in k4266 in ##sys#macro? in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4290(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4290,c,av);}
t2=(
/* expand.scm:196: lookup */
  f_3718(((C_word*)t0)[2],t1)
);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(t2)?C_i_pairp(t2):C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* ##sys#undefine-macro! in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4301(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4301,c,av);}
a=C_alloc(7);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4309,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4313,a[2]=t2,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:202: ##sys#macro-environment */
t5=*((C_word*)lf[20]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k4307 in ##sys#undefine-macro! in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4309(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4309,c,av);}
/* expand.scm:200: ##sys#macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4311 in ##sys#undefine-macro! in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4313(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_4313,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4315,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word)li17),tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];
f_4315(t5,((C_word*)t0)[3],t1);}

/* loop in k4311 in ##sys#undefine-macro! in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4315(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_4315,3,t0,t1,t2);}
a=C_alloc(4);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_caar(t2);
t4=C_eqp(((C_word*)t0)[2],t3);
if(C_truep(t4)){
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_u_i_cdr(t2);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_u_i_car(t2);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4338,a[2]=t1,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:205: loop */
t8=t6;
t9=C_u_i_cdr(t2);
t1=t8;
t2=t9;
goto loop;}}}

/* k4336 in loop in k4311 in ##sys#undefine-macro! in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4338(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_4338,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4346(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(22,c,7)))){
C_save_and_reclaim((void *)f_4346,c,av);}
a=C_alloc(22);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4349,a[2]=t3,a[3]=((C_word)li31),tmp=(C_word)a,a+=4,tmp));
t10=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4548,a[2]=t6,a[3]=((C_word)li32),tmp=(C_word)a,a+=4,tmp));
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4592,a[2]=t3,a[3]=t8,a[4]=t12,a[5]=t6,a[6]=t4,a[7]=((C_word)li36),tmp=(C_word)a,a+=8,tmp));
t14=((C_word*)t12)[1];
f_4592(t14,t1,t2);}

/* call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4349(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6){
C_word tmp;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_4349,7,t0,t1,t2,t3,t4,t5,t6);}
a=C_alloc(12);
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4359,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t8=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4364,a[2]=t2,a[3]=t4,a[4]=t6,a[5]=t3,a[6]=t5,a[7]=((C_word*)t0)[2],a[8]=((C_word)li30),tmp=(C_word)a,a+=9,tmp);
/* expand.scm:213: scheme#call-with-current-continuation */
t9=*((C_word*)lf[53]+1);{
C_word av2[3];
av2[0]=t9;
av2[1]=t7;
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* k4357 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4359(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4359,c,av);}
/* g613614 */
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)C_fast_retrieve_proc(t2))(2,av2);}}

/* a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4364(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_4364,c,av);}
a=C_alloc(15);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4370,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word)li21),tmp=(C_word)a,a+=5,tmp);
t4=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4471,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t2,a[9]=((C_word)li29),tmp=(C_word)a,a+=10,tmp);
/* expand.scm:213: chicken.condition#with-exception-handler */
t5=*((C_word*)lf[52]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=t3;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* a4369 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4370(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4370,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4376,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word)li20),tmp=(C_word)a,a+=5,tmp);
/* expand.scm:213: k610 */
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* a4375 in a4369 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4376(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_4376,c,av);}
a=C_alloc(11);
t2=C_i_structurep(((C_word*)t0)[2],lf[37]);
t3=(C_truep(t2)?C_i_memq(lf[38],C_slot(((C_word*)t0)[2],C_fix(1))):C_SCHEME_FALSE);
if(C_truep(t3)){
t4=C_slot(((C_word*)t0)[2],C_fix(1));
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4398,a[2]=t4,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t6=C_slot(((C_word*)t0)[2],C_fix(2));
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4404,a[2]=((C_word*)t0)[3],a[3]=t8,a[4]=((C_word)li19),tmp=(C_word)a,a+=5,tmp));
t10=((C_word*)t8)[1];
f_4404(t10,t5,t6);}
else{
/* expand.scm:216: chicken.condition#abort */
t4=*((C_word*)lf[39]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k4396 in a4375 in a4369 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4398(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4398,c,av);}
a=C_alloc(4);
t2=C_a_i_record3(&a,3,lf[37],((C_word*)t0)[2],t1);
/* expand.scm:216: chicken.condition#abort */
t3=*((C_word*)lf[39]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* copy in a4375 in a4369 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4404(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_4404,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_car(t2);
t4=C_u_i_cdr(t2);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4421,a[2]=t4,a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_equalp(lf[44],t3))){
t6=C_i_pairp(t4);
t7=t5;
f_4421(t7,(C_truep(t6)?C_i_stringp(C_u_i_car(t4)):C_SCHEME_FALSE));}
else{
t6=t5;
f_4421(t6,C_SCHEME_FALSE);}}}

/* k4419 in copy in a4375 in a4369 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4421(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,5)))){
C_save_and_reclaim_args((void *)trf_4421,2,t0,t1);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4432,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:232: scheme#string-append */
t3=*((C_word*)lf[41]+1);{
C_word av2[6];
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[42];
av2[3]=C_slot(((C_word*)t0)[4],C_fix(1));
av2[4]=lf[43];
av2[5]=C_i_car(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}
else{
/* expand.scm:238: copy */
t2=((C_word*)((C_word*)t0)[5])[1];
f_4404(t2,((C_word*)t0)[3],((C_word*)t0)[2]);}}

/* k4430 in k4419 in copy in a4375 in a4369 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4432(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_4432,c,av);}
a=C_alloc(6);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,t1,t2);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_cons(&a,2,lf[40],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a4470 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4471(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_4471,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4477,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word)li26),tmp=(C_word)a,a+=9,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4536,a[2]=((C_word*)t0)[8],a[3]=((C_word)li28),tmp=(C_word)a,a+=4,tmp);
/* expand.scm:213: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a4476 in a4470 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4477(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(31,c,4)))){
C_save_and_reclaim((void *)f_4477,c,av);}
a=C_alloc(31);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4481,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[3])){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4511,a[2]=((C_word*)t0)[2],a[3]=((C_word)li22),tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_SCHEME_FALSE;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4517,a[2]=t6,a[3]=t4,a[4]=((C_word)li23),tmp=(C_word)a,a+=5,tmp);
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4522,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word)li24),tmp=(C_word)a,a+=7,tmp);
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4528,a[2]=t4,a[3]=t6,a[4]=((C_word)li25),tmp=(C_word)a,a+=5,tmp);
/* expand.scm:243: ##sys#dynamic-wind */
t10=*((C_word*)lf[51]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t10;
av2[1]=t2;
av2[2]=t7;
av2[3]=t8;
av2[4]=t9;
((C_proc)(void*)(*((C_word*)t10+1)))(5,av2);}}
else{
/* expand.scm:246: handler */
t3=((C_word*)t0)[5];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
av2[3]=((C_word*)t0)[6];
av2[4]=((C_word*)t0)[7];
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}}

/* k4479 in a4476 in a4470 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4481(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_4481,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4484,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t3=C_i_not(((C_word*)t0)[4]);
t4=(C_truep(t3)?C_eqp(((C_word*)t0)[3],t1):C_SCHEME_FALSE);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4500,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4504,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* expand.scm:250: scheme#symbol->string */
t7=*((C_word*)lf[49]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}
else{
/* expand.scm:254: expansion-result-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[45]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[45]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
tp(4,av2);}}}

/* k4482 in k4479 in a4476 in a4470 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4484(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4484,c,av);}
/* expand.scm:254: expansion-result-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[45]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[45]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
tp(4,av2);}}

/* k4498 in k4479 in a4476 in a4470 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4500(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4500,c,av);}
/* expand.scm:248: ##sys#syntax-error-hook */
t2=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k4502 in k4479 in a4476 in a4470 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4504(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4504,c,av);}
/* expand.scm:249: scheme#string-append */
t2=*((C_word*)lf[41]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[47];
av2[3]=t1;
av2[4]=lf[48];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* f_4511 in a4476 in a4470 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4511(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4511,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a4516 in a4476 in a4470 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4517(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4517,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[50]+1));
t3=C_mutate((C_word*)lf[50]+1 /* (set! chicken.internal.syntax-rules#syntax-rules-mismatch ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a4521 in a4476 in a4470 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4522(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4522,c,av);}
/* expand.scm:245: handler */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=((C_word*)t0)[5];
((C_proc)C_fast_retrieve_proc(t2))(5,av2);}}

/* a4527 in a4476 in a4470 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4528(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4528,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[50]+1));
t3=C_mutate((C_word*)lf[50]+1 /* (set! chicken.internal.syntax-rules#syntax-rules-mismatch ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a4535 in a4470 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4536(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +4,c,2)))){
C_save_and_reclaim((void*)f_4536,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+4);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4542,a[2]=t2,a[3]=((C_word)li27),tmp=(C_word)a,a+=4,tmp);
/* expand.scm:213: k610 */
t4=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* a4541 in a4535 in a4470 in a4363 in call-handler in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4542(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4542,c,av);}{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
C_apply_values(3,av2);}}

/* expand in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4548(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,6)))){
C_save_and_reclaim_args((void *)trf_4548,5,t0,t1,t2,t3,t4);}
a=C_alloc(3);
t5=C_i_listp(t3);
if(C_truep(C_i_not(t5))){
/* expand.scm:266: ##sys#syntax-error-hook */
t6=*((C_word*)lf[46]+1);{
C_word av2[4];
av2[0]=t6;
av2[1]=t1;
av2[2]=lf[54];
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
if(C_truep(C_i_pairp(t4))){
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4574,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* expand.scm:270: call-handler */
t7=((C_word*)((C_word*)t0)[2])[1];
f_4349(t7,t6,t2,C_i_cadr(t4),t3,C_u_i_car(t4),C_SCHEME_FALSE);}
else{
/* expand.scm:272: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=t3;
av2[3]=C_SCHEME_FALSE;
C_values(4,av2);}}}}

/* k4572 in expand in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4574(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4574,c,av);}
/* expand.scm:268: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_TRUE;
C_values(4,av2);}}

/* loop in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4592(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,0,3)))){
C_save_and_reclaim_args((void *)trf_4592,3,t0,t1,t2);}
a=C_alloc(18);
if(C_truep(C_i_pairp(t2))){
t3=C_u_i_car(t2);
t4=C_u_i_cdr(t2);
if(C_truep(C_i_symbolp(t3))){
t5=(
/* expand.scm:278: lookup */
  f_3718(t3,((C_word*)t0)[2])
);
t6=(C_truep(t5)?t5:t3);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_4616,a[2]=t8,a[3]=t4,a[4]=t1,a[5]=((C_word*)t0)[2],a[6]=t2,a[7]=((C_word*)t0)[3],a[8]=t3,a[9]=((C_word*)t0)[4],a[10]=((C_word*)t0)[5],a[11]=((C_word*)t0)[6],tmp=(C_word)a,a+=12,tmp);
if(C_truep(C_i_pairp(((C_word*)t8)[1]))){
t10=t9;
f_4616(t10,C_SCHEME_UNDEFINED);}
else{
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4834,a[2]=t8,a[3]=t9,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:280: ##sys#macro-environment */
t11=*((C_word*)lf[20]+1);{
C_word av2[2];
av2[0]=t11;
av2[1]=t10;
((C_proc)(void*)(*((C_word*)t11+1)))(2,av2);}}}
else{
/* expand.scm:306: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=t2;
av2[3]=C_SCHEME_FALSE;
C_values(4,av2);}}}
else{
/* expand.scm:307: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=t2;
av2[3]=C_SCHEME_FALSE;
C_values(4,av2);}}}

/* k4614 in loop in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4616(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,6)))){
C_save_and_reclaim_args((void *)trf_4616,2,t0,t1);}
a=C_alloc(9);
t2=C_eqp(((C_word*)((C_word*)t0)[2])[1],lf[55]);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4625,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:282: ##sys#check-syntax */
t4=*((C_word*)lf[59]+1);{
C_word av2[7];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[60];
av2[3]=((C_word*)t0)[3];
av2[4]=lf[62];
av2[5]=C_SCHEME_FALSE;
av2[6]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t4+1)))(7,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4771,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[8],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[10],a[8]=((C_word*)t0)[4],tmp=(C_word)a,a+=9,tmp);
if(C_truep(((C_word*)t0)[11])){
if(C_truep(C_i_symbolp(((C_word*)((C_word*)t0)[2])[1]))){
t4=((C_word*)((C_word*)t0)[2])[1];
t5=t3;
f_4771(t5,C_i_getprop(t4,lf[64],C_SCHEME_FALSE));}
else{
t4=t3;
f_4771(t4,C_SCHEME_FALSE);}}
else{
t4=t3;
f_4771(t4,C_SCHEME_FALSE);}}}

/* k4623 in k4614 in loop in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4625(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,6)))){
C_save_and_reclaim((void *)f_4625,c,av);}
a=C_alloc(5);
t2=C_i_car(((C_word*)t0)[2]);
if(C_truep(C_i_symbolp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4637,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:285: ##sys#check-syntax */
t4=*((C_word*)lf[59]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[60];
av2[3]=((C_word*)t0)[2];
av2[4]=lf[61];
av2[5]=C_SCHEME_FALSE;
av2[6]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(7,av2);}}
else{
/* expand.scm:296: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_FALSE;
C_values(4,av2);}}}

/* k4635 in k4623 in k4614 in loop in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4637(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,3)))){
C_save_and_reclaim((void *)f_4637,c,av);}
a=C_alloc(19);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_i_check_list_2(t2,lf[17]);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4727,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4733,a[2]=t5,a[3]=t10,a[4]=t6,a[5]=((C_word)li34),tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_4733(t12,t8,t2);}

/* k4660 in k4725 in k4635 in k4623 in k4614 in loop in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4662(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4662,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,lf[58],t2);
/* expand.scm:287: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=t3;
av2[3]=C_SCHEME_TRUE;
C_values(4,av2);}}

/* map-loop709 in k4725 in k4635 in k4623 in k4614 in loop in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4664(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_4664,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_cadr(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4725 in k4635 in k4623 in k4614 in loop in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4727(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(41,c,3)))){
C_save_and_reclaim((void *)f_4727,c,av);}
a=C_alloc(41);
t2=C_i_cddr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,t1,t2);
t4=C_a_i_cons(&a,2,lf[56],t3);
t5=C_a_i_list(&a,2,((C_word*)t0)[3],t4);
t6=C_a_i_list(&a,1,t5);
t7=C_a_i_list(&a,3,lf[57],t6,((C_word*)t0)[3]);
t8=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t9=t8;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=((C_word*)t10)[1];
t12=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4662,a[2]=t7,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t13=C_SCHEME_UNDEFINED;
t14=(*a=C_VECTOR_TYPE|1,a[1]=t13,tmp=(C_word)a,a+=2,tmp);
t15=C_set_block_item(t14,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4664,a[2]=t10,a[3]=t14,a[4]=t11,a[5]=((C_word)li33),tmp=(C_word)a,a+=6,tmp));
t16=((C_word*)t14)[1];
f_4664(t16,t12,((C_word*)t0)[5]);}

/* map-loop682 in k4635 in k4623 in k4614 in loop in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4733(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_4733,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4769 in k4614 in loop in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4771(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,4)))){
C_save_and_reclaim_args((void *)trf_4771,2,t0,t1);}
a=C_alloc(9);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4775,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word)li35),tmp=(C_word)a,a+=9,tmp);
/* expand.scm:281: g742 */
t3=t2;
f_4775(t3,((C_word*)t0)[8],t1);}
else{
/* expand.scm:305: expand */
t2=((C_word*)((C_word*)t0)[3])[1];
f_4548(t2,((C_word*)t0)[8],((C_word*)t0)[4],((C_word*)t0)[2],((C_word*)((C_word*)t0)[5])[1]);}}

/* g742 in k4769 in k4614 in loop in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4775(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,6)))){
C_save_and_reclaim_args((void *)trf_4775,3,t0,t1,t2);}
a=C_alloc(8);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4779,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* expand.scm:299: call-handler */
t4=((C_word*)((C_word*)t0)[7])[1];
f_4349(t4,t3,((C_word*)t0)[4],C_i_car(t2),((C_word*)t0)[2],C_u_i_cdr(t2),C_SCHEME_TRUE);}

/* k4777 in g742 in k4769 in k4614 in loop in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4779(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_4779,c,av);}
a=C_alloc(5);
t2=C_eqp(t1,((C_word*)t0)[2]);
if(C_truep(t2)){
/* expand.scm:300: expand */
t3=((C_word*)((C_word*)t0)[3])[1];
f_4548(t3,((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[2],((C_word*)((C_word*)t0)[6])[1]);}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4791,a[2]=((C_word*)t0)[7],a[3]=((C_word*)t0)[4],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
if(C_truep(*((C_word*)lf[63]+1))){
/* expand.scm:303: ##sys#compiler-syntax-hook */
t4=*((C_word*)lf[63]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)((C_word*)t0)[6])[1];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
/* expand.scm:304: loop */
t4=((C_word*)((C_word*)t0)[7])[1];
f_4592(t4,((C_word*)t0)[4],t1);}}}

/* k4789 in k4777 in g742 in k4769 in k4614 in loop in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4791(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4791,c,av);}
/* expand.scm:304: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_4592(t2,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* k4832 in loop in ##sys#expand-0 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4834(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4834,c,av);}
t2=(
/* expand.scm:280: lookup */
  f_3718(((C_word*)((C_word*)t0)[2])[1],t1)
);
if(C_truep(t2)){
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t4=((C_word*)t0)[3];
f_4616(t4,t3);}
else{
t3=((C_word*)((C_word*)t0)[2])[1];
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t3);
t5=((C_word*)t0)[3];
f_4616(t5,t4);}}

/* chicken.syntax#expansion-result-hook in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4844(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4844,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.syntax#expand in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4847(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +5,c,2)))){
C_save_and_reclaim((void*)f_4847,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+5);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4851,a[2]=t3,a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_nullp(t3))){
/* expand.scm:316: ##sys#current-environment */
t5=*((C_word*)lf[4]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_i_car(t3);
f_4851(2,av2);}}}

/* k4849 in chicken.syntax#expand in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4851(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_4851,c,av);}
a=C_alloc(8);
t2=C_i_nullp(((C_word*)t0)[2]);
t3=(C_truep(t2)?C_SCHEME_END_OF_LIST:C_i_cdr(((C_word*)t0)[2]));
t4=C_i_nullp(t3);
t5=(C_truep(t4)?C_SCHEME_FALSE:C_i_car(t3));
t6=C_i_nullp(t3);
t7=(C_truep(t6)?C_SCHEME_END_OF_LIST:C_i_cdr(t3));
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4865,a[2]=t1,a[3]=t5,a[4]=t9,a[5]=((C_word)li41),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_4865(t11,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* loop in k4849 in chicken.syntax#expand in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4865(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,4)))){
C_save_and_reclaim_args((void *)trf_4865,3,t0,t1,t2);}
a=C_alloc(10);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4871,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word)li39),tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4877,a[2]=((C_word*)t0)[4],a[3]=((C_word)li40),tmp=(C_word)a,a+=4,tmp);
/* expand.scm:318: ##sys#call-with-values */{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=t3;
av2[3]=t4;
C_call_with_values(4,av2);}}

/* a4870 in loop in k4849 in chicken.syntax#expand in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4871(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4871,c,av);}
/* expand.scm:318: ##sys#expand-0 */
t2=*((C_word*)lf[36]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a4876 in loop in k4849 in chicken.syntax#expand in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4877(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4877,c,av);}
if(C_truep(t3)){
/* expand.scm:320: loop */
t4=((C_word*)((C_word*)t0)[2])[1];
f_4865(t4,t1,t2);}
else{
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* ##sys#extended-lambda-list? in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4913(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4913,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4919,a[2]=((C_word)li43),tmp=(C_word)a,a+=3,tmp);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=(
  f_4919(t2)
);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* loop in ##sys#extended-lambda-list? in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static C_word C_fcall f_4919(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_pairp(t1))){
t2=C_slot(t1,C_fix(0));
t3=C_eqp(t2,lf[68]);
if(C_truep(t3)){
if(C_truep(t3)){
return(C_SCHEME_TRUE);}
else{
t7=C_u_i_cdr(t1);
t1=t7;
goto loop;}}
else{
t4=C_eqp(t2,lf[69]);
if(C_truep(t4)){
if(C_truep(t4)){
return(C_SCHEME_TRUE);}
else{
t7=C_u_i_cdr(t1);
t1=t7;
goto loop;}}
else{
t5=C_eqp(t2,lf[70]);
if(C_truep(t5)){
return(C_SCHEME_TRUE);}
else{
t7=C_u_i_cdr(t1);
t1=t7;
goto loop;}}}}
else{
return(C_SCHEME_FALSE);}}

/* ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4960(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(c!=6) C_bad_argc_2(c,6,t0);
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_4960,c,av);}
a=C_alloc(18);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4963,a[2]=t4,a[3]=t2,a[4]=((C_word)li45),tmp=(C_word)a,a+=5,tmp);
t7=C_SCHEME_FALSE;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_SCHEME_FALSE;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4980,a[2]=t8,a[3]=t10,a[4]=t3,a[5]=t6,a[6]=t5,a[7]=t1,a[8]=t2,tmp=(C_word)a,a+=9,tmp);
/* expand.scm:351: macro-alias */
f_3735(t11,lf[92],*((C_word*)lf[93]+1));}

/* err in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4963(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_4963,3,t0,t1,t2);}
/* expand.scm:346: errh */
t3=((C_word*)t0)[2];{
C_word av2[4];
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
av2[3]=((C_word*)t0)[3];
((C_proc)C_fast_retrieve_proc(t3))(4,av2);}}

/* k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4980(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_4980,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4983,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* expand.scm:353: macro-alias */
f_3735(t2,lf[91],*((C_word*)lf[28]+1));}

/* k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4983(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_4983,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_4986,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* expand.scm:354: macro-alias */
f_3735(t2,lf[90],*((C_word*)lf[28]+1));}

/* k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_4986(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,7)))){
C_save_and_reclaim((void *)f_4986,c,av);}
a=C_alloc(14);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_4991,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=t3,a[10]=((C_word*)t0)[8],a[11]=((C_word)li48),tmp=(C_word)a,a+=12,tmp));
t5=((C_word*)t3)[1];
f_4991(t5,((C_word*)t0)[9],C_fix(0),C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,((C_word*)t0)[10]);}

/* loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_4991(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6){
C_word tmp;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(15,0,6)))){
C_save_and_reclaim_args((void *)trf_4991,7,t0,t1,t2,t3,t4,t5,t6);}
a=C_alloc(15);
if(C_truep(C_i_nullp(t6))){
t7=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_5005,a[2]=t4,a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=t5,a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[6],a[10]=((C_word*)t0)[7],tmp=(C_word)a,a+=11,tmp);
if(C_truep(((C_word*)((C_word*)t0)[3])[1])){
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5253,a[2]=t7,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:363: reverse */
t9=*((C_word*)lf[73]+1);{
C_word av2[3];
av2[0]=t9;
av2[1]=t8;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}
else{
/* expand.scm:363: reverse */
t8=*((C_word*)lf[73]+1);{
C_word av2[3];
av2[0]=t8;
av2[1]=t7;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}}
else{
if(C_truep(C_i_symbolp(t6))){
if(C_truep(C_fixnum_greaterp(t2,C_fix(2)))){
/* expand.scm:391: err */
t7=((C_word*)t0)[8];
f_4963(t7,t1,lf[79]);}
else{
if(C_truep(((C_word*)((C_word*)t0)[3])[1])){
t7=C_mutate(((C_word *)((C_word*)t0)[4])+1,t6);
/* expand.scm:395: loop */
t12=t1;
t13=C_fix(4);
t14=t3;
t15=t4;
t16=C_SCHEME_END_OF_LIST;
t17=C_SCHEME_END_OF_LIST;
t1=t12;
t2=t13;
t3=t14;
t4=t15;
t5=t16;
t6=t17;
goto loop;}
else{
t7=C_mutate(((C_word *)((C_word*)t0)[3])+1,t6);
t8=C_mutate(((C_word *)((C_word*)t0)[4])+1,t6);
/* expand.scm:395: loop */
t12=t1;
t13=C_fix(4);
t14=t3;
t15=t4;
t16=C_SCHEME_END_OF_LIST;
t17=C_SCHEME_END_OF_LIST;
t1=t12;
t2=t13;
t3=t14;
t4=t15;
t5=t16;
t6=t17;
goto loop;}}}
else{
t7=C_i_pairp(t6);
if(C_truep(C_i_not(t7))){
/* expand.scm:397: err */
t8=((C_word*)t0)[8];
f_4963(t8,t1,lf[80]);}
else{
t8=C_i_car(t6);
t9=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_5294,a[2]=t8,a[3]=t6,a[4]=t2,a[5]=((C_word*)t0)[9],a[6]=t1,a[7]=t3,a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[3],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[4],a[12]=t4,a[13]=t5,tmp=(C_word)a,a+=14,tmp);
if(C_truep(C_i_symbolp(t8))){
t10=C_eqp(C_fix(3),t2);
/* expand.scm:400: lookup */
t11=t9;
f_5294(t11,(C_truep(C_i_not(t10))?(
/* expand.scm:400: lookup */
  f_3718(t8,((C_word*)t0)[10])
):C_SCHEME_FALSE));}
else{
t10=t9;
f_5294(t10,C_SCHEME_FALSE);}}}}}

/* k5003 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5005(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(28,c,3)))){
C_save_and_reclaim((void *)f_5005,c,av);}
a=C_alloc(28);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_5009,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
if(C_truep(C_i_nullp(((C_word*)t0)[7]))){
t3=t2;
f_5009(t3,((C_word*)t0)[9]);}
else{
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5149,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[5],a[4]=((C_word)li46),tmp=(C_word)a,a+=5,tmp);
t8=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5209,a[2]=((C_word*)t0)[9],a[3]=((C_word*)t0)[10],a[4]=t2,a[5]=t5,a[6]=t7,a[7]=t6,tmp=(C_word)a,a+=8,tmp);
/* expand.scm:375: reverse */
t9=*((C_word*)lf[73]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t9;
av2[1]=t8;
av2[2]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}}

/* k5007 in k5003 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_5009(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,3)))){
C_save_and_reclaim_args((void *)trf_5009,2,t0,t1);}
a=C_alloc(11);
if(C_truep(C_i_nullp(((C_word*)t0)[2]))){
/* expand.scm:362: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=t1;
C_values(4,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_5021,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=t1,a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
if(C_truep(C_i_not(((C_word*)((C_word*)t0)[7])[1]))){
t3=C_i_nullp(((C_word*)t0)[8]);
t4=t2;
f_5021(t4,(C_truep(t3)?C_i_nullp(C_i_cdr(((C_word*)t0)[2])):C_SCHEME_FALSE));}
else{
t3=t2;
f_5021(t3,C_SCHEME_FALSE);}}}

/* k5019 in k5007 in k5003 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_5021(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_5021,2,t0,t1);}
a=C_alloc(12);
if(C_truep(t1)){
t2=C_i_caar(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5052,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* expand.scm:380: scheme#cadar */
t4=*((C_word*)lf[72]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t2=C_i_not(((C_word*)((C_word*)t0)[8])[1]);
t3=(C_truep(t2)?C_i_nullp(((C_word*)t0)[9]):C_SCHEME_FALSE);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5077,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[10],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],tmp=(C_word)a,a+=7,tmp);
/* expand.scm:384: reverse */
t5=*((C_word*)lf[73]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5096,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[10],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],tmp=(C_word)a,a+=7,tmp);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5100,a[2]=((C_word*)t0)[8],a[3]=t4,a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:387: reverse */
t6=*((C_word*)lf[73]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}}}

/* k5050 in k5019 in k5007 in k5003 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5052(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,3)))){
C_save_and_reclaim((void *)f_5052,c,av);}
a=C_alloc(27);
t2=C_a_i_list(&a,3,((C_word*)t0)[2],((C_word*)((C_word*)t0)[3])[1],t1);
t3=C_a_i_list(&a,2,((C_word*)t0)[4],t2);
t4=C_a_i_list(&a,1,t3);
t5=C_a_i_cons(&a,2,t4,((C_word*)t0)[5]);
t6=C_a_i_cons(&a,2,lf[55],t5);
t7=C_a_i_list(&a,1,t6);
/* expand.scm:362: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[6];
av2[2]=((C_word*)t0)[7];
av2[3]=t7;
C_values(4,av2);}}

/* k5075 in k5019 in k5007 in k5003 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5077(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_5077,c,av);}
a=C_alloc(12);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,((C_word*)((C_word*)t0)[3])[1],t2);
t4=C_a_i_cons(&a,2,((C_word*)t0)[4],t3);
t5=C_a_i_list(&a,1,t4);
/* expand.scm:362: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[5];
av2[2]=((C_word*)t0)[6];
av2[3]=t5;
C_values(4,av2);}}

/* k5094 in k5019 in k5007 in k5003 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5096(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_5096,c,av);}
a=C_alloc(12);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,((C_word*)((C_word*)t0)[3])[1],t2);
t4=C_a_i_cons(&a,2,((C_word*)t0)[4],t3);
t5=C_a_i_list(&a,1,t4);
/* expand.scm:362: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[5];
av2[2]=((C_word*)t0)[6];
av2[3]=t5;
C_values(4,av2);}}

/* k5098 in k5019 in k5007 in k5003 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5100(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_5100,c,av);}
a=C_alloc(3);
t2=((C_word*)((C_word*)t0)[2])[1];
if(C_truep(t2)){
t3=C_a_i_list1(&a,1,t2);
/* expand.scm:387: ##sys#append */
t4=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t3=((C_word*)((C_word*)t0)[4])[1];
t4=C_a_i_list1(&a,1,t3);
/* expand.scm:387: ##sys#append */
t5=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}

/* g845 in k5003 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_5149(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,2)))){
C_save_and_reclaim_args((void *)trf_5149,3,t0,t1,t2);}
a=C_alloc(10);
t3=C_i_car(t2);
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5202,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=t1,a[6]=t3,tmp=(C_word)a,a+=7,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5206,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
/* expand.scm:371: chicken.syntax#strip-syntax */
t6=*((C_word*)lf[12]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k5200 in g845 in k5003 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5202(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(30,c,1)))){
C_save_and_reclaim((void *)f_5202,c,av);}
a=C_alloc(30);
t2=C_a_i_list(&a,2,lf[75],t1);
t3=((C_word*)((C_word*)t0)[2])[1];
t4=(C_truep(t3)?t3:((C_word*)((C_word*)t0)[3])[1]);
if(C_truep(C_i_pairp(C_u_i_cdr(((C_word*)t0)[4])))){
t5=C_u_i_cdr(((C_word*)t0)[4]);
t6=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,t5);
t7=C_a_i_cons(&a,2,lf[76],t6);
t8=C_a_i_list(&a,1,t7);
t9=C_a_i_cons(&a,2,t4,t8);
t10=C_a_i_cons(&a,2,t2,t9);
t11=C_a_i_cons(&a,2,lf[77],t10);
t12=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t12;
av2[1]=C_a_i_list(&a,2,((C_word*)t0)[6],t11);
((C_proc)(void*)(*((C_word*)t12+1)))(2,av2);}}
else{
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_a_i_cons(&a,2,t2,t5);
t7=C_a_i_cons(&a,2,lf[77],t6);
t8=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_a_i_list(&a,2,((C_word*)t0)[6],t7);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k5204 in g845 in k5003 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5206(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5206,c,av);}
/* expand.scm:347: chicken.keyword#string->keyword */
t2=*((C_word*)lf[78]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_slot(t1,C_fix(1));
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k5207 in k5003 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5209(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_5209,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5212,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5214,a[2]=((C_word*)t0)[5],a[3]=t4,a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word)li47),tmp=(C_word)a,a+=7,tmp));
t6=((C_word*)t4)[1];
f_5214(t6,t2,t1);}

/* k5210 in k5207 in k5003 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5212(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_5212,c,av);}
a=C_alloc(9);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],t2);
t4=((C_word*)t0)[4];
f_5009(t4,C_a_i_list(&a,1,t3));}

/* map-loop839 in k5207 in k5003 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_5214(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_5214,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5239,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:368: g845 */
t4=((C_word*)t0)[4];
f_5149(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5237 in map-loop839 in k5207 in k5003 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5239(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5239,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_5214(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k5251 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5253(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5253,c,av);}
/* expand.scm:363: ##sys#append */
t2=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)((C_word*)t0)[3])[1];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k5292 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_5294(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,0,6)))){
C_save_and_reclaim_args((void *)trf_5294,2,t0,t1);}
a=C_alloc(13);
t2=(C_truep(t1)?t1:((C_word*)t0)[2]);
t3=C_u_i_cdr(((C_word*)t0)[3]);
t4=C_eqp(t2,lf[69]);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5307,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=t3,a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
if(C_truep(((C_word*)((C_word*)t0)[9])[1])){
t6=t5;
f_5307(t6,C_SCHEME_UNDEFINED);}
else{
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5323,a[2]=((C_word*)t0)[9],a[3]=t5,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:404: macro-alias */
f_3735(t6,lf[82],((C_word*)t0)[10]);}}
else{
t5=C_eqp(t2,lf[68]);
if(C_truep(t5)){
if(C_truep(C_fixnum_less_or_equal_p(((C_word*)t0)[4],C_fix(1)))){
t6=C_i_pairp(t3);
t7=(C_truep(t6)?C_i_symbolp(C_u_i_car(t3)):C_SCHEME_FALSE);
if(C_truep(t7)){
t8=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5344,a[2]=t3,a[3]=((C_word*)t0)[11],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[12],tmp=(C_word)a,a+=8,tmp);
if(C_truep(C_i_not(((C_word*)((C_word*)t0)[9])[1]))){
t9=C_i_car(t3);
t10=C_mutate(((C_word *)((C_word*)t0)[9])+1,t9);
t11=t8;
f_5344(t11,t10);}
else{
t9=t8;
f_5344(t9,C_SCHEME_UNDEFINED);}}
else{
/* expand.scm:415: err */
t8=((C_word*)t0)[8];
f_4963(t8,((C_word*)t0)[6],lf[83]);}}
else{
/* expand.scm:416: err */
t6=((C_word*)t0)[8];
f_4963(t6,((C_word*)t0)[6],lf[84]);}}
else{
t6=C_eqp(t2,lf[70]);
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_5383,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[12],a[7]=t3,a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
if(C_truep(C_i_not(((C_word*)((C_word*)t0)[9])[1]))){
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5402,a[2]=((C_word*)t0)[9],a[3]=t7,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:418: macro-alias */
f_3735(t8,lf[82],((C_word*)t0)[10]);}
else{
t8=t7;
f_5383(t8,C_SCHEME_UNDEFINED);}}
else{
if(C_truep(C_i_symbolp(((C_word*)t0)[2]))){
switch(((C_word*)t0)[4]){
case C_fix(0):
t7=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[7]);
/* expand.scm:425: loop */
t8=((C_word*)((C_word*)t0)[5])[1];
f_4991(t8,((C_word*)t0)[6],C_fix(0),t7,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,t3);
case C_fix(1):
t7=C_a_i_list2(&a,2,((C_word*)t0)[2],C_SCHEME_FALSE);
t8=C_a_i_cons(&a,2,t7,((C_word*)t0)[12]);
/* expand.scm:426: loop */
t9=((C_word*)((C_word*)t0)[5])[1];
f_4991(t9,((C_word*)t0)[6],C_fix(1),((C_word*)t0)[7],t8,C_SCHEME_END_OF_LIST,t3);
case C_fix(2):
/* expand.scm:427: err */
t7=((C_word*)t0)[8];
f_4963(t7,((C_word*)t0)[6],lf[86]);
default:
t7=C_a_i_list1(&a,1,((C_word*)t0)[2]);
t8=C_a_i_cons(&a,2,t7,((C_word*)t0)[13]);
/* expand.scm:428: loop */
t9=((C_word*)((C_word*)t0)[5])[1];
f_4991(t9,((C_word*)t0)[6],C_fix(3),((C_word*)t0)[7],((C_word*)t0)[12],t8,t3);}}
else{
t7=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_5470,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[8],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[12],a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[7],a[9]=t3,a[10]=((C_word*)t0)[13],tmp=(C_word)a,a+=11,tmp);
if(C_truep(C_i_listp(((C_word*)t0)[2]))){
t8=C_eqp(C_fix(2),C_u_i_length(((C_word*)t0)[2]));
t9=t7;
f_5470(t9,(C_truep(t8)?C_i_symbolp(C_i_car(((C_word*)t0)[2])):C_SCHEME_FALSE));}
else{
t8=t7;
f_5470(t8,C_SCHEME_FALSE);}}}}}}

/* k5305 in k5292 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_5307(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,6)))){
C_save_and_reclaim_args((void *)trf_5307,2,t0,t1);}
t2=C_eqp(((C_word*)t0)[2],C_fix(0));
if(C_truep(t2)){
/* expand.scm:406: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_4991(t3,((C_word*)t0)[4],C_fix(1),((C_word*)t0)[5],C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,((C_word*)t0)[6]);}
else{
/* expand.scm:407: err */
t3=((C_word*)t0)[7];
f_4963(t3,((C_word*)t0)[4],lf[81]);}}

/* k5321 in k5292 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5323(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5323,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=((C_word*)t0)[3];
f_5307(t3,t2);}

/* k5342 in k5292 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_5344(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,6)))){
C_save_and_reclaim_args((void *)trf_5344,2,t0,t1);}
t2=C_i_car(((C_word*)t0)[2]);
t3=C_mutate(((C_word *)((C_word*)t0)[3])+1,t2);
/* expand.scm:414: loop */
t4=((C_word*)((C_word*)t0)[4])[1];
f_4991(t4,((C_word*)t0)[5],C_fix(2),((C_word*)t0)[6],((C_word*)t0)[7],C_SCHEME_END_OF_LIST,C_u_i_cdr(((C_word*)t0)[2]));}

/* k5381 in k5292 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_5383(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,6)))){
C_save_and_reclaim_args((void *)trf_5383,2,t0,t1);}
if(C_truep(C_fixnum_less_or_equal_p(((C_word*)t0)[2],C_fix(2)))){
/* expand.scm:420: loop */
t2=((C_word*)((C_word*)t0)[3])[1];
f_4991(t2,((C_word*)t0)[4],C_fix(3),((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_END_OF_LIST,((C_word*)t0)[7]);}
else{
/* expand.scm:421: err */
t2=((C_word*)t0)[8];
f_4963(t2,((C_word*)t0)[4],lf[85]);}}

/* k5400 in k5292 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5402(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5402,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=((C_word*)t0)[3];
f_5383(t3,t2);}

/* k5468 in k5292 in loop in k4984 in k4981 in k4978 in ##sys#expand-extended-lambda-list in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_5470(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,6)))){
C_save_and_reclaim_args((void *)trf_5470,2,t0,t1);}
a=C_alloc(3);
if(C_truep(t1)){
switch(((C_word*)t0)[2]){
case C_fix(0):
/* expand.scm:431: err */
t2=((C_word*)t0)[3];
f_4963(t2,((C_word*)t0)[4],lf[87]);
case C_fix(1):
t2=C_a_i_cons(&a,2,((C_word*)t0)[5],((C_word*)t0)[6]);
/* expand.scm:432: loop */
t3=((C_word*)((C_word*)t0)[7])[1];
f_4991(t3,((C_word*)t0)[4],C_fix(1),((C_word*)t0)[8],t2,C_SCHEME_END_OF_LIST,((C_word*)t0)[9]);
case C_fix(2):
/* expand.scm:433: err */
t2=((C_word*)t0)[3];
f_4963(t2,((C_word*)t0)[4],lf[88]);
default:
t2=C_a_i_cons(&a,2,((C_word*)t0)[5],((C_word*)t0)[10]);
/* expand.scm:434: loop */
t3=((C_word*)((C_word*)t0)[7])[1];
f_4991(t3,((C_word*)t0)[4],C_fix(3),((C_word*)t0)[8],((C_word*)t0)[6],t2,((C_word*)t0)[9]);}}
else{
/* expand.scm:435: err */
t2=((C_word*)t0)[3];
f_4963(t2,((C_word*)t0)[4],lf[89]);}}

/* ##sys#expand-multiple-values-assignment in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5569(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_5569,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5575,a[2]=t3,a[3]=t2,a[4]=((C_word)li52),tmp=(C_word)a,a+=5,tmp);
/* expand.scm:453: ##sys#decompose-lambda-list */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[98]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[98]+1);
av2[1]=t1;
av2[2]=t2;
av2[3]=t4;
tp(4,av2);}}

/* a5574 in ##sys#expand-multiple-values-assignment in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5575(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(20,c,3)))){
C_save_and_reclaim((void *)f_5575,c,av);}
a=C_alloc(20);
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5579,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=((C_word*)t0)[3],a[6]=t4,tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_i_symbolp(((C_word*)t0)[3]))){
t6=t5;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_END_OF_LIST;
f_5579(2,av2);}}
else{
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=C_i_check_list_2(((C_word*)t0)[3],lf[17]);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5711,a[2]=t8,a[3]=t12,a[4]=t9,a[5]=((C_word)li51),tmp=(C_word)a,a+=6,tmp));
t14=((C_word*)t12)[1];
f_5711(t14,t5,((C_word*)t0)[3]);}}

/* k5577 in a5574 in ##sys#expand-multiple-values-assignment in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5579(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_5579,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5582,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
if(C_truep(C_i_not(((C_word*)t0)[6]))){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
f_5582(2,av2);}}
else{
/* expand.scm:457: chicken.base#gensym */
t3=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k5580 in k5577 in a5574 in ##sys#expand-multiple-values-assignment in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5582(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_5582,c,av);}
a=C_alloc(18);
t2=C_a_i_list(&a,3,lf[76],C_SCHEME_END_OF_LIST,((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_5601,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* expand.scm:461: scheme#append */
t4=*((C_word*)lf[16]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k5599 in k5580 in k5577 in a5574 in ##sys#expand-multiple-values-assignment in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5601(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,4)))){
C_save_and_reclaim((void *)f_5601,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5605,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_i_check_list_2(((C_word*)t0)[4],lf[17]);
t8=C_i_check_list_2(((C_word*)t0)[5],lf[17]);
t9=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5623,a[2]=((C_word*)t0)[6],a[3]=t2,a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],tmp=(C_word)a,a+=6,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5645,a[2]=t5,a[3]=t11,a[4]=t6,a[5]=((C_word)li50),tmp=(C_word)a,a+=6,tmp));
t13=((C_word*)t11)[1];
f_5645(t13,t9,((C_word*)t0)[4],((C_word*)t0)[5]);}

/* k5603 in k5599 in k5580 in k5577 in a5574 in ##sys#expand-multiple-values-assignment in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5605(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,1)))){
C_save_and_reclaim((void *)f_5605,c,av);}
a=C_alloc(15);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,lf[76],t2);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,3,lf[95],((C_word*)t0)[4],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k5621 in k5599 in k5580 in k5577 in a5574 in ##sys#expand-multiple-values-assignment in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5623(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_5623,c,av);}
a=C_alloc(12);
if(C_truep(C_i_nullp(((C_word*)t0)[2]))){
/* expand.scm:458: ##sys#append */
t2=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=lf[96];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
if(C_truep(C_i_nullp(((C_word*)t0)[4]))){
/* expand.scm:458: ##sys#append */
t2=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
t2=C_a_i_list(&a,3,lf[97],((C_word*)t0)[5],((C_word*)t0)[4]);
t3=C_a_i_list(&a,1,t2);
/* expand.scm:458: ##sys#append */
t4=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}}

/* map-loop976 in k5599 in k5580 in k5577 in a5574 in ##sys#expand-multiple-values-assignment in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_5645(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_5645,4,t0,t1,t2,t3);}
a=C_alloc(12);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list(&a,3,lf[97],t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop947 in a5574 in ##sys#expand-multiple-values-assignment in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_5711(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_5711,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5736,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:456: g953 */
t4=*((C_word*)lf[10]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5734 in map-loop947 in a5574 in ##sys#expand-multiple-values-assignment in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5736(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5736,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_5711(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5749(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +5,c,2)))){
C_save_and_reclaim((void*)f_5749,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+5);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5753,a[2]=t3,a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_nullp(t3))){
/* expand.scm:478: ##sys#current-environment */
t5=*((C_word*)lf[4]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_i_car(t3);
f_5753(2,av2);}}}

/* k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5753(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(32,c,6)))){
C_save_and_reclaim((void *)f_5753,c,av);}
a=C_alloc(32);
t2=C_i_nullp(((C_word*)t0)[2]);
t3=(C_truep(t2)?C_SCHEME_END_OF_LIST:C_i_cdr(((C_word*)t0)[2]));
t4=C_i_nullp(t3);
t5=(C_truep(t4)?C_SCHEME_FALSE:C_i_car(t3));
t6=C_i_nullp(t3);
t7=(C_truep(t6)?C_SCHEME_END_OF_LIST:C_i_cdr(t3));
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_SCHEME_UNDEFINED;
t15=(*a=C_VECTOR_TYPE|1,a[1]=t14,tmp=(C_word)a,a+=2,tmp);
t16=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5764,a[2]=t1,a[3]=((C_word)li54),tmp=(C_word)a,a+=4,tmp));
t17=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5847,a[2]=t9,a[3]=t15,a[4]=t1,a[5]=t5,a[6]=((C_word)li61),tmp=(C_word)a,a+=7,tmp));
t18=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6284,a[2]=t11,a[3]=t9,a[4]=((C_word)li64),tmp=(C_word)a,a+=5,tmp));
t19=C_set_block_item(t15,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6440,a[2]=t11,a[3]=t9,a[4]=t1,a[5]=t13,a[6]=t5,a[7]=((C_word)li67),tmp=(C_word)a,a+=8,tmp));
/* expand.scm:635: expand */
t20=((C_word*)t15)[1];
f_6440(t20,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* comp in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static C_word C_fcall f_5764(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_stack_overflow_check;{}
t3=(
/* expand.scm:480: lookup */
  f_3718(t2,((C_word*)t0)[2])
);
t4=C_eqp(t1,t3);
if(C_truep(t4)){
return(t4);}
else{
t5=C_eqp(t1,lf[104]);
if(C_truep(t5)){
return((C_truep(t3)?C_eqp(t3,*((C_word*)lf[99]+1)):C_eqp(t1,t2)));}
else{
t6=C_eqp(t1,lf[105]);
if(C_truep(t6)){
return((C_truep(t3)?C_eqp(t3,*((C_word*)lf[100]+1)):C_eqp(t1,t2)));}
else{
t7=C_eqp(t1,lf[106]);
if(C_truep(t7)){
return((C_truep(t3)?C_eqp(t3,*((C_word*)lf[101]+1)):C_eqp(t1,t2)));}
else{
t8=C_eqp(t1,lf[107]);
return((C_truep(t8)?(C_truep(t3)?C_eqp(t3,lf[102]):C_eqp(t1,t2)):C_eqp(t1,t2)));}}}}}

/* fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_5847(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,4)))){
C_save_and_reclaim_args((void *)trf_5847,6,t0,t1,t2,t3,t4,t5);}
a=C_alloc(14);
t6=C_i_nullp(t2);
t7=(C_truep(t6)?C_i_nullp(t4):C_SCHEME_FALSE);
if(C_truep(t7)){
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5859,a[2]=((C_word*)t0)[2],a[3]=t9,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word)li56),tmp=(C_word)a,a+=8,tmp));
t11=((C_word*)t9)[1];
f_5859(t11,t1,t5,C_SCHEME_END_OF_LIST);}
else{
t8=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t9=t8;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=((C_word*)t10)[1];
t12=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6100,a[2]=t1,a[3]=t5,a[4]=t4,a[5]=t3,a[6]=t2,a[7]=t10,a[8]=t11,tmp=(C_word)a,a+=9,tmp);
/* expand.scm:543: scheme#reverse */
t13=*((C_word*)lf[73]+1);{
C_word av2[3];
av2[0]=t13;
av2[1]=t12;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t13+1)))(3,av2);}}}

/* loop in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_5859(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_5859,4,t0,t1,t2,t3);}
a=C_alloc(12);
t4=C_i_pairp(t2);
if(C_truep(C_i_not(t4))){
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5873,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* expand.scm:495: scheme#reverse */
t6=*((C_word*)lf[73]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_5878,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=t6,a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[6],a[9]=((C_word)li55),tmp=(C_word)a,a+=10,tmp));
t8=((C_word*)t6)[1];
f_5878(t8,t1,t2);}}

/* k5871 in loop in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5873(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_5873,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,lf[108],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* loop2 in loop in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_5878(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,0,2)))){
C_save_and_reclaim_args((void *)trf_5878,3,t0,t1,t2);}
a=C_alloc(13);
t3=C_i_car(t2);
t4=C_u_i_cdr(t2);
t5=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_5889,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=t1,a[7]=t4,a[8]=((C_word*)t0)[5],a[9]=t2,a[10]=((C_word*)t0)[6],a[11]=((C_word*)t0)[7],a[12]=((C_word*)t0)[8],tmp=(C_word)a,a+=13,tmp);
if(C_truep(C_i_pairp(t3))){
t6=C_u_i_car(t3);
if(C_truep(C_i_symbolp(t6))){
t7=(
/* expand.scm:502: comp */
  f_5764(((C_word*)((C_word*)t0)[2])[1],lf[104],t6)
);
if(C_truep(t7)){
t8=t5;
f_5889(t8,t7);}
else{
t8=(
/* expand.scm:503: comp */
  f_5764(((C_word*)((C_word*)t0)[2])[1],lf[106],t6)
);
if(C_truep(t8)){
t9=t5;
f_5889(t9,t8);}
else{
t9=(
/* expand.scm:504: comp */
  f_5764(((C_word*)((C_word*)t0)[2])[1],lf[105],t6)
);
if(C_truep(t9)){
t10=t5;
f_5889(t10,t9);}
else{
t10=(
/* expand.scm:505: comp */
  f_5764(((C_word*)((C_word*)t0)[2])[1],lf[108],t6)
);
t11=t5;
f_5889(t11,(C_truep(t10)?t10:(
/* expand.scm:506: comp */
  f_5764(((C_word*)((C_word*)t0)[2])[1],lf[107],t6)
)));}}}}
else{
t7=t5;
f_5889(t7,C_SCHEME_FALSE);}}
else{
t6=t5;
f_5889(t6,C_SCHEME_FALSE);}}

/* k5887 in loop2 in loop in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_5889(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,4)))){
C_save_and_reclaim_args((void *)trf_5889,2,t0,t1);}
a=C_alloc(9);
if(C_truep(t1)){
t2=(
/* expand.scm:508: comp */
  f_5764(((C_word*)((C_word*)t0)[2])[1],lf[107],C_i_car(((C_word*)t0)[3]))
);
if(C_truep(t2)){
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]);
/* expand.scm:509: loop */
t4=((C_word*)((C_word*)t0)[5])[1];
f_5859(t4,((C_word*)t0)[6],((C_word*)t0)[7],t3);}
else{
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5909,a[2]=((C_word*)t0)[6],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5913,a[2]=t3,a[3]=((C_word*)t0)[8],a[4]=((C_word*)t0)[9],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:512: scheme#reverse */
t5=*((C_word*)lf[73]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}}
else{
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_5928,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[10],tmp=(C_word)a,a+=9,tmp);
/* expand.scm:513: ##sys#expand-0 */
t3=*((C_word*)lf[36]+1);{
C_word av2[5];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[11];
av2[4]=((C_word*)t0)[12];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}

/* k5907 in k5887 in loop2 in loop in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5909(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_5909,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,lf[108],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k5911 in k5887 in loop2 in loop in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5913(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_5913,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5921,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:512: expand */
t3=((C_word*)((C_word*)t0)[3])[1];
f_6440(t3,t2,((C_word*)t0)[4]);}

/* k5919 in k5911 in k5887 in loop2 in loop in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5921(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_5921,c,av);}
a=C_alloc(3);
t2=C_a_i_list1(&a,1,t1);
/* expand.scm:512: ##sys#append */
t3=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5926 in k5887 in loop2 in loop in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5928(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_5928,c,av);}
a=C_alloc(8);
t2=C_eqp(((C_word*)t0)[2],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5940,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
if(C_truep(C_i_pairp(((C_word*)t0)[2]))){
if(C_truep(C_i_symbolp(C_u_i_car(((C_word*)t0)[2])))){
t4=(
/* expand.scm:520: comp */
  f_5764(((C_word*)((C_word*)t0)[4])[1],lf[109],C_u_i_car(((C_word*)t0)[2]))
);
t5=t3;
f_5940(t5,(C_truep(t4)?t4:(
/* expand.scm:521: comp */
  f_5764(((C_word*)((C_word*)t0)[4])[1],lf[110],C_u_i_car(((C_word*)t0)[2]))
)));}
else{
t4=t3;
f_5940(t4,C_SCHEME_FALSE);}}
else{
t4=t3;
f_5940(t4,C_SCHEME_FALSE);}}
else{
t3=C_a_i_cons(&a,2,t1,((C_word*)t0)[5]);
/* expand.scm:530: loop2 */
t4=((C_word*)((C_word*)t0)[8])[1];
f_5878(t4,((C_word*)t0)[3],t3);}}

/* k5938 in k5926 in k5887 in loop2 in loop in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_5940(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_5940,2,t0,t1);}
a=C_alloc(9);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5947,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5951,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:523: scheme#reverse */
t4=*((C_word*)lf[73]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t2=C_a_i_cons(&a,2,((C_word*)t0)[4],((C_word*)t0)[6]);
/* expand.scm:529: loop */
t3=((C_word*)((C_word*)t0)[7])[1];
f_5859(t3,((C_word*)t0)[2],((C_word*)t0)[5],t2);}}

/* k5945 in k5938 in k5926 in k5887 in loop2 in loop in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5947(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_5947,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,lf[108],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k5949 in k5938 in k5926 in k5887 in loop2 in loop in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5951(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_5951,c,av);}
a=C_alloc(12);
t2=(
/* expand.scm:524: comp */
  f_5764(((C_word*)((C_word*)t0)[2])[1],lf[109],C_i_car(((C_word*)t0)[3]))
);
if(C_truep(t2)){
if(C_truep(C_i_nullp(((C_word*)t0)[4]))){
t3=C_a_i_list(&a,1,((C_word*)t0)[3]);
/* expand.scm:522: ##sys#append */
t4=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[5];
av2[2]=t1;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t3=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,((C_word*)t0)[4]);
t4=C_a_i_cons(&a,2,lf[55],t3);
t5=C_a_i_list(&a,2,((C_word*)t0)[3],t4);
/* expand.scm:522: ##sys#append */
t6=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=((C_word*)t0)[5];
av2[2]=t1;
av2[3]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5989,a[2]=((C_word*)t0)[5],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t4=C_u_i_cdr(((C_word*)t0)[3]);
t5=C_a_i_list(&a,1,((C_word*)t0)[4]);
/* expand.scm:528: ##sys#append */
t6=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=t4;
av2[3]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}}

/* k5987 in k5949 in k5938 in k5926 in k5887 in loop2 in loop in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_5989(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_5989,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,lf[110],t1);
t3=C_a_i_list(&a,1,t2);
/* expand.scm:522: ##sys#append */
t4=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k6098 in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6100(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,4)))){
C_save_and_reclaim((void *)f_6100,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6103,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6242,a[2]=t4,a[3]=((C_word)li60),tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_6242(t6,t2,t1,C_SCHEME_END_OF_LIST);}

/* k6101 in k6098 in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6103(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_6103,c,av);}
a=C_alloc(15);
t2=C_i_check_list_2(t1,lf[17]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6109,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6208,a[2]=((C_word*)t0)[7],a[3]=t5,a[4]=((C_word*)t0)[8],a[5]=((C_word)li58),tmp=(C_word)a,a+=6,tmp));
t7=((C_word*)t5)[1];
f_6208(t7,t3,t1);}

/* k6107 in k6101 in k6098 in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6109(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,2)))){
C_save_and_reclaim((void *)f_6109,c,av);}
a=C_alloc(17);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6113,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6135,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=t5,a[5]=t6,a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],tmp=(C_word)a,a+=8,tmp);
/* expand.scm:551: scheme#reverse */
t8=*((C_word*)lf[73]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}

/* k6111 in k6107 in k6101 in k6098 in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6113(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_6113,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[55],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k6133 in k6107 in k6101 in k6098 in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6135(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_6135,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6138,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* expand.scm:552: scheme#reverse */
t3=*((C_word*)lf[73]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6136 in k6133 in k6107 in k6101 in k6098 in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6138(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_6138,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6141,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,tmp=(C_word)a,a+=8,tmp);
/* expand.scm:553: scheme#reverse */
t3=*((C_word*)lf[73]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6139 in k6136 in k6133 in k6107 in k6101 in k6098 in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6141(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,5)))){
C_save_and_reclaim((void *)f_6141,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6144,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6146,a[2]=((C_word*)t0)[4],a[3]=t4,a[4]=((C_word*)t0)[5],a[5]=((C_word)li57),tmp=(C_word)a,a+=6,tmp));
t6=((C_word*)t4)[1];
f_6146(t6,t2,((C_word*)t0)[6],((C_word*)t0)[7],t1);}

/* k6142 in k6139 in k6136 in k6133 in k6107 in k6101 in k6098 in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6144(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6144,c,av);}
/* expand.scm:535: ##sys#append */
t2=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop1155 in k6139 in k6136 in k6133 in k6107 in k6101 in k6098 in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6146(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_6146,5,t0,t1,t2,t3,t4);}
a=C_alloc(9);
t5=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6153,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,a[6]=t3,a[7]=t4,a[8]=((C_word*)t0)[4],tmp=(C_word)a,a+=9,tmp);
if(C_truep(C_i_pairp(t2))){
t6=C_i_pairp(t3);
t7=t5;
f_6153(t7,(C_truep(t6)?C_i_pairp(t4):C_SCHEME_FALSE));}
else{
t6=t5;
f_6153(t6,C_SCHEME_FALSE);}}

/* k6151 in map-loop1155 in k6139 in k6136 in k6133 in k6107 in k6101 in k6098 in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6153(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,0,3)))){
C_save_and_reclaim_args((void *)trf_6153,2,t0,t1);}
a=C_alloc(17);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6179,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
t3=C_slot(((C_word*)t0)[5],C_fix(0));
t4=C_slot(((C_word*)t0)[6],C_fix(0));
if(C_truep(C_slot(((C_word*)t0)[7],C_fix(0)))){
/* expand.scm:549: ##sys#expand-multiple-values-assignment */
t5=*((C_word*)lf[94]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=t2;
av2[2]=t3;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
t5=C_i_car(t3);
t6=t2;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_a_i_list(&a,3,lf[97],t5,t4);
f_6179(2,av2);}}}
else{
t2=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_slot(((C_word*)t0)[8],C_fix(1));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k6177 in k6151 in map-loop1155 in k6139 in k6136 in k6133 in k6107 in k6101 in k6098 in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6179(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_6179,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_6146(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)),C_slot(((C_word*)t0)[6],C_fix(1)),C_slot(((C_word*)t0)[7],C_fix(1)));}

/* map-loop1107 in k6101 in k6098 in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6208(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_6208,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_a_i_list(&a,2,t3,lf[111]);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* foldl1130 in k6098 in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6242(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,5)))){
C_save_and_reclaim_args((void *)trf_6242,4,t0,t1,t2,t3);}
a=C_alloc(12);
if(C_truep(C_i_pairp(t2))){
t4=C_slot(t2,C_fix(1));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6272,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t6=C_slot(t2,C_fix(0));
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6266,a[2]=t5,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6268,a[2]=((C_word)li59),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:540: ##sys#decompose-lambda-list */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[98]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[98]+1);
av2[1]=t7;
av2[2]=t6;
av2[3]=t8;
tp(4,av2);}}
else{
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6264 in foldl1130 in k6098 in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6266(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6266,c,av);}
/* expand.scm:540: ##sys#append */
t2=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* a6267 in foldl1130 in k6098 in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6268(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6268,c,av);}
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k6270 in foldl1130 in k6098 in fini in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6272(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6272,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6242(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* fini/syntax in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6284(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,5)))){
C_save_and_reclaim_args((void *)trf_6284,6,t0,t1,t2,t3,t4,t5);}
a=C_alloc(14);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6292,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=t3,a[6]=t4,tmp=(C_word)a,a+=7,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6294,a[2]=t8,a[3]=((C_word*)t0)[3],a[4]=((C_word)li63),tmp=(C_word)a,a+=5,tmp));
t10=((C_word*)t8)[1];
f_6294(t10,t6,t5,C_SCHEME_END_OF_LIST,C_SCHEME_FALSE);}

/* k6290 in fini/syntax in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6292(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6292,c,av);}
/* expand.scm:558: fini */
t2=((C_word*)((C_word*)t0)[2])[1];
f_5847(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],t1);}

/* loop in fini/syntax in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6294(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(11,0,4)))){
C_save_and_reclaim_args((void *)trf_6294,5,t0,t1,t2,t3,t4);}
a=C_alloc(11);
if(C_truep(t4)){
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6316,a[2]=t2,a[3]=t1,a[4]=t7,a[5]=t8,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:562: scheme#reverse */
t10=*((C_word*)lf[73]+1);{
C_word av2[3];
av2[0]=t10;
av2[1]=t9;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}
else{
t5=C_i_pairp(t2);
if(C_truep(C_i_not(t5))){
/* expand.scm:563: loop */
t12=t1;
t13=t2;
t14=t3;
t15=C_SCHEME_TRUE;
t1=t12;
t2=t13;
t3=t14;
t4=t15;
goto loop;}
else{
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6368,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
t7=C_i_car(t2);
if(C_truep(C_i_listp(t7))){
t8=C_i_length(C_u_i_car(t2));
if(C_truep(C_fixnum_greater_or_equal_p(C_fix(3),t8))){
t9=C_i_caar(t2);
if(C_truep(C_i_symbolp(t9))){
t10=C_u_i_car(t2);
/* expand.scm:567: comp */
t11=t6;
f_6368(t11,(
/* expand.scm:567: comp */
  f_5764(((C_word*)((C_word*)t0)[3])[1],lf[105],C_u_i_car(t10))
));}
else{
t10=t6;
f_6368(t10,C_SCHEME_FALSE);}}
else{
t9=t6;
f_6368(t9,C_SCHEME_FALSE);}}
else{
t8=t6;
f_6368(t8,C_SCHEME_FALSE);}}}}

/* k6314 in loop in fini/syntax in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6316(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_6316,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6319,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6321,a[2]=((C_word*)t0)[4],a[3]=t4,a[4]=((C_word*)t0)[5],a[5]=((C_word)li62),tmp=(C_word)a,a+=6,tmp));
t6=((C_word*)t4)[1];
f_6321(t6,t2,t1);}

/* k6317 in k6314 in loop in fini/syntax in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6319(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_6319,c,av);}
a=C_alloc(9);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,lf[112],t2);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,1,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* map-loop1215 in k6314 in loop in fini/syntax in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6321(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_6321,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_cdr(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6366 in loop in fini/syntax in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6368(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,4)))){
C_save_and_reclaim_args((void *)trf_6368,2,t0,t1);}
a=C_alloc(10);
if(C_truep(t1)){
t2=C_u_i_car(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6372,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t4=C_i_car(t2);
t5=C_i_cadr(t2);
t6=C_eqp(t4,t5);
if(C_truep(t6)){
/* expand.scm:443: ##sys#syntax-error-hook */
t7=*((C_word*)lf[46]+1);{
C_word av2[4];
av2[0]=t7;
av2[1]=t3;
av2[2]=lf[113];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
t7=C_u_i_cdr(((C_word*)t0)[2]);
t8=C_a_i_cons(&a,2,t2,((C_word*)t0)[3]);
/* expand.scm:573: loop */
t9=((C_word*)((C_word*)t0)[4])[1];
f_6294(t9,((C_word*)t0)[5],t7,t8,C_SCHEME_FALSE);}}
else{
/* expand.scm:574: loop */
t2=((C_word*)((C_word*)t0)[4])[1];
f_6294(t2,((C_word*)t0)[5],((C_word*)t0)[2],((C_word*)t0)[3],C_SCHEME_TRUE);}}

/* k6370 in k6366 in loop in fini/syntax in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6372(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_6372,c,av);}
a=C_alloc(3);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]);
/* expand.scm:573: loop */
t4=((C_word*)((C_word*)t0)[5])[1];
f_6294(t4,((C_word*)t0)[6],t2,t3,C_SCHEME_FALSE);}

/* expand in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6440(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,6)))){
C_save_and_reclaim_args((void *)trf_6440,3,t0,t1,t2);}
a=C_alloc(11);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6446,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t4,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word)li66),tmp=(C_word)a,a+=9,tmp));
t6=((C_word*)t4)[1];
f_6446(t6,t1,t2,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST);}

/* loop in expand in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6446(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,5)))){
C_save_and_reclaim_args((void *)trf_6446,6,t0,t1,t2,t3,t4,t5);}
a=C_alloc(15);
t6=C_i_pairp(t2);
if(C_truep(C_i_not(t6))){
/* expand.scm:582: fini */
t7=((C_word*)((C_word*)t0)[2])[1];
f_5847(t7,t1,t3,t4,t5,t2);}
else{
t7=C_i_car(t2);
t8=C_u_i_cdr(t2);
t9=C_i_pairp(t7);
t10=(C_truep(t9)?C_u_i_car(t7):C_SCHEME_FALSE);
t11=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_6466,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t3,a[5]=t4,a[6]=t5,a[7]=t2,a[8]=((C_word*)t0)[3],a[9]=((C_word*)t0)[4],a[10]=t8,a[11]=((C_word*)t0)[5],a[12]=t7,a[13]=((C_word*)t0)[6],a[14]=((C_word*)t0)[7],tmp=(C_word)a,a+=15,tmp);
if(C_truep(t10)){
t12=C_i_symbolp(t10);
t13=t11;
f_6466(t13,(C_truep(t12)?t10:C_SCHEME_FALSE));}
else{
t12=t11;
f_6466(t12,C_SCHEME_FALSE);}}}

/* k6464 in loop in expand in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6466(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,6)))){
C_save_and_reclaim_args((void *)trf_6466,2,t0,t1);}
a=C_alloc(14);
t2=C_i_symbolp(t1);
if(C_truep(C_i_not(t2))){
/* expand.scm:588: fini */
t3=((C_word*)((C_word*)t0)[2])[1];
f_5847(t3,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7]);}
else{
t3=(
/* expand.scm:590: comp */
  f_5764(((C_word*)((C_word*)t0)[8])[1],lf[104],t1)
);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6484,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[9],a[6]=((C_word*)t0)[10],a[7]=((C_word*)t0)[11],a[8]=((C_word*)t0)[3],a[9]=((C_word*)t0)[12],tmp=(C_word)a,a+=10,tmp);
/* expand.scm:591: ##sys#check-syntax */
t5=*((C_word*)lf[59]+1);{
C_word av2[7];
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[104];
av2[3]=((C_word*)t0)[12];
av2[4]=lf[119];
av2[5]=C_SCHEME_FALSE;
av2[6]=((C_word*)t0)[11];
((C_proc)(void*)(*((C_word*)t5+1)))(7,av2);}}
else{
t4=(
/* expand.scm:617: comp */
  f_5764(((C_word*)((C_word*)t0)[8])[1],lf[105],t1)
);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6621,a[2]=((C_word*)t0)[13],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* expand.scm:618: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word av2[6];
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[105];
av2[3]=((C_word*)t0)[12];
av2[4]=lf[120];
av2[5]=((C_word*)t0)[11];
((C_proc)(void*)(*((C_word*)t6+1)))(6,av2);}}
else{
t5=(
/* expand.scm:620: comp */
  f_5764(((C_word*)((C_word*)t0)[8])[1],lf[106],t1)
);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6633,a[2]=((C_word*)t0)[12],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[10],tmp=(C_word)a,a+=9,tmp);
/* expand.scm:622: ##sys#check-syntax */
t7=*((C_word*)lf[59]+1);{
C_word av2[7];
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[106];
av2[3]=((C_word*)t0)[12];
av2[4]=lf[121];
av2[5]=C_SCHEME_FALSE;
av2[6]=((C_word*)t0)[11];
((C_proc)(void*)(*((C_word*)t7+1)))(7,av2);}}
else{
t6=(
/* expand.scm:624: comp */
  f_5764(((C_word*)((C_word*)t0)[8])[1],lf[108],t1)
);
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6669,a[2]=((C_word*)t0)[9],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* expand.scm:625: ##sys#append */
t8=*((C_word*)lf[74]+1);{
C_word av2[4];
av2[0]=t8;
av2[1]=t7;
av2[2]=C_i_cdr(((C_word*)t0)[12]);
av2[3]=((C_word*)t0)[10];
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}
else{
t7=C_a_i_list1(&a,1,t1);
if(C_truep(C_i_member(t7,((C_word*)t0)[4]))){
/* expand.scm:630: fini */
t8=((C_word*)((C_word*)t0)[2])[1];
f_5847(t8,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7]);}
else{
t8=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_6685,a[2]=((C_word*)t0)[12],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* expand.scm:631: ##sys#expand-0 */
t9=*((C_word*)lf[36]+1);{
C_word av2[5];
av2[0]=t9;
av2[1]=t8;
av2[2]=((C_word*)t0)[12];
av2[3]=((C_word*)t0)[11];
av2[4]=((C_word*)t0)[14];
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}}}}}}}

/* k6482 in k6464 in loop in expand in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6484(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_6484,c,av);}
a=C_alloc(12);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6489,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t3,a[9]=((C_word)li65),tmp=(C_word)a,a+=10,tmp));
t5=((C_word*)t3)[1];
f_6489(t5,((C_word*)t0)[8],((C_word*)t0)[9]);}

/* loop2 in k6482 in k6464 in loop in expand in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6489(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,6)))){
C_save_and_reclaim_args((void *)trf_6489,3,t0,t1,t2);}
a=C_alloc(10);
t3=C_i_cadr(t2);
t4=C_i_pairp(t3);
if(C_truep(C_i_not(t4))){
t5=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6502,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=t2,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=t1,a[9]=((C_word*)t0)[6],tmp=(C_word)a,a+=10,tmp);
/* expand.scm:595: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word av2[7];
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[104];
av2[3]=t2;
av2[4]=lf[115];
av2[5]=C_SCHEME_FALSE;
av2[6]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t6+1)))(7,av2);}}
else{
t5=C_i_car(t3);
if(C_truep(C_i_pairp(t5))){
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6555,a[2]=((C_word*)t0)[8],a[3]=t1,a[4]=t3,a[5]=t2,a[6]=((C_word*)t0)[7],tmp=(C_word)a,a+=7,tmp);
/* expand.scm:605: ##sys#check-syntax */
t7=*((C_word*)lf[59]+1);{
C_word av2[7];
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[104];
av2[3]=t2;
av2[4]=lf[117];
av2[5]=C_SCHEME_FALSE;
av2[6]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t7+1)))(7,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6569,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=t2,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=t1,a[9]=((C_word*)t0)[6],tmp=(C_word)a,a+=10,tmp);
/* expand.scm:610: ##sys#check-syntax */
t7=*((C_word*)lf[59]+1);{
C_word av2[7];
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[104];
av2[3]=t2;
av2[4]=lf[118];
av2[5]=C_SCHEME_FALSE;
av2[6]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t7+1)))(7,av2);}}}}

/* k6500 in loop2 in k6482 in k6464 in loop in expand in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6502(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_6502,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6505,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
t3=C_eqp(C_u_i_car(((C_word*)t0)[4]),((C_word*)t0)[2]);
if(C_truep(t3)){
/* expand.scm:443: ##sys#syntax-error-hook */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[113];
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_6505(2,av2);}}}

/* k6503 in k6500 in loop2 in k6482 in k6464 in loop in expand in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6505(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,5)))){
C_save_and_reclaim((void *)f_6505,c,av);}
a=C_alloc(12);
t2=C_a_i_list1(&a,1,((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,t2,((C_word*)t0)[3]);
t4=C_i_cddr(((C_word*)t0)[4]);
if(C_truep(C_i_pairp(t4))){
t5=C_i_caddr(((C_word*)t0)[4]);
t6=C_a_i_cons(&a,2,t5,((C_word*)t0)[5]);
t7=C_a_i_cons(&a,2,C_SCHEME_FALSE,((C_word*)t0)[6]);
/* expand.scm:598: loop */
t8=((C_word*)((C_word*)t0)[7])[1];
f_6446(t8,((C_word*)t0)[8],((C_word*)t0)[9],t3,t6,t7);}
else{
t5=C_a_i_cons(&a,2,lf[114],((C_word*)t0)[5]);
t6=C_a_i_cons(&a,2,C_SCHEME_FALSE,((C_word*)t0)[6]);
/* expand.scm:598: loop */
t7=((C_word*)((C_word*)t0)[7])[1];
f_6446(t7,((C_word*)t0)[8],((C_word*)t0)[9],t3,t5,t6);}}

/* k6553 in loop2 in k6482 in k6464 in loop in expand in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6555(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_6555,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6562,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:608: chicken.syntax#expand-curried-define */
t3=*((C_word*)lf[116]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_i_cddr(((C_word*)t0)[5]);
av2[4]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6560 in k6553 in loop2 in k6482 in k6464 in loop in expand in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6562(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6562,c,av);}
/* expand.scm:607: loop2 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_6489(t2,((C_word*)t0)[3],t1);}

/* k6567 in loop2 in k6482 in k6464 in loop in expand in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6569(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,5)))){
C_save_and_reclaim((void *)f_6569,c,av);}
a=C_alloc(18);
t2=C_u_i_car(((C_word*)t0)[2]);
t3=C_a_i_list1(&a,1,t2);
t4=C_a_i_cons(&a,2,t3,((C_word*)t0)[3]);
t5=C_u_i_cdr(((C_word*)t0)[2]);
t6=C_i_cddr(((C_word*)t0)[4]);
t7=C_a_i_cons(&a,2,t5,t6);
t8=C_a_i_cons(&a,2,lf[76],t7);
t9=C_a_i_cons(&a,2,t8,((C_word*)t0)[5]);
t10=C_a_i_cons(&a,2,C_SCHEME_FALSE,((C_word*)t0)[6]);
/* expand.scm:613: loop */
t11=((C_word*)((C_word*)t0)[7])[1];
f_6446(t11,((C_word*)t0)[8],((C_word*)t0)[9],t4,t9,t10);}

/* k6619 in k6464 in loop in expand in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6621(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6621,c,av);}
/* expand.scm:619: fini/syntax */
t2=((C_word*)((C_word*)t0)[2])[1];
f_6284(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7]);}

/* k6631 in k6464 in loop in expand in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6633(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_6633,c,av);}
a=C_alloc(9);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,t2,((C_word*)t0)[3]);
t4=C_i_caddr(((C_word*)t0)[2]);
t5=C_a_i_cons(&a,2,t4,((C_word*)t0)[4]);
t6=C_a_i_cons(&a,2,C_SCHEME_TRUE,((C_word*)t0)[5]);
/* expand.scm:623: loop */
t7=((C_word*)((C_word*)t0)[6])[1];
f_6446(t7,((C_word*)t0)[7],((C_word*)t0)[8],t3,t5,t6);}

/* k6667 in k6464 in loop in expand in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6669(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6669,c,av);}
/* expand.scm:625: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_6446(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6]);}

/* k6683 in k6464 in loop in expand in k5751 in ##sys#canonicalize-body in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6685(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_6685,c,av);}
a=C_alloc(3);
t2=C_eqp(((C_word*)t0)[2],t1);
if(C_truep(t2)){
/* expand.scm:633: fini */
t3=((C_word*)((C_word*)t0)[3])[1];
f_5847(t3,((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7],((C_word*)t0)[8]);}
else{
t3=C_a_i_cons(&a,2,t1,((C_word*)t0)[9]);
/* expand.scm:634: loop */
t4=((C_word*)((C_word*)t0)[10])[1];
f_6446(t4,((C_word*)t0)[4],t3,((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7]);}}

/* chicken.syntax#match-expression in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6755(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(14,c,4)))){
C_save_and_reclaim((void *)f_6755,c,av);}
a=C_alloc(14);
t5=C_SCHEME_END_OF_LIST;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6758,a[2]=t6,a[3]=t4,a[4]=t8,a[5]=((C_word)li70),tmp=(C_word)a,a+=6,tmp));
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6835,a[2]=t6,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:655: mwalk */
t11=((C_word*)t8)[1];
f_6758(t11,t10,t2,t3);}

/* mwalk in chicken.syntax#match-expression in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6758(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_6758,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=C_i_pairp(t3);
if(C_truep(C_i_not(t4))){
t5=C_i_assq(t3,((C_word*)((C_word*)t0)[2])[1]);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6772,a[2]=t2,a[3]=((C_word)li69),tmp=(C_word)a,a+=4,tmp);
/* expand.scm:646: g1305 */
t7=t1;{
C_word av2[2];
av2[0]=t7;
av2[1]=(
/* expand.scm:646: g1305 */
  f_6772(t6,t5)
);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
if(C_truep(C_i_memq(t3,((C_word*)t0)[3]))){
t6=C_a_i_cons(&a,2,t3,t2);
t7=C_a_i_cons(&a,2,t6,((C_word*)((C_word*)t0)[2])[1]);
t8=C_mutate(((C_word *)((C_word*)t0)[2])+1,t7);
t9=t1;{
C_word av2[2];
av2[0]=t9;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_eqp(t2,t3);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}}
else{
if(C_truep(C_i_pairp(t2))){
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6812,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:652: mwalk */
t10=t5;
t11=C_u_i_car(t2);
t12=C_i_car(t3);
t1=t10;
t2=t11;
t3=t12;
goto loop;}
else{
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}}

/* g1305 in mwalk in chicken.syntax#match-expression in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static C_word C_fcall f_6772(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_equalp(((C_word*)t0)[2],C_i_cdr(t1)));}

/* k6810 in mwalk in chicken.syntax#match-expression in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6812(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6812,c,av);}
if(C_truep(t1)){
/* expand.scm:653: mwalk */
t2=((C_word*)((C_word*)t0)[2])[1];
f_6758(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]),C_u_i_cdr(((C_word*)t0)[5]));}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k6833 in chicken.syntax#match-expression in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6835(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6835,c,av);}
if(C_truep(t1)){
t2=((C_word*)((C_word*)t0)[2])[1];
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* chicken.syntax#expand-curried-define in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6837(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_6837,c,av);}
a=C_alloc(13);
t5=C_SCHEME_FALSE;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6840,a[2]=t6,a[3]=t8,a[4]=((C_word)li72),tmp=(C_word)a,a+=5,tmp));
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6884,a[2]=t1,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:669: loop */
t11=((C_word*)t8)[1];
f_6840(t11,t10,t2,t3);}

/* loop in chicken.syntax#expand-curried-define in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6840(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_6840,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=C_i_car(t2);
if(C_truep(C_i_symbolp(t4))){
t5=C_mutate(((C_word *)((C_word*)t0)[2])+1,C_u_i_car(t2));
t6=C_u_i_cdr(t2);
t7=C_a_i_cons(&a,2,t6,t3);
t8=t1;{
C_word av2[2];
av2[0]=t8;
av2[1]=C_a_i_cons(&a,2,lf[76],t7);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t5=C_u_i_car(t2);
t6=C_u_i_cdr(t2);
t7=C_a_i_cons(&a,2,t6,t3);
t8=C_a_i_cons(&a,2,lf[76],t7);
t9=C_a_i_list(&a,1,t8);
/* expand.scm:668: loop */
t11=t1;
t12=t5;
t13=t9;
t1=t11;
t2=t12;
t3=t13;
goto loop;}}

/* k6882 in chicken.syntax#expand-curried-define in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6884(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_6884,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list3(&a,3,lf[104],((C_word*)((C_word*)t0)[3])[1],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.syntax#syntax-error in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6892(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +3,c,2)))){
C_save_and_reclaim((void*)f_6892,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+3);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6900,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* expand.scm:681: chicken.syntax#strip-syntax */
t4=*((C_word*)lf[12]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k6898 in chicken.syntax#syntax-error in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6900(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6900,c,av);}{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[127]+1);
av2[3]=lf[128];
av2[4]=t1;
C_apply(5,av2);}}

/* ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6903(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6903,c,av);}
a=C_alloc(5);
if(C_truep(C_i_nullp(*((C_word*)lf[125]+1)))){
/* expand.scm:694: ##sys#syntax-error-hook */
t4=*((C_word*)lf[46]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6970,a[2]=t1,a[3]=t3,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:695: chicken.base#open-output-string */
t5=*((C_word*)lf[147]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k6912 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6914(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_6914,c,av);}
a=C_alloc(6);
t2=(C_truep(t1)?t1:C_SCHEME_END_OF_LIST);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6919,a[2]=t4,a[3]=((C_word)li77),tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_6919(t6,((C_word*)t0)[2],t2);}

/* loop in k6912 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6919(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_6919,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_caar(t2);
t4=C_eqp(lf[144],t3);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6939,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:691: scheme#cadar */
t6=*((C_word*)lf[72]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
/* expand.scm:692: loop */
t7=t1;
t8=C_u_i_cdr(t2);
t1=t7;
t2=t8;
goto loop;}}}

/* k6937 in loop in k6912 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6939(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6939,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6943,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:691: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_6919(t3,t2,C_u_i_cdr(((C_word*)t0)[4]));}

/* k6941 in k6937 in loop in k6912 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6943(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_6943,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k6956 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6958(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6958,c,av);}
/* expand.scm:688: ##sys#get */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[145]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[145]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[146];
tp(4,av2);}}

/* k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6970(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_6970,c,av);}
a=C_alloc(18);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6972,a[2]=t1,a[3]=((C_word)li75),tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6979,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6988,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=t5,a[7]=((C_word)li78),tmp=(C_word)a,a+=8,tmp));
t7=((C_word*)t5)[1];
f_6988(t7,t3,*((C_word*)lf[125]+1));}

/* outstr in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6972(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_6972,3,t0,t1,t2);}
/* expand.scm:697: ##sys#print */
t3=*((C_word*)lf[130]+1);{
C_word av2[5];
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6977 in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6979(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6979,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6986,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* expand.scm:732: chicken.base#get-output-string */
t3=*((C_word*)lf[131]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6984 in k6977 in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6986(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6986,c,av);}
/* expand.scm:732: ##sys#syntax-error-hook */
t2=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_6988(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_6988,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_nullp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6998,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:700: outstr */
t4=((C_word*)t0)[2];
f_6972(t4,t3,((C_word*)t0)[5]);}
else{
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7024,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=t2,tmp=(C_word)a,a+=9,tmp);
/* expand.scm:707: chicken.syntax#strip-syntax */
t4=*((C_word*)lf[12]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_car(t2);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k6996 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_6998(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6998,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7001,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:701: outstr */
t3=((C_word*)t0)[2];
f_6972(t3,t2,lf[134]);}

/* k6999 in k6996 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7001(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_7001,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7004,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:702: ##sys#print */
t3=*((C_word*)lf[130]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k7002 in k6999 in k6996 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7004(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_7004,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7007,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:703: outstr */
t3=((C_word*)t0)[2];
f_6972(t3,t2,lf[133]);}

/* k7005 in k7002 in k6999 in k6996 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7007(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7007,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7010,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7017,a[2]=t2,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:704: chicken.syntax#strip-syntax */
t4=*((C_word*)lf[12]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_car(*((C_word*)lf[125]+1));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k7008 in k7005 in k7002 in k6999 in k6996 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7010(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7010,c,av);}
/* expand.scm:705: outstr */
t2=((C_word*)t0)[2];
f_6972(t2,((C_word*)t0)[3],lf[132]);}

/* k7015 in k7005 in k7002 in k6999 in k6996 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7017(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7017,c,av);}
/* expand.scm:704: ##sys#print */
t2=*((C_word*)lf[130]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7024(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_7024,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7027,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6914,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6958,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* expand.scm:688: chicken.syntax#strip-syntax */
t5=*((C_word*)lf[12]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k7025 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7027(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7027,c,av);}
a=C_alloc(8);
if(C_truep(C_i_pairp(t1))){
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7036,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* expand.scm:710: outstr */
t3=((C_word*)t0)[2];
f_6972(t3,t2,((C_word*)t0)[7]);}
else{
/* expand.scm:731: loop */
t2=((C_word*)((C_word*)t0)[8])[1];
f_6988(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[9]));}}

/* k7034 in k7025 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7036(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7036,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7039,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* expand.scm:711: outstr */
t3=((C_word*)t0)[3];
f_6972(t3,t2,lf[143]);}

/* k7037 in k7034 in k7025 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7039(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_7039,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7042,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* expand.scm:712: ##sys#print */
t3=*((C_word*)lf[130]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k7040 in k7037 in k7034 in k7025 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7042(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_7042,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7045,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* expand.scm:713: outstr */
t3=((C_word*)t0)[3];
f_6972(t3,t2,lf[142]);}

/* k7043 in k7040 in k7037 in k7034 in k7025 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7045(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_7045,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7048,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:714: ##sys#print */
t3=*((C_word*)lf[130]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k7046 in k7043 in k7040 in k7037 in k7034 in k7025 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7048(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_7048,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7051,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:715: outstr */
t3=((C_word*)t0)[3];
f_6972(t3,t2,lf[141]);}

/* k7049 in k7046 in k7043 in k7040 in k7037 in k7034 in k7025 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7051(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_7051,c,av);}
a=C_alloc(13);
t2=C_i_length(((C_word*)t0)[2]);
t3=C_eqp(C_fix(1),t2);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7064,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7068,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
/* expand.scm:720: scheme#symbol->string */
t6=*((C_word*)lf[49]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=C_i_car(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7079,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7083,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7085,a[2]=t7,a[3]=((C_word)li76),tmp=(C_word)a,a+=4,tmp));
t9=((C_word*)t7)[1];
f_7085(t9,t5,((C_word*)t0)[2]);}}

/* k7062 in k7049 in k7046 in k7043 in k7040 in k7037 in k7034 in k7025 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7064(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7064,c,av);}
/* expand.scm:717: outstr */
t2=((C_word*)t0)[2];
f_6972(t2,((C_word*)t0)[3],t1);}

/* k7066 in k7049 in k7046 in k7043 in k7040 in k7037 in k7034 in k7025 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7068(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7068,c,av);}
/* expand.scm:718: scheme#string-append */
t2=*((C_word*)lf[41]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[135];
av2[3]=t1;
av2[4]=lf[136];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k7077 in k7049 in k7046 in k7043 in k7040 in k7037 in k7034 in k7025 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7079(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7079,c,av);}
/* expand.scm:722: outstr */
t2=((C_word*)t0)[2];
f_6972(t2,((C_word*)t0)[3],t1);}

/* k7081 in k7049 in k7046 in k7043 in k7040 in k7037 in k7034 in k7025 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7083(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7083,c,av);}
/* expand.scm:723: scheme#string-append */
t2=*((C_word*)lf[41]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[137];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* loop in k7049 in k7046 in k7043 in k7040 in k7037 in k7034 in k7025 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_7085(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_7085,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=lf[138];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7099,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:729: scheme#symbol->string */
t4=*((C_word*)lf[49]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_car(t2);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k7097 in loop in k7049 in k7046 in k7043 in k7040 in k7037 in k7034 in k7025 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7099(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7099,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7103,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:730: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_7085(t3,t2,C_u_i_cdr(((C_word*)t0)[4]));}

/* k7101 in k7097 in loop in k7049 in k7046 in k7043 in k7040 in k7037 in k7034 in k7025 in k7022 in loop in k6968 in ##sys#syntax-error/context in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7103(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_7103,c,av);}
/* expand.scm:728: scheme#string-append */
t2=*((C_word*)lf[41]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[139];
av2[3]=((C_word*)t0)[3];
av2[4]=lf[140];
av2[5]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* chicken.syntax#get-line-number in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7124(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_7124,c,av);}
a=C_alloc(4);
if(C_truep(*((C_word*)lf[123]+1))){
if(C_truep(C_i_pairp(t2))){
t3=C_u_i_car(t2);
if(C_truep(C_i_symbolp(t3))){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7144,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:739: chicken.internal#hash-table-ref */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[149]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[149]+1);
av2[1]=t4;
av2[2]=*((C_word*)lf[123]+1);
av2[3]=t3;
tp(4,av2);}}
else{
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}
else{
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}
else{
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7142 in chicken.syntax#get-line-number in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7144(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7144,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7148,a[2]=((C_word*)t0)[2],a[3]=((C_word)li80),tmp=(C_word)a,a+=4,tmp);
/* expand.scm:739: g1394 */
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(
/* expand.scm:739: g1394 */
  f_7148(t2,t1)
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* g1394 in k7142 in chicken.syntax#get-line-number in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static C_word C_fcall f_7148(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_stack_overflow_check;{}
t2=C_i_assq(((C_word*)t0)[2],t1);
return((C_truep(t2)?C_i_cdr(t2):C_SCHEME_FALSE));}

/* ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7163(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c<5) C_bad_min_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-5)*C_SIZEOF_PAIR +8,c,2)))){
C_save_and_reclaim((void*)f_7163,c,av);}
a=C_alloc((c-5)*C_SIZEOF_PAIR+8);
t5=C_build_rest(&a,c,5,av);
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
t6=C_rest_nullp(c,5);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,5,av,5,t0));
t8=C_rest_nullp(c,5);
t9=(C_truep(t8)?C_SCHEME_END_OF_LIST:C_i_cdr(t5));
t10=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7173,a[2]=t9,a[3]=t3,a[4]=t2,a[5]=t1,a[6]=t4,a[7]=t7,tmp=(C_word)a,a+=8,tmp);
if(C_truep(C_i_nullp(t9))){
/* expand.scm:748: ##sys#current-environment */
t11=*((C_word*)lf[4]+1);{
C_word *av2=av;
av2[0]=t11;
av2[1]=t10;
((C_proc)(void*)(*((C_word*)t11+1)))(2,av2);}}
else{
t11=t10;{
C_word *av2=av;
av2[0]=t11;
av2[1]=C_i_car(t9);
f_7173(2,av2);}}}

/* k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7173(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(39,c,5)))){
C_save_and_reclaim((void *)f_7173,c,av);}
a=C_alloc(39);
t2=C_i_nullp(((C_word*)t0)[2]);
t3=(C_truep(t2)?C_SCHEME_END_OF_LIST:C_i_cdr(((C_word*)t0)[2]));
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7178,a[2]=t7,a[3]=((C_word)li82),tmp=(C_word)a,a+=4,tmp));
t15=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7190,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word)li83),tmp=(C_word)a,a+=5,tmp));
t16=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7217,a[2]=((C_word)li85),tmp=(C_word)a,a+=3,tmp));
t17=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7262,a[2]=((C_word)li86),tmp=(C_word)a,a+=3,tmp));
t18=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7268,a[2]=((C_word)li88),tmp=(C_word)a,a+=3,tmp));
t19=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_7295,a[2]=t7,a[3]=t5,a[4]=t11,a[5]=t13,a[6]=t9,a[7]=t1,a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[3],a[10]=((C_word*)t0)[6],tmp=(C_word)a,a+=11,tmp);
if(C_truep(((C_word*)t0)[7])){
t20=C_mutate((C_word*)lf[124]+1 /* (set! ##sys#syntax-error-culprit ...) */,((C_word*)t0)[7]);
t21=t19;
f_7295(t21,t20);}
else{
t20=t19;
f_7295(t20,C_SCHEME_UNDEFINED);}}

/* test in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_7178(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_7178,5,t0,t1,t2,t3,t4);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7185,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:751: pred */
t6=t3;{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
((C_proc)C_fast_retrieve_proc(t6))(3,av2);}}

/* k7183 in test in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7185(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7185,c,av);}
if(C_truep(t1)){
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
/* expand.scm:751: err */
t2=((C_word*)((C_word*)t0)[3])[1];
f_7190(t2,((C_word*)t0)[2],((C_word*)t0)[4]);}}

/* err in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_7190(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7190,3,t0,t1,t2);}
a=C_alloc(6);
t3=*((C_word*)lf[124]+1);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7194,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:755: get-line-number */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[148]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[148]+1);
av2[1]=t4;
av2[2]=*((C_word*)lf[124]+1);
tp(3,av2);}}

/* k7192 in err in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7194(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_7194,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7201,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(t1)){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7208,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:758: scheme#symbol->string */
t4=*((C_word*)lf[49]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7215,a[2]=t2,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:759: scheme#symbol->string */
t4=*((C_word*)lf[49]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k7199 in k7192 in err in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7201(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7201,c,av);}
/* expand.scm:756: ##sys#syntax-error-hook */
t2=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k7206 in k7192 in err in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7208(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_7208,c,av);}
/* expand.scm:758: scheme#string-append */
t2=*((C_word*)lf[41]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[150];
av2[3]=((C_word*)t0)[3];
av2[4]=lf[151];
av2[5]=t1;
av2[6]=lf[152];
av2[7]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(8,av2);}}

/* k7213 in k7192 in err in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7215(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_7215,c,av);}
/* expand.scm:759: scheme#string-append */
t2=*((C_word*)lf[41]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[153];
av2[3]=t1;
av2[4]=lf[154];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* lambda-list? in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7217(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7217,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7221,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:763: ##sys#extended-lambda-list? */
t4=*((C_word*)lf[67]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k7219 in lambda-list? in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7221(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7221,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7229,a[2]=((C_word)li84),tmp=(C_word)a,a+=3,tmp);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(
  f_7229(((C_word*)t0)[3])
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* loop in k7219 in lambda-list? in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static C_word C_fcall f_7229(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
t2=C_i_nullp(t1);
if(C_truep(t2)){
return(t2);}
else{
if(C_truep(C_i_symbolp(t1))){
return(C_SCHEME_TRUE);}
else{
if(C_truep(C_i_pairp(t1))){
t3=C_u_i_car(t1);
if(C_truep(C_i_symbolp(t3))){
t5=C_u_i_cdr(t1);
t1=t5;
goto loop;}
else{
return(C_SCHEME_FALSE);}}
else{
return(C_SCHEME_FALSE);}}}}

/* variable? in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7262(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7262,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_symbolp(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* proper-list? in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7268(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7268,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7274,a[2]=((C_word)li87),tmp=(C_word)a,a+=3,tmp);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=(
  f_7274(t2)
);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* loop in proper-list? in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static C_word C_fcall f_7274(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
t2=C_eqp(t1,C_SCHEME_END_OF_LIST);
if(C_truep(t2)){
return(t2);}
else{
if(C_truep(C_i_pairp(t1))){
t4=C_u_i_cdr(t1);
t1=t4;
goto loop;}
else{
return(C_SCHEME_FALSE);}}}

/* k7293 in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_7295(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,4)))){
C_save_and_reclaim_args((void *)trf_7295,2,t0,t1);}
a=C_alloc(12);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7300,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word)li91),tmp=(C_word)a,a+=10,tmp));
t5=((C_word*)t3)[1];
f_7300(t5,((C_word*)t0)[8],((C_word*)t0)[9],((C_word*)t0)[10]);}

/* walk in k7293 in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_7300(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(8,0,4)))){
C_save_and_reclaim_args((void *)trf_7300,4,t0,t1,t2,t3);}
a=C_alloc(8);
if(C_truep(C_i_vectorp(t3))){
t4=C_i_vector_ref(t3,C_fix(0));
t5=C_block_size(t3);
t6=C_fixnum_greaterp(t5,C_fix(1));
t7=(C_truep(t6)?C_i_vector_ref(t3,C_fix(1)):C_fix(0));
t8=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7319,a[2]=t7,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=t4,a[6]=t1,a[7]=t2,tmp=(C_word)a,a+=8,tmp);
t9=C_eqp(t5,C_fix(1));
if(C_truep(t9)){
t10=t8;
f_7319(t10,C_fix(1));}
else{
t10=C_fixnum_greaterp(t5,C_fix(2));
t11=t8;
f_7319(t11,(C_truep(t10)?C_i_vector_ref(t3,C_fix(2)):C_fix(99999)));}}
else{
if(C_truep(C_immp(t3))){
t4=C_eqp(t3,t2);
if(C_truep(C_i_not(t4))){
/* expand.scm:804: err */
t5=((C_word*)((C_word*)t0)[2])[1];
f_7190(t5,t1,lf[158]);}
else{
t5=C_SCHEME_UNDEFINED;
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}
else{
if(C_truep(C_i_symbolp(t3))){
t4=C_eqp(t3,lf[159]);
if(C_truep(t4)){
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_eqp(t3,lf[160]);
if(C_truep(t5)){
/* expand.scm:808: test */
t6=((C_word*)((C_word*)t0)[4])[1];
f_7178(t6,t1,t2,*((C_word*)lf[161]+1),lf[162]);}
else{
t6=C_eqp(t3,lf[163]);
if(C_truep(t6)){
/* expand.scm:809: test */
t7=((C_word*)((C_word*)t0)[4])[1];
f_7178(t7,t1,t2,((C_word*)((C_word*)t0)[5])[1],lf[164]);}
else{
t7=C_eqp(t3,lf[165]);
if(C_truep(t7)){
/* expand.scm:810: test */
t8=((C_word*)((C_word*)t0)[4])[1];
f_7178(t8,t1,t2,*((C_word*)lf[166]+1),lf[167]);}
else{
t8=C_eqp(t3,lf[168]);
if(C_truep(t8)){
/* expand.scm:811: test */
t9=((C_word*)((C_word*)t0)[4])[1];
f_7178(t9,t1,t2,((C_word*)((C_word*)t0)[6])[1],lf[169]);}
else{
t9=C_eqp(t3,lf[170]);
if(C_truep(t9)){
/* expand.scm:812: test */
t10=((C_word*)((C_word*)t0)[4])[1];
f_7178(t10,t1,t2,*((C_word*)lf[171]+1),lf[172]);}
else{
t10=C_eqp(t3,lf[173]);
if(C_truep(t10)){
/* expand.scm:813: test */
t11=((C_word*)((C_word*)t0)[4])[1];
f_7178(t11,t1,t2,*((C_word*)lf[174]+1),lf[175]);}
else{
t11=C_eqp(t3,lf[176]);
if(C_truep(t11)){
/* expand.scm:814: test */
t12=((C_word*)((C_word*)t0)[4])[1];
f_7178(t12,t1,t2,((C_word*)((C_word*)t0)[7])[1],lf[177]);}
else{
t12=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7497,a[2]=((C_word*)t0)[8],a[3]=t3,a[4]=((C_word)li90),tmp=(C_word)a,a+=5,tmp);
/* expand.scm:816: test */
t13=((C_word*)((C_word*)t0)[4])[1];
f_7178(t13,t1,t2,t12,lf[178]);}}}}}}}}}
else{
t4=C_i_pairp(t3);
if(C_truep(C_i_not(t4))){
/* expand.scm:823: err */
t5=((C_word*)((C_word*)t0)[2])[1];
f_7190(t5,t1,lf[179]);}
else{
t5=C_i_pairp(t2);
if(C_truep(C_i_not(t5))){
/* expand.scm:824: err */
t6=((C_word*)((C_word*)t0)[2])[1];
f_7190(t6,t1,lf[180]);}
else{
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7554,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:826: walk */
t14=t6;
t15=C_i_car(t2);
t16=C_i_car(t3);
t1=t14;
t2=t15;
t3=t16;
goto loop;}}}}}}

/* k7317 in walk in k7293 in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_7319(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,4)))){
C_save_and_reclaim_args((void *)trf_7319,2,t0,t1);}
a=C_alloc(11);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7324,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t3,a[5]=t1,a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=((C_word)li89),tmp=(C_word)a,a+=9,tmp));
t5=((C_word*)t3)[1];
f_7324(t5,((C_word*)t0)[6],((C_word*)t0)[7],C_fix(0));}

/* doloop1473 in k7317 in walk in k7293 in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_7324(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_7324,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=C_eqp(t2,C_SCHEME_END_OF_LIST);
if(C_truep(t4)){
if(C_truep(C_fixnum_lessp(t3,((C_word*)t0)[2]))){
/* expand.scm:797: err */
t5=((C_word*)((C_word*)t0)[3])[1];
f_7190(t5,t1,lf[155]);}
else{
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}
else{
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7343,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_fixnum_greater_or_equal_p(t3,((C_word*)t0)[5]))){
/* expand.scm:799: err */
t6=((C_word*)((C_word*)t0)[3])[1];
f_7190(t6,t5,lf[156]);}
else{
t6=C_i_pairp(t2);
if(C_truep(C_i_not(t6))){
/* expand.scm:801: err */
t7=((C_word*)((C_word*)t0)[3])[1];
f_7190(t7,t5,lf[157]);}
else{
/* expand.scm:802: walk */
t7=((C_word*)((C_word*)t0)[6])[1];
f_7300(t7,t5,C_i_car(t2),((C_word*)t0)[7]);}}}}

/* k7341 in doloop1473 in k7317 in walk in k7293 in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7343(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7343,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7324(t2,((C_word*)t0)[3],C_i_cdr(((C_word*)t0)[4]),C_fixnum_plus(((C_word*)t0)[5],C_fix(1)));}

/* a7496 in walk in k7293 in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7497(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7497,c,av);}
if(C_truep(C_i_symbolp(t2))){
t3=(
/* expand.scm:819: lookup */
  f_3718(t2,((C_word*)t0)[2])
);
t4=C_i_symbolp(t3);
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=(C_truep(t4)?C_eqp(t3,((C_word*)t0)[3]):C_eqp(t2,((C_word*)t0)[3]));
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t3=C_i_symbolp(C_SCHEME_FALSE);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=(C_truep(t3)?C_eqp(C_SCHEME_FALSE,((C_word*)t0)[3]):C_eqp(t2,((C_word*)t0)[3]));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7552 in walk in k7293 in k7171 in ##sys#check-syntax in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7554(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7554,c,av);}
/* expand.scm:827: walk */
t2=((C_word*)((C_word*)t0)[2])[1];
f_7300(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]),C_u_i_cdr(((C_word*)t0)[5]));}

/* chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_7607(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,5)))){
C_save_and_reclaim_args((void *)trf_7607,3,t1,t2,t3);}
a=C_alloc(8);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7613,a[2]=t3,a[3]=t2,a[4]=((C_word)li100),tmp=(C_word)a,a+=5,tmp);
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_a_i_record2(&a,2,lf[30],t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7613(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_7613,c,av);}
a=C_alloc(11);
t5=C_SCHEME_END_OF_LIST;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_i_listp(t3);
t8=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7620,a[2]=t6,a[3]=t3,a[4]=t4,a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[3],a[7]=t1,a[8]=t2,tmp=(C_word)a,a+=9,tmp);
if(C_truep(t7)){
t9=t8;{
C_word *av2=av;
av2[0]=t9;
av2[1]=t7;
f_7620(2,av2);}}
else{
/* expand.scm:837: ##sys#error */
t9=*((C_word*)lf[31]+1);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
av2[2]=lf[184];
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t9+1)))(4,av2);}}}

/* k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7620(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(40,c,4)))){
C_save_and_reclaim((void *)f_7620,c,av);}
a=C_alloc(40);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7622,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word)li93),tmp=(C_word)a,a+=6,tmp));
t11=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7710,a[2]=t5,a[3]=((C_word*)t0)[4],a[4]=((C_word)li97),tmp=(C_word)a,a+=5,tmp));
t12=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7932,a[2]=((C_word)li98),tmp=(C_word)a,a+=3,tmp));
t13=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7957,a[2]=t9,a[3]=((C_word*)t0)[3],a[4]=t7,a[5]=((C_word*)t0)[2],a[6]=t3,a[7]=((C_word)li99),tmp=(C_word)a,a+=8,tmp));
if(C_truep(((C_word*)t0)[5])){
/* expand.scm:927: handler */
t14=((C_word*)t0)[6];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t14;
av2[1]=((C_word*)t0)[7];
av2[2]=((C_word*)t0)[8];
av2[3]=((C_word*)t3)[1];
av2[4]=((C_word*)t5)[1];
((C_proc)C_fast_retrieve_proc(t14))(5,av2);}}
else{
t14=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8091,a[2]=t9,a[3]=((C_word*)t0)[7],tmp=(C_word)a,a+=4,tmp);
t15=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8095,a[2]=((C_word*)t0)[6],a[3]=t14,a[4]=t3,a[5]=t5,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:933: rename */
t16=((C_word*)t3)[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t16;
av2[1]=t15;
av2[2]=((C_word*)t0)[8];
f_7622(3,av2);}}}

/* rename in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7622(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
av[0]=t0;
av[1]=t1;
av[2]=t2;
C_save_and_reclaim((void *)f_7622,c,av);}
a=C_alloc(7);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7636,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:840: rename */
t7=t3;
t8=C_u_i_car(t2);
t1=t7;
t2=t8;
c=3;
goto loop;}
else{
if(C_truep(C_i_vectorp(t2))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7657,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7661,a[2]=((C_word*)t0)[2],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:842: scheme#vector->list */
t5=*((C_word*)lf[183]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t3=C_i_symbolp(t2);
t4=C_i_not(t3);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7670,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t4)){
t6=t5;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t4;
f_7670(2,av2);}}
else{
/* expand.scm:843: chicken.keyword#keyword? */
t6=*((C_word*)lf[11]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}}}}

/* k7634 in rename in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7636(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7636,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7640,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:840: rename */
t3=((C_word*)((C_word*)t0)[3])[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_u_i_cdr(((C_word*)t0)[4]);
f_7622(3,av2);}}

/* k7638 in k7634 in rename in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7640(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_7640,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7655 in rename in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7657,c,av);}
/* expand.scm:842: scheme#list->vector */
t2=*((C_word*)lf[182]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7659 in rename in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7661(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7661,c,av);}
/* expand.scm:842: rename */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
f_7622(3,av2);}}

/* k7668 in rename in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7670(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_7670,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_i_assq(((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);
if(C_truep(t2)){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_cdr(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7690,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:849: macro-alias */
f_3735(t3,((C_word*)t0)[3],((C_word*)t0)[5]);}}}

/* k7688 in k7668 in rename in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7690(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_7690,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,t2,((C_word*)((C_word*)t0)[3])[1]);
t4=C_mutate(((C_word *)((C_word*)t0)[3])+1,t3);
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* compare in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7710(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
loop:
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
av[0]=t0;
av[1]=t1;
av[2]=t2;
av[3]=t3;
C_save_and_reclaim((void *)f_7710,c,av);}
a=C_alloc(10);
if(C_truep(C_i_pairp(t2))){
if(C_truep(C_i_pairp(t3))){
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7732,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:857: compare */
t11=t4;
t12=C_u_i_car(t2);
t13=C_u_i_car(t3);
t1=t11;
t2=t12;
t3=t13;
c=4;
goto loop;}
else{
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}
else{
if(C_truep(C_i_vectorp(t2))){
if(C_truep(C_i_vectorp(t3))){
t4=C_block_size(t2);
t5=C_block_size(t3);
t6=C_eqp(t4,t5);
if(C_truep(t6)){
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7769,a[2]=t4,a[3]=t8,a[4]=((C_word*)t0)[2],a[5]=t2,a[6]=t3,a[7]=((C_word)li94),tmp=(C_word)a,a+=8,tmp));
t10=((C_word*)t8)[1];
f_7769(t10,t1,C_fix(0),C_SCHEME_TRUE);}
else{
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}
else{
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7813,a[2]=t2,a[3]=t3,a[4]=t1,a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_symbolp(t2))){
t5=C_i_symbolp(t3);
t6=t4;
f_7813(t6,(C_truep(t5)?C_SCHEME_TRUE:C_SCHEME_FALSE));}
else{
t5=t4;
f_7813(t5,C_SCHEME_FALSE);}}}}

/* k7730 in compare in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7732(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7732,c,av);}
if(C_truep(t1)){
/* expand.scm:858: compare */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=C_u_i_cdr(((C_word*)t0)[4]);
av2[3]=C_u_i_cdr(((C_word*)t0)[5]);
f_7710(4,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* doloop1547 in compare in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_7769(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_7769,4,t0,t1,t2,t3);}
a=C_alloc(5);
t4=C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]);
t5=(C_truep(t4)?t4:C_i_not(t3));
if(C_truep(t5)){
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_fixnum_plus(t2,C_fix(1));
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7790,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t6,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:864: compare */
t8=((C_word*)((C_word*)t0)[4])[1];{
C_word av2[4];
av2[0]=t8;
av2[1]=t7;
av2[2]=C_i_vector_ref(((C_word*)t0)[5],t2);
av2[3]=C_i_vector_ref(((C_word*)t0)[6],t2);
f_7710(4,av2);}}}

/* k7788 in doloop1547 in compare in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7790(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7790,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7769(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k7811 in compare in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_7813(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_7813,2,t0,t1);}
a=C_alloc(5);
if(C_truep(t1)){
t2=C_i_getprop(((C_word*)t0)[2],lf[7],C_SCHEME_FALSE);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7821,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t2)){
t4=t3;
f_7821(t4,t2);}
else{
t4=(
/* expand.scm:888: lookup */
  f_3718(((C_word*)t0)[2],((C_word*)t0)[5])
);
t5=t3;
f_7821(t5,(C_truep(t4)?t4:((C_word*)t0)[2]));}}
else{
t2=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_eqp(((C_word*)t0)[2],((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k7819 in k7811 in compare in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_7821(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_7821,2,t0,t1);}
a=C_alloc(4);
t2=C_i_getprop(((C_word*)t0)[2],lf[7],C_SCHEME_FALSE);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7829,a[2]=t1,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(t2)){
t4=t3;
f_7829(t4,t2);}
else{
t4=(
/* expand.scm:888: lookup */
  f_3718(((C_word*)t0)[2],((C_word*)t0)[4])
);
t5=t3;
f_7829(t5,(C_truep(t4)?t4:((C_word*)t0)[2]));}}

/* k7827 in k7819 in k7811 in compare in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_7829(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_7829,2,t0,t1);}
a=C_alloc(5);
if(C_truep(C_i_symbolp(((C_word*)t0)[2]))){
if(C_truep(C_i_symbolp(t1))){
t2=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_eqp(((C_word*)t0)[2],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7866,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:876: ##sys#macro-environment */
t3=*((C_word*)lf[20]+1);{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}
else{
if(C_truep(C_i_symbolp(t1))){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7894,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:880: ##sys#macro-environment */
t3=*((C_word*)lf[20]+1);{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_eqp(((C_word*)t0)[2],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}}

/* g1590 in k7864 in k7827 in k7819 in k7811 in compare in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static C_word C_fcall f_7851(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_eqp(C_i_cdr(t1),((C_word*)t0)[2]));}

/* k7864 in k7827 in k7819 in k7811 in compare in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7866(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7866,c,av);}
a=C_alloc(4);
t2=C_i_assq(((C_word*)t0)[2],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7851,a[2]=((C_word*)t0)[3],a[3]=((C_word)li95),tmp=(C_word)a,a+=4,tmp);
/* expand.scm:875: g1590 */
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=(
/* expand.scm:875: g1590 */
  f_7851(t3,t2)
);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* g1599 in k7892 in k7827 in k7819 in k7811 in compare in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static C_word C_fcall f_7879(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_eqp(((C_word*)t0)[2],C_i_cdr(t1)));}

/* k7892 in k7827 in k7819 in k7811 in compare in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7894(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7894,c,av);}
a=C_alloc(4);
t2=C_i_assq(((C_word*)t0)[2],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7879,a[2]=((C_word*)t0)[3],a[3]=((C_word)li96),tmp=(C_word)a,a+=4,tmp);
/* expand.scm:880: g1599 */
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=(
/* expand.scm:880: g1599 */
  f_7879(t3,t2)
);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* assq-reverse in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static C_word C_fcall f_7932(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_nullp(t2))){
return(C_SCHEME_FALSE);}
else{
t3=C_i_cdar(t2);
t4=C_eqp(t3,t1);
if(C_truep(t4)){
return(C_u_i_car(t2));}
else{
t6=t1;
t7=C_u_i_cdr(t2);
t1=t6;
t2=t7;
goto loop;}}}

/* mirror-rename in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_fcall f_7957(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_7957,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7971,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:902: mirror-rename */
t7=t3;
t8=C_u_i_car(t2);
t1=t7;
t2=t8;
goto loop;}
else{
if(C_truep(C_i_vectorp(t2))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7992,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7996,a[2]=((C_word*)t0)[2],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:904: scheme#vector->list */
t5=*((C_word*)lf[183]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t3=C_i_symbolp(t2);
t4=C_i_not(t3);
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8005,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
if(C_truep(t4)){
t6=t5;{
C_word av2[2];
av2[0]=t6;
av2[1]=t4;
f_8005(2,av2);}}
else{
/* expand.scm:905: chicken.keyword#keyword? */
t6=*((C_word*)lf[11]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}}}}

/* k7969 in mirror-rename in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7971(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7971,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7975,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:902: mirror-rename */
t3=((C_word*)((C_word*)t0)[3])[1];
f_7957(t3,t2,C_u_i_cdr(((C_word*)t0)[4]));}

/* k7973 in k7969 in mirror-rename in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7975(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_7975,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7990 in mirror-rename in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7992(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7992,c,av);}
/* expand.scm:904: scheme#list->vector */
t2=*((C_word*)lf[182]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7994 in mirror-rename in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_7996(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7996,c,av);}
/* expand.scm:904: mirror-rename */
t2=((C_word*)((C_word*)t0)[2])[1];
f_7957(t2,((C_word*)t0)[3],t1);}

/* k8003 in mirror-rename in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8005(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8005,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(
/* expand.scm:907: lookup */
  f_3718(((C_word*)t0)[3],((C_word*)t0)[4])
);
t3=(
/* expand.scm:908: assq-reverse */
  f_7932(((C_word*)t0)[3],((C_word*)((C_word*)t0)[6])[1])
);
if(C_truep(t3)){
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_car(t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
if(C_truep(C_i_not(t2))){
/* expand.scm:912: rename */
t4=((C_word*)((C_word*)t0)[7])[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
f_7622(3,av2);}}
else{
if(C_truep(C_i_pairp(t2))){
/* expand.scm:914: rename */
t4=((C_word*)((C_word*)t0)[7])[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
f_7622(3,av2);}}
else{
t4=C_i_getprop(((C_word*)t0)[3],lf[9],C_SCHEME_FALSE);
if(C_truep(t4)){
t5=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
/* expand.scm:921: rename */
t5=((C_word*)((C_word*)t0)[7])[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
f_7622(3,av2);}}}}}}}

/* k8089 in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8091(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8091,c,av);}
/* expand.scm:933: mirror-rename */
t2=((C_word*)((C_word*)t0)[2])[1];
f_7957(t2,((C_word*)t0)[3],t1);}

/* k8093 in k7618 in a7612 in chicken.syntax#make-er/ir-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8095(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8095,c,av);}
/* expand.scm:933: handler */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=((C_word*)((C_word*)t0)[4])[1];
av2[4]=((C_word*)((C_word*)t0)[5])[1];
((C_proc)C_fast_retrieve_proc(t2))(5,av2);}}

/* chicken.syntax#er-macro-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8100(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8100,c,av);}
/* expand.scm:935: make-er/ir-transformer */
f_7607(t1,t2,C_SCHEME_TRUE);}

/* chicken.syntax#ir-macro-transformer in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8106(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8106,c,av);}
/* expand.scm:936: make-er/ir-transformer */
f_7607(t1,t2,C_SCHEME_FALSE);}

/* k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8119(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_8119,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8122,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12159,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12161,a[2]=((C_word)li179),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:965: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8122(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_8122,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8126,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12044,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12046,a[2]=((C_word)li178),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:973: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8126(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_8126,c,av);}
a=C_alloc(9);
t2=C_mutate(&lf[102] /* (set! chicken.syntax#import-definition ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8129,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12009,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12011,a[2]=((C_word)li173),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:995: ##sys#er-transformer */
t6=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t4;
av2[2]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8129(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_8129,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8132,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11716,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11718,a[2]=((C_word)li172),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1004: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8132(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8132,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8136,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1056: ##sys#macro-environment */
t3=*((C_word*)lf[20]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8136(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_8136,c,av);}
a=C_alloc(9);
t2=C_mutate((C_word*)lf[189]+1 /* (set! ##sys#initial-macro-environment ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8139,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11541,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11543,a[2]=((C_word)li167),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1060: ##sys#er-transformer */
t6=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t4;
av2[2]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8139(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_8139,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8142,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11514,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11516,a[2]=((C_word)li166),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1120: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8142(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_8142,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8145,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11504,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11506,a[2]=((C_word)li165),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1130: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8145(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_8145,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8148,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11333,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11335,a[2]=((C_word)li164),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1139: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8148(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_8148,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8151,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11249,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11251,a[2]=((C_word)li161),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1174: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8151(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_8151,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8154,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11226,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11228,a[2]=((C_word)li160),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1197: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8154(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8154,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8158,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1204: ##sys#macro-environment */
t3=*((C_word*)lf[20]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8158(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8158,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[190]+1 /* (set! ##sys#chicken.module-macro-environment ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8162,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1207: ##sys#macro-environment */
t4=*((C_word*)lf[20]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8162(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8162,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8165,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11209,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11211,a[2]=((C_word)li159),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1212: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 */
static void C_ccall f_8165(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8165,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8168,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11192,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11194,a[2]=((C_word)li158),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1220: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in k3698 in ... */
static void C_ccall f_8168(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8168,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8171,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11175,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11177,a[2]=((C_word)li157),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1228: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in k3701 in ... */
static void C_ccall f_8171(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8171,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8174,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11158,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11160,a[2]=((C_word)li156),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1236: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in k3705 in ... */
static void C_ccall f_8174(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8174,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8178,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11031,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11033,a[2]=((C_word)li155),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1245: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in k3710 in ... */
static void C_ccall f_8178(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8178,c,av);}
a=C_alloc(10);
t2=C_mutate((C_word*)lf[99]+1 /* (set! chicken.syntax#define-definition ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8182,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10984,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10986,a[2]=((C_word)li153),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1273: ##sys#er-transformer */
t6=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t4;
av2[2]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in k3714 in ... */
static void C_ccall f_8182(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8182,c,av);}
a=C_alloc(10);
t2=C_mutate((C_word*)lf[100]+1 /* (set! chicken.syntax#define-syntax-definition ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8185,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10932,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10934,a[2]=((C_word)li152),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1287: ##sys#er-transformer */
t6=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t4;
av2[2]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in k4186 in ... */
static void C_ccall f_8185(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8185,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8188,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10910,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10912,a[2]=((C_word)li151),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1300: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in k8117 in ... */
static void C_ccall f_8188(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8188,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8191,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10888,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10890,a[2]=((C_word)li150),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1309: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in k8120 in ... */
static void C_ccall f_8191(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8191,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8194,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10866,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10868,a[2]=((C_word)li149),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1318: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in k8124 in ... */
static void C_ccall f_8194(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8194,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8197,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10822,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10824,a[2]=((C_word)li148),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1327: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in k8127 in ... */
static void C_ccall f_8197(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8197,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8200,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10785,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10787,a[2]=((C_word)li147),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1339: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in k8130 in ... */
static void C_ccall f_8200(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8200,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8203,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10733,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10735,a[2]=((C_word)li146),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1353: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in k8134 in ... */
static void C_ccall f_8203(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8203,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8206,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10350,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10352,a[2]=((C_word)li145),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1369: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in k8137 in ... */
static void C_ccall f_8206(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8206,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8209,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10114,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10116,a[2]=((C_word)li143),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1434: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in k8140 in ... */
static void C_ccall f_8209(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8209,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8212,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10063,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10065,a[2]=((C_word)li139),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1479: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in k8143 in ... */
static void C_ccall f_8212(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8212,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8215,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9867,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9869,a[2]=((C_word)li137),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1492: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in ... */
static void C_ccall f_8215(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8215,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8218,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9574,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9576,a[2]=((C_word)li134),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1521: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in ... */
static void C_ccall f_8218(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8218,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8221,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9541,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9543,a[2]=((C_word)li128),tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1573: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in ... */
static void C_ccall f_8221(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8221,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8224,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9505,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9507,a[2]=((C_word)li127),tmp=(C_word)a,a+=3,tmp);
/* synrules.scm:46: ##sys#er-transformer */
t5=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8222 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in ... */
static void C_ccall f_8224(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,4)))){
C_save_and_reclaim((void *)f_8224,c,av);}
a=C_alloc(20);
t2=C_a_i_provide(&a,1,lf[191]);
t3=C_mutate((C_word*)lf[50]+1 /* (set! chicken.internal.syntax-rules#syntax-rules-mismatch ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8226,a[2]=((C_word)li104),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[193]+1 /* (set! chicken.internal.syntax-rules#drop-right ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8232,a[2]=((C_word)li106),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[194]+1 /* (set! chicken.internal.syntax-rules#take-right ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8268,a[2]=((C_word)li108),tmp=(C_word)a,a+=3,tmp));
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9485,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1584: chicken.internal#macro-subset */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[198]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[198]+1);
av2[1]=t6;
av2[2]=((C_word*)t0)[3];
tp(3,av2);}}

/* chicken.internal.syntax-rules#syntax-rules-mismatch in k8222 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in ... */
static void C_ccall f_8226(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8226,c,av);}
/* synrules.scm:68: ##sys#syntax-error-hook */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=lf[192];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* chicken.internal.syntax-rules#drop-right in k8222 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in ... */
static void C_ccall f_8232(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_8232,c,av);}
a=C_alloc(7);
t4=C_i_length(t2);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8242,a[2]=t3,a[3]=t6,a[4]=((C_word)li105),tmp=(C_word)a,a+=5,tmp));
t8=((C_word*)t6)[1];
f_8242(t8,t1,t4,t2);}

/* loop in chicken.internal.syntax-rules#drop-right in k8222 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_fcall f_8242(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_8242,4,t0,t1,t2,t3);}
a=C_alloc(4);
if(C_truep(C_fixnum_greaterp(t2,((C_word*)t0)[2]))){
t4=C_i_car(t3);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8260,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* synrules.scm:77: loop */
t7=t5;
t8=C_fixnum_difference(t2,C_fix(1));
t9=C_u_i_cdr(t3);
t1=t7;
t2=t8;
t3=t9;
goto loop;}
else{
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k8258 in loop in chicken.internal.syntax-rules#drop-right in k8222 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in ... */
static void C_ccall f_8260(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_8260,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.internal.syntax-rules#take-right in k8222 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in ... */
static void C_ccall f_8268(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_8268,c,av);}
a=C_alloc(4);
t4=C_i_length(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8278,a[2]=t3,a[3]=((C_word)li107),tmp=(C_word)a,a+=4,tmp);
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=(
  f_8278(t5,t4,t2)
);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* loop in chicken.internal.syntax-rules#take-right in k8222 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static C_word C_fcall f_8278(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
if(C_truep(C_fixnum_greaterp(t1,((C_word*)t0)[2]))){
t4=C_fixnum_difference(t1,C_fix(1));
t5=C_i_cdr(t2);
t1=t4;
t2=t5;
goto loop;}
else{
return(t2);}}

/* k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in ... */
static void C_ccall f_8305(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(56,c,2)))){
C_save_and_reclaim((void *)f_8305,c,av);}
a=C_alloc(56);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=C_mutate(((C_word *)((C_word*)t0)[3])+1,lf[201]);
t4=C_mutate(((C_word *)((C_word*)t0)[4])+1,lf[202]);
t5=C_mutate(((C_word *)((C_word*)t0)[5])+1,lf[203]);
t6=C_mutate(((C_word *)((C_word*)t0)[6])+1,lf[204]);
t7=C_mutate(((C_word *)((C_word*)t0)[7])+1,lf[205]);
t8=C_mutate(((C_word *)((C_word*)t0)[8])+1,lf[206]);
t9=C_mutate(((C_word *)((C_word*)t0)[9])+1,lf[207]);
t10=C_mutate(((C_word *)((C_word*)t0)[10])+1,lf[208]);
t11=C_mutate(((C_word *)((C_word*)t0)[11])+1,lf[209]);
t12=(*a=C_CLOSURE_TYPE|55,a[1]=(C_word)f_8320,a[2]=((C_word*)t0)[12],a[3]=((C_word*)t0)[13],a[4]=((C_word*)t0)[14],a[5]=((C_word*)t0)[15],a[6]=((C_word*)t0)[16],a[7]=((C_word*)t0)[17],a[8]=((C_word*)t0)[18],a[9]=((C_word*)t0)[19],a[10]=((C_word*)t0)[20],a[11]=((C_word*)t0)[21],a[12]=((C_word*)t0)[22],a[13]=((C_word*)t0)[23],a[14]=((C_word*)t0)[24],a[15]=((C_word*)t0)[25],a[16]=((C_word*)t0)[26],a[17]=((C_word*)t0)[27],a[18]=((C_word*)t0)[28],a[19]=((C_word*)t0)[29],a[20]=((C_word*)t0)[30],a[21]=((C_word*)t0)[31],a[22]=((C_word*)t0)[32],a[23]=((C_word*)t0)[33],a[24]=((C_word*)t0)[34],a[25]=((C_word*)t0)[35],a[26]=((C_word*)t0)[36],a[27]=((C_word*)t0)[37],a[28]=((C_word*)t0)[38],a[29]=((C_word*)t0)[39],a[30]=((C_word*)t0)[4],a[31]=((C_word*)t0)[40],a[32]=((C_word*)t0)[2],a[33]=((C_word*)t0)[41],a[34]=((C_word*)t0)[42],a[35]=((C_word*)t0)[43],a[36]=((C_word*)t0)[44],a[37]=((C_word*)t0)[45],a[38]=((C_word*)t0)[46],a[39]=((C_word*)t0)[3],a[40]=((C_word*)t0)[6],a[41]=((C_word*)t0)[7],a[42]=((C_word*)t0)[47],a[43]=((C_word*)t0)[5],a[44]=((C_word*)t0)[9],a[45]=((C_word*)t0)[10],a[46]=((C_word*)t0)[11],a[47]=((C_word*)t0)[48],a[48]=((C_word*)t0)[49],a[49]=((C_word*)t0)[50],a[50]=((C_word*)t0)[8],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],a[55]=((C_word*)t0)[55],tmp=(C_word)a,a+=56,tmp);
/* synrules.scm:105: r */
t13=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t13;
av2[1]=t12;
av2[2]=lf[236];
((C_proc)C_fast_retrieve_proc(t13))(3,av2);}}

/* k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in ... */
static void C_ccall f_8320(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(56,c,2)))){
C_save_and_reclaim((void *)f_8320,c,av);}
a=C_alloc(56);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|55,a[1]=(C_word)f_8324,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[17],a[17]=((C_word*)t0)[18],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[21],a[21]=((C_word*)t0)[22],a[22]=((C_word*)t0)[23],a[23]=((C_word*)t0)[24],a[24]=((C_word*)t0)[25],a[25]=((C_word*)t0)[26],a[26]=((C_word*)t0)[27],a[27]=((C_word*)t0)[28],a[28]=((C_word*)t0)[29],a[29]=((C_word*)t0)[2],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],a[37]=((C_word*)t0)[37],a[38]=((C_word*)t0)[38],a[39]=((C_word*)t0)[39],a[40]=((C_word*)t0)[40],a[41]=((C_word*)t0)[41],a[42]=((C_word*)t0)[42],a[43]=((C_word*)t0)[43],a[44]=((C_word*)t0)[44],a[45]=((C_word*)t0)[45],a[46]=((C_word*)t0)[46],a[47]=((C_word*)t0)[47],a[48]=((C_word*)t0)[48],a[49]=((C_word*)t0)[49],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],a[55]=((C_word*)t0)[55],tmp=(C_word)a,a+=56,tmp);
/* synrules.scm:106: r */
t4=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[235];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in ... */
static void C_ccall f_8324(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(56,c,2)))){
C_save_and_reclaim((void *)f_8324,c,av);}
a=C_alloc(56);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=C_mutate(((C_word *)((C_word*)t0)[3])+1,lf[210]);
t4=(*a=C_CLOSURE_TYPE|55,a[1]=(C_word)f_8329,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],a[7]=((C_word*)t0)[9],a[8]=((C_word*)t0)[10],a[9]=((C_word*)t0)[11],a[10]=((C_word*)t0)[12],a[11]=((C_word*)t0)[13],a[12]=((C_word*)t0)[14],a[13]=((C_word*)t0)[15],a[14]=((C_word*)t0)[16],a[15]=((C_word*)t0)[17],a[16]=((C_word*)t0)[18],a[17]=((C_word*)t0)[19],a[18]=((C_word*)t0)[20],a[19]=((C_word*)t0)[21],a[20]=((C_word*)t0)[22],a[21]=((C_word*)t0)[23],a[22]=((C_word*)t0)[24],a[23]=((C_word*)t0)[25],a[24]=((C_word*)t0)[26],a[25]=((C_word*)t0)[27],a[26]=((C_word*)t0)[28],a[27]=((C_word*)t0)[29],a[28]=((C_word*)t0)[30],a[29]=((C_word*)t0)[2],a[30]=((C_word*)t0)[31],a[31]=((C_word*)t0)[32],a[32]=((C_word*)t0)[33],a[33]=((C_word*)t0)[34],a[34]=((C_word*)t0)[35],a[35]=((C_word*)t0)[36],a[36]=((C_word*)t0)[37],a[37]=((C_word*)t0)[38],a[38]=((C_word*)t0)[39],a[39]=((C_word*)t0)[40],a[40]=((C_word*)t0)[41],a[41]=((C_word*)t0)[42],a[42]=((C_word*)t0)[43],a[43]=((C_word*)t0)[44],a[44]=((C_word*)t0)[45],a[45]=((C_word*)t0)[46],a[46]=((C_word*)t0)[47],a[47]=((C_word*)t0)[48],a[48]=((C_word*)t0)[49],a[49]=((C_word*)t0)[3],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],a[55]=((C_word*)t0)[55],tmp=(C_word)a,a+=56,tmp);
/* synrules.scm:108: r */
t5=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[234];
((C_proc)C_fast_retrieve_proc(t5))(3,av2);}}

/* k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in ... */
static void C_ccall f_8329(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(56,c,2)))){
C_save_and_reclaim((void *)f_8329,c,av);}
a=C_alloc(56);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=C_mutate(((C_word *)((C_word*)t0)[3])+1,lf[211]);
t4=C_mutate(((C_word *)((C_word*)t0)[4])+1,lf[212]);
t5=(*a=C_CLOSURE_TYPE|55,a[1]=(C_word)f_8335,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[10],a[8]=((C_word*)t0)[11],a[9]=((C_word*)t0)[12],a[10]=((C_word*)t0)[13],a[11]=((C_word*)t0)[14],a[12]=((C_word*)t0)[15],a[13]=((C_word*)t0)[16],a[14]=((C_word*)t0)[17],a[15]=((C_word*)t0)[18],a[16]=((C_word*)t0)[19],a[17]=((C_word*)t0)[20],a[18]=((C_word*)t0)[21],a[19]=((C_word*)t0)[22],a[20]=((C_word*)t0)[23],a[21]=((C_word*)t0)[24],a[22]=((C_word*)t0)[25],a[23]=((C_word*)t0)[26],a[24]=((C_word*)t0)[27],a[25]=((C_word*)t0)[28],a[26]=((C_word*)t0)[29],a[27]=((C_word*)t0)[30],a[28]=((C_word*)t0)[2],a[29]=((C_word*)t0)[31],a[30]=((C_word*)t0)[32],a[31]=((C_word*)t0)[33],a[32]=((C_word*)t0)[34],a[33]=((C_word*)t0)[35],a[34]=((C_word*)t0)[36],a[35]=((C_word*)t0)[37],a[36]=((C_word*)t0)[38],a[37]=((C_word*)t0)[39],a[38]=((C_word*)t0)[40],a[39]=((C_word*)t0)[3],a[40]=((C_word*)t0)[4],a[41]=((C_word*)t0)[41],a[42]=((C_word*)t0)[42],a[43]=((C_word*)t0)[43],a[44]=((C_word*)t0)[44],a[45]=((C_word*)t0)[45],a[46]=((C_word*)t0)[46],a[47]=((C_word*)t0)[47],a[48]=((C_word*)t0)[48],a[49]=((C_word*)t0)[49],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],a[55]=((C_word*)t0)[55],tmp=(C_word)a,a+=56,tmp);
/* synrules.scm:111: r */
t6=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[233];
((C_proc)C_fast_retrieve_proc(t6))(3,av2);}}

/* k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in ... */
static void C_ccall f_8335(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(56,c,2)))){
C_save_and_reclaim((void *)f_8335,c,av);}
a=C_alloc(56);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|55,a[1]=(C_word)f_8339,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[17],a[17]=((C_word*)t0)[18],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[21],a[21]=((C_word*)t0)[22],a[22]=((C_word*)t0)[23],a[23]=((C_word*)t0)[2],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],a[37]=((C_word*)t0)[37],a[38]=((C_word*)t0)[38],a[39]=((C_word*)t0)[39],a[40]=((C_word*)t0)[40],a[41]=((C_word*)t0)[41],a[42]=((C_word*)t0)[42],a[43]=((C_word*)t0)[43],a[44]=((C_word*)t0)[44],a[45]=((C_word*)t0)[45],a[46]=((C_word*)t0)[46],a[47]=((C_word*)t0)[47],a[48]=((C_word*)t0)[48],a[49]=((C_word*)t0)[49],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],a[55]=((C_word*)t0)[55],tmp=(C_word)a,a+=56,tmp);
/* synrules.scm:112: r */
t4=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[232];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in ... */
static void C_ccall f_8339(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(56,c,2)))){
C_save_and_reclaim((void *)f_8339,c,av);}
a=C_alloc(56);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|55,a[1]=(C_word)f_8343,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[17],a[17]=((C_word*)t0)[18],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[21],a[21]=((C_word*)t0)[22],a[22]=((C_word*)t0)[23],a[23]=((C_word*)t0)[24],a[24]=((C_word*)t0)[25],a[25]=((C_word*)t0)[26],a[26]=((C_word*)t0)[27],a[27]=((C_word*)t0)[28],a[28]=((C_word*)t0)[29],a[29]=((C_word*)t0)[30],a[30]=((C_word*)t0)[31],a[31]=((C_word*)t0)[32],a[32]=((C_word*)t0)[33],a[33]=((C_word*)t0)[34],a[34]=((C_word*)t0)[35],a[35]=((C_word*)t0)[36],a[36]=((C_word*)t0)[37],a[37]=((C_word*)t0)[38],a[38]=((C_word*)t0)[39],a[39]=((C_word*)t0)[40],a[40]=((C_word*)t0)[41],a[41]=((C_word*)t0)[42],a[42]=((C_word*)t0)[43],a[43]=((C_word*)t0)[2],a[44]=((C_word*)t0)[44],a[45]=((C_word*)t0)[45],a[46]=((C_word*)t0)[46],a[47]=((C_word*)t0)[47],a[48]=((C_word*)t0)[48],a[49]=((C_word*)t0)[49],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],a[55]=((C_word*)t0)[55],tmp=(C_word)a,a+=56,tmp);
/* synrules.scm:113: r */
t4=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[231];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in ... */
static void C_ccall f_8343(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(56,c,2)))){
C_save_and_reclaim((void *)f_8343,c,av);}
a=C_alloc(56);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|55,a[1]=(C_word)f_8347,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[17],a[17]=((C_word*)t0)[18],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[21],a[21]=((C_word*)t0)[22],a[22]=((C_word*)t0)[23],a[23]=((C_word*)t0)[24],a[24]=((C_word*)t0)[25],a[25]=((C_word*)t0)[26],a[26]=((C_word*)t0)[27],a[27]=((C_word*)t0)[28],a[28]=((C_word*)t0)[29],a[29]=((C_word*)t0)[30],a[30]=((C_word*)t0)[31],a[31]=((C_word*)t0)[32],a[32]=((C_word*)t0)[33],a[33]=((C_word*)t0)[34],a[34]=((C_word*)t0)[35],a[35]=((C_word*)t0)[36],a[36]=((C_word*)t0)[37],a[37]=((C_word*)t0)[38],a[38]=((C_word*)t0)[39],a[39]=((C_word*)t0)[40],a[40]=((C_word*)t0)[41],a[41]=((C_word*)t0)[2],a[42]=((C_word*)t0)[42],a[43]=((C_word*)t0)[43],a[44]=((C_word*)t0)[44],a[45]=((C_word*)t0)[45],a[46]=((C_word*)t0)[46],a[47]=((C_word*)t0)[47],a[48]=((C_word*)t0)[48],a[49]=((C_word*)t0)[49],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],a[55]=((C_word*)t0)[55],tmp=(C_word)a,a+=56,tmp);
/* synrules.scm:114: r */
t4=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[230];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in ... */
static void C_ccall f_8347(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(56,c,2)))){
C_save_and_reclaim((void *)f_8347,c,av);}
a=C_alloc(56);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|55,a[1]=(C_word)f_8351,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[17],a[17]=((C_word*)t0)[18],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[21],a[21]=((C_word*)t0)[22],a[22]=((C_word*)t0)[23],a[23]=((C_word*)t0)[24],a[24]=((C_word*)t0)[2],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],a[37]=((C_word*)t0)[37],a[38]=((C_word*)t0)[38],a[39]=((C_word*)t0)[39],a[40]=((C_word*)t0)[40],a[41]=((C_word*)t0)[41],a[42]=((C_word*)t0)[42],a[43]=((C_word*)t0)[43],a[44]=((C_word*)t0)[44],a[45]=((C_word*)t0)[45],a[46]=((C_word*)t0)[46],a[47]=((C_word*)t0)[47],a[48]=((C_word*)t0)[48],a[49]=((C_word*)t0)[49],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],a[55]=((C_word*)t0)[55],tmp=(C_word)a,a+=56,tmp);
/* synrules.scm:115: r */
t4=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[60];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in ... */
static void C_ccall f_8351(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(56,c,2)))){
C_save_and_reclaim((void *)f_8351,c,av);}
a=C_alloc(56);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|55,a[1]=(C_word)f_8355,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[17],a[17]=((C_word*)t0)[18],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[21],a[21]=((C_word*)t0)[22],a[22]=((C_word*)t0)[23],a[23]=((C_word*)t0)[2],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],a[37]=((C_word*)t0)[37],a[38]=((C_word*)t0)[38],a[39]=((C_word*)t0)[39],a[40]=((C_word*)t0)[40],a[41]=((C_word*)t0)[41],a[42]=((C_word*)t0)[42],a[43]=((C_word*)t0)[43],a[44]=((C_word*)t0)[44],a[45]=((C_word*)t0)[45],a[46]=((C_word*)t0)[46],a[47]=((C_word*)t0)[47],a[48]=((C_word*)t0)[48],a[49]=((C_word*)t0)[49],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],a[55]=((C_word*)t0)[55],tmp=(C_word)a,a+=56,tmp);
/* synrules.scm:116: r */
t4=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[92];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in ... */
static void C_ccall f_8355(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(56,c,2)))){
C_save_and_reclaim((void *)f_8355,c,av);}
a=C_alloc(56);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=C_mutate(((C_word *)((C_word*)t0)[3])+1,lf[213]);
t4=(*a=C_CLOSURE_TYPE|55,a[1]=(C_word)f_8360,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],a[7]=((C_word*)t0)[9],a[8]=((C_word*)t0)[10],a[9]=((C_word*)t0)[11],a[10]=((C_word*)t0)[12],a[11]=((C_word*)t0)[13],a[12]=((C_word*)t0)[14],a[13]=((C_word*)t0)[15],a[14]=((C_word*)t0)[16],a[15]=((C_word*)t0)[17],a[16]=((C_word*)t0)[18],a[17]=((C_word*)t0)[19],a[18]=((C_word*)t0)[20],a[19]=((C_word*)t0)[21],a[20]=((C_word*)t0)[22],a[21]=((C_word*)t0)[23],a[22]=((C_word*)t0)[24],a[23]=((C_word*)t0)[25],a[24]=((C_word*)t0)[26],a[25]=((C_word*)t0)[27],a[26]=((C_word*)t0)[2],a[27]=((C_word*)t0)[28],a[28]=((C_word*)t0)[29],a[29]=((C_word*)t0)[30],a[30]=((C_word*)t0)[31],a[31]=((C_word*)t0)[32],a[32]=((C_word*)t0)[33],a[33]=((C_word*)t0)[34],a[34]=((C_word*)t0)[35],a[35]=((C_word*)t0)[36],a[36]=((C_word*)t0)[37],a[37]=((C_word*)t0)[38],a[38]=((C_word*)t0)[39],a[39]=((C_word*)t0)[3],a[40]=((C_word*)t0)[40],a[41]=((C_word*)t0)[41],a[42]=((C_word*)t0)[42],a[43]=((C_word*)t0)[43],a[44]=((C_word*)t0)[44],a[45]=((C_word*)t0)[45],a[46]=((C_word*)t0)[46],a[47]=((C_word*)t0)[47],a[48]=((C_word*)t0)[48],a[49]=((C_word*)t0)[49],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],a[55]=((C_word*)t0)[55],tmp=(C_word)a,a+=56,tmp);
/* synrules.scm:118: r */
t5=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[229];
((C_proc)C_fast_retrieve_proc(t5))(3,av2);}}

/* k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in ... */
static void C_ccall f_8360(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(56,c,2)))){
C_save_and_reclaim((void *)f_8360,c,av);}
a=C_alloc(56);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=C_mutate(((C_word *)((C_word*)t0)[3])+1,lf[214]);
t4=C_mutate(((C_word *)((C_word*)t0)[4])+1,lf[215]);
t5=(*a=C_CLOSURE_TYPE|55,a[1]=(C_word)f_8367,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[10],a[8]=((C_word*)t0)[11],a[9]=((C_word*)t0)[12],a[10]=((C_word*)t0)[13],a[11]=((C_word*)t0)[14],a[12]=((C_word*)t0)[15],a[13]=((C_word*)t0)[16],a[14]=((C_word*)t0)[17],a[15]=((C_word*)t0)[18],a[16]=((C_word*)t0)[19],a[17]=((C_word*)t0)[20],a[18]=((C_word*)t0)[21],a[19]=((C_word*)t0)[22],a[20]=((C_word*)t0)[23],a[21]=((C_word*)t0)[24],a[22]=((C_word*)t0)[25],a[23]=((C_word*)t0)[26],a[24]=((C_word*)t0)[27],a[25]=((C_word*)t0)[28],a[26]=((C_word*)t0)[29],a[27]=((C_word*)t0)[30],a[28]=((C_word*)t0)[31],a[29]=((C_word*)t0)[32],a[30]=((C_word*)t0)[33],a[31]=((C_word*)t0)[34],a[32]=((C_word*)t0)[35],a[33]=((C_word*)t0)[36],a[34]=((C_word*)t0)[37],a[35]=((C_word*)t0)[38],a[36]=((C_word*)t0)[39],a[37]=((C_word*)t0)[40],a[38]=((C_word*)t0)[41],a[39]=((C_word*)t0)[42],a[40]=((C_word*)t0)[43],a[41]=((C_word*)t0)[44],a[42]=((C_word*)t0)[2],a[43]=((C_word*)t0)[45],a[44]=((C_word*)t0)[3],a[45]=((C_word*)t0)[46],a[46]=((C_word*)t0)[4],a[47]=((C_word*)t0)[47],a[48]=((C_word*)t0)[48],a[49]=((C_word*)t0)[49],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],a[55]=((C_word*)t0)[55],tmp=(C_word)a,a+=56,tmp);
/* synrules.scm:122: r */
t6=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[228];
((C_proc)C_fast_retrieve_proc(t6))(3,av2);}}

/* k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in ... */
static void C_ccall f_8367(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(56,c,2)))){
C_save_and_reclaim((void *)f_8367,c,av);}
a=C_alloc(56);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,lf[216]);
t3=(*a=C_CLOSURE_TYPE|55,a[1]=(C_word)f_8372,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[17],a[17]=((C_word*)t0)[18],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[21],a[21]=((C_word*)t0)[22],a[22]=((C_word*)t0)[23],a[23]=((C_word*)t0)[24],a[24]=((C_word*)t0)[25],a[25]=((C_word*)t0)[26],a[26]=((C_word*)t0)[27],a[27]=((C_word*)t0)[28],a[28]=((C_word*)t0)[29],a[29]=((C_word*)t0)[2],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],a[37]=((C_word*)t0)[37],a[38]=((C_word*)t0)[38],a[39]=((C_word*)t0)[39],a[40]=((C_word*)t0)[40],a[41]=((C_word*)t0)[41],a[42]=((C_word*)t0)[42],a[43]=((C_word*)t0)[43],a[44]=((C_word*)t0)[44],a[45]=((C_word*)t0)[45],a[46]=((C_word*)t0)[46],a[47]=((C_word*)t0)[47],a[48]=((C_word*)t0)[48],a[49]=((C_word*)t0)[49],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],a[55]=((C_word*)t0)[55],tmp=(C_word)a,a+=56,tmp);
/* synrules.scm:124: r */
t4=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[219];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in ... */
static void C_ccall f_8372(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(56,c,2)))){
C_save_and_reclaim((void *)f_8372,c,av);}
a=C_alloc(56);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|55,a[1]=(C_word)f_8376,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[17],a[17]=((C_word*)t0)[18],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[21],a[21]=((C_word*)t0)[22],a[22]=((C_word*)t0)[23],a[23]=((C_word*)t0)[24],a[24]=((C_word*)t0)[25],a[25]=((C_word*)t0)[26],a[26]=((C_word*)t0)[27],a[27]=((C_word*)t0)[28],a[28]=((C_word*)t0)[29],a[29]=((C_word*)t0)[30],a[30]=((C_word*)t0)[31],a[31]=((C_word*)t0)[32],a[32]=((C_word*)t0)[33],a[33]=((C_word*)t0)[34],a[34]=((C_word*)t0)[35],a[35]=((C_word*)t0)[36],a[36]=((C_word*)t0)[37],a[37]=((C_word*)t0)[38],a[38]=((C_word*)t0)[39],a[39]=((C_word*)t0)[40],a[40]=((C_word*)t0)[41],a[41]=((C_word*)t0)[42],a[42]=((C_word*)t0)[43],a[43]=((C_word*)t0)[44],a[44]=((C_word*)t0)[45],a[45]=((C_word*)t0)[46],a[46]=((C_word*)t0)[47],a[47]=((C_word*)t0)[48],a[48]=((C_word*)t0)[49],a[49]=((C_word*)t0)[50],a[50]=((C_word*)t0)[2],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],a[55]=((C_word*)t0)[55],tmp=(C_word)a,a+=56,tmp);
/* synrules.scm:125: r */
t4=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[227];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in ... */
static void C_ccall f_8376(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(56,c,2)))){
C_save_and_reclaim((void *)f_8376,c,av);}
a=C_alloc(56);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|55,a[1]=(C_word)f_8380,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[2],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],a[37]=((C_word*)t0)[37],a[38]=((C_word*)t0)[38],a[39]=((C_word*)t0)[39],a[40]=((C_word*)t0)[40],a[41]=((C_word*)t0)[41],a[42]=((C_word*)t0)[42],a[43]=((C_word*)t0)[43],a[44]=((C_word*)t0)[44],a[45]=((C_word*)t0)[45],a[46]=((C_word*)t0)[46],a[47]=((C_word*)t0)[47],a[48]=((C_word*)t0)[48],a[49]=((C_word*)t0)[49],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],a[55]=((C_word*)t0)[55],tmp=(C_word)a,a+=56,tmp);
/* synrules.scm:126: r */
t4=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[226];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in ... */
static void C_ccall f_8380(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(56,c,2)))){
C_save_and_reclaim((void *)f_8380,c,av);}
a=C_alloc(56);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|55,a[1]=(C_word)f_8384,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[2],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],a[37]=((C_word*)t0)[37],a[38]=((C_word*)t0)[38],a[39]=((C_word*)t0)[39],a[40]=((C_word*)t0)[40],a[41]=((C_word*)t0)[41],a[42]=((C_word*)t0)[42],a[43]=((C_word*)t0)[43],a[44]=((C_word*)t0)[44],a[45]=((C_word*)t0)[45],a[46]=((C_word*)t0)[46],a[47]=((C_word*)t0)[47],a[48]=((C_word*)t0)[48],a[49]=((C_word*)t0)[49],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],a[55]=((C_word*)t0)[55],tmp=(C_word)a,a+=56,tmp);
/* synrules.scm:127: r */
t4=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[225];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in ... */
static void C_ccall f_8384(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(55,c,2)))){
C_save_and_reclaim((void *)f_8384,c,av);}
a=C_alloc(55);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|54,a[1]=(C_word)f_8389,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[17],a[17]=((C_word*)t0)[18],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[21],a[21]=((C_word*)t0)[22],a[22]=((C_word*)t0)[23],a[23]=((C_word*)t0)[24],a[24]=((C_word*)t0)[25],a[25]=((C_word*)t0)[26],a[26]=((C_word*)t0)[27],a[27]=((C_word*)t0)[2],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],a[37]=((C_word*)t0)[37],a[38]=((C_word*)t0)[38],a[39]=((C_word*)t0)[39],a[40]=((C_word*)t0)[40],a[41]=((C_word*)t0)[41],a[42]=((C_word*)t0)[42],a[43]=((C_word*)t0)[43],a[44]=((C_word*)t0)[44],a[45]=((C_word*)t0)[45],a[46]=((C_word*)t0)[46],a[47]=((C_word*)t0)[47],a[48]=((C_word*)t0)[48],a[49]=((C_word*)t0)[49],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],tmp=(C_word)a,a+=55,tmp);
/* synrules.scm:129: r */
t4=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[55];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in k8216 in ... */
static void C_ccall f_8389(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(55,c,2)))){
C_save_and_reclaim((void *)f_8389,c,av);}
a=C_alloc(55);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|54,a[1]=(C_word)f_8393,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[2],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],a[37]=((C_word*)t0)[37],a[38]=((C_word*)t0)[38],a[39]=((C_word*)t0)[39],a[40]=((C_word*)t0)[40],a[41]=((C_word*)t0)[41],a[42]=((C_word*)t0)[42],a[43]=((C_word*)t0)[43],a[44]=((C_word*)t0)[44],a[45]=((C_word*)t0)[45],a[46]=((C_word*)t0)[46],a[47]=((C_word*)t0)[47],a[48]=((C_word*)t0)[48],a[49]=((C_word*)t0)[49],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],tmp=(C_word)a,a+=55,tmp);
/* synrules.scm:130: r */
t4=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[194];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* k8391 in k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in k8219 in ... */
static void C_ccall f_8393(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(55,c,2)))){
C_save_and_reclaim((void *)f_8393,c,av);}
a=C_alloc(55);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|54,a[1]=(C_word)f_8397,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[17],a[17]=((C_word*)t0)[18],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[21],a[21]=((C_word*)t0)[22],a[22]=((C_word*)t0)[23],a[23]=((C_word*)t0)[24],a[24]=((C_word*)t0)[25],a[25]=((C_word*)t0)[26],a[26]=((C_word*)t0)[27],a[27]=((C_word*)t0)[28],a[28]=((C_word*)t0)[29],a[29]=((C_word*)t0)[30],a[30]=((C_word*)t0)[31],a[31]=((C_word*)t0)[32],a[32]=((C_word*)t0)[33],a[33]=((C_word*)t0)[34],a[34]=((C_word*)t0)[35],a[35]=((C_word*)t0)[36],a[36]=((C_word*)t0)[37],a[37]=((C_word*)t0)[38],a[38]=((C_word*)t0)[39],a[39]=((C_word*)t0)[40],a[40]=((C_word*)t0)[41],a[41]=((C_word*)t0)[42],a[42]=((C_word*)t0)[2],a[43]=((C_word*)t0)[43],a[44]=((C_word*)t0)[44],a[45]=((C_word*)t0)[45],a[46]=((C_word*)t0)[46],a[47]=((C_word*)t0)[47],a[48]=((C_word*)t0)[48],a[49]=((C_word*)t0)[49],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],a[54]=((C_word*)t0)[54],tmp=(C_word)a,a+=55,tmp);
/* synrules.scm:131: r */
t4=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[193];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* k8395 in k8391 in k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in a9506 in ... */
static void C_ccall f_8397(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(54,c,2)))){
C_save_and_reclaim((void *)f_8397,c,av);}
a=C_alloc(54);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|53,a[1]=(C_word)f_8401,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[17],a[17]=((C_word*)t0)[18],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[21],a[21]=((C_word*)t0)[22],a[22]=((C_word*)t0)[23],a[23]=((C_word*)t0)[24],a[24]=((C_word*)t0)[25],a[25]=((C_word*)t0)[26],a[26]=((C_word*)t0)[27],a[27]=((C_word*)t0)[28],a[28]=((C_word*)t0)[29],a[29]=((C_word*)t0)[30],a[30]=((C_word*)t0)[31],a[31]=((C_word*)t0)[32],a[32]=((C_word*)t0)[33],a[33]=((C_word*)t0)[34],a[34]=((C_word*)t0)[35],a[35]=((C_word*)t0)[36],a[36]=((C_word*)t0)[37],a[37]=((C_word*)t0)[38],a[38]=((C_word*)t0)[39],a[39]=((C_word*)t0)[40],a[40]=((C_word*)t0)[41],a[41]=((C_word*)t0)[2],a[42]=((C_word*)t0)[42],a[43]=((C_word*)t0)[43],a[44]=((C_word*)t0)[44],a[45]=((C_word*)t0)[45],a[46]=((C_word*)t0)[46],a[47]=((C_word*)t0)[47],a[48]=((C_word*)t0)[48],a[49]=((C_word*)t0)[49],a[50]=((C_word*)t0)[50],a[51]=((C_word*)t0)[51],a[52]=((C_word*)t0)[52],a[53]=((C_word*)t0)[53],tmp=(C_word)a,a+=54,tmp);
/* synrules.scm:133: r */
t4=((C_word*)t0)[54];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[50];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* k8399 in k8395 in k8391 in k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in k9509 in ... */
static void C_ccall f_8401(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(122,c,6)))){
C_save_and_reclaim((void *)f_8401,c,av);}
a=C_alloc(122);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=C_mutate(((C_word *)((C_word*)t0)[3])+1,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8403,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word)li109),tmp=(C_word)a,a+=5,tmp));
t4=C_mutate(((C_word *)((C_word*)t0)[6])+1,(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_8409,a[2]=((C_word*)t0)[7],a[3]=((C_word*)t0)[8],a[4]=((C_word*)t0)[9],a[5]=((C_word*)t0)[10],a[6]=((C_word*)t0)[11],a[7]=((C_word*)t0)[12],a[8]=((C_word*)t0)[13],a[9]=((C_word*)t0)[14],a[10]=((C_word*)t0)[15],a[11]=((C_word*)t0)[2],a[12]=((C_word*)t0)[16],a[13]=((C_word)li111),tmp=(C_word)a,a+=14,tmp));
t5=C_mutate(((C_word *)((C_word*)t0)[15])+1,(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_8503,a[2]=((C_word*)t0)[17],a[3]=((C_word*)t0)[18],a[4]=((C_word*)t0)[19],a[5]=((C_word*)t0)[20],a[6]=((C_word*)t0)[21],a[7]=((C_word*)t0)[11],a[8]=((C_word*)t0)[22],a[9]=((C_word)li113),tmp=(C_word)a,a+=10,tmp));
t6=C_mutate(((C_word *)((C_word*)t0)[22])+1,(*a=C_CLOSURE_TYPE|18,a[1]=(C_word)f_8569,a[2]=((C_word*)t0)[23],a[3]=((C_word*)t0)[8],a[4]=((C_word*)t0)[9],a[5]=((C_word*)t0)[24],a[6]=((C_word*)t0)[25],a[7]=((C_word*)t0)[26],a[8]=((C_word*)t0)[17],a[9]=((C_word*)t0)[13],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[22],a[12]=((C_word*)t0)[27],a[13]=((C_word*)t0)[28],a[14]=((C_word*)t0)[29],a[15]=((C_word*)t0)[30],a[16]=((C_word*)t0)[31],a[17]=((C_word*)t0)[32],a[18]=((C_word)li114),tmp=(C_word)a,a+=19,tmp));
t7=C_mutate(((C_word *)((C_word*)t0)[24])+1,(*a=C_CLOSURE_TYPE|17,a[1]=(C_word)f_8750,a[2]=((C_word*)t0)[33],a[3]=((C_word*)t0)[34],a[4]=((C_word*)t0)[35],a[5]=((C_word*)t0)[36],a[6]=((C_word*)t0)[37],a[7]=((C_word*)t0)[38],a[8]=((C_word*)t0)[17],a[9]=((C_word*)t0)[16],a[10]=((C_word*)t0)[12],a[11]=((C_word*)t0)[13],a[12]=((C_word*)t0)[39],a[13]=((C_word*)t0)[10],a[14]=((C_word*)t0)[40],a[15]=((C_word*)t0)[22],a[16]=((C_word*)t0)[27],a[17]=((C_word)li115),tmp=(C_word)a,a+=18,tmp));
t8=C_mutate(((C_word *)((C_word*)t0)[21])+1,(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_8877,a[2]=((C_word*)t0)[23],a[3]=((C_word*)t0)[41],a[4]=((C_word*)t0)[42],a[5]=((C_word*)t0)[21],a[6]=((C_word*)t0)[25],a[7]=((C_word*)t0)[14],a[8]=((C_word*)t0)[43],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[27],a[11]=((C_word*)t0)[29],a[12]=((C_word*)t0)[32],a[13]=((C_word)li117),tmp=(C_word)a,a+=14,tmp));
t9=C_mutate(((C_word *)((C_word*)t0)[19])+1,(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_9017,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[19],a[4]=((C_word*)t0)[44],a[5]=((C_word*)t0)[14],a[6]=((C_word*)t0)[45],a[7]=((C_word*)t0)[46],a[8]=((C_word*)t0)[47],a[9]=((C_word*)t0)[48],a[10]=((C_word*)t0)[49],a[11]=((C_word*)t0)[50],a[12]=((C_word*)t0)[51],a[13]=((C_word)li119),tmp=(C_word)a,a+=14,tmp));
t10=C_mutate(((C_word *)((C_word*)t0)[20])+1,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9209,a[2]=((C_word*)t0)[23],a[3]=((C_word*)t0)[20],a[4]=((C_word*)t0)[32],a[5]=((C_word)li120),tmp=(C_word)a,a+=6,tmp));
t11=C_mutate(((C_word *)((C_word*)t0)[46])+1,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9286,a[2]=((C_word*)t0)[46],a[3]=((C_word*)t0)[51],a[4]=((C_word)li121),tmp=(C_word)a,a+=5,tmp));
t12=C_mutate(((C_word *)((C_word*)t0)[32])+1,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9375,a[2]=((C_word*)t0)[51],a[3]=((C_word)li122),tmp=(C_word)a,a+=4,tmp));
t13=C_mutate(((C_word *)((C_word*)t0)[51])+1,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9403,a[2]=((C_word*)t0)[3],a[3]=((C_word)li123),tmp=(C_word)a,a+=4,tmp));
t14=C_mutate(((C_word *)((C_word*)t0)[47])+1,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9427,a[2]=((C_word*)t0)[47],a[3]=((C_word*)t0)[51],a[4]=((C_word)li124),tmp=(C_word)a,a+=5,tmp));
t15=C_mutate(((C_word *)((C_word*)t0)[44])+1,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9447,a[2]=((C_word*)t0)[3],a[3]=((C_word)li126),tmp=(C_word)a,a+=4,tmp));
/* synrules.scm:345: make-transformer */
t16=((C_word*)((C_word*)t0)[6])[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t16;
av2[1]=((C_word*)t0)[52];
av2[2]=((C_word*)t0)[53];
((C_proc)(void*)(*((C_word*)t16+1)))(3,av2);}}

/* f_8403 in k8399 in k8395 in k8391 in k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in ... */
static void C_ccall f_8403(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8403,c,av);}
/* synrules.scm:136: c */
t3=((C_word*)t0)[2];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
av2[3]=((C_word*)((C_word*)t0)[3])[1];
((C_proc)C_fast_retrieve_proc(t3))(4,av2);}}

/* f_8409 in k8399 in k8395 in k8391 in k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in ... */
static void C_ccall f_8409(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(52,c,3)))){
C_save_and_reclaim((void *)f_8409,c,av);}
a=C_alloc(52);
t3=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1],((C_word*)((C_word*)t0)[4])[1]);
t4=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[5])[1],((C_word*)((C_word*)t0)[2])[1]);
t5=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[6])[1],t4);
t6=C_a_i_list(&a,1,t5);
t7=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8437,a[2]=((C_word*)t0)[7],a[3]=((C_word*)t0)[8],a[4]=t6,a[5]=((C_word*)t0)[9],a[6]=t3,a[7]=t1,tmp=(C_word)a,a+=8,tmp);
t8=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t9=t8;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=((C_word*)t10)[1];
t12=((C_word*)((C_word*)t0)[10])[1];
t13=C_i_check_list_2(t2,lf[17]);
t14=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8447,a[2]=((C_word*)t0)[11],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[12],a[5]=t7,tmp=(C_word)a,a+=6,tmp);
t15=C_SCHEME_UNDEFINED;
t16=(*a=C_VECTOR_TYPE|1,a[1]=t15,tmp=(C_word)a,a+=2,tmp);
t17=C_set_block_item(t16,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8461,a[2]=t10,a[3]=t16,a[4]=t12,a[5]=t11,a[6]=((C_word)li110),tmp=(C_word)a,a+=7,tmp));
t18=((C_word*)t16)[1];
f_8461(t18,t14,t2);}

/* k8435 */
static void C_ccall f_8437(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,1)))){
C_save_and_reclaim((void *)f_8437,c,av);}
a=C_alloc(27);
t2=C_a_i_cons(&a,2,((C_word*)((C_word*)t0)[2])[1],t1);
t3=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[3])[1],((C_word*)t0)[4],t2);
t4=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[5])[1],((C_word*)t0)[6],t3);
t5=((C_word*)t0)[7];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_list(&a,2,lf[187],t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k8445 */
static void C_ccall f_8447(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_8447,c,av);}
a=C_alloc(15);
t2=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]);
t3=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[4])[1],t2);
t4=C_a_i_list(&a,1,t3);
/* synrules.scm:139: ##sys#append */
t5=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[5];
av2[2]=t1;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* map-loop2682 */
static void C_fcall f_8461(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_8461,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8486,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* synrules.scm:142: g2688 */
t4=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k8484 in map-loop2682 */
static void C_ccall f_8486(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8486,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_8461(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* f_8503 in k8399 in k8395 in k8391 in k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in ... */
static void C_ccall f_8503(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_8503,c,av);}
a=C_alloc(11);
t3=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_8510,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=t1,a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[7],a[10]=((C_word*)t0)[8],tmp=(C_word)a,a+=11,tmp);
if(C_truep(C_i_pairp(t2))){
t4=C_i_pairp(C_u_i_cdr(t2));
t5=t3;
f_8510(t5,(C_truep(t4)?C_i_nullp(C_i_cddr(t2)):C_SCHEME_FALSE));}
else{
t4=t3;
f_8510(t4,C_SCHEME_FALSE);}}

/* k8508 */
static void C_fcall f_8510(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,4)))){
C_save_and_reclaim_args((void *)trf_8510,2,t0,t1);}
a=C_alloc(11);
if(C_truep(t1)){
t2=C_i_cdar(((C_word*)t0)[2]);
t3=C_i_cadr(((C_word*)t0)[2]);
t4=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_8546,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=t3,a[7]=((C_word*)t0)[7],a[8]=t2,a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* synrules.scm:151: process-match */
t5=((C_word*)((C_word*)t0)[10])[1];{
C_word av2[5];
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)((C_word*)t0)[9])[1];
av2[3]=t2;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t5))(5,av2);}}
else{
/* synrules.scm:158: ##sys#syntax-error-hook */
t2=*((C_word*)lf[46]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=((C_word*)t0)[5];
av2[2]=lf[217];
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}}

/* k8529 in k8544 in k8508 */
static void C_ccall f_8531(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,5)))){
C_save_and_reclaim((void *)f_8531,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8535,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8539,a[2]=((C_word*)t0)[5],a[3]=t2,a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
/* synrules.scm:157: meta-variables */
t4=((C_word*)((C_word*)t0)[7])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[8];
av2[3]=C_fix(0);
av2[4]=C_SCHEME_END_OF_LIST;
av2[5]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t4))(6,av2);}}

/* k8533 in k8529 in k8544 in k8508 */
static void C_ccall f_8535(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,1)))){
C_save_and_reclaim((void *)f_8535,c,av);}
a=C_alloc(15);
t2=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[2])[1],((C_word*)t0)[3],t1);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,2,((C_word*)t0)[5],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k8537 in k8529 in k8544 in k8508 */
static void C_ccall f_8539(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8539,c,av);}
/* synrules.scm:155: process-template */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=C_fix(0);
av2[4]=t1;
((C_proc)C_fast_retrieve_proc(t2))(5,av2);}}

/* a8540 in k8544 in k8508 */
static void C_ccall f_8541(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8541,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k8544 in k8508 */
static void C_ccall f_8546(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,5)))){
C_save_and_reclaim((void *)f_8546,c,av);}
a=C_alloc(15);
t2=C_a_i_cons(&a,2,((C_word*)((C_word*)t0)[2])[1],t1);
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_8531,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8541,a[2]=((C_word)li112),tmp=(C_word)a,a+=3,tmp);
/* synrules.scm:152: process-pattern */
t5=((C_word*)((C_word*)t0)[9])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=((C_word*)t0)[8];
av2[3]=((C_word*)((C_word*)t0)[10])[1];
av2[4]=t4;
av2[5]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t5))(6,av2);}}

/* f_8569 in k8399 in k8395 in k8391 in k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in ... */
static void C_ccall f_8569(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_8569,c,av);}
a=C_alloc(24);
if(C_truep(C_i_symbolp(t3))){
if(C_truep(C_i_memq(t3,((C_word*)t0)[2]))){
t5=C_a_i_list(&a,2,lf[218],t3);
t6=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[3])[1],t5);
t7=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[4])[1],t2,t6);
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_a_i_list(&a,1,t7);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}
else{
t5=(*a=C_CLOSURE_TYPE|16,a[1]=(C_word)f_8603,a[2]=((C_word*)t0)[5],a[3]=t1,a[4]=t2,a[5]=t3,a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],tmp=(C_word)a,a+=17,tmp);
/* synrules.scm:167: segment-pattern? */
t6=((C_word*)((C_word*)t0)[17])[1];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t3;
av2[3]=t4;
((C_proc)C_fast_retrieve_proc(t6))(4,av2);}}}

/* k8601 */
static void C_ccall f_8603(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(35,c,4)))){
C_save_and_reclaim((void *)f_8603,c,av);}
a=C_alloc(35);
if(C_truep(t1)){
/* synrules.scm:168: process-segment-match */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
((C_proc)C_fast_retrieve_proc(t2))(4,av2);}}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[5]))){
t2=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[6])[1],((C_word*)t0)[4]);
t3=C_a_i_list(&a,1,t2);
t4=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[7])[1],((C_word*)((C_word*)t0)[6])[1]);
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8639,a[2]=t4,a[3]=((C_word*)t0)[8],a[4]=((C_word*)t0)[9],a[5]=t3,a[6]=((C_word*)t0)[3],tmp=(C_word)a,a+=7,tmp);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8643,a[2]=t5,a[3]=((C_word*)t0)[10],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[11],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t7=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[12])[1],((C_word*)((C_word*)t0)[6])[1]);
/* synrules.scm:172: process-match */
t8=((C_word*)((C_word*)t0)[11])[1];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t8;
av2[1]=t6;
av2[2]=t7;
av2[3]=C_u_i_car(((C_word*)t0)[5]);
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t8))(5,av2);}}
else{
if(C_truep(C_i_vectorp(((C_word*)t0)[5]))){
t2=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[6])[1],((C_word*)t0)[4]);
t3=C_a_i_list(&a,1,t2);
t4=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[13])[1],((C_word*)((C_word*)t0)[6])[1]);
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8696,a[2]=t4,a[3]=((C_word*)t0)[8],a[4]=((C_word*)t0)[9],a[5]=t3,a[6]=((C_word*)t0)[3],tmp=(C_word)a,a+=7,tmp);
t6=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[14])[1],((C_word*)((C_word*)t0)[6])[1]);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8704,a[2]=((C_word*)t0)[11],a[3]=t5,a[4]=t6,tmp=(C_word)a,a+=5,tmp);
/* synrules.scm:178: scheme#vector->list */
t8=*((C_word*)lf[183]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}
else{
t2=C_i_nullp(((C_word*)t0)[5]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8717,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[15],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[16],tmp=(C_word)a,a+=7,tmp);
if(C_truep(t2)){
t4=t3;
f_8717(t4,t2);}
else{
t4=C_booleanp(((C_word*)t0)[5]);
t5=t3;
f_8717(t5,(C_truep(t4)?t4:C_charp(((C_word*)t0)[5])));}}}}}

/* k8637 in k8601 */
static void C_ccall f_8639(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,1)))){
C_save_and_reclaim((void *)f_8639,c,av);}
a=C_alloc(18);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,((C_word*)((C_word*)t0)[3])[1],t2);
t4=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[4])[1],((C_word*)t0)[5],t3);
t5=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_list(&a,1,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k8641 in k8601 */
static void C_ccall f_8643(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_8643,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8647,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[3])[1],((C_word*)((C_word*)t0)[4])[1]);
/* synrules.scm:173: process-match */
t4=((C_word*)((C_word*)t0)[5])[1];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=t3;
av2[3]=C_u_i_cdr(((C_word*)t0)[6]);
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t4))(5,av2);}}

/* k8645 in k8641 in k8601 */
static void C_ccall f_8647(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8647,c,av);}
/* synrules.scm:170: ##sys#append */
t2=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k8694 in k8601 */
static void C_ccall f_8696(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,1)))){
C_save_and_reclaim((void *)f_8696,c,av);}
a=C_alloc(18);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,((C_word*)((C_word*)t0)[3])[1],t2);
t4=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[4])[1],((C_word*)t0)[5],t3);
t5=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_list(&a,1,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k8702 in k8601 */
static void C_ccall f_8704(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8704,c,av);}
/* synrules.scm:177: process-match */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=t1;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t2))(5,av2);}}

/* k8715 in k8601 */
static void C_fcall f_8717(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,0,1)))){
C_save_and_reclaim_args((void *)trf_8717,2,t0,t1);}
a=C_alloc(18);
if(C_truep(t1)){
t2=C_a_i_list(&a,2,lf[219],((C_word*)t0)[2]);
t3=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[3])[1],((C_word*)t0)[4],t2);
t4=((C_word*)t0)[5];{
C_word av2[2];
av2[0]=t4;
av2[1]=C_a_i_list(&a,1,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t2=C_a_i_list(&a,2,lf[219],((C_word*)t0)[2]);
t3=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[6])[1],((C_word*)t0)[4],t2);
t4=((C_word*)t0)[5];{
C_word av2[2];
av2[0]=t4;
av2[1]=C_a_i_list(&a,1,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* f_8750 in k8399 in k8395 in k8391 in k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in ... */
static void C_ccall f_8750(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(25,c,4)))){
C_save_and_reclaim((void *)f_8750,c,av);}
a=C_alloc(25);
t4=(*a=C_CLOSURE_TYPE|18,a[1]=(C_word)f_8754,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=t3,a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[7],a[10]=((C_word*)t0)[8],a[11]=((C_word*)t0)[9],a[12]=((C_word*)t0)[10],a[13]=((C_word*)t0)[11],a[14]=((C_word*)t0)[12],a[15]=t1,a[16]=((C_word*)t0)[13],a[17]=((C_word*)t0)[14],a[18]=((C_word*)t0)[15],tmp=(C_word)a,a+=19,tmp);
t5=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[16])[1],((C_word*)((C_word*)t0)[6])[1]);
/* synrules.scm:185: process-match */
t6=((C_word*)((C_word*)t0)[15])[1];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t4;
av2[2]=t5;
av2[3]=C_i_car(t3);
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t6))(5,av2);}}

/* k8752 */
static void C_ccall f_8754(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(75,c,4)))){
C_save_and_reclaim((void *)f_8754,c,av);}
a=C_alloc(75);
t2=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[2])[1],((C_word*)t0)[3]);
t3=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[4])[1],((C_word*)t0)[3]);
t4=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[5])[1],t3);
t5=C_a_i_list(&a,1,t4);
t6=C_i_cddr(((C_word*)t0)[6]);
t7=C_i_length(t6);
t8=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[7])[1],((C_word*)((C_word*)t0)[5])[1],t7);
t9=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[8])[1],((C_word*)t0)[3]);
t10=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[5])[1],((C_word*)((C_word*)t0)[5])[1]);
t11=C_a_i_list(&a,2,t9,t10);
t12=C_u_i_cdr(((C_word*)t0)[6]);
t13=C_i_length(C_u_i_cdr(t12));
t14=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[9])[1],((C_word*)((C_word*)t0)[5])[1],t13);
t15=(*a=C_CLOSURE_TYPE|17,a[1]=(C_word)f_8833,a[2]=t14,a[3]=((C_word*)t0)[10],a[4]=((C_word*)t0)[11],a[5]=((C_word*)t0)[12],a[6]=((C_word*)t0)[13],a[7]=((C_word*)t0)[14],a[8]=t11,a[9]=t8,a[10]=t5,a[11]=t2,a[12]=((C_word*)t0)[15],a[13]=((C_word*)t0)[16],a[14]=((C_word*)t0)[8],a[15]=((C_word*)t0)[17],a[16]=((C_word*)t0)[5],a[17]=t1,tmp=(C_word)a,a+=18,tmp);
t16=C_u_i_cdr(((C_word*)t0)[6]);
/* synrules.scm:193: process-match */
t17=((C_word*)((C_word*)t0)[18])[1];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t17;
av2[1]=t15;
av2[2]=((C_word*)((C_word*)t0)[8])[1];
av2[3]=C_u_i_cdr(t16);
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t17))(5,av2);}}

/* k8807 in k8831 in k8752 */
static void C_ccall f_8809(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(60,c,1)))){
C_save_and_reclaim((void *)f_8809,c,av);}
a=C_alloc(60);
t2=C_a_i_cons(&a,2,((C_word*)((C_word*)t0)[2])[1],t1);
t3=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[3])[1],t2);
t4=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[4])[1],((C_word*)t0)[5],t3);
t5=C_a_i_list(&a,4,((C_word*)((C_word*)t0)[6])[1],((C_word*)((C_word*)t0)[7])[1],((C_word*)t0)[8],t4);
t6=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[2])[1],((C_word*)t0)[9],t5);
t7=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[6])[1],((C_word*)t0)[10],t6);
t8=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[2])[1],((C_word*)t0)[11],t7);
t9=((C_word*)t0)[12];{
C_word *av2=av;
av2[0]=t9;
av2[1]=C_a_i_list(&a,1,t8);
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}

/* k8831 in k8752 */
static void C_ccall f_8833(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(43,c,3)))){
C_save_and_reclaim((void *)f_8833,c,av);}
a=C_alloc(43);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_8809,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=t2,a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],tmp=(C_word)a,a+=13,tmp);
t4=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[13])[1],((C_word*)((C_word*)t0)[14])[1]);
t5=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[15])[1],((C_word*)((C_word*)t0)[16])[1],C_fix(-1));
t6=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[7])[1],t4,t5);
t7=C_a_i_list(&a,1,t6);
/* synrules.scm:186: ##sys#append */
t8=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t8;
av2[1]=t3;
av2[2]=((C_word*)t0)[17];
av2[3]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}

/* f_8877 in k8399 in k8395 in k8391 in k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in ... */
static void C_ccall f_8877(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word *a;
if(c!=6) C_bad_argc_2(c,6,t0);
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_8877,c,av);}
a=C_alloc(15);
if(C_truep(C_i_symbolp(t2))){
if(C_truep(C_i_memq(t2,((C_word*)t0)[2]))){
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8901,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* synrules.scm:205: mapit */
t7=t4;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t7))(3,av2);}}}
else{
t6=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_8907,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=t1,a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=t4,a[9]=((C_word*)t0)[6],a[10]=((C_word*)t0)[7],a[11]=((C_word*)t0)[8],a[12]=((C_word*)t0)[9],a[13]=((C_word*)t0)[10],a[14]=((C_word*)t0)[11],tmp=(C_word)a,a+=15,tmp);
/* synrules.scm:206: segment-pattern? */
t7=((C_word*)((C_word*)t0)[12])[1];{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=t2;
av2[3]=t5;
((C_proc)C_fast_retrieve_proc(t7))(4,av2);}}}

/* k8899 */
static void C_ccall f_8901(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_8901,c,av);}
a=C_alloc(9);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list1(&a,1,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k8905 */
static void C_ccall f_8907(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,5)))){
C_save_and_reclaim((void *)f_8907,c,av);}
a=C_alloc(26);
if(C_truep(t1)){
t2=C_i_cddr(((C_word*)t0)[2]);
t3=C_i_length(t2);
t4=C_eqp(t3,C_fix(0));
t5=(C_truep(t4)?((C_word*)t0)[3]:C_a_i_list(&a,3,((C_word*)((C_word*)t0)[4])[1],((C_word*)t0)[3],t3));
t6=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_8920,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[3],a[6]=t3,a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t7=C_u_i_car(((C_word*)t0)[2]);
t8=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8936,a[2]=((C_word*)t0)[9],a[3]=((C_word*)t0)[8],a[4]=t5,a[5]=((C_word*)t0)[10],a[6]=((C_word*)t0)[11],a[7]=((C_word)li116),tmp=(C_word)a,a+=8,tmp);
/* synrules.scm:212: process-pattern */
t9=((C_word*)((C_word*)t0)[7])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t9;
av2[1]=t6;
av2[2]=t7;
av2[3]=((C_word*)((C_word*)t0)[9])[1];
av2[4]=t8;
av2[5]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t9))(6,av2);}}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[2]))){
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8982,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[12],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
t3=C_u_i_car(((C_word*)t0)[2]);
t4=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[13])[1],((C_word*)t0)[3]);
/* synrules.scm:223: process-pattern */
t5=((C_word*)((C_word*)t0)[7])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=t3;
av2[3]=t4;
av2[4]=((C_word*)t0)[8];
av2[5]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t5))(6,av2);}}
else{
if(C_truep(C_i_vectorp(((C_word*)t0)[2]))){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9011,a[2]=((C_word*)t0)[14],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[8],tmp=(C_word)a,a+=7,tmp);
/* synrules.scm:226: scheme#vector->list */
t3=*((C_word*)lf[183]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}}}

/* k8918 in k8905 */
static void C_ccall f_8920(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,5)))){
C_save_and_reclaim((void *)f_8920,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8924,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_i_cddr(((C_word*)t0)[3]);
t4=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[4])[1],((C_word*)t0)[5],((C_word*)t0)[6]);
/* synrules.scm:220: process-pattern */
t5=((C_word*)((C_word*)t0)[7])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=t3;
av2[3]=t4;
av2[4]=((C_word*)t0)[8];
av2[5]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t5))(6,av2);}}

/* k8922 in k8918 in k8905 */
static void C_ccall f_8924(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8924,c,av);}
/* synrules.scm:211: scheme#append */
t2=*((C_word*)lf[16]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* a8935 in k8905 */
static void C_ccall f_8936(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(21,c,2)))){
C_save_and_reclaim((void *)f_8936,c,av);}
a=C_alloc(21);
t3=C_eqp(((C_word*)((C_word*)t0)[2])[1],t2);
if(C_truep(t3)){
/* synrules.scm:215: mapit */
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=((C_word*)t0)[4];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}
else{
t4=C_a_i_list(&a,1,((C_word*)((C_word*)t0)[2])[1]);
t5=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[5])[1],t4,t2);
t6=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[6])[1],t5,((C_word*)t0)[4]);
/* synrules.scm:215: mapit */
t7=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t7;
av2[1]=t1;
av2[2]=t6;
((C_proc)C_fast_retrieve_proc(t7))(3,av2);}}}

/* k8980 in k8905 */
static void C_ccall f_8982(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_8982,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8986,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_u_i_cdr(((C_word*)t0)[3]);
t4=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[4])[1],((C_word*)t0)[5]);
/* synrules.scm:224: process-pattern */
t5=((C_word*)((C_word*)t0)[6])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=t3;
av2[3]=t4;
av2[4]=((C_word*)t0)[7];
av2[5]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t5))(6,av2);}}

/* k8984 in k8980 in k8905 */
static void C_ccall f_8986(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8986,c,av);}
/* synrules.scm:223: scheme#append */
t2=*((C_word*)lf[16]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k9009 in k8905 */
static void C_ccall f_9011(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_9011,c,av);}
a=C_alloc(6);
t2=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[2])[1],((C_word*)t0)[3]);
/* synrules.scm:226: process-pattern */
t3=((C_word*)((C_word*)t0)[4])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[5];
av2[2]=t1;
av2[3]=t2;
av2[4]=((C_word*)t0)[6];
av2[5]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(6,av2);}}

/* f_9017 in k8399 in k8395 in k8391 in k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in ... */
static void C_ccall f_9017(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_9017,c,av);}
a=C_alloc(15);
if(C_truep(C_i_symbolp(t2))){
t5=C_i_assq(t2,t4);
if(C_truep(t5)){
t6=C_i_cdr(t5);
if(C_truep(C_fixnum_less_or_equal_p(t6,t3))){
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
/* synrules.scm:238: ##sys#syntax-error-hook */
t7=*((C_word*)lf[46]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t1;
av2[2]=lf[220];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}}
else{
t6=C_a_i_list(&a,2,lf[218],t2);
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[2])[1],t6);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}
else{
t5=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_9056,a[2]=t3,a[3]=t1,a[4]=t2,a[5]=((C_word*)t0)[3],a[6]=t4,a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[6],a[10]=((C_word*)t0)[7],a[11]=((C_word*)t0)[8],a[12]=((C_word*)t0)[9],a[13]=((C_word*)t0)[10],a[14]=((C_word*)t0)[11],tmp=(C_word)a,a+=15,tmp);
/* synrules.scm:241: segment-template? */
t6=((C_word*)((C_word*)t0)[12])[1];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
((C_proc)C_fast_retrieve_proc(t6))(3,av2);}}}

/* k9054 */
static void C_ccall f_9056(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_9056,c,av);}
a=C_alloc(11);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_9059,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
/* synrules.scm:242: segment-depth */
t3=((C_word*)((C_word*)t0)[11])[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[4]))){
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_9179,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[12],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* synrules.scm:267: process-template */
t3=((C_word*)((C_word*)t0)[5])[1];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_u_i_car(((C_word*)t0)[4]);
av2[3]=((C_word*)t0)[2];
av2[4]=((C_word*)t0)[6];
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
if(C_truep(C_i_vectorp(((C_word*)t0)[4]))){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9200,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[13],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9204,a[2]=((C_word*)t0)[5],a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
/* synrules.scm:271: scheme#vector->list */
t4=*((C_word*)lf[183]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[14])[1],((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}}}

/* k9057 in k9054 */
static void C_ccall f_9059(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,5)))){
C_save_and_reclaim((void *)f_9059,c,av);}
a=C_alloc(12);
t2=C_fixnum_plus(((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_9065,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=t2,tmp=(C_word)a,a+=12,tmp);
/* synrules.scm:245: free-meta-variables */
t4=((C_word*)((C_word*)t0)[10])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_car(((C_word*)t0)[4]);
av2[3]=t2;
av2[4]=((C_word*)t0)[6];
av2[5]=C_SCHEME_END_OF_LIST;
((C_proc)C_fast_retrieve_proc(t4))(6,av2);}}

/* k9063 in k9057 in k9054 */
static void C_ccall f_9065(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_9065,c,av);}
a=C_alloc(12);
if(C_truep(C_i_nullp(t1))){
/* synrules.scm:247: ##sys#syntax-error-hook */
t2=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[221];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_9077,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=t1,a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* synrules.scm:248: process-template */
t3=((C_word*)((C_word*)t0)[4])[1];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_u_i_car(((C_word*)t0)[3]);
av2[3]=((C_word*)t0)[11];
av2[4]=((C_word*)t0)[6];
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}}

/* k9075 in k9063 in k9057 in k9054 */
static void C_ccall f_9077(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_9077,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9080,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9127,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[9],a[5]=((C_word*)t0)[10],a[6]=((C_word*)t0)[11],tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_i_pairp(((C_word*)t0)[10]))){
if(C_truep(C_i_nullp(C_u_i_cdr(((C_word*)t0)[10])))){
t4=C_i_symbolp(t1);
t5=t3;
f_9127(t5,(C_truep(t4)?C_eqp(t1,C_u_i_car(((C_word*)t0)[10])):C_SCHEME_FALSE));}
else{
t4=t3;
f_9127(t4,C_SCHEME_FALSE);}}
else{
t4=t3;
f_9127(t4,C_SCHEME_FALSE);}}

/* k9078 in k9075 in k9063 in k9057 in k9054 */
static void C_fcall f_9080(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,4)))){
C_save_and_reclaim_args((void *)trf_9080,2,t0,t1);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_9083,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9106,a[2]=t4,a[3]=((C_word)li118),tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_9106(t6,t2,((C_word*)t0)[8],t1);}

/* k9081 in k9078 in k9075 in k9063 in k9057 in k9054 */
static void C_ccall f_9083(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_9083,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9104,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* synrules.scm:262: segment-tail */
t3=((C_word*)((C_word*)t0)[6])[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* k9094 in k9102 in k9081 in k9078 in k9075 in k9063 in k9057 in k9054 */
static void C_ccall f_9096(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_9096,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[74],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k9098 in k9102 in k9081 in k9078 in k9075 in k9063 in k9057 in k9054 */
static void C_ccall f_9100(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9100,c,av);}
/* synrules.scm:264: process-template */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=((C_word*)t0)[4];
av2[4]=((C_word*)t0)[5];
((C_proc)C_fast_retrieve_proc(t2))(5,av2);}}

/* k9102 in k9081 in k9078 in k9075 in k9063 in k9057 in k9054 */
static void C_ccall f_9104(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_9104,c,av);}
a=C_alloc(10);
if(C_truep(C_i_nullp(t1))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9096,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9100,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
/* synrules.scm:264: segment-tail */
t4=((C_word*)((C_word*)t0)[7])[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[8];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}}

/* doloop2816 in k9078 in k9075 in k9063 in k9057 in k9054 */
static void C_fcall f_9106(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_9106,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=C_eqp(t2,C_fix(1));
if(C_truep(t4)){
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_fixnum_difference(t2,C_fix(1));
t6=C_a_i_list(&a,3,lf[222],lf[74],t3);
t8=t1;
t9=t5;
t10=t6;
t1=t8;
t2=t9;
t3=t10;
goto loop;}}

/* k9125 in k9075 in k9063 in k9057 in k9054 */
static void C_fcall f_9127(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,1)))){
C_save_and_reclaim_args((void *)trf_9127,2,t0,t1);}
a=C_alloc(15);
if(C_truep(t1)){
t2=((C_word*)t0)[2];
f_9080(t2,((C_word*)t0)[3]);}
else{
t2=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[4])[1],((C_word*)t0)[5],((C_word*)t0)[3]);
t3=C_a_i_cons(&a,2,t2,((C_word*)t0)[5]);
t4=((C_word*)t0)[2];
f_9080(t4,C_a_i_cons(&a,2,((C_word*)((C_word*)t0)[6])[1],t3));}}

/* k9177 in k9054 */
static void C_ccall f_9179(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_9179,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9183,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* synrules.scm:268: process-template */
t3=((C_word*)((C_word*)t0)[4])[1];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_u_i_cdr(((C_word*)t0)[5]);
av2[3]=((C_word*)t0)[6];
av2[4]=((C_word*)t0)[7];
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k9181 in k9177 in k9054 */
static void C_ccall f_9183(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_9183,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,((C_word*)((C_word*)t0)[3])[1],((C_word*)t0)[4],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k9198 in k9054 */
static void C_ccall f_9200(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_9200,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,2,((C_word*)((C_word*)t0)[3])[1],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k9202 in k9054 */
static void C_ccall f_9204(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9204,c,av);}
/* synrules.scm:271: process-template */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=((C_word*)t0)[4];
av2[4]=((C_word*)t0)[5];
((C_proc)C_fast_retrieve_proc(t2))(5,av2);}}

/* f_9209 in k8399 in k8395 in k8391 in k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in ... */
static void C_ccall f_9209(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word *a;
if(c!=6) C_bad_argc_2(c,6,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_9209,c,av);}
a=C_alloc(7);
if(C_truep(C_i_symbolp(t2))){
if(C_truep(C_i_memq(t2,((C_word*)t0)[2]))){
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_a_i_cons(&a,2,t2,t3);
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_a_i_cons(&a,2,t6,t4);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}
else{
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9235,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=t1,a[6]=t4,tmp=(C_word)a,a+=7,tmp);
/* synrules.scm:282: segment-pattern? */
t7=((C_word*)((C_word*)t0)[4])[1];{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=t2;
av2[3]=t5;
((C_proc)C_fast_retrieve_proc(t7))(4,av2);}}}

/* k9233 */
static void C_ccall f_9235(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_9235,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=C_i_car(((C_word*)t0)[2]);
t3=C_fixnum_plus(((C_word*)t0)[3],C_fix(1));
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9250,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* synrules.scm:284: meta-variables */
t5=((C_word*)((C_word*)t0)[4])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=C_i_cddr(((C_word*)t0)[2]);
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[6];
av2[5]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t5))(6,av2);}}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[2]))){
t2=C_u_i_car(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9269,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=t2,a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* synrules.scm:287: meta-variables */
t4=((C_word*)((C_word*)t0)[4])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_u_i_cdr(((C_word*)t0)[2]);
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[6];
av2[5]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t4))(6,av2);}}
else{
if(C_truep(C_i_vectorp(((C_word*)t0)[2]))){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9284,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
/* synrules.scm:289: scheme#vector->list */
t3=*((C_word*)lf[183]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}}}

/* k9248 in k9233 */
static void C_ccall f_9250(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_9250,c,av);}
/* synrules.scm:283: meta-variables */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
av2[4]=t1;
av2[5]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t2))(6,av2);}}

/* k9267 in k9233 */
static void C_ccall f_9269(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_9269,c,av);}
/* synrules.scm:286: meta-variables */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
av2[4]=t1;
av2[5]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t2))(6,av2);}}

/* k9282 in k9233 */
static void C_ccall f_9284(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_9284,c,av);}
/* synrules.scm:289: meta-variables */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=((C_word*)t0)[4];
av2[4]=((C_word*)t0)[5];
av2[5]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t2))(6,av2);}}

/* f_9286 in k8399 in k8395 in k8391 in k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in ... */
static void C_ccall f_9286(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(c!=6) C_bad_argc_2(c,6,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_9286,c,av);}
a=C_alloc(8);
if(C_truep(C_i_symbolp(t2))){
t6=C_i_memq(t2,t5);
if(C_truep(C_i_not(t6))){
t7=C_i_assq(t2,t4);
if(C_truep(t7)){
t8=C_i_cdr(t7);
t9=C_fixnum_greater_or_equal_p(t8,t3);
t10=t1;{
C_word *av2=av;
av2[0]=t10;
av2[1]=(C_truep(t9)?C_a_i_cons(&a,2,t2,t5):t5);
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}
else{
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}
else{
t6=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_9328,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=t3,a[6]=t4,a[7]=t5,tmp=(C_word)a,a+=8,tmp);
/* synrules.scm:301: segment-template? */
t7=((C_word*)((C_word*)t0)[3])[1];{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=t2;
((C_proc)C_fast_retrieve_proc(t7))(3,av2);}}}

/* k9326 */
static void C_ccall f_9328(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,5)))){
C_save_and_reclaim((void *)f_9328,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=C_i_car(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9339,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* synrules.scm:304: free-meta-variables */
t4=((C_word*)((C_word*)t0)[3])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_cddr(((C_word*)t0)[2]);
av2[3]=((C_word*)t0)[5];
av2[4]=((C_word*)t0)[6];
av2[5]=((C_word*)t0)[7];
((C_proc)C_fast_retrieve_proc(t4))(6,av2);}}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[2]))){
t2=C_u_i_car(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9358,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* synrules.scm:309: free-meta-variables */
t4=((C_word*)((C_word*)t0)[3])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_u_i_cdr(((C_word*)t0)[2]);
av2[3]=((C_word*)t0)[5];
av2[4]=((C_word*)t0)[6];
av2[5]=((C_word*)t0)[7];
((C_proc)C_fast_retrieve_proc(t4))(6,av2);}}
else{
if(C_truep(C_i_vectorp(((C_word*)t0)[2]))){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9373,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],tmp=(C_word)a,a+=7,tmp);
/* synrules.scm:312: scheme#vector->list */
t3=*((C_word*)lf[183]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}}}

/* k9337 in k9326 */
static void C_ccall f_9339(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_9339,c,av);}
/* synrules.scm:302: free-meta-variables */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
av2[4]=((C_word*)t0)[6];
av2[5]=t1;
((C_proc)C_fast_retrieve_proc(t2))(6,av2);}}

/* k9356 in k9326 */
static void C_ccall f_9358(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_9358,c,av);}
/* synrules.scm:307: free-meta-variables */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
av2[4]=((C_word*)t0)[6];
av2[5]=t1;
((C_proc)C_fast_retrieve_proc(t2))(6,av2);}}

/* k9371 in k9326 */
static void C_ccall f_9373(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_9373,c,av);}
/* synrules.scm:312: free-meta-variables */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=((C_word*)t0)[4];
av2[4]=((C_word*)t0)[5];
av2[5]=((C_word*)t0)[6];
((C_proc)C_fast_retrieve_proc(t2))(6,av2);}}

/* f_9375 in k8399 in k8395 in k8391 in k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in ... */
static void C_ccall f_9375(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_9375,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9382,a[2]=t3,a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* synrules.scm:316: segment-template? */
t5=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)C_fast_retrieve_proc(t5))(3,av2);}}

/* k9380 */
static void C_ccall f_9382(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9382,c,av);}
if(C_truep(t1)){
if(C_truep(((C_word*)t0)[2])){
/* synrules.scm:319: ##sys#syntax-error-hook */
t2=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[223];
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
t2=C_i_listp(((C_word*)t0)[4]);
if(C_truep(C_i_not(t2))){
/* synrules.scm:321: ##sys#syntax-error-hook */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[224];
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* f_9403 in k8399 in k8395 in k8391 in k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in ... */
static void C_ccall f_9403(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9403,c,av);}
if(C_truep(C_i_pairp(t2))){
if(C_truep(C_i_pairp(C_u_i_cdr(t2)))){
/* synrules.scm:327: ellipsis? */
t3=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
av2[2]=C_i_cadr(t2);
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}
else{
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}
else{
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* f_9427 in k8399 in k8395 in k8391 in k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in ... */
static void C_ccall f_9427(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_9427,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9434,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* synrules.scm:332: segment-template? */
t4=((C_word*)((C_word*)t0)[3])[1];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* k9432 */
static void C_ccall f_9434(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_9434,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9441,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* synrules.scm:333: segment-depth */
t3=((C_word*)((C_word*)t0)[3])[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_cdr(((C_word*)t0)[4]);
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fix(0);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k9439 in k9432 */
static void C_ccall f_9441(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_9441,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fixnum_plus(C_fix(1),t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_9447 in k8399 in k8395 in k8391 in k8387 in k8382 in k8378 in k8374 in k8370 in k8365 in k8358 in k8353 in k8349 in k8345 in k8341 in k8337 in k8333 in k8327 in k8322 in k8318 in k8303 in k9517 in ... */
static void C_ccall f_9447(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_9447,c,av);}
a=C_alloc(7);
t3=C_i_cdr(t2);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9457,a[2]=t5,a[3]=((C_word*)t0)[2],a[4]=((C_word)li125),tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_9457(t7,t1,t3);}

/* loop */
static void C_fcall f_9457(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_9457,3,t0,t1,t2);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9464,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_pairp(t2))){
/* synrules.scm:341: ellipsis? */
t4=((C_word*)((C_word*)t0)[3])[1];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_u_i_car(t2);
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}
else{
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k9462 in loop */
static void C_ccall f_9464(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9464,c,av);}
if(C_truep(t1)){
/* synrules.scm:342: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_9457(t2,((C_word*)t0)[3],C_i_cdr(((C_word*)t0)[4]));}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k9483 in k8222 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in ... */
static void C_ccall f_9485(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_9485,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[21]+1 /* (set! ##sys#scheme-macro-environment ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9489,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9501,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1590: ##sys#macro-environment */
t5=*((C_word*)lf[20]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k9487 in k9483 in k8222 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_ccall f_9489(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_9489,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[93]+1 /* (set! ##sys#default-macro-environment ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9493,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9497,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* expand.scm:1592: ##sys#macro-environment */
t5=*((C_word*)lf[20]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k9491 in k9487 in k9483 in k8222 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in ... */
static void C_ccall f_9493(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_9493,c,av);}
t2=C_mutate((C_word*)lf[195]+1 /* (set! ##sys#meta-macro-environment ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k9495 in k9487 in k9483 in k8222 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in ... */
static void C_ccall f_9497(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9497,c,av);}
/* expand.scm:1592: chicken.base#make-parameter */
t2=*((C_word*)lf[196]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k9499 in k9483 in k8222 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_ccall f_9501(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9501,c,av);}
/* expand.scm:1590: chicken.internal#fixup-macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[197]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[197]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
tp(3,av2);}}

/* k9503 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in ... */
static void C_ccall f_9505(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9505,c,av);}
/* synrules.scm:43: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[199];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in ... */
static void C_ccall f_9507(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_9507,c,av);}
a=C_alloc(6);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9511,a[2]=t2,a[3]=t4,a[4]=t1,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* synrules.scm:48: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[199];
av2[3]=t2;
av2[4]=lf[239];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in ... */
static void C_ccall f_9511(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,4)))){
C_save_and_reclaim((void *)f_9511,c,av);}
a=C_alloc(20);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_u_i_cdr(((C_word*)t0)[2]);
t6=C_u_i_cdr(t5);
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=lf[200];
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_9519,a[2]=t9,a[3]=t7,a[4]=t4,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],tmp=(C_word)a,a+=8,tmp);
if(C_truep(C_i_symbolp(((C_word*)t4)[1]))){
t11=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9528,a[2]=t9,a[3]=t4,a[4]=t7,a[5]=t10,tmp=(C_word)a,a+=6,tmp);
/* synrules.scm:53: ##sys#check-syntax */
t12=*((C_word*)lf[59]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t12;
av2[1]=t11;
av2[2]=lf[199];
av2[3]=((C_word*)t0)[2];
av2[4]=lf[238];
((C_proc)(void*)(*((C_word*)t12+1)))(5,av2);}}
else{
t11=t10;
f_9519(t11,C_SCHEME_UNDEFINED);}}

/* k9517 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_fcall f_9519(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word t44;
C_word t45;
C_word t46;
C_word t47;
C_word t48;
C_word t49;
C_word t50;
C_word t51;
C_word t52;
C_word t53;
C_word t54;
C_word t55;
C_word t56;
C_word t57;
C_word t58;
C_word t59;
C_word t60;
C_word t61;
C_word t62;
C_word t63;
C_word t64;
C_word t65;
C_word t66;
C_word t67;
C_word t68;
C_word t69;
C_word t70;
C_word t71;
C_word t72;
C_word t73;
C_word t74;
C_word t75;
C_word t76;
C_word t77;
C_word t78;
C_word t79;
C_word t80;
C_word t81;
C_word t82;
C_word t83;
C_word t84;
C_word t85;
C_word t86;
C_word t87;
C_word t88;
C_word t89;
C_word t90;
C_word t91;
C_word t92;
C_word t93;
C_word t94;
C_word t95;
C_word t96;
C_word t97;
C_word t98;
C_word t99;
C_word t100;
C_word t101;
C_word t102;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(152,0,2)))){
C_save_and_reclaim_args((void *)trf_9519,2,t0,t1);}
a=C_alloc(152);
t2=((C_word*)((C_word*)t0)[2])[1];
t3=((C_word*)((C_word*)t0)[3])[1];
t4=((C_word*)((C_word*)t0)[4])[1];
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_SCHEME_UNDEFINED;
t14=(*a=C_VECTOR_TYPE|1,a[1]=t13,tmp=(C_word)a,a+=2,tmp);
t15=C_SCHEME_UNDEFINED;
t16=(*a=C_VECTOR_TYPE|1,a[1]=t15,tmp=(C_word)a,a+=2,tmp);
t17=C_SCHEME_UNDEFINED;
t18=(*a=C_VECTOR_TYPE|1,a[1]=t17,tmp=(C_word)a,a+=2,tmp);
t19=C_SCHEME_UNDEFINED;
t20=(*a=C_VECTOR_TYPE|1,a[1]=t19,tmp=(C_word)a,a+=2,tmp);
t21=C_SCHEME_UNDEFINED;
t22=(*a=C_VECTOR_TYPE|1,a[1]=t21,tmp=(C_word)a,a+=2,tmp);
t23=C_SCHEME_UNDEFINED;
t24=(*a=C_VECTOR_TYPE|1,a[1]=t23,tmp=(C_word)a,a+=2,tmp);
t25=C_SCHEME_UNDEFINED;
t26=(*a=C_VECTOR_TYPE|1,a[1]=t25,tmp=(C_word)a,a+=2,tmp);
t27=C_SCHEME_UNDEFINED;
t28=(*a=C_VECTOR_TYPE|1,a[1]=t27,tmp=(C_word)a,a+=2,tmp);
t29=C_SCHEME_UNDEFINED;
t30=(*a=C_VECTOR_TYPE|1,a[1]=t29,tmp=(C_word)a,a+=2,tmp);
t31=C_SCHEME_UNDEFINED;
t32=(*a=C_VECTOR_TYPE|1,a[1]=t31,tmp=(C_word)a,a+=2,tmp);
t33=C_SCHEME_UNDEFINED;
t34=(*a=C_VECTOR_TYPE|1,a[1]=t33,tmp=(C_word)a,a+=2,tmp);
t35=C_SCHEME_UNDEFINED;
t36=(*a=C_VECTOR_TYPE|1,a[1]=t35,tmp=(C_word)a,a+=2,tmp);
t37=C_SCHEME_UNDEFINED;
t38=(*a=C_VECTOR_TYPE|1,a[1]=t37,tmp=(C_word)a,a+=2,tmp);
t39=C_SCHEME_UNDEFINED;
t40=(*a=C_VECTOR_TYPE|1,a[1]=t39,tmp=(C_word)a,a+=2,tmp);
t41=C_SCHEME_UNDEFINED;
t42=(*a=C_VECTOR_TYPE|1,a[1]=t41,tmp=(C_word)a,a+=2,tmp);
t43=C_SCHEME_UNDEFINED;
t44=(*a=C_VECTOR_TYPE|1,a[1]=t43,tmp=(C_word)a,a+=2,tmp);
t45=C_SCHEME_UNDEFINED;
t46=(*a=C_VECTOR_TYPE|1,a[1]=t45,tmp=(C_word)a,a+=2,tmp);
t47=C_SCHEME_UNDEFINED;
t48=(*a=C_VECTOR_TYPE|1,a[1]=t47,tmp=(C_word)a,a+=2,tmp);
t49=C_SCHEME_UNDEFINED;
t50=(*a=C_VECTOR_TYPE|1,a[1]=t49,tmp=(C_word)a,a+=2,tmp);
t51=C_SCHEME_UNDEFINED;
t52=(*a=C_VECTOR_TYPE|1,a[1]=t51,tmp=(C_word)a,a+=2,tmp);
t53=C_SCHEME_UNDEFINED;
t54=(*a=C_VECTOR_TYPE|1,a[1]=t53,tmp=(C_word)a,a+=2,tmp);
t55=C_SCHEME_UNDEFINED;
t56=(*a=C_VECTOR_TYPE|1,a[1]=t55,tmp=(C_word)a,a+=2,tmp);
t57=C_SCHEME_UNDEFINED;
t58=(*a=C_VECTOR_TYPE|1,a[1]=t57,tmp=(C_word)a,a+=2,tmp);
t59=C_SCHEME_UNDEFINED;
t60=(*a=C_VECTOR_TYPE|1,a[1]=t59,tmp=(C_word)a,a+=2,tmp);
t61=C_SCHEME_UNDEFINED;
t62=(*a=C_VECTOR_TYPE|1,a[1]=t61,tmp=(C_word)a,a+=2,tmp);
t63=C_SCHEME_UNDEFINED;
t64=(*a=C_VECTOR_TYPE|1,a[1]=t63,tmp=(C_word)a,a+=2,tmp);
t65=C_SCHEME_UNDEFINED;
t66=(*a=C_VECTOR_TYPE|1,a[1]=t65,tmp=(C_word)a,a+=2,tmp);
t67=C_SCHEME_UNDEFINED;
t68=(*a=C_VECTOR_TYPE|1,a[1]=t67,tmp=(C_word)a,a+=2,tmp);
t69=C_SCHEME_UNDEFINED;
t70=(*a=C_VECTOR_TYPE|1,a[1]=t69,tmp=(C_word)a,a+=2,tmp);
t71=C_SCHEME_UNDEFINED;
t72=(*a=C_VECTOR_TYPE|1,a[1]=t71,tmp=(C_word)a,a+=2,tmp);
t73=C_SCHEME_UNDEFINED;
t74=(*a=C_VECTOR_TYPE|1,a[1]=t73,tmp=(C_word)a,a+=2,tmp);
t75=C_SCHEME_UNDEFINED;
t76=(*a=C_VECTOR_TYPE|1,a[1]=t75,tmp=(C_word)a,a+=2,tmp);
t77=C_SCHEME_UNDEFINED;
t78=(*a=C_VECTOR_TYPE|1,a[1]=t77,tmp=(C_word)a,a+=2,tmp);
t79=C_SCHEME_UNDEFINED;
t80=(*a=C_VECTOR_TYPE|1,a[1]=t79,tmp=(C_word)a,a+=2,tmp);
t81=C_SCHEME_UNDEFINED;
t82=(*a=C_VECTOR_TYPE|1,a[1]=t81,tmp=(C_word)a,a+=2,tmp);
t83=C_SCHEME_UNDEFINED;
t84=(*a=C_VECTOR_TYPE|1,a[1]=t83,tmp=(C_word)a,a+=2,tmp);
t85=C_SCHEME_UNDEFINED;
t86=(*a=C_VECTOR_TYPE|1,a[1]=t85,tmp=(C_word)a,a+=2,tmp);
t87=C_SCHEME_UNDEFINED;
t88=(*a=C_VECTOR_TYPE|1,a[1]=t87,tmp=(C_word)a,a+=2,tmp);
t89=C_SCHEME_UNDEFINED;
t90=(*a=C_VECTOR_TYPE|1,a[1]=t89,tmp=(C_word)a,a+=2,tmp);
t91=C_SCHEME_UNDEFINED;
t92=(*a=C_VECTOR_TYPE|1,a[1]=t91,tmp=(C_word)a,a+=2,tmp);
t93=C_SCHEME_UNDEFINED;
t94=(*a=C_VECTOR_TYPE|1,a[1]=t93,tmp=(C_word)a,a+=2,tmp);
t95=C_SCHEME_UNDEFINED;
t96=(*a=C_VECTOR_TYPE|1,a[1]=t95,tmp=(C_word)a,a+=2,tmp);
t97=C_SCHEME_UNDEFINED;
t98=(*a=C_VECTOR_TYPE|1,a[1]=t97,tmp=(C_word)a,a+=2,tmp);
t99=C_SCHEME_UNDEFINED;
t100=(*a=C_VECTOR_TYPE|1,a[1]=t99,tmp=(C_word)a,a+=2,tmp);
t101=(*a=C_CLOSURE_TYPE|55,a[1]=(C_word)f_8305,a[2]=t6,a[3]=t8,a[4]=t10,a[5]=t12,a[6]=t14,a[7]=t16,a[8]=t18,a[9]=t20,a[10]=t22,a[11]=t24,a[12]=t26,a[13]=t28,a[14]=t30,a[15]=t32,a[16]=t34,a[17]=t36,a[18]=t38,a[19]=t40,a[20]=t42,a[21]=t44,a[22]=t46,a[23]=t48,a[24]=t50,a[25]=t52,a[26]=t54,a[27]=t56,a[28]=t58,a[29]=t60,a[30]=t62,a[31]=t64,a[32]=t66,a[33]=t68,a[34]=t70,a[35]=t72,a[36]=t74,a[37]=t76,a[38]=((C_word*)t0)[5],a[39]=t78,a[40]=t80,a[41]=t88,a[42]=t90,a[43]=t86,a[44]=t82,a[45]=t4,a[46]=t84,a[47]=t94,a[48]=t100,a[49]=t92,a[50]=t98,a[51]=t96,a[52]=((C_word*)t0)[6],a[53]=t3,a[54]=((C_word*)t0)[7],a[55]=t2,tmp=(C_word)a,a+=56,tmp);
/* synrules.scm:93: r */
t102=((C_word*)t0)[7];{
C_word av2[3];
av2[0]=t102;
av2[1]=t101;
av2[2]=lf[237];
((C_proc)C_fast_retrieve_proc(t102))(3,av2);}}

/* k9526 in k9509 in a9506 in k8219 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_ccall f_9528(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_9528,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,((C_word*)((C_word*)t0)[3])[1]);
t3=C_i_car(((C_word*)((C_word*)t0)[4])[1]);
t4=C_mutate(((C_word *)((C_word*)t0)[3])+1,t3);
t5=C_i_cdr(((C_word*)((C_word*)t0)[4])[1]);
t6=C_mutate(((C_word *)((C_word*)t0)[4])+1,t5);
t7=((C_word*)t0)[5];
f_9519(t7,t6);}

/* k9539 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in ... */
static void C_ccall f_9541(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9541,c,av);}
/* expand.scm:1570: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[240];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a9542 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in ... */
static void C_ccall f_9543(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_9543,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9547,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1575: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[240];
av2[3]=t2;
av2[4]=lf[244];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k9545 in a9542 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in ... */
static void C_ccall f_9547(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_9547,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9554,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1576: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[243];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* k9552 in k9545 in a9542 in k8216 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in ... */
static void C_ccall f_9554(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(30,c,1)))){
C_save_and_reclaim((void *)f_9554,c,av);}
a=C_alloc(30);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_a_i_list(&a,3,lf[76],C_SCHEME_END_OF_LIST,t2);
t4=C_a_i_list(&a,3,lf[95],t3,lf[241]);
t5=C_a_i_list(&a,2,lf[242],t4);
t6=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_list(&a,2,t1,t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* k9572 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in ... */
static void C_ccall f_9574(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9574,c,av);}
/* expand.scm:1518: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[245];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in ... */
static void C_ccall f_9576(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_9576,c,av);}
a=C_alloc(6);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9580,a[2]=t4,a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1523: r */
t6=t3;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[245];
((C_proc)C_fast_retrieve_proc(t6))(3,av2);}}

/* k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in ... */
static void C_ccall f_9580(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_9580,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9583,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* expand.scm:1524: r */
t3=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[246];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in ... */
static void C_ccall f_9583(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_9583,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9586,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* expand.scm:1525: r */
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[248];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in ... */
static void C_ccall f_9586(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(28,c,4)))){
C_save_and_reclaim((void *)f_9586,c,av);}
a=C_alloc(28);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9588,a[2]=t7,a[3]=t5,a[4]=((C_word)li129),tmp=(C_word)a,a+=5,tmp));
t9=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_9598,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=t1,a[6]=((C_word*)t0)[4],a[7]=((C_word)li130),tmp=(C_word)a,a+=8,tmp));
t10=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9761,a[2]=t7,a[3]=((C_word)li133),tmp=(C_word)a,a+=4,tmp));
t11=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9856,a[2]=t3,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1567: ##sys#check-syntax */
t12=*((C_word*)lf[59]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t12;
av2[1]=t11;
av2[2]=lf[245];
av2[3]=((C_word*)t0)[6];
av2[4]=lf[258];
((C_proc)(void*)(*((C_word*)t12+1)))(5,av2);}}

/* walk in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_fcall f_9588(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_9588,4,t0,t1,t2,t3);}
a=C_alloc(4);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9596,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1526: walk1 */
t5=((C_word*)((C_word*)t0)[3])[1];
f_9598(t5,t4,t2,t3);}

/* k9594 in walk in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in ... */
static void C_ccall f_9596(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9596,c,av);}
/* expand.scm:1526: simplify */
t2=((C_word*)((C_word*)t0)[2])[1];
f_9761(t2,((C_word*)t0)[3],t1);}

/* walk1 in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_fcall f_9598(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_9598,4,t0,t1,t2,t3);}
a=C_alloc(12);
if(C_truep(C_i_vectorp(t2))){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9612,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9616,a[2]=((C_word*)t0)[2],a[3]=t4,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1529: scheme#vector->list */
t6=*((C_word*)lf[183]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t4=C_i_pairp(t2);
if(C_truep(C_i_not(t4))){
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_a_i_list(&a,2,lf[75],t2);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_i_car(t2);
t6=C_u_i_cdr(t2);
t7=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_9635,a[2]=t3,a[3]=t1,a[4]=t6,a[5]=t2,a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[2],a[8]=((C_word*)t0)[4],a[9]=t5,a[10]=((C_word*)t0)[5],a[11]=((C_word*)t0)[6],tmp=(C_word)a,a+=12,tmp);
/* expand.scm:1534: c */
t8=((C_word*)t0)[6];{
C_word av2[4];
av2[0]=t8;
av2[1]=t7;
av2[2]=((C_word*)t0)[3];
av2[3]=t5;
((C_proc)C_fast_retrieve_proc(t8))(4,av2);}}}}

/* k9610 in walk1 in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in ... */
static void C_ccall f_9612(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_9612,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,2,lf[206],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k9614 in walk1 in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in ... */
static void C_ccall f_9616(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9616,c,av);}
/* expand.scm:1529: walk */
t2=((C_word*)((C_word*)t0)[2])[1];
f_9588(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* k9633 in walk1 in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in ... */
static void C_ccall f_9635(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_9635,c,av);}
a=C_alloc(10);
if(C_truep(t1)){
t2=C_eqp(((C_word*)t0)[2],C_fix(0));
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9644,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1536: ##sys#check-syntax */
t4=*((C_word*)lf[59]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[246];
av2[3]=((C_word*)t0)[5];
av2[4]=lf[247];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t3=C_a_i_list(&a,2,lf[75],((C_word*)t0)[6]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9658,a[2]=((C_word*)t0)[3],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1539: walk */
t5=((C_word*)((C_word*)t0)[7])[1];
f_9588(t5,t4,((C_word*)t0)[4],C_fixnum_difference(((C_word*)t0)[2],C_fix(1)));}}
else{
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_9668,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[9],a[8]=((C_word*)t0)[10],a[9]=((C_word*)t0)[11],tmp=(C_word)a,a+=10,tmp);
/* expand.scm:1540: c */
t3=((C_word*)t0)[11];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[8];
av2[3]=((C_word*)t0)[9];
((C_proc)C_fast_retrieve_proc(t3))(4,av2);}}}

/* k9642 in k9633 in walk1 in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in ... */
static void C_ccall f_9644(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_9644,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_i_car(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k9656 in k9633 in walk1 in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in ... */
static void C_ccall f_9658(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_9658,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list3(&a,3,lf[210],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k9666 in k9633 in walk1 in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in ... */
static void C_ccall f_9668(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_9668,c,av);}
a=C_alloc(10);
if(C_truep(t1)){
t2=C_a_i_list(&a,2,lf[75],((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9679,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1542: walk */
t4=((C_word*)((C_word*)t0)[4])[1];
f_9588(t4,t3,((C_word*)t0)[5],C_fixnum_plus(((C_word*)t0)[6],C_fix(1)));}
else{
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_9689,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
if(C_truep(C_i_pairp(((C_word*)t0)[7]))){
/* expand.scm:1543: c */
t3=((C_word*)t0)[9];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[8];
av2[3]=C_u_i_car(((C_word*)t0)[7]);
((C_proc)C_fast_retrieve_proc(t3))(4,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_9689(2,av2);}}}}

/* k9677 in k9666 in k9633 in walk1 in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in ... */
static void C_ccall f_9679(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_9679,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list3(&a,3,lf[210],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k9687 in k9666 in k9633 in walk1 in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in ... */
static void C_ccall f_9689(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_9689,c,av);}
a=C_alloc(13);
if(C_truep(t1)){
t2=C_eqp(((C_word*)t0)[2],C_fix(0));
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9698,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[2],tmp=(C_word)a,a+=7,tmp);
/* expand.scm:1545: ##sys#check-syntax */
t4=*((C_word*)lf[59]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[248];
av2[3]=((C_word*)t0)[3];
av2[4]=lf[249];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t3=C_a_i_list(&a,2,lf[75],((C_word*)t0)[7]);
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9728,a[2]=t3,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[2],tmp=(C_word)a,a+=7,tmp);
/* expand.scm:1550: walk */
t5=((C_word*)((C_word*)t0)[5])[1];
f_9588(t5,t4,C_i_cdr(((C_word*)t0)[3]),C_fixnum_difference(((C_word*)t0)[2],C_fix(1)));}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9743,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1553: walk */
t3=((C_word*)((C_word*)t0)[5])[1];
f_9588(t3,t2,((C_word*)t0)[3],((C_word*)t0)[2]);}}

/* k9696 in k9687 in k9666 in k9633 in walk1 in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in ... */
static void C_ccall f_9698(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_9698,c,av);}
a=C_alloc(4);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9709,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1546: walk */
t4=((C_word*)((C_word*)t0)[4])[1];
f_9588(t4,t3,((C_word*)t0)[5],((C_word*)t0)[6]);}

/* k9707 in k9696 in k9687 in k9666 in k9633 in walk1 in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in ... */
static void C_ccall f_9709(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_9709,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[74],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k9718 in k9726 in k9687 in k9666 in k9633 in walk1 in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in ... */
static void C_ccall f_9720(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_9720,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[210],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k9726 in k9687 in k9666 in k9633 in walk1 in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in ... */
static void C_ccall f_9728(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_9728,c,av);}
a=C_alloc(13);
t2=C_a_i_list(&a,3,lf[210],((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9720,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1551: walk */
t4=((C_word*)((C_word*)t0)[4])[1];
f_9588(t4,t3,((C_word*)t0)[5],((C_word*)t0)[6]);}

/* k9741 in k9687 in k9666 in k9633 in walk1 in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in ... */
static void C_ccall f_9743(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_9743,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9747,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1553: walk */
t3=((C_word*)((C_word*)t0)[3])[1];
f_9588(t3,t2,((C_word*)t0)[4],((C_word*)t0)[5]);}

/* k9745 in k9741 in k9687 in k9666 in k9633 in walk1 in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in ... */
static void C_ccall f_9747(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_9747,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[210],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* simplify in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_fcall f_9761(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_9761,3,t0,t1,t2);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9765,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1555: chicken.syntax#match-expression */
t4=*((C_word*)lf[122]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[256];
av2[4]=lf[257];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k9763 in simplify in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in ... */
static void C_ccall f_9765(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_9765,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9769,a[2]=((C_word*)t0)[2],a[3]=((C_word)li131),tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1555: g2546 */
t3=t2;
f_9769(t3,((C_word*)t0)[3],t1);}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9791,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1557: chicken.syntax#match-expression */
t3=*((C_word*)lf[122]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[254];
av2[4]=lf[255];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}

/* g2546 in k9763 in simplify in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in ... */
static void C_fcall f_9769(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_9769,3,t0,t1,t2);}
a=C_alloc(6);
t3=C_i_assq(lf[250],t2);
t4=C_i_cdr(t3);
t5=C_a_i_list(&a,2,lf[241],t4);
/* expand.scm:1556: simplify */
t6=((C_word*)((C_word*)t0)[2])[1];
f_9761(t6,t1,t5);}

/* k9789 in k9763 in simplify in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in ... */
static void C_ccall f_9791(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_9791,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9795,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li132),tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1555: g2553 */
t3=t2;
f_9795(t3,((C_word*)t0)[4],t1);}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9838,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* expand.scm:1564: chicken.syntax#match-expression */
t3=*((C_word*)lf[122]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[252];
av2[4]=lf[253];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}

/* g2553 in k9789 in k9763 in simplify in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in ... */
static void C_fcall f_9795(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_9795,3,t0,t1,t2);}
a=C_alloc(6);
t3=C_i_assq(lf[251],t2);
t4=C_i_length(t3);
if(C_truep(C_fixnum_lessp(t4,C_fix(32)))){
t5=C_i_assq(lf[250],t2);
t6=C_i_cdr(t5);
t7=C_i_cdr(t3);
t8=C_a_i_cons(&a,2,t6,t7);
t9=C_a_i_cons(&a,2,lf[241],t8);
/* expand.scm:1561: simplify */
t10=((C_word*)((C_word*)t0)[2])[1];
f_9761(t10,t1,t9);}
else{
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k9836 in k9789 in k9763 in simplify in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in ... */
static void C_ccall f_9838(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_9838,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(t1)?C_i_cdr(C_i_assq(lf[250],t1)):((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k9854 in k9584 in k9581 in k9578 in a9575 in k8213 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_ccall f_9856(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9856,c,av);}
/* expand.scm:1568: walk */
t2=((C_word*)((C_word*)t0)[2])[1];
f_9588(t2,((C_word*)t0)[3],C_i_cadr(((C_word*)t0)[4]),C_fix(0));}

/* k9865 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in ... */
static void C_ccall f_9867(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9867,c,av);}
/* expand.scm:1489: ##sys#extend-macro-environment */
t2=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[259];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a9868 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in k8146 in ... */
static void C_ccall f_9869(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_9869,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9873,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* expand.scm:1494: ##sys#check-syntax */
t6=*((C_word*)lf[59]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[259];
av2[3]=t2;
av2[4]=lf[264];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k9871 in a9868 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in k8149 in ... */
static void C_ccall f_9873(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_9873,c,av);}
a=C_alloc(6);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_i_caddr(((C_word*)t0)[2]);
t4=C_u_i_cdr(((C_word*)t0)[2]);
t5=C_u_i_cdr(t4);
t6=C_u_i_cdr(t5);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9885,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=t6,tmp=(C_word)a,a+=6,tmp);
/* expand.scm:1498: r */
t8=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=lf[263];
((C_proc)C_fast_retrieve_proc(t8))(3,av2);}}

/* k9883 in k9871 in a9868 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in k8152 in ... */
static void C_ccall f_9885(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,3)))){
C_save_and_reclaim((void *)f_9885,c,av);}
a=C_alloc(20);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(((C_word*)t0)[2],lf[17]);
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9913,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10027,a[2]=t4,a[3]=t9,a[4]=t5,a[5]=((C_word)li136),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_10027(t11,t7,((C_word*)t0)[2]);}

/* k9911 in k9883 in k9871 in a9868 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in k8156 in ... */
static void C_ccall f_9913(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,2)))){
C_save_and_reclaim((void *)f_9913,c,av);}
a=C_alloc(17);
t2=C_i_car(((C_word*)t0)[2]);
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_eqp(t3,C_SCHEME_END_OF_LIST);
t5=(C_truep(t4)?lf[260]:C_a_i_cons(&a,2,lf[108],t3));
t6=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_9934,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=t5,a[5]=((C_word*)t0)[4],a[6]=t1,a[7]=((C_word*)t0)[5],tmp=(C_word)a,a+=8,tmp);
t7=C_eqp(((C_word*)t0)[6],C_SCHEME_END_OF_LIST);
if(C_truep(t7)){
t8=t6;
f_9934(t8,lf[262]);}
else{
t8=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,((C_word*)t0)[6]);
t9=t6;
f_9934(t9,C_a_i_cons(&a,2,lf[55],t8));}}

/* k9932 in k9911 in k9883 in k9871 in a9868 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in k8160 in ... */
static void C_fcall f_9934(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,0,3)))){
C_save_and_reclaim_args((void *)trf_9934,2,t0,t1);}
a=C_alloc(21);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_9975,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9977,a[2]=t4,a[3]=t8,a[4]=t5,a[5]=((C_word)li135),tmp=(C_word)a,a+=6,tmp));
t10=((C_word*)t8)[1];
f_9977(t10,t6,((C_word*)t0)[7]);}

/* k9973 in k9932 in k9911 in k9883 in k9871 in a9868 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_ccall f_9975(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(39,c,1)))){
C_save_and_reclaim((void *)f_9975,c,av);}
a=C_alloc(39);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,lf[58],t2);
t4=C_a_i_list(&a,3,lf[108],((C_word*)t0)[3],t3);
t5=C_a_i_list(&a,4,lf[261],((C_word*)t0)[4],((C_word*)t0)[5],t4);
t6=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_list(&a,4,lf[55],((C_word*)t0)[2],((C_word*)t0)[7],t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* map-loop2458 in k9932 in k9911 in k9883 in k9871 in a9868 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in k8163 in ... */
static void C_fcall f_9977(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_9977,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9987,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=C_slot(t2,C_fix(0));
t5=C_i_cdr(t4);
t6=C_i_cdr(t5);
t7=C_eqp(t6,C_SCHEME_END_OF_LIST);
if(C_truep(t7)){
t8=C_u_i_car(t4);
t9=t3;
f_9987(t9,C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST));}
else{
t8=C_i_cdr(C_u_i_cdr(t4));
t9=C_i_car(t8);
t10=t3;
f_9987(t10,C_a_i_cons(&a,2,t9,C_SCHEME_END_OF_LIST));}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9985 in map-loop2458 in k9932 in k9911 in k9883 in k9871 in a9868 in k8210 in k8207 in k8204 in k8201 in k8198 in k8195 in k8192 in k8189 in k8186 in k8183 in k8180 in k8176 in k8172 in k8169 in k8166 in ... */
static void C_fcall f_9987(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_9987,2,t0,t1);}
t2=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t1);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t4=((C_word*)((C_word*)t0)[3])[1];
f_9977(t4,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_expand_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("expand"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_expand_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(3348))){
C_save(t1);
C_rereclaim2(3348*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,375);
lf[0]=C_h_intern(&lf[0],6, C_text("expand"));
lf[1]=C_h_intern(&lf[1],15, C_text("chicken.syntax#"));
lf[2]=C_h_intern(&lf[2],14, C_text("##sys#features"));
lf[3]=C_h_intern(&lf[3],29, C_text("##sys#current-source-filename"));
lf[4]=C_h_intern(&lf[4],25, C_text("##sys#current-environment"));
lf[5]=C_h_intern(&lf[5],30, C_text("##sys#current-meta-environment"));
lf[7]=C_h_intern(&lf[7],18, C_text("##core#macro-alias"));
lf[9]=C_h_intern(&lf[9],16, C_text("##core#real-name"));
lf[10]=C_h_intern(&lf[10],19, C_text("chicken.base#gensym"));
lf[11]=C_h_intern(&lf[11],24, C_text("chicken.keyword#keyword\077"));
lf[12]=C_h_intern(&lf[12],27, C_text("chicken.syntax#strip-syntax"));
lf[13]=C_h_intern(&lf[13],18, C_text("scheme#make-vector"));
lf[14]=C_h_intern(&lf[14],15, C_text("##sys#extend-se"));
lf[15]=C_h_intern(&lf[15],8, C_text("for-each"));
lf[16]=C_h_intern(&lf[16],13, C_text("scheme#append"));
lf[17]=C_h_intern(&lf[17],3, C_text("map"));
lf[18]=C_h_intern(&lf[18],15, C_text("##sys#globalize"));
lf[19]=C_h_intern(&lf[19],23, C_text("##sys#alias-global-hook"));
lf[20]=C_h_intern(&lf[20],23, C_text("##sys#macro-environment"));
lf[21]=C_h_intern(&lf[21],30, C_text("##sys#scheme-macro-environment"));
lf[22]=C_h_intern(&lf[22],31, C_text("##sys#chicken-macro-environment"));
lf[23]=C_h_intern(&lf[23],35, C_text("##sys#chicken-ffi-macro-environment"));
lf[24]=C_h_intern(&lf[24],41, C_text("##sys#chicken.condition-macro-environment"));
lf[25]=C_h_intern(&lf[25],36, C_text("##sys#chicken.time-macro-environment"));
lf[26]=C_h_intern(&lf[26],36, C_text("##sys#chicken.type-macro-environment"));
lf[27]=C_h_intern(&lf[27],38, C_text("##sys#chicken.syntax-macro-environment"));
lf[28]=C_h_intern(&lf[28],36, C_text("##sys#chicken.base-macro-environment"));
lf[29]=C_h_intern(&lf[29],24, C_text("##sys#ensure-transformer"));
lf[30]=C_h_intern(&lf[30],11, C_text("transformer"));
lf[31]=C_h_intern(&lf[31],11, C_text("##sys#error"));
lf[32]=C_decode_literal(C_heaptop,C_text("\376B\000\000$expected syntax-transformer, but got"));
lf[33]=C_h_intern(&lf[33],30, C_text("##sys#extend-macro-environment"));
lf[34]=C_h_intern(&lf[34],12, C_text("##sys#macro\077"));
lf[35]=C_h_intern(&lf[35],21, C_text("##sys#undefine-macro!"));
lf[36]=C_h_intern(&lf[36],14, C_text("##sys#expand-0"));
lf[37]=C_h_intern(&lf[37],9, C_text("condition"));
lf[38]=C_h_intern(&lf[38],3, C_text("exn"));
lf[39]=C_h_intern(&lf[39],23, C_text("chicken.condition#abort"));
lf[40]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\003\001exn\376\001\000\000\007\001message"));
lf[41]=C_h_intern(&lf[41],20, C_text("scheme#string-append"));
lf[42]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025during expansion of ("));
lf[43]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010 ...) - "));
lf[44]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\003\001exn\376\001\000\000\007\001message"));
lf[45]=C_h_intern(&lf[45],36, C_text("chicken.syntax#expansion-result-hook"));
lf[46]=C_h_intern(&lf[46],23, C_text("##sys#syntax-error-hook"));
lf[47]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030syntax transformer for `"));
lf[48]=C_decode_literal(C_heaptop,C_text("\376B\000\000@\047 returns original form, which would result in endless expansion"));
lf[49]=C_h_intern(&lf[49],21, C_text("scheme#symbol->string"));
lf[50]=C_h_intern(&lf[50],51, C_text("chicken.internal.syntax-rules#syntax-rules-mismatch"));
lf[51]=C_h_intern(&lf[51],18, C_text("##sys#dynamic-wind"));
lf[52]=C_h_intern(&lf[52],40, C_text("chicken.condition#with-exception-handler"));
lf[53]=C_h_intern(&lf[53],37, C_text("scheme#call-with-current-continuation"));
lf[54]=C_decode_literal(C_heaptop,C_text("\376B\000\000\034invalid syntax in macro form"));
lf[55]=C_h_intern(&lf[55],10, C_text("##core#let"));
lf[56]=C_h_intern(&lf[56],18, C_text("##core#loop-lambda"));
lf[57]=C_h_intern(&lf[57],14, C_text("##core#letrec\052"));
lf[58]=C_h_intern(&lf[58],10, C_text("##core#app"));
lf[59]=C_h_intern(&lf[59],18, C_text("##sys#check-syntax"));
lf[60]=C_h_intern(&lf[60],3, C_text("let"));
lf[61]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\000\000\000\002\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\377\016\376\377\001\000\000\000\000\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000"
"\000\001"));
lf[62]=C_decode_literal(C_heaptop,C_text("\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\002"));
lf[63]=C_h_intern(&lf[63],26, C_text("##sys#compiler-syntax-hook"));
lf[64]=C_h_intern(&lf[64],26, C_text("##compiler#compiler-syntax"));
lf[65]=C_h_intern(&lf[65],27, C_text("##sys#enable-runtime-macros"));
lf[66]=C_h_intern(&lf[66],21, C_text("chicken.syntax#expand"));
lf[67]=C_h_intern(&lf[67],27, C_text("##sys#extended-lambda-list\077"));
lf[68]=C_h_intern(&lf[68],6, C_text("#!rest"));
lf[69]=C_h_intern(&lf[69],10, C_text("#!optional"));
lf[70]=C_h_intern(&lf[70],5, C_text("#!key"));
lf[71]=C_h_intern(&lf[71],33, C_text("##sys#expand-extended-lambda-list"));
lf[72]=C_h_intern(&lf[72],12, C_text("scheme#cadar"));
lf[73]=C_h_intern(&lf[73],14, C_text("scheme#reverse"));
lf[74]=C_h_intern(&lf[74],12, C_text("##sys#append"));
lf[75]=C_h_intern(&lf[75],12, C_text("##core#quote"));
lf[76]=C_h_intern(&lf[76],13, C_text("##core#lambda"));
lf[77]=C_h_intern(&lf[77],17, C_text("##sys#get-keyword"));
lf[78]=C_h_intern(&lf[78],31, C_text("chicken.keyword#string->keyword"));
lf[79]=C_decode_literal(C_heaptop,C_text("\376B\000\000+rest argument list specified more than once"));
lf[80]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032invalid lambda list syntax"));
lf[81]=C_decode_literal(C_heaptop,C_text("\376B\000\000-`#!optional\047 argument marker in wrong context"));
lf[82]=C_h_intern(&lf[82],4, C_text("rest"));
lf[83]=C_decode_literal(C_heaptop,C_text("\376B\000\000#invalid syntax of `#!rest\047 argument"));
lf[84]=C_decode_literal(C_heaptop,C_text("\376B\000\000)`#!rest\047 argument marker in wrong context"));
lf[85]=C_decode_literal(C_heaptop,C_text("\376B\000\000(`#!key\047 argument marker in wrong context"));
lf[86]=C_decode_literal(C_heaptop,C_text("\376B\000\0000invalid lambda list syntax after `#!rest\047 marker"));
lf[87]=C_decode_literal(C_heaptop,C_text("\376B\000\000 invalid required argument syntax"));
lf[88]=C_decode_literal(C_heaptop,C_text("\376B\000\0000invalid lambda list syntax after `#!rest\047 marker"));
lf[89]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032invalid lambda list syntax"));
lf[90]=C_h_intern(&lf[90],14, C_text("let-optionals\052"));
lf[91]=C_h_intern(&lf[91],8, C_text("optional"));
lf[92]=C_h_intern(&lf[92],4, C_text("let\052"));
lf[93]=C_h_intern(&lf[93],31, C_text("##sys#default-macro-environment"));
lf[94]=C_h_intern(&lf[94],39, C_text("##sys#expand-multiple-values-assignment"));
lf[95]=C_h_intern(&lf[95],22, C_text("##sys#call-with-values"));
lf[96]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016\376\377\016"));
lf[97]=C_h_intern(&lf[97],11, C_text("##core#set!"));
lf[98]=C_h_intern(&lf[98],27, C_text("##sys#decompose-lambda-list"));
lf[99]=C_h_intern(&lf[99],32, C_text("chicken.syntax#define-definition"));
lf[100]=C_h_intern(&lf[100],39, C_text("chicken.syntax#define-syntax-definition"));
lf[101]=C_h_intern(&lf[101],39, C_text("chicken.syntax#define-values-definition"));
lf[103]=C_h_intern(&lf[103],23, C_text("##sys#canonicalize-body"));
lf[104]=C_h_intern(&lf[104],6, C_text("define"));
lf[105]=C_h_intern(&lf[105],13, C_text("define-syntax"));
lf[106]=C_h_intern(&lf[106],13, C_text("define-values"));
lf[107]=C_h_intern(&lf[107],6, C_text("import"));
lf[108]=C_h_intern(&lf[108],12, C_text("##core#begin"));
lf[109]=C_h_intern(&lf[109],13, C_text("##core#module"));
lf[110]=C_h_intern(&lf[110],14, C_text("##core#include"));
lf[111]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[112]=C_h_intern(&lf[112],20, C_text("##core#letrec-syntax"));
lf[113]=C_decode_literal(C_heaptop,C_text("\376B\000\000,redefinition of currently used defining form"));
lf[114]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[115]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\010\001variable\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\000"));
lf[116]=C_h_intern(&lf[116],36, C_text("chicken.syntax#expand-curried-define"));
lf[117]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\003\000\000\002\376\001\000\000\001\001_\376\001\000\000\013\001lambda-list\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001"));
lf[118]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001variable\376\001\000\000\013\001lambda-list\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001"));
lf[119]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\000"));
lf[120]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001"));
lf[121]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\013\001lambda-list\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[122]=C_h_intern(&lf[122],31, C_text("chicken.syntax#match-expression"));
lf[123]=C_h_intern(&lf[123],26, C_text("##sys#line-number-database"));
lf[124]=C_h_intern(&lf[124],26, C_text("##sys#syntax-error-culprit"));
lf[125]=C_h_intern(&lf[125],20, C_text("##sys#syntax-context"));
lf[126]=C_h_intern(&lf[126],27, C_text("chicken.syntax#syntax-error"));
lf[127]=C_h_intern(&lf[127],17, C_text("##sys#signal-hook"));
lf[128]=C_h_intern_kw(&lf[128],12, C_text("syntax-error"));
lf[129]=C_h_intern(&lf[129],26, C_text("##sys#syntax-error/context"));
lf[130]=C_h_intern(&lf[130],11, C_text("##sys#print"));
lf[131]=C_h_intern(&lf[131],30, C_text("chicken.base#get-output-string"));
lf[132]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006 ...)\047"));
lf[133]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025\012inside expression `("));
lf[134]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002: "));
lf[135]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027  Suggesting: `(import "));
lf[136]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002)\047"));
lf[137]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025  Suggesting one of:\012"));
lf[138]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[139]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017\012      (import "));
lf[140]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002)\047"));
lf[141]=C_decode_literal(C_heaptop,C_text("\376B\000\000# ...)\047 without importing it first.\012"));
lf[142]=C_decode_literal(C_heaptop,C_text("\376B\000\000-\012\012  Perhaps you intended to use the syntax `("));
lf[143]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002: "));
lf[144]=C_h_intern(&lf[144],6, C_text("syntax"));
lf[145]=C_h_intern(&lf[145],9, C_text("##sys#get"));
lf[146]=C_h_intern(&lf[146],9, C_text("##core#db"));
lf[147]=C_h_intern(&lf[147],31, C_text("chicken.base#open-output-string"));
lf[148]=C_h_intern(&lf[148],30, C_text("chicken.syntax#get-line-number"));
lf[149]=C_h_intern(&lf[149],31, C_text("chicken.internal#hash-table-ref"));
lf[150]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001("));
lf[151]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006) in `"));
lf[152]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004\047 - "));
lf[153]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004in `"));
lf[154]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004\047 - "));
lf[155]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024not enough arguments"));
lf[156]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022too many arguments"));
lf[157]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021not a proper list"));
lf[158]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021unexpected object"));
lf[159]=C_h_intern(&lf[159],1, C_text("_"));
lf[160]=C_h_intern(&lf[160],4, C_text("pair"));
lf[161]=C_h_intern(&lf[161],12, C_text("scheme#pair\077"));
lf[162]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015pair expected"));
lf[163]=C_h_intern(&lf[163],8, C_text("variable"));
lf[164]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023identifier expected"));
lf[165]=C_h_intern(&lf[165],6, C_text("symbol"));
lf[166]=C_h_intern(&lf[166],14, C_text("scheme#symbol\077"));
lf[167]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017symbol expected"));
lf[168]=C_h_intern(&lf[168],4, C_text("list"));
lf[169]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024proper list expected"));
lf[170]=C_h_intern(&lf[170],6, C_text("number"));
lf[171]=C_h_intern(&lf[171],14, C_text("scheme#number\077"));
lf[172]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017number expected"));
lf[173]=C_h_intern(&lf[173],6, C_text("string"));
lf[174]=C_h_intern(&lf[174],14, C_text("scheme#string\077"));
lf[175]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017string expected"));
lf[176]=C_h_intern(&lf[176],11, C_text("lambda-list"));
lf[177]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024lambda-list expected"));
lf[178]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017missing keyword"));
lf[179]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017incomplete form"));
lf[180]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015pair expected"));
lf[182]=C_h_intern(&lf[182],19, C_text("scheme#list->vector"));
lf[183]=C_h_intern(&lf[183],19, C_text("scheme#vector->list"));
lf[184]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033(expand.scm:837) not a list"));
lf[185]=C_h_intern(&lf[185],35, C_text("chicken.syntax#er-macro-transformer"));
lf[186]=C_h_intern(&lf[186],35, C_text("chicken.syntax#ir-macro-transformer"));
lf[187]=C_h_intern(&lf[187],20, C_text("##sys#er-transformer"));
lf[188]=C_h_intern(&lf[188],20, C_text("##sys#ir-transformer"));
lf[189]=C_h_intern(&lf[189],31, C_text("##sys#initial-macro-environment"));
lf[190]=C_h_intern(&lf[190],38, C_text("##sys#chicken.module-macro-environment"));
lf[191]=C_h_intern(&lf[191],30, C_text("chicken.internal.syntax-rules#"));
lf[192]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024no rule matches form"));
lf[193]=C_h_intern(&lf[193],40, C_text("chicken.internal.syntax-rules#drop-right"));
lf[194]=C_h_intern(&lf[194],40, C_text("chicken.internal.syntax-rules#take-right"));
lf[195]=C_h_intern(&lf[195],28, C_text("##sys#meta-macro-environment"));
lf[196]=C_h_intern(&lf[196],27, C_text("chicken.base#make-parameter"));
lf[197]=C_h_intern(&lf[197],40, C_text("chicken.internal#fixup-macro-environment"));
lf[198]=C_h_intern(&lf[198],29, C_text("chicken.internal#macro-subset"));
lf[199]=C_h_intern(&lf[199],12, C_text("syntax-rules"));
lf[200]=C_h_intern(&lf[200],3, C_text("..."));
lf[201]=C_h_intern(&lf[201],9, C_text("##sys#car"));
lf[202]=C_h_intern(&lf[202],9, C_text("##sys#cdr"));
lf[203]=C_h_intern(&lf[203],12, C_text("##sys#length"));
lf[204]=C_h_intern(&lf[204],13, C_text("##sys#vector\077"));
lf[205]=C_h_intern(&lf[205],18, C_text("##sys#vector->list"));
lf[206]=C_h_intern(&lf[206],18, C_text("##sys#list->vector"));
lf[207]=C_h_intern(&lf[207],8, C_text("##sys#>="));
lf[208]=C_h_intern(&lf[208],7, C_text("##sys#="));
lf[209]=C_h_intern(&lf[209],7, C_text("##sys#+"));
lf[210]=C_h_intern(&lf[210],10, C_text("##sys#cons"));
lf[211]=C_h_intern(&lf[211],9, C_text("##sys#eq\077"));
lf[212]=C_h_intern(&lf[212],12, C_text("##sys#equal\077"));
lf[213]=C_h_intern(&lf[213],11, C_text("##sys#list\077"));
lf[214]=C_h_intern(&lf[214],9, C_text("##sys#map"));
lf[215]=C_h_intern(&lf[215],11, C_text("##sys#map-n"));
lf[216]=C_h_intern(&lf[216],11, C_text("##sys#pair\077"));
lf[217]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026ill-formed syntax rule"));
lf[218]=C_h_intern(&lf[218],13, C_text("##core#syntax"));
lf[219]=C_h_intern(&lf[219],5, C_text("quote"));
lf[220]=C_decode_literal(C_heaptop,C_text("\376B\000\000,template dimension error (too few ellipses\077)"));
lf[221]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021too many ellipses"));
lf[222]=C_h_intern(&lf[222],11, C_text("##sys#apply"));
lf[223]=C_decode_literal(C_heaptop,C_text("\376B\000\000%Only one segment per level is allowed"));
lf[224]=C_decode_literal(C_heaptop,C_text("\376B\000\000\047Cannot combine dotted tail and ellipsis"));
lf[225]=C_h_intern(&lf[225],4, C_text("temp"));
lf[226]=C_h_intern(&lf[226],4, C_text("tail"));
lf[227]=C_h_intern(&lf[227],6, C_text("rename"));
lf[228]=C_h_intern(&lf[228],2, C_text("or"));
lf[229]=C_h_intern(&lf[229],4, C_text("loop"));
lf[230]=C_h_intern(&lf[230],6, C_text("lambda"));
lf[231]=C_h_intern(&lf[231],3, C_text("len"));
lf[232]=C_h_intern(&lf[232],1, C_text("l"));
lf[233]=C_h_intern(&lf[233],5, C_text("input"));
lf[234]=C_h_intern(&lf[234],4, C_text("else"));
lf[235]=C_h_intern(&lf[235],4, C_text("cond"));
lf[236]=C_h_intern(&lf[236],7, C_text("compare"));
lf[237]=C_h_intern(&lf[237],3, C_text("and"));
lf[238]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\004\001list\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\000"));
lf[239]=C_decode_literal(C_heaptop,C_text("\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\002"));
lf[240]=C_h_intern(&lf[240],5, C_text("delay"));
lf[241]=C_h_intern(&lf[241],10, C_text("##sys#list"));
lf[242]=C_h_intern(&lf[242],18, C_text("##sys#make-promise"));
lf[243]=C_h_intern(&lf[243],11, C_text("delay-force"));
lf[244]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[245]=C_h_intern(&lf[245],10, C_text("quasiquote"));
lf[246]=C_h_intern(&lf[246],7, C_text("unquote"));
lf[247]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[248]=C_h_intern(&lf[248],16, C_text("unquote-splicing"));
lf[249]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[250]=C_h_intern(&lf[250],1, C_text("a"));
lf[251]=C_h_intern(&lf[251],1, C_text("b"));
lf[252]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\014\001##sys#append\376\003\000\000\002\376\001\000\000\001\001a\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001##core#quote\376\003\000\000\002\376\377\016\376\377\016\376\377\016"));
lf[253]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001a\376\377\016"));
lf[254]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\012\001##sys#cons\376\003\000\000\002\376\001\000\000\001\001a\376\003\000\000\002\376\003\000\000\002\376\001\000\000\012\001##sys#list\376\001\000\000\001\001b\376\377\016"));
lf[255]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001a\376\003\000\000\002\376\001\000\000\001\001b\376\377\016"));
lf[256]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\012\001##sys#cons\376\003\000\000\002\376\001\000\000\001\001a\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001##core#quote\376\003\000\000\002\376\377\016\376\377\016\376\377\016"));
lf[257]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001a\376\377\016"));
lf[258]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[259]=C_h_intern(&lf[259],2, C_text("do"));
lf[260]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[261]=C_h_intern(&lf[261],9, C_text("##core#if"));
lf[262]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[263]=C_h_intern(&lf[263],6, C_text("doloop"));
lf[264]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\000\000\000\002\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\001\376\001\000\000\001\001_\376\377\001\000\000\000\000\376\000\000\000\002\376\001\000"
"\000\001\001_\376\377\001\000\000\000\001"));
lf[265]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\000\000\000\002\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\377\016\376\377\001\000\000\000\000\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000"
"\000\001"));
lf[266]=C_h_intern(&lf[266],4, C_text("case"));
lf[267]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001eqv\077\376\001\000\000\013\001scheme#eqv\077\376\377\016"));
lf[268]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[269]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\014\001##core#begin\376\377\016"));
lf[270]=C_h_intern(&lf[270],10, C_text("##sys#warn"));
lf[271]=C_decode_literal(C_heaptop,C_text("\376B\000\000(clause following `else\047 clause in `case\047"));
lf[272]=C_h_intern(&lf[272],16, C_text("##core#undefined"));
lf[273]=C_decode_literal(C_heaptop,C_text("\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001"));
lf[274]=C_h_intern(&lf[274],4, C_text("eqv\077"));
lf[275]=C_h_intern(&lf[275],2, C_text("=>"));
lf[276]=C_h_intern(&lf[276],3, C_text("tmp"));
lf[277]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\000"));
lf[278]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[279]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\014\001##core#begin\376\377\016"));
lf[280]=C_h_intern(&lf[280],7, C_text("sprintf"));
lf[281]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022\047 clause in `cond\047"));
lf[282]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022clause following `"));
lf[283]=C_h_intern(&lf[283],2, C_text("if"));
lf[284]=C_h_intern(&lf[284],20, C_text("##sys#srfi-4-vector\077"));
lf[285]=C_h_intern(&lf[285],18, C_text("chicken.blob#blob\077"));
lf[286]=C_decode_literal(C_heaptop,C_text("\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001"));
lf[287]=C_h_intern(&lf[287],4, C_text("set!"));
lf[288]=C_h_intern(&lf[288],12, C_text("##sys#setter"));
lf[289]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[290]=C_h_intern(&lf[290],13, C_text("letrec-syntax"));
lf[291]=C_h_intern(&lf[291],44, C_text("chicken.internal#check-for-multiple-bindings"));
lf[292]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015letrec-syntax"));
lf[293]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\000\000\000\002\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\377\016\376\377\001\000\000\000\000\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000"
"\000\001"));
lf[294]=C_h_intern(&lf[294],10, C_text("let-syntax"));
lf[295]=C_h_intern(&lf[295],17, C_text("##core#let-syntax"));
lf[296]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012let-syntax"));
lf[297]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\000\000\000\002\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\377\016\376\377\001\000\000\000\000\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000"
"\000\001"));
lf[298]=C_h_intern(&lf[298],6, C_text("letrec"));
lf[299]=C_h_intern(&lf[299],13, C_text("##core#letrec"));
lf[300]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006letrec"));
lf[301]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\000\000\000\002\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\377\016\376\377\001\000\000\000\000\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000"
"\000\001"));
lf[302]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003let"));
lf[303]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\000\000\000\002\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\377\016\376\377\001\000\000"
"\000\000\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001"));
lf[304]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003let"));
lf[305]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\000\000\000\002\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\377\016\376\377\001\000\000\000\000\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000"
"\000\001"));
lf[306]=C_h_intern(&lf[306],20, C_text("##core#define-syntax"));
lf[307]=C_h_intern(&lf[307],21, C_text("##sys#register-export"));
lf[308]=C_h_intern(&lf[308],20, C_text("##sys#current-module"));
lf[309]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[310]=C_h_intern(&lf[310],33, C_text("##core#ensure-toplevel-definition"));
lf[311]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[312]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\010\001variable\376\000\000\000\003\376\001\000\000\001\001_\376\377\001\000\000\000\000\376\377\001\000\000\000\001"));
lf[313]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\003\000\000\002\376\001\000\000\001\001_\376\001\000\000\013\001lambda-list\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001"));
lf[314]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001variable\376\001\000\000\013\001lambda-list\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001"));
lf[315]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001"));
lf[316]=C_h_intern(&lf[316],5, C_text("begin"));
lf[317]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\000"));
lf[318]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\001\376\001\000\000\001\001_"));
lf[319]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[320]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\013\001lambda-list\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001"));
lf[321]=C_h_intern(&lf[321],14, C_text("current-module"));
lf[322]=C_h_intern(&lf[322],17, C_text("##sys#module-name"));
lf[323]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[324]=C_h_intern(&lf[324],16, C_text("define-interface"));
lf[325]=C_h_intern(&lf[325],16, C_text("##core#interface"));
lf[326]=C_h_intern(&lf[326],18, C_text("##sys#put/restore!"));
lf[327]=C_h_intern(&lf[327],26, C_text("##core#elaborationtimeonly"));
lf[328]=C_h_intern(&lf[328],1, C_text("\052"));
lf[329]=C_h_intern_kw(&lf[329],9, C_text("interface"));
lf[330]=C_h_intern(&lf[330],22, C_text("##sys#validate-exports"));
lf[331]=C_h_intern(&lf[331],17, C_text("syntax-error-hook"));
lf[332]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017invalid exports"));
lf[333]=C_decode_literal(C_heaptop,C_text("\376B\000\000-`\052\047 is not allowed as a name for an interface"));
lf[334]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[335]=C_h_intern(&lf[335],7, C_text("functor"));
lf[336]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030invalid functor argument"));
lf[337]=C_h_intern(&lf[337],41, C_text("chicken.internal#valid-library-specifier\077"));
lf[338]=C_h_intern(&lf[338],22, C_text("##sys#register-functor"));
lf[339]=C_h_intern(&lf[339],6, C_text("scheme"));
lf[340]=C_h_intern(&lf[340],14, C_text("chicken.syntax"));
lf[341]=C_h_intern(&lf[341],16, C_text("begin-for-syntax"));
lf[342]=C_h_intern(&lf[342],27, C_text("chicken.internal#library-id"));
lf[343]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\377\016\376\377\001\000\000\000\000\376\003\000\000\002\376\001\000\000\001\001_"
"\376\001\000\000\001\001_"));
lf[344]=C_h_intern(&lf[344],8, C_text("reexport"));
lf[345]=C_h_intern(&lf[345],19, C_text("##sys#expand-import"));
lf[346]=C_h_intern(&lf[346],6, C_text("export"));
lf[347]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[348]=C_h_intern(&lf[348],24, C_text("##sys#add-to-export-list"));
lf[349]=C_h_intern(&lf[349],6, C_text("module"));
lf[350]=C_h_intern(&lf[350],1, C_text("="));
lf[351]=C_h_intern(&lf[351],21, C_text("scheme#string->symbol"));
lf[352]=C_h_intern(&lf[352],19, C_text("##sys#string-append"));
lf[353]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001_"));
lf[354]=C_h_intern(&lf[354],25, C_text("##sys#instantiate-functor"));
lf[355]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\000\376\377\016"));
lf[356]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\000"));
lf[357]=C_h_intern(&lf[357],11, C_text("cond-expand"));
lf[358]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042syntax error in `cond-expand\047 form"));
lf[359]=C_h_intern(&lf[359],25, C_text("chicken.platform#feature\077"));
lf[360]=C_h_intern(&lf[360],3, C_text("not"));
lf[361]=C_decode_literal(C_heaptop,C_text("\376B\000\000(no matching clause in `cond-expand\047 form"));
lf[362]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[363]=C_h_intern(&lf[363],17, C_text("import-for-syntax"));
lf[364]=C_h_intern(&lf[364],30, C_text("##sys#register-meta-expression"));
lf[365]=C_h_intern(&lf[365],22, C_text("##sys#decompose-import"));
lf[366]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[367]=C_h_intern(&lf[367],14, C_text("##core#require"));
lf[368]=C_h_intern(&lf[368],35, C_text("chicken.internal#module-requirement"));
lf[369]=C_decode_literal(C_heaptop,C_text("\376B\000\000#cannot import from undefined module"));
lf[370]=C_h_intern(&lf[370],12, C_text("##sys#import"));
lf[371]=C_decode_literal(C_heaptop,C_text("\376B\000\0001cannot import from module currently being defined"));
lf[372]=C_h_intern(&lf[372],24, C_text("import-syntax-for-syntax"));
lf[373]=C_h_intern(&lf[373],13, C_text("import-syntax"));
lf[374]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\006\002srfi-0\376\003\000\000\002\376\001\000\000\006\002srfi-2\376\003\000\000\002\376\001\000\000\006\002srfi-6\376\003\000\000\002\376\001\000\000\006\002srfi-9\376\003\000\000\002\376\001\000\000\007\002s"
"rfi-46\376\003\000\000\002\376\001\000\000\007\002srfi-55\376\003\000\000\002\376\001\000\000\007\002srfi-61\376\377\016"));
C_register_lf2(lf,375,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3700,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_internal_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[671] = {
{C_text("f_10027:expand_2escm"),(void*)f_10027},
{C_text("f_10063:expand_2escm"),(void*)f_10063},
{C_text("f_10065:expand_2escm"),(void*)f_10065},
{C_text("f_10069:expand_2escm"),(void*)f_10069},
{C_text("f_10079:expand_2escm"),(void*)f_10079},
{C_text("f_10104:expand_2escm"),(void*)f_10104},
{C_text("f_10114:expand_2escm"),(void*)f_10114},
{C_text("f_10116:expand_2escm"),(void*)f_10116},
{C_text("f_10120:expand_2escm"),(void*)f_10120},
{C_text("f_10128:expand_2escm"),(void*)f_10128},
{C_text("f_10131:expand_2escm"),(void*)f_10131},
{C_text("f_10134:expand_2escm"),(void*)f_10134},
{C_text("f_10137:expand_2escm"),(void*)f_10137},
{C_text("f_10140:expand_2escm"),(void*)f_10140},
{C_text("f_10151:expand_2escm"),(void*)f_10151},
{C_text("f_10153:expand_2escm"),(void*)f_10153},
{C_text("f_10167:expand_2escm"),(void*)f_10167},
{C_text("f_10173:expand_2escm"),(void*)f_10173},
{C_text("f_10176:expand_2escm"),(void*)f_10176},
{C_text("f_10180:expand_2escm"),(void*)f_10180},
{C_text("f_10186:expand_2escm"),(void*)f_10186},
{C_text("f_10189:expand_2escm"),(void*)f_10189},
{C_text("f_10204:expand_2escm"),(void*)f_10204},
{C_text("f_10245:expand_2escm"),(void*)f_10245},
{C_text("f_10249:expand_2escm"),(void*)f_10249},
{C_text("f_10252:expand_2escm"),(void*)f_10252},
{C_text("f_10285:expand_2escm"),(void*)f_10285},
{C_text("f_10300:expand_2escm"),(void*)f_10300},
{C_text("f_10302:expand_2escm"),(void*)f_10302},
{C_text("f_10350:expand_2escm"),(void*)f_10350},
{C_text("f_10352:expand_2escm"),(void*)f_10352},
{C_text("f_10359:expand_2escm"),(void*)f_10359},
{C_text("f_10362:expand_2escm"),(void*)f_10362},
{C_text("f_10365:expand_2escm"),(void*)f_10365},
{C_text("f_10370:expand_2escm"),(void*)f_10370},
{C_text("f_10384:expand_2escm"),(void*)f_10384},
{C_text("f_10390:expand_2escm"),(void*)f_10390},
{C_text("f_10393:expand_2escm"),(void*)f_10393},
{C_text("f_10397:expand_2escm"),(void*)f_10397},
{C_text("f_10403:expand_2escm"),(void*)f_10403},
{C_text("f_10406:expand_2escm"),(void*)f_10406},
{C_text("f_10409:expand_2escm"),(void*)f_10409},
{C_text("f_10412:expand_2escm"),(void*)f_10412},
{C_text("f_10416:expand_2escm"),(void*)f_10416},
{C_text("f_10422:expand_2escm"),(void*)f_10422},
{C_text("f_10425:expand_2escm"),(void*)f_10425},
{C_text("f_10428:expand_2escm"),(void*)f_10428},
{C_text("f_10434:expand_2escm"),(void*)f_10434},
{C_text("f_10460:expand_2escm"),(void*)f_10460},
{C_text("f_10488:expand_2escm"),(void*)f_10488},
{C_text("f_10505:expand_2escm"),(void*)f_10505},
{C_text("f_10511:expand_2escm"),(void*)f_10511},
{C_text("f_10514:expand_2escm"),(void*)f_10514},
{C_text("f_10533:expand_2escm"),(void*)f_10533},
{C_text("f_10551:expand_2escm"),(void*)f_10551},
{C_text("f_10554:expand_2escm"),(void*)f_10554},
{C_text("f_10581:expand_2escm"),(void*)f_10581},
{C_text("f_10608:expand_2escm"),(void*)f_10608},
{C_text("f_10671:expand_2escm"),(void*)f_10671},
{C_text("f_10683:expand_2escm"),(void*)f_10683},
{C_text("f_10699:expand_2escm"),(void*)f_10699},
{C_text("f_10733:expand_2escm"),(void*)f_10733},
{C_text("f_10735:expand_2escm"),(void*)f_10735},
{C_text("f_10758:expand_2escm"),(void*)f_10758},
{C_text("f_10777:expand_2escm"),(void*)f_10777},
{C_text("f_10785:expand_2escm"),(void*)f_10785},
{C_text("f_10787:expand_2escm"),(void*)f_10787},
{C_text("f_10818:expand_2escm"),(void*)f_10818},
{C_text("f_10822:expand_2escm"),(void*)f_10822},
{C_text("f_10824:expand_2escm"),(void*)f_10824},
{C_text("f_10828:expand_2escm"),(void*)f_10828},
{C_text("f_10851:expand_2escm"),(void*)f_10851},
{C_text("f_10866:expand_2escm"),(void*)f_10866},
{C_text("f_10868:expand_2escm"),(void*)f_10868},
{C_text("f_10872:expand_2escm"),(void*)f_10872},
{C_text("f_10875:expand_2escm"),(void*)f_10875},
{C_text("f_10888:expand_2escm"),(void*)f_10888},
{C_text("f_10890:expand_2escm"),(void*)f_10890},
{C_text("f_10894:expand_2escm"),(void*)f_10894},
{C_text("f_10897:expand_2escm"),(void*)f_10897},
{C_text("f_10910:expand_2escm"),(void*)f_10910},
{C_text("f_10912:expand_2escm"),(void*)f_10912},
{C_text("f_10916:expand_2escm"),(void*)f_10916},
{C_text("f_10919:expand_2escm"),(void*)f_10919},
{C_text("f_10932:expand_2escm"),(void*)f_10932},
{C_text("f_10934:expand_2escm"),(void*)f_10934},
{C_text("f_10938:expand_2escm"),(void*)f_10938},
{C_text("f_10949:expand_2escm"),(void*)f_10949},
{C_text("f_10959:expand_2escm"),(void*)f_10959},
{C_text("f_10984:expand_2escm"),(void*)f_10984},
{C_text("f_10986:expand_2escm"),(void*)f_10986},
{C_text("f_10990:expand_2escm"),(void*)f_10990},
{C_text("f_11007:expand_2escm"),(void*)f_11007},
{C_text("f_11010:expand_2escm"),(void*)f_11010},
{C_text("f_11016:expand_2escm"),(void*)f_11016},
{C_text("f_11023:expand_2escm"),(void*)f_11023},
{C_text("f_11027:expand_2escm"),(void*)f_11027},
{C_text("f_11031:expand_2escm"),(void*)f_11031},
{C_text("f_11033:expand_2escm"),(void*)f_11033},
{C_text("f_11037:expand_2escm"),(void*)f_11037},
{C_text("f_11042:expand_2escm"),(void*)f_11042},
{C_text("f_11057:expand_2escm"),(void*)f_11057},
{C_text("f_11068:expand_2escm"),(void*)f_11068},
{C_text("f_11071:expand_2escm"),(void*)f_11071},
{C_text("f_11093:expand_2escm"),(void*)f_11093},
{C_text("f_11100:expand_2escm"),(void*)f_11100},
{C_text("f_11104:expand_2escm"),(void*)f_11104},
{C_text("f_11113:expand_2escm"),(void*)f_11113},
{C_text("f_11120:expand_2escm"),(void*)f_11120},
{C_text("f_11123:expand_2escm"),(void*)f_11123},
{C_text("f_11158:expand_2escm"),(void*)f_11158},
{C_text("f_11160:expand_2escm"),(void*)f_11160},
{C_text("f_11164:expand_2escm"),(void*)f_11164},
{C_text("f_11175:expand_2escm"),(void*)f_11175},
{C_text("f_11177:expand_2escm"),(void*)f_11177},
{C_text("f_11181:expand_2escm"),(void*)f_11181},
{C_text("f_11192:expand_2escm"),(void*)f_11192},
{C_text("f_11194:expand_2escm"),(void*)f_11194},
{C_text("f_11198:expand_2escm"),(void*)f_11198},
{C_text("f_11209:expand_2escm"),(void*)f_11209},
{C_text("f_11211:expand_2escm"),(void*)f_11211},
{C_text("f_11215:expand_2escm"),(void*)f_11215},
{C_text("f_11226:expand_2escm"),(void*)f_11226},
{C_text("f_11228:expand_2escm"),(void*)f_11228},
{C_text("f_11232:expand_2escm"),(void*)f_11232},
{C_text("f_11235:expand_2escm"),(void*)f_11235},
{C_text("f_11245:expand_2escm"),(void*)f_11245},
{C_text("f_11249:expand_2escm"),(void*)f_11249},
{C_text("f_11251:expand_2escm"),(void*)f_11251},
{C_text("f_11255:expand_2escm"),(void*)f_11255},
{C_text("f_11258:expand_2escm"),(void*)f_11258},
{C_text("f_11261:expand_2escm"),(void*)f_11261},
{C_text("f_11284:expand_2escm"),(void*)f_11284},
{C_text("f_11287:expand_2escm"),(void*)f_11287},
{C_text("f_11333:expand_2escm"),(void*)f_11333},
{C_text("f_11335:expand_2escm"),(void*)f_11335},
{C_text("f_11339:expand_2escm"),(void*)f_11339},
{C_text("f_11342:expand_2escm"),(void*)f_11342},
{C_text("f_11365:expand_2escm"),(void*)f_11365},
{C_text("f_11393:expand_2escm"),(void*)f_11393},
{C_text("f_11398:expand_2escm"),(void*)f_11398},
{C_text("f_11405:expand_2escm"),(void*)f_11405},
{C_text("f_11408:expand_2escm"),(void*)f_11408},
{C_text("f_11417:expand_2escm"),(void*)f_11417},
{C_text("f_11462:expand_2escm"),(void*)f_11462},
{C_text("f_11464:expand_2escm"),(void*)f_11464},
{C_text("f_11489:expand_2escm"),(void*)f_11489},
{C_text("f_11500:expand_2escm"),(void*)f_11500},
{C_text("f_11504:expand_2escm"),(void*)f_11504},
{C_text("f_11506:expand_2escm"),(void*)f_11506},
{C_text("f_11514:expand_2escm"),(void*)f_11514},
{C_text("f_11516:expand_2escm"),(void*)f_11516},
{C_text("f_11520:expand_2escm"),(void*)f_11520},
{C_text("f_11523:expand_2escm"),(void*)f_11523},
{C_text("f_11526:expand_2escm"),(void*)f_11526},
{C_text("f_11533:expand_2escm"),(void*)f_11533},
{C_text("f_11541:expand_2escm"),(void*)f_11541},
{C_text("f_11543:expand_2escm"),(void*)f_11543},
{C_text("f_11547:expand_2escm"),(void*)f_11547},
{C_text("f_11553:expand_2escm"),(void*)f_11553},
{C_text("f_11559:expand_2escm"),(void*)f_11559},
{C_text("f_11562:expand_2escm"),(void*)f_11562},
{C_text("f_11574:expand_2escm"),(void*)f_11574},
{C_text("f_11577:expand_2escm"),(void*)f_11577},
{C_text("f_11608:expand_2escm"),(void*)f_11608},
{C_text("f_11612:expand_2escm"),(void*)f_11612},
{C_text("f_11615:expand_2escm"),(void*)f_11615},
{C_text("f_11622:expand_2escm"),(void*)f_11622},
{C_text("f_11631:expand_2escm"),(void*)f_11631},
{C_text("f_11656:expand_2escm"),(void*)f_11656},
{C_text("f_11690:expand_2escm"),(void*)f_11690},
{C_text("f_11704:expand_2escm"),(void*)f_11704},
{C_text("f_11716:expand_2escm"),(void*)f_11716},
{C_text("f_11718:expand_2escm"),(void*)f_11718},
{C_text("f_11724:expand_2escm"),(void*)f_11724},
{C_text("f_11734:expand_2escm"),(void*)f_11734},
{C_text("f_11748:expand_2escm"),(void*)f_11748},
{C_text("f_11764:expand_2escm"),(void*)f_11764},
{C_text("f_11788:expand_2escm"),(void*)f_11788},
{C_text("f_11823:expand_2escm"),(void*)f_11823},
{C_text("f_11857:expand_2escm"),(void*)f_11857},
{C_text("f_11879:expand_2escm"),(void*)f_11879},
{C_text("f_11904:expand_2escm"),(void*)f_11904},
{C_text("f_11906:expand_2escm"),(void*)f_11906},
{C_text("f_11985:expand_2escm"),(void*)f_11985},
{C_text("f_11997:expand_2escm"),(void*)f_11997},
{C_text("f_12009:expand_2escm"),(void*)f_12009},
{C_text("f_12011:expand_2escm"),(void*)f_12011},
{C_text("f_12015:expand_2escm"),(void*)f_12015},
{C_text("f_12026:expand_2escm"),(void*)f_12026},
{C_text("f_12036:expand_2escm"),(void*)f_12036},
{C_text("f_12044:expand_2escm"),(void*)f_12044},
{C_text("f_12046:expand_2escm"),(void*)f_12046},
{C_text("f_12055:expand_2escm"),(void*)f_12055},
{C_text("f_12061:expand_2escm"),(void*)f_12061},
{C_text("f_12067:expand_2escm"),(void*)f_12067},
{C_text("f_12071:expand_2escm"),(void*)f_12071},
{C_text("f_12074:expand_2escm"),(void*)f_12074},
{C_text("f_12077:expand_2escm"),(void*)f_12077},
{C_text("f_12090:expand_2escm"),(void*)f_12090},
{C_text("f_12112:expand_2escm"),(void*)f_12112},
{C_text("f_12121:expand_2escm"),(void*)f_12121},
{C_text("f_12123:expand_2escm"),(void*)f_12123},
{C_text("f_12148:expand_2escm"),(void*)f_12148},
{C_text("f_12159:expand_2escm"),(void*)f_12159},
{C_text("f_12161:expand_2escm"),(void*)f_12161},
{C_text("f_12169:expand_2escm"),(void*)f_12169},
{C_text("f_12171:expand_2escm"),(void*)f_12171},
{C_text("f_3700:expand_2escm"),(void*)f_3700},
{C_text("f_3703:expand_2escm"),(void*)f_3703},
{C_text("f_3707:expand_2escm"),(void*)f_3707},
{C_text("f_3712:expand_2escm"),(void*)f_3712},
{C_text("f_3716:expand_2escm"),(void*)f_3716},
{C_text("f_3718:expand_2escm"),(void*)f_3718},
{C_text("f_3735:expand_2escm"),(void*)f_3735},
{C_text("f_3742:expand_2escm"),(void*)f_3742},
{C_text("f_3748:expand_2escm"),(void*)f_3748},
{C_text("f_3782:expand_2escm"),(void*)f_3782},
{C_text("f_3788:expand_2escm"),(void*)f_3788},
{C_text("f_3804:expand_2escm"),(void*)f_3804},
{C_text("f_3859:expand_2escm"),(void*)f_3859},
{C_text("f_3866:expand_2escm"),(void*)f_3866},
{C_text("f_3884:expand_2escm"),(void*)f_3884},
{C_text("f_3893:expand_2escm"),(void*)f_3893},
{C_text("f_3914:expand_2escm"),(void*)f_3914},
{C_text("f_3924:expand_2escm"),(void*)f_3924},
{C_text("f_3928:expand_2escm"),(void*)f_3928},
{C_text("f_3953:expand_2escm"),(void*)f_3953},
{C_text("f_3968:expand_2escm"),(void*)f_3968},
{C_text("f_3970:expand_2escm"),(void*)f_3970},
{C_text("f_4018:expand_2escm"),(void*)f_4018},
{C_text("f_4067:expand_2escm"),(void*)f_4067},
{C_text("f_4092:expand_2escm"),(void*)f_4092},
{C_text("f_4104:expand_2escm"),(void*)f_4104},
{C_text("f_4110:expand_2escm"),(void*)f_4110},
{C_text("f_4126:expand_2escm"),(void*)f_4126},
{C_text("f_4144:expand_2escm"),(void*)f_4144},
{C_text("f_4188:expand_2escm"),(void*)f_4188},
{C_text("f_4198:expand_2escm"),(void*)f_4198},
{C_text("f_4222:expand_2escm"),(void*)f_4222},
{C_text("f_4226:expand_2escm"),(void*)f_4226},
{C_text("f_4229:expand_2escm"),(void*)f_4229},
{C_text("f_4236:expand_2escm"),(void*)f_4236},
{C_text("f_4254:expand_2escm"),(void*)f_4254},
{C_text("f_4264:expand_2escm"),(void*)f_4264},
{C_text("f_4268:expand_2escm"),(void*)f_4268},
{C_text("f_4290:expand_2escm"),(void*)f_4290},
{C_text("f_4301:expand_2escm"),(void*)f_4301},
{C_text("f_4309:expand_2escm"),(void*)f_4309},
{C_text("f_4313:expand_2escm"),(void*)f_4313},
{C_text("f_4315:expand_2escm"),(void*)f_4315},
{C_text("f_4338:expand_2escm"),(void*)f_4338},
{C_text("f_4346:expand_2escm"),(void*)f_4346},
{C_text("f_4349:expand_2escm"),(void*)f_4349},
{C_text("f_4359:expand_2escm"),(void*)f_4359},
{C_text("f_4364:expand_2escm"),(void*)f_4364},
{C_text("f_4370:expand_2escm"),(void*)f_4370},
{C_text("f_4376:expand_2escm"),(void*)f_4376},
{C_text("f_4398:expand_2escm"),(void*)f_4398},
{C_text("f_4404:expand_2escm"),(void*)f_4404},
{C_text("f_4421:expand_2escm"),(void*)f_4421},
{C_text("f_4432:expand_2escm"),(void*)f_4432},
{C_text("f_4471:expand_2escm"),(void*)f_4471},
{C_text("f_4477:expand_2escm"),(void*)f_4477},
{C_text("f_4481:expand_2escm"),(void*)f_4481},
{C_text("f_4484:expand_2escm"),(void*)f_4484},
{C_text("f_4500:expand_2escm"),(void*)f_4500},
{C_text("f_4504:expand_2escm"),(void*)f_4504},
{C_text("f_4511:expand_2escm"),(void*)f_4511},
{C_text("f_4517:expand_2escm"),(void*)f_4517},
{C_text("f_4522:expand_2escm"),(void*)f_4522},
{C_text("f_4528:expand_2escm"),(void*)f_4528},
{C_text("f_4536:expand_2escm"),(void*)f_4536},
{C_text("f_4542:expand_2escm"),(void*)f_4542},
{C_text("f_4548:expand_2escm"),(void*)f_4548},
{C_text("f_4574:expand_2escm"),(void*)f_4574},
{C_text("f_4592:expand_2escm"),(void*)f_4592},
{C_text("f_4616:expand_2escm"),(void*)f_4616},
{C_text("f_4625:expand_2escm"),(void*)f_4625},
{C_text("f_4637:expand_2escm"),(void*)f_4637},
{C_text("f_4662:expand_2escm"),(void*)f_4662},
{C_text("f_4664:expand_2escm"),(void*)f_4664},
{C_text("f_4727:expand_2escm"),(void*)f_4727},
{C_text("f_4733:expand_2escm"),(void*)f_4733},
{C_text("f_4771:expand_2escm"),(void*)f_4771},
{C_text("f_4775:expand_2escm"),(void*)f_4775},
{C_text("f_4779:expand_2escm"),(void*)f_4779},
{C_text("f_4791:expand_2escm"),(void*)f_4791},
{C_text("f_4834:expand_2escm"),(void*)f_4834},
{C_text("f_4844:expand_2escm"),(void*)f_4844},
{C_text("f_4847:expand_2escm"),(void*)f_4847},
{C_text("f_4851:expand_2escm"),(void*)f_4851},
{C_text("f_4865:expand_2escm"),(void*)f_4865},
{C_text("f_4871:expand_2escm"),(void*)f_4871},
{C_text("f_4877:expand_2escm"),(void*)f_4877},
{C_text("f_4913:expand_2escm"),(void*)f_4913},
{C_text("f_4919:expand_2escm"),(void*)f_4919},
{C_text("f_4960:expand_2escm"),(void*)f_4960},
{C_text("f_4963:expand_2escm"),(void*)f_4963},
{C_text("f_4980:expand_2escm"),(void*)f_4980},
{C_text("f_4983:expand_2escm"),(void*)f_4983},
{C_text("f_4986:expand_2escm"),(void*)f_4986},
{C_text("f_4991:expand_2escm"),(void*)f_4991},
{C_text("f_5005:expand_2escm"),(void*)f_5005},
{C_text("f_5009:expand_2escm"),(void*)f_5009},
{C_text("f_5021:expand_2escm"),(void*)f_5021},
{C_text("f_5052:expand_2escm"),(void*)f_5052},
{C_text("f_5077:expand_2escm"),(void*)f_5077},
{C_text("f_5096:expand_2escm"),(void*)f_5096},
{C_text("f_5100:expand_2escm"),(void*)f_5100},
{C_text("f_5149:expand_2escm"),(void*)f_5149},
{C_text("f_5202:expand_2escm"),(void*)f_5202},
{C_text("f_5206:expand_2escm"),(void*)f_5206},
{C_text("f_5209:expand_2escm"),(void*)f_5209},
{C_text("f_5212:expand_2escm"),(void*)f_5212},
{C_text("f_5214:expand_2escm"),(void*)f_5214},
{C_text("f_5239:expand_2escm"),(void*)f_5239},
{C_text("f_5253:expand_2escm"),(void*)f_5253},
{C_text("f_5294:expand_2escm"),(void*)f_5294},
{C_text("f_5307:expand_2escm"),(void*)f_5307},
{C_text("f_5323:expand_2escm"),(void*)f_5323},
{C_text("f_5344:expand_2escm"),(void*)f_5344},
{C_text("f_5383:expand_2escm"),(void*)f_5383},
{C_text("f_5402:expand_2escm"),(void*)f_5402},
{C_text("f_5470:expand_2escm"),(void*)f_5470},
{C_text("f_5569:expand_2escm"),(void*)f_5569},
{C_text("f_5575:expand_2escm"),(void*)f_5575},
{C_text("f_5579:expand_2escm"),(void*)f_5579},
{C_text("f_5582:expand_2escm"),(void*)f_5582},
{C_text("f_5601:expand_2escm"),(void*)f_5601},
{C_text("f_5605:expand_2escm"),(void*)f_5605},
{C_text("f_5623:expand_2escm"),(void*)f_5623},
{C_text("f_5645:expand_2escm"),(void*)f_5645},
{C_text("f_5711:expand_2escm"),(void*)f_5711},
{C_text("f_5736:expand_2escm"),(void*)f_5736},
{C_text("f_5749:expand_2escm"),(void*)f_5749},
{C_text("f_5753:expand_2escm"),(void*)f_5753},
{C_text("f_5764:expand_2escm"),(void*)f_5764},
{C_text("f_5847:expand_2escm"),(void*)f_5847},
{C_text("f_5859:expand_2escm"),(void*)f_5859},
{C_text("f_5873:expand_2escm"),(void*)f_5873},
{C_text("f_5878:expand_2escm"),(void*)f_5878},
{C_text("f_5889:expand_2escm"),(void*)f_5889},
{C_text("f_5909:expand_2escm"),(void*)f_5909},
{C_text("f_5913:expand_2escm"),(void*)f_5913},
{C_text("f_5921:expand_2escm"),(void*)f_5921},
{C_text("f_5928:expand_2escm"),(void*)f_5928},
{C_text("f_5940:expand_2escm"),(void*)f_5940},
{C_text("f_5947:expand_2escm"),(void*)f_5947},
{C_text("f_5951:expand_2escm"),(void*)f_5951},
{C_text("f_5989:expand_2escm"),(void*)f_5989},
{C_text("f_6100:expand_2escm"),(void*)f_6100},
{C_text("f_6103:expand_2escm"),(void*)f_6103},
{C_text("f_6109:expand_2escm"),(void*)f_6109},
{C_text("f_6113:expand_2escm"),(void*)f_6113},
{C_text("f_6135:expand_2escm"),(void*)f_6135},
{C_text("f_6138:expand_2escm"),(void*)f_6138},
{C_text("f_6141:expand_2escm"),(void*)f_6141},
{C_text("f_6144:expand_2escm"),(void*)f_6144},
{C_text("f_6146:expand_2escm"),(void*)f_6146},
{C_text("f_6153:expand_2escm"),(void*)f_6153},
{C_text("f_6179:expand_2escm"),(void*)f_6179},
{C_text("f_6208:expand_2escm"),(void*)f_6208},
{C_text("f_6242:expand_2escm"),(void*)f_6242},
{C_text("f_6266:expand_2escm"),(void*)f_6266},
{C_text("f_6268:expand_2escm"),(void*)f_6268},
{C_text("f_6272:expand_2escm"),(void*)f_6272},
{C_text("f_6284:expand_2escm"),(void*)f_6284},
{C_text("f_6292:expand_2escm"),(void*)f_6292},
{C_text("f_6294:expand_2escm"),(void*)f_6294},
{C_text("f_6316:expand_2escm"),(void*)f_6316},
{C_text("f_6319:expand_2escm"),(void*)f_6319},
{C_text("f_6321:expand_2escm"),(void*)f_6321},
{C_text("f_6368:expand_2escm"),(void*)f_6368},
{C_text("f_6372:expand_2escm"),(void*)f_6372},
{C_text("f_6440:expand_2escm"),(void*)f_6440},
{C_text("f_6446:expand_2escm"),(void*)f_6446},
{C_text("f_6466:expand_2escm"),(void*)f_6466},
{C_text("f_6484:expand_2escm"),(void*)f_6484},
{C_text("f_6489:expand_2escm"),(void*)f_6489},
{C_text("f_6502:expand_2escm"),(void*)f_6502},
{C_text("f_6505:expand_2escm"),(void*)f_6505},
{C_text("f_6555:expand_2escm"),(void*)f_6555},
{C_text("f_6562:expand_2escm"),(void*)f_6562},
{C_text("f_6569:expand_2escm"),(void*)f_6569},
{C_text("f_6621:expand_2escm"),(void*)f_6621},
{C_text("f_6633:expand_2escm"),(void*)f_6633},
{C_text("f_6669:expand_2escm"),(void*)f_6669},
{C_text("f_6685:expand_2escm"),(void*)f_6685},
{C_text("f_6755:expand_2escm"),(void*)f_6755},
{C_text("f_6758:expand_2escm"),(void*)f_6758},
{C_text("f_6772:expand_2escm"),(void*)f_6772},
{C_text("f_6812:expand_2escm"),(void*)f_6812},
{C_text("f_6835:expand_2escm"),(void*)f_6835},
{C_text("f_6837:expand_2escm"),(void*)f_6837},
{C_text("f_6840:expand_2escm"),(void*)f_6840},
{C_text("f_6884:expand_2escm"),(void*)f_6884},
{C_text("f_6892:expand_2escm"),(void*)f_6892},
{C_text("f_6900:expand_2escm"),(void*)f_6900},
{C_text("f_6903:expand_2escm"),(void*)f_6903},
{C_text("f_6914:expand_2escm"),(void*)f_6914},
{C_text("f_6919:expand_2escm"),(void*)f_6919},
{C_text("f_6939:expand_2escm"),(void*)f_6939},
{C_text("f_6943:expand_2escm"),(void*)f_6943},
{C_text("f_6958:expand_2escm"),(void*)f_6958},
{C_text("f_6970:expand_2escm"),(void*)f_6970},
{C_text("f_6972:expand_2escm"),(void*)f_6972},
{C_text("f_6979:expand_2escm"),(void*)f_6979},
{C_text("f_6986:expand_2escm"),(void*)f_6986},
{C_text("f_6988:expand_2escm"),(void*)f_6988},
{C_text("f_6998:expand_2escm"),(void*)f_6998},
{C_text("f_7001:expand_2escm"),(void*)f_7001},
{C_text("f_7004:expand_2escm"),(void*)f_7004},
{C_text("f_7007:expand_2escm"),(void*)f_7007},
{C_text("f_7010:expand_2escm"),(void*)f_7010},
{C_text("f_7017:expand_2escm"),(void*)f_7017},
{C_text("f_7024:expand_2escm"),(void*)f_7024},
{C_text("f_7027:expand_2escm"),(void*)f_7027},
{C_text("f_7036:expand_2escm"),(void*)f_7036},
{C_text("f_7039:expand_2escm"),(void*)f_7039},
{C_text("f_7042:expand_2escm"),(void*)f_7042},
{C_text("f_7045:expand_2escm"),(void*)f_7045},
{C_text("f_7048:expand_2escm"),(void*)f_7048},
{C_text("f_7051:expand_2escm"),(void*)f_7051},
{C_text("f_7064:expand_2escm"),(void*)f_7064},
{C_text("f_7068:expand_2escm"),(void*)f_7068},
{C_text("f_7079:expand_2escm"),(void*)f_7079},
{C_text("f_7083:expand_2escm"),(void*)f_7083},
{C_text("f_7085:expand_2escm"),(void*)f_7085},
{C_text("f_7099:expand_2escm"),(void*)f_7099},
{C_text("f_7103:expand_2escm"),(void*)f_7103},
{C_text("f_7124:expand_2escm"),(void*)f_7124},
{C_text("f_7144:expand_2escm"),(void*)f_7144},
{C_text("f_7148:expand_2escm"),(void*)f_7148},
{C_text("f_7163:expand_2escm"),(void*)f_7163},
{C_text("f_7173:expand_2escm"),(void*)f_7173},
{C_text("f_7178:expand_2escm"),(void*)f_7178},
{C_text("f_7185:expand_2escm"),(void*)f_7185},
{C_text("f_7190:expand_2escm"),(void*)f_7190},
{C_text("f_7194:expand_2escm"),(void*)f_7194},
{C_text("f_7201:expand_2escm"),(void*)f_7201},
{C_text("f_7208:expand_2escm"),(void*)f_7208},
{C_text("f_7215:expand_2escm"),(void*)f_7215},
{C_text("f_7217:expand_2escm"),(void*)f_7217},
{C_text("f_7221:expand_2escm"),(void*)f_7221},
{C_text("f_7229:expand_2escm"),(void*)f_7229},
{C_text("f_7262:expand_2escm"),(void*)f_7262},
{C_text("f_7268:expand_2escm"),(void*)f_7268},
{C_text("f_7274:expand_2escm"),(void*)f_7274},
{C_text("f_7295:expand_2escm"),(void*)f_7295},
{C_text("f_7300:expand_2escm"),(void*)f_7300},
{C_text("f_7319:expand_2escm"),(void*)f_7319},
{C_text("f_7324:expand_2escm"),(void*)f_7324},
{C_text("f_7343:expand_2escm"),(void*)f_7343},
{C_text("f_7497:expand_2escm"),(void*)f_7497},
{C_text("f_7554:expand_2escm"),(void*)f_7554},
{C_text("f_7607:expand_2escm"),(void*)f_7607},
{C_text("f_7613:expand_2escm"),(void*)f_7613},
{C_text("f_7620:expand_2escm"),(void*)f_7620},
{C_text("f_7622:expand_2escm"),(void*)f_7622},
{C_text("f_7636:expand_2escm"),(void*)f_7636},
{C_text("f_7640:expand_2escm"),(void*)f_7640},
{C_text("f_7657:expand_2escm"),(void*)f_7657},
{C_text("f_7661:expand_2escm"),(void*)f_7661},
{C_text("f_7670:expand_2escm"),(void*)f_7670},
{C_text("f_7690:expand_2escm"),(void*)f_7690},
{C_text("f_7710:expand_2escm"),(void*)f_7710},
{C_text("f_7732:expand_2escm"),(void*)f_7732},
{C_text("f_7769:expand_2escm"),(void*)f_7769},
{C_text("f_7790:expand_2escm"),(void*)f_7790},
{C_text("f_7813:expand_2escm"),(void*)f_7813},
{C_text("f_7821:expand_2escm"),(void*)f_7821},
{C_text("f_7829:expand_2escm"),(void*)f_7829},
{C_text("f_7851:expand_2escm"),(void*)f_7851},
{C_text("f_7866:expand_2escm"),(void*)f_7866},
{C_text("f_7879:expand_2escm"),(void*)f_7879},
{C_text("f_7894:expand_2escm"),(void*)f_7894},
{C_text("f_7932:expand_2escm"),(void*)f_7932},
{C_text("f_7957:expand_2escm"),(void*)f_7957},
{C_text("f_7971:expand_2escm"),(void*)f_7971},
{C_text("f_7975:expand_2escm"),(void*)f_7975},
{C_text("f_7992:expand_2escm"),(void*)f_7992},
{C_text("f_7996:expand_2escm"),(void*)f_7996},
{C_text("f_8005:expand_2escm"),(void*)f_8005},
{C_text("f_8091:expand_2escm"),(void*)f_8091},
{C_text("f_8095:expand_2escm"),(void*)f_8095},
{C_text("f_8100:expand_2escm"),(void*)f_8100},
{C_text("f_8106:expand_2escm"),(void*)f_8106},
{C_text("f_8119:expand_2escm"),(void*)f_8119},
{C_text("f_8122:expand_2escm"),(void*)f_8122},
{C_text("f_8126:expand_2escm"),(void*)f_8126},
{C_text("f_8129:expand_2escm"),(void*)f_8129},
{C_text("f_8132:expand_2escm"),(void*)f_8132},
{C_text("f_8136:expand_2escm"),(void*)f_8136},
{C_text("f_8139:expand_2escm"),(void*)f_8139},
{C_text("f_8142:expand_2escm"),(void*)f_8142},
{C_text("f_8145:expand_2escm"),(void*)f_8145},
{C_text("f_8148:expand_2escm"),(void*)f_8148},
{C_text("f_8151:expand_2escm"),(void*)f_8151},
{C_text("f_8154:expand_2escm"),(void*)f_8154},
{C_text("f_8158:expand_2escm"),(void*)f_8158},
{C_text("f_8162:expand_2escm"),(void*)f_8162},
{C_text("f_8165:expand_2escm"),(void*)f_8165},
{C_text("f_8168:expand_2escm"),(void*)f_8168},
{C_text("f_8171:expand_2escm"),(void*)f_8171},
{C_text("f_8174:expand_2escm"),(void*)f_8174},
{C_text("f_8178:expand_2escm"),(void*)f_8178},
{C_text("f_8182:expand_2escm"),(void*)f_8182},
{C_text("f_8185:expand_2escm"),(void*)f_8185},
{C_text("f_8188:expand_2escm"),(void*)f_8188},
{C_text("f_8191:expand_2escm"),(void*)f_8191},
{C_text("f_8194:expand_2escm"),(void*)f_8194},
{C_text("f_8197:expand_2escm"),(void*)f_8197},
{C_text("f_8200:expand_2escm"),(void*)f_8200},
{C_text("f_8203:expand_2escm"),(void*)f_8203},
{C_text("f_8206:expand_2escm"),(void*)f_8206},
{C_text("f_8209:expand_2escm"),(void*)f_8209},
{C_text("f_8212:expand_2escm"),(void*)f_8212},
{C_text("f_8215:expand_2escm"),(void*)f_8215},
{C_text("f_8218:expand_2escm"),(void*)f_8218},
{C_text("f_8221:expand_2escm"),(void*)f_8221},
{C_text("f_8224:expand_2escm"),(void*)f_8224},
{C_text("f_8226:expand_2escm"),(void*)f_8226},
{C_text("f_8232:expand_2escm"),(void*)f_8232},
{C_text("f_8242:expand_2escm"),(void*)f_8242},
{C_text("f_8260:expand_2escm"),(void*)f_8260},
{C_text("f_8268:expand_2escm"),(void*)f_8268},
{C_text("f_8278:expand_2escm"),(void*)f_8278},
{C_text("f_8305:expand_2escm"),(void*)f_8305},
{C_text("f_8320:expand_2escm"),(void*)f_8320},
{C_text("f_8324:expand_2escm"),(void*)f_8324},
{C_text("f_8329:expand_2escm"),(void*)f_8329},
{C_text("f_8335:expand_2escm"),(void*)f_8335},
{C_text("f_8339:expand_2escm"),(void*)f_8339},
{C_text("f_8343:expand_2escm"),(void*)f_8343},
{C_text("f_8347:expand_2escm"),(void*)f_8347},
{C_text("f_8351:expand_2escm"),(void*)f_8351},
{C_text("f_8355:expand_2escm"),(void*)f_8355},
{C_text("f_8360:expand_2escm"),(void*)f_8360},
{C_text("f_8367:expand_2escm"),(void*)f_8367},
{C_text("f_8372:expand_2escm"),(void*)f_8372},
{C_text("f_8376:expand_2escm"),(void*)f_8376},
{C_text("f_8380:expand_2escm"),(void*)f_8380},
{C_text("f_8384:expand_2escm"),(void*)f_8384},
{C_text("f_8389:expand_2escm"),(void*)f_8389},
{C_text("f_8393:expand_2escm"),(void*)f_8393},
{C_text("f_8397:expand_2escm"),(void*)f_8397},
{C_text("f_8401:expand_2escm"),(void*)f_8401},
{C_text("f_8403:expand_2escm"),(void*)f_8403},
{C_text("f_8409:expand_2escm"),(void*)f_8409},
{C_text("f_8437:expand_2escm"),(void*)f_8437},
{C_text("f_8447:expand_2escm"),(void*)f_8447},
{C_text("f_8461:expand_2escm"),(void*)f_8461},
{C_text("f_8486:expand_2escm"),(void*)f_8486},
{C_text("f_8503:expand_2escm"),(void*)f_8503},
{C_text("f_8510:expand_2escm"),(void*)f_8510},
{C_text("f_8531:expand_2escm"),(void*)f_8531},
{C_text("f_8535:expand_2escm"),(void*)f_8535},
{C_text("f_8539:expand_2escm"),(void*)f_8539},
{C_text("f_8541:expand_2escm"),(void*)f_8541},
{C_text("f_8546:expand_2escm"),(void*)f_8546},
{C_text("f_8569:expand_2escm"),(void*)f_8569},
{C_text("f_8603:expand_2escm"),(void*)f_8603},
{C_text("f_8639:expand_2escm"),(void*)f_8639},
{C_text("f_8643:expand_2escm"),(void*)f_8643},
{C_text("f_8647:expand_2escm"),(void*)f_8647},
{C_text("f_8696:expand_2escm"),(void*)f_8696},
{C_text("f_8704:expand_2escm"),(void*)f_8704},
{C_text("f_8717:expand_2escm"),(void*)f_8717},
{C_text("f_8750:expand_2escm"),(void*)f_8750},
{C_text("f_8754:expand_2escm"),(void*)f_8754},
{C_text("f_8809:expand_2escm"),(void*)f_8809},
{C_text("f_8833:expand_2escm"),(void*)f_8833},
{C_text("f_8877:expand_2escm"),(void*)f_8877},
{C_text("f_8901:expand_2escm"),(void*)f_8901},
{C_text("f_8907:expand_2escm"),(void*)f_8907},
{C_text("f_8920:expand_2escm"),(void*)f_8920},
{C_text("f_8924:expand_2escm"),(void*)f_8924},
{C_text("f_8936:expand_2escm"),(void*)f_8936},
{C_text("f_8982:expand_2escm"),(void*)f_8982},
{C_text("f_8986:expand_2escm"),(void*)f_8986},
{C_text("f_9011:expand_2escm"),(void*)f_9011},
{C_text("f_9017:expand_2escm"),(void*)f_9017},
{C_text("f_9056:expand_2escm"),(void*)f_9056},
{C_text("f_9059:expand_2escm"),(void*)f_9059},
{C_text("f_9065:expand_2escm"),(void*)f_9065},
{C_text("f_9077:expand_2escm"),(void*)f_9077},
{C_text("f_9080:expand_2escm"),(void*)f_9080},
{C_text("f_9083:expand_2escm"),(void*)f_9083},
{C_text("f_9096:expand_2escm"),(void*)f_9096},
{C_text("f_9100:expand_2escm"),(void*)f_9100},
{C_text("f_9104:expand_2escm"),(void*)f_9104},
{C_text("f_9106:expand_2escm"),(void*)f_9106},
{C_text("f_9127:expand_2escm"),(void*)f_9127},
{C_text("f_9179:expand_2escm"),(void*)f_9179},
{C_text("f_9183:expand_2escm"),(void*)f_9183},
{C_text("f_9200:expand_2escm"),(void*)f_9200},
{C_text("f_9204:expand_2escm"),(void*)f_9204},
{C_text("f_9209:expand_2escm"),(void*)f_9209},
{C_text("f_9235:expand_2escm"),(void*)f_9235},
{C_text("f_9250:expand_2escm"),(void*)f_9250},
{C_text("f_9269:expand_2escm"),(void*)f_9269},
{C_text("f_9284:expand_2escm"),(void*)f_9284},
{C_text("f_9286:expand_2escm"),(void*)f_9286},
{C_text("f_9328:expand_2escm"),(void*)f_9328},
{C_text("f_9339:expand_2escm"),(void*)f_9339},
{C_text("f_9358:expand_2escm"),(void*)f_9358},
{C_text("f_9373:expand_2escm"),(void*)f_9373},
{C_text("f_9375:expand_2escm"),(void*)f_9375},
{C_text("f_9382:expand_2escm"),(void*)f_9382},
{C_text("f_9403:expand_2escm"),(void*)f_9403},
{C_text("f_9427:expand_2escm"),(void*)f_9427},
{C_text("f_9434:expand_2escm"),(void*)f_9434},
{C_text("f_9441:expand_2escm"),(void*)f_9441},
{C_text("f_9447:expand_2escm"),(void*)f_9447},
{C_text("f_9457:expand_2escm"),(void*)f_9457},
{C_text("f_9464:expand_2escm"),(void*)f_9464},
{C_text("f_9485:expand_2escm"),(void*)f_9485},
{C_text("f_9489:expand_2escm"),(void*)f_9489},
{C_text("f_9493:expand_2escm"),(void*)f_9493},
{C_text("f_9497:expand_2escm"),(void*)f_9497},
{C_text("f_9501:expand_2escm"),(void*)f_9501},
{C_text("f_9505:expand_2escm"),(void*)f_9505},
{C_text("f_9507:expand_2escm"),(void*)f_9507},
{C_text("f_9511:expand_2escm"),(void*)f_9511},
{C_text("f_9519:expand_2escm"),(void*)f_9519},
{C_text("f_9528:expand_2escm"),(void*)f_9528},
{C_text("f_9541:expand_2escm"),(void*)f_9541},
{C_text("f_9543:expand_2escm"),(void*)f_9543},
{C_text("f_9547:expand_2escm"),(void*)f_9547},
{C_text("f_9554:expand_2escm"),(void*)f_9554},
{C_text("f_9574:expand_2escm"),(void*)f_9574},
{C_text("f_9576:expand_2escm"),(void*)f_9576},
{C_text("f_9580:expand_2escm"),(void*)f_9580},
{C_text("f_9583:expand_2escm"),(void*)f_9583},
{C_text("f_9586:expand_2escm"),(void*)f_9586},
{C_text("f_9588:expand_2escm"),(void*)f_9588},
{C_text("f_9596:expand_2escm"),(void*)f_9596},
{C_text("f_9598:expand_2escm"),(void*)f_9598},
{C_text("f_9612:expand_2escm"),(void*)f_9612},
{C_text("f_9616:expand_2escm"),(void*)f_9616},
{C_text("f_9635:expand_2escm"),(void*)f_9635},
{C_text("f_9644:expand_2escm"),(void*)f_9644},
{C_text("f_9658:expand_2escm"),(void*)f_9658},
{C_text("f_9668:expand_2escm"),(void*)f_9668},
{C_text("f_9679:expand_2escm"),(void*)f_9679},
{C_text("f_9689:expand_2escm"),(void*)f_9689},
{C_text("f_9698:expand_2escm"),(void*)f_9698},
{C_text("f_9709:expand_2escm"),(void*)f_9709},
{C_text("f_9720:expand_2escm"),(void*)f_9720},
{C_text("f_9728:expand_2escm"),(void*)f_9728},
{C_text("f_9743:expand_2escm"),(void*)f_9743},
{C_text("f_9747:expand_2escm"),(void*)f_9747},
{C_text("f_9761:expand_2escm"),(void*)f_9761},
{C_text("f_9765:expand_2escm"),(void*)f_9765},
{C_text("f_9769:expand_2escm"),(void*)f_9769},
{C_text("f_9791:expand_2escm"),(void*)f_9791},
{C_text("f_9795:expand_2escm"),(void*)f_9795},
{C_text("f_9838:expand_2escm"),(void*)f_9838},
{C_text("f_9856:expand_2escm"),(void*)f_9856},
{C_text("f_9867:expand_2escm"),(void*)f_9867},
{C_text("f_9869:expand_2escm"),(void*)f_9869},
{C_text("f_9873:expand_2escm"),(void*)f_9873},
{C_text("f_9885:expand_2escm"),(void*)f_9885},
{C_text("f_9913:expand_2escm"),(void*)f_9913},
{C_text("f_9934:expand_2escm"),(void*)f_9934},
{C_text("f_9975:expand_2escm"),(void*)f_9975},
{C_text("f_9977:expand_2escm"),(void*)f_9977},
{C_text("f_9987:expand_2escm"),(void*)f_9987},
{C_text("toplevel:expand_2escm"),(void*)C_expand_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
o|hiding unexported module binding: chicken.syntax#d 
o|hiding unexported module binding: chicken.syntax#define-alias 
o|hiding unexported module binding: chicken.syntax#d 
o|hiding unexported module binding: chicken.syntax#map-se 
o|hiding unexported module binding: chicken.syntax#dd 
o|hiding unexported module binding: chicken.syntax#dm 
o|hiding unexported module binding: chicken.syntax#dx 
o|hiding unexported module binding: chicken.syntax#lookup 
o|hiding unexported module binding: chicken.syntax#macro-alias 
o|hiding unexported module binding: chicken.syntax#expansion-result-hook 
o|hiding unexported module binding: chicken.syntax#defjam-error 
o|hiding unexported module binding: chicken.syntax#define-definition 
o|hiding unexported module binding: chicken.syntax#define-syntax-definition 
o|hiding unexported module binding: chicken.syntax#define-values-definition 
o|hiding unexported module binding: chicken.syntax#import-definition 
o|hiding unexported module binding: chicken.syntax#make-er/ir-transformer 
o|hiding unexported module binding: chicken.internal.syntax-rules#process-syntax-rules 
S|applied compiler syntax:
S|  chicken.format#sprintf		1
S|  chicken.base#foldl		1
S|  ##sys#map		5
S|  scheme#for-each		1
S|  scheme#map		12
o|eliminated procedure checks: 549 
o|eliminated procedure checks: 1 
o|eliminated procedure checks: 1 
o|specializations:
o|  1 (scheme#zero? integer)
o|  1 (##sys#check-output-port * * *)
o|  6 (scheme#cddr (pair * pair))
o|  1 (scheme#= fixnum fixnum)
o|  2 (scheme#cdddr (pair * (pair * pair)))
o|  2 (scheme#vector-length vector)
o|  8 (scheme#eqv? (or eof null fixnum char boolean symbol keyword) *)
o|  4 (scheme#not false)
o|  4 (keyword (not keyword))
o|  1 (scheme#caar (pair pair *))
o|  1 (scheme#>= fixnum fixnum)
o|  3 (scheme#length list)
o|  19 (scheme#eqv? * (or eof null fixnum char boolean symbol keyword))
o|  2 (scheme#cdar (pair pair *))
o|  10 (##sys#check-list (or pair list) *)
o|  1 (scheme#set-cdr! pair *)
o|  72 (scheme#cdr pair)
o|  1 (scheme#set-car! pair *)
o|  59 (scheme#car pair)
(o e)|safe calls: 1257 
(o e)|dropped branches: 2 
(o e)|assignments to immediate values: 9 
o|inlining procedure: k3720 
o|inlining procedure: k3720 
o|contracted procedure: "(expand.scm:93) g310311" 
o|inlining procedure: k3737 
o|inlining procedure: k3737 
o|contracted procedure: "(expand.scm:103) g343344" 
o|contracted procedure: "(expand.scm:102) g338339" 
o|contracted procedure: "(expand.scm:101) g332333" 
o|contracted procedure: "(expand.scm:97) g320321" 
o|inlining procedure: k3793 
o|inlining procedure: k3793 
o|inlining procedure: k3805 
o|inlining procedure: k3824 
o|inlining procedure: k3824 
o|contracted procedure: "(expand.scm:117) g374375" 
o|contracted procedure: "(expand.scm:116) g363364" 
o|inlining procedure: k3805 
o|inlining procedure: k3873 
o|inlining procedure: k3895 
o|inlining procedure: k3895 
o|inlining procedure: k3873 
o|inlining procedure: k3972 
o|contracted procedure: "(expand.scm:142) g475485" 
o|inlining procedure: k3972 
o|inlining procedure: k4020 
o|contracted procedure: "(expand.scm:137) g433441" 
o|contracted procedure: "(expand.scm:140) g451452" 
o|contracted procedure: "(expand.scm:139) g447448" 
o|inlining procedure: k4020 
o|inlining procedure: k4069 
o|inlining procedure: k4069 
o|inlining procedure: k4112 
o|inlining procedure: k4112 
o|inlining procedure: k4128 
o|inlining procedure: k4128 
o|inlining procedure: k4146 
o|inlining procedure: k4146 
o|contracted procedure: "(expand.scm:150) g512513" 
o|inlining procedure: k4203 
o|inlining procedure: k4203 
o|inlining procedure: k4233 
o|inlining procedure: k4233 
o|inlining procedure: k4275 
o|inlining procedure: k4275 
o|inlining procedure: k4317 
o|inlining procedure: k4317 
o|inlining procedure: k4382 
o|inlining procedure: k4406 
o|inlining procedure: k4406 
o|inlining procedure: k4449 
o|inlining procedure: k4449 
o|inlining procedure: k4382 
o|inlining procedure: k4553 
o|inlining procedure: k4553 
o|inlining procedure: k4594 
o|inlining procedure: k4617 
o|inlining procedure: k4666 
o|inlining procedure: k4666 
o|inlining procedure: k4735 
o|contracted procedure: "(expand.scm:292) g688697" 
o|inlining procedure: k4735 
o|inlining procedure: k4617 
o|inlining procedure: k4780 
o|inlining procedure: k4780 
o|inlining procedure: k4810 
o|contracted procedure: "(expand.scm:297) g738739" 
o|inlining procedure: k4810 
o|inlining procedure: k4828 
o|inlining procedure: k4828 
o|inlining procedure: k4594 
o|inlining procedure: k4879 
o|inlining procedure: k4879 
o|inlining procedure: k4921 
o|inlining procedure: k4947 
o|inlining procedure: k4947 
o|substituted constant variable: a4954 
o|substituted constant variable: a4956 
o|substituted constant variable: a4958 
o|inlining procedure: k4921 
o|inlining procedure: k4993 
o|inlining procedure: k5010 
o|inlining procedure: k5010 
o|substituted constant variable: %let822 
o|inlining procedure: k5053 
o|inlining procedure: k5053 
o|inlining procedure: k5106 
o|inlining procedure: k5106 
o|inlining procedure: k5118 
o|inlining procedure: k5118 
o|inlining procedure: k5178 
o|substituted constant variable: %lambda819 
o|inlining procedure: k5178 
o|contracted procedure: "(expand.scm:371) ->keyword813" 
o|inlining procedure: k5216 
o|inlining procedure: k5216 
o|inlining procedure: k4993 
o|inlining procedure: k5263 
o|inlining procedure: k5263 
o|inlining procedure: k5280 
o|inlining procedure: k5280 
o|inlining procedure: k5308 
o|inlining procedure: k5308 
o|inlining procedure: k5324 
o|inlining procedure: k5336 
o|inlining procedure: k5336 
o|inlining procedure: k5324 
o|inlining procedure: k5384 
o|inlining procedure: k5384 
o|inlining procedure: k5403 
o|inlining procedure: k5422 
o|inlining procedure: k5422 
o|substituted constant variable: a5460 
o|substituted constant variable: a5462 
o|substituted constant variable: a5464 
o|inlining procedure: k5403 
o|inlining procedure: k5471 
o|inlining procedure: k5471 
o|inlining procedure: k5493 
o|inlining procedure: k5493 
o|substituted constant variable: a5510 
o|substituted constant variable: a5512 
o|substituted constant variable: a5514 
o|inlining procedure: k5521 
o|inlining procedure: k5521 
o|substituted constant variable: a5537 
o|substituted constant variable: a5539 
o|substituted constant variable: a5541 
o|inlining procedure: k5545 
o|inlining procedure: k5545 
o|inlining procedure: k5625 
o|inlining procedure: k5625 
o|inlining procedure: k5647 
o|contracted procedure: "(expand.scm:462) g982992" 
o|inlining procedure: k5647 
o|inlining procedure: k5713 
o|inlining procedure: k5713 
o|inlining procedure: k5772 
o|inlining procedure: k5772 
o|inlining procedure: k5781 
o|inlining procedure: k5781 
o|inlining procedure: k5790 
o|inlining procedure: k5790 
o|inlining procedure: k5811 
o|inlining procedure: k5811 
o|inlining procedure: k5820 
o|inlining procedure: k5820 
o|substituted constant variable: a5839 
o|substituted constant variable: a5841 
o|substituted constant variable: a5843 
o|substituted constant variable: a5845 
o|inlining procedure: k5849 
o|inlining procedure: k5861 
o|inlining procedure: k5861 
o|inlining procedure: k5884 
o|inlining procedure: k5884 
o|inlining procedure: k5935 
o|inlining procedure: k5953 
o|inlining procedure: k5953 
o|inlining procedure: k5935 
o|inlining procedure: k6010 
o|inlining procedure: k6010 
o|inlining procedure: k6042 
o|inlining procedure: k6057 
o|inlining procedure: k6057 
o|inlining procedure: k6069 
o|inlining procedure: k6069 
o|inlining procedure: k6042 
o|inlining procedure: k5849 
o|inlining procedure: k6148 
o|contracted procedure: "(expand.scm:544) g11611172" 
o|inlining procedure: k6120 
o|inlining procedure: k6120 
o|inlining procedure: k6148 
o|inlining procedure: k6198 
o|inlining procedure: k6198 
o|inlining procedure: k6210 
o|contracted procedure: "(expand.scm:536) g11131122" 
o|inlining procedure: k6210 
o|inlining procedure: k6244 
o|contracted procedure: "(expand.scm:539) g11371138" 
o|inlining procedure: k6244 
o|substituted constant variable: g11291132 
o|inlining procedure: k6296 
o|inlining procedure: k6323 
o|inlining procedure: k6323 
o|inlining procedure: k6296 
o|inlining procedure: k6363 
o|inlining procedure: "(expand.scm:572) chicken.syntax#defjam-error" 
o|inlining procedure: k6363 
o|inlining procedure: k6402 
o|inlining procedure: k6402 
o|substituted constant variable: a6424 
o|inlining procedure: k6448 
o|inlining procedure: k6448 
o|inlining procedure: k6476 
o|inlining procedure: k6494 
o|inlining procedure: k6522 
o|inlining procedure: k6522 
o|inlining procedure: "(expand.scm:597) chicken.syntax#defjam-error" 
o|inlining procedure: k6494 
o|inlining procedure: k6476 
o|inlining procedure: k6625 
o|inlining procedure: k6625 
o|inlining procedure: k6674 
o|inlining procedure: k6674 
o|inlining procedure: k6710 
o|inlining procedure: k6710 
o|inlining procedure: k6760 
o|inlining procedure: k6784 
o|inlining procedure: k6784 
o|inlining procedure: k6760 
o|inlining procedure: k6807 
o|inlining procedure: k6807 
o|inlining procedure: k6830 
o|inlining procedure: k6830 
o|inlining procedure: k6842 
o|inlining procedure: k6842 
o|inlining procedure: k6959 
o|inlining procedure: k6959 
o|inlining procedure: k6990 
o|inlining procedure: k6990 
o|inlining procedure: k7052 
o|inlining procedure: k7052 
o|inlining procedure: k7087 
o|inlining procedure: k7087 
o|contracted procedure: "(expand.scm:708) syntax-imports1337" 
o|inlining procedure: k6921 
o|inlining procedure: k6921 
o|inlining procedure: k7126 
o|inlining procedure: k7136 
o|inlining procedure: k7153 
o|inlining procedure: k7153 
o|inlining procedure: k7136 
o|inlining procedure: k7126 
o|inlining procedure: k7180 
o|inlining procedure: k7180 
o|inlining procedure: k7199 
o|inlining procedure: k7199 
o|propagated global variable: sexp1427 ##sys#syntax-error-culprit 
o|inlining procedure: k7222 
o|inlining procedure: k7222 
o|inlining procedure: k7234 
o|inlining procedure: k7234 
o|inlining procedure: k7243 
o|inlining procedure: k7243 
o|inlining procedure: k7279 
o|inlining procedure: k7279 
o|inlining procedure: k7302 
o|inlining procedure: k7326 
o|inlining procedure: k7326 
o|inlining procedure: k7361 
o|inlining procedure: k7361 
o|inlining procedure: k7384 
o|inlining procedure: k7384 
o|inlining procedure: k7302 
o|inlining procedure: k7405 
o|inlining procedure: k7405 
o|inlining procedure: k7418 
o|inlining procedure: k7430 
o|inlining procedure: k7430 
o|inlining procedure: k7448 
o|inlining procedure: k7448 
o|inlining procedure: k7466 
o|inlining procedure: k7466 
o|inlining procedure: k7484 
o|inlining procedure: k7484 
o|inlining procedure: k7506 
o|inlining procedure: k7506 
o|substituted constant variable: a7519 
o|substituted constant variable: a7521 
o|substituted constant variable: a7523 
o|substituted constant variable: a7525 
o|substituted constant variable: a7527 
o|substituted constant variable: a7529 
o|substituted constant variable: a7531 
o|substituted constant variable: a7533 
o|inlining procedure: k7418 
o|inlining procedure: k7543 
o|inlining procedure: k7543 
o|inlining procedure: k7624 
o|inlining procedure: k7624 
o|inlining procedure: k7662 
o|inlining procedure: k7662 
o|contracted procedure: "(expand.scm:839) g15241525" 
o|inlining procedure: k7712 
o|inlining procedure: k7727 
o|inlining procedure: k7727 
o|inlining procedure: k7712 
o|inlining procedure: k7750 
o|inlining procedure: k7771 
o|inlining procedure: k7771 
o|inlining procedure: k7750 
o|inlining procedure: k7808 
o|inlining procedure: k7836 
o|inlining procedure: k7836 
o|inlining procedure: k7867 
o|inlining procedure: k7867 
o|inlining procedure: k7901 
o|inlining procedure: k7901 
o|removed unused parameter to known procedure: n1605 "(expand.scm:872) lookup21511" 
o|contracted procedure: "(expand.scm:871) g15731574" 
o|inlining procedure: k7907 
o|inlining procedure: k7907 
o|removed unused parameter to known procedure: n1605 "(expand.scm:869) lookup21511" 
o|contracted procedure: "(expand.scm:868) g15631564" 
o|inlining procedure: k7808 
o|inlining procedure: k7916 
o|inlining procedure: k7916 
o|removed unused formal parameters: (n1605) 
o|inlining procedure: k7934 
o|inlining procedure: k7934 
o|inlining procedure: k7959 
o|inlining procedure: k7959 
o|inlining procedure: k7997 
o|inlining procedure: k7997 
o|contracted procedure: "(expand.scm:908) g16331634" 
o|inlining procedure: k8026 
o|inlining procedure: k8026 
o|inlining procedure: k8055 
o|contracted procedure: "(expand.scm:908) g16501651" 
o|inlining procedure: k8055 
o|contracted procedure: "(expand.scm:915) g16461647" 
o|inlining procedure: k8079 
o|inlining procedure: k8079 
o|inlining procedure: k8244 
o|inlining procedure: k8244 
o|inlining procedure: k8280 
o|inlining procedure: k8280 
o|contracted procedure: "(synrules.scm:57) chicken.internal.syntax-rules#process-syntax-rules" 
o|removed side-effect free assignment to unused variable: %vector-length2627 
o|removed side-effect free assignment to unused variable: %vector-ref2628 
o|removed side-effect free assignment to unused variable: %null?2650 
o|removed side-effect free assignment to unused variable: %or2651 
o|removed side-effect free assignment to unused variable: %syntax-error2657 
o|inlining procedure: k8463 
o|inlining procedure: k8463 
o|inlining procedure: k8505 
o|inlining procedure: k8505 
o|inlining procedure: k8553 
o|inlining procedure: k8553 
o|inlining procedure: k8571 
o|inlining procedure: k8571 
o|inlining procedure: k8607 
o|inlining procedure: k8607 
o|inlining procedure: k8709 
o|inlining procedure: k8709 
o|inlining procedure: k8743 
o|inlining procedure: k8743 
o|inlining procedure: k8879 
o|inlining procedure: k8879 
o|inlining procedure: k8942 
o|inlining procedure: k8942 
o|inlining procedure: k8970 
o|inlining procedure: k8970 
o|inlining procedure: k9019 
o|inlining procedure: k9031 
o|inlining procedure: k9031 
o|inlining procedure: k9019 
o|inlining procedure: k9066 
o|inlining procedure: k9066 
o|substituted constant variable: %append2620 
o|inlining procedure: k9108 
o|inlining procedure: k9108 
o|substituted constant variable: %apply2621 
o|substituted constant variable: %append2620 
o|inlining procedure: k9142 
o|inlining procedure: k9142 
o|inlining procedure: k9167 
o|inlining procedure: k9167 
o|inlining procedure: k9211 
o|inlining procedure: k9211 
o|inlining procedure: k9255 
o|inlining procedure: k9255 
o|inlining procedure: k9288 
o|inlining procedure: k9309 
o|inlining procedure: k9309 
o|inlining procedure: k9288 
o|inlining procedure: k9344 
o|inlining procedure: k9344 
o|inlining procedure: k9377 
o|inlining procedure: k9389 
o|inlining procedure: k9389 
o|inlining procedure: k9377 
o|inlining procedure: k9405 
o|inlining procedure: k9405 
o|inlining procedure: k9429 
o|inlining procedure: k9429 
o|inlining procedure: k9459 
o|inlining procedure: k9459 
o|inlining procedure: k9600 
o|inlining procedure: k9600 
o|inlining procedure: k9630 
o|inlining procedure: k9630 
o|inlining procedure: k9684 
o|inlining procedure: k9684 
o|inlining procedure: k9766 
o|inlining procedure: k9766 
o|inlining procedure: k9800 
o|inlining procedure: k9800 
o|inlining procedure: k9839 
o|contracted procedure: "(expand.scm:1555) g25612562" 
o|inlining procedure: k9839 
o|inlining procedure: k9979 
o|contracted procedure: "(expand.scm:1512) g24642473" 
o|inlining procedure: k9949 
o|inlining procedure: k9949 
o|inlining procedure: k9979 
o|inlining procedure: k10029 
o|contracted procedure: "(expand.scm:1501) g24302439" 
o|inlining procedure: k10029 
o|inlining procedure: k10081 
o|inlining procedure: k10081 
o|inlining procedure: k10155 
o|inlining procedure: k10155 
o|inlining procedure: k10181 
o|inlining procedure: k10199 
o|inlining procedure: k10199 
o|inlining procedure: k10181 
o|inlining procedure: k10304 
o|inlining procedure: k10304 
o|inlining procedure: k10372 
o|inlining procedure: k10372 
o|substituted constant variable: a10399 
o|substituted constant variable: a10400 
o|inlining procedure: k10417 
o|inlining procedure: k10444 
o|inlining procedure: k10444 
o|inlining procedure: k10417 
o|inlining procedure: k10506 
o|inlining procedure: k10506 
o|inlining procedure: k10642 
o|inlining procedure: k10642 
o|inlining procedure: k10654 
o|inlining procedure: k10654 
o|inlining procedure: k10666 
o|inlining procedure: k10666 
o|inlining procedure: k10678 
o|inlining procedure: k10678 
o|inlining procedure: k10687 
o|inlining procedure: k10687 
o|inlining procedure: k10740 
o|inlining procedure: k10740 
o|inlining procedure: k10792 
o|inlining procedure: k10792 
o|inlining procedure: k10835 
o|inlining procedure: k10835 
o|inlining procedure: "(expand.scm:1281) chicken.syntax#defjam-error" 
o|contracted procedure: "(expand.scm:1278) g21442145" 
o|inlining procedure: k11049 
o|inlining procedure: k11084 
o|inlining procedure: k11084 
o|inlining procedure: "(expand.scm:1256) chicken.syntax#defjam-error" 
o|contracted procedure: "(expand.scm:1253) g21152116" 
o|inlining procedure: k11049 
o|inlining procedure: k11236 
o|inlining procedure: k11236 
o|inlining procedure: k11285 
o|inlining procedure: k11285 
o|inlining procedure: k11300 
o|inlining procedure: k11300 
o|inlining procedure: k11406 
o|inlining procedure: k11406 
o|inlining procedure: k11421 
o|inlining procedure: k11433 
o|inlining procedure: k11433 
o|substituted constant variable: a11450 
o|inlining procedure: k11421 
o|inlining procedure: k11466 
o|inlining procedure: k11466 
o|propagated global variable: g19891990 ##sys#expand-import 
o|inlining procedure: k11524 
o|inlining procedure: k11524 
o|inlining procedure: k11554 
o|inlining procedure: k11554 
o|inlining procedure: k11651 
o|inlining procedure: k11651 
o|inlining procedure: k11671 
o|inlining procedure: k11671 
o|inlining procedure: k11736 
o|inlining procedure: k11736 
o|inlining procedure: k11765 
o|inlining procedure: k11777 
o|inlining procedure: k11777 
o|inlining procedure: k11765 
o|inlining procedure: k11809 
o|inlining procedure: k11824 
o|inlining procedure: k11824 
o|inlining procedure: k11809 
o|inlining procedure: k11845 
o|inlining procedure: k11845 
o|substituted constant variable: a11866 
o|substituted constant variable: a11868 
o|substituted constant variable: a11870 
o|inlining procedure: k11881 
o|inlining procedure: k11908 
o|contracted procedure: "(expand.scm:1036) g19011910" 
o|inlining procedure: k11908 
o|inlining procedure: k11881 
o|inlining procedure: k11952 
o|inlining procedure: k11952 
o|inlining procedure: k11971 
o|inlining procedure: k11971 
o|inlining procedure: k11980 
o|inlining procedure: k11980 
o|inlining procedure: k12078 
o|inlining procedure: k12078 
o|inlining procedure: k12125 
o|inlining procedure: k12125 
o|propagated global variable: g17741775 ##sys#expand-import 
o|propagated global variable: g17601761 ##sys#expand-import 
o|replaced variables: 2412 
o|removed binding forms: 471 
o|substituted constant variable: prop313 
o|removed call to pure procedure with unused result: "(expand.scm:104) chicken.base#void" 
o|substituted constant variable: prop346 
o|substituted constant variable: prop341 
o|substituted constant variable: prop335 
o|substituted constant variable: prop377 
o|substituted constant variable: prop366 
o|substituted constant variable: prop454 
o|inlining procedure: k3942 
o|inlining procedure: k3942 
o|substituted constant variable: prop450 
o|substituted constant variable: prop515 
o|substituted constant variable: r431812208 
o|substituted constant variable: r440712212 
o|substituted constant variable: r445012215 
o|removed call to pure procedure with unused result: "(expand.scm:253) chicken.base#void" 
o|removed call to pure procedure with unused result: "(expand.scm:212) chicken.base#void" 
o|removed call to pure procedure with unused result: "(expand.scm:211) chicken.base#void" 
o|removed call to pure procedure with unused result: "(expand.scm:256) chicken.base#void" 
o|inlining procedure: k4789 
o|substituted constant variable: prop741 
o|substituted constant variable: r481112230 
o|substituted constant variable: r492212241 
o|substituted constant variable: r511912254 
o|substituted constant variable: r517912257 
o|substituted constant variable: r517912257 
o|substituted constant variable: r552212283 
o|substituted constant variable: r554612285 
o|converted assignments to bindings: (err812) 
o|removed side-effect free assignment to unused variable: chicken.syntax#defjam-error 
o|substituted constant variable: r562612286 
o|substituted constant variable: r562612286 
o|inlining procedure: k5625 
o|substituted constant variable: r601112316 
o|substituted constant variable: r604312322 
o|removed call to pure procedure with unused result: "(expand.scm:555) chicken.base#void" 
o|substituted constant variable: r619912329 
o|substituted constant variable: r640312346 
o|substituted constant variable: r652312353 
o|substituted constant variable: r652312353 
o|substituted constant variable: r671112367 
o|substituted constant variable: r678512369 
o|substituted constant variable: r680812373 
o|substituted constant variable: r683112375 
o|substituted constant variable: r708812384 
o|substituted constant variable: r692212386 
o|converted assignments to bindings: (outstr1350) 
o|substituted constant variable: r715412391 
o|substituted constant variable: r713712392 
o|substituted constant variable: r712712393 
o|substituted constant variable: r724412405 
o|substituted constant variable: r738512414 
o|removed call to pure procedure with unused result: "(expand.scm:846) chicken.base#void" 
o|removed call to pure procedure with unused result: "(expand.scm:850) chicken.base#void" 
o|removed call to pure procedure with unused result: "(expand.scm:885) chicken.base#void" 
o|inlining procedure: k7712 
o|substituted constant variable: r772812445 
o|inlining procedure: k7712 
o|inlining procedure: k7712 
o|inlining procedure: k7712 
o|substituted constant variable: r775112455 
o|inlining procedure: k7712 
o|inlining procedure: k7712 
o|substituted constant variable: prop1576 
o|substituted constant variable: prop1566 
o|substituted constant variable: r791712466 
o|substituted constant variable: r791712467 
o|removed call to pure procedure with unused result: "(expand.scm:889) chicken.base#void" 
o|substituted constant variable: r793512468 
o|removed call to pure procedure with unused result: "(expand.scm:910) chicken.base#void" 
o|removed call to pure procedure with unused result: "(expand.scm:912) chicken.base#void" 
o|removed call to pure procedure with unused result: "(expand.scm:914) chicken.base#void" 
o|removed call to pure procedure with unused result: "(expand.scm:917) chicken.base#void" 
o|removed call to pure procedure with unused result: "(expand.scm:922) chicken.base#void" 
o|substituted constant variable: prop1649 
o|substituted constant variable: r824512481 
o|removed side-effect free assignment to unused variable: %append2620 
o|removed side-effect free assignment to unused variable: %apply2621 
o|substituted constant variable: r855412489 
o|substituted constant variable: r914312515 
o|substituted constant variable: r931012524 
o|substituted constant variable: r939012530 
o|substituted constant variable: r937812531 
o|substituted constant variable: r940612533 
o|substituted constant variable: r943012535 
o|substituted constant variable: r1015612558 
o|substituted constant variable: r1037312566 
o|substituted constant variable: r1068812583 
o|substituted constant variable: r1074112584 
o|substituted constant variable: r1079312586 
o|inlining procedure: k11008 
o|substituted constant variable: prop2147 
o|substituted constant variable: r1108512598 
o|substituted constant variable: r1108512598 
o|substituted constant variable: prop2118 
o|substituted constant variable: r1123712607 
o|substituted constant variable: r1128612608 
o|substituted constant variable: r1128612608 
o|substituted constant variable: r1143412626 
o|substituted constant variable: r1142212627 
o|substituted constant variable: r1167212641 
o|substituted constant variable: r1181012651 
o|substituted constant variable: r1197212660 
o|substituted constant variable: r1207912664 
o|simplifications: ((let . 2)) 
o|replaced variables: 176 
o|removed binding forms: 1943 
o|inlining procedure: k3728 
o|contracted procedure: k3773 
o|substituted constant variable: prop45412682 
o|substituted constant variable: prop45412688 
o|contracted procedure: k4351 
o|contracted procedure: k4354 
o|contracted procedure: k4485 
o|contracted procedure: k4550 
o|inlining procedure: k4936 
o|inlining procedure: k4936 
o|inlining procedure: k5272 
o|inlining procedure: k5272 
o|contracted procedure: k6082 
o|contracted procedure: k7679 
o|contracted procedure: k7691 
o|contracted procedure: k7715 
o|substituted constant variable: r771312809 
o|removed call to pure procedure with unused result: "(expand.scm:885) chicken.base#void" 
o|substituted constant variable: r771312814 
o|removed call to pure procedure with unused result: "(expand.scm:885) chicken.base#void" 
o|substituted constant variable: r771312823 
o|removed call to pure procedure with unused result: "(expand.scm:885) chicken.base#void" 
o|substituted constant variable: r771312828 
o|removed call to pure procedure with unused result: "(expand.scm:885) chicken.base#void" 
o|substituted constant variable: r771312833 
o|removed call to pure procedure with unused result: "(expand.scm:885) chicken.base#void" 
o|substituted constant variable: r771312838 
o|removed call to pure procedure with unused result: "(expand.scm:885) chicken.base#void" 
o|contracted procedure: k7928 
o|contracted procedure: k8017 
o|contracted procedure: k8032 
o|contracted procedure: k8044 
o|contracted procedure: k8060 
o|contracted procedure: k8069 
o|inlining procedure: k9297 
o|inlining procedure: k9297 
o|inlining procedure: k10000 
o|inlining procedure: k12100 
o|replaced variables: 8 
o|removed binding forms: 250 
o|contracted procedure: k3757 
o|contracted procedure: k3765 
o|contracted procedure: k3770 
o|contracted procedure: k3813 
o|contracted procedure: k3818 
o|contracted procedure: k3933 
o|contracted procedure: k4120 
o|inlining procedure: k4482 
o|substituted constant variable: r562612744 
o|contracted procedure: k771512813 
o|contracted procedure: k771512818 
o|contracted procedure: k771512827 
o|contracted procedure: k771512832 
o|contracted procedure: k7816 
o|contracted procedure: k7824 
o|contracted procedure: k771512837 
o|contracted procedure: k771512842 
o|inlining procedure: "(expand.scm:872) lookup21511" 
o|inlining procedure: "(expand.scm:869) lookup21511" 
o|contracted procedure: k8052 
o|substituted constant variable: r929812986 
o|substituted constant variable: r929812987 
o|contracted procedure: k10999 
o|contracted procedure: k11060 
o|substituted constant variable: r1210113011 
o|replaced variables: 3 
o|removed binding forms: 53 
o|removed conditional forms: 3 
o|inlining procedure: k3760 
o|inlining procedure: k3760 
o|removed side-effect free assignment to unused variable: lookup21511 
o|replaced variables: 15 
o|removed binding forms: 17 
o|inlining procedure: k7898 
o|inlining procedure: k7904 
o|replaced variables: 4 
o|removed binding forms: 12 
o|removed binding forms: 4 
o|simplifications: ((let . 81) (if . 55) (##core#call . 1089)) 
o|  call simplifications:
o|    scheme#cdddr
o|    scheme#cddddr
o|    scheme#number?
o|    scheme#eof-object?
o|    scheme#cadddr	2
o|    chicken.fixnum#fx-	2
o|    scheme#>=
o|    scheme#+	3
o|    scheme#=
o|    scheme#<=
o|    scheme#boolean?
o|    scheme#char?	2
o|    scheme#>	2
o|    scheme#-	3
o|    scheme#cdar	2
o|    ##sys#immediate?
o|    scheme#vector-ref	5
o|    chicken.fixnum#fx<	2
o|    chicken.fixnum#fx=	7
o|    scheme#apply
o|    scheme#memq	5
o|    scheme#member
o|    scheme#caddr	16
o|    scheme#length	13
o|    chicken.fixnum#fx<=	2
o|    chicken.fixnum#fx>	4
o|    scheme#cddr	10
o|    ##sys#list	179
o|    ##sys#cons	86
o|    scheme#list?	7
o|    scheme#cadr	35
o|    scheme#values	8
o|    ##sys#call-with-values	3
o|    ##sys#apply	2
o|    scheme#memv
o|    scheme#equal?	2
o|    scheme#string?	3
o|    ##sys#make-structure	2
o|    scheme#list	16
o|    scheme#set-car!	2
o|    ##sys#structure?	2
o|    scheme#caar	6
o|    scheme#eq?	84
o|    scheme#null?	47
o|    scheme#car	68
o|    ##sys#check-list	14
o|    scheme#assq	12
o|    scheme#symbol?	45
o|    scheme#vector?	13
o|    ##sys#size	4
o|    chicken.fixnum#fx>=	5
o|    ##sys#slot	73
o|    chicken.fixnum#fx+	4
o|    scheme#cons	82
o|    ##sys#setslot	20
o|    scheme#not	38
o|    scheme#pair?	87
o|    scheme#cdr	48
o|contracted procedure: k3743 
o|contracted procedure: k3752 
o|contracted procedure: k3790 
o|contracted procedure: k3808 
o|contracted procedure: k3827 
o|contracted procedure: k3833 
o|contracted procedure: k3839 
o|contracted procedure: k3842 
o|contracted procedure: k3870 
o|contracted procedure: k3846 
o|contracted procedure: k3849 
o|contracted procedure: k3852 
o|contracted procedure: k3876 
o|contracted procedure: k3879 
o|contracted procedure: k3920 
o|contracted procedure: k3886 
o|contracted procedure: k3898 
o|contracted procedure: k3901 
o|contracted procedure: k3908 
o|contracted procedure: k3916 
o|contracted procedure: k3945 
o|contracted procedure: k3948 
o|contracted procedure: k3958 
o|contracted procedure: k4011 
o|contracted procedure: k3975 
o|contracted procedure: k4001 
o|contracted procedure: k4005 
o|contracted procedure: k3997 
o|contracted procedure: k3978 
o|contracted procedure: k3981 
o|contracted procedure: k3989 
o|contracted procedure: k3993 
o|contracted procedure: k4048 
o|contracted procedure: k4023 
o|contracted procedure: k4041 
o|contracted procedure: k4045 
o|contracted procedure: k4026 
o|contracted procedure: k4033 
o|contracted procedure: k4037 
o|contracted procedure: k4054 
o|contracted procedure: k4057 
o|contracted procedure: k4060 
o|contracted procedure: k4072 
o|contracted procedure: k4075 
o|contracted procedure: k4078 
o|contracted procedure: k4086 
o|contracted procedure: k4094 
o|contracted procedure: k4182 
o|contracted procedure: k4115 
o|contracted procedure: k4131 
o|contracted procedure: k4149 
o|contracted procedure: k4178 
o|contracted procedure: k4168 
o|inlining procedure: k4158 
o|inlining procedure: k4158 
o|contracted procedure: k4215 
o|contracted procedure: k4200 
o|contracted procedure: k4206 
o|contracted procedure: k4238 
o|contracted procedure: k4241 
o|contracted procedure: k4249 
o|contracted procedure: k4260 
o|contracted procedure: k4256 
o|contracted procedure: k4272 
o|contracted procedure: k4291 
o|contracted procedure: k4320 
o|contracted procedure: k4342 
o|contracted procedure: k4326 
o|contracted procedure: k4392 
o|contracted procedure: k4382 
o|contracted procedure: k4400 
o|contracted procedure: k4409 
o|contracted procedure: k4412 
o|contracted procedure: k4426 
o|contracted procedure: k4436 
o|contracted procedure: k4440 
o|contracted procedure: k4446 
o|contracted procedure: k4452 
o|contracted procedure: k4460 
o|contracted procedure: k4467 
o|contracted procedure: k4505 
o|contracted procedure: k4491 
o|contracted procedure: k4585 
o|contracted procedure: k4556 
o|contracted procedure: k4565 
o|contracted procedure: k4576 
o|contracted procedure: k4597 
o|contracted procedure: k4605 
o|contracted procedure: k4611 
o|contracted procedure: k4620 
o|contracted procedure: k4626 
o|contracted procedure: k4632 
o|contracted procedure: k4638 
o|contracted procedure: k4714 
o|contracted procedure: k4722 
o|contracted procedure: k4729 
o|contracted procedure: k4710 
o|contracted procedure: k4706 
o|contracted procedure: k4702 
o|contracted procedure: k4698 
o|contracted procedure: k4653 
o|contracted procedure: k4657 
o|contracted procedure: k4649 
o|contracted procedure: k4645 
o|contracted procedure: k4669 
o|contracted procedure: k4691 
o|contracted procedure: k4687 
o|contracted procedure: k4672 
o|contracted procedure: k4675 
o|contracted procedure: k4683 
o|contracted procedure: k4738 
o|contracted procedure: k4760 
o|contracted procedure: k4756 
o|contracted procedure: k4741 
o|contracted procedure: k4744 
o|contracted procedure: k4752 
o|contracted procedure: k4783 
o|contracted procedure: k4799 
o|contracted procedure: k4813 
o|contracted procedure: k4821 
o|contracted procedure: k4897 
o|contracted procedure: k4852 
o|contracted procedure: k4891 
o|contracted procedure: k4855 
o|contracted procedure: k4885 
o|contracted procedure: k4858 
o|contracted procedure: k4903 
o|contracted procedure: k4924 
o|contracted procedure: k4927 
o|contracted procedure: k4933 
o|contracted procedure: k4944 
o|contracted procedure: k4936 
o|contracted procedure: k4996 
o|contracted procedure: k5013 
o|contracted procedure: k5042 
o|contracted procedure: k5046 
o|contracted procedure: k5038 
o|contracted procedure: k5034 
o|contracted procedure: k5030 
o|contracted procedure: k5026 
o|inlining procedure: k5010 
o|contracted procedure: k5109 
o|contracted procedure: k5056 
o|contracted procedure: k5071 
o|contracted procedure: k5067 
o|contracted procedure: k5063 
o|inlining procedure: k5010 
o|contracted procedure: k5090 
o|contracted procedure: k5086 
o|contracted procedure: k5082 
o|inlining procedure: k5010 
o|inlining procedure: k5102 
o|inlining procedure: k5102 
o|contracted procedure: k5115 
o|contracted procedure: k5121 
o|contracted procedure: k5128 
o|contracted procedure: k5131 
o|contracted procedure: k5146 
o|contracted procedure: k5151 
o|contracted procedure: k5166 
o|contracted procedure: k5162 
o|contracted procedure: k5158 
o|contracted procedure: k5174 
o|contracted procedure: k5181 
o|contracted procedure: k5192 
o|contracted procedure: k5188 
o|contracted procedure: k5178 
o|contracted procedure: k4975 
o|contracted procedure: k5142 
o|contracted procedure: k5138 
o|contracted procedure: k5219 
o|contracted procedure: k5222 
o|contracted procedure: k5225 
o|contracted procedure: k5233 
o|contracted procedure: k5241 
o|contracted procedure: k5260 
o|contracted procedure: k5266 
o|contracted procedure: k5559 
o|contracted procedure: k5283 
o|contracted procedure: k5289 
o|contracted procedure: k5295 
o|contracted procedure: k5302 
o|contracted procedure: k5311 
o|contracted procedure: k5327 
o|contracted procedure: k5333 
o|contracted procedure: k5364 
o|contracted procedure: k5339 
o|contracted procedure: k5346 
o|contracted procedure: k5354 
o|contracted procedure: k5358 
o|contracted procedure: k5378 
o|contracted procedure: k5387 
o|contracted procedure: k5396 
o|contracted procedure: k5406 
o|contracted procedure: k5412 
o|contracted procedure: k5419 
o|contracted procedure: k5425 
o|contracted procedure: k5436 
o|contracted procedure: k5432 
o|contracted procedure: k5442 
o|contracted procedure: k5456 
o|contracted procedure: k5452 
o|contracted procedure: k5474 
o|contracted procedure: k5483 
o|contracted procedure: k5490 
o|contracted procedure: k5496 
o|contracted procedure: k5506 
o|contracted procedure: k5518 
o|contracted procedure: k5524 
o|contracted procedure: k5531 
o|contracted procedure: k5542 
o|contracted procedure: k5555 
o|contracted procedure: k5548 
o|contracted procedure: k5587 
o|contracted procedure: k5595 
o|contracted procedure: k5591 
o|contracted procedure: k5607 
o|contracted procedure: k5615 
o|contracted procedure: k5618 
o|contracted procedure: k5628 
o|contracted procedure: k5634 
o|contracted procedure: k5641 
o|contracted procedure: k5625 
o|contracted procedure: k5686 
o|contracted procedure: k5650 
o|contracted procedure: k5676 
o|contracted procedure: k5680 
o|contracted procedure: k5672 
o|contracted procedure: k5653 
o|contracted procedure: k5656 
o|contracted procedure: k5664 
o|contracted procedure: k5668 
o|contracted procedure: k5692 
o|contracted procedure: k5698 
o|contracted procedure: k5701 
o|contracted procedure: k5704 
o|contracted procedure: k5716 
o|contracted procedure: k5719 
o|contracted procedure: k5722 
o|contracted procedure: k5730 
o|contracted procedure: k5738 
o|contracted procedure: k6739 
o|contracted procedure: k5754 
o|contracted procedure: k6733 
o|contracted procedure: k5757 
o|contracted procedure: k6727 
o|contracted procedure: k5760 
o|contracted procedure: k5769 
o|contracted procedure: k5778 
o|contracted procedure: k5793 
o|contracted procedure: k5808 
o|contracted procedure: k5823 
o|contracted procedure: k6277 
o|contracted procedure: k5852 
o|contracted procedure: k6076 
o|contracted procedure: k5864 
o|contracted procedure: k5880 
o|contracted procedure: k5900 
o|contracted procedure: k5915 
o|contracted procedure: k5923 
o|contracted procedure: k5932 
o|contracted procedure: k5962 
o|inlining procedure: k5953 
o|contracted procedure: k5976 
o|contracted procedure: k5972 
o|inlining procedure: k5953 
o|contracted procedure: k5983 
o|inlining procedure: k5953 
o|contracted procedure: k5993 
o|contracted procedure: k5997 
o|contracted procedure: k6004 
o|contracted procedure: k6007 
o|contracted procedure: k6013 
o|contracted procedure: k6035 
o|contracted procedure: k6038 
o|contracted procedure: k6045 
o|contracted procedure: k6090 
o|contracted procedure: k6104 
o|contracted procedure: k6086 
o|contracted procedure: k6115 
o|contracted procedure: k6154 
o|contracted procedure: k6157 
o|contracted procedure: k6165 
o|contracted procedure: k6169 
o|contracted procedure: k6173 
o|contracted procedure: k6181 
o|contracted procedure: k6185 
o|contracted procedure: k6189 
o|contracted procedure: k6130 
o|contracted procedure: k6195 
o|contracted procedure: k6201 
o|contracted procedure: k6213 
o|contracted procedure: k6235 
o|contracted procedure: k6231 
o|contracted procedure: k6216 
o|contracted procedure: k6219 
o|contracted procedure: k6227 
o|contracted procedure: k6247 
o|contracted procedure: k6254 
o|contracted procedure: k6274 
o|contracted procedure: k6311 
o|contracted procedure: k6307 
o|contracted procedure: k6303 
o|contracted procedure: k6326 
o|contracted procedure: k6348 
o|contracted procedure: k6344 
o|contracted procedure: k6329 
o|contracted procedure: k6332 
o|contracted procedure: k6340 
o|contracted procedure: k6436 
o|contracted procedure: k6357 
o|contracted procedure: k6379 
o|contracted procedure: k6389 
o|contracted procedure: k6393 
o|contracted procedure: k6382 
o|contracted procedure: k6432 
o|contracted procedure: k6399 
o|contracted procedure: k6426 
o|contracted procedure: k6405 
o|contracted procedure: k6421 
o|contracted procedure: k6411 
o|contracted procedure: k6721 
o|contracted procedure: k6451 
o|contracted procedure: k6457 
o|contracted procedure: k6716 
o|contracted procedure: k6461 
o|contracted procedure: k6707 
o|contracted procedure: k6470 
o|contracted procedure: k6491 
o|contracted procedure: k6610 
o|contracted procedure: k6497 
o|contracted procedure: k6536 
o|contracted procedure: k6510 
o|contracted procedure: k6518 
o|contracted procedure: k6532 
o|contracted procedure: k6525 
o|contracted procedure: k6522 
o|contracted procedure: k6539 
o|contracted procedure: k6606 
o|contracted procedure: k6550 
o|contracted procedure: k6564 
o|contracted procedure: k6600 
o|contracted procedure: k6574 
o|contracted procedure: k6596 
o|contracted procedure: k6590 
o|contracted procedure: k6586 
o|contracted procedure: k6578 
o|contracted procedure: k6582 
o|contracted procedure: k6654 
o|contracted procedure: k6638 
o|contracted procedure: k6650 
o|contracted procedure: k6642 
o|contracted procedure: k6646 
o|contracted procedure: k6671 
o|contracted procedure: k6703 
o|contracted procedure: k6677 
o|contracted procedure: k6689 
o|contracted procedure: k6699 
o|contracted procedure: k6713 
o|contracted procedure: k6745 
o|contracted procedure: k6827 
o|contracted procedure: k6763 
o|contracted procedure: k6766 
o|contracted procedure: k6778 
o|contracted procedure: k6787 
o|contracted procedure: k6795 
o|contracted procedure: k6791 
o|contracted procedure: k6804 
o|contracted procedure: k6823 
o|contracted procedure: k6879 
o|contracted procedure: k6845 
o|contracted procedure: k6854 
o|contracted procedure: k6873 
o|contracted procedure: k6869 
o|contracted procedure: k6865 
o|contracted procedure: k6962 
o|contracted procedure: k6993 
o|contracted procedure: k7019 
o|contracted procedure: k7031 
o|contracted procedure: k7111 
o|contracted procedure: k7055 
o|contracted procedure: k7070 
o|contracted procedure: k7090 
o|contracted procedure: k7107 
o|contracted procedure: k6915 
o|contracted procedure: k6924 
o|contracted procedure: k6952 
o|contracted procedure: k6930 
o|contracted procedure: k7120 
o|contracted procedure: k7132 
o|contracted procedure: k7139 
o|contracted procedure: k7150 
o|contracted procedure: k7600 
o|contracted procedure: k7165 
o|contracted procedure: k7594 
o|contracted procedure: k7168 
o|contracted procedure: k7579 
o|contracted procedure: k7174 
o|contracted procedure: k7231 
o|contracted procedure: k7240 
o|contracted procedure: k7246 
o|contracted procedure: k7253 
o|contracted procedure: k7276 
o|contracted procedure: k7285 
o|contracted procedure: k7305 
o|contracted procedure: k7308 
o|contracted procedure: k7311 
o|contracted procedure: k7393 
o|contracted procedure: k7314 
o|contracted procedure: k7329 
o|contracted procedure: k7335 
o|contracted procedure: k7348 
o|contracted procedure: k7352 
o|contracted procedure: k7355 
o|contracted procedure: k7378 
o|contracted procedure: k7364 
o|contracted procedure: k7374 
o|contracted procedure: k7381 
o|contracted procedure: k7387 
o|contracted procedure: k7402 
o|contracted procedure: k7415 
o|contracted procedure: k7408 
o|contracted procedure: k7421 
o|contracted procedure: k7427 
o|contracted procedure: k7433 
o|contracted procedure: k7442 
o|contracted procedure: k7451 
o|contracted procedure: k7460 
o|contracted procedure: k7469 
o|contracted procedure: k7478 
o|contracted procedure: k7487 
o|contracted procedure: k7509 
o|contracted procedure: k7512 
o|contracted procedure: k7575 
o|contracted procedure: k7537 
o|contracted procedure: k7571 
o|contracted procedure: k7546 
o|contracted procedure: k7563 
o|contracted procedure: k7567 
o|contracted procedure: k7585 
o|contracted procedure: k7615 
o|contracted procedure: k7627 
o|contracted procedure: k7648 
o|contracted procedure: k7706 
o|contracted procedure: k7665 
o|contracted procedure: k7671 
o|contracted procedure: k7699 
o|contracted procedure: k7695 
o|contracted procedure: k7718 
o|contracted procedure: k7724 
o|contracted procedure: k7747 
o|contracted procedure: k7753 
o|contracted procedure: k7756 
o|contracted procedure: k7803 
o|contracted procedure: k7762 
o|contracted procedure: k7774 
o|contracted procedure: k7777 
o|contracted procedure: k7784 
o|contracted procedure: k7792 
o|contracted procedure: k7796 
o|contracted procedure: k7833 
o|contracted procedure: k7839 
o|contracted procedure: k7845 
o|contracted procedure: k7857 
o|contracted procedure: k7870 
o|contracted procedure: k7873 
o|contracted procedure: k7885 
o|contracted procedure: k7913 
o|contracted procedure: k7919 
o|contracted procedure: k7937 
o|contracted procedure: k7953 
o|contracted procedure: k7943 
o|contracted procedure: k7962 
o|contracted procedure: k7983 
o|contracted procedure: k8076 
o|contracted procedure: k8000 
o|contracted procedure: k8029 
o|contracted procedure: k8041 
o|contracted procedure: k8238 
o|contracted procedure: k8247 
o|contracted procedure: k8254 
o|contracted procedure: k8262 
o|contracted procedure: k8274 
o|contracted procedure: k8283 
o|contracted procedure: k8290 
o|contracted procedure: k8294 
o|contracted procedure: k9512 
o|contracted procedure: k8419 
o|contracted procedure: k8499 
o|contracted procedure: k8495 
o|contracted procedure: k8427 
o|contracted procedure: k8431 
o|contracted procedure: k8423 
o|contracted procedure: k8415 
o|contracted procedure: k8439 
o|contracted procedure: k8442 
o|contracted procedure: k8457 
o|contracted procedure: k8453 
o|contracted procedure: k8449 
o|contracted procedure: k8466 
o|contracted procedure: k8469 
o|contracted procedure: k8472 
o|contracted procedure: k8480 
o|contracted procedure: k8488 
o|contracted procedure: k8511 
o|contracted procedure: k8514 
o|contracted procedure: k8521 
o|contracted procedure: k8525 
o|contracted procedure: k8550 
o|contracted procedure: k8556 
o|contracted procedure: k8563 
o|contracted procedure: k8574 
o|contracted procedure: k8580 
o|contracted procedure: k8595 
o|contracted procedure: k8591 
o|contracted procedure: k8587 
o|contracted procedure: k8610 
o|contracted procedure: k8661 
o|contracted procedure: k8621 
o|contracted procedure: k8633 
o|contracted procedure: k8629 
o|contracted procedure: k8625 
o|contracted procedure: k8617 
o|contracted procedure: k8649 
o|contracted procedure: k8655 
o|contracted procedure: k8667 
o|contracted procedure: k8706 
o|contracted procedure: k8678 
o|contracted procedure: k8690 
o|contracted procedure: k8686 
o|contracted procedure: k8682 
o|contracted procedure: k8674 
o|contracted procedure: k8698 
o|contracted procedure: k8712 
o|contracted procedure: k8726 
o|contracted procedure: k8722 
o|contracted procedure: k8737 
o|contracted procedure: k8733 
o|contracted procedure: k8740 
o|contracted procedure: k8763 
o|contracted procedure: k8865 
o|contracted procedure: k8861 
o|contracted procedure: k8771 
o|contracted procedure: k8857 
o|contracted procedure: k8853 
o|contracted procedure: k8779 
o|contracted procedure: k8845 
o|contracted procedure: k8849 
o|contracted procedure: k8787 
o|contracted procedure: k8838 
o|contracted procedure: k8827 
o|contracted procedure: k8795 
o|contracted procedure: k8803 
o|contracted procedure: k8799 
o|contracted procedure: k8791 
o|contracted procedure: k8783 
o|contracted procedure: k8775 
o|contracted procedure: k8767 
o|contracted procedure: k8759 
o|contracted procedure: k8819 
o|contracted procedure: k8823 
o|contracted procedure: k8815 
o|contracted procedure: k8811 
o|contracted procedure: k8869 
o|contracted procedure: k8873 
o|contracted procedure: k8882 
o|contracted procedure: k8888 
o|contracted procedure: k8895 
o|contracted procedure: k8967 
o|contracted procedure: k8908 
o|contracted procedure: k8959 
o|contracted procedure: k8911 
o|contracted procedure: k8926 
o|contracted procedure: k8930 
o|contracted procedure: k8945 
o|contracted procedure: k8956 
o|contracted procedure: k8952 
o|contracted procedure: k8942 
o|contracted procedure: k8973 
o|contracted procedure: k8990 
o|contracted procedure: k8996 
o|contracted procedure: k9002 
o|contracted procedure: k9013 
o|contracted procedure: k9022 
o|contracted procedure: k9025 
o|contracted procedure: k9041 
o|contracted procedure: k9034 
o|contracted procedure: k9048 
o|contracted procedure: k9060 
o|contracted procedure: k9069 
o|contracted procedure: k9087 
o|contracted procedure: k9111 
o|substituted constant variable: g13655 
o|contracted procedure: k9118 
o|contracted procedure: k9122 
o|contracted procedure: k9136 
o|contracted procedure: k9132 
o|contracted procedure: k9139 
o|contracted procedure: k9145 
o|contracted procedure: k9151 
o|contracted procedure: k9164 
o|contracted procedure: k9170 
o|contracted procedure: k9191 
o|contracted procedure: k9214 
o|contracted procedure: k9220 
o|contracted procedure: k9227 
o|contracted procedure: k9240 
o|contracted procedure: k9244 
o|contracted procedure: k9252 
o|contracted procedure: k9258 
o|contracted procedure: k9275 
o|contracted procedure: k9291 
o|contracted procedure: k9320 
o|contracted procedure: k9303 
o|contracted procedure: k9306 
o|contracted procedure: k9316 
o|contracted procedure: k9297 
o|contracted procedure: k9333 
o|contracted procedure: k9341 
o|contracted procedure: k9347 
o|contracted procedure: k9364 
o|contracted procedure: k9399 
o|contracted procedure: k9392 
o|contracted procedure: k9408 
o|contracted procedure: k9414 
o|contracted procedure: k9421 
o|contracted procedure: k9443 
o|contracted procedure: k9453 
o|contracted procedure: k9469 
o|contracted procedure: k9472 
o|contracted procedure: k9523 
o|contracted procedure: k9531 
o|contracted procedure: k9535 
o|contracted procedure: k9568 
o|contracted procedure: k9564 
o|contracted procedure: k9560 
o|contracted procedure: k9556 
o|contracted procedure: k9603 
o|contracted procedure: k9757 
o|contracted procedure: k9620 
o|contracted procedure: k9626 
o|contracted procedure: k9639 
o|contracted procedure: k9652 
o|contracted procedure: k9660 
o|contracted procedure: k9673 
o|contracted procedure: k9681 
o|contracted procedure: k9693 
o|contracted procedure: k9703 
o|contracted procedure: k9722 
o|contracted procedure: k9714 
o|contracted procedure: k9730 
o|contracted procedure: k9734 
o|contracted procedure: k9748 
o|contracted procedure: k9783 
o|contracted procedure: k9779 
o|contracted procedure: k9775 
o|contracted procedure: k9797 
o|contracted procedure: k9830 
o|contracted procedure: k9803 
o|contracted procedure: k9826 
o|contracted procedure: k9818 
o|contracted procedure: k9822 
o|contracted procedure: k9814 
o|contracted procedure: k9810 
o|contracted procedure: k9848 
o|contracted procedure: k9861 
o|contracted procedure: k9874 
o|contracted procedure: k9877 
o|contracted procedure: k9890 
o|contracted procedure: k9908 
o|contracted procedure: k9919 
o|contracted procedure: k10020 
o|contracted procedure: k9924 
o|contracted procedure: k9944 
o|contracted procedure: k9940 
o|contracted procedure: k9936 
o|contracted procedure: k9928 
o|contracted procedure: k9915 
o|contracted procedure: k9982 
o|contracted procedure: k9988 
o|contracted procedure: k9996 
o|contracted procedure: k10004 
o|contracted procedure: k9970 
o|contracted procedure: k9966 
o|contracted procedure: k9952 
o|contracted procedure: k9960 
o|contracted procedure: k10000 
o|contracted procedure: k10010 
o|contracted procedure: k10017 
o|contracted procedure: k10032 
o|contracted procedure: k10054 
o|contracted procedure: k9899 
o|contracted procedure: k9903 
o|contracted procedure: k10050 
o|contracted procedure: k10035 
o|contracted procedure: k10038 
o|contracted procedure: k10046 
o|contracted procedure: k10070 
o|contracted procedure: k10084 
o|contracted procedure: k10091 
o|contracted procedure: k10108 
o|contracted procedure: k10098 
o|contracted procedure: k10121 
o|contracted procedure: k10344 
o|contracted procedure: k10145 
o|contracted procedure: k10340 
o|contracted procedure: k10158 
o|contracted procedure: k10161 
o|contracted procedure: k10193 
o|contracted procedure: k10209 
o|contracted procedure: k10216 
o|contracted procedure: k10230 
o|contracted procedure: k10219 
o|contracted procedure: k10226 
o|contracted procedure: k10282 
o|contracted procedure: k10291 
o|contracted procedure: k10295 
o|contracted procedure: k10239 
o|contracted procedure: k10257 
o|contracted procedure: k10264 
o|contracted procedure: k10278 
o|contracted procedure: k10267 
o|contracted procedure: k10274 
o|contracted procedure: k10307 
o|contracted procedure: k10310 
o|contracted procedure: k10313 
o|contracted procedure: k10321 
o|contracted procedure: k10329 
o|contracted procedure: k10336 
o|contracted procedure: k10354 
o|contracted procedure: k10727 
o|contracted procedure: k10375 
o|contracted procedure: k10378 
o|contracted procedure: k10439 
o|contracted procedure: k10468 
o|contracted procedure: k10447 
o|contracted procedure: k10482 
o|contracted procedure: k10471 
o|contracted procedure: k10478 
o|contracted procedure: k10494 
o|contracted procedure: k10543 
o|contracted procedure: k10539 
o|contracted procedure: k10519 
o|contracted procedure: k10535 
o|contracted procedure: k10527 
o|contracted procedure: k10523 
o|contracted procedure: k10591 
o|contracted procedure: k10559 
o|contracted procedure: k10587 
o|contracted procedure: k10571 
o|contracted procedure: k10583 
o|contracted procedure: k10575 
o|contracted procedure: k10567 
o|contracted procedure: k10563 
o|contracted procedure: k10598 
o|contracted procedure: k10602 
o|contracted procedure: k10611 
o|contracted procedure: k10618 
o|contracted procedure: k10634 
o|contracted procedure: k10623 
o|contracted procedure: k10630 
o|contracted procedure: k10639 
o|contracted procedure: k10645 
o|contracted procedure: k10651 
o|contracted procedure: k10657 
o|contracted procedure: k10663 
o|contracted procedure: k10675 
o|contracted procedure: k10690 
o|contracted procedure: k10701 
o|contracted procedure: k10723 
o|contracted procedure: k10737 
o|contracted procedure: k10743 
o|contracted procedure: k10746 
o|contracted procedure: k10753 
o|contracted procedure: k10779 
o|contracted procedure: k10763 
o|contracted procedure: k10771 
o|contracted procedure: k10767 
o|contracted procedure: k10789 
o|contracted procedure: k10795 
o|contracted procedure: k10798 
o|contracted procedure: k10805 
o|contracted procedure: k10812 
o|contracted procedure: k10829 
o|contracted procedure: k10832 
o|contracted procedure: k10838 
o|contracted procedure: k10845 
o|contracted procedure: k10855 
o|contracted procedure: k10882 
o|contracted procedure: k10904 
o|contracted procedure: k10926 
o|contracted procedure: k10954 
o|contracted procedure: k10964 
o|contracted procedure: k10978 
o|contracted procedure: k10967 
o|contracted procedure: k10974 
o|contracted procedure: k10991 
o|contracted procedure: k10994 
o|contracted procedure: k11002 
o|contracted procedure: k11044 
o|contracted procedure: k11152 
o|contracted procedure: k11052 
o|contracted procedure: k11063 
o|contracted procedure: k11076 
o|contracted procedure: k11087 
o|inlining procedure: k11080 
o|inlining procedure: k11080 
o|contracted procedure: k11148 
o|contracted procedure: k11108 
o|contracted procedure: k11132 
o|contracted procedure: k11142 
o|contracted procedure: k11138 
o|contracted procedure: k11128 
o|contracted procedure: k11169 
o|contracted procedure: k11186 
o|contracted procedure: k11203 
o|contracted procedure: k11220 
o|contracted procedure: k11270 
o|contracted procedure: k11274 
o|contracted procedure: k11266 
o|contracted procedure: k11288 
o|contracted procedure: k11294 
o|inlining procedure: k11285 
o|contracted procedure: k11303 
o|contracted procedure: k11313 
o|contracted procedure: k11317 
o|contracted procedure: k11320 
o|contracted procedure: k11327 
o|contracted procedure: k11343 
o|contracted procedure: k11346 
o|contracted procedure: k11350 
o|contracted procedure: k11375 
o|contracted procedure: k11395 
o|contracted procedure: k11400 
o|contracted procedure: k11412 
o|contracted procedure: k11424 
o|contracted procedure: k11430 
o|contracted procedure: k11447 
o|contracted procedure: k11436 
o|contracted procedure: k11443 
o|contracted procedure: k11454 
o|contracted procedure: k11457 
o|contracted procedure: k11379 
o|contracted procedure: k11383 
o|contracted procedure: k11387 
o|contracted procedure: k11356 
o|contracted procedure: k11367 
o|contracted procedure: k11371 
o|contracted procedure: k11469 
o|contracted procedure: k11472 
o|contracted procedure: k11475 
o|contracted procedure: k11483 
o|contracted procedure: k11491 
o|contracted procedure: k11535 
o|contracted procedure: k11548 
o|contracted procedure: k11563 
o|contracted procedure: k11569 
o|contracted procedure: k11602 
o|contracted procedure: k11598 
o|contracted procedure: k11594 
o|contracted procedure: k11582 
o|contracted procedure: k11590 
o|contracted procedure: k11586 
o|contracted procedure: k11626 
o|contracted procedure: k11636 
o|contracted procedure: k11684 
o|contracted procedure: k11644 
o|contracted procedure: k11648 
o|contracted procedure: k11665 
o|contracted procedure: k11661 
o|contracted procedure: k11651 
o|contracted procedure: k11668 
o|contracted procedure: k11674 
o|contracted procedure: k11692 
o|contracted procedure: k11695 
o|contracted procedure: k11706 
o|contracted procedure: k11710 
o|contracted procedure: k11720 
o|contracted procedure: k11730 
o|contracted procedure: k11739 
o|contracted procedure: k11872 
o|contracted procedure: k11752 
o|contracted procedure: k11758 
o|contracted procedure: k11768 
o|contracted procedure: k11771 
o|contracted procedure: k11780 
o|contracted procedure: k11793 
o|contracted procedure: k11806 
o|contracted procedure: k11842 
o|contracted procedure: k11812 
o|contracted procedure: k11818 
o|contracted procedure: k11831 
o|contracted procedure: k11848 
o|contracted procedure: k11859 
o|contracted procedure: k11884 
o|contracted procedure: k11891 
o|contracted procedure: k11899 
o|contracted procedure: k11911 
o|contracted procedure: k11933 
o|contracted procedure: k11929 
o|contracted procedure: k11914 
o|contracted procedure: k11917 
o|contracted procedure: k11925 
o|contracted procedure: k12003 
o|contracted procedure: k11942 
o|contracted procedure: k11948 
o|contracted procedure: k11999 
o|contracted procedure: k11955 
o|contracted procedure: k11961 
o|contracted procedure: k11967 
o|contracted procedure: k11974 
o|contracted procedure: k12020 
o|contracted procedure: k12038 
o|contracted procedure: k12030 
o|contracted procedure: k12052 
o|contracted procedure: k12081 
o|contracted procedure: k12091 
o|contracted procedure: k12100 
o|contracted procedure: k12113 
o|contracted procedure: k12116 
o|contracted procedure: k12128 
o|contracted procedure: k12131 
o|contracted procedure: k12134 
o|contracted procedure: k12142 
o|contracted procedure: k12150 
o|simplifications: ((if . 6) (let . 290)) 
o|replaced variables: 9 
o|removed binding forms: 911 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest395398 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest395398 0 
o|substituted constant variable: r415913695 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest541543 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest541543 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest572574 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest572574 0 
o|contracted procedure: k4385 
o|inlining procedure: k5170 
o|inlining procedure: k5170 
o|inlining procedure: k6370 
o|inlining procedure: k6514 
o|inlining procedure: k6514 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest14031407 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest14031407 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest14031407 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest14031407 0 
o|inlining procedure: k7499 
o|inlining procedure: k9462 
o|contracted procedure: k10944 
o|inlining procedure: k11278 
o|inlining procedure: k11278 
o|inlining procedure: k11278 
o|inlining procedure: k11640 
o|inlining procedure: k11640 
o|simplifications: ((let . 1)) 
o|removed binding forms: 17 
o|removed conditional forms: 1 
o|substituted constant variable: r750013886 
o|substituted constant variable: r750013886 
o|substituted constant variable: r946313900 
o|removed binding forms: 6 
o|removed conditional forms: 1 
o|removed binding forms: 2 
o|direct leaf routine/allocation: chicken.syntax#lookup 0 
o|direct leaf routine/allocation: g561562 0 
o|direct leaf routine/allocation: loop790 0 
o|direct leaf routine/allocation: g13051306 0 
o|direct leaf routine/allocation: g13941395 0 
o|direct leaf routine/allocation: loop1433 0 
o|direct leaf routine/allocation: loop1448 0 
o|direct leaf routine/allocation: g15901591 0 
o|direct leaf routine/allocation: g15991600 0 
o|direct leaf routine/allocation: assq-reverse1512 0 
o|direct leaf routine/allocation: loop2606 0 
o|direct leaf routine/allocation: g23652374 15 
o|contracted procedure: "(expand.scm:100) k3749" 
o|contracted procedure: "(expand.scm:182) k4230" 
o|contracted procedure: "(expand.scm:194) k4269" 
o|contracted procedure: "(expand.scm:196) k4278" 
o|contracted procedure: "(expand.scm:278) k4608" 
o|contracted procedure: "(expand.scm:280) k4825" 
o|converted assignments to bindings: (loop790) 
o|contracted procedure: "(expand.scm:480) k5766" 
o|converted assignments to bindings: (loop1433) 
o|converted assignments to bindings: (loop1448) 
o|contracted procedure: "(expand.scm:819) k7499" 
o|contracted procedure: "(expand.scm:888) k7898" 
o|contracted procedure: "(expand.scm:888) k7904" 
o|contracted procedure: "(expand.scm:907) k8006" 
o|contracted procedure: "(expand.scm:908) k8009" 
o|converted assignments to bindings: (loop2606) 
o|contracted procedure: "(expand.scm:1467) k10325" 
o|simplifications: ((if . 1) (let . 4)) 
o|removed binding forms: 13 
o|direct leaf routine/allocation: comp1038 0 
o|contracted procedure: "(expand.scm:508) k5893" 
o|contracted procedure: "(expand.scm:524) k5956" 
o|contracted procedure: "(expand.scm:520) k6016" 
o|contracted procedure: "(expand.scm:502) k6048" 
o|contracted procedure: "(expand.scm:503) k6054" 
o|contracted procedure: "(expand.scm:504) k6060" 
o|contracted procedure: "(expand.scm:505) k6066" 
o|contracted procedure: "(expand.scm:590) k6479" 
o|contracted procedure: "(expand.scm:617) k6616" 
o|contracted procedure: "(expand.scm:620) k6628" 
o|contracted procedure: "(expand.scm:624) k6660" 
o|simplifications: ((if . 2)) 
o|removed binding forms: 11 
o|customizable procedures: (g17901799 map-loop17841836 expand1887 map-loop18951913 test1858 err1857 k11557 k11654 g20122021 map-loop20062033 loop2103 expand2231 map-loop23592380 k10243 expand2328 expand2402 map-loop24242442 k9932 k9985 map-loop24582476 g25532554 g25462547 walk2490 walk12491 simplify2492 k9517 loop2867 k9125 k9078 doloop28162817 k8715 k8508 map-loop26822699 loop2595 chicken.syntax#make-er/ir-transformer mirror-rename1513 k7811 k7819 k7827 doloop15471548 k7293 test1418 k7317 walk1458 doloop14731474 err1419 loop1339 loop1352 loop1369 outstr1350 loop1316 mwalk1292 k6464 fini/syntax1040 loop21262 loop1246 k6366 loop1202 map-loop12151232 fini1039 foldl11301134 map-loop11071146 k6151 map-loop11551182 k5887 loop21062 k5938 expand1041 loop1059 map-loop947964 map-loop9761000 k5292 k5468 k5381 k5342 chicken.syntax#macro-alias k5305 loop823 err812 g845854 map-loop839867 k5007 k5019 loop777 k4614 k4769 g742743 loop655 expand600 map-loop682700 map-loop709726 call-handler599 k4419 copy618 loop588 loop519 g516517 loop1504 map-loop406423 for-each-loop432459 map-loop469490 doloop384385 walk354) 
o|calls to known targets: 339 
o|identified direct recursive calls: f_3970 1 
o|identified direct recursive calls: f_4018 1 
o|unused rest argument: rest395398 f_3924 
o|identified direct recursive calls: f_4144 2 
o|unused rest argument: rest541543 f_4198 
o|unused rest argument: rest572574 f_4264 
o|identified direct recursive calls: f_4315 1 
o|identified direct recursive calls: f_4664 1 
o|identified direct recursive calls: f_4733 1 
o|identified direct recursive calls: f_4919 3 
o|identified direct recursive calls: f_4991 2 
o|identified direct recursive calls: f_5645 1 
o|identified direct recursive calls: f_6208 1 
o|identified direct recursive calls: f_6321 1 
o|identified direct recursive calls: f_6294 1 
o|identified direct recursive calls: f_6758 1 
o|identified direct recursive calls: f_6840 1 
o|identified direct recursive calls: f_6919 1 
o|identified direct recursive calls: f_7229 1 
o|identified direct recursive calls: f_7274 1 
o|identified direct recursive calls: f_7300 1 
o|identified direct recursive calls: f_7622 1 
o|identified direct recursive calls: f_7710 1 
o|identified direct recursive calls: f_7932 1 
o|identified direct recursive calls: f_7957 1 
o|identified direct recursive calls: f_8242 1 
o|identified direct recursive calls: f_8278 1 
o|identified direct recursive calls: f_9106 1 
o|identified direct recursive calls: f_10027 1 
o|identified direct recursive calls: f_10079 1 
o|identified direct recursive calls: f_10302 1 
o|identified direct recursive calls: f_11906 1 
o|fast box initializations: 69 
o|fast global references: 21 
o|fast global assignments: 5 
o|dropping unused closure argument: f_3718 
o|dropping unused closure argument: f_3735 
o|dropping unused closure argument: f_4919 
o|dropping unused closure argument: f_7229 
o|dropping unused closure argument: f_7274 
o|dropping unused closure argument: f_7607 
o|dropping unused closure argument: f_7932 
*/
/* end of file */
